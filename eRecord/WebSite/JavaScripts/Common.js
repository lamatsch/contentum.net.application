﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />

(function () {
    Sys.Browser.WebKit = {};
    if (navigator.userAgent.indexOf('WebKit/') > -1) {
        Sys.Browser.agent = Sys.Browser.WebKit;
        Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);
        Sys.Browser.name = 'WebKit';
    }
})();


//Több javascript fájlban és oldalon használható függvények és osztályok kerülnek ide
function swapByName(obj, newImageName) 
{
    var myobj = document.getElementById(obj); 
    if (myobj) 
    {
        var fileNameStartIndex = myobj.src.lastIndexOf("/");
        var newUrl = myobj.src.substring(0,fileNameStartIndex+1)+newImageName;
        
        myobj.src = newUrl;            
    }
}

var IsOnTestPage = window.testpage ? true : false;
    
Type.registerNamespace("Utility");

Type.registerNamespace("Utility.Constants");

Utility.Constants.Database = {
    yes : '1',
    no : '0'
}

//String műveletek
Utility.String = function()
{
}

Utility._importedScriptsList = new Array();

Utility.$import = function(scriptPath)
{
    if(!Array.contains(Utility._importedScriptsList,scriptPath.toLowerCase()))
    {
        Array.add(Utility._importedScriptsList,scriptPath.toLowerCase());
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = scriptPath;
        document.body.appendChild(script);
    }  
}

Utility.String.DoubleLetters = new Array('cs','dz','gy','ly','ny','sz','ty','zs');
Utility.String.TrippleLetters = new Array('ccs','ddz','ggy','lly','nny','ssz','tty','zzs','dzs');

Utility.String.SplitEnd = function(text)
{
    var l = text.length;
    if(l > 0)
    {
        //Tripla betű ellenőrzés
        if(l >= 3)
        {
            var end = text.substring(l-3,l).toLowerCase();
            if(Array.indexOf(Utility.String.TrippleLetters,end) > -1)
            {
                text = text.substring(0,l-3);
                return text;
            }
        }
        //Dupla betű ellenőrzés
        if(l >= 2)
        {
            var end = text.substring(l-2,l).toLowerCase();
            if(Array.indexOf(Utility.String.DoubleLetters,end) > -1)
            {
                text = text.substring(0,l-2);
                return text;
            }
        }
        
        text = text.substring(0,l-1);
        return text;
    }
    
    return text;
};

Utility.String.StartWith = function(source, target)
{
    return Utility.String.StartWith(source, target, false);
};
    
Utility.String.StartWith = function(source, target, multipleLetters)
{   
    if(typeof(Utility.PartnerAutoComplete) =='function')
    {
        source = Utility.PartnerAutoComplete.CutForras(source);
    }
    if(multipleLetters)
    {
        if(source.length > target.length)
        {
            source = Utility.String.SplitEnd(source);
        }
    }
    if(source.toLowerCase().indexOf(target.toLowerCase()) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
};

Utility.String.FindCommonStart = function(source, target)
{   
    while(source.length > -1 && target.length > -1)
    {
        if(source.length > target.length)
        {
           if(Utility.String.StartWith(source,target,true))
           {
                return target;
           }
           
           target = Utility.String.SplitEnd(target);
        }
        
        if(source.length < target.length)
        {
           if(Utility.String.StartWith(target,source,true))
           {
                return source;
           }
           
           source = Utility.String.SplitEnd(source);
        }
        
        if(source.length == target.length)
        {
           if(source.toLowerCase() == target.toLowerCase())
           {
              return source;
           }
           else
           {
                source = Utility.String.SplitEnd(source);
           }
        }
    }
    
    return '';
};

Utility.String.registerClass('Utility.String');


//Selection műveletek
Utility.Selection = function()
{
}

Utility.Selection.deleteSelectedText = function(oTextBox)
{
    var selectedText = Utility.Selection.getSelectedText();
    if(selectedText != '')
    {
        var text = oTextBox.value;
        var cutIndex = text.length - selectedText.length;
        if(cutIndex > -1)
        {
            text = text.substring(0,cutIndex);
            oTextBox.value = text;
            var endIndex = text.length;
            Utility.Selection.textboxSelect(oTextBox,endIndex,endIndex);
         }
    }
};

Utility.Selection.textboxSelect = function (oTextbox, iStart, iEnd) 
{

    switch(arguments.length)
    {
        case 1:
        break;

        case 2:
        iEnd = oTextbox.value.length;
       /* falls through */
       
        case 3:          
        if (Sys.Browser.agent == Sys.Browser.InternetExplorer) 
        {
           var oRange = oTextbox.createTextRange();
           oRange.moveStart("character", iStart);
           oRange.moveEnd("character", -oTextbox.value.length + iEnd);      
           oRange.select();                                              
        } 
        else if (Sys.Browser.agent == Sys.Browser.Firefox)
        {
           oTextbox.setSelectionRange(iStart, iEnd);
        }                    
   }

};

Utility.Selection.getSelectedText = function()
{
    if (window.getSelection){
        txt = window.getSelection().toString();
    }
    else if (document.getSelection) {
        txt = document.getSelection();
    }
    else if (document.selection){
        txt = document.selection.createRange().text;
    }
    else return '';
    return txt;
 };
 
Utility.Selection.getSelectionStart = function(o) 
{
    if (o.createTextRange) 
    {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    }    
    else return o.selectionStart
 };

Utility.Selection.registerClass('Utility.Selection');

//Ui (user interface) műveletek
Utility.UI = function()
{
}

Utility.UI.SetElementOpacity = function(element, value)
{
    var floatValue = parseFloat(value);
    if(element && !isNaN(floatValue) && floatValue>=0 && floatValue<=1)
    {
        if (element.filters) {
            var filters = element.filters;
            var createFilter = true;
            if (filters.length !== 0) {
                var alphaFilter = filters['DXImageTransform.Microsoft.Alpha'];
                if (alphaFilter) {
                    createFilter = false;
                    alphaFilter.opacity = floatValue * 100;
                }
            }
            if (createFilter) {
                element.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + (floatValue * 100) + ')';
            }
        }
        else {
            element.style.opacity = floatValue;
        }
    }
};

Utility.UI.registerClass('Utility.UI');
//Cookie műveletek
Utility.Cookie = function()
{
}

Utility.Cookie.setCookie = function(cookieName,cookieValue,expiresSecs)
{
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime( today.getTime() );

   if ( expiresSecs )
   {
      expiresSecs = expiresSecs * 1000;
   }

   var expires_date = new Date( today.getTime() + (expiresSecs) );

   document.cookie = cookieName + '=' +escape( cookieValue ) +
   ( ( expiresSecs ) ? ';expires=' + expires_date.toGMTString() : '' );
};

Utility.Cookie.getCookie = function(cookieName)
{
    var cookie = document.cookie;
    if (cookie.length>0)
    {
        c_start=cookie.indexOf(cookieName + '=');
        if (c_start!=-1)
        { 
            c_start=c_start + cookieName.length+1; 
            c_end=cookie.indexOf(';',c_start);
            if (c_end==-1) c_end=document.cookie.length;
            return unescape(cookie.substring(c_start,c_end));
        } 
  }
  return '';
};

Utility.Cookie.deleteCookie = function(cookieName)
{
   var cookieValue = Utility.Cookie.getCookie(cookieName);
   if ( cookieValue && cookieValue!= '' ) document.cookie = name + '=' +
   ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
};

Utility.Cookie.registerClass('Utility.Cookie');

//Popup műveletek
Utility.Popup = function()
{
}

Utility.Popup.getParentWindow = function()
{
    if(!window)
    {
        return null;
    }
    if(window.dialogArguments)
    {
        if(!window.dialogArguments.parentWindow)
            return null;
        return window.dialogArguments.parentWindow;
    }
    else
    {
        if(!window.opener)
            return null;
        return window.opener;    
    }
}


Utility.Popup.registerClass('Utility.Popup');

//Select Object

Utility.DomElement = {};

Utility.DomElement.Select = {};

Utility.DomElement.Select.Add = function(select, option, insertFirstPosition)
{
    if(!select || !select.options || ! option)
        return 0;
    
    var options = select.options;
    var optionsLength = options.length;
    
    if(arguments.length < 3) insertFirstPosition = false;
    
    if(!insertFirstPosition || optionsLength == 0)
    {
        try
        {
           select.add(option,null); // standards compliant
           return 1;
        }
        catch(ex)
        {
           select.add(option); // IE only
           return 1;
        }
    }
    else
    {
        select.add(option,0); // standards compliant
        return 1;
    }
}

Utility.DomElement.Select.Clear = function(select) {
    if (!select)
        return;

    var options = select.options;

    if (!options || !options.length)
        return;

    var optionsLength = options.length;

    for (i = 0; i < optionsLength; i++) {
        //Note: Always remove(0) and NOT remove(i)			 
        select.remove(0);
    }
}

Utility.DomElement.Select.createItemToolTips = function(select) {
    if (!select)
        return;

    for (var i = 0; i < select.options.length; i++) {
        select.options[i].title = select.options[i].innerText;
    }

    if (select.selectedIndex && select.selectedIndex >= 0) {
        select.title = select.options[select.selectedIndex].title;
    }
}

Utility.FixMemoryLeaks = function()
{
    if(typeof(AjaxControlToolkit) != 'undefined' && (AjaxControlToolkit.ValidatorCalloutBehavior != undefined
    || AjaxControlToolkit.CascadingDropDownBehavior != undefined || AjaxControlToolkit.CollapsiblePanelBehavior))
    {
        var componets = Sys.Application.getComponents();
        for(var i=0; i<componets.length; i++)
        {
            var component = componets[i];
            var componentName = Object.getType(component).getName();
            if( componentName == 'AjaxControlToolkit.ValidatorCalloutBehavior')
            {
                component.add_disposing(Utility.OnValidatorCalloutDisposing);
            }
            if( componentName == 'AjaxControlToolkit.CascadingDropDownBehavior')
            {
                component.add_disposing(Utility.OnCascadingDropDownDisposing);
            }
            if( componentName == 'AjaxControlToolkit.CollapsiblePanelBehavior')
            {
                component.add_disposing(Utility.OnCollapsiblePanelDisposing);
            }
        }
    }
}



Utility.OnValidatorCalloutDisposing = function(sender, args)
{
    var elt = sender.get_element();
    if(elt)
    {
        elt.evaluationfunction = null;
    }
    
    if(sender.cleanPopupTable != undefined)
    {
        var popup = sender.cleanPopupTable;
        if(popup)
        {
            $clearHandlers(popup);
        }
        
        sender.cleanPopupTable = null;
    }
}

Utility.OnCascadingDropDownDisposing = function(sender, args)
{
    sender._parentElement = null;
}

Utility.OnCollapsiblePanelDisposing = function(sender, args)
{
    sender._childDiv = null;
}

Sys.Application.add_load(Utility.FixMemoryLeaks);

// ValidatorCallout eltüntetése, ha a validátor nincs engedélyezve (az eredeti fv-ek meghívásával)
Utility.OverrideValidatorCalloutFunctions = function() {
    if (typeof (AjaxControlToolkit) != 'undefined' && AjaxControlToolkit.ValidatorCalloutBehavior != undefined) {
        AjaxControlToolkit.ValidatorCalloutBehavior.prototype._originalOnFocus = AjaxControlToolkit.ValidatorCalloutBehavior.prototype._onfocus;
        AjaxControlToolkit.ValidatorCalloutBehavior.prototype._originalOnValidate = AjaxControlToolkit.ValidatorCalloutBehavior.prototype._onvalidate;

        AjaxControlToolkit.ValidatorCalloutBehavior.prototype._onfocus = function (e) {
            Utility.registerCalloutCancelButtons();
            var elt = this.get_element();
            if (elt.enabled != false) {
                return this._originalOnFocus(e);
            }
            else {
                if (this._highlightCssClass) {
                    Sys.UI.DomElement.removeCssClass(this._elementToValidate, this._highlightCssClass)
                }
                this.hide();
                return true;
            }
        };
        
        AjaxControlToolkit.ValidatorCalloutBehavior.prototype._onvalidate = function (val) {
            Utility.registerCalloutCancelButtons();
            var elt = this.get_element();
            if (elt.enabled != false) {
                return this._originalOnValidate(val);
            } else {
                if (this._highlightCssClass) {
                    Sys.UI.DomElement.removeCssClass(this._elementToValidate, this._highlightCssClass)
                }
                this._invalid = false;
                this.hide();
                return true;
            }
        };
    }
}

Utility.registerCalloutCancelButtons = function () {
    try {
        var ua = window.navigator.userAgent;
        var isIE = /MSIE|Trident/.test(ua);

        if (isIE) {
            return; //IE nél nem kell enforcolni a postback-et.
        }
        if (!window.jQuery) { return; }
        var elements = $('.ajax__validatorcallout_popup_table_row');

        for (var elementIndex = 0; elementIndex < elements.length; elementIndex++) {
            var currentElement = elements[elementIndex];
            var parent = currentElement.parentNode.parentNode;
            parent.onmouseover = function (e) {
                e.target.style.visibility = 'hidden';
            };
        }
    } catch (err) {
        console.error("registerCalloutCancelButtons error: " + err);
    }
}


Sys.Application.add_init(Utility.OverrideValidatorCalloutFunctions);

Utility.TryFireEvent = function ()
{
    if (typeof ($common) != 'undefined') {
        $common.tryFireEvent = function(element, eventName, properties) {
            try {
                if (document.createEventObject) {
                    var e = document.createEventObject();
                    $common.applyProperties(e, properties || {});
                    element.fireEvent("on" + eventName, e);
                    return true;
                } else if (document.createEvent) {
                    var def = $common.__DOMEvents[eventName];
                    if (def) {
                        var e = document.createEvent(def.eventGroup);
                        def.init(e, properties || {});
                        $common.applyProperties(e, properties || {});
                        element.dispatchEvent(e);
                        return true;
                    }
                }
            } catch (e) {
            }
            return false;
        }
    }
}

Sys.Application.add_init(Utility.TryFireEvent);

//Function Key

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();

// BUG_4651
//Kiszedve DatumIntervallum_SearchCalendarControl.ascx-ből
function OnClientShown(sender, args) {
    sender._popupBehavior._element.style.zIndex = 10005;
}