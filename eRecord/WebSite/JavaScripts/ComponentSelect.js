﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.ComponentSelect = function (element) {
    Utility.ComponentSelect.initializeBase(this, [element]);
    //Properties
    this.HiddenField_readOnlyComponents_id = '';
    this.HiddenField_disabledComponents_id = '';
    this.HiddenField_invisibleComponents_id = '';
    this.HiddenField_tabIndexComponents_id = '';
    this.HiddenField_userComponents_id = '';
    this.HiddenField_pageViewCustomTabOrder_id = '';
    this.Id_componentRights = '';
    this.ClickableComponentsClientIds = null;
    this.SaveButtonClientId = '';
    //Variables
    this.ReadOnlyComponents = new Array();
    this.DisabledComponents = new Array();
    this.InvisibleComponents = new Array();    
    this.UserComponents = null;
    this.mouseDownHandler = null;
    this.mouseUpHandler = null;
    this.mouseUpHandlerAdded = false;
    this.mouseMoveHandler = null;
    this.mouseMoveHandlerAdded = false;
    this.MouseDiv = null;
    this.componentCache = {};
}

var TabIndexComponents = new Map();
var useCustomTabOrder = '0';

Utility.ComponentSelect.ComponentState = function () {
}

Utility.ComponentSelect.ComponentState.prototype = {
    readWrite: 0,
    readOnly: 1,
    disabled: 2,
    invisible: 3,
    mouseOver: 4,
    mouseOut: 5

}

Utility.ComponentSelect.ComponentState.registerEnum("Utility.ComponentSelect.ComponentState", false);

Utility.ComponentSelect.prototype = {
    initialize: function () {
        Utility.ComponentSelect.callBaseMethod(this, 'initialize');
        var str_readOnlyComponents =
            document.getElementById(this.HiddenField_readOnlyComponents_id).value;
        var str_disabledComponents =
            document.getElementById(this.HiddenField_disabledComponents_id).value;
        var str_invisibleComponents = $get(this.HiddenField_invisibleComponents_id).value;

        useCustomTabOrder =
            document.getElementById(this.HiddenField_pageViewCustomTabOrder_id).value;

        var str_tabIndexComponents =
            document.getElementById(this.HiddenField_tabIndexComponents_id).value;

        var temp1 = new Array();
        if (str_readOnlyComponents != null && str_readOnlyComponents != "") {
            temp1 = str_readOnlyComponents.split(',');
            var i = 0;
            for (i = 0; i < temp1.length; i++) {
                this.addToReadWriteComponents(temp1[i]);
                this.addToReadOnlyComponents(temp1[i]);
            }
        }

        if (str_disabledComponents != null && str_disabledComponents != "") {
            var temp2 = new Array();
            temp2 = str_disabledComponents.split(',');
            var i = 0;
            for (i = 0; i < temp2.length; i++) {
                this.addToReadWriteComponents(temp2[i]);
                this.addToDisabledComponents(temp2[i]);
            }
        }

        if (str_invisibleComponents != null && str_invisibleComponents != "") {
            var temp3 = new Array();
            temp3 = str_invisibleComponents.split(',');
            var i = 0;
            for (i = 0; i < temp3.length; i++) {
                this.addToReadWriteComponents(temp3[i]);
                this.addToInvisibleComponents(temp3[i]);
            }
        }

        if (useCustomTabOrder) {
            if (str_tabIndexComponents != null && str_tabIndexComponents != "") {
                var temp4 = new Array();
                temp4 = str_tabIndexComponents.split(',');
                var i = 0;
                for (i = 0; i < temp4.length; i++) {
                    var temp5 = new Array();
                    temp5 = temp4[i].split('|');
                    this.addToTabIndexComponents(temp5[0], temp5[1]);
                }
                this.initDropdownsFromMap();
            }
            else {
                this.initDropdownsWithDefaults();
            }
        }

        if (this.ClickableComponentsClientIds) {
            for (var i = 0; i < this.ClickableComponentsClientIds.length; i++) {
                var componentId = this.ClickableComponentsClientIds[i];
                var component = $get(componentId);
                var userComponentId = this.getUserComponentId(componentId);
                if (component) {
                    $addHandler(component, 'click', Function.createDelegate(this, Function.createCallback(this.componentOnClick, userComponentId)));
                    component.onclick = 'return false;'
                    Sys.UI.DomElement.addCssClass(component, 'Component_Select');
                    this.componentCache[componentId] = component;
                    //$addHandler(component, 'mouseover', Function.createDelegate(this, Function.createCallback(this.componentOnMouseOver, userComponentId)));
                    //$addHandler(component, 'mouseout', Function.createDelegate(this, Function.createCallback(this.componentOnMouseOut, userComponentId)));
                }
            }
        }

        var saveButton = $get(this.SaveButtonClientId);
        if (saveButton) {
            $addHandler(saveButton, 'click', Function.createDelegate(this, this.sendBackComponentLists));
        }

        this.mouseDownHandler = Function.createDelegate(this, this.OnMouseDown);
        this.mouseUpHandler = Function.createDelegate(this, this.OnMouseUp);
        this.mouseMoveHandler = Function.createDelegate(this, this.OnMouseMove);
        $addHandler(document.body, 'mousedown', this.mouseDownHandler);

        this.MouseDiv = document.createElement('div');
        Sys.UI.DomElement.addCssClass(this.MouseDiv, 'MouseMoveRectangle');
    },



    OnMouseDown: function (e) {
        var ev = new Sys.UI.DomEvent(e);
        if (ev.button == Sys.UI.MouseButton.leftButton) {
            if (!this.mouseMoveHandlerAdded) {
                $addHandler(document.body, 'mousemove', this.mouseMoveHandler);
                this.mouseMoveHandlerAdded = true;
            }
            if (!this.mouseUpHandlerAdded) {
                $addHandler(document.body, 'mouseup', this.mouseUpHandler);
                this.mouseUpHandlerAdded = true;
            }
            this.MouseDiv.style.top = ev.clientY + document.documentElement.scrollTop;
            this.MouseDiv.style.left = ev.clientX + document.documentElement.scrollLeft;
            this.MouseDiv.style.width = '0px';
            this.MouseDiv.style.height = '0px';

            if (!this.MouseDiv.parentNode || !this.MouseDiv.parentNode.tagName)
                document.body.appendChild(this.MouseDiv);
        }
    },

    OnMouseUp: function (e) {
        if (this.mouseMoveHandlerAdded) {
            $removeHandler(document.body, 'mousemove', this.mouseMoveHandler);
            this.mouseMoveHandlerAdded = false;
        }
        if (this.mouseUpHandlerAdded) {
            $removeHandler(document.body, 'mouseup', this.mouseUpHandler);
            this.mouseUpHandlerAdded = false;
        }
        var bounds = Sys.UI.DomElement.getBounds(this.MouseDiv);

        if (this.MouseDiv.parentNode && this.MouseDiv.parentNode.tagName)
            document.body.removeChild(this.MouseDiv);

        if (bounds.height > 5 && bounds.width > 5) {
            if (this.ClickableComponentsClientIds) {
                var userComponentArray = new Array();
                for (var i = 0; i < this.ClickableComponentsClientIds.length; i++) {
                    var componentId = this.ClickableComponentsClientIds[i];
                    var component = this.componentCache[componentId];
                    if (component) {
                        var componentBounds = Sys.UI.DomElement.getBounds(component);
                        if (bounds.x <= componentBounds.x
                            && bounds.y <= componentBounds.y
                            && (bounds.x + bounds.width) >= (componentBounds.x + componentBounds.width)
                            && (bounds.y + bounds.height) >= (componentBounds.y + componentBounds.height)
                        ) {
                            var userComponentId = this.getUserComponentId(componentId);
                            if (!Array.contains(userComponentArray, userComponentId)) {
                                userComponentArray.push(userComponentId);
                                component.click();
                            }
                        }
                    }
                }
            }

        }

    },

    OnMouseMove: function (e) {
        var ev = new Sys.UI.DomEvent(e);
        var bounds = Sys.UI.DomElement.getBounds(this.MouseDiv);

        var cursorY = ev.clientY + document.documentElement.scrollTop;
        var cursorX = ev.clientX + document.documentElement.scrollLeft;

        if ((cursorY - bounds.y) >= 0) {
            this.MouseDiv.style.height = cursorY - bounds.y + 'px';
        }

        if ((cursorX - bounds.x) >= 0) {
            this.MouseDiv.style.width = cursorX - bounds.x + 'px';
        }
    },

    dispose: function () {

        if (this.mouseDownHandler) {
            $removeHandler(document.body, 'mousedown', this.mouseDownHandler);
        }

        if (this.mouseMoveHandler) {
            if (this.mouseMoveHandlerAdded) {
                $removeHandler(document.body, 'mousemove', this.mouseMoveHandler);
                this.mouseMoveHandlerAdded = false;
            }
        }

        if (this.mouseMoveHandler) {
            if (this.mouseUpHandlerAdded) {
                $removeHandler(document.body, 'mouseup', this.mouseUpHandler);
                this.mouseUpHandlerAdded = false;
            }
        }
        this.mouseDownHandler = null;
        this.mouseUpHandler = null;
        this.mouseMoveHandler = null;

        Utility.ComponentSelect.callBaseMethod(this, 'dispose');
    },

    getUserComponentId: function (componentId) {
        this.initalizeUserComponets();

        if (this.UserComponents[componentId]) {
            if (Object.getTypeName(this.UserComponents[componentId]) == 'String') {
                return this.UserComponents[componentId];
            }
        }
        return componentId;
    },

    initalizeUserComponets: function () {
        if (this.UserComponents == null) {
            this.UserComponents = {};
            var str_userComponents =
                $get(this.HiddenField_userComponents_id).value;
            var temp = new Array();
            if (str_userComponents != null && str_userComponents != '') {
                temp = str_userComponents.split(';');
                var i = 0;
                for (i = 0; i < temp.length; i++) {
                    if (temp[i] != null && temp[i] != '') {
                        var temp2 = temp[i].split(':');
                        var userComponetId = temp2[0];
                        this.UserComponents[userComponetId] = new Array();
                        var temp3 = temp2[1].split(',');
                        for (var j = 0; j < temp3.length; j++) {
                            var componetId = temp3[j];
                            this.UserComponents[userComponetId].push(componetId);
                            this.UserComponents[componetId] = userComponetId;
                        }
                    }
                }
            }


        }
    },

    componentOnClick: function (ev, componentId) {
        var componentRight;
        var radio1_id = this.Id_componentRights + "_0";
        if (document.getElementById(radio1_id).checked == true) { componentRight = "1"; }
        else if (document.getElementById(this.Id_componentRights + "_1").checked == true) { componentRight = "2"; }
        else if (document.getElementById(this.Id_componentRights + "_2").checked == true) { componentRight = "3"; }
        else if (document.getElementById(this.Id_componentRights + "_3").checked == true) { componentRight = "4"; }

        switch (componentRight) {
            case "1": this.addToReadWriteComponents(componentId); break;
            case "2": this.addToReadOnlyComponents(componentId); break;
            case "3": this.addToDisabledComponents(componentId); break;
            case "4": this.addToInvisibleComponents(componentId); break;
        }

        ev.preventDefault();
        ev.stopPropagation();
    },

    countElements: function () {
        var all = document.getElementsByTagName("*");
        var counter = 0;

        for (var i = 0, max = all.length; i < max; i++) {
            var control = all[i];

            if (control && control.id) {
                var skip = control.getAttribute('data-componentSelector');

                if (skip || control.id.toLowerCase().indexOf('radiobuttonlist_componentrights') !== -1)
                    continue;

                if (control.tagName.toLowerCase() == 'select' && !control.id.endsWith('tabIndexSelect')) {
                    counter++;
                }
                else if (control.tagName.toLowerCase() == 'input') {
                    var type = control.getAttribute('type');
                    if (type == 'text' || type == 'button'
                        || type == 'image'
                        || type == 'radio') {
                        counter++;
                    }
                }
            }
        }

        return counter;
    },

    initDropdownsWithDefaults: function () {
        var all = document.getElementsByTagName("*");
        var counter = 0;
        var maxTabIndex = this.countElements();

        for (var i = 0; i < all.length; i++) {
            var control = all[i];

            if (control && control.id) {
                var skip = control.getAttribute('data-componentSelector');

                if (skip || control.id.toLowerCase().indexOf('radiobuttonlist_componentrights') !== -1)
                    continue;

                if (control.tagName.toLowerCase() == 'select' && !control.id.endsWith('tabIndexSelect')) {
                    var newControl = document.createElement('select');
                    newControl.setAttribute("id", control.id + "_" + "tabIndexSelect");
                    control.parentNode.insertBefore(newControl, control.nextSibling);
                    this.addValuesToDropdown(newControl, maxTabIndex);
                    newControl.selectedIndex = counter;
                    this.addToTabIndexComponents(control.id, counter);
                    counter++;

                    newControl.addEventListener("change", Utility.ComponentSelect.prototype.dropDownChange);
                }
                else if (control.tagName.toLowerCase() == 'input') {
                    var type = control.getAttribute('type');
                    if (type == 'text' || type == 'button'
                        || type == 'image'
                        || type == 'radio') {
                        var newControl1 = document.createElement('select');
                        newControl1.setAttribute("id", control.id + "_" + "tabIndexSelect");
                        control.parentNode.insertBefore(newControl1, control.nextSibling);
                        this.addValuesToDropdown(newControl1, maxTabIndex);
                        newControl1.selectedIndex = counter;
                        this.addToTabIndexComponents(control.id, counter);
                        counter++;

                        newControl1.addEventListener("change", Utility.ComponentSelect.prototype.dropDownChange);
                    }
                }
            }
        }
    },

    initDropdownsFromMap: function () {

        var maxTabIndex = TabIndexComponents.size;

        TabIndexComponents.forEach(function (value, key, map) {
            try {
                var control = document.getElementById(key);
                var newControl1 = document.createElement('select');
                newControl1.setAttribute("id", control.id + "_" + "tabIndexSelect");
                control.parentNode.insertBefore(newControl1, control.nextSibling);                
                Utility.ComponentSelect.prototype.addValuesToDropdown(newControl1, maxTabIndex);                
                newControl1.selectedIndex = value;
                newControl1.addEventListener("change", Utility.ComponentSelect.prototype.dropDownChange);
            }
            catch (err) {
                console.log(err.message);
            }
        });
    },

    addValuesToDropdown: function (dropDown, count) {
        for (var i = 0, max = count; i < max; i++) {
            var z = document.createElement("option");
            z.setAttribute("value", i);
            var t = document.createTextNode(i);
            z.appendChild(t);
            dropDown.appendChild(z);
        }
    },

    dropDownChange: function (evt) {
        var changedControlId = this.id.replace("_tabIndexSelect", "");        
        var changedControlIndex = TabIndexComponents.get(changedControlId);
        var newIndex = this.selectedIndex;

        var otherControlId;
        var otherControlIndex;
        TabIndexComponents.forEach(function (value, key, map) {
            if (newIndex == value)
            {
                otherControlId = key;
                otherControlIndex = value;
            }            
        });

        if (otherControlId && this.id != otherControlId + '_tabIndexSelect') {
            document.getElementById(otherControlId +'_tabIndexSelect').selectedIndex = changedControlIndex;
            TabIndexComponents.set(otherControlId, changedControlIndex);
        }

        TabIndexComponents.set(changedControlId, this.selectedIndex);
    },



    componentOnMouseOver: function (ev, componentId) {
        this.setComponentStyle(componentId, Utility.ComponentSelect.ComponentState.mouseOver);
    },



    containsElement: function (stringarray, element) {
        var contains = false;
        for (i = 0; i < stringarray.length; i++) {
            if (element == stringarray[i]) { contains = true; break; }
        }
        return contains;
    },

    removeComponent: function (component_id) {
        if (this.containsElement(this.ReadOnlyComponents, component_id)) {
            for (i = 0; i < this.ReadOnlyComponents.length; i++) {
                if (component_id == this.ReadOnlyComponents[i]) {
                    this.ReadOnlyComponents.splice(i, 1);
                }
            }
        }
        if (this.containsElement(this.DisabledComponents, component_id)) {
            for (i = 0; i < this.DisabledComponents.length; i++) {
                if (component_id == this.DisabledComponents[i]) {
                    this.DisabledComponents.splice(i, 1);
                }
            }
        }

        if (this.containsElement(this.InvisibleComponents, component_id)) {
            for (i = 0; i < this.InvisibleComponents.length; i++) {
                if (component_id == this.InvisibleComponents[i]) {
                    this.InvisibleComponents.splice(i, 1);
                }
            }
        }
    },

    setComponentStyle: function (component_id, state) {
        // ellenorzes, benne van-e a userComponents-ben
        //        var str_userComponents =
        //        document.getElementById(this.HiddenField_userComponents_id).value;

        var isUserComponent = false;

        //        var temp1 = new Array();
        //        if (str_userComponents != null && str_userComponents != "") {
        //            temp1 = str_userComponents.split(';');
        //            var i = 0;
        //            for (i = 0; i < temp1.length; i++) {
        //                if (temp1[i] != null && temp1[i] != "") {
        //                    var temp2 = temp1[i].split(':');
        //                    var temp3 = temp2[1].split(',');
        //                    if (component_id == temp2[0]) {
        //                        //userComponent
        //                        isUserComponent = true;
        //                        var j = 0;
        //                        for (j = 0; j < temp3.length; j++) {
        //                            var comp = document.getElementById(temp3[j]);
        //                            this._setComponentStyle(comp, state)
        //                        }
        //                    }
        //                }
        //            }
        //        }

        this.initalizeUserComponets();

        if (this.UserComponents[component_id]) {
            var componets = this.UserComponents[component_id];
            if (Object.getTypeName(componets) == 'Array') {
                isUserComponent = true;
                for (var i = 0; i < componets.length; i++) {
                    var component = $get(componets[i]);
                    this._setComponentStyle(component, state)
                }
            }
        }

        if (!isUserComponent) {
            var component = $get(component_id);
            this._setComponentStyle(component, state);
        }

    },

    _setComponentStyle: function (component, state) {
        if (!component) return;

        if (state == Utility.ComponentSelect.ComponentState.mouseOver) {
            Sys.UI.DomElement.addCssClass(component, 'Component_HighLight');
            return;
        }

        if (state == Utility.ComponentSelect.ComponentState.mouseOut) {
            Sys.UI.DomElement.removeCssClass(component, 'Component_HighLight')
            return;
        }
        Sys.UI.DomElement.removeCssClass(component, 'Component_ReadWrite');
        Sys.UI.DomElement.removeCssClass(component, 'Component_ReadOnly');
        Sys.UI.DomElement.removeCssClass(component, 'Component_Disabled');
        Sys.UI.DomElement.removeCssClass(component, 'Component_Invisible');
        switch (state) {
            case Utility.ComponentSelect.ComponentState.readWrite:
                Sys.UI.DomElement.addCssClass(component, 'Component_ReadWrite');
                break;
            case Utility.ComponentSelect.ComponentState.readOnly:
                Sys.UI.DomElement.addCssClass(component, 'Component_ReadOnly');
                break;
            case Utility.ComponentSelect.ComponentState.disabled:
                Sys.UI.DomElement.addCssClass(component, 'Component_Disabled');
                break;
            case Utility.ComponentSelect.ComponentState.invisible:
                Sys.UI.DomElement.addCssClass(component, 'Component_Invisible');
                break;
        }
    },

    addToReadWriteComponents: function (component_id) {
        this.removeComponent(component_id);
        this.setComponentStyle(component_id, Utility.ComponentSelect.ComponentState.readWrite);
    },

    addToReadOnlyComponents: function (component_id) {
        if (this.containsElement(this.ReadOnlyComponents, component_id) == false) {
            this.removeComponent(component_id);
            this.ReadOnlyComponents.push(component_id);
            this.setComponentStyle(component_id, Utility.ComponentSelect.ComponentState.readOnly);
        }
        else {
            this.addToReadWriteComponents(component_id)
        }
    },

    addToTabIndexComponents: function (component_id, tabIndex) {
        TabIndexComponents.set(component_id, tabIndex);
    },

    valueTabIndexComponents: function () {
        return TabIndexComponents;
    },

    addToDisabledComponents: function (component_id) {
        if (this.containsElement(this.DisabledComponents, component_id) == false) {
            this.removeComponent(component_id);
            this.DisabledComponents.push(component_id);
            this.setComponentStyle(component_id, Utility.ComponentSelect.ComponentState.disabled);
        }
        else {
            this.addToReadWriteComponents(component_id)
        }
    },

    addToInvisibleComponents: function (component_id) {
        if (this.containsElement(this.InvisibleComponents, component_id) == false) {
            this.removeComponent(component_id);
            this.InvisibleComponents.push(component_id);
            this.setComponentStyle(component_id, Utility.ComponentSelect.ComponentState.invisible);
        }
        else {
            this.addToReadWriteComponents(component_id)
        }
    },

    sendBackComponentLists: function () {
        document.getElementById(this.HiddenField_readOnlyComponents_id).value =
            this.ReadOnlyComponents.join(",");
        document.getElementById(this.HiddenField_disabledComponents_id).value =
            this.DisabledComponents.join(",");
        $get(this.HiddenField_invisibleComponents_id).value = this.InvisibleComponents.join(",");

        if (useCustomTabOrder) {
            var strTab = '';
            TabIndexComponents.forEach(function (value, key, map) {
                strTab += key + '|' + value + ',';
            });
            strTab = strTab.slice(0, -1);
            document.getElementById(this.HiddenField_tabIndexComponents_id).value = strTab;
        }
    },

    componentOnMouseOver: function (ev, componentId) {
        this.setComponentStyle(componentId, Utility.ComponentSelect.ComponentState.mouseOver);
    },

    componentOnMouseOut: function (ev, componentId) {
        this.setComponentStyle(componentId, Utility.ComponentSelect.ComponentState.mouseOut);
    },

    //getter, setter methods
    get_HiddenField_readOnlyComponents_id: function () {
        return this.HiddenField_readOnlyComponents_id;
    },

    set_HiddenField_readOnlyComponents_id: function (value) {
        if (this.HiddenField_readOnlyComponents_id != value) {
            this.HiddenField_readOnlyComponents_id = value;
        }
    },

    get_HiddenField_disabledComponents_id: function () {
        return this.HiddenField_disabledComponents_id;
    },

    set_HiddenField_disabledComponents_id: function (value) {
        if (this.HiddenField_disabledComponents_id != value) {
            this.HiddenField_disabledComponents_id = value;
        }
    },

    get_HiddenField_invisibleComponents_id: function () {
        return this.HiddenField_invisibleComponents_id;
    },

    set_HiddenField_invisibleComponents_id: function (value) {
        if (this.HiddenField_invisibleComponents_id != value) {
            this.HiddenField_invisibleComponents_id = value;
        }
    },

    get_HiddenField_tabIndexComponents_id: function () {
        return this.HiddenField_tabIndexComponents_id;
    },

    set_HiddenField_tabIndexComponents_id: function (value) {
        if (this.HiddenField_tabIndexComponents_id != value) {
            this.HiddenField_tabIndexComponents_id = value;
        }
    },

    get_HiddenField_pageViewCustomTabOrder_id: function () {
        return this.HiddenField_pageViewCustomTabOrder_id;
    },

    set_HiddenField_pageViewCustomTabOrder_id: function (value) {
        if (this.HiddenField_pageViewCustomTabOrder_id != value) {
            this.HiddenField_pageViewCustomTabOrder_id = value;
        }
    },

    get_HiddenField_userComponents_id: function () {
        return this.HiddenField_userComponents_id;
    },

    set_HiddenField_userComponents_id: function (value) {
        if (this.HiddenField_userComponents_id != value) {
            this.HiddenField_userComponents_id = value;
        }
    },

    get_Id_componentRights: function () {
        return this.Id_componentRights;
    },

    set_Id_componentRights: function (value) {
        if (this.Id_componentRights != value) {
            this.Id_componentRights = value;
        }
    },

    get_ClickableComponentsClientIds: function () {
        return this.ClickableComponentsClientIds;
    },

    set_ClickableComponentsClientIds: function (value) {
        if (this.ClickableComponentsClientIds != value) {
            this.ClickableComponentsClientIds = value;
        }
    },

    get_SaveButtonClientId: function () {
        return this.SaveButtonClientId;
    },

    set_SaveButtonClientId: function (value) {
        if (this.SaveButtonClientId != value) {
            this.SaveButtonClientId = value;
        }
    }

}


Utility.ComponentSelect.registerClass('Utility.ComponentSelect', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded(); 
