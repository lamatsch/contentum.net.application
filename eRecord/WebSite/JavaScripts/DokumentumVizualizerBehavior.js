﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");


Utility.DokumentumVizualizerBehavior = function(element) {
    Utility.DokumentumVizualizerBehavior.initializeBase(this, [element]);

    //Properties
    this.panelVizualizerClientID = null;
    this.frameBodyClientID = null;
    this.elementsList = null;
    this.panelCloseClientID = null;
    this.labelTitleClientID = null;
    this.displayAfter = 0;
    //Variables
    this._panelVizualizer = null;
    this._frameBody = null;
    this._elementsDictionary = null;
    this._elementMouseOverHandler = null;
    this._elementMouseOutHandler = null;
    this._panelClose = null;
    this._closeClickHandler = null;
    this._labelTitle = null;
    this._elementClickHandler = null;
    this._timeOut = null;
    this._timeOutHandler = null;
}

Utility.DokumentumVizualizerBehavior.prototype =
{

    initialize: function() {
        Utility.DokumentumVizualizerBehavior.callBaseMethod(this, 'initialize');

        this._elementMouseOverHandler = Function.createDelegate(this, this._OnElementMouseOver);
        this._closeClickHandler = Function.createDelegate(this, this._OnCloseClick);
        this._elementClickHandler = Function.createDelegate(this, this._OnElementClick);

        if (this._IsDisplayAfter()) {
            this._timeOutHandler = Function.createDelegate(this, this._OnTimeOut);
            this._elementMouseOutHandler = Function.createDelegate(this, this._OnElementMouseOut);
        }

        if (this.panelVizualizerClientID) {
            this._panelVizualizer = $get(this.panelVizualizerClientID);
        }

        if (this.frameBodyClientID) {
            this._frameBody = $get(this.frameBodyClientID);
        }

        this.initializeElements();

        if (this.panelCloseClientID) {
            this._panelClose = $get(this.panelCloseClientID);
        }

        if (this._panelClose) {
            $addHandler(this._panelClose, 'click', this._closeClickHandler);
        }

        if (this.labelTitleClientID) {
            this._labelTitle = $get(this.labelTitleClientID);
        }
    },

    initializeElements: function() {
        if (this.elementsList) {
            this._elementsDictionary = new Object();
            for (var i = 0; i < this.elementsList.length; i++) {
                this._elementsDictionary[this.elementsList[i].ClientID] = this.elementsList[i];
                var element = $get(this.elementsList[i].ClientID);
                if (element) {
                    $addHandler(element, 'mouseover', this._elementMouseOverHandler);
                    $addHandler(element, 'click', this._elementClickHandler);
                    if (this._elementMouseOutHandler) {
                        $addHandler(element, 'mouseout', this._elementMouseOutHandler);
                    }
                }
            }
        }
    },

    dispose: function() {

        this._panelVizualizer = null;
        this._frameBody = null;

        if (this._elementMouseOverHandler || this._elementClickHandler || this._elementMouseOutHandler) {

            this.disposeElements();

            this._elementMouseOverHandler = null;
            this._elementClickHandler = null;
            this._elementMouseOutHandler = null;
        }

        this._elementsDictionary = null;

        if (this._closeClickHandler) {
            $removeHandler(this._panelClose, 'click', this._closeClickHandler);
            this._closeClickHandler = null;
        }

        this._panelClose = null;
        this._labelTitle = null;
        this._timeOutHandler = null;

        Utility.DokumentumVizualizerBehavior.callBaseMethod(this, 'dispose');
    },

    disposeElements: function() {
        if (this.elementsList) {
            for (var i = 0; i < this.elementsList.length; i++) {
                var element = $get(this.elementsList[i].ClientID);
                if (element) {
                    if (this._elementMouseOverHandler) {
                        $removeHandler(element, 'mouseover', this._elementMouseOverHandler);
                    }
                    if (this._elementClickHandler) {
                        $removeHandler(element, 'click', this._elementClickHandler);
                    }

                    if (this._elementMouseOutHandler) {
                        $removeHandler(element, 'mouseout', this._elementMouseOutHandler);
                    }
                }
            }

            this.elementsList = null;
        }
    },

    _IsDisplayAfter: function() {
        return (this.displayAfter && this.displayAfter > 0);
    },

    _OnTimeOut: function(e) {
        this._ClearTimeOut();
        this._DisplayDocument(e);
    },

    _ClearTimeOut: function() {
        if (this._timeOut) {
            window.clearTimeout(this._timeOut);
            this._timeOut = null;
        }
    },

    _OnElementMouseOver: function(e) {
        if (this._IsDisplayAfter()) {
            var self = this;
            this._timeOut = window.setTimeout(function() { self._timeOutHandler(e); }, this.displayAfter);
        }
        else
            this._DisplayDocument(e);
    },

    _OnElementMouseOut: function(e) {
        this._ClearTimeOut();
    },

    _DisplayDocument: function(e) {
        var targetElement = e.target;
        if (targetElement) {
            var targetElementID = targetElement.id;
            if (targetElementID) {
                var dokumentElement = this._elementsDictionary[targetElementID];
                if (dokumentElement) {
                    if ($(this._panelVizualizer).is(':hidden')) {
                        var pos = this.getPanelPosition(e);
                        var url = 'GetDocumentContent.aspx?id=' + dokumentElement.DokumentumId + '&version=' + encodeURIComponent(dokumentElement.Version);
                        $(this._frameBody).empty();
                        $(this._panelVizualizer).css('left', pos.x).css('top', pos.y).show();
                        $(this._labelTitle).html('<a href="' + url + '" target="_blank">' + dokumentElement.FileName + '</a>');
                        $(this._frameBody).attr('src', url + '&externalLink=' + encodeURIComponent(dokumentElement.ExternalLink) + '&fileName=' + encodeURIComponent(dokumentElement.FileName) + '&mode=display');
                    }
                }
            }
        }
    },

    _OnCloseClick: function(e) {
        this._HideDocument();
    },

    _HideDocument: function() {
        $(this._panelVizualizer).hide();
        $(this._frameBody).empty();
        $(this._frameBody).attr('src', '');
    },

    _OnElementClick: function(e) {
        this._HideDocument();
    },


    getMousePositon: function(e) {
        var posx = 0;
        var posy = 0;
        if (!e) var e = window.event;
        if (e.pageX || e.pageY) {
            posx = e.pageX;
            posy = e.pageY;
        }
        else if (e.clientX || e.clientY) {
            posx = e.clientX + document.body.scrollLeft
	            + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop
	            + document.documentElement.scrollTop;
        }

        return { 'x': posx, 'y': posy };
    },

    getPanelPosition: function(e) {
        var mousePos = this.getMousePositon(e);
        var panelWidth = $(this._panelVizualizer).width();
        var panelHeight = $(this._panelVizualizer).height();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();

        var overflowWidth = mousePos.x + panelWidth - windowWidth;
        if (overflowWidth > 0) {
            if (mousePos.x - panelWidth > 0) {
                mousePos.x = mousePos.x - panelWidth;
            }
            else {
                mousePos.x = mousePos.x - overflowWidth;
            }
        }

        var overflowHeight = mousePos.y + panelHeight - windowHeight;
        if (overflowHeight > 0) {
            if (mousePos.y - panelHeight > 0) {
                mousePos.y = mousePos.y - panelHeight;
            }
            else {
                mousePos.y = Math.max(0, mousePos.y - overflowHeight);
            }
        }

        return mousePos;
    },
    //
    // Behavior properties
    //
    get_PanelVizualizerClientID: function() {
        return this.panelVizualizerClientID;
    },

    set_PanelVizualizerClientID: function(value) {
        if (this.panelVizualizerClientID !== value) {
            this.panelVizualizerClientID = value;
            this.raisePropertyChanged('PanelVizualizerClientID');
        }
    },

    get_FrameBodyClientID: function() {
        return this.frameBodyClientID;
    },

    set_FrameBodyClientID: function(value) {
        if (this.frameBodyClientID !== value) {
            this.frameBodyClientID = value;
            this.raisePropertyChanged('FrameBodyClientID');
        }
    },

    get_ElementsList: function() {
        return this.elementsList;
    },

    set_ElementsList: function(value) {
        if (this.elementsList !== value) {
            this.elementsList = value;
            this.raisePropertyChanged('ElementsList');
        }
    },

    get_PanelCloseClientID: function() {
        return this.panelCloseClientID;
    },

    set_PanelCloseClientID: function(value) {
        if (this.panelCloseClientID !== value) {
            this.panelCloseClientID = value;
            this.raisePropertyChanged('PanelCloseClientID');
        }
    },

    get_LabelTitleClientID: function() {
        return this.labelTitleClientID;
    },

    set_LabelTitleClientID: function(value) {
        if (this.labelTitleClientID !== value) {
            this.labelTitleClientID = value;
            this.raisePropertyChanged('LabelTitleClientID');
        }
    },

    get_DisplayAfter: function() {
        return this.displayAfter;
    },

    set_DisplayAfter: function(value) {
        if (this.displayAfter !== value) {
            this.displayAfter = value;
            this.raisePropertyChanged('DisplayAfter');
        }
    }
}


Utility.DokumentumVizualizerBehavior.registerClass('Utility.DokumentumVizualizerBehavior', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();