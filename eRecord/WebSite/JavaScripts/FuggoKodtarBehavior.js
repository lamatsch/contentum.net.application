﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.FuggoKodtarBehavior = function (element) {
    Utility.FuggoKodtarBehavior.initializeBase(this, [element]);

    //Properties
    this.felhasznaloId = null;
    this.servicePath = null;
    this.serviceMethod = null;
    this.vezerloKodcsoportKod = null;
    this.fuggoKodcsoportKod = null;
    this.parentControlClientID = null;
    //Variables
    this.parentControl = null;
    this.targetControl = null;
    this.parentControlChangedHandler = null;
    this.applicationLoadHandler = null;
};

Utility.FuggoKodtarBehavior.prototype = {
    initialize: function () {
        Utility.FuggoKodtarBehavior.callBaseMethod(this, 'initialize');

        this.targetControl = this.get_element();

        if (this.parentControlClientID)
            this.parentControl = $get(this.parentControlClientID);

        if (this.parentControl) {
            this.parentControlChangedHandler = Function.createDelegate(this, this.onParentControlChanged);

            if (this.parentControlChangedHandler) {
                $addHandler(this.parentControl, 'change', this.parentControlChangedHandler);
            }
        }

        this.applicationLoadHandler = Function.createDelegate(this, this.onApplicationLoad);
        Sys.Application.add_load(this.applicationLoadHandler);
    },

    dispose: function () {

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
        }

        if (this.parentControlChangedHandler) {
            $removeHandler(this.parentControl, 'change', this.parentControlChangedHandler);
            this.parentControlChangedHandler = null;
        }

        Utility.FuggoKodtarBehavior.callBaseMethod(this, 'dispose');
    },

    onApplicationLoad: function () {
        this.fillTargetControl();
    },

    onParentControlChanged: function (args) {
        this.fillTargetControl();
    },

    fillTargetControl: function () {
        var parentValue = this.get_ParentValue();

        if (parentValue) {
            var params = { felhasznaloId: this.felhasznaloId, vezerloKodcsoportKod: this.vezerloKodcsoportKod, fuggoKodcsoportKod: this.fuggoKodcsoportKod, parentValue: parentValue };

            // Invoke the web service
            Sys.Net.WebServiceProxy.invoke(this.servicePath, this.serviceMethod, false, params,
                Function.createDelegate(this, this.onWsCallBack_GetFuggoKodtarakListSuccess),
                Function.createDelegate(this, this.onWsCallBack_GetFuggoKodtarakLisError), null);
        }
    },

    onWsCallBack_GetFuggoKodtarakListSuccess: function (result) {
        if (result) {

            if (this.targetControl) {
                var selectedValue = null;
                var selectedValueChanged = true;
                if (this.targetControl.selectedIndex > -1) {
                    selectedValue = this.targetControl.options[this.targetControl.selectedIndex].value;
                }

                while (this.targetControl.options.length > 0) {
                    this.targetControl.remove(0);
                }

                for (var i in result) {
                    var listItem = result[i];
                    var option = document.createElement("option");
                    option.text = listItem.name;
                    option.value = listItem.value;
                    if (option.value == selectedValue) {
                        option.selected = true;
                        selectedValueChanged = false;
                    }
                    this.targetControl.add(option);
                }

                if (selectedValueChanged)
                    $common.tryFireEvent(this.targetControl, "change");
            }
        }
    },

    onWsCallBack_GetFuggoKodtarakLisError: function (error) {
        alert(error.get_message());
    },

    get_ParentValue: function () {
        if (this.parentControl) {
            if (this.parentControl.type == 'hidden')
                return this.parentControl.value;

            if (this.parentControl.selectedIndex > -1) {
                return this.parentControl.options[this.parentControl.selectedIndex].value;
            }
        }

        return '';
    },

    get_FelhasznaloId: function () {
        return this.felhasznaloId;
    },

    set_FelhasznaloId: function (value) {
        if (this.felhasznaloId != value) {
            this.felhasznaloId = value;
        }
    },

    get_ServicePath: function () {
        return this.servicePath;
    },

    set_ServicePath: function (value) {
        if (this.servicePath != value) {
            this.servicePath = value;
        }
    },

    get_ServiceMethod: function () {
        return this.serviceMethod;
    },

    set_ServiceMethod: function (value) {
        if (this.serviceMethod != value) {
            this.serviceMethod = value;
        }
    },

    get_VezerloKodcsoportKod: function () {
        return this.vezerloKodcsoportKod;
    },

    set_VezerloKodcsoportKod: function (value) {
        if (this.vezerloKodcsoportKod != value) {
            this.vezerloKodcsoportKod = value;
        }
    },

    get_FuggoKodcsoportKod: function () {
        return this.fuggoKodcsoportKod;
    },

    set_FuggoKodcsoportKod: function (value) {
        if (this.fuggoKodcsoportKod != value) {
            this.fuggoKodcsoportKod = value;
        }
    },

    get_ParentControlClientID: function () {
        return this.parentControlClientID;
    },

    set_ParentControlClientID: function (value) {
        if (this.parentControlClientID != value) {
            this.parentControlClientID = value;
        }
    }
};

Utility.FuggoKodtarBehavior.registerClass('Utility.FuggoKodtarBehavior', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();