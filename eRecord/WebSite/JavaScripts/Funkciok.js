﻿///<reference name="MicrosoftAjax.js" />

Type.registerNamespace("Utility");

Utility.FunkciokBehavior = function(element) {
    Utility.FunkciokBehavior.initializeBase(this, [element]);
    
    //Properties
    this._autoCompleteExtenderId = null;
    this._hiddenFieldId = null;
    this._tipusHiddenFieldId = null;
    this._customValueEnabled = false;
    this._feladatMode = false;
    this._feladatDefinicioMode = false;
    this._tipusHiddenFieldFilterId = null;
    this.hiddenValue = '';
    //Variables
    this._autoCompleteExtender = null;
    this._hiddenField = null;
    this._tipusHiddenField = null;
    this._tipusHiddenFieldFilter = null;
    this._itemSelectedHandler = null;
    this._textChangedHandler = null;
    this._blurHandler = null;
    this._contextKey = null;
    this._focusHandler = null;
    this._hiddenChangedHandler = null;
}

Utility.FunkciokBehavior.prototype = {
    initialize : function() {
        Utility.FunkciokBehavior.callBaseMethod(this, 'initialize');
       
        if(this._hiddenFieldId)
        {
            this._hiddenField = $get(this._hiddenFieldId);
        }
        
        if(this._tipusHiddenFieldId)
        {
            this._tipusHiddenField = $get(this._tipusHiddenFieldId);
        }
        
        if(this._tipusHiddenFieldFilterId)
        {
            this._tipusHiddenFieldFilter = $get(this._tipusHiddenFieldFilterId);
        }
        
        this._itemSelectedHandler = Function.createDelegate(this,this._OnItemSelected);
        
        var element = this.get_element();
 
        if(!this._customValueEnabled)
        {
            this._blurHandler = Function.createDelegate(this, this._OnBlur);
            if(element)
            {
                $addHandler(element,'blur',this._blurHandler);
            }
        }
        
        this._textChangedHandler = Function.createDelegate(this,this._OnTextChanged);
        this._focusHandler = Function.createDelegate(this,this._OnFocus);
        
        if(element)
        {
           $addHandler(element,'change',this._textChangedHandler);
           $addHandler(element,'focus',this._focusHandler);
        }

        this._hiddenChangedHandler = Function.createDelegate(this, this._OnHiddenTextChanged);
        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);

        Sys.Application.add_load(this._applicationLoadHandler);
    },
    
    _OnApplicationLoad : function()
    {
        if(this._autoCompleteExtenderId)
        {
            this._autoCompleteExtender = $find(this._autoCompleteExtenderId);
        }
        
        if(this._autoCompleteExtender)
        {
            this._autoCompleteExtender.add_itemSelected(this._itemSelectedHandler);
        }

        // BUG_2099
        //if (this.hiddenFieldId) {
        //    if (this.hiddenFieldId.trim() != '') {
        //        var hidden = $get(this.hiddenFieldId);
        //        if (hidden != null) {
        //            if (this.hiddenChangedHandler != null) {
        //                $addHandler(hidden, 'change', this.hiddenChangedHandler);
        //            }
        //        }
        //    }
        //}
        if (this._hiddenFieldId) {
            if (this._hiddenFieldId.trim() != '') {
                var hidden = $get(this._hiddenFieldId);
                if (hidden != null) {
                    if (this._hiddenChangedHandler != null) {
                        $addHandler(hidden, 'change', this._hiddenChangedHandler);
                    }
                }
            }
        }

    },
    
    dispose : function() {
     
        if(this._applicationLoadHandler)
        {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }
        
        if(this._itemSelectedHandler)
        {
            if(this._autoCompleteExtender)
            {
                this._autoCompleteExtender.remove_itemSelected(this._itemSelectedHandler);
            }
            
            this._itemSelectedHandler = null;
        }
        
        if(!this._customValueEnabled)
        {
           if(this._blurHandler)
            {
                if(this.get_element())
                {
                    $removeHandler(this.get_element(),'blur',this._blurHandler);
                }
                
                this._blurHandler = null;    
            }
        }
        
        if(this._textChangedHandler)
        {
           if(this.get_element())
           {
               $removeHandler(this.get_element(),'change',this._textChangedHandler);
           }
                
           this._textChangedHandler = null;    
        }
        
        if(this._focusHandler)
        {
           if(this.get_element())
           {
               $removeHandler(this.get_element(),'focus',this._focusHandler);
           }
                
           this._focusHandler = null;    
        }

        if (this._hiddenFieldId) {
            if (this._hiddenFieldId.trim() != '') {
                var hidden = $get(this._hiddenFieldId);
                if (hidden != null) {
                    if (this._hiddenChangedHandler != null) {
                        $removeHandler(hidden, 'change', this._hiddenChangedHandler);
                    }
                }
            }
        }

        this._autoCompleteExtender = null;
        this._hiddenField = null;
        this._tipusHiddenField = null;
        this._tipusHiddenFieldFilter = null;
        this._hiddenChangedHandler = null;

        Utility.FunkciokBehavior.callBaseMethod(this, 'dispose');
    },
    
    _OnFocus: function(sender,e)
    {
        this._SetContextKey(this.get_ObjektumTipusKodFilter());     
    },
    
    _SetContextKey: function(value)
    {
         if(this._autoCompleteExtender)
         {
             if(this._IsContextKeyChanged(value))
             {
                 this._contextKey = value;
                 autoCompleteExtender = this._autoCompleteExtender;
                 this._ClearAutoCompleteCache();
                 var contextKey = autoCompleteExtender.get_contextKey();
                 var parts = contextKey.split(';');
                 if(parts.length > 0)
                 {                
                    contextKey = parts[0] + ';' + (this._feladatMode ? 'FeladatMode' : (this._feladatDefinicioMode ? 'FeladatDefinicioMode' : 'NormalMode')) + ';' + value;
                 }   
                 autoCompleteExtender.set_contextKey(contextKey);
             }
         }
    },
    
    _IsContextKeyChanged: function(value)
    {
        if(!this._contextKey)
        {
            return true;
        }
        
        if(this._contextKey != value)
        {
            return true;
        }
        
        return false;
    },
    
    _ClearAutoCompleteCache : function()
    {
        if(this._autoCompleteExtender)
        {
            this._autoCompleteExtender._cache = {};
        }
    },
    
    _OnItemSelected: function(sender, args)
    {         
        if ((typeof this._hiddenField != "undefined") && this._hiddenField != null)
        {
            var value = args.get_value();
            
            if(value != null)
            {
                 var values = value.split(';');
                 
                 if (values.length > 0) {
                     this._hiddenField.value = values[0].trim();
                     this.hiddenValue = values[0].trim();
                 }
                 if(values.length > 1 && this._tipusHiddenField)
                      this._tipusHiddenField.value = values[1].trim();
            }
        }
        
        this.raiseDataChanged();
    },
    
    _OnTextChanged: function(args)
    {
        var changed = false;
        if ((typeof this._hiddenField != "undefined") && this._hiddenField != null)
        {
            if(this._hiddenField.value != '')
            {
                this._hiddenField.value = '';
                changed = true;
            }
        }
        if ((typeof this._tipusHiddenField != "undefined") && this._tipusHiddenField != null)
        {
            this._tipusHiddenField.value = '';
        }
    },
    
    _OnBlur: function(args)
    {  
        if ((typeof this._hiddenField != "undefined") && (typeof hiddenField != "undefined") && this._hiddenField != null)
        {
            var value = this.hiddenValue;
            if(hiddenField.value == '' && value != '')
                hiddenField.value = value;

            if(this._hiddenField.value == '')
            {     
                 if(this._autoCompleteExtender != null)
                 {
                     this._autoCompleteExtender.get_element().value = '';
                 }   
            }
        }
        
        this.raiseDataChanged();
    },

    _OnHiddenTextChanged: function (args)
    {
        var hiddenField = $get(this.hiddenFieldClientId);

        if ((typeof hiddenField != "undefined") && hiddenField != null) {
            this.hiddenValue = hiddenField.value;
            this.raiseHiddenfieldValueChanged(hiddenField.value);
        }
    },

    //getter, setter methods
    get_AutoCompleteExtenderId : function()
    {
        return this._autoCompleteExtenderId;
    },
    
    set_AutoCompleteExtenderId : function(value)
    {
       if(this._autoCompleteExtenderId != value)
       {
          this._autoCompleteExtenderId = value;
       }
    },

    get_HiddenFieldId : function()
    {
        return this._hiddenFieldId;
    },
    
    set_HiddenFieldId : function(value)
    {
       if(this._hiddenFieldId != value)
       {
          this._hiddenFieldId = value;
       }
    },
    
    get_TipusHiddenFieldId : function()
    {
        return this._tipusHiddenFieldId;
    },
    
    set_TipusHiddenFieldId : function(value)
    {
       if(this._tipusHiddenFieldId != value)
       {
          this._tipusHiddenFieldId= value;
       }
    },
    
    get_CustomValueEnabled : function()
    {
        return this._customValueEnabled;
    },
    
    set_CustomValueEnabled : function(value)
    {
       if(this._customValueEnabled != value)
       {
          this._customValueEnabled = value;
       }
    },
    
    get_FeladatMode : function()
    {
        return this._feladatMode;
    },
    
    set_FeladatMode: function(value)
    {
       if(this._feladatMode != value)
       {
          this._feladatMode = value;
          // nem lehet egyidejűleg bekapcsolva
          if (value == true)
          {
            this.set_FeladatDefinicioMode(false);
          }
       }
    },
    
    get_FeladatDefinicioMode : function()
    {
        return this._feladatDefinicioMode;
    },
    
    set_FeladatDefinicioMode: function(value)
    {
       if(this._feladatDefinicioMode != value)
       {
          this._feladatDefinicioMode = value;
          // nem lehet egyidejűleg bekapcsolva
          if (value == true)
          {
            this.set_FeladatMode(false);
          }
       }
    },
    
    get_TipusHiddenFieldFilterId : function()
    {
        return this._tipusHiddenFieldFilterId;
    },
    
    set_TipusHiddenFieldFilterId : function(value)
    {
       if(this._tipusHiddenFieldFilterId != value)
       {
          this._tipusHiddenFieldFilterId= value;
       }
    },
    
    get_ObjektumTipusDropDownId : function()
    {
        return this._objektumTipusDropDownId;
    },
    
    set_ObjektumTipusDropDownId : function(value)
    {
       if(this._objektumTipusDropDownId != value)
       {
          this._objektumTipusDropDownId= value;
       }
    },

    get_HiddenValue: function () {
        return this.hiddenValue;
    },

    set_HiddenValue: function (value) {
        this.hiddenValue = value;;
    },

    
    //
    // Events
    //
    
    add_DataChanged : function(handler) {

        this.get_events().addHandler('DataChanged', handler);
    },
    
    remove_DataChanged : function(handler) {

        this.get_events().removeHandler('DataChanged', handler);
    },
    
    raiseDataChanged : function(eventArgs) 
    {
        
        var handler = this.get_events().getHandler('DataChanged');
        if (handler) 
        {
            handler(this, eventArgs);
        }
    },
    
    //Public Functions
    get_ObjektumTipusKod: function()
    {
        if(this._tipusHiddenField)
        {
            return this._tipusHiddenField.value;
        }
        
        return '';
    },
    
    get_ObjektumTipusKodFilter: function()
    {
        if(this._tipusHiddenFieldFilter)
        {
            return this._tipusHiddenFieldFilter.value;
        }
        
        return '';
    },
    
    get_IsEmpty: function()
    {
        if(this.get_element())
        {
            if(this.get_element().value == '')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        return true;    
    }  
}

Utility.FunkciokBehavior.registerClass('Utility.FunkciokBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();