﻿// A DokumentumAlairas.aspx-en van használva ez a fájl,
// az ott lévő HiddenField-ek alapján működik, amik szerver oldalon vannak kitöltve

// ESign objektum létrehozása
// TODO: a log:true paramétert, és az url-ből a '-test' szövegrészt el kell távolítani élesítéskor
var esign = new ESign({ 'baseURL': 'https://proxy.microsigner.com/esign' });

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function getFirstInputNameEndWith(text) {

    var inputs = document.getElementsByTagName("input");

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].id.endsWith(text)) {
            return inputs[i];
        }
    }

    return null;
}

function getHiddenfieldValue(fieldID) {
    var hiddenField = document.getElementById("ctl00_ContentPlaceHolder1_MicroSigner1_" + fieldID);
    if (hiddenField !== null) {
        return hiddenField.getAttribute("value");
    }
    else {
        var hiddenFieldIn = getFirstInputNameEndWith("_MicroSigner1_" + fieldID);
        if (hiddenFieldIn !== null) {
            return hiddenFieldIn.getAttribute("value");
        } else {
            return null;
        }
    }
};
function onSuccessGetCertificate(certificate) { 
    var esignresponse = getHiddenfieldValue("hfEsignResponse");
    var iratIds = getHiddenfieldValue("hfIratIds");
    var recordIds = getHiddenfieldValue("hfRecordIds");
    var externallinks = getHiddenfieldValue("hfExternalLinks");
    if (esignresponse !== null && iratIds !== null && recordIds !== null && externallinks !== null) {
        var newSigningSessionResponse = jQuery.parseJSON(esignresponse);
        MicroSignerCallbackService.SetUnsignedHashes(certificate, newSigningSessionResponse.sessionId, newSigningSessionResponse.documentIds, iratIds, recordIds, externallinks, onSetUnsignedHashes);
    }
};
function onSuccessGetSignedHashes(signedHashes) {
    var iratIds = getHiddenfieldValue("hfIratIds");
    var recordIds = getHiddenfieldValue("hfRecordIds");
    var csatolmanyokIds = getHiddenfieldValue("hfCsatolmanyokIds");
    var folyamatTetelekIds = getHiddenfieldValue("hfFolyamatTetelekIds");
    if (signedHashes.length > 0 &&
        iratIds !== null &&
        recordIds !== null &&
        csatolmanyokIds !== null &&
        folyamatTetelekIds !== null) {
        MicroSignerCallbackService.SetSignedHashes(signedHashes, iratIds, recordIds, csatolmanyokIds, folyamatTetelekIds, onSetSignedHashes);
    }
};
function selectCertAndSign() {
    var esignresponse = getHiddenfieldValue("hfEsignResponse");
    if (esignresponse !== null) {
        var newSigningSessionResponse = jQuery.parseJSON(esignresponse);
        esign.selectCertificateAndSign(newSigningSessionResponse, 'a_esign', onSuccessGetCertificate, onSuccessGetSignedHashes);
    }
};
function verifyPdfs(esignresponse, recordIds, externallinks, folyamatTetelekIds, procId) {
    //var esignresponse = getHiddenfieldValue("hfEsignResponse");
    //var iratIds = getHiddenfieldValue("hfIratIds");
    //var recordIds = getHiddenfieldValue("hfRecordIds");
    //var externallinks = getHiddenfieldValue("hfExternalLinks");
    //var folyamatTetelekIds = getHiddenfieldValue("hfFolyamatTetelekIds");
    //var procId = getHiddenfieldValue("hfProcId");
    /*if (typeof jQuery !== 'undefined') {
        // jQuery is loaded => print the version
        alert(jQuery.fn.jquery);
    } else {
        alert('Gond van a jqueryvel!');
    }*/
    if (esignresponse !== null && recordIds !== null && externallinks !== null) {
        showUpdateProgress('Ellenőrzés folyamatban...');
        var newSigningSessionResponse = jQuery.parseJSON(esignresponse);
        MicroSignerCallbackService.PdfVerify(newSigningSessionResponse.sessionId, newSigningSessionResponse.documentIds, recordIds, externallinks, folyamatTetelekIds, procId, onPdfVerify);
    }
};
function onPdfVerify(result) {
    hideUpdateProgress();
    if (result.IsError) {
        handleError(result.ErrorMessage);
    } else {
        alert('Aláírás ellenőrzés sikeresen lefutott!');
    }
    //window.returnValue = true;
    //if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }
    //window.close();
};
function serversideSign(esignresponse, iratIds, recordIds, externallinks, folyamatTetelekIds, procId, megjegyzes, csatolmanyokIds) {
    if (esignresponse !== null && iratIds !== null && recordIds !== null && externallinks !== null) {
        showUpdateProgress('Szerver oldali aláírás folyamatban...');
        var newSigningSessionResponse = jQuery.parseJSON(esignresponse);
        MicroSignerCallbackService.ServersidePDFSign(newSigningSessionResponse.sessionId, iratIds, newSigningSessionResponse.documentIds, recordIds, externallinks, folyamatTetelekIds, procId, megjegyzes, csatolmanyokIds, onServerSideSign);
    }
};
function onServerSideSign(result) {
    hideUpdateProgress();

    if (result) {
        if (result.IsError) {
            handleError(result.ErrorMessage);
        } else {
            alert('Szerver oldali aláírás sikeresen lefutott!');
        }
    }
    window.returnValue = true;
    if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }
    window.close();
};
function onSetUnsignedHashes(result) {
    if (!result.IsError) {
        showUpdateProgress('Feldolgozás folyamatban...');
    }
    else {
        handleError(result.ErrorMessage);
    }
};
function onSetSignedHashes(result) {
    hideUpdateProgress();
    if (!result.IsError) {
        window.returnValue = true;
        if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }
        window.close();
    }
    else {
        handleError(result.ErrorMessage);
    }
};
function showUpdateProgress(text) {
    if (text) {
        $('#updateProgressText').html(text);
    }
    var progressPanel = $('#UpdateProgressPanel');
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    progressPanel[0].style.left = (width / 2).toString() + "px";
    progressPanel.show();
};
function hideUpdateProgress() {
    $('#UpdateProgressPanel').hide();
};
function handleError(message) {
    alert(message);
};
