﻿/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.ObjektumValasztoPanelBehavior = function(element)
{
    Utility.ObjektumValasztoPanelBehavior.initializeBase(this, [element]);
    //Properties
    this.hfActiveTabClientID = '';
    this.labelObjektumNameClientID = '';
    //Variables
    this.applicationLoadHandler = null;
    this.tabContainer = null;
    this.activeTabChangedhandler = null;
    this.hfActiveTab = null;
    this.labelObjektumName = null;
    
}

Utility.ObjektumValasztoPanelBehavior.prototype = {
    initialize:function()
    {
        Utility.ObjektumValasztoPanelBehavior.callBaseMethod(this, 'initialize');
        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        Sys.Application.add_load(this.applicationLoadHandler);
        
        this.activeTabChangedhandler = Function.createDelegate(this, this.OnActiveTabChanged);
        
        if(this.hfActiveTabClientID != '')
        {
            this.hfActiveTab = $get(this.hfActiveTabClientID);
        }
        
        if(this.labelObjektumNameClientID != '')
        {
            this.labelObjektumName = $get(this.labelObjektumNameClientID);
        }
    },
    dispose: function() 
    { 
        if(this.applicationLoadHandler != null)
        {
            Sys.Application.remove_load(this.applicationLoadHandler);
            this.applicationLoadHandler = null;
        }
        
        if(this.activeTabChangedhandler != null)
        {
            if(this.tabContainer)
            {
                this.tabContainer.remove_activeTabChanged(this.activeTabChangedhandler);
            }    
        } 
        
        this.tabContainer = null;
        this.hfActiveTab = null;
        this.labelObjektumName = null;
        this.activeTabChangedhandler = null;
        
        Utility.ObjektumValasztoPanelBehavior.callBaseMethod(this, 'dispose');        
    },
    
    OnActiveTabChanged: function(sender, args)
    {
        this.SetActiveObjektum(sender,args);
    },
    
    OnApplicationLoad: function()
    {
        var e = this.get_element();
        if(e != null)
        {
            this.tabContainer = $find(e.id);
            if(this.activeTabChangedhandler != null)
            {
                this.tabContainer.add_activeTabChanged(this.activeTabChangedhandler);
            } 
        }
    },
    
    SetActiveObjektum: function(sender,args)
    {
        if(sender && typeof(AjaxControlToolkit) != 'undefined' && AjaxControlToolkit.TabContainer != undefined)
        {
            if(AjaxControlToolkit.TabContainer.isInstanceOfType(sender))
            {           
                var activeTab = sender.get_activeTab();
                if(activeTab)
                {
                    this.SaveActiveTab(activeTab);
                    
                    if(this.labelObjektumName)
                    {
                        if(this.labelObjektumName.firstChild)
                        {
                            this.labelObjektumName.firstChild.nodeValue = activeTab._header.firstChild.nodeValue + ':';
                        }    
                    }
                       
                 }
            }     
        }
    },
    
    SaveActiveTab:function(activeTab)
    {
        if(this.hfActiveTab)
        {
            debugger;
        }
    },
    
    //getter, setter methods
    get_HfActiveTabClientID : function()
    {
        return this.hfActiveTabClientID;
    },
    
    set_HfActiveTabClientID : function(value)
    {
       if(this.hfActiveTabClientID != value)
       {
          this.hfActiveTabClientID = value;
       }
    },
    get_LabelObjektumNameClientID : function()
    {
        return this.labelObjektumNameClientID;
    },
    
    set_LabelObjektumNameClientID : function(value)
    {
       if(this.labelObjektumNameClientID != value)
       {
          this.labelObjektumNameClientID = value;
       }
    }
 }


Utility.ObjektumValasztoPanelBehavior.registerClass('Utility.ObjektumValasztoPanelBehavior', Sys.UI.Behavior); 


if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
