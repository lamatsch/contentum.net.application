﻿// JScript File
Type.registerNamespace("Utility");

Utility.ScrollManager = function() {
    // register the object as disposable, so the application will call it's dispose method when needed
    if (typeof (Sys) !== "undefined") Sys.Application.registerDisposableObject(this);

    this.prm = Sys.WebForms.PageRequestManager.getInstance();
    this.scrolls = {};
    this.collapsiblePanels = new Array();
    this.initializeHandler = null;
    this.endHandler = null;
    this.scrollHandler = null;
    this.collapseHandler = null;
    this.expandHandler = null;

    this.pageLoadedHandler = null;

    this._scrollTopCorrection = -2; // header offset to hide scrolled table content at top
    this.toggleButtonExtenders = null; //new Array();

    this._ParentCPEs = null;

    this.initialize();
}
Utility.ScrollManager.prototype = {
    initialize: function() {
        this.initializeHandler = Function.createDelegate(this, this.InitializeRequest);
        this.endHandler = Function.createDelegate(this, this.EndRequest);
        this.scrollHandler = Function.createDelegate(this, this.OnScroll);
        this.prm.add_initializeRequest(this.initializeHandler);
        this.prm.add_endRequest(this.endHandler);

        this.collapseHandler = Function.createDelegate(this, this.OnCollapsedStatusChanged);
        this.expandHandler = Function.createDelegate(this, this.OnCollapsedStatusChanged);

        this.pageLoadedHandler = Function.createDelegate(this, this.PageLoaded);

        Sys.Application.add_load(this.pageLoadedHandler);
    },
    dispose: function() {
        var prm = this.prm;
        if (this.initializeHandler != null) {
            prm.remove_initializeRequest(this.initializeHandler);
            this.initializeHandler = null;
        }

        if (this.endHandler != null) {
            prm.remove_endRequest(this.endHandler);
            this.endHandler = null;
        }

        if (this.pageLoadedHandler != null) {
            prm.remove_pageLoaded(this.pageLoadedHandler);
            this.pageLoadedHandler = null;
        }

        var scrolls = this.scrolls;
        var collapsiblePanels = this.collapsiblePanels;
        if (collapsiblePanels.length == 0) {
            var elements = Sys.Application.getComponents();
            var j = 0;
            for (i in elements) {
                var element = elements[i];
                if (Object.getType(element).getName() == 'AjaxControlToolkit.CollapsiblePanelBehavior') {
                    collapsiblePanels[j] = element.get_id();
                    j++;
                    this._dispose(element);
                }
            }
        }
        else {
            for (i in collapsiblePanels) {
                var element = $find(collapsiblePanels[i]);
                if (element) {
                    this._dispose(element);
                }
            }
        }
        this.scrollHandler = null;
        this.collapseHandler = null;
        this.expandHandler = null;

        this._ParentCPEs = null;
    },
    InitializeRequest: function(sender, args) {
        var prm = this.prm;
        var scrolls = this.scrolls;
        var collapsiblePanels = this.collapsiblePanels;
        if (prm.get_isInAsyncPostBack()) {
            args.set_cancel(true);
        }
        if (collapsiblePanels.length == 0) {
            var elements = Sys.Application.getComponents();
            var j = 0;
            for (i in elements) {
                var element = elements[i];
                if (Object.getType(element).getName() == 'AjaxControlToolkit.CollapsiblePanelBehavior') {
                    collapsiblePanels[j] = element.get_id();
                    j++;
                    if (element.get_ScrollContents()) {
                        scrolls[element.get_id()] = element.get_element().scrollTop;
                    }
                    else {
                        scrolls[element.get_id()] = 0;
                    }
                }
            }
        }
        else {
            for (i in collapsiblePanels) {
                var element = $find(collapsiblePanels[i]);
                if (element) {
                    if (element.get_ScrollContents()) {
                        scrolls[element.get_id()] = element.get_element().scrollTop;
                    }
                    else {
                        scrolls[element.get_id()] = 0;
                    }
                }
            }
        }
    },
    EndRequest: function(sender, args) {
        var collapsiblePanels = this.collapsiblePanels;
        for (i in collapsiblePanels) {
            var element = $find(collapsiblePanels[i]);
            if (element && element.get_ScrollContents()) {
                if (args.get_dataItems()[element.get_id()] != undefined && Boolean.parse(args.get_dataItems()[element.get_id()])) {
                    this.scrolls[element.get_id()] = 0;
                }
                else {
                    this.setScrollPosition(element);
                }
                if (this.scrolls[element.get_id()] != undefined)
                    element.get_element().scrollTop = this.scrolls[element.get_id()];
            }

            this.setToggleButtonExtenders(element);
            if (element) {
                var panel = element.get_element();

                var allTables = this.getAllTables(panel);
                for (t in allTables) {
                    var tbl = allTables[t];
                    if (tbl) {
                        this.setHeaderOnCPE(tbl, element)
                    }
                }
            }
        }
    },
    PageLoaded: function(sender, args) {
        var collapsiblePanels = this.collapsiblePanels;
        if (collapsiblePanels.length == 0) {
            var collapsiblePanelTargets = new Array();
            var elements = Sys.Application.getComponents();
            var j = 0;
            for (i in elements) {
                var element = elements[i];
                if (Object.getType(element).getName() == 'AjaxControlToolkit.CollapsiblePanelBehavior') {
                    collapsiblePanels[j] = element.get_id();
                    j++;
                    this._pageLoaded(element);
                }
            }
        }
        else {
            for (i in collapsiblePanels) {
                var element = $find(collapsiblePanels[i]);
                if (element) {
                    this._pageLoaded(element);
                }
            }
        }

        // if it was hidden when loading the page first, try to fill it again
        if (this.toggleButtonExtenders == null) {
            var elements = Sys.Application.getComponents();
            for (i in elements) {
                var element = elements[i];
                this._fillToggleButtonExtenders(element);
            }

        }
    },
    OnScroll: function(sender, args) {
        var collapsiblePanels = this.collapsiblePanels;
        if (collapsiblePanels && collapsiblePanels.length > 0) {
            for (i in collapsiblePanels) {
                var element = $find(collapsiblePanels[i]);
                if (element) {
                    this.adjustGridHeaderTop(element);
                    this.adjustGridHeaderLeft(element);
                }
            }
        }
    },
    OnCollapsedStatusChanged: function(sender, args) {
        this.adjustOnCollapsedStatusChanged(sender);
    },
    setScrollPosition: function(cpe) {
        var panel = cpe.get_element();
        var table = this.getTable(panel);
        if (table != null) {
            if (table.rows.length > 1) {
                var selectedIndex = this.getSelectedIndex(table, 'check');
                if (selectedIndex > -1) {
                    var rowNumber = selectedIndex + 1;
                    var rowHeight = table.rows[selectedIndex].offsetHeight;
                    var panelHeight = panel.clientHeight;
                    var rowPos = 0;
                    for (var i = 0; i < rowNumber; i++) {
                        rowPos += table.rows[i].offsetHeight;
                    }
                    var pos = rowPos - panelHeight;
                    var scrollPos = this.scrolls[cpe.get_id()];
                    if (!this.isSelectedRowVisible(rowPos, rowHeight, scrollPos, panelHeight)) {
                        var newPos = pos + (panelHeight / 2) - (rowHeight / 2);
                        this.scrolls[cpe.get_id()] = newPos;
                    }
                }
            }
        }


    },
    _dispose: function(cpe) {
        if (cpe) {
            if (cpe.get_ScrollContents()) {
                if (this.scrollHandler != null) {
                    $removeHandler(cpe.get_element(), 'scroll', this.scrollHandler);
                }
            }

            if (this.collapseHandler != null) {
                cpe.remove_collapseComplete(this.collapseHandler);
            }
            if (this.expandHandler != null) {
                cpe.remove_expandComplete(this.expandHandler);
            }
        }
    },

    _pageLoaded: function(cpe) {
        if (cpe) {
            this.adjustGridHeaderTop(cpe);
            this._addEventCPEHandlers(cpe);
        }
    },

    _addEventCPEHandlers: function(cpe) {
        if (cpe) {
            // workaround: remove event handler before adding it to avoid adding it more
            if (this.scrollHandler != null) {
                try {
                    $removeHandler(cpe.get_element(), 'scroll', this.scrollHandler);
                }
                catch (ex) {
                    // do nothing
                }
                $addHandler(cpe.get_element(), 'scroll', this.scrollHandler);
            }

            if (this.collapseHandler != null) {
                try {
                    cpe.remove_collapseComplete(this.collapseHandler);
                }
                catch (ex) {
                    // do nothing
                }
                cpe.add_collapseComplete(this.collapseHandler);
            }
            if (this.expandHandler != null) {
                try {
                    cpe.remove_expandComplete(this.expandHandler);
                }
                catch (ex) {
                    // do nothing
                }
                cpe.add_expandComplete(this.expandHandler);
            }
        }
    },

    _fillToggleButtonExtenders: function(element) {
        if (element) {
            if (Object.getType(element).getName() == 'AjaxControlToolkit.ToggleButtonBehavior') {
                var tbeTarget = element.get_element();
                var parentCPETarget = this.getParentCPETargets(tbeTarget, this.collapsiblePanels);
                if (parentCPETarget) {
                    for (var k = 0; k < parentCPETarget.length; k++) {
                        var parentId = parentCPETarget[k].id;
                        if (this.toggleButtonExtenders == null) {
                            this.toggleButtonExtenders = new Object();
                        }
                        if (this.toggleButtonExtenders[parentId] == null) {
                            this.toggleButtonExtenders[parentId] = new Array();
                        }
                        Array.add(this.toggleButtonExtenders[parentId], element.get_id());
                    }
                }
            }
        }
    },

    adjustGridHeaderTop: function(cpe) {
        if (cpe) {
            var panel = cpe.get_element();
            var table = this.getTable(panel);

            if (!table) {
                return;
            }
            // Adjust header
            var header = table.rows[0];
            if (header) {
                header.style.position = 'relative';
                header.style.top = panel.scrollTop + this._scrollTopCorrection;
            }
        }
    },

    adjustGridHeaderLeft: function(cpe) {
        if (cpe) {
            var panel = cpe.get_element();
            var table = this.getTable(panel);

            if (!table) {
                return;
            }
            // Adjust header
            var header = table.rows[0];
            if (header) {
                header.style.position = 'relative';
                header.style.left = panel.scrollLeft;
            }
        }
    },

    adjustOnCollapsedStatusChanged: function(cpe) {
        if (cpe) {
            if (Object.getType(cpe).getName() != 'AjaxControlToolkit.CollapsiblePanelBehavior') {
                return;
            }

            var collapsiblePanels = this.collapsiblePanels;
            if (collapsiblePanels && collapsiblePanels.length > 0) {
                for (i in collapsiblePanels) {
                    var element = $find(collapsiblePanels[i]);
                    if (element) {
                        this._adjustOnCollapsedStatusChanged(cpe, element);
                    }
                }
            }

            this.setToggleButtonExtenders(cpe);
        }
    },

    isAbove: function(panelAbove, panelBelow) {
        var abovePos = this.findPos(panelAbove);
        var belowPos = this.findPos(panelBelow);
        var aboveWidth = panelAbove.clientWidth;
        var belowWidth = panelBelow.clientWidth;
        if (abovePos[1] > belowPos[1])
        { return false; }

        if (abovePos[0] > belowPos[0] + belowWidth)
        { return false; }

        if (abovePos[0] + aboveWidth < belowPos[0])
        { return false; }

        return true;
    },

    isLeft: function(panelLeft, panelRight) {
        var leftPos = this.findPos(panelLeft);
        var rightPos = this.findPos(panelRight);
        var leftHeight = panelLeft.clientHeight;
        var rightHeight = panelRight.clientHeight;
        if (leftPos[0] > rightPos[0])
        { return false; }

        if (leftPos[1] + leftHeight < rightPos[1])
        { return false; }

        if (leftPos[1] > rightPos[1] + rightHeight)
        { return false; }

        return true;

    },

    _adjustOnCollapsedStatusChanged: function(cpe, element) {
        if (cpe && element) {
            var panel = element.get_element();
            var table = this.getTable(panel);
            // TODO: loop through all tables on the panel
            if (table) {
                var senderPanel = cpe.get_element();
                if (cpe != element) {
                    if (cpe.get_ExpandDirection() == AjaxControlToolkit.CollapsiblePanelExpandDirection.Vertical) {
                        if (this.isAbove(senderPanel, panel)) {
                            // Adjust header
                            var header = table.rows[0];
                            if (header) {
                                // workaround: because parent of header could be changed, move it outside of the cpe (back to the panel with the gridview)
                                header.style.top = 1;
                                this.adjustGridHeaderTop(element);
                            }
                        }
                    }
                    else {
                        if (cpe.get_ExpandDirection() == AjaxControlToolkit.CollapsiblePanelExpandDirection.Horizontal) {
                            if (this.isLeft(senderPanel, panel)) {
                                // Adjust header
                                var header = table.rows[0];
                                if (header) {
                                    // workaround: because parent of header changed, move it to outside of the cpe (back to the panel with the gridview)
                                    header.style.left = 1;
                                    this.adjustGridHeaderLeft(element);
                                }
                            }
                        }
                    }
                }
                else {
                    var allTables = this.getAllTables(panel);
                    for (t in allTables) {
                        var tbl = allTables[t];
                        if (tbl) {
                            this.setHeaderOnCPE(tbl, element)
                        }
                    }
                }
            }
        }
    },

    findPos: function(obj) {
        var curleft = curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;

            } while ((obj = obj.offsetParent) != null);
        }
        return [curleft, curtop];
    },

    isChildOf: function(obj, parent) {
        if (obj && parent && obj.parentNode) {
            do {
                if (parent == obj.parentNode)
                    return true;

            } while ((obj = obj.parentNode) != null);
        }
        return false;
    },

    // returns parent CPEs element (e.g. panel) as array of objects
    getParentCPETargets: function(obj, collapsiblePanels) {
        var parents = null;
        if (collapsiblePanels && collapsiblePanels.length > 0) {
            for (i in collapsiblePanels) {
                var cpe = $find(collapsiblePanels[i]);
                if (cpe) {
                    var parent = cpe.get_element();
                    if (this.isChildOf(obj, parent)) {
                        if (parents == null) {
                            parents = new Array();
                        }
                        Array.add(parents, parent);
                    }
                }
            }
        }
        return parents;
    },

    // returns parent CPEs as array of objects
    getParentCPEs: function(obj, collapsiblePanels) {
        var parents = null;
        if (obj && collapsiblePanels && collapsiblePanels.length > 0) {
            if (this._parentCPEs && obj.id && obj.id != '' && this._parentCPEs.hasOwnProperty(obj.id)) {
                return this._parentCPEs[obj.id];
            }

            for (i in collapsiblePanels) {
                var cpe = $find(collapsiblePanels[i]);
                if (cpe) {
                    var parent = cpe.get_element();
                    if (this.isChildOf(obj, parent)) {
                        if (parents == null) {
                            parents = new Array();
                        }
                        Array.add(parents, cpe);
                    }
                }
            }

            if (obj.id && obj.id != '') {
                if (!this._parentCPEs) {
                    this._parentCPEs = new Object();
                }
                this._parentCPEs[obj.id] = parents;
            }
        }
        return parents;
    },

    setToggleButtonExtenders: function(cpe) {
        if (cpe) {
            var senderPanel = cpe.get_element();
            if (senderPanel) {
                var toggleButtonExtenders = this.toggleButtonExtenders;
                var cpeTargetId = senderPanel.id;
                if (toggleButtonExtenders && toggleButtonExtenders[cpeTargetId]) {
                    for (i in toggleButtonExtenders[cpeTargetId]) {
                        var toggleButtonExtender = $find(toggleButtonExtenders[cpeTargetId][i]);
                        if (toggleButtonExtender) {
                            var scrollElement = toggleButtonExtender.get_element();
                            var decoyElement = toggleButtonExtender._decoyElement;
                            if (decoyElement) { // toggle button checkbox image

                                if (cpe.get_Collapsed() == true) {
                                    var scrollElementPosition = this.findPos(scrollElement);

                                    var senderPanelPosition = this.findPos(senderPanel);
                                    if (senderPanelPosition[1] + senderPanel.clientHeight < scrollElementPosition[1]
                                    || senderPanelPosition[1] + senderPanel.clientHeight < scrollElementPosition[1] + scrollElement.clientHeight
                                    ) {
                                        decoyElement.style.display = 'none';
                                    }
                                    else {
                                        decoyElement.style.display = '';
                                    }

                                }
                                else {
                                    decoyElement.style.display = '';
                                }
                            }
                        }
                    }
                }
            }
        }
    },

    // returns first collapsed CPE as object in the hierarchy
    getCollapsedCPEInHierarchy: function(cpe) {
        if (cpe) {
            if (cpe.get_Collapsed())
            { return cpe; }
            var parentCPEs = this.getParentCPEs(cpe.get_element(), this.collapsiblePanels);
            if (parentCPEs) {
                for (var i = 0; i < parentCPEs.length; i++) {
                    if (parentCPEs[i].get_Collapsed()) {
                        return parentCPEs[i];
                    }
                }
            }
        }
        return null;
    },

    setHeaderOnCPE: function(table, cpe) {
        if (table && cpe) {
            if (!this.isChildOf(table, cpe.get_element()))
            { return; }
            var header = table.rows[0];
            if (header) {
                var senderPanel = cpe.get_element();
                // workaround: if it were outside of the panel, hide the header
                var collapsedParentCPE = this.getCollapsedCPEInHierarchy(cpe);
                if (collapsedParentCPE) {
                    senderPanel = collapsedParentCPE.get_element();
                    var headerPosition = this.findPos(header);

                    var senderPanelPosition = this.findPos(senderPanel);
                    if (senderPanelPosition[1] + senderPanel.clientHeight < headerPosition[1]
                            || senderPanelPosition[1] + senderPanel.clientHeight < headerPosition[1] + header.clientHeight
                            || headerPosition[1] < senderPanelPosition[1]
                            ) {
                        header.style.visibility = 'hidden';
                        //header.style.display = 'none';
                    }
                    else {
                        header.style.visibility = '';
                        //header.style.display = '';
                    }
                }
                else {
                    header.style.visibility = '';
                    //header.style.display = '';
                }
            }
        }
    },

    isSelectedRowVisible: function(rowPos, rowHeight, scrollPos, panelHeight) {
        var pos = rowPos - panelHeight;
        if (pos - scrollPos > 0 || pos - scrollPos - rowHeight < -panelHeight) {
            return false;
        }
        else {
            return true;
        }
    },

    getAllTables: function(panel) {
        var tables = this.findAllElements(panel, 'gridview', 'table');
        return tables;

    },
    findAllElements: function(parentControl, id, tagName, type) {
        var control = parentControl;
        var tables = new Array();
        if (control.childNodes.length > 0) {
            for (var i = 0; i < control.childNodes.length; i++) {
                var child = control.childNodes[i];
                if (child.id != undefined) {
                    if (child.id.toLowerCase().indexOf(id.toLowerCase()) != -1) {
                        if (tagName != undefined) {
                            if (child.tagName != undefined && child.tagName.toLowerCase() == tagName) {
                                if (type != undefined) {
                                    if (child.type != undefined && child.type.toLowerCase() == type) {
                                        Array.add(tables, child);
                                    }
                                }
                                else {
                                    Array.add(tables, child);
                                }
                            }
                        }
                        else {
                            Array.add(tables, child);
                        }
                    }
                }
                var table = this.findAllElements(child, id, tagName, type);
                if (table != null) {
                    Array.addRange(tables, table);
                }
            }
        }

        return tables;
    },

    getTable: function(panel) {
        var table = this.findElement(panel, 'gridview', 'table');
        return table;

    },
    findElement: function(parentControl, id, tagName, type) {
        var control = parentControl;
        if (control.childNodes.length > 0) {
            for (var i = 0; i < control.childNodes.length; i++) {
                var child = control.childNodes[i];
                if (child.id != undefined) {
                    if (child.id.toLowerCase().indexOf(id.toLowerCase()) != -1) {
                        if (tagName != undefined) {
                            if (child.tagName != undefined && child.tagName.toLowerCase() == tagName) {
                                if (type != undefined) {
                                    if (child.type != undefined && child.type.toLowerCase() == type) {
                                        return child;
                                    }
                                }
                                else {
                                    return child;
                                }
                            }
                        }
                        else {
                            return child;
                        }
                    }
                }
                var table = this.findElement(child, id, tagName, type);
                if (table != null) {
                    return table;
                }
            }
        }

        return null;
    },
    getSelectedIndex: function(table, checkBoxId) {
        var selectedIndex = -1;

        for (var i = 0; i < table.rows.length; i++) {
            var row = table.rows[i];
            for (var j = 0; j < row.cells.length; j++) {
                var cell = row.cells[j];
                var selectCheckBox = this.findElement(cell, checkBoxId, 'input', 'checkbox');
                if (selectCheckBox != null) {
                    if (selectCheckBox.checked) {
                        selectedIndex = i;
                        return selectedIndex;
                    }
                }
            }
        }

        return selectedIndex;
    }

}

Utility.ScrollManager.registerClass('Utility.ScrollManager', null, Sys.IDisposable);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();  