﻿///<reference name="MicrosoftAjax.js" />

Type.registerNamespace("Utility");

Utility.TargySzavakBehavior = function(element) {
    Utility.TargySzavakBehavior.initializeBase(this, [element]);
    
    //Properties
    this._autoCompleteExtenderId = null;
    this._hiddenFieldId = null;
    this._tipusHiddenFieldId = null;
    this._regexpHiddenFieldId = null;
    this._alapertelmezettertekHiddenFieldId = null;
    this._tooltipHiddenFieldId = null;
    this._controltypesourceHiddenFieldId = null;
    this._controltypedatasourceHiddenFieldFieldId = null;
    this._customValueEnabled = true;
    //Variables
    this._autoCompleteExtender = null;
    this._hiddenField = null;
    this._tipusHiddenField = null;
    this._regexpHiddenField = null;
    this._alapertelmezettertekHiddenField = null;
    this._tooltipHiddenField = null;
    this._controltypesourceHiddenField = null;
    this._controltypedatasourceHiddenField = null;
    this._itemSelectedHandler = null;
    this._textChangedHandler = null;
}

Utility.TargySzavakBehavior.prototype = {
    initialize : function() {
        Utility.TargySzavakBehavior.callBaseMethod(this, 'initialize');
       
        if(this._hiddenFieldId)
        {
            this._hiddenField = $get(this._hiddenFieldId);
        }
        
        if(this._tipusHiddenFieldId)
        {
            this._tipusHiddenField = $get(this._tipusHiddenFieldId);
        }
        
        if(this._regexpHiddenFieldId)
        {
            this._regexpHiddenField = $get(this._regexpHiddenFieldId);
        }
        
        if(this._alapertelmezettertekHiddenFieldId)
        {
            this._alapertelmezettertekHiddenField = $get(this._alapertelmezettertekHiddenFieldId);
        }  
        
        if(this._tooltipHiddenFieldId)
        {
            this._tooltipHiddenField = $get(this._tooltipHiddenFieldId);
        }

        if (this._controltypesourceHiddenFieldId) {
            this._controltypesourceHiddenField = $get(this._controltypesourceHiddenFieldId);
        }

        if (this._controltypedatasourceHiddenFieldId) {
            this._controltypedatasourceHiddenField = $get(this._controltypedatasourceHiddenFieldId);
        }
        
        this._itemSelectedHandler = Function.createDelegate(this,this._OnItemSelected);
        
        if(!this._customValueEnabled)
        {
            this._textChangedHandler = Function.createDelegate(this,this._OnTextChanged);
        }    
        
        this._applicationLoadHandler = Function.createDelegate(this,this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);
    },
    
    _OnApplicationLoad : function()
    {
        if(this._autoCompleteExtenderId)
        {
            this._autoCompleteExtender = $find(this._autoCompleteExtenderId);
        }
        
        if(this._autoCompleteExtender)
        {
            this._autoCompleteExtender.add_itemSelected(this._itemSelectedHandler);
            if(!this._customValueEnabled)
            {
               $addHandler(this._autoCompleteExtender.get_element(),'blur',this._textChangedHandler);
            }   
        }
    },
    
    dispose : function() {
     
        if(this._applicationLoadHandler)
        {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }
        
        if(this._itemSelectedHandler)
        {
            if(this._autoCompleteExtender)
            {
                this._autoCompleteExtender.remove_itemSelected(this._itemSelectedHandler);
            }
            
            this._itemSelectedHandler = null;
        }
        
        if(!this._customValueEnabled)
        {
            if(this._textChangedHandler)
            {
                if(this._autoCompleteExtender && this._autoCompleteExtender.get_element())
                {
                    $removeHandler(this._autoCompleteExtender.get_element(),'blur',this._textChangedHandler);
                }
                
                this._textChangedHandler = null;    
            }
        }
        
        this._autoCompleteExtender = null;
        this._hiddenField = null;
        this._tipusHiddenField = null;
        this._regexpHiddenField = null;
        this._alapertelmezettertekHiddenField = null;
        this._tooltipHiddenField = null;
        this._controltypesourceHiddenField = null;
        this._controltypedatasourceHiddenField = null;

        Utility.TargySzavakBehavior.callBaseMethod(this, 'dispose');
    },
    
    _OnItemSelected: function(sender, args)
    {         
        if(this._hiddenField != null)
        {
            var value = args.get_value();
            
            if(value != null)
            {
                 var values = value.split('××');
                 
                 if(values.length > 0)
                    this._hiddenField.value = values[0].trim();
                    
                 if(values.length > 1 && this._tipusHiddenField)
                    this._tipusHiddenField.value = values[1].trim();
                    
                 if(values.length > 2 && this._regexpHiddenField)
                    this._regexpHiddenField.value = values[2].trim();
                    
                 if(values.length > 3 && this._alapertelmezettertekHiddenField)
                    this._alapertelmezettertekHiddenField.value = values[3].trim();
                    
                 if(values.length > 4 && this._tooltipHiddenField)
                    this._tooltipHiddenField.value = values[4].trim();

                if (values.length > 5 && this._controltypesourceHiddenField)
                    this._controltypesourceHiddenField.value = values[5].trim();

                if (values.length > 6 && this._controltypedatasourceHiddenField)
                    this._controltypedatasourceHiddenField.value = values[6].trim(); 

            }
        }
        
    },
    
    _OnTextChanged: function(args)
    {        
        if(this._hiddenField != null)
        {
            if(this._hiddenField.value == '')
            {     
                 if(this._autoCompleteExtender != null)
                 {
                     this._autoCompleteExtender.get_element().value = '';
                 }   
            }
       }
    },

    //getter, setter methods
    get_AutoCompleteExtenderId : function()
    {
        return this._autoCompleteExtenderId;
    },
    
    set_AutoCompleteExtenderId : function(value)
    {
       if(this._autoCompleteExtenderId != value)
       {
          this._autoCompleteExtenderId = value;
       }
    },

    get_HiddenFieldId : function()
    {
        return this._hiddenFieldId;
    },
    
    set_HiddenFieldId : function(value)
    {
       if(this._hiddenFieldId != value)
       {
          this._hiddenFieldId = value;
       }
    },
    
    get_TipusHiddenFieldId : function()
    {
        return this._tipusHiddenFieldId;
    },
    
    set_TipusHiddenFieldId : function(value)
    {
       if(this._tipusHiddenFieldId != value)
       {
          this._tipusHiddenFieldId= value;
       }
    },
    
    get_RegExpHiddenFieldId : function()
    {
        return this._regexpHiddenFieldId;
    },
    
    set_RegExpHiddenFieldId : function(value)
    {
       if(this._regexpHiddenFieldId != value)
       {
          this._regexpHiddenFieldId= value;
       }
    },
    
    get_AlapertelmezettErtekHiddenFieldId : function()
    {
        return this._alapertelmezettertekHiddenFieldId;
    },
    
    set_AlapertelmezettErtekHiddenFieldId : function(value)
    {
       if(this._alapertelmezettertekHiddenFieldId != value)
       {
          this._alapertelmezettertekHiddenFieldId= value;
       }
    },
    
    get_ToolTipHiddenFieldId : function()
    {
        return this._tooltipHiddenFieldId;
    },
    
    set_ToolTipHiddenFieldId : function(value)
    {
       if(this._tooltipHiddenFieldId != value)
       {
          this._tooltipHiddenFieldId= value;
       }
   },

   get_ControlTypeSourceHiddenFieldId: function() {
       return this._controltypesourceHiddenFieldId;
   },

   set_ControlTypeSourceHiddenFieldId: function(value) {
       if (this._controltypesourceHiddenFieldId != value) {
           this._controltypesourceHiddenFieldId = value;
       }
   },

   get_ControlTypeDataSourceHiddenFieldId: function() {
       return this._controltypedatasourceHiddenFieldId;
   },

   set_ControlTypeDataSourceHiddenFieldId: function(value) {
       if (this._controltypedatasourceHiddenFieldId != value) {
           this._controltypedatasourceHiddenFieldId = value;
       }
   },
    
    get_CustomValueEnabled : function()
    {
        return this._customValueEnabled;
    },
    
    set_CustomValueEnabled : function(value)
    {
       if(this._customValueEnabled != value)
       {
          this._customValueEnabled = value;
       }
    }     
}

Utility.TargySzavakBehavior.registerClass('Utility.TargySzavakBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();