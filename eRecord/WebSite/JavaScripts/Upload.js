﻿function AddImageButtonClick(FileUploadId,
                             SelectedFilesNamesHiddenFieldId,
                             SelectedFilesCountHiddenFieldId) {
	var FileUpload = document.getElementById(FileUploadId);
	var SelectedFilesNamesHiddenField = document.getElementById(SelectedFilesNamesHiddenFieldId);
	var SelectedFilesCountHiddenFieldId = document.getElementById(SelectedFilesCountHiddenFieldId);
	
	if(FileUpload.value != '') {
	    var re = new Regexp = /\\/gi;
	    var x = FileUpload.value.replace(re, "\\\");
		var n = parseInt(SelectedFilesCountHiddenFieldId.value);
		SelectedFilesNamesHiddenField.value = SelectedFilesNamesHiddenField.value+";"+x;
		n = n+1;
		SelectedFilesCountHiddenFieldId.value = n;
		return true;
	} else {
		alert("Nincs állomány kiválasztva!");
	}
}

function KivalasztasOnClick(CheckBoxListId,
                            CheckBoxListItemsCount,
                            SelectedFilesNamesHiddenFieldId,
                            SelectedFilesCountHiddenFieldId) {
	var cbl_id = document.getElementById(CheckBoxListId);
	var cbl_count = parseInt(CheckBoxListItemsCount);
	var sfnhf_id = document.getElementById(SelectedFilesNamesHiddenFieldId);
	var sfchf_id = document.getElementById(SelectedFilesCountHiddenFieldId);

	var uncheckedItems = 0;
	for (var j=0; j<cbl_count; j++) {
		var objx = document.getElementById(CheckBoxListId+"_"+j)
		if(!objx.checked) {
			uncheckedItems++;
		}
	}
	if(cbl_count == 0 || uncheckedItems == cbl_count) {
		alert("Nincs kiválasztott állomány!");
		return false;
	} else {
		return true;
	}
}
