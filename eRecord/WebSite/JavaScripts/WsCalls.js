﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />


var eSetTextBoxesByUgykorUgytipusType = { All: 0, OnlySzignalas: 1, OnlyIntezesiIdo: 2, None: 3 };

var ugykor_lastValue = '';
var ugytipus_lastValue = '';

var eljarasiszakasz_lastValue = '00';
var irattipus_lastValue = '';

var ugyGeneraltTargy_lastValue = '';
var kezelo_lastValue = '';  //userId;  // eredetileg a formon beírjuk az aktuális felhasználót kezelőnek, ezért ezzel hasonlítunk
var ugyintezo_lastValue = '';
var ugyfelelos_lastValue = '';

function clearIratHatarido() {
    var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
    var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
    var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);
    var ugyiratHataridoKitolasIratHiddenField = $get(UgyiratHataridoKitolas_Irat_HiddenField_Id)

    if (iratIntezesiHataridoTextBox) {
        iratIntezesiHataridoTextBox.value = "";

        if (iratIntezesiHataridoHourTextBox) {
            iratIntezesiHataridoHourTextBox.value = "00";
        }

        if (iratIntezesiHataridoMinuteTextBox) {
            iratIntezesiHataridoMinuteTextBox.value = "00";
        }
        // TOOLTIP
        iratIntezesiHataridoTextBox.title = "";

    }

    if (ugyiratHataridoKitolasIratHiddenField) {
        // kitolhat? | kötött?
        ugyiratHataridoKitolasIratHiddenField.value = "1|0";
    }

    setIratHataridoControlEnableState(true);
}

function setIratHataridoControlEnableState(isEnabled) {
    var bDisabled = !isEnabled;
    var classNameControls = (isEnabled ? "" : "disabledCalendarImage");

    try {
        var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        if (iratIntezesiHataridoTextBox) {
            iratIntezesiHataridoTextBox.disabled = bDisabled;
        }
    } catch (e) {
        ;
    }

    try {
        var iratIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);
        if (iratIntezesiHatarido_CalendarImage) {
            iratIntezesiHatarido_CalendarImage.disabled = bDisabled;
            iratIntezesiHatarido_CalendarImage.className = classNameControls;
        }
    } catch (e) {
        ;
    }

    try {
        var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
        if (iratIntezesiHataridoHourTextBox) {
            iratIntezesiHataridoHourTextBox.disabled = bDisabled;
        }
    } catch (e) {
        ;
    }

    try {
        var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);
        if (iratIntezesiHataridoMinuteTextBox) {
            iratIntezesiHataridoMinuteTextBox.disabled = bDisabled;
        }

    } catch (e) {
        ;
    }

    try {
        var iratIntezesiHatarido_ArrowUpImage = $get(IraIratHatarido_ArrowUpImage_Id);
        if (iratIntezesiHatarido_ArrowUpImage !== null) {
            iratIntezesiHatarido_ArrowUpImage.disabled = bDisabled;
            iratIntezesiHatarido_ArrowUpImage.className = classNameControls;
        }
    } catch (e) {
        ;
    }
    try {
        var iratIntezesiHatarido_ArrowDownImage = $get(IraIratHatarido_ArrowDownImage_Id);
        if (iratIntezesiHatarido_ArrowDownImage !== null) {
            iratIntezesiHatarido_ArrowDownImage.disabled = bDisabled;
            iratIntezesiHatarido_ArrowDownImage.className = classNameControls;
        }
    } catch (e) {
        ;
    }


    try {
        var ugyIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        if (ugyIntezesiHataridoTextBox) {
            ugyIntezesiHataridoTextBox.disabled = bDisabled;
        }
    } catch (e) {
        ;
    }

    try {
        var ugyIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);
        if (ugyIntezesiHatarido_CalendarImage !== null) {
            ugyIntezesiHatarido_CalendarImage.disabled = bDisabled;
            ugyIntezesiHatarido_CalendarImage.className = classNameControls;
        }
    } catch (e) {
        ;
    }
}

function setUgyiratHataridoControlEnableState(isEnabled) {
    var bDisabled = !isEnabled;
    var classNameControls = (isEnabled ? "" : "disabledCalendarImage");

    var ugyIntezesiHataridoTextBox = $get(UgyUgyiratHatarido_TextBox_Id);
    var ugyIntezesiHatarido_CalendarImage = $get(UgyUgyiratHatarido_CalendarImage_Id);

    if (ugyIntezesiHataridoTextBox) {
        ugyIntezesiHataridoTextBox.disabled = bDisabled;
    }

    if (ugyIntezesiHatarido_CalendarImage !== null) {
        ugyIntezesiHatarido_CalendarImage.disabled = bDisabled;
        ugyIntezesiHatarido_CalendarImage.className = classNameControls;
    }

}

function SetTextBoxesByIratMetaDefinicioForUgyiratModify() {
    // ellenőrzés
    if (!userId) return false;
    // Ügykör_Id:
    var UgykorHiddenField = $get(UgykorHiddenField_Id);
    var selectedUgykor = '';
    if (UgykorHiddenField) {
        selectedUgykor = UgykorHiddenField.value;
    }

    // Ügytípus:
    var UgytipusDropDown = $get(UgytipusDropDown_Id);
    var selectedUgytipus = '';
    if (UgytipusDropDown) {
        if (UgytipusDropDown.selectedIndex >= 0) { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumUgyirat = Get_UgyIrat_UgyintezesKezdete();

    // 1. ugyirat hatarido
    if (!selectedUgykor || selectedUgykor === '') {
        // nem kell ws hívás
    }
    else {
        Ajax_eRecord.GetIntezesiIdoWithToolTip(
            selectedUgykor, selectedUgytipus, userId, ugyintezesKezdeteDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
    }
}

function SetTextBoxesByIratMetaDefinicio(type) {
    // ellenőrzés
    switch (type) {
        case eSetTextBoxesByUgykorUgytipusType.None:
            return false;
        case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
            if (!userId || !userCsoportId) return false;
            break;
        case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
            if (!userId) return false;
            break;
        case eSetTextBoxesByUgykorUgytipusType.All: default:
            if (!userId || !userCsoportId) return false;
            break;
    }

    // Ügykör_Id:
    var UgykorDropDown = $get(UgykorDropDown_Id);
    var selectedUgykor = '';
    if (UgykorDropDown) {
        if (UgykorDropDown.selectedIndex >= 0) { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
    }

    // Ügytípus:
    var UgytipusDropDown = $get(UgytipusDropDown_Id);
    var selectedUgytipus = '';
    if (UgytipusDropDown) {
        if (UgytipusDropDown.selectedIndex >= 0) { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }

    }

    // Eljárási szakasz:
    var EljarasiSzakaszHiddenField = $get(EljarasiSzakaszHiddenField_Id);
    var selectedEljarasiSzakasz = '00';
    if (EljarasiSzakaszHiddenField) {
        if (EljarasiSzakaszHiddenField.value !== '') { selectedEljarasiSzakasz = EljarasiSzakaszHiddenField.value; }

    }

    // Irattípus:
    var IrattipusDropDown = $get(IrattipusDropDown_Id);
    var selectedIrattipus = '';
    if (IrattipusDropDown) {
        if (IrattipusDropDown.selectedIndex >= 0) { selectedIrattipus = IrattipusDropDown.options[IrattipusDropDown.selectedIndex].value; }

    }

    // 1. ugyirat hatarido
    if (!selectedUgykor || selectedUgykor === '') {
        // nem kell ws hívás
    }
    else if (selectedUgykor === ugykor_lastValue && selectedUgytipus === ugytipus_lastValue) {
        // nem kell ws hívás
    }
    else {
        // ezt az információt még használjuk, ezért később végezzük csak el az értékadást
        //ugykor_lastValue = selectedUgykor;
        //ugytipus_lastValue = selectedUgytipus;
        var ugyintezesKezdeteDatumUgyirat = null;
        switch (type) {
            case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                    selectedUgykor, selectedUgytipus, userId, userCsoportId, OnWsCallBack_SetFelelosCsoportok);
                Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                    selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                break;
            case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                {
                    ugyintezesKezdeteDatumUgyirat = Get_UgyIrat_UgyintezesKezdete();
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor, selectedUgytipus, userId, ugyintezesKezdeteDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
                    Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                        selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                }
                break;
            case eSetTextBoxesByUgykorUgytipusType.All: default:
                {
                    ugyintezesKezdeteDatumUgyirat = Get_UgyIrat_UgyintezesKezdete();
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor, selectedUgytipus, userId, ugyintezesKezdeteDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor, selectedUgytipus, userId, userCsoportId, OnWsCallBack_SetFelelosCsoportok);
                    Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                        selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                }
                break;
        }
    }

    // 2. irat hatarido
    if (type === eSetTextBoxesByUgykorUgytipusType.All || type === eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo) {
        if (!selectedUgytipus || selectedUgytipus === ''
            || !selectedEljarasiSzakasz || selectedEljarasiSzakasz === '' || !selectedIrattipus || selectedIrattipus === '') {
            // nem kell ws hívás
            clearIratHatarido();
        }
        else if (selectedUgykor !== ugykor_lastValue || selectedUgytipus !== ugytipus_lastValue
            || selectedEljarasiSzakasz !== eljarasiszakasz_lastValue || selectedIrattipus !== irattipus_lastValue) {
            // mivel az ügykör/ügytípus változott, itt mindenképp kell hívás
            eljarasiszakasz_lastValue = selectedEljarasiSzakasz;
            irattipus_lastValue = selectedIrattipus;

            var iktatasDatumIrat = '';
            var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
            if (IktatasDatumIrat_HiddenField) {
                iktatasDatumIrat = IktatasDatumIrat_HiddenField.value;
            }
            Ajax_eRecord.GetIratIntezesiIdoWithToolTip(
                selectedUgykor, selectedUgytipus, selectedEljarasiSzakasz, selectedIrattipus, userId, iktatasDatumIrat, OnWsCallBack_SetIratHatarido);

        }
    }

    ugykor_lastValue = selectedUgykor;
    ugytipus_lastValue = selectedUgytipus;
}

function createItemToolTips(dropdown) {
    Utility.DomElement.Select.createItemToolTips(dropdown);
}

function clearDropDown(dropdown) {
    if (dropdown) {
        for (var i = dropdown.options.length - 1; i >= 0; i--) {
            dropdown.options[i].selected = false;
            dropdown.options[i] = null; // remove the option
        }
    }
}
/*    
    function FillUgytipusDropDown()
    {
        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);
        
        if (!UgytipusDropDown ) return false;
        
         // Ügykör_Id:
         var UgykorHiddenField = $get(UgykorHiddenField_Id);
         
         if (!UgykorHiddenField || UgykorHiddenField.value == '') return false;

        Ajax_eRecord.GetUgytipusokByUgykorForDropDown(UgykorHiddenField.value, OnWsCallBack_FillUgytipusDropDown);

    }
    
    function OnWsCallBack_FillUgytipusDropDown(result)
    {
        // result feldolgozása: tömb, elemei Ugytipus és UgytipusNev párok egymástól pontosvesszővel pipe ("|") karakterrel elválasztva
        
        //alert(result);
       
       // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);
        
        if (!UgytipusDropDown ) return false;
        
        clearDropDown(UgytipusDropDown);
 
        resultArray = result;

        if (resultArray.length > 0)
        {
            for ( var i = 0; i < resultArray.length; i++)
            {
                var elementArray = resultArray[i].split('|');
                if (elementArray.length == 2)
                {
                    var ugytipus = elementArray[0];
                    var ugytipusNev = elementArray[1];
                    var option = document.createElement('option');
                    option.appendChild(document.createTextNode(ugytipusNev));
                    option.setAttribute('value', ugytipus);
                    option.title = ugytipusNev;
                    UgytipusDropDown.appendChild(option);
                }
            }
        }
        
        if (UgytipusDropDown.options.length == 0)
        {
            var option = document.createElement('option');
            option.appendChild(document.createTextNode('---'));
            option.setAttribute('value', '');
            UgytipusDropDown.appendChild(option);
        }
        
        UgytipusDropDown.options[0].selected = true;
        createItemToolTips(UgytipusDropDown);
        
        $common.tryFireEvent(UgytipusDropDown, 'change');
    }
*/


/*    
    function SetTextBoxesByUgykorUgytipus(type)
    {
        // ellenőrzés
        switch(type)
        {
            case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                if (!userId || !userCsoportId) return false;
                break;
            case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                if (!userId) return false;
                break;                
            case eSetTextBoxesByUgykorUgytipusType.All: default:
                if (!userId || !userCsoportId) return false;
                break;
        }

        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
        {
            if (UgykorDropDown.selectedIndex >= 0)
            { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        
        }
        
        if (!selectedUgykor || selectedUgykor == ''
            || (selectedUgykor == ugykor_lastValue && selectedUgytipus == ugytipus_lastValue))
        {                    
            // nem kell ws hívás
        }
        else
        {
            ugykor_lastValue = selectedUgykor;
            ugytipus_lastValue = selectedUgytipus;
            
            switch(type)
            {
                case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
                    break;
                case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor,selectedUgytipus,userId,OnWsCallBack_SetIntezesiHatarido);
                    break;                
                case eSetTextBoxesByUgykorUgytipusType.All: default:
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor,selectedUgytipus,userId,OnWsCallBack_SetIntezesiHatarido);
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
                    break;
            }


        }
    }    
*/

//LZS - BLG_335
//Módosítva: HataridoClearIsRequired paraméter hozzáadva, az "ügyirat ügyintézési határidő másolása az irat ügyintézési határidejéhez" funkció miatt.
function SetTextBoxesIrattipusByUgyirat(Ugyirat_Id, HataridoClearIsRequired) {
    HataridoClearIsRequired = typeof HataridoClearIsRequired !== 'undefined' ? HataridoClearIsRequired : 'true';

    if (!Ugyirat_Id) return false;

    // Eljárási szakasz:
    var EljarasiSzakaszHiddenField = $get(EljarasiSzakaszHiddenField_Id);
    var selectedEljarasiSzakasz = '00';
    if (EljarasiSzakaszHiddenField) {
        if (EljarasiSzakaszHiddenField.value !== '') { selectedEljarasiSzakasz = EljarasiSzakaszHiddenField.value; }

    }

    // Irattípus:
    var IrattipusDropDown = $get(IrattipusDropDown_Id);
    var selectedIrattipus = '';
    if (IrattipusDropDown) {
        if (IrattipusDropDown.selectedIndex >= 0) { selectedIrattipus = IrattipusDropDown.options[IrattipusDropDown.selectedIndex].value; }

    }

    if (!selectedEljarasiSzakasz || selectedEljarasiSzakasz === '' || !selectedIrattipus || selectedIrattipus === '') {
        //LZS - BLG_335
        //Csak akkor töröljük ki a határidő calendar textbox-ot, ha ha a paraméter true
        if (HataridoClearIsRequired === "true")
            clearIratHatarido();
    }
    else {
        eljarasiszakasz_lastValue = selectedEljarasiSzakasz;
        irattipus_lastValue = selectedIrattipus;

        var iktatasDatumIrat = '';
        var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
        if (IktatasDatumIrat_HiddenField) {
            iktatasDatumIrat = IktatasDatumIrat_HiddenField.value;
        }
        Ajax_eRecord.GetIratIntezesiIdoWithToolTipFromUgyiratId(
            Ugyirat_Id, selectedEljarasiSzakasz, selectedIrattipus, userId, iktatasDatumIrat, OnWsCallBack_SetIratHatarido);

    }

}

function OnWsCallBack_SetIntezesiHatarido(result) {
    // result feldolgozása: 'intézési idő, kötöttség, számított határidő és esetleges tooltip szöveg pontosvesszővel (';') elválasztva

    //alert(result);
    //alert("OnWsCallBack_SetIntezesiHatarido");
    var ugyIntezesiHataridoTextBox = $get(UgyUgyiratHatarido_TextBox_Id);
    var ugyiratHataridoKitolasUgyiratHiddenField = $get(UgyiratHataridoKitolas_Ugyirat_HiddenField_Id);

    var ugyIntezesiHataridoHourTextBox = $get(UgyUgyiratHatarido_HourTextBox_Id);
    var ugyIntezesiHataridoMinuteTextBox = $get(UgyUgyiratHatarido_MinuteTextBox_Id);

    var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
    var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);

    if (ugyIntezesiHataridoTextBox) {
        var strIntezesiIdo = '';
        var strIntezesiIdoKotott = '';
        var shortDateStr = null;
        var tooltip = '';
        var strUgyiratHataridoKitolas = '';
        var hourStr_irat = null;
        var minuteStr_irat = null;

        var ugyKezdeteDateStr = null;
        var ugyKezdeteHourStr = null;
        var ugyKezdeteMinuteStr = null;

        var ugyIntezesiHataridoArray = new Array();
        ugyIntezesiHataridoArray = result.split(';');

        if (ugyIntezesiHataridoArray.length === 5) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
        }
        else if (ugyIntezesiHataridoArray.length === 7) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];
        }
        else if (ugyIntezesiHataridoArray.length === 10) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];

            ugyKezdeteDateStr = ugyIntezesiHataridoArray[7];
            ugyKezdeteHourStr = ugyIntezesiHataridoArray[8];
            ugyKezdeteMinuteStr = ugyIntezesiHataridoArray[9];

            SetUgyintezesKezdete(ugyKezdeteDateStr, ugyKezdeteHourStr, ugyKezdeteMinuteStr);
            SetUgyintezesHatarido(shortDateStr, hourStr_irat, minuteStr_irat);
        }

        if (shortDateStr !== null) {
            // határidő formázott (yyyy.mm.dd) beírása
            ugyIntezesiHataridoTextBox.value = shortDateStr;
        }

        ugyIntezesiHatarido_CalendarImage = $get(UgyUgyiratHatarido_CalendarImage_Id);
        if (strIntezesiIdoKotott === "1" || strUgyiratHataridoKitolas === "0") {
            setUgyiratHataridoControlEnableState(false);
        }
        else {
            setUgyiratHataridoControlEnableState(true);
        }

        try {
            if (hourStr_irat !== '') {
                iratIntezesiHataridoHourTextBox.value = hourStr_irat;
            }
            else {
                iratIntezesiHataridoHourTextBox.value = "00";
            }

            if (minuteStr_irat !== '') {
                iratIntezesiHataridoMinuteTextBox.value = minuteStr_irat;
            }
            else {
                iratIntezesiHataridoMinuteTextBox.value = "00";
            }
        } catch (e) { /*alert(e)*/ }

        try {
            if (hourStr_irat !== '') {
                ugyIntezesiHataridoHourTextBox.value = hourStr_irat;
            }
            else {
                ugyIntezesiHataridoHourTextBox.value = "00";
            }

            if (minuteStr_irat !== '') {
                ugyIntezesiHataridoMinuteTextBox.value = minuteStr_irat;
            }
            else {
                ugyIntezesiHataridoMinuteTextBox.value = "00";
            }
        } catch (e) { /*alert(e)*/ }

        if (ugyiratHataridoKitolasUgyiratHiddenField) {
            if (strUgyiratHataridoKitolas === '0') {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '0';
            }
            else {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '1';
            }
        }

        // TOOLTIP
        ugyIntezesiHataridoTextBox.title = tooltip;
        // BUG_10524
        $common.tryFireEvent(ugyIntezesiHataridoTextBox, 'change');
    }
    //        else
    //        {
    //            return false;
    //        }
}
function SetUgyintezesKezdete(ugyKezdeteDateStr, ugyKezdeteHourStr, ugyKezdeteMinuteStr) {

    try {
        var ugyIntezesiKezdeteTextBox = $get(UgyintezesKezdete_TextBox_Id);
        var ugyIntezesiKezdeteoHourTextBox = $get(UgyintezesKezdete_HourTextBox_Id);
        var ugyIntezesiKezdeteMinuteTextBox = $get(UgyintezesKezdete_MinuteTextBox_Id);

        if (ugyKezdeteDateStr !== null) {
            ugyIntezesiKezdeteTextBox.value = ugyKezdeteDateStr;
        }

        if (ugyKezdeteHourStr != null && ugyKezdeteHourStr !== '') {
            ugyIntezesiKezdeteoHourTextBox.value = ugyKezdeteHourStr;
        }
        else {
            ugyIntezesiKezdeteoHourTextBox.value = "00";
        }

        if (ugyKezdeteMinuteStr != null && ugyKezdeteMinuteStr !== '') {
            ugyIntezesiKezdeteMinuteTextBox.value = ugyKezdeteMinuteStr;
        }
        else {
            ugyIntezesiKezdeteMinuteTextBox.value = "00";
        }
    } catch (e) {
        //alert(e);
    }

}

/*    
    function SetTextBoxesFromSzignalasiJegyzek()
    {
        if (!userId || !userCsoportId)
        {
            return false;
        }

        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
        {
            if (UgykorDropDown.selectedIndex >= 0)
            { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        
        }

        if (!selectedUgykor || selectedUgykor == ''
            || (selectedUgykor == ugykor_lastValue && selectedUgytipus == ugytipus_lastValue))
        {                    
            // nem kell ws hívás
        }
        else
        {  
            ugykor_lastValue = selectedUgykor;
            ugytipus_lastValue = selectedUgytipus;
        
            Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
        }
            
    }       
*/

function OnWsCallBack_SetIratHatarido(result) {
    // result feldolgozása: 'intézési idő, időegység, kötöttség, számított határidő, esetleges tooltip szöveg
    // és ügyirat határidő kitolhatóság pontosvesszővel (';') elválasztva

    //alert(result);
    //alert("OnWsCallBack_SetIratHatarido");
    var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
    var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
    var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);

    var ugyiratHataridoKitolasIratHiddenField = $get(UgyiratHataridoKitolas_Irat_HiddenField_Id);

    if (iratIntezesiHataridoTextBox) {
        var strIntezesiIdo_irat = '';
        var strIntezesiIdoKotott_irat = '';
        var shortDateStr_irat = null;
        var hourStr_irat = null;
        var minuteStr_irat = null;
        var tooltip_irat = '';
        var strUgyiratHataridoKitolas_irat = '1';

        var iratIntezesiHataridoArray = new Array();
        iratIntezesiHataridoArray = result.split(';');

        if (iratIntezesiHataridoArray.length === 7) {
            strIntezesiIdo_irat = iratIntezesiHataridoArray[0];
            strIntezesiIdoKotott_irat = iratIntezesiHataridoArray[1];
            shortDateStr_irat = iratIntezesiHataridoArray[2];
            hourStr_irat = iratIntezesiHataridoArray[3];
            minuteStr_irat = iratIntezesiHataridoArray[4];
            tooltip_irat = iratIntezesiHataridoArray[5];
            strUgyiratHataridoKitolas_irat = iratIntezesiHataridoArray[6];
        }
        else {
            // hiba
            //alert('Hiba: az irat intézési idő tömb nem megfelelő számú elemet tartalmaz!');
            return false;
        }

        if (strIntezesiIdoKotott_irat !== '1') {
            strIntezesiIdoKotott_irat = '0';
        }

        if (ugyiratHataridoKitolasIratHiddenField) {
            if (strUgyiratHataridoKitolas_irat === '0') {
                ugyiratHataridoKitolasIratHiddenField.value = '0' + '|' + strIntezesiIdoKotott_irat;
            }
            else {
                ugyiratHataridoKitolasIratHiddenField.value = '1' + '|' + strIntezesiIdoKotott_irat;
            }
        }

        if (shortDateStr_irat !== '') {
            // határidő formázott (yyyy.mm.dd) beírása
            iratIntezesiHataridoTextBox.value = shortDateStr_irat;

            if (hourStr_irat !== '') {
                iratIntezesiHataridoHourTextBox.value = hourStr_irat;
            }
            else {
                iratIntezesiHataridoHourTextBox.value = "00";
            }

            if (minuteStr_irat !== '') {
                iratIntezesiHataridoMinuteTextBox.value = minuteStr_irat;
            }
            else {
                iratIntezesiHataridoMinuteTextBox.value = "00";
            }
        }
        else     // ha nincs előírás, akkor töröljük
        {
            clearIratHatarido();
        }

        if (strIntezesiIdoKotott_irat === "1") {
            setIratHataridoControlEnableState(false);
        }
        else {
            setIratHataridoControlEnableState(true);
        }

        // TOOLTIP
        iratIntezesiHataridoTextBox.title = tooltip_irat;

    }
    else {
        return false;
    }
}

function OnWsCallBack_SetFelelosCsoportok(result) {
    // result feldolgozása: ';'-vel elválasztva vannak az ügyfelelős;ügyintéző;kezelő -k
    //alert(result);

    if (result === null) {
        return false;
    }

    var resultArray = new Array();
    resultArray = result.split(';');

    if (result !== '' && resultArray.length !== 3) {
        return null;
    }
    else {

        var ugyFelelos_str = '';
        var ugyintezo_str = '';
        var kezelo_str = '';

        if (resultArray.length === 3) {
            ugyFelelos_str = resultArray[0];
            ugyintezo_str = resultArray[1];
            kezelo_str = resultArray[2];
        }

        if (ugyFelelos_str !== null) {
            var ugyFelelosHiddenField = $get(Ugyfelelos_HiddenField_Id);
            var ugyFelelosTextBox = $get(Ugyfelelos_TextBox_Id);

            var ugyFelelosArray = new Array();
            ugyFelelosArray = ugyFelelos_str.split(',');
            if (ugyFelelosArray.length === 2) {
                if (ugyFelelosHiddenField && ugyFelelosTextBox) {
                    ugyFelelosHiddenField.value = ugyFelelosArray[0];
                    ugyFelelosTextBox.value = ugyFelelosArray[1];

                    ugyfelelos_lastValue = ugyFelelosArray[0];
                }
            }
            else if (ugyFelelos_str === '') {
                if (ugyFelelosHiddenField && ugyFelelosTextBox) {
                    // kézzel beírtat nem írunk felül üres értékkel
                    if (ugyFelelosHiddenField.value === '' || ugyFelelosHiddenField.value === ugyfelelos_lastValue) {
                        ugyFelelosHiddenField.value = '';
                        ugyFelelosTextBox.value = '';

                        ugyfelelos_lastValue = '';
                    }
                }
            }
        }
        if (ugyintezo_str !== null) {
            var ugyintezoHiddenField = $get(Ugyintezo_HiddenField_Id);
            var ugyintezoTextBox = $get(Ugyintezo_TextBox_Id);

            var ugyintezoArray = new Array();
            ugyintezoArray = ugyintezo_str.split(',');
            if (ugyintezoArray.length === 2) {
                if (ugyintezoHiddenField && ugyintezoTextBox) {
                    ugyintezoHiddenField.value = ugyintezoArray[0];
                    ugyintezoTextBox.value = ugyintezoArray[1];

                    ugyintezo_lastValue = ugyintezoArray[0];
                }
            }
            else if (ugyintezo_str === '') {
                if (ugyintezoHiddenField && ugyintezoTextBox) {
                    // kézzel beírtat nem írunk felül üres értékkel
                    if (ugyintezoHiddenField.value === '' || ugyintezoHiddenField.value === ugyintezo_lastValue) {
                        ugyintezoHiddenField.value = '';
                        ugyintezoTextBox.value = '';

                        ugyintezo_lastValue = '';
                    }
                }
            }
        }
        if (kezelo_str !== null) {
            var kezeloHiddenField = $get(Kezelo_HiddenField_Id);
            var kezeloTextBox = $get(Kezelo_TextBox_Id);

            var kezeloArray = new Array();
            kezeloArray = kezelo_str.split(',');
            if (kezeloArray.length === 2) {
                if (kezeloHiddenField && kezeloTextBox) {
                    kezeloHiddenField.value = kezeloArray[0];
                    kezeloTextBox.value = kezeloArray[1];

                    kezelo_lastValue = kezeloArray[0];
                }
            }
            else if (kezelo_str === '') {
                if (kezeloHiddenField && kezeloTextBox) {
                    // kézzel beírtat nem írunk felül üres értékkel
                    if (kezeloHiddenField.value === '' || kezeloHiddenField.value === kezelo_lastValue) {
                        kezeloHiddenField.value = '';
                        kezeloTextBox.value = '';

                        kezelo_lastValue = '';
                    }
                }
            }
        }
    }

}

function OnWsCallBack_SetGeneraltTargy(result) {
    // result feldolgozása: egyelőre csak az ügyirat tárgy mezőt töltjük

    //alert(result);

    if (result === null) {
        return false;
    }

    var ugyTargyTextBox = $get(UgyTargy_TextBox_Id);

    if (ugyTargyTextBox) {
        if (ugyTargyTextBox.value !== '' && ugyTargyTextBox.value !== ugyGeneraltTargy_lastValue) {
            return false;   // kézzel beírtat nem írunk felül
        }
        ugyTargyTextBox.value = result;
        ugyGeneraltTargy_lastValue = result;
    }

}

// érkeztetőkönyv dropdown:
var emptyMessage = '[Nincs megadva elem]';

Type.registerNamespace("Constants");

Constants.IktatoKonyvekDropDownListMode = function () {
};

Constants.IktatoKonyvekDropDownListMode.prototype = {
    ErkeztetoKonyvek: 0,
    Iktatokonyvek: 1,
    IktatokonyvekWithRegiAdatok: 2
};

Constants.IktatoKonyvekDropDownListMode.registerEnum("Constants.IktatoKonyvekDropDownListMode", false);

function fillErkeztetokonyvDropDownByEv(erkKonyv_ClientId, evTol_ClientId, evIg_ClientId, userId, userCsoportId, mode, valuesFilledWithId) {
    //alert('fillErkeztetokonyvDropDownByEv');

    var erkKonyv = $get(erkKonyv_ClientId);
    if (erkKonyv === null) {
        return false;
    }

    var evTol_TextBox = $get(evTol_ClientId);
    var evIg_TextBox = $get(evIg_ClientId);

    if (evTol_TextBox && evIg_TextBox) {
        var evTol = evTol_TextBox.value;
        var evIg = evIg_TextBox.value;

        var _mode;
        if (arguments.length < 6) _mode = Constants.IktatoKonyvekDropDownListMode.ErkeztetoKonyvek;
        else _mode = mode;

        Ajax_eRecord.GetErkeztetoKonyvekList(evTol, evIg, userId, userCsoportId, _mode, valuesFilledWithId, OnWsCallBack_FillErkKonyvDropDown, OnWsErrorCallBack_FillErkKonyvDropDown, erkKonyv);

    }
}

function fillAllErkeztetokonyvDropDownByEv(erkKonyv_ClientId, evTol_ClientId, evIg_ClientId, userId, userCsoportId, mode, valuesFilledWithId) {
    //alert('fillErkeztetokonyvDropDownByEv');

    var erkKonyv = $get(erkKonyv_ClientId);
    if (erkKonyv === null) {
        return false;
    }

    var evTol_TextBox = $get(evTol_ClientId);
    var evIg_TextBox = $get(evIg_ClientId);

    if (evTol_TextBox && evIg_TextBox) {
        var evTol = evTol_TextBox.value;
        var evIg = evIg_TextBox.value;

        var _mode;
        if (arguments.length < 6) _mode = Constants.IktatoKonyvekDropDownListMode.ErkeztetoKonyvek;
        else _mode = mode;

        Ajax_eRecord.GetAllErkeztetoKonyvekList(evTol, evIg, userId, userCsoportId, _mode, valuesFilledWithId, OnWsCallBack_FillErkKonyvDropDown, OnWsErrorCallBack_FillErkKonyvDropDown, erkKonyv);

    }
}



function OnWsCallBack_FillErkKonyvDropDown(returnValues, erkKonyv) {
    if (returnValues !== null && erkKonyv !== null) {
        // selected item elmentése:
        var selectedOption = null;
        var addEmptyItem = false;
        if (erkKonyv.selectedIndex >= 0) {
            selectedOption = erkKonyv.options[erkKonyv.selectedIndex];
        }

        // dropdown törlése:
        var numberOfOptions = erkKonyv.options.length;
        if (numberOfOptions > 0) {
            if (erkKonyv.options[0].value === '') {
                addEmptyItem = true;
            }
        }
        var i = 0;
        for (i = 0; i < numberOfOptions; i++) {
            //Note: Always remove(0) and NOT remove(i)			 
            erkKonyv.remove(0);
        }

        // üres elem hozzáadása:
        if (addEmptyItem) {
            var emptyOption = document.createElement('option');
            emptyOption.text = emptyMessage;
            emptyOption.value = '';
            erkKonyv.options.add(emptyOption);
        }


        var temp1;
        var err;
        try {
            temp1 = new Sys.Serialization.JavaScriptSerializer.deserialize(returnValues);
        }
        catch (err) {
            alert('Hiba az iktatókönyvek lekérése során!');
            return false;
        }

        //temp1 = returnValues.split(';');

        //            if(!addEmptyItem && temp1.length == 1)
        //            {
        //                erkKonyv.disabled = true;
        //            }
        //            else
        //            {
        //                erkKonyv.disabled = false;
        //            }

        for (var key in temp1) {
            var erkKonyvNev = temp1[key];
            var megkulJelzes = key;
            if (erkKonyvNev && megkulJelzes) {
                var opt = document.createElement('option');
                opt.text = erkKonyvNev;
                opt.value = megkulJelzes;

                erkKonyv.options.add(opt);

                // ha ez volt a kiválasztott:
                if (selectedOption && selectedOption.value === opt.value) {
                    opt.selected = true;
                }
            }
        }

        if (!addEmptyItem && erkKonyv.options.length === 1) {
            erkKonyv.disabled = true;
        }
        else {
            erkKonyv.disabled = false;
        }
        //            
        //            for (i=0;i<temp1.length;i++)
        //            {
        //                var temp2 = new Array();
        //                temp2 = temp1[i].split('&');
        //                var erkKonyvNev = temp2[0];
        //                var megkulJelzes = temp2[1];
        //                
        //                if (erkKonyvNev && megkulJelzes)
        //                {
        //                    var opt = document.createElement('option');
        //                    opt.text = erkKonyvNev;
        //                    opt.value = megkulJelzes;
        //                    
        //                    erkKonyv.options.add(opt);
        //                    
        //                    // ha ez volt a kiválasztott:
        //                    if (selectedOption && selectedOption.value == opt.value)
        //                    {
        //                        opt.selected = true;
        //                    }
        //                }
        //            }

        $common.tryFireEvent(erkKonyv, 'change');
    }

}

function OnWsErrorCallBack_FillErkKonyvDropDown(error) {
    AjaxLogging.ExceptionManager.getInstance().publishException(ERROR_CODE_WEB_SERVICE, error);
}


function SetMerge_IrattariTetelszamTextBox() {
    // Ügykör_Id:
    var UgykorDropDown = $get(UgykorDropDown_Id);
    var selectedUgykor = '';
    if (UgykorDropDown) {
        if (UgykorDropDown.selectedIndex >= 0) {
            selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value;
            if (selectedUgykor) {
                Ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(
                    selectedUgykor, userId, OnWsCallBack_Merge_IrattariTetelszamTextBox);
            }

        }
    }

}

function OnWsCallBack_Merge_IrattariTetelszamTextBox(result) {
    if (result === null) {
        return false;
    }

    var Merge_IrattariTetelszamTextBox = $get(Merge_IrattariTetelszamTextBox_Id);
    if (Merge_IrattariTetelszamTextBox) {
        Merge_IrattariTetelszamTextBox.value = result;
    }
}

function SetIntezesiIdoManual() {
    //alert("SetIntezesiIdoManual");
    var IntezesiIdo_DropDownList = $get(IntezesiIdo_DropDownList_Id);
    var intezesiIdo = '';
    if (IntezesiIdo_DropDownList) {
        if (IntezesiIdo_DropDownList.selectedIndex >= 0) { intezesiIdo = IntezesiIdo_DropDownList.options[IntezesiIdo_DropDownList.selectedIndex].value; }
    }

    var IntezesiIdoegyseg_DropDownList = $get(IntezesiIdoegyseg_DropDownList_Id);
    var intezesiIdoegyseg = '';
    if (IntezesiIdoegyseg_DropDownList) {
        if (IntezesiIdoegyseg_DropDownList.selectedIndex >= 0) { intezesiIdoegyseg = IntezesiIdoegyseg_DropDownList.options[IntezesiIdoegyseg_DropDownList.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumUgyirat = Get_UgyIrat_UgyintezesKezdete();
    Ajax_eRecord.GetIntezesiIdoWithToolTipManual(
        intezesiIdo, intezesiIdoegyseg, userId, ugyintezesKezdeteDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
}

function SetIntezesiIdoManualV2() {
    //alert("SetIntezesiIdoManualV2");
    var inputUgyUgyintezesiNapok = $get(inputUgyUgyintezesiNapok_Id);
    var UgyIratintezesiIdo = '';
    try {
        UgyIratintezesiIdo = inputUgyUgyintezesiNapok.value;
    } catch (e) {
        return;
    }

    var IntezesiIdoegyseg_DropDownList = $get(IntezesiIdoegyseg_DropDownList_Id);
    var intezesiIdoegyseg = '';
    if (IntezesiIdoegyseg_DropDownList) {
        if (IntezesiIdoegyseg_DropDownList.selectedIndex >= 0) { intezesiIdoegyseg = IntezesiIdoegyseg_DropDownList.options[IntezesiIdoegyseg_DropDownList.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumUgyirat = Get_UgyIrat_UgyintezesKezdete();
    Ajax_eRecord.GetIntezesiIdoWithToolTipManual(
        UgyIratintezesiIdo, intezesiIdoegyseg, userId, ugyintezesKezdeteDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
}

function Get_UgyIrat_UgyintezesKezdete() {
    var ugyintezesKezdeteDatumIrat = '';

    if (typeof Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId !== 'undefined' && Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId) {
        var Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId_Ctrl = $get(Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId);
        if (Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId_Ctrl) {
            ugyintezesKezdeteDatumIrat = Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId_Ctrl.value;
            return ugyintezesKezdeteDatumIrat;
        }
    }

    var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
    if (IktatasDatumIrat_HiddenField) {
        ugyintezesKezdeteDatumIrat = IktatasDatumIrat_HiddenField.value;
    }

    return ugyintezesKezdeteDatumIrat;
}

// BLG_1020
function Get_Irat_UgyintezesKezdete() {
    var ugyintezesKezdeteDatumIrat = '';
    // BLG_1348
    // A vizsgálat nem csak a papíralapúakra vonatkozik

    //var papirAlapu = true;

    //if (typeof UgyintezesModja_DropDownList_Id != 'undefined' && UgyintezesModja_DropDownList_Id) {
    //    var UgyintezesModja_DropDownList = $get(UgyintezesModja_DropDownList_Id);
    //    if (UgyintezesModja_DropDownList) {
    //        if (UgyintezesModja_DropDownList.selectedIndex > 0) {
    //            var selectedValue = UgyintezesModja_DropDownList.options[UgyintezesModja_DropDownList.selectedIndex].value;
    //            papirAlapu = selectedValue == "0" ? true : false;
    //        }

    //    }
    //}
    // if (papirAlapu) {

    if (typeof Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId !== 'undefined' && Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId) {
        var Irat_Ugyintezes_Kezdete_Datum_Hiddenfield = $get(Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId);
        if (Irat_Ugyintezes_Kezdete_Datum_Hiddenfield) {
            ugyintezesKezdeteDatumIrat = Irat_Ugyintezes_Kezdete_Datum_Hiddenfield.value;
            return ugyintezesKezdeteDatumIrat;
        }
    }
    //}

    var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
    if (IktatasDatumIrat_HiddenField) {
        ugyintezesKezdeteDatumIrat = IktatasDatumIrat_HiddenField.value;
    }

    return ugyintezesKezdeteDatumIrat;
}

//function setIratHataridoControlEnableState(isEnabled) {
//    var bDisabled = !isEnabled;
//    var classNameControls = (isEnabled ? "" : "disabledCalendarImage");

//    var ugyIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
//    var ugyIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);

//    if (ugyIntezesiHataridoTextBox) {
//        ugyIntezesiHataridoTextBox.disabled = bDisabled;
//    }

//    if (ugyIntezesiHatarido_CalendarImage !== null) {
//        ugyIntezesiHatarido_CalendarImage.disabled = bDisabled;
//        ugyIntezesiHatarido_CalendarImage.className = classNameControls;
//    }

//}

function OnWsCallBack_SetIratIntezesiHatarido(result) {
    // result feldolgozása: 'intézési idő, kötöttség, számított határidő és esetleges tooltip szöveg pontosvesszővel (';') elválasztva

    //alert(result);
    //alert("OnWsCallBack_SetIratIntezesiHatarido");
    var ugyIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
    var ugyiratHataridoKitolasUgyiratHiddenField = $get(UgyiratHataridoKitolas_Ugyirat_HiddenField_Id);

    if (ugyIntezesiHataridoTextBox) {
        var strIntezesiIdo = '';
        var strIntezesiIdoKotott = '';
        var shortDateStr = null;
        var tooltip = '';
        var strUgyiratHataridoKitolas = '';

        var hourStr_irat = null;
        var minuteStr_irat = null;

        var ugyKezdeteDateStr = null;
        var ugyKezdeteHourStr = null;
        var ugyKezdeteMinuteStr = null;

        var ugyIntezesiHataridoArray = new Array();
        ugyIntezesiHataridoArray = result.split(';');

        if (ugyIntezesiHataridoArray.length === 5) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
        }
        else if (ugyIntezesiHataridoArray.length === 7) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];
        }
        else if (ugyIntezesiHataridoArray.length === 10) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];

            ugyKezdeteDateStr = ugyIntezesiHataridoArray[7];
            ugyKezdeteHourStr = ugyIntezesiHataridoArray[8];
            ugyKezdeteMinuteStr = ugyIntezesiHataridoArray[9];

            SetUgyintezesKezdete(ugyKezdeteDateStr, ugyKezdeteHourStr, ugyKezdeteMinuteStr);
            SetUgyintezesHatarido(shortDateStr, hourStr_irat, minuteStr_irat);
        }

        if (shortDateStr !== null) {
            // határidő formázott (yyyy.mm.dd) beírása
            ugyIntezesiHataridoTextBox.value = shortDateStr;
        }

        ugyIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);
        if (strIntezesiIdoKotott === "1" || strUgyiratHataridoKitolas === "0") {
            setIratHataridoControlEnableState(false);
        }
        else {
            setIratHataridoControlEnableState(true);
        }

        if (ugyiratHataridoKitolasUgyiratHiddenField) {
            if (strUgyiratHataridoKitolas === '0') {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '0';
            }
            else {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '1';
            }
        }

        // TOOLTIP
        ugyIntezesiHataridoTextBox.title = tooltip;
    }
    //        else
    //        {
    //            return false;
    //        }
}

function SetIratIntezesiIdoManual() {
    //alert("SetIratIntezesiIdoManual");
    var IratIntezesiIdo_DropDownList = $get(IratIntezesiIdo_DropDownList_Id);
    var IratintezesiIdo = '';
    if (IratIntezesiIdo_DropDownList) {
        if (IratIntezesiIdo_DropDownList.selectedIndex >= 0) { IratintezesiIdo = IratIntezesiIdo_DropDownList.options[IratIntezesiIdo_DropDownList.selectedIndex].value; }
    }

    var IratIntezesiIdoegyseg_DropDownList = $get(IratIntezesiIdoegyseg_DropDownList_Id);
    var IratintezesiIdoegyseg = '';
    if (IratIntezesiIdoegyseg_DropDownList) {
        if (IratIntezesiIdoegyseg_DropDownList.selectedIndex >= 0) { IratintezesiIdoegyseg = IratIntezesiIdoegyseg_DropDownList.options[IratIntezesiIdoegyseg_DropDownList.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumIrat = Get_Irat_UgyintezesKezdete();
    Ajax_eRecord.GetIntezesiIdoWithToolTipManual(
        IratintezesiIdo, IratintezesiIdoegyseg, userId, ugyintezesKezdeteDatumIrat, OnWsCallBack_SetIratIntezesiHatarido);
}
///Irat intezesi ido v2
function SetIratIntezesiIdoManualv2() {
    //alert("SetIratIntezesiIdoManualv2");
    var inputUgyintezesiNapok = $get(inputUgyintezesiNapok_Id);
    var IratintezesiIdo = '';
    try {
        IratintezesiIdo = inputUgyintezesiNapok.value;
    } catch (e) {
        return;
    }

    var IratIntezesiIdoegyseg_DropDownList = $get(IratIntezesiIdoegyseg_DropDownList_Id);
    var IratintezesiIdoegyseg = '';
    if (IratIntezesiIdoegyseg_DropDownList) {
        if (IratIntezesiIdoegyseg_DropDownList.selectedIndex >= 0) { IratintezesiIdoegyseg = IratIntezesiIdoegyseg_DropDownList.options[IratIntezesiIdoegyseg_DropDownList.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumIrat = Get_Irat_UgyintezesKezdete();
    Ajax_eRecord.GetIntezesiIdoWithToolTipManual(
        IratintezesiIdo, IratintezesiIdoegyseg, userId, ugyintezesKezdeteDatumIrat, OnWsCallBack_SetIratIntezesiHatarido);
}
/**
 * OnWsCallBack_SetUgyIntezesiHatarido
 * @param {string} result - OnWsCallBack_SetUgyIntezesiHatarido
 */
function OnWsCallBack_SetUgyIntezesiHatarido(result) {
    var ugyIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);

    var ugyiratHataridoKitolasUgyiratHiddenField = $get(UgyiratHataridoKitolas_Ugyirat_HiddenField_Id);
    //alert("OnWsCallBack_SetUgyIntezesiHatarido==" + result);
    if (ugyIntezesiHataridoTextBox) {
        var shortDateStr = null;
        var tooltip = '';
        var strUgyiratHataridoKitolas = '';

        var hourStr_irat = null;
        var minuteStr_irat = null;

        var ugyKezdeteDateStr = null;
        var ugyKezdeteHourStr = null;
        var ugyKezdeteMinuteStr = null;

        var ugyIntezesiHataridoArray = new Array();
        ugyIntezesiHataridoArray = result.split(';');

        if (ugyIntezesiHataridoArray.length === 5) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
        }
        else if (ugyIntezesiHataridoArray.length === 7) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];
        }
        else if (ugyIntezesiHataridoArray.length === 10) {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
            shortDateStr = ugyIntezesiHataridoArray[2];
            tooltip = ugyIntezesiHataridoArray[3];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            hourStr_irat = ugyIntezesiHataridoArray[5];
            minuteStr_irat = ugyIntezesiHataridoArray[6];

            ugyKezdeteDateStr = ugyIntezesiHataridoArray[7];
            ugyKezdeteHourStr = ugyIntezesiHataridoArray[8];
            ugyKezdeteMinuteStr = ugyIntezesiHataridoArray[9];
            
            SetUgyintezesKezdete(ugyKezdeteDateStr, ugyKezdeteHourStr, ugyKezdeteMinuteStr);
            SetUgyintezesHatarido(shortDateStr, hourStr_irat, minuteStr_irat);
        }

        if (shortDateStr !== null) {
            // határidő formázott (yyyy.mm.dd) beírása
            ugyIntezesiHataridoTextBox.value = shortDateStr;
        }

        ugyIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);

        if (ugyiratHataridoKitolasUgyiratHiddenField) {
            if (strUgyiratHataridoKitolas === '0') {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '0';
            }
            else {
                ugyiratHataridoKitolasUgyiratHiddenField.value = '1';
            }
        }
        ugyIntezesiHataridoTextBox.title = tooltip;
    }
}

///Ugy intezesi ido v2
function SetUgyIntezesiIdoManualv2() {
    //alert("SetUgyIntezesiIdoManualv2");
    var inputUgyintezesiNapok = $get(inputUgyintezesiNapok_Id);
    var intezesiIdo = '';
    try {
        intezesiIdo = inputUgyintezesiNapok.value;
    } catch (e) {
        return;
    }

    var IntezesiIdoegys_DropDownList_IdValue = null;
    try {
        IntezesiIdoegys_DropDownList_IdValue = $get(IntezesiIdoegyseg_DropDownList_Id);
    } catch (e) {
        return;
    }

    var ugyintezesiIdoegyseg = '';
    if (IntezesiIdoegys_DropDownList_IdValue) {
        if (IntezesiIdoegys_DropDownList_IdValue.selectedIndex >= 0) { ugyintezesiIdoegyseg = IntezesiIdoegys_DropDownList_IdValue.options[IntezesiIdoegys_DropDownList_IdValue.selectedIndex].value; }
    }

    var ugyintezesKezdeteDatumIrat = Get_UgyIrat_UgyintezesKezdete();
    //alert("ugyintezesKezdeteDatumIrat=" + ugyintezesKezdeteDatumIrat);
    Ajax_eRecord.GetIntezesiIdoWithToolTipManual(
        intezesiIdo, ugyintezesiIdoegyseg, userId, ugyintezesKezdeteDatumIrat, OnWsCallBack_SetUgyIntezesiHatarido);
}

function SetUgyintezesHatarido(hatidoDateStr, hatidoHourStr, hatidoMinuteStr) {

    try {
        //var hataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        var hataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
        var hataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);

        //if (hatidoDateStr !== null) {
        //    hataridoTextBox.value = hatidoDateStr;
        //}
        //alert("SetUgyintezesHatarido " + hatidoDateStr, hatidoHourStr, hatidoMinuteStr);
        if (hatidoHourStr !== null && hatidoHourStr !== '') {
            hataridoHourTextBox.value = hatidoHourStr;
        }
        else {
            hataridoHourTextBox.value = "00";
        }

        if (hatidoMinuteStr !== null && hatidoMinuteStr !== '') {
            hataridoMinuteTextBox.value = hatidoMinuteStr;
        }
        else {
            hataridoMinuteTextBox.value = "00";
        }
    } catch (e) {
        //alert(e);
    }

}

