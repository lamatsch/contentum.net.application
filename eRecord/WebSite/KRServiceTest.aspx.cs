﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KRServiceTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnFeldolgozas_Click(object sender, EventArgs e)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        try
        {
            svc.PostaFiokFeldolgozas();
        }
        catch (Exception ex)
        {
            output.Text = ex.Message;
        }
    }
}