﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="KezbesitesiCsomagokList.aspx.cs" Inherits="KezbesitesiCsomagokList" Title="Kézbesítési csomagok" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="KezbesitesiCsomagokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
    <asp:UpdatePanel ID="KezbesitesiCsomagokUpdatePanel" Runat="server" OnLoad="KezbesitesiCsomagokUpdatePanel_Load">
        <ContentTemplate>
            <ajaxToolkit:CollapsiblePanelExtender ID="KezbesitesiCsomagokCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" ExpandControlID="KezbesitesiCsomagokCPEButton" CollapseControlID="KezbesitesiCsomagokCPEButton"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="KezbesitesiCsomagokCPEButton"
                ExpandedSize="0" ExpandedText="Küldemények listája" CollapsedText="Küldemények listája">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel ID="Panel1" runat="server">
             <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                
                <asp:GridView ID="KezbesitesiCsomagokGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                    OnRowCommand="KezbesitesiCsomagokGridView_RowCommand" OnPreRender="KezbesitesiCsomagokGridView_PreRender"
                    OnSorting="KezbesitesiCsomagokGridView_Sorting" OnRowDataBound="KezbesitesiCsomagokGridView_RowDataBound"
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                &nbsp;&nbsp;
                                <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:CommandField>                        
                       
                        <asp:BoundField DataField="Letrehozo_Nev" HeaderText="Felhasználó" SortExpression="Letrehozo_Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                      
                                                 
                        <asp:BoundField DataField="LetrehozasIdo" HeaderText="Létrehozás&nbsp;ideje" SortExpression="LetrehozasIdo">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>   
                        
                        <asp:BoundField DataField="TetelSzam" HeaderText="Tételszám" SortExpression="TetelSzam">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ItemTemplate>
                                <asp:Label ID="labelTipusId" runat="server" Text='<%#Eval("Tipus")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <%--<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                       </asp:TemplateField>--%>
                       
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
                </td>
                                 </tr>
                             </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
           </td>
        </tr>
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
       <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                            CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        
                                        <asp:Panel ID="Panel8" runat="server">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%"
                                             OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                             OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
                                           
                                                <ajaxToolkit:TabPanel ID="KezbesitesiTetelekTabPanel" runat="server" TabIndex="0">
                                                    <HeaderTemplate>
                                                        <asp:UpdatePanel ID="LabelUpdatePanelKezbesitesiTetelek" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="headerKezbesitesiTetelek" runat="server" Text="Kézbesítési tételek"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="KezbesitesiTetelekUpdatePanel" runat="server" OnLoad="KezbesitesiTetelekUpdatePanel_Load">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="KezbesitesiTetelekPanel" runat="server" Visible="true" Width="100%">
                                                                    <uc1:SubListHeader ID="KezbesitesiTetelekSubListHeader" runat="server" />
                                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                                <ajaxToolkit:CollapsiblePanelExtender ID="KezbesitesiTetelekCPE" runat="server" TargetControlID="panelKezbesitesiTetelekList"
                                                                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                    AutoExpand="false" ExpandedSize="0">
                                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                                                <asp:Panel ID="panelKezbesitesiTetelekList" runat="server">
                                                                                    <asp:GridView ID="KezbesitesiTetelekGridView" runat="server" AutoGenerateColumns="False"
                                                                                        CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                                                        BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                                        OnPreRender="KezbesitesiTetelekGridView_PreRender" OnRowCommand="KezbesitesiTetelekGridView_RowCommand"
                                                                                        DataKeyNames="Id" OnSorting="KezbesitesiTetelekGridView_Sorting" OnRowDataBound="KezbesitesiTetelekGridView_RowDataBound" >
                                                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <div class="DisableWrap">
                                                                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                                    </div>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                                        CssClass="HideCheckBoxText" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                                                HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                                SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                                <HeaderStyle Width="25px" />
                                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            </asp:CommandField>
                                                                                            <asp:BoundField DataField="Atado_Nev" HeaderText="Átadó" SortExpression="Atado_Nev">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="Cimzett_Nev" HeaderText="Címzett" SortExpression="Cimzett_Nev">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="Azonosito" HeaderText="Azonosító" SortExpression="Azonosito">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="220px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="Barcode" HeaderText="Vonalkód" SortExpression="Barcode">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="220px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <asp:LinkButton ID="headerTipus" runat="server" CommandName="Sort" CommandArgument="Obj_type">Típus</asp:LinkButton>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="labelTipus" runat="server" Text='<%#Eval("Obj_type")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="labelTipusId" runat="server" Text='<%#Eval("Obj_Id")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>                                                                                           
                                                                                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Átir.&nbsp;dátuma" SortExpression="LetrehozasIdo">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="AtadasDat" HeaderText="Átadás&nbsp;dátuma" SortExpression="AtadasDat">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="AtvetelDat" HeaderText="Átvétel&nbsp;dátuma" SortExpression="AtvetelDat">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="Atvevo_Nev" HeaderText="Átvevő" SortExpression="Atvevo_Nev">
                                                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="labelAllapot" runat="server" Text='<%#Eval("Allapot")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
       </tr>
    </table>
</asp:Content>
