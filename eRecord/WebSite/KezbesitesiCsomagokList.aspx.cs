using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class KezbesitesiCsomagokList : Contentum.eUtility.UI.PageBase
{    
    public const string kuldemeny = "K�ldem�ny";
    public const string iratpeldany = "Iratp�ld�ny";
    public const string ugyirat = "�gyirat";
    UI ui = new UI();
    private int selectedKezbFejRowNumber = -1;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KezbesitesiCsomagokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        KezbesitesiTetelekSubListHeader.RowCount_Changed += new EventHandler(KezbesitesiTetelekSubListHeader_RowCount_Changed);

        KezbesitesiTetelekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(KezbesitesiTetelekSubListHeader_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ListHeader1.HeaderLabel = Resources.List.KezbesitesiCsomagokListHeaderTitle;

        ListHeader1.SearchObjectType = typeof(EREC_IraKezbesitesiFejekSearch);

        #region Ikonok l�that�s�ga

        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.InvalidateVisible = false;

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.UgyiratpotlonyomtatasVisible = true;

        #endregion

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KezbesitesiCsomagokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, KezbesitesiCsomagokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("KezbesitesiCsomagokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidth, Defaults.PopupHeight, KezbesitesiCsomagokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KezbesitesiCsomagokGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KezbesitesiCsomagokListajaPrintForm.aspx");

        string column_v = "";
        for (int i = 0; i < KezbesitesiCsomagokGridView.Columns.Count; i++)
        {
            if (KezbesitesiCsomagokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('KezbesitesiCsomagokSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(KezbesitesiCsomagokGridView.ClientID);

        ListHeader1.UgyiratpotlonyomtatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(KezbesitesiCsomagokGridView.ClientID);
        //ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(KezbesitesiCsomagokGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(KezbesitesiCsomagokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, KezbesitesiCsomagokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = KezbesitesiCsomagokGridView;

        KezbesitesiTetelekSubListHeader.NewEnabled = false;
        KezbesitesiTetelekSubListHeader.ModifyEnabled = false;       
        //KezbesitesiTetelekSubListHeader.InvalidateEnabled = false;

        KezbesitesiTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KezbesitesiTetelekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        KezbesitesiTetelekSubListHeader.ButtonsClick += new CommandEventHandler(KezbesitesiTetelekSubListHeader_ButtonsClick);

      
        //selectedRecordId kezel�se
        KezbesitesiTetelekSubListHeader.AttachedGridView = KezbesitesiTetelekGridView;
        /**/
    
        ui.SetClientScriptToGridViewSelectDeSelectButton(KezbesitesiCsomagokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(KezbesitesiTetelekGridView);
        
        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraKezbesitesiFejekSearch());

        if (!IsPostBack) KezbesitesiCsomagokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = false;
        ListHeader1.ModifyEnabled = false;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiCsomag" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.LockEnabled = false;
        ListHeader1.UnlockEnabled = false;

        //KezbesitesiTetelekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_IktatokonyvList");

        KezbesitesiTetelekSubListHeader.NewEnabled = false;
        KezbesitesiTetelekSubListHeader.ViewEnabled = true;
        KezbesitesiTetelekSubListHeader.ModifyEnabled = false;
        KezbesitesiTetelekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelSztorno");

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        string column_v = "";
        for (int i = 0; i < KezbesitesiCsomagokGridView.Columns.Count; i++)
        {
            if (KezbesitesiCsomagokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('KezbesitesiCsomagokSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
    }


    #endregion

    #region Master List

    protected void KezbesitesiCsomagokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KezbesitesiCsomagokGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KezbesitesiCsomagokGridView", ViewState, SortDirection.Descending);

        KezbesitesiCsomagokGridViewBind(sortExpression, sortDirection);
    }

    protected void KezbesitesiCsomagokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_IraKezbesitesiFejekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiFejekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraKezbesitesiFejekSearch search = null;

        // Keres�si objektum kiv�tele a sessionb�l; ha nincs benne, a default sz�r�ssel beletessz�k
        if (!Search.IsSearchObjectInSession(Page, typeof(EREC_IraKezbesitesiFejekSearch)))
        {
            search = (EREC_IraKezbesitesiFejekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraKezbesitesiFejekSearch), Page);

            // sessionbe ment�s
            Search.SetSearchObject(Page, search);
        }

        search = (EREC_IraKezbesitesiFejekSearch)Search.GetSearchObject(Page, new EREC_IraKezbesitesiFejekSearch());
    
        search.OrderBy = Search.GetOrderBy("KezbesitesiCsomagokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(execParam, search);

        UI.GridViewFill(KezbesitesiCsomagokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        
    }

    protected void KezbesitesiCsomagokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbTitkos", "Titkos");
    }

    protected void KezbesitesiCsomagokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = KezbesitesiCsomagokGridView.PageIndex;

        KezbesitesiCsomagokGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = KezbesitesiCsomagokGridView.PageCount;

        if (prev_PageIndex != KezbesitesiCsomagokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, KezbesitesiCsomagokCPE);
            KezbesitesiCsomagokGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, KezbesitesiCsomagokCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(KezbesitesiCsomagokGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KezbesitesiCsomagokGridViewBind();
    }

    protected void KezbesitesiCsomagokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());

            selectedKezbFejRowNumber = selectedRowNumber;

            UI.SetGridViewCheckBoxesToSelectedRow(KezbesitesiCsomagokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //if (Header.Text.IndexOf(" (") > 0)
            //    Header.Text = Header.Text.Remove(Header.Text.IndexOf(" ("));
            //if (headerKezbesitesiTetelek.Text.IndexOf(" (") > 0)
            //    headerKezbesitesiTetelek.Text = headerKezbesitesiTetelek.Text.Remove(headerKezbesitesiTetelek.Text.IndexOf(" ("));
            //if (Label1.Text.IndexOf(" (") > 0)
            //    Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            ActiveTabRefresh(TabContainer1, id);
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            
            //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("KezbesitesiCsomagokForm.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, KezbesitesiCsomagokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraKezbesitesiFejek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, KezbesitesiCsomagokUpdatePanel.ClientID);
                     
            //KezbesitesiTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraKezbesitesiTetelek_IktatoKonyvek_Form.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, KezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            EREC_IraKezbesitesiFejekService fej_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiFejekService();
            
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            Result fej_result = fej_service.Get(execParam);

            EREC_IraKezbesitesiFejek erec_IraKezbesitesiFejek = (EREC_IraKezbesitesiFejek)fej_result.Record;

            string UtolsoNyomtatas_Id = erec_IraKezbesitesiFejek.UtolsoNyomtatas_Id;

            if (String.IsNullOrEmpty(UtolsoNyomtatas_Id))
            {
                ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + id);
            }
            else
            {
                Contentum.eDocument.Service.KRT_DokumentumokService service = Contentum.eRecord.Utility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

                ExecParam dok_execParam = UI.SetExecParamDefault(Page, new ExecParam());
                dok_execParam.Record_Id = UtolsoNyomtatas_Id;

                Result result = service.Get(dok_execParam);

                KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)result.Record;

                //string url = krt_Dokumentumok.External_Link.ToString();
                string url = "GetDocumentContent.aspx?id=" + krt_Dokumentumok.Id;

                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(url);
                
                ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm(url);
            }

            EREC_IraKezbesitesiTetelekService tetel_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

            erec_IraKezbesitesiTetelekSearch.WhereByManual = " and (EREC_IraKezbesitesiTetelek.KezbesitesFej_Id='" + id + "' or EREC_IraKezbesitesiTetelek.AtveteliFej_Id='" + id + "')";

            Result tetel_result = tetel_service.GetAll(execParam, erec_IraKezbesitesiTetelekSearch);

            string ugyiratIds = "";
            Boolean kolcs = false;

            foreach (DataRow _row in tetel_result.Ds.Tables[0].Rows)
            {
                ugyiratIds = ugyiratIds + "'" + _row["Obj_Id"].ToString() + "',";
            }
            if (!string.IsNullOrEmpty(ugyiratIds))
            {
                ugyiratIds = ugyiratIds.TrimEnd(',');

                EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                erec_UgyUgyiratokSearch.Id.Value = ugyiratIds;
                erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

                erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.Allapot = '04' ";

                Result ugyresult = ugyservice.GetAllWithExtensionForUgyiratpotlo(execParam, erec_UgyUgyiratokSearch);

                foreach (DataRow _row in ugyresult.Ds.Tables[0].Rows)
                {
                    kolcs = true;
                }
            }

            if (kolcs)
            {
                ListHeader1.UgyiratpotlonyomtatasOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratpotloLapPrintForm.aspx?fejId=" + id);
            }
            else
            {
                ListHeader1.UgyiratpotlonyomtatasOnClientClick = "alert('Nincs a kijel�lt t�telek k�z�tt kik�lcs�nz�tt!')";
            }
        }
    }

    protected void KezbesitesiCsomagokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KezbesitesiCsomagokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedKezbesitesiCsomagok();
        //    KezbesitesiCsomagokGridViewBind();
        //}
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            //case CommandName.Lock:
            //    LockSelectedIraIktatoKonyvRecords();
            //    KezbesitesiCsomagokGridViewBind();
            //    break;

            //case CommandName.Unlock:
            //    UnlockSelectedIraIktatoKonyvRecords();
            //    KezbesitesiCsomagokGridViewBind();
            //    break;
            case CommandName.SendObjects:
                SendMailSelectedKezbesitesiCsomagok();
                break;
        }
    }

    //private void LockSelectedIraIktatoKonyvRecords()
    //{
    //    LockManager.LockSelectedGridViewRecords(KezbesitesiCsomagokGridView, "EREC_IraKezbesitesiFejek"
    //        , "KezbesitesiCsomagLock", "KezbesitesiCsomagForceLock"
    //         , Page, EErrorPanel1, ErrorUpdatePanel);
    //}

    //private void UnlockSelectedIraIktatoKonyvRecords()
    //{
    //    LockManager.UnlockSelectedGridViewRecords(KezbesitesiCsomagokGridView, "EREC_IraKezbesitesiFejek"
    //        , "IktatoKonyvLock", "IktatoKonyvForceLock"
    //        , Page, EErrorPanel1, ErrorUpdatePanel);
    //}

    ///// <summary>
    ///// T�rli a KezbesitesiCsomagokGridView -ban kijel�lt elemeket
    ///// </summary>
    //private void deleteSelectedKezbesitesiCsomagok()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "IktatoKonyvInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(KezbesitesiCsomagokGridView, EErrorPanel1, ErrorUpdatePanel);
    //        EREC_KezbesitesiCsomagokService service = eRecordService.ServiceFactory.GetEREC_KezbesitesiCsomagokService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    /// <summary>
    /// Elkuldi emailben a KezbesitesiCsomagokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedKezbesitesiCsomagok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(KezbesitesiCsomagokGridView, EErrorPanel1, ErrorUpdatePanel),
                UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraKezbesitesiFejek");
        }
    }

    protected void KezbesitesiCsomagokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KezbesitesiCsomagokGridViewBind(e.SortExpression, UI.GetSortToGridView("KezbesitesiCsomagokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (KezbesitesiCsomagokGridView.SelectedIndex == -1)
        {
            return;
        }
        string kezbesitesiFejId = UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, kezbesitesiFejId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string kezbesitesiFejId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                KezbesitesiTetelekGridViewBind(kezbesitesiFejId);
                KezbesitesiTetelekPanel.Visible = true;
                break;            
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(KezbesitesiTetelekGridView);
                break;            
        }
    }

    protected void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        KezbesitesiTetelekSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(KezbesitesiTetelekTabPanel))
        {
            KezbesitesiTetelekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(KezbesitesiTetelekGridView));
        }
        //else if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
        //{
        //    JogosultakGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JogosultakGridView), UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
        //}
    }


    #endregion



    #region KezbesitesiTetelek Detail

    protected void KezbesitesiTetelekGridViewBind(string kezbesitesiFejId)
    {
        string sortExpression = Search.GetSortExpressionFromViewState("KezbesitesiTetelekGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KezbesitesiTetelekGridView", ViewState, SortDirection.Descending);

        KezbesitesiTetelekGridViewBind(kezbesitesiFejId, sortExpression, sortDirection);

    }

    protected void KezbesitesiTetelekGridViewBind(string kezbesitesiFejId, string SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(kezbesitesiFejId))
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch(true);

            if (selectedKezbFejRowNumber == -1)
            {
                selectedKezbFejRowNumber = KezbesitesiCsomagokGridView.SelectedIndex;
            }

            Label tipusLabel = null;
            if (selectedKezbFejRowNumber > -1)
            {
                tipusLabel = (Label)KezbesitesiCsomagokGridView.Rows[selectedKezbFejRowNumber].FindControl("labelTipusId");                
            }

            if (tipusLabel != null && tipusLabel.Text == "V")
            {
                search.AtveteliFej_Id.Value = kezbesitesiFejId;
                search.AtveteliFej_Id.Operator = Query.Operators.equals;
            }
            else
            {
                search.KezbesitesFej_Id.Value = kezbesitesiFejId;
                search.KezbesitesFej_Id.Operator = Query.Operators.equals;
            }

            search.OrderBy = Search.GetOrderBy("KezbesitesiTetelekGridView", ViewState, SortExpression, SortDirection);

            KezbesitesiTetelekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(execParam, search);

            UI.GridViewFill(KezbesitesiTetelekGridView, res, KezbesitesiTetelekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerKezbesitesiTetelek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(KezbesitesiTetelekGridView);
        }
        else
        {
            ui.GridViewClear(KezbesitesiTetelekGridView);
        }

    }

    //SubListHeader f�ggv�nyei(4)
    private void KezbesitesiTetelekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            KezbesitesiTetelSztorno();
            KezbesitesiCsomagokGridViewBind();
            KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
        }
    }


    // K�zbes�t�si t�tel sztorn�z�sa
    // (Ezzel a felel�s vissza�ll az el�z� felel�sre)
    private void KezbesitesiTetelSztorno()
    {
        String tetel_Id = UI.GetGridViewSelectedRecordId(KezbesitesiTetelekGridView);
        if (!String.IsNullOrEmpty(tetel_Id))
        {
            if (FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelSztorno"))
            {
                EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.Sztorno(execParam, tetel_Id);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
                , Resources.Error.UINoSelectedRow);
            ErrorUpdatePanel.Update();
        }
    }


    //private void deleteSelectedKezbesitesiTetelekFromIktatokonyv()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel_IktatokonyvInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(KezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
    //        EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    private void KezbesitesiTetelekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
    }

    private void KezbesitesiTetelekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(KezbesitesiTetelekSubListHeader.RowCount);
        KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
    }

    ////GridView esem�nykezel�i(3-4)
    //////specialis, nem mindig kell kell
    //protected void KezbesitesiTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
    //        , "cb_System", "System");
    //}

    protected void KezbesitesiTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KezbesitesiTetelekGridView, selectedRowNumber, "check");
        }
    }

    protected void KezbesitesiTetelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = KezbesitesiTetelekGridView.PageIndex;

        KezbesitesiTetelekGridView.PageIndex = KezbesitesiTetelekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != KezbesitesiTetelekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, KezbesitesiTetelekCPE);
            KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(KezbesitesiTetelekSubListHeader.Scrollable, KezbesitesiTetelekCPE);
        }
        KezbesitesiTetelekSubListHeader.PageCount = KezbesitesiTetelekGridView.PageCount;
        KezbesitesiTetelekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(KezbesitesiTetelekGridView);


        ui.SetClientScriptToGridViewSelectDeSelectButton(KezbesitesiTetelekGridView);
    }


    protected void KezbesitesiTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView)
            , e.SortExpression, UI.GetSortToGridView("KezbesitesiTetelekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void KezbesitesiTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(KezbesitesiTetelekTabPanel))
                    //{
                    //    KezbesitesiTetelekGridViewBind(UI.GetGridViewSelectedRecordId(KezbesitesiCsomagokGridView));
                    //}
                    ActiveTabRefreshDetailList(KezbesitesiTetelekTabPanel);
                    break;
            }
        }
    }


    protected void KezbesitesiTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //T�pus ki�r�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty)
            {
                switch (labelTipus.Text.Trim())
                {
                    case Constants.TableNames.EREC_KuldKuldemenyek:
                        labelTipus.Text = kuldemeny;
                        break;
                    case Constants.TableNames.EREC_PldIratPeldanyok:
                        labelTipus.Text = iratpeldany;
                        break;
                    case Constants.TableNames.EREC_UgyUgyiratok:
                        labelTipus.Text = ugyirat;
                        break;
                }
            }
        }

        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }



    //Gombok kliens oldali szkripjeinek friss�t�se(1)
    private void KezbesitesiTetelekGridView_RefreshOnClientClicks(string kezbesitesiTetelId)
    {
        string id = kezbesitesiTetelId;
        if (!String.IsNullOrEmpty(id))
        {
            #region View

            KezbesitesiTetelekSubListHeader.ViewOnClientClick = "return false;";

            //a megfelel� form bek�t�se
            string url = String.Empty;
            string tipusId = String.Empty;
            GridViewRow row = KezbesitesiTetelekGridView.SelectedRow;
            if (row != null)
            {
                Label labelTipus = (Label)row.FindControl("labelTipus");
                Label labelTipusId = (Label)row.FindControl("labelTipusId");
                if (labelTipus != null && labelTipus.Text.Trim() != String.Empty && labelTipusId != null && labelTipusId.Text.Trim() != String.Empty)
                {
                    switch (labelTipus.Text.Trim())
                    {
                        case kuldemeny:
                            url = "KuldKuldemenyekForm.aspx";
                            break;
                        case iratpeldany:
                            url = "PldIratPeldanyokForm.aspx";
                            break;
                        case ugyirat:
                            url = "UgyUgyiratokForm.aspx";
                            break;
                    }

                    tipusId = labelTipusId.Text;
                }

                if (!String.IsNullOrEmpty(url))
                {
                    KezbesitesiTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick(url
                        , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + tipusId
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KezbesitesiTetelekUpdatePanel.ClientID);
                }
            }

            #endregion

            // Sztorn� (Invalidate)
            KezbesitesiTetelekSubListHeader.InvalidateOnClientClick = 
                "if (confirm('" + Resources.Question.UIConfirmHeader_KezbesitesiTetelSztorno
                   + "')) {} else {return false;}";

            if (row != null)
            {
                Label labelAllapot = (Label)row.FindControl("labelAllapot");
                if (labelAllapot != null)
                {
                    string allapot = Server.UrlDecode(labelAllapot.Text).Trim();
                    if (allapot == Constants.KezbesitesiTetel_Allapot.Atvett)
                    {
                        KezbesitesiTetelekSubListHeader.InvalidateOnClientClick = 
                            "alert('" + Resources.Error.UINemSztornozhatoKezbesitesiTetel + "');return false;";
                    }
                }
            }
        }
    }

    #endregion
}
