﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class KezbesitesiCsomagokListajaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        EREC_IraKezbesitesiFejekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiFejekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraKezbesitesiFejekSearch search = (EREC_IraKezbesitesiFejekSearch)Search.GetSearchObject(Page, new EREC_IraKezbesitesiFejekSearch());

        Result result = service.GetAllWithExtension(execParam, search);

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Where";
        column.AutoIncrement = false;
        column.Caption = "Where";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Where"] = search.ReadableWhere;
        table.Rows.Add(row);

        //string xml = result.Ds.GetXml();
        //string xsd = result.Ds.GetXmlSchema();
        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        WebRequest wr = WebRequest.Create(SP_TM_site_url + "Kezbesitesi csomagok listaja.xml");
        wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
        StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
        templateText = template.ReadToEnd();
        template.Close();
        //string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns2=\"kcsl\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>Kiss Gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>0</o:TotalTime><o:Created>2008-03-12T16:09:00Z</o:Created><o:LastSaved>2008-03-12T16:09:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>11</o:Words><o:Characters>83</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>93</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"05FC4C9D\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"E32EE660\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"toc 1\"/><w:lsdException w:name=\"toc 2\"/><w:lsdException w:name=\"toc 3\"/><w:lsdException w:name=\"toc 4\"/><w:lsdException w:name=\"toc 5\"/><w:lsdException w:name=\"toc 6\"/><w:lsdException w:name=\"toc 7\"/><w:lsdException w:name=\"toc 8\"/><w:lsdException w:name=\"toc 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Default Paragraph Font\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"Table Grid\"/><w:lsdException w:name=\"Placeholder Text\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"Light Shading\"/><w:lsdException w:name=\"Light List\"/><w:lsdException w:name=\"Light Grid\"/><w:lsdException w:name=\"Medium Shading 1\"/><w:lsdException w:name=\"Medium Shading 2\"/><w:lsdException w:name=\"Medium List 1\"/><w:lsdException w:name=\"Medium List 2\"/><w:lsdException w:name=\"Medium Grid 1\"/><w:lsdException w:name=\"Medium Grid 2\"/><w:lsdException w:name=\"Medium Grid 3\"/><w:lsdException w:name=\"Dark List\"/><w:lsdException w:name=\"Colorful Shading\"/><w:lsdException w:name=\"Colorful List\"/><w:lsdException w:name=\"Colorful Grid\"/><w:lsdException w:name=\"Light Shading Accent 1\"/><w:lsdException w:name=\"Light List Accent 1\"/><w:lsdException w:name=\"Light Grid Accent 1\"/><w:lsdException w:name=\"Medium Shading 1 Accent 1\"/><w:lsdException w:name=\"Medium Shading 2 Accent 1\"/><w:lsdException w:name=\"Medium List 1 Accent 1\"/><w:lsdException w:name=\"Revision\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Medium List 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 1 Accent 1\"/><w:lsdException w:name=\"Medium Grid 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 3 Accent 1\"/><w:lsdException w:name=\"Dark List Accent 1\"/><w:lsdException w:name=\"Colorful Shading Accent 1\"/><w:lsdException w:name=\"Colorful List Accent 1\"/><w:lsdException w:name=\"Colorful Grid Accent 1\"/><w:lsdException w:name=\"Light Shading Accent 2\"/><w:lsdException w:name=\"Light List Accent 2\"/><w:lsdException w:name=\"Light Grid Accent 2\"/><w:lsdException w:name=\"Medium Shading 1 Accent 2\"/><w:lsdException w:name=\"Medium Shading 2 Accent 2\"/><w:lsdException w:name=\"Medium List 1 Accent 2\"/><w:lsdException w:name=\"Medium List 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 1 Accent 2\"/><w:lsdException w:name=\"Medium Grid 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 3 Accent 2\"/><w:lsdException w:name=\"Dark List Accent 2\"/><w:lsdException w:name=\"Colorful Shading Accent 2\"/><w:lsdException w:name=\"Colorful List Accent 2\"/><w:lsdException w:name=\"Colorful Grid Accent 2\"/><w:lsdException w:name=\"Light Shading Accent 3\"/><w:lsdException w:name=\"Light List Accent 3\"/><w:lsdException w:name=\"Light Grid Accent 3\"/><w:lsdException w:name=\"Medium Shading 1 Accent 3\"/><w:lsdException w:name=\"Medium Shading 2 Accent 3\"/><w:lsdException w:name=\"Medium List 1 Accent 3\"/><w:lsdException w:name=\"Medium List 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 1 Accent 3\"/><w:lsdException w:name=\"Medium Grid 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 3 Accent 3\"/><w:lsdException w:name=\"Dark List Accent 3\"/><w:lsdException w:name=\"Colorful Shading Accent 3\"/><w:lsdException w:name=\"Colorful List Accent 3\"/><w:lsdException w:name=\"Colorful Grid Accent 3\"/><w:lsdException w:name=\"Light Shading Accent 4\"/><w:lsdException w:name=\"Light List Accent 4\"/><w:lsdException w:name=\"Light Grid Accent 4\"/><w:lsdException w:name=\"Medium Shading 1 Accent 4\"/><w:lsdException w:name=\"Medium Shading 2 Accent 4\"/><w:lsdException w:name=\"Medium List 1 Accent 4\"/><w:lsdException w:name=\"Medium List 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 1 Accent 4\"/><w:lsdException w:name=\"Medium Grid 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 3 Accent 4\"/><w:lsdException w:name=\"Dark List Accent 4\"/><w:lsdException w:name=\"Colorful Shading Accent 4\"/><w:lsdException w:name=\"Colorful List Accent 4\"/><w:lsdException w:name=\"Colorful Grid Accent 4\"/><w:lsdException w:name=\"Light Shading Accent 5\"/><w:lsdException w:name=\"Light List Accent 5\"/><w:lsdException w:name=\"Light Grid Accent 5\"/><w:lsdException w:name=\"Medium Shading 1 Accent 5\"/><w:lsdException w:name=\"Medium Shading 2 Accent 5\"/><w:lsdException w:name=\"Medium List 1 Accent 5\"/><w:lsdException w:name=\"Medium List 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 1 Accent 5\"/><w:lsdException w:name=\"Medium Grid 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 3 Accent 5\"/><w:lsdException w:name=\"Dark List Accent 5\"/><w:lsdException w:name=\"Colorful Shading Accent 5\"/><w:lsdException w:name=\"Colorful List Accent 5\"/><w:lsdException w:name=\"Colorful Grid Accent 5\"/><w:lsdException w:name=\"Light Shading Accent 6\"/><w:lsdException w:name=\"Light List Accent 6\"/><w:lsdException w:name=\"Light Grid Accent 6\"/><w:lsdException w:name=\"Medium Shading 1 Accent 6\"/><w:lsdException w:name=\"Medium Shading 2 Accent 6\"/><w:lsdException w:name=\"Medium List 1 Accent 6\"/><w:lsdException w:name=\"Medium List 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 1 Accent 6\"/><w:lsdException w:name=\"Medium Grid 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 3 Accent 6\"/><w:lsdException w:name=\"Dark List Accent 6\"/><w:lsdException w:name=\"Colorful Shading Accent 6\"/><w:lsdException w:name=\"Colorful List Accent 6\"/><w:lsdException w:name=\"Colorful Grid Accent 6\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"Bibliography\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"00EC0F1C\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"004117C7\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"004117C7\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"2050\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"004117C7\"/><wsp:rsid wsp:val=\"004117C7\"/><wsp:rsid wsp:val=\"006220EE\"/><wsp:rsid wsp:val=\"00EC0F1C\"/></wsp:rsids></w:docPr><w:body><ns2:NewDataSet><w:p wsp:rsidR=\"00EC0F1C\" wsp:rsidRPr=\"004117C7\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"004117C7\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"004117C7\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Kézbesítési csomagok listája</w:t></w:r></w:p><w:p wsp:rsidR=\"004117C7\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"004117C7\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>(</w:t></w:r><ParentTable><Where/></ParentTable><w:r><w:t>)</w:t></w:r></w:p><w:p wsp:rsidR=\"004117C7\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"004117C7\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"392\"/><w:gridCol w:w=\"5103\"/><w:gridCol w:w=\"3261\"/><w:gridCol w:w=\"2693\"/></w:tblGrid><w:tr wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidTr=\"006220EE\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"006220EE\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"5103\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"006220EE\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"006220EE\"><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr><w:t>Felhasználó</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3261\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"006220EE\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"006220EE\"><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr><w:t>Létrehozás ideje</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"006220EE\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"006220EE\"><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr><w:t>Tételszám</w:t></w:r></w:p></w:tc></w:tr><Table ns2:repeater=\"true\"><w:tr wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidTr=\"006220EE\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"004117C7\" wsp:rsidRPr=\"006220EE\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"006220EE\"><w:pPr><w:pStyle w:val=\"Listaszerbekezds\"/><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"1\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"0\" w:first-line=\"0\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><Letrehozo_Nev><w:tc><w:p /></w:tc></Letrehozo_Nev><LetrehozasIdo><w:tc><w:p /></w:tc></LetrehozasIdo><TetelSzam><w:tc><w:p /></w:tc></TetelSzam></w:tr></Table></w:tbl><w:p wsp:rsidR=\"004117C7\" wsp:rsidRDefault=\"004117C7\" wsp:rsidP=\"004117C7\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p></ns2:NewDataSet><w:sectPr wsp:rsidR=\"004117C7\" wsp:rsidSect=\"004117C7\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }

        result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

        string filename = "";

        if (pdf)
        {
            filename = "Kezbesitesi_csomagok_listaja_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            filename = "Kezbesitesi_csomagok_listaja_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }

        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (string.IsNullOrEmpty(csop_result.ErrorCode))
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals("3"))
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

        byte[] res = (byte[])result.Record;

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
            }
        }
    }
}
