﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="KezbesitesiCsomagokSearch.aspx.cs" Inherits="KezbesitesiCsomagokSearch" Title="Untitled Page" %>


<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register src="Component/DatumIntervallum_SearchCalendarControl.ascx" tagname="DatumIntervallum_SearchCalendarControl" tagprefix="uc3" %>
<%@ Register src="Component/FelhasznaloCsoportTextBox.ascx" tagname="FelhasznaloCsoportTextBox" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label16" runat="server" Text="Felhasználó:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc4:FelhasznaloCsoportTextBox ID="Letrehozo_FelhasznaloCsoportTextBox" 
                                        runat="server" SearchMode="True" />
                                </td>
                            </tr>
                                                        
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label17" runat="server" Text="Létrehozás ideje:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:DatumIntervallum_SearchCalendarControl ID="LetrehozasIdo_DatumIntervallum_SearchCalendarControl" 
                                        runat="server" Validate="False" ValidateDateFormat="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label18" runat="server" Text="Típus:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButtonList ID="Tipus_RadioButtonList" runat="server" 
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Value="A">Átadási csomag</asp:ListItem>
                                        <asp:ListItem Value="V">Átvételi csomag</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="0">Mindkettő</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;</td>
                                <td class="mrUrlapMezo">
                                    &nbsp;</td>
                            </tr>
                                                        
                            <tr class="urlapSor">
                                <td colspan="2">
                                    <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                        runat="server"></uc10:TalalatokSzama_SearchFormComponent>
                                    &nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;</td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
