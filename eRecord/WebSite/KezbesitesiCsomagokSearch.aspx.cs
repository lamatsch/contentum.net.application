﻿using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class KezbesitesiCsomagokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IraKezbesitesiFejekSearch);
    private const string kcsKod_IKT_SZAM_OSZTAS = "IKT_SZAM_OSZTAS";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.IraKezbesitesiFejekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraKezbesitesiFejekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IraKezbesitesiFejekSearch)Search.GetSearchObject(Page, new EREC_IraKezbesitesiFejekSearch());              
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraKezbesitesiFejekSearch erec_IraKezbesitesiFejekSearch = null;
        if (searchObject != null) erec_IraKezbesitesiFejekSearch = (EREC_IraKezbesitesiFejekSearch)searchObject;

        if (erec_IraKezbesitesiFejekSearch != null)
        {
            Letrehozo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraKezbesitesiFejekSearch.Manual_Letrehozo_id.Value;
            Letrehozo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_IraKezbesitesiFejekSearch.Manual_LetrehozasIdo);

            if (erec_IraKezbesitesiFejekSearch.Tipus.Value == "A")
            {
                Tipus_RadioButtonList.SelectedValue = "A";
            }
            else if (erec_IraKezbesitesiFejekSearch.Tipus.Value == "V")
            {
                Tipus_RadioButtonList.SelectedValue = "V";
            }
            else
            {
                Tipus_RadioButtonList.SelectedValue = "";
            }
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IraKezbesitesiFejekSearch SetSearchObjectFromComponents()
    {
        EREC_IraKezbesitesiFejekSearch erec_IraKezbesitesiFejekSearch = (EREC_IraKezbesitesiFejekSearch)SearchHeader1.TemplateObject;
        if (erec_IraKezbesitesiFejekSearch == null)
        {
            erec_IraKezbesitesiFejekSearch = new EREC_IraKezbesitesiFejekSearch();
        }

        if (!String.IsNullOrEmpty(Letrehozo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_IraKezbesitesiFejekSearch.Manual_Letrehozo_id.Value = Letrehozo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_IraKezbesitesiFejekSearch.Manual_Letrehozo_id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Tipus_RadioButtonList.SelectedValue) && Tipus_RadioButtonList.SelectedValue != "0")
        {
            erec_IraKezbesitesiFejekSearch.Tipus.Value = Tipus_RadioButtonList.SelectedValue;
            erec_IraKezbesitesiFejekSearch.Tipus.Operator = Query.Operators.equals;
        }

        LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_IraKezbesitesiFejekSearch.Manual_LetrehozasIdo);

        return erec_IraKezbesitesiFejekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraKezbesitesiFejekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);                
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);                
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraKezbesitesiFejekSearch GetDefaultSearchObject()
    {
        return new EREC_IraKezbesitesiFejekSearch();
    }

}
