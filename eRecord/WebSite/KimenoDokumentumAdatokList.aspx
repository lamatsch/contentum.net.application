<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KimenoDokumentumAdatokList.aspx.cs" Inherits="KimenoDokumentumAdatokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 

     <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeHKP_DokumentumAdatok" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelHKP_DokumentumAdatok" runat="server" OnLoad="updatePanelHKP_DokumentumAdatok_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeHKP_DokumentumAdatok" runat="server" TargetControlID="panelHKP_DokumentumAdatok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeHKP_DokumentumAdatok" CollapseControlID="btnCpeHKP_DokumentumAdatok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeHKP_DokumentumAdatok"
                            ExpandedSize="0" ExpandedText="E-mail bor�t�kok list�ja" CollapsedText="E-mail bor�t�kok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelHKP_DokumentumAdatok" runat="server" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewHKP_DokumentumAdatok" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewHKP_DokumentumAdatok_RowCommand" 
                                     OnPreRender="gridViewHKP_DokumentumAdatok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                     DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewHKP_DokumentumAdatok_RowDataBound" OnSorting="gridViewHKP_DokumentumAdatok_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                <div style="white-space:nowrap">                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </div>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Csny.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" ToolTip="Csatolm�ny" OnClientClick='<%#String.Format("window.open(\"GetDocumentContent.aspx?id={0}\");return false;",Eval("Dokumentum_Id")) %>'/>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                            <asp:BoundField DataField="ErkeztetesiSzam" HeaderText="�rkeztet�si sz�m" SortExpression="HKP_DokumentumAdatok.ErkeztetesiSzam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                             <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="HKP_DokumentumAdatok.Allapot">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>    
                                            <asp:BoundField DataField="Nev" HeaderText="C�mzett" SortExpression="HKP_DokumentumAdatok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Email" HeaderText="C�mzett e-mail c�me" SortExpression="HKP_DokumentumAdatok.Email" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RovidNev" HeaderText="Hivatal r�vid neve" SortExpression="HKP_DokumentumAdatok.RovidNev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KapcsolatiKod" HeaderText="Kapcsolati k�d" SortExpression="HKP_DokumentumAdatok.KapcsolatiKod" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KRID" HeaderText="Hivatal KRID-je" SortExpression="HKP_DokumentumAdatok.KRID" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                             <asp:BoundField DataField="MAKKod" HeaderText="Hivatal M�K k�dja" SortExpression="HKP_DokumentumAdatok.MAKKod" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DokTipusHivatal" HeaderText="Hivatal" SortExpression="HKP_DokumentumAdatok.DokTipusHivatal">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DokTipusAzonosito" HeaderText="T�pus" SortExpression="HKP_DokumentumAdatok.DokTipusAzonosito">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DokTipusLeiras" HeaderText="T�pus n�v" SortExpression="HKP_DokumentumAdatok.DokTipusLeiras" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Hivatkoz�si sz�m" SortExpression="HKP_DokumentumAdatok.HivatkozasiSzam" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyz�s" SortExpression="HKP_DokumentumAdatok.Megjegyzes">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="F�jln�v" SortExpression="HKP_DokumentumAdatok.FileNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                <ItemTemplate>
                                                      <asp:Label ID="labelFileNev" runat="server" Text='<%# GetDocumentLink(Eval("Dokumentum_Id") as Guid?, Eval("FileNev") as string)%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ErkeztetesiDatum" HeaderText="�rkeztet�si d�tum" SortExpression="HKP_DokumentumAdatok.ErkeztetesiDatum">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="ET�rtivev�ny" SortExpression="HKP_DokumentumAdatok.ETertiveveny">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="ETertiveveny" runat="server" Text='<%#DisplayBoolean(Eval("ETertiveveny"))%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Iratp�ld�ny" SortExpression="HKP_DokumentumAdatok.IratPeldany_Azonosito">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                <ItemTemplate>
                                                      <asp:Label ID="labelIratpeldany" runat="server" Text='<%# GeIratPelanyLink(Eval("IratPeldany_Id") as Guid?, Eval("IratPeldany_Azonosito") as string)%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="V�lasz titkos�t�s" SortExpression="HKP_DokumentumAdatok.ValaszTitkositas" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="ValaszTitkositas" runat="server" Text='<%#DisplayBoolean(Eval("ValaszTitkositas"))%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ValaszUtvonalNev" HeaderText="V�lasz �tvonal" SortExpression="HKP_DokumentumAdatok.ValaszUtvonal" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:TemplateField  HeaderText="Rendszer�zenet" SortExpression="HKP_DokumentumAdatok.Rendszeruzenet" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="Rendszeruzenet" runat="server" Text='<%#DisplayBoolean(Eval("Rendszeruzenet"))%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                         <PagerSettings Visible="False" />
                                     </asp:GridView>
                                </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>                            
          </tr>
   </table>
</asp:Content>

