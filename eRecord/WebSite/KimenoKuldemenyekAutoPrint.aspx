<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KimenoKuldemenyekAutoPrint.aspx.cs" Inherits="KimenoKuldemenyekAutoPrint" Title="Kimen� k�ldeme�ny nyomtat�s" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Kimen� k�ldeme�ny nyomtat�s" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <div class="watermark">
        <div>
            <h1>Nyomtat�s</h1>
        </div>
    </div>
    <style>
        .watermark {
        width: 300px;
        height: 100px;
        display: block;
        position: relative;
        }

        .watermark::after {
        content: "";
        opacity: 0.2;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        position: absolute;
        z-index: -1;
        }
    </style>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
