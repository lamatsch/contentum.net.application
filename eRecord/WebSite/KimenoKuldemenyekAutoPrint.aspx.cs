using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

public partial class KimenoKuldemenyekAutoPrint : Contentum.eUtility.UI.PageBase
{
    private string ConstFelado =
          HttpUtility.UrlEncode("Buda�rs Polg�rmesteri Hivatal")
        + HttpUtility.UrlEncode(Environment.NewLine)
        + HttpUtility.UrlEncode("Buda�rs, Szabads�g �t 134,")
        + HttpUtility.UrlEncode(Environment.NewLine)
        + "2040";
    //private string ConstKrtParameterNevFelado = "EREC_KIMENO_KULDEMENYEK_AUTOPRINT_FELADO";
    private string ConstKrtParameterIdFelado = "B1023BF0-5555-6666-2222-0BE1338EB129";
    private string ConstPreview = "1";
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KimenoKuldemenyekList");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeParameters();
        Core();
    }

    private void InitializeParameters()
    {
        string felado = FindKrtParameter(ConstKrtParameterIdFelado);
        if (!string.IsNullOrEmpty(felado))
            ConstFelado = felado;
    }

    /// <summary>
    /// Core 
    /// </summary>
    private void Core()
    {
        if (Request.QueryString["ids"] != null && Request.QueryString["docId"] != null)
        {
            string iratIdsForService = ConvertToServiceIdList(Request.QueryString["ids"]);
            Result resultKuldemenyek = FindAllKuldKuldemenyek(iratIdsForService);

            if (resultKuldemenyek != null && resultKuldemenyek.Ds != null && resultKuldemenyek.Ds.Tables != null && resultKuldemenyek.Ds.Tables.Count > 0 && resultKuldemenyek.Ds.Tables[0].Rows.Count > 0)
            {
                string cimIds = GetCimekFromKuldKuldemenyek(resultKuldemenyek.Ds.Tables[0].Rows);
                Result resultCimek = FindAllKrtCimek(cimIds);
                List<string> uris = new List<string>();
                foreach (DataRow iratItem in resultKuldemenyek.Ds.Tables[0].Rows)
                {
                    if (resultCimek != null && resultCimek.Ds != null && resultCimek.Ds.Tables != null && resultCimek.Ds.Tables.Count > 0 && resultCimek.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow[] cimekLista = resultCimek.Ds.Tables[0].Select("Id = '" + resultKuldemenyek.Ds.Tables[0].Rows[0]["Cim_id"] + "'");
                        if (cimekLista != null && cimekLista.Length > 0)
                            SetUri(ref uris, iratItem, cimekLista[0]);
                        else
                            SetUri(ref uris, iratItem, null);
                    }
                    else
                        SetUri(ref uris, iratItem, null);

                }
                if (uris != null && uris.Count > 0)
                    OpenUris(uris);
            }
        }
    }
    /// <summary>
    /// Return urim
    /// </summary>
    /// <param name="docid"></param>
    /// <param name="feladoNeve"></param>
    /// <param name="cimzett"></param>
    /// <param name="varos"></param>
    /// <param name="utca"></param>
    /// <param name="irsz"></param>
    /// <param name="preview"></param>
    /// <returns></returns>
    private string GetPrintAppUri(string docid, string feladoNeve, string cimzett, string varos, string utca, string irsz, string preview, string ragszam, string ragszamTagolt, string iktatoszamok)
    {
        string ragszamPrefix = "H";
        string utcaMod = utca.Trim().Replace(" ", "_");
        string ragszamTagoltMod = ragszamTagolt.Trim().Replace(" ", "_");
        switch (docid)
        {
            case "3": //Felad�vev�ny
                if (cimzett.Length < 49)
                    return string.Format("axelprint://docid={0};preview={1};1={2};2={3};",
                        docid,
                        preview,
                        feladoNeve,
                        HttpUtility.UrlEncode(cimzett)
                        + HttpUtility.UrlEncode(Environment.NewLine)
                        + HttpUtility.UrlEncode(varos)
                        + HttpUtility.UrlEncode(", ")
                        + HttpUtility.UrlEncode(utcaMod)
                        + HttpUtility.UrlEncode(Environment.NewLine)
                        + irsz
                        );
                else
                    return string.Format("axelprint://docid={0};preview={1};1={2};2={3};",
                          docid,
                          preview,
                          feladoNeve,
                          HttpUtility.UrlEncode(cimzett)
                          + HttpUtility.UrlEncode(Environment.NewLine)
                          + HttpUtility.UrlEncode(varos)
                          + HttpUtility.UrlEncode(", ")
                          + HttpUtility.UrlEncode(utcaMod)
                          + HttpUtility.UrlEncode(" ")
                          + irsz
                          );
            case "6": //Bor�t�k
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     feladoNeve,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );

            case "171": //Bor�t�k kis szabv�nyos LC/6 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "172": //C�m bet�tlap LA/4 ablakos bor�t�kba 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "203": //Bor�t�k k�zepes LC/5 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "213": //Bor�t�k nagy LC/4 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "195": //T�RTIVEV�NY HIVATALOS IRATHOZ 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6},6={7},7={8},8={9}",
                     docid,
                     preview,
                     string.IsNullOrEmpty(ragszam) ? null : ragszamPrefix + ragszam,
                     string.IsNullOrEmpty(ragszamTagoltMod) ? null : ragszamPrefix + ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz,
                     feladoNeve,
                     HttpUtility.UrlEncode(iktatoszamok)
                     );
            default:
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     feladoNeve,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
        }
    }
    /// <summary>
    /// Send parameters to uri
    /// </summary>
    /// <param name="list"></param>
    /// <param name="rat"></param>
    /// <param name="cim"></param>
    private void SetUri(ref List<string> list, DataRow rat, DataRow cim)
    {
        if (cim != null)
            list.Add(
                GetPrintAppUri(
                Request.QueryString["docId"].ToString(),
                ConstFelado,
                rat["NevSTR_Bekuldo"].ToString(),
                cim["TelepulesNev"].ToString(),

                GetUtcaCim(cim),

                cim["IRSZ"].ToString(),
                ConstPreview,
                rat["Ragszam"].ToString(),
                RagszamTagolas(rat["Ragszam"].ToString()),
                rat["Azonosito"].ToString().Trim()
                ));
        else
            list.Add(
                GetPrintAppUri(
                 Request.QueryString["docId"].ToString(),
                 ConstFelado,
                 rat["NevSTR_Bekuldo"].ToString(),
                 rat["CimSTR_Bekuldo"].ToString(),
                 string.Empty,
                 string.Empty,
                 ConstPreview,
                 rat["Ragszam"].ToString(),
                 RagszamTagolas(rat["Ragszam"].ToString()),
                 rat["Azonosito"].ToString().Trim()
                 ));
    }
    private string GetTelepulesCim(DataRow cim)
    {
        if (string.IsNullOrEmpty(cim["KozteruletNev"].ToString()))
            return string.Empty;
        else
            return cim["TelepulesNev"].ToString();
    }

    private string GetUtcaCim(DataRow cim)
    {
        // BLG_288
        //if (cim["Tipus"].ToString() == "02") return string.Format("Pf. {0}", cim["Hazszam"].ToString());
        if (cim["Tipus"].ToString() == "02")
        {
            if (String.IsNullOrEmpty(cim["Hazszam"].ToString())) { return String.Empty; }
            else return string.Format("Pf. {0}", cim["Hazszam"].ToString());
        }
        else if (string.IsNullOrEmpty(cim["KozteruletNev"].ToString()))
            return cim["CimTobbi"].ToString();
        else
            return string.Format("{0} {1} {2}{3}{4} {5} {6}{7}",
                                cim["KozteruletNev"].ToString(),
                                cim["KozteruletTipusNev"].ToString(),
                                cim["Hazszam"].ToString(),
                                cim["HazszamBetujel"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["HazszamBetujel"].ToString()),
                                cim["Lepcsohaz"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["Lepcsohaz"].ToString()),
                                cim["Szint"].ToString(),
                                cim["Ajto"].ToString(),
                                cim["AjtoBetujel"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["AjtoBetujel"].ToString())
                                );
    }

    /// <summary>
    /// Open uri list in browser
    /// </summary>
    /// <param name="uris"></param>
    private void OpenUris(List<string> uris)
    {
        if (uris.Count <= 0)
            return;

        if (uris.Count == 1)
        {
            if (Request.Browser.Browser.ToLower() == "chrome")
                OpenInChrome(uris[0]);
            else
                OpenInIE(uris[0]);
        }
        else
            OpenMultiple(uris);
    }
    /// <summary>
    /// Open multiple uris
    /// </summary>
    /// <param name="uris"></param>

    #region BROWSER
    private void OpenMultiple(List<string> uris)
    {
        for (int i = 0; i < uris.Count; i++)
        {
            string s = "window.open('" + uris[0] + "', 'popup_window', 'resizable=no');close();";
            ClientScript.RegisterStartupScript(this.GetType(), "script" + (i + 1), s, true);
        }
    }
    /// <summary>
    /// Open uri in chrome 
    /// </summary>
    /// <param name="uri"></param>
    private void OpenInChrome(string uri)
    {
        Response.Redirect(uri);
    }
    /// <summary>
    /// Open uri in InternetExplorer
    /// </summary>
    /// <param name="uri"></param>
    private void OpenInIE(string uri)
    {
        string script = string.Format("<script>window.open('{0}','_self')</script>", uri);
        Response.Write(script);
    }

    #endregion

    #region HELPER
    private string ConvertToServiceIdList(string ids)
    {
        StringBuilder result = new StringBuilder();
        string[] splitted = ids.Split(',');
        bool first = true;
        foreach (var item in splitted)
        {
            if (first)
                first = false;
            else
                result.Append(",");

            result.Append("'");
            result.Append(item);
            result.Append("'");
        }
        return result.ToString();
    }
    /// <summary>
    /// Return cim id's from dataset rows
    /// </summary>
    /// <param name="collection"></param>
    /// <returns></returns>
    private string GetCimekFromKuldKuldemenyek(DataRowCollection collection)
    {
        StringBuilder sb = new StringBuilder();
        bool first = true;
        foreach (DataRow item in collection)
        {
            if (string.IsNullOrEmpty(item["Cim_id"].ToString()))
                continue;

            if (first)
                first = false;
            else
                sb.Append(",");

            sb.Append("'");
            sb.Append(item["Cim_id"]);
            sb.Append("'");

        }
        return sb.ToString();
    }

    private string RagszamTagolas(string ragszam)
    {
        if (string.IsNullOrEmpty(ragszam))
            return string.Empty;

        string output = null;
        for (int i = 0; i < ragszam.Length; i++)
        {
            output += ragszam[i];
            if (i == 1 || i == 5 || i == 8 || i == 11 || i == 14)
                output += " ";
        }
        return output;
    }
    #endregion

    #region SERVICE
    private Result FindAllKuldKuldemenyek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        ExecParam execParam = UI.SetExecParamDefault(Page);
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        Result result = service.GetAll(execParam, search);
        return result;
    }
    private Result FindAllKrtCimek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        KRT_CimekSearch search = new KRT_CimekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        Contentum.eAdmin.Service.KRT_CimekService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CimekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAll(execParam, search);
        return result;
    }
    private string FindKrtParameter(string id)
    {
        Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam param = UI.SetExecParamDefault(Page, new ExecParam());
        param.Record_Id = id;
        Result result = service_parameterek.Get(param);
        if (result == null || result.ErrorCode != null)
            throw new Exception("Hi�nyz� rendszerparam�ter!");
        KRT_Parameterek ret = (KRT_Parameterek)result.Record;
        return ret.Ertek;
    }
    #endregion
}
