using Contentum.eBusinessDocuments; 
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using Microsoft.Reporting.WebForms;
using System.Configuration;

public partial class KimenoKuldemenyekAutoPrint : Contentum.eUtility.UI.PageBase
{
    private string docId = String.Empty;
    private string ids = String.Empty;
    private string cim = String.Empty;
    private string riport_path = String.Empty;

    private string iratIdsForServices = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;

    private string ConstFelado =
          HttpUtility.UrlEncode("Nemzeti M�dia- �s H�rk�zl�si Hat�s�g")
        + HttpUtility.UrlEncode(Environment.NewLine)
        + HttpUtility.UrlEncode("Budapest, Ostrom utca 23-25,")
        + HttpUtility.UrlEncode(Environment.NewLine)
        + "1015";
    //private string ConstKrtParameterNevFelado = "EREC_KIMENO_KULDEMENYEK_AUTOPRINT_FELADO";
    //private string ConstKrtParameterIdFelado = "B1023BF0-5555-6666-2222-0BE1338EB129";
    private string ConstPreview = "1";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KimenoKuldemenyekList");
        if (Request.QueryString["ids"] != null)
        {
            iratIdsForServices = ConvertToServiceIdList(Request.QueryString["ids"]);
            ids = Request.QueryString["ids"];

        }

        if (Request.QueryString["docid"] != null)
        {
            docId = Request.QueryString["docid"];

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            riport_path = ReportViewer1.ServerReport.ReportPath;
            switch (docId)
            {
                case "1":
                    riport_path = riport_path + "/TertivevenyKisBoritek";
                    break;
                case "2":
                    riport_path = riport_path + "/TertivevenyKozepesBoritek";
                    break;
                case "3":
                    riport_path = riport_path + "/TertivevenyNagyBoritek";
                    break;
                case "4":
                    riport_path = riport_path + "/Etikett";
                    break;
                case "5":
                    riport_path = riport_path + "/TertivevenyHivatalosIrathoz";
                    break;
                    //case "6":
                    //    riport_path = riport_path + "Feladoveveny";
                    //    break;
            }

            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + riport_path;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    private Result FindAllKuldKuldemenyek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        ExecParam execParam = UI.SetExecParamDefault(Page);
        executor = execParam.LoginUser_Id;
        szervezet = execParam.Org_Id;
        execParam.Fake = true;
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        Result result = service.GetAll(execParam, search);
        return result;
    }

    private Result FindAllKrtCimek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        KRT_CimekSearch search = new KRT_CimekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        Contentum.eAdmin.Service.KRT_CimekService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CimekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Fake = true;
        Result result = service.GetAll(execParam, search);
        return result;
    }
    private string FindKrtParameter(string id)
    {
        Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam param = UI.SetExecParamDefault(Page, new ExecParam());
        param.Fake = true;
        param.Record_Id = id;
        Result result = service_parameterek.Get(param);
        if (result == null || result.ErrorCode != null)
            throw new Exception("Hi�nyz� rendszerparam�ter!");
        KRT_Parameterek ret = (KRT_Parameterek)result.Record;
        return ret.Ertek;
    }

    private string GetPrintAppUri(string docid, string feladoNeve, string cimzett, string varos, string utca, string irsz, string preview, string ragszam, string ragszamTagolt, string iktatoszamok)
    {
        string ragszamPrefix = "H";
        string utcaMod = utca.Trim().Replace(" ", "_");
        string ragszamTagoltMod = ragszamTagolt.Trim().Replace(" ", "_");
        switch (docid)
        {
            case "3": //Felad�vev�ny
                if (cimzett.Length < 49)
                    return string.Format("axelprint://docid={0};preview={1};1={2};2={3};",
                        docid,
                        preview,
                        feladoNeve,
                        HttpUtility.UrlEncode(cimzett)
                        + HttpUtility.UrlEncode(Environment.NewLine)
                        + HttpUtility.UrlEncode(varos)
                        + HttpUtility.UrlEncode(", ")
                        + HttpUtility.UrlEncode(utcaMod)
                        + HttpUtility.UrlEncode(Environment.NewLine)
                        + irsz
                        );
                else
                    return string.Format("axelprint://docid={0};preview={1};1={2};2={3};",
                          docid,
                          preview,
                          feladoNeve,
                          HttpUtility.UrlEncode(cimzett)
                          + HttpUtility.UrlEncode(Environment.NewLine)
                          + HttpUtility.UrlEncode(varos)
                          + HttpUtility.UrlEncode(", ")
                          + HttpUtility.UrlEncode(utcaMod)
                          + HttpUtility.UrlEncode(" ")
                          + irsz
                          );
            case "6": //Bor�t�k
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     feladoNeve,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );

            case "171": //Bor�t�k kis szabv�nyos LC/6 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "172": //C�m bet�tlap LA/4 ablakos bor�t�kba 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "203": //Bor�t�k k�zepes LC/5 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "213": //Bor�t�k nagy LC/4 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     ragszam,
                     ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
            case "195": //T�RTIVEV�NY HIVATALOS IRATHOZ 
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6},6={7},7={8},8={9}",
                     docid,
                     preview,
                     string.IsNullOrEmpty(ragszam) ? null : ragszamPrefix + ragszam,
                     string.IsNullOrEmpty(ragszamTagoltMod) ? null : ragszamPrefix + ragszamTagoltMod,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz,
                     feladoNeve,
                     HttpUtility.UrlEncode(iktatoszamok)
                     );
            default:
                return string.Format("axelprint://docid={0};preview={1};1={2};2={3};3={4};4={5};5={6}",
                     docid,
                     preview,
                     feladoNeve,
                     HttpUtility.UrlEncode(cimzett),
                     HttpUtility.UrlEncode(varos),
                     HttpUtility.UrlEncode(utcaMod),
                     irsz
                     );
        }
    }

    private string GetCimekFromKuldKuldemenyek(DataRowCollection collection)
    {
        StringBuilder sb = new StringBuilder();
        bool first = true;
        foreach (DataRow item in collection)
        {
            if (string.IsNullOrEmpty(item["Cim_id"].ToString()))
                continue;

            if (first)
                first = false;
            else
                sb.Append(",");

            sb.Append("'");
            sb.Append(item["Cim_id"]);
            sb.Append("'");

        }
        return sb.ToString();
    }

    private string GetUtcaCim(DataRow cim)
    {
        // BLG_288
        //if (cim["Tipus"].ToString() == "02") return string.Format("Pf. {0}", cim["Hazszam"].ToString());
        if (cim["Tipus"].ToString() == "02")
        {
            if (String.IsNullOrEmpty(cim["Hazszam"].ToString())) { return String.Empty; }
            else return string.Format("Pf. {0}", cim["Hazszam"].ToString());
        }
        else if (string.IsNullOrEmpty(cim["KozteruletNev"].ToString()))
            return cim["CimTobbi"].ToString();
        else
            return string.Format("{0} {1} {2}{3}{4} {5} {6}{7}",
                                cim["KozteruletNev"].ToString(),
                                cim["KozteruletTipusNev"].ToString(),
                                cim["Hazszam"].ToString(),
                                cim["HazszamBetujel"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["HazszamBetujel"].ToString()),
                                cim["Lepcsohaz"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["Lepcsohaz"].ToString()),
                                cim["Szint"].ToString(),
                                cim["Ajto"].ToString(),
                                cim["AjtoBetujel"].ToString().Trim() == string.Empty ? string.Empty : (" " + cim["AjtoBetujel"].ToString())
                                );
    }

    private void SetUri(ref List<string> list, DataRow rat, DataRow cim)
    {
        if (cim != null)
            list.Add(
                GetPrintAppUri(
                Request.QueryString["docId"].ToString(),
                ConstFelado,
                rat["NevSTR_Bekuldo"].ToString(),
                cim["TelepulesNev"].ToString(),

                GetUtcaCim(cim),

                cim["IRSZ"].ToString(),
                ConstPreview,
                rat["Ragszam"].ToString(),
                RagszamTagolas(rat["Ragszam"].ToString()),
                rat["Azonosito"].ToString().Trim()
                ));
        else
            list.Add(
                GetPrintAppUri(
                 Request.QueryString["docId"].ToString(),
                 ConstFelado,
                 rat["NevSTR_Bekuldo"].ToString(),
                 rat["CimSTR_Bekuldo"].ToString(),
                 string.Empty,
                 string.Empty,
                 ConstPreview,
                 rat["Ragszam"].ToString(),
                 RagszamTagolas(rat["Ragszam"].ToString()),
                 rat["Azonosito"].ToString().Trim()
                 ));
    }

    private string RagszamTagolas(string ragszam)
    {
        if (string.IsNullOrEmpty(ragszam))
            return string.Empty;

        string output = null;
        for (int i = 0; i < ragszam.Length; i++)
        {
            output += ragszam[i];
            if (i == 1 || i == 5 || i == 8 || i == 11 || i == 14)
                output += " ";
        }
        return output;
    }

    private void OpenUris(List<string> uris)
    {
        if (uris.Count <= 0)
            return;

        //if (uris.Count == 1)
        //{
        //    if (Request.Browser.Browser.ToLower() == "chrome")
        //      //  OpenInChrome(uris[0]);
        //    else
        //     //   OpenInIE(uris[0]);
        //}
        //else
        //  //  OpenMultiple(uris);
    }

    private string ConvertToServiceIdList(string ids)
    {
        StringBuilder result = new StringBuilder();
        string[] splitted = ids.Split(',');
        bool first = true;
        foreach (var item in splitted)
        {
            if (first)
                first = false;
            else
                result.Append(",");

            result.Append("'");
            result.Append(item);
            result.Append("'");
        }
        return result.ToString();
    }




    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {


        ReportParameter[] ReportParameters = null;





        if (Request.QueryString["ids"] != null)
        {
            iratIdsForServices = ConvertToServiceIdList(Request.QueryString["ids"]);
            Result resultKuldemenyek = FindAllKuldKuldemenyek(iratIdsForServices);



            if (resultKuldemenyek != null && resultKuldemenyek.Ds != null && resultKuldemenyek.Ds.Tables != null && resultKuldemenyek.Ds.Tables.Count > 0 && resultKuldemenyek.Ds.Tables[0].Rows.Count > 0)
            {

                string cimIds = GetCimekFromKuldKuldemenyek(resultKuldemenyek.Ds.Tables[0].Rows);
                Result resultCimek = FindAllKrtCimek(cimIds);
                List<string> uris = new List<string>();
                foreach (DataRow iratItem in resultKuldemenyek.Ds.Tables[0].Rows)
                {
                    if (resultCimek != null && resultCimek.Ds != null && resultCimek.Ds.Tables != null && resultCimek.Ds.Tables.Count > 0 && resultCimek.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow[] cimekLista = resultCimek.Ds.Tables[0].Select("Id = '" + resultKuldemenyek.Ds.Tables[0].Rows[0]["Cim_id"] + "'");
                        if (cimekLista != null && cimekLista.Length > 0)
                            SetUri(ref uris, iratItem, cimekLista[0]);
                        else
                            SetUri(ref uris, iratItem, null);
                    }
                    else
                        SetUri(ref uris, iratItem, null);

                }
                if (uris != null && uris.Count > 0)
                {
                    //  OpenUris(uris);
                }



                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {

                        case "Where":
                                ReportParameters[i].Values.Add(String.Format("EREC_KuldKuldemenyek.Id in ({0})", iratIdsForServices));
                            break;


                        case "ExecutorUserId":

                            if (!string.IsNullOrEmpty(executor))
                            {
                                ReportParameters[i].Values.Add(executor);
                            }


                            break;




                        //case "Jogosultak":
                        //    ReportParameters[i].Values.Add(resultKuldemenyek.SqlCommand.GetParamValue("@Jogosultak"));
                        //    break;



                        case "FelhasznaloSzervezet_Id":

                            if (!string.IsNullOrEmpty(szervezet))
                            {
                                ReportParameters[i].Values.Add(szervezet);
                            }

                            break;
                        //case "pageNumber":
                        //    if (string.IsNullOrEmpty(resultKuldemenyek.SqlCommand.GetParamValue("@pageNumber")))
                        //    {
                        //        ReportParameters[i].Values.Add("0");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(resultKuldemenyek.SqlCommand.GetParamValue("@pageNumber"));
                        //    }
                        //    break;
                        //case "pageSize":
                        //    if (string.IsNullOrEmpty(resultKuldemenyek.SqlCommand.GetParamValue("@pageSize")))
                        //    {
                        //        ReportParameters[i].Values.Add("10000");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(resultKuldemenyek.SqlCommand.GetParamValue("@pageSize"));
                        //    }
                        //    break;
                        //case "SelectedRowId":
                        //    if (string.IsNullOrEmpty(resultKuldemenyek.SqlCommand.GetParamValue("@SelectedRowId")))
                        //    {
                        //        ReportParameters[i].Values.Add(" ");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(resultKuldemenyek.SqlCommand.GetParamValue("@SelectedRowId"));
                        //    }
                        //    break;




                        //case "cim":
                        //    if (!string.IsNullOrEmpty(cim))
                        //    {
                        //        ReportParameters[i].Values.Add(cim);
                        //    }

                        //    break;

                        case "ids":
                            if (!string.IsNullOrEmpty(ids))
                            {
                                ReportParameters[i].Values.Add(ids);
                            }

                            break;





                    }//switch
                }//for


            }
        }

        return ReportParameters;
    }

}
