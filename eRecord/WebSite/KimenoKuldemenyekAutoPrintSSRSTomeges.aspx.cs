﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;
using System.Text;

public partial class KimenoKuldemenyekAutoPrint : Contentum.eUtility.ReportPageBase
{
    private string ids = String.Empty;
    private string[] uid = { };
    private string riport_path = String.Empty;
    private string docId = String.Empty;
    private string iratIdsForServices = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["ids"] != null)
        {
            iratIdsForServices = ConvertToServiceIdList(Request.QueryString["ids"]);
            ids = Request.QueryString["ids"].ToString();
            uid = ids.Split(',');
        }

        if (Request.QueryString["docid"] != null)
        {
            docId = Request.QueryString["docid"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var riport = "";
            var fn = "";
            switch (docId)
            {
                case "1":
                    fn = "Tértivevény kis boríték";
                    riport = "/TertivevenyKisBoritekTomeges";
                    break;
                case "2":
                    fn = "Tértivevény közepes boríték";
                    riport = "/TertivevenyKozepesBoritekTomeges";
                    break;
                case "3":
                    fn = "Tértivevény nagy boríték";
                    riport = "/TertivevenyNagyBoritekTomeges";
                    break;
                case "4":
                    fn = "Etikett";
                    riport = "/EtikettTomeges";
                    break;
                case "5":
                    fn = "Tértivevény hivatalos irathoz";
                    riport = "/TertivevenyHivatalosIrathozTomeges";
                    break;
                //case "6":
                //    riport = "/FeladovevenyTomeges";
                //    break;
                default:
                    return;
            }

            ReportViewer1.ServerReport.ReportPath += riport;
            InitReport(ReportViewer1, false, "", "");

            //LZS - BUG_9561
            //Etikett az a régi módon megy
            if (docId != "4")
            {
                RenderFileToDownload("WORD", fn);
                //RenderFileToDownload("IMAGE", fn);
                //Print();
            }
        }
    }

    private Result FindAllKuldKuldemenyek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        ExecParam execParam = UI.SetExecParamDefault(Page);
        executor = execParam.LoginUser_Id;
        szervezet = execParam.Org_Id;
       
        execParam.Fake = true;
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        Result result = service.GetAll(execParam, search);
        return result;
    }

    private string ConvertToServiceIdList(string ids)
    {
        StringBuilder result = new StringBuilder();
        string[] splitted = ids.Split(',');
        bool first = true;
        foreach (var item in splitted)
        {
            if (first)
                first = false;
            else
                result.Append(",");

            result.Append("'");
            result.Append(item);
            result.Append("'");
        }
        return result.ToString();
    }

    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                Result resultKuldemenyek = FindAllKuldKuldemenyek(iratIdsForServices);
                if (resultKuldemenyek != null && resultKuldemenyek.Ds != null && resultKuldemenyek.Ds.Tables != null && resultKuldemenyek.Ds.Tables.Count > 0 && resultKuldemenyek.Ds.Tables[0].Rows.Count > 0)
                {
                    ReportParameters = new ReportParameter[rpis.Count];
                    for (int i = 0; i < rpis.Count; i++)
                    {
                        ReportParameters[i] = new ReportParameter(rpis[i].Name);
                        switch (rpis[i].Name)
                        {
                            case "ExecutorUserId":
                                ReportParameters[i].Values.Add(executor);
                                break;

                            case "FelhasznaloSzervezet_Id":
                                ReportParameters[i].Values.Add(szervezet);
                                break;

                            case "Where":
                                break;

                            case "ids":
                                ReportParameters[i].Values.AddRange(uid);
                                break;
                        }
                    }
                }
            }
        }
        return ReportParameters;
    }
}
