using Contentum.eRecord.Utility;
using System;
using System.Linq;

public partial class KimenoKuldemenyekAutoPrintSelector : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KimenoKuldemenyekList");
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        Core();
    }

   
    /// <summary>
    /// Core 
    /// </summary>
    private void Core()
    {
        if (Request.QueryString["ids"] == null)
            return;

        string ids = Request.QueryString["ids"];

        // BUG_7072
        int poz = Convert.ToInt32(DropDownList_Pozicio.SelectedValue);
        String zeroguidstr = new Guid("00000000-0000-0000-0000-000000000000").ToString()+',';
        string uresEtikett = new String('0', poz-1).Replace("0", zeroguidstr);
        ids = uresEtikett + ids;
        // BUG_7072
        // Minden esetben t�megeset h�v
        //if (Request.QueryString["Tomeges"] == null)
        //{

        //    HyperLinkN1.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=1&ids=" + ids + "')");
        //    HyperLinkN2.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=2&ids=" + ids + "')");
        //    HyperLinkN3.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=3&ids=" + ids + "')");
        //    HyperLinkN4.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=4&ids=" + ids + "')");
        //    HyperLinkN5.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=5&ids=" + ids + "')");
        //    HyperLinkN6.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=6&ids=" + ids + "')");
        //    HyperLinkN48.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRS.aspx?&docid=48&ids=" + ids + "')");
        //}

        //else
        //{
            HyperLinkN1.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=1&ids=" + ids + "')");
            HyperLinkN2.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=2&ids=" + ids + "')");
            HyperLinkN3.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=3&ids=" + ids + "')");
            HyperLinkN4.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=4&ids=" + ids + "')");
            HyperLinkN5.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=5&ids=" + ids + "')");
            HyperLinkN6.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=6&ids=" + ids + "')");
            HyperLinkN48.Attributes.Add("onclick", "window.open('KimenoKuldemenyekAutoPrintSSRSTomeges.aspx?&docid=48&ids=" + ids + "')");
        //}

    }
    private string GetClientOnClick(string docid, string ids)
    {
        return string.Format("window.open('KimenoKuldemenyekAutoPrint.aspx?" +
            "docId={0}" +
            "&ids={1}','WindowKimenoKuldemenyAxelPrint','width=550, height=200,toolbar=no, menubar=no');" +
            "return false;",
            docid,
            ids);
    }
    // BUG_7072
    protected void DropDownList_Pozicio_SelectedIndexChanged(object sender, EventArgs e)
    {
        Core();
    }
}
