﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="KimenoKuldemenyekList.aspx.cs" Inherits="KimenoKuldemenyekList" Title="Untitled Page" %>


<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />


    <%--HiddenFields--%>

    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="KuldemenyekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="KuldemenyekUpdatePanel" runat="server" OnLoad="KuldemenyekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="KuldemenyekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="KuldemenyekCPEButton" CollapseControlID="KuldemenyekCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="KuldemenyekCPEButton"
                            ExpandedSize="0" ExpandedText="Küldemények listája" CollapsedText="Küldemények listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponentKuld" />
                                        <asp:GridView ID="KuldemenyekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="KuldemenyekGridView_RowCommand" OnPreRender="KuldemenyekGridView_PreRender"
                                            OnSorting="KuldemenyekGridView_Sorting" OnRowDataBound="KuldemenyekGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="Csny.">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--BUG_9249--%>
                                                <asp:BoundField DataField="EREC_IraIktatoKonyvek_Nev" HeaderText="Postakönyv" SortExpression="EREC_IraIktatoKonyvek_Postakonyv.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Azonosito" HeaderText="<%$Forditas:headerKuldemenyAzonosito|Küldemény azonosító%>" SortExpression="EREC_KuldKuldemenyek.Azonosito">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KuldesMod_Nev" HeaderText="Küldésmód" SortExpression="KRT_KodTarakKuldesMod.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cimzett_Nev_Partner" HeaderText="Címzett" SortExpression="EREC_KuldKuldemenyek.NevSTR_Bekuldo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cimzett_Cim_Partner" HeaderText="Címzett&nbsp;címe" SortExpression="EREC_KuldKuldemenyek.CimSTR_Bekuldo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BarCode" HeaderText="Vonalkód" SortExpression="EREC_KuldKuldemenyek.BarCode">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--BLG_237--%>
                                                <asp:TemplateField HeaderText="Postai azonosító" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <%-- iktatott állapot esetén link az iratra --%>
                                                        <asp:Label ID="labelRagszam" runat="server" Text='<%#Eval("Ragszam").ToString().Length>20?String.Concat(Eval("Ragszam").ToString().Substring(0,20),"..."):Eval("Ragszam").ToString() %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:BoundField DataField="RagSzam" HeaderText="Postai azonosító" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle"/>
                                                </asp:BoundField>--%>
                                                <asp:BoundField DataField="BelyegzoDatuma_rovid" HeaderText="Feladás&nbsp;ideje" ItemStyle-HorizontalAlign="Center"
                                                    SortExpression="EREC_KuldKuldemenyek.BelyegzoDatuma">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ar" HeaderText="Ár" ItemStyle-HorizontalAlign="Center"
                                                    SortExpression="EREC_KuldKuldemenyek.Ar">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PeldanySzam" HeaderText="Mennyiség" Visible="false" ItemStyle-HorizontalAlign="Center" SortExpression="EREC_KuldKuldemenyek.PeldanySzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="40px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Note" HeaderText="Megjegyzés" SortExpression="EREC_KuldKuldemenyek.Note">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--                        <asp:BoundField DataField="AllapotNev" HeaderText="Állapot" SortExpression="KRT_KodTarakAllapot.Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Állapot" SortExpression="KRT_KodTarakAllapot.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%# string.Concat(Eval("AllapotNev"),                                          
	                              (Eval("Allapot") as string) == "03" ?
			                            "<br/>(" + (Eval("TovabbitasAlattAllapotNev") as string) + ")" : "")  %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye"
                                                    SortExpression="KRT_CsoportokOrzo.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_ExpedialNev" HeaderText="Expediáló"
                                                    SortExpression="KRT_CsoportokExpedialo.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ExpedialasIdeje_rovid" HeaderText="Expediálás időpontja"
                                                    SortExpression="EREC_KuldKuldemenyek.ExpedialasIdeje">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Csoport_Id_KikuldoNev" HeaderText="Kiküldő" SortExpression="Csoportok_KikuldoNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--BUG_6232--%>
                                                <asp:BoundField DataField="iratpeldanyok" HeaderText="Iratpéldányok">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Zárolás"
                                                    ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="150px" HeaderText="Nyomtatás ⎙">
                                                    <ItemTemplate>
                                                        <asp:Panel runat="server"
                                                            ToolTip="Expediált iratpéldány nyomtatása"
                                                            Visible='<%# Eval("Allapot").ToString() ==  Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Expedialt.ToString() %>'>
                                                            <asp:LinkButton ID="LinkButton2" Text="Nyomtat" runat="server"
                                                                OnClientClick='<%# String.Format("return popupAxelPrintSelector(\"{0}\")", Eval("Id")) %>' />
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TertiKezbesitesEredmenye" HeaderText="Kézb. eredménye" Visible="false"
                                                     SortExpression="EREC_KuldTertivevenyek.TertivisszaKod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAtvevoSzemely" HeaderText="Átvevő személy" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.AtvevoSzemely">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField> 
                                                <asp:BoundField DataField="TertiAtvetelIdopontja" HeaderText="Átvétel időpontja" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.AtvetelDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAtvetelJogcime" HeaderText="Átvétel jogcíme" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.Note">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAllapot" HeaderText="Tertiv. állapot" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.Allapot">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiVisszaerkezesIdeje" HeaderText="Visszaérk. ideje" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.TertiVisszaDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertivevenyVonalkod" HeaderText="Tértivevény vonalkód" Visible="false" SortExpression="EREC_KuldTertivevenyek.BarCode">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2"></td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">

                                    <ajaxToolkit:TabPanel ID="IratPeldanyokTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelIratPeldanyok" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerIratPeldanyok" runat="server" Text="Iratpéldányok"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="IratPeldanyokUpdatePanel" runat="server" OnLoad="IratPeldanyokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="IratPeldanyokPanel" runat="server" Visible="true" Width="100%">
                                                        <uc1:SubListHeader ID="IratPeldanyokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="IratPeldanyokCPE" runat="server" TargetControlID="panelIratPeldanyokList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelIratPeldanyokList" runat="server">
                                                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponentPld" />
                                                                        <asp:GridView ID="IratPeldanyokGridView" runat="server" AutoGenerateColumns="False"
                                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                            OnPreRender="IratPeldanyokGridView_PreRender" OnRowCommand="IratPeldanyokGridView_RowCommand"
                                                                            DataKeyNames="Id" OnSorting="IratPeldanyokGridView_Sorting" OnRowDataBound="IratPeldanyokGridView_RowDataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <div class="DisableWrap">
                                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                        </div>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                                    HeaderStyle-Width="25px" HeaderText="Csny.">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="BarCode" HeaderText="Vonalkód" SortExpression="BarCode">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok_Targy">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Határidő" SortExpression="VisszaerkezesiHatarido">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="NevSTR_Cimzett">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="KuldesMod_Nev" HeaderText="Küldésmód" SortExpression="KuldesMod_Nev">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Ugyintez_Nev" HeaderText="Ügyintéző" SortExpression="EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez_Nev">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Példány helye"
                                                                                    SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <asp:HiddenField ID="clickOrderInGrid" runat="server" />

    <%--<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>--%>
    <script type="text/javascript">
        function popupAxelPrintSelector(id) {
            newname = "WindowKimenoKuldemenyekAutoPrintSelector";
            settings = "scrollbars=yes,resizable=yes, width=550, height=400,toolbar=no, menubar=no";
            window.open("KimenoKuldemenyekAutoPrintSelector.aspx?ids=" + id, newname, settings);
        }

        var elemsClick = [];

        function getSelectedInClickOrder() {
            var strIds = new String();
            for (i = 0; i < elemsClick.length; i++) {
                if (i == 0) {
                    strIds = elemsClick[i];
                }
                else {
                    strIds = strIds + "," + elemsClick[i];
                }
            }
            return strIds;
        }

        function CheckCheckboxes() {
            try {

                var kuldemenyGridView = $('#ctl00_ContentPlaceHolder1_KuldemenyekGridView');
                var checkBoxID = 'check';
                var idPrefix = 'ctl00_ContentPlaceHolder1_KuldemenyekGridView';

                var count = 0;
                var prefixLength = idPrefix.length;

                var x = document.getElementsByTagName("input");

                var i = 0;
                for (i = 0; i < x.length; i++) {
                    if (x[i].type == "checkbox"
                        && x[i].id.substr(0, prefixLength) == idPrefix && x[i].id.indexOf(checkBoxID) != -1
                    ) {
                        var gui = x[i].nextSibling.firstChild.nodeValue;
                        if (elemsClick.indexOf(gui) != -1) {
                            if (x[i].checked == false) {
                                for (var i = 0; i < elemsClick.length; i++) {
                                    if (elemsClick[i] === gui) {
                                        elemsClick.splice(i, 1);
                                    }
                                }
                            }
                        }
                        else {
                            if (x[i].checked == true) {
                                elemsClick.push(gui);
                            }
                        }

                        $('#' + x[i].id).on('change', function () {

                            var checkId = $(event.target).attr('id');
                            var hField = $('#ctl00_ContentPlaceHolder1_clickOrderInGrid');
                            var currentCheckBox = document.getElementById(checkId);
                            var guid = currentCheckBox.nextSibling.firstChild.nodeValue;

                            if (elemsClick.indexOf(guid) == -1) {
                                if (currentCheckBox.checked == true) {
                                    elemsClick.push(guid);
                                }
                            }
                            else {
                                if (currentCheckBox.checked == false) {
                                    for (var i = 0; i < elemsClick.length; i++) {
                                        if (elemsClick[i] === guid) {
                                            elemsClick.splice(i, 1);
                                        }
                                    }
                                }
                            }

                            hField.val(JSON.stringify(elemsClick));
                        });
                    }
                }
                var hField = $('#ctl00_ContentPlaceHolder1_clickOrderInGrid');
                hField.val(JSON.stringify(elemsClick));
            } catch (e) {
                //console.log(e);
            }
        }


        Sys.Application.add_load(function () {
            CheckCheckboxes();
        });

        //$(document).ready(function () {



        //});

    </script>
</asp:Content>
