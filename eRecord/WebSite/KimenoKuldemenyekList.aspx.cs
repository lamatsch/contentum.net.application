﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Text;
using Newtonsoft.Json;

public partial class KimenoKuldemenyekList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private string vis = String.Empty;

    private String Startup = "";
    private String Mode = "";
    private string POSTAI_RAGSZAM_KIOSZTAS_SORREND = "A";

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KimenoKuldemenyekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        if (Session["KimenoKuldemenyekListStartup"] == null)
        {
            if (Startup == Constants.Startup.SearchForm || Startup == Constants.Startup.KimenoKuldemenyFelvitel)
            {
                Session["KimenoKuldemenyekListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
        }

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        IratPeldanyokSubListHeader.RowCount_Changed += new EventHandler(IratPeldanyokSubListHeader_RowCount_Changed);

        IratPeldanyokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IratPeldanyokSubListHeader_ErvenyessegFilter_Changed);

        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        POSTAI_RAGSZAM_KIOSZTAS_SORREND = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "POSTAI_RAGSZAM_KIOSZTAS_SORREND");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.KimenoKuldemenyekLisHeaderTitle;
        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch;
        ListHeader1.SearchObjectType = typeof(EREC_KuldKuldemenyekSearch);

        #region Ikonok láthatósága

        // ListHeader1.ModifyVisible = false;
        ListHeader1.ModifyVisible = true;
        ListHeader1.InvalidateVisible = false;

        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        //bernat.laszlo eddig

        #region CR3155 - CSV export
        ListHeader1.CSVExportVisible = true;
        ListHeader1.CSVExportOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"; ;
        #endregion

        // A baloldali, alapból invisible ikonok megjelenítése:
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.SSRSPrintFutarVisible = true;


        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.AtadasraKijelolVisible = true;
        ListHeader1.AtvetelVisible = true;
        ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;

        ListHeader1.PostazasVisible = true;
        ListHeader1.PostazasTomegesVisible = true;

        ListHeader1.SztornoVisible = true;

        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        #endregion

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KimenoKuldemenyekSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx", QueryStringVars.Command + "=" + CommandName.KimenoKuldemenyFelvitel
            , Defaults.PopupWidth, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KuldemenyekGridView.ClientID);

        //ListHeader1.PrintOnClientClick = "javascript:window.open('KimenoKuldemenyekSSRS.aspx?" + QueryStringVars.IktatokonyvId + "=" + vis + "')";
        //ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //1019   ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KimenoKuldemenyekPrintForm.aspx");


        if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
        {
            ListHeader1.PrintOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                + "javascript:window.open('KimenoKuldemenyekAutoPrintSelector.aspx?Tomeges=true&ids=" + "'+getSelectedInClickOrder()" + ")";
        }
        else
        {
            ListHeader1.PrintOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                + "javascript:window.open('KimenoKuldemenyekAutoPrintSelector.aspx?Tomeges=true&ids=" + "'+getIdsBySelectedCheckboxes('" + KuldemenyekGridView.ClientID + "','check')" + ")";
        }        

        ListHeader1.SSRSPrintFutarOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                + "javascript:window.open('IratokSSSRSFutarjegyzek.aspx?ids=" + "'+getIdsBySelectedCheckboxes('" + KuldemenyekGridView.ClientID + "','check')" + ")";

        //BLG 1880 AUTO_RAGSZAMOSZTAS_TOMEGESEN = 1 esetén
        ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
        ListHeader1.TomegesRagszamGeneralasVisible = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.AUTO_RAGSZAMOSZTAS_TOMEGESEN).Trim().Equals("1");

        string column_v = "";

        for (int i = 0; i < KuldemenyekGridView.Columns.Count; i++)
        {
            if (KuldemenyekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";

            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('KimenoKuldemenyekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        //  ListHeader1.SSRSPrintFutarOnClientClick = "javascript:window.open('KimenoKuldemenyekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(KuldemenyekGridView.ClientID);

        ListHeader1.PostazasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(KuldemenyekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(KuldemenyekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(KuldemenyekGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, "", true);

        //BLG 1880 tögemes ragszám generálás
        ListHeader1.TomegesRagszamGeneralasOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + KuldemenyekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = KuldemenyekGridView;

        IratPeldanyokSubListHeader.NewEnabled = false;
        IratPeldanyokSubListHeader.ModifyEnabled = false;
        IratPeldanyokSubListHeader.InvalidateEnabled = false;

        IratPeldanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IratPeldanyokSubListHeader.ButtonsClick += new CommandEventHandler(IratPeldanyokSubListHeader_ButtonsClick);


        //selectedRecordId kezelése
        IratPeldanyokSubListHeader.AttachedGridView = IratPeldanyokGridView;
        /**/
        
        ui.SetClientScriptToGridViewSelectDeSelectButton(IratPeldanyokGridView);

        /* Breaked Records */
        //Search.SetIdsToSearchObject(Page, "Id", new EREC_KuldKuldemenyekSearch());
        Search.SetIdsToSearchObject(Page, "Id", new EREC_KuldKuldemenyekSearch(true), Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch);

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Startup == "EFeladoJegyzek")
        {
            if (!IsPostBack)
            {
                // Ha popup képernyőt is kell nyitni, nem töltjük fel a listát:
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("PostazasEFeladoJegyzek.aspx", QueryStringVars.Command + "=" + CommandName.New
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "EFeladoJegyzek", script, true);

                popupNyitas = true;
            }
        }
        else if (Startup == "PostazasTomeges")
        {
            if (!IsPostBack)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";

                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("PostazasForm.aspx",
                    QueryStringVars.Command + "=" + CommandName.Postazas
                    + "&" + QueryStringVars.Startup + "=PostazasTomeges"
                    + "&" + QueryStringVars.Mode + "=" + Mode
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "PostazasTomeges", script, true);

                popupNyitas = true;
            }
        }
        else if (Session["KimenoKuldemenyekListStartup"] != null)
        {
            Startup = Session["KimenoKuldemenyekListStartup"].ToString();
            if (Startup == Constants.Startup.SearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";

                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("KimenoKuldemenyekSearch.aspx", String.Empty
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "KimenoKuldemenyekSearch", script, true);


                Session.Remove("KimenoKuldemenyekListStartup");

                popupNyitas = true;
            }
            else if (Startup == Constants.Startup.KimenoKuldemenyFelvitel)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";

                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("PostazasForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.KimenoKuldemenyFelvitel
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "KimenoKuldemenyFelvitel", script, true);


                Session.Remove("KimenoKuldemenyekListStartup");

                popupNyitas = true;
            }
        }

        // Ha iktató vagy kereső képernyőt is kell nyitni, nem töltjük fel a listát:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            KuldemenyekGridViewBind();
        }

        SetClientScriptToGridViewSelectDeSelectButton(KuldemenyekGridView);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        // fv. beregisztrálása:
        string js_refreshFunction = " function refreshMasterList() { " + ListHeader1.RefreshOnClientClick + " }";
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "refreshFunction", js_refreshFunction, true);

        // BUG_10701
        ScriptManager.RegisterStartupScript(Page, this.GetType(), "clearElemsClick", "if (elemsClick) { elemsClick = []; }", true);
    }

    public void SetClientScriptToGridViewSelectDeSelectButton(GridView ParentGridView)
    {
        if (ParentGridView == null) return;
        if (ParentGridView.Rows.Count > 0)
        {
            //(ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

            //(ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

            ImageButton sib = (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton);
            if (sib != null)
                sib.Attributes["onClick"] = "SelectAllCheckBox('" + ParentGridView.ClientID + "','check');CheckCheckboxes();return false";
            else
            {
                Logger.Error("Sorkiválasztó gomb nem talalhato !!!");
                //throw new Exception("Sorkiválasztó gomb nem talalhato !!!");
            }

            ImageButton desib = (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton);
            if (desib != null)
                desib.Attributes["onClick"] = "DeSelectAllCheckBox('" + ParentGridView.ClientID + "','check');CheckCheckboxes();return false;";
            else
            {
                Logger.Error("Sorkiválasztást törlõ gomb nem talalhato !!!");
                // throw new Exception("Sorkiválasztást törlõ gomb nem talalhato !!!");
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Postazas"); // Kimenő küldemény felvitel (uaz szinte, mint a postázás)
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyView");
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Postazas");
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyViewHistory");

        //bernat.laszlo added
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "KimenoKuldemeny" + CommandName.ExcelExport);
        //bernat.laszlo eddig

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");
        ListHeader1.SSRSPrintFutarEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.PostazasEnabled = FunctionRights.GetFunkcioJog(Page, "Postazas");
        ListHeader1.SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "KimenoKuldemenySztorno");

        ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyAtadas");
        ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyAtvetel");
        ListHeader1.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyAtvetel"); // egyelőre az átvétel jogához kötjük

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyLock");
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyLock");

        //IratPeldanyokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_IktatokonyvList");

        IratPeldanyokSubListHeader.NewEnabled = false;
        IratPeldanyokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyView");
        IratPeldanyokSubListHeader.ModifyEnabled = false;
        IratPeldanyokSubListHeader.InvalidateEnabled = false;

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(KuldemenyekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        string column_v = "";
        for (int i = 0; i < KuldemenyekGridView.Columns.Count; i++)
        {
            if (KuldemenyekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('KimenoKuldemenyekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
    }


    #endregion

    #region Master List

    protected void KuldemenyekGridViewBind()
    {
        // BLG_8117

        String sortExpression = "";

        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            sortExpression = Search.GetSortExpressionFromViewState("KuldemenyekGridView", ViewState, "EREC_KuldKuldemenyek.LetrehozasIdo");
        }
        else
        {
            sortExpression = Search.GetSortExpressionFromViewState("KuldemenyekGridView", ViewState, "EREC_IraIktatoKonyvek.Nev DESC, EREC_IraIktatoKonyvek.Ev DESC, EREC_KuldKuldemenyek.Erkezteto_Szam ");
        }

        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KuldemenyekGridView", ViewState, SortDirection.Descending);
        KuldemenyekGridViewBind(sortExpression, sortDirection);
    }

    protected void KuldemenyekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponentKuld.ClearDokumentElements();

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldKuldemenyekSearch search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject_CustomSessionName(
            Page, new EREC_KuldKuldemenyekSearch(true), Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch);

        // Azokat kell hozni, ahol Postazas_Iranya == Kimeno

        search.Manual_PostazasIranya_DefaultFilter.Value = KodTarak.POSTAZAS_IRANYA.Kimeno;
        search.Manual_PostazasIranya_DefaultFilter.Operator = Query.Operators.equals;

        search.OrderBy = Search.GetOrderBy("KuldemenyekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(execParam, ListHeader1);

        Result res = service.KimenoKuldGetAllWithExtensionAndJogosultak(execParam, search, true);

        UI.GridViewFill(KuldemenyekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void KuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetCsatolmanyInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetCsatolmanyInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        #region BLG_577
                        if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        {   /*NINCS JOGA*/
                            CsatolmanyImage.Visible = false;
                            return;
                        }
                        #endregion

                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            DokumentumVizualizerComponentKuld.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                        else
                        {
                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            if (!String.IsNullOrEmpty(id))
                            {
                                // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    protected void KuldemenyekGridView_PreRender(object sender, EventArgs e)
    {
        //int prev_PageIndex = KuldemenyekGridView.PageIndex;

        //KuldemenyekGridView.PageIndex = ListHeader1.PageIndex;
        //ListHeader1.PageCount = KuldemenyekGridView.PageCount;

        //if (prev_PageIndex != KuldemenyekGridView.PageIndex)
        //{
        //scroll állapotának törlése
        //JavaScripts.ResetScroll(Page, KuldemenyekCPE);
        //KuldemenyekGridViewBind();
        //ActiveTabClear();
        //}
        //else
        //{
        UI.GridViewSetScrollable(ListHeader1.Scrollable, KuldemenyekCPE);
        //}

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(KuldemenyekGridView);

        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KuldemenyekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, KuldemenyekCPE);
        KuldemenyekGridViewBind();
    }

    protected void KuldemenyekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KuldemenyekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //if (Header.Text.IndexOf(" (") > 0)
            //    Header.Text = Header.Text.Remove(Header.Text.IndexOf(" ("));
            //if (headerIratPeldanyok.Text.IndexOf(" (") > 0)
            //    headerIratPeldanyok.Text = headerIratPeldanyok.Text.Remove(headerIratPeldanyok.Text.IndexOf(" ("));
            //if (Label1.Text.IndexOf(" (") > 0)
            //    Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            ActiveTabRefresh(TabContainer1, id);
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID);

            string tableName = "EREC_KuldKuldemenyek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, KuldemenyekUpdatePanel.ClientID);

            ExecParam execParam = UI.SetExecParamDefault(Page);
            ErrorDetails errorDetail = null;
            Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(id, execParam, EErrorPanel1);

            if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Postazott)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.KuldemenyId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_KuldemenyMegtekintes + "')) {" +
                        JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldemenyekUpdatePanel.ClientID)
                    + "} else return false;";
            }

            if (Kuldemenyek.Postazhato(statusz, execParam, out errorDetail))
            {
                ListHeader1.PostazasOnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx",
                    QueryStringVars.KuldemenyId + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.PostazasOnClientClick = "alert('" + Resources.Error.ErrorCode_52535
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }


            if (Kuldemenyek.KimenoKuldemenySztornozhato(statusz, out errorDetail))
            {
                ListHeader1.SztornoOnClientClick = "if (confirm('"
                    + Resources.Question.UIConfirmHeader_KimenoKuldSztorno
                    + "')) { } else { return false; }";
            }
            else
            {
                // Nem sztornózható:
                ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.ErrorCode_54201
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // az esetleg felesleges hívások elkerülése miatt nem végezzük el a teljes ellenőrzést
            //if (Kuldemenyek.CheckVisszakuldhetoWithFunctionRight(Page, statusz, out errorDetail))
            if (Kuldemenyek.Visszakuldheto(execParam, statusz, out errorDetail))
            {
                VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
            }
            else
            {
                // Nem küldhető vissza:
                ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            //ListHeader1.PrintOnClientClick = "javascript:window.open('KuldemenyekPrintForm.aspx?" + QueryStringVars.IktatokonyvId + "=" + id + "')";

        }
    }

    protected void KuldemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KuldemenyekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedKuldemenyek();
        //    KuldemenyekGridViewBind();
        //}
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, KuldemenyekGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
        //bernat.laszlo eddig
        #region CSV export
        if (e.CommandName == CommandName.CSVExport)
        {
            Contentum.eUtility.CSVExport csvExport = new Contentum.eUtility.CSVExport(true);
            //csvExport.ExportToCSVFromDb(UI.SetExecParamDefault(Page), "select * from EREC_KuldKuldemenyek", "EREC_KuldKuldemenyek");
            csvExport.ExportToCSVFromDataGridViewWithProcedure(UI.SetExecParamDefault(Page), KuldemenyekGridView, System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AuditLogConnectionString"].ConnectionString, Constants.CSVParams.sp_CSVExport_Postazas, Constants.CSVParams.EREC_KuldKuldemenyekTable);
        }
        #endregion
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String RecordId = UI.GetGridViewSelectedRecordId(KuldemenyekGridView);

        switch (e.CommandName)
        {
            case CommandName.Sztorno:
                KimenoKuldemenySztornozas(RecordId);
                KuldemenyekGridViewBind();
                break;
            case CommandName.AtadasraKijeloles:
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (string s in ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session["SelectedKuldemenyIds"] = sb.ToString();

                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedIratPeldanyIds"] = null;
                    Session["SelectedDosszieIds"] = null;

                    string js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                }
                break;
            case CommandName.Atvetel:
                {
                    bool mindigTomegesAtvetel = Rendszerparameterek.GetBoolean(Page, "TOMEGES_ATVETEL_ENABLED", false);

                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(KuldemenyekGridView, "check") == 1
                        && !String.IsNullOrEmpty(RecordId)
                        && lstGridViewSelectedRows.Contains(RecordId) && !mindigTomegesAtvetel)
                    {
                        Kuldemenyek.Atvetel(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
                        KuldemenyekGridViewBind();
                    }
                    else
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session["SelectedKuldemenyIds"] = sb.ToString();

                        Session["SelectedBarcodeIds"] = null;
                        Session["SelectedUgyiratIds"] = null;
                        Session["SelectedIratPeldanyIds"] = null;
                        Session["SelectedDosszieIds"] = null;
                        //ne okozzon gondot, ha korábban hívtuk a tük visszavétel funkciót
                        Session["TUKVisszavetel"] = null;

                        string js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                    }
                }
                break;
            case CommandName.Visszakuldes:
                {
                    ErrorDetails errorDetail;
                    bool visszakuldheto = Kuldemenyek.CheckVisszakuldhetoWithFunctionRight(Page, RecordId, out errorDetail);

                    if (!visszakuldheto)
                    {
                        string js = String.Format("alert('{0}{1}');", Resources.Error.UINemVisszakuldhetoTetel, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemKuldhetoVissza", js, true);
                    }
                    else
                    {
                        Kuldemenyek.Visszakuldes(RecordId, VisszakuldesPopup.VisszakuldesIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                        KuldemenyekGridViewBind();
                    }
                }
                break;
            case CommandName.Lock:
                LockSelectedIraIktatoKonyvRecords();
                KuldemenyekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraIktatoKonyvRecords();
                KuldemenyekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedKuldemenyek();
                break;
            case CommandName.TomegesRagszamGeneralas:
                #region BLG1880 tömeges ragszám generálás                

                System.Text.StringBuilder sbIds = new System.Text.StringBuilder();
                //első körben a kimenő küldemények meghatározása a gridview alapján
                foreach (string s in ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sbIds.Append(s + ",");
                }
                sbIds.Remove(sbIds.Length - 1, 1);
                Session["SelectedKuldemenyIds"] = sbIds.ToString();
                Session["Tomeges"] = "1";
                Session["RagszamSource"] = "KimenoKuldemenyekList";

                if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
                {
                    string[] clickOrder = JsonConvert.DeserializeObject<string[]>(clickOrderInGrid.Value);
                    if (clickOrder != null)
                    {
                        List<string> keys = new List<string>();
                        foreach (DataKey key in KuldemenyekGridView.DataKeys)
                        {
                            keys.Add(key.Value.ToString());
                        }
                        List<string> toRemove = new List<string>();
                        for (int i = 0; i < clickOrder.Length; i++)
                        {
                            if (!keys.Contains(clickOrder[i]))
                            {
                                toRemove.Add(clickOrder[i]);
                            }
                        }

                        var clone = clickOrder.ToList();

                        foreach (string key in toRemove)
                        {
                            clone.Remove(key);
                        }

                        clickOrder = clone.ToArray();
                        clickOrderInGrid.Value = JsonConvert.SerializeObject(clickOrder);

                        Session["SelectedKuldemenyIdsClickOrder"] = clickOrder;
                    }
                    else
                    {
                        Session["SelectedKuldemenyIdsClickOrder"] = null;
                    }

                }

                //js = JavaScripts.SetOnClientClickWithTimeout("ExpedialasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                string jsRagszam = JavaScripts.SetOnClientClickWithTimeout("RagszamOsztasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyRagszamOsztas", jsRagszam, true);

                #endregion
                break;
            case CommandName.Postazas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (lstGridViewSelectedRows == null || lstGridViewSelectedRows.Count < 1)
                    {
                        string errorJs = String.Format("alert('{0}{1}');", Resources.Error.DefaultErrorHeader, Resources.Error.MessageNoSelectedItem);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PostazasTomegesHiba", errorJs, true);

                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.MessageNoSelectedItem);
                        return;
                    }

                    string js = string.Empty;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    bool first = true;
                    foreach (string id in lstGridViewSelectedRows)
                    {
                        if (first)
                        {
                            first = false;
                            sb.Append(string.Format("'{0}'", id));
                        }
                        else
                        {
                            sb.Append(string.Format(",'{0}'", id));
                        }
                    }
                    Session["SelectedKuldemenyIds"] = sb.ToString();
                    // Ki kell üríteni a "SelectedIratPldIds" Session-változót, különben az jut érvényre a tömeges postázás formon:
                    Session["SelectedIratPldIds"] = null;

                    js = JavaScripts.SetOnClientClickWithTimeout("PostazasTomegesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectePostazasTomegesForm", js, true);
                }

                break;
        }
    }

    private void KimenoKuldemenySztornozas(string kuldemenyId)
    {
        if (!String.IsNullOrEmpty(kuldemenyId))
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

            ExecParam execParam = UI.SetExecParamDefault(Page);
            execParam.Record_Id = kuldemenyId;

            Result result = service.KimenoKuldemenySztorno(execParam);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
    }

    private void LockSelectedIraIktatoKonyvRecords()
    {
        LockManager.LockSelectedGridViewRecords(KuldemenyekGridView, "EREC_KuldKuldemenyek"
            , "KuldemenyLock", "KuldemenyForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraIktatoKonyvRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(KuldemenyekGridView, "EREC_KuldKuldemenyek"
            , "KuldemenyLock", "KuldemenyForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }



    /// <summary>
    /// Elkuldi emailben a KuldemenyekGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedKuldemenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel),
                UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_KuldKuldemenyek");
        }
    }

    protected void KuldemenyekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KuldemenyekGridViewBind(e.SortExpression, UI.GetSortToGridView("KuldemenyekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (KuldemenyekGridView.SelectedIndex == -1)
        {
            return;
        }
        string kuldemenyId = UI.GetGridViewSelectedRecordId(KuldemenyekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, kuldemenyId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string kuldemenyId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                IratPeldanyokGridViewBind(kuldemenyId);
                IratPeldanyokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(IratPeldanyokGridView);
                break;
        }
    }

    protected void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        IratPeldanyokSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(IratPeldanyokTabPanel))
        {
            IratPeldanyokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IratPeldanyokGridView));
        }
    }

    #endregion



    #region IratPeldanyok Detail

    protected void IratPeldanyokGridViewBind(string kuldemenyId)
    {
        string sortExpression = Search.GetSortExpressionFromViewState("IratPeldanyokGridView", ViewState, "EREC_PldIratPeldanyok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IratPeldanyokGridView", ViewState, SortDirection.Descending);

        IratPeldanyokGridViewBind(kuldemenyId, sortExpression, sortDirection);

    }

    protected void IratPeldanyokGridViewBind(string kuldemenyId, string SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponentPld.ClearDokumentElements();

        if (!String.IsNullOrEmpty(kuldemenyId))
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(true);

            search.OrderBy = Search.GetOrderBy("IratPeldanyokGridView", ViewState, SortExpression, SortDirection);

            IratPeldanyokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtensionByKimenoKuldemeny(execParam, search, kuldemenyId);

            UI.GridViewFill(IratPeldanyokGridView, res, IratPeldanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerIratPeldanyok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(IratPeldanyokGridView);
        }
        else
        {
            ui.GridViewClear(IratPeldanyokGridView);
        }

    }

    //SubListHeader függvényei(4)
    private void IratPeldanyokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedIratPeldanyokFromIktatokonyv();
        //    IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
        //}
    }


    private void IratPeldanyokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
    }

    private void IratPeldanyokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(IratPeldanyokSubListHeader.RowCount);
        IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
    }


    protected void IratPeldanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IratPeldanyokGridView, selectedRowNumber, "check");
        }
    }

    protected void IratPeldanyokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IratPeldanyokGridView.PageIndex;

        IratPeldanyokGridView.PageIndex = IratPeldanyokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != IratPeldanyokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, IratPeldanyokCPE);
            IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(IratPeldanyokSubListHeader.Scrollable, IratPeldanyokCPE);
        }
        IratPeldanyokSubListHeader.PageCount = IratPeldanyokGridView.PageCount;
        IratPeldanyokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IratPeldanyokGridView);


        ui.SetClientScriptToGridViewSelectDeSelectButton(IratPeldanyokGridView);
    }


    protected void IratPeldanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView)
            , e.SortExpression, UI.GetSortToGridView("IratPeldanyokGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel eseménykezelői(1)
    protected void IratPeldanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(IratPeldanyokTabPanel))
                    //{
                    //    IratPeldanyokGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekGridView));
                    //}
                    ActiveTabRefreshDetailList(IratPeldanyokTabPanel);
                    break;
            }
        }
    }


    protected void IratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetVegyesInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            DokumentumVizualizerComponentPld.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                        else
                        {
                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            if (!String.IsNullOrEmpty(id))
                            {
                                // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    //Gombok kliens oldali szkripjeinek frissítése(1)
    private void IratPeldanyokGridView_RefreshOnClientClicks(string kezbesitesiTetelId)
    {
        string id = kezbesitesiTetelId;
        if (!String.IsNullOrEmpty(id))
        {

            IratPeldanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IratPeldanyokUpdatePanel.ClientID);

        }
    }

    #endregion

    protected StringBuilder GetSelectedId()
    {


        System.Text.StringBuilder idk = new System.Text.StringBuilder();
        foreach (string s in ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
        {
            idk.Append(s + ",");

        }
        if (idk.Length != 0)
        {
            idk.Remove(idk.Length - 1, 1);
        }

        return idk;

    }
}
