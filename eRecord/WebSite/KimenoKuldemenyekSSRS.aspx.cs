﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class KimenoKuldemenyekSSRS : Contentum.eUtility.UI.PageBase
{
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        vis = Request.QueryString.Get(QueryStringVars.Filter);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_KuldKuldemenyekSearch search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new EREC_KuldKuldemenyekSearch(true), Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch);

                // Azokat kell hozni, ahol Postazas_Iranya == Kimeno

                search.Manual_PostazasIranya_DefaultFilter.Value = KodTarak.POSTAZAS_IRANYA.Kimeno;
                search.Manual_PostazasIranya_DefaultFilter.Operator = Query.Operators.equals;

                search.TopRow = 0;

                execParam.Fake = true;

                Result result = service.KimenoKuldGetAllWithExtensionAndJogosultak(execParam, search, true);
                
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_KuldKuldemenyek.LetrehozasIdo desc");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            //ReportParameters[i].Values.Add(result.SqlCommand.Parameters["@OrderBy"]);
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "Where_IratPeldany_Cimzett":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_IratPeldany_Cimzett"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "CsnyVisibility":
                            if (vis.Substring(2, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "KuldesModVisibility":
                            if (vis.Substring(3, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "CimzettVisibility":
                            if (vis.Substring(4, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "CimzettCimeVisibility":
                            if (vis.Substring(5, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "VonalkodVisibility":
                            if (vis.Substring(6, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "RagszamVisibility":
                            if (vis.Substring(7, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "TertivevenyVonalkodVisibility":
                            if (vis.Substring(8, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "FeladasIdejeVisibility":
                            if (vis.Substring(9, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "ArVisibility":
                            if (vis.Substring(10, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "MennyisegVisibility":
                            if (vis.Substring(11, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "MegjegyzesVisibility":
                            if (vis.Substring(12, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AllapotVisibility":
                            if (vis.Substring(13, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IratHelyeVisibility":
                            if (vis.Substring(14, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ExpedialoVisibility":
                            if (vis.Substring(15, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ExpedialasDatumaVisibility":
                            if (vis.Substring(16, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "KikuldoVisibility":
                            if (vis.Substring(17, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ZarolasVisibility":
                            if (vis.Substring(18, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
