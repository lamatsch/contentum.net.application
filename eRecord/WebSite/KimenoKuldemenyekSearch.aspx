﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KimenoKuldemenyekSearch.aspx.cs" Inherits="KimenoKuldemenyekSearch"
    Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
    <%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="csp" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/KimenoKuldemenyFajta_SearchFormControl.ascx" TagName="KimenoKuldemenyFajta_SearchFormControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc21" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style= "width:80%">
                                <uc11:EvIntervallum_SearchFormControl ID="IraIkt_Ev_EvIntervallum_SearchFormControl" runat="server" />
                            </td>
                        </tr>
                       
                     <tr class="urlapSor">                    
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="Iktatókönyv:"></asp:Label>
                            </td>                       
                            <td class="mrUrlapMezo" style="width: 3px">
                                <uc4:IraIktatoKonyvekDropDownList ID="UgyUgyir_IraIktatoKonyvekDropDownList" 
                                    runat="server" 
                                    EvIntervallum_SearchFormControlId="IraIkt_Ev_EvIntervallum_SearchFormControl" 
                                    Filter_IdeIktathat="false" IsMultiSearchMode="true" Mode="Iktatokonyvek" />
                            </td>
                        </tr>                    
                      
                    <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="Főszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" >
                                <uc6:SzamIntervallum_SearchFormControl ID="UgyIratDarab_Foszam_SzamIntervallum_SearchFormControl1" runat="server" />
                                </td>
                        </tr>
                    <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Alszám:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:SzamIntervallum_SearchFormControl ID="IraIrat_Alszam_SzamIntervallum_SearchFormControl2" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label29" runat="server" Text="Sorszám:"></asp:Label>
                            </td>                          
                            <td class="mrUrlapMezo">
                                <uc6:SzamIntervallum_SearchFormControl ID="Sorszam_SzamIntervallum_SearchFormControl3" runat="server" />
                            </td>
                        </tr>     
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Azonosító:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label12" runat="server" Text="Ügyintéző:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc21:FelhasznaloCsoportTextBox ID="fcstbUgyintezo" runat="server" SearchMode="true" Validate="false" />
                            </td>                      
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPostakonyv" runat="server" Text="Postakönyv:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList_Postakonyv" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Feladás dátuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="BelyegzoDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="False" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label22" runat="server" Text="Címzett:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc2:PartnerTextBox ID="Cimzett_PartnerTextBox" runat="server" SearchMode="True"
                                    Validate="False" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label23" runat="server" Text="Címzett címe:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CimekTextBox ID="CimzettCime_CimekTextBox" runat="server" SearchMode="True"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKikuldo" runat="server" Text="Kiküldő:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <csp:CsoportTextBox ID="Kikuldo_CsoportTextBox" SzervezetCsoport="true" Validate="false"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label28" runat="server" Text="Vonalkód:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:VonalKodTextBox ID="VonalKodTextBox1" runat="server" RequiredValidate="false"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label24" runat="server" Text="Postai azonosító:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:VonalKodTextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInput"  RequiredValidate="false"
                                                     Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="OrzoLabel" runat="server" CssClass="mrUrlapCaption" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:FelhasznaloTextBox ID="OrzoTextBox" runat="server" NewImageButtonVisible="false"
                                    SearchMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAllapot" runat="server" Text="Állapot:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right: 15px;">
                                <uc4:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label30" runat="server" Text="Megjegyzés:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="FelelosLabel" runat="server" CssClass="mrUrlapCaption" Text="Kezelő:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <csp:CsoportTextBox ID="Felelos_CsoportTextBox" runat="server" SearchMode="true"
                                    Validate="False" />
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKuldesMod" runat="server" Text="Küldésmód:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right: 15px;">
                                <uc4:KodtarakDropDownList ID="KuldesMod_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKuldemenyFajta" runat="server" Text="Küldemény fajtája:"/>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:KimenoKuldemenyFajta_SearchFormControl ID="KimenoKuldemenyFajta_SearchFormControl1" runat="server"/>
                                <asp:CheckBox ID="Elsobbsegi_CheckBox" runat="server" Text="Elsőbbségi" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" rowspan="2">
                                <asp:Label ID="Label25" runat="server" Text="Szolgáltatások:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="Ajanlott_CheckBox" runat="server" Text="Ajánlott" />
                                <asp:CheckBox ID="Tertiveveny_CheckBox" runat="server" Text="Tértivevény" />
                                <asp:CheckBox ID="SajatKezbe_CheckBox" runat="server" Text="Saját kézbe" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="E_Ertesites_CheckBox" runat="server" Text="E-értesítés" />
                                <asp:CheckBox ID="E_Elorejelzes_CheckBox" runat="server" Text="E-előrejelzés" />
                                <asp:CheckBox ID="PostaiLezaroSzolgalat_CheckBox" runat="server" Text="Postai lezáró szolgálat" />
                            </td>
                        </tr>
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label27" runat="server" Text="Ár:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:SzamIntervallum_SearchFormControl ID="Ar_SzamIntervallum_SearchFormControl" 
                                    runat="server" />
                            </td>
                        </tr>--%>                    
                        
                    </table>                    
                </eUI:eFormPanel>
                &nbsp;
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
           
                        
                        
    </table>
</asp:Content>
