<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KimenoeBeadvanyokList.aspx.cs" Inherits="KimenoeBeadvanyokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <%--HiddenFields--%>

    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeEREC_eBeadvanyok" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="updatePanelEREC_eBeadvanyok" runat="server" OnLoad="updatePanelEREC_eBeadvanyok_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeEREC_eBeadvanyok" runat="server" TargetControlID="panelEREC_eBeadvanyok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeEREC_eBeadvanyok" CollapseControlID="btnCpeEREC_eBeadvanyok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeEREC_eBeadvanyok"
                            ExpandedSize="0" ExpandedText="E-mail bor�t�kok list�ja" CollapsedText="E-mail bor�t�kok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelEREC_eBeadvanyok" runat="server" Width="100%">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="gridViewEREC_eBeadvanyok" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewEREC_eBeadvanyok_RowCommand"
                                            OnPreRender="gridViewEREC_eBeadvanyok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0"
                                            DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewEREC_eBeadvanyok_RowDataBound" OnSorting="gridViewEREC_eBeadvanyok_Sorting" AllowPaging="true">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div style="white-space: nowrap">
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage"
                                                    ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="Csny.">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" OnClientClick="return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- K�LD� RENSDZER--%>
                                                <asp:BoundField DataField="KuldoRendszer" HeaderText="K�ld�rendszer" SortExpression="EREC_eBeadvanyok.KuldoRendszer">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- �ZENET �LLAPOT --%>
                                                <asp:TemplateField HeaderText="�zenet �llapota" SortExpression="EREC_eBeadvanyok.Allapot">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="AllapotLabel" runat="server" Text='<%#Contentum.eRecord.BaseUtility.eBeadvanyHelper.GetAllapot(Eval("Allapot")+"",Page)%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- �ZENET TIPUSA --%>
                                                <asp:BoundField DataField="UzenetTipusa" HeaderText="�zenet t�pusa" SortExpression="EREC_eBeadvanyok.UzenetTipusa">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- DOKUMENTUM AZONOSITO --%>
                                                <asp:BoundField DataField="KR_DokTipusAzonosito" HeaderText="<%$Forditas:LabelDokumentumAzonosito*|Dokumentum azonos�t�%>" SortExpression="EREC_eBeadvanyok.KR_DokTipusAzonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <%-- DOKUMENTUM LEIRAS --%>
                                                <asp:BoundField DataField="KR_DokTipusLeiras" HeaderText="<%$Forditas:LabelDokumentumLeiras*|Dokumentum le�r�s%>" SortExpression="EREC_eBeadvanyok.KR_DokTipusLeiras">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- DOKUMENTUM HIVATAL --%>
                                                <asp:BoundField DataField="KR_DokTipusHivatal" HeaderText="<%$Forditas:LabelDokumentumHivatal*|Dokumentum hivatal%>" SortExpression="EREC_eBeadvanyok.KR_DokTipusHivatal">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- EREDETI CSATOLM�NY --%>
                                                <asp:TemplateField HeaderText="Csatolm�ny" SortExpression="EREC_eBeadvanyok.KR_FileNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <%# GetDocumentLink(Eval("Dokumentum_Id") as Guid?, Eval("KR_FileNev") as string) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- MEGJEGYZ�S --%>
                                                <asp:BoundField DataField="KR_Megjegyzes" HeaderText="Megjegyz�s" SortExpression="EREC_eBeadvanyok.KR_Megjegyzes">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- LET�LT�S IDEJE --%>
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Let�lt�s ideje" SortExpression="EREC_eBeadvanyok.LetrehozasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- PARTNER T�PUS --%>
                                                <asp:TemplateField HeaderText="Partner t�pus" SortExpression="EREC_eBeadvanyok.FeladoTipusa">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="FeladoTipusaLabel" runat="server" Text='<%#Contentum.eRecord.BaseUtility.PartnerHelper.GetPartnerTipus(Eval("FeladoTipusa")+"",Page)%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- PARTNER EMAIL --%>
                                                <asp:BoundField DataField="PartnerEmail" HeaderText="Partner e-mail" SortExpression="EREC_eBeadvanyok.PartnerEmail">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- PARTNER NEV --%>
                                                <asp:BoundField DataField="PartnerNev" HeaderText="Partner n�v" SortExpression="EREC_eBeadvanyok.PartnerNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- PARTNER ROVID NEV --%>
                                                <asp:BoundField DataField="PartnerRovidNev" HeaderText="Partner r�vid n�v" SortExpression="EREC_eBeadvanyok.PartnerRovidNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <%-- PARTNER CIM --%>
                                                <asp:TemplateField HeaderText="<%$Forditas:LabelPartnerCim*|Partner C�m%>" SortExpression="EREC_eBeadvanyok.PartnerKRID">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="PartnerCimLabel" runat="server" Text='<%#Contentum.eRecord.BaseUtility.PartnerHelper.GetCorrectPartnerCim(Eval("FeladoTipusa")+"",Eval("PartnerKapcsolatiKod")+"",Eval("PartnerKRID")+"")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- PARTNER M�k K�D --%>
                                                <asp:BoundField DataField="PartnerMAKKod" HeaderText="Partner M�K k�d" SortExpression="EREC_eBeadvanyok.PartnerMAKKod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <%-- HIVATKOZ�SI SZ�M --%>
                                                <asp:BoundField DataField="KR_HivatkozasiSzam" HeaderText="Hivatkoz�si sz�m" SortExpression="EREC_eBeadvanyok.KR_HivatkozasiSzam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- �RKEZTET�SI SZ�M --%>
                                                <asp:BoundField DataField="KR_ErkeztetesiSzam" HeaderText="�rkeztet�si sz�m" SortExpression="EREC_eBeadvanyok.KR_ErkeztetesiSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- �RV�NYESSG�I D�TUM --%>
                                                <asp:BoundField DataField="KR_ErvenyessegiDatum" HeaderText="�rv�nyess�gi d�tum" SortExpression="EREC_eBeadvanyok.KR_ErvenyessegiDatum" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <%-- �RKEZTET�SI D�TUM --%>
                                                <asp:BoundField DataField="KR_ErkeztetesiDatum" HeaderText="�rkeztet�si d�tum" SortExpression="EREC_eBeadvanyok.KR_ErkeztetesiDatum">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <%-- V�LASZ TITKOS�T�S --%>
                                                <asp:TemplateField HeaderText="V�lasz titkos�t�s" SortExpression="EREC_eBeadvanyok.KR_Valasztitkositas">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_Valasztitkositas" runat="server" Text='<%#DisplayBoolean(Eval("KR_Valasztitkositas"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- RENDSZER �ZENET --%>
                                                <asp:TemplateField HeaderText="Rendszer�zenet" SortExpression="EREC_eBeadvanyok.KR_Rendszeruzenet">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_Rendszeruzenet" runat="server" Text='<%#DisplayBoolean(Eval("KR_Rendszeruzenet"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- V�LASZ �TVONAL --%>
                                                <asp:TemplateField HeaderText="V�lasz �tvonal" SortExpression="EREC_eBeadvanyok.KR_Valaszutvonal">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_ValaszutvonalLabel" runat="server" Text='<%#Contentum.eRecord.BaseUtility.eBeadvanyHelper.GetValaszUtvonal(Eval("KR_Valaszutvonal"),Page)%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- T�RTER�LET --%>
                                                <asp:TemplateField HeaderText="T�rter�let" SortExpression="EREC_eBeadvanyok.KR_Tarterulet">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_TarteruletLabel" runat="server" Text='<%#Contentum.eRecord.BaseUtility.eBeadvanyHelper.GetTarterulet(Eval("KR_Tarterulet"),Page)%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- FI�K --%>
                                                <asp:BoundField DataField="KR_Fiok" HeaderText="Fi�k" SortExpression="EREC_eBeadvanyok.KR_Fiok">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="50px" />
                                                </asp:BoundField>
                                                <%-- E. �RKEZTET� AZONOS�T� --%>
                                                <asp:BoundField DataField="PR_ErkeztetesiSzam" HeaderText="E. �rkeztet� azon." SortExpression="EREC_eBeadvanyok.PR_ErkeztetesiSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- E. HIVATKOZASI AZONOS�T� --%>
                                                <asp:BoundField DataField="PR_ErkeztetesiSzam" HeaderText="E. hivatkoz�si azon." SortExpression="EREC_eBeadvanyok.PR_HivatkozasiSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <%-- E. TERTIVEV�NY --%>
                                                <asp:TemplateField HeaderText="ET�rtivev�ny" SortExpression="EREC_eBeadvanyok.KR_ETertiveveny">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_ETertiveveny" runat="server" Text='<%#DisplayBoolean(Eval("KR_ETertiveveny"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- 
                                                <asp:BoundField DataField="KR_ErkeztetesiSzam" HeaderText="�rkeztet�si sz�m" SortExpression="EREC_eBeadvanyok.KR_ErkeztetesiSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="EREC_eBeadvanyok.Allapot">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerNev" HeaderText="C�mzett" SortExpression="EREC_eBeadvanyok.PartnerNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerEmail" HeaderText="C�mzett e-mail c�me" SortExpression="EREC_eBeadvanyok.PartnerEmail" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerRovidNev" HeaderText="Hivatal r�vid neve" SortExpression="EREC_eBeadvanyok.PartnerRovidNev" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerKapcsolatiKod" HeaderText="Kapcsolati k�d" SortExpression="EREC_eBeadvanyok.PartnerKapcsolatiKod" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerKRID" HeaderText="Hivatal PartnerKRID-je" SortExpression="EREC_eBeadvanyok.PartnerKRID" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PartnerMAKKod" HeaderText="Hivatal M�K k�dja" SortExpression="EREC_eBeadvanyok.PartnerMAKKod" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KR_DokTipusHivatal" HeaderText="Hivatal" SortExpression="EREC_eBeadvanyok.KR_DokTipusHivatal">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KR_DokTipusAzonosito" HeaderText="T�pus" SortExpression="EREC_eBeadvanyok.KR_DokTipusAzonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KR_DokTipusLeiras" HeaderText="T�pus n�v" SortExpression="EREC_eBeadvanyok.KR_DokTipusLeiras" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KR_HivatkozasiSzam" HeaderText="Hivatkoz�si sz�m" SortExpression="EREC_eBeadvanyok.KR_HivatkozasiSzam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KR_Megjegyzes" HeaderText="Megjegyz�s" SortExpression="EREC_eBeadvanyok.KR_Megjegyzes">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="F�jln�v" SortExpression="EREC_eBeadvanyok.KR_FileNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <%#GetDocumentLink(Eval("Dokumentum_Id") as Guid?, Eval("KR_FileNev") as string) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="KR_ErkeztetesiDatum" HeaderText="�rkeztet�si d�tum" SortExpression="EREC_eBeadvanyok.KR_ErkeztetesiDatum">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="ET�rtivev�ny" SortExpression="EREC_eBeadvanyok.KR_ETertiveveny">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_ETertiveveny" runat="server" Text='<%#DisplayBoolean(Eval("KR_ETertiveveny"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Iratp�ld�ny" SortExpression="EREC_eBeadvanyok.IratPeldany_Azonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelIratpeldany" runat="server" Text='<%# GeIratPelanyLink(Eval("IratPeldany_Id") as Guid?, Eval("IratPeldany_Azonosito") as string)%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="V�lasz titkos�t�s" SortExpression="EREC_eBeadvanyok.KR_Valasztitkositas" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_Valasztitkositas" runat="server" Text='<%#DisplayBoolean(Eval("KR_Valasztitkositas"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="KR_ValaszutvonalNev" HeaderText="V�lasz �tvonal" SortExpression="EREC_eBeadvanyok.KR_Valaszutvonal" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Rendszer�zenet" SortExpression="EREC_eBeadvanyok.KR_Rendszeruzenet" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="KR_Rendszeruzenet" runat="server" Text='<%#DisplayBoolean(Eval("KR_Rendszeruzenet"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="KR_Fiok" HeaderText="Fi�k" SortExpression="EREC_eBeadvanyok.KR_Fiok" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="50px" />
                                                </asp:BoundField>
                                                --%>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

</asp:Content>

