using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class KimenoeBeadvanyokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    #region  BLG_3642
    private string Startup = "";
    private string Mode = String.Empty;
    #endregion

    protected void Page_PreInit(object sender, EventArgs e)
    {
        #region BLG_3642
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName) ?? String.Empty;

        if ((Startup == Constants.Startup.SearchForm)
            && (Session["eBeadvanyokListStartup"] == null))
        {
            Session["eBeadvanyokListStartup"] = Page.Request.QueryString.Get(Constants.Startup.StartupName);
            Response.Clear();
            Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
        }
        #endregion

        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "eBeadvanyokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(EREC_eBeadvanyokSearch);
        ListHeader1.CustomSearchObjectSessionName = "KimenoeBeadvanyokSearch";

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = "Elk�ld�tt hivatali kapus �zenetek";

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokSearch.aspx?Mode=Kimeno", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = "";
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokForm.aspx"
                 , CommandName.Command + "=" + CommandName.New
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = "";
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewEREC_eBeadvanyok.ClientID, "", "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewEREC_eBeadvanyok.ClientID);
        ListHeader1.HistoryOnClientClick = "";

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEREC_eBeadvanyok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewEREC_eBeadvanyok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEREC_eBeadvanyok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewEREC_eBeadvanyok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEREC_eBeadvanyok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, e-mail megtekint�s */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_eBeadvanyokSearch(), "KimenoeBeadvanyokSearch");

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        //BLG_3642
        //if (!IsPostBack)
        //    EREC_eBeadvanyokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);

        #region BLG_3642
        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["eBeadvanyokListStartup"] != null && Session["eBeadvanyokListStartup"].ToString() == Constants.Startup.SearchForm)
        {
            string script = "OpenNewWindow(); function OpenNewWindow() { ";
            if (Session["eBeadvanyokListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("eBeadvanyokSearch.aspx?Mode=Kimeno", ""
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList
                , CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            //Lez�r�s
            script += "}";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "eBeadvanyokListStartup", script, true);
            Session.Remove("eBeadvanyokListStartup");

            popupNyitas = true;
        }

        // Ha keres� k�perny�t is kell nyitni, nem t�ltj�k fel a list�t:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            EREC_eBeadvanyokGridViewBind();
        }
        #endregion
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = false;
        ListHeader1.NewVisible = false;

        //teszt
        //ListHeader1.NewEnabled = true;
        //ListHeader1.NewVisible = true;

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.View);
        ListHeader1.ModifyEnabled = false;
        ListHeader1.ModifyVisible = false;
        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdat" + CommandName.Invalidate);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.ViewHistory);


        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.Lock);

        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);

        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.ExcelExport);
    }

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void EREC_eBeadvanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEREC_eBeadvanyok", ViewState, "EREC_eBeadvanyok.KR_ErkeztetesiDatum");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEREC_eBeadvanyok", ViewState, SortDirection.Descending);

        EREC_eBeadvanyokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void EREC_eBeadvanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_eBeadvanyokSearch search = (EREC_eBeadvanyokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_eBeadvanyokSearch(), "KimenoeBeadvanyokSearch");
        search.OrderBy = Search.GetOrderBy("gridViewEREC_eBeadvanyok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        search.Irany.Value = "1"; //Kimen�
        search.Irany.Operator = Query.Operators.equals;

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewEREC_eBeadvanyok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewEREC_eBeadvanyok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetCsatolmanyInfo(e);
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetCsatolmanyInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "return false;";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            //DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewEREC_eBeadvanyok_PreRender(object sender, EventArgs e)
    {
        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEREC_eBeadvanyok);

        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEREC_eBeadvanyok);
        ListHeader1.RefreshPagerLabel();

    }


    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EREC_eBeadvanyokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEREC_eBeadvanyok);
        EREC_eBeadvanyokGridViewBind();
    }


    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewEREC_eBeadvanyok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEREC_eBeadvanyok, selectedRowNumber, "check");

            string id = gridViewEREC_eBeadvanyok.DataKeys[selectedRowNumber].Value.ToString();

        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID);
            ListHeader1.ModifyOnClientClick = "";
            string tableName = "EREC_eBeadvanyok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelEREC_eBeadvanyok.ClientID);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelEREC_eBeadvanyok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    EREC_eBeadvanyokGridViewBind();
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, gridViewEREC_eBeadvanyok, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedEmailBoritekRecords();
                EREC_eBeadvanyokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedEmailBoritekRecords();
                EREC_eBeadvanyokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedEREC_eBeadvanyok();
                EREC_eBeadvanyokGridViewBind();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedEmailBoritekRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewEREC_eBeadvanyok, "EREC_eBeadvanyok"
            , "eBeadvanyokLock", "eBeadvanyokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedEmailBoritekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewEREC_eBeadvanyok, "EREC_eBeadvanyok"
            , "eBeadvanyokLock", "eBeadvanyokForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEREC_eBeadvanyok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEREC_eBeadvanyok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEREC_eBeadvanyok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_eBeadvanyok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewEREC_eBeadvanyok_Sorting(object sender, GridViewSortEventArgs e)
    {
        EREC_eBeadvanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewEREC_eBeadvanyok", ViewState, e.SortExpression));
    }

    #endregion

    public string GetDocumentLink(Guid? dokumentumId, string fileNev)
    {
        if (dokumentumId != null)
        {
            return String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('GetDocumentContent.aspx?id={0}'); return false;\" style=\"text-decoration:underline\">{1}</a>", dokumentumId, fileNev);
        }
        else
        {
            return fileNev;
        }
    }

    public string GeIratPelanyLink(Guid? iratPeldany_Id, string azonosito)
    {
        if (iratPeldany_Id == null)
        {
            return String.Empty;
        }
        else
        {
            string onclick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratPeldany_Id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            return String.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\" style=\"text-decoration:underline\">{1}</a>", onclick, azonosito);
        }
    }

    #region segedFv

    // Segedfv.

    private const string IratIktatoszamEsId_Delimitter = "***";

    protected string GetIktatoszamFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return String.Empty;
        }
    }

    protected string GetIdFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return String.Empty;
            }
        }
        else
        {
            return String.Empty;
        }
    }

    protected string DisplayBoolean(object value)
    {
        if (value != null)
        {
            if ("1".Equals(value.ToString()))
            {
                return "Igen";
            }
        }

        return "Nem";
    }

    #endregion

}
