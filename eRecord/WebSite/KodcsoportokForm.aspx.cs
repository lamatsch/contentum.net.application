using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Web.UI.WebControls;

public partial class KodcsoportokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region utility
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxNev.TextBox.Enabled = false;
        requiredTextBoxKod.TextBox.Enabled = false;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        //textHossz.Enabled = false;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelKod.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        //labelHossz.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        requiredTextBoxKod.TextBox.Enabled = false;

        labelKod.CssClass = "mrUrlapInputWaterMarked";
    }

    #endregion
    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Kodcsoport" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_KodCsoportok krt_KodCsoportok = (KRT_KodCsoportok)result.Record;
                    LoadComponentsFromBusinessObject(krt_KodCsoportok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_KodCsoportok"></param>
    private void LoadComponentsFromBusinessObject(KRT_KodCsoportok krt_KodCsoportok)
    {
        requiredTextBoxNev.Text = krt_KodCsoportok.Nev;
        requiredTextBoxKod.Text = krt_KodCsoportok.Kod;
        ErvenyessegCalendarControl1.ErvKezd = krt_KodCsoportok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_KodCsoportok.ErvVege;
        //textHossz.Text = krt_KodCsoportok.Hossz;

        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = krt_KodCsoportok.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_KodCsoportok.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_KodCsoportok GetBusinessObjectFromComponents()
    {
        KRT_KodCsoportok krt_KodCsoportok = new KRT_KodCsoportok();
        krt_KodCsoportok.Updated.SetValueAll(false);
        krt_KodCsoportok.Base.Updated.SetValueAll(false);

        krt_KodCsoportok.Nev = requiredTextBoxNev.Text;
        krt_KodCsoportok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        krt_KodCsoportok.Kod = requiredTextBoxKod.Text;
        krt_KodCsoportok.Updated.Kod = pageView.GetUpdatedByView(requiredTextBoxKod);

        //krt_KodCsoportok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_KodCsoportok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_KodCsoportok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_KodCsoportok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_KodCsoportok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_KodCsoportok.Modosithato = BoolString.Yes;
        krt_KodCsoportok.Updated.Modosithato = true;
        //krt_KodCsoportok.Hossz = textHossz.Text;
        //krt_KodCsoportok.Updated.Hossz = pageView.GetUpdatedByView(textHossz);

        //ezt nem tudom honnan k�ne venni
        krt_KodCsoportok.BesorolasiSema = "0";
        krt_KodCsoportok.Updated.BesorolasiSema = true;

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_KodCsoportok.Base.Ver = FormHeader1.Record_Ver;
        krt_KodCsoportok.Base.Updated.Ver = true;

        return krt_KodCsoportok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            //compSelector.Add_ComponentOnClick(textHossz);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
                            KRT_KodCsoportok krt_KodCsoportok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_KodCsoportok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
                                KRT_KodCsoportok krt_KodCsoportok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_KodCsoportok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
