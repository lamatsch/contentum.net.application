<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="KodcsoportokLovList.aspx.cs" Inherits="KodcsoportokLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,KodcsoportokLovListHeaderTitle%>" />
    <br />
    <div class="popupBody">
         <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />                    
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td>
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">                                                
                                        <asp:Label ID="Label1" runat="server" Text="K�dcsoport neve:" Font-Bold="true"></asp:Label><br />
                                        <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%"></asp:TextBox>&nbsp;
                                        <asp:Button ID="ButtonSearch" runat="server" Text="Keres�s" OnClick="ButtonSearch_Click" />
                                        <asp:Button ID="ButtonAdvancedSearch" runat="server" Text="R�szletes keres�s" />
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">                    
                                        <div class="listaFulFelsoCsikKicsi"><img src="images/hu/design/spacertrans.gif" alt="" /></div>                            
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="20" Width="95%">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.jpg" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>       
                        </ContentTemplate>
                    </asp:UpdatePanel>
        </eUI:eFormPanel>
        <br />
        <uc2:LovListFooter ID="LovListFooter1" runat="server" />
    </div> 
</asp:Content>

