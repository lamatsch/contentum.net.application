using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class KodcsoportokSearch : Contentum.eUtility.UI.PageBase
{

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        static public bool Contain(string value)
        {
            if ((value == Yes) || (value == No) || (value == NotSet))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    private Type _type = typeof(KRT_KodCsoportokSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_KodCsoportokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_KodCsoportokSearch)Search.GetSearchObject(Page, new KRT_KodCsoportokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_KodCsoportokSearch _KRT_KodCsoportokSearch = (KRT_KodCsoportokSearch)searchObject;

        if (_KRT_KodCsoportokSearch != null)
        {
            textNev.Text = _KRT_KodCsoportokSearch.Nev.Value;
            textKod.Text = _KRT_KodCsoportokSearch.Kod.Value;
            if (BoolString.Contain(_KRT_KodCsoportokSearch.Modosithato.Value))
            {
                ddownModosithato.SelectedValue = _KRT_KodCsoportokSearch.Modosithato.Value;
            }
            else
            {
                ddownModosithato.SelectedValue = BoolString.NotSet;
            }
            //textHossz.Text = _KRT_KodCsoportokSearch.Hossz.Value;
            //switch (_KRT_KodCsoportokSearch.Hossz.Operator)
            //{
            //    case Query.Operators.greaterorequal:
            //        radioListHossz.SelectedValue = "Greater";
            //        break;
            //    case Query.Operators.equals:
            //        radioListHossz.SelectedValue = "Equal";
            //        break;
            //    case Query.Operators.lessorequal:
            //        radioListHossz.SelectedValue = "Lesser";
            //        break;
            //    default:
            //        radioListHossz.SelectedValue = "Equal";
            //        break;
            //}
            Ervenyesseg_SearchFormComponent1.SetDefault(
                _KRT_KodCsoportokSearch.ErvKezd, _KRT_KodCsoportokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_KodCsoportokSearch SetSearchObjectFromComponents()
    {
        KRT_KodCsoportokSearch _KRT_KodCsoportokSearch = (KRT_KodCsoportokSearch)SearchHeader1.TemplateObject;

        if (_KRT_KodCsoportokSearch == null)
        {
            _KRT_KodCsoportokSearch = new KRT_KodCsoportokSearch();
        }

        _KRT_KodCsoportokSearch.Nev.Value = textNev.Text;
        _KRT_KodCsoportokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _KRT_KodCsoportokSearch.Kod.Value = textKod.Text;
        _KRT_KodCsoportokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(textKod.Text);
        _KRT_KodCsoportokSearch.Modosithato.Value = ddownModosithato.SelectedValue;
        if (ddownModosithato.SelectedValue != BoolString.NotSet)
        {
            _KRT_KodCsoportokSearch.Modosithato.Operator = Query.Operators.equals;
        }
        else
        {
            _KRT_KodCsoportokSearch.Modosithato.Operator = "";
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_KodCsoportokSearch.ErvKezd, _KRT_KodCsoportokSearch.ErvVege);

        //int hossz = 0;
        //if (Int32.TryParse(textHossz.Text, out hossz))
        //{
        //    _KRT_KodCsoportokSearch.Hossz.Value = hossz.ToString();
        //    switch (radioListHossz.SelectedValue)
        //    {
        //        case "Greater":
        //            _KRT_KodCsoportokSearch.Hossz.Operator = Query.Operators.greaterorequal;
        //            break;
        //        case "Lesser":
        //            _KRT_KodCsoportokSearch.Hossz.Operator = Query.Operators.lessorequal;
        //            break;
        //        case "Equal":
        //            _KRT_KodCsoportokSearch.Hossz.Operator = Query.Operators.equals;
        //            break;

        //    }
        //}

        return _KRT_KodCsoportokSearch;
    }
    
    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_KodCsoportokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }        
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_KodCsoportokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_KodCsoportokSearch();
    }
}
