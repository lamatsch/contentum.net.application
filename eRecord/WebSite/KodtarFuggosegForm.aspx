<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KodtarFuggosegForm.aspx.cs" Inherits="KodtarFuggosegForm" Title="K�dt�r f�gg�s�g" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KodcsoportokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <asp:UpdatePanel runat="server" ID="FormUpdatePanel" UpdateMode="Always">
                <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapNyitoSor">
                                <td class="mrUrlapCaption">
                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                <td class="mrUrlapMezo">
                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="LabelKodcsoportVezerlo" runat="server" Text="Vez�rl� k�dcsoport:" /></td>
                                <td class="mrUrlapMezo" colspan="3">
                                    <uc6:KodcsoportokTextBox ID="KodcsoportokVezerloTextBox" runat="server" SearchMode="true" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKodcsoportFuggo" runat="server" Text="F�gg� k�dcsoport:" /></td>
                                <td class="mrUrlapMezo" colspan="3">
                                    <uc6:KodcsoportokTextBox ID="KodcsoportokFuggoTextBox" runat="server" SearchMode="true" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor" runat="server" id="TROsszerendeles">
                                <td class="mrUrlapCaption" />
                                <td class="mrUrlapMezo" colspan="3">
                                    <asp:Button runat="server" ID="ButtonAssign" Text="�sszerendel�s" OnClick="ButtonAssign_Click" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:Table runat="server" ID="TableKodtarFuggosegMatrix" />
                    <%--<asp:PlaceHolder runat="server" ID="PlaceHolderMatrix" />--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
