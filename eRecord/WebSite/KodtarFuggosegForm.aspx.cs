using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class KodtarFuggosegForm : Contentum.eUtility.UI.PageBase
{
    #region MIX
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    #endregion

    #region CONSTANTS
    private const string ConstKrtTableName = "KRT_KodtarFuggoseg";
    private const string ConstCheckBoxNincs = "#CheckBox_Nincs#";
    private const string ConstCheckBoxItem = "#CheckBox_Item#";
    private const string ConstPipeLine = "|";
    private const string ConstDefaultKodtarFuggosegOrderBy = "Nev ASC";
    private const string ConsQueryStringVezerloKodtarIdParamName = "VKTID";
    private const string ConsQueryStringFuggoKodtarIdParamName = "FKTID";
    private const string ConsQueryStringEditTypeParamName = "TYPE";
    private string ConstSessionKeyKodtarFuggosegFormReload = "ConstSessionKeyKodtarFuggosegFormReload";
    private string ConstSessionKeyKodtarFuggosegInsertAndOpenModify = "ConstSessionKeyKodtarFuggosegInsertAndOpenModify";
    public enum EnumEditItemyType
    {
        NINCS,
        FUGGO,
        VEZERLO,
        INVALID
    }
    #endregion

    #region UTILITY
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }

    #endregion

    #region PAGE EVENTS
    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session[ConstSessionKeyKodtarFuggosegFormReload] != null && Session[ConstSessionKeyKodtarFuggosegFormReload].ToString().ToLower() == "true")
        {
            Session.Remove(ConstSessionKeyKodtarFuggosegFormReload);
            Response.Redirect(Request.RawUrl);
        }
        Command = Request.QueryString.Get(CommandName.Command);
        FormHeader1.HeaderTitle = Resources.List.KodtarFuggosegListHeader;
        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Command != null)
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, ConstKrtTableName + Command);
                else
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KRT_KodtarFuggosegKezeles");
                break;
        }
        //KodtarFuggosegDataClass inputData = null;
        //if (Command == CommandName.View || Command == CommandName.Modify)
        //{
        //    string id = null;
        //    if (!CheckInputId(out id))
        //        return;

        //    Result result = null;
        //    if (!KodtarFuggosegGet(id, out result))
        //    {
        //        return;
        //    }

        //    if (result == null || result.Ds.Tables[0].Rows.Count < 1)
        //        return;

        //    KRT_KodtarFuggoseg KRT_KodtarFuggoseg = new KRT_KodtarFuggoseg();
        //    DataSetHelper.LoadUpdateBusinessDocumentFromDataRow(KRT_KodtarFuggoseg, result.Ds.Tables[0].Rows[0]);

        //    LoadComponentsFromBusinessObject(KRT_KodtarFuggoseg);
        //    inputData = JSONFunctions.DeSerialize(KRT_KodtarFuggoseg.Adat);
        //    GenerateKodtarFuggosegMatrix(inputData);
        //}
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (Command == CommandName.New)
        {
            GenerateKodtarFuggosegMatrix(null);
        }
        else if (Command == CommandName.View || Command == CommandName.Modify)
        {
            KodtarFuggosegDataClass inputData = null;
            string id = null;
            if (!CheckInputId(out id))
                return;

            Result result = null;
            if (!KodtarFuggosegGet(id, out result))
            {
                return;
            }

            if (result == null || result.Ds.Tables[0].Rows.Count < 1)
                return;

            KRT_KodtarFuggoseg KRT_KodtarFuggoseg = new KRT_KodtarFuggoseg();
            DataSetHelper.LoadUpdateBusinessDocumentFromDataRow(KRT_KodtarFuggoseg, result.Ds.Tables[0].Rows[0]);

            LoadComponentsFromBusinessObject(KRT_KodtarFuggoseg);
            inputData = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(KRT_KodtarFuggoseg.Adat);
            GenerateKodtarFuggosegMatrix(inputData);
        }
        SetVisibility();
    }

    /// <summary>
    /// Assign button click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonAssign_Click(object sender, EventArgs e)
    {
        AssignProcedure();
    }
    /// <summary>
    /// Ment�s gomb folyamat ind�t�sa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        SaveButtonProcedure(e);
    }
    #endregion

    #region BO <-> CONTROLS
    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="item"></param>
    private void LoadComponentsFromBusinessObject(KRT_KodtarFuggoseg item)
    {
        if (item == null)
            return;

        #region VEZERLO
        KodcsoportokVezerloTextBox.Id_HiddenField = item.Vezerlo_KodCsoport_Id;
        if (KodcsoportokVezerloTextBox.Id_HiddenField != "")
        {
            KodcsoportokVezerloTextBox.SetTextBoxById(null);
        }
        else
        {
            KodcsoportokVezerloTextBox.Text = "";
        }
        #endregion

        #region FUGGO
        KodcsoportokFuggoTextBox.Id_HiddenField = item.Fuggo_KodCsoport_Id;
        if (KodcsoportokFuggoTextBox.Id_HiddenField != "")
        {
            KodcsoportokFuggoTextBox.SetTextBoxById(null);
        }
        else
        {
            KodcsoportokFuggoTextBox.Text = "";
        }
        #endregion

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = item.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(item.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_KodtarFuggoseg GetBusinessObjectFromComponents()
    {
        KRT_KodtarFuggoseg item = new KRT_KodtarFuggoseg();
        item.Updated.SetValueAll(false);
        item.Base.Updated.SetValueAll(false);

        if (KodcsoportokVezerloTextBox.Id_HiddenField != "")
        {
            item.Vezerlo_KodCsoport_Id = KodcsoportokVezerloTextBox.Id_HiddenField;
        }
        if (KodcsoportokFuggoTextBox.Id_HiddenField != "")
        {
            item.Fuggo_KodCsoport_Id = KodcsoportokFuggoTextBox.Id_HiddenField;
        }
        item.Base.Ver = FormHeader1.Record_Ver;
        item.Base.Updated.Ver = true;

        return item;
    }
    #endregion

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            FormFooter1.SaveEnabled = false;
        }
    }

    /// <summary>
    /// Start save procedure
    /// </summary>
    /// <param name="e"></param>
    private void SaveButtonProcedure(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + Command))
            {
                KodtarFuggosegDataClass jsonData = GetSelectedData();
                string aktiv = (jsonData.Items.Count > 0 || jsonData.ItemsNincs.Count > 0) ? BoolString.Yes : BoolString.No;
                string jsonDataAsString = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.Serialize(jsonData);

                if (e.CommandName == CommandName.Save)
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                //if (jsonData.Items.Count < 1 && jsonData.ItemsNincs.Count < 1)
                                //{
                                //    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageKodtarFuggosegError, Resources.Error.MessageKodtarFuggosegRequiredMin1Kodtar);
                                //    return;
                                //}
                                string outId;
                                InsertProcedure(jsonDataAsString, aktiv, true, out outId);
                                Session[ConstSessionKeyKodtarFuggosegInsertAndOpenModify] = outId;
                                break;
                            }

                        case CommandName.Modify:
                            {
                                UpdateProcedure(jsonDataAsString, aktiv);
                                break;
                            }
                    }
                }
                else if (e.CommandName == CommandName.SaveAndClose)
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                //if (jsonData.Items.Count < 1 && jsonData.ItemsNincs.Count < 1)
                                //{
                                //    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageKodtarFuggosegError, Resources.Error.MessageKodtarFuggosegRequiredMin1Kodtar);
                                //    return;
                                //}
                                string outId;
                                InsertProcedure(jsonDataAsString, aktiv, true, out outId);
                                break;
                            }
                    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    #region CRUD PROCEDURES
    /// <summary>
    /// Execute update rpocedure
    /// </summary>
    private void UpdateProcedure(string jsonData, string aktiv)
    {
        String recordId = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(recordId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {
            KRT_KodtarFuggoseg item = GetBusinessObjectFromComponents();
            item.Adat = jsonData;
            item.Updated.Adat = true;
            item.Aktiv = aktiv;
            item.Updated.Aktiv = true;
            Update(recordId, item);
        }
    }
    /// <summary>
    /// Execute insert procedure
    /// </summary>
    private void InsertProcedure(string jsonData, string aktiv, bool requireClose, out string outId)
    {
        outId = null;
        KRT_KodtarFuggoseg item = GetBusinessObjectFromComponents();
        item.Adat = jsonData;
        item.Updated.Adat = true;
        item.Aktiv = aktiv;
        item.Updated.Aktiv = true;
        outId = item.Id = Guid.NewGuid().ToString();
        item.Updated.Id = true;

        if (IsFuggoKodtarExisting(item.Vezerlo_KodCsoport_Id, item.Fuggo_KodCsoport_Id))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageKodtarFuggosegError, Resources.Error.MessageKodtarFuggosegExists);
        }
        else
        {
            Insert(item, requireClose);
        }
    }
    #endregion

    #region HELPER
    /// <summary>
    /// Check existing of id
    /// </summary>
    /// <returns></returns>
    private bool CheckInputId(out string id)
    {
        id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }
        return true;
    }

    private TableCell GetDefaultTableCell(string text, bool isBold)
    {
        TableCell cell = new TableCell();
        //cell.Attributes.Add("style", "BORDER-TOP: ridge;BORDER - BOTTOM: ridge; BORDER - LEFT: ridge");
        cell.Attributes.Add("style", "BORDER: ridge;");
        if (!string.IsNullOrEmpty(text))
            cell.Text = text;
        cell.Font.Bold = isBold;
        return cell;
    }
    /// <summary>
    /// �sszegy�jti a m�trix alapj�n a kiv�lasztott elemeket
    /// </summary>
    /// <returns></returns>
    private KodtarFuggosegDataClass GetSelectedData()
    {
        KodtarFuggosegDataClass data = new KodtarFuggosegDataClass();
        data.VezerloKodCsoportId = KodcsoportokVezerloTextBox.Id_HiddenField;
        data.FuggoKodCsoportId = KodcsoportokFuggoTextBox.Id_HiddenField;

        if (Page.IsPostBack)
            foreach (string item in Request.Params.Keys)
            {
                if (item.Contains(ConstCheckBoxNincs))
                {
                    var on = Request.Params[item];
                    string[] splitted = item.Split(new string[] { ConstCheckBoxNincs }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitted.Length != 2)
                        continue;
                    string vezerloId = splitted[1];

                    data.ItemsNincs.Add(new KodtarFuggosegDataNincsClass()
                    {
                        VezerloKodTarId = vezerloId,
                        Nincs = true,
                    }
                  );
                }
                if (item.Contains(ConstCheckBoxItem))
                {
                    var on = Request.Params[item];
                    string[] splitted = item.Split(new string[] { ConstCheckBoxItem }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitted.Length != 2)
                        continue;

                    string[] splittedIds = splitted[1].Split(new string[] { ConstPipeLine }, StringSplitOptions.RemoveEmptyEntries);
                    string vezerlo = splittedIds[0];
                    string fuggo = splittedIds[1];

                    data.Items.Add(new KodtarFuggosegDataItemClass()
                    {
                        VezerloKodTarId = vezerlo,
                        FuggoKodtarId = fuggo,
                        Aktiv = true
                    }
                    );
                }
            }
        return data;
    }

    /// <summary>
    /// L�that�s�gok be�ll�t�sa
    /// </summary>
    private void SetVisibility()
    {
        switch (Command)
        {
            case CommandName.View:
                TableKodtarFuggosegMatrix.Enabled = false;
                KodcsoportokVezerloTextBox.Enabled = false;
                KodcsoportokFuggoTextBox.Enabled = false;
                ButtonAssign.Visible = false;
                TROsszerendeles.Visible = false;
                break;
            case CommandName.New:
                TableKodtarFuggosegMatrix.Enabled = true;
                KodcsoportokVezerloTextBox.Enabled = true;
                KodcsoportokFuggoTextBox.Enabled = true;
                ButtonAssign.Visible = true;
                TROsszerendeles.Visible = true;
                //FormFooter1.ImageButton_SaveAndClose.Visible = true;
                break;
            case CommandName.Modify:
                TableKodtarFuggosegMatrix.Enabled = true;
                KodcsoportokVezerloTextBox.Enabled = false;
                KodcsoportokFuggoTextBox.Enabled = false;
                ButtonAssign.Visible = false;
                TROsszerendeles.Visible = false;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// K�dt�rak �sszerendel�s, �s m�trix el��ll�t�sa
    /// </summary>
    private void AssignProcedure()
    {
        string VezerloKodCsoportId = KodcsoportokVezerloTextBox.Id_HiddenField;
        string FuggoKodCsoportId = KodcsoportokFuggoTextBox.Id_HiddenField;
        if (string.IsNullOrEmpty(VezerloKodCsoportId) || string.IsNullOrEmpty(FuggoKodCsoportId))
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageKodtarFuggosegError, Resources.Error.MessageKodtarFuggosegInvalidParameters);
        GenerateKodtarFuggosegMatrix(null);
    }
    #endregion

    #region MATRIX
    /// <summary>
    /// Legneer�lja a m�trixot
    /// </summary>
    /// <param name="inputData"></param>
    private void GenerateKodtarFuggosegMatrix(KodtarFuggosegDataClass inputData)
    {
        Result result = null;
        string vezerlo = KodcsoportokVezerloTextBox.Id_HiddenField;
        string fuggo = KodcsoportokFuggoTextBox.Id_HiddenField;
        if (string.IsNullOrEmpty(vezerlo) || string.IsNullOrEmpty(fuggo))
            return;

        if (!KodtarakGet(vezerlo, fuggo, out result))
            return;

        if (result == null || result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
            return;

        List<KodtarFuggosegHelperClass> kodtarElementsVezerlo, kodtarElementsFuggo;
        KodtarFuggosegMatrixFillObjects(result, vezerlo, fuggo, out kodtarElementsVezerlo, out kodtarElementsFuggo);

        KodtarFuggosegMatrixCreateTable(kodtarElementsVezerlo, kodtarElementsFuggo, inputData);

    }
    /// <summary>
    /// �ssze�ll�tja az oszt�lyokat az adatb�zisb�l kapott adatok alapj�n
    /// </summary>
    /// <param name="result"></param>
    /// <param name="vezerlo"></param>
    /// <param name="fuggo"></param>
    /// <param name="kodtarElementsVezerlo"></param>
    /// <param name="kodtarElementsFuggo"></param>
    private void KodtarFuggosegMatrixFillObjects(Result result, string vezerlo, string fuggo, out List<KodtarFuggosegHelperClass> kodtarElementsVezerlo, out List<KodtarFuggosegHelperClass> kodtarElementsFuggo)
    {
        kodtarElementsVezerlo = new List<KodtarFuggosegHelperClass>();
        kodtarElementsFuggo = new List<KodtarFuggosegHelperClass>();
        KodtarFuggosegHelperClass temp;
        foreach (DataRow item in result.Ds.Tables[0].Rows)
        {
            temp = new KodtarFuggosegHelperClass();
            temp.Id = item["Id"].ToString();
            temp.KodCsoportId = item["KodCsoport_Id"].ToString();
            temp.Nev = item["Nev"].ToString();
            if (temp.KodCsoportId != null)
            {
                if (temp.KodCsoportId.Equals(vezerlo, StringComparison.CurrentCultureIgnoreCase))
                {
                    kodtarElementsVezerlo.Add(temp);
                }
                else if (temp.KodCsoportId.Equals(fuggo,StringComparison.CurrentCultureIgnoreCase))
                {
                    kodtarElementsFuggo.Add(temp);
                }
            }
        }
    }
    /// <summary>
    /// M�trix t�bl�zat gener�l�sa, �s kit�lt�se adatokkal.
    /// </summary>
    /// <param name="kodtarElementsVezerlo"></param>
    /// <param name="kodtarElementsFuggo"></param>
    /// <param name="data"></param>
    private void KodtarFuggosegMatrixCreateTable(List<KodtarFuggosegHelperClass> kodtarElementsVezerlo, List<KodtarFuggosegHelperClass> kodtarElementsFuggo, KodtarFuggosegDataClass data)
    {
        TableKodtarFuggosegMatrix.Rows.Clear();

        #region HEADER
        TableHeaderRow headerRow = new TableHeaderRow();
        TableHeaderCell hedaerCell = null;

        hedaerCell = new TableHeaderCell();
        hedaerCell.Text = @"V \ F";
        headerRow.Cells.Add(hedaerCell);

        #region NINCS
        hedaerCell = new TableHeaderCell();
        hedaerCell.Text = Resources.Form.KodtarFuggosegNincsColumnTitle;
        hedaerCell.Attributes.Add("style", "BORDER: ridge;");

        hedaerCell.Controls.Add(GetNincsHyperlink(Resources.Form.KodtarFuggosegNincsColumnTitle));

        headerRow.Cells.Add(hedaerCell);
        #endregion

        #region KODTAR
        foreach (KodtarFuggosegHelperClass item in kodtarElementsFuggo)
        {
            hedaerCell = new TableHeaderCell();
            hedaerCell.ID = item.Id;
            hedaerCell.Text = item.Nev;
            hedaerCell.Attributes.Add("style", "BORDER: ridge;");

            hedaerCell.Controls.Add(GetFuggoKcsHyperlink(item.Nev, item.Id));

            headerRow.Cells.Add(hedaerCell);
        }
        #endregion

        TableKodtarFuggosegMatrix.Rows.Add(headerRow);
        #endregion

        #region ROWS
        TableRow row = null;
        TableCell cell = null;
        foreach (KodtarFuggosegHelperClass itemVezerlo in kodtarElementsVezerlo)
        {
            row = new TableRow();
            row.ID = itemVezerlo.Id;

            #region VEZERLO COLUMN
            cell = GetDefaultTableCell(itemVezerlo.Nev, true);
            HyperLink link = GetVezerloKcsHyperlink(itemVezerlo.Nev, itemVezerlo.Id);
            cell.Controls.Add(link);
            row.Cells.Add(cell);
            #endregion

            #region NINCS
            cell = GetDefaultTableCell(null, false);
            CheckBox checkBoxNincs = new CheckBox() { AutoPostBack = true, ID = ConstCheckBoxNincs + itemVezerlo.Id };
            SetCheckBoxNincs(ref checkBoxNincs, itemVezerlo.Id, data);
            cell.Controls.Add(checkBoxNincs);
            row.Cells.Add(cell);
            #endregion

            CheckBox checkBoxItems = null;
            foreach (KodtarFuggosegHelperClass itemFuggo in kodtarElementsFuggo)
            {
                cell = GetDefaultTableCell(null, false);
                checkBoxItems = new CheckBox() { AutoPostBack = true, ID = ConstCheckBoxItem + itemVezerlo.Id + ConstPipeLine + itemFuggo.Id };
                SetCheckBoxItem(ref checkBoxItems, itemVezerlo.Id, itemFuggo.Id, data);
                cell.Controls.Add(checkBoxItems);
                row.Cells.Add(cell);
            }

            TableKodtarFuggosegMatrix.Rows.Add(row);
        }
        #endregion
    }
    /// <summary>
    /// A Nincs checkbox oszlop be�ll�t�sa
    /// </summary>
    /// <param name="control"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <param name="data"></param>
    private void SetCheckBoxNincs(ref CheckBox control, string vezerloKodtarId, KodtarFuggosegDataClass data)
    {
        if (data == null)
            return;
        foreach (KodtarFuggosegDataNincsClass item in data.ItemsNincs)
        {
            if (item.Nincs == null || item.Nincs.Value == false)
                continue;

            if (item.VezerloKodTarId != null && item.VezerloKodTarId.Equals(vezerloKodtarId,StringComparison.CurrentCultureIgnoreCase))
            {
                control.Checked = true;
                break;
            }
        }
    }
    /// <summary>
    /// Adat checkbox oszlop be�ll�t�sa
    /// </summary>
    /// <param name="control"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <param name="fuggoKodtarId"></param>
    /// <param name="data"></param>
    private void SetCheckBoxItem(ref CheckBox control, string vezerloKodtarId, string fuggoKodtarId, KodtarFuggosegDataClass data)
    {
        if (data == null)
            return;
        foreach (KodtarFuggosegDataItemClass item in data.Items)
        {
            if (item.VezerloKodTarId != null && item.FuggoKodtarId != null &&  item.VezerloKodTarId.Equals(vezerloKodtarId,StringComparison.CurrentCultureIgnoreCase) && item.FuggoKodtarId.Equals(fuggoKodtarId,StringComparison.CurrentCultureIgnoreCase))
            {
                control.Checked = true;
                break;
            }
        }
    }
    /// <summary>
    /// Create hypelink for edit
    /// </summary>
    /// <param name="nev"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <returns></returns>
    private HyperLink GetVezerloKcsHyperlink(string nev, string vezerloKodtarId)
    {
        HyperLink link = new HyperLink();
        link.Text = nev;

        if (Command == CommandName.Modify)
        {
            link.NavigateUrl = " ";
            string javascript = JavaScripts.SetOnClientClick("KodtarFuggosegItemEdit.aspx",
             CommandName.Command + "=" + CommandName.Modify + "&" +
            QueryStringVars.Id + "=" + Request.QueryString.Get(QueryStringVars.Id) + "&" +
            ConsQueryStringEditTypeParamName + "=" + EnumEditItemyType.VEZERLO + "&" +
            ConsQueryStringVezerloKodtarIdParamName + "=" + vezerloKodtarId,
            Defaults.PopupWidth, Defaults.PopupHeight, TableKodtarFuggosegMatrix.ClientID, EventArgumentConst.refresh);
            link.Attributes.Add("onclick", javascript);
        }
        return link;
    }
    /// <summary>
    /// Create hyperlink for edit
    /// </summary>
    /// <param name="nev"></param>
    /// <param name="fuggoKodtarId"></param>
    /// <returns></returns>
    private HyperLink GetFuggoKcsHyperlink(string nev, string fuggoKodtarId)
    {
        HyperLink link = new HyperLink();
        link.Text = nev;
        if (Command == CommandName.Modify)
        {
            link.NavigateUrl = " ";
            string javascript =
            JavaScripts.SetOnClientClick("KodtarFuggosegItemEdit.aspx",
            CommandName.Command + "=" + CommandName.Modify + "&" +
            QueryStringVars.Id + "=" + Request.QueryString.Get(QueryStringVars.Id) + "&" +
            ConsQueryStringEditTypeParamName + "=" + EnumEditItemyType.FUGGO + "&" +
            ConsQueryStringFuggoKodtarIdParamName + "=" + fuggoKodtarId,
            Defaults.PopupWidth, Defaults.PopupHeight, TableKodtarFuggosegMatrix.ClientID, EventArgumentConst.refresh);
            link.Attributes.Add("onclick", javascript);
        }
        return link;
    }
    /// <summary>
    /// Create hyperlink for edit
    /// </summary>
    /// <param name="nev"></param>
    /// <returns></returns>
    private HyperLink GetNincsHyperlink(string nev)
    {
        HyperLink link = new HyperLink();
        link.Text = nev;
        if (Command == CommandName.Modify)
        {
            link.NavigateUrl = " ";
            string javascript =
                JavaScripts.SetOnClientClick("KodtarFuggosegItemEdit.aspx",
                CommandName.Command + "=" + CommandName.Modify + "&" +
                QueryStringVars.Id + "=" + Request.QueryString.Get(QueryStringVars.Id) + "&" +
                ConsQueryStringEditTypeParamName + "=" + EnumEditItemyType.NINCS,
                Defaults.PopupWidth, Defaults.PopupHeight, TableKodtarFuggosegMatrix.ClientID, EventArgumentConst.refresh);
            link.Attributes.Add("onclick", javascript);
        }
        return link;
    }
    #endregion

    #region SERVICE
    /// <summary>
    /// L�tezik-e vez�rl� �s f�g� k�dcsoport p�ros
    /// </summary>
    /// <param name="vezerloKCSId"></param>
    /// <param name="fuggoKCSId"></param>
    /// <returns></returns>
    private bool IsFuggoKodtarExisting(string vezerloKCSId, string fuggoKCSId)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = vezerloKCSId;
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.equals;
        search.Fuggo_KodCsoport_Id.Value = fuggoKCSId;
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.equals;

        Result result = service.GetAll(execParam, search);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (result == null || result.Ds.Tables[0].Rows.Count < 1)
                return false;

            return true;
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
    }
    /// <summary>
    /// L�trehoz�s
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    private Result Insert(KRT_KodtarFuggoseg item, bool requireClose)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.Insert(execParam, item);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (requireClose)
            {
                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, string.Empty))
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
                else
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        return result;
    }
    /// <summary>
    /// M�dos�t�s
    /// </summary>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    private Result Update(string id, KRT_KodtarFuggoseg item)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        Result result = service.Update(execParam, item);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, string.Empty))
            {
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
            }
            else
            {
                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                JavaScripts.RegisterCloseWindowClientScript(Page, true);
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        return result;
    }
    /// <summary>
    /// Lek�rdez�s azonos�t� alapj�n
    /// </summary>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    private bool KodtarFuggosegGet(string id, out Result item)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Id.Value = id;
        search.Id.Operator = Query.Operators.equals;

        Result result = item = service.GetAllWithExtension(execParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }

        return true;
    }
    /// <summary>
    /// Lek�rdez�s k�dcsoport p�ros alapj�n
    /// </summary>
    /// <param name="vezerloId"></param>
    /// <param name="fuggoId"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private bool KodtarakGet(string vezerloId, string fuggoId, out Result result)
    {
        KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodTarakSearch search = new KRT_KodTarakSearch();
        search.KodCsoport_Id.Value = "'" + vezerloId + "','" + fuggoId + "'";
        search.KodCsoport_Id.Operator = Query.Operators.inner;
        search.OrderBy = ConstDefaultKodtarFuggosegOrderBy;

        result = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
    }
    #endregion
}
