<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="KozpontiIrattarForm.aspx.cs" Inherits="KozpontiIrattarForm" Title="Untitled Page" %>

<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KozpontiIrattarFormHeaderTitle %>" />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short" style="height: 24px">
                                <asp:Label ID="label7" runat="server" Text="Iratt�ri hely:"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="5" style="height: 24px">
                                <asp:TextBox ID="IrattariHelyTextBox" runat="server" Width="600px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short" nowrap="true">
                                <asp:Label ID="Label8" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label ID="label1" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" ReadOnly="true" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label12" runat="server" Text="Meg�rz�si m�d:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:ReadOnlyTextBox ID="IrattariJelReadOnlyTextBox" width="80" runat="server" ReadOnly="true" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label11" runat="server" Text="Meg�rz�si id�:"></asp:Label></td>
                            <td class="mrUrlapMezo" >
                                <uc8:ReadOnlyTextBox ID="MegorzesiIdoReadOnlyTextBox" width="80" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label5" runat="server" Text="Megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="5">
                                <uc8:ReadOnlyTextBox ID="MegnevezesReadOnlyTextBox" runat="server" Width="600px" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <%--BUG_8507--%>
                                <asp:Label ID="labelUserNev" runat="server" Text="<%$Forditas:labelUserNev|Fel�lvizsg�l�:%>"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="3">
                                <uc3:FelhasznaloTextBox ID="FelulvizsgaloFelhasznaloTextBox" runat="server" ReadOnly="true" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label2" runat="server" Text="Fel�lvizsg�lati id�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="FelulvizsgalatiIdoCalendarControl" runat="server" ReadOnly="true" Validate="false" />
                                </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                    
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
</asp:Content>

