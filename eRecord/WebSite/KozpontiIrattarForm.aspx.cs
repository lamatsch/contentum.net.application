using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class KozpontiIrattarForm : Contentum.eUtility.UI.PageBase
{    
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattar" + Command);
                break;
        }

        if (Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                if (!IsPostBack)
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                        LoadComponentsFromBusinessObject(erec_UgyUgyiratok);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
        }

        ////FunkcioGombsor.HideButtons();
        ////FunkcioGombsor.SzignalasVisible = true;
        ////FunkcioGombsor.TovabbitasVisible = true;
        ////FunkcioGombsor.Szkontro_inditasVisible = true;
        ////FunkcioGombsor.UgyUgyirat_lezarasaVisible = true;

        ////FunkcioGombsor.DisableButtons();

        //Command = Request.QueryString.Get(CommandName.Command);

        ////switch (Command)
        ////{
        ////    case CommandName.View:
        ////        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo" + Command);
        ////        break;
        ////    default:
        ////        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo" + Command);
        ////        break;
        ////}

        //if (Command == CommandName.View || Command == CommandName.Modify)
        //{
        //    String id = Request.QueryString.Get(QueryStringVars.Id);
        //    if (String.IsNullOrEmpty(id))
        //    {
        //        // nincs Id megadva:
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //    }
        //    else
        //    {
        //        /*
        //        KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
        //        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParam.Record_Id = id;

        //        Result result = service.Get(execParam);
        //        if (String.IsNullOrEmpty(result.ErrorCode))
        //        {
        //            KRT_Telepulesek krt_Telepulesek = (KRT_Telepulesek)result.Record;
        //            LoadComponentsFromBusinessObject(krt_Telepulesek);
        //        }
        //        else
        //        {
        //            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        //        }
        //         */
        //    }
        //}
        //else if (Command == CommandName.New)
        //{
        //    /*
        //    string orszagID = Request.QueryString.Get("OrszagID");
        //    if (!String.IsNullOrEmpty(orszagID))
        //    {
        //        OrszagokTextBox1.Id_HiddenField = orszagID;
        //        OrszagokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        //        OrszagokTextBox1.Enabled = false;
        //    }
        //    KodtarakDropDownListMegye.FillAndSetEmptyValue(kodcsoportMegye, FormHeader1.ErrorPanel);
        //    KodtarakDropDownListRegio.FillAndSetEmptyValue(kodcsoportRegio, FormHeader1.ErrorPanel);
        //     */
        //}
        //if (Command == CommandName.View)
        //{
        //    SetViewControls();
        //}
        //if (Command == CommandName.Modify)
        //{
        //    SetModifyControls();
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (IsPostBack)
        {
        }

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        IrattariHelyTextBox.Text = erec_UgyUgyiratok.IrattariHely;
        IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
        IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

        if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IraIrattariTetel_Id))
        {

            EREC_IraIrattariTetelekService erec_IraIrattariTetelekService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
            execparam.Record_Id = erec_UgyUgyiratok.IraIrattariTetel_Id;
            Result result = erec_IraIrattariTetelekService.Get(execparam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }
            EREC_IraIrattariTetelek erec_IraIrattariTetelek = (EREC_IraIrattariTetelek)result.Record;
            MegnevezesReadOnlyTextBox.Text = erec_IraIrattariTetelek.Nev;
            IrattariJelReadOnlyTextBox.Text = erec_IraIrattariTetelek.IrattariJel;
            MegorzesiIdoReadOnlyTextBox.Text = erec_IraIrattariTetelek.MegorzesiIdo;
        }
        
        FelulvizsgaloFelhasznaloTextBox.Id_HiddenField = erec_UgyUgyiratok.Base.Modosito_id;
        FelulvizsgaloFelhasznaloTextBox.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FelulvizsgalatiIdoCalendarControl.Text = erec_UgyUgyiratok.Base.ModositasIdo;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = erec_UgyUgyiratok.Base.Ver;
        
    }

    // form --> business object
    private EREC_UgyUgyiratok GetBusinessObjectFromComponents()
    {        
        EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
        erec_UgyUgyiratok.Updated.SetValueAll(false);
        erec_UgyUgyiratok.Base.Updated.SetValueAll(false);
        
        erec_UgyUgyiratok.IraIrattariTetel_Id = IraIrattariTetelTextBox1.Id_HiddenField;
        erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;
        
        erec_UgyUgyiratok.IrattariHely = IrattariHelyTextBox.Text;
        erec_UgyUgyiratok.Updated.IrattariHely = true;

        erec_UgyUgyiratok.Base.Ver = FormHeader1.Record_Ver;
        erec_UgyUgyiratok.Base.Updated.Ver = true;

        return erec_UgyUgyiratok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(IrattariHelyTextBox);
            compSelector.Add_ComponentOnClick(IrattariJelReadOnlyTextBox);
            compSelector.Add_ComponentOnClick(MegorzesiIdoReadOnlyTextBox);
            compSelector.Add_ComponentOnClick(MegnevezesReadOnlyTextBox);
            compSelector.Add_ComponentOnClick(FelulvizsgaloFelhasznaloTextBox);
            compSelector.Add_ComponentOnClick(FelulvizsgaloFelhasznaloTextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + Command))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                EREC_UgyUgyiratok erec_UgyUgyiratok = GetBusinessObjectFromComponents();

                                // T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;
                                
                                Result result = service.Update(execParam, erec_UgyUgyiratok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
            }
        }
    }

}
