using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class KozteruletTipusokLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Nincs funkci� ellen�rz�s Kov�cs Andris(AXIS) javaslat�ra
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_KozteruletTipusokService service = eAdminService.ServiceFactory.GetKRT_KozteruletTipusokService();
        KRT_KozteruletTipusokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_KozteruletTipusokSearch)Search.GetSearchObject(Page, new KRT_KozteruletTipusokSearch());
            TextBoxSearch.Text = "";
        }
        else
        {
            search = new KRT_KozteruletTipusokSearch();
            search.Nev.Value = SearchKey;
            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }
        search.OrderBy = "Nev";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(execParam, search);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }

}
