<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true"
    CodeFile="KuldKuldemenyekForm.aspx.cs"
    Inherits="KuldKuldemenyekForm"
    Title="Küldemények karbantartása" %>


<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="eRecordComponent/KuldKuldemenyFormTab.ascx" TagName="KuldemenyTab" TagPrefix="tp1" %>
<%@ Register Src="eRecordComponent/TovabbiBekuldokTab.ascx" TagName="TovabbiBekuldokTab" TagPrefix="tp2" %>
<%@ Register Src="eRecordComponent/MellekletekTab.ascx" TagName="MellekletekTab" TagPrefix="tp3" %>
<%@ Register Src="eRecordComponent/FeljegyzesekTab.ascx" TagName="FeljegyzesekTab" TagPrefix="tp4" %>
<%@ Register Src="eRecordComponent/CsatolmanyokTab.ascx" TagName="CsatolmanyokTab" TagPrefix="tp5" %>
<%@ Register Src="eRecordComponent/CsatolasokTab.ascx" TagName="CsatolasokTab" TagPrefix="tp6" %>
<%@ Register Src="eRecordComponent/JogosultakTab.ascx" TagName="JogosultakTab" TagPrefix="tp6" %>
<%@ Register Src="eRecordComponent/BontasTab.ascx" TagName="BontasTab" TagPrefix="tp7" %>
<%@ Register Src="eRecordComponent/TortenetTab.ascx" TagName="TortenetTab" TagPrefix="tp8" %>
<%@ Register Src="eRecordComponent/KapcsolatokTab.ascx" TagName="KapcsolatokTab" TagPrefix="tp9" %>
<%@ Register Src="eRecordComponent/FeladatokTab.ascx" TagName="FeladatokTab" TagPrefix="tp10" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        window.addEventListener('keydown', function (e) {
            if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
                if (e.target.nodeName == 'INPUT' && e.target.type == 'text') {
                    e.preventDefault(); return false;
                }
            }
        }, true);
    </script>

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KuldemenyekFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/json2.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/libs/jquery/jquery.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/esign_v2.js" />
            <asp:ScriptReference Path="~/JavaScripts/Microsec.js" />
        </Scripts>
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/MicroSignerCallbackService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:Panel ID="KuldKuldemenyFormPanel" runat="server">
                    <%-- OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged" --%>
                    <ajaxToolkit:TabContainer ID="KuldKuldemenyTabContainer"
                        runat="server"
                        Width="100%"
                        OnActiveTabChanged="KuldKuldemenyTabContainer_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged">

                        <%-- Küldemény --%>
                        <ajaxToolkit:TabPanel ID="TabKuldemenyPanel" runat="server" Width="100%" TabIndex="0">
                            <HeaderTemplate>
                                <asp:Label ID="TabKuldemenyPanelHeader" runat="server" Text="Küldemény"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp1:KuldemenyTab ID="KuldemenyTab1" runat="server"></tp1:KuldemenyTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- További beküldők --%>
                        <ajaxToolkit:TabPanel ID="TabTovabbiBekuldokPanel" runat="server" Enabled="true" TabIndex="1">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelTovabbiBekuldok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabTovabbiBekuldokPanelHeader" runat="server" Text="További beküldők"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp2:TovabbiBekuldokTab ID="TovabbiBekuldokTab1" runat="server"></tp2:TovabbiBekuldokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Mellékletek --%>
                        <ajaxToolkit:TabPanel ID="TabMellekletekPanel" runat="server" TabIndex="2">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelKuldemenyElemek" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabMellekletekPanelHeader" runat="server" Text="Küldemény elemek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp3:MellekletekTab ID="MellekletekTab1" runat="server"></tp3:MellekletekTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Csatolmányok --%>
                        <ajaxToolkit:TabPanel ID="TabCsatolmanyokPanel" runat="server" TabIndex="4">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelCsatolmanyok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabCsatolmanyokPanelHeader" runat="server" Text="Csatolmányok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp5:CsatolmanyokTab ID="CsatolmanyokTab1" runat="server"></tp5:CsatolmanyokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Jogosultak --%>
                        <ajaxToolkit:TabPanel ID="TabJogosultakPanel" runat="server" TabIndex="5">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabJogosultakPanelHeader" runat="server" Text="Jogosultak"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp6:JogosultakTab ID="JogosultakTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Bontás --%>
                        <ajaxToolkit:TabPanel ID="TabBontasPanel" runat="server" TabIndex="6">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelBontas" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabBontasPanelHeader" runat="server" Text="Megbontás"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp7:BontasTab ID="BontasTab1" runat="server"></tp7:BontasTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Történet --%>
                        <ajaxToolkit:TabPanel ID="TabTortenetPanel" runat="server" TabIndex="7">
                            <HeaderTemplate>
                                <asp:Label ID="TabTortenetPanelHeader" runat="server" Text="Történet"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp8:TortenetTab ID="TortenetTab1" runat="server"></tp8:TortenetTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Kapcsolat --%>
                        <%--						<ajaxToolkit:TabPanel ID="TabKapcsolatokPanel" runat="server" TabIndex="8">
							<HeaderTemplate>
								<asp:Label ID="TabKapcsolatokPanelHeader" runat="server" Text="Kapcsolat"></asp:Label>
							</HeaderTemplate>
							<ContentTemplate>
								<tp9:KapcsolatokTab ID="KapcsolatokTab1" runat="server"></tp9:KapcsolatokTab>
							</ContentTemplate>
						</ajaxToolkit:TabPanel>--%>

                        <ajaxToolkit:TabPanel ID="TabCsatolasPanel" runat="server" TabIndex="8">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelCsatolas" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="labelCsatolas" runat="server" Text="Kapcsolatok" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <%-- Kapcsolatok --%>
                                <tp6:CsatolasokTab ID="CsatolasokTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <%-- Feladatok --%>
                        <ajaxToolkit:TabPanel ID="TabFeladatokPanel" runat="server" TabIndex="9">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="updatePanelFeladatokHeader" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabFeladatokPanelHeader" runat="server" Text="Feladatok/Kezelési feljegyzések"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp10:FeladatokTab ID="FeladatokTab1" runat="server"></tp10:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
