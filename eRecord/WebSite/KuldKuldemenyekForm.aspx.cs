using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class KuldKuldemenyekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Startup = String.Empty;

    private Type _type = typeof(EREC_KuldKuldemenyek);

    EREC_KuldKuldemenyek obj_kuldemeny = null;

    Contentum.eUtility.PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, KuldKuldemenyTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(KuldKuldemenyTabContainer);

        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";

        Command = Request.QueryString.Get(CommandName.Command);
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        string FunkcioBase = null;
        if (Startup == Constants.Startup.Szamla)
        {
            FunkcioBase = "Szamla";
        }
        else
        {
            FunkcioBase = "Kuldemeny";
        }

        if (Command == CommandName.DesignView)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
        }
        else
        {
            // ha Modify m�dban pr�b�ljuk megnyitni, de csak View joga van, �tir�ny�tjuk, ha egyik sincs, hiba)
            if (Command == CommandName.Modify
                && !FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Modify)
                && FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.View))
            {
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
            }
            else
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioBase + Command);
            }
        }

        KuldemenyTab1.FormHeader = FormHeader1;

        // Template kezel�s csak CommandName.New eset�n:
        if (Command == CommandName.New)
        {
            if (Startup == Constants.Startup.Szamla)
            {
                FormHeader1.TemplateObjectType = typeof(SzamlaErkeztetesFormTemplateObject);
            }
            else
            {
                FormHeader1.TemplateObjectType =_type;
            }
            KuldemenyTab1.FormHeader.FormTemplateLoader1_Visibility = true;
        }

        SetEnabledAllTabsByFunctionRights();

        if (Command == CommandName.New)
        { 
            // Ha uj rekorodot vesznek fel, akkor csak az elso tab aktiv, addig amig nem nyomnak mentest!
            SetEnabledAllTabs(false);
            KuldKuldemenyTabContainer.Tabs[TabKuldemenyPanel.TabIndex].Enabled = true;

            if (Startup == Constants.Startup.Szamla)
            {
                FormHeader1.HeaderTitle = Resources.Form.KuldemenyekFormHeaderTitle_Szamla;
            }
            else
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.KuldemenyErkeztetesFormHeaderTitle;
            }
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                if (!IsPostBack)
                {
                    CheckRights(id);
                    DisableTabsByMissingObjectRights(id);
                }
            }
        }

        KuldemenyTab1.Active = false;
        KuldemenyTab1.ParentForm = Constants.ParentForms.Kuldemeny;

        CsatolmanyokTab1.Active = false;
        CsatolmanyokTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        CsatolmanyokTab1.TabHeader = TabCsatolmanyokPanelHeader;

        TovabbiBekuldokTab1.Active = false;
        TovabbiBekuldokTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        TovabbiBekuldokTab1.TabHeader = TabTovabbiBekuldokPanelHeader;

        MellekletekTab1.Active = false;
        MellekletekTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        MellekletekTab1.TabHeader = TabMellekletekPanelHeader;

        JogosultakTab1.Active = false;
        JogosultakTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        JogosultakTab1.TabHeader = TabJogosultakPanelHeader;

        BontasTab1.Active = false;
        BontasTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        BontasTab1.TabHeader = TabBontasPanelHeader;

        TortenetTab1.Active = false;
        TortenetTab1.ParentForm = Constants.ParentForms.Kuldemeny;

        //KapcsolatokTab1.Active = false;
        //KapcsolatokTab1.ParentForm = Constants.ParentForms.Kuldemeny;

        CsatolasokTab1.Active = false;
        CsatolasokTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        CsatolasokTab1.TabHeader = labelCsatolas;

        FeladatokTab1.Active = false;
        FeladatokTab1.ParentForm = Constants.ParentForms.Kuldemeny;
        FeladatokTab1.TabHeader = TabFeladatokPanelHeader;

        //TODO: Ideiglenesen letiltva:
        //JogosultakTab1.Visible = false;
        //TortenetTab1.Visible = false;
        ////KapcsolatokTab1.Visible = false;
        //FeladatokTab1.Visible = false;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();
        
        if (!IsPostBack)
        {
            // Default panel beallitasa
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            //if (!String.IsNullOrEmpty(selectedTab) && selectedTab == "Mellekletek")
            //{
            //    KuldKuldemenyTabContainer.ActiveTab = TabMellekletekPanel;
            //}
            //else
            //{
            //    KuldKuldemenyTabContainer.ActiveTab = TabKuldemenyPanel;
            //}
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in KuldKuldemenyTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        KuldKuldemenyTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    KuldKuldemenyTabContainer.ActiveTab = TabKuldemenyPanel;
                }
            }
            else
            {
                KuldKuldemenyTabContainer.ActiveTab = TabKuldemenyPanel;
            }
            ActiveTabRefresh(KuldKuldemenyTabContainer);
        }
        
        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //�rkeztet�s eset�n bez�r gomb elt�ntet�se
        if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_Close.Visible = false;
        }
        
    }

    private void CheckRights(string id)
    {
        char jogszint = Command == CommandName.View ? 'O' : 'I';
        Contentum.eRecord.Service.EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
        ep.Record_Id = id;
        Result result = service.GetWithRightCheck(ep, jogszint);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            KuldKuldemenyFormPanel.Visible = false;
            return;
        }

        obj_kuldemeny = (EREC_KuldKuldemenyek)result.Record;

        #region Kimen� k�ldem�nyn�l a f�l�sleges f�lek elt�vol�t�sa

        if (obj_kuldemeny.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            TovabbiBekuldokTab1.Visible = false;
            //MellekletekTab1.Visible = false;
            BontasTab1.Visible = false;

            TabTovabbiBekuldokPanel.Enabled = false;
            //TabMellekletekPanel.Enabled = false;
            TabBontasPanel.Enabled = false;
        }

        #endregion

        #region �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
        if (Command == CommandName.Modify || Command == CommandName.View)
        {
            bool bKuldemenyModosithato = false;
            String linkURL = String.Empty;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            #region K�ldem�ny m�dos�that�-e
           // CR3322 Sz�mla m�dos�t�s ikon ne legyen akt�v, ha v�gleges�tett a sz�mla
           // A K�ldem�ny-vizsg�lat nem volt j�, pir-�llapotot nem n�zett
            Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(id, Page, FormHeader1.ErrorPanel);
           // Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            ErrorDetails errorDetail = null;
            bKuldemenyModosithato = Kuldemenyek.Modosithato(statusz, execParam, out errorDetail);
            #endregion K�ldem�ny m�dos�that�-e

            // ha az orzo nem a futtato felhasznalo, akkor nem modosithatja a recordot, igy View uzemmodba valtunk!
            // TODO: meg megkell csinalni, hogy a felhasznaloiprofilban benne legyen a felhasznalo egyszemelyes csoportja is!
            // + erre kell ellenorizni az orzot!
            if (Command == CommandName.Modify)
            {
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.FelhasznaloId(Page))
                {
                    Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                }

                if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva)
                {
                    Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                }

                if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Lezarva)
                {
                    Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                }

                if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Sztorno)
                {
                    Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                }

                // Ha lockolva van a record, akkor is View uzemmodba valtunk!
                if (!String.IsNullOrEmpty(statusz.Zarolo_id) && statusz.Zarolo_id != FelhasznaloProfil.FelhasznaloId(Page))
                {
                    Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                }
            }
            else if (Command == CommandName.View)
            {
                string FunkcioBase = null;
                if (Startup == Constants.Startup.Szamla)
                {
                    FunkcioBase = "Szamla";
                }
                else
                {
                    FunkcioBase = "Kuldemeny";
                }

                if (bKuldemenyModosithato == true)
                {
                    String funkcioJog = FunkcioBase + CommandName.Modify;
                    //jogosults�g ellen�rz�s
                    if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
                    {
                        // Original QueryString without Command and Id and SelectedTab
                        string qsOriginalParts = QueryStringVars.Id + "=" + id + "&"
                            + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                        linkURL = "KuldKuldemenyekForm.aspx?"
                                                + QueryStringVars.Command + "=" + CommandName.Modify
                                                + "&" + qsOriginalParts
                                                + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + KuldKuldemenyTabContainer.ClientID + "') + '";
                    }
                }

            }
            // ModifyLink be�ll�t�sa
            JavaScripts.RegisterSetLinkOnForm(Page, (String.IsNullOrEmpty(linkURL) ? false : true), linkURL, FormHeader1.ModifyLink);
        }
        #endregion �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
    }

    #region Forms Tab

    protected void KuldKuldemenyTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        KuldemenyTab1.Active = false;
        CsatolmanyokTab1.Active = false;
        TovabbiBekuldokTab1.Active = false;
        MellekletekTab1.Active = false;
        JogosultakTab1.Active = false;
        BontasTab1.Active = false;
        TortenetTab1.Active = false;
        //KapcsolatokTab1.Active = false;
        CsatolasokTab1.Active = false;
        FeladatokTab1.Active = false;

        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabKuldemenyPanel))
        {
            KuldemenyTab1.Active = true;

            // a m�r esetleg lek�rt k�ldem�ny objektum �tad�sa (New-n�l null)
            KuldemenyTab1.obj_kuldemeny = this.obj_kuldemeny;

            KuldemenyTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabCsatolmanyokPanel))
        {
            CsatolmanyokTab1.Active = true;
            CsatolmanyokTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabTovabbiBekuldokPanel))
        {
            TovabbiBekuldokTab1.Active = true;
            TovabbiBekuldokTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabMellekletekPanel))
        {
            MellekletekTab1.Active = true;
            MellekletekTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabJogosultakPanel))
        {
            JogosultakTab1.Active = true;
            JogosultakTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabBontasPanel))
        {
            BontasTab1.Active = true;
            BontasTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabTortenetPanel))
        {
            TortenetTab1.Active = true;
            TortenetTab1.ReLoadTab();
        }
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabFeladatokPanel))
        {
            FeladatokTab1.Active = true;
            FeladatokTab1.ReLoadTab();
        }
        //if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabKapcsolatokPanel))
        //{
        //    KapcsolatokTab1.Active = true;
        //    KapcsolatokTab1.ReLoadTab();
        //}
        if (KuldKuldemenyTabContainer.ActiveTab.Equals(TabCsatolasPanel))
        {
            CsatolasokTab1.Active = true;
            CsatolasokTab1.ReLoadTab();
        }
    }
  
    #endregion    
    
    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void SetEnabledAllTabs(Boolean value)
    {
        TabKuldemenyPanel.Enabled = value;
        TabCsatolmanyokPanel.Enabled = value;
        TabTovabbiBekuldokPanel.Enabled = value;
        TabMellekletekPanel.Enabled = value;
        TabJogosultakPanel.Enabled = value;
        TabBontasPanel.Enabled = value;
        TabTortenetPanel.Enabled = value;
        TabFeladatokPanel.Enabled = value;
        //TabKapcsolatokPanel.Enabled = value;
        TabCsatolasPanel.Enabled = value;
    }

    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni!
        TabKuldemenyPanel.Enabled = true;
        TabCsatolmanyokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.List);
        TabTovabbiBekuldokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.List);
        TabMellekletekPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyMelleklet" + CommandName.List);
        TabBontasPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyBontas" + CommandName.List);
        TabTortenetPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTortenetList");

        // TODO: ideiglenesen letiltva:
            TabJogosultakPanel.Enabled = true;
        TabFeladatokPanel.Enabled = false;
        //TabKapcsolatokPanel.Enabled = false;
        TabCsatolasPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolas" + CommandName.List);

        //TabJogosultakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyJogosult" + CommandName.List);
        //TabTortenetPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTortenet" + CommandName.List);
        TabFeladatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyFeladat" + CommandName.List);
        //TabKapcsolatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyKapcsolat" + CommandName.List);
    }


    // ellen�rz�s �s sz�ks�g eset�n tabf�l tilt�s az aktu�lis irat alapj�n
    private void DisableTabsByMissingObjectRights(string id)
    {
        if (!Kuldemenyek.CheckRights_Csatolmanyok(Page, id, false))
        {
            TabCsatolmanyokPanel.Enabled = false;
        }
    }

    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            SetEnabledAllTabs(false);
            KuldKuldemenyTabContainer.Tabs[TabKuldemenyPanel.TabIndex].Enabled = true;
            pageView.CompSelector.Add_ComponentOnClick(TabKuldemenyPanel);
            pageView.CompSelector.Add_ComponentOnClick(KuldemenyTab1);
            pageView.CompSelector.Add_ComponentOnClick(TabTovabbiBekuldokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabMellekletekPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolmanyokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabJogosultakPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabBontasPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTortenetPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolasPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabFeladatokPanel);
            FormFooter1.SaveEnabled = false;
        }
    }


    
}
