<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"
                       AutoEventWireup="true"
                       CodeFile="KuldKuldemenyekList.aspx.cs"
                       Inherits="KuldKuldemenyekList" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/KuldemenyekList.ascx" TagName="KuldemenyekList" TagPrefix="kl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
	</asp:ScriptManager>
	<uc2:ListHeader ID="ListHeader1" runat="server" />
	<kl:KuldemenyekList ID="KuldemenyekList" runat="server" />

</asp:Content>
