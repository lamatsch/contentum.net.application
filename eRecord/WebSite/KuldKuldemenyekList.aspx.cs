using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class KuldKuldemenyekList : Contentum.eUtility.UI.PageBase
{
    
    protected void Page_Init(object sender, EventArgs e)
    {
        KuldemenyekList.ScriptManager = ScriptManager1;
        KuldemenyekList.ListHeader = ListHeader1;

        KuldemenyekList.InitPage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        KuldemenyekList.LoadPage();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        KuldemenyekList.PreRenderPage();
    }

    
}
