using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class KuldKuldemenyekLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterByObjektumKapcsolat;
    private string ObjektumId;

    /// <summary>
    /// Ha true, mindenk�ppen csak az iktathat� k�ldem�nyeket fogjuk hozni
    /// </summary>
    private bool filterForIktathatoak = false;

    private string PostazasIranya = "";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KuldemenyList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        // Sz�r�si felt�tel van-e?
        String filterParam = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterParam == Constants.IktathatoKuldemenyek)
        {
            filterForIktathatoak = true;

            // m�dos�tjuk a fejl�c c�m�t is:
            LovListHeader1.HeaderTitle = Resources.LovList.KuldKuldemenyekLovListHeaderTitle_Filtered_Iktathatok;
        }

        string filterPostazasIranya = Request.QueryString.Get(QueryStringVars.PostazasIranya);
        if (!String.IsNullOrEmpty(filterPostazasIranya))
        {
            PostazasIranya = filterPostazasIranya;
        }

        filterByObjektumKapcsolat = Request.QueryString.Get(QueryStringVars.ObjektumKapcsolatTipus);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);

    }
    protected void Page_Load(object sender, EventArgs e)
    {        

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "KuldKuldemenyekForm.aspx", "", GridViewSelectedId.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            //IraIktatoKonyvekDropDownList1.FillAndSetEmptyValue(false, false, Constants.IktatoErkezteto.Erkezteto, LovListHeader1.ErrorPanel);

            // �rkeztet�k�nyvek megk�l�nb�ztet� jelz�s alapj�n val� felt�lt�se:
            IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                , Erkeztetes_Ev_EvIntervallum_SearchFormControl.EvTol, Erkeztetes_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, "", LovListHeader1.ErrorPanel);

            //FillGridViewSearchResult(false);
        }

        // f�kusz a "Vonalk�d"-ra
        JavaScripts.SetFocus(VonalkodTextBox.TextBox);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        //if (IsPostBack)
        //{
        //    String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
        //    RefreshOnClientClicks(selectedRowId);
        //}
    }

    //private void RefreshOnClientClicks(string id)
    //{
    //    if (!String.IsNullOrEmpty(id))
    //    {

    //        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
    //            "KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View
    //            + "&" + QueryStringVars.Id + "=" + id
    //        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, null);
    //    }
    //}

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        EREC_KuldKuldemenyekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject(Page, new EREC_KuldKuldemenyekSearch(true));
        }
        else
        {
            search = new EREC_KuldKuldemenyekSearch(true);
            //if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            //{
            //    search.IraIktatokonyv_Id.Value = IraIktatoKonyvekDropDownList1.SelectedValue;
            //    search.IraIktatokonyv_Id.Operator = Query.Operators.equals;
            //}

            if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                // Megk�l�nb�ztet� jelz�sre sz�r�nk, nem az id-ra
                IraIktatoKonyvekDropDownList1.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
            }

            Erkeztetes_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
                search.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            /// �rkeztet�sz�m be�ll�t�sa
            ErkeztetoSzam_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(search.Erkezteto_Szam);

            if (!String.IsNullOrEmpty(Targy_TextBox.Text))
            {
                search.Targy.Value = Targy_TextBox.Text;
                search.Targy.Operator = Search.GetOperatorByLikeCharater(Targy_TextBox.Text);
            }

            if (!string.IsNullOrEmpty(VonalkodTextBox.Text))
            {
                search.BarCode.Value = VonalkodTextBox.Text;
                search.BarCode.Operator = Query.Operators.equals;
            }
        }

        // ha meg van adva sz�r�s, csak az iktathat� k�ldem�nyek fognak j�nni
        // mindenk�pp van sz�r�s, m�g ha a r�szletes keres�sben ezt m�shogy �ll�tott�k is be
        if (filterForIktathatoak == true)
        {            
            Kuldemenyek.SetSearchObjectTo_Iktathatok(search);
            search.FelhasznaloCsoport_Id_Orzo.Value = FelhasznaloProfil.FelhasznaloId(Page);
            search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(PostazasIranya))
        {
            bool bInner = false;
            if (PostazasIranya.Split(',').Length > 1)
            {
                PostazasIranya = "'" + PostazasIranya.Replace(",", "','") + ",";
                bInner = true;
            }
            search.PostazasIranya.Value = PostazasIranya;
            search.PostazasIranya.Operator = (bInner ?  Query.Operators.inner : Query.Operators.equals);
        }

        if (!String.IsNullOrEmpty(filterByObjektumKapcsolat) && !String.IsNullOrEmpty(ObjektumId))
        {
            switch (filterByObjektumKapcsolat)
            {
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Egyeb:
                    search.Extended_EREC_KuldKuldemenyekSearch.Id.Value = ObjektumId;
                    search.Extended_EREC_KuldKuldemenyekSearch.Id.Operator = Query.Operators.notequals;
                    Kuldemenyek.SetCsatolhatoSearchObject(search);
                    break;
            }
        }

        // Rendez�s:
        search.OrderBy = "Erkezteto_Szam";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search,true);

        // C�mzett �tv�tele az �rkeztet�sz�m hely�re, ha kimen� k�ldem�ny
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            DataTable table = result.Ds.Tables[0];
            DataRow[] rows_kimeno = table.Select("PostazasIranya='" + KodTarak.POSTAZAS_IRANYA.Kimeno + "'");
            if (rows_kimeno != null)
            {
                foreach (DataRow row in rows_kimeno)
                {
                    if (row.Table.Columns.Contains("FullErkeztetoSzam") && row.Table.Columns.Contains("NevSTR_Bekuldo"))
                    {
                        row["FullErkeztetoSzam"] = String.Format(Resources.List.UI_FormatString_Kuldemeny_Cimzett
                            , row["NevSTR_Bekuldo"].ToString());
                    }
                }
            }
        }

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
