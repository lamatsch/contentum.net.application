﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class KuldKuldemenyekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iratPeldanyId = String.Empty;
    private string iratId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {


        iratPeldanyId = Request.QueryString.Get("IratPeldanyId");

        if (String.IsNullOrEmpty(iratPeldanyId))
        {
            Response.Write("nincs is ilyen haver!");
        }
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.Id.Value = iratPeldanyId;
        erec_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_f()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = iratPeldanyId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = false;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_d()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.IraIrat_Id.Value = iratId;
        erec_CsatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_e()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.Obj_Id.Value = iratPeldanyId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.ObjTip_Id.Value = "B29081B9-3106-4DF8-AB1F-D2008DCED7AA";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();

            //Response.Write(proba);
        }
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;



        if (rpis != null)
        {
            if (rpis.Count > 0)

            {
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                //execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

                //  iratId = result.SqlCommand.GetParamValue("@Obj_Id");

                iratId = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

                execParam.Fake = true;

                result = service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);




                EREC_HataridosFeladatokService f_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponents_f();

                Result f_result = f_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);



                EREC_CsatolmanyokService d_service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = GetSearchObjectFromComponents_d();

                Result d_result = d_service.GetAllWithExtension(execParam, erec_CsatolmanyokSearch);



                Contentum.eAdmin.Service.KRT_EsemenyekService e_service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                KRT_EsemenyekSearch krt_EsemenyekSearch = GetSearchObjectFromComponents_e();

                Result e_result = e_service.GetAllWithExtension(execParam, krt_EsemenyekSearch);




                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;

                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;

                        case "Where_Szamlak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Szamlak"));
                            break;


                        case "Where_KuldKuldemenyek_Csatolt":
                            
                                ReportParameters[i].Values.Add("EREC_Csatolmanyok.ErvKezd <= getdate() and EREC_Csatolmanyok.ErvVege >= getdate() and EREC_Csatolmanyok.IraIrat_id = '"+ iratId     +"'");
                            


                            break;

                        case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;

                        case "Where_Felj":
                            if (f_result.Ds.Tables[0].Rows.Count > 0)
                            {
                                ReportParameters[i].Values.Add("EREC_HataridosFeladatok.Obj_id = '"+iratPeldanyId+"'");

                            }

                            break;

                        case "Dokumentum_Id":
                            if (d_result.Ds.Tables[0].Rows.Count > 0)
                            {
                                // ReportParameters[i].Values.Add(d_result.SqlCommand.GetParamValue("@DokumentumId"));
                                ReportParameters[i].Values.Add(d_result.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString());

                            }

                            break;





                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":

                           
                                ReportParameters[i].Values.Add(FelhasznaloProfil.LoginUserId(Page));
                            

                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;



                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;


                        case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;

                        case "IratPeldanyId":

                            ReportParameters[i].Values.Add(iratPeldanyId);



                            break;

                        case "IratId":
                            // if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            // {
                            ReportParameters[i].Values.Add(iratId);

                            // }

                            break;

                        case "Where_Esemeny":
                            ReportParameters[i].Values.Add("KRT_Esemenyek.Funkcio_Id not in ('d5fd9dc2-19f9-4d3d-971f-1053fdf2013d','ae131060-8552-42c7-97a0-59526f88c2f5','ff760ce5-be6e-4c60-b373-75d2817c9998','56cc5b77-bf80-407f-a27a-ddad1f504832','c948a74c-1613-4bf6-9b29-38b7cefbe84b','e2422e74-9465-4910-a696-10f2044f0136','a5bdc3bd-58ec-42c8-9aba-0011ec48d9c0','8d84174e-671c-49f1-a18c-a4db97ad0e16','3d4d0a42-6a71-4810-873b-70d3ff27b403','2b8e71d6-278c-4ab0-b485-623b1c2c585c') and (KRT_Esemenyek.ErvKezd <= getdate()) and (KRT_Esemenyek.ErvVege >= getdate())");
                            break;



                    }
                }




            }

        }
        return ReportParameters;
    }
}

