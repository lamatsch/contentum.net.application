﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class KuldKuldemenyekPrintForm : Contentum.eUtility.ReportPageBase
{
    private string Id = String.Empty;
	private string kuldemenyId = String.Empty;
	private string tipus = String.Empty;
	
    protected void Page_Init(object sender, EventArgs e)
    {
        kuldemenyId = Request.QueryString.Get("KuldemenyID");
		Id = kuldemenyId;
		//tipus = Request.QueryString.Get("tipus");

        if (String.IsNullOrEmpty(kuldemenyId))
        {
            // nincs Id megadva:
        }
    }
	
	    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.Id.Value = Id;
        erec_KuldKuldemenyekSearch.Id.Operator = Query.Operators.equals;

        return erec_KuldKuldemenyekSearch;
    }

    private EREC_MellekletekSearch GetSearchObjectFromComponents_m()
    {
        EREC_MellekletekSearch erec_MellekletekSearch = new EREC_MellekletekSearch();

        erec_MellekletekSearch.KuldKuldemeny_Id.Value = Id;
        erec_MellekletekSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        return erec_MellekletekSearch;
    }

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponenets_d()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.KuldKuldemeny_Id.Value = Id;
        erec_CsatolmanyokSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponenets_f()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = Id;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = false;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_e()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.Obj_Id.Value = Id;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.ObjTip_Id.Value = "265E5281-EF84-4AB2-BD54-5847CC4DA106";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        InitReport(ReportViewer1, Rendszerparameterek.GetBoolean(this, Rendszerparameterek.SSRS_AUTO_PDF_RENDER), GetReportFileName("küldemény kísérő"), "");
    }
	
	
	protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        
         
        if (rpis != null)
        {
            if (rpis.Count > 0)
				
            {
				EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
				
			 	execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_KuldKuldemenyekSearch);
				
								
				ReportParameters = new ReportParameter[rpis.Count];
				
				//EREC_CsatolmanyokService d_service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
    //            EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = GetSearchObjectFromComponenets_d();

    //            Result d_result = d_service.GetAllWithExtension(execParam, erec_CsatolmanyokSearch);
				         
				
				//EREC_MellekletekService m_service = eRecordService.ServiceFactory.GetEREC_MellekletekService();
    //            EREC_MellekletekSearch erec_MellekletekSearch = GetSearchObjectFromComponents_m();

    //            Result m_result = m_service.GetAllWithExtension(execParam, erec_MellekletekSearch);
               
			   
			 //   EREC_HataridosFeladatokService f_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
    //            EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponenets_f();

    //            Result f_result = f_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);
				
				//Contentum.eAdmin.Service.KRT_EsemenyekService e_service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
    //            KRT_EsemenyekSearch krt_EsemenyekSearch = GetSearchObjectFromComponents_e();

    //            Result e_result = e_service.GetAllWithExtension(execParam, krt_EsemenyekSearch);

                for (int i = 0; i < rpis.Count; i++)
                {
                    
                     
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);
					
					

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                           if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            { 
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
							
							
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add("EREC_KuldKuldemenyek.Id = '" + kuldemenyId + "' and (EREC_KuldKuldemenyek.ErvKezd <= getdate()) and (EREC_KuldKuldemenyek.ErvVege >= getdate())");
                            break;
							
											
						case "Where_Felj":
						   
							{
							 
							 ReportParameters[i].Values.Add(" EREC_IraKezFeljegyzesek.IraIrat_Id = '" + kuldemenyId + "' and (EREC_IraKezFeljegyzesek.ErvKezd <= getdate()) and (EREC_IraKezFeljegyzesek.ErvVege >= getdate())");
							 
							}
                            
                            break;
							
						case "Where_Es":
                            {							

                                    ReportParameters[i].Values.Add("KRT_Esemenyek.Obj_Id = '"+kuldemenyId +"' and (KRT_Esemenyek.ObjTip_Id = '265E5281-EF84-4AB2-BD54-5847CC4DA106') and (KRT_Esemenyek.ErvKezd <= getdate()) and (KRT_Esemenyek.ErvVege >= getdate()) ");
							 
							}
						    
                            
                            break;
							
						//case "IraIrat_Id":
                            
							
						//	if (m_result.Ds.Tables[0].Rows.Count > 0 )
						//	{
							 
						//	 ReportParameters[i].Values.Add(m_result.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"].ToString());
							 
						//	}
      //                      break;
							
							
						case "Where_KuldKuldemenyek_Csatolt":
                            {
                                ReportParameters[i].Values.Add("EREC_Csatolmanyok.KuldKuldemeny_Id = '"+ kuldemenyId+ "' and (EREC_Csatolmanyok.ErvKezd <= getdate() ) and (EREC_Csatolmanyok.ErvVege >= getdate())");
                            }
							
							
                            break;

                        case "Where_Mellekletek":
                            {
                                ReportParameters[i].Values.Add("EREC_Mellekletek.KuldKuldemeny_Id = '"+ kuldemenyId + "'and (EREC_Mellekletek.ErvKezd <= getdate()) and (EREC_Mellekletek.ErvVege >= getdate())");
                            }


                            break;

                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                         case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                          //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
							
						
						
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                               ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        
							
						case "KuldemenyId":
                            ReportParameters[i].Values.Add(Id);
                            break;
							
										
                    }
				}
				
                
				
				
				}
            
        }
        return ReportParameters;
    }
}
	
	