<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="KuldKuldemenyekSearch.aspx.cs" Inherits="KuldemenyekSearch" Title="K�ldem�nyek keres�se" EnableEventValidation="false" %>

<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc7" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="Ervenyesseg_SearchCalendarControl" TagPrefix="escc" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="tsz" %>
<%@ Register Src="Component/ErkezesDatumaControl.ascx" TagName="ErkezesDatumaControl" TagPrefix="erk" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="prt" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="csp" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="c" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>

<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/DatumIdoIntervallum_SearchCalendarControl.ascx" TagName="DatumIdoIntervallum_SearchCalendarControl" TagPrefix="uc6" %>
<%--<%@ Register Src="eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
--%>
<%@ Register Src="eRecordComponent/SzamlaAdatokPanel.ascx" TagName="SzamlaAdatokPanel" TagPrefix="szap" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label1" runat="server" CssClass="mrUrlapCaption_shortest" Text="�v:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:EvIntervallum_SearchFormControl ID="Erkeztetes_Ev_EvIntervallum_SearchFormControl"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Erkeztetokonyvek_Label" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="�rkeztet�k�nyvek:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc4:IraIktatoKonyvekDropDownList ID="Erkeztetokonyvek_DropDownList" EvIntervallum_SearchFormControlId="Erkeztetes_Ev_EvIntervallum_SearchFormControl"
                                        runat="server" IsMultiSearchMode="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Erkeztetoszam_Label" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="�rkeztet�si azonos�t�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc7:SzamIntervallum_SearchFormControl ID="ErkeztetoSzam_SzamIntervallum_SearchFormControl"
                                        runat="server" />
                                </td>
                                <%--  <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="ErkezesDatuma_Label" runat="server" Text="�rkeztet�s id�pontja:" CssClass="mrUrlapCaption_shortest"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <escc:Ervenyesseg_SearchCalendarControl ID="ErkeztetesIdeje_DatumIntervallum_SearchCalendarControl1"
                                        runat="server" AktualisEv_Visible="true" AktualisEv_Enable="true" ValidateDateFormat="true"></escc:Ervenyesseg_SearchCalendarControl>
                                </td>--%>

                                <%-- bernat.laszlo added --%>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Beerkezes_Idopontja_Label" runat="server" Text="Be�rkez�s id�pontja:" CssClass="mrUrlapCaption_shortest"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <escc:Ervenyesseg_SearchCalendarControl ID="Beerkezes_Idopontja_SearchCalendarControl"
                                        runat="server" AktualisEv_Visible="true" AktualisEv_Enable="true" ValidateDateFormat="true"></escc:Ervenyesseg_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="labelErkezteto" runat="server" Text="�rkeztet�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloTextBox ID="FelhasznaloTextBox_Erkezteto" runat="server" />
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Bontas_Idopontja_Label" runat="server" Text="Bont�s id�pontja:" CssClass="mrUrlapCaption_shortest"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <escc:Ervenyesseg_SearchCalendarControl ID="Bontas_Idopontja_SearchCalendarControl"
                                        runat="server" AktualisEv_Visible="true" AktualisEv_Enable="true" ValidateDateFormat="true"></escc:Ervenyesseg_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Kuldemeny_Bontoja_Label" runat="server" Text="K�ldem�ny bont�ja:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloTextBox ID="Kuldemeny_Bontoja_FelhasznaloTextBox" runat="server" />
                                </td>
                                <td class="mrUrlapCaption_shortest">&nbsp;</td>
                                <td class="mrUrlapMezo">&nbsp;</td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">�rkeztet�s id�pontja:</td>
                                <td class="mrUrlapMezo">
                                    <uc6:DatumIdoIntervallum_SearchCalendarControl ID="DatumIdoIntervallum_SearchCalendarControl_ErkeztetesDatuma" runat="server" />
                                </td>
                                <td class="mrUrlapCaption_shortest">&nbsp;</td>
                                <td class="mrUrlapMezo">&nbsp;</td>
                            </tr>
                            <%-- bernat.laszlo eddig --%>
                            <%-- <tr class="urlapElvalasztoSor"></tr>--%>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="VomalkodLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="Vonalk�d:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc10:VonalKodTextBox ID="VonalkodTextBox" runat="server"
                                        CssClass="mrUrlapInput" Validate="false" />
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="EredetiCimzettLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="C�mzett neve:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <csp:CsoportTextBox ID="EredetiCimzett_CsoportTextBox" runat="server"
                                        SearchMode="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="BekuldoNeveLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="K�ld�/felad� neve:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <prt:PartnerTextBox ID="BekuldoNeveTextBox" runat="server"
                                        CssClass="mrUrlapInputFTS" SearchMode="true" Width="60" />
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="BekuldoCimeLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="K�ld�/felad� c�me:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <c:CimekTextBox ID="CimId_BekuldoCimeTextBox" runat="server"
                                        CssClass="mrUrlapInputComboBox" Enabled="true" SearchMode="true" Width="120" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="OrzoLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="Irat helye:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloTextBox ID="OrzoTextBox" runat="server"
                                        NewImageButtonVisible="false" SearchMode="true" Validate="false" />
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="FelelosLabel" runat="server" Text="Kezel�:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <csp:CsoportTextBox ID="Felelos_CsoportTextBox" runat="server" SearchMode="true"></csp:CsoportTextBox>
                                </td>
                            </tr>
                            <%--<tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_shortest">
                                        &nbsp;</td>
                                    <td class="mrUrlapMezo">
                                        &nbsp;</td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="SzervezetnelJartLabel" runat="server" Text="A k�ldem�ny a<br>szervezetn�l m�r j�rt:"
                                            CssClass="mrUrlapCaption_shortest" Enabled="False" Visible="False"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <csp:CsoportTextBox ID="SzervezetnelJartTextBox" runat="server" SearchMode="true"
                                            Enabled="false" Validate="false" Visible="false"></csp:CsoportTextBox>
                                    </td>
                                </tr>--%>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest" align="right">
                                    <asp:Label ID="ErkezesModjaLabel" runat="server" Text="Be�rkez�s m�dja:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" nowrap>
                                    <ktddl:KodtarakDropDownList ID="ErkezesModjaDropDownList" runat="server" CssClass="mrUrlapInputComboBox"></ktddl:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label runat="server" Text="<%$Resources:Form,KuldemenyKulsoAzonosito %>" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                    <%--                                        <asp:Label ID="TovabbitoSzervezetLabel" runat="server" Text="Tov�bb�t� szervezet:"
                                            CssClass="mrUrlapCaption_shortest"></asp:Label>--%>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="TextBox_KulsoAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    <%--                                        <ktddl:KodtarakDropDownList ID="TovabbitoSzervezetDropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                                        </ktddl:KodtarakDropDownList>--%>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest" id="td_labelKuldoIktatoszama" runat="server">
                                    <asp:Label ID="HivatkozasiSzamLabel" runat="server" Text="Hivatkoz�si sz�m:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" id="td_KuldoIktatoszama" runat="server">
                                    <%-- asp:DropDownList ID="TovabbitoSzervezetDropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="120">
                    </asp:DropDownList --%>
                                    <asp:TextBox ID="HivatkozasiSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="RagszamLabel" runat="server" Text="Postai azonos�t�:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%-- asp:DropDownList ID="ErkezesModjaDropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="120">
                    </asp:DropDownList --%>
                                    <uc10:VonalKodTextBox ID="RagszamTextBox" runat="server" CssClass="mrUrlapInput"
                                        Validate="false"></uc10:VonalKodTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" id="tr_Surgosseg_Adathordozo" runat="server">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="SurgossegLabel" runat="server" Text="K�zbes�t�s priorit�sa:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <ktddl:KodtarakDropDownList ID="SurgossegDropDownList" runat="server" CssClass="mrUrlapInputComboBox"></ktddl:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="UgyintezesAlapjaLabel" runat="server" Text="K�ldem�ny t�pusa:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%-- asp:DropDownList ID="SurgossegDropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="80">
                    </asp:DropDownList --%>
                                    <ktddl:KodtarakDropDownList ID="UgyintezesAlapjaDropDownList" runat="server" CssClass="mrUrlapInputComboBox"></ktddl:KodtarakDropDownList>
                                </td>
                            </tr>
                            <%--MAIN bernat.laszlo added--%>
                            <tr class="urlapSor_kicsi" runat="server" id="trCimzesKezbesites">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label153" runat="server" Text="K�zbes�t�s m�dja:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <ktddl:KodtarakDropDownList ID="Kezbesites_modja_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label182" runat="server" Text="C�mz�s t�pusa:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <ktddl:KodtarakDropDownList ID="CimzesTipusa_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" runat="server" id="trMunkaallomas">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label154" runat="server" Text="Munka�llom�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <asp:TextBox ID="Munkaallomas_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor_kicsi" runat="server" id="trIktatasExtra">
                                <td class="mrUrlapCaption_shortest">&nbsp;
                                </td>
                                <td class="mrUrlapMezo" width="0%">&nbsp;
                                </td>

                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label156" runat="server" Text="S�r�lt k�ldem�ny?"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <asp:RadioButton ID="serult_kuld_Igen_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Igen" />
                                    <asp:RadioButton ID="serult_kuld_Nem_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Nem" />
                                </td>
                            </tr>

                            <tr class="urlapSor_kicsi" runat="server" id="trTevedesSerules">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label157" runat="server" Text="T�ves c�mz�s?"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <asp:RadioButton ID="teves_cim_Igen_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Igen" />
                                    <asp:RadioButton ID="teves_cim_Nem_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Nem" />
                                </td>

                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="Label158" runat="server" Text="T�ves �rkeztet�s?"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" width="0%">
                                    <asp:RadioButton ID="teves_erk_Igen_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Igen" />
                                    <asp:RadioButton ID="teves_erk_Nem_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Nem" />
                                </td>
                            </tr>
                            <%--MAIN bernat.laszlo eddig--%>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" id="tr_Tartalom_IktatasiKotelezettseg" runat="server">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="TargyLabel0" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="Tartalom:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="TartalomTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="IktatasiKotelezettsegLabel" runat="server" Text="Iktat�si k�telezetts�g:"
                                        CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%-- asp:DropDownList ID="IktatasiKotelezettsegDropDown" runat="server" CssClass="mrUrlapInputComboBox" Width="120">
                    </asp:DropDownList --%>
                                    <ktddl:KodtarakDropDownList ID="IktatasiKotelezettsegDropDown" runat="server"></ktddl:KodtarakDropDownList>
                                </td>
                            </tr>
                            <%--                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="FeljegyzesTipusaLabel" runat="server" Text="Kezel�si feljegyz�s t�pusa:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="KuldKezFelj_KezelesTipus_KodtarDropDown" runat="server"
                                            Enabled="false"></ktddl:KodtarakDropDownList>
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="KezelesiFeljegyzesLabel" runat="server" Text="Kezel�si feljegyz�s:"
                                            CssClass="mrUrlapCaption_shortest"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="KuldKezFelj_Leiras_TextBox" runat="server" CssClass="mrUrlapInput"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>--%>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="AllapotLabel" runat="server" Text="�llapot:" CssClass="mrUrlapCaption_shortest"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%-- asp:DropDownList ID="AllapotDropDown" runat="server" CssClass="mrUrlapInput" Width="120">
                    </asp:DropDownList --%>
                                    <ktddl:KodtarakDropDownList ID="AllapotDropDown" runat="server"></ktddl:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption_shortest">
                                    <asp:Label ID="TargyLabel" runat="server" CssClass="mrUrlapCaption_shortest"
                                        Text="T�rgy:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="TargyTextBox" runat="server" CssClass="mrUrlapInputFTS">
                                    </asp:TextBox>
                                </td>
                            </tr>
                              <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">                            
                            <td class="mrUrlapCaption_short">                                
                                <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                            <%--</table>
    </eUI:eFormPanel>

    <eUI:eFormPanel ID="EFormPanel4" runat="server">
        <table cellspacing="0" cellpadding="0">--%>
                            <%--<tr class="urlapElvalasztoSor">
                </tr>--%>

                            <tr id="tr_SzamlaAdatokPanel" runat="server" visible="false">
                                <td colspan="4">
                                    <szap:SzamlaAdatokPanel ID="SzamlaAdatokPanel" runat="server" SzuloTipus="Kuldemeny" HrAtTopVisible="true" SearchMode="true" />
                                </td>
                            </tr>
                            <%--			                    <tr id="tr_ObjektumTargyszavaiPanel" runat="server" visible="false">
			                        <td colspan="4">
			                            <hr />
                                        <otp:ObjektumTargyszavaiPanel ID="ObjektumTargyszavaiPanel1" runat="server" SearchMode="true"
                                            RepeatColumns="1" RepeatDirection="Vertical" />
                                    </td>
                                </tr>--%>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td class="mrUrlapCaption_shortest"></td>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_shortest">
                                                    <asp:CheckBox ID="SajatCheckBox" runat="server" Text="Saj�t"></asp:CheckBox>
                                                </td>
                                                <td class="mrUrlapCaption_shortest">
                                                    <asp:CheckBox ID="IktathatokCheckBox" runat="server" Text="Iktathat�k"></asp:CheckBox>
                                                </td>
                                                <td class="mrUrlapCaption_shortest">
                                                    <asp:CheckBox ID="SztornozhatokCheckBox" runat="server" Text="Sztorn�zhat�k"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_shortest"></td>
                                                <td class="mrUrlapCaption_shortest">
                                                    <asp:CheckBox ID="IktatottakCheckBox" runat="server" Text="Iktatottak"></asp:CheckBox>
                                                </td>
                                                <td class="mrUrlapCaption_shortest">
                                                    <asp:CheckBox ID="LezartakCheckBox" runat="server" Text="Lez�rtak"></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <asp:Table ID="Table1" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <tsz:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                    runat="server"></tsz:TalalatokSzama_SearchFormComponent>
            </asp:TableCell><asp:TableCell>
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
