using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Collections.Generic;
using System.Data;

public partial class KuldemenyekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_KuldKuldemenyekSearch);
    private const string KodCsoportUgyintezesAlapja = "UGYINTEZES_ALAPJA";
    private const string KodCsoportSurgosseg = "SURGOSSEG";
    private const string KodCsoportAllapot = "KULDEMENY_ALLAPOT";
    private const string KodCsoportIktatasikotelezettseg = "IKTATASI_KOTELEZETTSEG";
    private const string KodCsoportKuldemenyKuldesModja = "KULDEMENY_KULDES_MODJA";
    private const string KodCsoportTovabbitoSzervezet = "TOVABBITO_SZERVEZET";
    //private const string KodCsoportKEZELESI_FELJEGYZES_KULDEMENY = "KEZELESI_FELJEGYZES_KULDEMENY";
    // KEZELESI_FELJEGYZES_KULDEMENY helyett
    private const string KodCsoportFELADAT_ALTIPUS = "FELADAT_ALTIPUS";

    //bernat.laszlo added
    private const string KodCsoportKULD_KEZB_MODJA = "KULD_KEZB_MODJA";
    private const string KodCsoportKULD_CIMZES_TIPUS = "KULD_CIMZES_TIPUS";
    //bernat.laszlo eddig

    private string Startup = String.Empty;
    private bool isTUKRendszer = false;

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        isTUKRendszer = Rendszerparameterek.IsTUK(Page);

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (Startup == Constants.Startup.Szamla)
        {
            SearchHeader1.HeaderTitle = Resources.Search.KuldemenySearchHeaderTitle_Szamla;
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SzamlaSearch;
        }
        else
        {
            SearchHeader1.HeaderTitle = Resources.Search.KuldemenySearchHeaderTitle;
        }
        SearchHeader1.TemplateObjectType = _type;


        // Tabindex el lett �ll�tva a komponensen, itt vissza�ll�tjuk:
        Erkeztetokonyvek_DropDownList.DropDownList.TabIndex = 0;

        //ErkeztetoszamSearch.Validate = false;
        //ErkeztetesIdeje_DatumIntervallum_SearchCalendarControl1.Validate = false;
        //bernat.laszlo added
        Beerkezes_Idopontja_SearchCalendarControl.Validate = false;
        Kuldemeny_Bontoja_FelhasznaloTextBox.Validate = false;
        Bontas_Idopontja_SearchCalendarControl.Validate = false;
        //bernat.laszlo eddig
        BekuldoNeveTextBox.Validate = false;
        CimId_BekuldoCimeTextBox.Validate = false;
        EredetiCimzett_CsoportTextBox.Validate = false;
        Felelos_CsoportTextBox.Validate = false;
        OrzoTextBox.Validate = false;
        //SzervezetnelJartTextBox.Validate = false;
        FelhasznaloTextBox_Erkezteto.Validate = false;
        FelhasznaloTextBox_Erkezteto.SearchMode = true;
        Kuldemeny_Bontoja_FelhasznaloTextBox.SearchMode = true;

        // Partner c�m�nek �tm�sol�s�hoz:
        BekuldoNeveTextBox.CimTextBox = CimId_BekuldoCimeTextBox.TextBox;
        BekuldoNeveTextBox.CimHiddenField = CimId_BekuldoCimeTextBox.HiddenField;

        if (Startup == Constants.Startup.Szamla)
        {
            td_labelKuldoIktatoszama.Visible = false;
            td_KuldoIktatoszama.Visible = false;
            tr_Surgosseg_Adathordozo.Visible = false;
            tr_Tartalom_IktatasiKotelezettseg.Visible = false;

            IktathatokCheckBox.Visible = false;
            IktatottakCheckBox.Visible = false;

            //tr_ObjektumTargyszavaiPanel.Visible = true;
            tr_SzamlaAdatokPanel.Visible = true;
        }

        registerJavascripts();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            EREC_KuldKuldemenyekSearch searchObject = null;
            if (Startup == Constants.Startup.Szamla)
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SzamlaSearch))
                {
                    searchObject = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_KuldKuldemenyekSearch(), Constants.CustomSearchObjectSessionNames.SzamlaSearch);
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }

                //tr_ObjektumTargyszavaiPanel.Visible = true;
                //FillObjektumTargyszavaiPanelForSearch(KodTarak.KULDEMENY_TIPUS.Szamla);
                //if (ObjektumTargyszavaiPanel1.Count == 0)
                //{
                //    tr_ObjektumTargyszavaiPanel.Visible = false;
                //}
            }
            else
            {
                if (Search.IsSearchObjectInSession(Page, _type))
                {
                    searchObject = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject(Page, new EREC_KuldKuldemenyekSearch());
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            LoadComponentsFromSearchObject(searchObject);

            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Validate = false;
                tr_fizikaihely.Visible = true;
                IrattariHelyLevelekDropDownTUK.Visible = true;
                //JavaScripts.SetFocus(IrattariHelyLevelekDropDownTUK);                  
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // BUG_13391 (BUG_13388)
        if (!IsPostBack)
        {
            //F�kusz be�ll�t�sa a vonalk�d mez�re
            Page.SetFocus(VonalkodTextBox.TextBox);
        }
    }

    #endregion

    //protected void FillObjektumTargyszavaiPanelForSearch(String tipus)
    //{
    //    EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
    //    ObjektumTargyszavaiPanel1.FillObjektumTargyszavai(search, null, null
    //    , Constants.TableNames.EREC_KuldKuldemenyek, Constants.ColumnNames.EREC_KuldKuldemenyek.Tipus, new string[] { tipus }, false
    //    , null, false, false, SearchHeader1.ErrorPanel);
    //}


    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = null;
        if (searchObject != null) erec_KuldKuldemenyekSearch = (EREC_KuldKuldemenyekSearch)searchObject;

        if (erec_KuldKuldemenyekSearch != null)
        {
            Erkeztetes_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
               erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);


            //Erkeztetokonyvek_DropDownList.FillAndSetSelectedValue(false, Constants.IktatoErkezteto.Erkezteto,
            //    erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value, true, SearchHeader1.ErrorPanel);

            // �rkeztet�k�nyvek megk�l�nb�ztet� jelz�s alapj�n val� felt�lt�se:
            Erkeztetokonyvek_DropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto, false
                , Erkeztetes_Ev_EvIntervallum_SearchFormControl.EvTol, Erkeztetes_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);

            #region ERKEZTETES IDEJE
            ////ErkeztetesIdeje_DatumIntervallum_SearchCalendarControl1.SetComponentFromSearchObjectFields(
            ////                  erec_KuldKuldemenyekSearch.Manual_ErkeztetesIdeje);

            DatumIdoIntervallum_SearchCalendarControl_ErkeztetesDatuma.DatumKezdValue = erec_KuldKuldemenyekSearch.Manual_ErkeztetesIdeje.Value;
            DatumIdoIntervallum_SearchCalendarControl_ErkeztetesDatuma.DatumVegeValue = erec_KuldKuldemenyekSearch.Manual_ErkeztetesIdeje.ValueTo;
            DatumIdoIntervallum_SearchCalendarControl_ErkeztetesDatuma.SetSearchObjectFields(erec_KuldKuldemenyekSearch.Manual_ErkeztetesIdeje);

            #endregion
            ErkeztetoSzam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_KuldKuldemenyekSearch.Erkezteto_Szam);

            FelhasznaloTextBox_Erkezteto.Id_HiddenField = erec_KuldKuldemenyekSearch.Manual_Erkezteto_Id.Value;
            FelhasznaloTextBox_Erkezteto.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            ErkezesModjaDropDownList.FillAndSetSelectedValue(KodCsoportKuldemenyKuldesModja
                    , erec_KuldKuldemenyekSearch.KuldesMod.Value, true, SearchHeader1.ErrorPanel);

            //TovabbitoSzervezetDropDownList.FillAndSetSelectedValue(KodCsoportTovabbitoSzervezet
            //        , erec_KuldKuldemenyekSearch.Tovabbito.Value, true, SearchHeader1.ErrorPanel);

            HivatkozasiSzamTextBox.Text = erec_KuldKuldemenyekSearch.HivatkozasiSzam.Value;
            RagszamTextBox.Text = erec_KuldKuldemenyekSearch.RagSzam.Value;

            SurgossegDropDownList.FillAndSetSelectedValue(KodCsoportSurgosseg
                    , erec_KuldKuldemenyekSearch.Surgosseg.Value, true, SearchHeader1.ErrorPanel);

            UgyintezesAlapjaDropDownList.FillAndSetSelectedValue(KodCsoportUgyintezesAlapja
                    , erec_KuldKuldemenyekSearch.UgyintezesModja.Value, true, SearchHeader1.ErrorPanel);

            #region K�zbes�t�s m�dja
            if (String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.KezbesitesModja.Value))
            {
                Kezbesites_modja_DropDownList.FillAndSetEmptyValue(KodCsoportKULD_KEZB_MODJA,
                    SearchHeader1.ErrorPanel, true);
            }
            else
            {
                Kezbesites_modja_DropDownList.FillAndSetSelectedValue(KodCsoportKULD_KEZB_MODJA,
                    erec_KuldKuldemenyekSearch.KezbesitesModja.Value, true, SearchHeader1.ErrorPanel);
            }
            #endregion

            #region C�mz�s t�pusa
            if (String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.CimzesTipusa.Value))
            {
                CimzesTipusa_DropDownList.FillAndSetEmptyValue(KodCsoportKULD_CIMZES_TIPUS,
                    SearchHeader1.ErrorPanel, true);
            }
            else
            {
                CimzesTipusa_DropDownList.FillAndSetSelectedValue(KodCsoportKULD_CIMZES_TIPUS,
                    erec_KuldKuldemenyekSearch.CimzesTipusa.Value, true, SearchHeader1.ErrorPanel);
            }
            #endregion

            Munkaallomas_TextBox.Text = erec_KuldKuldemenyekSearch.Munkaallomas.Value;

            #region S�r�lt k�ldem�ny setup
            if (!string.IsNullOrEmpty(erec_KuldKuldemenyekSearch.SerultKuldemeny.Value))
            {
                if (erec_KuldKuldemenyekSearch.SerultKuldemeny.Value == "1")
                {
                    serult_kuld_Igen_RadioButton.Checked = true;
                }
                else
                {
                    if (erec_KuldKuldemenyekSearch.SerultKuldemeny.Value == "0")
                    {
                        serult_kuld_Nem_RadioButton.Checked = true;
                    }
                }
            }
            #endregion

            #region T�ves c�mz�s setup
            if (string.IsNullOrEmpty(erec_KuldKuldemenyekSearch.TevesCimzes.Value))
            {
                if (erec_KuldKuldemenyekSearch.TevesCimzes.Value == "1")
                {
                    teves_cim_Igen_RadioButton.Checked = true;
                }
                else
                {
                    if (erec_KuldKuldemenyekSearch.TevesCimzes.Value == "0")
                    {
                        teves_cim_Nem_RadioButton.Checked = true;
                    }
                }
            }
            #endregion

            #region T�ves �rkeztet�s setup
            if (string.IsNullOrEmpty(erec_KuldKuldemenyekSearch.TevesErkeztetes.Value))
            {
                if (erec_KuldKuldemenyekSearch.TevesErkeztetes.Value == "1")
                {
                    teves_erk_Igen_RadioButton.Checked = true;
                }
                else
                {
                    if (erec_KuldKuldemenyekSearch.TevesErkeztetes.Value == "0")
                    {
                        teves_erk_Nem_RadioButton.Checked = true;
                    }
                }
            }
            #endregion

            Beerkezes_Idopontja_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_KuldKuldemenyekSearch.BeerkezesIdeje);

            Bontas_Idopontja_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_KuldKuldemenyekSearch.FelbontasDatuma);

            #region K�ldem�ny bont�ja
            if (!String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Bonto.Value))
            {
                Kuldemeny_Bontoja_FelhasznaloTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Manual_Erkezteto_Id.Value;
                Kuldemeny_Bontoja_FelhasznaloTextBox.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);
            }
            #endregion

            //bernat.laszlo eddig

            if (!String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value))
            {
                BekuldoNeveTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value;
                BekuldoNeveTextBox.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            }
            else
            {
                BekuldoNeveTextBox.Text = erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;
            }

            CimId_BekuldoCimeTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Cim_Id.Value;
            CimId_BekuldoCimeTextBox.SetCimekTextBoxById(SearchHeader1.ErrorPanel);
            if (erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value != String.Empty)
                CimId_BekuldoCimeTextBox.Text = erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value;

            EredetiCimzett_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value;
            EredetiCimzett_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            // csak ha t�nyleg egy Id van benne
            string Csoport_Id_Felelos = erec_KuldKuldemenyekSearch.Csoport_Id_Felelos.Value.Replace("'", "");
            string[] separator = new string[] { "," };
            int cntIds = Csoport_Id_Felelos.Split(separator, StringSplitOptions.None).Length;
            if (cntIds < 2)
            {
                Felelos_CsoportTextBox.Id_HiddenField = Csoport_Id_Felelos;
            }
            else
            {
                Felelos_CsoportTextBox.Id_HiddenField = "";
            }
            //Felelos_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Csoport_Id_Felelos.Value;
            Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            OrzoTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Value;
            OrzoTextBox.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            /// TODO: SzervezetnelJartTextBox.Id_HiddenField = ???;
            ///SzervezetnelJartTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            ///

            TargyTextBox.Text = erec_KuldKuldemenyekSearch.Targy.Value;

            TartalomTextBox.Text = erec_KuldKuldemenyekSearch.Tartalom.Value;

            IktatasiKotelezettsegDropDown.FillAndSetSelectedValue(KodCsoportIktatasikotelezettseg
                    , erec_KuldKuldemenyekSearch.IktatniKell.Value, true, SearchHeader1.ErrorPanel);


            //KuldKezFelj_KezelesTipus_KodtarDropDown.FillAndSetSelectedValue(KodCsoportKEZELESI_FELJEGYZES_KULDEMENY,
            //    erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_KezelesTipus.Value, true, SearchHeader1.ErrorPanel);

            //KuldKezFelj_Leiras_TextBox.Text = erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_Leiras.Value;

            //// KEZELESI_FELJEGYZES_KULDEMENY helyett
            //// el�k�sz�tve, de t�rolt elj�r�s fel�l m�g nem implement�lt
            //KuldKezFelj_KezelesTipus_KodtarDropDown.FillAndSetSelectedValue(KodCsoportFELADAT_ALTIPUS,
            //    erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Altipus.Value, true, SearchHeader1.ErrorPanel);

            //KuldKezFelj_Leiras_TextBox.Text = erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Leiras.Value;


            AllapotDropDown.FillAndSetSelectedValue(KodCsoportAllapot, erec_KuldKuldemenyekSearch.Allapot.Value, true,
                        SearchHeader1.ErrorPanel);

            VonalkodTextBox.Text = erec_KuldKuldemenyekSearch.BarCode.Value;

            //ObjektumTargyszavaiPanel1.FillFromFTSTree(erec_KuldKuldemenyekSearch.fts_tree_objektumtargyszavai);

            SajatCheckBox.Checked = (!String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Sajat.Value));

            IktathatokCheckBox.Checked =
                (!String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Iktathatok_Allapot.Value)
                || !String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Iktathatok_IktatniKell.Value));

            SztornozhatokCheckBox.Checked = !String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Sztornozhatok.Value);

            IktatottakCheckBox.Checked = !String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Iktatottak.Value);

            LezartakCheckBox.Checked = !String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Manual_Lezartak.Value);

            if (Startup == Constants.Startup.Szamla)
            {
                SzamlaAdatokPanel.LoadComponentsFromSearchObject(erec_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch, SearchHeader1.ErrorPanel, null);
            }
            TextBox_KulsoAzonosito.Text = erec_KuldKuldemenyekSearch.KulsoAzonosito.Value;
            if (isTUKRendszer)
            {
                ReloadIrattariHelyLevelekDropDownTUK(erec_KuldKuldemenyekSearch.IrattarId.Value);
            }
        }
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string value)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = ExecParam.Felhasznalo_Id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode) || result_csoportok.Ds.Tables[0].Rows.Count < 1)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }

                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, SearchHeader1.ErrorPanel);
                    if (!string.IsNullOrEmpty(value))
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(value);

                }
            }
        }
    }

    private EREC_KuldKuldemenyekSearch SetSearchObjectFromComponents()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = (EREC_KuldKuldemenyekSearch)SearchHeader1.TemplateObject;
        if (erec_KuldKuldemenyekSearch == null)
        {
            erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch(true);
        }

        if (!String.IsNullOrEmpty(Erkeztetokonyvek_DropDownList.SelectedValue))
        {
            // �rkeztet�k�nyvn�l a megk�l�nb�ztet� jelz�sre sz�r�nk, nem az id-ra
            Erkeztetokonyvek_DropDownList.SetSearchObject(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);
        }

        #region ERKEZTETES IDEJE
        DatumIdoIntervallum_SearchCalendarControl_ErkeztetesDatuma.SetSearchObjectFields(erec_KuldKuldemenyekSearch.Manual_ErkeztetesIdeje);

        #endregion

        ErkeztetoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_KuldKuldemenyekSearch.Erkezteto_Szam);

        if (!String.IsNullOrEmpty(FelhasznaloTextBox_Erkezteto.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Manual_Erkezteto_Id.Filter(FelhasznaloTextBox_Erkezteto.Id_HiddenField);
        }

        Erkeztetes_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        if (!String.IsNullOrEmpty(ErkezesModjaDropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.KuldesMod.Filter(ErkezesModjaDropDownList.SelectedValue);
        }

        //if (!String.IsNullOrEmpty(TovabbitoSzervezetDropDownList.SelectedValue))
        //{
        //    erec_KuldKuldemenyekSearch.Tovabbito.Value = TovabbitoSzervezetDropDownList.SelectedValue;
        //    erec_KuldKuldemenyekSearch.Tovabbito.Operator = Query.Operators.equals;
        //}

        if (!String.IsNullOrEmpty(HivatkozasiSzamTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.HivatkozasiSzam.Value = HivatkozasiSzamTextBox.Text;
            erec_KuldKuldemenyekSearch.HivatkozasiSzam.Operator = Search.GetOperatorByLikeCharater(HivatkozasiSzamTextBox.Text);
        }

        if (!String.IsNullOrEmpty(RagszamTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.RagSzam.Value = RagszamTextBox.Text;
            erec_KuldKuldemenyekSearch.RagSzam.Operator = Search.GetOperatorByLikeCharater(RagszamTextBox.Text);
        }

        if (!String.IsNullOrEmpty(SurgossegDropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.Surgosseg.Filter(SurgossegDropDownList.SelectedValue);
        }

        if (!String.IsNullOrEmpty(UgyintezesAlapjaDropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.UgyintezesModja.Filter(UgyintezesAlapjaDropDownList.SelectedValue);
        }

        //bernat.laszlo added
        #region Munka�llom�s
        if (!String.IsNullOrEmpty(Munkaallomas_TextBox.Text))
        {
            erec_KuldKuldemenyekSearch.Munkaallomas.Value = Munkaallomas_TextBox.Text;
            erec_KuldKuldemenyekSearch.Munkaallomas.Operator = Query.Operators.contains;
        }
        #endregion

        #region Be�rkez�s id�pontja
        if (Beerkezes_Idopontja_SearchCalendarControl.AktualisEv_CheckBox.Checked)
        {
            DateTime evEleje = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime evVege = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            erec_KuldKuldemenyekSearch.BeerkezesIdeje.Between(evEleje.ToString(), evVege.ToString());
        }
        else
        {
            Beerkezes_Idopontja_SearchCalendarControl.SetSearchObjectFields(erec_KuldKuldemenyekSearch.BeerkezesIdeje);
        }
        #endregion

        #region Bont�s id�pontja
        if (Bontas_Idopontja_SearchCalendarControl.AktualisEv_CheckBox.Checked)
        {
            DateTime evEleje = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime evVege = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            erec_KuldKuldemenyekSearch.FelbontasDatuma.Between(evEleje.ToString(), evVege.ToString());
        }
        else
        {
            Bontas_Idopontja_SearchCalendarControl.SetSearchObjectFields(erec_KuldKuldemenyekSearch.FelbontasDatuma);
        }
        #endregion

        #region K�ldem�ny bont�ja
        if (!String.IsNullOrEmpty(Kuldemeny_Bontoja_FelhasznaloTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Bonto.Filter(Kuldemeny_Bontoja_FelhasznaloTextBox.Id_HiddenField);
        }
        #endregion

        #region K�zbes�t�s m�dja
        if (!String.IsNullOrEmpty(Kezbesites_modja_DropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.KezbesitesModja.Filter(Kezbesites_modja_DropDownList.SelectedValue);
        }
        #endregion

        #region C�mz�s t�pusa
        if (!String.IsNullOrEmpty(CimzesTipusa_DropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.CimzesTipusa.Filter(CimzesTipusa_DropDownList.SelectedValue);
        }
        #endregion

        #region S�r�lt k�ldem�ny
        if (serult_kuld_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyekSearch.SerultKuldemeny.Filter("1");
        }
        else
        {
            if (serult_kuld_Nem_RadioButton.Checked)
            {
                erec_KuldKuldemenyekSearch.SerultKuldemeny.Filter("0");
            }
        }
        #endregion

        #region T�ves c�mz�s
        if (teves_cim_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyekSearch.TevesCimzes.Filter("1");
        }
        else
        {
            if (teves_cim_Nem_RadioButton.Checked)
            {
                erec_KuldKuldemenyekSearch.TevesCimzes.Filter("0");
            }
        }
        #endregion

        #region T�ves �rkeztet�s
        if (teves_erk_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyekSearch.TevesCimzes.Value = "1";
            erec_KuldKuldemenyekSearch.TevesCimzes.Operator = Query.Operators.equals;
        }
        else
        {
            if (teves_erk_Nem_RadioButton.Checked)
            {
                erec_KuldKuldemenyekSearch.TevesCimzes.Value = "0";
                erec_KuldKuldemenyekSearch.TevesCimzes.Operator = Query.Operators.equals;
            }
        }
        #endregion
        //bernat.laszlo eddig

        if (!String.IsNullOrEmpty(BekuldoNeveTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Filter(BekuldoNeveTextBox.Id_HiddenField);
        }
        else
        {
            if (!String.IsNullOrEmpty(BekuldoNeveTextBox.Text))
            {
                erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = BekuldoNeveTextBox.Text;
                erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Query.Operators.contains; //Search.GetOperatorByLikeCharater(BekuldoNeveTextBox.Text);
            }
        }

        if (!String.IsNullOrEmpty(CimId_BekuldoCimeTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Cim_Id.Filter(CimId_BekuldoCimeTextBox.Id_HiddenField);
        }
        else
        {
            if (!String.IsNullOrEmpty(CimId_BekuldoCimeTextBox.Text))
            {
                erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value = CimId_BekuldoCimeTextBox.Text;
                erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(CimId_BekuldoCimeTextBox.Text);
            }
        }

        if (!String.IsNullOrEmpty(EredetiCimzett_CsoportTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Filter(EredetiCimzett_CsoportTextBox.Id_HiddenField);
        }

        if (!String.IsNullOrEmpty(Felelos_CsoportTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Csoport_Id_Felelos.Filter(Felelos_CsoportTextBox.Id_HiddenField);
        }

        if (!String.IsNullOrEmpty(OrzoTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Filter(OrzoTextBox.Id_HiddenField);
        }

        //TODO: if (!String.IsNullOrEmpty(SzervezetnelJartTextBox.Id_HiddenField))
        //{

        //}

        if (!String.IsNullOrEmpty(TargyTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.Targy.Value = TargyTextBox.Text;
            erec_KuldKuldemenyekSearch.Targy.Operator = Query.Operators.contains; //Search.GetOperatorByLikeCharater(TargyTextBox.Text);  // BUG_3944 kapcs�n visszarakva
        }
        if (!String.IsNullOrEmpty(TartalomTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.Tartalom.Value = TartalomTextBox.Text;
            erec_KuldKuldemenyekSearch.Tartalom.Operator = Search.GetOperatorByLikeCharater(TartalomTextBox.Text);
        }

        if (!String.IsNullOrEmpty(IktatasiKotelezettsegDropDown.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.IktatniKell.Filter(IktatasiKotelezettsegDropDown.SelectedValue);
        }

        //if (!String.IsNullOrEmpty(KuldKezFelj_KezelesTipus_KodtarDropDown.SelectedValue))
        //{
        //    erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_KezelesTipus.Value =
        //        KuldKezFelj_KezelesTipus_KodtarDropDown.SelectedValue;
        //    erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_KezelesTipus.Operator =
        //        Query.Operators.equals;
        //}

        //if (!String.IsNullOrEmpty(KuldKezFelj_Leiras_TextBox.Text))
        //{
        //    erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_Leiras.Value =
        //        KuldKezFelj_Leiras_TextBox.Text;
        //    erec_KuldKuldemenyekSearch.Manual_KuldKezFelj_Leiras.Operator =
        //        Search.GetOperatorByLikeCharater(KuldKezFelj_Leiras_TextBox.Text);
        //}

        //// Manual_KuldKezFelj_KezelesTipus helyett
        //// el�k�sz�tve, de t�rolt elj�r�s fel�l m�g nem implement�lt
        //if (!String.IsNullOrEmpty(KuldKezFelj_KezelesTipus_KodtarDropDown.SelectedValue))
        //{
        //    erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Altipus.Value =
        //        KuldKezFelj_KezelesTipus_KodtarDropDown.SelectedValue;
        //    erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Altipus.Operator =
        //        Query.Operators.equals;
        //}

        //if (!String.IsNullOrEmpty(KuldKezFelj_Leiras_TextBox.Text))
        //{
        //    erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Leiras.Value =
        //        KuldKezFelj_Leiras_TextBox.Text;
        //    erec_KuldKuldemenyekSearch.Manual_Feljegyzes_Leiras.Operator =
        //        Search.GetOperatorByLikeCharater(KuldKezFelj_Leiras_TextBox.Text);
        //}

        if (!String.IsNullOrEmpty(AllapotDropDown.SelectedValue))
        {
            // A Tovabbitasalattallapot mez�ben is kell keresni: (VAGY kapcsolat)

            erec_KuldKuldemenyekSearch.Allapot.Filter(AllapotDropDown.SelectedValue);
            erec_KuldKuldemenyekSearch.Allapot.OrGroup("531");

            erec_KuldKuldemenyekSearch.TovabbitasAlattAllapot.Filter(erec_KuldKuldemenyekSearch.Allapot.Value);
            erec_KuldKuldemenyekSearch.TovabbitasAlattAllapot.OrGroup("531");
        }

        if (!String.IsNullOrEmpty(VonalkodTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.BarCode.Value = VonalkodTextBox.Text;
            erec_KuldKuldemenyekSearch.BarCode.Operator = Search.GetOperatorByLikeCharater(VonalkodTextBox.Text);
        }

        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyekSearch.Tipus.Filter(KodTarak.KULDEMENY_TIPUS.Szamla);

            erec_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch = SzamlaAdatokPanel.SetSearchObjectFromComponents(erec_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch);
        }

        #region FTS

        //#region objektum t�rgyszavai

        //if (Startup == Constants.Startup.Szamla)
        //{
        //    // kuldemeny tipus auto
        //    try
        //    {
        //        erec_KuldKuldemenyekSearch.fts_tree_objektumtargyszavai = ObjektumTargyszavaiPanel1.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);

        //        if (erec_KuldKuldemenyekSearch.fts_tree_objektumtargyszavai != null)
        //        {
        //            erec_KuldKuldemenyekSearch.ObjektumTargyszavai_ObjIdFilter = erec_KuldKuldemenyekSearch.fts_tree_objektumtargyszavai.TransformToFTSContainsConditions();
        //        }
        //        else
        //        {
        //            erec_KuldKuldemenyekSearch.ObjektumTargyszavai_ObjIdFilter = "";
        //        }
        //    }
        //    catch (FullTextSearchException e)
        //    {
        //        ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        //    }
        //}
        //#endregion objektum t�rgyszavai

        #endregion FTS


        #region CheckBoxok

        if (SajatCheckBox.Checked)
        {
            // Saj�t == A felhaszn�l� az �rz�
            erec_KuldKuldemenyekSearch.Manual_Sajat.Filter(Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page)));
        }

        #region Manu�lisan kezeltek (WhereByManual �ll�tva)

        String WhereByManual = "";

        if (SztornozhatokCheckBox.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_KuldKuldemenyekSearch.Manual_Sztornozhatok.Name
                + " not in (" + Search.GetSqlInnerString(Kuldemenyek.GetNemSztornozhatoAllapotok().ToArray()) + ") ";

            /// Sztorn�zhat� == NEM Erkeztetve �S NEM Lezarva �S NEM Iktatva �S NEM Sztorno
            erec_KuldKuldemenyekSearch.Manual_Sztornozhatok.Value =
                Search.GetSqlInnerString(Kuldemenyek.GetNemSztornozhatoAllapotok().ToArray());
            // oper�tort nem �ll�tjuk, hogy ne szerepeljen k�tszer a felt�tel(a .Value -t az�rt �ll�tjuk be, hogy visszat�lt�skor l�ssuk, hogy ez be volt �ll�tva)
            //erec_KuldKuldemenyekSearch.Manual_Sztornozhatok.Operator = Query.Operators.notinner;
        }

        if (IktathatokCheckBox.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += "(" + erec_KuldKuldemenyekSearch.Manual_Iktathatok_IktatniKell.Name
                + "='" + KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando + "' and "
                + erec_KuldKuldemenyekSearch.Manual_Iktathatok_Allapot.Name + " in "
                + "('" + KodTarak.KULDEMENY_ALLAPOT.Erkeztetve
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Szignalva
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Atveve
                + "')"
                + ")";


            erec_KuldKuldemenyekSearch.Manual_Iktathatok_IktatniKell.Value = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            //erec_KuldKuldemenyekSearch.Manual_Iktathatok_IktatniKell.Operator = Query.Operators.equals;

            erec_KuldKuldemenyekSearch.Manual_Iktathatok_Allapot.Value =
                "'" + KodTarak.KULDEMENY_ALLAPOT.Erkeztetve
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Szignalva
                + "','" + KodTarak.KULDEMENY_ALLAPOT.Atveve
                + "'";
            //erec_KuldKuldemenyekSearch.Manual_Iktathatok_Allapot.Operator = Query.Operators.inner;

            //Kuldemenyek.SetSearchObjectTo_Iktathatok(erec_KuldKuldemenyekSearch);
        }

        if (IktatottakCheckBox.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_KuldKuldemenyekSearch.Manual_Iktatottak.Name
                + "='" + KodTarak.KULDEMENY_ALLAPOT.Iktatva + "'";

            erec_KuldKuldemenyekSearch.Manual_Iktatottak.Value = KodTarak.KULDEMENY_ALLAPOT.Iktatva;
            //erec_KuldKuldemenyekSearch.Manual_Iktatottak.Operator = Query.Operators.equals;
        }

        if (LezartakCheckBox.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_KuldKuldemenyekSearch.Manual_Lezartak.Name
                + "='" + KodTarak.KULDEMENY_ALLAPOT.Lezarva + "'";


            erec_KuldKuldemenyekSearch.Manual_Lezartak.Value = "'" + KodTarak.KULDEMENY_ALLAPOT.Lezarva + "'";
            //erec_KuldKuldemenyekSearch.Manual_Lezartak.Operator = Query.Operators.equals;
        }

        // WhereByManual lez�r�sa:
        if (!string.IsNullOrEmpty(WhereByManual))
        {
            WhereByManual += ") ";
        }

        // BUG_9316
        if ((erec_KuldKuldemenyekSearch.Manual_Iktathatok_IktatniKell.Value == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando ||
            erec_KuldKuldemenyekSearch.IktatniKell.Value == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando) && !IktatottakCheckBox.Checked)
        {
            WhereByManual = "(" + erec_KuldKuldemenyekSearch.Allapot.Name + "<>'" + KodTarak.KULDEMENY_ALLAPOT.Iktatva + "' AND NOT (" + erec_KuldKuldemenyekSearch.Allapot.Name +
                "='" + KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt + "' AND " + erec_KuldKuldemenyekSearch.TovabbitasAlattAllapot.Name + "='" + KodTarak.KULDEMENY_ALLAPOT.Iktatva + "'))" +
                 (String.IsNullOrEmpty(WhereByManual) ? "" : " AND " + WhereByManual);
        }

        //
        erec_KuldKuldemenyekSearch.WhereByManual = WhereByManual;

        #endregion

        #endregion

        if (isTUKRendszer)
        {
            if (!String.IsNullOrEmpty(IrattariHelyLevelekDropDownTUK.SelectedValue))
            {
                erec_KuldKuldemenyekSearch.IrattarId.Value = IrattariHelyLevelekDropDownTUK.SelectedValue;
                erec_KuldKuldemenyekSearch.IrattarId.Operator = Query.Operators.equals;
            }
        }

        if (!string.IsNullOrEmpty(TextBox_KulsoAzonosito.Text))
        {
            erec_KuldKuldemenyekSearch.KulsoAzonosito.Value = TextBox_KulsoAzonosito.Text;
            erec_KuldKuldemenyekSearch.KulsoAzonosito.Operator = Search.GetOperatorByLikeCharater(TextBox_KulsoAzonosito.Text);
        }

        return erec_KuldKuldemenyekSearch;
    }


    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added: Tal�lati list�k ment�se
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_KuldKuldemenyekSearch searchObject = SetSearchObjectFromComponents();
            Kuldemenyek.SetDefaultFilter(searchObject);
            SearchHeader1.NewResultList(searchObject, execParam);
        }
        //bernat.laszlo eddig
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_KuldKuldemenyekSearch searchObject = SetSearchObjectFromComponents();

            //Keres�si felt�telek elment�se kiirathat� form�ban
            searchObject.ReadableWhere = Search.GetReadableWhere(Form);
            //searchObject.ReadableWhere = Search.GetReadableWhere(EFormPanel1)
            //        + Search.GetReadableWhere(tr_ObjektumTargyszavaiPanel);

            if (Startup == Constants.Startup.Szamla)
            {
                EREC_KuldKuldemenyekSearch defaultSearchObject = GetDefaultSearchObject();

                if (Search.IsIdentical(defaultSearchObject, searchObject, defaultSearchObject.WhereByManual, searchObject.WhereByManual)
                    && Search.IsIdentical(defaultSearchObject.Extended_EREC_SzamlakSearch, searchObject.Extended_EREC_SzamlakSearch))
                //&& searchObject.fts_tree_objektumtargyszavai == null) // �sszehasonl�t�s az "�res" default objektummal
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SzamlaSearch);
                }
                else
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.SzamlaSearch);
                }
            }
            else
            {
                if (Search.IsDefaultSearchObject(_type, searchObject)) // �sszehasonl�t�s az "�res" default objektummal
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            // Feleslegesen ne k�ldj�nk le annyi adatot, ha m�r �gyis bez�rjuk az ablakot:
            EFormPanel1.Visible = false;
        }
    }

    private EREC_KuldKuldemenyekSearch GetDefaultSearchObject()
    {
        // TODO: kiemelni Search.GetDefault... ba
        EREC_KuldKuldemenyekSearch searchObject = (EREC_KuldKuldemenyekSearch)Search.GetDefaultSearchObject(_type);
        if (Startup == Constants.Startup.Szamla)
        {
            searchObject.Tipus.Filter(KodTarak.KULDEMENY_TIPUS.Szamla);
        }

        return searchObject;
    }

    private void registerJavascripts()
    {
        Beerkezes_Idopontja_SearchCalendarControl.EnableOrDisableByCheckBox(Beerkezes_Idopontja_SearchCalendarControl.AktualisEv_CheckBox, true);
        Bontas_Idopontja_SearchCalendarControl.EnableOrDisableByCheckBox(Bontas_Idopontja_SearchCalendarControl.AktualisEv_CheckBox, true);
    }
}

