<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KuldTertivevenyekForm.aspx.cs" Inherits="KuldTertivevenyekForm" Title="Untitled Page" %>

<%@ Register Src="Component/CimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc12" %>
<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList"
    TagPrefix="uc10" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc13" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:formheader id="FormHeader1" runat="server" headertitle="<%$Resources:Form,KuldTertivevenyekFormHeaderTitle %>" />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eui:eformpanel id="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" align="center">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelBarcodeStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelBarcode" runat="server" Text="Vonalk�d:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:VonalKodTextBox ID="BarCode" runat="server" RequiredValidate="false" LabelRequiredIndicatorID="LabelBarcodeStar" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelTertvisszaDatStar" runat="server" CssClass="ReqStar" Text="*" />
                                <asp:Label ID="LabelTertvisszaDat" runat="server" Text="Vissza�rkez�s d�tuma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="TertivisszaDat_CalendarControl" runat="server" Validate="false" TimeVisible="false" LabelRequiredIndicatorID="LabelTertvisszaDatStar" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelRagszamDat" runat="server" Text="Postai azonos�t�:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:VonalKodTextBox ID="Ragszam" runat="server" Validate="false" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAllapot" runat="server" Text="�llapot:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <ktddl:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelTertivisszaKodStar" runat="server" CssClass="ReqStar" Text="*" />
                                <asp:Label ID="LabelTertivisszaKod" runat="server" Text="K�zbes�t�s eredm�nye:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <ktddl:KodtarakDropDownList ID="TertivisszaKod_KodtarakDropDownList" runat="server" LabelRequiredIndicatorID="LabelTertivisszaKodStar" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvevoSzemely" runat="server" Text="�tvev� szem�ly:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtAtvevoSzemely" runat="server" CssClass="mrUrlapInput" />
                            </td>

                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvetelDat" runat="server" Text="�tv�tel id�pontja:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="AtvetelDat_CalendarControl" runat="server"
                                    Validate="false" TimeVisible="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNote_AtvetelJogcime" runat="server" Text="�tv�tel jogc�me:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtNote_AtvetelJogcime" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>

                        <%--bernat.laszlo added: K�zbes�t�si metaadatok--%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label150" runat="server" Text="K�zbes�t�s v�lelem be�llta"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButton ID="KezbVelelemBe_Igen_RadioButton" runat="server" GroupName="KezbVelelemBe_Selector" Text="Igen" />
                                <asp:RadioButton ID="KezbVelelemBe_Nem_RadioButton" runat="server" GroupName="KezbVelelemBe_Selector" Text="Nem" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label151" runat="server" Text="K�zbes�t�s v�lelem be�llt�nak d�tuma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="KezbVelelemDatum_CalendarControl" runat="server"
                                    Validate="false" TimeVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelHasImage" runat="server" Text="T�rtivev�ny k�p" />
                            </td>
                            <td class="mrUrlapMezo">
                                  <asp:ImageButton id="ImageButtonTertivevenyKep" runat="server" ImageUrl="~/images/hu/ikon/tertiveveny.gif"
                                       AlternateText="T�rtivev�ny (csatolm�ny)" ToolTip="T�rtivev�ny (csatolm�ny)" Visible="false"/>
                            </td>
                        </tr>
                        <%--bernat.laszlo eddig --%>
                    </table>
                </eui:eformpanel>
                <div style="margin-top:150px!important" ></div>
                <uc4:formpart_createdmodified id="FormPart_CreatedModified1" runat="server" />
                <uc2:formfooter id="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
