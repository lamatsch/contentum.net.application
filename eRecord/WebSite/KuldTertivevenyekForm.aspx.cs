using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class KuldTertivevenyekForm : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private ComponentSelectControl compSelector = null;
    private Contentum.eRecord.Utility.PageView pageView = null;
    private string kcs_TERTIVEVENY_VISSZA_KOD = "TERTIVEVENY_VISSZA_KOD";
    private string kcs_TERTIVEVENY_ALLAPOT = "TERTIVEVENY_ALLAPOT";
    //private string imageWidth = "25px";
    //private string imageHeight = "20px";

    private string Command = "";
    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eRecord.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            case CommandName.Modify:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KuldTertivevenyErkeztetes");
                break;
            default:
                // BUG_1552
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KuldTertiveveny" + Command);
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KuldTertivevenyErkeztetes");
                break;
        }
        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                //Result result = service.Get(execParam);
                Result result = service.GetWithDokumentum(execParam, id);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_KuldTertivevenyek erec_KuldTertivevenyek = null;
                    if (result.Record != null)
                        erec_KuldTertivevenyek = (EREC_KuldTertivevenyek)result.Record;
                    else
                    {
                        erec_KuldTertivevenyek = new EREC_KuldTertivevenyek();
                        LoadBusinessDocumentFromDataRow(erec_KuldTertivevenyek, result.Ds.Tables[0].Rows[0]);
                        result.Record = erec_KuldTertivevenyek;
                    }

                    LoadComponentsFromBusinessObject(erec_KuldTertivevenyek);
                    if (Command == CommandName.Modify)
                    {
                        SetModifyControlsByBusinessDocument(erec_KuldTertivevenyek);
                    }

                    if (result.Ds != null && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                    {
                        SetTertivevenyImage(result.Ds.Tables[0].Rows[0]);
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            TertivisszaDat_CalendarControl.SetToday();
            TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(
                kcs_TERTIVEVENY_VISSZA_KOD,
                KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek,
                FormHeader1.ErrorPanel
            );
            KezbVelelemBe_Nem_RadioButton.Checked = true;
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.KuldTertivevenyekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void SetViewControls()
    {
        BarCode.ReadOnly = true;
        TertivisszaDat_CalendarControl.ReadOnly = true;
        Ragszam.ReadOnly = true;
        TertivisszaKod_KodtarakDropDownList.ReadOnly = true;
        txtAtvevoSzemely.ReadOnly = true;
        AtvetelDat_CalendarControl.ReadOnly = true;
        txtNote_AtvetelJogcime.ReadOnly = true;
        Allapot_KodtarakDropDownList.ReadOnly = true;

        //bernat.laszlo added: sose lehet tudni :)
        if (Command == CommandName.View)
        {
            KezbVelelemBe_Igen_RadioButton.Enabled = false;
            KezbVelelemBe_Nem_RadioButton.Enabled = false;
            KezbVelelemDatum_CalendarControl.Enabled = false;
        }
        //bernat.laszlo eddig
    }

    private void SetModifyControls()
    {
        BarCode.ReadOnly = true;
        Ragszam.ReadOnly = true;
        TertivisszaDat_CalendarControl.Validate = true;
    }

    private void SetModifyControlsByBusinessDocument(EREC_KuldTertivevenyek erec_KuldTertivevenyek)
    {
        if (erec_KuldTertivevenyek != null)
        {
            if (erec_KuldTertivevenyek.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
            {
                BarCode.ReadOnly = true;
                TertivisszaDat_CalendarControl.ReadOnly = true;
                TertivisszaKod_KodtarakDropDownList.ReadOnly = true;
                Ragszam.ReadOnly = true;
                Allapot_KodtarakDropDownList.ReadOnly = true;
            }
            else
            {
                BarCode.ReadOnly = true;
                TertivisszaDat_CalendarControl.ReadOnly = false;
                TertivisszaDat_CalendarControl.Validate = true;
                TertivisszaKod_KodtarakDropDownList.Validate = true;
                TertivisszaKod_KodtarakDropDownList.ReadOnly = false;
                Ragszam.ReadOnly = true;
                Allapot_KodtarakDropDownList.ReadOnly = true;


                if (String.IsNullOrEmpty(erec_KuldTertivevenyek.TertivisszaKod))
                {
                    TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(
                        kcs_TERTIVEVENY_VISSZA_KOD,
                        KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek,
                        FormHeader1.ErrorPanel
                        );
                }

                if (String.IsNullOrEmpty(erec_KuldTertivevenyek.TertivisszaDat))
                {
                    TertivisszaDat_CalendarControl.SetToday();
                }
            }
        }
    }

    public static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
    {
        try
        {
            if (row != null)
            {
                System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo P in Properties)
                {
                    if (row.Table.Columns.Contains(P.Name))
                    {
                        if (!(row.IsNull(P.Name)))
                        {
                            P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                        }
                    }
                }
                System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                object BaseObject = BaseField.GetValue(BusinessDocument);

                System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo P in BaseProperties)
                {
                    if (row.Table.Columns.Contains(P.Name))
                    {
                        if (!(row.IsNull(P.Name)))
                        {
                            P.SetValue(BaseObject, row[P.Name].ToString(), null);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void LoadComponentsFromBusinessObject(EREC_KuldTertivevenyek erec_KuldTertivevenyek)
    {
        if (erec_KuldTertivevenyek != null)
        {
            BarCode.Text = erec_KuldTertivevenyek.BarCode;

            TertivisszaDat_CalendarControl.Text = erec_KuldTertivevenyek.TertivisszaDat;

            Ragszam.Text = erec_KuldTertivevenyek.Ragszam;

            if (!String.IsNullOrEmpty(erec_KuldTertivevenyek.TertivisszaKod))
            {
                TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_VISSZA_KOD, erec_KuldTertivevenyek.TertivisszaKod, FormHeader1.ErrorPanel);
            }

            Allapot_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_ALLAPOT, erec_KuldTertivevenyek.Allapot, FormHeader1.ErrorPanel);

            txtAtvevoSzemely.Text = erec_KuldTertivevenyek.AtvevoSzemely;

            AtvetelDat_CalendarControl.Text = erec_KuldTertivevenyek.AtvetelDat;

            // A Note mez�ben jelenleg az �tv�tel jogc�m�t tartjuk nyilv�n
            txtNote_AtvetelJogcime.Text = erec_KuldTertivevenyek.Base.Note;

            //aktu�lis verzi� elt�rol�sa
            FormHeader1.Record_Ver = erec_KuldTertivevenyek.Base.Ver;

            FormPart_CreatedModified1.SetComponentValues(erec_KuldTertivevenyek.Base);

            //bernat.laszlo added:
            #region K�zbes�t�s v�lelem be�llta
            if (string.IsNullOrEmpty(erec_KuldTertivevenyek.KezbVelelemBeallta.ToString()))
            {
                KezbVelelemBe_Nem_RadioButton.Checked = true;
            }
            else
            {
                if (erec_KuldTertivevenyek.KezbVelelemBeallta == "1")
                {
                    KezbVelelemBe_Igen_RadioButton.Checked = true;
                }
                else
                {
                    KezbVelelemBe_Nem_RadioButton.Checked = true;
                }
            }
            #endregion

            KezbVelelemDatum_CalendarControl.Text = erec_KuldTertivevenyek.KezbVelelemDatuma;
            //bernat.laszlo eddig
        }
    }

    private EREC_KuldTertivevenyek GetBusinessObjectFromComponents()
    {
        EREC_KuldTertivevenyek erec_KuldTertivevenyek = new EREC_KuldTertivevenyek();

        erec_KuldTertivevenyek.Updated.SetValueAll(false);
        erec_KuldTertivevenyek.Base.Updated.SetValueAll(false);

        erec_KuldTertivevenyek.TertivisszaDat = TertivisszaDat_CalendarControl.Text;
        erec_KuldTertivevenyek.Updated.TertivisszaDat = pageView.GetUpdatedByView(TertivisszaDat_CalendarControl);

        erec_KuldTertivevenyek.TertivisszaKod = TertivisszaKod_KodtarakDropDownList.SelectedValue;
        erec_KuldTertivevenyek.Updated.TertivisszaKod = pageView.GetUpdatedByView(TertivisszaKod_KodtarakDropDownList);

        erec_KuldTertivevenyek.Ragszam = Ragszam.Text;
        erec_KuldTertivevenyek.Updated.Ragszam = pageView.GetUpdatedByView(Ragszam);

        erec_KuldTertivevenyek.AtvevoSzemely = txtAtvevoSzemely.Text;
        erec_KuldTertivevenyek.Updated.AtvevoSzemely = pageView.GetUpdatedByView(txtAtvevoSzemely);

        erec_KuldTertivevenyek.AtvetelDat = AtvetelDat_CalendarControl.Text;
        erec_KuldTertivevenyek.Updated.AtvetelDat = pageView.GetUpdatedByView(AtvetelDat_CalendarControl);

        erec_KuldTertivevenyek.Allapot = Allapot_KodtarakDropDownList.SelectedValue;
        erec_KuldTertivevenyek.Updated.Allapot = pageView.GetUpdatedByView(Allapot_KodtarakDropDownList);

        //bernat.laszlo added
        #region K�zbes�t�s v�lelem be�llta
        if (KezbVelelemBe_Igen_RadioButton.Checked)
        {
            erec_KuldTertivevenyek.KezbVelelemBeallta = "1";
            erec_KuldTertivevenyek.Updated.KezbVelelemBeallta = pageView.GetUpdatedByView(KezbVelelemBe_Igen_RadioButton);
        }
        else
        {
            erec_KuldTertivevenyek.KezbVelelemBeallta = "0";
            erec_KuldTertivevenyek.Updated.KezbVelelemBeallta = pageView.GetUpdatedByView(KezbVelelemBe_Igen_RadioButton);
        }
        #endregion

        erec_KuldTertivevenyek.KezbVelelemDatuma = KezbVelelemDatum_CalendarControl.Text;
        erec_KuldTertivevenyek.Updated.KezbVelelemDatuma = pageView.GetUpdatedByView(KezbVelelemDatum_CalendarControl);
        //bernat.laszlo eddig

        // �tv�tel jogc�me a megjegyz�s (Note) mez�be
        erec_KuldTertivevenyek.Base.Note = txtNote_AtvetelJogcime.Text;
        erec_KuldTertivevenyek.Base.Updated.Note = pageView.GetUpdatedByView(txtNote_AtvetelJogcime);

        erec_KuldTertivevenyek.Base.Ver = FormHeader1.Record_Ver;
        erec_KuldTertivevenyek.Base.Updated.Ver = true;

        return erec_KuldTertivevenyek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(BarCode);
            compSelector.Add_ComponentOnClick(TertivisszaDat_CalendarControl);
            compSelector.Add_ComponentOnClick(Ragszam);
            compSelector.Add_ComponentOnClick(TertivisszaKod_KodtarakDropDownList);
            compSelector.Add_ComponentOnClick(txtAtvevoSzemely);
            compSelector.Add_ComponentOnClick(AtvetelDat_CalendarControl);
            compSelector.Add_ComponentOnClick(txtNote_AtvetelJogcime);
            compSelector.Add_ComponentOnClick(Allapot_KodtarakDropDownList);
            //bernat.laszlo added: K�zbes�t�s metaadatok
            compSelector.Add_ComponentOnClick(KezbVelelemBe_Igen_RadioButton);
            compSelector.Add_ComponentOnClick(KezbVelelemBe_Nem_RadioButton);
            compSelector.Add_ComponentOnClick(KezbVelelemDatum_CalendarControl);
            //bernat.laszlo eddig
            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //if (FunctionRights.GetFunkcioJog(Page, "KuldTertiveveny" + Command))
            if (FunctionRights.GetFunkcioJog(Page, "KuldTertivevenyErkeztetes"))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                            EREC_KuldTertivevenyek erec_KuldTertivevenyek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_KuldTertivevenyek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, BarCode.Text))
                                {
                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                    if (!String.IsNullOrEmpty(refreshCallingWindow)
                                        && refreshCallingWindow == "1"
                                        )
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                                EREC_KuldTertivevenyek erec_KuldTertivevenyek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = null;
                                if (erec_KuldTertivevenyek.Allapot == KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
                                {
                                    // az el�k�sz�tettet "�rkeztetj�k"
                                    result = service.TertivevenyErkeztetes(execParam, recordId, erec_KuldTertivevenyek);
                                }
                                else
                                {
                                    result = service.Update(execParam, erec_KuldTertivevenyek);
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
    /// <summary>
    /// Set tertiveveny image button to open file
    /// </summary>
    /// <param name="dataRow"></param>
    private void SetTertivevenyImage(DataRow dataRow)
    {
        if (dataRow.Table.Columns.Contains("Tertiveveny_Dokumentum_Id"))
        {
            string tertivevenyDokumentumId = dataRow["Tertiveveny_Dokumentum_Id"].ToString();
            if (!String.IsNullOrEmpty(tertivevenyDokumentumId))
            {
                string function = string.Format("window.open('GetDocumentContent.aspx?id={0}'); return true;", tertivevenyDokumentumId);
                ImageButtonTertivevenyKep.Attributes.Add("onclick", function);
                ImageButtonTertivevenyKep.Visible = true;
            }
            else
            {
                ImageButtonTertivevenyKep.Visible = false;
            }
        }
    }
}
