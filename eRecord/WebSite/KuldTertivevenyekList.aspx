<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KuldTertivevenyekList.aspx.cs" Inherits="KuldTertivevenyekList" Title="Untitled Page" %>

<%@ Register Src="Component/CimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="ud11" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="ud12" %>
<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList" TagPrefix="ud10" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="ud8" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="ud5" %>
<%@ Register Src="Component/DateTimeCompareValidator.ascx" TagName="DateTimeCompareValidator" TagPrefix="refonly" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ud7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="ud1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="ud2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="ud4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="ud9" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="ud13" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <%--HiddenFields--%>
    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="updatePanelKuldTertivevenyek"
                    runat="server"
                    OnLoad="updatePanelKuldTertivevenyek_Load">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelBarcodeRagszam" runat="server" Text="Vonalk�d:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ud13:VonalKodTextBox ID="BarCode" runat="server" Validate="false"></ud13:VonalKodTextBox>
                                        <ud13:VonalKodTextBox ID="TextBoxRagszamSearch" runat="server" Validate="false" Visible="false"></ud13:VonalKodTextBox>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <%-- asp:Image ID="ImageAdd"
														 runat="server"
														 ImageUrl="~/images/hu/egyeb/kispipa.jpg"
														 AlternateText="Ok" / --%>
                                    </td>
                                    <td class="mrUrlapCaption_short">&nbsp;</td>
                                    <td class="mrUrlapMezo">&nbsp;</td>
                                </tr>
                                <tr runat="server" id="tr_TertivisszaDat" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="LabelTertvisszaDat" runat="server" Text="Vissza�rkez�s d�tuma:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ud5:CalendarControl ID="TertivisszaDat_CalendarControl" runat="server"
                                            Validate="false" TimeVisible="false" bOpenDirectionTop="true" />
                                    </td>
                                    <td class="mrUrlapMezo" colspan="2">&nbsp;</td>
                                    <%--									<td rowspan="2" >
                                        <asp:ImageButton ID="ImageButtonTertivevenyErkeztetes" runat="server" 
                                            ImageUrl="~/images/hu/ovalgomb/rendben.jpg" 
                                            onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" 
                                            CommandName="Save" onclick="ImageButtonTertivevenyErkeztetes_Click"/>
                                    </td>--%>
                                    <td style="width: 30%"></td>
                                    <%--
									<td class="mrUrlapMezo" >
										<ud13:VonalKodTextBox ID="Ragszam" runat="server" Validate="false"></ud13:VonalKodTextBox>
									</td>--%>
                                </tr>
                                <tr runat="server" id="tr_TertiVisszaKod" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="LabelTertivisszaKodStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label12" runat="server" Text="K�zbes�t�s eredm�nye:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="TertivisszaKod_KodtarakDropDownList" AutoPostBack="true"
                                            runat="server" />
                                    </td>
                                    <td class="mrUrlapMezo">&nbsp;</td>
                                    <%--<td class="mrUrlapCaption_short">
										<asp:Label ID="LabelTertivisszaAllapotStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
										<asp:Label ID="LabelTertivisszaAllapot" runat="server" Text="�llapot:"></asp:Label>
									</td>
									<td class="mrUrlapMezo" >
										<ktddl:KodtarakDropDownList ID="TertivisszaAllapot" width="150" runat="server" />
									</td>--%>
                                </tr>
                                <tr runat="server" id="tr_AtvevoSzemely" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelAtvevoSzemely" runat="server" Text="�tvev� szem�ly:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="txtAtvevoSzemely" runat="server" CssClass="mrUrlapInput" />
                                    </td>

                                </tr>
                                <tr runat="server" id="tr_AtvetelDat" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="LabelTertvisszaDatStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelAtvetelDat" runat="server" Text="�tv�tel id�pontja:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ud5:CalendarControl ID="AtvetelDat_CalendarControl" runat="server"
                                            Validate="true" TimeVisible="true" bOpenDirectionTop="true" />

                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Note_AtvetelJogcime" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNote_AtvetelJogcime" runat="server" Text="�tv�tel jogc�me:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="txtNote_AtvetelJogcime" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_Submit" visible="false">
                                    <td class="mrUrlapMezo">&nbsp;</td>
                                    <td class="mrUrlapMezo">
                                        <asp:ImageButton ID="ImageButtonTertivevenyErkeztetes" runat="server"
                                            ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                            onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                            CommandName="Save" OnClick="ImageButtonTertivevenyErkeztetes_Click" />
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeKuldTertivevenyek"
                            runat="server"
                            TargetControlID="panelKuldTertivevenyek"
                            CollapsedSize="20"
                            Collapsed="False"
                            ExpandControlID="btnCpeKuldTertivevenyek"
                            CollapseControlID="btnCpeKuldTertivevenyek"
                            ExpandDirection="Vertical"
                            AutoCollapse="false"
                            AutoExpand="false"
                            CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="btnCpeKuldTertivevenyek"
                            ExpandedSize="0"
                            ExpandedText="bla"
                            CollapsedText="blabla">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelKuldTertivevenyek" runat="server" Width="100%">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top; width: 0px;">
                                        <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeKuldTertivevenyek"
                                            OnClientClick="return false;" />
                                    </td>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="gridViewKuldTertivevenyek" runat="server" GridLines="None" BorderWidth="1px"
                                            AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" DataKeyNames="Id"
                                            AutoGenerateColumns="False" AllowPaging="true" OnRowCommand="gridViewKuldTertivevenyek_RowCommand"
                                            OnPreRender="gridViewKuldTertivevenyek_PreRender" OnRowDataBound="gridViewKuldTertivevenyek_RowDataBound"
                                            OnSorting="gridViewKuldTertivevenyek_Sorting">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
														<asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Cimzett_Nev_Partner" HeaderText="C�mzett" SortExpression="IsNull(EREC_PldIratPeldanyok.CimSTR_Cimzett, EREC_KuldKuldemenyek.CimSTR_Bekuldo)">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cimzett_Cim_Partner" HeaderText="C�mzett&nbsp;c�me" SortExpression="IsNull(EREC_PldIratPeldanyok.NevSTR_Cimzett, EREC_KuldKuldemenyek.NevSTR_Bekuldo)">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d" SortExpression="EREC_KuldTertivevenyek.BarCode">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--BLG_237--%>
                                                <asp:TemplateField HeaderText="Postai azonos�t�" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <%-- iktatott �llapot eset�n link az iratra --%>
                                                        <asp:Label ID="labelRagszam" runat="server" Text='<%#Eval("Ragszam").ToString().Length>20?String.Concat(Eval("Ragszam").ToString().Substring(0,20),"..."):Eval("Ragszam").ToString() %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:BoundField DataField="RagSzam" HeaderText="Postai azonos�t�" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:BoundField DataField="BelyegzoDatuma_rovid" HeaderText="Felad�s&nbsp;ideje"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="EREC_KuldKuldemenyek.BelyegzoDatuma">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertivisszaDat_rovid" HeaderText="Vissza�rk.ideje"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="EREC_KuldTertivevenyek.TertivisszaDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertivisszaNev" HeaderText="K�zb.eredm�nye" SortExpression="KRT_KodTarak_TERTIVEVENY_VISSZA_KOD.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertivevenyAllapotNev" HeaderText="T�rtiv.�llapot" SortExpression="KRT_KodTarak_TERTIVEVENY_ALLAPOT.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AtvevoSzemely" HeaderText="�tvev�" SortExpression="EREC_KuldTertivevenyek.AtvevoSzemely" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AtvetelDat" DataFormatString="{0:yyyy.MM.dd}" HtmlEncodeFormatString="true"
                                                    HeaderText="�tv�tel ideje" SortExpression="EREC_KuldTertivevenyek.AtvetelDat" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Note" HeaderText="�tv�tel jogc�me" SortExpression="EREC_KuldTertivevenyek.Note" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelTertivevenyAllapot" runat="server" CssClass="ReqStar" Text='<%# Eval("Allapot") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

</asp:Content>
