﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
//using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KuldTertivevenyekList : Contentum.eUtility.UI.PageBase
{
    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    private string funkcioKod_KuldTertivevenyekList = "KuldTertivevenyekList";
    private string funkcioKod_KuldTertivevenyErkeztetes = "KuldTertivevenyErkeztetes";
    private string funkcioKod_KuldTertivevenyView = "KuldTertivevenyView";
    private string kcs_TERTIVEVENY_VISSZA_KOD = "TERTIVEVENY_VISSZA_KOD";
    UI ui = new UI();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod_KuldTertivevenyekList);
    }

    #region Page functions

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }


    //LZS - BUG_6532
    private bool AtvetelDatumaVisibility()
    {
        //LZS - BUG_12725
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH)
        {
            switch (TertivisszaKod_KodtarakDropDownList.SelectedValue)
            {
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_cim_nem_azonosithato:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_nem_kereste:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_elkoltozott:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_bejelentve_meghalt_megszunt:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_cimzett_ismeretlen:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_atvetelt_megtagadta:
                case KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldés_oka_kezbesites_akadalyozott:
                    return false;

                default:
                    return true;
            }
        }
        else
            return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //ne lehessen többször kattintani szkript
        ImageButtonTertivevenyErkeztetes.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButtonTertivevenyErkeztetes);

        //Keresési objektum típusának megadása
        ListHeader1.SearchObjectType = typeof(EREC_KuldTertivevenyekSearch);

        //Fõ lista megjelenések testreszabása
        ListHeader1.HeaderLabel = Resources.List.KuldTertivevenyekLisHeader;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = true;
        ListHeader1.ModifyVisible = true;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false; // mivel egyelõre nincs "KuldTertivevenyViewHistory" jog

        ListHeader1.SendObjectsVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        //Fõ lista gombokhoz kliens oldali szkriptek regisztrálása
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KuldTertivevenyekSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelKuldTertivevenyek.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewKuldTertivevenyek.ClientID);
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewKuldTertivevenyek.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewKuldTertivevenyek.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewKuldTertivevenyek.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = gridViewKuldTertivevenyek;

        //Fõ lista összes kiválasztása és az összes kiválasztás megszüntetése checkbox-aihoz szkriptek beállítása
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKuldTertivevenyek);

        //Fõ lista eseménykezelõ függvényei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztrálása
        JavaScripts.RegisterPopupWindowClientScript(Page);
        //JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        //ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //Hibaüzenet panel elüntetése
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire jó */
        //Search.SetIdsToSearchObject(Page, "Id", new EREC_KuldTertivevenyekSearch());
        Search.SetIdsToSearchObject(Page, "Id", new EREC_KuldTertivevenyekSearch(true));

        //A fõ lista adatkötés, ha nem postback a kérés
        if (!IsPostBack)
        {
            KuldTertivevenyekGridViewBind();
        }

        ////a beviteli form beállításai
        //if (!IsPostBack)
        //{
        //    TertivisszaDat_CalendarControl.SetToday();
        //    TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_VISSZA_KOD, KodTarak.TERTIVEVENY_VISSZA_KOD.Cimzett_atvette, EErrorPanel1);
        //}

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        registerJavascripts();
        SetSearchInputTitle();
        SetSearchInputVisibility();

        // BUG_4790
        JavaScripts.SetOnClientEnterPressed(EFormPanel1, ClientScript.GetPostBackEventReference(ImageButtonTertivevenyErkeztetes, "click"));
    }

    // BUG_4790
    private bool _isAfterTertivevenyErkeztetes;

    // BUG_7922
    private bool IsTomegesTertivevenyErkeztetes()
    {
        return Contentum.eRecord.Utility.FelhasznaloProfil.OrgIs(this, Constants.OrgKod.FPH) ? Request.QueryString.Get("Tomeges") == "1" : true;
    }

    //
    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Fõ lista jogosultságainak beállítása
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, funkcioKod_KuldTertivevenyView);//FunctionRights.GetFunkcioJog(Page, "KuldTertiveveny" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, funkcioKod_KuldTertivevenyErkeztetes);//FunctionRights.GetFunkcioJog(Page, "KuldTertiveveny" + CommandName.Modify); ;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, funkcioKod_KuldTertivevenyErkeztetes);//FunctionRights.GetFunkcioJog(Page, "KuldTertiveveny" + CommandName.ViewHistory);

        // kiválasztott sor alapján engedélyezzük/tiltjuk a form elemeit
        ClearFormComponents();

        // BUG_4790, 7922
        bool enableComponents = IsTomegesTertivevenyErkeztetes() && _isAfterTertivevenyErkeztetes;

        //LZS - BUG_6532
        bool enableAtvetelDatuma = AtvetelDatumaVisibility();

        if (gridViewKuldTertivevenyek.SelectedRow != null)
        {
            // ha nem módosíthat, nem jelenítjük meg a panelt
            if (FunctionRights.GetFunkcioJog(Page, funkcioKod_KuldTertivevenyErkeztetes))
            {
                Label labelAllapot = (Label)gridViewKuldTertivevenyek.SelectedRow.FindControl("LabelTertivevenyAllapot");
                if (labelAllapot != null)
                {
                    string allapot = Server.UrlDecode(labelAllapot.Text.Trim());
                    if (allapot == KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
                    {
                        enableComponents = true;
                    }
                }
            }

            SetSearchInputSelected();
        }

        tr_TertivisszaDat.Visible = enableComponents;
        tr_TertiVisszaKod.Visible = enableComponents;
        tr_AtvetelDat.Visible = enableComponents && enableAtvetelDatuma;
        tr_AtvevoSzemely.Visible = enableComponents;
        tr_Note_AtvetelJogcime.Visible = enableComponents;
        //LZS - BUG_6532
        tr_Submit.Visible = enableComponents;

        // BUG_4790
        if (IsPostBack && _isAfterTertivevenyErkeztetes)
        {
            BarCode.Text = "";
            TextBoxRagszamSearch.Text = "";
            AtvetelDat_CalendarControl.DisableCompareValidation();
            UI.ClearGridViewRowSelection(gridViewKuldTertivevenyek);
        }

        //if (IsPostBack)
        //{
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewKuldTertivevenyek);
        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        //}
    }

    #endregion

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        SetSearchInputFocus();

        StringBuilder sb = new StringBuilder("");
        sb.Append("<script language='JavaScript1.2'>");
        sb.Append("function VK(p_vk){");
        sb.Append(SetSearchInputPostBackParam());
        sb.Append("return 0;");
        sb.Append("}");
        sb.Append("</script>");
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Script1", "" + sb.ToString());

        SetSearchInputPostKeyUpEventJs();
    }

    //adatkötés meghívása default értékekkel
    protected void KuldTertivevenyekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewKuldTertivevenyek", ViewState, "EREC_KuldTertivevenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewKuldTertivevenyek", ViewState, SortDirection.Descending);
        KuldTertivevenyekGridViewBind(sortExpression, sortDirection);
    }

    //adatkötés webszolgáltatástól kapott adatokkal
    protected void KuldTertivevenyekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldTertivevenyekSearch search = (EREC_KuldTertivevenyekSearch)Search.GetSearchObject(Page, new EREC_KuldTertivevenyekSearch(true));

        // Azokat kell hozni, ahol a Küldemény állapota Postázott
        search.Extended_EREC_KuldKuldemenyekSearch.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Postazott;
        search.Extended_EREC_KuldKuldemenyekSearch.Allapot.Operator = Query.Operators.equals;

        search.OrderBy = Search.GetOrderBy("gridViewKuldTertivevenyek", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(execParam, ListHeader1);

        Result res = service.GetAllWithExtension(execParam, search);

        UI.GridViewFill(gridViewKuldTertivevenyek, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    //A GridView RowDataBound esemény kezelõje, ami egy sor adatkötése után hívódik
    protected void gridViewKuldTertivevenyek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok beállítása
        /*if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbKarbantarthato = (CheckBox)e.Row.FindControl("cbKarbantarthato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            SetCheckBox(cbKarbantarthato, drw["Karbantarthato"].ToString());
        }*/

        //Lockolás jelzése
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender eseménykezelõje
    protected void gridViewKuldTertivevenyek_PreRender(object sender, EventArgs e)
    {
        //int prev_PageIndex = gridViewKuldTertivevenyek.PageIndex;

        ////oldalszámok beállítása
        //gridViewKuldTertivevenyek.PageIndex = ListHeader1.PageIndex;

        ////lapozás esetén adatok frissítése
        //if (prev_PageIndex != gridViewKuldTertivevenyek.PageIndex )
        //{
        //    //scroll állapotának törlése
        //    JavaScripts.ResetScroll(Page, cpeKuldTertivevenyek);
        //    KuldTertivevenyekGridViewBind();
        //    //ActiveTabClear();
        //}
        //else
        //{
        //    UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeKuldTertivevenyek);
        //}

        ////select,deselect szkriptek
        //ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKuldTertivevenyek);

        ////oldalszámok beállítása
        //ListHeader1.PageCount = gridViewKuldTertivevenyek.PageCount;
        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewKuldTertivevenyek);

        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeKuldTertivevenyek);
        ListHeader1.RefreshPagerLabel();

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKuldTertivevenyek);

    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KuldTertivevenyekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeKuldTertivevenyek);
        KuldTertivevenyekGridViewBind();
    }

    //GridView RowCommand eseménykezelõje, amit valamelyik sorban fellépett parancs vált ki
    protected void gridViewKuldTertivevenyek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiválasztása (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewKuldTertivevenyek, selectedRowNumber, "check");

            string id = gridViewKuldTertivevenyek.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewKuldTertivevenyek_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KuldTertivevenyekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKuldTertivevenyek.ClientID, EventArgumentConst.refreshMasterList);


            //módosíthatóság ellenõrzése
            bool modosithato = true;//getModosithatosag(gridViewKuldTertivevenyek);
            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("KuldTertivevenyekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKuldTertivevenyek.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            string tableName = "EREC_KuldTertivevenyek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelKuldTertivevenyek.ClientID);


            //LZS BUG_5036 - Meghívjuk a GetFeladasIdejeFromSelectedRow(id) függvényt, hogy kinyerjük a Feladás idejét DB-ben [BelyegzoDatuma].
            //Ez a validációhoz szükséges.
            GetFeladasIdejeFromSelectedRow(id);
        }
    }

    //UpdatePanel Load eseménykezelõje
    protected void updatePanelKuldTertivevenyek_Load(object sender, EventArgs e)
    {
        //Fõ lista frissítése
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KuldTertivevenyekGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewKuldTertivevenyek));                    
                    break;
                case "VanVonalkod":
                    SetSelectedRowByBarCode();
                    KuldTertivevenyekGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewKuldTertivevenyek));                    
                    break;
                case "VanRagszam":
                    SetSelectedRowByRagszam();
                    KuldTertivevenyekGridViewBind();
                    break;
            }
        }
    }

    private void SetSelectedRowByBarCode()
    {
        if (!String.IsNullOrEmpty(BarCode.Text))
        {
            ClearFormComponents();

            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch(true);

            search.BarCode.Value = BarCode.Text;
            search.BarCode.Operator = Query.Operators.equals;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                string tertivevenyId = result.Ds.Tables[0].Rows[0]["Id"].ToString();

                ListHeader1.SelectedRecordId = tertivevenyId;
            }
        }
    }

    private void SetSelectedRowByRagszam()
    {
        if (!String.IsNullOrEmpty(TextBoxRagszamSearch.Text))
        {
            ClearFormComponents();

            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch(true);

            search.Ragszam.Value = TextBoxRagszamSearch.Text;
            search.Ragszam.Operator = Query.Operators.equals;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                string tertivevenyId = result.Ds.Tables[0].Rows[0]["Id"].ToString();

                ListHeader1.SelectedRecordId = tertivevenyId;
            }
        }
    }

    #region BUG_5036
    //LZS - Lekérjük a kiválasztott EREC_KuldTertivevenyek objektum id-ja alapján a hozzá tartozó Kuldemeny_Id-t, 
    //majd ennek a Kuldemeny_Id-nak a segítségével lekérjük a Feladás idejét, amelyet átadunk a validátoroknak.
    //A megfelelő hibaüzeneteket is itt állítjuk be.
    private void GetFeladasIdejeFromSelectedRow(string id)
    {
        string kuldemenyId = "";
        string feladasIdeje = "";

        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch(true);

        search.Id.Value = id;
        search.Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
            return;
        }

        if (result.Ds.Tables[0].Rows.Count == 1)
        {
            kuldemenyId = result.Ds.Tables[0].Rows[0]["Kuldemeny_Id"].ToString();
        }


        if (!string.IsNullOrEmpty(kuldemenyId))
        {
            EREC_KuldKuldemenyekService serviceKuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_KuldKuldemenyekSearch searchKuldemenyek = new EREC_KuldKuldemenyekSearch(true);

            searchKuldemenyek.Id.Value = kuldemenyId;
            searchKuldemenyek.Id.Operator = Query.Operators.equals;

            ExecParam execParamKuldemenyek = UI.SetExecParamDefault(Page, new ExecParam());

            Result resultKuldemenyek = serviceKuldemenyek.GetAll(execParamKuldemenyek, searchKuldemenyek);
            if (!String.IsNullOrEmpty(resultKuldemenyek.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultKuldemenyek);
                ErrorUpdatePanel.Update();
                return;
            }

            if (resultKuldemenyek.Ds.Tables[0].Rows.Count == 1)
            {
                feladasIdeje = resultKuldemenyek.Ds.Tables[0].Rows[0]["BelyegzoDatuma"].ToString();
            }

            //Átadás a validátoroknak
            AtvetelDat_CalendarControl.EnableCompareValidation();
            AtvetelDat_CalendarControl.DateTimeCompareValidator_1.Operator = ValidationCompareOperator.LessThanEqual;
            AtvetelDat_CalendarControl.DateTimeCompareValidator_1.ValueToCompare = DateTime.Today.ToShortDateString();
            AtvetelDat_CalendarControl.DateTimeCompareValidator_1.DateCompareCustomValdator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_RecieveToday; //"Az átvétel időpontja nem lehet későbbi a mai napnál!";

            AtvetelDat_CalendarControl.DateTimeCompareValidator_2.Operator = ValidationCompareOperator.GreaterThanEqual;
            AtvetelDat_CalendarControl.DateTimeCompareValidator_2.ValueToCompare = Convert.ToDateTime(feladasIdeje).ToShortDateString();
            AtvetelDat_CalendarControl.DateTimeCompareValidator_2.DateCompareCustomValdator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_Receive;


            TertivisszaDat_CalendarControl.EnableCompareValidation();
            TertivisszaDat_CalendarControl.DateTimeCompareValidator_1.Operator = ValidationCompareOperator.LessThanEqual;
            TertivisszaDat_CalendarControl.DateTimeCompareValidator_1.ValueToCompare = DateTime.Today.ToShortDateString();
            TertivisszaDat_CalendarControl.DateTimeCompareValidator_1.DateCompareCustomValdator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_TakeToday; //"A visszaérkezés időpontja nem lehet későbbi a mai napnál!";

            TertivisszaDat_CalendarControl.DateTimeCompareValidator_2.Operator = ValidationCompareOperator.GreaterThanEqual;
            TertivisszaDat_CalendarControl.DateTimeCompareValidator_2.ValueToCompare = Convert.ToDateTime(feladasIdeje).ToShortDateString();
            TertivisszaDat_CalendarControl.DateTimeCompareValidator_2.DateCompareCustomValdator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_Take;//"A visszaérkezés időpontja nem lehet korábbi a feladás időpontjánál!";
        }

    }
    #endregion

    //Fõ lista bal oldali funkciógombjainak eseménykezelõje
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ////törlés parancs
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    //deleteSelectedKuldTertivevenyek();
        //    KuldTertivevenyekGridViewBind();
        //}
    }


    //Fõ lista jobb oldali funkciógombjainak eseménykezelõje
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail küldés parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKuldTertivevenyekRecords();
                KuldTertivevenyekGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKuldTertivevenyekRecords();
                KuldTertivevenyekGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedKuldTertivevenyek();
                break;
        }
    }

    //kiválasztott elemek zárolása
    private void lockSelectedKuldTertivevenyekRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewKuldTertivevenyek, "EREC_KuldTertivevenyek"
            , "KuldTertivevenyekLock", "KuldTertivevenyekForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiválasztott emlemek elengedése
    /// </summary>
    private void unlockSelectedKuldTertivevenyekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewKuldTertivevenyek, "EREC_KuldTertivevenyek"
            , "KuldTertivevenyekLock", "KuldTertivevenyekForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewKuldTertivevenyek -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedKuldTertivevenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewKuldTertivevenyek, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_KuldTertivevenyek");
        }
    }

    //GridView Sorting esménykezelõje
    protected void gridViewKuldTertivevenyek_Sorting(object sender, GridViewSortEventArgs e)
    {
        KuldTertivevenyekGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewKuldTertivevenyek", ViewState, e.SortExpression));
    }


    private EREC_KuldTertivevenyek GetBusinessObjectFromComponents()
    {
        EREC_KuldTertivevenyek tertivevenyObj = new EREC_KuldTertivevenyek();

        tertivevenyObj.Updated.SetValueAll(false);
        tertivevenyObj.Base.Updated.SetValueAll(false);

        tertivevenyObj.TertivisszaDat = TertivisszaDat_CalendarControl.Text;
        tertivevenyObj.Updated.TertivisszaDat = true;

        tertivevenyObj.TertivisszaKod = TertivisszaKod_KodtarakDropDownList.SelectedValue;
        tertivevenyObj.Updated.TertivisszaKod = true;

        tertivevenyObj.AtvevoSzemely = txtAtvevoSzemely.Text;
        tertivevenyObj.Updated.AtvevoSzemely = true;

        //LZS - BUG_12725
        if (tr_AtvetelDat.Visible)
        {
            tertivevenyObj.AtvetelDat = AtvetelDat_CalendarControl.Text;
            tertivevenyObj.Updated.AtvetelDat = true;
        }

        // átvétel jogcíme a megjegyzés (Note) mezõbe
        tertivevenyObj.Base.Note = txtNote_AtvetelJogcime.Text;
        tertivevenyObj.Base.Updated.Note = true;

        return tertivevenyObj;
    }

    private void ClearFormComponents()
    {
        //a beviteli form beállításai
        TertivisszaDat_CalendarControl.SetToday();

        if (!IsPostBack)
        {
            TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_VISSZA_KOD, KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek, EErrorPanel1);
            AtvetelDat_CalendarControl.Text = String.Empty; // BUG_4790
        }
        else if (!IsTomegesTertivevenyErkeztetes()) // BUG_7922: tömeges érkeztetésnél meg kell hagyni az átvétel dátumát
        {
            AtvetelDat_CalendarControl.Text = String.Empty;
        }

        //LZS - BUG_6532
        //TertivisszaKod_KodtarakDropDownList.SetSelectedValue(KodTarak.TERTIVEVENY_VISSZA_KOD.Cimzett_atvette);

        txtAtvevoSzemely.Text = String.Empty;

        // A Note mezõben jelenleg az átvétel jogcímét tartjuk nyilván
        txtNote_AtvetelJogcime.Text = String.Empty;

    }

    /// <summary>
    /// Tértivevény érkeztetés
    /// </summary>
    protected void ImageButtonTertivevenyErkeztetes_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, funkcioKod_KuldTertivevenyErkeztetes))
        {
            string tertivevenyId = UI.GetGridViewSelectedRecordId(gridViewKuldTertivevenyek);
            if (String.IsNullOrEmpty(tertivevenyId))
            {
                return;
            }

            EREC_KuldTertivevenyek tertivevenyObj = GetBusinessObjectFromComponents();

            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.TertivevenyErkeztetes(execParam, tertivevenyId, tertivevenyObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }
            else
            {
                // lista frissítés:
                _isAfterTertivevenyErkeztetes = true; // BUG_4790
                KuldTertivevenyekGridViewBind();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }
    /// <summary>
    /// SetSearchInputTitle
    /// </summary>
    private void SetSearchInputTitle()
    {
        string param = Rendszerparameterek.Get(Page, Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES);
        if (string.IsNullOrEmpty(param))
        {
            labelBarcodeRagszam.Text = Resources.Form.KuldTertiveveny_Keres_Vonalkod;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM))
        {
            labelBarcodeRagszam.Text = Resources.Form.KuldTertiveveny_Keres_Ragszam;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_VONALKOD))
        {
            labelBarcodeRagszam.Text = Resources.Form.KuldTertiveveny_Keres_Vonalkod;
        }
        else
        {
            labelBarcodeRagszam.Text = Resources.Form.KuldTertiveveny_Keres_Vonalkod;
        }
    }

    /// <summary>
    /// SetSearchInputVisibility
    /// </summary>
    private void SetSearchInputVisibility()
    {
        string param = Rendszerparameterek.Get(Page, Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES);
        if (string.IsNullOrEmpty(param))
        {
            BarCode.Visible = true;
            TextBoxRagszamSearch.Visible = false;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM))
        {
            BarCode.Visible = false;
            TextBoxRagszamSearch.Visible = true;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_VONALKOD))
        {
            BarCode.Visible = true;
            TextBoxRagszamSearch.Visible = false;
        }
        else
        {
            BarCode.Visible = true;
            TextBoxRagszamSearch.Visible = false;
        }
    }
    /// <summary>
    /// SetSearchInputFocus
    /// </summary>
    private void SetSearchInputFocus()
    {
        string param = Rendszerparameterek.Get(Page, Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES);
        var control = string.IsNullOrEmpty(param) || !param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM) ?
            BarCode.TextBox : TextBoxRagszamSearch.TextBox;
        JavaScripts.SetFocusOnEnd(control);
    }

    /// <summary>
    /// SetSearchInputPostKeyUpEventJs
    /// </summary>
    private void SetSearchInputPostKeyUpEventJs()
    {
        // BUG_8925
        //BarCode.PostKeyUpEventJs = "if(this.value.length == 13){VK(this.value);this.value = '';}else{}";
        //TextBoxRagszamSearch.PostKeyUpEventJs = "if(this.value.length >= 16){VK(this.value);this.value = '';}else{}";
        BarCode.PostKeyUpEventJs = "if(barCodeBehaviorParam.get_element().value.length >= barCodeBehaviorParam.barcodeLength-1){VK(barCodeBehaviorParam.get_element().value);}else{}";
        TextBoxRagszamSearch.PostKeyUpEventJs = "if(barCodeBehaviorParam.get_element().value.length >= barCodeBehaviorParam.barcodeLength-1){VK(barCodeBehaviorParam.get_element().value);}else{}";

    }
    /// <summary>
    /// SetSearchInputSelected
    /// </summary>
    private void SetSearchInputSelected()
    {
        string param = Rendszerparameterek.Get(Page, Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES);
        if (string.IsNullOrEmpty(param))
        {
            string selectedBarCode = UI.GetGridViewColumnText(gridViewKuldTertivevenyek.SelectedRow, "BarCode").Replace("&nbsp;", "");
            BarCode.Text = selectedBarCode;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM))
        {
            string selectedBarCode = UI.GetGridViewColumnText(gridViewKuldTertivevenyek.SelectedRow, "Ragszam").Replace("&nbsp;", "");
            TextBoxRagszamSearch.Text = selectedBarCode;
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_VONALKOD))
        {
            string selectedBarCode = UI.GetGridViewColumnText(gridViewKuldTertivevenyek.SelectedRow, "BarCode").Replace("&nbsp;", "");
            BarCode.Text = selectedBarCode;
        }
        else
        {
            string selectedBarCode = UI.GetGridViewColumnText(gridViewKuldTertivevenyek.SelectedRow, "BarCode").Replace("&nbsp;", "");
            BarCode.Text = selectedBarCode;
        }
    }
    /// <summary>
    /// SetSearchInputPostBackParam
    /// </summary>
    /// <returns></returns>
    private string SetSearchInputPostBackParam()
    {
        string param = Rendszerparameterek.Get(Page, Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES);
        if (string.IsNullOrEmpty(param))
        {
            return "__doPostBack('" + updatePanelKuldTertivevenyek.ClientID + "','VanVonalkod');";
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM))
        {
            return "__doPostBack('" + updatePanelKuldTertivevenyek.ClientID + "','VanRagszam');";
        }
        else if (param.Equals(Rendszerparameterek.DEFAULT_TERTIVEVENY_KERESES_ERTEK_VONALKOD))
        {
            return "__doPostBack('" + updatePanelKuldTertivevenyek.ClientID + "','VanVonalkod');";
        }
        else
        {
            return "__doPostBack('" + updatePanelKuldTertivevenyek.ClientID + "','VanVonalkod');";
        }
    }


}