<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KuldTertivevenyekSearch.aspx.cs" Inherits="KuldTertivevenyekSearch"
    Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="csp" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/KimenoKuldemenyFajta_SearchFormControl.ascx" TagName="KimenoKuldemenyFajta_SearchFormControl"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <%-- T�rtivev�ny adatai: --%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:VonalKodTextBox ID="VonalKodTextBox1" runat="server" RequiredValidate="false"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAllapot" runat="server" Text="T�rtivev�ny �llapot:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right: 15px;">
                                <uc4:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTertivisszaKod" runat="server" Text="K�zbes�t�s eredm�nye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:KodtarakDropDownList ID="TertivisszaKod_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTertvisszaDat" runat="server" Text="Vissza�rkez�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="TertivisszaDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" TimeVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvevoSzemely" runat="server" Text="�tvev� szem�ly:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtAtvevoSzemely" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvetelDat" runat="server" Text="�tv�tel d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="AtvetelDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" TimeVisible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNote_AtvetelJogcime" runat="server" Text="�tv�tel jogc�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtNote_AtvetelJogcime" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <%-- Kimen� k�ldem�ny adatai: --%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBelyegzoDatuma" runat="server" Text="Felad�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="BelyegzoDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="False" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPostakonyv" runat="server" Text="Postak�nyv:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList_Postakonyv" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label22" runat="server" Text="C�mzett:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc2:PartnerTextBox ID="Cimzett_PartnerTextBox" runat="server" SearchMode="True"
                                    Validate="False" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label23" runat="server" Text="C�mzett c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CimekTextBox ID="CimzettCime_CimekTextBox" runat="server" SearchMode="True"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKikuldo" runat="server" Text="Kik�ld�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <csp:CsoportTextBox ID="Kikuldo_CsoportTextBox" SzervezetCsoport="true" Validate="false"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label24" runat="server" Text="Postai azonos�t�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <%--BUG_8402--%>
                                <%--<asp:TextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>--%>
                                 <uc5:VonalKodTextBox ID="Ragszam_TextBox" runat="server" RequiredValidate="false"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="OrzoLabel" runat="server" CssClass="mrUrlapCaption" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:FelhasznaloTextBox ID="OrzoTextBox" runat="server" NewImageButtonVisible="false"
                                    SearchMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label30" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="FelelosLabel" runat="server" CssClass="mrUrlapCaption" Text="Kezel�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <csp:CsoportTextBox ID="Felelos_CsoportTextBox" runat="server" SearchMode="true"
                                    Validate="False" />
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKuldesMod" runat="server" Text="K�ld�sm�d:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right: 15px;">
                                <uc4:KodtarakDropDownList ID="KuldesMod_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="K�ldem�ny fajt�ja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:KimenoKuldemenyFajta_SearchFormControl ID="KimenoKuldemenyFajta_SearchFormControl1" runat="server"/>
                                <asp:CheckBox ID="Elsobbsegi_CheckBox" runat="server" Text="Els�bbs�gi" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" rowspan="2">
                                <asp:Label ID="labelSzolgaltatasok" runat="server" Text="Szolg�ltat�sok:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <%--                                <asp:CheckBox ID="Ajanlott_CheckBox" runat="server" Text="Aj�nlott" />
                                <asp:CheckBox ID="Tertiveveny_CheckBox" runat="server" Text="T�rtivev�ny" />--%>
                                <asp:CheckBox ID="SajatKezbe_CheckBox" runat="server" Text="Saj�t k�zbe" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="E_Ertesites_CheckBox" runat="server" Text="E-�rtes�t�s" />
                                <asp:CheckBox ID="E_Elorejelzes_CheckBox" runat="server" Text="E-el�rejelz�s" />
                                <asp:CheckBox ID="PostaiLezaroSzolgalat_CheckBox" runat="server" Text="Postai lez�r� szolg�lat" />
                            </td>
                        </tr>
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label27" runat="server" Text="�r:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:SzamIntervallum_SearchFormControl ID="Ar_SzamIntervallum_SearchFormControl" 
                                    runat="server" />
                            </td>
                        </tr>--%>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
