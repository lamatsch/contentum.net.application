﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class KuldTertivevenyekSearch : Contentum.eUtility.UI.PageBase
{
    private const string kcs_KIMENO_KULDEMENY_FAJTA = "KIMENO_KULDEMENY_FAJTA";
    private const string kcs_TERTIVEVENY_ALLAPOT = "TERTIVEVENY_ALLAPOT";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_TERTIVEVENY_VISSZA_KOD = "TERTIVEVENY_VISSZA_KOD";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        SearchHeader1.HeaderTitle = Resources.Search.KuldTertivevenyekSearchHeaderTitle;
        SearchHeader1.TemplateObjectType = typeof(EREC_KuldTertivevenyekSearch);

        // Partner címének átmásolásához:
        Cimzett_PartnerTextBox.CimTextBox = CimzettCime_CimekTextBox.TextBox;
        Cimzett_PartnerTextBox.CimHiddenField = CimzettCime_CimekTextBox.HiddenField;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillIktatoKonyvekList();

            EREC_KuldTertivevenyekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, typeof(EREC_KuldTertivevenyekSearch)))
            {
                searchObject = (EREC_KuldTertivevenyekSearch)Search.GetSearchObject(Page, new EREC_KuldTertivevenyekSearch(true));
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    private void FillIktatoKonyvekList()
    {
        // (Postakönyv jelenleg nem zárható le)
        // egyelőre minden postakönyvet beleteszünk a listába, mert tértivevényeknél nincs jogosultságszűrés
        #region Postakönyvek lekérése jogosultságszűrés nélkül
        IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList_SetIdToValues(Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv
            , false, "", "", true
            , false, SearchHeader1.ErrorPanel);
        #endregion Postakönyvek lekérése jogosultságszűrés nélkül

        // (Postakönyv jelenleg nem zárható le)
        //IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv, true, SearchHeader1.ErrorPanel);
    }

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_KuldTertivevenyekSearch tertivevenySearch = null;
        if (searchObject != null) tertivevenySearch = (EREC_KuldTertivevenyekSearch)searchObject;

        if (tertivevenySearch != null)
        {
            Allapot_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_ALLAPOT, tertivevenySearch.Allapot.Value
                , true, SearchHeader1.ErrorPanel);

            TertivisszaKod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TERTIVEVENY_VISSZA_KOD, tertivevenySearch.TertivisszaKod.Value
                , true, SearchHeader1.ErrorPanel);

            VonalKodTextBox1.Text = tertivevenySearch.BarCode.Value;

            TertivisszaDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(tertivevenySearch.TertivisszaDat);

            AtvetelDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(tertivevenySearch.AtvetelDat);

            txtAtvevoSzemely.Text = tertivevenySearch.AtvevoSzemely.Value;

            // A Note mezőben jelenleg az átvétel jogcímét tároljuk
            txtNote_AtvetelJogcime.Text = tertivevenySearch.Manual_Note.Value;

            LoadComponentsFromSearchObject_KimenoKuldemeny(tertivevenySearch.Extended_EREC_KuldKuldemenyekSearch);
        }
    }

    private void LoadComponentsFromSearchObject_KimenoKuldemeny(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        if (erec_KuldKuldemenyekSearch != null)
        {
            BelyegzoDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_KuldKuldemenyekSearch.BelyegzoDatuma);

            IraIktatoKonyvekDropDownList_Postakonyv.SetSelectedValue(erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value);

            // címzett

            Cimzett_PartnerTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value;
            Cimzett_PartnerTextBox.Text = erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;

            CimzettCime_CimekTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Cim_Id.Value;
            CimzettCime_CimekTextBox.Text = erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value;

            Kikuldo_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value;
            Kikuldo_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //VonalKodTextBox1.Text = erec_KuldKuldemenyekSearch.BarCode.Value;
            
            Ragszam_TextBox.Text = erec_KuldKuldemenyekSearch.RagSzam.Value;

            OrzoTextBox.Id_HiddenField = erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Value;
            OrzoTextBox.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            Megjegyzes_TextBox.Text = erec_KuldKuldemenyekSearch.Note.Value;

            //KimenoKuldFajta_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KIMENO_KULDEMENY_FAJTA, erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta.Value
            //    , true, SearchHeader1.ErrorPanel);

            KimenoKuldemenyFajta_SearchFormControl1.LoadComponentFromSearchObjectField(erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta);

            KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA, erec_KuldKuldemenyekSearch.KuldesMod.Value
                , true, SearchHeader1.ErrorPanel);

            Elsobbsegi_CheckBox.Checked = (erec_KuldKuldemenyekSearch.Elsobbsegi.Value == "1") ? true : false;

            //Ajanlott_CheckBox.Checked = (erec_KuldKuldemenyekSearch.Ajanlott.Value == "1") ? true : false;
            //Tertiveveny_CheckBox.Checked = (erec_KuldKuldemenyekSearch.Tertiveveny.Value == "1") ? true : false;
            SajatKezbe_CheckBox.Checked = (erec_KuldKuldemenyekSearch.SajatKezbe.Value == "1") ? true : false;
            E_Ertesites_CheckBox.Checked = (erec_KuldKuldemenyekSearch.E_ertesites.Value == "1") ? true : false;
            E_Elorejelzes_CheckBox.Checked = (erec_KuldKuldemenyekSearch.E_elorejelzes.Value == "1") ? true : false;
            PostaiLezaroSzolgalat_CheckBox.Checked = (erec_KuldKuldemenyekSearch.PostaiLezaroSzolgalat.Value == "1") ? true : false;

            //Ar_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_KuldKuldemenyekSearch.Ar);
        }
    
    }



    private EREC_KuldTertivevenyekSearch SetSearchObjectFromComponents()
    {
        EREC_KuldTertivevenyekSearch tertivevenyekSearch = (EREC_KuldTertivevenyekSearch)SearchHeader1.TemplateObject;

        if (tertivevenyekSearch == null)
        {
            tertivevenyekSearch = GetDefaultSearchObject();//new EREC_KuldTertivevenyekSearch(true);
        }

        if (!String.IsNullOrEmpty(VonalKodTextBox1.Text))
        {
            tertivevenyekSearch.BarCode.Value = VonalKodTextBox1.Text;
            tertivevenyekSearch.BarCode.Operator = Search.GetOperatorByLikeCharater(VonalKodTextBox1.Text);
        }

        if (!String.IsNullOrEmpty(Allapot_KodtarakDropDownList.SelectedValue))
        {
            tertivevenyekSearch.Allapot.Value = Allapot_KodtarakDropDownList.SelectedValue;
            tertivevenyekSearch.Allapot.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(TertivisszaKod_KodtarakDropDownList.SelectedValue))
        {
            tertivevenyekSearch.TertivisszaKod.Value = TertivisszaKod_KodtarakDropDownList.SelectedValue;
            tertivevenyekSearch.TertivisszaKod.Operator = Query.Operators.equals;
        }

        TertivisszaDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(tertivevenyekSearch.TertivisszaDat);

        AtvetelDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(tertivevenyekSearch.AtvetelDat);

        if (!String.IsNullOrEmpty(txtAtvevoSzemely.Text))
        {
            tertivevenyekSearch.AtvevoSzemely.Value = txtAtvevoSzemely.Text;
            tertivevenyekSearch.AtvevoSzemely.Operator = Search.GetOperatorByLikeCharater(txtAtvevoSzemely.Text);
        }

        if (!String.IsNullOrEmpty(txtNote_AtvetelJogcime.Text))
        {
            // A Note mezőben jelenleg az átvétel jogcímét tároljuk
            tertivevenyekSearch.Manual_Note.Value = txtNote_AtvetelJogcime.Text;
            tertivevenyekSearch.Manual_Note.Operator = Search.GetOperatorByLikeCharater(txtNote_AtvetelJogcime.Text);
        }

        tertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch = SetSearchObjectFromComponents_KimenoKuldemeny();

        return tertivevenyekSearch;
    }


    private EREC_KuldKuldemenyekSearch SetSearchObjectFromComponents_KimenoKuldemeny()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = null;

        EREC_KuldTertivevenyekSearch tertivevenyekSearch = (EREC_KuldTertivevenyekSearch)SearchHeader1.TemplateObject;
        if (tertivevenyekSearch != null && tertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
        {
            erec_KuldKuldemenyekSearch = tertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch;
        }

        if (erec_KuldKuldemenyekSearch == null)
        {
            erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch(true);
        }

        BelyegzoDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_KuldKuldemenyekSearch.BelyegzoDatuma);

        if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
            erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
        }

        // Címzett

        if (!String.IsNullOrEmpty(Cimzett_PartnerTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = Cimzett_PartnerTextBox.Text;
            erec_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Cimzett_PartnerTextBox.Text);
        }

        if (!String.IsNullOrEmpty(CimzettCime_CimekTextBox.Text))
        {
            erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value = CimzettCime_CimekTextBox.Text;
            erec_KuldKuldemenyekSearch.CimSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(CimzettCime_CimekTextBox.Text);
        }

        if (!String.IsNullOrEmpty(Kikuldo_CsoportTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value = Kikuldo_CsoportTextBox.Id_HiddenField;
            erec_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Operator = Query.Operators.equals;
        }


        if (!String.IsNullOrEmpty(Ragszam_TextBox.Text))
        {
            erec_KuldKuldemenyekSearch.RagSzam.Value = Ragszam_TextBox.Text;
            erec_KuldKuldemenyekSearch.RagSzam.Operator = Search.GetOperatorByLikeCharater(Ragszam_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(OrzoTextBox.Id_HiddenField))
        {
            erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Value = OrzoTextBox.Id_HiddenField;
            erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Megjegyzes_TextBox.Text))
        {
            erec_KuldKuldemenyekSearch.Note.Value = Megjegyzes_TextBox.Text;
            erec_KuldKuldemenyekSearch.Note.Operator = Search.GetOperatorByLikeCharater(Megjegyzes_TextBox.Text);
        }

        KimenoKuldemenyFajta_SearchFormControl1.SetSearchObjectFieldFromComponent(erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta);

        //if (!String.IsNullOrEmpty(Allapot_KodtarakDropDownList.SelectedValue))
        //{
        //    erec_KuldKuldemenyekSearch.Allapot.Value = Allapot_KodtarakDropDownList.SelectedValue;
        //    erec_KuldKuldemenyekSearch.Allapot.Operator = Query.Operators.equals;
        //}

        if (!String.IsNullOrEmpty(KuldesMod_KodtarakDropDownList.SelectedValue))
        {
            erec_KuldKuldemenyekSearch.KuldesMod.Value = KuldesMod_KodtarakDropDownList.SelectedValue;
            erec_KuldKuldemenyekSearch.KuldesMod.Operator = Query.Operators.equals;
        }

        if (Elsobbsegi_CheckBox.Checked)
        {
            erec_KuldKuldemenyekSearch.Elsobbsegi.Value = "1";
            erec_KuldKuldemenyekSearch.Elsobbsegi.Operator = Query.Operators.equals;
        }

        //if (Ajanlott_CheckBox.Checked)
        //{
        //    erec_KuldKuldemenyekSearch.Ajanlott.Value = "1";
        //    erec_KuldKuldemenyekSearch.Ajanlott.Operator = Query.Operators.equals;
        //}
        //if (Tertiveveny_CheckBox.Checked)
        //{
        //    erec_KuldKuldemenyekSearch.Tertiveveny.Value = "1";
        //    erec_KuldKuldemenyekSearch.Tertiveveny.Operator = Query.Operators.equals;
        //}
        if (SajatKezbe_CheckBox.Checked)
        {
            erec_KuldKuldemenyekSearch.SajatKezbe.Value = "1";
            erec_KuldKuldemenyekSearch.SajatKezbe.Operator = Query.Operators.equals;
        }
        if (E_Ertesites_CheckBox.Checked)
        {
            erec_KuldKuldemenyekSearch.E_ertesites.Value = "1";
            erec_KuldKuldemenyekSearch.E_ertesites.Operator = Query.Operators.equals;
        }
        if (E_Elorejelzes_CheckBox.Checked)
        {
            erec_KuldKuldemenyekSearch.E_elorejelzes.Value = "1";
            erec_KuldKuldemenyekSearch.E_elorejelzes.Operator = Query.Operators.equals;
        }
        if (PostaiLezaroSzolgalat_CheckBox.Checked)
        {
            erec_KuldKuldemenyekSearch.PostaiLezaroSzolgalat.Value = "1";
            erec_KuldKuldemenyekSearch.PostaiLezaroSzolgalat.Operator = Query.Operators.equals;
        }

        //Ar_SzamIntervallum_SearchFormControl.SetSearchObjectFields(erec_KuldKuldemenyekSearch.Ar);

        return erec_KuldKuldemenyekSearch;
    }


    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
                SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_KuldTertivevenyekSearch searchObject = SetSearchObjectFromComponents();

            EREC_KuldTertivevenyekSearch baseSearchObject = new EREC_KuldTertivevenyekSearch(true);

            //EREC_PldIratPeldanyokSearch base_PldSearch = new EREC_PldIratPeldanyokSearch();
            //base_PldSearch.ErvKezd.Clear();
            //base_PldSearch.ErvVege.Clear();

            if (Search.IsIdentical(searchObject, baseSearchObject)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, baseSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                //&& (searchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch == null
                //    || Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch
                //                        , base_PldSearch)
                                        //)
                )
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, typeof(EREC_KuldTertivevenyekSearch));
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            // Feleslegesen ne küldjünk le annyi adatot, ha már úgyis bezárjuk az ablakot:
            EFormPanel1.Visible = false;
        }

    }

    private EREC_KuldTertivevenyekSearch GetDefaultSearchObject()
    {
        return new EREC_KuldTertivevenyekSearch(true);
    }
}
