﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class LezarasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    string UgyiratId = "";
    private String[] UgyiratokArray;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_UgyiratLezaras = "UgyiratLezaras";

    public string maxTetelszam = "0";

    private UI ui = new UI();

    private bool TomegesLezarasEnabled { get; set; }

    private const String kcs_LEZARAS_OKA = "LEZARAS_OKA";

    bool LezarasOkaKotelezo
    {
        get;
        set;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (String.IsNullOrEmpty(UgyiratId))
        {
            UgyiratId = Session["SelectedUgyiratIds"] as string;
            TomegesLezarasEnabled = true;
        }
        else
        {
            TomegesLezarasEnabled = false;
        }

        if (!String.IsNullOrEmpty(UgyiratId))
        {
            UgyiratokArray = UgyiratId.Split(',');
        }


        // Jogosultságellenõrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratLezaras);
                break;
        }


        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
        LezarasOkaKotelezo = Rendszerparameterek.GetBoolean(Page, "LEZARAS_OKA_KOTELEZO", false);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.DisableModeLabel = true;


        FormHeader1.HeaderTitle = Resources.Form.UgyiratLezarasFormHeaderTitle;


        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (TomegesLezarasEnabled)
        {
            FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                   + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                   + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

            labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
        }

        //LZS - BUG_1367
        LezarasDat_CalendarControl.EnableCompareValidation();
        LezarasDat_CalendarControl.DateTimeCompareValidator_1.Operator = ValidationCompareOperator.LessThanEqual;
        LezarasDat_CalendarControl.DateTimeCompareValidator_1.ValueToCompare = DateTime.Today.ToShortDateString();
        LezarasDat_CalendarControl.DateTimeCompareValidator_1.DateCompareCustomValdator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_CloseToday; //"A lezárás időpontja nem lehet későbbi a mai napnál!";
        LezarasDat_CalendarControl.DateTimeCompareValidator_2.Enabled = false;
    }



    private void LoadFormComponents()
    {
        // BUG 5041 - tömeges lezárás űrlapra is kerüljön fel a lezárás oka mező
        tr_LezarasOka.Visible = true;
        LezarasOka_KodtarakDropDownList.Validate = LezarasOkaKotelezo;
        labelLezaraOkaReq.Visible = LezarasOkaKotelezo;

        panelTomegesMode.Visible = TomegesLezarasEnabled;
        panelSimpleMode.Visible = !TomegesLezarasEnabled;

        //
        if (TomegesLezarasEnabled)
        {
            if (UgyiratokArray != null && UgyiratokArray.Length > 0)
            {
                FillUgyiratokGridView();
            }
            else
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
        }
        else
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                EREC_UgyUgyiratok erec_UgyUgyiratok = UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                if (erec_UgyUgyiratok == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    MainPanel.Visible = false;
                    return;
                }
                else
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page);
                    ErrorDetails errorDetail = null;

                    // Ellenõrzés, lezárható-e:
                    Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
                    if (!Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                            ResultError.CreateNewResultWithErrorCode(52261, errorDetail));
                        MainPanel.Visible = false;
                        return;

                    }

                }
            }
        }


        LezarasDat_CalendarControl.Text = DateTime.Now.ToString();
        LezarasOka_KodtarakDropDownList.FillDropDownList(kcs_LEZARAS_OKA, true, FormHeader1.ErrorPanel);

    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellenõrzés:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem vehetõ át? 
        // CheckBoxok vizsgálatával:
        int count_lezarhatoak = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_nem_lezarhatoak = UgyiratokArray.Length - count_lezarhatoak;

        if (count_nem_lezarhatoak > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_LezarasWarning, count_nem_lezarhatoak);
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

            search.Id.In(UgyiratokArray);

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UgyiratokGridView_RowDataBound_CheckLezaras(e);
    }

    public void UgyiratokGridView_RowDataBound_CheckLezaras(GridViewRowEventArgs e)
    {
        if (e == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Ellenõrzés, lezárható-e:
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);

            ExecParam execParam = UI.SetExecParamDefault(Page);
            ErrorDetails errorDetail;
            bool lezarhato = Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail);
            UI.SetRowCheckboxAndInfo(lezarhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, ugyiratStatusz.Id, Page);
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratLezaras))
            {
                if (TomegesLezarasEnabled)
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }
                    String[] UgyiratIds = selectedItemsList.ToArray();
                    Result result = service.Lezaras_Tomeges(execParam.Clone(), UgyiratIds, LezarasDat_CalendarControl.Text, LezarasOka_KodtarakDropDownList.SelectedValue);
                    if (!string.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                    }
                    else
                    {
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(UgyiratId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        // Lezárás

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result result = service.Lezaras(execParam, UgyiratId, LezarasDat_CalendarControl.Text, LezarasOka_KodtarakDropDownList.SelectedValue);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                    }
                }

            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }


}
