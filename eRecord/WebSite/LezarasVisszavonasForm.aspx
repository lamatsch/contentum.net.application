<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="LezarasVisszavonasForm.aspx.cs" Inherits="LezarasVisszavonasForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/UgyiratDarabMasterAdatok.ascx" TagName="UgyiratDarabMasterAdatok"
    TagPrefix="uc11" %>

<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList"
    TagPrefix="uc10" %>

<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc6" %>

<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc10" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>

    <!--Friss�t�s jelz�se-->
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />    

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,LezarasVisszavonasFormHeaderTitle %>" />

    <asp:Panel ID="MainPanel" runat="server">
        <uc:InfoModalPopup ID="InfoModalPopup1" runat="server" />

        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                        <table cellpadding="0" cellspacing="0" width="90%">
                            <tr>
                                <td>
                                    <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                        BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                        OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                        AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <HeaderStyle CssClass="GridViewHeaderStyle"/>  
                    
                        <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />                               
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                    </ItemTemplate>
                                </asp:TemplateField>    
                            
                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                    <ItemTemplate>                                    
                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                            Visible="false" OnClientClick="return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>      
                             <asp:TemplateField HeaderText="Iratt�r" Visible="false">
                               <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label_IrattarTipus" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" 
                                    SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>                                                
                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito"  >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev"  >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>                        
                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>                                        
                        <PagerSettings Visible="False" />
                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                                        <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                         </table>
                </td>
            </tr>
            <tr>
                <td>                  
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
     </asp:Panel>
</asp:Content>

