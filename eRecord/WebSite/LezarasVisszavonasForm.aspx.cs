using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class LezarasVisszavonasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    string UgyiratId = "";
    private String[] UgyiratokArray;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_UgyiratLezarasVisszavonas = "UgyiratLezarasVisszavonas";

    public string maxTetelszam = "0";

    private UI ui = new UI();

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (String.IsNullOrEmpty(UgyiratId))
        {
            UgyiratId = Session["SelectedUgyiratIds"] as string;
        }

        if (!String.IsNullOrEmpty(UgyiratId))
        {
            UgyiratokArray = UgyiratId.Split(',');
        }


        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratLezarasVisszavonas);
                break;
        }


        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }


        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                   + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                   + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());

    }


    private void LoadFormComponents()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            FillUgyiratokGridView();
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_lezarhatoak = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_nem_lezarhatoak = UgyiratokArray.Length - count_lezarhatoak;

        if (count_nem_lezarhatoak > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_LezarasVisszavonasWarning, count_nem_lezarhatoak);
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UgyiratokGridView_RowDataBound_CheckLezarasVisszavonas(e);
    }

    public void UgyiratokGridView_RowDataBound_CheckLezarasVisszavonas(GridViewRowEventArgs e)
    {
        if (e == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Ellen�rz�s, lez�rhat�-e:
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
            ExecParam execParam = UI.SetExecParamDefault(Page);
            ErrorDetails errorDetail;
            bool lezarasVisszavonhato = Ugyiratok.LezarasVisszavonhato(ugyiratStatusz, execParam, out errorDetail);
            UI.SetRowCheckboxAndInfo(lezarasVisszavonhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, ugyiratStatusz.Id, Page);
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratLezarasVisszavonas))
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                if (selectedItemsList.Count == 0)
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                    return;
                }
                String[] UgyiratIds = selectedItemsList.ToArray();
                Result result = service.LezarasVisszavonas_Tomeges(execParam.Clone(), UgyiratIds);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }

            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }


}
