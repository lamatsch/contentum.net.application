<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><asp:Literal  runat="server" ID="LoginTitle" Text="<%$Resources:Form,LoginPageTitle%>"></asp:Literal></title>
    <base target="_self" />
</head>
<body>
    <script language="JavaScript" type="text/javascript">

    function swapByName(obj, newImageName) {
    var myobj = document.getElementById(obj); 
    if (myobj) 
    {
        var fileNameStartIndex = myobj.src.lastIndexOf("/");
        var newUrl = myobj.src.substring(0,fileNameStartIndex+1)+newImageName;
        
        myobj.src = newUrl;    
    }
    }
    </script>
    
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="ScriptManager1" runat="server" 
        EnableScriptGlobalization="True"
        EnableScriptLocalization="true" >
        </asp:ScriptManager>
        <center>
        <table style="width: 400px; height: 400px">
            <tbody>
                <tr>
                    <td valign="middle" align="center">
                        <eUI:eFormPanel id="EFormPanel1" runat="server">
                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <table id="table_baseLogin" runat="server">
                            <tbody>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label id="AuthLabel" runat="server" Text="Hitelesítés:"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:CheckBox id="LoginType" runat="server" Text="Automatikus" OnCheckedChanged="LoginType_CheckedChanged" Checked="True" AutoPostBack="True"></asp:CheckBox></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label id="UserNameLabel" runat="server" Text="Felhasználói név:"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:TextBox id="UserName" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label id="PasswordLabel" runat="server" Text="Jelszó:"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:TextBox id="Password" runat="server" Enabled="false" TextMode="Password"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_Felhasznalo" runat="server" UseContextKey="true"
                                                ServicePath="~/WrappedWebService/Ajax.asmx" ServiceMethod="GetLoginFelhasznalok"
                                                Category="Felhasznalo" Enabled="true" TargetControlID="DropDownList_Felhasznalo"
                                                EmptyText="---" EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]"
                                                PromptText="<Felhasználó kiválasztása>" PromptValue="">
                        </ajaxToolkit:CascadingDropDown>
                        <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_CsoportTagsag" runat="server" TargetControlID="DropDownList_Csoport"
                                                ParentControlID="DropDownList_Felhasznalo" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax.asmx"
                                                ServiceMethod="GetCsoportTagsagokByFelhasznalo" Category="CsoportTagsag" Enabled="true" EmptyText="---"
                                                EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Csoport kiválasztása>"
                                                PromptValue="">
                        </ajaxToolkit:CascadingDropDown>
                        <table id="table_extraLogin" runat="server">
                            <tbody>
                                <tr>
                                    <td style="text-align: right">
                                    </td>
                                    <td class="mrUrlapCaption" style="text-align: center;vertical-align:top;height:20px;">
                                        <asp:Label id="labelHeader" runat="server" Text="Szerepválasztás" />
                                    </td>
                                    <td></td>
                                </tr>                                                             
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label id="labelFelhasznalo" runat="server" Text="Kinek a nevében:" CssClass="formLoaderCaption"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList id="DropDownList_Felhasznalo" runat="server" CssClass="formLoaderComboBox" Width="255px" Visible="true"></asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>                            
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label id="labelCsoport" runat="server" Text="Csoport:" CssClass="formLoaderCaption"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList id="DropDownList_Csoport" runat="server" CssClass="formLoaderComboBox" Width="255px" Visible="true"></asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                    </td>
                                    <td style="text-align: left;height:14px">
                                        <asp:Label id="labelInfo" Text="&nbsp;" runat="server"/></td>
                                </tr>
                            </tbody>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        <table style="width: 100%">
                            <tbody>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:ImageButton ID="LoginButton" runat="server" ImageUrl="~/images/hu/ovalgomb/bejelentkezes.gif" 
                                            onmouseover="swapByName(this.id,'bejelentkezes2.gif')" onmouseout="swapByName(this.id,'bejelentkezes.gif')"
                                            OnClick="LoginButton_Click"
                                            AlternateText="Bejelentkezés" /></td>
<%--                                    <td style="text-align: left">
                                        <asp:ImageButton ID="OwnButton" runat="server"
                                        ImageUrl="~/images/hu/ovalgomb/sajat_magam_neveben.gif"
                                        onmouseover="swapByName(this.id,'sajat_magam_neveben2.gif')"
                                        onmouseout="swapByName(this.id,'sajat_magam_neveben.gif')"
                                        OnClick="OwnButton_Click"
                                        AlternateText="Saját magam nevében"/>
                                    <td style="text-align: left">
                                        <asp:ImageButton ID="ElseButton" runat="server" 
                                        ImageUrl="~/images/hu/ovalgomb/mas_neveben.gif" 
                                        onmouseover="swapByName(this.id,'mas_neveben2.gif')"
                                        onmouseout="swapByName(this.id,'mas_neveben.gif')"
                                        OnClick="ElseButton_Click"
                                        AlternateText="Más nevében"/>--%>
                                    <td style="text-align: center">
                                        <asp:ImageButton ID="LoginByListButton" runat="server" ImageUrl="~/images/hu/ovalgomb/bejelentkezes.gif"
                                            onmouseover="swapByName(this.id,'bejelentkezes2.gif')" onmouseout="swapByName(this.id,'bejelentkezes.gif')"
                                            OnClick="LoginByListButton_Click"
                                            AlternateText="Bejelentkezés" /></td>
                                </tr>
                            </tbody>
                        </table>
                        </eUI:eFormPanel> 
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </div>
    </form>
</body>
</html>
