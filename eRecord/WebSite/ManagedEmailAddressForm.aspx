<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ManagedEmailAddressForm.aspx.cs" Inherits="ManagedEmailAddressForm"
    Title="Automatikus email �rkeztet�s/iktat�s kezel�s" ValidateRequest="false" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc9" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc8" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,ManagedEmailAddressNewHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <asp:HiddenField runat="server" ID="HiddenFieldId" />
            <asp:HiddenField runat="server" ID="HiddenFieldErvKezd" />
            <asp:HiddenField runat="server" ID="HiddenFieldErvVege" />
            <asp:HiddenField runat="server" ID="HiddenFieldFelhasznalo_id" />
            <asp:HiddenField runat="server" ID="HiddenFieldKarbantarthato" />
            <asp:HiddenField runat="server" ID="HiddenFieldNev" />
            <asp:HiddenField runat="server" ID="HiddenFieldOrg" />
            <%--BUG_14076--%>
            <asp:HiddenField runat="server" ID="HiddenFieldImapPsw" />
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Azonos�t� email c�m:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="TextBoxIdentityEmailAddress" runat="server" ReadOnly="true" Width="95%" />
                        </td>
                    </tr>
                    <%--IMAP--%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap email c�m:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxImapEmailAddress" Width="95%" />
                            <%-- <asp:TextBox ID="TextBoxImapEmailAddress" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap szerver c�m:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxImapServer" Width="95%" />
                            <%-- <asp:TextBox ID="TextBoxImapServer" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap szerver port:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxImpaServerPort" Width="95%" />
                            <%--<asp:TextBox ID="TextBoxImpaServerPort" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap szerverhez kell SSL?:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="CheckBoxImapUseSSL" runat="server" Width="95%" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap felhaszn�l�n�v:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxImapLoginUser" Width="95%" />
                            <%-- <asp:TextBox ID="TextBoxImapLoginUser" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Imap jelsz�:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <%--  <uc5:RequiredTextBox runat="server" ID="TextBoxImapLoginPassword" Width="95%"/>--%>
                            <asp:TextBox ID="TextBoxImapLoginPassword" runat="server" TextMode="Password" Width="95%" />
                        </td>
                    </tr>
                    <%--   --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Dokumentum Feldolgozas Tipusa"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList runat="server" ID="DropDownListDokumentumFeldolgozasTipusa" Width="95%" AutoPostBack="true">
                                <asp:ListItem Value="1" Text="�rkeztet�s">�rkeztet�s</asp:ListItem>
                                <asp:ListItem Value="2" Text="�rkeztet�s �s iktat�s">�rkeztet�s �s iktat�s</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--   --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="V�lasz email automatikusan"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="CheckBoxValaszEmailAutomatikusan" runat="server" Width="95%" />
                        </td>
                    </tr>
                    <%-- <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Felel�s felhaszn�l�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="TextBoxResponsible" runat="server" />
                            <uc7:CsoportTextBox ID="CsoportTextBox1" runat="server" Validate="false" Width="95%" />
                        </td>
                    </tr>--%>
                    <%-- <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Technikai felhaszn�l� nev�ben"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="CheckBoxTechnikaiFelhasznaloNeveben" runat="server" Width="95%" />
                        </td>
                    </tr>--%>
                    <%-- IDS  --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="�rkeztet� k�nyv azonos�t�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxKeresesiParameterErkeztetoKonyvAzonosito"
                                Width="95%" />
                            <%--   <asp:TextBox ID="TextBoxKeresesiParameterErkeztetoKonyvAzonosito" runat="server"
                                Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Iktat� k�nyv azonos�t�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxKeresesiParameterIktatoKonyvAzonosito"
                                Width="95%" />
                            <%--  <asp:TextBox ID="TextBoxKeresesiParameterIktatoKonyvAzonosito" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label7" runat="server" Text="Iratt�ri t�tel azonos�t�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxKeresesiParameterIrattariTetelAzonosito"
                                Width="95%" />
                            <%-- <asp:TextBox ID="TextBoxKeresesiParameterIratt�riT�telAzonos�t�" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <%-- EXEC PARAM --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Felhaszn�l� azonos�t�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc8:FelhasznaloTextBox ID="FelhasznaloTextBoxId" runat="server" Width="95%" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="FelhasznaloSzervezet azonos�t�"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="CsoportTextBoxFelhasznaloSzervezetId" runat="server" Width="95%" />
                        </td>
                    </tr>
                    <%-- RESPONSE EMAIL --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="V�lasz lev�l k�ld� mail c�me"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxValaszEmailFeladoCim" Width="95%" />
                            <%--  <asp:TextBox ID="TextBoxValaszEmailFeladoCim" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="V�lasz lev�l t�rgy mez�je"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxValaszEmailTargy" Width="95%" />
                            <%-- <asp:TextBox ID="TextBoxValaszEmailTargy" runat="server" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label8" runat="server" Text="�rkeztet�s v�lasz lev�l sablonja"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxValaszEmailSablonErkeztetes" TextMode="MultiLine"
                                Rows="10" Width="95%" />
                            <%--  <asp:TextBox ID="TextBoxValaszEmailSablonErkeztetes" TextMode="MultiLine" runat="server"
                                Rows="10" Width="95%" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Iktat�s v�lasz lev�l sablonja"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox runat="server" ID="TextBoxValaszEmailSablonIktatas" TextMode="MultiLine"
                                Rows="10" Width="95%" />
                            <%--  <asp:TextBox ID="TextBoxValaszEmailSablonIktatas" TextMode="MultiLine" runat="server"
                                Rows="10" Width="95%" />--%>
                        </td>
                    </tr>
                    <%-- KULDEMENY --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Kuldemenyek Csoport Id Cimzett"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="CsoportTextBoxKuldemenyek_Csoport_Id_Cimzett" runat="server"
                                Width="95%" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Kuldemenyek Csoport Id Felelos"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="CsoportTextBoxKuldemenyek_Csoport_Id_Felelos" runat="server"
                                Width="95%" />
                        </td>
                    </tr>
                    <%-- UGYIRAT  --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="UgyUgyiratok Csoport Id Felelos"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos" runat="server"
                                Width="95%" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="Iratt�ri T�tel"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList runat="server" ID="DropDownListUgyUgyiratok_iraIrattariTetel_Id"
                                Width="95%" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="UgyUgyiratok felhasznaloCsoport Ugyintez"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc10:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez"
                                runat="server" Width="95%" />
                        </td>
                    </tr>
                    <%-- IRAIRATOK --%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="IraIratok FelhasznaloCsoport Ugyintez"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc10:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez"
                                runat="server" Width="95%" />
                        </td>
                    </tr>
                    <tr id="rowIktatasiParameter" runat="server" class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" Text="IktatasiParameterek Ugykor / Iratt�riT�tel"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList runat="server" ID="DropDownListIktatasiParameterek_UgykorId" Width="95%" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
