﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI.WebControls;

public partial class ManagedEmailAddressForm : Contentum.eUtility.UI.PageBase
{
    #region PROPERTIES
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    #endregion

    #region PAGE EVENTS
    protected void Page_PreInit(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Az oldal Init eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BarkodSav" + Command);
                //TODO: a valódi jogokhoz kötni! (ld. előző sor, de a listán is figyelni kellene)
                // csak ideiglenes biztonsági intézkedés, majd 
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ManagedEmailAddressList");
                break;
        }
        FormHeader1.HeaderTitle = "Automatikus email érkeztetés/iktatás kezelés";
        if (Command == CommandName.New)
        {
            FormHeader1.ModeText = "Felvitel";
            LoadComponents(string.Empty, string.Empty);
            ErkezIktatSetup(DropDownListDokumentumFeldolgozasTipusa.SelectedValue);
        }
        else
        {
            if (Command == CommandName.View)
            {
                FormHeader1.ModeText = "Megtekintés";
                String id = Request.QueryString.Get(QueryStringVars.Id);
                if (String.IsNullOrEmpty(id))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                }
                else
                {
                    INT_ManagedEmailAddressService service = eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        INT_ManagedEmailAddress items = (INT_ManagedEmailAddress)result.Record;
                        LoadComponentsFromBusinessObject(items);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }

            if (Command == CommandName.Modify)
            {
                ErkezIktatSetup(DropDownListDokumentumFeldolgozasTipusa.SelectedValue);
                String id = Request.QueryString.Get(QueryStringVars.Id);
                if (String.IsNullOrEmpty(id))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                }
                else
                {
                    INT_ManagedEmailAddressService service = eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        INT_ManagedEmailAddress item = (INT_ManagedEmailAddress)result.Record;
                        LoadComponentsFromBusinessObject(item);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Oldal Load eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        DropDownListDokumentumFeldolgozasTipusa.SelectedIndexChanged += 
            new System.EventHandler(DropDownClick1);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            if (Command == CommandName.New)
            {
                FormHeader1.FullManualHeaderTitle = "Automatikus email érkeztetés/iktatás létrehozás";
                FormFooter1.Command = CommandName.New;
            }
            if (Command == CommandName.View)
            {
                FormHeader1.FullManualHeaderTitle = "Automatikus email érkeztetés/iktatás megtekintés";
                EFormPanel1.Enabled = false;
                FormFooter1.Command = CommandName.View;
            }
            if (Command == CommandName.Modify)
            {
                FormHeader1.FullManualHeaderTitle = "Automatikus email érkeztetés/iktatás módosítás";
                TextBoxImapEmailAddress.Enabled = false;
                FormFooter1.Command = CommandName.Modify;
            }
            pageView.SetViewOnPage(Command);

            //LZS-BUG_13584
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            String orgKod = GetOrgKod(execParam);
            bool isBOPMH = (orgKod == Constants.OrgKod.BOPMH);

            if (isBOPMH)
            {
                rowIktatasiParameter.Visible = false;
            }
        }
        // BUG_14076
       // TextBoxImapLoginPassword.Attributes["type"] = "password";
    }
    #endregion

    /// <summary>
    /// Komponensek betöltése üzleti objektumból
    /// </summary>
    /// <param name="krt_KodTarak"></param>
    private void LoadComponentsFromBusinessObject(INT_ManagedEmailAddress int_Managed)
    {
        var settingValue = JSONFunctions.DeSerializeManagedEmailParameters(int_Managed.Ertek);
        if (settingValue == null)
            return;
        #region HIDDENFIELDS
        HiddenFieldId.Value = int_Managed.Id.ToString();
        HiddenFieldErvKezd.Value = int_Managed.ErvKezd;
        HiddenFieldErvVege.Value = int_Managed.ErvVege;
        HiddenFieldFelhasznalo_id.Value = int_Managed.Felhasznalo_id;
        HiddenFieldKarbantarthato.Value = int_Managed.Karbantarthato;
        HiddenFieldNev.Value = int_Managed.Nev;
        HiddenFieldOrg.Value = int_Managed.Org;
        // BUG_14076
        HiddenFieldImapPsw.Value = settingValue.ImapLoginPassword;
   
        #endregion

        #region SET VALUES
        TextBoxIdentityEmailAddress.Text = int_Managed.Nev;
        TextBoxImapEmailAddress.Text = settingValue.EmailAddress;
        TextBoxImapServer.Text = settingValue.ImapServerAddress;
        TextBoxImpaServerPort.Text = settingValue.ImapServerPort.ToString();
        CheckBoxImapUseSSL.Checked = settingValue.ImapUseSSL;
        TextBoxImapLoginUser.Text = settingValue.ImapLoginUser;
        TextBoxImapLoginPassword.Text = settingValue.ImapLoginPassword;

        DropDownListDokumentumFeldolgozasTipusa.SelectedValue = settingValue.DokumentumFeldolgozasTipusa.ToString();
        if (settingValue.ValaszEmailAutomatikusan.HasValue)
            CheckBoxValaszEmailAutomatikusan.Checked = settingValue.ValaszEmailAutomatikusan.Value;

        TextBoxKeresesiParameterErkeztetoKonyvAzonosito.Text = settingValue.KeresesiParameterErkeztetoKonyvAzonosito;
        TextBoxKeresesiParameterIktatoKonyvAzonosito.Text = settingValue.KeresesiParameterIktatoKonyvAzonosito;
        TextBoxKeresesiParameterIrattariTetelAzonosito.Text = settingValue.KeresesiParameterIrattáriTételAzonosító;

        #region FELH
        string felhasznaloNev = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(settingValue.FelhasznaloId, Page);
        FelhasznaloTextBoxId.Text = felhasznaloNev;
        FelhasznaloTextBoxId.Id_HiddenField = settingValue.FelhasznaloId;

        String csoportNevFelhSzervezet = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.FelhasznaloSzervezetId, Page);
        CsoportTextBoxFelhasznaloSzervezetId.Text = csoportNevFelhSzervezet;
        CsoportTextBoxFelhasznaloSzervezetId.Id_HiddenField = settingValue.FelhasznaloSzervezetId;
        #endregion

        TextBoxValaszEmailFeladoCim.Text = settingValue.ValaszEmailFeladoCim;
        TextBoxValaszEmailTargy.Text = settingValue.ValaszEmailTargy;
        TextBoxValaszEmailSablonErkeztetes.Text = settingValue.ValaszEmailSablonErkeztetes;
        TextBoxValaszEmailSablonIktatas.Text = settingValue.ValaszEmailSablonIktatas;

        String csoportNevKuldCimzett = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.Kuldemenyek_Csoport_Id_Cimzett, Page);
        CsoportTextBoxKuldemenyek_Csoport_Id_Cimzett.Text = csoportNevKuldCimzett;
        CsoportTextBoxKuldemenyek_Csoport_Id_Cimzett.Id_HiddenField = settingValue.Kuldemenyek_Csoport_Id_Cimzett;

        String csoportNevKuldFelelos = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.Kuldemenyek_Csoport_Id_Felelos, Page);
        CsoportTextBoxKuldemenyek_Csoport_Id_Felelos.Text = csoportNevKuldFelelos;
        CsoportTextBoxKuldemenyek_Csoport_Id_Felelos.Id_HiddenField = settingValue.Kuldemenyek_Csoport_Id_Felelos;

        String csoportNevUgyFelelos = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.UgyUgyiratok_Csoport_Id_Felelos, Page);
        CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Text = csoportNevUgyFelelos;
        CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Id_HiddenField = settingValue.UgyUgyiratok_Csoport_Id_Felelos;

        //SetIrattariTetelList(DropDownListUgyUgyiratok_iraIrattariTetel_Id, settingValue.UgyUgyiratok_iraIrattariTetel_Id);

        String csoportNevUgyIrat = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez, Page);
        FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Text = csoportNevUgyIrat;
        FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Id_HiddenField = settingValue.UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez;

        String csoportNevIraIrat = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(settingValue.IraIratok_FelhasznaloCsoport_Id_Ugyintez, Page);
        FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Text = csoportNevIraIrat;
        FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Id_HiddenField = settingValue.IraIratok_FelhasznaloCsoport_Id_Ugyintez;

        //SetIrattariTetelList(DropDownListIktatasiParameterek_UgykorId, settingValue.IktatasiParameterek_UgykorId);

        LoadComponents(settingValue.UgyUgyiratok_iraIrattariTetel_Id, settingValue.IktatasiParameterek_UgykorId);
        #endregion
        //aktuális verzió ektárolása
        FormHeader1.Record_Ver = int_Managed.Base.Ver;
        FormPart_CreatedModified1.SetComponentValues(int_Managed.Base);
    }
    /// <summary>
    /// Fill components to default
    /// </summary>
    /// <param name="ugyIraIrattariTetel"></param>
    /// <param name="iktParamUgykorId"></param>
    private void LoadComponents(string ugyIraIrattariTetel, string iktParamUgykorId)
    {
        SetIrattariTetelList(DropDownListUgyUgyiratok_iraIrattariTetel_Id, ugyIraIrattariTetel);
        SetIrattariTetelList(DropDownListIktatasiParameterek_UgykorId, iktParamUgykorId);
    }
    /// <summary>
    /// Üzleti objektum feltöltése komponensekből
    /// </summary>
    /// <returns></returns>
    private INT_ManagedEmailAddress GetBusinessObjectFromComponents()
    {
        INT_ManagedEmailAddress item = new INT_ManagedEmailAddress();
        DBO_ManagedEmailParameters settingValue = new DBO_ManagedEmailParameters();
        item.Updated.SetValueAll(false);
        item.Base.Updated.SetValueAll(false);
        #region MAIN PROPERTIES
        if (Command == CommandName.Modify)
        {
            #region HIDDEN FIELDS
            item.Id = HiddenFieldId.Value;
            item.ErvKezd = HiddenFieldErvKezd.Value;
            item.ErvVege = HiddenFieldErvVege.Value;
            item.Karbantarthato = HiddenFieldKarbantarthato.Value;
            item.Nev = HiddenFieldNev.Value;
            item.Org = HiddenFieldOrg.Value;
            #endregion

            #region UPDATED
            item.Felhasznalo_id = FelhasznaloProfil.FelhasznaloId(Page);
            item.Updated.Felhasznalo_id = true;
            #endregion
        }
        else if (Command == CommandName.New)
        {
            item.Id = Guid.NewGuid().ToString();
            item.Updated.Id = true;

            item.ErvKezd = DateTime.Now.ToString();
            item.Updated.ErvKezd = true;

            item.ErvVege = new DateTime(2099, 12, 31).ToString();
            item.Updated.ErvVege = true;

            item.Felhasznalo_id = FelhasznaloProfil.FelhasznaloId(Page);
            item.Updated.Felhasznalo_id = true;

            item.Karbantarthato = "1";
            item.Updated.Karbantarthato = true;

            //item.Org = null;

            item.Nev = TextBoxImapEmailAddress.Text;
            item.Updated.Nev = true;
        }
        #endregion

        #region SET VALUES
        settingValue.EmailAddress = TextBoxImapEmailAddress.Text;
        settingValue.ImapServerAddress = TextBoxImapServer.Text;
        settingValue.ImapServerPort = int.Parse(TextBoxImpaServerPort.Text);
        settingValue.ImapUseSSL = CheckBoxImapUseSSL.Checked;
        settingValue.ImapLoginUser = TextBoxImapLoginUser.Text;
        // BUG_14076
        if (String.IsNullOrEmpty(TextBoxImapLoginPassword.Text))
        {
            settingValue.ImapLoginPassword = HiddenFieldImapPsw.Value;
        } else
        {
            settingValue.ImapLoginPassword = TextBoxImapLoginPassword.Text;
        }
        settingValue.DokumentumFeldolgozasTipusa = int.Parse(DropDownListDokumentumFeldolgozasTipusa.SelectedValue);

        settingValue.KeresesiParameterErkeztetoKonyvAzonosito = TextBoxKeresesiParameterErkeztetoKonyvAzonosito.Text;
        settingValue.KeresesiParameterIktatoKonyvAzonosito = TextBoxKeresesiParameterIktatoKonyvAzonosito.Text;
        settingValue.KeresesiParameterIrattáriTételAzonosító = TextBoxKeresesiParameterIrattariTetelAzonosito.Text;

        settingValue.FelhasznaloId = FelhasznaloTextBoxId.Id_HiddenField;
        settingValue.FelhasznaloSzervezetId = CsoportTextBoxFelhasznaloSzervezetId.Id_HiddenField;

        //LZS - BUG_13586
        settingValue.ValaszEmailAutomatikusan = CheckBoxValaszEmailAutomatikusan.Checked;

        settingValue.ValaszEmailFeladoCim = TextBoxValaszEmailFeladoCim.Text;
        settingValue.ValaszEmailTargy = TextBoxValaszEmailTargy.Text;
        settingValue.ValaszEmailSablonErkeztetes = TextBoxValaszEmailSablonErkeztetes.Text;
        settingValue.ValaszEmailSablonIktatas = TextBoxValaszEmailSablonIktatas.Text;

        settingValue.Kuldemenyek_Csoport_Id_Cimzett = CsoportTextBoxKuldemenyek_Csoport_Id_Cimzett.Id_HiddenField;
        settingValue.Kuldemenyek_Csoport_Id_Felelos = CsoportTextBoxKuldemenyek_Csoport_Id_Felelos.Id_HiddenField;
        settingValue.UgyUgyiratok_Csoport_Id_Felelos = CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Id_HiddenField;

        settingValue.UgyUgyiratok_iraIrattariTetel_Id = DropDownListUgyUgyiratok_iraIrattariTetel_Id.SelectedValue;
        settingValue.UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez = FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Id_HiddenField;
        settingValue.IraIratok_FelhasznaloCsoport_Id_Ugyintez = FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Id_HiddenField;
        settingValue.IktatasiParameterek_UgykorId = DropDownListIktatasiParameterek_UgykorId.SelectedValue;
        #endregion

        #region GENERATE ERTEK
        item.Ertek = JSONFunctions.SerializeManagedEmailParameters(settingValue);
        item.Updated.Ertek = true;
        #endregion

        //a megnyitási verzió megadása ellenörzés miatt
        item.Base.Ver = FormHeader1.Record_Ver;
        item.Base.Updated.Ver = true;

        return item;
    }
    /// <summary>
    /// Fill dropdownlist with irattaritetel list
    /// </summary>
    /// <param name="ddl"></param>
    /// <param name="selected"></param>
    private void SetIrattariTetelList(DropDownList ddl, string selected)
    {
        using (Contentum.eRecord.Service.EREC_IraIrattariTetelekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
            Result result = service.GetAllWithExtension(execParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds != null || result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    ListItem li = null;
                    foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                    {
                        li = new ListItem();
                        li.Text = row["Nev"].ToString();
                        li.Value = row["Id"].ToString();
                        if (!string.IsNullOrEmpty(selected) && string.Equals(row["Id"].ToString(), selected, StringComparison.InvariantCultureIgnoreCase))
                        {
                            li.Selected = true;
                        }
                        ddl.Items.Add(li);
                    }
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            FormFooter1.SaveEnabled = false;
        }
    }
    private void DropDownClick1(object s, System.EventArgs e)
    {
        DropDownList dropList = (DropDownList)s;
        ErkezIktatSetup(dropList.SelectedValue);
    }

    private void ErkezIktatSetup(string value)
    {
        if (value == "1")
        {
            TextBoxKeresesiParameterIktatoKonyvAzonosito.Enabled = false;
            TextBoxKeresesiParameterIktatoKonyvAzonosito.Validate = false;

            TextBoxKeresesiParameterIrattariTetelAzonosito.Enabled = false;
            TextBoxKeresesiParameterIrattariTetelAzonosito.Validate = false;

            TextBoxValaszEmailSablonIktatas.Enabled = false;
            TextBoxValaszEmailSablonIktatas.Validate = false;

            CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Enabled = false;
            CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Validate = false;

            DropDownListUgyUgyiratok_iraIrattariTetel_Id.Enabled = false;
            //DropDownListUgyUgyiratok_iraIrattariTetel_Id.Validate = false;

            FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Enabled = false;
            FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Validate = false;

            FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Enabled = false;
            FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Validate = false;

            DropDownListIktatasiParameterek_UgykorId.Enabled = false;
            //DropDownListIktatasiParameterek_UgykorId.Validate = false;
        }
        else if (value == "2")
        {
            TextBoxKeresesiParameterIktatoKonyvAzonosito.Enabled = true;
            TextBoxKeresesiParameterIktatoKonyvAzonosito.Validate = true;

            TextBoxKeresesiParameterIrattariTetelAzonosito.Enabled = true;
            TextBoxKeresesiParameterIrattariTetelAzonosito.Validate = true;

            TextBoxValaszEmailSablonIktatas.Enabled = true;
            TextBoxValaszEmailSablonIktatas.Validate = true;

            CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Enabled = true;
            CsoportTextBoxUgyUgyiratok_Csoport_Id_Felelos.Validate = true;

            DropDownListUgyUgyiratok_iraIrattariTetel_Id.Enabled = true;
            //DropDownListUgyUgyiratok_iraIrattariTetel_Id.Validate = true;

            FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Enabled = true;
            FelhasznaloCsoportTextBoxUgyUgyiratok_felhasznaloCsoport_Id_Ugyintez.Validate = true;

            FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Enabled = true;
            FelhasznaloCsoportTextBoxIraIratok_FelhasznaloCsoport_Id_Ugyintez.Validate = true;

            DropDownListIktatasiParameterek_UgykorId.Enabled = true;
            //DropDownListIktatasiParameterek_UgykorId.Validate = true;
        }
    }
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //if (FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + Command))
            //{
            switch (Command)
            {
                case CommandName.New:
                    {
                        INT_ManagedEmailAddressService service = eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();
                        INT_ManagedEmailAddress intmanaged = GetBusinessObjectFromComponents();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result result = service.Insert(execParam, intmanaged);
                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        break;
                    }
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            INT_ManagedEmailAddressService service = eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();
                            INT_ManagedEmailAddress item = GetBusinessObjectFromComponents();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = recordId;

                            Result result = service.Update(
                                execParam,
                                item
                            );
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                        }
                        break;
                    }
            }
            //} 
            //else
            //{
            //    UI.RedirectToAccessDeniedErrorPage(Page);
            //}
        }
    }

    private static string GetOrgKod(ExecParam execParam)
    {
        String orgKod = String.Empty;
        Result org = new Result();
        using (Contentum.eAdmin.Service.KRT_OrgokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_OrgokService())
        {
            ExecParam _exec = execParam.Clone();
            _exec.Record_Id = execParam.Org_Id;
            org = service.Get(_exec);
        }

        if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
        return orgKod;
    }
}
