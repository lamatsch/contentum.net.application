﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="ManagedEmailAddressList.aspx.cs" Inherits="ManagedEmailAddressList"
    Title="Automatikus email érkeztetés / iktatás" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="ManagedEmailAddressListCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="ManagedEmailAddressListUpdatePanel" runat="server" OnLoad="ManagedEmailAddressListUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="ManagedEmailAddressListCPE" runat="server"
                            TargetControlID="Panel1" CollapsedSize="20" Collapsed="False" ExpandControlID="ManagedEmailAddressListCPEButton"
                            CollapseControlID="ManagedEmailAddressListCPEButton" ExpandDirection="Vertical"
                            AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="ManagedEmailAddressListCPEButton"
                            ExpandedSize="0" ExpandedText="Feladat definíciók listája" CollapsedText="Feladat definíciók listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="ManagedEmailAddressListGridView" runat="server" CellPadding="0"
                                            CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="ManagedEmailAddressListGridView_RowCommand" OnPreRender="ManagedEmailAddressListGridView_PreRender"
                                            OnSorting="ManagedEmailAddressListGridView_Sorting" OnRowDataBound="ManagedEmailAddressListGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Nev" HeaderText="Elnevezés" SortExpression="Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="220px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"/>
                                                </asp:BoundField>
                                                <%--  <asp:BoundField DataField="Karbantarthato" HeaderText="Karbantartható" SortExpression="Karbantarthato">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:BoundField DataField="Letrehozasido" HeaderText="Léterhozva" SortExpression="Letrehozasido">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ModositasIdo" HeaderText="Módosítva" SortExpression="ModositasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px"  />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ver" HeaderText="Verzió" SortExpression="Ver">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"/>
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
