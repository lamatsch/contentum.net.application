﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Contentum.eIntegrator.Service;

public partial class ManagedEmailAddressList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ManagedEmailAddressList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.ManagedEmailAddressHeaderTitle;
        ListHeader1.SearchObjectType = typeof(INT_ManagedEmailAddressSearch);
        // A baloldali, alapból invisible ikonok megjelenítése:
        ListHeader1.PrintVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SearchVisible = false;
        ListHeader1.HistoryVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("ManagedEmailAddressSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, ManagedEmailAddressListUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ManagedEmailAddressForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, ManagedEmailAddressListUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ManagedEmailAddressListGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("ManagedEmailAddressListPrintForm.aspx");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(ManagedEmailAddressListGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(ManagedEmailAddressListGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(ManagedEmailAddressListGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(ManagedEmailAddressListGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ManagedEmailAddressListUpdatePanel.ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = ManagedEmailAddressListGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(ManagedEmailAddressListGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new INT_ManagedEmailAddressSearch());

        if (!IsPostBack) ManagedEmailAddressListGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddress" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(ManagedEmailAddressListGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }

    #region Master List

    protected void ManagedEmailAddressListGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ManagedEmailAddressListGridView", ViewState, "INT_ManagedEmailAddress.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ManagedEmailAddressListGridView", ViewState);

        ManagedEmailAddressListGridViewBind(sortExpression, sortDirection);
    }

    protected void ManagedEmailAddressListGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(ManagedEmailAddressListGridView);

        INT_ManagedEmailAddressService service = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        INT_ManagedEmailAddressSearch search = null;
        search = (INT_ManagedEmailAddressSearch)Search.GetSearchObject(Page, new INT_ManagedEmailAddressSearch());

        search.OrderBy = Search.GetOrderBy("ManagedEmailAddressListGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        if (service == null)
            return;
        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(ManagedEmailAddressListGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void ManagedEmailAddressListGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void ManagedEmailAddressListGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ManagedEmailAddressListGridView.PageIndex;

        ManagedEmailAddressListGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = ManagedEmailAddressListGridView.PageCount;

        if (prev_PageIndex != ManagedEmailAddressListGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, ManagedEmailAddressListCPE);
            ManagedEmailAddressListGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, ManagedEmailAddressListCPE);
        }

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(ManagedEmailAddressListGridView);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ManagedEmailAddressListGridViewBind();
    }

    protected void ManagedEmailAddressListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ManagedEmailAddressListGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ManagedEmailAddressForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, ManagedEmailAddressListUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ManagedEmailAddressForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ManagedEmailAddressListUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_ManagedEmailAddress";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, ManagedEmailAddressListUpdatePanel.ClientID);

            //ListHeader1.PrintOnClientClick = "javascript:window.open('ManagedEmailAddressPrintForm.aspx?" + QueryStringVars.Id + "=" + id + "')";

        }
    }

    protected void ManagedEmailAddressListUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ManagedEmailAddressListGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedManagedEmailAddress();
            ManagedEmailAddressListGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedManagedEmailAddressRecords();
                ManagedEmailAddressListGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedManagedEmailAddressRecords();
                ManagedEmailAddressListGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedManagedEmailAddress();
                break;
        }
    }

    private void LockSelectedManagedEmailAddressRecords()
    {
        LockManager.LockSelectedGridViewRecords(ManagedEmailAddressListGridView, "EREC_ManagedEmailAddress"
            , "ManagedEmailAddressLock", "ManagedEmailAddressForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedManagedEmailAddressRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(ManagedEmailAddressListGridView, "EREC_ManagedEmailAddress"
            , "ManagedEmailAddressLock", "ManagedEmailAddressForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Törli a ManagedEmailAddressListGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedManagedEmailAddress()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ManagedEmailAddressInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ManagedEmailAddressListGridView, EErrorPanel1, ErrorUpdatePanel);
            INT_ManagedEmailAddressService service = eIntegratorService.ServiceFactory.GetINT_ManagedEmailAddressService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a ManagedEmailAddressListGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedManagedEmailAddress()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(ManagedEmailAddressListGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_ManagedEmailAddress");
        }
    }

    protected void ManagedEmailAddressListGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ManagedEmailAddressListGridViewBind(e.SortExpression, UI.GetSortToGridView("ManagedEmailAddressListGridView", ViewState, e.SortExpression));
    }

    #endregion
}