using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using System.Collections.Generic;
using System.IO;

public partial class MasterPage : System.Web.UI.MasterPage, Contentum.eUtility.ILastLoginData
{

    public HtmlGenericControl Body
    {
        get
        {
            return masterBody;
        }
    }

    public void SetPopupFromat()
    {
        MasterPageMenu.Visible = false;
        //Fejlec
        Panel2.Visible = false;
        //linkek miatt
        if (!IsPostBack)
        {
            LiteralControl li = new LiteralControl("<base target=\"_self\" />");
            Page.Header.Controls.Add(li);
        }

        Response.CacheControl = "no-cache";
    }

    protected void Page_Init(object sender, EventArgs e)
    {
              
        Authentication.CheckLogin(Page);

        BelepesiAdatok.FelhasznaloNev = FelhasznaloNev;
        BelepesiAdatok.FelettesSzervezet = labelFelettesSzervezet;
        BelepesiAdatok.HelyettesitesMod = HelyettesitesMod;
        BelepesiAdatok.LoginUserNev = LoginUserNev;

	// Logo
        BelepesiAdatok.OrgLogo_Top = OrgLogo_Top;
        BelepesiAdatok.OrgLogo_Bottom = OrgLogo_Bottom;      

	BelepesiAdatok.LoadBelepesiAdatok();

        // Logo
        //LogoImage.Src = "images/hu/fejlec/" + UI.GetAppSetting("ApplicationName") + "_top_left.jpg";
		LogoImage.Src = "images/hu/fejlec/" + UI.GetAppSetting("ApplicationName") + ".png";
		//LogoImage.Alt = UI.GetAppSetting("ApplicationName");

        //AjaxLogging.js referencia hozz�ad�sa
        Contentum.eUtility.JavaScripts.RegisterAjaxLoggingScript(Page);
        //Common.js referencia hozz�ad�sa
        Contentum.eUtility.JavaScripts.RegisterCommonScripts(Page);

        //verzi� megjelen�t�se
        ShowVersion();

    }


    public string ContentumVersion
    {
        get
        {
            return Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.APPLICATION_VERSION);
        }
    }

    private void ShowVersion()
    {
        string verzio = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.APPLICATION_VERSION);
        if (!String.IsNullOrEmpty(verzio))
        {
            string verzioText = String.Format("Verzi�: {0}", verzio);
            string js = "alert('" + verzioText + "');";
            LogoImage.Attributes["onclick"] = js;
            LogoImage.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
			LogoContentum.Attributes["onclick"] = js;
            LogoContentum.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        BelepesiAdatok.LoadBelepesiAdatok();

        JavaScripts.RegisterPopupWindowClientScript(Page);
        foreach (Contentum.eUIControls.eErrorPanel errorPanel in UI.FindControls<Contentum.eUIControls.eErrorPanel>(Page.Form.Controls))
        {
            errorPanel.EmailButtonClick += new CommandEventHandler(errorPanel_EmailButtonClick);
        }

        ////K�rnyezetf�gg� help: a s�g� gombhoz az oldalhoz tartoz� s�g� f�jl el�r�si �tvonal�nak hozz�rendel�s
        
        string pageUrl = Page.Request.Url.ToString();
        //string helpDirectory = "/" + helpDirectoryName + "/";
        //string helpPageName = Path.GetFileNameWithoutExtension(pageUrl) + helpFileExtension;
        //string helpPageUrl = "~" + VirtualPathUtility.Combine(helpDirectory, helpPageName);
        //string helpPageServerPath = Server.MapPath(helpPageUrl);
        //FileInfo fiHelpPage = new FileInfo(helpPageServerPath);

        //if (fiHelpPage.Exists)
        //{
        //    linkHelp.HRef = helpPageUrl;
        //    linkHelp.Target = "_blank";
        //}
        //else
        //{
        //    linkHelp.HRef = "";
        //    linkHelp.Target = "";
        //    linkHelp.Attributes.Add("onclick", "alert('" + helpFileNotFound + "')");
        //}

        string pageFileName = Path.GetFileName(pageUrl).ToLower();
        
        if (pageFileName != "default.aspx")
        {
            Component_MasterPageMenuControl menuControl = Component_MasterPageMenuControl.AddMenuControl(MasterPageMenu);
        }

        #region Emoticon-on a fejl�cbe
        DateTime now = DateTime.Now;

        // r�szeg t�lap�
        if (now.Month == 12 && now.Day > 2 && now.Day < 8)
        {
            td_emoticon_helye.Style["background-image"] = "url('images/hu/fejlec/santaclaus.gif');";
            td_emoticon_helye.Style["background-repeat"] = "no-repeat;";
        }
        if (now.Month == 12 && now.Day > 19 && now.Day < 29)
        {
            td_emoticon_helye.Style["background-image"] = "url('images/hu/fejlec/xmas_tree.gif');";
            td_emoticon_helye.Style["background-repeat"] = "no-repeat;";
        }
        if (now.Month == 12 && now.Day > 30 && now.Day < 7)
        {
            td_emoticon_helye.Style["background-image"] = "url('images/hu/fejlec/santaclaus.gif');";
            td_emoticon_helye.Style["background-repeat"] = "no-repeat;";
        }
        // torta gyerty�val: EDOK sz�linap: 2008.04.28. (els� 3 �v)
        if (now.Year >= 2009 && now.Year <= 2011 && now.Month == 4 && now.Day > 25 && now.Day <= 30)
        {
            td_emoticon_helye.Style["background-image"] = "url('images/hu/fejlec/torta_animalt_" + (now.Year-2008).ToString() + "ev.gif');";
            td_emoticon_helye.Style["background-repeat"] = "no-repeat;";
        }

        #endregion Emoticon-on a fejl�cbe
    }

    protected void errorPanel_EmailButtonClick(object sender, CommandEventArgs e)
    {
        Contentum.eUIControls.eErrorPanel errorPanel = (Contentum.eUIControls.eErrorPanel) sender;
        string[] parameters = e.CommandArgument.ToString().Split(';');
        string ErrorCode = parameters[0];
        string ErrorMessage = parameters[1];
        string CurrentLogEntry = parameters[2];

        bool IsSuccess = Notify.SendErrorInEmail(Page, ErrorCode, ErrorMessage, CurrentLogEntry);

        if (errorPanel != null)
        {
            JavaScripts.RegisterEmailFeedbackMessage(errorPanel, IsSuccess);
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
    }

    protected void ImageButtonLogOut_Click(object sender, ImageClickEventArgs e)
    {
        Authentication.LogOut(Page);
    }

    #region ILastLoginData Members

    public string FelhasznaloHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldFelhasznaloId.UniqueID;
        }
    }

    public string SzervezetHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldSzervezetId.UniqueID;
        }
    }

    public string FelhasznaloCsoporttagsagHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldCsoporttagId.UniqueID;
        }
    }

    public string HelyettesitesHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldHelyettesitesId.UniqueID;
        }
    }

    public string LoginTypeHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldLoginType.UniqueID;
        }
    }

    #endregion
}
