using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MegbizasokList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    private const string normalReadonlyRowStyle = "GridViewReadOnlyRowStyle";
    private const string selectedReadOnlyRowStyle = "GridViewReadOnlySelectedRowStyle";

    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";
    private const string funkcio_MegbizasFelvetelOsszesDolgozora = "MegbizasFelvetelOsszesDolgozora";

    public bool isUserAllowedToModifyAllEmployees = false;
 
    #region Utils

    /// <summary>
    /// A megadott keres�si objektum �rv�nyess�gi mez�inek be�ll�t�sa,
    /// a MegbizasokGridView Helyettesiteskezd �s HelyettesitesVege mez�iben szerepl� �rt�kek felhaszn�l�s�val
    /// </summary>
    /// <param name="helyettesitesId"></param>
    /// <param name="ervKezd"></param>
    /// <param name="ervVege"></param>
    private void SetSzerepkorokErvenyessegFields(string helyettesitesId, Field ervKezd, Field ervVege)
    {
        // az �rv�nyess�get a megb�z�s id�tartama szerint n�zz�k
        // 1. �rv�nyes: az �rv�nyess�g eleje �s v�ge bel�l van a megb�z�s idej�n
        // 2. �rv�nytelen: az �rv�nyess�g v�ge megel�zi a megb�z�s idej�t
        GridViewRow row = getSelectedRowByID(helyettesitesId, MegbizasokGridView);
        if (row != null)
        {
            string HelyettesitesKezd = UI.GetGridViewColumnText(row, "HelyettesitesKezd");
            string HelyettesitesVege = UI.GetGridViewColumnText(row, "HelyettesitesVege");

            Component_SubListHeader.ValidityType ValidFilterSetting = SzerepkorokSubListHeader.ValidFilterSetting;
        if (ValidFilterSetting == Component_SubListHeader.ValidityType.Valid)
        {
            // �rv�nyes
            ervKezd.Value = HelyettesitesKezd;
            ervKezd.Operator = Query.Operators.greaterorequal;
            ervKezd.Group = "100";
            ervKezd.GroupOperator = Query.Operators.and;

            ervVege.Value = HelyettesitesVege;
            ervVege.Operator = Query.Operators.lessorequal;
            ervVege.Group = "100";
            ervVege.GroupOperator = Query.Operators.and;
        }
        else if (ValidFilterSetting == Component_SubListHeader.ValidityType.Invalid)
        {
            // �rv�nytelen
            ervKezd.Value = HelyettesitesVege;
            ervKezd.Operator = Query.Operators.greaterorequal;
            ervKezd.Group = "100";
            ervKezd.GroupOperator = Query.Operators.or;

            ervVege.Value = HelyettesitesKezd;
            ervVege.Operator = Query.Operators.lessorequal;
            ervVege.Group = "100";
            ervVege.GroupOperator = Query.Operators.or;
        }
        else if (ValidFilterSetting == Component_SubListHeader.ValidityType.All)
        {
            // �sszes
            ervKezd.Clear();

            ervVege.Clear();
        }
        }
    }

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MegbizasokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        SzerepkorokSubListHeader.RowCount_Changed += new EventHandler(SzerepkorokSubListHeader_RowCount_Changed);

        SzerepkorokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(SzerepkorokSubListHeader_ErvenyessegFilter_Changed);

        isUserAllowedToModifyAllEmployees = FunctionRights.GetFunkcioJog(Page, funkcio_MegbizasFelvetelOsszesDolgozora);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.MegbizasokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_HelyettesitesekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("MegbizasokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("MegbizasokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(MegbizasokGridView.ClientID, "", "check");
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(MegbizasokGridView.ClientID, "check", "cbInvalidationAllowed", true);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(MegbizasokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(MegbizasokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(MegbizasokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(MegbizasokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = MegbizasokGridView;

        SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //SzerepkorokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(SzerepkorokGridView.ClientID);
        SzerepkorokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(SzerepkorokGridView.ClientID, "check", "cbEndedAtPast", false);
        SzerepkorokSubListHeader.ButtonsClick += new CommandEventHandler(SzerepkorokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        SzerepkorokSubListHeader.AttachedGridView = SzerepkorokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(MegbizasokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_HelyettesitesekSearch());

        if (!IsPostBack) MegbizasokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Lock);

        // megb�z�sn�l az �tadott jogk�rt adhassa �t akkor is, ha egy�bk�nt nem rendelhetne felhaszn�l�hoz szerepk�rt
        //SzerepkorokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New);
        SzerepkorokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify);

        SzerepkorokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.View);
        //SzerepkorokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        SzerepkorokSubListHeader.ModifyVisible = false;

        // megb�z�sn�l az �tadott jogk�rt �rvenytelen�thesse akkor is, ha egy�bk�nt nem �rv�nytelen�thetne felhaszn�l�hoz rendelt szerepk�rt
        // SzerepkorokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate);
        SzerepkorokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MegbizasokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #endregion

    #region Master List

    protected void MegbizasokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MegbizasokGridView", ViewState, "KRT_Helyettesitesek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MegbizasokGridView", ViewState, SortDirection.Descending);

        MegbizasokGridViewBind(sortExpression, sortDirection);
    }

    protected void MegbizasokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_HelyettesitesekSearch search = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
        search.OrderBy = Search.GetOrderBy("MegbizasokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        search.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Megbizas;
        search.HelyettesitesMod.Operator = Query.Operators.equals;

        // TODO: sz�r�sek hibakezel�se

        // ha nem kezelheti mindet
        // ha vezet�:
        // -  l�thatja az �sszes rekordot, ahol a megb�z� vagy a megb�zott az � szervezeti hierarchi�j�nak tagja
        // -  m�dos�thatja az �sszes rekordot, ahol a megb�z� vagy a megb�zott az � szervezeti hierarchi�j�nak tagja
        // -  megb�z� �s megb�zott csak az � szervezeti hierarchi�j�nak tagja lehet
        // ha dolgoz�, csak a saj�t csoportj�n bel�lieket l�thatja
        // -  l�thatja az �sszes rekordot, ahol � a megb�z� vagy a megb�zott
        // -  m�dos�thatja az �sszes rekordot, ahol � a megb�z�
        // -  megb�zottja csak az � csoportj�nak tagja lehet

        if (!isUserAllowedToModifyAllEmployees)
        {
            string CsoportTag_Id = FelhasznaloProfil.FelhasznaloCsoportTagId(Page);
            search.ExtendedFilter.CsoportTag_Id = CsoportTag_Id;
        }
        else
        {
            search.ExtendedFilter.CsoportTag_Id = "";
        }

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(MegbizasokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void MegbizasokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        CheckBox cbCurrentUserAllowedToModify = (CheckBox)e.Row.FindControl("cbCurrentUserAllowedToModify");
        CheckBox cbInvalidationAllowed = (CheckBox)e.Row.FindControl("cbInvalidationAllowed");
        CheckBox cbStartedAtPast = (CheckBox)e.Row.FindControl("cbStartedAtPast");
        CheckBox cbEndedAtPast = (CheckBox)e.Row.FindControl("cbEndedAtPast");

        #region m�dos�that�s�g
        if (cbCurrentUserAllowedToModify != null)
        {
            if (isUserAllowedToModifyAllEmployees)
            {
                cbCurrentUserAllowedToModify.Checked = true;
            }
            else
            {
                string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
                if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.dolgozo)
                {
                    string helyettesitett = UI.GetGridViewColumnText(e.Row, "Felhasznalo_ID_helyettesitett");
                    if (helyettesitett == FelhasznaloProfil.FelhasznaloId(Page))
                    {
                        cbCurrentUserAllowedToModify.Checked = true;
                    }
                    else
                    {
                        cbCurrentUserAllowedToModify.Checked = false;
                    }
                }
                else if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
                {
                    cbCurrentUserAllowedToModify.Checked = true;
                }
            }
        }
        #endregion m�dos�that�s�g

        #region �rv�nytelen�thet�s�g
        //if (cbInvalidationAllowed != null && cbStartedAtPast != null && cbCurrentUserAllowedToModify != null)
        //{
        //    cbInvalidationAllowed.Checked = !cbStartedAtPast.Checked && cbCurrentUserAllowedToModify.Checked;
        //}

        if (cbInvalidationAllowed != null && cbEndedAtPast != null && cbCurrentUserAllowedToModify != null)
        {
            cbInvalidationAllowed.Checked = cbCurrentUserAllowedToModify.Checked && !cbEndedAtPast.Checked;
        }
        #endregion �rv�nytelen�thet�s�g
    }

    protected void MegbizasokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, MegbizasokCPE);
        ListHeader1.RefreshPagerLabel();

        ui.SetClientScriptToGridViewSelectDeSelectButton(MegbizasokGridView);

    }
    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        MegbizasokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, MegbizasokCPE);
        MegbizasokGridViewBind();
    }

    protected void MegbizasokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MegbizasokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);

            //MegbizasokGridView_SelectRowCommand(id);
        }
    }
      
    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            bool isCurrentUserAllowedToModify = UI.isGridViewCheckBoxChecked(MegbizasokGridView, "cbCurrentUserAllowedToModify", id);

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("MegbizasokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID);

            if (isCurrentUserAllowedToModify)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("MegbizasokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_MegbizasMegtekintes
                    + "')) {" + JavaScripts.SetOnClientClick("MegbizasokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, MegbizasokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                    + "} else return false;";
            }

            string tableName = "KRT_Helyettesitesek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, MegbizasokUpdatePanel.ClientID);

            bool modosithato = isCurrentUserAllowedToModify && UI.isGridViewCheckBoxChecked(MegbizasokGridView, "cbNotExpired", id);
            if (modosithato)
            {
                SzerepkorokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify);
                SzerepkorokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify);
                SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Megbizas_Szerepkor_Form.aspx"
                    , "Command=" + CommandName.New + "&" + QueryStringVars.HelyettesitesId + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                SzerepkorokSubListHeader.NewEnabled = false;
                SzerepkorokSubListHeader.InvalidateEnabled = false;
            }

        }
    }

    protected void MegbizasokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    MegbizasokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MegbizasokGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedMegbizasok();
            MegbizasokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedHelyettesitesRecords();
                MegbizasokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedHelyettesitesRecords();
                MegbizasokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedMegbizasok();
                break;
        }
    }

    private void LockSelectedHelyettesitesRecords()
    {
        LockManager.LockSelectedGridViewRecords(MegbizasokGridView, "KRT_Helyettesitesek"
            , "MegbizasLock", "MegbizasForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedHelyettesitesRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(MegbizasokGridView, "KRT_Helyettesitesek"
            , "MegbizasLock", "MegbizasForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a MegbizasokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedMegbizasok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "MegbizasInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(MegbizasokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = UI.isGridViewCheckBoxChecked(MegbizasokGridView, "cbInvalidationAllowed", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidateMegbizas(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MegbizasokGridView));
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }
    
    /// <summary>
    /// Elkuldi emailben a MegbizasokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedMegbizasok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            //Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(MegbizasokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Helyettesitesek");
            // a t�blan�v ugyan a KRT_Helyettesitesek lenne, de kij�tsszuk egy manipul�lt n�vvel,
            // hogy a megb�z�sok list�j�ra ir�ny�tson
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(MegbizasokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Megbizasok");
        }
    }

    protected void MegbizasokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MegbizasokGridViewBind(e.SortExpression, UI.GetSortToGridView("MegbizasokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MegbizasokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (MegbizasokGridView.SelectedIndex == -1)
        {
            return;
        }
        string helyettesitesId = UI.GetGridViewSelectedRecordId(MegbizasokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, helyettesitesId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string helyettesitesId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                SzerepkorokGridViewBind(helyettesitesId);
                SzerepkorokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(SzerepkorokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        SzerepkorokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
        {
            SzerepkorokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
    }

    #endregion


    #region Szerepkorok Detail

    private void SzerepkorokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedSzerepkorok();
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView));
        }
    }

    protected void SzerepkorokGridViewBind(string helyettesitesId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SzerepkorokGridView", ViewState, "Szerepkor_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SzerepkorokGridView", ViewState);

        SzerepkorokGridViewBind(helyettesitesId, sortExpression, sortDirection);
    }

    protected void SzerepkorokGridViewBind(string helyettesitesId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(helyettesitesId))
        {
            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();
            search.Helyettesites_Id.Value = helyettesitesId;
            search.Helyettesites_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("SzerepkorokGridView", ViewState, SortExpression, SortDirection);

            //SzerepkorokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);
            // az �rv�nyess�get a megb�z�s id�tartama szerint n�zz�k
            // 1. �rv�nyes: az �rv�nyess�g eleje �s v�ge bel�l van a megb�z�s idej�n
            // 2. �rv�nytelen: az �rv�nyess�g v�ge megel�zi a megb�z�s idej�t
            SetSzerepkorokErvenyessegFields(helyettesitesId, search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);

            UI.GridViewFill(SzerepkorokGridView, res, SzerepkorokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);
        }
        else
        {
            ui.GridViewClear(SzerepkorokGridView);
        }
    }

    //GridView esem�nykezel�i(3-4)
    ////specialis, nem mindig kell kell
    protected void SzerepkorokGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        // read only sorok sz�nez�se
        GridView gv = (GridView)sender;
        if (gv == null) return;

        if (gv.SelectedIndex > -1 && (UI.isGridViewCheckBoxChecked(gv.SelectedRow, "cbEndedAtPast")))
        {
            gv.SelectedRow.ControlStyle.CssClass = normalReadonlyRowStyle;
        }

        if (UI.isGridViewCheckBoxChecked(gv.Rows[e.NewSelectedIndex], "cbEndedAtPast"))
        {
            gv.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedReadOnlyRowStyle;
        }
    }

    protected void SzerepkorokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
         // true, ha az adott sor nem m�dos�that�, mert m�r lez�rult a m�ltban
        bool bReadOnly = UI.isGridViewCheckBoxChecked(e.Row, "cbEndedAtPast");

        if (bReadOnly)
        {
            e.Row.ControlStyle.CssClass = normalReadonlyRowStyle;
        }

    }

    void SzerepkorokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView));
    }

    protected void SzerepkorokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
                    //{
                    //    SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView));
                    //}
                    ActiveTabRefreshDetailList(SzerepkorokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsoporttagokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedSzerepkorok()
    {
        // megb�z�sn�l az �tadott jogk�rt �rvenytelen�thesse akkor is, ha egy�bk�nt nem �rv�nytelen�thetne felhaszn�l�hoz rendelt szerepk�rt
        //if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate") || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.New) || FunctionRights.GetFunkcioJog(Page, "Megbizas" + CommandName.Modify))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = !UI.isGridViewCheckBoxChecked(SzerepkorokGridView, "cbEndedAtPast", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void SzerepkorokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SzerepkorokGridView, selectedRowNumber, "check");
        }
    }

    private void SzerepkorokGridView_RefreshOnClientClicks(string szerepkorId)
    {
        string id = szerepkorId;
        if (!String.IsNullOrEmpty(id))
        {
            SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void SzerepkorokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = SzerepkorokGridView.PageIndex;

        SzerepkorokGridView.PageIndex = SzerepkorokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != SzerepkorokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, SzerepkorokCPE);
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(SzerepkorokSubListHeader.Scrollable, SzerepkorokCPE);
        }
        SzerepkorokSubListHeader.PageCount = SzerepkorokGridView.PageCount;
        SzerepkorokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(SzerepkorokGridView);
    }

    void SzerepkorokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(SzerepkorokSubListHeader.RowCount);
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView));
    }

    protected void SzerepkorokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(MegbizasokGridView)
            , e.SortExpression, UI.GetSortToGridView("SzerepkorokGridView", ViewState, e.SortExpression));
    }

    #endregion
}
