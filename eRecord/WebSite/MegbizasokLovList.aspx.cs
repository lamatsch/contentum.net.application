using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class MegbizasokLovList : Contentum.eUtility.UI.PageBase
{
    

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("MegbizasokSearch.aspx", "", 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refresh);

        ImageButtonReszletesAdatok.OnClientClick = "if (document.forms[0]." + ListBoxSearchResult.ClientID + ".value == '') return false; "
            + JavaScripts.SetOnClientClick("MegbizasokForm.aspx", "Command=View&Id='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            if (TextBoxSearch.Text.Trim() != "*")
            {
                FillListBoxSearchResult(TextBoxSearch.Text);
            }
        }

    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text);
    }
    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text);
    }

    protected void FillListBoxSearchResult(string SearchKey)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

        KRT_HelyettesitesekSearch _KRT_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();

        _KRT_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        _KRT_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Value = SearchKey;

        _KRT_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Megbizas;
        _KRT_HelyettesitesekSearch.HelyettesitesMod.Operator = Query.Operators.equals;

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result _res = service.GetAllWithExtension(ExecParam, _KRT_HelyettesitesekSearch);

        _KRT_HelyettesitesekSearch.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        if (_res.ErrorCode == null && _res.Ds != null)
        {
            for (int ind = 0; ind < _res.Ds.Tables[0].Rows.Count; ind++)
            {
                string strItem = _res.Ds.Tables[0].Rows[ind]["Felhasznalo_Nev_helyettesitett"].ToString();
                string csoportNev = _res.Ds.Tables[0].Rows[ind]["Csoport_Nev_helyettesitett"].ToString();
                if (!String.IsNullOrEmpty(csoportNev))
                {
                    strItem += " (" + csoportNev + ")";
                }
                ListItem li = new ListItem(strItem, _res.Ds.Tables[0].Rows[ind]["Id"].ToString());
                ListBoxSearchResult.Items.Add(li);
            }
        }

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(_res, _KRT_HelyettesitesekSearch.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            string hiddenFieldId = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            string lovValue = ListBoxSearchResult.SelectedItem.Value;

            string textBoxId = Request.QueryString.Get(QueryStringVars.TextBoxId);
            string text = ListBoxSearchResult.SelectedItem.Text;

            Dictionary<string, string> returnLovValues = new Dictionary<string, string>();
            if (hiddenFieldId != null) returnLovValues.Add(hiddenFieldId, lovValue);
            if (textBoxId != null) returnLovValues.Add(textBoxId, text);

            JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnLovValues);
            JavaScripts.RegisterCloseWindowClientScript(Page, false);
        }
    }
}
