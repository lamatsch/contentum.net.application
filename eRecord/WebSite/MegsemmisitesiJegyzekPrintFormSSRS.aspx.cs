﻿using System;
using System.Data;
using System.Configuration;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;


public partial class MegsemmisitesiJegyzekPrintFormSSRS : Contentum.eUtility.UI.ListSSRSPageBase

{

    private string executor = String.Empty;
    private string szervezet = String.Empty;

    private string id = String.Empty;
  //  private string id = "21B6C567-7F32-E811-80C9-00155D020DD3";

    protected void Page_Init(object sender, EventArgs e)
    {
      //  id = Request.QueryString["Id"];
    }


    protected override ReportViewer ReportViewer { get { return ReportViewer1; } }
    protected override String DefaultOrderBy { get { return "1"; } }
    protected override Contentum.eUIControls.eErrorPanel ErrorPanel { get { return EErrorPanel1; } }

    protected override Result GetData()
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        executor = execParam.LoginUser_Id;
        szervezet = execParam.Org_Id;
        execParam.Fake = true;

        EREC_KuldKuldemenyekSearch search = (EREC_KuldKuldemenyekSearch)this.SearchObject;

        Result result = service.GetAllWithExtensionAndJogosultak(execParam, search, true);

        return result;
    }

    protected override BaseSearchObject GetSearchObject()
    {
        EREC_KuldKuldemenyekSearch search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject(Page, new EREC_KuldKuldemenyekSearch(true));

        search.TopRow = 0;

        return search;
    }

    protected override Dictionary<string, int> CreateVisibilityColumnsInfoDictionary()
    {
        Dictionary<string, int> dict = new Dictionary<string, int>();



        return dict;
    }

    protected override void SetSpecificReportParameter(ReportParameter rp)
    {
        id = Request.QueryString["Id"];

        if (rp != null && !String.IsNullOrEmpty(rp.Name))
        {
            switch (rp.Name)
            {
                case "ExecutorUserId":
                    if (!string.IsNullOrEmpty(executor))
                    {
                        rp.Values.Add(executor);
                    }
                    break;


                case "FelhasznaloSzervezet_Id":
                    if (!string.IsNullOrEmpty(szervezet))
                    {
                        rp.Values.Add(szervezet);
                    }
                    break;


                case "Id":
                    rp.Values.Add(id);
                    break;
            }
        }
    }

    protected override bool IsGetAllParameter(String pname)
    {

        return false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
        if (!IsPostBack)
        {
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;
        }

        //ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();


        //if (rpis.Count > 0)
        //{
        //      ReportParameter[] ReportParameters = GetReportParameters(rpis);


        //      ReportViewer1.ServerReport.SetParameters(ReportParameters);
        //}

        ////  ReportViewer1.ShowParameterPrompts = true;
        //ReportViewer1.ServerReport.Refresh();

        Response.Write(id);
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        
                        //case "ExecutorUserId":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                        //    break;
                        //case "Jogosultak":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                        //    break;



                        //case "FelhasznaloSzervezet_Id":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                        //    break;

                        case "Id":
                            ReportParameters[i].Values.Add(id);
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}