<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="MentettTalalatokForm.aspx.cs" Inherits="MentettTalalatokForm" Title="Untitled Page" %>


<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc150" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="ListName_Label" runat="server" Text="Tal�lati lista neve:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="ListName_TextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Searched_Table_Label" runat="server" Text="Keres�si t�bla:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Searched_Table_TextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="SaveDate_Label" runat="server" Text="Ment�s d�tuma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="SaveDate_CalendarControl" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Requestor_Label" runat="server" Text="Keres� szem�ly:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc150:FelhasznaloTextBox ID="Requestor_PartnerTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Workstation_Label" runat="server" Text="Munka�llom�s" />
                            </td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:TextBox ID="Workstation_TextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Ervenyesseg_Label" runat="server" Text="�rv�nyess�g:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="Ervenyesseg_CalendarControl" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Label ID="gridViewHeaderXML_Label" runat="server" Text="Keres�si param�terek" class="mrUrlapCaption" a />
    <asp:GridView ID="gridViewHeaderXML" runat="server" BorderWidth="1px" AllowSorting="True" PagerSettings-Visible="false" CellPadding="1" Style="width: 88%; margin-left: 2%;" GridLine="Horizontal"
        CellSpacing="1" BorderStyle="Solid" BorderColor="Black" OnSorting="gridViewHeaderXML_Sorting">
        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Center" />
        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
        <HeaderStyle CssClass="GridViewHeaderStyle" />
        <PagerSettings Visible="False" />
    </asp:GridView>
    <asp:Label ID="gridViewBodyXML_Label" runat="server" Text="Keres�si tal�latok" class="mrUrlapCaption" />
    <div style="overflow: scroll; width: 88%; margin-left: 2%; height: 800px">
        <asp:GridView ID="gridViewBodyXML" runat="server" BorderWidth="1px" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0"
            OnSorting="gridViewBodyXML_Sorting" GridLines="Both" AutoGenerateColumns="true">
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Center" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <PagerSettings Visible="False" />
        </asp:GridView>
    </div>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>


