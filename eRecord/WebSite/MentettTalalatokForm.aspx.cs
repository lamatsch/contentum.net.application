using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

public partial class MentettTalalatokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private SqlConnection _temporaryConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ConnectionString);
    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MentettTalalatok" + Command);
                break;
        }

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_Mentett_Talalati_ListakService service = eRecordService.ServiceFactory.GetKRT_Mentett_Talalati_ListakService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Mentett_Talalati_Listak ResultList_eBussCore = (KRT_Mentett_Talalati_Listak)result.Record;
                    LoadComponentsFromBusinessObject(ResultList_eBussCore);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        string s = Request.QueryString.Get("GetExcel");
        if ("1".Equals(s))
        {
            SaveToExcel();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Mentett tal�lati list�k";

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string url = "MentettTalalatokForm.aspx?Command=View&GetExcel=1&Id=" + Request.QueryString.Get(QueryStringVars.Id);
        FormFooter1.ImageButton_ResultList_ExportExcel.OnClientClick = Contentum.eRecord.Utility.JavaScripts.SetOnClientClickIFramePrintForm(url);
        FormFooter1.ImageButton_ResultList_ExportExcel.OnClientClick += ";return false;";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Mentett_Talalati_Listak ResultList_eBussCore)
    {
        ListName_TextBox.Text = ResultList_eBussCore.ListName;
        Searched_Table_TextBox.Text = ResultList_eBussCore.Searched_Table;
        SaveDate_CalendarControl.Text = ResultList_eBussCore.SaveDate;

        Requestor_PartnerTextBox.Id_HiddenField = ResultList_eBussCore.Requestor;
        Requestor_PartnerTextBox.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);

        Workstation_TextBox.Text = ResultList_eBussCore.WorkStation;
        Ervenyesseg_CalendarControl.ErvKezd = ResultList_eBussCore.ErvKezd;
        Ervenyesseg_CalendarControl.ErvVege = ResultList_eBussCore.ErvVege;

        HeaderXMLGridViewBind();
        BodyXMLGridViewBind();

        FormHeader1.Record_Ver = ResultList_eBussCore.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(ResultList_eBussCore.Base);

        if (Command == CommandName.View)
        {
            ListName_TextBox.ReadOnly = true;
            Searched_Table_TextBox.ReadOnly = true;
            SaveDate_CalendarControl.ReadOnly = true;
            Requestor_PartnerTextBox.ReadOnly = true;
            Workstation_TextBox.ReadOnly = true;
            Ervenyesseg_CalendarControl.ReadOnly = true;
            FormFooter1.ImageButton_ResultList_ExportExcel.Visible = true;
        }
    }

    // form --> business object
    private KRT_Mentett_Talalati_Listak GetBusinessObjectFromComponents()
    {
        KRT_Mentett_Talalati_Listak ResultList_eBussCore = new KRT_Mentett_Talalati_Listak();
        ResultList_eBussCore.Updated.SetValueAll(false);
        ResultList_eBussCore.Base.Updated.SetValueAll(false);

        ResultList_eBussCore.ListName = ListName_TextBox.Text;
        ResultList_eBussCore.Updated.ListName = pageView.GetUpdatedByView(ListName_TextBox);

        ResultList_eBussCore.Searched_Table = Searched_Table_TextBox.Text;
        ResultList_eBussCore.Updated.Searched_Table = pageView.GetUpdatedByView(Searched_Table_TextBox);

        ResultList_eBussCore.SaveDate = SaveDate_CalendarControl.Text;
        ResultList_eBussCore.Updated.SaveDate = pageView.GetUpdatedByView(SaveDate_CalendarControl);

        ResultList_eBussCore.WorkStation = Workstation_TextBox.Text;
        ResultList_eBussCore.Updated.WorkStation = pageView.GetUpdatedByView(Workstation_TextBox);

        Ervenyesseg_CalendarControl.SetErvenyessegFields(ResultList_eBussCore, pageView.GetUpdatedByView(Ervenyesseg_CalendarControl));

        ResultList_eBussCore.Base.Ver = FormHeader1.Record_Ver;
        ResultList_eBussCore.Base.Updated.Ver = true;

        return ResultList_eBussCore;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(ListName_TextBox);
            compSelector.Add_ComponentOnClick(Searched_Table_TextBox);
            compSelector.Add_ComponentOnClick(SaveDate_CalendarControl);
            compSelector.Add_ComponentOnClick(Workstation_TextBox);
            compSelector.Add_ComponentOnClick(Ervenyesseg_CalendarControl);
            compSelector.Add_ComponentOnClick(gridViewHeaderXML);

            FormFooter1.SaveEnabled = false;
        }
    }
    
    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "MentettTalalatok" + Command))
            {

            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
        //if (e.CommandName == "ResultList_ExportExcel")
        //{
        //    _temporaryConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ConnectionString);
        //    Result_List res = new Result_List(_temporaryConnection);
        //    string BodyXML = res.DataTable_To_XMLstring((gridViewBodyXML.DataSource as DataView).Table).Replace("\r\n","");
        //    string HeaderXML = res.DataTable_To_XMLstring((gridViewHeaderXML.DataSource as DataView).Table).Replace("\r\n","");
        //    res.SaveToExcel(BodyXML, HeaderXML, ListName_TextBox.Text);
        //}
    }

    private void SaveToExcel()
    {
        _temporaryConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ConnectionString);
        Result_List res = new Result_List(_temporaryConnection);
        string BodyXML = res.DataTable_To_XMLstring((gridViewBodyXML.DataSource as DataView).Table).Replace("\r\n", "");
        string HeaderXML = res.DataTable_To_XMLstring((gridViewHeaderXML.DataSource as DataView).Table).Replace("\r\n", "");
        res.SaveToExcel(BodyXML, HeaderXML, ListName_TextBox.Text);
    }


    #region GridView met�dusok
    protected void HeaderXMLGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewHeaderXML", ViewState, "DataTag");

        HeaderXMLGridViewBind(sortExpression, ASCENDING);
    }

    public SortDirection GridViewSortDirection
    {
        get 
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gridViewHeaderXML_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            HeaderXMLGridViewBind(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            HeaderXMLGridViewBind(sortExpression, ASCENDING);
        }
    }

    protected void HeaderXMLGridViewBind(string SortExpression, string SortDirection)
    {
        Result_List resultListHandler = new Result_List(_temporaryConnection);

        List<IdValue> searchList = new List<IdValue>();
        IdValue _item = new IdValue();

        _item.Id = "Id";
        _item.Value = Request.QueryString.Get(QueryStringVars.Id);
        searchList.Add(_item);
        DataTable _resultTable = new DataTable();
        _resultTable = resultListHandler.Load(searchList);
        if (_resultTable.Rows.Count != 0)
        {
            _resultTable = resultListHandler.XMLstring_To_DataTable(_resultTable.Rows[0]["HeaderXML"].ToString());
            DataView _view = new DataView(_resultTable);
            _view.Sort = SortExpression + SortDirection;
            gridViewHeaderXML.DataSource = _view;
            gridViewHeaderXML.DataBind();
        }
    }

    protected void BodyXMLGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewBodyXML", ViewState, "");

        BodyXMLGridViewBind(sortExpression, ASCENDING);
    }

    protected void BodyXMLGridViewBind(string SortExpression, string SortDirection)
    {
        _temporaryConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ConnectionString);
        Result_List resultListHandler = new Result_List(_temporaryConnection);

        List<IdValue> searchList = new List<IdValue>();
        IdValue _item = new IdValue();

        _item.Id = "Id";
        _item.Value = Request.QueryString.Get(QueryStringVars.Id);
        searchList.Add(_item);
        DataTable _resultTable = new DataTable();
        _resultTable = resultListHandler.Load(searchList);
        if (_resultTable.Rows.Count != 0)
        {
            _resultTable = resultListHandler.XMLstring_To_DataTable(_resultTable.Rows[0]["BodyXML"].ToString());
            DataView _view = new DataView(_resultTable);
            if (!string.IsNullOrEmpty(SortExpression))
            {
                _view.Sort = SortExpression + SortDirection;
            }
            gridViewBodyXML.DataSource = _view;
            gridViewBodyXML.DataBind();
            if (gridViewBodyXML.HeaderRow != null)
                gridViewBodyXML.HeaderRow.Cells[1].Visible = false;
            foreach (GridViewRow gvr in gridViewBodyXML.Rows)
            {
                gvr.Cells[1].Visible = false; //Id mez� l�that�s�ga 
            }
        }
    }

    protected void gridViewBodyXML_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            BodyXMLGridViewBind(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            BodyXMLGridViewBind(sortExpression, ASCENDING);
        }
    }
    #endregion
}
