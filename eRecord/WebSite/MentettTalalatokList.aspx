<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="MentettTalalatokList.aspx.cs" Inherits="MentettTalalatokList" Title="Mentett tal�lati list lek�rdez�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/JogosultakSubListHeader.ascx" TagName="JogosultakSubListHeader" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/JogosultakTabPage.ascx" TagName="JogosultakTabPage" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="ResultListCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
    <asp:UpdatePanel ID="ResultListUpdatePanel" Runat="server" OnLoad="ResultListUpdatePanel_Load">
        <ContentTemplate>
            <ajaxToolkit:CollapsiblePanelExtender ID="ResultListCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" ExpandControlID="ResultListCPEButton" CollapseControlID="ResultListCPEButton"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="ResultListCPEButton"
                ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja" CollapsedText="K�ldem�nyek list�ja">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel ID="Panel1" runat="server">
             <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                
                <asp:GridView ID="ResultListGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                    OnRowCommand="ResultListGridView_RowCommand" OnPreRender="ResultListGridView_PreRender"
                    OnSorting="ResultListGridView_Sorting"
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                &nbsp;&nbsp;
                                <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:CommandField>                        
                        <asp:BoundField DataField="SaveDate" HeaderText="Keres�s&nbsp;d�tuma" SortExpression="SaveDate">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ListName" HeaderText="Tal�lati&nbsp;lista&nbsp;neve" SortExpression="ListName">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Searched_Table" HeaderText="Keres�si&nbsp;t�bla" SortExpression="Searched_Table">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="WorkStation" HeaderText="Munka�llom�s" SortExpression="WorkStation">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                       <asp:BoundField DataField="ErvKezd" HeaderText="�rv.Kezd." SortExpression="ErvKezd">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ErvVege" HeaderText="�rv.V�ge" SortExpression="ErvVege">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="Note"> 
                             <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                                                                                                              
                       </asp:BoundField>   
                            <asp:BoundField DataField="Requestor" HeaderText="Keres� szem�ly" SortExpression="Note"> 
                             <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                                                                                                              
                       </asp:BoundField>        
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
                </td>
                                 </tr>
                             </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
           </td>
        </tr>
            </table>
</asp:Content>

