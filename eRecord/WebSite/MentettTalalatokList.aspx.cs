using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class MentettTalalatokList : Contentum.eUtility.UI.PageBase
{      
    
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MentettTalalatokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = "Mentett tal�lati list�k megjelen�t�se";
        ListHeader1.SearchObjectType = typeof(KRT_Mentett_Talalati_ListakSearch);

        ListHeader1.PrintVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("MentettTalalatokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, ResultListUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ResultListGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(ResultListGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(ResultListGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ResultListUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = ResultListGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(ResultListGridView);

        Search.SetIdsToSearchObject(Page, "Id", new KRT_Mentett_Talalati_ListakSearch());

        if (!IsPostBack) ResultListGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.IktatokonyvLezarasEnabled = false;

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "MentettTalalatok" + CommandName.View);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "MentettTalalatok" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(ResultListGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }


    #endregion

    #region Master List

    protected void ResultListGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ResultListGridView", ViewState, "SaveDate");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ResultListGridView", ViewState);

        ResultListGridViewBind(sortExpression, sortDirection);
    }

    protected void ResultListGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(ResultListGridView);
               
        KRT_Mentett_Talalati_ListakService service = eRecordService.ServiceFactory.GetKRT_Mentett_Talalati_ListakService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_Mentett_Talalati_ListakSearch search = null;
        search = (KRT_Mentett_Talalati_ListakSearch)Search.GetSearchObject(Page, new KRT_Mentett_Talalati_ListakSearch());

        search.OrderBy = Search.GetOrderBy("ResultListGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAll(ExecParam, search); 

        UI.GridViewFill(ResultListGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void ResultListGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ResultListGridView.PageIndex;

        ResultListGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = ResultListGridView.PageCount;

        if (prev_PageIndex != ResultListGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ResultListCPE);
            ResultListGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, ResultListCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(ResultListGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ResultListGridViewBind();
    }

    protected void ResultListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ResultListGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

        }
    }

    

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("MentettTalalatokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, ResultListUpdatePanel.ClientID);
        }
    }

    protected void ResultListUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ResultListGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedResultList();
            ResultListGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.SendObjects:
                SendMailSelectedResultList();
                break;
        }
    }

    /// <summary>
    /// T�rli a ResultListGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedResultList()
    {
        if (FunctionRights.GetFunkcioJog(Page, "MentettTalalatokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ResultListGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_Mentett_Talalati_ListakService service = eRecordService.ServiceFactory.GetKRT_Mentett_Talalati_ListakService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a ResultListGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedResultList()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(ResultListGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Mentett_Talalati_Listak");
        }
    }

    protected void ResultListGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ResultListGridViewBind(e.SortExpression, UI.GetSortToGridView("ResultListGridView", ViewState, e.SortExpression));
    }

    #endregion

}
