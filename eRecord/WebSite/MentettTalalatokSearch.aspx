<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="MentettTalalatokSearch.aspx.cs" Inherits="MentettTalalatokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc151" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc150" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc3" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoportokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <center>
                        <table cellspacing="0" cellpadding="0" width="70%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label1" runat="server" Text="Tal�lati lista neve:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="ListName_TextBox" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Keres�si t�bla:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Searched_Table_TextBox" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label3" runat="server" Text="Ment�s d�tuma:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc151:CalendarControl ID="SaveDate_CalendarControl" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label4" runat="server" Text="Keres� szem�ly:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc150:FelhasznaloTextBox ID="Requestor_PartnerTextBox" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label5" runat="server" Text="Munka�llom�s" />
                                </td>
                                <td class="mrUrlapMezo" style="height: 30px">
                                    <asp:TextBox ID="Workstation_TextBox" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Ervenyesseg_Label" runat="server" Text="�rv�nyess�g:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server"></uc5:Ervenyesseg_SearchFormComponent>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapMezo" colspan="2">
                                    <uc3:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                        runat="server"></uc3:TalalatokSzama_SearchFormComponent>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption"></td>
                            </tr>
                        </table>
                    </center>      
                </eUI:eFormPanel>
                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>


