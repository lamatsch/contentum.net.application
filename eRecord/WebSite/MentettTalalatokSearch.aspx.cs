using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class MentettTalalatokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_Mentett_Talalati_ListakSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Requestor_PartnerTextBox.Validate = false;
        SaveDate_CalendarControl.Validate = false;
        SearchHeader1.HeaderTitle = "Mentett találati listák";

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_Mentett_Talalati_ListakSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_Mentett_Talalati_ListakSearch)Search.GetSearchObject(Page, new KRT_Mentett_Talalati_ListakSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_Mentett_Talalati_ListakSearch ResultList_Search = null;
        if (searchObject != null) ResultList_Search = (KRT_Mentett_Talalati_ListakSearch)searchObject;

        if (ResultList_Search != null)
        {
            ListName_TextBox.Text = ResultList_Search.ListName.Value;
            Searched_Table_TextBox.Text = ResultList_Search.Searched_Table.Value;
            SaveDate_CalendarControl.Text = ResultList_Search.SaveDate.Value;

            Requestor_PartnerTextBox.Id_HiddenField = ResultList_Search.Requestor.Value;
            Requestor_PartnerTextBox.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            Workstation_TextBox.Text = ResultList_Search.WorkStation.Value;
            Ervenyesseg_SearchFormComponent1.SetDefault(ResultList_Search.ErvKezd, ResultList_Search.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_Mentett_Talalati_ListakSearch SetSearchObjectFromComponents()
    {
        KRT_Mentett_Talalati_ListakSearch ResultList_Search = (KRT_Mentett_Talalati_ListakSearch)SearchHeader1.TemplateObject;
        if (ResultList_Search == null)
        {
            ResultList_Search = new KRT_Mentett_Talalati_ListakSearch();
        }

        if (!String.IsNullOrEmpty(ListName_TextBox.Text))
        {
            ResultList_Search.ListName.Value = ListName_TextBox.Text;
            ResultList_Search.ListName.Operator = Query.Operators.like;
        }

        if (!String.IsNullOrEmpty(Searched_Table_TextBox.Text))
        {
            ResultList_Search.Searched_Table.Value = Searched_Table_TextBox.Text;
            ResultList_Search.Searched_Table.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(SaveDate_CalendarControl.Text))
        {
            var dateFrom = DateTime.Parse(SaveDate_CalendarControl.Text);
            var dateTo = dateFrom.AddDays(1);
            ResultList_Search.SaveDate.Value = dateFrom.ToShortDateString();
            ResultList_Search.SaveDate.ValueTo = dateTo.ToShortDateString();
            ResultList_Search.SaveDate.Operator = Query.Operators.between;
        }
        if (!String.IsNullOrEmpty(Requestor_PartnerTextBox.Id_HiddenField))
        {
            ResultList_Search.Requestor.Value = Requestor_PartnerTextBox.Id_HiddenField;
            ResultList_Search.Requestor.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Workstation_TextBox.Text))
        {
            ResultList_Search.WorkStation.Value = Workstation_TextBox.Text;
            ResultList_Search.WorkStation.Operator = Query.Operators.like;
        }
        
        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            ResultList_Search.ErvKezd, ResultList_Search.ErvVege);

        return ResultList_Search;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_Mentett_Talalati_ListakSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_Mentett_Talalati_ListakSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_Mentett_Talalati_ListakSearch();
    }

}

