using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class MetaAdatokForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Funkci�jog ellen�rz�s
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MetaadatokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, MetaAdatokTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(MetaAdatokTabContainer);

        SetEnabledAllTabsByFunctionRights();

        // Minden tab akt�v
        //SetEnabledAllTabs(true);
        TabMetaDefinicioPanel.Enabled = true;


        // TODO: Header
        //FormHeader1.FullManualHeaderTitle = Resources.Form.MetaAdatokFormHeaderTitle;
        FormHeader1.FullManualHeaderTitle = "Irat metadefinici� metaadatok";


       String id = Request.QueryString.Get(QueryStringVars.Id);
      

        // FormHeader be�ll�t�sa az IratMetaDefinicioTerkepTab1 -nek:
        IratMetaDefinicioTerkepTab1.FormHeader = FormHeader1;

        IratMetaDefinicioTerkepTab1.Active = true;
        // TODO: parentform
        //IratMetaDefinicioTerkepTab1.ParentForm = Constants.ParentForms.IraIrat;
        IratMetaDefinicioTerkepTab1.ParentForm = "MetaAdatKarbantartas";

    }



    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            // Default panel beallitasa
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            MetaAdatokTabContainer.ActiveTab = TabMetaDefinicioPanel;

            ActiveTabRefresh(MetaAdatokTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    #region Forms Tab

    protected void MetaAdatokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        IratMetaDefinicioTerkepTab1.Active = false;


        if (MetaAdatokTabContainer.ActiveTab.Equals(TabMetaDefinicioPanel))
        {
            IratMetaDefinicioTerkepTab1.Active = true;
            IratMetaDefinicioTerkepTab1.ReLoadTab();
        }

    }

    #endregion



    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void SetEnabledAllTabs(Boolean value)
    {
        TabMetaDefinicioPanel.Enabled = value;
    }

    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni!
        TabMetaDefinicioPanel.Enabled = true;
    }

}
