<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="MetaAdatokHierarchiaSearch.aspx.cs" Inherits="MetaAdatokHierarchiaSearch" Title="Metaadat keres�s" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/AgazatiJelTextBox.ascx" TagName="AgazatiJelTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="width: 1027px">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAgazatiJelKod" runat="server" Text="�gazati jel k�d:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="AgazatiJelKodTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAgazatiJelNev" runat="server" Text="�gazati jel megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="AgazatiJelNevTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="IrattariTetelszamTextBox" runat="server" CssClass="mrUrlapInput" Width="287px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;&nbsp;
                                <asp:Label ID="Label16" runat="server" Text="Iratt�ri t�tel megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="Nev_TextBox" runat="server" Width="287px"></asp:TextBox></td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgytipuskod" runat="server" Text="�gyt�pus k�d:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="UgyTipusKodTextBox" runat="server" Width="250"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="�gyt�pus megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="UgyTipusNevTextBox" runat="server" Width="250"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Elj�r�si szakasz:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="EljarasiSzakaszKodtarakDropDownList" runat="server" />
                            </td>                            
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Iratt�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="IratTipusKodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="Gener�lt t�rgy:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="GeneraltTargyTextBox" runat="server" Width="250"></asp:TextBox></td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelContentType" runat="server" Text="ContentType:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="ContentTypeTextBox" runat="server" />
                            </td>
                        </tr>
		        
		        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFunkcio" runat="server" Text="Funkci�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="FunkcioTextBox" runat="server" CssClass="mrUrlapInput" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
		        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="labelTargySzavak" runat="server" Text="T�rgyszavak:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="TargySzavakTextBox" runat="server" />
                            </td>
                        </tr>
<%-- 
                        <tr class="urlapSor">
                            <td colspan="4">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                        --%>
                    </table>
                    </eUI:eFormPanel>
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">
                                &nbsp;</td>
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                      </table>

                </td>
            </tr>
        </table>
</asp:Content>

