using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;


public partial class MetaAdatokHierarchiaSearch : System.Web.UI.Page
{
    /*
    public const string Header = "$Header: MetaAdatokHierarchiaSearch.aspx.cs, 2, 2008.08.06. 15:49:12, Boda Eszter$";
    public const string Version = "$Revision: 2$";
    */

    private Type _type = typeof(MetaAdatHierarchiaSearch);
    private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";
    private const string kcs_ELJARASISZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_SZIGNALASTIPUSA = "SZIGNALAS_TIPUSA";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (" + Resources.Search.IratMetaDefinicioSearchHeaderTitle + ")");
        SearchHeader1.HeaderTitle = Resources.Search.IratMetaDefinicioSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            MetaAdatHierarchiaSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (MetaAdatHierarchiaSearch)Search.GetSearchObject(Page, new MetaAdatHierarchiaSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        Logger.Info("LoadComponentsFromSearchObject");
        MetaAdatHierarchiaSearch metaAdatHierarchiaSearch = null;
        if (searchObject != null) metaAdatHierarchiaSearch = (MetaAdatHierarchiaSearch)searchObject;

        if (metaAdatHierarchiaSearch != null)
        {
            AgazatiJelKodTextBox.Text = metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Value;
            AgazatiJelNevTextBox.Text = metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Value;

            IrattariTetelszamTextBox.Text = metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Value;
            Nev_TextBox.Text = metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Value;
                   
            EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, SearchHeader1.ErrorPanel);
            EljarasiSzakaszKodtarakDropDownList.SetSelectedValue(metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Value);

            IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, SearchHeader1.ErrorPanel);
            IratTipusKodtarakDropDownList.SetSelectedValue(metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Value);

            UgyTipusNevTextBox.Text = metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Value;

            UgyTipusKodTextBox.Text = metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Value;

            GeneraltTargyTextBox.Text = metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Value;
            
            ContentTypeTextBox.Text = metaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch.ContentType.Value;
            
            FunkcioTextBox.Text = metaAdatHierarchiaSearch.EREC_Obj_MetaAdataiSearch.Funkcio.Value;

            TargySzavakTextBox.Text = metaAdatHierarchiaSearch.EREC_TargySzavakSearch.TargySzavak.Value;
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private MetaAdatHierarchiaSearch SetSearchObjectFromComponents()
    {
        Logger.Info("SetSearchObjectFromComponents");
        MetaAdatHierarchiaSearch metaAdatHierarchiaSearch = (MetaAdatHierarchiaSearch)SearchHeader1.TemplateObject;
        if (metaAdatHierarchiaSearch == null)
        {
            metaAdatHierarchiaSearch = new MetaAdatHierarchiaSearch();
        }
        
        if (!String.IsNullOrEmpty(AgazatiJelKodTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Value = AgazatiJelKodTextBox.Text;
            metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Operator = Search.GetOperatorByLikeCharater(AgazatiJelKodTextBox.Text);
        }

        if (!String.IsNullOrEmpty(AgazatiJelNevTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Value = AgazatiJelNevTextBox.Text;
            metaAdatHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(AgazatiJelNevTextBox.Text);
        }

        if (!String.IsNullOrEmpty(IrattariTetelszamTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Value = IrattariTetelszamTextBox.Text;
            metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Operator = Search.GetOperatorByLikeCharater(IrattariTetelszamTextBox.Text);
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Value = Nev_TextBox.Text;
            metaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }


        if (!String.IsNullOrEmpty(EljarasiSzakaszKodtarakDropDownList.SelectedValue))
        {
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Value = EljarasiSzakaszKodtarakDropDownList.SelectedValue;
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IratTipusKodtarakDropDownList.SelectedValue))
        {
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Value = IratTipusKodtarakDropDownList.SelectedValue;
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(UgyTipusKodTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Value = UgyTipusKodTextBox.Text;
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Operator = Search.GetOperatorByLikeCharater(UgyTipusKodTextBox.Text);
        }

        if (!String.IsNullOrEmpty(UgyTipusNevTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Value = UgyTipusNevTextBox.Text;
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Operator = Search.GetOperatorByLikeCharater(UgyTipusNevTextBox.Text);
        }

        if (!String.IsNullOrEmpty(GeneraltTargyTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Value = GeneraltTargyTextBox.Text;
            metaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Operator = Search.GetOperatorByLikeCharater(GeneraltTargyTextBox.Text);
        }

        //// csak ügyiratnyilvántartáshoz
        //metaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch.DefinicioTipus.Value = KodTarak.OBJMETADEFINICIO_TIPUS.C2;
        //metaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch.DefinicioTipus.Operator = Query.Operators.equals;

        if (!String.IsNullOrEmpty(ContentTypeTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch.ContentType.Value = ContentTypeTextBox.Text;
            metaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch.ContentType.Operator = Search.GetOperatorByLikeCharater(ContentTypeTextBox.Text);
        }
        
        if (!String.IsNullOrEmpty(FunkcioTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_Obj_MetaAdataiSearch.Funkcio.Value = FunkcioTextBox.Text;
            metaAdatHierarchiaSearch.EREC_Obj_MetaAdataiSearch.Funkcio.Operator = Search.GetOperatorByLikeCharater(FunkcioTextBox.Text);
        }
        
        if (!String.IsNullOrEmpty(TargySzavakTextBox.Text))
        {
            metaAdatHierarchiaSearch.EREC_TargySzavakSearch.TargySzavak.Value = TargySzavakTextBox.Text;
            metaAdatHierarchiaSearch.EREC_TargySzavakSearch.TargySzavak.Operator = Search.GetOperatorByLikeCharater(TargySzavakTextBox.Text);
        }

        return metaAdatHierarchiaSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchHeader1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchFooter1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            MetaAdatHierarchiaSearch searchObject = SetSearchObjectFromComponents();
            MetaAdatHierarchiaSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject.EREC_AgazatiJelekSearch, defaultSearchObject.EREC_AgazatiJelekSearch)
                && Search.IsIdentical(searchObject.EREC_IraIrattariTetelekSearch, defaultSearchObject.EREC_IraIrattariTetelekSearch)
                && Search.IsIdentical(searchObject.EREC_IratMetaDefinicioSearch, defaultSearchObject.EREC_IratMetaDefinicioSearch)
                && Search.IsIdentical(searchObject.EREC_Obj_MetaDefinicioSearch, defaultSearchObject.EREC_Obj_MetaDefinicioSearch)
                && Search.IsIdentical(searchObject.EREC_Obj_MetaAdataiSearch, defaultSearchObject.EREC_Obj_MetaAdataiSearch)
                && Search.IsIdentical(searchObject.EREC_TargySzavakSearch, defaultSearchObject.EREC_TargySzavakSearch)
                )
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            //TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private MetaAdatHierarchiaSearch GetDefaultSearchObject()
    {
        MetaAdatHierarchiaSearch metaAdatHierarchiaSearch = new MetaAdatHierarchiaSearch();
        // érvényességet nem adunk át
        return metaAdatHierarchiaSearch;
    }

}
