<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Munkanaplo.aspx.cs" Inherits="Munkanaplo" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="slh" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="fcstb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="UgyintezoUpdatePanel" runat="server">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="UgyintezoPanel" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelUgyintezo" runat="server" Text="�gyint�z�:" />
                                    </td>
                                    <td style="text-align: left;">
                                        <fcstb:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox_Ugyintezo" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="UgyUgyiratokCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="UgyUgyiratokUpdatePanel" runat="server" OnLoad="UgyUgyiratokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="UgyUgyiratokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="UgyUgyiratokCPEButton"
                            CollapseControlID="UgyUgyiratokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="UgyUgyiratokCPEButton" ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja"
                            CollapsedText="K�ldem�nyek list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <%--                                <tr>
                                    <td>
                                        <eUI:eFormPanel ID="UgyintezoPanel" runat="server">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelUgyintezo" runat="server" Text="�gyint�z�:" />
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <fcstb:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox_Ugyintezo" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </eUI:eFormPanel>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="UgyUgyiratokGridView_RowCommand" OnPreRender="UgyUgyiratokGridView_PreRender"
                                            OnSorting="UgyUgyiratokGridView_Sorting" OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" OnRowUpdating="UgyUgyiratokGridView_RowUpdating"
                                            OnRowCancelingEdit="UgyUgyiratokGridView_RowCancelingEdit" OnSelectedIndexChanging="UgyUgyiratokGridView_SelectedIndexChanging">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div class="DisableWrap">
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" BorderWidth="1" HorizontalAlign="Center"
                                                        VerticalAlign="Middle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="25px" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ID="ibtnSelect" CommandName="Select" ImageUrl="~/images/hu/Grid/3Drafts.gif"
                                                            AlternateText="<%$Resources:List,AlternateText_RowSelectButton%>" />
                                                        <asp:ImageButton runat="server" ID="ibtnUpdate" CommandName="Update" ImageUrl="~/images/hu/Grid/saverow.gif"
                                                            AlternateText="Elment" Visible="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <%--BLG_619--%>
                                                <asp:BoundField DataField="UgyFelelos_SzervezetKod" HeaderText="<%$Forditas:BoundFieldUgyFelelos_SzervezetKod|Szk%>" SortExpression="Csoportok_UgyFelelosNev.Kod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="20px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:HyperLinkField DataTextField="Foszam_Merge" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="UgyUgyiratokList.aspx?Id={0}" HeaderText="Iktat�sz�m" SortExpression="EREC_UgyUgyiratok.Foszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UgyTipus_Nev" HeaderText="�gyt�pus" SortExpression="EREC_UgyUgyiratok.UgyTipus_Nev" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px"  />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="EREC_UgyUgyiratok.NevSTR_Ugyindito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField SortExpression="EREC_UgyUgyiratok_Szulo.Foszam" HeaderText="El�iratok">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" Width="120px" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="labelEloiratok" Text='<%# FormatEloiratok(Eval("ElozmenyAzonositok"),Eval("ElozmenyIds"),Eval("ElozmenyTypes")) %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField SortExpression="EREC_UgyUgyiratok_Szulo.Foszam" HeaderText="Ut�irat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" Width="120px"/>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="labelUtoirat" onclick='<%# GetFoszamViewClientClick(Eval("UgyUgyirat_Id_Szulo")) %>'
                                                            ToolTip="Megtekint">
                                                            <asp:HyperLink runat="server" ID="hplUtoirat" NavigateUrl="" Text='<%#Eval("Foszam_Merge_Szulo") %>'>
                                                            </asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:BoundField DataField="LetrehozasIdo_Rovid" HeaderText="Iktat�s&nbsp;d�tuma"
                                                    SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbIratUgyintezo" runat="server" Checked="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" Width="120px" />
                                                    <ItemTemplate>
                                                        <%-- skontr�ban eset�n (07) skontr� v�ge megjelen�t�se --%>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%#                                        
                                                        String.Concat(                                          
                                                        Eval("Allapot_Nev")
                                                        , (Eval("Allapot") as string) == "07" ? "<br />(" + Eval("SkontroVege_Rovid") + ")" : ""
                                                        , (Eval("Allapot") as string) == "60" ? "<br /><span onclick=\"" + GetFoszamViewClientClick(Eval("UgyUgyirat_Id_Szulo")) +"\" title=\"Megtekint\">"
                                                        + "<a href=\"\" style=\"text-decoration:underline\"> ("
                                                        + Eval("Foszam_Merge_Szulo") as string +")<a /></span>" : "") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Alsz�m/Db" SortExpression="EREC_UgyUgyiratok.UtolsoAlszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelUtolsoAlszam" runat="server" Text='<%#String.Format("{0} / {1}",Eval("UtolsoAlszam"),Eval("IratSzam"))%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Kezel�si feljegyz�s" SortExpression="">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelNote" runat="server" Text='<%# Eval("MunkanaploKezelesiFeljegyzes") %>'></asp:Label>
                                                        <asp:TextBox ID="txtNote" runat="server" Text='<%# Bind("MunkanaploKezelesiFeljegyzes") %>'
                                                            Visible="false" Style="width: 90%"></asp:TextBox>
                                                        <asp:Label ID="labelNote_Id" runat="server" Text='<%# Eval("MunkanaploKezelesiFeljegyzes_Id") %>'
                                                            Visible="false"></asp:Label>
                                                        <asp:Label ID="labelNote_Ver" runat="server" Text='<%# Eval("MunkanaploKezelesiFeljegyzes_Ver") %>'
                                                            Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="EsemenyekTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:Label ID="Header1" runat="server" Text="Esem�nyek"></asp:Label>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="EsemenyekUpdatePanel" runat="server" OnLoad="EsemenyekUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="EsemenyekPanel" runat="server" Visible="false" Width="100%">
                                                        <table cellpadding="0" cellspacing="0" style="width:100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="cbEnableItems_View" runat="server" Text="Megtekint�sek is" Checked="false"
                                                                        AutoPostBack="true" OnCheckedChanged="cbEnableItems_View_CheckedChanged" ToolTip="Megtekint�sek is megjelenjenek"/>
                                                                </td>
                                                                <td>
                                                                    <slh:SubListHeader ID="EsemenyekSubListHeader" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="EsemenyCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                        <asp:GridView ID="EsemenyekGridView" runat="server" CellPadding="0" BorderWidth="1px"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                            OnSorting="EsemenyekGridView_Sorting" OnPreRender="EsemenyekGridView_PreRender"
                                                                            OnRowCommand="EsemenyekGridView_RowCommand" DataKeyNames="Id" OnSelectedIndexChanging="EsemenyekGridView_SelectedIndexChanging"
                                                                            OnRowUpdating="EsemenyekGridView_RowUpdating" OnRowCancelingEdit="EsemenyekGridView_RowCancelingEdit"
                                                                            OnRowDataBound="EsemenyekGridView_RowDataBound">
                                                                            <PagerSettings Visible="False" />
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <%--<asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" BorderWidth="1" HorizontalAlign="Center"
                                                                                        VerticalAlign="Middle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="25px" />
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="ibtnSelect" CommandName="Select" ImageUrl="~/images/hu/Grid/3Drafts.gif"
                                                                                            AlternateText="<%$Resources:List,AlternateText_RowSelectButton%>" />
                                                                                        <asp:ImageButton runat="server" ID="ibtnUpdate" CommandName="Update" ImageUrl="~/images/hu/Grid/saverow.gif"
                                                                                            AlternateText="Elment" Visible="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField SortExpression="" HeaderText="Alsz�m">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="labelAlszam" onclick='<%# GetAlszamModifyClientClick(Eval("Obj_Id")) %>'
                                                                                            ToolTip="M�dos�t">
                                                                                            <asp:HyperLink runat="server" ID="hplAlszam" NavigateUrl="" Text='<%#Eval("Alszam") %>'>
                                                                                            </asp:HyperLink>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Funkciok_Nev" HeaderText="Esem�ny" SortExpression="KRT_Funkciok.Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="K�ld� / c�mzett" SortExpression="">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="labelParnterCim" Text='<%# FormatPartnerCim(Eval("PartnerNev"),Eval("PartnerCim")) %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="T�rgy&nbsp;/&nbsp;Kifejt�s" SortExpression="KRT_Esemenyek.Azonositoja">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="labelObjektum" Text='<%# GetFeljegyzesViewLink(Eval("Obj_Type"),Eval("Obj_Id"),Eval("Targy")) %>'>
                                                                                    </asp:Label>      
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Hiv.sz�m" SortExpression="">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Esem�ny kelte" SortExpression="KRT_Esemenyek.LetrehozasIdo">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="labelIntezkedesKelte" Text='<%# Eval("LetrehozasIdo")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Felhasznalo_Id_User_Nev" HeaderText="Int�zked�" SortExpression="KRT_Felhasznalok.Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Megjegyz�s" SortExpression="KRT_Esemenyek.Note">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelNote" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                                                                                        <asp:TextBox ID="txtNote" runat="server" Text='<%# Bind("Note") %>' Visible="false"
                                                                                            Style="width: 90%"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelVer" runat="server" Text='<%# Eval("Ver") %>'></asp:Label>
                                                                                        <asp:Label ID="labelObjType" runat="server" Text='<%# Eval("ObjTip_Id_Kod") %>'></asp:Label>
                                                                                        <asp:Label ID="labelObjTypeFeladat" runat="server" Text='<%# Eval("ObjType_Feladat") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--<asp:BoundField DataField="Felhasznalo_Id_User_Nev" HeaderText="Felhaszn�l�" SortExpression="KRT_Felhasznalok.Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                                <%--<asp:CommandField CancelText="M�gse" 
                                                                                    CausesValidation="False" DeleteText="T�r�l" EditText="M�dos�t" 
                                                                                    InsertText="Besz�r" InsertVisible="False" NewText="�j" SelectText="Kiv�laszt" 
                                                                                    ShowEditButton="True" UpdateText="Elment" />--%>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
