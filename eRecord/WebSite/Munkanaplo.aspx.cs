using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Munkanaplo : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    private readonly System.Drawing.Color colorCsabi = System.Drawing.Color.FromArgb(251, 212, 180);
    private const string GridViewSpecialRowStyle = "GridViewSpecialRowStyle";
    private const string GridViewSpecialSelectedRowStyle = "GridViewSpecialSelectedRowStyle";

    #region Utils
    private CheckBox GetIratUgyintezoCheckBox(GridViewRow row)
    {
        return (CheckBox)row.FindControl("cbIratUgyintezo");
    }

    private void SetIratUgyintezoRow(GridViewRow row)
    {
        if (row != null && row.DataItem != null)
        {
            CheckBox cbIratUgyintezo = GetIratUgyintezoCheckBox(row);
            if (cbIratUgyintezo != null)
            {
                System.Data.DataRowView drw = (System.Data.DataRowView)row.DataItem;

                string FelhasznaloCsoport_Id_Ugyintez = drw["FelhasznaloCsoport_Id_Ugyintez"].ToString();

                if (!String.IsNullOrEmpty(FelhasznaloCsoport_Id_Ugyintez))
                {
                    string ugyintezoId = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
                    if (!String.IsNullOrEmpty(FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField))
                    {
                        ugyintezoId = FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField;
                    }

                    cbIratUgyintezo.Checked = !String.Equals(FelhasznaloCsoport_Id_Ugyintez
                        , ugyintezoId
                        , StringComparison.InvariantCultureIgnoreCase);
                }
            }
        }
    }

    private bool IsIratUgyintezoRow(GridViewRow row)
    {
        CheckBox cbIratUgyintezo = GetIratUgyintezoCheckBox(row);
        if (cbIratUgyintezo != null)
        {
            return cbIratUgyintezo.Checked ;
        }

        return false;
    }
    #endregion Utils

    private String FunkcioIds_View
    {
        get
        {
            if (ViewState["FunkcioIds_View"] == null)
            {
                KRT_FunkciokService service_funkciok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();
                ExecParam ExecParam_funkciok = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_FunkciokSearch search_funkciok = new KRT_FunkciokSearch();

                List<string> ViewFunkcioKodList = new List<string>();
                ViewFunkcioKodList.Add("UgyiratView");
                ViewFunkcioKodList.Add("IraIratView");
                ViewFunkcioKodList.Add("IratPeldanyView");
                ViewFunkcioKodList.Add("KuldemenyView");
                //ViewFunkcioKodList.Add("UgyiratViewHistory");
                //ViewFunkcioKodList.Add("IraIratViewHistory");
                //ViewFunkcioKodList.Add("IratPeldanyViewHistory");
                //ViewFunkcioKodList.Add("KuldemenyViewHistory");
                //ViewFunkcioKodList.Add("IraIratCsatolmanyView");
                //ViewFunkcioKodList.Add("VezetoiPanelView");

                search_funkciok.Kod.Value = Search.GetSqlInnerString(ViewFunkcioKodList.ToArray());
                search_funkciok.Kod.Operator = Contentum.eQuery.Query.Operators.inner;


                Result result_funkciok = service_funkciok.GetAll(ExecParam_funkciok, search_funkciok);
                string ViewIds = String.Empty;

                if (!result_funkciok.IsError)
                {

                    List<string> ViewIdsList = new List<string>();
                    foreach (DataRow row in result_funkciok.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(id))
                        {
                            ViewIdsList.Add(id);
                        }
                    }

                    if (ViewIdsList.Count > 0)
                    {
                        ViewIds = Search.GetSqlInnerString(ViewIdsList.ToArray());
                    }

                }

                ViewState["FunkcioIds_View"] = ViewIds;
            }
            return (string)ViewState["FunkcioIds_View"];
        }

    }

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {        
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        EsemenyekSubListHeader.RowCount_Changed += new EventHandler(EsemenyekSubListHeader_RowCount_Changed);

        EsemenyekSubListHeader.PagingButtonsClick += new EventHandler(EsemenyekSubListHeader_PagingButtonsClick);

        //EsemenyekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(EsemenyekSubListHeader_ErvenyessegFilter_Changed);
        EsemenyekSubListHeader.ValidFilterVisible = false;

        FelhasznaloCsoportTextBox_Ugyintezo.Validate = false;
        FelhasznaloCsoportTextBox_Ugyintezo.TextBox.CssClass = "mrUrlapInputSzeles";
        FelhasznaloCsoportTextBox_Ugyintezo.CsakSajatSzervezetDolgozoi = true;
        FelhasznaloCsoportTextBox_Ugyintezo.SearchMode = true;

        // BLG_619
        if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {
            UI.SetGridViewColumnVisiblity(UgyUgyiratokGridView, "UgyFelelos_SzervezetKod", true);
        }
        else UI.SetGridViewColumnVisiblity(UgyUgyiratokGridView, "UgyFelelos_SzervezetKod", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // jobb oldali ikonok l�that�s�g�nak be�ll�t�sa
        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.AtadasraKijelolVisible = true;
        ListHeader1.IrattarozasraVisible = true;
        ListHeader1.LezarasVisible = true;
        ListHeader1.LezarasVisszavonasVisible = true;
        ListHeader1.AtvetelVisible = true;
        ListHeader1.AtvetelUgyintezesreVisible = true;

        ListHeader1.HeaderLabel = Resources.List.MunkanaploListHeaderTitle;


        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;
        //ListHeader1.ModifyVisible = false;
        ListHeader1.UgyiratTerkepVisible = true;
        ListHeader1.HistoryVisible = false;
        //ListHeader1.FTSearchVisible = true;

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = false;

        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;

        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.MunaknaploSearch;

        ListHeader1.NumericSearchVisible = true;

        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);

        //LZS - BLG_5998
        ListHeader1.ElintezetteNyilvanitasVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes", "Javascripts/CheckBoxes.js");
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);


        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);


        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkanaplo
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkanaplo, Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //LZS - BLG_5998
        ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UgyUgyiratokGridView.ClientID);

        ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + UgyUgyiratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        // BUG_8005
        // ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.IrattarozasraOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + UgyUgyiratokGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                + UgyUgyiratokGridView.ClientID + "','check'); "
                                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        // BUG_5993
        //ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.AtvetelUgyintezesreOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LezarasVisszavonasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(UgyUgyiratokGridView.ClientID);
        //ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(UgyUgyiratokGridView.ClientID);

        //ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratokGridView.ClientID);

        //ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
        //   QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
        //            , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = UgyUgyiratokGridView;
        // sorkijel�l�s megsz�ntet�se funkci� tilt�sa, mert bezavar:
        ListHeader1.AttachedGridView_RowDeselectFunctionEnabled = false;

        //EsemenyekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EsemenyekSubListHeader.NewVisible = false;
        //EsemenyekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EsemenyekSubListHeader.ViewVisible = false;
        //EsemenyekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EsemenyekSubListHeader.ModifyVisible = false;
        //EsemenyekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(EsemenyekGridView.ClientID);
        EsemenyekSubListHeader.InvalidateVisible = false;
        EsemenyekSubListHeader.ButtonsClick += new CommandEventHandler(EsemenyekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        EsemenyekSubListHeader.AttachedGridView = EsemenyekGridView;

        Search.SetIdsToSearchObject(Page, "Id", GetDefaultSearchObject(), Constants.CustomSearchObjectSessionNames.MunaknaploSearch);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!IsPostBack)
        {
            #region �gyint�z� sz�r�s be�ll�t�sa
            // alap�rtelmez�sben az aktu�lis felhaszn�l� van be�ll�tva
            FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
            FelhasznaloCsoportTextBox_Ugyintezo.SetCsoportTextBoxById(EErrorPanel1);
            string CsoportTagsag_Tipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
            if (CsoportTagsag_Tipus == KodTarak.CsoprttagsagTipus.vezeto)
            {
                UgyintezoPanel.Visible = true;
            }
            else
            {
                UgyintezoPanel.Visible = false;
            }
            #endregion

            UgyUgyiratokGridViewBind();
        }

        FelhasznaloCsoportTextBox_Ugyintezo.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
        FelhasznaloCsoportTextBox_Ugyintezo.TextBox.AutoPostBack = true;
        FelhasznaloCsoportTextBox_Ugyintezo.RefreshCallingWindow = true;

    }

    protected void TextBox_TextChanged(object sender, EventArgs e)
    {
        TextBox tb = (TextBox)sender;
        if (tb != null)
        {
            if (tb.ID == FelhasznaloCsoportTextBox_Ugyintezo.TextBox.ID)
            {
                UgyUgyiratokGridViewBind();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {        
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Modify);
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.ViewHistory);

        ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtadas");
        ListHeader1.IrattarozasraEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba");
        ListHeader1.LezarasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratLezaras");
        ListHeader1.LezarasVisszavonasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratLezarasVisszavonas");
        ListHeader1.AtvetelUgyintezesreEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetelUgyintezesre");
        ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetel");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);

        EsemenyekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Esemeny" + CommandName.New); ;
        EsemenyekSubListHeader.ViewEnabled = false;
        EsemenyekSubListHeader.ModifyEnabled = false;
        EsemenyekSubListHeader.InvalidateEnabled = false;

        //LZS - BLG_5998
        ListHeader1.ElintezetteNyilvanitasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratElintezes");

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            ActiveTabRefreshOnClientClicks();
            //ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(UgyUgyiratokGridView);
        //ui.SetClientScriptToGridViewSelectDeSelectButton(EsemenyekGridView);

        // FelhasznaloCsoportTextBox Reset hat�s�ra az eredeti felhaszn�l�ra ugrik
        string felhasznaloId = FelhasznaloProfil.FelhasznaloId(Page);
        string felhasznaloCsoportNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(felhasznaloId, Page);
        FelhasznaloCsoportTextBox_Ugyintezo.OnClick_Reset = "var tbox=$get('" + FelhasznaloCsoportTextBox_Ugyintezo.TextBox.ClientID + "');tbox.value = '" + felhasznaloCsoportNev
            + "';$get('" + FelhasznaloCsoportTextBox_Ugyintezo.HiddenField.ClientID + "').value = '" + felhasznaloId + "';"
            //+ "$common.tryFireEvent(tbox, 'change');"
            + "return true;";

        string jsSelectAll = @"function SelectAllUgyintezo(e){var tb=$get('" + FelhasznaloCsoportTextBox_Ugyintezo.TextBox.ClientID + @"');
                              if(tb){Utility.Selection.textboxSelect(tb,0);};}
                              Sys.Application.add_load(function(){var tb=$get('" + FelhasznaloCsoportTextBox_Ugyintezo.TextBox.ClientID + @"');
                              if(tb){$addHandler(tb,'focus',SelectAllUgyintezo);}});";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SelectAllUgyintezo", jsSelectAll, true);
    }


    #endregion

    #region Master List

    protected EREC_UgyUgyiratokSearch GetDefaultSearchObject()
    {
        var search = new EREC_UgyUgyiratokSearch(true);
        
        // BLG_7987
        var kornyezet = FelhasznaloProfil.OrgKod(Page);
        if (kornyezet == "CSPH" || kornyezet == "NMHH")
        {
            search.CsakAktivIrat = true;
        }
        return search;
    }

    protected void UgyUgyiratokGridViewBind()
    {
        //String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratokGridView", ViewState, "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam");
        // L�trehoz�s ideje szerint rendez�nk alapb�l
        String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratokGridView", ViewState, "EREC_UgyUgyiratok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("UgyUgyiratokGridView", ViewState, SortDirection.Descending);

        UgyUgyiratokGridViewBind(sortExpression, sortDirection);
    }

    protected void UgyUgyiratokGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        var search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, GetDefaultSearchObject(), Constants.CustomSearchObjectSessionNames.MunaknaploSearch);

        /// Alap sz�r�si felt�tel: (nem hozzuk az iktat�srael�k�sz�tett �gyiratokat)
        //search.FelhasznaloCsoport_Id_Ugyintez.Value = ExecParam.Felhasznalo_Id;
        //search.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

        if (!String.IsNullOrEmpty(FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField))
        {
            if(!String.IsNullOrEmpty(search.UgyUgyiratSearchUgyintezoGuid))
            {
                search.FelhasznaloCsoport_Id_Ugyintez.Value = search.UgyUgyiratSearchUgyintezoGuid;
            }
            else
            {
                search.FelhasznaloCsoport_Id_Ugyintez.Value = FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField;
            }

            search.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("UgyUgyiratokGridView", ViewState, SortExpression, SortDirection);
            search.TopRow = UI.GetTopRow(Page);

            // Lapoz�s be�ll�t�sa:
            UI.SetPaging(ExecParam, ListHeader1);
            //SetPaging(ExecParam, ListHeader1);

            Result res = service.GetAllWithExtensionAndJogosultakForMunkanaplo(ExecParam, search, true);
            //Result res = service.GetAllWithExtension(ExecParam, search);

            object oldDataKey= UgyUgyiratokGridView.SelectedValue;
            int oldIndex = UgyUgyiratokGridView.SelectedIndex;
            UI.GridViewFill(UgyUgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            object newDataKey = UgyUgyiratokGridView.SelectedValue;
            int newIndex = UgyUgyiratokGridView.SelectedIndex;
            RaiseGridViewSelectedIndexChanged(UgyUgyiratokGridView, oldIndex, oldDataKey, newIndex, newDataKey);
            //GridViewFill(UgyUgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {
            ui.GridViewClear(UgyUgyiratokGridView);
        }
    }

    private void RaiseGridViewSelectedIndexChanged(GridView gridView, int oldIndex, object oldDataKey, int newIndex, object newDataKey)
    {
        string oldDataKeyString = (oldDataKey ?? String.Empty).ToString();
        string newDataKeyString = (newDataKey ?? String.Empty).ToString();

        if (newDataKeyString != oldDataKeyString)
        {
            GridViewSelectEventArgs gsea = new GridViewSelectEventArgs(newIndex);
            
            gridView.SelectedIndex = -1;

            if (gridView == UgyUgyiratokGridView)
                UgyUgyiratokGridView_SelectedIndexChanging(gridView, gsea);

            if(gridView == EsemenyekGridView)
                EsemenyekGridView_SelectedIndexChanging(gridView, gsea);
                
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //GridView_RowDataBound_SetSzerelesInfo(e);
        //GridView_RowDataBound_SetCsatolasInfo(e);
        //GridView_RowDataBound_SetVegyesInfo(e);
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);    

        GridView_RowDataBound_SetUgyintezoInfo(e);
    }

    private void GridView_RowDataBound_SetUgyintezoInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SetIratUgyintezoRow(e.Row);
            if (e.Row.RowIndex == (e.Row.NamingContainer as GridView).SelectedIndex)
            {
                SetGridViewRowBackGroundColor(e.Row, 1);
            }
            else
            {
                SetGridViewRowBackGroundColor(e.Row, 0);
            }
        }
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        CsatolmanyImage.Visible = true;
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage  != null)
                {
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolsaCount = String.Empty;

            if (drw["CsatolasCount"] != null)
            {
                csatolsaCount = drw["CsatolasCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolsaCount))
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

                if (CsatoltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolsaCount, out count);
                    if (count > 0)
                    {
                        CsatoltImage.Visible = true;
                        CsatoltImage.ToolTip = String.Format("Csatol�s: {0} darab", csatolsaCount);
                        if (drw["Id"] != null)
                        {
                            string ugyiratId = drw["Id"].ToString();
                            string onclick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + ugyiratId +
                            "&" + QueryStringVars.SelectedTab + "=" + "Csatolas"
                             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
                            CsatoltImage.Attributes.Add("onclick",onclick);
                            
                        }
                    }
                    else
                    {
                        CsatoltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");
                if (CsatoltImage != null)
                {
                    CsatoltImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetSzerelesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string szereltCount = String.Empty;

            if (drw["SzereltCount"] != null)
            {
                szereltCount = drw["SzereltCount"].ToString();
            }

            if (!String.IsNullOrEmpty(szereltCount))
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(szereltCount,out count);
                    if (count > 0)
                    {
                        SzereltImage.Visible = true;
                    }
                    else
                    {
                        SzereltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");
                if (SzereltImage != null)
                {
                    SzereltImage.Visible = false;
                }

            }
        }
    }

    protected void UgyUgyiratokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, UgyUgyiratokCPE);

        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        UgyUgyiratokGridViewBind();
        ActiveTabClear();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, UgyUgyiratokCPE);
        UgyUgyiratokGridViewBind();
        ActiveTabClear();
    }

    protected void UgyUgyiratokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            //int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(UgyUgyiratokGridView, selectedRowNumber, "check");

           //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

           //ActiveTabRefresh(TabContainer1, id);

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            //string tableName = "EREC_UgyUgyiratok";
            //ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
            //    , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
            //    , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, UgyUgyiratokUpdatePanel.ClientID);
            //ListHeader1.PrintOnClientClick = "javascript:window.open('UgyUgyiratokPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + id + "')";

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(id, execParam, EErrorPanel1);
            ErrorDetails errorDetail;
            
            // M�dos�t�s
            if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes
                   + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" + JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";

                // �gyiratt�rk�p View m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                       + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                // �gyiratt�rk�p Modify m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                   + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
            }

            // �tv�tel �gyint�z�sre
            // BUG_5993
            //if (Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail))
            //{
            //    ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreForm.aspx",
            //        QueryStringVars.UgyiratId + "=" + id
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52161
            //        + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
            //        + "'); return false;";
            //}
            

            // BUG_8005
            //K�zponti Irattarba kuldes
            //if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
            //{
            //    if (Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam, out errorDetail))
            //    {
            //        if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
            //            || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
            //        {
            //            ListHeader1.IrattarozasraOnClientClick = "if (confirm('"
            //            + Resources.Question.UIConfirmHeader_KolcsonzesVissza
            //            + "')) { } else { return false; }";
            //        }
            //        else
            //            ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClick("IrattarbaAdasForm.aspx",
            //                QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
            //                "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat
            //                , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //    }
            //    else
            //    {
            //        //ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52202 + "'); return false;";
            //        ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52209
            //            + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            //    }
            //}
            //else
            //{
            //    ListHeader1.IrattarozasraToolTip = Resources.Buttons.Irattarozas_IrattarozasJovahagyasToolTip;
            //    if (Ugyiratok.IrattarbaKuldesJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
            //    {
            //        ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClick("IrattarbaAdasForm.aspx",
            //            QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
            //                "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat +
            //                "&" + QueryStringVars.Mode + "=" + CommandName.UgyiratIrattarozasJovahagyasa
            //            , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //    }
            //    else
            //    {
            //        ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52208 +
            //            ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            //    }
            //}

            // Lez�r�s:
            if (Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClick("LezarasForm.aspx",
                    QueryStringVars.UgyiratId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // Nem z�rhat� le:
                ListHeader1.LezarasOnClientClick = "alert('" + Resources.Error.ErrorCode_52261
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // Lez�r�s visszavon�sa:
            if (Ugyiratok.LezarasVisszavonhato(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.LezarasVisszavonasOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_LezarasVisszavonas + "')) {} else { return false; }";
            }
            else
            {
                ListHeader1.LezarasVisszavonasOnClientClick = "alert('" + Resources.Error.ErrorCode_52264
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            //LZS - BLG_5998
            // Elint�zett� nyilv�n�t�s
            if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
            {
                ListHeader1.ElintezetteNyilvanitas.ToolTip = Resources.Buttons.ElintezetteNyilvanitas;
                ListHeader1.ElintezetteNyilvanitas.AlternateText = Resources.Buttons.ElintezetteNyilvanitas;
                ListHeader1.ElintezetteNyilvanitas.CommandName = CommandName.ElintezetteNyilvanitas;
                if (Ugyiratok.ElintezetteNyilvanithato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
                         , QueryStringVars.Id + "=" + id
                         , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = "alert('" + Resources.Error.ErrorCode_52242
                         + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                         + "'); return false;";
                }
            }
            else
            {
                ListHeader1.ElintezetteNyilvanitas.ToolTip = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
                ListHeader1.ElintezetteNyilvanitas.AlternateText = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
                ListHeader1.ElintezetteNyilvanitas.CommandName = CommandName.ElintezetteNyilvanitasJovahagyasa;
                if (Ugyiratok.ElintezetteNyilvanitasJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
                         , QueryStringVars.Id + "=" + id
                         , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = "alert('" + Resources.Error.ErrorCode_52245 +
                         ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
                }
            }

            EsemenyekSubListHeader.NewVisible = false;
            EsemenyekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                , CommandName.Command + "=" + CommandName.New + "&" + QueryStringVars.ObjektumId + "=" + id
                + "&" + QueryStringVars.Mode + "=" + CommandName.Munkanaplo
                , Defaults.PopupWidth, Defaults.PopupHeight, EsemenyekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
        else
        {
            EsemenyekSubListHeader.NewVisible = false;
        }
    }

    protected void UgyUgyiratokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    UgyUgyiratokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;
        String RecordId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedUgyUgyiratok();
                break;
            case CommandName.Atvetel:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check") == 1
                        && !String.IsNullOrEmpty(RecordId)
                        && lstGridViewSelectedRows.Contains(RecordId))
                    {
                        if (lstGridViewSelectedRows.Count > 0)
                        {
                            ErrorDetails errorDetail;
                            bool atveheto = Ugyiratok.CheckAtvehetoWithFunctionRight(Page, RecordId, out errorDetail);

                            if (!atveheto)
                            {
                                js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52232, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAt", js, true);
                            }
                            else
                            {
                                Ugyiratok.Atvetel(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
                                UgyUgyiratokGridViewBind();
                            }
                        }
                    }
                    else
                    {
                        sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session["SelectedUgyiratIds"] = sb.ToString();

                        Session["SelectedBarcodeIds"] = null;
                        Session["SelectedKuldemenyIds"] = null;
                        Session["SelectedIratPeldanyIds"] = null;
                        Session["SelectedDosszieIds"] = null;
                        //ne okozzon gondot, ha kor�bban h�vtuk a t�k visszav�tel funkci�t
                        Session["TUKVisszavetel"] = null;

                        js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtadas", js, true);
                    }
                }
                break;
            // BUG_5993
            case CommandName.AtvetelUgyintezesre:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
                    /*   if (UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check") == 1
							 && !String.IsNullOrEmpty(RecordId)
							 && lstGridViewSelectedRows.Contains(RecordId))
						{
							 if (lstGridViewSelectedRows.Count > 0)
							 {
								  ErrorDetails errorDetail;
								  bool atveheto = Ugyiratok.CheckAtvehetoUgyintezesreWithFunctionRight(Page, RecordId, out errorDetail);

								  if (!atveheto)
								  {
										js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52161, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
										ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAtUgyintezesre", js, true);
								  }
								  else
								  {
										Ugyiratok.AtvetelUgyintezesre(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
										UgyUgyiratokGridViewBind();
								  }
							 }
						}
						else*/
                    {
                        sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session[Constants.SelectedUgyiratIds] = sb.ToString();

                        js = JavaScripts.SetOnClientClickWithTimeout("AtvetelUgyintezesreTomegesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtvetelUgyintezesre", js, true);
                    }
                }
                break;
            case CommandName.Irattarozasra:
                { 
                // BUG_8005
                // BUG_6460
                bool iskolcsonzes = false;
                ErrorDetails errorDetails = null;
                List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);

                //egyszeres kijel�l�s
                //  if (!String.IsNullOrEmpty(RecordId))
                if (!String.IsNullOrEmpty(RecordId) && (lstGridViewSelectedRows.Count == 1))

                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = RecordId;

                    EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                    Result result_ugyiratGet = service_ugyiratok.Get(execParam);
                    if (result_ugyiratGet.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGet);
                        ErrorUpdatePanel.Update();
                    }

                    EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                    Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
                    // BUG_8005
                    if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                    {
                        if (Ugyiratok.IrattarbaKuldesJovahagyhato(ugyiratStatusz, execParam, out errorDetails))
                        {
                            if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
                                    || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
                            {
                                iskolcsonzes = true;
                            }
                        }
                    }
                    else
                    //if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                    {
                        if (Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam, out errorDetails))
                        {
                            if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
                                    || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
                            {
                                iskolcsonzes = true;
                            }
                        }
                    }
                }
                if (iskolcsonzes)
                {
                    EREC_IrattariKikeroService kikeroService = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                kikeroExecParam.Record_Id = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
                Result kikeroResult = kikeroService.KolcsonzesVissza(kikeroExecParam);
                if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                else
                    UgyUgyiratokGridViewBind();
                break;
                }
                // t�meges
                else
                {
                    //ErrorDetails errorDetails = null;
                    //List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);

                    ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
                    if (lstGridViewSelectedRows.Count == 0)
                    {
                        js = string.Format("alert('{0}')", Resources.List.UI_NoSelectedItem);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HibaNincsKijelolve", js, true);
                        return;
                    }

                    errorDetails = null;
                    EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

                    //ExecParam ep = execParam.Clone();
                    ep.Record_Id = lstGridViewSelectedRows.ToArray()[0];
                    // Statusz statusz = new Statusz();

                    EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    Result res = srv.Get(ep);
                    if (res.IsError || res.Record == null)
                    {
                        //errorDetails = statusz.CreateErrorDetail("�gyirat adatok lek�r�se sikertelen!");
                        //return false;
                    }
                    EREC_UgyUgyiratok ugyiratObj = res.Record as EREC_UgyUgyiratok;
                    //if (!String.IsNullOrEmpty(ugyiratObj.IraIrattariTetel_Id.ToString()))
                    //{
                    search.Id.Value = Search.GetSqlInnerString(lstGridViewSelectedRows.ToArray());
                    search.Id.Operator = Query.Operators.inner;

                    search.Allapot.Value = ugyiratObj.Allapot;
                    search.Allapot.Operator = Query.Operators.notequals;
                    search.TopRow = 1;

                    res = srv.GetAll(ep, search);
                    if (res.IsError || res.Ds == null)
                    {
                    }

                    if (res.Ds.Tables[0].Rows.Count > 0)
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A kijel�lt �gyiratok k�z�tt elt�rnek a st�tuszok, csak azonos st�tusz� ugyiratokat lehet egy�tt kezelni!");
                        ErrorUpdatePanel.Update();
                        return;

                    }
                    bool ok = false;
                    string queryString = Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat;
                    if (ugyiratObj.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                    {
                        ok = Ugyiratok.IrattarbaKuldesJovahagyhatoTomeges(ep, lstGridViewSelectedRows.ToArray(), out errorDetails);
                        queryString += "&Mode=" + CommandName.UgyiratIrattarozasJovahagyasa;
                    }
                    else
                        ok = Ugyiratok.IrattarbaKuldhetoTomeges(ep, lstGridViewSelectedRows.ToArray(), out errorDetails);
                    if (errorDetails != null || !ok)
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", errorDetails.Message);
                        ErrorUpdatePanel.Update();
                        return;
                    }

                    sb = new System.Text.StringBuilder();

                    foreach (string s in lstGridViewSelectedRows)
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session[Constants.SelectedUgyiratIds] = sb.ToString();

                    js = JavaScripts.SetOnClientClickWithTimeout("IrattarbaAdasTomegesForm.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemAdhatoAt", js, true);
                }
        }
        break;
            case CommandName.LezarasVisszavonas:
                EREC_UgyUgyiratokService ugyiratokService = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam ugyiratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                ugyiratokExecParam.Record_Id = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
                Result ugyiratokResult = ugyiratokService.LezarasVisszavonas(ugyiratokExecParam, ugyiratokExecParam.Record_Id);
                if (!string.IsNullOrEmpty(ugyiratokResult.ErrorCode))
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ugyiratokResult);
                else
                    UgyUgyiratokGridViewBind();
                break;
            case CommandName.AtadasraKijeloles:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedUgyiratIds"] = sb.ToString();

                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedIratPeldanyIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratokAtadas", js, true);
                break;
        }
    }

    private void LockSelectedIraUgyUgyiratRecords()
    {
        LockManager.LockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraUgyUgyiratRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }
        
    /// <summary>
    /// Elkuldi emailben a UgyUgyiratokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedUgyUgyiratok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_UgyUgyiratok");
        }
    }

    protected void UgyUgyiratokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        UgyUgyiratokGridViewBind(e.SortExpression, UI.GetSortToGridView("UgyUgyiratokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    protected void UgyUgyiratokGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int oldSelectedIndex = UgyUgyiratokGridView.SelectedIndex;
        int newSelectedIndex = e.NewSelectedIndex;


        if (oldSelectedIndex > -1)
        {
            GridViewRow oldRow = UgyUgyiratokGridView.Rows[oldSelectedIndex];
            SetGridViewRowNormalMode(oldRow);
        }

        UgyUgyiratokGridView.SelectedIndex = newSelectedIndex;

        if (newSelectedIndex > -1)
        {
            GridViewRow row = UgyUgyiratokGridView.Rows[newSelectedIndex];
            SetGridViewRowEditMode(row);

            #region Selected Index changed
            if (oldSelectedIndex != newSelectedIndex || EsemenyekGridView.Rows.Count == 0)
            {
                int selectedRowNumber = newSelectedIndex;
                UI.SetGridViewCheckBoxesToSelectedRow(UgyUgyiratokGridView, selectedRowNumber, "check");

                string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

                ActiveTabRefresh(TabContainer1, id);
            }
            #endregion
        }
        else
        {
            ActiveTabClear();
        }

    }

    protected void UgyUgyiratokGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        bool updated = false;
        int index = e.RowIndex;
        GridViewRow row = UgyUgyiratokGridView.Rows[index];

        if (!e.Cancel)
        {
            TextBox txtNote = (TextBox)row.FindControl("txtNote");
            Label labelNote = (Label)row.FindControl("labelNote");
            Label labelVer = (Label)row.FindControl("labelNote_Ver");
            Label labelId = (Label)row.FindControl("labelNote_Id");
            string ugyiratId = UgyUgyiratokGridView.DataKeys[index].Value.ToString();
            if (txtNote != null && labelVer != null && labelId!= null && labelNote!=null)
            {
                if (labelNote.Text != txtNote.Text)
                {
                    EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                    EREC_HataridosFeladatok record = new EREC_HataridosFeladatok();
                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Obj_Id = ugyiratId;
                    record.Updated.Obj_Id = true;
                    record.Obj_type = Constants.TableNames.EREC_UgyUgyiratok;
                    record.Updated.Obj_type = true;
                    record.Leiras = txtNote.Text;
                    record.Updated.Leiras= true;
                    record.Altipus = KodTarak.FELADAT_ALTIPUS.EloadoiMunkanaploMegjegyzes;
                    record.Updated.Altipus = true;
                    record.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
                    record.Updated.Tipus = true;
                    Result res = new Result();
                    if (String.IsNullOrEmpty(labelId.Text))
                    {
                        //Insert
                        res = svc.Insert(xpm, record);
                    }
                    else
                    {
                        //Update
                        record.Base.Ver = labelVer.Text;
                        record.Base.Updated.Ver = true;
                        xpm.Record_Id = labelId.Text;
                        res = svc.Update(xpm, record);
                    }
                    if (res.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                        ErrorUpdatePanel.Update();
                    }
                    else
                    {
                        updated = true;
                    }
                }
            }
        }

        if (updated)
        {
            UgyUgyiratokGridViewBind();
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
        }
        else
        {
            SetGridViewRowNormalMode(row);
        }
    }

    protected void UgyUgyiratokGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
    }

    /// <summary>
    /// ASPX oldalr�l h�vjuk
    /// </summary>
    /// <param name="oElozmenyAzonositok">El�zm�ny azonos�tok</param>
    /// <param name="oElozmenyIds">El�zm�ny Id-k</param>
    /// <param name="oElozmenyTypes">El�zm�ny t�pusok</param>
    /// <returns>Megjelen�tend� HTML</returns>
    protected string FormatEloiratok(object oElozmenyAzonositok, object oElozmenyIds, object oElozmenyTypes)
    {
        if (oElozmenyAzonositok == null)
            return String.Empty;

        string ElozmenyAzonositok = oElozmenyAzonositok as string;

        if (String.IsNullOrEmpty(ElozmenyAzonositok))
            return String.Empty;

        string ElozmenyIds = oElozmenyIds as string;
        string ElozmenyTypes = oElozmenyTypes as string;

        string[] arrayElozmenyAzonositok = ElozmenyAzonositok.Split(';');
        string[] arrayElozmenyIds = ElozmenyIds.Split(';');
        string[] arrayElozmenyTypes = ElozmenyTypes.Split(';');

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < arrayElozmenyAzonositok.Length; i++)
        {
            if (i > 0)
                sb.Append("<br/>");

            sb.Append("<span onclick=\"");
            if (arrayElozmenyIds.Length > i && arrayElozmenyTypes.Length > i)
            {
                if (arrayElozmenyTypes[i] == Constants.TableNames.EREC_UgyUgyiratok)
                {
                    sb.Append(JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&"
                + QueryStringVars.Id + "=" + arrayElozmenyIds[i], Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max));
                }
                else
                {
                    sb.Append(JavaScripts.SetOnCLientClick_NoPostBack(eMigration.migralasFormUrl, QueryStringVars.Command + "=" + CommandName.View + "&"
                    + QueryStringVars.Id + "=" + arrayElozmenyIds[i], Defaults.PopupWidth_Max, Defaults.PopupHeight_Max));
                }

                sb.Append("\" title=\"Megtekint\">");
                sb.Append("<a href=\"\">");
                sb.Append(arrayElozmenyAzonositok[i]);
                sb.Append("</a>");
            }
            else
            {
                sb.Append("\">");
                sb.Append(arrayElozmenyAzonositok[i]);
            }
            
            
            sb.Append("</span>");
        }

        return sb.ToString();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (EsemenyekGridView.SelectedIndex == -1)
        {
            return;
        }
        string ugyiratId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, ugyiratId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string ugyiratId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                EsemenyekGridViewBind(ugyiratId);
                EsemenyekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(EsemenyekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        EsemenyekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(EsemenyekTabPanel))
        {
            EsemenyekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(EsemenyekGridView));
        }

    }

    #endregion

    #region Esemenyek Detail

    private void EsemenyekSubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
        //}
    }

    protected void EsemenyekGridViewBind(string ugyiratId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("EsemenyekGridView", ViewState, "KRT_Esemenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("EsemenyekGridView", ViewState);

        EsemenyekGridViewBind(ugyiratId, sortExpression, sortDirection);
    }


    private string prevUgyiratId = String.Empty;
    protected void EsemenyekGridViewBind(string ugyiratId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(ugyiratId))
        {
            if (ugyiratId != prevUgyiratId)
            {
                prevUgyiratId = ugyiratId;

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_EsemenyekSearch search = new KRT_EsemenyekSearch();

                if (!cbEnableItems_View.Checked)
                {
                    string FilterViewIds = FunkcioIds_View;
                    if (!String.IsNullOrEmpty(FilterViewIds))
                    {
                        search.Funkcio_Id.Value = FilterViewIds;
                        search.Funkcio_Id.Operator = Contentum.eQuery.Query.Operators.notinner;
                    }

                }

                search.OrderBy = Search.GetOrderBy("EsemenyekGridView", ViewState, SortExpression, SortDirection);


                //// Lapoz�s be�ll�t�sa:
                UI.SetPaging(execParam, EsemenyekSubListHeader);

                // �gyint�z� alap�rtelmez�sben az aktu�lis felhaszn�l�
                string ugyintezoId = execParam.Felhasznalo_Id;
                if (!String.IsNullOrEmpty(FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField))
                {
                    ugyintezoId = FelhasznaloCsoportTextBox_Ugyintezo.Id_HiddenField;
                }

                Result res = service.GetAllEsemenyWithExtensionForMunkanaplo(execParam, search, ugyiratId, ugyintezoId);

                object oldDataKey = EsemenyekGridView.SelectedValue;
                int oldIndex = EsemenyekGridView.SelectedIndex;
                UI.GridViewFill(EsemenyekGridView, res, EsemenyekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
                object newDataKey = EsemenyekGridView.SelectedValue;
                int newIndex = EsemenyekGridView.SelectedIndex;
                RaiseGridViewSelectedIndexChanged(EsemenyekGridView, oldIndex, oldDataKey, newIndex, newDataKey);

                UI.SetTabHeaderRowCountText(res, Header1);

                //ui.SetClientScriptToGridViewSelectDeSelectButton(EsemenyekGridView);
            }
        }
        else
        {
            ui.GridViewClear(EsemenyekGridView);
        }
    }


    void EsemenyekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
    }

    protected void EsemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(EsemenyekTabPanel))
                    //{
                    //    EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
                    //}
                    ActiveTabRefreshDetailList(EsemenyekTabPanel);
                    break;
            }
        }
    }

    protected void EsemenyekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Select")
        //{
        //    int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
        //    UI.SetGridViewCheckBoxesToSelectedRow(EsemenyekGridView, selectedRowNumber, "check");
        //}
    }

    private void EsemenyekGridView_RefreshOnClientClicks(string esemenyId)
    {
        //string id = esemenyId;
        //if (!String.IsNullOrEmpty(id))
        //{
        //    EsemenyekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, EsemenyekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        //}
    }

    protected void EsemenyekGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(EsemenyekSubListHeader.Scrollable, EsemenyCPE);

        EsemenyekSubListHeader.RefreshPagerLabel();
    }

    void EsemenyekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(EsemenyekSubListHeader.RowCount);
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
    }

    void EsemenyekSubListHeader_PagingButtonsClick(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, EsemenyCPE);
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
    }

    protected void EsemenyekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView)
            , e.SortExpression, UI.GetSortToGridView("EsemenyekGridView", ViewState, e.SortExpression));
    }

    protected void EsemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SetGridViewRowBackGroundColor(e.Row, 0);
            UI.SetGridViewDateTime(e, "labelIntezkedesKelte");
        }
    }

    protected void EsemenyekGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int oldSelectedIndex = EsemenyekGridView.SelectedIndex;
        int newSelectedIndex = e.NewSelectedIndex;

        if (oldSelectedIndex> -1)
        {
            GridViewRow oldRow = EsemenyekGridView.Rows[EsemenyekGridView.SelectedIndex];
            SetGridViewRowNormalMode(oldRow);
        }

        EsemenyekGridView.SelectedIndex = newSelectedIndex;

        if (newSelectedIndex > -1)
        {
            GridViewRow row = EsemenyekGridView.Rows[newSelectedIndex];
            SetGridViewRowEditMode(row);
        }

    }

    private void SetGridViewRowNormalMode(GridViewRow row)
    {
        SetGridViewRowMode(row, 0);
    }

    private void SetGridViewRowEditMode(GridViewRow row)
    {
        SetGridViewRowMode(row, 1);
    }

    private void SetGridViewRowMode(GridViewRow row, int mode)
    {
        if (row != null)
        {
            TextBox txtNote = (TextBox)row.FindControl("txtNote");
            Label labelNote = (Label)row.FindControl("labelNote");
            ImageButton ibtnSelect = (ImageButton)row.FindControl("ibtnSelect");
            ImageButton ibtnUpdate = (ImageButton)row.FindControl("ibtnUpdate");

            if (labelNote != null)
            {
                if (mode == 0)
                    labelNote.Visible = true;
                else
                    labelNote.Visible = false;
            }
            if (txtNote != null)
            {
                if (mode == 0)
                {
                    txtNote.Visible = false;
                }
                else
                {
                    if (labelNote != null)
                    {
                        txtNote.Text = labelNote.Text;
                    }
                    txtNote.Visible = true;
                    JavaScripts.SetOnClientEnterPressed(txtNote, ClientScript.GetPostBackEventReference(ibtnUpdate,"click"));
                    ScriptManager1.SetFocus(txtNote);
                }
            }

            if (ibtnSelect != null)
            {
                if (mode == 0)
                    ibtnSelect.Visible = true;
                else
                    ibtnSelect.Visible = false;
            }

            if (ibtnUpdate != null)
            {
                if (mode == 0)
                    ibtnUpdate.Visible = false;
                else
                    ibtnUpdate.Visible = true;
            }

            SetGridViewRowBackGroundColor(row, mode);
        }
    }

    private void SetGridViewRowBackGroundColor(GridViewRow row, int mode)
    {
        Label labelObjType = (Label)row.FindControl("labelObjType");
        if (labelObjType != null)
        {
            string objType = labelObjType.Text;
            if (objType == Constants.TableNames.EREC_HataridosFeladatok)
            {
                Label labelObjTypeFeladat = (Label)row.FindControl("labelObjTypeFeladat");
                if (labelObjTypeFeladat != null)
                {
                    objType = labelObjTypeFeladat.Text;
                }
            }
            if (objType == Constants.TableNames.EREC_UgyUgyiratok)
            {
                if (mode == 0)
                {
                    //System.Drawing.Color colorCsabi = System.Drawing.Color.FromArgb(251, 212, 180);
                    row.BackColor = colorCsabi;
                }
                else
                {
                    row.BackColor = EsemenyekGridView.SelectedRowStyle.BackColor;
                }
            }
        }
        else if ((row.NamingContainer as GridView).Equals(UgyUgyiratokGridView))
        {
            bool isIratUgyintezo = IsIratUgyintezoRow(row);

            switch (mode)
            {
                case 0:
                    if (isIratUgyintezo)
                    {
                        row.CssClass = GridViewSpecialRowStyle;
                    }
                    else
                    {
                        row.BackColor = (row.NamingContainer as GridView).RowStyle.BackColor;
                    }
                    break;
                default:
                    if (isIratUgyintezo)
                    {
                        row.CssClass = GridViewSpecialSelectedRowStyle;
                    }
                    else
                    {
                        row.BackColor = (row.NamingContainer as GridView).SelectedRowStyle.BackColor;
                    }
                    break;
            }
        }
    }

    protected void EsemenyekGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        bool updated = false;
        int index = e.RowIndex;
        GridViewRow row = EsemenyekGridView.Rows[index];

        if (!e.Cancel)
        {
            TextBox txtNote = (TextBox)row.FindControl("txtNote");
            Label labelNote = (Label)row.FindControl("labelNote");
            Label labelVer = (Label)row.FindControl("labelVer");
            string id = EsemenyekGridView.DataKeys[index].Value.ToString();
            if (txtNote != null && labelVer != null)
            {
                if (labelNote.Text != txtNote.Text)
                {
                    KRT_EsemenyekService svc = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                    xpm.Record_Id = id;
                    KRT_Esemenyek record = new KRT_Esemenyek();
                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Base.Ver = labelVer.Text;
                    record.Base.Updated.Ver = true;
                    record.Base.Note = txtNote.Text;
                    record.Base.Updated.Note = true;
                    Result res = svc.Update(xpm, record);
                    if (res.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                        ErrorUpdatePanel.Update();
                    }
                    else
                    {
                        updated = true;
                    }
                }
            }
        }

        if (updated)
        {
            EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
        }
        else
        {
            //EsemenyekGridView.SelectedIndex = -1;
            SetGridViewRowNormalMode(row);
        }
    }

    protected void EsemenyekGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
    }

    protected void cbEnableItems_View_CheckedChanged(object sender, EventArgs e)
    {
        ActiveTabRefreshDetailList(EsemenyekTabPanel);
    }

    /// <summary>
    /// ASPX f�jlb�l h�vjuk
    /// </summary>
    /// <param name="PartnerNev">Partner neve</param>
    /// <param name="PartnerCim">Pertner c�me</param>
    /// <returns>Form�zott string</returns>
    protected string FormatPartnerCim(object oPartnerNev, object oPartnerCim)
    {
        string PartnerNev = oPartnerNev as string;
        string PartnerCim = oPartnerCim as string;

        if (String.IsNullOrEmpty(PartnerNev) && String.IsNullOrEmpty(PartnerCim))
        {
            return String.Empty;
        }

        string delimeter = " / ";

        if (String.IsNullOrEmpty(PartnerCim) || String.IsNullOrEmpty(PartnerNev))
        {
            delimeter = String.Empty;
        }

        string ret = String.Format("{0}{1}{2}",PartnerNev,delimeter,PartnerCim);
        return ret;  
    }

    /// <summary>
    /// ASPX f�jlb�l h�vjuk
    /// </summary>
    /// <param name="oAlszamId">A megtekintend� irat id-ja</param>
    /// <returns>Javascript k�d</returns>
    protected string GetAlszamModifyClientClick(object oAlszamId)
    {
        if (oAlszamId == null)
            return String.Empty;

        string AlszamId = oAlszamId.ToString();

        if (String.IsNullOrEmpty(AlszamId))
            return String.Empty;

        string js = String.Empty;

        js = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify + "&"
            + QueryStringVars.Id + "=" + AlszamId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        return js;
    }

    /// <summary>
    /// ASPX f�jlb�l h�vjuk
    /// </summary>
    /// <param name="oAlszamId">A megtekintend� �gyirat id-ja</param>
    /// <returns>Javascript k�d</returns>
    protected string GetFoszamViewClientClick(object oFoszamId)
    {
        if (oFoszamId == null)
            return String.Empty;

        string FoszamId = oFoszamId.ToString();

        if (String.IsNullOrEmpty(FoszamId))
            return String.Empty;

        string js = String.Empty;

        js = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&"
            + QueryStringVars.Id + "=" + FoszamId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        return js;
    }

    /// <summary>
    /// ASPX oldalr�l h�vjuk
    /// </summary>
    /// <param name="oObj_Type"></param>
    /// <param name="oObj_Id">El�zm�ny Id-k</param>
    /// <param name="oElozmenyTypes">El�zm�ny t�pusok</param>
    /// <param name="oText">Megjelen�tend� sz�veg</param>
    /// <returns>Feljegyz�s megtekint�s link</returns>
    protected string GetFeljegyzesViewLink(object oObj_Type, object oObj_Id, object oText)
    {
        string text = oText as String;

        if(String.IsNullOrEmpty(text))
        {
            return String.Empty;
        }

        if (oObj_Type == null || oObj_Id == null)
            return text;

        string Obj_Type = oObj_Type as String;
        string Obj_Id = oObj_Id.ToString();

        if (String.IsNullOrEmpty(Obj_Type) || String.IsNullOrEmpty(Obj_Id))
        {
            return text;
        }

        if (Obj_Type == Constants.TableNames.EREC_HataridosFeladatok)
        {
            string url = ObjektumTipusok.GetViewUrl(Obj_Id, Obj_Type);
            string js = JavaScripts.SetOnCLientClick_NoPostBack(url, String.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
            string link = String.Format("<span onclick=\"{0}\" title=\"Megtekint�s\">",js);
            link += String.Format("<a class=\"linkStyle\">{0}</a></span>",text);
            return link;
        }

        return text;
    }

    #endregion
}
