using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;

public partial class MuveletekLovList : Contentum.eUtility.UI.PageBase
{

    private bool disable_refreshLovList = false;

    Dictionary<string, string> MuveletKodDictionary
    {
        get
        {
            object o = ViewState["MuveletKodDictionary"];
            if (o != null)
                return (Dictionary<string, string>)ViewState["MuveletKodDictionary"];
            return null;
        }
        set
        {
            ViewState["MuveletKodDictionary"] = value;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MuveletekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        ButtonAdvancedSearch.Enabled = false;
        ButtonAdvancedSearch.CssClass = "disableditem";

        ImageButtonReszletesAdatok.Enabled = false;
        ImageButtonReszletesAdatok.CssClass = "disableditem";

        //ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("MuveletekSearch.aspx", ""
        //    , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = "if (document.forms[0]." + ListBoxSearchResult.ClientID + ".value == '') return false; "
        //    + JavaScripts.SetOnClientClick("MuveletekForm.aspx", "Command=View&Id='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_MuveletekService service = eAdminService.ServiceFactory.GetKRT_MuveletekService();
        KRT_MuveletekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_MuveletekSearch)Search.GetSearchObject(Page, new KRT_MuveletekSearch());
        }
        else
        {
            search = new KRT_MuveletekSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.OrderBy = "Nev";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);

        if (!result.IsError)
        {
            if (MuveletKodDictionary == null)
            {
                MuveletKodDictionary = new Dictionary<string, string>();
            }

            MuveletKodDictionary.Clear();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string kod = row["Kod"].ToString();
                if (!MuveletKodDictionary.ContainsKey(id))
                    MuveletKodDictionary.Add(id, kod);
            }

        }
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;
                string selectedKod = String.Empty;

                if (MuveletKodDictionary != null)
                {
                    if (MuveletKodDictionary.ContainsKey(selectedId))
                        selectedKod = MuveletKodDictionary[selectedId];
                }

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                string HiddenFieldKodId = Request.QueryString.Get("HiddenFieldKodId");
                if (!String.IsNullOrEmpty(HiddenFieldKodId))
                {
                    Dictionary<string,string> componetValues = new Dictionary<string,string>(1);
                    componetValues.Add(HiddenFieldKodId,selectedKod);
                    JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, componetValues, "MuveletKodScript");
                }
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }
}
