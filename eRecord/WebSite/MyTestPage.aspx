﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyTestPage.aspx.cs" Inherits="MyTestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title></title>
     <style type="text/css">
         .TestTable tr td
         {
            border-bottom: solid 2px silver;
            border-top: solid 2px silver;
         }
         .TestTable
         {
            border-collapse:collapse;
            border: solid 2px silver;
            margin:5px;
         }
         .TestTable tr
         {
            padding:5px;
         }
     </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <eUI:eErrorPanel runat="server" ID="ErrorPanel" Visible="false">
        </eUI:eErrorPanel>
         <table class="TestTable" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Label runat="server" ID="labelKozpontiIrattarbaVetel" Text="Központi irattárba vétel:" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="textKozpontiIrattarbaVetelDarab" Text="1" Width="40"/>
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbOnEngedelyezettKikero" Text="Engedélyezett kikérővel"  Checked="false"/>
                    <br />
                    <asp:CheckBox runat="server" ID="cbWithRegiSzerelt" Text="Régi szerelt főszámmal"  Checked="false"/>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnKozpontiIrattarbaVetel" Text="Mehet" OnClick="btnKozpontiIrattarbaAdas_Click"/>
                </td>
                <td>
                    <asp:Label runat="server" ID="labelKozpontiIrattarbaVetelResultTime" />
                </td>
                <td>
                    <asp:Label runat="server" ID="labelKozpontiIrattarbaVetelResult" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="labelAtIktatas" Text="Átiktatás:" />
                </td>
                <td>
                </td>
                <td>
                     <asp:CheckBox runat="server" ID="cbCimzettFopolgarmester" Text="Főpolgármester címzett" Checked="true"/>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnAtIktatas" Text="Mehet" OnClick="btnAtIktatas_Click"/>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="labelAtIktatasResult" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="labelAtvetelUgyintezesre" Text="Átvétel ügyintézésre:" />
                </td>
                <td>
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbElektronikus" Text="Elektronikus ügyirat" Checked="true"/>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnAtvetelUgyintezesre" Text="Mehet" OnClick="btnAtvetelUgyintezesre_Click"/>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="labelAtvetelUgyintezesreResult" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="labelElintezetteNyilvanitas" Text="Elintézetté nyilvánítás:" />
                </td>
                <td>
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbElintezetteNyilvanitasJovahagyas" Text="Jóváhagyás" Checked="false"/>
                </td>
                <td>
                    <asp:Button runat="server" ID="btnElintezetteNyilvanitas" Text="Mehet" OnClick="btnElintezetteNyilvanitas_Click"/>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="labelElintezetteNyilvanitasResult" />
                </td>
            </tr>
         </table>
    </div>
    </form>
</body>
</html>
