﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Threading;
using System.Diagnostics;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.BusinessDocuments;
using System.Data;

public partial class MyTestPage : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnKozpontiIrattarbaAdas_Click(object sender, EventArgs e)
    {
        try
        {
            int num = Int32.Parse(textKozpontiIrattarbaVetelDarab.Text);
            KozpontiIrattarbaVetelTest(num, cbOnEngedelyezettKikero.Checked, cbWithRegiSzerelt.Checked);
            labelKozpontiIrattarbaVetelResult.Text = Search.GetSqlInnerString(ugyiratIds.ToArray());
            labelKozpontiIrattarbaVetelResultTime.Text = String.Format("{0}ms", kozpontiIrattarbaVetelTime.ElapsedMilliseconds);
        }
        catch (Exception ex)
        {
            Result res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
        }
    }

    protected void btnAtIktatas_Click(object sender, EventArgs e)
    {
        try
        {
            labelAtIktatasResult.Text = AtIktatasTest(cbCimzettFopolgarmester.Checked);
        }
        catch (Exception ex)
        {
            Result res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
        }
    }

    protected void btnAtvetelUgyintezesre_Click(object sender, EventArgs e)
    {
        try
        {
            labelAtvetelUgyintezesreResult.Text = AtvetelUgyintezesreTest(cbElektronikus.Checked);
        }
        catch (Exception ex)
        {
            Result res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
        }
    }

    protected void btnElintezetteNyilvanitas_Click(object sender, EventArgs e)
    {
        try
        {
            labelElintezetteNyilvanitasResult.Text = ElintezetteNyilvanitasTest(cbElintezetteNyilvanitasJovahagyas.Checked);
        }
        catch (Exception ex)
        {
            Result res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
        }
    }

    #region Test
    ManualResetEvent[] waits;
    Thread[] threads;
    Result[] results;
    volatile List<string> ugyiratIds = new List<string>();
    Stopwatch kozpontiIrattarbaVetelTime = new Stopwatch();
    private bool IsOnEngedelyezettKikero = false;
    private bool IsWithRegiSzerelt = false;
    List<Foszam> foszamList;
    public void KozpontiIrattarbaVetelTest(int num, bool IsOnEngedelyezettKikero, bool IsWithRegiSzerelt)
    {
        this.IsWithRegiSzerelt = IsWithRegiSzerelt;
        this.IsOnEngedelyezettKikero = IsOnEngedelyezettKikero;

        if (IsWithRegiSzerelt)
        {
            foszamList = GetNemSzereltFoszam(num);
        }

        AtmenetiIrattarbaVetelTest(num);

        waits = new ManualResetEvent[num];
        threads = new Thread[num];
        results = new Result[num];

        for (int i = 0; i < num; i++)
        {
            waits[i] = new ManualResetEvent(false);
            Thread t = threads[i] = new Thread(KozpontiIrattarbaAdasTestAsync);
            results[i] = new Result();
            t.Start(i);
        }
        WaitHandle.WaitAll(waits);

        foreach (Result res in results)
        {
            if (res.IsError)
                throw new Contentum.eUtility.ResultException(res);
        }

        kozpontiIrattarbaVetelTime.Start();
        WS_AtvetelIrattarbaTomegesTest(ugyiratIds);
        kozpontiIrattarbaVetelTime.Stop();
    }

    public void AtmenetiIrattarbaVetelTest(int num)
    {
        waits = new ManualResetEvent[num];
        threads = new Thread[num];
        results = new Result[num];

        for (int i = 0; i < num; i++)
        {
            waits[i] = new ManualResetEvent(false);
            Thread t = threads[i] = new Thread(AtmenetiIrattarbaAdasTestAsync);
            results[i] = new Result();
            t.Start(i);
        }
        WaitHandle.WaitAll(waits);

        foreach (Result res in results)
        {
            if (res.IsError)
                throw new Contentum.eUtility.ResultException(res);
        }

        WS_AtvetelIrattarbaTomegesTest(ugyiratIds);
    }

    private void KozpontiIrattarbaAdasTestAsync(object _threadNumber)
    {
        int threadNum = (int)_threadNumber;
        try
        {
            string ugyiratId = ugyiratIds[threadNum];
            WS_AtadasKozpontiIrattarbaTest(ugyiratId);
            if (IsOnEngedelyezettKikero)
            {
                WS_BelsoIratIktatasaAlszamraTest(ugyiratId, String.Format("Irattárba küldött iktatás_{0}_{1}", DateTime.Now, threadNum + 1));
            }
        }
        catch (Exception ex)
        {
            results[threadNum] = Contentum.eUtility.ResultException.GetResultFromException(ex);
        }
        finally
        {
            waits[threadNum].Set();
        }
    }

    private void AtmenetiIrattarbaAdasTestAsync(object _threadNumber)
    {
        int threadNum = (int)_threadNumber;
        try
        {
            string ugyiratId = AtmenetiIrattarbaAdasTest(String.Format("Test_{0}_{1}", DateTime.Now, threadNum + 1));
            ugyiratIds.Add(ugyiratId);
        }
        catch (Exception ex)
        {
            results[threadNum] = Contentum.eUtility.ResultException.GetResultFromException(ex);
        }
        finally
        {
            waits[threadNum].Set();
        }
    }

    public string AtmenetiIrattarbaAdasTest(string targy)
    {
        ErkeztetesIktatasResult ei = WS_BelsoIratIktatasaTest(targy);
        string ugyiratId = ei.UgyiratId;
        if (IsWithRegiSzerelt)
        {
            WS_RegiAdatSzerelesTeszt(ei.UgyiratId, ei.UgyiratAzonosito);
        }
        WS_AtvetelUgyintezesreTest(ugyiratId);
        WS_LezarasTest(ugyiratId);
        WS_AtadasAtmenetiIrattarbaTest(ugyiratId);
        return ugyiratId;
    }

    public string AtIktatasTest(bool IsCimzettFopolgarmester)
    {
        string kuldemenyCimzettId = IsCimzettFopolgarmester ? "1332b737-a72f-4cc3-810a-505ba3335b14" : this.ExecParam.Felhasznalo_Id;
        string targy = String.Format("{0}_{1}", IsCimzettFopolgarmester ? "Főpolgármesternek" : "Nem főpolgármesternek", DateTime.Now);
        string iratId = WS_EgyszerusitettIktatasaTest(targy, kuldemenyCimzettId).IratId;
        string atiktatottIratId = WS_IratAtIktatasaTest(iratId, String.Format("Atiktatott irat:{0}", DateTime.Now)).IratId;
        return atiktatottIratId;
    }

    private bool IsElektronikusUgyirat = false;
    public string AtvetelUgyintezesreTest(bool IsElektronikusUgyirat)
    {
        this.IsElektronikusUgyirat = IsElektronikusUgyirat;
        string targy = String.Format("{0}_{1}", "Átvétel ügyintézésre teszt", DateTime.Now);
        ErkeztetesIktatasResult ei = WS_BelsoIratIktatasaTest(targy);
        string ugyiratId = ei.UgyiratId;
        WS_AtvetelUgyintezesreTest(ugyiratId);
        return ugyiratId;
    }

    private bool IsElintezetteNyilvanitasJovahagyas = false;
    public string ElintezetteNyilvanitasTest(bool IsElintezetteNyilvanitasJovahagyas)
    {
        this.IsElektronikusUgyirat = true;
        this.IsElintezetteNyilvanitasJovahagyas = IsElintezetteNyilvanitasJovahagyas;
        string targy = String.Format("{0}_{1}", "Elintézetté nyilvánítás teszt", DateTime.Now);
        ErkeztetesIktatasResult ei = WS_BelsoIratIktatasaTest(targy);
        string ugyiratId = ei.UgyiratId;
        WS_AtvetelUgyintezesreTest(ugyiratId);
        WS_ElintezetteNyilvanitasTest(ugyiratId);
        return ugyiratId;
    }

    #endregion

    #region Parameters

    private const string IraIrattariTetel_Id = "7389913d-4b52-43ad-8e0f-bf3479d82771";
    private const string IktatokonyvId = "ebcd4fdf-b98f-4c7c-bc84-8eec68acf6ab";
    private ExecParam ExecParam
    {
        get
        {
            ExecParam xpm = UI.SetExecParamDefault(Page);
            if (IsElintezetteNyilvanitasJovahagyas)
            {
                //Teszt felhasználó
                xpm.Felhasznalo_Id = xpm.LoginUser_Id = "154ea6ca-6fc2-4c40-ab4f-30f44b92d48e";
                xpm.CsoportTag_Id = "2aee5bf4-97ae-df11-aa75-0019db6da442";
                xpm.FelhasznaloSzervezet_Id = "ecb902e5-fd38-4d06-9e0e-a0b2733fb2e9";
            }
            return xpm;
        }
    }

    private EREC_IraIratok GetEREC_IraIratok(string targy)
    {
        EREC_IraIratok irat = new EREC_IraIratok();
        irat.AdathordozoTipusa = IsElektronikusUgyirat ? "1" : "0";
        irat.Jelleg = "1";
        irat.KiadmanyozniKell = "0";
        irat.Targy = targy;
        irat.UgyintezesAlapja = "0";
        return irat;
    }

    private EREC_PldIratPeldanyok GetEREC_PldIratPeldanyok()
    {
        EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
        peldany.Cim_id_Cimzett = "d4cb082d-9369-df11-aa39-0019db6da442";
        peldany.CimSTR_Cimzett = "8000 Székesfehérvár, Pf.: 12";
        peldany.NevSTR_Cimzett = "Lamatsch András";
        peldany.Partner_Id_Cimzett = "e8a55d48-d5c9-4de6-9f44-a90dca158216";
        peldany.UgyintezesModja = "0";
        peldany.VisszaerkezesiHatarido = DateTime.Today.AddMonths(1).ToString();
        return peldany;
    }

    private EREC_UgyUgyiratok GetEREC_UgyUgyiratok(string targy)
    {
        EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
        ugyirat.Csoport_Id_Felelos = this.ExecParam.Felhasznalo_Id;
        ugyirat.FelhasznaloCsoport_Id_Ugyintez = this.ExecParam.Felhasznalo_Id;
        ugyirat.Hatarido = DateTime.Today.AddMonths(1).ToString();
        ugyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
        ugyirat.Targy = targy;
        ugyirat.UgyTipus = "01";
        return ugyirat;
    }

    private EREC_KuldKuldemenyek GetEREC_KuldKuldemenyek(string targy, string cimzettId)
    {
        EREC_KuldKuldemenyek kuldemeny = new EREC_KuldKuldemenyek();
        kuldemeny.AdathordozoTipusa = "0";
        kuldemeny.BeerkezesIdeje = DateTime.Now.ToString();
        kuldemeny.BoritoTipus = "02";
        kuldemeny.Cim_Id = "6505ba3f-5833-46e1-958f-b02a5dfeb358";
        kuldemeny.CimSTR_Bekuldo = "Magyarország 8000 Székesfehérvár Székesfehérvár, Móricz Zs. u. 14.";
        kuldemeny.Csoport_Id_Cimzett = cimzettId;
        kuldemeny.Csoport_Id_Felelos = this.ExecParam.Felhasznalo_Id;
        kuldemeny.Erkeztetes_Ev = DateTime.Today.Year.ToString();
        kuldemeny.FelbontasDatuma = DateTime.Now.ToString();
        kuldemeny.FelhasznaloCsoport_Id_Bonto = this.ExecParam.Felhasznalo_Id;
        kuldemeny.IktatniKell = "1";
        kuldemeny.IraIktatokonyv_Id = "7b7c7e4f-2901-4037-90cd-2d264c54417c";
        kuldemeny.KuldesMod = "09";
        kuldemeny.MegorzesJelzo = "1";
        kuldemeny.NevSTR_Bekuldo = "Axis Consulting 2000 Kft.";
        kuldemeny.Partner_Id_Bekuldo = "d3cbaa7c-4541-47c0-9842-a6bb76fea1ad";
        kuldemeny.PeldanySzam = "1";
        kuldemeny.Surgosseg = "30";
        kuldemeny.Targy = targy;
        return kuldemeny;
    }

    #endregion

    #region WebService

    private EREC_UgyUgyiratokService EREC_UgyUgyiratokService
    {
        get
        {
            return eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        }
    }
    private EREC_IraIratokService EREC_IraIratokService
    {
        get
        {
            return eRecordService.ServiceFactory.GetEREC_IraIratokService();
        }
    }

    private ErkeztetesIktatasResult WS_BelsoIratIktatasaTest(string targy)
    {
        EREC_UgyUgyiratok ugyirat = GetEREC_UgyUgyiratok(targy);
        EREC_IraIratok irat = GetEREC_IraIratok(targy);
        EREC_PldIratPeldanyok peldany = GetEREC_PldIratPeldanyok();
        IktatasiParameterek ip = new IktatasiParameterek();
        Result res = this.EREC_IraIratokService.BelsoIratIktatasa(this.ExecParam, IktatokonyvId,
            ugyirat, null, irat, null, peldany, ip);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);

        ErkeztetesIktatasResult ei = res.Record as ErkeztetesIktatasResult;
        return ei;
    }

    private ErkeztetesIktatasResult WS_BelsoIratIktatasaAlszamraTest(string UgyiratId, string targy)
    {
        EREC_IraIratok irat = GetEREC_IraIratok(targy);
        EREC_PldIratPeldanyok peldany = GetEREC_PldIratPeldanyok(); ;
        IktatasiParameterek ip = new IktatasiParameterek();
        Result res = this.EREC_IraIratokService.BelsoIratIktatasa_Alszamra(this.ExecParam, IktatokonyvId,
            UgyiratId, null, irat, null, peldany, ip);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);

        ErkeztetesIktatasResult ei = res.Record as ErkeztetesIktatasResult;
        return ei;
    }

    private void WS_AtvetelUgyintezesreTest(string ugyiratId)
    {
        Result res = this.EREC_UgyUgyiratokService.AtvetelUgyintezesre(this.ExecParam, ugyiratId);
        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private void WS_LezarasTest(string ugyiratId)
    {
        Result res = this.EREC_UgyUgyiratokService.Lezaras(this.ExecParam, ugyiratId, DateTime.Now.ToString(),null);
        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private void WS_AtadasAtmenetiIrattarbaTest(string ugyiratId)
    {
        string IrattarJellege = Constants.IrattarJellege.AtmenetiIrattar;
        string csoport_Id_Felelos_Kovetkezo = "ecb902e5-fd38-4d06-9e0e-a0b2733fb2e9";//Központi Ügyiratkezelési Alosztály

        Result res = this.EREC_UgyUgyiratokService.AtadasIrattarba(this.ExecParam, ugyiratId, IraIrattariTetel_Id, String.Empty,
            IrattarJellege, csoport_Id_Felelos_Kovetkezo);
        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private void WS_AtvetelIrattarbaTomegesTest(List<string> ugyiratIds)
    {
        Result res = this.EREC_UgyUgyiratokService.AtvetelIrattarba_Tomeges(this.ExecParam, String.Join(",", ugyiratIds.ToArray()));
        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private void WS_AtadasKozpontiIrattarbaTest(string ugyiratId)
    {
        string IrattarJellege = Constants.IrattarJellege.KozpontiIrattar;
        string csoport_Id_Felelos_Kovetkezo = "f2ea78c8-515a-41a6-8bae-08b937870262";//központi irattár

        Result res = this.EREC_UgyUgyiratokService.AtadasKozpontiIrattarba(this.ExecParam, ugyiratId, IraIrattariTetel_Id, String.Empty,
            IrattarJellege, csoport_Id_Felelos_Kovetkezo);
        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private ErkeztetesIktatasResult WS_EgyszerusitettIktatasaTest(string targy, string kuldemenyCimzettId)
    {
        EREC_KuldKuldemenyek kuldemeny = GetEREC_KuldKuldemenyek(targy, kuldemenyCimzettId);
        EREC_UgyUgyiratok ugyirat = GetEREC_UgyUgyiratok(targy);
        EREC_IraIratok irat = GetEREC_IraIratok(targy);
        IktatasiParameterek ip = new IktatasiParameterek();
        ErkeztetesParameterek ep = new ErkeztetesParameterek();
        Result res = this.EREC_IraIratokService.EgyszerusitettIktatasa(this.ExecParam, IktatokonyvId,
            ugyirat, null, irat, null, kuldemeny, ip, ep);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);

        ErkeztetesIktatasResult ei = res.Record as ErkeztetesIktatasResult;
        return ei;
    }

    private ErkeztetesIktatasResult WS_IratAtIktatasaTest(string Irat_Id_AtIktatando, string targy)
    {
        EREC_UgyUgyiratok ugyirat = GetEREC_UgyUgyiratok(targy);
        EREC_IraIratok irat = GetEREC_IraIratok(targy);
        EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
        IktatasiParameterek ip = new IktatasiParameterek();
        ip.EmptyUgyiratSztorno = true;
        Result res = this.EREC_IraIratokService.IratAtIktatasa(this.ExecParam, Irat_Id_AtIktatando, IktatokonyvId,
            ugyirat, null, irat, peldany, null, ip);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);

        ErkeztetesIktatasResult ei = res.Record as ErkeztetesIktatasResult;
        return ei;
    }

    private void WS_RegiAdatSzerelesTeszt(string cel_Ugyirat_Id, string cel_Ugyirat_Azon)
    {
        string foszamId = foszamList[0].Id;
        string foszamAzon = foszamList[0].Azonosito;
        foszamList.RemoveAt(0);

        Result res = this.EREC_UgyUgyiratokService.RegiAdatSzereles(this.ExecParam, cel_Ugyirat_Id, cel_Ugyirat_Azon,
            foszamId, foszamAzon, null);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }

    private void WS_ElintezetteNyilvanitasTest(string ugyiratId)
    {
        string elintezesDat = DateTime.Now.AddDays(-1).ToString();
        string elintezesMod = String.Empty;
        ExecParam xpm = this.ExecParam;
        xpm.Record_Id = ugyiratId;
        Result res = this.EREC_UgyUgyiratokService.ElintezetteNyilvanitas(xpm,elintezesDat, elintezesMod ,null);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);
    }


    #endregion

    #region Foszam

    private List<Foszam> GetNemSzereltFoszam(int num)
    {
        MIG_FoszamService svc = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        MIG_FoszamSearch sch = new MIG_FoszamSearch();
        sch.Csatolva_Id.Operator = Query.Operators.isnull;
        sch.Edok_Utoirat_Azon.Operator = Query.Operators.isnull;
        sch.Edok_Utoirat_Id.Operator = Query.Operators.isnull;
        sch.TopRow = num;
        Result res = svc.GetAll(this.ExecParam, sch);

        if (res.IsError)
            throw new Contentum.eUtility.ResultException(res);

        List<Foszam> foszamList = new List<Foszam>();
        foreach (DataRow foszamRow in res.Ds.Tables[0].Rows)
        {
            string foszamId = foszamRow["Id"].ToString();
            string sav = foszamRow["UI_SAV"].ToString();
            string ev = foszamRow["UI_YEAR"].ToString();
            string foszam = foszamRow["UI_NUM"].ToString();
            string foszamAzon = GetRegiAdatAzonosito(sav, ev, foszam);
            foszamList.Add(new Foszam { Id = foszamId, Azonosito = foszamAzon });
        }

        return foszamList;

    }

    private class Foszam
    {
        public string Id { get; set; }
        public string Azonosito { get; set; }
    }

    private static string GetRegiAdatAzonosito(string sav, string ev, string foszam)
    {
        string azonosito;
        azonosito = String.Format("({0}) {1}{2}{3}{4}", new string[5] { sav, "/", foszam, "/", ev });
        return azonosito;
    }

    #endregion
}
