﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="NetlockAlairasTeszt.aspx.cs" Inherits="NetlockAlairasTeszt" %>

<%@ Register Src="~/eRecordComponent/JavaSigner.ascx" TagName="JavaSigner" TagPrefix="uc" %>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="Stylesheets" Runat="Server">
    <link rel="stylesheet" type="text/css" href="netlock/css/smoothness/jquery-ui-1.10.1.custom.min.css" />
    <link rel="stylesheet" type="text/css" href="netlock/css/yui/logger.css" />
    <link rel="stylesheet" type="text/css" href="netlock/css/NlJavaSigner.css" />
</asp:Content>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true">
    <Scripts>
        <%--<asp:ScriptReference Path="~/netlock/js/deployJava.js" />
        <asp:ScriptReference Path="~/netlock/js/swfobject.js" />

        <asp:ScriptReference Path="~/netlock/js/jquery-1.9.1.min.js" />
        <asp:ScriptReference Path="~/netlock/js/jquery-ui-1.10.1.custom.min.js" />

        <asp:ScriptReference Path="~/netlock/js/yui/yui_combined-2.9-min.js" />

        <asp:ScriptReference Path="~/netlock/js/date.format.js" />--%>
        <%--<asp:ScriptReference Path="~/netlock/js/NlJavaSigner.js" />--%>
    </Scripts>
    </asp:ScriptManager>

<%--    <script type="text/javascript">
        baseDownloadURL = "./netlock/applet";

        var signatureType = "NlPDFClientServer";
        var localeLanguage = 'hu';
        var localeCountry = 'HU';	
	</script>--%>

    <div id="main_cont">
	    <h2>PDF Aláírás</h2>
	    Kérjük, adja meg az aláírandó PDF dokumentum url-két, majd kattintson az Aláírás gombra!
	    <br/><br/>
	
	    <table>	
		    <tr>
			    <td>Aláírandó PDF dokumentum letöltési URL:</td>
			    <td><input type="text" id="contentToSign" style="width: 400px;" value="http://www.netlock.hu/docs/dokumentumok/ASZF_0701114.pdf" /></td>
		    </tr>	
		    <tr>
			    <td colspan="2">&nbsp;</td>
		    </tr>
		    <tr>
			    <td colspan="2"><%--<input id="signbutton" type="button" value="Aláírás" />--%>
                    <asp:Button ID="btnSign" runat="server" Text="Aláírás" OnClientClick="return false;" />
                    
                </td>
		    </tr>
	    </table>	
	        
    </div>

    <uc:JavaSigner ID="JavaSigner1" runat="server" SignButtonID="btnSign" />

    
</asp:Content>

