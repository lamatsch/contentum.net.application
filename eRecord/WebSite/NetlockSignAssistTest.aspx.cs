﻿using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using System.Data;

public partial class NetlockSignAssistTest : System.Web.UI.Page
{

    protected void Page_Init(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static string IratSigned(eRecordComponent_NetlockSignAssistSigner.IratFiles signedIrat)
    {
        return eRecordComponent_NetlockSignAssistSigner.IratSigned(signedIrat);
    }
}