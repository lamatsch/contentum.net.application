<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ObjMetaAdataiForm.aspx.cs" Inherits="ObjMetaAdataiForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox"
    TagPrefix="tsztb" %>
<%@ Register Src="eRecordComponent/ObjMetaDefinicioTextBox.ascx" TagName="ObjMetaDefinicioTextBox"
    TagPrefix="omdtb" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%--BLG_608--%>
<%@ Register Src="~/Component/KodtarakListBox.ascx" TagName="KodtarakListBox"
    TagPrefix="ut" %>	
<%@ Register Src="~/Component/KodtarakCheckBoxList.ascx" TagName="KodtarakCheckBoxList"
    TagPrefix="ut" %>	

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="Obj_MetaAdataiPanel" runat="server">
                    <asp:HiddenField ID="IratMetadefinicio_Id_HiddenField" runat="server" />
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelObjMetaDefinicio" runat="server" Text="Objektum metadefin�ci�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:HiddenField ID="DefinicioTipus_HiddenField" runat="server" />
                                    <omdtb:ObjMetaDefinicioTextBox ID="ObjMetaDefinicioTextBox1" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:UpdatePanel ID="TargySzavakUpdatePanel" runat="server" UpdateMode="Conditional"
                        ChildrenAsTriggers="false">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="TargySzavakTextBox1" EventName="TextChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelTargySzavakStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelTargySzavak" runat="server" Text="T�rgysz�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <tsztb:TargySzavakTextBox ID="TargySzavakTextBox1" CustomValueEnabled="false" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelTargySzavakControlTypeSource" runat="server" Text="T�rgysz� vez�rl�elem forr�sa:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="KodTarakDropDownList_TargySzavakControlTypeSource"
                                                CssClass="mrUrlapInputComboBox" ReadOnly="true" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelTargySzavakControlTypeDataSource" runat="server" Text="T�rgysz� vez�rl�elem adatforr�sa:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
                                                ID="DynamicValueControl_TargySzavakControlTypeDataSource" CssClass="mrUrlapInput" ReadOnly="true"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelTargySzavakAlapertelmezettErtek" runat="server" Text="T�rgysz� alap�rtelmezett �rt�k:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <%-- <asp:TextBox ID="TargySzavakAlapertelmezettErtek"  CssClass="mrUrlapInput" ReadOnly="true" runat="server" /> --%>
                                            <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
                                                ID="DynamicValueControl_TargySzavakAlapertelmezettErtek" CssClass="mrUrlapInput" ReadOnly="true"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelTargySzavakRegExp" runat="server" Text="T�rgysz� regul�ris kifejez�s:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="TargySzavakRegExp" CssClass="mrUrlapInput" TextMode="MultiLine"
                                                Rows="3" ReadOnly="true" runat="server" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="ObjMetaAdataiUpdatePanel" runat="server" UpdateMode="Conditional"
                        OnLoad="ObjMetaAdataiUpdatePanel_Load">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="TargySzavakTextBox1" EventName="TextChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelControlTypeSource" runat="server" Text="Vez�rl�elem forr�sa (fel�l�r�s):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="KodTarakDropDownList_ControlTypeSource" CssClass="mrUrlapInputComboBox"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelControlTypeDataSource" runat="server" Text="Vez�rl�elem adatforr�sa (fel�l�r�s):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <%--<asp:TextBox ID="TextBoxControlTypeDataSource" CssClass="mrUrlapInput" runat="server" />--%>
                                            <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
                                                ID="DynamicValueControl_ControlTypeDataSource" CssClass="mrUrlapInput" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAlapertelmezettErtek" runat="server" Text="Alap�rtelmezett �rt�k (fel�l�r�s):" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <%--<asp:TextBox ID="TextBoxAlapertelmezettErtek" CssClass="mrUrlapInput" runat="server" />--%>
                                            <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
                                                ID="DynamicValueControl_AlapertelmezettErtek" CssClass="mrUrlapInput" runat="server" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelSorszam" runat="server" Text="Sorsz�m:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%--  
			                            <asp:TextBox ID="textSorszam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
			                   --%>
                                    <asp:TextBox ID="TextBoxSorszam" runat="server" Text="" Width="35"></asp:TextBox>
                                    <ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server" Minimum="0" Maximum="1000"
                                        TargetControlID="TextBoxSorszam" Width="60" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                </td>
                                <td class="mrUrlapMezo">
                                     <%--BLG_608--%>
                                    <%--<asp:CheckBox ID="cbOpcionalis" runat="server" Text="Opcion�lis" TextAlign="Right">
                                    </asp:CheckBox>--%>
                                    <asp:CheckBox ID="cbOpcionalis" runat="server" Text="Opcion�lis" TextAlign="Right" Checked="true">
                                    </asp:CheckBox>

                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="cbIsmetlodo" runat="server" Text="Ism�tl�d�" TextAlign="Right">
                                    </asp:CheckBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFunkcio" runat="server" Text="Funkci�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="textFunkcio" runat="server" CssClass="mrUrlapInput" MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelObj_MetaAdataiErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelObj_MetaAdataiNote" runat="server" Text="Megjegyz�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="textObj_MetaAdataiNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="SzignalasiJegyzekVerSion" runat="server" />
</asp:Content>
