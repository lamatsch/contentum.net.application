using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUIControls;


public partial class ObjMetaAdataiForm : System.Web.UI.Page
{
    private string Command = "";

    private const string kcs_CONTROLTYPE_SOURCE = "CONTROLTYPE_SOURCE";

    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    public string[][] KodCsoportok
    {
        get
        {
            if (ViewState["KodCsoportokList"] == null)
            {
                #region felt�lt�s

                Contentum.eAdmin.Service.KRT_KodCsoportokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch search = new Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch();
                search.OrderBy = " KRT_KodCsoportok.Nev ASC ";

                Result result = service.GetAll(execParam, search);

                if (!result.IsError)
                {
                    List<string[]> listItems = new List<string[]>();
                    listItems.Add(new string[2] { "", Resources.Form.EmptyListItem });
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                        {
                            System.Data.DataRow row = result.Ds.Tables[0].Rows[i];
                            string Kod = row["Kod"].ToString();
                            string Nev = row["Nev"].ToString();
                            if (!String.IsNullOrEmpty(Kod))
                            {
                                listItems.Add(new string[2] { String.IsNullOrEmpty(Nev) ? Kod : Nev, Kod });
                            }
                        }
                    }

                    ViewState["KodCsoportokList"] = listItems.ToArray();
                }
                #endregion felt�lt�s
            }
            return ViewState["KodCsoportokList"] as string[][];
        }
    }

    private String RegisterCheckErtekJavaScript(TextBox ertekTextBox, TextBox regExpTextBox)
    {
        string js = "";
        if (ertekTextBox != null && ertekTextBox.Visible == true && regExpTextBox != null && regExpTextBox.Visible == true)
        {
            js = @"var text = $get('" + ertekTextBox.ClientID + @"');
                   var regexp = $get('" + regExpTextBox.ClientID + @"');
                   if (text && text.value != '' && regexp && regexp.value != '') {
                        var re= new RegExp(regexp.value);
                        if (!re.test(text.value)) {
                            alert('" + String.Format(Resources.Form.RegularExpressionValidationMessage, "(�rt�k: ' + text.value + '; RegExp:' + regexp.value +')'") + @");
                            return false;
                        }
                    }";
        }
        return js;
    }

  
    private void SetNewControls()
    {

    }

    private void SetViewControls()
    {
        ObjMetaDefinicioTextBox1.ReadOnly = true;
        TargySzavakTextBox1.ReadOnly = true;
        TextBoxSorszam.ReadOnly = true;
        DynamicValueControl_AlapertelmezettErtek.ReadOnly = true;
        KodTarakDropDownList_TargySzavakControlTypeSource.ReadOnly = true;
        DynamicValueControl_TargySzavakControlTypeDataSource.ReadOnly = true;
        KodTarakDropDownList_ControlTypeSource.ReadOnly = true;
        DynamicValueControl_ControlTypeDataSource.ReadOnly = true;

        nupeSorszam.Enabled = false;
        cbOpcionalis.Enabled = false;
        cbIsmetlodo.Enabled = false;

        textFunkcio.ReadOnly = true;

        ErvenyessegCalendarControl1.ReadOnly = true;
        textObj_MetaAdataiNote.ReadOnly = true;
        
        labelFunkcio.CssClass = "mrUrlapInputWaterMarked";
        labelObj_MetaAdataiErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelObj_MetaAdataiNote.CssClass = "mrUrlapInputWaterMarked";
        labelObjMetaDefinicio.CssClass = "mrUrlapInputWaterMarked";
        labelSorszam.CssClass = "mrUrlapInputWaterMarked";
        labelTargySzavak.CssClass = "mrUrlapInputWaterMarked";
        labelAlapertelmezettErtek.CssClass = "mrUrlapInputWaterMarked";
        labelTargySzavakAlapertelmezettErtek.CssClass = "mrUrlapInputWaterMarked";
        labelTargySzavakRegExp.CssClass = "mrUrlapInputWaterMarked";
        labelTargySzavakControlTypeSource.CssClass = "mrUrlapInputWaterMarked";
        labelTargySzavakControlTypeDataSource.CssClass = "mrUrlapInputWaterMarked";
        labelControlTypeSource.CssClass = "mrUrlapInputWaterMarked";
        labelControlTypeDataSource.CssClass = "mrUrlapInputWaterMarked";
    }

    private void SetModifyControls()
    {
        TargySzavakTextBox1.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        Logger.Info("Page_Init (Command = " + Command + ")");

        TargySzavakTextBox1.RefreshCallingWindow = true; // a Tipus �rt�k v�ltoz�s�nak �rz�kel�s�hez sz�ks�ges
        TargySzavakTextBox1.CustomValueEnabled = false;
        TargySzavakTextBox1.TextBox.AutoPostBack = true;
        TargySzavakTextBox1.TextChanged += new EventHandler(TargySzavakTextBox_TextChanged);


        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjMetaAdatai" + Command);
                break;
        }

        if (Command == CommandName.New)
        {
            string ObjMetaDefinicio_Id = Request.QueryString.Get(QueryStringVars.ObjMetaDefinicioId);
            string Targyszavak_Id = Request.QueryString.Get(QueryStringVars.TargyszavakId);

            if (!String.IsNullOrEmpty(ObjMetaDefinicio_Id))
            {
                ObjMetaDefinicioTextBox1.Id_HiddenField = ObjMetaDefinicio_Id;
                ObjMetaDefinicioTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

                ObjMetaDefinicioTextBox1.ReadOnly = true;

            }

            if (!String.IsNullOrEmpty(Targyszavak_Id))
            {
                TargySzavakTextBox1.Id_HiddenField = Targyszavak_Id;
                TargySzavakTextBox1.SetTargySzavakTextBoxById(FormHeader1.ErrorPanel);
                TargySzavakTextBox1.ReadOnly = true;

            }
            else
            {
                TargySzavakTextBox1.Validate = true;
            }
            this.SetNewControls();

            if (!IsPostBack)
            {
                KodTarakDropDownList_TargySzavakControlTypeSource.FillAndSetEmptyValue(kcs_CONTROLTYPE_SOURCE, FormHeader1.ErrorPanel);
                KodTarakDropDownList_ControlTypeSource.FillAndSetEmptyValue(kcs_CONTROLTYPE_SOURCE, FormHeader1.ErrorPanel);
            }

        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                Logger.Error("Nincs megadva id!");
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                Logger.Info("Record_Id = " + id);
                EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_Obj_MetaAdatai erec_Obj_MetaAdatai = (EREC_Obj_MetaAdatai)result.Record;

                    LoadComponentsFromBusinessObject(erec_Obj_MetaAdatai);

                }
                else
                {
                    Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

        }


        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();

            string Targyszavak_Id = Request.QueryString.Get(QueryStringVars.TargyszavakId);
            if (!String.IsNullOrEmpty(Targyszavak_Id))
            {
                TargySzavakTextBox1.Id_HiddenField = Targyszavak_Id;
                TargySzavakTextBox1.SetTargySzavakTextBoxById(FormHeader1.ErrorPanel);
                TargySzavakTextBox1.ReadOnly = true;

            }
            else
            {
                TargySzavakTextBox1.Validate = true;
            }
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }


        if (Command == CommandName.New || Command == CommandName.Modify)
        {
            FormFooter1.ImageButton_Save.OnClientClick += RegisterCheckErtekJavaScript(DynamicValueControl_AlapertelmezettErtek.Control as TextBox, TargySzavakRegExp);

            string PostBack_ControlTypeSource = Page.ClientScript.GetPostBackEventReference(ObjMetaAdataiUpdatePanel, "ChangeType") + ";";
            KodTarakDropDownList_ControlTypeSource.DropDownList.Attributes["onchange"] += PostBack_ControlTypeSource;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (page " + Resources.Form.ObjMetaAdataiFormHeaderTitle + ")");
        FormHeader1.HeaderTitle = Resources.Form.ObjMetaAdataiFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (IsPostBack)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }
    }

    protected void FillKodCsoportokDropDownList(DropDownList ddl)
    {
        if (ddl != null)
        {
            ddl.Items.Clear();

            string[][] listItems = this.KodCsoportok;

            if (listItems != null)
            {
                // felt�lt�s
                ddl.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));
                foreach (string[] items in listItems)
                {
                    if (items.Length > 1)
                    {
                        ddl.Items.Add(new ListItem(items[0], items[1]));
                    }
                }
            }
            else
            {
                ddl.Items.Add(new ListItem("", "Hiba a felt�lt�s sor�n!"));
            }

        }
    }

    // BLG_608
    protected void FillKodCsoportokListBox(ListBox listBox)
    {
        if (listBox != null)
        {
            listBox.Items.Clear();

            string[][] listItems = this.KodCsoportok;

            if (listItems != null)
            {
                // felt�lt�s
                // listBox.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));
                foreach (string[] items in listItems)
                {
                    if (items.Length > 1)
                    {
                        listBox.Items.Add(new ListItem(items[0], items[1]));
                    }
                }
            }
            else
            {
                listBox.Items.Add(new ListItem("", "Hiba a felt�lt�s sor�n!"));
            }

        }
    }
    protected void SetDynamicValueControl(Contentum.eUIControls.DynamicValueControl dvc, string controlTypeSource, string controlTypeDataSource)
    {
        if (dvc != null)
        {
            dvc.ControlTypeSource = controlTypeSource;

            switch (controlTypeSource)
            {
                // BLG_608
                case KodTarak.CONTROLTYPE_SOURCE.KodTarakListBox:
                    Component_KodtarakListBox ktlb = dvc.Control as Component_KodtarakListBox;
                    if (ktlb != null)
                    {
                        dvc.Control.GetType().GetProperty("Ismetlodo").SetValue(dvc.Control, dvc.Ismetlodo, null);
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            ktlb.FillListBox(controlTypeDataSource, FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            ktlb.FillAndSetSelectedValues(controlTypeDataSource, dvc.Value, FormHeader1.ErrorPanel);
                        }
                    }
                    //}
                    break;

                case KodTarak.CONTROLTYPE_SOURCE.KodTarakCheckBoxList:
                    var ktcbl = dvc.Control as Component_KodtarakCheckBoxList;
                    if (ktcbl != null)
                    {
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            ktcbl.FillListBox(controlTypeDataSource, FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            ktcbl.FillAndSetSelectedValues(controlTypeDataSource, dvc.Value, FormHeader1.ErrorPanel);
                        }
                    }
                    break;

                case KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList:
                    //if (!String.IsNullOrEmpty(controlTypeDataSource) && dvc.Control != null)
                    //{
                        Component_KodtarakDropDownList ktddl = dvc.Control as Component_KodtarakDropDownList;
                        if (ktddl != null)
                        {
                            if (String.IsNullOrEmpty(dvc.Value))
                            {
                                ktddl.FillAndSetEmptyValue(controlTypeDataSource, FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                ktddl.FillAndSetSelectedValue(controlTypeDataSource, dvc.Value, true, FormHeader1.ErrorPanel);
                            }
                        }
                    //}
                    break;
            }

            if (dvc.Control is UI.ILovListTextBox)
            {
                UI.ILovListTextBox lovtb = dvc.Control as UI.ILovListTextBox;
                if (lovtb != null)
                {
                    lovtb.SetTextBoxById(FormHeader1.ErrorPanel, null);
                }
            }
        }
    }

    protected void ControlTypeSourceChanged(Component_KodtarakDropDownList ktddl_ControlTypeSource
        , DynamicValueControl dvc_ControlTypeDataSource, DynamicValueControl dvc_AlapertelmezettErtek
        , bool bReadOnlyDefault)
    {
        dvc_AlapertelmezettErtek.Value = "";
        dvc_ControlTypeDataSource.Value = "";
        switch (ktddl_ControlTypeSource.SelectedValue)
        {
            // BLG_608
            case KodTarak.CONTROLTYPE_SOURCE.KodTarakListBox:
            case KodTarak.CONTROLTYPE_SOURCE.KodTarakCheckBoxList:
            case KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList:
                SetDynamicValueControl(dvc_ControlTypeDataSource, KodTarak.CONTROLTYPE_SOURCE.eDropDownList, null);
                if (dvc_ControlTypeDataSource.Control != null)
                {
                    DropDownList ddl = dvc_ControlTypeDataSource.Control as DropDownList;
                    if (ddl != null)
                    {
                        FillKodCsoportokDropDownList(ddl);

                        ddl.Attributes["onchange"] += Page.ClientScript.GetPostBackEventReference(ObjMetaAdataiUpdatePanel, "ChangeSource") + ";";
                    }
                }
                dvc_ControlTypeDataSource.ReadOnly = bReadOnlyDefault;
                break;
            default:
                SetDynamicValueControl(dvc_ControlTypeDataSource, KodTarak.CONTROLTYPE_SOURCE.CustomTextBox, "");
                dvc_ControlTypeDataSource.ReadOnly = true;
                break;
        }

        SetDynamicValueControl(dvc_AlapertelmezettErtek, ktddl_ControlTypeSource.SelectedValue, dvc_ControlTypeDataSource.Value);

    }

    private void SetControlTypeControlsByTargySzavakTipus(string tipus)
    {
        if (tipus == "1")
        {
            DynamicValueControl_AlapertelmezettErtek.ReadOnly = false;
            KodTarakDropDownList_ControlTypeSource.ReadOnly = false;
            DynamicValueControl_ControlTypeDataSource.ReadOnly = false;
        }
        else
        {
            //TextBoxAlapertelmezettErtek.Text = "";
            DynamicValueControl_AlapertelmezettErtek.Value = "";
            DynamicValueControl_AlapertelmezettErtek.ReadOnly = true;
            KodTarakDropDownList_ControlTypeSource.ReadOnly = true;
            KodTarakDropDownList_ControlTypeSource.SetSelectedValue("");
            KodTarakDropDownList_TargySzavakControlTypeSource.SetSelectedValue("");
            DynamicValueControl_ControlTypeDataSource.ReadOnly = true;
            DynamicValueControl_ControlTypeDataSource.Value = "";
        }
    }

    protected void ObjMetaAdataiUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case "ChangeType":
                    ControlTypeSourceChanged(KodTarakDropDownList_ControlTypeSource
                        , DynamicValueControl_ControlTypeDataSource, DynamicValueControl_AlapertelmezettErtek
                        , false);
                    break;
                case "ChangeSource":
                    DynamicValueControl_AlapertelmezettErtek.Value = "";
                    if (KodTarakDropDownList_ControlTypeSource.SelectedValue == KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList
                        && DynamicValueControl_ControlTypeDataSource.ControlTypeSource == KodTarak.CONTROLTYPE_SOURCE.eDropDownList)
                    {
                        SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, KodTarakDropDownList_ControlTypeSource.SelectedValue, DynamicValueControl_ControlTypeDataSource.Value);
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="erec_Obj_MetaAdatai"></param>
    private void LoadComponentsFromBusinessObject(EREC_Obj_MetaAdatai erec_Obj_MetaAdatai)
    {
        Logger.Info("Komponensek feltoltese Obj_MetaAdatai BusinessObject -b�l");

        ObjMetaDefinicioTextBox1.Id_HiddenField = erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id;
        ObjMetaDefinicioTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

        DefinicioTipus_HiddenField.Value = ObjMetaDefinicioTextBox1.DefinicioTipus;

        TargySzavakTextBox1.Id_HiddenField = erec_Obj_MetaAdatai.Targyszavak_Id;
        TargySzavakTextBox1.SetTargySzavakTextBoxById(FormHeader1.ErrorPanel);
        TargySzavakTextBox_TextChanged(TargySzavakTextBox1, new EventArgs());
        // kieg�sz�t� adatok a t�rgyszavak alapj�n
        if (!IsPostBack)
        {
            //// t�rgyszavak
            //KodTarakDropDownList_TargySzavakControlTypeSource.FillAndSetSelectedValue(kcs_CONTROLTYPE_SOURCE, TargySzavakTextBox1.ControlTypeSource, true, FormHeader1.ErrorPanel);
            //ControlTypeSourceChanged(KodTarakDropDownList_TargySzavakControlTypeSource
            //    , DynamicValueControl_TargySzavakControlTypeDataSource, DynamicValueControl_TargySzavakAlapertelmezettErtek
            //    , true);
            //DynamicValueControl_TargySzavakControlTypeDataSource.Value = TargySzavakTextBox1.ControlTypeDataSource;

            //DynamicValueControl_TargySzavakAlapertelmezettErtek.Value = TargySzavakTextBox1.AlapertelmezettErtek;
            //SetDynamicValueControl(DynamicValueControl_TargySzavakAlapertelmezettErtek, TargySzavakTextBox1.ControlTypeSource, DynamicValueControl_TargySzavakControlTypeDataSource.Value);

            // objektum metaadatai
            KodTarakDropDownList_ControlTypeSource.FillAndSetSelectedValue(kcs_CONTROLTYPE_SOURCE, erec_Obj_MetaAdatai.ControlTypeSource, true, FormHeader1.ErrorPanel);
            ControlTypeSourceChanged(KodTarakDropDownList_ControlTypeSource
                , DynamicValueControl_ControlTypeDataSource, DynamicValueControl_AlapertelmezettErtek
                , false);
            DynamicValueControl_ControlTypeDataSource.Value = erec_Obj_MetaAdatai.ControlTypeDataSource;
            DynamicValueControl_AlapertelmezettErtek.Value = erec_Obj_MetaAdatai.AlapertelmezettErtek;
            SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, erec_Obj_MetaAdatai.ControlTypeSource, DynamicValueControl_ControlTypeDataSource.Value);


        }
        //TargySzavakRegExp.Text = TargySzavakTextBox1.RegExp;

        TextBoxSorszam.Text = erec_Obj_MetaAdatai.Sorszam;

        cbOpcionalis.Checked = (erec_Obj_MetaAdatai.Opcionalis == "1" ? true : false);
        cbIsmetlodo.Checked = (erec_Obj_MetaAdatai.Ismetlodo == "1" ? true : false);

        textFunkcio.Text = erec_Obj_MetaAdatai.Funkcio;

        ErvenyessegCalendarControl1.ErvKezd = erec_Obj_MetaAdatai.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_Obj_MetaAdatai.ErvVege;
        textObj_MetaAdataiNote.Text = erec_Obj_MetaAdatai.Base.Note;

        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = erec_Obj_MetaAdatai.Base.Ver;

        // A m�dos�t�s, l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(erec_Obj_MetaAdatai.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private EREC_Obj_MetaAdatai GetBusinessObjectFromComponents()
    {
        Logger.Info("Obj_MetaAdatai BusinessObject felt�lt�se komponensekb�l.");
        EREC_Obj_MetaAdatai erec_Obj_MetaAdatai = new EREC_Obj_MetaAdatai();
        erec_Obj_MetaAdatai.Updated.SetValueAll(false);
        erec_Obj_MetaAdatai.Base.Updated.SetValueAll(false);

        if (!String.IsNullOrEmpty(ObjMetaDefinicioTextBox1.Id_HiddenField))
        {
            erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id = ObjMetaDefinicioTextBox1.Id_HiddenField;
            erec_Obj_MetaAdatai.Updated.Obj_MetaDefinicio_Id = pageView.GetUpdatedByView(ObjMetaDefinicioTextBox1);
        }

        if (!String.IsNullOrEmpty(TargySzavakTextBox1.Id_HiddenField))
        {
            erec_Obj_MetaAdatai.Targyszavak_Id = TargySzavakTextBox1.Id_HiddenField;
            erec_Obj_MetaAdatai.Updated.Targyszavak_Id = pageView.GetUpdatedByView(TargySzavakTextBox1);
        }

        //erec_Obj_MetaAdatai.AlapertelmezettErtek = TextBoxAlapertelmezettErtek.Text;
        erec_Obj_MetaAdatai.AlapertelmezettErtek = DynamicValueControl_AlapertelmezettErtek.Value;
        erec_Obj_MetaAdatai.Updated.AlapertelmezettErtek = pageView.GetUpdatedByView(DynamicValueControl_AlapertelmezettErtek);

        erec_Obj_MetaAdatai.ControlTypeSource = KodTarakDropDownList_ControlTypeSource.SelectedValue;
        erec_Obj_MetaAdatai.Updated.ControlTypeSource = pageView.GetUpdatedByView(KodTarakDropDownList_ControlTypeSource);

        erec_Obj_MetaAdatai.ControlTypeDataSource = DynamicValueControl_ControlTypeDataSource.Value;
        erec_Obj_MetaAdatai.Updated.ControlTypeDataSource = pageView.GetUpdatedByView(DynamicValueControl_ControlTypeDataSource);

        erec_Obj_MetaAdatai.Sorszam = TextBoxSorszam.Text;
        erec_Obj_MetaAdatai.Updated.Sorszam = pageView.GetUpdatedByView(TextBoxSorszam);
        erec_Obj_MetaAdatai.Funkcio = textFunkcio.Text;
        erec_Obj_MetaAdatai.Updated.Funkcio = pageView.GetUpdatedByView(textFunkcio);

        erec_Obj_MetaAdatai.Opcionalis = (cbOpcionalis.Checked ? "1" : "0");
        erec_Obj_MetaAdatai.Updated.Opcionalis = pageView.GetUpdatedByView(cbOpcionalis);
        erec_Obj_MetaAdatai.Ismetlodo = (cbIsmetlodo.Checked ? "1" : "0");
        erec_Obj_MetaAdatai.Updated.Ismetlodo = pageView.GetUpdatedByView(cbIsmetlodo);
        erec_Obj_MetaAdatai.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_Obj_MetaAdatai.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Obj_MetaAdatai.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_Obj_MetaAdatai.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Obj_MetaAdatai.Base.Note = textObj_MetaAdataiNote.Text;
        erec_Obj_MetaAdatai.Base.Updated.Note = pageView.GetUpdatedByView(textObj_MetaAdataiNote);
           
        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        erec_Obj_MetaAdatai.Base.Ver = FormHeader1.Record_Ver;
        erec_Obj_MetaAdatai.Base.Updated.Ver = true;

        return erec_Obj_MetaAdatai;
    }

    // ha B1 t�pus� a kiv�lasztott objektum metadefin�ci�,
    // akkor t�r�lni fogjuk a cache-t, az�rt igazat adunk vissza
    private bool isCacheToClear()
    {
        if (ObjMetaDefinicioTextBox1.DefinicioTipus == KodTarak.OBJMETADEFINICIO_TIPUS.B1)
        {
            return true;
        }
        else if (DefinicioTipus_HiddenField.Value == KodTarak.OBJMETADEFINICIO_TIPUS.B1)
        {
            // ha a r�gi defin�ci�t�pus volt B1, akkor is t�r�lni kell a cache-t
            return true;
        }
        else
        {
            return false;
        }
    }

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(ObjMetaDefinicioTextBox1);
            compSelector.Add_ComponentOnClick(TargySzavakTextBox1);
            compSelector.Add_ComponentOnClick(cbIsmetlodo);
            compSelector.Add_ComponentOnClick(cbOpcionalis);
            compSelector.Add_ComponentOnClick(textFunkcio);
            compSelector.Add_ComponentOnClick(TextBoxSorszam);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textObj_MetaAdataiNote);

            compSelector.Add_ComponentOnClick(KodTarakDropDownList_TargySzavakControlTypeSource);
            compSelector.Add_ComponentOnClick(DynamicValueControl_TargySzavakControlTypeDataSource);
            compSelector.Add_ComponentOnClick(KodTarakDropDownList_ControlTypeSource);
            compSelector.Add_ComponentOnClick(DynamicValueControl_ControlTypeDataSource);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        Logger.Info("FormFooter1ButtonsClick (CommandEventArgs=" + e.CommandName + ")");
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "ObjMetaAdatai" + Command))
            {
                Logger.Info("ObjMetaAdatai.Command = " + Command);
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                            EREC_Obj_MetaAdatai erec_Obj_MetaAdatai = GetBusinessObjectFromComponents();

                            if (String.IsNullOrEmpty(erec_Obj_MetaAdatai.Targyszavak_Id))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Nincs megadva a t�rgysz� azonos�t�ja!");
                                Logger.Error("ErrorMessage = " + "Nincs megadva a t�rgysz� azonos�t�ja!");
                            }
                            else
                            {

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                // van-e m�r ilyen rekord
                                EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                                if (!String.IsNullOrEmpty(erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id))
                                {
                                    search.Obj_MetaDefinicio_Id.Value = erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id;
                                    search.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

                                }
                                else
                                {
                                    search.Obj_MetaDefinicio_Id.Operator = Query.Operators.isnull;
                                }

                                search.Targyszavak_Id.Value = erec_Obj_MetaAdatai.Targyszavak_Id;
                                search.Targyszavak_Id.Operator = Query.Operators.equals;

                                if (!String.IsNullOrEmpty(erec_Obj_MetaAdatai.Funkcio))
                                {
                                    search.Funkcio.Value = erec_Obj_MetaAdatai.Funkcio;
                                    search.Funkcio.Operator = Query.Operators.equals;
                                }
                                else
                                {
                                    search.Funkcio.Operator = Query.Operators.isnull;
                                }

                                Result searchresult = service.GetAll(execParam, search);

                                if (String.IsNullOrEmpty(searchresult.ErrorCode)
                                    && searchresult.Ds.Tables[0].Rows.Count > 0)
                                {
                                    //volt m�r ilyen
                                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                                    Logger.Error("ErrorMessage = " + Resources.Error.ErrorText_RecordNotUnique);
                                }
                                else
                                {
                                    Result result = service.Insert(execParam, erec_Obj_MetaAdatai);

                                    // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                    // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // B1 objektum metadefin�ci� eset�n t�r�lj�k a cache-t
                                        if (isCacheToClear())
                                        {
                                            EREC_ObjektumTargyszavaiService service_otsz = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                            service_otsz.ClearObjectMetaCache(execParam);
                                        }

                                        if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, TargySzavakTextBox1.Text))
                                        {
                                            String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                            if (!String.IsNullOrEmpty(refreshCallingWindow)
                                                && refreshCallingWindow == "1"
                                                )
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                            }
                                            else
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                            }
                                        }
                                        else
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                        }
                                    }
                                    else
                                    {
                                        Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }

                                }
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                                Logger.Error("ErrorMessage = " + Resources.Error.UINoIdParam);
                            }
                            else
                            {
                                EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                                EREC_Obj_MetaAdatai erec_Obj_MetaAdatai = GetBusinessObjectFromComponents();

                                if (String.IsNullOrEmpty(erec_Obj_MetaAdatai.Targyszavak_Id))
                                {
                                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Nincs megadva a t�rgysz� azonos�t�ja!");
                                    Logger.Error("ErrorMessage = " + "Nincs megadva a t�rgysz� azonos�t�ja!");
                                }
                                else
                                {
                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    // van-e m�r ilyen rekord
                                    EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                                    if (!String.IsNullOrEmpty(erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id))
                                    {
                                        search.Obj_MetaDefinicio_Id.Value = erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id;
                                        search.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;
                                    }
                                    else
                                    {
                                        search.Obj_MetaDefinicio_Id.Operator = Query.Operators.isnull;
                                    }

                                    search.Targyszavak_Id.Value = erec_Obj_MetaAdatai.Targyszavak_Id;
                                    search.Targyszavak_Id.Operator = Query.Operators.equals;

                                    if (!String.IsNullOrEmpty(erec_Obj_MetaAdatai.Funkcio))
                                    {
                                        search.Funkcio.Value = erec_Obj_MetaAdatai.Funkcio;
                                        search.Funkcio.Operator = Query.Operators.equals;
                                    }
                                    else
                                    {
                                        search.Funkcio.Operator = Query.Operators.isnull;
                                    }

                                    // az aktu�lis rekord kiv�tel�vel
                                    search.Id.Value = recordId;
                                    search.Id.Operator = Query.Operators.notequals;

                                    Result searchresult = service.GetAll(execParam, search);

                                    if (String.IsNullOrEmpty(searchresult.ErrorCode)
                                        && searchresult.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        //volt m�r ilyen
                                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                                        Logger.Error("ErrorMessage = " + Resources.Error.ErrorText_RecordNotUnique);
                                    }
                                    else
                                    {
                                        Result result = service.Update(execParam, erec_Obj_MetaAdatai);

                                        // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                        // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!

                                        if (String.IsNullOrEmpty(result.ErrorCode))
                                        {
                                            // B1 objektum metadefin�ci� eset�n t�r�lj�k a cache-t
                                            if (isCacheToClear())
                                            {
                                                EREC_ObjektumTargyszavaiService service_otsz = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                                service_otsz.ClearObjectMetaCache(execParam);
                                            }

                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                            JavaScripts.RegisterCloseWindowClientScript(Page);
                                        }
                                        else
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                            Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                                        }
                                    }

                                }
                            }
                           
                        }
                        break;
                }
            }
            else
            {
                Logger.Error("AccessDenied");
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
     }

    protected void TargySzavakTextBox_TextChanged(object sender, EventArgs e)
    {
        if (TargySzavakTextBox1.Tipus == "0")
        {
            // t�rgyszavak
            if (KodTarakDropDownList_TargySzavakControlTypeSource.DropDownList.Items.Count > 0)
            {
                KodTarakDropDownList_TargySzavakControlTypeSource.SetSelectedValue("");
            }
            else
            {
                KodTarakDropDownList_TargySzavakControlTypeSource.FillAndSetEmptyValue(kcs_CONTROLTYPE_SOURCE, FormHeader1.ErrorPanel);
            }
            DynamicValueControl_TargySzavakControlTypeDataSource.Value = TargySzavakTextBox1.ControlTypeDataSource;
            ControlTypeSourceChanged(KodTarakDropDownList_TargySzavakControlTypeSource
                , DynamicValueControl_TargySzavakControlTypeDataSource, DynamicValueControl_TargySzavakAlapertelmezettErtek
                , true);
            
            DynamicValueControl_TargySzavakAlapertelmezettErtek.Value = "";
            SetDynamicValueControl(DynamicValueControl_TargySzavakAlapertelmezettErtek, TargySzavakTextBox1.ControlTypeSource, DynamicValueControl_TargySzavakControlTypeDataSource.Value);
        }
        else
        {
            // t�rgyszavak
            if (KodTarakDropDownList_TargySzavakControlTypeSource.DropDownList.Items.Count > 0)
            {
                KodTarakDropDownList_TargySzavakControlTypeSource.SetSelectedValue(TargySzavakTextBox1.ControlTypeSource);
            }
            else
            {
                KodTarakDropDownList_TargySzavakControlTypeSource.FillAndSetSelectedValue(kcs_CONTROLTYPE_SOURCE, TargySzavakTextBox1.ControlTypeSource, true, FormHeader1.ErrorPanel);
            }
            //KodTarakDropDownList_TargySzavakControlTypeSource.SetSelectedValue(TargySzavakTextBox1.ControlTypeSource);
            ControlTypeSourceChanged(KodTarakDropDownList_TargySzavakControlTypeSource
                , DynamicValueControl_TargySzavakControlTypeDataSource, DynamicValueControl_TargySzavakAlapertelmezettErtek
                , true);
            DynamicValueControl_TargySzavakControlTypeDataSource.Value = TargySzavakTextBox1.ControlTypeDataSource;

            DynamicValueControl_TargySzavakAlapertelmezettErtek.Value = TargySzavakTextBox1.AlapertelmezettErtek;
            SetDynamicValueControl(DynamicValueControl_TargySzavakAlapertelmezettErtek, TargySzavakTextBox1.ControlTypeSource, DynamicValueControl_TargySzavakControlTypeDataSource.Value);
        }
        TargySzavakRegExp.Text = (TargySzavakTextBox1.Tipus == "0" ? "" : TargySzavakTextBox1.RegExp);

        SetControlTypeControlsByTargySzavakTipus(TargySzavakTextBox1.Tipus);
    }

}