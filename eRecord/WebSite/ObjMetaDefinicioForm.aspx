<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ObjMetaDefinicioForm.aspx.cs" Inherits="ObjMetaDefinicioForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="kddl" %>
<%@ Register Src="Component/KodCsoportokTextBox.ascx" TagName="KodCsoportokTextBox"
    TagPrefix="kcstb" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="eRecordComponent/ObjMetaDefinicioTextBox.ascx" TagName="ObjMetaDefinicioTextBox"
    TagPrefix="omdtb" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox"
    TagPrefix="tsztb" %>
    <%@ Register Src="eRecordComponent/IratMetaDefinicioTextBox.ascx" TagName="IratMetaDefinicioTextBox"
    TagPrefix="imdtb" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <asp:HiddenField ID="IratMetadefinicio_Id_HiddenField" runat="server" />
    <table id="tableObjMetaDefinicioAlarendeles" runat="server" cellpadding="0" cellspacing="0"
        width="90%" visible="false">
        <tr>
            <td>
                <%-- csak al�rendel�si kapcsolat l�trehoz�sakor l�that� --%>
                <eUI:eFormPanel ID="Obj_MetaDefinicio_AlarendelesPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelAlarendeltStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelAlarendelt" runat="server" Text="Al�rendelt objektum metadefin�ci�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <omdtb:ObjMetaDefinicioTextBox ID="ObjMetaDefinicioTextBoxAlarendelt" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <table id="tableObjMetaDefinicio" runat="server" cellpadding="0" cellspacing="0"
        width="90%">
        <tr runat="server">
            <td>
                <eUI:eFormPanel ID="ContentTypePanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelContentTypeStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelContentType" runat="server" Text="ContentType:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc8:RequiredTextBox ID="requiredTextBoxContentType" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelDefinicioTipusStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelDefinicioTipus" runat="server" Text="Defin�ci� t�pus:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodtarakDropDownList ID="KodtarakDropDownList_DefinicioTipus" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="ObjMetaDefinicioUpdatePanel" runat="server" UpdateMode="Conditional"
                    OnLoad="ObjMetaDefinicioUpdatePanel_Load">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="ObjTipusokPanel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr id="tr_Table" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelObjTipusokTablaStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelObjTipusokTabla" runat="server" Text="Objektum t�pus (t�bla):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_Tabla" CssClass="mrUrlapInputSzeles"
                                                Validate="false" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="tr_Column" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelObjTipusokOszlopStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelObjTipusokOszlop" runat="server" Text="Objektum t�pus (oszlop):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_Oszlop" CssClass="mrUrlapInputSzeles"
                                                Validate="false" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="tr_KodCsoport" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelKodCsoport" runat="server" Text="K�dcsoport:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <kcstb:KodCsoportokTextBox ID="KodCsoportokTextBox1" CssClass="mrUrlapInput" ReadOnly="true"
                                                Validate="false" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="tr_ColumnValue" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                        <asp:Label ID="labelColumnValueStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelColumnValue" runat="server" Text="Oszlop�rt�k:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredTextBox ID="requiredTextBoxColumnValue" Validate="false" CssClass="mrUrlapInput" runat="server" Text="" />
                                            <imdtb:IratMetaDefinicioTextBox ID="IratMetaDefinicioTextBox1" CssClass="mrUrlapInputSzeles"
                                                 Validate="false"  runat="server" Text="" />
                                            <kddl:KodtarakDropDownList ID="KodTarakDropDownList_ColumnValue" runat="server" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="IratMetaDefinicioUpdatePanel" runat="server" UpdateMode="Conditional"
                    OnLoad="IratMetaDefinicioUpdatePanel_Load">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="IratMetaDefinicioPanel" runat="server" Visible="false">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <%-- IratMetaDefinicio --%>
                                    <%-- �gyirat szint --%>
                                    <tr id="tr_Ugykor" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUgykor" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" CssClass="mrUrlapInputSzeles"
                                                runat="server" Text="" ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <tr id="tr_Ugytipus" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUgytipus" runat="server" Text="�gyt�pus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="TextBoxUgytipus" CssClass="mrUrlapInputSzeles" runat="server" Text=""
                                                ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <%-- �gyiratdarab szint --%>
                                    <tr id="tr_EljarasiSzakasz" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelEljarasiSzakasz" runat="server" Text="Elj�r�si szakasz:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <kddl:KodtarakDropDownList ID="KodTarakDropDownList_EljarasiSzakasz" runat="server"
                                                Text="" ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <%-- Irat szint --%>
                                    <tr id="tr_Irattipus" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIrattipus" runat="server" Text="Iratt�pus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <kddl:KodtarakDropDownList ID="KodTarakDropDownList_Irattipus" runat="server" Text=""
                                                ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <%-- /IratMetaDefinicio --%>
                                </tbody>
                            </table>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="KiegeszitoAdatokPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Text="SPSSzinkronizalt" Enabled="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFelettesObjId" runat="server" Text="Felettes objektum metadefin�ci�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <omdtb:ObjMetaDefinicioTextBox ID="ObjMetaDefinicioTextBoxFelettes" CssClass="mrUrlapInputSzeles"
                                        runat="server" Text="" />
                                </td>
                            </tr>
                            <tr id="tr_FelettesTipus" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFelettesTipus" runat="server" Text="Felettes metaadatok k�t�se:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButtonList ID="rblFelettesTipus" runat="server">
                                        <asp:ListItem Text="hivatkoz�ssal" Value="reference" Selected="True" />
                                        <asp:ListItem Text="m�sol�ssal" Value="copy" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelErvenyessegStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <table id="tableFooter" runat="server" cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
