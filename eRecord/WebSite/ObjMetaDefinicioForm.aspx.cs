using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI.WebControls;


public partial class ObjMetaDefinicioForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Mode = "";

    private const string kcs_ObjMetaDefinicio_Tipus = "OBJMETADEFINICIO_TIPUS";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_ELJARASISZAKASZ = "ELJARASI_SZAKASZ";

    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private void SetNewControls()
    {
        if (Mode == "Alarendelt")
        {
            tableObjMetaDefinicioAlarendeles.Visible = true;
            tableObjMetaDefinicio.Visible = false;
        }
        else
        {
            tableObjMetaDefinicioAlarendeles.Visible = false;
            tableObjMetaDefinicio.Visible = true;

            ObjTipusokTextBox_Tabla.Validate = true;
            ObjTipusokTextBox_Oszlop.Validate = true;
            requiredTextBoxColumnValue.Validate = true;
            IratMetaDefinicioTextBox1.Validate = true;
        }
    }

    private void SetViewControls()
    {
        IraIrattariTetelTextBox1.ReadOnly = true;
        TextBoxUgytipus.ReadOnly = true;
        KodTarakDropDownList_EljarasiSzakasz.ReadOnly = true;
        KodTarakDropDownList_Irattipus.ReadOnly = true;

        KodtarakDropDownList_DefinicioTipus.ReadOnly = true;
        ObjTipusokTextBox_Tabla.ReadOnly = true;
        ObjTipusokTextBox_Oszlop.ReadOnly = true;
        requiredTextBoxColumnValue.ReadOnly = true;
        IratMetaDefinicioTextBox1.ReadOnly = true;
        KodTarakDropDownList_ColumnValue.ReadOnly = true;
        ObjMetaDefinicioTextBoxFelettes.ReadOnly = true;
        requiredTextBoxContentType.ReadOnly = true;

        ErvenyessegCalendarControl1.ReadOnly = true;
        textNote.ReadOnly = true;

        labelUgykor.CssClass = "mrUrlapInputWaterMarked";
        labelUgytipus.CssClass = "mrUrlapInputWaterMarked";
        labelEljarasiSzakasz.CssClass = "mrUrlapInputWaterMarked";
        labelIrattipus.CssClass = "mrUrlapInputWaterMarked";
        labelDefinicioTipus.CssClass = "mrUrlapInputWaterMarked";
        labelObjTipusokTabla.CssClass = "mrUrlapInputWaterMarked";
        labelObjTipusokOszlop.CssClass = "mrUrlapInputWaterMarked";
        labelColumnValue.CssClass = "mrUrlapInputWaterMarked";
        labelFelettesObjId.CssClass = "mrUrlapInputWaterMarked";
        labelContentType.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelKodCsoport.CssClass = "mrUrlapInputWaterMarked";

        tr_FelettesTipus.Visible = false;
    }

    private void SetModifyControls()
    {
        // TODO: IratMetaDefinicioTextBox vagy Component
        //IratMetaDefinicioPanel.Visible = false;
        KodtarakDropDownList_DefinicioTipus.ReadOnly = true;
        IratMetaDefinicioPanel.Visible = false;

        ObjTipusokTextBox_Tabla.Validate = true;
        ObjTipusokTextBox_Oszlop.Validate = true;
        requiredTextBoxColumnValue.Validate = true;
        IratMetaDefinicioTextBox1.Validate = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        Logger.Info("Page_Init (Command = " + Command + ")");

        ObjTipusokTextBox_Tabla.Filter = Constants.FilterType.ObjTipusok.Tabla;
        ObjTipusokTextBox_Oszlop.Filter = Constants.FilterType.ObjTipusok.Oszlop;

        ObjMetaDefinicioTextBoxFelettes.CustomValueEnabled = false;
        ObjMetaDefinicioTextBoxFelettes.RefreshCallingWindow = false;
        ObjMetaDefinicioTextBoxFelettes.DefinicioTipusFilter = KodTarak.OBJMETADEFINICIO_TIPUS.A0;

        ObjMetaDefinicioTextBoxFelettes.Validate = false;

        ObjMetaDefinicioTextBoxAlarendelt.RefreshCallingWindow = false;

        ObjTipusokTextBox_Oszlop.RefreshCallingWindow = true; // a kiv�lasztott �rt�k v�ltoz�s�nak �rz�kel�s�hez sz�ks�ges
        ObjTipusokTextBox_Oszlop.TextBox.AutoPostBack = true;
        ObjTipusokTextBox_Oszlop.TextBox.TextChanged += new EventHandler(ObjTipusokTextBox_Oszlop_TextChanged);

        //IratMetaDefinicioTextBox1.SearchMode = true;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjMetaDefinicio" + Command);
                break;
        }

        if (Command == CommandName.New)
        {
            String NoSuccessorLoopWithId = Request.QueryString.Get(QueryStringVars.NoSuccessorLoopWithId);

            if (!String.IsNullOrEmpty(NoSuccessorLoopWithId))
            {
                Mode = "Alarendelt";
                ObjMetaDefinicioTextBoxAlarendelt.NoSuccessorLoopWithId = NoSuccessorLoopWithId;

                SetNewControls();
            }
            else
            {
                this.SetNewControls();

                String IratMetadefinicio_Id = Request.QueryString.Get(QueryStringVars.IratMetadefinicioId);
                String DefinicioTipus = Request.QueryString.Get(QueryStringVars.DefinicioTipus);
                String Felettes_Obj_Meta = Request.QueryString.Get(QueryStringVars.ObjMetaDefinicioId);

                if (!String.IsNullOrEmpty(IratMetadefinicio_Id))
                {
                    // EREC_IratMetaDefinicio rekord keres�se
                    EREC_IratMetaDefinicio erec_IratMetaDefinicio = GetBusinessObjectById_IratMetaDefinicio(IratMetadefinicio_Id);
                    SetControlsByDefinicioTipus(KodTarak.OBJMETADEFINICIO_TIPUS.C2);

                    if (erec_IratMetaDefinicio != null)
                    {
                        string ObjTipusTabla = "";
                        string ObjTipusOszlop = "";

                        if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus))
                        {
                            ObjTipusTabla = Constants.TableNames.EREC_IraIratok;
                            ObjTipusOszlop = Constants.ColumnNames.EREC_IraIratok.IratMetaDefinicio_Id;
                        }
                        else if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                        {
                            ObjTipusTabla = Constants.TableNames.EREC_UgyUgyiratdarabok;
                            ObjTipusOszlop = "";
                        }
                        else
                        {
                            ObjTipusTabla = Constants.TableNames.EREC_UgyUgyiratok;
                            ObjTipusOszlop = Constants.ColumnNames.EREC_UgyUgyiratok.IratMetaDefinicio_Id;
                        }

                        // KRT_ObjTipusok rekordok meghat�roz�sa (t�bla, oszlop, Kodcsoport, ha van)
                        if (TryLoad_KRT_ObjTipusok_TablaOszlop(ObjTipusTabla, ObjTipusOszlop) == true)
                        {
                            ObjTipusokTextBox_Tabla.ReadOnly = true;
                            ObjTipusokTextBox_Oszlop.ReadOnly = true;

                            KodtarakDropDownList_DefinicioTipus.FillWithOneValue(kcs_ObjMetaDefinicio_Tipus, KodTarak.OBJMETADEFINICIO_TIPUS.C2, FormHeader1.ErrorPanel);
                            KodtarakDropDownList_DefinicioTipus.ReadOnly = true;
                        }

                        // ha az irat metadefin�ci� k�t�tt, akkor nem l�that� az oszlop �rt�k
                        tr_ColumnValue.Visible = false;

                    }
                    else
                    {
                        Logger.Warn("Az EREC_IratMetaDefinicio objektum �rt�ke null!");
                        IraIrattariTetelTextBox1.ReadOnly = false;
                    }
                }
                else if (!String.IsNullOrEmpty(DefinicioTipus))
                {
                    KodtarakDropDownList_DefinicioTipus.FillWithOneValue(kcs_ObjMetaDefinicio_Tipus, DefinicioTipus, FormHeader1.ErrorPanel);
                    KodtarakDropDownList_DefinicioTipus.ReadOnly = true;

                    SetControlsByDefinicioTipus(DefinicioTipus);
                }
                else
                {
                    KodtarakDropDownList_DefinicioTipus.FillDropDownList(kcs_ObjMetaDefinicio_Tipus, FormHeader1.ErrorPanel);
                    SetControlsByDefinicioTipus(KodtarakDropDownList_DefinicioTipus.SelectedValue);
                }

                if (!String.IsNullOrEmpty(Felettes_Obj_Meta))
                {
                    ObjMetaDefinicioTextBoxFelettes.Id_HiddenField = Felettes_Obj_Meta;
                    ObjMetaDefinicioTextBoxFelettes.SetTextBoxById(FormHeader1.ErrorPanel);
                    ObjMetaDefinicioTextBoxFelettes.ReadOnly = true;
                }
            }
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                Logger.Error("Nincs megadva id!");
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                Logger.Info("Record_Id = " + id);
                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = (EREC_Obj_MetaDefinicio)result.Record;

                    // nem mindegy a sorrend
                    SetControlsByDefinicioTipus(erec_Obj_MetaDefinicio.DefinicioTipus);
                    LoadComponentsFromBusinessObject(erec_Obj_MetaDefinicio);

                    ObjMetaDefinicioTextBoxFelettes.NoAncestorLoopWithId = id;

                    if (erec_Obj_MetaDefinicio.DefinicioTipus == KodTarak.OBJMETADEFINICIO_TIPUS.C2)
                    {
                        String IratMetadefinicio_Id = erec_Obj_MetaDefinicio.ColumnValue;
                        if (!String.IsNullOrEmpty(IratMetadefinicio_Id))
                        {
                            // EREC_IratMetaDefinicio rekord keres�se
                            EREC_IratMetaDefinicio erec_IratMetaDefinicio = GetBusinessObjectById_IratMetaDefinicio(IratMetadefinicio_Id);

                            if (erec_IratMetaDefinicio != null)
                            {
                                IratMetaDefinicioTextBox1.ReadOnly = true;
                                ObjTipusokTextBox_Oszlop.ReadOnly = true;

                                //    string ObjTipusTabla = "";
                                //    string ObjTipusOszlop = "";

                                //    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus))
                                //    {
                                //        ObjTipusTabla = "EREC_IraIratok";
                                //        ObjTipusOszlop = "IratMetaDef_Id";
                                //    }
                                //    else if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                                //    {
                                //        ObjTipusTabla = "EREC_UgyUgyiratdarabok";
                                //        ObjTipusOszlop = "";
                                //    }
                                //    else
                                //    {
                                //        ObjTipusTabla = "EREC_UgyUgyiratok";
                                //        ObjTipusOszlop = "IratMetadefinicio_Id";
                                //    }

                                //    // KRT_ObjTipusok rekordok meghat�roz�sa (t�bla, oszlop, Kodcsoport, ha van)
                                //    if (TryLoad_KRT_ObjTipusok_TablaOszlop(ObjTipusTabla, ObjTipusOszlop) == true)
                                //    {
                                //        ObjTipusokTextBox_Tabla.ReadOnly = true;
                                //        ObjTipusokTextBox_Oszlop.ReadOnly = true;

                                //        KodtarakDropDownList_DefinicioTipus.ReadOnly = true;
                                //    }

                            }
                            else
                            {
                                Logger.Warn("Az EREC_IratMetaDefinicio objektum �rt�ke null!");
                                //IraIrattariTetelTextBox1.ReadOnly = false;
                                IratMetaDefinicioTextBox1.ReadOnly = false;
                            }
                        }
                    }
                }
                else
                {
                    Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (page " + Resources.Form.ObjMetaDefinicioFormHeaderTitle + ")");
        FormHeader1.HeaderTitle = Resources.Form.ObjMetaDefinicioFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        KodtarakDropDownList_DefinicioTipus.DropDownList.Attributes.Add("onchange", "__doPostBack('" + ObjMetaDefinicioUpdatePanel.ClientID + "','" + EventArgumentConst.refresh + "');");
        //ObjTipusokTextBox_Oszlop.TextBox.Attributes.Add("onchange", "__doPostBack('" + ObjMetaDefinicioUpdatePanel.ClientID + "','" + EventArgumentConst.refreshMasterList + "');");

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    protected void ObjMetaDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refresh:
                    SetControlsByDefinicioTipus(KodtarakDropDownList_DefinicioTipus.SelectedValue);
                    break;
            }
        }
    }

    protected void IratMetaDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refresh:
                    break;
            }
        }
    }

    protected void ObjTipusokTextBox_Oszlop_TextChanged(object sender, EventArgs e)
    {
        KodCsoportokTextBox1.Id_HiddenField = ObjTipusokTextBox_Oszlop.KodCsoport;
        KodCsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

        if (KodtarakDropDownList_DefinicioTipus.SelectedValue == KodTarak.OBJMETADEFINICIO_TIPUS.B2)
        {
            if (!String.IsNullOrEmpty(KodCsoportokTextBox1.Kodcsoport_Kod))
            {
                requiredTextBoxColumnValue.Visible = false;
                requiredTextBoxColumnValue.Text = "";
                KodTarakDropDownList_ColumnValue.Visible = true;
                KodTarakDropDownList_ColumnValue.FillDropDownList(KodCsoportokTextBox1.Kodcsoport_Kod, FormHeader1.ErrorPanel);
            }
            else
            {
                requiredTextBoxColumnValue.Visible = true;
                KodTarakDropDownList_ColumnValue.Visible = false;
                KodTarakDropDownList_ColumnValue.Clear();
            }
        }

    }

    // l�that�s�gok be�ll�t�sa, mez�k t�rl�se
    protected void SetControlsByDefinicioTipus(string DefinicioTipus)
    {
        switch (DefinicioTipus)
        {
            case KodTarak.OBJMETADEFINICIO_TIPUS.A0:
                ObjTipusokPanel.Visible = false;
                IratMetaDefinicioPanel.Visible = false;

                ObjTipusokTextBox_Tabla.Id_HiddenField = "";
                ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                ObjTipusokTextBox_Oszlop.Id_HiddenField = "";
                ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                requiredTextBoxColumnValue.Text = "";
                IratMetaDefinicioTextBox1.Id_HiddenField = "";
                IratMetaDefinicioTextBox1.SetIratMetaDefinicioTextBoxById(FormHeader1.ErrorPanel);
                KodTarakDropDownList_ColumnValue.SetSelectedValue("");
                break;
            case KodTarak.OBJMETADEFINICIO_TIPUS.B1:
            case KodTarak.OBJMETADEFINICIO_TIPUS.B3:
                ObjTipusokPanel.Visible = true;
                IratMetaDefinicioPanel.Visible = false;
                tr_Table.Visible = true;
                tr_Column.Visible = false;
                tr_ColumnValue.Visible = false;
                tr_KodCsoport.Visible = false;

                ObjTipusokTextBox_Oszlop.Id_HiddenField = "";
                ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                requiredTextBoxColumnValue.Text = "";
                IratMetaDefinicioTextBox1.Id_HiddenField = "";
                IratMetaDefinicioTextBox1.SetIratMetaDefinicioTextBoxById(FormHeader1.ErrorPanel);
                KodTarakDropDownList_ColumnValue.SetSelectedValue("");
                break;
            case KodTarak.OBJMETADEFINICIO_TIPUS.B2:
            case KodTarak.OBJMETADEFINICIO_TIPUS.B4:
                ObjTipusokPanel.Visible = true;
                IratMetaDefinicioPanel.Visible = false;
                tr_Table.Visible = false;
                tr_Column.Visible = true;
                tr_ColumnValue.Visible = true;
                if (!String.IsNullOrEmpty(KodCsoportokTextBox1.Kodcsoport_Kod))
                {
                    KodTarakDropDownList_ColumnValue.Visible = true;
                    KodTarakDropDownList_ColumnValue.FillDropDownList(KodCsoportokTextBox1.Kodcsoport_Kod, FormHeader1.ErrorPanel);
                    requiredTextBoxColumnValue.Visible = false;
                }
                else
                {
                    KodTarakDropDownList_ColumnValue.Visible = false;
                    KodTarakDropDownList_ColumnValue.SetSelectedValue("");
                    requiredTextBoxColumnValue.Visible = true;
                }
                IratMetaDefinicioTextBox1.Visible = false;
                IratMetaDefinicioTextBox1.Validate = false;
                tr_KodCsoport.Visible = true;

                ObjTipusokTextBox_Tabla.Id_HiddenField = "";
                ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                IratMetaDefinicioTextBox1.Id_HiddenField = "";
                IratMetaDefinicioTextBox1.SetIratMetaDefinicioTextBoxById(FormHeader1.ErrorPanel);

                ObjTipusokTextBox_Oszlop.UgyiratHierarchia = false;
                break;
            case KodTarak.OBJMETADEFINICIO_TIPUS.C2:
                ObjTipusokPanel.Visible = true;
                IratMetaDefinicioPanel.Visible = true;
                tr_Table.Visible = false;
                tr_Column.Visible = true;
                tr_ColumnValue.Visible = true;
                requiredTextBoxColumnValue.Visible = false;
                IratMetaDefinicioTextBox1.Visible = true;
                IratMetaDefinicioTextBox1.Validate = true;
                KodTarakDropDownList_ColumnValue.Visible = false;
                tr_KodCsoport.Visible = false;

                ObjTipusokTextBox_Tabla.Id_HiddenField = "";
                ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                ObjTipusokTextBox_Oszlop.Id_HiddenField = "";
                ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
                requiredTextBoxColumnValue.Text = "";
                KodTarakDropDownList_ColumnValue.Clear();

                ObjTipusokTextBox_Oszlop.UgyiratHierarchia = true;
                break;
        }
    }

    /// <summary>
    /// �zleti objektum rekordj�nak bet�lt�se Id alapj�n
    /// </summary>
    /// <param name="Id"></param>
    private EREC_IratMetaDefinicio GetBusinessObjectById_IratMetaDefinicio(String Id)
    {
        if (String.IsNullOrEmpty(Id))
        {
            Logger.Warn("Nincs megadva EREC_IratMetaDefinicio Id!");
            return null;
        }

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = null;
        // EREC_IratMetaDefinicio rekord keres�se
        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = Id;
        Logger.Info("IratMetaDefinicio keresese IratMetadefinicio_Id = " + Id);

        Result result = service.Get(execParam);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
            if (erec_IratMetaDefinicio != null)
            {
                LoadComponentsFromBusinessObject_IratMetaDefinicio(erec_IratMetaDefinicio);
            }
            else
            {
                Logger.Error("Az EREC_IratMetaDefinicio objektum �rt�ke null!");
            }
        }
        else
        {
            Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }

        return erec_IratMetaDefinicio;  // hiba eset�n �rt�ke null

    }

    /// <summary>
    /// �zleti objektum rekordj�nak bet�lt�se t�blan�v �s oszlopn�v alapj�n
    /// </summary>
    /// <param name="Id"></param>
    private bool TryLoad_KRT_ObjTipusok_TablaOszlop(String Tabla, String Oszlop)
    {
        if (String.IsNullOrEmpty(Tabla))
        {
            Logger.Warn("Nincs megadva Tabla!");
            return false;
        }

        if (String.IsNullOrEmpty(Oszlop))
        {
            Logger.Warn("Nincs megadva Oszlop!");
            return false;
        }

        // KRT_ObjTipusok rekord keres�se
        KRT_ObjTipusokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Logger.Info("ObjTipusok keresese Tabla = " + Tabla + "; Oszlop = " + Oszlop);
        Result result = service.GetAllByTableColumn(execParam, Tabla, Oszlop);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                ObjTipusokTextBox_Tabla.Id_HiddenField = result.Ds.Tables[0].Rows[0]["Obj_Id_Szulo"].ToString();
                ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);

                ObjTipusokTextBox_Oszlop.Id_HiddenField = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);

                // ColumnValue Kodcsoport?
                KodCsoportokTextBox1.Id_HiddenField = result.Ds.Tables[0].Rows[0]["KodCsoport_Id"].ToString();
                KodCsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
            }
            else
            {
                Logger.Error("Az KRT_ObjTipusok objektum nem meghat�rozhat�!");
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Az objektum t�pusa nem meghat�rozhat�!");
                return false;
            }
        }
        else
        {
            Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
    }

    private void LoadComponentsFromBusinessObject_IratMetaDefinicio(EREC_IratMetaDefinicio erec_IratMetaDefinicio)
    {
        Logger.Info("Komponensek feltoltese IratMetaDefinicio BusinessObject -b�l");

        //requiredTextBoxColumnValue.Text = erec_IratMetaDefinicio.Id;
        //requiredTextBoxColumnValue.ReadOnly = true;

        IratMetaDefinicioTextBox1.Id_HiddenField = erec_IratMetaDefinicio.Id;
        IratMetaDefinicioTextBox1.SetIratMetaDefinicioTextBoxById(FormHeader1.ErrorPanel);

        tr_Irattipus.Visible = false;
        tr_EljarasiSzakasz.Visible = false;
        tr_Ugytipus.Visible = false;
        tr_Ugykor.Visible = false;
        if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugykor_Id))
        {
            tr_Ugykor.Visible = true;
            IraIrattariTetelTextBox1.Id_HiddenField = erec_IratMetaDefinicio.Ugykor_Id;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

            if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.UgytipusNev))
            {
                tr_Ugytipus.Visible = true;
                TextBoxUgytipus.Text = erec_IratMetaDefinicio.UgytipusNev;

                if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                {
                    tr_EljarasiSzakasz.Visible = true;
                    KodTarakDropDownList_EljarasiSzakasz.FillWithOneValue(kcs_ELJARASISZAKASZ, erec_IratMetaDefinicio.EljarasiSzakasz, FormHeader1.ErrorPanel);

                    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus))
                    {
                        tr_Irattipus.Visible = true;
                        KodTarakDropDownList_Irattipus.FillWithOneValue(kcs_IRATTIPUS, erec_IratMetaDefinicio.Irattipus, FormHeader1.ErrorPanel);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="erec_Obj_MetaDefinicio"></param>
    private void LoadComponentsFromBusinessObject(EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio)
    {
        Logger.Info("Komponensek feltoltese Obj_MetaDefinicio BusinessObject -b�l");

        requiredTextBoxContentType.Text = erec_Obj_MetaDefinicio.ContentType;

        KodtarakDropDownList_DefinicioTipus.FillAndSetSelectedValue(kcs_ObjMetaDefinicio_Tipus, erec_Obj_MetaDefinicio.DefinicioTipus, FormHeader1.ErrorPanel);

        ObjTipusokTextBox_Tabla.Id_HiddenField = erec_Obj_MetaDefinicio.Objtip_Id;
        ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);

        if (!String.IsNullOrEmpty(erec_Obj_MetaDefinicio.Objtip_Id_Column))
        {
            ObjTipusokTextBox_Oszlop.Id_HiddenField = erec_Obj_MetaDefinicio.Objtip_Id_Column;
            ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
        }

        if (erec_Obj_MetaDefinicio.DefinicioTipus == KodTarak.OBJMETADEFINICIO_TIPUS.C2)
        {
            IratMetaDefinicioTextBox1.Id_HiddenField = erec_Obj_MetaDefinicio.ColumnValue;
            IratMetaDefinicioTextBox1.SetIratMetaDefinicioTextBoxById(FormHeader1.ErrorPanel);

            KodTarakDropDownList_ColumnValue.Clear();
            requiredTextBoxColumnValue.Text = "";
            KodTarakDropDownList_ColumnValue.Visible = false;
            requiredTextBoxColumnValue.Visible = false;
        }
        else if (!String.IsNullOrEmpty(ObjTipusokTextBox_Oszlop.KodCsoport))
        {
            KodCsoportokTextBox1.Id_HiddenField = ObjTipusokTextBox_Oszlop.KodCsoport;
            KodCsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

            KodTarakDropDownList_ColumnValue.FillAndSetSelectedValue(KodCsoportokTextBox1.Kodcsoport_Kod, erec_Obj_MetaDefinicio.ColumnValue, FormHeader1.ErrorPanel);
            requiredTextBoxColumnValue.Text = "";
            KodTarakDropDownList_ColumnValue.Visible = true;
            requiredTextBoxColumnValue.Visible = false;
        }
        else
        {
            KodTarakDropDownList_ColumnValue.Clear();
            requiredTextBoxColumnValue.Text = erec_Obj_MetaDefinicio.ColumnValue;
            KodTarakDropDownList_ColumnValue.Visible = false;
            requiredTextBoxColumnValue.Visible = true;
        }

        ObjMetaDefinicioTextBoxFelettes.Id_HiddenField = erec_Obj_MetaDefinicio.Felettes_Obj_Meta;
        ObjMetaDefinicioTextBoxFelettes.SetTextBoxById(FormHeader1.ErrorPanel);
        //ObjMetaDefinicioTextBoxFelettes.ReadOnly = true;
        //requiredTextBoxContentType.ReadOnly = true;

        cbSPSSzinkronizalt.Checked = (erec_Obj_MetaDefinicio.SPSSzinkronizalt == "1" ? true : false);

        ErvenyessegCalendarControl1.ErvKezd = erec_Obj_MetaDefinicio.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_Obj_MetaDefinicio.ErvVege;
        textNote.Text = erec_Obj_MetaDefinicio.Base.Note;

        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = erec_Obj_MetaDefinicio.Base.Ver;

        // A m�dos�t�s, l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(erec_Obj_MetaDefinicio.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private EREC_Obj_MetaDefinicio GetBusinessObjectFromComponents()
    {
        Logger.Info("Obj_MetaDefinicio BusinessObject felt�lt�se komponensekb�l.");
        EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = new EREC_Obj_MetaDefinicio();
        erec_Obj_MetaDefinicio.Updated.SetValueAll(false);
        erec_Obj_MetaDefinicio.Base.Updated.SetValueAll(false);

        erec_Obj_MetaDefinicio.ContentType = requiredTextBoxContentType.Text;
        erec_Obj_MetaDefinicio.Updated.ContentType = pageView.GetUpdatedByView(requiredTextBoxContentType);

        erec_Obj_MetaDefinicio.DefinicioTipus = KodtarakDropDownList_DefinicioTipus.SelectedValue;
        erec_Obj_MetaDefinicio.Updated.DefinicioTipus = pageView.GetUpdatedByView(KodtarakDropDownList_DefinicioTipus);

        //erec_Obj_MetaDefinicio.Objtip_Id = ObjTipusokTextBox_Tabla.Id_HiddenField;
        //erec_Obj_MetaDefinicio.Updated.Objtip_Id = pageView.GetUpdatedByView(ObjTipusokTextBox_Tabla);
        //erec_Obj_MetaDefinicio.Objtip_Id_Column = ObjTipusokTextBox_Oszlop.Id_HiddenField;
        //erec_Obj_MetaDefinicio.Updated.Objtip_Id_Column = pageView.GetUpdatedByView(ObjTipusokTextBox_Oszlop);

        if (KodtarakDropDownList_DefinicioTipus.SelectedValue == KodTarak.OBJMETADEFINICIO_TIPUS.C2)
        {
            erec_Obj_MetaDefinicio.ColumnValue = IratMetaDefinicioTextBox1.Id_HiddenField;
            erec_Obj_MetaDefinicio.Updated.ColumnValue = pageView.GetUpdatedByView(IratMetaDefinicioTextBox1);
        }
        else if (!String.IsNullOrEmpty(KodCsoportokTextBox1.Id_HiddenField))
        {
            erec_Obj_MetaDefinicio.ColumnValue = KodTarakDropDownList_ColumnValue.SelectedValue;
            erec_Obj_MetaDefinicio.Updated.ColumnValue = pageView.GetUpdatedByView(KodTarakDropDownList_ColumnValue);
        }
        else
        {
            erec_Obj_MetaDefinicio.ColumnValue = requiredTextBoxColumnValue.Text;
            erec_Obj_MetaDefinicio.Updated.ColumnValue = pageView.GetUpdatedByView(requiredTextBoxColumnValue);
        }

        // Objektum t�pus (t�bla) B2 �s C2 defin�ci�k eset�n az oszloppal egy�tt
        if (KodtarakDropDownList_DefinicioTipus.SelectedValue == KodTarak.OBJMETADEFINICIO_TIPUS.C2
            || KodtarakDropDownList_DefinicioTipus.SelectedValue == KodTarak.OBJMETADEFINICIO_TIPUS.B2)
        {
            erec_Obj_MetaDefinicio.Objtip_Id = ObjTipusokTextBox_Oszlop.Obj_Id_Szulo;
            erec_Obj_MetaDefinicio.Updated.Objtip_Id = pageView.GetUpdatedByView(ObjTipusokTextBox_Oszlop);
            erec_Obj_MetaDefinicio.Objtip_Id_Column = ObjTipusokTextBox_Oszlop.Id_HiddenField;
            erec_Obj_MetaDefinicio.Updated.Objtip_Id_Column = pageView.GetUpdatedByView(ObjTipusokTextBox_Oszlop);
        }
        else
        {
            erec_Obj_MetaDefinicio.Objtip_Id = ObjTipusokTextBox_Tabla.Id_HiddenField;
            erec_Obj_MetaDefinicio.Updated.Objtip_Id = pageView.GetUpdatedByView(ObjTipusokTextBox_Tabla);
            erec_Obj_MetaDefinicio.Objtip_Id_Column = "";
            erec_Obj_MetaDefinicio.Updated.Objtip_Id_Column = pageView.GetUpdatedByView(ObjTipusokTextBox_Tabla);
        }

        // TODO: Obj_MetaDefinicioTextBox?

        erec_Obj_MetaDefinicio.Felettes_Obj_Meta = ObjMetaDefinicioTextBoxFelettes.Id_HiddenField;
        erec_Obj_MetaDefinicio.Updated.Felettes_Obj_Meta = pageView.GetUpdatedByView(ObjMetaDefinicioTextBoxFelettes);

        erec_Obj_MetaDefinicio.ContentType = requiredTextBoxContentType.Text;
        erec_Obj_MetaDefinicio.Updated.ContentType = pageView.GetUpdatedByView(requiredTextBoxContentType);

        erec_Obj_MetaDefinicio.SPSSzinkronizalt = (cbSPSSzinkronizalt.Checked ? "1" : "0");
        erec_Obj_MetaDefinicio.Updated.SPSSzinkronizalt = pageView.GetUpdatedByView(cbSPSSzinkronizalt);

        erec_Obj_MetaDefinicio.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_Obj_MetaDefinicio.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Obj_MetaDefinicio.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_Obj_MetaDefinicio.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Obj_MetaDefinicio.Base.Note = textNote.Text;
        erec_Obj_MetaDefinicio.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        erec_Obj_MetaDefinicio.Base.Ver = FormHeader1.Record_Ver;
        erec_Obj_MetaDefinicio.Base.Updated.Ver = true;

        return erec_Obj_MetaDefinicio;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(KodtarakDropDownList_DefinicioTipus);
            compSelector.Add_ComponentOnClick(ObjTipusokTextBox_Tabla);
            compSelector.Add_ComponentOnClick(ObjTipusokTextBox_Oszlop);
            compSelector.Add_ComponentOnClick(requiredTextBoxColumnValue);
            compSelector.Add_ComponentOnClick(ObjMetaDefinicioTextBoxFelettes);
            compSelector.Add_ComponentOnClick(requiredTextBoxContentType);
            compSelector.Add_ComponentOnClick(cbSPSSzinkronizalt);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textNote);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        //Logger.Info("FormFooter1ButtonsClick (CommandEventArgs=" + e.CommandName + ")");
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + Command))
            {
                //Logger.Info("ObjMetaDefinicio.Command = "+Command);
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            if (Mode == "Alarendelt" && !String.IsNullOrEmpty(ObjMetaDefinicioTextBoxAlarendelt.Id_HiddenField))
                            {
                                Logger.Info("ObjMetadefinicio al�rendelt (Update) start");
                                execParam.Record_Id = ObjMetaDefinicioTextBoxAlarendelt.Id_HiddenField;

                                Result result_Get = service.Get(execParam);

                                if (String.IsNullOrEmpty(result_Get.ErrorCode))
                                {
                                    EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = (EREC_Obj_MetaDefinicio)result_Get.Record;
                                    if (erec_Obj_MetaDefinicio != null && !String.IsNullOrEmpty(ObjMetaDefinicioTextBoxAlarendelt.NoSuccessorLoopWithId))
                                    {
                                        erec_Obj_MetaDefinicio.Felettes_Obj_Meta = ObjMetaDefinicioTextBoxAlarendelt.NoSuccessorLoopWithId;
                                        erec_Obj_MetaDefinicio.Updated.Felettes_Obj_Meta = true;

                                        Result result_Update = service.Update(execParam, erec_Obj_MetaDefinicio);
                                        if (!result_Update.IsError)
                                        {
                                            if (JavaScripts.SendBackResultToCallingWindow(Page, execParam.Record_Id, ObjMetaDefinicioTextBoxAlarendelt.Text))
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page);
                                            }
                                            else
                                            {
                                                JavaScripts.RegisterSelectedRecordIdToParent(Page, execParam.Record_Id);
                                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                            }
                                        }
                                        else
                                        {
                                            //Logger.Error("result.ErrorMessage = " + result_Update.ErrorMessage);
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_Update);
                                        }
                                    }
                                    else
                                    {
                                        //Logger.Error("result.ErrorMessage = " + result_Get.ErrorMessage);
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_Get);
                                    }
                                }
                            }
                            else // nem al�rendel�s
                            {
                                EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = GetBusinessObjectFromComponents();

                                DateTime dt;

                                if (DateTime.TryParse(erec_Obj_MetaDefinicio.ErvKezd, out dt))
                                {
                                    if (dt == DateTime.Today)
                                    {
                                        erec_Obj_MetaDefinicio.ErvKezd = DateTime.Now.ToString();
                                    }
                                }

                                Logger.Info("ObjMetadefinicio INSERT start");
                                Result result = service.Insert(execParam, erec_Obj_MetaDefinicio);

                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    bool isThereAnError = false;
                                    if (rblFelettesTipus.SelectedValue == "copy")
                                    {
                                        execParam.Record_Id = result.Uid;
                                        //Logger.Info("ObjMetadefinicio SetMetaFromFelettes start");
                                        Result result_copy = service.SetMetaFromFelettes(execParam);

                                        if (!String.IsNullOrEmpty(result_copy.ErrorCode))
                                        {
                                            //Logger.Error("result.ErrorMessage = " + result_copy.ErrorMessage);
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_copy);
                                            isThereAnError = true;
                                        }
                                    }

                                    if (!isThereAnError)
                                    {
                                        if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxContentType.Text))
                                        {
                                            JavaScripts.RegisterCloseWindowClientScript(Page);
                                        }
                                        else
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                        }
                                    }
                                }
                                else
                                {
                                    //Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                        }
                        break;
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                                Logger.Error("ErrorMessage = " + Resources.Error.UINoIdParam);
                            }
                            else
                            {
                                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                                EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_Obj_MetaDefinicio);

                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!

                                if (!result.IsError)
                                {
                                    bool isThereAnError = false;
                                    if (rblFelettesTipus.SelectedValue == "copy")
                                    {
                                        Logger.Info("ObjMetadefinicio SetMetaFromFelettes start");
                                        Result result_copy = service.SetMetaFromFelettes(execParam);

                                        if (result_copy.IsError)
                                        {
                                            //Logger.Error("result.ErrorMessage = " + result_copy.ErrorMessage);
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_copy);
                                            isThereAnError = true;
                                        }
                                    }

                                    if (!isThereAnError)
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    //Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                                }
                            }

                        }
                        break;
                }
            }
            else
            {
                Logger.Error("AccessDenied");
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}