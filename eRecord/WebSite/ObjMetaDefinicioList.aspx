<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ObjMetaDefinicioList.aspx.cs" Inherits="ObjMetaDefinicioList" Title="<%$Resources:List,ObjMetaDefinicioListHeaderTitle%>" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="ObjMetaDefinicioCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="ObjMetaDefinicioUpdatePanel" runat="server" OnLoad="ObjMetaDefinicioUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="ObjMetaDefinicioCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="ObjMetaDefinicioCPEButton"
                            CollapseControlID="ObjMetaDefinicioCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="ObjMetaDefinicioCPEButton" ExpandedSize="0" ExpandedText="Objektum metadefin�ci�k list�ja"
                            CollapsedText="Objektum metadefin�ci�k list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="ObjMetaDefinicioGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="ObjMetaDefinicioGridView_RowCommand" OnPreRender="ObjMetaDefinicioGridView_PreRender"
                                            OnSorting="ObjMetaDefinicioGridView_Sorting" OnRowDataBound="ObjMetaDefinicioGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="ContentType" HeaderText="ContentType" SortExpression="ContentType">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ObjTipusok_Tabla" HeaderText="T�bla" SortExpression="ObjTipusok_Tabla">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ObjTipusok_Oszlop" HeaderText="Oszlop" SortExpression="ObjTipusok_Oszlop">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ColumnValue_Lekepezett" HeaderText="�rt�k" SortExpression="ColumnValue_Lekepezett">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DefinicioTipus_Nev" HeaderText="Defin�ci� t�pus" SortExpression="DefinicioTipus_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Al�rendel�s megengedett" SortExpression="DefinicioTipus">
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="150px" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderTemplate />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSubdefinitionEnabled" runat="server" Checked='<%# (Eval("DefinicioTipus") as string) == "A0" ? true : false %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
<%--                                                <asp:BoundField DataField="Felettes_Obj_Meta_CTT" HeaderText="Felettes" SortExpression="Felettes_Obj_Meta_CTT">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Felettes" SortExpression="Felettes_Obj_Meta_CTT">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelFelettesCTT" runat="server" Text='<%#
                                                            string.IsNullOrEmpty(Eval("Felettes_Obj_Meta_CTT") as string) ? "" : "<a href=\"ObjMetaDefinicioList.aspx?Id="+ Eval("Felettes_Obj_Meta") as string + "\" style=\"text-decoration:underline\"> "
                                                            + Eval("Felettes_Obj_Meta_CTT") as string +"<a />"%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Szinkroniz�lt" SortExpression="SPSSzinkronizalt">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="panelDetail" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <%-- panelDetail --%>
                            <asp:Panel ID="panelDetail" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
                                    <ajaxToolkit:TabPanel ID="ObjMetaAdataiTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="ObjMetaAdataiHeaderUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerObjMetaAdatai" runat="server" Text="Metaadat hozz�rendel�sek"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="ObjMetaAdataiUpdatePanel" runat="server" OnLoad="ObjMetaAdataiUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="ObjMetaAdataiPanel" runat="server" Visible="true" Width="100%">
                                                        <uc1:SubListHeader ID="ObjMetaAdataiSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="ObjMetaAdataiCPE" runat="server" TargetControlID="panelObjMetaAdataiList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelObjMetaAdataiList" runat="server">
                                                                        <asp:GridView ID="ObjMetaAdataiGridView" runat="server" AutoGenerateColumns="False"
                                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                            OnPreRender="ObjMetaAdataiGridView_PreRender" OnRowCommand="ObjMetaAdataiGridView_RowCommand"
                                                                            OnSelectedIndexChanging="ObjMetaAdataiGridView_SelectedIndexChanging"
                                                                            DataKeyNames="Id" OnSorting="ObjMetaAdataiGridView_Sorting" OnRowDataBound="ObjMetaAdataiGridView_RowDataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Sorszam" HeaderText="Sorsz�m" SortExpression="Sorszam">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
<%--                                                                                <asp:BoundField DataField="TargySzavak" HeaderText="T�rgysz�" SortExpression="TargySzavak">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                                <asp:TemplateField HeaderText="T�rgysz�" SortExpression="TargySzavak">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelTargySzavak" runat="server" Text='<%#
                                                                                            string.IsNullOrEmpty(Eval("TargySzavak") as string) ? "" : "<a href=\"TargySzavakList.aspx?Id="+ Eval("Targyszavak_Id") as string + "\" style=\"text-decoration:underline\"> "
                                                                                            + Eval("TargySzavak") as string +"<a />"%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderTipus" runat="server" Text="�rt�k tartozik hozz�" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbTipus" runat="server" Enabled="false" AutoPostBack="false" Checked='<%# (Eval("Tipus") as string) == "1" ? true : false  %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
<%--                                                                                <asp:BoundField DataField="AlapertelmezettErtek" HeaderText="Alap�rtelmezett �rt�k" SortExpression="AlapertelmezettErtek">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                                <asp:TemplateField AccessibleHeaderText="AlapertelmezettErtek" HeaderText="Alap�rtelmezett �rt�k"
                                                                                    SortExpression="AlapertelmezettErtek_Lekepezett">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <%-- A Labeleket nem akarjuk besz�rk�teni, ez�rt csak a checkboxra �ll�tjuk be az Enabled="false"-t --%>
                                                                                        <eUI:DynamicValueControl ID="DVC_AlapertelmezettErtek" runat="server"
                                                                                        DefaultControlTypeSource='<%# Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label %>'
                                                                                        ControlTypeSource='<%# (Eval("ControlTypeSource_Lekepezett") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox : Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label  %>'
                                                                                        Value='<%# Eval("AlapertelmezettErtek_Lekepezett") %>' Validate="false" Enabled='<%# (Eval("ControlTypeSource_Lekepezett") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? false : true  %>'
                                                                                        />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderOpcionalis" runat="server" Text="Opcion�lis" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbOpcionalis" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            Checked='<%# (Eval("Opcionalis") as string) == "1" ? true : false  %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderIsmetlodo" runat="server" Text="Ism�tl�d�" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbIsmetlodo" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            Checked='<%# (Eval("Ismetlodo") as string) == "1" ? true : false  %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Funkcio" HeaderText="Funkci�" SortExpression="Funkcio">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderSPSSzinkronizalt" runat="server" Text="Szinkroniz�lt" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            Checked='<%# (Eval("SPSSzinkronizalt") as string) == "1" ? true : false  %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Forr�s (felettes CTT)" SortExpression="ContentType">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbOrokolt" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            CssClass="HideCheckBoxText" Checked="false" Visible="false" />
                                                                                            <asp:Label ID="labelSzint" Text='<%# Eval("Szint") %>' Visible="false" runat="server" />
<%--                                                                                             <asp:Label ID="labelObjMetaAdatContentType" Text='<%# Eval("ContentType") %>' Visible="false" runat="server" /> --%>
                                                                                            <asp:Label ID="labelObjMetaAdatContentType" runat="server" Text='<%#
                                                                                                (Eval("Szint") != null && Eval("Szint").ToString() == "0") || string.IsNullOrEmpty(Eval("ContentType") as string) ? "" : "<a href=\"ObjMetaDefinicioList.aspx?Id="+ Eval("Obj_MetaDefinicio_Id") as string + "\" style=\"text-decoration:underline\"> "
                                                                                                + Eval("ContentType") as string +"<a />"%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbInvalidateEnabled" runat="server" Enabled="false" Checked="true" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="ObjMetaDefinicioAlarendeltTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="ObjMetaDefinicioAlarendeltHeaderUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerObjMetaDefinicioAlarendelt" runat="server" Text="Al�rendelt objektum metadefin�ci�k"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="ObjMetaDefinicioAlarendeltUpdatePanel" runat="server" OnLoad="ObjMetaDefinicioAlarendeltUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="ObjMetaDefinicioAlarendeltPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="ObjMetaDefinicioAlarendeltSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="ObjMetaDefinicioAlarendeltCPE" runat="server"
                                                                        TargetControlID="Panel2" CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                                                        AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <asp:GridView ID="ObjMetaDefinicioAlarendeltGridView" runat="server" CellPadding="0"
                                                                            CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="ObjMetaDefinicioAlarendeltGridView_Sorting"
                                                                            OnPreRender="ObjMetaDefinicioAlarendeltGridView_PreRender" OnRowCommand="ObjMetaDefinicioAlarendeltGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
<%--                                                                                <asp:BoundField DataField="ContentType" HeaderText="ContentType" SortExpression="ContentType">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                                <asp:TemplateField HeaderText="ContentType" SortExpression="ContentType">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelContentType" runat="server" Text='<%#
                                                                                            string.IsNullOrEmpty(Eval("ContentType") as string) ? "" : "<a href=\"ObjMetaDefinicioList.aspx?Id="+ Eval("Id") as string + "\" style=\"text-decoration:underline\"> "
                                                                                            + Eval("ContentType") as string +"<a />"%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="ObjTipusok_Tabla" HeaderText="T�bla" SortExpression="ObjTipusok_Tabla">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ObjTipusok_Oszlop" HeaderText="Oszlop" SortExpression="ObjTipusok_Oszlop">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ColumnValue_Lekepezett" HeaderText="�rt�k" SortExpression="ColumnValue">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DefinicioTipus_Nev" HeaderText="Def. t�pus" SortExpression="DefinicioTipus_Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderSPSSzinkronizalt" runat="server" Text="Szinkroniz�lt" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            Checked='<%# (Eval("SPSSzinkronizalt") as string) == "1" ? true : false  %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                            <%-- /panelDetail --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
