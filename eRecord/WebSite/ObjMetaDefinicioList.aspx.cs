using System;
//using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class ObjMetaDefinicioList : Contentum.eUtility.UI.PageBase
{
    //private System.Drawing.Color colorReadOnly = System.Drawing.Color.LightGray;
    //private System.Drawing.Color colorReadOnlySelected = System.Drawing.Color.LightSteelBlue;

    private const string normalReadonlyRowStyle = "GridViewReadOnlyRowStyle";
    private const string selectedReadOnlyRowStyle = "GridViewReadOnlySelectedRowStyle";

    UI ui = new UI();

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";


    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }

    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        return !isCheckBoxChecked(gridView, rowIndex, "cbSPSSzinkronizalt");
    }

    protected bool getOrokolt(GridView gridView)
    {
        return getOrokolt(gridView, -1);
    }

    protected bool getOrokolt(GridView gridView, int rowIndex)
    {
        return isCheckBoxChecked(gridView, rowIndex, "cbOrokolt");
    }

    protected bool getAlarendelesMegengedett(GridView gridView)
    {
        return getAlarendelesMegengedett(gridView, -1);
    }

    protected bool getAlarendelesMegengedett(GridView gridView, int rowIndex)
    {
        return isCheckBoxChecked(gridView, rowIndex, "cbSubdefinitionEnabled");
    }

    private bool isCheckBoxChecked(GridView gridView, int rowIndex, string CheckBoxName)
    {
        bool bChecked = false;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            CheckBox cb = (CheckBox)selectedRow.FindControl(CheckBoxName);
            if (cb != null)
            {
                bChecked = cb.Checked;
            }
        }
        return bChecked;
    }

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjMetaDefinicioList");   
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ObjMetaDefinicioAlarendeltSubListHeader.RowCount_Changed += new EventHandler(ObjMetaDefinicioAlarendeltSubListHeader_RowCount_Changed);
        ObjMetaAdataiSubListHeader.RowCount_Changed += new EventHandler(ObjMetaAdataiSubListHeader_RowCount_Changed);

        ObjMetaDefinicioAlarendeltSubListHeader.ErvenyessegFilter_Changed += new EventHandler(ObjMetaDefinicioAlarendeltSubListHeader_ErvenyessegFilter_Changed);
        ObjMetaAdataiSubListHeader.ErvenyessegFilter_Changed += new EventHandler(ObjMetaAdataiSubListHeader_ErvenyessegFilter_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.ObjMetaDefinicioListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_Obj_MetaDefinicioSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel1.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ObjMetaDefinicioGridView.ClientID, null, "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(ObjMetaDefinicioGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(ObjMetaDefinicioGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(ObjMetaDefinicioGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(ObjMetaDefinicioGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = ObjMetaDefinicioGridView;

        ObjMetaDefinicioAlarendeltSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioAlarendeltSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioAlarendeltSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioAlarendeltSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ObjMetaDefinicioAlarendeltGridView.ClientID, null, "check");
        ObjMetaDefinicioAlarendeltSubListHeader.ButtonsClick += new CommandEventHandler(ObjMetaDefinicioAlarendeltSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        ObjMetaDefinicioAlarendeltSubListHeader.AttachedGridView = ObjMetaDefinicioAlarendeltGridView;

        ObjMetaAdataiSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaAdataiSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaAdataiSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ObjMetaAdataiSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ObjMetaAdataiGridView.ClientID, null, "check");
        ObjMetaAdataiSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(ObjMetaAdataiGridView.ClientID, "check", "cbInvalidateEnabled", true);

        ObjMetaAdataiSubListHeader.ButtonsClick += new CommandEventHandler(ObjMetaAdataiSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        ObjMetaAdataiSubListHeader.AttachedGridView = ObjMetaAdataiGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioAlarendeltGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaAdataiGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_Obj_MetaDefinicioSearch());

        if (!IsPostBack) ObjMetaDefinicioGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Lock);

        ObjMetaDefinicioAlarendeltPanel.Visible = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioList");
        ObjMetaAdataiPanel.Visible = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiList");

        ObjMetaDefinicioAlarendeltSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.New);
        ObjMetaDefinicioAlarendeltSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.View);
        ObjMetaDefinicioAlarendeltSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Modify);
        ObjMetaDefinicioAlarendeltSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Invalidate);

        ObjMetaAdataiSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdatai" + CommandName.New);
        ObjMetaAdataiSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdatai" + CommandName.View);
        ObjMetaAdataiSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdatai" + CommandName.Modify);
        ObjMetaAdataiSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdatai" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

    }



    #endregion

    #region Master List

    protected void ObjMetaDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ObjMetaDefinicioGridViewBind();
                    ObjMetaDefinicioGridView_SelectRowCommand(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
                    break;
            }
        }

    }

    protected void ObjMetaDefinicioGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ObjMetaDefinicioGridView", ViewState, "ContentType");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ObjMetaDefinicioGridView", ViewState);

        ObjMetaDefinicioGridViewBind(sortExpression, sortDirection);
    }

    protected void ObjMetaDefinicioGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(ObjMetaDefinicioGridView);

        EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_Obj_MetaDefinicioSearch search = (EREC_Obj_MetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_Obj_MetaDefinicioSearch());
        search.OrderBy = Search.GetOrderBy("ObjMetaDefinicioGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(ExecParam, search);

        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            UI.GridViewFill(ObjMetaDefinicioGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioGridView);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            ErrorUpdatePanel1.Update();
        }
    }

    protected void ObjMetaDefinicioGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void ObjMetaDefinicioGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ObjMetaDefinicioGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
            ActiveTabRefresh(TabContainer1, id);
        }
    }

    private void ObjMetaDefinicioGridView_SelectRowCommand(string ObjMetaDefinicioId)
    {
        string id = ObjMetaDefinicioId;
        if (!String.IsNullOrEmpty(id))
        {
            ActiveTabRefresh(TabContainer1, id);

            //RefreshOnClientClicksByMasterListSelectedRow(id);

        }
    }

    protected void ObjMetaDefinicioGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ObjMetaDefinicioGridView.PageIndex;

        ObjMetaDefinicioGridView.PageIndex = ListHeader1.PageIndex;
        string masterRowCount = "";
        if (Session[Constants.MasterRowCount] != null)
        {
            masterRowCount = Session[Constants.MasterRowCount].ToString();
        }

        if (prev_PageIndex != ObjMetaDefinicioGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ObjMetaDefinicioCPE);
            ObjMetaDefinicioGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, ObjMetaDefinicioCPE);
        }

        ListHeader1.PageCount = ObjMetaDefinicioGridView.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(ObjMetaDefinicioGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioGridView);
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ObjMetaDefinicioGridViewBind();
        ActiveTabClear();
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_Obj_MetaDefinicio";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, ObjMetaDefinicioUpdatePanel.ClientID);

            if (getAlarendelesMegengedett(ObjMetaDefinicioGridView))
            {
                ObjMetaDefinicioAlarendeltSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.NoSuccessorLoopWithId + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioAlarendeltUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

                ObjMetaDefinicioAlarendeltSubListHeader.NewEnabled = true;
            }
            else
            {
                ObjMetaDefinicioAlarendeltSubListHeader.NewEnabled = false;
            }

            ObjMetaAdataiSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.ObjMetaDefinicioId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaAdataiUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedObjMetaDefinicio();
            ObjMetaDefinicioGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedObjMetaDefinicioRecords();
                ObjMetaDefinicioGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedObjMetaDefinicioRecords();
                ObjMetaDefinicioGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedObjMetaDefiniciok();
                break;
        }
    }

    private void LockSelectedObjMetaDefinicioRecords()
    {
        LockManager.LockSelectedGridViewRecords(ObjMetaDefinicioGridView, "EREC_Obj_MetaDefinicio"
            , "ObjMetaDefinicioLock", "ObjMetaDefinicioForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel1);
    }

    private void UnlockSelectedObjMetaDefinicioRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(ObjMetaDefinicioGridView, "EREC_Obj_MetaDefinicio"
            , "ObjMetaDefinicioLock", "ObjMetaDefinicioForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel1);
    }

    /// <summary>
    /// T�rli a ObjMetaDefinicioGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedObjMetaDefinicio()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate"))
        {
            List<string[]> deletableItemsList = ui.GetGridViewSelectedRowsWithIndex(ObjMetaDefinicioGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(ObjMetaDefinicioGridView, Int32.Parse(deletableItemsList[i][1]));
                if (modosithato)
                {
                    execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                    execParams[i].Record_Id = deletableItemsList[i][0];
                }
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben az ObjMetaDefinicioGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedObjMetaDefiniciok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(ObjMetaDefinicioGridView, EErrorPanel1, ErrorUpdatePanel1), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_ObjMetaDefinicio");
        }
    }

     protected void ObjMetaDefinicioGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ObjMetaDefinicioGridViewBind(e.SortExpression, UI.GetSortToGridView("ObjMetaDefinicioGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

     //protected void SetMasterRowCount(string RowCount)
     //{
     //    Session[Constants.MasterRowCount] = RowCount;

     //    ListHeader1.RowCount = RowCount;
     //}

    #endregion


    #region Detail Tab

     // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
     protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
     {
         if (TabContainer1.ActiveTab.Equals(tab))
         {
             ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
             ActiveTabRefreshOnClientClicks();
         }
     }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (ObjMetaDefinicioGridView.SelectedIndex == -1)
        {
            return;
        }
        string ObjMetaDefinicioId = UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, ObjMetaDefinicioId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string ObjMetaDefinicioId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                ObjMetaAdataiGridViewBind(ObjMetaDefinicioId);
                ObjMetaAdataiPanel.Visible = true;
                break;
            case 1:
                ObjMetaDefinicioAlarendeltGridViewBind(ObjMetaDefinicioId);
                ObjMetaDefinicioAlarendeltPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(ObjMetaAdataiGridView);
                break;
            case 1:
                ui.GridViewClear(ObjMetaDefinicioAlarendeltGridView);
                break;
        }
    }

    protected void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        ObjMetaDefinicioAlarendeltSubListHeader.RowCount = RowCount;
        ObjMetaAdataiSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(ObjMetaDefinicioAlarendeltTabPanel))
        {
            ObjMetaDefinicioAlarendeltGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioAlarendeltGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(ObjMetaAdataiTabPanel))
        {
            ObjMetaAdataiGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(ObjMetaAdataiGridView));
        }
    }

    #endregion

    #region ObjMetaDefinicioAlarendelt Detail

    private void ObjMetaDefinicioAlarendeltSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelected_ObjMetaDefinicioAlarendelt();
            // csak az al�rendel�si kapcsolatot �rv�nytelen�tj�k, nem mag�t a metadefin�ci�t
            removeSelected_ObjMetaDefinicioAlarendelt_Felettes_Obj_Meta();

            ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
        }
    }

    protected void ObjMetaDefinicioAlarendeltGridViewBind(string ObjMetaDefinicioId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ObjMetaDefinicioAlarendeltGridView", ViewState, "ContentType");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ObjMetaDefinicioAlarendeltGridView", ViewState);

        ObjMetaDefinicioAlarendeltGridViewBind(ObjMetaDefinicioId, sortExpression, sortDirection);
    }

    protected void ObjMetaDefinicioAlarendeltGridViewBind(string ObjMetaDefinicioId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(ObjMetaDefinicioId))
        {
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();
            search.Felettes_Obj_Meta.Value = ObjMetaDefinicioId;
            search.Felettes_Obj_Meta.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("ObjMetaDefinicioAlarendeltGridView", ViewState, SortExpression, SortDirection);

            ObjMetaDefinicioAlarendeltSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                if (headerObjMetaDefinicioAlarendelt.Text.IndexOf(" (") > 0)
                    headerObjMetaDefinicioAlarendelt.Text = headerObjMetaDefinicioAlarendelt.Text.Remove(headerObjMetaDefinicioAlarendelt.Text.IndexOf(" ("));
                headerObjMetaDefinicioAlarendelt.Text += " (" + res.Ds.Tables[0].Rows.Count.ToString() + ")";

                UI.GridViewFill(ObjMetaDefinicioAlarendeltGridView, res, ObjMetaDefinicioAlarendeltSubListHeader, EErrorPanel1, ErrorUpdatePanel1);

                ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioAlarendeltGridView);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            ui.GridViewClear(ObjMetaDefinicioAlarendeltGridView);
        }
    }


    void ObjMetaDefinicioAlarendeltSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
    }

    protected void ObjMetaDefinicioAlarendeltUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(ObjMetaDefinicioAlarendeltTabPanel))
                    //{
                    //    ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
                    //}
                    ActiveTabRefreshDetailList(ObjMetaDefinicioAlarendeltTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a ObjMetaDefinicioAlarendeltGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_ObjMetaDefinicioAlarendelt()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ObjMetaDefinicioAlarendeltGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// T�rli a ObjMetaDefinicioAlarendeltGridView -ban kijel�lt elemekb�l a felettes objektum metadefin�ci� �rt�k�t
    /// </summary>
    private void removeSelected_ObjMetaDefinicioAlarendelt_Felettes_Obj_Meta()
    {
        // TODO: levinni webservice-szintre
        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioModify"))
        {
            bool isThereAnError = false;
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ObjMetaDefinicioAlarendeltGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            for (int i = 0; i < deletableItemsList.Count && !isThereAnError; i++)
            {
                execParam.Record_Id = deletableItemsList[i];
                Result result_get = service.Get(execParam);
                if (!String.IsNullOrEmpty(result_get.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_get);
                    ErrorUpdatePanel1.Update();
                    isThereAnError = true;
                }
                else
                {
                    EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = (EREC_Obj_MetaDefinicio)result_get.Record;

                    if (erec_Obj_MetaDefinicio != null)
                    {
                        erec_Obj_MetaDefinicio.Felettes_Obj_Meta = "";
                        erec_Obj_MetaDefinicio.Updated.Felettes_Obj_Meta = true;
                        Result result = service.Update(execParam, erec_Obj_MetaDefinicio);

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();
                            isThereAnError = true;
                        }
                    }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void ObjMetaDefinicioAlarendeltGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ObjMetaDefinicioAlarendeltGridView, selectedRowNumber, "check");
        }
    }

    private void ObjMetaDefinicioAlarendeltGridView_RefreshOnClientClicks(string ObjMetaDefinicioId)
    {
        string id = ObjMetaDefinicioId;

        if (!String.IsNullOrEmpty(id))
        {
            ObjMetaDefinicioAlarendeltSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioAlarendeltUpdatePanel.ClientID);
            ObjMetaDefinicioAlarendeltSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioAlarendeltUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void ObjMetaDefinicioAlarendeltGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ObjMetaDefinicioAlarendeltGridView.PageIndex;

        ObjMetaDefinicioAlarendeltGridView.PageIndex = ObjMetaDefinicioAlarendeltSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != ObjMetaDefinicioAlarendeltGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ObjMetaDefinicioAlarendeltCPE);
            ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
        }
        else
        {
            UI.GridViewSetScrollable(ObjMetaDefinicioAlarendeltSubListHeader.Scrollable, ObjMetaDefinicioAlarendeltCPE);
        }
        ObjMetaDefinicioAlarendeltSubListHeader.PageCount = ObjMetaDefinicioAlarendeltGridView.PageCount;
        ObjMetaDefinicioAlarendeltSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(ObjMetaDefinicioAlarendeltGridView);
    }


    void ObjMetaDefinicioAlarendeltSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(ObjMetaDefinicioAlarendeltSubListHeader.RowCount);
        ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
    }

    protected void ObjMetaDefinicioAlarendeltGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ObjMetaDefinicioAlarendeltGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView)
            , e.SortExpression, UI.GetSortToGridView("ObjMetaDefinicioAlarendeltGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region ObjMetaAdatai Detail

    protected void ObjMetaAdataiGridViewBind(String ObjMetaDefinicioId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ObjMetaAdataiGridView", ViewState, "Sorszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ObjMetaAdataiGridView", ViewState);

        ObjMetaAdataiGridViewBind(ObjMetaDefinicioId, sortExpression, sortDirection);

    }

    protected void ObjMetaAdataiGridViewBind(String ObjMetaDefinicioId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(ObjMetaDefinicioId))
        {
            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();
            //search.Obj_MetaDefinicio_Id.Value = ObjMetaDefinicioId;
            //search.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("ObjMetaAdataiGridView", ViewState, SortExpression, SortDirection);

            ObjMetaAdataiSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            //Result res = service.GetAllWithExtension(ExecParam, search);

            // �r�k�lt hozz�rendel�sek is
            Result res = service.GetAllWithExtensionByObjMetaDefinicio(ExecParam, search, ObjMetaDefinicioId, false);


            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                if (headerObjMetaAdatai.Text.IndexOf(" (") > 0)
                    headerObjMetaAdatai.Text = headerObjMetaAdatai.Text.Remove(headerObjMetaAdatai.Text.IndexOf(" ("));
                headerObjMetaAdatai.Text += " (" + res.Ds.Tables[0].Rows.Count.ToString() + ")";

                UI.GridViewFill(ObjMetaAdataiGridView, res, ObjMetaAdataiSubListHeader, EErrorPanel1, ErrorUpdatePanel1);

                ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaAdataiGridView);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            ui.GridViewClear(ObjMetaAdataiGridView);
        }

    }

    //SubListHeader f�ggv�nyei(4)
    private void ObjMetaAdataiSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_ObjMetaAdatai();
            ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
        }
    }

    private void deleteSelected_ObjMetaAdatai()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiInvalidate"))
        {
            List<string[]> deletableItemsList = ui.GetGridViewSelectedRowsWithIndex(ObjMetaAdataiGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            List<ExecParam> execParams = new List<ExecParam>();

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(ObjMetaAdataiGridView, Int32.Parse(deletableItemsList[i][1]));
                bool orokolt = getOrokolt(ObjMetaAdataiGridView, Int32.Parse(deletableItemsList[i][1]));
                if (modosithato && !orokolt)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i][0];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void ObjMetaAdataiSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
    }

    private void ObjMetaAdataiSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(ObjMetaAdataiSubListHeader.RowCount);
        ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
    }

    //GridView esem�nykezel�i(3-4)
    ////specialis, nem mindig kell kell
    protected void ObjMetaAdataiGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        // read only sorok sz�nez�se
        GridView gv = (GridView)sender;
        if (gv == null) return;

        if (gv.SelectedIndex > -1 && (!getModosithatosag(gv, gv.SelectedIndex) || getOrokolt(gv, gv.SelectedIndex)))
        {
            gv.SelectedRow.ControlStyle.CssClass = normalReadonlyRowStyle;
        }

        if (!getModosithatosag(gv, e.NewSelectedIndex) || getOrokolt(gv, e.NewSelectedIndex))
        {
            gv.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedReadOnlyRowStyle;
        }
    }

    protected void ObjMetaAdataiGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cb_System", "System");

        bool bReadOnly = false; // true, ha az adott sor nem m�dos�that�, mert szinkroniz�lt vagy �r�k�lt

        CheckBox cbOrokolt = (CheckBox)e.Row.FindControl("cbOrokolt");
        Label lbSzint = (Label)e.Row.FindControl("labelSzint");
        Label lbObjMetaAdatContentType = (Label)e.Row.FindControl("labelObjMetaAdatContentType");
        if (cbOrokolt != null && lbSzint != null)
        {
            if(lbSzint.Text != "0")
            {
                cbOrokolt.Checked = true;
                bReadOnly = true;
                if (lbObjMetaAdatContentType != null)
                {
                    lbObjMetaAdatContentType.Visible = true;
                }
            }
        }

        CheckBox cbInvalidateEnabled = (CheckBox)e.Row.FindControl("cbInvalidateEnabled");
        CheckBox cbSPSSzinkronizalt = (CheckBox)e.Row.FindControl("cbSPSSzinkronizalt");
        if (cbSPSSzinkronizalt != null && cbOrokolt != null && cbInvalidateEnabled != null)
        {
            if (cbSPSSzinkronizalt.Checked || cbOrokolt.Checked)
            {
                cbInvalidateEnabled.Checked = false;
                bReadOnly = true;
            }
        }

        if (bReadOnly)
        {
            e.Row.ControlStyle.CssClass = normalReadonlyRowStyle;
        }

    }

    protected void ObjMetaAdataiGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ObjMetaAdataiGridView, selectedRowNumber, "check");
        }
    }

    protected void ObjMetaAdataiGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ObjMetaAdataiGridView.PageIndex;

        ObjMetaAdataiGridView.PageIndex = ObjMetaAdataiSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != ObjMetaAdataiGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ObjMetaAdataiCPE);
            ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
        }
        else
        {
            UI.GridViewSetScrollable(ObjMetaAdataiSubListHeader.Scrollable, ObjMetaAdataiCPE);
        }
        ObjMetaAdataiSubListHeader.PageCount = ObjMetaAdataiGridView.PageCount;
        ObjMetaAdataiSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(ObjMetaAdataiGridView);


        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaAdataiGridView);

    }

    protected void ObjMetaAdataiGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView)
            , e.SortExpression, UI.GetSortToGridView("ObjMetaAdataiGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void ObjMetaAdataiUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(ObjMetaAdataiTabPanel))
                    //{
                    //    ObjMetaAdataiGridViewBind(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
                    //}
                    ActiveTabRefreshDetailList(ObjMetaAdataiTabPanel);
                    break;
            }
        }
    }

    //Gombok kliens oldali szkripjeinek friss�t�se(1)
    private void ObjMetaAdataiGridView_RefreshOnClientClicks(string ObjMetaDefinicioId)
    {
        string id = ObjMetaDefinicioId;
        
        if (!String.IsNullOrEmpty(id))
        {

            ObjMetaAdataiSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.ObjMetaDefinicioId + "=" + ObjMetaDefinicioId
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaAdataiUpdatePanel.ClientID);

            if (getModosithatosag(ObjMetaAdataiGridView) && !getOrokolt(ObjMetaAdataiGridView))
            {
                ObjMetaAdataiSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                        + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                        , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaAdataiUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                ObjMetaAdataiSubListHeader.ModifyOnClientClick = jsNemModosithato;
            }
        }
    }

    #endregion

}
