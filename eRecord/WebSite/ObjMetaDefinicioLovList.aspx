<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"  EnableEventValidation="True"
CodeFile="ObjMetaDefinicioLovList.aspx.cs" Inherits="ObjMetaDefinicioLovList" Title="<%$Resources:LovList,ObjMetaDefinicioLovListHeaderTitle%>" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,ObjMetaDefinicioLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <BR />
                            <TABLE style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0>
                                <TBODY>
                                    <TR id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <TD>
                                            <asp:Label id="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>">
                                            </asp:Label>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD>
                                            <TABLE>
                                                <TBODY>
                                                    <TR class="urlapSor">
                                                        <TD class="mrUrlapMezo">
                                                            <asp:TextBox id="ObjMetaDefinicio_TextBoxSearch" runat="server">*</asp:TextBox>
                                                        </TD>
                                                        <TD class="mrUrlapCaption">
                                                            <asp:Label id="labelContentType" runat="server" Text="ContentType:" Font-Bold="True"></asp:Label>
                                                        </TD>
                                                        <TD class="mrUrlapMezo">
                                                            <asp:TextBox id="TextBoxContentType" runat="server" />
                                                        </TD>
                                                    </TR>
                                                    <TR class="urlapSor">
                                                        <TD colSpan="4">
                                                            <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s"></asp:ImageButton>
                                                            <asp:ImageButton id="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                        </TD>
                                                    </TR>
                                                </TBODY>
                                            </TABLE>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="PADDING-BOTTOM: 5px; VERTICAL-ALIGN: top; PADDING-TOP: 5px; TEXT-ALIGN: left">
                                            <DIV class="listaFulFelsoCsikKicsi">
                                                <IMG alt="" src="images/hu/design/spacertrans.gif" />
                                            </DIV>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                            <ajaxToolkit:CollapsiblePanelExtender id="ObjMetaDefinicioCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel id="Panel1" runat="server">
                                                <TABLE style="WIDTH: 98%" cellSpacing=0 cellPadding=0>
                                                    <TBODY>
                                                        <TR>
                                                            <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left" class="GridViewHeaderBackGroundStyle">
                                                            <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField id="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField id="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView id="GridViewSearchResult" runat="server"
                                                                    CssClass="GridViewLovListStyle" CellPadding="0" BorderWidth="1px"
                                                                    AllowPaging="True" PagerSettings-Visible="false" AutoGenerateColumns="False"
                                                                    DataKeyNames="Id"
                                                                    OnRowDataBound="GridView_RowDataBound">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ContentType" SortExpression="ContentType" HeaderText="ContentType">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>                                                                          
                                                                        <asp:BoundField DataField="ObjTipusok_Tabla" SortExpression="ObjTipusok_Tabla" HeaderText="T�bla">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ObjTipusok_Oszlop" SortExpression="ObjTipusok_Oszlop" HeaderText="Oszlop">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ColumnValue_Lekepezett" SortExpression="ColumnValue" HeaderText="Oszlop�rt�k">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Felettes_Obj_Meta_CTT" SortExpression="Felettes_Obj_Meta_CTT" HeaderText="Felettes">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SPSSzinkronizalt" SortExpression="SPSSzinkronizalt" HeaderText="Szinkroniz�lt">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="labelSPSSzinkronizalt" Text="SPS-szinkroniz�lt" runat="server" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" AutoPostBack="false" Text='<%# Eval("SPSSzinkronizalt") %>' Checked='<%# (Eval("SPSSzinkronizalt") as string) == "1" ? true : false %>'   Enabled="false" CssClass="HideCheckBoxText" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DefinicioTipus" SortExpression="DefinicioTipus">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>

                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>

                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>

                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </TD>
                                                        </TR>
                                                    </TBODY>
                                                </TABLE>
                                            </asp:Panel>
                                            <%--
                                            <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                            </asp:ListBox>
                                            --%>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                            &nbsp; <BR />
                                            <asp:ImageButton id="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </TD>
                                    </TR>
                                </TBODY>
                            </TABLE>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>

