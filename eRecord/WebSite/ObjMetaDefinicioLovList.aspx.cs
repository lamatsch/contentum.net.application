
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class ObjMetaDefinicioLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjMetaDefinicioList");        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
                
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("ObjMetaDefinicioForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioSearch.aspx", ""
            , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, ObjMetaDefinicioCPE,15);
    }



    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, ObjMetaDefinicioCPE);
        FillGridViewSearchResult(false);
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
        EREC_Obj_MetaDefinicioSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_Obj_MetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_Obj_MetaDefinicioSearch());
        }
        else
        {
            search = new EREC_Obj_MetaDefinicioSearch();

            if (!String.IsNullOrEmpty(ObjMetaDefinicio_TextBoxSearch.Text))
            {
                search.ContentType.Value = ObjMetaDefinicio_TextBoxSearch.Text;
                search.ContentType.Operator = Search.GetOperatorByLikeCharater(ObjMetaDefinicio_TextBoxSearch.Text);                
            }

        }

        search.OrderBy = "EREC_Obj_MetaDefinicio.ContentType";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        String NoAncestorLoopWithId = Request.QueryString.Get(QueryStringVars.NoAncestorLoopWithId);
        String NoSuccessorLoopWithId = Request.QueryString.Get(QueryStringVars.NoSuccessorLoopWithId);
        String definiciotipus = Request.QueryString.Get(QueryStringVars.DefinicioTipus);
        if (!String.IsNullOrEmpty(definiciotipus))
        {
            search.DefinicioTipus.Value = definiciotipus;
            search.DefinicioTipus.Operator = Query.Operators.equals;
        }

        Result result = service.GetAllWithExtensionNoLoop(ExecParam, search, NoAncestorLoopWithId, NoSuccessorLoopWithId);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        
     }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string ContentType = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string SPSSzinkronizaltValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 7);
                    string DefinicioTipusValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 9);

                    string selectedText = ContentType;

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                    String DefinicioTipusHiddenFieldId = Request.QueryString.Get(QueryStringVars.DefinicioTipusHiddenFieldId);
                    if (!String.IsNullOrEmpty(DefinicioTipusHiddenFieldId))
                    {
                        Dictionary<string, string> dictionaryTipus = new Dictionary<string, string>();
                        dictionaryTipus.Add(DefinicioTipusHiddenFieldId, DefinicioTipusValue);

                        String SPSSzinkronizaltHiddenFieldId = Request.QueryString.Get(QueryStringVars.SPSSzinkronizaltHiddenFieldId);
                        if (!String.IsNullOrEmpty(SPSSzinkronizaltHiddenFieldId))
                        {
                            dictionaryTipus.Add(SPSSzinkronizaltHiddenFieldId, SPSSzinkronizaltValue);
                        }

                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, dictionaryTipus, "ReturnValuesToParentWindowTipus", false);
                    }

                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    

    

    
    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }


}
