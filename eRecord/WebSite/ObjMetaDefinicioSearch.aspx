<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" CodeFile="ObjMetaDefinicioSearch.aspx.cs"
    Inherits="ObjMetaDefinicioSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="kddl" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="eRecordComponent/ObjMetaDefinicioTextBox.ascx" TagName="ObjMetaDefinicioTextBox"
    TagPrefix="omdtb" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
    
    
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                                
		                        <tr class="urlapSor">
			                        <td class="mrUrlapCaption">
				                        <asp:Label ID="labelContentType" runat="server" Text="ContentType:"></asp:Label></td>
			                        <td class="mrUrlapMezo">
				                        <uc8:RequiredTextBox ID="requiredTextBoxContentType" Validate="false" runat="server" />
			                        </td>
		                        </tr>
		                        
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDefinicioTipus" runat="server" Text="Defin�ci� t�pus:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <kddl:KodtarakDropDownList ID="KodtarakDropDownList_DefinicioTipus" runat="server" />
                                    </td>
                                </tr>
                                
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelObjTipusokTabla" runat="server" Text="Objektum t�pus (t�bla):"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_Tabla" Validate="false" runat="server" />
                                    </td>
                                </tr>
                                
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelObjTipusokOszlop" runat="server" Text="Objektum t�pus (oszlop):"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_Oszlop" Validate="false" runat="server" />
                                    </td>
                                </tr>                            
                                                                                               
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelColumnValue" runat="server" Text="Oszlop�rt�k:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBoxColumnValue" runat="server" Text="" />
                                    </td>
                                </tr>                                
                                   	                                	                    
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                     </td>
                                    <td class="mrUrlapMezo">
                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Text="SPSSzinkronizalt" Enabled="false" />
                                    </td>
                                </tr>
                                
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFelettesObjId" runat="server" Text="Felettes objektum metadefin�ci�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                       <%-- <asp:TextBox ID="TextBoxFelettesObjId" runat="server" Text="" />--%>
                                       <omdtb:ObjMetaDefinicioTextBox ID="ObjMetaDefinicioTextBoxFelettes" CssClass="mrUrlapInputSzeles"
                                        runat="server" Text="" />
                                    </td>
                                </tr>

		                <tr class="urlapSor">
			                  <td class="mrUrlapCaption">
			                  </td>
			                  <td class="mrUrlapMezo">
			                      <asp:RadioButtonList ID="rblSPSSzinkronizalt" runat="server" >
				                        <asp:ListItem  Value="1" Selected="false" Text="Szinkroniz�lt" />
				                        <asp:ListItem Value="0" Selected="false" Text="Nem szinkroniz�lt" />
				                        <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
				                    </asp:RadioButtonList>
			                  </td>
		                </tr>		                    		                                                
                        <tr class="urlapSor">
                            <td colspan="2" >
                                <uc9:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc9:Ervenyesseg_SearchFormComponent>
                                &nbsp;</td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                &nbsp; &nbsp;
                    
                    &nbsp; &nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
