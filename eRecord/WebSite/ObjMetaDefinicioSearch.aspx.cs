using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ObjMetaDefinicioSearch : Contentum.eUtility.UI.PageBase
{
    private const string kcs_ObjMetaDefinicio_Tipus = "OBJMETADEFINICIO_TIPUS";

    private Type _type = typeof(EREC_Obj_MetaDefinicioSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.ObjMetaDefinicioSearchHeaderTitle;
       
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_Obj_MetaDefinicioSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_Obj_MetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_Obj_MetaDefinicioSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

        ObjTipusokTextBox_Tabla.Filter = Constants.FilterType.ObjTipusok.Tabla;
        ObjTipusokTextBox_Oszlop.Filter = Constants.FilterType.ObjTipusok.Oszlop;
        requiredTextBoxContentType.Validate = false;

        ObjMetaDefinicioTextBoxFelettes.CustomValueEnabled = false;
        ObjMetaDefinicioTextBoxFelettes.RefreshCallingWindow = false;
        ObjMetaDefinicioTextBoxFelettes.DefinicioTipusFilter = KodTarak.OBJMETADEFINICIO_TIPUS.A0;

        ObjMetaDefinicioTextBoxFelettes.Validate = false;
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_Obj_MetaDefinicioSearch erec_Obj_MetaDefinicioSearch = null;
        if (searchObject != null) erec_Obj_MetaDefinicioSearch = (EREC_Obj_MetaDefinicioSearch)searchObject;

        if (erec_Obj_MetaDefinicioSearch != null)
        {
            requiredTextBoxContentType.Text = erec_Obj_MetaDefinicioSearch.ContentType.Value;
            KodtarakDropDownList_DefinicioTipus.FillAndSetSelectedValue(kcs_ObjMetaDefinicio_Tipus, erec_Obj_MetaDefinicioSearch.DefinicioTipus.Value, true, SearchHeader1.ErrorPanel);
            ObjTipusokTextBox_Tabla.Id_HiddenField = erec_Obj_MetaDefinicioSearch.Objtip_Id.Value;
            ObjTipusokTextBox_Tabla.SetObjTipusokTextBoxById(SearchHeader1.ErrorPanel);
            if (!String.IsNullOrEmpty(erec_Obj_MetaDefinicioSearch.Objtip_Id_Column.Value))
            {
                ObjTipusokTextBox_Oszlop.Id_HiddenField = erec_Obj_MetaDefinicioSearch.Objtip_Id_Column.Value;
                ObjTipusokTextBox_Oszlop.SetObjTipusokTextBoxById(SearchHeader1.ErrorPanel);
            }
            TextBoxColumnValue.Text = erec_Obj_MetaDefinicioSearch.ColumnValue.Value;

            ObjMetaDefinicioTextBoxFelettes.Id_HiddenField = erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Value;
            ObjMetaDefinicioTextBoxFelettes.SetTextBoxById(SearchHeader1.ErrorPanel);

            switch (erec_Obj_MetaDefinicioSearch.SPSSzinkronizalt.Value)
            {
                case "0":
                    rblSPSSzinkronizalt.SelectedValue = "0";
                    break;
                case "1":
                    rblSPSSzinkronizalt.SelectedValue = "1";
                    break;
                default:
                    rblSPSSzinkronizalt.SelectedValue = "NotSet";
                    break;
            }

            Ervenyesseg_SearchFormComponent1.SetDefault(
                erec_Obj_MetaDefinicioSearch.ErvKezd, erec_Obj_MetaDefinicioSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_Obj_MetaDefinicioSearch SetSearchObjectFromComponents()
    {
        EREC_Obj_MetaDefinicioSearch erec_Obj_MetaDefinicioSearch = (EREC_Obj_MetaDefinicioSearch)SearchHeader1.TemplateObject;
        if (erec_Obj_MetaDefinicioSearch == null)
        {
            erec_Obj_MetaDefinicioSearch = new EREC_Obj_MetaDefinicioSearch();
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_DefinicioTipus.SelectedValue))
        {
            erec_Obj_MetaDefinicioSearch.DefinicioTipus.Value = KodtarakDropDownList_DefinicioTipus.SelectedValue;
            erec_Obj_MetaDefinicioSearch.DefinicioTipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ObjTipusokTextBox_Tabla.Id_HiddenField))
        {
            erec_Obj_MetaDefinicioSearch.Objtip_Id.Value = ObjTipusokTextBox_Tabla.Id_HiddenField;
            erec_Obj_MetaDefinicioSearch.Objtip_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ObjTipusokTextBox_Oszlop.Id_HiddenField))
        {
            erec_Obj_MetaDefinicioSearch.Objtip_Id_Column.Value = ObjTipusokTextBox_Oszlop.Id_HiddenField;
            erec_Obj_MetaDefinicioSearch.Objtip_Id_Column.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(TextBoxColumnValue.Text))
        {
            erec_Obj_MetaDefinicioSearch.ColumnValue.Value = TextBoxColumnValue.Text;
            erec_Obj_MetaDefinicioSearch.ColumnValue.Operator = Search.GetOperatorByLikeCharater(TextBoxColumnValue.Text);
        }

        //if (!String.IsNullOrEmpty(TextBoxFelettesObjId.Text))
        //{
        //    erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Value = TextBoxFelettesObjId.Text;
        //    erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Operator = Query.Operators.equals;
        //}

        if (!String.IsNullOrEmpty(ObjMetaDefinicioTextBoxFelettes.Id_HiddenField))
        {
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Value = ObjMetaDefinicioTextBoxFelettes.Id_HiddenField;
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(requiredTextBoxContentType.Text))
        {
            erec_Obj_MetaDefinicioSearch.ContentType.Value = requiredTextBoxContentType.Text;
            erec_Obj_MetaDefinicioSearch.ContentType.Operator = Search.GetOperatorByLikeCharater(requiredTextBoxContentType.Text);
        }

        switch (rblSPSSzinkronizalt.SelectedValue)
        {
            case "0":
                erec_Obj_MetaDefinicioSearch.SPSSzinkronizalt.Value = "0";
                erec_Obj_MetaDefinicioSearch.SPSSzinkronizalt.Operator = Query.Operators.equals;
                break;
            case "1":
                erec_Obj_MetaDefinicioSearch.SPSSzinkronizalt.Value = "1";
                erec_Obj_MetaDefinicioSearch.SPSSzinkronizalt.Operator = Query.Operators.equals;
                break;
            case "NotSet":
            default:
                break;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            erec_Obj_MetaDefinicioSearch.ErvKezd, erec_Obj_MetaDefinicioSearch.ErvVege);        
              
        return erec_Obj_MetaDefinicioSearch;
    }

  

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_Obj_MetaDefinicioSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_Obj_MetaDefinicioSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_Obj_MetaDefinicioSearch();
    }

}
