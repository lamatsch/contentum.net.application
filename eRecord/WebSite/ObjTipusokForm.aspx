<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ObjTipusokForm.aspx.cs" Inherits="ObjTipusokForm" Title="Objektum t�pusok" %>


    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
    
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
    
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>


<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKodStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelKod" runat="server" Text="Objektum t�pus k�dja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredTextBox ID="requiredTextBoxKod" runat="server" />
                                </td>
                        </tr>                    
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNevStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelNev" runat="server" Text="Objektum t�pus neve:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:ObjTipusokTextBox ID="TipusObjTipusokTextBox" runat="server" />
                            </td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSzulo" runat="server" Text="Sz�l� t�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:ObjTipusokTextBox ID="SzuloObjTipusokTextBox" runat="server" />
                            </td>
                        </tr>
                        <%-- TODO: eRecordban nem l�tezik a LovList, Form, Search - egyel�re elt�ntetj�k --%>
                        <tr class="urlapSor" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKodcsoport" runat="server" Text="K�dcsoport:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodcsoportokTextBox ID="KodcsoportokTextBox1" runat="server" />
                            </td>
                        </tr>                         

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="labelErvenyessegStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>                           
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

