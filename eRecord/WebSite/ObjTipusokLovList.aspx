<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" EnableEventValidation="True"
    CodeFile="ObjTipusokLovList.aspx.cs" Inherits="ObjTipusokLovList" Title="<%$Resources:LovList,ObjTipusokLovListHeaderTitle%>" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,ObjTipusokLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr id="tr_keresomezok" class="urlapSor" runat="server">
                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelKod" Text="K�d:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="TextBoxKod" runat="server" CssClass="mrUrlapInput" />
                                                        </td>

                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelNev" Text="N�v:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="TextBoxNev" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_TipusOszlop" class="urlapSor" runat="server">
                                                        <td class="mrUrlapCaption">&nbsp;
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:CheckBox ID="cbTipusOszlop" Checked="false" runat="server" Text="Csak t�bl�k oszlop-defin�ci�val" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="4">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s"></asp:ImageButton>
                                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="ObjTipusokCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table style="width: 98%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    CellPadding="0" BorderWidth="1px" AllowPaging="True"
                                                                    PagerSettings-Visible="false" AutoGenerateColumns="False" DataKeyNames="Id"
                                                                    OnRowDataBound="GridView_RowDataBound">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="Kod" SortExpression="Kod" HeaderText="K�d">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Nev" SortExpression="Nev" HeaderText="N�v">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Obj_Id_Szulo" SortExpression="Obj_Id_Szulo">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="KodCsoport_Id" SortExpression="KodCsoport_Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <%--
					                                                    <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelKodNev" Text="Objektum t�pus" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:Label ID="itemKodNev" runat="server" Text='<%# string.Format("{0} ({1})", Eval("Kod"), Eval("Nev")) %>' />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>                                                                        
                                                                        --%>
                                                                        <asp:BoundField DataField="TipusObjTipusKod" SortExpression="TipusObjTipusKod" HeaderText="T�pus k�d">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <%-- 
                                                                        <asp:BoundField DataField="TipusObjTipusNev" SortExpression="TipusObjTipusNev" HeaderText="T�pus n�v">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>                                                                         
                                                                        --%>
                                                                        <%--
					                                                    <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelObjTipus" Text="T�pus" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:Label ID="itemObTipus" runat="server" Text='<%# string.Format("{0} ({1})", Eval("TipusObjTipusKod"), Eval("TipusObjTipusNev")) %>' />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>
                                                                        --%>

                                                                        <asp:BoundField DataField="SzuloObjTipusKod" SortExpression="SzuloObjTipusKod" HeaderText="Sz�l� k�d">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <%--
                                                                        <asp:BoundField DataField="SzuloObjTipusNev" SortExpression="SzuloObjTipusNev" HeaderText="Sz�l� n�v">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>                                                                                                                                                
                                                                        --%>
                                                                        <%--
					                                                    <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelSzulo" Text="Sz�l�" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:Label ID="itemSzulo" runat="server" Text='<%# string.Format("{0} ({1})", Eval("SzuloObjTipusKod"), Eval("SzuloObjTipusNev")) %>' />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>
                                                                        --%>
                                                                        <asp:TemplateField Visible="false">
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="labelOszlopDefinicioLetezik" Text="Oszlop defini�lt" runat="server" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="cbOszlopDefinicioLetezik" runat="server" AutoPostBack="false" Text='<%# Eval("OszlopDefinicioLetezik") %>' Enabled="false" CssClass="HideCheckBoxText" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>

                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>

                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>

                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                            <%--
                                            <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                            </asp:ListBox>
                                            --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">&nbsp;
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>

