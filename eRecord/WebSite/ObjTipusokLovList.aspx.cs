
using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class ObjTipusokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string Filter = String.Empty;
    private bool UgyiratHierarchia = false; // ha igaz, csak az EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok �s EREC_IraIratok t�bla ill. ezek oszlopai

    #region Utils
    private String GetUgyiratHierarchiaObjTipusIds()
    {
        // TODO: cache-b�l?
        // objektum Id-k lek�r�se
        KRT_ObjTipusokService service_ot = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
        KRT_ObjTipusokSearch search_ot = new KRT_ObjTipusokSearch();
        ExecParam execParam_ot = UI.SetExecParamDefault(Page, new ExecParam());
        search_ot.Kod.Value = "'" + Constants.TableNames.EREC_UgyUgyiratok + "','" + Constants.TableNames.EREC_UgyUgyiratdarabok + "','" + Constants.TableNames.EREC_IraIratok + "'";
        search_ot.Kod.Operator = Query.Operators.inner;
        Result result_ot = service_ot.GetAllWithExtension(execParam_ot, search_ot, Constants.FilterType.ObjTipusok.Tabla);

        List<string> listIds = new List<string>();
        if (String.IsNullOrEmpty(result_ot.ErrorCode))
        {
            foreach (System.Data.DataRow row in result_ot.Ds.Tables[0].Rows)
            {
                switch (row["Kod"].ToString())
                {
                    case Constants.TableNames.EREC_UgyUgyiratok:
                    case Constants.TableNames.EREC_UgyUgyiratdarabok:
                    case Constants.TableNames.EREC_IraIratok:
                        if (!String.IsNullOrEmpty(row["Id"].ToString()))
                        {
                            listIds.Add("'" + row["Id"].ToString() + "'");
                        }
                        break;
                }
            }
        }

        String IdFilter = String.Empty;
        if (listIds.Count > 0)
        {
            IdFilter = String.Join(",", listIds.ToArray());
        }

        return IdFilter;
    }
    #endregion Utils

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjTipusokList");        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
        
        String Mode = Request.QueryString.Get(QueryStringVars.Mode);
        // van-e sz�r�s �gyirat hierarchi�ra
        UgyiratHierarchia = (Mode == Contentum.eUtility.Constants.UgyiratHierarchia ? true : false);

        // van-e sz�r�s az objektum t�pusra
        Filter = Request.QueryString.Get(QueryStringVars.Filter);
        if (!String.IsNullOrEmpty(Filter))
        {
            if (Filter == Constants.FilterType.ObjTipusok.Tabla)
            {          
                LovListHeader1.HeaderTitle = Resources.LovList.ObjTipusokLovListHeaderTitle_Filtered_Tabla;
            }
            else if (Filter == Constants.FilterType.ObjTipusok.Oszlop)
            {          
                LovListHeader1.HeaderTitle = Resources.LovList.ObjTipusokLovListHeaderTitle_Filtered_Oszlop;
            }
            else if (Filter == Constants.FilterType.ObjTipusok.All)
            {         
                LovListHeader1.HeaderTitle = Resources.LovList.ObjTipusokLovListHeaderTitle_Filtered_All;
            }            
        }            
                
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("ObjTipusokForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("ObjTipusokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, ObjTipusokCPE,15);
        if (Filter == Constants.FilterType.ObjTipusok.Oszlop)
        {
            tr_TipusOszlop.Visible = false; // nincs �rtelme, hogy l�tsszon
        }

        if (Filter != Constants.FilterType.ObjTipusok.Oszlop)
        {
            GridViewSearchResult.Columns[5].Visible = true; // OszlopdefinicioLetezik
        }

        if (UgyiratHierarchia == true && Filter == Constants.FilterType.ObjTipusok.Tabla)
        {
            tr_keresomezok.Visible = false;
        }

        string js = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}";

        TextBoxKod.Attributes.Add("onkeypress", js);
        TextBoxNev.Attributes.Add("onkeypress", js);

    }



    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, ObjTipusokCPE);
        FillGridViewSearchResult(false);
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbOszlopDefinicioLetezik", "OszlopDefinicioLetezik");
    }

    //r�szletes keres�s ut�n az lovlist keres�si mez�inek alapl�llapotba �ll�t�sa
    private void SetDefaultState()
    {
        TextBoxKod.Text = "";
        TextBoxNev.Text = "";
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
        KRT_ObjTipusokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_ObjTipusokSearch)Search.GetSearchObject(Page, new KRT_ObjTipusokSearch());
            SetDefaultState();
        }
        else
        {
            search = new KRT_ObjTipusokSearch();

            if (!String.IsNullOrEmpty(TextBoxKod.Text))
            {
                search.Kod.Value = TextBoxKod.Text;
                search.Kod.Operator = Search.GetOperatorByLikeCharater(TextBoxKod.Text);                
            }

            if (!String.IsNullOrEmpty(TextBoxNev.Text))
            {
                search.Nev.Value = TextBoxNev.Text;
                search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxNev.Text);
            }

        }

        String KodFilter = String.Empty;
        if (!String.IsNullOrEmpty(Filter))
        {
           
            switch (Filter)
            {
                case Constants.FilterType.ObjTipusok.Tabla:
                    KodFilter = Filter;
                    if (UgyiratHierarchia == true)
                    { 
                        search.Kod.Value = "'" + Constants.TableNames.EREC_UgyUgyiratok + "','" + Constants.TableNames.EREC_UgyUgyiratdarabok + "','" + Constants.TableNames.EREC_IraIratok + "'";
                        search.Kod.Operator = Query.Operators.inner;
                    }
                    break;
                case Constants.FilterType.ObjTipusok.Oszlop:
                    KodFilter = Filter;
                    if (UgyiratHierarchia == true)
                    {
                        // be kell olvasni a sz�l� id-ket, itt nem tudunk nevet megadni
                        String IdFilter = GetUgyiratHierarchiaObjTipusIds();
                        if (!String.IsNullOrEmpty(IdFilter))
                        {
                            search.Obj_Id_Szulo.Value = IdFilter;
                            search.Obj_Id_Szulo.Operator = Query.Operators.inner;
                        }

                        List<string> oszlopokList = new List<string>();
                        oszlopokList.Add("'" + Constants.ColumnNames.EREC_UgyUgyiratok.IratMetaDefinicio_Id + "'");
                        oszlopokList.Add("'" + Constants.ColumnNames.EREC_UgyUgyiratdarabok.IratMetaDefinicio_Id + "'");
                        oszlopokList.Add("'" + Constants.ColumnNames.EREC_IraIratok.IratMetaDefinicio_Id + "'");
                        string oszlopok = String.Join(",", oszlopokList.ToArray());

                        search.Kod.Value = oszlopok;
                        search.Kod.Operator = Query.Operators.inner;
                    }
                    break;
                case Constants.FilterType.ObjTipusok.All:
                    break;
                default:
                    break;
                
            }
        }
        else if (UgyiratHierarchia == true)
        {
            // be kell olvasni a sz�l� id-ket, itt nem tudunk nevet megadni
            String IdFilter = GetUgyiratHierarchiaObjTipusIds();
            if (!String.IsNullOrEmpty(IdFilter))
            {
                search.Obj_Id_Szulo.Group = "555";
                search.Obj_Id_Szulo.GroupOperator = Query.Operators.or;
                search.Obj_Id_Szulo.Value = IdFilter;
                search.Obj_Id_Szulo.Operator = Query.Operators.inner;
                search.Id.Group = "555";
                search.Id.GroupOperator = Query.Operators.or;
                search.Id.Value = IdFilter;
                search.Id.Operator = Query.Operators.inner;
            }
        }

        if (Filter != Constants.FilterType.ObjTipusok.Oszlop)
        {
            String WhereByManual = "";
            if (cbTipusOszlop.Checked == true)
            {
                // az �rv�nyess�g sz�r�s el�je ker�l, �s rosszul f�zi �ssze,
                // ez�rt k�zzel �rjuk hozz� az AND-et is
                WhereByManual = " and OszlopDefinicioLetezik = 1";
            }

            search.WhereByManual += WhereByManual;
        }

        search.OrderBy = "Kod";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(ExecParam, search, KodFilter);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        
     }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string TipusKod = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string TipusNev = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);

                    string Obj_Id_Szulo = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string KodCsoport_Id = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4);

                    //string selectedText = TipusKod + " (" + TipusNev + ")";
                    string selectedText = TipusNev;

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                    // Obj_Id_Szulo, KodCsoport_Id visszak�ld�se:
                    Dictionary<string, string> objtipDictionary = new Dictionary<string, string>();
                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get("ObjIdSzuloHiddenField")))
                    {
                        objtipDictionary.Add(Page.Request.QueryString.Get("ObjIdSzuloHiddenField"), Obj_Id_Szulo);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get("KodcsoportHiddenField")))
                    {
                        objtipDictionary.Add(Page.Request.QueryString.Get("KodcsoportHiddenField"), KodCsoport_Id);
                    }

                    if (objtipDictionary.Count > 0)
                    {
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, objtipDictionary, "objtip");
                    }

                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                    Panel1.Visible = false;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    

    

    
    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }


}
