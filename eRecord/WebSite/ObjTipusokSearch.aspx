<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" CodeFile="ObjTipusokSearch.aspx.cs"
    Inherits="ObjTipusokSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
    
    
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
    
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKod" runat="server" Text="Objektum t�pus k�dja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredTextBox ID="requiredTextBoxKod" runat="server" Validate="false" />
                                </td>
                        </tr>                    
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNev" runat="server" Text="Objektum t�pus neve:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredTextBox ID="requiredTextBoxNev" runat="server" Validate="false" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:ObjTipusokTextBox ID="TipusObjTipusokTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSzulo" runat="server" Text="Sz�l� t�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:ObjTipusokTextBox ID="SzuloObjTipusokTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <%-- TODO: eRecordban nem l�tezik a LovList, Form, Search - egyel�re elt�ntetj�k --%>
                        <tr class="urlapSor" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKodcsoport" runat="server" Text="K�dcsoport:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodcsoportokTextBox ID="KodcsoportokTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>                         
		                        
                        <tr class="urlapSor">
                            <td colspan="2" >
                                <uc9:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc9:Ervenyesseg_SearchFormComponent>
                                &nbsp;</td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                &nbsp; &nbsp;
                    
                    &nbsp; &nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
