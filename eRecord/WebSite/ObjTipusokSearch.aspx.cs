using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ObjTipusokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_ObjTipusokSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.ObjTipusokSearchHeaderTitle;
       
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_ObjTipusokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_ObjTipusokSearch)Search.GetSearchObject(Page, new KRT_ObjTipusokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_ObjTipusokSearch KRT_ObjTipusokSearch = null;
        if (searchObject != null) KRT_ObjTipusokSearch = (KRT_ObjTipusokSearch)searchObject;

        if (KRT_ObjTipusokSearch != null)
        {
        
            requiredTextBoxKod.Text = KRT_ObjTipusokSearch.Kod.Value;    
            requiredTextBoxNev.Text = KRT_ObjTipusokSearch.Nev.Value;

            TipusObjTipusokTextBox.Id_HiddenField = KRT_ObjTipusokSearch.ObjTipus_Id_Tipus.Value;
            TipusObjTipusokTextBox.SetObjTipusokTextBoxById(SearchHeader1.ErrorPanel);
            SzuloObjTipusokTextBox.Id_HiddenField = KRT_ObjTipusokSearch.Obj_Id_Szulo.Value;
            SzuloObjTipusokTextBox.SetObjTipusokTextBoxById(SearchHeader1.ErrorPanel);
        
            KodcsoportokTextBox1.Id_HiddenField = KRT_ObjTipusokSearch.KodCsoport_Id.Value;
            KodcsoportokTextBox1.SetTextBoxById(SearchHeader1.ErrorPanel);
        
            Ervenyesseg_SearchFormComponent1.SetDefault(
                KRT_ObjTipusokSearch.ErvKezd, KRT_ObjTipusokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_ObjTipusokSearch SetSearchObjectFromComponents()
    {
        KRT_ObjTipusokSearch KRT_ObjTipusokSearch = (KRT_ObjTipusokSearch)SearchHeader1.TemplateObject;
        if (KRT_ObjTipusokSearch == null)
        {
            KRT_ObjTipusokSearch = new KRT_ObjTipusokSearch();
        }

        if (!String.IsNullOrEmpty(requiredTextBoxNev.Text))
        {
            KRT_ObjTipusokSearch.Nev.Value = requiredTextBoxNev.Text;
            KRT_ObjTipusokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(requiredTextBoxNev.Text);
        }
        
        if (!String.IsNullOrEmpty(requiredTextBoxKod.Text))
        {        
            KRT_ObjTipusokSearch.Kod.Value = requiredTextBoxKod.Text;
            KRT_ObjTipusokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(requiredTextBoxKod.Text);       
        }
        
        if (!String.IsNullOrEmpty(TipusObjTipusokTextBox.Id_HiddenField))
        {        
            KRT_ObjTipusokSearch.ObjTipus_Id_Tipus.Value = TipusObjTipusokTextBox.Id_HiddenField;
            KRT_ObjTipusokSearch.ObjTipus_Id_Tipus.Operator = Query.Operators.equals;
        }
        
        if (!String.IsNullOrEmpty(SzuloObjTipusokTextBox.Id_HiddenField))
        {        
            KRT_ObjTipusokSearch.Obj_Id_Szulo.Value = SzuloObjTipusokTextBox.Id_HiddenField;
            KRT_ObjTipusokSearch.Obj_Id_Szulo.Operator = Query.Operators.equals;

        }

        if (!String.IsNullOrEmpty(KodcsoportokTextBox1.Id_HiddenField))
        {
            KRT_ObjTipusokSearch.KodCsoport_Id.Value = KodcsoportokTextBox1.Id_HiddenField;
            KRT_ObjTipusokSearch.KodCsoport_Id.Operator = Query.Operators.equals;
        }     


        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            KRT_ObjTipusokSearch.ErvKezd, KRT_ObjTipusokSearch.ErvVege);        
              
        return KRT_ObjTipusokSearch;
    }

  

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_ObjTipusokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_ObjTipusokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_ObjTipusokSearch();
    }

}
