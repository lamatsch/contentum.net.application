<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="OktatasiAnyagokList.aspx.cs" Inherits="OktatasiAnyagokList" %>
<%@ MasterType TypeName="MasterPage" %>    

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register src="eRecordComponent/DokumentumokList.ascx" tagname="DokumentumokList" tagprefix="dl" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <dl:DokumentumokList ID="DokumentumokLista" runat="server" />
    
</asp:Content>
