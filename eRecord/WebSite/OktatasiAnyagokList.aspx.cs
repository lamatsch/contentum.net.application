using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Utility;

public partial class OktatasiAnyagokList : Contentum.eUtility.UI.PageBase
{
    private string DokumentumId
    {
        get
        {
            return QueryStringVars.Get<Guid>(Page,QueryStringVars.DokumentumId);
        }
    }

    private bool IsVersionView
    {
        get
        {
            return (!String.IsNullOrEmpty(DokumentumId));
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (IsVersionView)
        {
            Master.SetPopupFromat();
        }

        DokumentumokLista.ScriptManager = ScriptManager1;
        DokumentumokLista.ListHeader = ListHeader1;
        DokumentumokLista.DokumentumId = DokumentumId;
        DokumentumokLista.IsVersionView = IsVersionView;

        DokumentumokLista.InitPage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DokumentumokLista.LoadPage();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DokumentumokLista.PreRenderPage();
    }


    
}




