using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;


public partial class OrszagokList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "OrszagokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        TelepulesekSubListHeader.RowCount_Changed += new EventHandler(TelepulesekSubListHeader_RowCount_Changed);

        TelepulesekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(TelepulesekSubListHeader_ErvenyessegFilter_Changed);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_OrszagokSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.OrszagokLisHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("OrszagokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelOrszagok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("OrszagokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelOrszagok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewOrszagok.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewOrszagok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewOrszagok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewOrszagok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewOrszagok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelOrszagok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewOrszagok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewOrszagok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //Allist�k gombjaihoz kliens oldali szkriptek regisztr�l�sa
        TelepulesekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TelepulesekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TelepulesekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TelepulesekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewTelepulesek.ClientID, "", "check");

        //selectedRecordId kezel�se
        TelepulesekSubListHeader.AttachedGridView = gridViewTelepulesek;

        //Allist�k �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewTelepulesek);

        //Allist�k esem�nykezel� f�ggv�nyei
        TelepulesekSubListHeader.ButtonsClick += new
        CommandEventHandler(TelepulesekSubListHeader_ButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        //TabContainer beregisztr�l�sa a ScriptManager-ben
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_OrszagokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            OrszagokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Orszag" + CommandName.Lock);

        //Allist�k l�that�s�ga jogosults�g alapj�n
        panelTelepulesek.Visible = FunctionRights.GetFunkcioJog(Page, "TelepulesekList");

        //Allist�k funkci�inak enged�lyez�se jogosults�gok alapj�n
        TelepulesekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Telepules" + CommandName.New);
        TelepulesekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Telepules" + CommandName.View);
        TelepulesekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Telepules" + CommandName.Modify);
        TelepulesekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Telepules" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewOrszagok);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void OrszagokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewOrszagok", ViewState, "KRT_Orszagok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewOrszagok", ViewState);

        OrszagokGridViewBind(sortExpression, sortDirection);
    }


    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void OrszagokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_OrszagokService service = eAdminService.ServiceFactory.GetKRT_OrszagokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_OrszagokSearch search = (KRT_OrszagokSearch)Search.GetSearchObject(Page, new KRT_OrszagokSearch());

        search.OrderBy = Search.GetOrderBy("gridViewOrszagok", ViewState, SortExpression, SortDirection);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(gridViewOrszagok, res,ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewOrszagok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewOrszagok_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeOrszagok);
        ListHeader1.RefreshPagerLabel();

    }
    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        OrszagokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeOrszagok);
        OrszagokGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewOrszagok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewOrszagok, selectedRowNumber, "check");

            string id = gridViewOrszagok.DataKeys[selectedRowNumber].Value.ToString();
            
            //Allist�k friss�t�se a kiv�lasztott sor f�ggv�ny�ben
            ActiveTabRefresh(TabContainer1, id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("OrszagokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelOrszagok.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("OrszagokForm.aspx"
                 , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelOrszagok.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "Krt_Orszagok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelOrszagok.ClientID);

            //Allist�k �rv�nyes funkci�inak regisztr�l�sa
            TelepulesekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("TelepulesekForm.aspx"
               , "Command=" + CommandName.New + "&" + "OrszagID" + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTelepulesek.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelOrszagok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    OrszagokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewOrszagok));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedOrszag();
            OrszagokGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedOrszag()
    {

        if (FunctionRights.GetFunkcioJog(Page, "OrszagInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewOrszagok, EErrorPanel1, ErrorUpdatePanel);

            KRT_OrszagokService service = eAdminService.ServiceFactory.GetKRT_OrszagokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }      

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedOrszagRecords();
                OrszagokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedOrszagRecords();
                OrszagokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedOrszagok();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedOrszagRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewOrszagok, "KRT_Orszagok"
            , "OrszagLock", "OrszagForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedOrszagRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewOrszagok, "KRT_Orszagok"
            , "OrszagLock", "OrszagForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewOrszagok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedOrszagok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewOrszagok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Orszagok");
        }
    }


    //GridView Sorting esm�nykezel�je
    protected void gridViewOrszagok_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrszagokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewOrszagok", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewOrszagok));
            ActiveTabRefreshOnClientClicks();
        }
    }

    /// <summary>
    /// TabContainer ActiveTabChanged esem�nykezel�je
    /// A kiv�lasztott record adataival friss�ti az �j panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewOrszagok.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewOrszagok);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az akt�v panel friss�t�se a f� list�ban kiv�lasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                TelepulesekGridViewBind(masterId);
                panelTelepulesek.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az akt�v panel tartalm�nak t�rl�se
    /// </summary>
    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(gridViewTelepulesek);
                break;
        }
    }

    /// <summary>
    /// Az allist�k egy oldalon megjelen�tett sorok sz�m�nak be�ll�t�sa
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        TelepulesekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(tabPanelTelepulesek))
        {
            gridViewTelepulesek_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(gridViewTelepulesek));
        }
    }


    #endregion

    #region Telepulesek Detail

    /// <summary>
    /// Allista funkci�gomjainak esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void TelepulesekSubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTelepulesek();
            TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok));
        }
    }

    /// <summary>
    /// Kiv�lasztott record-ok t�rl�se
    /// </summary>
    private void deleteSelectedTelepulesek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "TelepulesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewTelepulesek, EErrorPanel1, ErrorUpdatePanel);

            KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                    execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                    execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// A Telepulesek GridView adatk�t�se a OrszagID f�ggv�ny�ben alap�rtelmezett
    /// rendez�si param�terekkel
    /// </summary>
    /// <param name="OrszagId"></param>
    protected void TelepulesekGridViewBind(String OrszagId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewTelepulesek", ViewState, "KRT_Telepulesek.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewTelepulesek", ViewState);

        TelepulesekGridViewBind(OrszagId, sortExpression, sortDirection);
    }

    /// <summary>
    /// A Telepulesek GridView adatk�t�se a OrszagID �s rendez�si param�terek f�ggv�ny�ben
    /// </summary>
    /// <param name="OrszagId"></param>
    /// <param name="SortExpression"></param>
    /// <param name="SortDirection"></param>
    protected void TelepulesekGridViewBind(String OrszagId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(OrszagId))
        {
            KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_TelepulesekSearch _KRT_TelepulesekSearch = (KRT_TelepulesekSearch)Search.GetSearchObject(Page, new KRT_TelepulesekSearch());
            _KRT_TelepulesekSearch.OrderBy = Search.GetOrderBy("gridViewTelepulesek", ViewState, SortExpression, SortDirection);
            _KRT_TelepulesekSearch.Orszag_Id.Filter(OrszagId);

            TelepulesekSubListHeader.SetErvenyessegFields(_KRT_TelepulesekSearch.ErvKezd, _KRT_TelepulesekSearch.ErvVege);

            Result res = service.GetAllWithOrszag(ExecParam, _KRT_TelepulesekSearch);
            UI.GridViewFill(gridViewTelepulesek, res, TelepulesekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewTelepulesek);
        }
        else
        {
            ui.GridViewClear(gridViewTelepulesek);
        }
    }
    
    void TelepulesekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok));
    }

    /// <summary>
    /// Telepulesek updatepanel Load esem�nykezel�je
    /// Telepulesek panel friss�t�se, amennyiben akt�v
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void updatePanelTelepulesek_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(tabPanelTelepulesek))
                    //{
                    //    TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok));
                    //}
                    ActiveTabRefreshDetailList(tabPanelTelepulesek);
                    break;
            }
        }
    }

    /// <summary>
    /// Telepulesek GridView RowCommand esem�nylezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewTelepulesek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewTelepulesek, selectedRowNumber, "check");
        }
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewTelepulesek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    /// <summary>
    /// A Telepulesek lista kiv�lasztott sor�ra �rv�nye funkci�gombok be�ll�t�sa
    /// </summary>
    /// <param name="selectedId"></param>
    private void gridViewTelepulesek_RefreshOnClientClicks(string selectedId)
    {
        string id = selectedId;
        if (!String.IsNullOrEmpty(id))
        {
            TelepulesekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("TelepulesekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTelepulesek.ClientID, EventArgumentConst.refreshDetailList);

            TelepulesekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("TelepulesekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTelepulesek.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    /// <summary>
    /// Telepulesek GridView PreRender esem�nykezel�je
    /// Lapoz�s kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewTelepulesek_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewTelepulesek.PageIndex;
        gridViewTelepulesek.PageIndex = TelepulesekSubListHeader.PageIndex;

        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != gridViewTelepulesek.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeTelepulesek);
            TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok));
        }
        else
        {
            UI.GridViewSetScrollable(TelepulesekSubListHeader.Scrollable, cpeTelepulesek);
        }

        TelepulesekSubListHeader.PageCount = gridViewTelepulesek.PageCount;
        TelepulesekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(gridViewTelepulesek);
    }

    void TelepulesekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(TelepulesekSubListHeader.RowCount);
        TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok));
    }

    /// <summary>
    /// Telepulesek GridView Sorting esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewTelepulesek_Sorting(object sender, GridViewSortEventArgs e)
    {
        TelepulesekGridViewBind(UI.GetGridViewSelectedRecordId(gridViewOrszagok)
            , e.SortExpression, UI.GetSortToGridView("gridViewTelepulesek", ViewState, e.SortExpression));
    }

    #endregion
}
