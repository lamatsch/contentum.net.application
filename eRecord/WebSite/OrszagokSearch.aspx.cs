using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class OrszagokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_OrszagokSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
   
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);       
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_OrszagokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_OrszagokSearch)Search.GetSearchObject(Page, new KRT_OrszagokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_OrszagokSearch _KRT_OrszagokSearch = (KRT_OrszagokSearch)searchObject;

        if (_KRT_OrszagokSearch != null)
        {
            textNev.Text = _KRT_OrszagokSearch.Nev.Value;
            textKod.Text = _KRT_OrszagokSearch.Kod.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                _KRT_OrszagokSearch.ErvKezd, _KRT_OrszagokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_OrszagokSearch SetSearchObjectFromComponents()
    {
        KRT_OrszagokSearch _KRT_OrszagokSearch = (KRT_OrszagokSearch)SearchHeader1.TemplateObject;

        if (_KRT_OrszagokSearch == null)
        {
            _KRT_OrszagokSearch = new KRT_OrszagokSearch();
        }

        _KRT_OrszagokSearch.Nev.Value = textNev.Text;
        _KRT_OrszagokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _KRT_OrszagokSearch.Kod.Value = textKod.Text;
        _KRT_OrszagokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(textKod.Text);


        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_OrszagokSearch.ErvKezd, _KRT_OrszagokSearch.ErvVege);


        return _KRT_OrszagokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_OrszagokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }       
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_OrszagokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_OrszagokSearch();
    }
}
