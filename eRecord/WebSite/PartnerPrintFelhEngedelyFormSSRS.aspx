<%@ Page Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true" 
    CodeFile="PartnerPrintFelhEngedelyFormSSRS.aspx.cs" Inherits="PartnerekPrintFormSSRS" 
    Title="Untitled Page"  %>   

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
</asp:ScriptManager>

 



<asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
        <table cellpadding="0" cellspacing="0" width="95%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" 
                                    Width="1200px" Height="1000px" EnableTelemetry="false">
                                   <ServerReport ReportPath = "/Sablonok/PartnerFelhTitok" />
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
    </table>
</asp:Content>