<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerekForm.aspx.cs" Inherits="PartnerekForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/OrszagokTextBox.ascx" TagName="OrszagokTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc11" %>
<%@ Register Src="Component/AdoszamTextBox.ascx" TagName="AdoszamTextBox" TagPrefix="uc1" %>
<%@ Register Src="Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 80px;
            min-width: 80px;
        }

        .mrUrlapCaption_short {
            text-align: left;
            width: 80px;
            min-width: 80px;
        }

        
    </style>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnerekFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelPartnerTipus" runat="server" Text="T�pus:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="KodtarakDropDownListPartnerTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr id="trPartnerNev" runat="server" class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:RequiredTextBox ID="RequiredTextBoxNev" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc8:OrszagokTextBox ID="OrszagokTextBox1" runat="server" Validate="false" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelBelso" runat="server" Text="Bels�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="ddownBelso" runat="server" CssClass="mrUrlapInputComboBox"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="label8" runat="server" Text="�llapot:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="KodtarakDropDownListPartnerAllapot" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="urlapSorkapcsolatTipus" runat="server" visible="false">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                           <asp:Label ID="labelKapcsolatTipu" runat="server" Text="Kapcsolat t�pus:"></asp:Label>
                                        </td>
                                    <td class="mrUrlapMezo">
                                        <asp:RadioButtonList runat="server" ID="RadioButtonKapcsolatTipus" >
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Szem�lyek -->
                        <asp:Panel ID="panelSzemelyek" runat="server">
                            <div class="mrUrlapSubTitleWrapper">
                                <span class="mrUrlapSubTitleLeftSide"></span>
                                <span class="mrUrlapSubTitle">
                                    <asp:Label ID="labelTovabbiSzemely" runat="server" Text="Szem�ly tov�bbi adatai"></asp:Label>
                                </span>
                                <span class="mrUrlapSubTitleRightSide"></span>
                            </div>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td>
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTitulus" runat="server" Text="Titulus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTitulus" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short"></td>
                                        <td class="mrUrlapMezo"></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelCsaladiNev" runat="server" Text="Csal�di n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:RequiredTextBox ID="RequiredTexBoxCsaladiNev" runat="server" CssClass="mrUrlapInputSearch" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelUtoNev" runat="server" Text="Ut�n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:RequiredTextBox ID="RequiredTextBoxUtoNev" runat="server" CssClass="mrUrlapInputSearch" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTovabbiUtonev" runat="server" Text="Tov�bbi ut�nevek:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTovabbiUtonev" runat="server" CssClass="mrUrlapInputSearch" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short"></td>
                                        <td class="mrUrlapMezo"></td>
                                    </tr>
                                    <tr class="urlapSor" runat="server" id="ParentsDataRow">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAnyjaCsaladiNeve" runat="server" Text="Anyja csal�di neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAnyjaCsaladiNeve" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <%--BLG_1952--%>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAnyjaUtoNev" runat="server" Text="Anyja Ut�neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAnyjaUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" runat="server" id="ParentsDataRow2">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAnyjaTovabbiUtoNev" runat="server" Text="Anyja tov�bbi ut�nevei:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAnyjaTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>                                        
                                        <td class="mrUrlapCaption_short">                                            
                                        </td>
                                        <td class="mrUrlapMezo">                                            
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiCsaladiNev" runat="server" Text="Sz�let�si csal�di neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzuletesiCsaladiNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiUtoneNev" runat="server" Text="Sz�let�si ut�n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzuletesiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiTovabbiUtoneNev" runat="server" Text="Sz�let�si tov�bbi ut�n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzuletesiTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiIdo" runat="server" Text="Sz�let�si id�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc11:CalendarControl ID="CalendarSzuletesiIdo" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiOrszag" runat="server" Text="Sz�let�si orsz�g:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc9:OrszagTextBox ID="OrszagTextBoxSzuletesi" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiHely" runat="server" Text="Sz�let�si hely:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:TelepulesTextBox ID="TelepulesTextBoxSzuletesi" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr id="rowTAJ_Szemelyi" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTajSzam" runat="server" Text="TAJ sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTajSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzemelyi" runat="server" Text="<%$Forditas:labelSzemelyi|Szem�lyi igazolv�ny sz�m:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzigSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelNeme" runat="server" Text="Neme:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rbListNeme" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputSearchRadioButtonList">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzemelyiAzonosito" runat="server" Text="<%$Forditas:labelSzemelyiAzonosito|Szem�lyi azonos�t�:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzemelyiAzonosito" runat="server"  CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="rowAdoSzam_AdoAzonosito" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoAzonosito" runat="server" Text="Ad�azonos�t� jel:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAdoAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoSzamSzemely" runat="server" Text="Ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc1:AdoszamTextBox ID="textAdoszamSzemely" runat="server" CssClass="mrUrlapInputSearch"
                                                KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="true" />
                                        </td>
                                    </tr>
                                    <%--BLG_346--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelBeosztas" runat="server" Text="<%$Forditas:labelBeosztas|Beoszt�s:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textBeosztas" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelMinositesiSzint" runat="server" Text="<%$Forditas:labelMinositesiSzint|Min�s�t�si szint:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:KodtarakDropDownList ID="KodtarakDropDownListMinositesiSzint" runat="server" CssClass="mrUrlapInputComboBox" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <!-- V�llalkoz�sok -->
                        <asp:Panel ID="panelVallalkozasok" runat="server">
                            <div class="mrUrlapSubTitleWrapper">
                                <span class="mrUrlapSubTitleLeftSide"></span>
                                <span class="mrUrlapSubTitle">
                                    <asp:Label ID="labelTovabbiSzervezet" runat="server" Text="Szervezet tov�bbi adatai"></asp:Label>
                                </span>
                                <span class="mrUrlapSubTitleRightSide"></span>
                            </div>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo" style="padding-right: 232px">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td>
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo" style="padding-right: 230px">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoSzamVallalkozas" runat="server" Text="Ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc1:AdoszamTextBox ID="textAdoSzamVallalkozas" runat="server" CssClass="mrUrlapInputSearch"
                                                KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="true" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTBSzam" runat="server" Text="TB T�rzssz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTBSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelCegJegyzekSzam" runat="server" Text="C�gjegyz�ksz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textCegJegyzekSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelCegTipus" runat="server" Text="<%$Forditas:labelCegTipus|C�g t�pus:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:KodtarakDropDownList ID="KodtarakDropDownListCegTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption_short">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption_short" style="padding-right: 19px;">
                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                    </td>
                                </tr>
                                </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                    <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

