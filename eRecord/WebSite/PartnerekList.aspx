<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PartnerekList.aspx.cs" Inherits="Pages_PartnerekList" Title="Untitled Page"%>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register src="eRecordComponent/DokumentumokList.ascx" tagname="DokumentumokList" tagprefix="dl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<uc2:ListHeader ID="ListHeader1" runat="server" />
<!--Hiba megjelen�t�se-->
<asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<!--Friss�t�s jelz�se-->
<uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--HiddenFields--%> 
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpePartnerek" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="PartnerekUpdatePanel" runat="server" OnLoad="PartnerekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpePartnerek" runat="server" TargetControlID="panelPartnerek"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpePartnerek" CollapseControlID="btnCpePartnerek"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpePartnerek"
                            ExpandedSize="0" ExpandedText="K�dcsoportok list�ja" CollapsedText="K�dcsoportok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        
                        <asp:Panel ID="panelPartnerek" runat="server" Width="100%" printable="true"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="PartnerekGridView" runat="server" OnSorting="PartnerekGridView_Sorting"
                                        GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                        DataKeyNames="Id" AutoGenerateColumns="False" OnPreRender="PartnerekGridView_PreRender" 
                                        AllowSorting="True" PagerSettings-Visible="false" AllowPaging="True" 
                                        CellPadding="0" OnRowCommand="PartnerekGridView_RowCommand" OnRowDataBound="PartnerekGridView_RowDataBound">        
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                    &nbsp;&nbsp;
                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox id="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>        
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                            <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="KRT_Partnerek.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipus_Nev" HeaderText="Tipus" SortExpression="KRT_Partnerek.Tipus">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Forras_Nev" HeaderText="Forr�s" SortExpression="KRT_Partnerek.Forras">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                             <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="labelBelso" runat="server">Bels�</asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbBelso" runat="server" Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>                        
                                        </Columns>
                                    <PagerSettings Visible="False" />
                                   </asp:GridView>
                                   <div class="MediaPrintBreak" />
                                   </td>
                                 </tr>
                             </table>
                        </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>                            
          </tr>      
          <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
          </tr>
          <!--Allist�k--> 
          <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeDetail" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;"> 
            <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                 CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeDetail" CollapseControlID="btnCpeDetail" ExpandDirection="Vertical"
                 AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeDetail">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="panelDetail" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%" ActiveTabIndex="0">
                        <!--Hozz�rendelt Felhaszn�l�k list�ja -->
                        <ajaxToolkit:TabPanel ID="FelhasznalokTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltFelhasznalok" runat="server">
                                    <ContentTemplate>                            
                                        <asp:Label ID="headerFelhasznalok" runat="server" Text="Hozz�rendelt Felhaszn�l�k"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="FelhasznalokUpdatePanel" runat="server" OnLoad="FelhasznalokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="FelhasznalokPanel" runat="server" Visible="true" Width="100%">
                                            <uc1:SubListHeader ID="FelhasznalokSubListHeader" runat="server" />
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <ajaxToolkit:CollapsiblePanelExtender ID="FelhasznalokCPE" runat="server" TargetControlID="panelFelhasznalokList"
                                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                                AutoCollapse="false" AutoExpand="false" ExpandedSize="0">            
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="panelFelhasznalokList" runat="server" printable="true">
                                                    <asp:GridView ID="FelhasznalokGridView" runat="server" AutoGenerateColumns="False" 
                                                     CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                     AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                                     OnPreRender="FelhasznalokGridView_PreRender" OnRowCommand="FelhasznalokGridView_RowCommand" DataKeyNames="Id" 
                                                     OnSorting="FelhasznalokGridView_Sorting" OnRowDataBound="FelhasznalokGridView_RowDataBound">        
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                        <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                        <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server"  AutoPostBack="false" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>        
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="Nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />               
                                                        </asp:BoundField>                                   
                                                        <asp:BoundField DataField="UserNev" HeaderText="Felhaszn�l� n�v" SortExpression="UserNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                        </asp:BoundField>                                                                               
                                                        <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />             
                                                        </asp:BoundField>                  
                                                        <asp:TemplateField>
                                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <asp:Label ID="labelSystem" runat="server" Text="Rendszer felhaszn�l�" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbSystem" runat="server" Enabled="false" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField> 
                                                        <asp:BoundField DataField="EMail" HeaderText="E-mail c�m" SortExpression="EMail">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />             
                                                        </asp:BoundField>                                                           
                                                       </Columns>                
                                                    </asp:GridView>
                                                 </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Kapcsolatok list�ja(nem al�rendelt) -->
                        <ajaxToolkit:TabPanel ID="KapcsolatokTabPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelKapcsolatok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="headerKapcsolatok" runat="server" Text="Kapcsolatok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="KapcsolatokUpdatePanel" runat="server" OnLoad="KapcsolatokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="KapcsolatokPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="KapcsolatokSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="KapcsolatokCPE" runat="server" TargetControlID="panelKapcsolatokList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelKapcsolatokList" runat="server" printable="true">
                                                <asp:GridView ID="KapcsolatokGridView" runat="server" 
                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"     
                                                    OnPreRender="KapcsolatokGridView_PreRender" OnRowCommand="KapcsolatokGridView_RowCommand" DataKeyNames="Id" OnSorting="KapcsolatokGridView_Sorting">        
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                    <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server"  AutoPostBack="false" Text='<%# Eval("Partner_kapcsolt_Nev") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>        
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                        ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="TipusNev" HeaderText="Kapcsolat t�pusa" SortExpression="TipusNev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Partner neve" SortExpression="Partner_kapcsolt_Nev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_TipusNev" HeaderText="Partner t�pusa" SortExpression="Partner_kapcsolt_TipusNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    </Columns>                
                                                  </asp:GridView>
                                                 </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Al�rendelt Kapcsolatok -->
                        <ajaxToolkit:TabPanel ID="AlarendeltKapcsolatokTabPanel" runat="server" TabIndex="2">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelPartnerHierarchia" runat="server">
                                    <ContentTemplate>                            
                                        <asp:Label ID="headerAlarendeltKapcsolatok" runat="server" Text="Partner hierarchia"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="AlarendeltKapcsolatokUpdatePanel" runat="server" OnLoad="AlarendeltKapcsolatokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="AlarendeltKapcsolatokPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="AlarendeltKapcsolatokSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="AlarendeltKapcsolatokCPE" runat="server" TargetControlID="panelAlarendeltKapcsolatokList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelAlarendeltKapcsolatokList" runat="server" printable="true">
                                                <asp:GridView ID="AlarendeltKapcsolatokGridView" runat="server" 
                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"     
                                                    OnPreRender="AlarendeltKapcsolatokGridView_PreRender" OnRowCommand="AlarendeltKapcsolatokGridView_RowCommand" DataKeyNames="Id" OnSorting="AlarendeltKapcsolatokGridView_Sorting">        
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                    <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server"  AutoPostBack="false" Text='<%# Eval("Partner_kapcsolt_Nev") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>        
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                        ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="TipusNev" HeaderText="Kapcsolat t�pusa" SortExpression="TipusNev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Partner neve" SortExpression="Partner_kapcsolt_Nev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_TipusNev" HeaderText="Partner t�pusa" SortExpression="Partner_kapcsolt_TipusNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    </Columns>                
                                                  </asp:GridView>
                                                 </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Banksz�mlasz�mok -->
                        <ajaxToolkit:TabPanel ID="BankszamlaszamokTabPanel" runat="server" TabIndex="3">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelBankszamlaszamok" runat="server">
                                    <ContentTemplate>                            
                                        <asp:Label ID="headerBankszamlaszamok" runat="server" Text="Banksz�mlasz�mok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                
                            </HeaderTemplate>
                             <ContentTemplate>
                                <asp:UpdatePanel ID="BankszamlaszamokUpdatePanel" runat="server" OnLoad="BankszamlaszamokUpdatePanel_Load">
                                <ContentTemplate>           
                                <asp:Panel ID="BankszamlaszamokPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="BankszamlaszamokSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="BankszamlaszamokCPE" runat="server" TargetControlID="panelBankszamlaszamokList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelBankszamlaszamokList" runat="server" printable="true">
                                                <asp:GridView ID="BankszamlaszamokGridView" runat="server"
                                                CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" 
                                                AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="BankszamlaszamokGridView_PreRender"
                                                AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="BankszamlaszamokGridView_Sorting"
                                                OnRowDataBound="BankszamlaszamokGridView_RowDataBound" OnRowCommand="BankszamlaszamokGridView_RowCommand">
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                <Columns>
                                                    <asp:TemplateField>                                        
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField ButtonType="Image" 
                                                            ShowSelectButton="True" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                    </asp:CommandField>
<%--                                                    <asp:BoundField DataField="Bankszamlaszam" HeaderText="Banksz�mlasz�m" SortExpression="Bankszamlaszam">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>--%>
                                                    <asp:TemplateField>
                                                        <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="linkBankszamlaszam" runat="server" CommandName="sort" CommandArgument="Bankszamlaszam">Banksz�mlasz�m</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelBankszamlaszam" runat="server" Text='<%# string.Format("{0}{1}{2}", Eval("Bankszamlaszam1"), (string.IsNullOrEmpty(Eval("Bankszamlaszam2") as string) ? "" : string.Format("-{0}",Eval("Bankszamlaszam2"))), (string.IsNullOrEmpty(Eval("Bankszamlaszam3") as string) ? "" : string.Format("-{0}",Eval("Bankszamlaszam3"))))  %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>    
                                                    <asp:BoundField DataField="Partner_Nev_Bank" HeaderText="Bank" SortExpression="KRT_Partnerek_Bank.Nev" >
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <PagerSettings Visible="False" />
                                                </asp:GridView>
                                             </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <!-- Postai c�mek-->
                        <ajaxToolkit:TabPanel ID="PostaiCimekTabPanel" runat="server" TabIndex="4">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelPostaiCimek" runat="server">
                                    <ContentTemplate>                            
                                        <asp:Label ID="headerPostaiCimek" runat="server" Text="Postai c�mek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                
                            </HeaderTemplate>
                             <ContentTemplate>
                                <asp:UpdatePanel ID="PostaiCimekUpdatePanel" runat="server" OnLoad="PostaiCimekUpdatePanel_Load">
                                <ContentTemplate>           
                                <asp:Panel ID="PostaiCimekPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="PostaiCimekSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="PostaiCimekCPE" runat="server" TargetControlID="panelPostaiCimekList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelPostaiCimekList" runat="server" printable="true">
                                                <asp:GridView ID="PostaiCimekGridView" runat="server"
                                                CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" 
                                                AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="PostaiCimekGridView_PreRender"
                                                AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="PostaiCimekGridView_Sorting"
                                                OnRowDataBound="PostaiCimekGridView_RowDataBound" OnRowCommand="PostaiCimekGridView_RowCommand">
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                <Columns>
                                                    <asp:TemplateField>                                        
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField ButtonType="Image" 
                                                            ShowSelectButton="True" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="PartnerCimFajta_Nev" HeaderText="C�m&nbsp;fajt�ja" SortExpression="PartnerCimFajta_Nev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>   
                                                    <asp:BoundField DataField="TelepulesNev" HeaderText="Telep&#252;l&#233;s" SortExpression="TelepulesNev" >
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IRSZ" HeaderText="Ir&#225;ny&#237;t&#243;sz&#225;m" SortExpression="IRSZ" >
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="KozteruletNev" HeaderText="K&#246;zter&#252;let" SortExpression="KozteruletNev">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="KozteruletTipusNev" HeaderText="K&#246;zter&#252;let t&#237;pus" SortExpression="KozteruletNev" >
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="linkHazszam" runat="server" CommandName="sort" CommandArgument="Hazszam" >H�zsz�m</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelHazszam" runat="server" Text='<%# Eval("Hazszam") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Lepcsohaz" HeaderText="L&#233;pcs�h&#225;z" SortExpression="Lepcsohaz">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Szint" HeaderText="Szint" SortExpression="Szint">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="linkAjto" runat="server" CommandName="sort" CommandArgument="Ajto" >Ajt�</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelAjto" runat="server" Text='<%# Eval("Ajto") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <%--BLG_1347--%>
                                                    <asp:TemplateField HeaderText="Hrsz." SortExpression="Hrsz">
                                                        <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                                         <HeaderTemplate>
                                                            <asp:LinkButton ID="linkHrsz" runat="server" CommandName="sort" CommandArgument="Hrsz" >Hrsz</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                            
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelHrsz" runat="server" Text='<%# Eval("Hrsz") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerSettings Visible="False" />
                                                </asp:GridView>
                                             </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Egy�b c�mek -->
                        <ajaxToolkit:TabPanel ID="EgyebCimekTabPanel" runat="server" TabIndex="5">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelEgyebCimek" runat="server">
                                    <ContentTemplate>                         
                                        <asp:Label ID="labelEgyebCimek" runat="server" Text="Egy�b c�mek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                
                            </HeaderTemplate>
                             <ContentTemplate>
                                <asp:UpdatePanel ID="EgyebCimekUpdatePanel" runat="server" OnLoad="EgyebCimekUpdatePanel_Load">
                                <ContentTemplate>           
                                <asp:Panel ID="EgyebCimekPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="EgyebCimekSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="EgyebCimekCPE" runat="server" TargetControlID="panelEgyebCimekList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelEgyebCimekList" runat="server" printable="true">
                                                <asp:GridView ID="EgyebCimekGridView" runat="server"
                                                CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="EgyebCimekGridView_PreRender"
                                                AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="EgyebCimekGridView_Sorting" OnRowCommand="EgyebCimekGridView_RowCommand">
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                <Columns>
                                                    <asp:TemplateField>                                        
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="TipusNev" HeaderText="C�m T�pusa" SortExpression="TipusNev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>   
                                                    <asp:BoundField DataField="CimTobbi" HeaderText="C�m" SortExpression="CimTobbi">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>   
                                                </Columns>
                                                <PagerSettings Visible="False" />
                                                </asp:GridView>
                                               </asp:Panel>
                                              </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate> 
                         </ajaxToolkit:TabPanel>
                        <!-- Konszolid�ci� -->
                        <ajaxToolkit:TabPanel ID="KonszolidacioTabPanel" runat="server" TabIndex="6">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelKonszolidacio" runat="server">
                                    <ContentTemplate>                            
                                        <asp:Label ID="headerKonszolidacio" runat="server" Text="Konszolidacio"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="KonszolidacioUpdatePanel" runat="server" OnLoad="KonszolidacioUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="KonszolidacioPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="KonszolidacioSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="KonszolidacioCPE" runat="server" TargetControlID="panelKonszolidacioList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelKonszolidacioList" runat="server" printable="true">
                                                <asp:GridView ID="KonszolidacioGridView" runat="server" 
                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"     
                                                    OnPreRender="KonszolidacioGridView_PreRender" OnRowCommand="KonszolidacioGridView_RowCommand" DataKeyNames="Id" OnSorting="KonszolidacioGridView_Sorting">        
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                    <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server"  AutoPostBack="false" Text='<%# Eval("Partner_kapcsolt_Nev") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>        
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                        ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="TipusNev" HeaderText="Kapcsolat t�pusa" SortExpression="TipusNev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Partner neve" SortExpression="Partner_kapcsolt_Nev"> 
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                    
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Partner_kapcsolt_TipusNev" HeaderText="Partner t�pusa" SortExpression="Partner_kapcsolt_TipusNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    </Columns>                
                                                  </asp:GridView>
                                                 </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Min�s�t�sek -->
                        <ajaxToolkit:TabPanel ID="MinositesekTabPanel" runat="server" TabIndex="7" Visible="false">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelMinositesek" runat="server">
                                    <ContentTemplate>                               
                                        <asp:Label ID="headerMinositesek" runat="server" Text="Min�sit�sek" Visible="false"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="MinositesekUpdatePanel" runat="server" OnLoad="MinositesekUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="MinositesekPanel" runat="server" Visible="true" Width="100%">
                                        <uc1:SubListHeader ID="MinositesekSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="MinositesekCPE" runat="server" TargetControlID="panelMinositesekList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false"  ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelMinositesekList" runat="server" printable="true">
                                                <asp:GridView ID="MinositesekGridView" runat="server" 
                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"     
                                                    OnPreRender="MinositesekGridView_PreRender" OnRowCommand="MinositesekGridView_RowCommand" DataKeyNames="Id" OnSorting="MinositesekGridView_Sorting">        
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                    <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server"  AutoPostBack="false" Text='<%# Eval("ALLAPOT_Nev") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>        
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                        ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="ALLAPOT_Nev" HeaderText="�llapot" SortExpression="ALLAPOT_Nev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="KertMinositesNev" HeaderText="K�rt min�s�t�s" SortExpression="KertMinositesNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FelhasznaloKeroNev" HeaderText="Ig�nyl�" SortExpression="FelhasznaloKeroNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="KerelemBeadIdo" HeaderText="Bead�s id�pontja" SortExpression="KerelemBeadIdo">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FelhasznaloDontoNev" HeaderText="Elb�r�l�" SortExpression="FelhasznaloDontoNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="KerelemDontesIdo" HeaderText="D�nt�s id�pontja" SortExpression="KerelemDontesIdo">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                    </asp:BoundField> 
                                                    </Columns>                
                                                  </asp:GridView>
                                                 </asp:Panel>
                                               </td>
                                             </tr>
                                           </table>                                            
                                      </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- T�K adatok -->
                        <ajaxToolkit:TabPanel ID="TUKadatokTabPanel" runat="server" TabIndex="8">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelTUKadatok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="headerTUKadatok" runat="server" Text="T�K adatok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="TUKadatokUpdatePanel" runat="server" OnLoad="TUKadatokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="TUKadatokPanel" runat="server" Visible="true" Width="100%">
                                            <%--BLG_2521--%> 
                                            <uc2:ListHeader ID="ListHeader2" runat="server" />
                                            <uc1:SubListHeader ID="TUKadatokSubListHeader" runat="server" />
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="TUKadatokCPE" runat="server" TargetControlID="panelTUKadatokList"
                                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <asp:Panel ID="panelTUKadatokList" runat="server" printable="true">
                                                            <dl:DokumentumokList ID="DokumentumokLista" runat="server" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>  
                 </asp:Panel> 
                </td>
            </tr>
        </table>
       </td>
      </tr>
   </table>
   <br /> 
</asp:Content>