using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Data;
using Contentum.eRecord.Service;

public partial class Pages_PartnerekList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page

    private string DokumentumId
    {
        get
        {
            return Contentum.eRecord.Utility.QueryStringVars.Get<Guid>(Page, Contentum.eRecord.Utility.QueryStringVars.DokumentumId);
        }
    }

    private bool IsVersionView
    {
        get
        {
            return (!String.IsNullOrEmpty(DokumentumId));
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        FelhasznalokSubListHeader.RowCount_Changed += new EventHandler(FelhasznalokSubListHeader_RowCount_Changed);
        KapcsolatokSubListHeader.RowCount_Changed += new EventHandler(KapcsolatokSubListHeader_RowCount_Changed);
        AlarendeltKapcsolatokSubListHeader.RowCount_Changed += new EventHandler(AlarendeltKapcsolatokSubListHeader_RowCount_Changed);
        BankszamlaszamokSubListHeader.RowCount_Changed += new EventHandler(BankszamlaszamokSubListHeader_RowCount_Changed);
        PostaiCimekSubListHeader.RowCount_Changed += new EventHandler(PostaiCimekSubListHeader_RowCount_Changed);
        EgyebCimekSubListHeader.RowCount_Changed += new EventHandler(EgyebCimekSubListHeader_RowCount_Changed);
        KonszolidacioSubListHeader.RowCount_Changed += new EventHandler(KonszolidacioSubListHeader_RowCount_Changed);
        MinositesekSubListHeader.RowCount_Changed += new EventHandler(MinositesekSubListHeader_RowCount_Changed);
        TUKadatokSubListHeader.RowCount_Changed += new EventHandler(TUKadatokSubListHeader_RowCount_Changed);

        FelhasznalokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(FelhasznalokSubListHeader_ErvenyessegFilter_Changed);
        KapcsolatokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(KapcsolatokSubListHeader_ErvenyessegFilter_Changed);
        AlarendeltKapcsolatokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(AlarendeltKapcsolatokSubListHeader_ErvenyessegFilter_Changed);
        BankszamlaszamokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(BankszamlaszamokSubListHeader_ErvenyessegFilter_Changed);
        PostaiCimekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(PostaiCimekSubListHeader_ErvenyessegFilter_Changed);
        EgyebCimekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(EgyebCimekSubListHeader_ErvenyessegFilter_Changed);
        KonszolidacioSubListHeader.ErvenyessegFilter_Changed += new EventHandler(KonszolidacioSubListHeader_ErvenyessegFilter_Changed);
        MinositesekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(MinositesekSubListHeader_ErvenyessegFilter_Changed);
        TUKadatokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(TUKadatokSubListHeader_ErvenyessegFilter_Changed);

        DokumentumokLista.ScriptManager = ScriptManager1;
        // BLG_2521
        //DokumentumokLista.ListHeader = ListHeader1;
        DokumentumokLista.ListHeader = ListHeader2;
        DokumentumokLista.ListHeader.Visible = false;

        DokumentumokLista.SubListHeader = TUKadatokSubListHeader;
        DokumentumokLista.DokumentumId = DokumentumId;
        DokumentumokLista.IsVersionView = IsVersionView;
        DokumentumokLista.IsPartnerMode = true;

        DokumentumokLista.InitPage();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
       ListHeader1.LeftFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);

        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        // BLG_2521
        //ListHeader1.HeaderLabel = "Partnerek list�ja";
        ListHeader1.HeaderLabel = "T�K Felhaszn�l�k list�ja";
        ListHeader1.SearchObjectType = typeof(KRT_PartnerekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        ListHeader1.PrintHtmlVisible = true;
        ListHeader1.RevalidateVisible = true;
        // BLG_1132
        ExecParam execParam = UI.SetExecParamDefault(Page);
        if (Rendszerparameterek.IsTUK(execParam))
        {
            ListHeader1.PrintVisible = true;
        }
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx", "Command=New"
            , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(PartnerekGridView.ClientID,"","check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(PartnerekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(PartnerekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(PartnerekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(PartnerekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, "", true);


        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = PartnerekGridView;

        FelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FelhasznalokGridView.ClientID);
        FelhasznalokSubListHeader.ButtonsClick += new CommandEventHandler(FelhasznalokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        FelhasznalokSubListHeader.AttachedGridView = FelhasznalokGridView;

        KapcsolatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KapcsolatokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KapcsolatokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KapcsolatokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KapcsolatokGridView.ClientID);
        KapcsolatokSubListHeader.ButtonsClick += new CommandEventHandler(KapcsolatokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        KapcsolatokSubListHeader.AttachedGridView = KapcsolatokGridView;

        AlarendeltKapcsolatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlarendeltKapcsolatokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlarendeltKapcsolatokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlarendeltKapcsolatokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(AlarendeltKapcsolatokGridView.ClientID);
        AlarendeltKapcsolatokSubListHeader.ButtonsClick += new CommandEventHandler(AlarendeltKapcsolatokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        AlarendeltKapcsolatokSubListHeader.AttachedGridView = AlarendeltKapcsolatokGridView;

        BankszamlaszamokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        BankszamlaszamokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        BankszamlaszamokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        BankszamlaszamokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(BankszamlaszamokGridView.ClientID);
        BankszamlaszamokSubListHeader.ButtonsClick += new CommandEventHandler(BankszamlaszamokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        BankszamlaszamokSubListHeader.AttachedGridView = BankszamlaszamokGridView;

        PostaiCimekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PostaiCimekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PostaiCimekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PostaiCimekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(PostaiCimekGridView.ClientID);
        PostaiCimekSubListHeader.ButtonsClick += new CommandEventHandler(PostaiCimekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        PostaiCimekSubListHeader.AttachedGridView = PostaiCimekGridView;

        EgyebCimekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EgyebCimekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EgyebCimekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        EgyebCimekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(EgyebCimekGridView.ClientID);
        EgyebCimekSubListHeader.ButtonsClick += new CommandEventHandler(EgyebCimekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        EgyebCimekSubListHeader.AttachedGridView = EgyebCimekGridView;

        KonszolidacioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KonszolidacioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KonszolidacioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KonszolidacioSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KonszolidacioGridView.ClientID);
        KonszolidacioSubListHeader.ButtonsClick += new CommandEventHandler(KonszolidacioSubListHeader_ButtonsClick);
        //�rv�nyess�g r�di� gombok elt�ntet�se
        KonszolidacioSubListHeader.ValidFilterVisible = false;

        //selectedRecordId kezel�se
        KonszolidacioSubListHeader.AttachedGridView = KonszolidacioGridView;

        MinositesekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        MinositesekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        MinositesekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        MinositesekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(MinositesekGridView.ClientID);
        MinositesekSubListHeader.ButtonsClick += new CommandEventHandler(MinositesekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        MinositesekSubListHeader.AttachedGridView = MinositesekGridView;

        TUKadatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TUKadatokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TUKadatokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TUKadatokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(DokumentumokLista.GridView.ClientID);
        TUKadatokSubListHeader.ButtonsClick += new CommandEventHandler(TUKadatokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        TUKadatokSubListHeader.AttachedGridView = DokumentumokLista.GridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_PartnerekSearch());

        if (!IsPostBack) PartnerekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        ListHeader1.RevalidateOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(PartnerekGridView.ClientID);

        DokumentumokLista.LoadPage();

        DokumentumokLista.GridView.PreRender += DokumentumokGridView_PreRender;
        DokumentumokLista.GridView.Sorting += DokumentumokGridView_Sorting;

        //BUG 2364
        // BLG_2521
        //ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.Invalidate);
        ListHeader1.RevalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.Revalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.Lock);

        FelhasznalokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "FelhasznalokList");
        if (FelhasznalokSubListHeader.NewEnabled)
            FelhasznalokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.New);
        FelhasznalokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.View);
        FelhasznalokSubListHeader.ModifyEnabled = false;
        FelhasznalokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Invalidate);

        KapcsolatokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatokList");
        KapcsolatokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.New);
        KapcsolatokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.View);
        KapcsolatokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.Modify);
        KapcsolatokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.Invalidate);

        AlarendeltKapcsolatokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatokList");
        AlarendeltKapcsolatokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.New);
        AlarendeltKapcsolatokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.View);
        AlarendeltKapcsolatokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.Modify);
        AlarendeltKapcsolatokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + CommandName.Invalidate);

        BankszamlaszamokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "BankszamlaszamokList");
        BankszamlaszamokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Bankszamlaszam" + CommandName.New);
        BankszamlaszamokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Bankszamlaszam" + CommandName.View);
        BankszamlaszamokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Bankszamlaszam" + CommandName.Modify);
        BankszamlaszamokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Bankszamlaszam" + CommandName.Invalidate);

        PostaiCimekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "CimekList");
        PostaiCimekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.New);
        PostaiCimekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.View);
        PostaiCimekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Modify);
        PostaiCimekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Invalidate);

        EgyebCimekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "CimekList");
        EgyebCimekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.New);
        EgyebCimekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.View);
        EgyebCimekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Modify);
        EgyebCimekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Invalidate);

        KonszolidacioPanel.Visible = FunctionRights.GetFunkcioJog(Page, "PartnerKonszolidaciokList");
        KonszolidacioSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerKonszolidacio" + CommandName.New);
        KonszolidacioSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerKonszolidacio" + CommandName.View);
        KonszolidacioSubListHeader.ModifyEnabled = false;
        KonszolidacioSubListHeader.InvalidateEnabled = false;

        MinositesekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "PartnerMinositesekList");
        MinositesekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerMinosites" + CommandName.New);
        MinositesekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerMinosites" + CommandName.View);
        MinositesekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerMinosites" + CommandName.Modify);
        MinositesekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerMinosites" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(PartnerekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerekGridView);

        DokumentumokLista.PreRenderPage();

        #region BLG_5778
        ExecParam execParam = UI.SetExecParamDefault(Page);
        if (Rendszerparameterek.IsTUK(execParam))
        {
            BankszamlaszamokTabPanel.Visible = false;
            PostaiCimekTabPanel.Visible = false;
            EgyebCimekTabPanel.Visible = false;
            KonszolidacioTabPanel.Visible = false;
        }
        #endregion
    }

    #endregion

    #region Master List

    protected void PartnerekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("PartnerekGridView", ViewState, "KRT_Partnerek.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PartnerekGridView", ViewState);

        PartnerekGridViewBind(sortExpression, sortDirection);
    }

    protected void PartnerekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        KRT_PartnerekSearch search = (KRT_PartnerekSearch)Search.GetSearchObject(Page, new KRT_PartnerekSearch());
        search.OrderBy = Search.GetOrderBy("PartnerekGridView", ViewState, SortExpression, SortDirection);

        // BLG_2521
        search.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
        search.Tipus.Operator = Query.Operators.equals;
        search.Belso.Value = Constants.Database.Yes;
        search.Belso.Operator = Query.Operators.equals;

        if (Search.IsSearchObjectInSession(Page, typeof(KRT_PartnerekSearch)) == false)
        {
            Search.SetSearchObject(Page, search);
        }

        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        bool isTUK = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.TUK, false);

        UI.GridViewFill(PartnerekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void PartnerekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbBelso", "Belso");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void PartnerekGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpePartnerek);

        ListHeader1.RefreshPagerLabel();
    }

    private void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        PartnerekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpePartnerek);
        PartnerekGridViewBind();
    }

    protected void PartnerekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PartnerekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
               , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            string tableName = "KRT_Partnerek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, PartnerekGridView.ClientID);

            FelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Partner_Felhasznalo_Form.aspx"
               , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            KapcsolatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerkapcsolatokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id + "&" + QueryStringVars.PartnerKapcsolatTipus + "=" + Constants.PartnerKapcsolatTipus.normal
                , Defaults.PopupWidth, Defaults.PopupHeight, KapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            AlarendeltKapcsolatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerkapcsolatokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id + "&" + QueryStringVars.PartnerKapcsolatTipus + "=" + Constants.PartnerKapcsolatTipus.alarendelt
                , Defaults.PopupWidth, Defaults.PopupHeight, AlarendeltKapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            BankszamlaszamokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("BankszamlaszamokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, BankszamlaszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            PostaiCimekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id + "&" + QueryStringVars.Filter + "=" + KodTarak.Cim_Tipus.Postai
                , Defaults.PopupWidth, Defaults.PopupHeight, PostaiCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            EgyebCimekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id + "&" + QueryStringVars.Filter + "=" + KodTarak.Cim_Tipus.Egyeb
                , Defaults.PopupWidth, Defaults.PopupHeight, EgyebCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            KonszolidacioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerkonszolidacioForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
                , Defaults.PopupWidth_1900, Defaults.PopupHeight_Max, KonszolidacioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            MinositesekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerminositesekForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, MinositesekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            TUKadatokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Contentum.eRecord.Utility.Constants.Startup.FromPartnerekList
                , Defaults.PopupWidth, Defaults.PopupHeight, TUKadatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            // BLG_2521
            //ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            // BLG_1132
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result_partnerGet = service.Get(execParam);
            if (result_partnerGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partnerGet);
                ErrorUpdatePanel.Update();
            }

            KRT_Partnerek partnerObj = (KRT_Partnerek)result_partnerGet.Record;

            ListHeader1.PrintOnClientClick = "javascript:window.open('" + GetPrintUrl(partnerObj) + "')";

        }
    }

    // BLG_1132
    private string GetPrintUrl(KRT_Partnerek partner)
    {
        string url = "PartnerekPrintForm.aspx?" + QueryStringVars.PartnerId + "=" + partner.Id;
        //if (!partner.Base.Typed.LetrehozasIdo.IsNull)
        //{
        //    url += "&" + QueryStringVars.Ev + "=" + ugyirat.Base.Typed.LetrehozasIdo.Value.Year.ToString();
        //}
        return url;
    }

    protected void PartnerekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    PartnerekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    break;
            }
        }

    }

    private void deleteSelectedPartner()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(PartnerekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void PartnerekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PartnerekGridViewBind(e.SortExpression, UI.GetSortToGridView("PartnerekGridView", ViewState, e.SortExpression));
    }

    private void RevalidateSelectedPartnerek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerRevalidate"))
        {
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(PartnerekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            Result res = new Result();
            Result resError = new Result();
            int errorCount = 0;
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                xpm.Record_Id = selectedItemsList[i];
                res = service.Revalidate(xpm);
                if (res.IsError)
                {
                    resError = res;
                    errorCount++;
                }
            }

            if (errorCount > 0)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                if (errorCount > 1)
                {
                    EErrorPanel1.Header = EErrorPanel1.Header + " (Hib�k sz�ma: " + errorCount.ToString() + ")";
                }
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    #endregion

    #region Mapping ListHeader Function Button Events

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedPartner();
            PartnerekGridViewBind();
        }
        else if (e.CommandName == CommandName.Revalidate)
        {
            RevalidateSelectedPartnerek();
            PartnerekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedPartnerRecords();
                PartnerekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedPartnerRecords();
                PartnerekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedPartnerek();
                break;
        }
    }


    private void LockSelectedPartnerRecords()
    {
        LockManager.LockSelectedGridViewRecords(PartnerekGridView, "KRT_Partnerek"
            , "PartnerLock", "PartnerForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedPartnerRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(PartnerekGridView, "KRT_Partnerek"
            , "PartnerLock", "PartnerForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void SendMailSelectedPartnerek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(PartnerekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Partnerek");
        }
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(PartnerekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (PartnerekGridView.SelectedIndex == -1)
        {
            return;
        }

        string PartnerId = UI.GetGridViewSelectedRecordId(PartnerekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, PartnerId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string PartnerId)
    {
        //BUG 6198
        //Az ActiveTab -ban val�j�ban az adott poz�ci�n l�v� tab adatai vannak, teh�t az elrejtett tabok miatt l�tre kell hozni egy list�t a l�that� tabokr�l, �s azok k�z�tt kell megkeresni a tabindexet
        switch (sender.GetRealActiveTab().TabIndex)
        {
            case 0:
                FelhasznalokGridViewBind(PartnerId);
                FelhasznalokPanel.Visible = true;
                break;
            case 1:
                KapcsolatokGridViewBind(PartnerId);
                KapcsolatokPanel.Visible = true;
                break;
            case 2:
                AlarendeltKapcsolatokGridViewBind(PartnerId);
                AlarendeltKapcsolatokPanel.Visible = true;
                break;
            case 3:
                BankszamlaszamokGridViewBind(PartnerId);
                BankszamlaszamokPanel.Visible = true;
                break;
            case 4:
                PostaiCimekGridViewBind(PartnerId);
                PostaiCimekPanel.Visible = true;
                break;
            case 5:
                EgyebCimekGridViewBind(PartnerId);
                EgyebCimekPanel.Visible = true;
                break;
            case 6:
                KonszolidacioGridViewBind(PartnerId);
                KonszolidacioPanel.Visible = true;
                break;
            case 7:
                MinositesekGridViewBind(PartnerId);
                MinositesekPanel.Visible = true;
                break;
            case 8:
                DokumentumokGridViewBind(PartnerId);
                DokumentumokLista.PartnerId = PartnerId;
                TUKadatokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.GetRealActiveTab().TabIndex)
        {
            case 0:
                ui.GridViewClear(FelhasznalokGridView);
                break;
            case 1:
                ui.GridViewClear(KapcsolatokGridView);
                break;
            case 2:
                ui.GridViewClear(AlarendeltKapcsolatokGridView);
                break;
            case 3:
                ui.GridViewClear(BankszamlaszamokGridView);
                break;
            case 4:
                ui.GridViewClear(PostaiCimekGridView);
                break;
            case 5:
                ui.GridViewClear(EgyebCimekGridView);
                break;
            case 6:
                ui.GridViewClear(KonszolidacioGridView);
                break;
            case 7:
                ui.GridViewClear(MinositesekGridView);
                break;
            case 8:
                ui.GridViewClear(DokumentumokLista.GridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        FelhasznalokSubListHeader.RowCount = RowCount;
        KapcsolatokSubListHeader.RowCount = RowCount;
        AlarendeltKapcsolatokSubListHeader.RowCount = RowCount;
        BankszamlaszamokSubListHeader.RowCount = RowCount;
        PostaiCimekSubListHeader.RowCount = RowCount;
        EgyebCimekSubListHeader.RowCount = RowCount;
        KonszolidacioSubListHeader.RowCount = RowCount;
        MinositesekSubListHeader.RowCount = RowCount;
        TUKadatokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(FelhasznalokTabPanel))
        {
            FelhasznalokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(KapcsolatokTabPanel))
        {
            KapcsolatokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(KapcsolatokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(AlarendeltKapcsolatokTabPanel))
        {
            AlarendeltKapcsolatokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(AlarendeltKapcsolatokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(BankszamlaszamokTabPanel))
        {
            BankszamlaszamokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(BankszamlaszamokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(PostaiCimekTabPanel))
        {
            PostaiCimekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(PostaiCimekGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(EgyebCimekTabPanel))
        {
            EgyebCimekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(EgyebCimekGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(KonszolidacioTabPanel))
        {
            KonszolidacioGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(KonszolidacioGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(MinositesekTabPanel))
        {
            MinositesekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(MinositesekGridView));
        }

    }




    #endregion

    #region Felhasznalok Detail

    //Data Binding(2)
    protected void FelhasznalokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FelhasznalokGridView", ViewState, "KRT_Felhasznalok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FelhasznalokGridView", ViewState);

        FelhasznalokGridViewBind(PartnerId, sortExpression, sortDirection);

    }

    protected void FelhasznalokGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
            search.Partner_id.Value = PartnerId;
            search.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("FelhasznalokGridView", ViewState, SortExpression, SortDirection);

            FelhasznalokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAll(ExecParam, search);

            //Ha tartozik m�r a partnerhez felhaszn�l� nem lehet �jat felvenni
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                if (res.Ds != null && res.Ds.Tables[0] != null)
                {
                    if (res.Ds.Tables[0].Rows.Count > 0)
                    {
                        FelhasznalokSubListHeader.NewEnabled = false;
                    }
                    else
                    {
                        FelhasznalokSubListHeader.NewEnabled = true;
                    }
                }
            }

            UI.GridViewFill(FelhasznalokGridView, res, FelhasznalokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerFelhasznalok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);
        }
        else
        {
            ui.GridViewClear(FelhasznalokGridView);
        }

    }

    //SubListHeader f�ggv�nyei(4)
    private void FelhasznalokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedFelhasznalokFromPartner();
            FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedFelhasznalokFromPartner()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FelhasznaloModify"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiDeletePartnerID(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void FelhasznalokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void FelhasznalokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(FelhasznalokSubListHeader.RowCount);
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    ////specialis, nem mindig kell kell
    protected void FelhasznalokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
            , "cb_System", "System");
    }

    protected void FelhasznalokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FelhasznalokGridView, selectedRowNumber, "check");
        }
    }

    protected void FelhasznalokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FelhasznalokGridView.PageIndex;

        FelhasznalokGridView.PageIndex = FelhasznalokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != FelhasznalokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, FelhasznalokCPE);
            FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(FelhasznalokSubListHeader.Scrollable, FelhasznalokCPE);
        }
        FelhasznalokSubListHeader.PageCount = FelhasznalokGridView.PageCount;
        FelhasznalokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(FelhasznalokGridView);


        ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);

    }

    protected void FelhasznalokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("FelhasznalokGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void FelhasznalokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(FelhasznalokTabPanel))
                    //{
                    //    FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(FelhasznalokTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void FelhasznalokGridView_RefreshOnClientClicks(string Partner_kapcsolat_Id)
    {
        string id = Partner_kapcsolat_Id;
        if (!String.IsNullOrEmpty(id))
        {
            FelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Partner_Felhasznalo_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    #endregion

    #region Kapcsolatok Detail

    //Data Binding(2)
    protected void KapcsolatokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KapcsolatokGridView", ViewState, "Partner_kapcsolt_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KapcsolatokGridView", ViewState);

        KapcsolatokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void KapcsolatokGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
            search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.nemnormal;
            search.Tipus.Operator = Query.Operators.notinner;
            search.OrderBy = Search.GetOrderBy("KapcsolatokGridView", ViewState, SortExpression, SortDirection);

            KapcsolatokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(ExecParam, partner, search);
            UI.GridViewFill(KapcsolatokGridView, res, KapcsolatokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerKapcsolatok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(KapcsolatokGridView);
        }
        else
        {
            ui.GridViewClear(KapcsolatokGridView);
        }


    }

    //SubListHeader f�ggv�nyei(4)
    private void KapcsolatokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKapcsolatok();
            KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedKapcsolatok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(KapcsolatokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void KapcsolatokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    void KapcsolatokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(KapcsolatokSubListHeader.RowCount);
        KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    protected void KapcsolatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KapcsolatokGridView, selectedRowNumber, "check");
        }
    }

    protected void KapcsolatokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = KapcsolatokGridView.PageIndex;

        KapcsolatokGridView.PageIndex = KapcsolatokSubListHeader.PageIndex;

        if (prev_PageIndex != KapcsolatokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, KapcsolatokCPE);
            KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(KapcsolatokSubListHeader.Scrollable, KapcsolatokCPE);
        }

        KapcsolatokSubListHeader.PageCount = KapcsolatokGridView.PageCount;
        KapcsolatokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(KapcsolatokGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(KapcsolatokGridView);
    }

    protected void KapcsolatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("KapcsolatokGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void KapcsolatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(KapcsolatokTabPanel))
                    //{
                    //    KapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(KapcsolatokTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void KapcsolatokGridView_RefreshOnClientClicks(string Partner_kapcsolat_Id)
    {
        string id = Partner_kapcsolat_Id;
        if (!String.IsNullOrEmpty(id))
        {
            KapcsolatokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerkapcsolatokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, KapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            KapcsolatokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerkapcsolatokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, KapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    #endregion

    #region AlarendeltKapcsolatok Detail

    //Data Binding(2)
    protected void AlarendeltKapcsolatokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("AlarendeltKapcsolatokGridView", ViewState, "Partner_kapcsolt_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("AlarendeltKapcsolatokGridView", ViewState);

        AlarendeltKapcsolatokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void AlarendeltKapcsolatokGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
            search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.alarendelt;
            search.Tipus.Operator = Query.Operators.inner;
            search.OrderBy = Search.GetOrderBy("AlarendeltKapcsolatokGridView", ViewState, SortExpression, SortDirection);

            AlarendeltKapcsolatokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(ExecParam, partner, search);
            UI.GridViewFill(AlarendeltKapcsolatokGridView, res, AlarendeltKapcsolatokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerAlarendeltKapcsolatok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(AlarendeltKapcsolatokGridView);
        }
        else
        {
            ui.GridViewClear(AlarendeltKapcsolatokGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void AlarendeltKapcsolatokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedAlarendeltKapcsolatok();
            AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedAlarendeltKapcsolatok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(AlarendeltKapcsolatokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void AlarendeltKapcsolatokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void AlarendeltKapcsolatokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(AlarendeltKapcsolatokSubListHeader.RowCount);
        AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    protected void AlarendeltKapcsolatokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = AlarendeltKapcsolatokGridView.PageIndex;

        AlarendeltKapcsolatokGridView.PageIndex = AlarendeltKapcsolatokSubListHeader.PageIndex;

        if (prev_PageIndex != AlarendeltKapcsolatokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, AlarendeltKapcsolatokCPE);
            AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(AlarendeltKapcsolatokSubListHeader.Scrollable, AlarendeltKapcsolatokCPE);
        }

        AlarendeltKapcsolatokSubListHeader.PageCount = AlarendeltKapcsolatokGridView.PageCount;
        AlarendeltKapcsolatokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(AlarendeltKapcsolatokGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(AlarendeltKapcsolatokGridView);
    }

    protected void AlarendeltKapcsolatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("AlarendeltKapcsolatokGridView", ViewState, e.SortExpression));
    }

    protected void AlarendeltKapcsolatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(AlarendeltKapcsolatokGridView, selectedRowNumber, "check");
        }
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void AlarendeltKapcsolatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(AlarendeltKapcsolatokTabPanel))
                    //{
                    //    AlarendeltKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(AlarendeltKapcsolatokTabPanel);
                    break;
            }
        }

    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void AlarendeltKapcsolatokGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            AlarendeltKapcsolatokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerKapcsolatokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AlarendeltKapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            AlarendeltKapcsolatokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerKapcsolatokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AlarendeltKapcsolatokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }



    #endregion

    #region Banksz�mlasz�mok

    //Data Binding(2)
    protected void BankszamlaszamokGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "Bankszamlaszam";
        String sortExpression = Search.GetSortExpressionFromViewState("BankszamlaszamokGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("BankszamlaszamokGridView", ViewState);

        BankszamlaszamokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void BankszamlaszamokGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;

            KRT_BankszamlaszamokSearch search = new KRT_BankszamlaszamokSearch();

            search.Partner_Id.Value = PartnerId;
            search.Partner_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("BankszamlaszamokGridView", ViewState, SortExpression, SortDirection);

            BankszamlaszamokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(execParam, search);

            UI.GridViewFill(BankszamlaszamokGridView, res, BankszamlaszamokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerBankszamlaszamok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(BankszamlaszamokGridView);
        }
        else
        {
            ui.GridViewClear(BankszamlaszamokGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void BankszamlaszamokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedBankszamlaszamok();
            BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedBankszamlaszamok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "BankszamlaszamInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(BankszamlaszamokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void BankszamlaszamokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void BankszamlaszamokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(BankszamlaszamokSubListHeader.RowCount);
        BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    //specialis, nem mindig kell kell
    protected void BankszamlaszamokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ////spec. mez�k be�ll�t�sa
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    DataRowView drv = (DataRowView)e.Row.DataItem;
        //}

    }

    protected void BankszamlaszamokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(BankszamlaszamokGridView, selectedRowNumber, "check");
        }
    }

    protected void BankszamlaszamokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = BankszamlaszamokGridView.PageIndex;

        BankszamlaszamokGridView.PageIndex = BankszamlaszamokSubListHeader.PageIndex;

        if (prev_PageIndex != BankszamlaszamokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, BankszamlaszamokCPE);
            BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(BankszamlaszamokSubListHeader.Scrollable, BankszamlaszamokCPE);
        }

        BankszamlaszamokSubListHeader.PageCount = BankszamlaszamokGridView.PageCount;
        BankszamlaszamokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(BankszamlaszamokGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(BankszamlaszamokGridView);
    }

    protected void BankszamlaszamokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("BankszamlaszamokGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void BankszamlaszamokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(BankszamlaszamokTabPanel))
                    //{
                    //    BankszamlaszamokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(BankszamlaszamokTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void BankszamlaszamokGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            BankszamlaszamokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("BankszamlaszamokForm.aspx"
              , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
              , Defaults.PopupWidth, Defaults.PopupHeight, BankszamlaszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            BankszamlaszamokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("BankszamlaszamokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, BankszamlaszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    #endregion Banksz�mlasz�mok

    #region Postai Cimek

    //Data Binding(2)
    protected void PostaiCimekGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev";
        String sortExpression = Search.GetSortExpressionFromViewState("PostaiCimekGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PostaiCimekGridView", ViewState);

        PostaiCimekGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void PostaiCimekGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;

            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.OrderBy = Search.GetOrderBy("PostaiCimekGridView", ViewState, SortExpression, SortDirection);

            PostaiCimekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(execParam, partner, KodTarak.Cim_Tipus.Postai, search);

            UI.GridViewFill(PostaiCimekGridView, res, PostaiCimekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerPostaiCimek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(PostaiCimekGridView);
        }
        else
        {
            ui.GridViewClear(PostaiCimekGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void PostaiCimekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedPostaiCimek();
            PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedPostaiCimek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerCimInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(PostaiCimekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void PostaiCimekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void PostaiCimekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(PostaiCimekSubListHeader.RowCount);
        PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    //specialis, nem mindig kell kell
    protected void PostaiCimekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //a t�bb oszlopb�l vett mez�k be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            string hazszamIg = drv["Hazszamig"].ToString();
            string hazszamBetujel = drv["HazszamBetujel"].ToString();
            string ajtoBetujel = drv["AjtoBetujel"].ToString();

            Label hazszam = (Label)e.Row.FindControl("labelHazszam");
            Label ajto = (Label)e.Row.FindControl("labelAjto");

            if (!String.IsNullOrEmpty(hazszamIg))
            {
                if (hazszam != null)
                    hazszam.Text += "-" + hazszamIg;
            }
            if (!String.IsNullOrEmpty(hazszamBetujel))
            {
                if (hazszam != null)
                    hazszam.Text += "/" + hazszamBetujel;
            }
            if (!String.IsNullOrEmpty(ajtoBetujel))
            {
                if (ajto != null)
                    ajto.Text += "/" + ajtoBetujel;
            }

        }

    }

    protected void PostaiCimekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PostaiCimekGridView, selectedRowNumber, "check");
        }
    }

    protected void PostaiCimekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = PostaiCimekGridView.PageIndex;

        PostaiCimekGridView.PageIndex = PostaiCimekSubListHeader.PageIndex;

        if (prev_PageIndex != PostaiCimekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, PostaiCimekCPE);
            PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(PostaiCimekSubListHeader.Scrollable, PostaiCimekCPE);
        }

        PostaiCimekSubListHeader.PageCount = PostaiCimekGridView.PageCount;
        PostaiCimekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(PostaiCimekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(PostaiCimekGridView);
    }

    protected void PostaiCimekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("PostaiCimekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void PostaiCimekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(PostaiCimekTabPanel))
                    //{
                    //    PostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(PostaiCimekTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void PostaiCimekGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            PostaiCimekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
              , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
              , Defaults.PopupWidth, Defaults.PopupHeight, PostaiCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            PostaiCimekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.Filter + "=" + KodTarak.Cim_Tipus.Postai
                , Defaults.PopupWidth, Defaults.PopupHeight, PostaiCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    #endregion

    #region Egyeb Cimek

    //Data Binding(2)
    protected void EgyebCimekGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "TipusNev, CimTobbi";
        String sortExpression = Search.GetSortExpressionFromViewState("EgyebCimekGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("EgyebCimekGridView", ViewState);

        EgyebCimekGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void EgyebCimekGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;

            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.OrderBy = Search.GetOrderBy("EgyebCimekGridView", ViewState, SortExpression, SortDirection);

            string WhereByManual = "KRT_Cimek.Tipus " + Query.Operators.notequals + " '" + KodTarak.Cim_Tipus.Postai+"'";

            EgyebCimekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Query query = new Query();
            query.BuildFromBusinessDocument(search);

            if (query.Where == "")
                search.WhereByManual = WhereByManual;
            else
                search.WhereByManual = " and " + WhereByManual;

            Result res = service.GetAllByPartner(execParam, partner, "", search);

            UI.GridViewFill(EgyebCimekGridView, res, EgyebCimekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, labelEgyebCimek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(EgyebCimekGridView);
        }
        else
        {
            ui.GridViewClear(EgyebCimekGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void EgyebCimekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedEgyebCimek();
            EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedEgyebCimek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerCimInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(EgyebCimekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void EgyebCimekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void EgyebCimekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(EgyebCimekSubListHeader.RowCount);
        EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    protected void EgyebCimekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(EgyebCimekGridView, selectedRowNumber, "check");
        }
    }

    protected void EgyebCimekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = EgyebCimekGridView.PageIndex;

        EgyebCimekGridView.PageIndex = EgyebCimekSubListHeader.PageIndex;

        if (prev_PageIndex != EgyebCimekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, EgyebCimekCPE);
            EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(EgyebCimekSubListHeader.Scrollable, EgyebCimekCPE);
        }

        EgyebCimekSubListHeader.PageCount = EgyebCimekGridView.PageCount;
        EgyebCimekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(EgyebCimekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(EgyebCimekGridView);
    }

    protected void EgyebCimekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("EgyebCimekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void EgyebCimekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(EgyebCimekTabPanel))
                    //{
                    //    EgyebCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(EgyebCimekTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void EgyebCimekGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            EgyebCimekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
              , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
              , Defaults.PopupWidth, Defaults.PopupHeight, EgyebCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            EgyebCimekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnercimekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.Filter + "=" + KodTarak.Cim_Tipus.Egyeb
                , Defaults.PopupWidth, Defaults.PopupHeight, EgyebCimekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    #endregion

    #region Konszolidacio Detail

    //Data Binding(2)
    protected void KonszolidacioGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KonszolidacioGridView", ViewState, "Partner_kapcsolt_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KonszolidacioGridView", ViewState);

        KonszolidacioGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void KonszolidacioGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
            search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.konszolidalt;
            search.Tipus.Operator = Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("KonszolidacioGridView", ViewState, SortExpression, SortDirection);

            KonszolidacioSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(ExecParam, partner, search);
            UI.GridViewFill(KonszolidacioGridView, res, KonszolidacioSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerKonszolidacio);

            ui.SetClientScriptToGridViewSelectDeSelectButton(KonszolidacioGridView);
        }
        else
        {
            ui.GridViewClear(KonszolidacioGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void KonszolidacioSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
    }

    private void deleteSelectedKonszolidaciok()
    {
    }

    private void KonszolidacioSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        KonszolidacioGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void KonszolidacioSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(KonszolidacioSubListHeader.RowCount);
        KonszolidacioGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    protected void KonszolidacioGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KonszolidacioGridView, selectedRowNumber, "check");

        }
    }

    protected void KonszolidacioGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = KonszolidacioGridView.PageIndex;

        KonszolidacioGridView.PageIndex = KonszolidacioSubListHeader.PageIndex;

        if (prev_PageIndex != KonszolidacioGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, KonszolidacioCPE);
            KonszolidacioGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(KonszolidacioSubListHeader.Scrollable, KonszolidacioCPE);
        }

        KonszolidacioSubListHeader.PageCount = KonszolidacioGridView.PageCount;
        KonszolidacioSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(KonszolidacioGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(KonszolidacioGridView);

    }

    protected void KonszolidacioGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KonszolidacioGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("KonszolidacioGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void KonszolidacioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(KonszolidacioTabPanel))
                    //{
                    //    KonszolidacioGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(KonszolidacioTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void KonszolidacioGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            KonszolidacioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerkonszolidacioForm.aspx"
          , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
          , Defaults.PopupWidth, Defaults.PopupHeight, KonszolidacioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            KonszolidacioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerkonszolidacioForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_1900, Defaults.PopupHeight_Max, KonszolidacioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }



    #endregion

    #region Minositesek Detail

    //Data Binding(2)
    protected void MinositesekGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MinositesekGridView", ViewState, "ALLAPOT");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MinositesekGridView", ViewState);

        MinositesekGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void MinositesekGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_PartnerMinositesekService service = eAdminService.ServiceFactory.GetKRT_PartnerMinositesekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;
            KRT_PartnerMinositesekSearch search = new KRT_PartnerMinositesekSearch();
            search.OrderBy = Search.GetOrderBy("MinositesekGridView", ViewState, SortExpression, SortDirection);

            MinositesekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(ExecParam, partner, search);

            UI.GridViewFill(MinositesekGridView, res, MinositesekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerMinositesek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(MinositesekGridView);
        }
        else
        {
            ui.GridViewClear(MinositesekGridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void MinositesekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedMinositesek();
            MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedMinositesek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerMinositesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(MinositesekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerMinositesekService service = eAdminService.ServiceFactory.GetKRT_PartnerMinositesekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void MinositesekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void MinositesekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(MinositesekSubListHeader.RowCount);
        MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    protected void MinositesekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MinositesekGridView, selectedRowNumber, "check");
        }
    }

    protected void MinositesekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = MinositesekGridView.PageIndex;

        MinositesekGridView.PageIndex = MinositesekSubListHeader.PageIndex;

        if (prev_PageIndex != MinositesekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, MinositesekCPE);
            MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(MinositesekSubListHeader.Scrollable, MinositesekCPE);
        }

        MinositesekSubListHeader.PageCount = MinositesekGridView.PageCount;
        MinositesekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(MinositesekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(MinositesekGridView);
    }

    protected void MinositesekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView)
            , e.SortExpression, UI.GetSortToGridView("MinositesekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void MinositesekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(MinositesekTabPanel))
                    //{
                    //    MinositesekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    //}
                    ActiveTabRefreshDetailList(MinositesekTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void MinositesekGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            MinositesekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerMinositesekForm.aspx"
               , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, MinositesekUpdatePanel.ClientID);
            MinositesekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerMinositesekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, MinositesekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    #endregion

    #region TUKadatok Detail

    Dictionary<string, string> Partner_Dokumentumok_Ids
    {
        get
        {
            object o = ViewState["Partner_Dokumentumok_Ids"];

            if (o == null)
            {
                o = ViewState["Partner_Dokumentumok_Ids"] = new Dictionary<string, string>();
            }

            return o as Dictionary<string, string>;
        }
    }

    //Data Binding(2)
    protected void DokumentumokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("DokumentumokLista.GridView", ViewState, "KRT_Dokumentumok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("DokumentumokLista.GridView", ViewState, SortDirection.Descending);

        DokumentumokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void DokumentumokGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_Partner_DokumentumokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_Partner_DokumentumokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_Partner_DokumentumokSearch search = new KRT_Partner_DokumentumokSearch();
            search.Partner_id.Value = PartnerId;
            search.Partner_id.Operator = Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("DokumentumokLista.GridView", ViewState, SortExpression, SortDirection);

            TUKadatokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);

            UI.GridViewFill(DokumentumokLista.GridView, res, TUKadatokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerTUKadatok);

            ui.SetClientScriptToGridViewSelectDeSelectButton(DokumentumokLista.GridView);

            Partner_Dokumentumok_Ids.Clear();
            if (!res.IsError)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    string dokumentumId = row["Id"].ToString();
                    string partner_Dokumentumok_Id = row["Partner_Dokumentumok_Id"].ToString();

                    if (!Partner_Dokumentumok_Ids.ContainsKey(dokumentumId))
                    {
                        Partner_Dokumentumok_Ids.Add(dokumentumId, partner_Dokumentumok_Id);
                    }
                }
            }
        }
        else
        {
            ui.GridViewClear(DokumentumokLista.GridView);
            Partner_Dokumentumok_Ids.Clear();
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void TUKadatokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTUKadatok();
            DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
    }

    private void deleteSelectedTUKadatok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerMinositesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(DokumentumokLista.GridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_Partner_DokumentumokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_Partner_DokumentumokService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                string dokumentumId = deletableItemsList[i];
                if (Partner_Dokumentumok_Ids.ContainsKey(dokumentumId))
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = Partner_Dokumentumok_Ids[dokumentumId];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void TUKadatokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    private void TUKadatokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(TUKadatokSubListHeader.RowCount);
        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void TUKadatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainer1.ActiveTab.Equals(TUKadatokTabPanel))
                    {
                        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
                    }
                    ActiveTabRefreshDetailList(TUKadatokTabPanel);
                    break;
            }
        }
    }

    protected void DokumentumokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = DokumentumokLista.GridView.PageIndex;

        DokumentumokLista.GridView.PageIndex = TUKadatokSubListHeader.PageIndex;

        if (prev_PageIndex != DokumentumokLista.GridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, TUKadatokCPE);
            DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(TUKadatokSubListHeader.Scrollable, TUKadatokCPE);
        }

        TUKadatokSubListHeader.PageCount = DokumentumokLista.GridView.PageCount;
        TUKadatokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(DokumentumokLista.GridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(DokumentumokLista.GridView);
    }

    protected void DokumentumokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (sortExpression == "TipusKodTarak.Nev")
        {
            sortExpression = "Tipus_Nev";
        }
        else if (sortExpression == "FormatumKodTarak.Nev")
        {
            sortExpression = "Formatum_Nev";
        }

        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerekGridView), sortExpression, UI.GetSortToGridView("DokumentumokLista.GridView", ViewState, sortExpression));
    }

    #endregion
}
