<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerekLovList.aspx.cs" Inherits="PartnerekLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc7" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>

<%@ Register Src="eRecordComponent/ElosztoIvCimzettListaControl.ascx" TagName="ElosztoIvCimzettListaControl" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,PartnerekLovListHeaderTitle%>" />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <!--   // CR3321 Partner r�gz�t�s csak keres�s ut�n -->
                    <asp:TextBox ID="PartnerMegnevezes" runat="server" EnableViewState="true" OnTextChanged="PartnerMegnevezes_ValueChanged" Style="display: none"></asp:TextBox>
                    <asp:HiddenField ID="PartnerHiddenField" runat="server" />

                    <asp:TextBox ID="CimTextBox" runat="server" Style="display: none" />
                    <asp:HiddenField ID="CimHiddenField" runat="server" />
                    <%--CR3321 end--%>
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="radioButtonList_Mode" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:RadioButtonList ID="radioButtonList_Mode" runat="server"
                                RepeatDirection="Horizontal" Width="200px" Visible="false" AutoPostBack="true">
                                <asp:ListItem Value="P" Selected="True">Partnerek</asp:ListItem>
                                <asp:ListItem Value="E">Eloszt��vek</asp:ListItem>
                            </asp:RadioButtonList>
                            <br />

                            <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                            <asp:HiddenField ID="GridViewSelectedId" runat="server" />

                            <table id="partnerekTable" style="width: 100%" cellspacing="0" cellpadding="0" border="0" runat="server">
                                <tbody>
                                    <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Partnern�v:"></asp:Label><br />
                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%">*</asp:TextBox>
                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"></asp:ImageButton>
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" AlternateText="R�szletes keres�s" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; padding-left: 5px;">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeCimSearch" runat="server" CollapsedText="" ExpandedText="" ExpandedSize="0" ImageControlID="btnCpeCimSearch"
                                                ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif" AutoExpand="false" AutoCollapse="false" ExpandDirection="Vertical" CollapseControlID="btnCpeCimSearch"
                                                ExpandControlID="btnCpeCimSearch" Collapsed="True" CollapsedSize="25" TargetControlID="panelCimSearch">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel Style="padding-top: 3px" ID="panelCimSearch" runat="server" Width="97.5%">
                                                <div style="display: inline; position: absolute; margin-left: -9px;">
                                                    <img id="btnCpeCimSearch" src="images/hu/Grid/minus.gif" alt="" runat="server" style="cursor: pointer;" />
                                                </div>
                                                <ajaxToolkit:TabContainer ID="TabContainerCimSearch" runat="server" Width="100%">
                                                    <!-- Postai c�mek -->
                                                    <ajaxToolkit:TabPanel ID="TabPanelPostai" runat="server" TabIndex="0">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="labelPostaiHeader" runat="server" Text="Postai c�mek"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ContentTemplate>
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapSpacer">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapCaption">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelTelepules" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc4:TelepulesTextBox ID="TelepulesTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc5:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelKozterulet" runat="server" Text="K�zter�let neve:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc6:KozteruletTextBox ID="KozteruletTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelKozteruletTipus" runat="server" Text="K�zter�let tipusa:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc7:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelHazszam" runat="server" Text="H�zsz�m:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <asp:TextBox ID="textHazszam" runat="server" CssClass="mrUrlapInput" Width="100px" Wrap="False"></asp:TextBox></td>
                                                                        <td class="mrUrlapMezo" colspan="3"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </ContentTemplate>
                                                    </ajaxToolkit:TabPanel>
                                                    <!-- Egy�b c�mek -->
                                                    <ajaxToolkit:TabPanel ID="TabPanelEgyeb" runat="server" TabIndex="1">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="labelEgyebHeader" runat="server" Text="Egy�b c�mek"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ContentTemplate>
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapSpacer">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapCaption">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc8:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption"></td>
                                                                        <td class="mrUrlapMezo"></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption">
                                                                            <asp:Label ID="labelEgyebCim" runat="server" Text="Tartalom:"></asp:Label></td>
                                                                        <td class="mrUrlapMezo">
                                                                            <asp:TextBox ID="textEgyebCim" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox></td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption"></td>
                                                                        <td class="mrUrlapMezo"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </ContentTemplate>
                                                    </ajaxToolkit:TabPanel>
                                                </ajaxToolkit:TabContainer>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="LovListCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server" Width="95%">
                                                <table style="width: 102%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" id="headerBackGround" class="GridViewHeaderBackGroundStyle" runat="server">

                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <%-- <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" /> --%>

                                                                <asp:GridView
                                                                    ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle" PageSize="5000"
                                                                    DataKeyNames="Id" AutoGenerateColumns="False" AllowSorting="False"
                                                                    PagerSettings-Visible="false" AllowPaging="True" GridLines="Both"
                                                                    BorderWidth="1" CellSpacing="0" CellPadding="0"
                                                                    OnSelectedIndexChanging="GridViewSearchResult_SelectedIndexChanging"
                                                                    OnPreRender="GridViewSearchResult_PreRender">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <SelectedRowStyle CssClass="GridViewLovListSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Nev" HeaderText="Partner neve" SortExpression="Nev">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="200px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CimId" SortExpression="CimId">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Cim" HeaderText="Partner c&#237;me" SortExpression="Nev">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="70%" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <PagerSettings Visible="False" />
                                                                    <EmptyDataTemplate>
                                                                        <asp:Label runat="server" Text="<%$Resources:Search,NoSearchResult%>"></asp:Label>
                                                                    </EmptyDataTemplate>
                                                                    <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                                                </asp:GridView>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table id="elosztoivekTable" style="width: 90%" cellspacing="0" cellpadding="0" border="0" runat="server">
                                <tbody>
                                    <tr id="tr1" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="label2" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Eloszt��v neve:"></asp:Label><br />
                                            <asp:TextBox ID="TextBox1" runat="server" Width="50%"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton1" OnClick="ButtonElosztoivekSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"></asp:ImageButton>
                                            <asp:ImageButton ID="ImageButton2" runat="server" AlternateText="R�szletes keres�s" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel3" runat="server" Width="95%">
                                                <ajaxToolkit:TabContainer ID="TabContainerEIV" runat="server" Width="100%">
                                                    <ajaxToolkit:TabPanel ID="tabPaneleBeadvanyCsatolmanyok" runat="server" TabIndex="0">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="LabelCimlista" runat="server" Text="Elszt��v"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ContentTemplate>

                                                            <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                            <asp:HiddenField ID="ElosztoivId_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="ElosztoivIndex_HiddenField" runat="server" />

                                                            <asp:GridView
                                                                ID="gridViewElosztoivek" runat="server" CssClass="GridViewLovListStyle" 
                                                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" 
                                                                AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                PageSize="5000"
                                                                DataKeyNames="Id" AutoGenerateColumns="False"
                                                                OnSelectedIndexChanging="GridViewElosztoivekSearchResult_SelectedIndexChanging"
                                                                OnPreRender="GridViewSearchResult_PreRender" EnableModelValidation="True">
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="Id" SortExpression="Id">
                                                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="NEV" HeaderText="Eloszt��v neve" SortExpression="NEV">
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="200px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Fajta_Nev" HeaderText="Eloszt��v t�pusa" SortExpression="Fajta_Nev">
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="200px" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImageButtonShowItems" runat="server" CausesValidation="False" CommandName="show" Text="T�telek megtekint�se" CommandArgument='<%# Eval("ID" )%>'
                                                                                ToolTip="T�telek megtekint�se" AlternateText="T�telek megtekint�se"
                                                                                ImageUrl="~/images/hu/egyeb/nagyito.gif" OnClick="ImageButtonShowItems_Click" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerSettings Visible="False" />
                                                                <EmptyDataTemplate>
                                                                    <asp:Label ID="Label12" runat="server" Text="<%$Resources:Search,NoSearchResult%>"></asp:Label>
                                                                </EmptyDataTemplate>
                                                                <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                                            </asp:GridView>
                                                            <br />
                                                            <asp:GridView ID="ElosztoivTagokGridView" runat="server"
                                                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" 
                                                                AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                AutoGenerateColumns="false" DataKeyNames="Id">
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                        <HeaderTemplate>
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;&nbsp;
                                                                             <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                        <HeaderStyle Width="25px" />
                                                                        <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    </asp:CommandField>
                                                                    <asp:BoundField DataField="Partner_Nev" HeaderText="Partner neve"
                                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                        SortExpression="Partner_Nev" HeaderStyle-Width="200px"></asp:BoundField>
                                                                    <asp:BoundField DataField="CimSTR" HeaderText="Partner c�me"
                                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                        SortExpression="CimSTR" HeaderStyle-Width="200px"></asp:BoundField>
                                                                    <asp:BoundField DataField="Kuldesmod_Nev" HeaderText="K�ld�sm�d"
                                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                        SortExpression="Kuldesmod_Nev" HeaderStyle-Width="200px"></asp:BoundField>
                                                                    <asp:BoundField DataField="AlairoSzerep_Nev" HeaderText="Al��r�szerep"
                                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                        SortExpression="AlairoSzerep_Nev" HeaderStyle-Width="200px"></asp:BoundField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </ContentTemplate>
                                                    </ajaxToolkit:TabPanel>
                                                </ajaxToolkit:TabContainer>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <br />
                                            <asp:ImageButton ID="ImageButton3" runat="server"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <uc9:ElosztoIvCimzettListaControl ID="ElosztoIvCimzettListaControlInstance" runat="server" />
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <uc2:LovListFooter ID="LovListFooter1" runat="server" />
            </td>
            <td>
                <asp:ImageButton TabIndex="-1" ID="NewImageButton" runat="server"
                    ImageUrl="~/images/hu/ovalgomb/uj_letrehozasa.png"
                    onmouseover="swapByName(this.id,'uj_letrehozasa2.png')"
                    onmouseout="swapByName(this.id,'uj_letrehozasa.png')"
                    OnClientClick="window.close(); return false;" CommandName="New"
                    Visible="True" />
            </td>
        </tr>
    </table>
</asp:Content>
