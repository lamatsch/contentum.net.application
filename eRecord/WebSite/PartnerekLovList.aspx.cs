﻿using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;
using System.Data;
using System.Text;

public partial class PartnerekLovList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterType = "All"; // lehetséges értékek: "All" (==""), "Szervezet", "Szemely"

    private const int tabIndexPostai = 0;
    private const int tabIndexEgyeb = 1;

    private const string kodcsoportCim = "CIM_TIPUS";

    private bool withElosztoivek;
    private bool elosztoivMode;

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(DataRow row)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (row["CimTipus"].ToString())
            {
                case KodTarak.Cim_Tipus.Postai:
                    string orszag = row["OrszagNev"].ToString();
                    string iranyitoszam = row["IRSZ"].ToString();
                    string telepules = row["TelepulesNev"].ToString();
                    // BLG_1347
                    //cim.Add(kozterulet, delimeterSpace);
                    string kozteruletNev = row["KozteruletNev"].ToString();
                    string kozteruletTipusNev = row["KozteruletTipusNev"].ToString();
                    string hazszam = row["Hazszam"].ToString();
                    string hrsz = row["HRSZ"].ToString();
                    string hazszamIg = row["Hazszamig"].ToString();
                    string hazszamBetujel = row["HazszamBetujel"].ToString();
                    string lepcsohaz = row["Lepcsohaz"].ToString();
                    string szint = row["Szint"].ToString();
                    string ajto = row["Ajto"].ToString();
                    string ajtoBetujel = row["AjtoBetujel"].ToString();

                    cim.Add(orszag, delimeter);
                    cim.Add(iranyitoszam, delimeter);
                    cim.Add(telepules, delimeter);

                    //LZS - BUG_12131
                    //PL:Magyarország 8200 Veszprém Horváth István [Közterület típusa hiányzik] [házszám hiányzik] ( HRSZ. 123456 )
                    if (!String.IsNullOrEmpty(kozteruletNev)
                             && String.IsNullOrEmpty(kozteruletTipusNev)
                             && String.IsNullOrEmpty(hazszam)
                             && !String.IsNullOrEmpty(hrsz))
                    {

                        cim.Add(kozteruletNev, delimeterSpace);

                        AddMorePostAddressData(cim, delimeter, delimeterSpace, hazszamIg, hazszamBetujel, lepcsohaz, szint, ajto, ajtoBetujel, hazszam);
                        AddHRSZ(cim, delimeterSpace, hrsz);

                    }//PL: Magyarország 8200 Veszprém Fő út [házszám hiányzik] ( HRSZ. 123456 )
                    else if (!String.IsNullOrEmpty(kozteruletNev)
                             && !String.IsNullOrEmpty(kozteruletTipusNev)
                             && String.IsNullOrEmpty(hazszam)
                             && !String.IsNullOrEmpty(hrsz))
                    {
                        cim.Add(kozteruletNev, delimeterSpace);
                        cim.Add(kozteruletTipusNev, delimeterSpace);

                        AddMorePostAddressData(cim, delimeter, delimeterSpace, hazszamIg, hazszamBetujel, lepcsohaz, szint, ajto, ajtoBetujel, hazszam);
                        AddHRSZ(cim, delimeterSpace, hrsz);
                    }
                    else
                    {
                        cim.Add(kozteruletNev, delimeterSpace);
                        cim.Add(kozteruletTipusNev, delimeterSpace);

                        AddMorePostAddressData(cim, delimeter, delimeterSpace, hazszamIg, hazszamBetujel, lepcsohaz, szint, ajto, ajtoBetujel, hazszam);

                        if (!(!String.IsNullOrEmpty(kozteruletNev)
                             && !String.IsNullOrEmpty(kozteruletTipusNev)
                             && !String.IsNullOrEmpty(hazszam)))
                        {
                            AddHRSZ(cim, delimeterSpace, hrsz);
                        }
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = row["CimTobbi"].ToString();
                    cim.Add(Cim, delimeter);
                    break;

                case "":
                    return String.Empty;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            throw e;
        }

        return text.ToString();
    }

    private CimCollection AddHRSZ(CimCollection cim, string delimeterSpace, string hrsz)
    {
        if (!String.IsNullOrEmpty(hrsz))
        {
            cim.Add("HRSZ.", delimeterSpace);
            cim.Add(hrsz, delimeterSpace);
        }

        return cim;
    }

    private CimCollection AddMorePostAddressData(CimCollection cim, string delimeter, string delimeterSpace,
                                                  string hazszamIg, string hazszamBetujel, string lepcsohaz,
                                                  string szint, string ajto, string ajtoBetujel, string hazszam)
    {
        if (!String.IsNullOrEmpty(hazszamIg))
            hazszam += "-" + hazszamIg;
        if (!String.IsNullOrEmpty(hazszamBetujel))
            hazszam += "/" + hazszamBetujel;
        cim.Add(hazszam, delimeter);

        if (!String.IsNullOrEmpty(lepcsohaz))
            lepcsohaz += " lépcsõház";
        cim.Add(lepcsohaz, delimeter);

        if (!String.IsNullOrEmpty(szint))
            szint += ". emelet";
        cim.Add(szint, delimeter);

        if (!String.IsNullOrEmpty(ajto))
        {
            if (!String.IsNullOrEmpty(ajtoBetujel))
                ajto += "/" + ajtoBetujel;
            ajto += " ajtó";
        }
        cim.Add(ajto, delimeter);

        return cim;
    }

    public bool Collapsed
    {
        get
        {
            if (cpeCimSearch.ClientState == null)
            {
                return cpeCimSearch.Collapsed;
            }
            else
            {
                return Boolean.Parse(cpeCimSearch.ClientState);
            }
        }
        set { cpeCimSearch.ClientState = value.ToString(); }
    }

    private GridViewRow prevouseRow = null;
    private bool alternateRow = false;
    private const string alternateRowStyle = "GridViewAlternateRowStyle";
    private const string rowStyle = "GridViewRowStyle";
    private const string selectedRowStyle = "GridViewLovListSelectedRowStyle";

    private string CimTipus { get; set; }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerekList");
        CimTipus = Request.QueryString.Get("CimTipus");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // CR3321 Partner rögzítés csak keresés után
        //bopmh
        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            NewImageButton.Visible = false;
        }

        try
        {
            radioButtonList_Mode.Visible = withElosztoivek = bool.Parse(Request.QueryString.Get(QueryStringVars.WithElosztoivek)) && FunctionRights.GetFunkcioJog(Page, "ElosztoivekList");
        }
        catch
        {
            radioButtonList_Mode.Visible = withElosztoivek = false;
        }

        registerJavascripts();
        if (withElosztoivek)
            elosztoivMode = radioButtonList_Mode.SelectedValue == "P" ? false : true;
        else
            elosztoivMode = false;

        elosztoivekTable.Visible = elosztoivMode;
        partnerekTable.Visible = !elosztoivMode;

        if (!elosztoivMode)
        {
            // van-e szûrés esetleg szervezetre, vagy szemelyre:
            filterType = Request.QueryString.Get(QueryStringVars.Filter);
            if (filterType == null) filterType = "";
            switch (filterType)
            {
                case "Szervezet":
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_Szervezet;
                    break;
                case "Szemely":
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_Szemely;
                    break;
                #region BLG_452_MINOSITO
                case Contentum.eUtility.Constants.PartnerKapcsolatTipus.Minosito:
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_Minosito;
                    break;
                #endregion BLG_452_MINOSITO
                case Constants.FilterType.Partnerek.BelsoSzemely:
                    goto case "Szemely";
                case Constants.FilterType.Partnerek.SzervezetKapcsolattartoja:
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_SzervezetKapcsolattartoja;
                    break;
            }

            LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

            LovListFooter1.ButtonsClick += new System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

            // CR3321 Partner rögzítés csak keresés után
            //string query = "&" + QueryStringVars.HiddenFieldId + "=" + Request.QueryString.Get(QueryStringVars.HiddenFieldId)
            //          + "&" + QueryStringVars.TextBoxId + "=" + Request.QueryString.Get(QueryStringVars.TextBoxId);
            string query = "&" + QueryStringVars.HiddenFieldId + "=" + PartnerHiddenField.ClientID
                      + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID;


            if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.TryFireChangeEvent)))
            {
                query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
            }

            //if ((!String.IsNullOrEmpty(Request.QueryString.Get("CimTextBoxClientId")) && (!String.IsNullOrEmpty(Request.QueryString.Get("CimHiddenFieldClientId")))))
            //{
            //    query += "&CimTextBoxId=" + Request.QueryString.Get("CimTextBoxClientId") + "&CimHiddenFieldId=" + Request.QueryString.Get("CimHiddenFieldClientId");
            //}
            if ((CimTextBox != null) && (CimHiddenField != null))
            {
                query += "&CimTextBoxId=" + CimTextBox.ClientID + "&CimHiddenFieldId=" + CimHiddenField.ClientID;
            }

            // CR3321 Partner rögzítés csak keresés után
            string jsdoPostback = "function NewPartnerCallback(){__doPostBack('" + PartnerMegnevezes.ClientID + @"','');}";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NewPartnerCallback", jsdoPostback, true);

            //NewImageButton.OnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
            //            , CommandName.Command + "=" + CommandName.New
            //            + "&HiddenFieldId=" + cimid.ClientID + "&TextBoxId=" + cimtext.ClientID + "&" + QueryStringVars.ParentWindowCallbackFunction + "=NewPartnerCallback"
            //            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
            NewImageButton.OnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query + "&" + QueryStringVars.ParentWindowCallbackFunction + "=NewPartnerCallback"
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

            ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", GridViewSelectedId.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
                , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

            if (!IsPostBack)
            {
                KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportCim, LovListHeader1.ErrorPanel);
                ListItem itemPostai = KodtarakDropDownListTipus.DropDownList.Items.FindByValue(KodTarak.Cim_Tipus.Postai);
                KodtarakDropDownListTipus.DropDownList.Items.Remove(itemPostai);
                //FillGridViewSearchResult(TextBoxSearch.Text, false);

                //FillGridViewElosztoivekSearchResult(TextBox1.Text, false);
                FilterCimTipusok(KodtarakDropDownListTipus.DropDownList);


            }
        }
        else
        {
            ImageButton3.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("ElosztoivekForm.aspx", "", GridViewSelectedIndex.ClientID);

            ImageButton2.OnClientClick = JavaScripts.SetOnClientClick("ElosztoivSearch.aspx", ""
                , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

            LovListFooter1.ButtonsClick += new
                System.Web.UI.WebControls.CommandEventHandler(LovListFooterElosztoiv_ButtonsClick);
        }

        LovListHeader1.ErrorPanel.Visible = false;
        LovListHeader1.ErrorUpdatePanel.Update();

        ScriptManager1.SetFocus(ButtonSearch);

        #region BLG_612
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        if (radioButtonList_Mode.SelectedValue == "E")
            ElosztoIvCimzettListaControlInstance.SetCotrolVisibility(true);
        else
            ElosztoIvCimzettListaControlInstance.SetCotrolVisibility(false);
        //ElosztoIvCimzettListaControlInstance.ElosztoIvNev = string.Format("CIMZETT_LISTA_{0}", DateTime.Now.ToString("yyyy-dd-MM_HH-mm-ss"));
        ElosztoIvCimzettListaControlInstance.OnSaved += ElosztoIvCimzettListaControlInstance_OnSaved;
        if (!string.IsNullOrEmpty(ElosztoivId_HiddenField.Value))
            ElosztoivTagokGridViewBind(ElosztoivId_HiddenField.Value);
        #endregion
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!elosztoivMode)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            JavaScripts.ResetScroll(Page, LovListCPE);
                            FillGridViewSearchResult("", true);
                        }
                        break;
                }
            }

            // scrollozhatóság állítása
            UI.SetLovListGridViewScrollable(GridViewSearchResult, LovListCPE, 12);
            if (!LovListCPE.ScrollContents)
            {
                Panel1.Width = Unit.Percentage(96.6);
            }
            else
            {
                Panel1.Width = Unit.Percentage(95);
            }
        }
        else
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
                            FillGridViewElosztoivekSearchResult("", true);
                        }
                        break;
                }
            }

            // scrollozhatóság állítása
            UI.SetLovListGridViewScrollable(gridViewElosztoivek, CollapsiblePanelExtender2, 12);
            if (!CollapsiblePanelExtender2.ScrollContents)
            {
                Panel3.Width = Unit.Percentage(96.6);
            }
            else
            {
                Panel3.Width = Unit.Percentage(95);
            }
        }
    }


    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, LovListCPE);
        FillGridViewSearchResult(TextBoxSearch.Text, false);
    }

    private KRT_CimekSearch GetCimekFromComponents()
    {
        KRT_CimekSearch search = new KRT_CimekSearch();

        if (!Collapsed)
        {
            switch (TabContainerCimSearch.ActiveTabIndex)
            {
                case tabIndexPostai:
                    {
                        search.TelepulesNev.Value = TelepulesTextBox1.Text;
                        search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBox1.Text);
                        search.IRSZ.Value = IranyitoszamTextBox1.Text;
                        search.IRSZ.Operator = Search.GetOperatorByLikeCharater(IranyitoszamTextBox1.Text);
                        search.KozteruletNev.Value = KozteruletTextBox1.Text;
                        search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTextBox1.Text);
                        search.KozteruletTipusNev.Value = KozteruletTipusTextBox1.Text;
                        search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTipusTextBox1.Text);
                        // CR3321 Partner rögzítés csak keresés után
                        //        Házszám szûrés
                        search.Hazszam.Value = textHazszam.Text;

                        if (!String.IsNullOrEmpty(textHazszam.Text))
                        {
                            search.Hazszam.Operator = Search.GetOperatorByLikeCharater(textHazszam.Text);
                        }
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.equals;
                    }
                    break;
                case tabIndexEgyeb:
                    if (!String.IsNullOrEmpty(textEgyebCim.Text))
                    {
                        search.CimTobbi.Value = textEgyebCim.Text;
                        search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(textEgyebCim.Text);
                    }
                    if (String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                    {
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.notequals;
                    }
                    else
                    {
                        search.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                        search.Tipus.Operator = Query.Operators.equals;
                    }
                    break;
            }
        }

        return search;
    }

    protected void FillGridViewSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        #region BLG_452_MINOSITO
        if (filterType == Contentum.eUtility.Constants.PartnerKapcsolatTipus.Minosito)
        {
            FillGridViewByMinosito(SearchKey, searchObjectFromSession);
            return;
        }
        #endregion BLG_452_MINOSITO

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        KRT_PartnerekSearch searchPartner = null;

        searchPartner = SetPartnerSearchObject(SearchKey, searchObjectFromSession);

        // ha kell, szûrünk a típusra, akár felülvágva a részletes keresésnél megadott típust is
        switch (filterType)
        {
            case "Szervezet":
                searchPartner.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                searchPartner.Tipus.Operator = Query.Operators.equals;
                break;
            case "Szemely":
                searchPartner.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
                searchPartner.Tipus.Operator = Query.Operators.equals;
                break;
            case Constants.FilterType.Partnerek.BelsoSzemely:
                searchPartner.Belso.Value = Constants.Database.Yes;
                searchPartner.Belso.Operator = Query.Operators.equals;
                goto case "Szemely";
            case Constants.FilterType.Partnerek.SzervezetKapcsolattartoja:
                searchPartner.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                searchPartner.Tipus.Operator = Query.Operators.equals;
                break;
        }

        FillPartnerGridFromSearch(searchObjectFromSession, service, searchPartner);
    }
    /// <summary>
    /// PartnerSearchObject beállítása session-bõl vagy név filter alapján
    /// </summary>
    /// <param name="SearchKey"></param>
    /// <param name="searchObjectFromSession"></param>
    /// <returns></returns>
    private KRT_PartnerekSearch SetPartnerSearchObject(string SearchKey, bool searchObjectFromSession)
    {
        KRT_PartnerekSearch searchPartner;
        if (searchObjectFromSession == true)
        {
            searchPartner = (KRT_PartnerekSearch)Search.GetSearchObject(Page, new KRT_PartnerekSearch());
            SetDefaultState();
        }
        else
        {
            searchPartner = new KRT_PartnerekSearch();
            searchPartner.Nev.Value = SearchKey;
            searchPartner.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }
        searchPartner.OrderBy = "KRT_Partnerek.Nev";
        return searchPartner;
    }

    private void FillPartnerGridFromSearch(bool searchObjectFromSession, KRT_PartnerekService service, KRT_PartnerekSearch searchPartner)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (searchPartner == null)
            return;
        searchPartner.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = new Result();

        if (filterType == Constants.FilterType.Partnerek.SzervezetKapcsolattartoja)
        {
            result = service.GetSzervezetekWithKapcsolattarto(execParam, searchPartner);
        }
        else
        {
            if (searchObjectFromSession)
            {
                result = service.GetAllWithExtnesions(execParam, searchPartner);
            }
            else
            {
                KRT_CimekSearch searchCim = GetCimekFromComponents();
                if (!String.IsNullOrEmpty(this.CimTipus))
                {
                    if (String.IsNullOrEmpty(searchCim.Tipus.Operator))
                    {
                        searchCim.Tipus.Value = GetCimTipusFilter();
                        searchCim.Tipus.Operator = Query.Operators.inner;
                    }
                    else
                    {
                        searchCim.WhereByManual = String.Format(" and KRT_Cimek.Tipus in ({0})", GetCimTipusFilter());
                    }
                }
                searchCim.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);
                result = service.GetAllWithCim(execParam, searchPartner, searchCim);
            }
        }
        GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, searchPartner.TopRow);
    }

    #region BLG_452_MINOSITO
    /// <summary>
    /// Partnerek minõsítõ és szûrõ alapján
    /// </summary>
    /// <param name="SearchKey"></param>
    /// <param name="searchObjectFromSession"></param>
    private void FillGridViewByMinosito(string SearchKey, bool searchObjectFromSession)
    {
        string PartnerId = (Page.Request.QueryString.Get(QueryStringVars.PartnerId));
        if (string.IsNullOrEmpty(PartnerId))
            return;

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_Partnerek partner = new KRT_Partnerek();
        partner.Id = PartnerId;

        KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
        search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Minosito;
        search.Tipus.Operator = Query.Operators.equals;

        Result resultPartnerKapcs = service.GetAllByPartner(ExecParam, partner, search);

        KRT_PartnerekSearch searchPartner = GetPartnerekSearchFromKapcsoltPartnerAndFilter(resultPartnerKapcs, SearchKey, searchObjectFromSession);

        FillPartnerGridFromSearch(searchObjectFromSession, service, searchPartner);
    }
    /// <summary>
    /// KRT_PartnerekSearch obj kitöltése kapcsolt partnerekkel és a filterekkel
    /// </summary>
    /// <param name="resultPartnerKapcs"></param>
    /// <param name="SearchKey"></param>
    /// <param name="searchObjectFromSession"></param>
    /// <returns></returns>
    private KRT_PartnerekSearch GetPartnerekSearchFromKapcsoltPartnerAndFilter(Result resultPartnerKapcs, string SearchKey, bool searchObjectFromSession)
    {
        KRT_PartnerekSearch searchPartner = null;

        List<string> kapcsoltPartnerekLista = new List<string>();
        if (resultPartnerKapcs.Ds != null && resultPartnerKapcs.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in resultPartnerKapcs.Ds.Tables[0].Rows)
            {
                string kapcsoltPartner = row["Partner_id_kapcsolt"].ToString();
                kapcsoltPartnerekLista.Add(kapcsoltPartner);
            }
        }
        else
            return null;

        searchPartner = SetPartnerSearchObject(SearchKey, searchObjectFromSession);

        SetPartnerSearchIds(searchPartner, kapcsoltPartnerekLista);
        searchPartner.OrderBy = "KRT_Partnerek.Nev";

        return searchPartner;
    }
    /// <summary>
    /// Partner keresõ obj szûrése id-kra
    /// </summary>
    /// <param name="searchPartner"></param>
    /// <param name="kapcsoltPartnerekLista"></param>
    private static void SetPartnerSearchIds(KRT_PartnerekSearch searchPartner, List<string> kapcsoltPartnerekLista)
    {
        if (kapcsoltPartnerekLista != null && kapcsoltPartnerekLista.Count > 0)
        {
            string inQuery = null;
            foreach (var item in kapcsoltPartnerekLista)
            {
                if (!string.IsNullOrEmpty(inQuery))
                    inQuery += ", ";
                inQuery += "'" + item + "'";
            }
            searchPartner.Id.Value = inQuery;
            searchPartner.Id.Operator = Query.Operators.inner;
        }
    }
    #endregion BLG_452_MINOSITO

    //részletes keresés után az lovlist keresési mezõinek alaplállapotba állítása
    private void SetDefaultState()
    {
        TextBoxSearch.Text = "";
        Collapsed = true;
        KodtarakDropDownListTipus.SetSelectedValue("");
        textEgyebCim.Text = "";
        TelepulesTextBox1.Text = "";
        IranyitoszamTextBox1.Text = "";
        KozteruletTextBox1.Text = "";
        KozteruletTipusTextBox1.Text = "";
        // CR3321
        textHazszam.Text = "";
    }

    private void GridViewFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }

        DataSet res = new DataSet();
        DataTable table = new DataTable();
        table.Columns.Add("Id", typeof(Guid));
        table.Columns.Add("Nev");
        table.Columns.Add("CimId");
        table.Columns.Add("Cim");
        res.Tables.Add(table);

        if (result.Ds != null)
        {
            try
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string nev = row["Nev"].ToString();

                        #region BUG 5197 - 'ALL' szûréskor Partner adatok
                        try
                        {
                            if ("All".Equals(filterType, StringComparison.CurrentCultureIgnoreCase))
                            {
                                string id = row["Id"].ToString();

                                KRT_PartnerKapcsolatokService pkservice = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                                KRT_PartnerKapcsolatokSearch pksearch = new KRT_PartnerKapcsolatokSearch();

                                pksearch.Partner_id_kapcsolt.Value = id;
                                pksearch.Partner_id_kapcsolt.Operator = Query.Operators.equals;

                                Result respk = pkservice.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), pksearch);
                                if (!respk.IsError)
                                {
                                    if (respk.Ds.Tables.Count > 0 && respk.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        DataRow rowpk = respk.Ds.Tables[0].Rows[0];
                                        string partnerId = rowpk["Partner_id"].ToString();

                                        KRT_PartnerekService partnerekService1 = eAdminService.ServiceFactory.GetKRT_PartnerekService();

                                        ExecParam execParam1 = UI.SetExecParamDefault(Page, new ExecParam());
                                        execParam1.Record_Id = partnerId;

                                        Result respartner1 = partnerekService1.Get(execParam1);
                                        if (!respartner1.IsError)
                                        {
                                            KRT_Partnerek partner = (KRT_Partnerek)respartner1.Record;
                                            if (!string.IsNullOrEmpty(partner.Nev))
                                            {
                                                nev += " - " + partner.Nev;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Error("PartnerekLovList GridViewFill név beállítás error: " + e.Message);
                        }
                        #endregion

                        string cim = GetAppendedCim(row);
                        DataRow newRow = res.Tables[0].NewRow();
                        newRow["Id"] = row["Id"];
                        newRow["Nev"] = nev;
                        // BLG_1347
                        //newRow["Cim"] = row["CimNev"];
                        newRow["Cim"] = cim;
                        newRow["CimId"] = row["CimId"];
                        res.Tables[0].Rows.Add(newRow);
                    }
                }
                else
                {
                    HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
                    Panel1.Width = Unit.Percentage(96.6);
                    gridView.BorderWidth = Unit.Pixel(0);
                }
            }
            catch (Exception e)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), e.Message);
            }
        }
        else
        {
            HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
            Panel1.Width = Unit.Percentage(96.6);
            gridView.BorderWidth = Unit.Pixel(0);
        }

        gridView.DataSource = res;
        gridView.DataBind();
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    string cimTextBoxId = Request.QueryString.Get("CimTextBoxId");
                    string cimHiddenFieldId = Request.QueryString.Get("CimHiddenFieldId");

                    string cimText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string cimId = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);

                    //BUG 5197 - nem kell, forceoljuk
                    //bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                    //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);
                    //BUG 5197 - 3 -as pont miatt mindenképp kell a "tryFireChangeEvent"
                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, true);

                    //if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId) && !String.IsNullOrEmpty(cimText))
                    if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId))
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        // Sorrend fontos!!!
                        // elõbb hiddenfield, aztán textbox (mert másképp a change lefutásakor még nincs átírva a hiddenfield id,
                        // és az esetleges klónozásnál helytelen (a módosítás elõtti) érték kerül be
                        parameters.Add(cimHiddenFieldId, cimId);
                        parameters.Add(cimTextBoxId, cimText);
                        //BUG 5197 - 3 -as pont miatt mindenképp kell a "tryFireChangeEvent"
                        //JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true, tryFireChangeEvent, true);
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true, true, true);
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        foreach (GridViewRow row in gridViewElosztoivek.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedId, GridViewSelectedIndex);
            }
        }

        base.Render(writer);

    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!withElosztoivek)
        {
            //autocomplete-ek függõségeinek beállítása, postback után contextkey újra beállítása
            JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

            //tabfülre kattintva is nyíljon a panel, de ne csukódjon
            string js = @"var postai = $get('" + labelPostaiHeader.ClientID + @"');
                    var egyeb = $get('" + labelEgyebHeader.ClientID + @"');
                    $addHandler(postai,'click',openPanel);
                    $addHandler(egyeb,'click',openPanel);
                    function openPanel(sender,e) {var a = $find('" + cpeCimSearch.ClientID + @"');
                    if(a.get_Collapsed()){a.expandPanel();};}";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenPanelByTab", js, true);
        }
    }

    protected void GridViewSearchResult_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewSearchResult.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedRowStyle;
    }

    //sorok színezése
    protected void GridViewSearchResult_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (prevouseRow != null)
                {

                    if (prevouseRow.Cells[0].Text == row.Cells[0].Text)
                    {
                        if (String.IsNullOrEmpty(HttpUtility.HtmlDecode(prevouseRow.Cells[2].Text).Trim()))
                        {
                            prevouseRow.Visible = false;

                        }
                    }
                    else
                    {
                        alternateRow = !alternateRow;
                    }
                }

                if (row.RowState == DataControlRowState.Normal || row.RowState == DataControlRowState.Alternate)
                {
                    if (alternateRow)
                    {
                        row.RowState = DataControlRowState.Alternate;
                        row.ControlStyle.CssClass = alternateRowStyle;
                    }
                    else
                    {
                        row.RowState = DataControlRowState.Normal;
                        row.ControlStyle.CssClass = rowStyle;
                    }
                }

                prevouseRow = row;
            }
        }
    }


    #region Elosztóívek

    protected void ButtonElosztoivekSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
        FillGridViewElosztoivekSearchResult(TextBox1.Text, false);
    }

    protected void FillGridViewElosztoivekSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedIndex.Value = String.Empty;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        EREC_IraElosztoivekSearch searchElosztoiv = null;

        if (searchObjectFromSession == true)
        {
            searchElosztoiv = (EREC_IraElosztoivekSearch)Search.GetSearchObject(Page, new EREC_IraElosztoivekSearch());
            SetDefaultElosztoivState();
        }
        else
        {
            searchElosztoiv = new EREC_IraElosztoivekSearch();
            searchElosztoiv.NEV.Value = SearchKey;
            searchElosztoiv.NEV.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }

        searchElosztoiv.OrderBy = "NEV";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        searchElosztoiv.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, searchElosztoiv);

        GridViewElosztoivekFill(gridViewElosztoivek, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        //trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, searchElosztoiv.TopRow);
    }

    //részletes keresés után az lovlist keresési mezõinek alaplállapotba állítása
    private void SetDefaultElosztoivState()
    {
        TextBox1.Text = "";
        //Collapsed = true;
        //KodtarakDropDownListTipus.SetSelectedValue("");
        //textEgyebCim.Text = "";
        //TelepulesTextBox1.Text = "";
        //IranyitoszamTextBox1.Text = "";
        //KozteruletTextBox1.Text = "";
        //KozteruletTipusTextBox1.Text = "";
    }

    private void GridViewElosztoivekFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }

        DataSet res = new DataSet();
        DataTable table = new DataTable();
        table.Columns.Add("Id", typeof(Guid));
        table.Columns.Add("Nev");
        table.Columns.Add("Fajta_Nev");
        res.Tables.Add(table);

        if (result.Ds != null)
        {
            try
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string nev = row["NEV"].ToString();
                        string Fajta_Nev = row["Fajta_Nev"].ToString();
                        DataRow newRow = res.Tables[0].NewRow();
                        newRow["Id"] = row["Id"];
                        newRow["Nev"] = nev;
                        newRow["Fajta_Nev"] = Fajta_Nev;
                        res.Tables[0].Rows.Add(newRow);
                    }
                }
                else
                {
                    HtmlTableCell td = (HtmlTableCell)Panel3.FindControl("headerBackGround");
                    Panel3.Width = Unit.Percentage(96.6);
                    gridView.BorderWidth = Unit.Pixel(0);
                }
            }
            catch (Exception e)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), e.Message);
            }
        }
        else
        {
            HtmlTableCell td = (HtmlTableCell)Panel3.FindControl("headerBackGround");
            Panel3.Width = Unit.Percentage(96.6);
            gridView.BorderWidth = Unit.Pixel(0);
        }

        gridView.DataSource = res;
        gridView.DataBind();
    }

    protected void GridViewElosztoivekSearchResult_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        gridViewElosztoivek.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedRowStyle;
        List<string> ids = ui.GetGridViewSelectedRows(gridViewElosztoivek, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);
        if (ids != null && ids.Count > 0)
        {
            ElosztoivTagokGridViewBind(ids[0]);
        };
    }

    protected void LovListFooterElosztoiv_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedId.Value) && !String.IsNullOrEmpty(GridViewSelectedIndex.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedId.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    gridViewElosztoivek.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(gridViewElosztoivek);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(gridViewElosztoivek, 1);

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, true, true);

                    // cím mezõ törlése a hívó formon
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(Request.QueryString.Get("CimTextBoxId"), "címzettlista alapján"); // BUG_7475
                    parameters.Add(Request.QueryString.Get("CimHiddenFieldId"), string.Empty);
                    JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true);


                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, false);
                    disable_refreshLovList = true;

                }
            }
            else
            {
                GridViewSelectedIndex.Value = String.Empty;
                GridViewSelectedId.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    #endregion

    // CR3321 Partner rögzítés csak keresés után
    protected void PartnerMegnevezes_ValueChanged(object sender, EventArgs e)
    {
        string selectedId = PartnerHiddenField.Value;
        string selectedText = PartnerMegnevezes.Text;

        //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
        string cimTextBoxId = Request.QueryString.Get("CimTextBoxId");
        string cimHiddenFieldId = Request.QueryString.Get("CimHiddenFieldId");


        string cimText = CimTextBox.Text;
        string cimId = CimHiddenField.Value;

        bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

        JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);


        if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId))
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            // Sorrend fontos!!!
            // elõbb hiddenfield, aztán textbox (mert másképp a change lefutásakor még nincs átírva a hiddenfield id,
            // és az esetleges klónozásnál helytelen (a módosítás elõtti) érték kerül be
            parameters.Add(cimHiddenFieldId, cimId);
            parameters.Add(cimTextBoxId, cimText);

            JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true, tryFireChangeEvent, true);
        }

        JavaScripts.RegisterCloseWindowClientScript(Page, false);
        disable_refreshLovList = true;

    }

    #region BLG_612
    private void ElosztoIvCimzettListaControlInstance_OnSaved(string id, string name)
    {
        //GridViewSelectedId.Value = id;
        //ElosztoivId_HiddenField.Value = id;
        //ElosztoivIndex_HiddenField
        //TextBox1.Text = name;
        FillGridViewElosztoivekSearchResult(name, false);
    }

    private void ElosztoivTagokGridViewBind(string elosztoivId)
    {
        if (!String.IsNullOrEmpty(elosztoivId))
        {
            EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraElosztoivek szuloElosztoIv = new EREC_IraElosztoivek();
            szuloElosztoIv.Id = elosztoivId;

            EREC_IraElosztoivTetelekSearch search = new EREC_IraElosztoivTetelekSearch();

            search.ElosztoIv_Id.Value = elosztoivId;
            search.ElosztoIv_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result res = service.GetAllWithExtension(ExecParam, search);
            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                ElosztoivTagokGridView.DataSource = res.Ds.Tables[0];
                ElosztoivTagokGridView.DataBind();
            }
            ui.SetClientScriptToGridViewSelectDeSelectButton(ElosztoivTagokGridView);
        }
        else
        {
            ui.GridViewClear(ElosztoivTagokGridView);
        }
    }
    #endregion BLG_612

    protected void ImageButtonShowItems_Click(object sender, ImageClickEventArgs e)
    {
        string elosztoivid = ((System.Web.UI.WebControls.ImageButton)sender).CommandArgument;
        ElosztoivTagokGridViewBind(elosztoivid);
    }

    string GetCimTipusFilter()
    {
        string filter = String.Empty;

        if (!String.IsNullOrEmpty(CimTipus))
        {
            foreach (string tipus in CimTipus.Split(','))
            {
                if (!String.IsNullOrEmpty(filter))
                    filter += ",";

                filter += String.Format("'{0}'", tipus);
            }
        }

        return filter;
    }

    void FilterCimTipusok(DropDownList dropDownList)
    {
        if (!String.IsNullOrEmpty(CimTipus))
        {
            List<string> cimTipusok = new List<string>(CimTipus.Split(','));

            for (int i = dropDownList.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = dropDownList.Items[i];

                if (!String.IsNullOrEmpty(item.Value))
                {
                    if (!cimTipusok.Contains(item.Value))
                    {
                        dropDownList.Items.Remove(item);
                    }
                }
            }
        }
    }
}
