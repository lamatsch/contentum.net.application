<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerekSearch.aspx.cs" Inherits="PartnerekSearch" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/OrszagokTextBox.ascx" TagName="OrszagokTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc11" %>
<%@ Register Src="Component/Letrehozas_SearchFormComponent.ascx" TagName="Letrehozas_SearchFormComponent" TagPrefix="uc12" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }

        .mrUrlapCaption_short {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,PartnerekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelPartnerTipus" runat="server" Text="T�pus:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="KodtarakDropDownListPartnerTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr id="trPartnerNev" runat="server" class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textNev" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc8:OrszagokTextBox ID="OrszagokTextBox1" runat="server" SearchMode="true" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelForras" runat="server" Text="Forr�s:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="KodtarakDropDownListPartnerForras" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelBelso" runat="server" Text="Bels�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="ddownBelso" runat="server" CssClass="mrUrlapInputComboBox"></asp:DropDownList>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Szem�lyek -->
                        <asp:Panel ID="panelSzemelyek" runat="server">
                            <div class="mrUrlapSubTitleWrapper">
                                <span class="mrUrlapSubTitleLeftSide"></span>
                                <span class="mrUrlapSubTitle">
                                    <asp:Label ID="labelTovabbiSzemely" runat="server" Text="Szem�ly tov�bbi adatai"></asp:Label>
                                </span>
                                <span class="mrUrlapSubTitleRightSide"></span>
                            </div>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td>
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTitulus" runat="server" Text="Titulus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTitulus" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short"></td>
                                        <td class="mrUrlapMezo"></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelCsaladiNev" runat="server" Text="Csal�di n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textCsaladiNev" runat="server" CssClass="mrUrlapInputSearch" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUtoNev" runat="server" Text="Ut�n�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUtoNev" runat="server" CssClass="mrUrlapInputSearch" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAnyjaNeve" runat="server" Text="Anyja Neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAnyjaNeve" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelApjaNeve" runat="server" Text="Apja Neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textApjaNeve" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiNev" runat="server" Text="Le�nykori Neve:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzuletesiNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiOrszag" runat="server" Text="Sz�let�si orsz�g:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc9:OrszagTextBox ID="OrszagTextBoxSzuletesi" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiHely" runat="server" Text="Sz�let�si hely:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:TelepulesTextBox ID="TelepulesTextBoxSzuletesi" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzuletesiIdo" runat="server" Text="Sz�let�si id�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc11:CalendarControl ID="CalendarSzuletesiIdo" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr id="rowTAJ_Szemelyi" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTajSzam" runat="server" Text="TAJ sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTajSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzemelyi" runat="server" Text="Szem�lyi igazolv�ny sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzigSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelNeme" runat="server" Text="Neme:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rbListNeme" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputSearchRadioButtonList">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzemelyiAzonosito" runat="server" Text="Szem�lyi azonos�t�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textSzemelyiAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="rowAdoSzam_AdoAzonosito" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoAzonosito" runat="server" Text="Ad�azonos�t� jel:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAdoAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoSzamSzemely" runat="server" Text="Ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAdoszamSzemely" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short"></td>
                                        <td class="mrUrlapMezo"></td>
                                        <td></td>
                                        <td class="mrUrlapCaption" style="padding-left: 20px;">
                                            <asp:Label ID="labelKulfoldoiAdoszamSzemely" runat="server" Text="K�lf�ldi ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rblKulfoldiAdoszamSzemely" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Igen" Value="1" />
                                                <asp:ListItem Text="Nem" Value="0" />
                                                <asp:ListItem Text="�sszes" Value="X" Selected="True" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <%--BLG_346--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelBeosztas" runat="server" Text="Beoszt�s:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textBeosztas" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelMinositesiSzint" runat="server" Text="Min�s�t�si szint:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:KodtarakDropDownList ID="KodtarakDropDownListMinositesiSzint" runat="server" CssClass="mrUrlapInputComboBox" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <!-- V�llalkoz�sok -->
                        <asp:Panel ID="panelVallalkozasok" runat="server">
                            <div class="mrUrlapSubTitleWrapper">
                                <span class="mrUrlapSubTitleLeftSide"></span>
                                <span class="mrUrlapSubTitle">
                                    <asp:Label ID="labelTovabbiSzervezet" runat="server" Text="Szervezet tov�bbi adatai"></asp:Label>
                                </span>
                                <span class="mrUrlapSubTitleRightSide"></span>
                            </div>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo" style="padding-right: 232px">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td>
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapCaption_short">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo" style="padding-right: 230px">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAdoSzamVallalkozas" runat="server" Text="Ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAdoSzamVallalkozas" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTBSzam" runat="server" Text="TB T�rzssz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textTBSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="padding-left: 20px;">
                                            <asp:Label ID="labelKulfoldiAdoszamVallalkozas" runat="server" Text="K�lf�ldi ad�sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rblKulfoldiAdoszamVallalkozas" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Igen" Value="1" />
                                                <asp:ListItem Text="Nem" Value="0" />
                                                <asp:ListItem Text="�sszes" Value="X" Selected="True" />
                                            </asp:RadioButtonList>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short"></td>
                                        <td class="mrUrlapMezo"></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelCegJegyzekSzam" runat="server" Text="C�gjegyz�ksz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textCegJegyzekSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelCegTipus" runat="server" Text="C�g t�pus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:KodtarakDropDownList ID="KodtarakDropDownListCegTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption_short" colspan="2" style="padding-left: 20px">
                                        <uc3:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server"></uc3:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption_short" colspan="2" style="padding-left: 20px">
                                        <uc12:Letrehozas_SearchFormComponent ID="Letrehozas_SearchFormComponent" runat="server"></uc12:Letrehozas_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" style="padding-left: 90px">
                                        <div style="height: 3px"></div>
                                        <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

