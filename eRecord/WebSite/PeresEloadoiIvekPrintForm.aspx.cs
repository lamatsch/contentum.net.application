﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.Collections.Generic;


public partial class PeresEloadoiIvekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        /*ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (String.IsNullOrEmpty(ugyiratId))
        {
            // nincs Id megadva:
        }*/
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && Session["SelectedUgyiratok"] == null)
        {
            //a nyitó oldalról érkező paramétereket kiolvasó script regisztrálása
            string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldID))
            {
                Dictionary<string, string> ParentChildControl = new Dictionary<string, string>(1);
                ParentChildControl.Add(hiddenFieldID, hfSelectedUgyiratok.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl, "", EventArgumentConst.refreshValuesFromParent);

            }
        }
        else
        {
            if (Session["SelectedUgyiratok"] != null)
            {
                ugyiratId = Session["SelectedUgyiratok"].ToString();
            }
            else
            {
                string eventArgument = Request.Params.Get("__EVENTARGUMENT");
                if (!String.IsNullOrEmpty(eventArgument))
                {
                    if (eventArgument == EventArgumentConst.refreshValuesFromParent)
                    {
                        if (hfSelectedUgyiratok.Value.Trim() != String.Empty)
                        {
                            ugyiratId = hfSelectedUgyiratok.Value;
                        }
                    }
                }
            }

            ugyiratId = ugyiratId.Replace("$", "'");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
            erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

            erec_UgyUgyiratokSearch.OrderBy = " EREC_UgyUgyiratok.Foszam ";

            Result tempman_result = erec_UgyUgyiratokService.GetAllWithExtensionForEloadoiIvek(execParam, erec_UgyUgyiratokSearch);

            EREC_UgyiratObjKapcsolatokService kap_service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
            EREC_UgyiratObjKapcsolatokSearch erec_UgyiratObjKapcsolatokSearch = new EREC_UgyiratObjKapcsolatokSearch();

            erec_UgyiratObjKapcsolatokSearch.Obj_Id_Elozmeny.Value = ugyiratId;
            erec_UgyiratObjKapcsolatokSearch.Obj_Id_Elozmeny.Operator = Query.Operators.inner;
            erec_UgyiratObjKapcsolatokSearch.Obj_Type_Kapcsolt.Value = "EREC_UgyUgyiratok";
            erec_UgyiratObjKapcsolatokSearch.Obj_Type_Kapcsolt.Operator = Query.Operators.equals;
            erec_UgyiratObjKapcsolatokSearch.KapcsolatTipus.Value = "01";
            erec_UgyiratObjKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
            erec_UgyiratObjKapcsolatokSearch.WhereByManual = " AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege ";

            Result kap_result = kap_service.GetAllWithExtension(execParam, erec_UgyiratObjKapcsolatokSearch);

            foreach (DataRow tempman_row in tempman_result.Ds.Tables[0].Rows)
            {
                string kap = "";

                foreach (DataRow kap_row in kap_result.Ds.Tables[0].Rows)
                {
                    if (tempman_row["Id"].ToString().Equals(kap_row["Obj_Id_Elozmeny"].ToString()))
                    {
                        kap = kap + kap_row["Obj_Kapcsolt_Azonosito"].ToString() + ", ";
                    }
                }

                if (kap.Length > 2)
                {
                    kap = kap.Remove(kap.Length - 2);
                }

                tempman_row["kap_Foszam_Merge"] = kap;

                if (tempman_row["szervezeti_egyseg"].ToString().IndexOf("iktatókönyve") >= 0)
                {
                    tempman_row["szervezeti_egyseg"] = tempman_row["szervezeti_egyseg"].ToString().Remove(tempman_row["szervezeti_egyseg"].ToString().IndexOf("iktatókönyve"));
                }

                if (Convert.ToInt32(tempman_row["LetrehozasIdo"].ToString().Substring(0, 4)) < 2010)
                {
                    if (tempman_row["IrattariTetelszam"].ToString().Length >= 3)
                    {
                        tempman_row["Itsz2"] = tempman_row["IrattariTetelszam"].ToString().Substring(0, 1);
                        tempman_row["Itsz3"] = tempman_row["IrattariTetelszam"].ToString().Substring(1, 1);
                        tempman_row["Itsz4"] = tempman_row["IrattariTetelszam"].ToString().Substring(2, 1);
                    }

                    if (tempman_row["AgazatiJel"].ToString().Length >= 1)
                    {
                        tempman_row["selejtkod"] = tempman_row["AgazatiJel"].ToString().Substring(0, 1);
                    }
                }
                else
                {
                    if (tempman_row["IrattariTetelszam"].ToString().Length >= 3)
                    {
                        tempman_row["Itsz1"] = "";
                        tempman_row["Itsz2"] = tempman_row["IrattariTetelszam"].ToString().Substring(0, 1);
                        tempman_row["Itsz3"] = tempman_row["IrattariTetelszam"].ToString().Substring(1, 1);
                        tempman_row["Itsz4"] = tempman_row["IrattariTetelszam"].ToString().Substring(2, 1);
                    }
                    if (tempman_row["IrattariTetelszam"].ToString().Length >= 4)
                    {
                        tempman_row["Itsz1"] = tempman_row["IrattariTetelszam"].ToString().Substring(0, 1);
                        tempman_row["Itsz2"] = tempman_row["IrattariTetelszam"].ToString().Substring(1, 1);
                        tempman_row["Itsz3"] = tempman_row["IrattariTetelszam"].ToString().Substring(2, 1);
                        tempman_row["Itsz4"] = tempman_row["IrattariTetelszam"].ToString().Substring(3, 1);
                    }
                    if (tempman_row["IrattariJel"].ToString().Equals("L") || tempman_row["IrattariJel"].ToString().Equals("N"))
                    {
                        tempman_row["selejtkod"] = "NS";

                        if (tempman_row["IrattariJel"].ToString().Equals("N"))
                        {
                            tempman_row["MegorzesiIdo"] = "-";
                        }
                    }
                    else
                    {
                        if (tempman_row["IrattariJel"].ToString().Equals("N"))
                        {
                            tempman_row["selejtkod"] = "-";
                        }
                        else
                        {
                            tempman_row["selejtkod"] = tempman_row["MegorzesiIdo"];
                        }

                        tempman_row["MegorzesiIdo"] = "";
                    }
                }
            }

            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            WebRequest wr = WebRequest.Create(SP_TM_site_url + "Peres Eloadoi Ivek.xml");
            wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
            StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
            templateText = template.ReadToEnd();
            template.Close();

            //string xml = tempman_result.Ds.GetXml();

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            tempman_result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

            string filename = "";

            if (pdf)
            {
                filename = "Eloadoi_ivek_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = "Eloadoi_ivek_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = tempman_result.Uid;
            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(csop_result.ErrorCode))
            {
                foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            tempman_result = tms.GetWordDocument_DataSet_Thread(templateText, tempman_result, pdf, priority, filename, 25);

            byte[] res = (byte[])tempman_result.Record;

            if (string.IsNullOrEmpty(tempman_result.ErrorCode))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                if (pdf)
                {
                    Response.ContentType = "application/pdf";
                }
                else
                {
                    Response.ContentType = "application/msword";
                }
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else
            {
                if (tempman_result.ErrorCode == "99999" || tempman_result.ErrorCode == "99998")
                {
                    string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                }
            }
        }
    }
}
