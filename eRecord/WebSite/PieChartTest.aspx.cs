using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class PieChartTest : System.Web.UI.Page
{
    void Page_Load(Object Src, EventArgs E)
    {
        ExamplePieChart.ChartTitle = "Ügyirat statisztika";
        ExamplePieChart.ImageAlt = "Ügyirat statisztika";
        ExamplePieChart.ImageWidth = 450;
        ExamplePieChart.ImageHeight = 250;

        eRecordComponent_PieChart.ChartElement myChartElement1 = new eRecordComponent_PieChart.ChartElement();
        eRecordComponent_PieChart.ChartElement myChartElement2 = new eRecordComponent_PieChart.ChartElement();
        eRecordComponent_PieChart.ChartElement myChartElement3 = new eRecordComponent_PieChart.ChartElement();

        myChartElement1.Name = "Szignált";
        myChartElement1.Value = 27.0;
        myChartElement1.Color = Color.FromArgb(205, 117, 105);
        ExamplePieChart.addChartElement(myChartElement1);

        myChartElement2.Name = "Ügyintézés alatt";
        myChartElement2.Value = 51.0;
        myChartElement2.Color = Color.FromArgb(254, 254, 155);
        ExamplePieChart.addChartElement(myChartElement2);

        myChartElement3.Name = "Elintézett";
        myChartElement3.Value = 22.0;
        myChartElement3.Color = Color.FromArgb(170, 240, 170);
        ExamplePieChart.addChartElement(myChartElement3);

        ExamplePieChart.generateChartImage();
    }

}
