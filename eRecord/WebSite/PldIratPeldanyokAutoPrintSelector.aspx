<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PldIratPeldanyokAutoPrintSelector.aspx.cs" Inherits="PldIratPeldanyokAutoPrintSelector" Title="Irat p�ld�ny nyomtat�s" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Irat p�ld�ny nyomtat�s" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

    <h3>Nyomtat�s</h3>
    <div class="box_center">
        <ul class="custom-counter">
            <li>
                <asp:HyperLink runat="server" ID="HyperLinkN1" Text="Kis bor�t�kra (LC/6)" ToolTip="1. Kis bor�t�kra (LC/6)" Target="_blank" />
            </li>
            <li>
                <asp:HyperLink runat="server" ID="HyperLinkN2" Text="K�zepes bor�t�kra (LC/5)" ToolTip="2. K�zepes bor�t�kra (LC/5)" Target="_blank" />
            </li>
            <li>
                <asp:HyperLink runat="server" ID="HyperLinkN3" Text="Nagy bor�t�kra (LC/4)" ToolTip="3. Nagy bor�t�kra (LC/4)" Target="_blank" />
            </li>
            <li>
                <asp:HyperLink runat="server" ID="HyperLinkN4" Text="Etikettre" ToolTip="Etikettre)" Target="_blank" />
                <%--BUG_7072--%>
                <table cellspacing="0" cellpadding="0" style="text-align: left;">
                        <tr class="urlapSor" id="tr1" runat="server">
                             <td class="mrUrlapCaption_nowidth" style="width: 85px;" id="tdLabelKezeles" runat="server">
                                <asp:Label ID="labelPozicio" runat="server" Text="Pozic�� megad�sa:"></asp:Label>
                             
                            </td>
                            <td runat="server" class="mrUrlapMezo" style="width: 280px;">
                                 <asp:DropDownList ID="DropDownList_Pozicio" runat="server" ToolTip="Poz�ci�" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_Pozicio_SelectedIndexChanged" >
                                     <asp:ListItem>1</asp:ListItem>
                                     <asp:ListItem>3</asp:ListItem>
                                     <asp:ListItem>5</asp:ListItem>
                                     <asp:ListItem>7</asp:ListItem>
                                     <asp:ListItem>9</asp:ListItem>
                                     <asp:ListItem>11</asp:ListItem>
                                     <asp:ListItem>13</asp:ListItem>
                                 </asp:DropDownList>
                            </td>
                        </tr>
                </table>
            </li>
            <li>
                <asp:HyperLink runat="server" ID="HyperLinkN5" Text="T�rtivev�ny hivatalos irathoz" ToolTip="5. T�rtivev�ny hivatalos irathoz" Target="_blank" />
            </li>
            <!--  <li>
                <asp:HyperLink runat="server" ID="HyperLinkN6" Text="Felad�vev�ny" ToolTip="6. Felad�vev�ny" Target="_blank" />
            </li> -->
        </ul>
    </div>

    <uc2:FormFooter ID="FormFooter1" runat="server" />

    <style type="text/css">
            .box_center {
            width: 300px;
            margin: auto;
            }

            .custom-counter {
            padding: 5px 20px;
            margin: 0;
            list-style-type: none;
            }

            .custom-counter li {
            text-align: left;
            padding: 5px 20px;
            counter-increment: step-counter;
            margin-bottom: 5px;
            }
            .custom-counter li a:hover{
            text-decoration: underline;
            }

            .custom-counter li::before {
            content: counter(step-counter);
            margin-right: 20px;
            font-size: 80%;
            background-color: rgb(180, 180, 180);
            color: white;
            font-weight: bold;
            padding: 3px 8px;
            border-radius: 11px;
            }
    </style>
</asp:Content>
