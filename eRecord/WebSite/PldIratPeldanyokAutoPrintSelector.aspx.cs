using Contentum.eRecord.Utility;
using System;
using System.Web.UI.WebControls;

public partial class PldIratPeldanyokAutoPrintSelector : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratPeldanyView");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Core();
    }

    /// <summary>
    /// Core 
    /// </summary>
    private void Core()
    {
        if (Request.QueryString["ids"] == null)
            return;

        var idList = Request.QueryString["ids"].Split(',');
        var kuldemenyek = Kuldemenyek.GetIratpeldanyKuldemenyekForSSRS(Page, idList);
        if (kuldemenyek == null)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, "A k�ldem�nyek lek�rdez�se sikertelen!");
            return;
        }

        if (kuldemenyek.Length < idList.Length)
        {
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, "Csak Expedi�lt �s Post�zott t�teleket lehet nyomtatni!");
            return;
        }
        else
        {
            ResultError.ResetErrorPanel(EErrorPanel1);
        }

        // BUG_7072
        int poz = Convert.ToInt32(DropDownList_Pozicio.SelectedValue);
        String zeroguidstr = new Guid("00000000-0000-0000-0000-000000000000").ToString() + ',';
        string uresEtikett = new String('0', poz - 1).Replace("0", zeroguidstr);
        var ids = uresEtikett + String.Join(",", kuldemenyek);

        // BUG_7072
        // Minden esetben t�megeset h�v
        SetLink(HyperLinkN1, 1, ids);
        SetLink(HyperLinkN2, 2, ids);
        SetLink(HyperLinkN3, 3, ids);
        SetLink(HyperLinkN4, 4, ids);
        SetLink(HyperLinkN5, 5, ids);
    }

    private void SetLink(HyperLink link, int docId, string ids)
    {
        link.Attributes.Add("onclick", "window.open('PldIratpeldanyokAutoPrintSSRSTomeges.aspx?docid=" + docId.ToString() + "&ids=" + ids + "')");
    }

    private string GetClientOnClick(string docid, string ids)
    {
        return string.Format("window.open('PldIratPeldanyokAutoPrint.aspx?" +
            "docId={0}" +
            "&ids={1}','WindowAxelPrint','width=500, height=200,toolbar=no, menubar=no');" +
            "return false;",
            docid, 
            ids);
    }
    // BUG_7072
    protected void DropDownList_Pozicio_SelectedIndexChanged(object sender, EventArgs e)
    {
        Core();
    }
}
