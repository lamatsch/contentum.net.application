<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PldIratPeldanyokForm.aspx.cs" Inherits="PldIratPeldanyokForm" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/PldIratPeldanyFormTab.ascx" TagName="PldIratPeldanyFormTab"
    TagPrefix="tp1" %>
<%@ Register Src="eRecordComponent/TovabbiBekuldokTab.ascx" TagName="TovabbiBekuldokTab"
    TagPrefix="tp2" %>
<%@ Register Src="eRecordComponent/MellekletekTab.ascx" TagName="MellekletekTab"
    TagPrefix="tp3" %>
<%@ Register Src="eRecordComponent/FeljegyzesekTab.ascx" TagName="FeljegyzesekTab"
    TagPrefix="tp4" %>
<%@ Register Src="eRecordComponent/CsatolmanyokTab.ascx" TagName="CsatolmanyokTab"
    TagPrefix="tp5" %>
<%@ Register Src="eRecordComponent/CsatolasokTab.ascx" TagName="CsatolasokTab" TagPrefix="tp6" %>    
<%@ Register Src="eRecordComponent/JogosultakTab.ascx" TagName="JogosultakTab" TagPrefix="tp6" %>
<%@ Register Src="eRecordComponent/BontasTab.ascx" TagName="BontasTab" TagPrefix="tp7" %>
<%@ Register Src="eRecordComponent/TortenetTab.ascx" TagName="TortenetTab" TagPrefix="tp8" %>
<%@ Register Src="eRecordComponent/KapcsolatokTab.ascx" TagName="KapcsolatokTab"
    TagPrefix="tp9" %>
<%@ Register Src="eRecordComponent/FeladatokTab.ascx" TagName="FeladatokTab" TagPrefix="tp10" %>
<%@ Register Src="eRecordComponent/UgyiratTerkepTab.ascx" TagName="UgyiratTerkepTab"
    TagPrefix="tp11" %>
<%@ Register Src="eRecordComponent/KapjakMegTab.ascx" TagName="KapjakMegTab"
    TagPrefix="tp12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IratpeldanyokFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/json2.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/libs/jquery/jquery.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/esign_v2.js" />
            <asp:ScriptReference Path="~/JavaScripts/Microsec.js" />
        </Scripts>
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/MicroSignerCallbackService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:Panel ID="PldIratpeldanyokFormPanel" runat="server">
                    <%-- OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged" --%>
                    <ajaxToolkit:TabContainer ID="PldIratpeldanyokTabContainer" runat="server" Width="100%"
                        OnActiveTabChanged="PldIratpeldanyokTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged"
                        ActiveTabIndex="0">
                        <%-- Iratp�ld�ny --%>
                        <ajaxToolkit:TabPanel ID="TabIratpeldanyPanel" runat="server">
                            <HeaderTemplate>
                                <asp:Label ID="TabIratpeldanyPanelHeader" runat="server" Text="Iratp�ld�ny"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp1:PldIratPeldanyFormTab ID="IratpeldanyTab1" runat="server"></tp1:PldIratPeldanyFormTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <ajaxToolkit:TabPanel ID="TabUgyiratTerkepPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="�gyiratt�rk�p" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp11:UgyiratTerkepTab ID="UgyiratTerkepTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <%-- Csatolm�nyok --%>
                        <ajaxToolkit:TabPanel ID="TabCsatolmanyokPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelCsatolmanyok" runat="server">
				                    <ContentTemplate>	                               
                                        <asp:Label ID="TabCsatolmanyokPanelHeader" runat="server" Text="Csatolm�nyok"></asp:Label>
					                </ContentTemplate>
					            </asp:UpdatePanel>                                     
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp5:CsatolmanyokTab ID="CsatolmanyokTab1" runat="server"></tp5:CsatolmanyokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <ajaxToolkit:TabPanel ID="TabCsatolasPanel" runat="server">
                            <HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelKapcsolatok" runat="server">
				                    <ContentTemplate>	                            
                                        <asp:Label ID="Label5" runat="server" Text="Kapcsolatok" />
					                </ContentTemplate>
					            </asp:UpdatePanel>                                           
                            </HeaderTemplate>
                            <ContentTemplate>
                                <%-- Kapcsolatok --%>
                                <tp6:CsatolasokTab ID="CsatolasokTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%-- Kapcsolatok--%>
                        <%--<ajaxToolkit:TabPanel ID="TabKapcsolatokPanel" runat="server" TabIndex="2">
							<HeaderTemplate>
								<asp:Label ID="TabKapcsolatokPanelHeader" runat="server" Text="Kapcsolat"></asp:Label>
							</HeaderTemplate>
							<ContentTemplate>
								<tp9:KapcsolatokTab ID="KapcsolatokTab1" runat="server"></tp9:KapcsolatokTab>
							</ContentTemplate>
						</ajaxToolkit:TabPanel>--%>
                        <%-- T�rt�net --%>
                        <ajaxToolkit:TabPanel ID="TabTortenetPanel" runat="server" TabIndex="3">
                            <HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelTortenet" runat="server">
				                    <ContentTemplate>                            
                                        <asp:Label ID="TabTortenetPanelHeader" runat="server" Text="T�rt�net"></asp:Label>
					                </ContentTemplate>
					            </asp:UpdatePanel>                                            
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp8:TortenetTab ID="TortenetTab1" runat="server"></tp8:TortenetTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <%-- Kapj�k m�g --%>
                        <ajaxToolkit:TabPanel ID="TabKapjakMegPanel" runat="server" TabIndex="4">
                            <HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelKapjakMeg" runat="server">
				                    <ContentTemplate>                               
                                        <asp:Label ID="TabKapjakMegPanelHeader" runat="server" Text="Kapj�k m�g"></asp:Label>
					                </ContentTemplate>
					            </asp:UpdatePanel>                                
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp12:KapjakMegTab ID="KapjakMegTab1" runat="server"></tp12:KapjakMegTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%-- Feljegyz�sek --%>
                        <%--<ajaxToolkit:TabPanel ID="TabFeljegyzesekPanel" runat="server" TabIndex="5">
							<HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelFeljegyzesek" runat="server">
				                    <ContentTemplate>                               
                                        <asp:Label ID="TabFeljegyzesekPanelHeader" runat="server" Text="Kezel�si feljegyz�sek"></asp:Label>
					                </ContentTemplate>
					            </asp:UpdatePanel>                                
                            </HeaderTemplate>
							<ContentTemplate>
								<tp4:FeljegyzesekTab ID="FeljegyzesekTab1" runat="server"></tp4:FeljegyzesekTab>
							</ContentTemplate>
						</ajaxToolkit:TabPanel>--%>
                        <%-- Feladatok --%>
                        <ajaxToolkit:TabPanel ID="TabFeladatokPanel" runat="server" TabIndex="7">
                            <HeaderTemplate>
                            <asp:UpdatePanel ID="updatePanelFeladatokHeader" runat="server">
				                <ContentTemplate>
                                    <asp:Label ID="TabFeladatokPanelHeader" runat="server" Text="Feladatok/Kezel�si feljegyz�sek"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>    
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp10:FeladatokTab ID="FeladatokTab" runat="server"></tp10:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        <ajaxToolkit:TabPanel ID="TabJogosultakPanel" runat="server" TabIndex="6">
                            <HeaderTemplate>
				                <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
				                    <ContentTemplate>                                 
                                        <asp:Label ID="TabJogosultakPanelHeader" runat="server" Text="Jogosultak" />
					                </ContentTemplate>
					            </asp:UpdatePanel>                                           
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp6:JogosultakTab ID="JogosultakTab" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
