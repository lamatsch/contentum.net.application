using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class PldIratPeldanyokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    Contentum.eUtility.PageView pageView;


    // A sz�ban forg� iratp�ld�ny objektum (megtekint�s, m�dos�t�s eset�n haszn�latos)
    private EREC_PldIratPeldanyok obj_IratPeldany = null;
    private EREC_IraIratok obj_Irat = null;
    private EREC_UgyUgyiratdarabok obj_UgyiratDarab = null;
    private EREC_UgyUgyiratok obj_Ugyirat = null;

    // TODO: Funkci�jogosults�gok!!
    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, PldIratpeldanyokTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(PldIratpeldanyokTabContainer);
        
        Command = Request.QueryString.Get(CommandName.Command);

        if (Command == CommandName.DesignView)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
        }
        else
        {
            // ha Modify m�dban pr�b�ljuk megnyitni, de csak View joga van, �tir�ny�tjuk, ha egyik sincs, hiba)
            if (Command == CommandName.Modify
                && !FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Modify)
                && FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View))
            {
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
            }
            else
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratPeldany" + Command);
            }
        }

        // FormHeader be�ll�t�sa az Iratpeldany Tab-nak:
        IratpeldanyTab1.FormHeader = FormHeader1;

        if (Command != CommandName.View)
        {
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        }


        SetEnabledAllTabsByFunctionRights();

        if (Command == CommandName.New) 
        {
            // Ha uj rekorodot vesznek fel, akkor csak az elso tab aktiv, addig amig nem nyomnak mentest!
            SetEnabledAllTabs(false);
            TabIratpeldanyPanel.Enabled = true;
        }

        if (!IsPostBack)
        {
            if (Command == CommandName.View || Command == CommandName.Modify)
            {
                String id = Request.QueryString.Get(QueryStringVars.Id);
                if (String.IsNullOrEmpty(id))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                }
                else
                {
                    // Jogosults�g ellen�rz�se:
                    CheckRights(id);
                    DisableTabsByMissingObjectRights(this.obj_IratPeldany);
                }
            }
        }

        // Esem�nyekre feliratkoz�s:
        IratpeldanyTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        UgyiratTerkepTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        
        IratpeldanyTab1.Active = true;
        IratpeldanyTab1.ParentForm = Constants.ParentForms.IratPeldany;

        UgyiratTerkepTab1.Active = false;
        UgyiratTerkepTab1.ParentForm = Constants.ParentForms.IratPeldany;

        CsatolmanyokTab1.Active = false;
        CsatolmanyokTab1.ParentForm = Constants.ParentForms.IratPeldany;
        CsatolmanyokTab1.TabHeader = TabCsatolmanyokPanelHeader;

        CsatolasokTab1.Active = false;
        CsatolasokTab1.ParentForm = Constants.ParentForms.IratPeldany;
        CsatolasokTab1.TabHeader = Label5;

        //KapcsolatokTab1.Active = false;
        //KapcsolatokTab1.ParentForm = Constants.ParentForms.IratPeldany;

        TortenetTab1.Active = false;
        TortenetTab1.ParentForm = Constants.ParentForms.IratPeldany;
        TortenetTab1.TabHeader = TabTortenetPanelHeader;

        KapjakMegTab1.Active = false;
        KapjakMegTab1.ParentForm = Constants.ParentForms.IratPeldany;
        KapjakMegTab1.TabHeader = TabKapjakMegPanelHeader;

        //FeljegyzesekTab1.Active = false;
        //FeljegyzesekTab1.ParentForm = Constants.ParentForms.IratPeldany;
        //FeljegyzesekTab1.TabHeader = TabFeljegyzesekPanelHeader;

        JogosultakTab.Active = false;
        JogosultakTab.ParentForm = Constants.ParentForms.IratPeldany;
        JogosultakTab.TabHeader = TabJogosultakPanelHeader;

        FeladatokTab.Active = false;
        FeladatokTab.ParentForm = Constants.ParentForms.IratPeldany;
        FeladatokTab.TabHeader = TabFeladatokPanelHeader;
       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            // Default panel beallitasa
            //PldIratpeldanyokTabContainer.ActiveTab = TabIratpeldanyPanel;
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in PldIratpeldanyokTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        PldIratpeldanyokTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    PldIratpeldanyokTabContainer.ActiveTab = TabIratpeldanyPanel;
                }
            }
            else
            {
                PldIratpeldanyokTabContainer.ActiveTab = TabIratpeldanyPanel;
            }
            ActiveTabRefresh(PldIratpeldanyokTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodnie, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // �j iratp�ld�ny l�trehoz�sakor Bez�r gomb elt�ntet�se, mert az majd a tablapon lesz
        if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_Close.Visible = false;
        }
    }


    /// <summary>
    /// Ha v�ltozott valamilyen tulajdons�ga az iratp�ld�nynak, jogosults�gellen�rz�s
    /// </summary>    
    void CallBack_OnChangedObjectProperties(object sender, EventArgs e)
    {
        string id = Request.QueryString.Get(QueryStringVars.Id);
        CheckRights(id);
        DisableTabsByMissingObjectRights(this.obj_IratPeldany);

        // a lek�rt objektumok tov�bbad�sa, hogy ott ne kelljen �jra lek�rni:
        IratpeldanyTab1.obj_IratPeldany = this.obj_IratPeldany;
        IratpeldanyTab1.obj_Irat = this.obj_Irat;
        IratpeldanyTab1.obj_UgyiratDarab = this.obj_UgyiratDarab;
        IratpeldanyTab1.obj_Ugyirat = this.obj_Ugyirat;
    }


    private void CheckRights(string id)
    {
        //char jogszint = Command == CommandName.View ? 'O' : 'I';

        // csak olvas�si jogszint ellen�rz�s, a m�dos�that�s�got k�s�bb k�l�n vizsg�ljuk
        char jogszint = 'O';
        Contentum.eRecord.Service.EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
        ep.Record_Id = id;

        //Result result = service.GetWithRightCheck(ep, jogszint);
        Result result = service.GetIratPeldanyHierarchiaWithRightCheck(ep, jogszint);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            PldIratpeldanyokFormPanel.Visible = false;
            return;
        }
        else
        {
            //obj_IratPeldany = (EREC_PldIratPeldanyok)result.Record;

            IratPeldanyHierarchia pldHier = (IratPeldanyHierarchia)result.Record;

            obj_IratPeldany = pldHier.IratPeldanyObj;
            obj_Irat = pldHier.IratObj;
            obj_UgyiratDarab = pldHier.UgyiratDarabObj;
            obj_Ugyirat = pldHier.UgyiratObj;
        }

        #region �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
        if (Command == CommandName.Modify || Command == CommandName.View)
        {
            bool isModosithato = false;
            String linkURL = String.Empty;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            // Irat, �gyiratdarab �s �gyirat lek�r�se a st�tuszokhoz:
            if (obj_Irat == null)
            {
                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                execParam.Record_Id = obj_IratPeldany.IraIrat_Id;

                Result result_iratGet = service_iratok.Get(execParam);
                if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratGet);
                    PldIratpeldanyokFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_Irat = (EREC_IraIratok)result_iratGet.Record;
                }
            }

            if (obj_UgyiratDarab == null)
            {
                EREC_UgyUgyiratdarabokService service_ugyiratDarab = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                execParam.Record_Id = obj_Irat.UgyUgyIratDarab_Id;

                Result result_ugyiratDarabGet = service_ugyiratDarab.Get(execParam);
                if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratDarabGet);
                    PldIratpeldanyokFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_UgyiratDarab = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
                }
            }

            if (obj_Ugyirat == null)
            {
                EREC_UgyUgyiratokService service_ugyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                execParam.Record_Id = obj_UgyiratDarab.UgyUgyirat_Id;

                Result result_ugyiratGet = service_ugyirat.Get(execParam);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
                    PldIratpeldanyokFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_Ugyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                }
            }

            //IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotById(id, execParam, FormHeader1.ErrorPanel);
            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_IratPeldany);

            // irathoz els� iratp�ld�ny �rz�j�t csak akkor �ll�tjuk be, ha ez az iratp�ld�ny volt az els�
            // TODO: minden esetben be k�ne �ll�tani az els� iratp�ld�ny �rz�j�t
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_UgyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);

            ErrorDetails errorDetail = null;
            isModosithato = IratPeldanyok.Modosithato(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            bool isModosithatoOverride = IratPeldanyok.IsModosithatoOveride(iratPeldanyStatusz, execParam);

            // M�dos�that�s�g ellen�rz�se:
            if (Command == CommandName.Modify)
            {
                if (isModosithato == false)
                {
                    string qs = QueryStringVars.Id + "=" + id + "&"
                        + Contentum.eUtility.Utils.GetViewRedirectQs(Page, new string[] { QueryStringVars.Id });

                    // Nem m�dos�that� a rekord, �tir�ny�t�s a megtekint�s oldalra
                    if (!IsPostBack)
                    {
                        Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                    }
                    else
                    {
                        UI.popupRedirect(Page, "PldIratPeldanyokForm.aspx?" + qs);
                    }
                }

            }
            else if (Command == CommandName.View)
            {
                if (isModosithato == true)
                {
                    String funkcioJog = "IratPeldany" + CommandName.Modify;
                    //jogosults�g ellen�rz�s
                    if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
                    {
                        // Original QueryString without Command and Id and SelectedTab
                        string qsOriginalParts = QueryStringVars.Id + "=" + id + "&"
                            + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                        linkURL = "PldIratPeldanyokForm.aspx?"
                                                + QueryStringVars.Command + "=" + CommandName.Modify
                                                + "&" + qsOriginalParts
                                                + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + PldIratpeldanyokTabContainer.ClientID + "') + '";
                    }
                }

            }
            // ModifyLink be�ll�t�sa
            JavaScripts.RegisterSetLinkOnForm(Page, (String.IsNullOrEmpty(linkURL) ? false : true), linkURL, FormHeader1.ModifyLink);
        }
        #endregion �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
    }

    #region Forms Tab

    protected void PldIratpeldanyokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        IratpeldanyTab1.Active = false;
        UgyiratTerkepTab1.Active = false;
        CsatolmanyokTab1.Active = false;
        //KapcsolatokTab1.Active = false;
        CsatolasokTab1.Active = false;
        TortenetTab1.Active = false;
        KapjakMegTab1.Active = false;
        //FeljegyzesekTab1.Active = false;
        JogosultakTab.Active = false;
        FeladatokTab.Active = false;
             

        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabIratpeldanyPanel))
        {
            IratpeldanyTab1.Active = true;

            IratpeldanyTab1.obj_IratPeldany = this.obj_IratPeldany;
            IratpeldanyTab1.obj_Irat = this.obj_Irat;
            IratpeldanyTab1.obj_UgyiratDarab = this.obj_UgyiratDarab;
            IratpeldanyTab1.obj_Ugyirat = this.obj_Ugyirat;

            IratpeldanyTab1.ReLoadTab(true);
        }
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabUgyiratTerkepPanel))
        {
            UgyiratTerkepTab1.Active = true;
            UgyiratTerkepTab1.ReLoadTab();
        }
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabCsatolmanyokPanel))
        {
            CsatolmanyokTab1.Active = true;
            CsatolmanyokTab1.ReLoadTab();
        }
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabCsatolasPanel))
        {
            CsatolasokTab1.Active = true;
            CsatolasokTab1.ReLoadTab();
        }
        //if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabKapcsolatokPanel))
        //{
        //    KapcsolatokTab1.Active = true;
        //    KapcsolatokTab1.ReLoadTab();
        //}
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabTortenetPanel))
        {
            TortenetTab1.Active = true;
            TortenetTab1.ReLoadTab();
        }
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabKapjakMegPanel))
        {
            KapjakMegTab1.Active = true;
            KapjakMegTab1.ReLoadTab();
        }
        //if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabFeljegyzesekPanel))
        //{
        //    FeljegyzesekTab1.Active = true;
        //    FeljegyzesekTab1.ReLoadTab();
        //}
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabJogosultakPanel))
        {
            JogosultakTab.Active = true;
            JogosultakTab.ReLoadTab();
        }
        if (PldIratpeldanyokTabContainer.ActiveTab.Equals(TabFeladatokPanel))
        {
            FeladatokTab.Active = true;
            FeladatokTab.ReLoadTab();
        }
    }
       
    #endregion
      

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void SetEnabledAllTabs(Boolean value)
    {
        TabIratpeldanyPanel.Enabled = value;
        TabUgyiratTerkepPanel.Enabled = value;
        TabCsatolmanyokPanel.Enabled = value;
        TabCsatolasPanel.Enabled = value;
        //TabKapcsolatokPanel.Enabled = value;
        TabTortenetPanel.Enabled = value;
        TabKapjakMegPanel.Enabled = value;
        //TabFeljegyzesekPanel.Enabled = value;
        TabJogosultakPanel.Enabled = value;
        TabFeladatokPanel.Enabled = value;
    }

    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni!
        TabIratpeldanyPanel.Enabled = true;
        
        TabUgyiratTerkepPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View);
                
        TabCsatolmanyokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.List);

        TabCsatolasPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolas" + CommandName.List);
        
        // TODO:
        //TabTortenetPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyTortenet" + CommandName.List);
        TabKapjakMegPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.List);
        //TabJogosultakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyJogosult" + CommandName.List);
        TabFeladatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "Feladatok" + CommandName.List);
        //TabFeladatokPanel.Enabled = true;
    }

    // ellen�rz�s �s sz�ks�g eset�n tabf�l tilt�s az aktu�lis irat alapj�n
    private void DisableTabsByMissingObjectRights(string id)
    {
        if (!IratPeldanyok.CheckRights_Csatolmanyok(Page, id))
        {
            TabCsatolmanyokPanel.Enabled = false;
        }
    }

    // ellen�rz�s �s sz�ks�g eset�n tabf�l tilt�s az aktu�lis irat alapj�n
    private void DisableTabsByMissingObjectRights(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        if (!IratPeldanyok.CheckRights_Csatolmanyok(Page, erec_PldIratPeldanyok, false))
        {
            TabCsatolmanyokPanel.Enabled = false;
        }
    }

    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            SetEnabledAllTabs(false);
            TabIratpeldanyPanel.Enabled = true;
            pageView.CompSelector.Add_ComponentOnClick(TabIratpeldanyPanel);
            pageView.CompSelector.Add_ComponentOnClick(IratpeldanyTab1);
            pageView.CompSelector.Add_ComponentOnClick(TabUgyiratTerkepPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolmanyokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolasPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTortenetPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabKapjakMegPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabFeladatokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabJogosultakPanel);
            FormFooter1.SaveEnabled = false;

        }
    }
}
