using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class PldIratPeldanyokFormTabPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iratPeldanyId = String.Empty;
    private string tipus = String.Empty;
    private string iratId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        iratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);
        tipus = Request.QueryString.Get("tipus");

        if (String.IsNullOrEmpty(iratPeldanyId) || String.IsNullOrEmpty(tipus))
        {
            // nincs Id megadva:
        }
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.Id.Value = iratPeldanyId;
        erec_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_f()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = iratPeldanyId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = false;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_d()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.IraIrat_Id.Value = iratId;
        erec_CsatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_e()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.Obj_Id.Value = iratPeldanyId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.ObjTip_Id.Value = "B29081B9-3106-4DF8-AB1F-D2008DCED7AA";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

        iratId = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Date";
        column.AutoIncrement = false;
        column.Caption = "Date";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FelhNev";
        column.AutoIncrement = false;
        column.Caption = "FelhNev";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Date"] = System.DateTime.Now.ToString();
        row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        table.Rows.Add(row);

        //string xml = "";
        //string xsd = result.Ds.GetXmlSchema();
        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        
        string filename = "";

        switch (tipus)
        {
            case "BoritoA4":
                Response.Redirect("IratPeldanyokSSRSBorito.aspx?Iratpeldanyid=" + iratPeldanyId);
                break;
            case "BoritoA3":
                Response.Redirect("IratPeldanyokSSRSBoritoA3.aspx?Iratpeldanyid=" + iratPeldanyId);
                break;
            case "Atveteli":
                //xml = "";
                templateText = "";
                filename = "Atveteli_";
                break;
            case "Kisero":
                EREC_HataridosFeladatokService f_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponents_f();

                Result f_result = f_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);

                DataTable tempTable = new DataTable();
                tempTable = f_result.Ds.Tables[0].Copy();
                tempTable.TableName = "f_Table";
                result.Ds.Tables.Add(tempTable);

                EREC_CsatolmanyokService d_service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = GetSearchObjectFromComponents_d();

                Result d_result = d_service.GetAllWithExtension(execParam, erec_CsatolmanyokSearch);

                tempTable = new DataTable();
                tempTable = d_result.Ds.Tables[0].Copy();
                tempTable.TableName = "d_Table";
                result.Ds.Tables.Add(tempTable);

                Contentum.eAdmin.Service.KRT_EsemenyekService e_service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                KRT_EsemenyekSearch krt_EsemenyekSearch = GetSearchObjectFromComponents_e();

                Result e_result = e_service.GetAllWithExtension(execParam, krt_EsemenyekSearch);

                tempTable = new DataTable();
                tempTable = e_result.Ds.Tables[0].Copy();
                tempTable.TableName = "e_Table";
                result.Ds.Tables.Add(tempTable);

                //xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                WebRequest wr = WebRequest.Create(SP_TM_site_url + "Iratpeldany kisero.xml");
                wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                templateText = template.ReadToEnd();
                template.Close();
                //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns3=\"iratplkisero\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>kiss.gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>26</o:TotalTime><o:Created>2007-10-16T14:52:00Z</o:Created><o:LastSaved>2008-11-27T08:46:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>54</o:Words><o:Characters>376</o:Characters><o:Lines>3</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>429</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000004B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"00A42649\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Cmsor1\"><w:name w:val=\"heading 1\"/><wx:uiName wx:val=\"C�msor 1\"/><w:basedOn w:val=\"Norml\"/><w:next w:val=\"Norml\"/><w:link w:val=\"Cmsor1Char\"/><w:rsid w:val=\"00FF3B28\"/><w:pPr><w:keepNext/><w:keepLines/><w:spacing w:before=\"480\" w:after=\"0\"/><w:outlineLvl w:val=\"0\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Cambria\" w:fareast=\"Times New Roman\" w:h-ansi=\"Cambria\"/><wx:font wx:val=\"Cambria\"/><w:b/><w:b-cs/><w:color w:val=\"365F91\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"lfej\"><w:name w:val=\"header\"/><wx:uiName wx:val=\"�l�fej\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"lfejChar\"/><w:rsid w:val=\"00256776\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"lfejChar\"><w:name w:val=\"�l�fej Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"lfej\"/><w:rsid w:val=\"00256776\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"llb\"><w:name w:val=\"footer\"/><wx:uiName wx:val=\"�l�l�b\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"llbChar\"/><w:rsid w:val=\"00256776\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"llbChar\"><w:name w:val=\"�l�l�b Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"llb\"/><w:rsid w:val=\"00256776\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"R�csos t�bl�zat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00256776\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"character\" w:styleId=\"Cmsor1Char\"><w:name w:val=\"C�msor 1 Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"Cmsor1\"/><w:rsid w:val=\"00FF3B28\"/><w:rPr><w:rFonts w:ascii=\"Cambria\" w:fareast=\"Times New Roman\" w:h-ansi=\"Cambria\" w:cs=\"Times New Roman\"/><w:b/><w:b-cs/><w:color w:val=\"365F91\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"3074\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:footnotePr><w:footnote w:type=\"separator\"><w:p wsp:rsidR=\"00F91DAE\" wsp:rsidRDefault=\"00F91DAE\" wsp:rsidP=\"00256776\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:footnote><w:footnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"00F91DAE\" wsp:rsidRDefault=\"00F91DAE\" wsp:rsidP=\"00256776\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:footnote></w:footnotePr><w:endnotePr><w:endnote w:type=\"separator\"><w:p wsp:rsidR=\"00F91DAE\" wsp:rsidRDefault=\"00F91DAE\" wsp:rsidP=\"00256776\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:endnote><w:endnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"00F91DAE\" wsp:rsidRDefault=\"00F91DAE\" wsp:rsidP=\"00256776\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:endnote></w:endnotePr><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00256776\"/><wsp:rsid wsp:val=\"00256776\"/><wsp:rsid wsp:val=\"00586A17\"/><wsp:rsid wsp:val=\"00971A87\"/><wsp:rsid wsp:val=\"00A42649\"/><wsp:rsid wsp:val=\"00F345E3\"/><wsp:rsid wsp:val=\"00F91DAE\"/><wsp:rsid wsp:val=\"00FF3B28\"/></wsp:rsids></w:docPr><w:body><ns3:NewDataSet><Table><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00FF3B28\"><w:rPr><w:rStyle w:val=\"Cmsor1Char\"/><w:rFonts w:fareast=\"Calibri\"/><wx:font wx:val=\"Cambria\"/></w:rPr><w:tab/></w:r><w:r wsp:rsidRPr=\"00FF3B28\"><w:rPr><w:rStyle w:val=\"Cmsor1Char\"/><w:rFonts w:fareast=\"Calibri\"/><wx:font wx:val=\"Cambria\"/><w:color w:val=\"auto\"/></w:rPr><w:t>Iratp�ld�ny k�s�r�</w:t></w:r></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Irat jellege: </w:t></w:r><Jelleg/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Iktat�sz�m: </w:t></w:r><IktatoSzam_Merge/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Iktat�s id�pontja: </w:t></w:r><IktatasDatuma/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�rkeztet� sz�m: </w:t></w:r><FullErkeztetoSzam/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Hivatkoz�si sz�m: </w:t></w:r><HivatkozasiSzam/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�gyirat</w:t></w:r><w:r wsp:rsidR=\"00971A87\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> kezel�je</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>: </w:t></w:r><Felelos_Nev/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�ld�/felad� neve:</w:t></w:r><NevSTR_Bekuldo/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>C�mzett:</w:t></w:r><NevSTR_Cimzett/></w:p></Table><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00FF3B28\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00FF3B28\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"120\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00FF3B28\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr><w:t>Csatolm�nyok</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"1809\"/><w:gridCol w:w=\"2410\"/><w:gridCol w:w=\"2268\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1307\"/></w:tblGrid><w:tr wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><w:tc><w:tcPr><w:tcW w:w=\"1809\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Csat. tulajdonosa</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2410\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�llom�ny</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2268\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Megjegyz�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�rv. kezdete</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1307\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�rv. v�ge</w:t></w:r></w:p></w:tc></w:tr><d_Table ns3:repeater=\"true\"><w:tr wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><IktatoSzam_Merge><w:tc><w:tcPr><w:tcW w:w=\"1809\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></IktatoSzam_Merge><FajlNev><w:tc><w:tcPr><w:tcW w:w=\"2410\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></FajlNev><Leiras><w:tc><w:tcPr><w:tcW w:w=\"2268\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Leiras><ErvKezd_f><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc></ErvKezd_f><ErvVege_f><w:tc><w:tcPr><w:tcW w:w=\"1307\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc></ErvVege_f></w:tr></d_Table></w:tbl><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00FF3B28\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00FF3B28\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"120\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00FF3B28\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr><w:t>Kezel�si feljegyz�sek</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"1809\"/><w:gridCol w:w=\"1701\"/><w:gridCol w:w=\"1985\"/><w:gridCol w:w=\"3717\"/></w:tblGrid><w:tr wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><w:tc><w:tcPr><w:tcW w:w=\"1809\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Id�pont</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1701\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>L�trehoz�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1985\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Kezel�si feljegyz�s t�pusa</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3717\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Lei�r�s</w:t></w:r></w:p></w:tc></w:tr><f_Table ns3:repeater=\"true\"><w:tr wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><LetrehozasIdo_f><w:tc><w:tcPr><w:tcW w:w=\"1809\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></LetrehozasIdo_f><Letrehozo_Nev><w:tc><w:tcPr><w:tcW w:w=\"1701\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Letrehozo_Nev><KezelesTipusNev><w:tc><w:tcPr><w:tcW w:w=\"1985\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></KezelesTipusNev><Leiras><w:tc><w:tcPr><w:tcW w:w=\"3717\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Leiras></w:tr></f_Table></w:tbl><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00FF3B28\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00FF3B28\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"120\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00FF3B28\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:u w:val=\"single\"/></w:rPr><w:t>Esem�nyek</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblInd w:w=\"108\" w:type=\"dxa\"/><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"1375\"/><w:gridCol w:w=\"1886\"/><w:gridCol w:w=\"2126\"/><w:gridCol w:w=\"2126\"/><w:gridCol w:w=\"1559\"/></w:tblGrid><w:tr wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><w:tc><w:tcPr><w:tcW w:w=\"1375\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Id�pont</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1886\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Azonos�t�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2126\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Esem�ny</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2126\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>V�grehajt</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Kinek</w:t></w:r></w:p></w:tc></w:tr><e_Table ns3:repeater=\"true\"><w:tr wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidTr=\"00586A17\"><LetrehozasIdo><w:tc><w:tcPr><w:tcW w:w=\"1375\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></LetrehozasIdo><Azonositoja><w:tc><w:tcPr><w:tcW w:w=\"1886\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Azonositoja><Funkciok_Nev><w:tc><w:tcPr><w:tcW w:w=\"2126\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Funkciok_Nev><Felhasznalo_Id_User_Nev><w:tc><w:tcPr><w:tcW w:w=\"2126\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00A42649\" wsp:rsidRDefault=\"00A42649\"/></w:tc></Felhasznalo_Id_User_Nev><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00FF3B28\" wsp:rsidRPr=\"00586A17\" wsp:rsidRDefault=\"00FF3B28\" wsp:rsidP=\"00586A17\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc></w:tr></e_Table></w:tbl><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�sz�tette:</w:t></w:r><w:r wsp:rsidR=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> </w:t></w:r><FelhNev/></w:p><w:p wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:tabs><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�sz�lt:</w:t></w:r><w:r wsp:rsidR=\"00586A17\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> </w:t></w:r><Date/></w:p></ParentTable></ns3:NewDataSet><w:sectPr wsp:rsidR=\"00256776\" wsp:rsidRPr=\"00256776\" wsp:rsidSect=\"00A42649\"><w:hdr w:type=\"odd\"><w:p wsp:rsidR=\"00256776\" wsp:rsidRDefault=\"00256776\" wsp:rsidP=\"00256776\"><w:pPr><w:pStyle w:val=\"lfej\"/><w:tabs><w:tab w:val=\"clear\" w:pos=\"4536\"/><w:tab w:val=\"left\" w:pos=\"6804\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>Lapsz�m:</w:t></w:r><w:r><w:tab/></w:r><w:fldSimple w:instr=\" PAGE   \\* MERGEFORMAT \"><w:r wsp:rsidR=\"00971A87\"><w:rPr><w:noProof/></w:rPr><w:t>1</w:t></w:r></w:fldSimple><w:r><w:t> </w:t></w:r></w:p></w:hdr><w:pgSz w:w=\"11906\" w:h=\"16838\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";
                filename = "Kisero_";
                break;
            default:
                //xml = "";
                templateText = "";
                break;
        }


        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }

        result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

        if (pdf)
        {
            filename = filename +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            filename = filename +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }

        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (string.IsNullOrEmpty(csop_result.ErrorCode))
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals("3"))
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

        byte[] res = (byte[])result.Record;

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elk�sz�l�s�r�l e-mail �rtes�t�st fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
            }
        }
    }
}
