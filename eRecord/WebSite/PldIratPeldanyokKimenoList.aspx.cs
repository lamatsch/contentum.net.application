using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;

public partial class PldIratPeldanyokKimenoList : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        IratpeldanyokList.ScriptManager = ScriptManager1;
        IratpeldanyokList.ListHeader = ListHeader1;

        IratpeldanyokList.InitPage();
        IratpeldanyokList.Startup = Constants.Startup.FromKimenoIratPeldany;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IratpeldanyokList.LoadPage();

        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch;

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromKimenoIratPeldany
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratpeldanyokList.IratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromKimenoIratPeldany,
            "", Defaults.PopupWidth, Defaults.PopupHeight, IratpeldanyokList.IratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        // BUG_10701
        ScriptManager.RegisterStartupScript(Page, this.GetType(), "clearElemsClick", "if (elemsClick) { elemsClick = []; }", true);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.PldIratPeldanyokKimenoHeaderTitle;
        IratpeldanyokList.PreRenderPage();
    }
}
