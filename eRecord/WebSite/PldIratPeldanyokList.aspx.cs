using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class PldIratPeldanyokList : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        IratpeldanyokList.ScriptManager = ScriptManager1;
        IratpeldanyokList.ListHeader = ListHeader1;

        IratpeldanyokList.InitPage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IratpeldanyokList.LoadPage();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        IratpeldanyokList.PreRenderPage();
    }
}
