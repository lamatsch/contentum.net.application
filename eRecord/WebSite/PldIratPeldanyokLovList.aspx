<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PldIratPeldanyokLovList.aspx.cs" Inherits="PldIratPeldanyokLovList" Title="Untitled Page" EnableEventValidation="false" %>


<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>


<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc5" %>

<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,PldIratPeldanyokLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td>
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel runat="server" ID="panelExpedialas" Visible="false" Style="text-align: left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_nowidth" colspan="4" style="text-align: left;">
                                                        <asp:Label ID="Label1" runat="server" Text="Sz�r�si felt�telek az iratp�ld�nyokra (kimen� k�ldem�ny �ssze�ll�t�s�hoz):"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short" style="width: 50px;">
                                                        <asp:Label ID="Label2" runat="server" Text="C�mzett:&nbsp;"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" style="width: 300px">
                                                        <asp:Label ID="Label_Cimzett" runat="server" Text=""></asp:Label>
                                                        <asp:TextBox ID="Cimzett_PartnerTextBox" runat="server" Width="300px" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label4" runat="server" Text="K�ld�s&nbsp;m�dja:&nbsp;"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:Label ID="Label_KuldesModja" runat="server" Text=""></asp:Label>
                                                        <asp:HiddenField ID="KuldesModja_Hiddenfield" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short " style="width: 50px;">
                                                        <asp:Label ID="Label3" runat="server" Text="C�m:&nbsp;"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" style="width: 300px">
                                                        <asp:Label ID="Label_Cim" runat="server" Text=""></asp:Label>
                                                        <asp:TextBox ID="Cim_CimekTextBox" runat="server" Width="300px" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panelSearch" runat="server">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label5" runat="server" CssClass="mrUrlapCaption" Text="�v:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc3:EvIntervallum_SearchFormControl ID="Iktatas_EvIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label6" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" style="width: 340px">
                                                        <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" EvIntervallum_SearchFormControlId="Iktatas_EvIntervallum_SearchFormControl"
                                                            Mode="Iktatokonyvek" runat="server" IsMultiSearchMode="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label7" runat="server" Text="F�sz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc5:SzamIntervallum_SearchFormControl ID="FoSzam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label8" runat="server" Text="Alsz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc5:SzamIntervallum_SearchFormControl ID="AlSzam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label10" runat="server" Text="Sorsz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc5:SzamIntervallum_SearchFormControl ID="SorSzam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label28" runat="server" Text="Vonalk�d:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc6:VonalKodTextBox ID="VonalKodTextBox1" runat="server" RequiredValidate="false" Width="130 px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="SearchButtons_Panel" runat="server" Style="padding-top: 15px; padding-bottom: 15px;">
                                            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img src="images/hu/design/spacertrans.gif" alt="" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                        <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" />

                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>


                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m"
                                                                    SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="False" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d"
                                                                    SortExpression="EREC_PldIratPeldanyok.BarCode">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="T�rgy" SortExpression="EREC_IraIratok_Targy">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="C�mzett" SortExpression="NevSTR_Cimzett">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KuldesMod_Nev" HeaderText="K�ld�sm�d" SortExpression="KuldesMod_Nev">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="P�ld�ny helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="�llapot">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%-- skontr�ban eset�n (07) skontr� v�ge megjelen�t�se --%>
                                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%# Eval("Allapot_Nev") %>' />
                                                                        <asp:Label ID="labelAllapot" runat="server" CssClass="GridViewLovListInvisibleCoulumnStyle"
                                                                            Text='<%# Eval("Allapot") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">&nbsp;
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server"
                                            ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                            onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                            AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>

