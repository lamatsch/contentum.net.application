using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Generic;

public partial class PldIratPeldanyokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterByObjektumKapcsolat;
    private string ObjektumId;


    // Expedi�l� formhoz: --> azokra az iratp�ld�nyokra kell sz�rni, ahol
    // azonos a c�m, a c�mzett, k�ld�s m�dja, �s ha kiadm�nyozand� az irat, akkor �llapota kiadm�nyozott
    private bool filterForExpedialas = false;
    private bool filterForPostazas = false;
    private string IratPeldanyId = "";

    bool KeresesExpedialaskor = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratPeldanyokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        KeresesExpedialaskor = Rendszerparameterek.GetBoolean(Page, "KIMENOKULDEMENY_ELTERO_CIMZETTEL", false);

        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);


        // Sz�r�si felt�tel van-e?
        String filterParam = Request.QueryString.Get(QueryStringVars.Filter);
        switch (filterParam)
        {
            case Constants.Expedialas:

                //   if (filterParam == Constants.Expedialas)
                {
                    filterForExpedialas = true;
                    filterForPostazas = false;

                    IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

                    if (KeresesExpedialaskor)
                    {
                        ButtonSearch.Visible = true;
                        ButtonAdvancedSearch.Visible = false;
                        Label_Cimzett.Visible = false;
                        Label_Cim.Visible = false;
                    }
                    else
                    {
                        SearchButtons_Panel.Visible = false;
                        Cimzett_PartnerTextBox.Visible = false;
                        Cim_CimekTextBox.Visible = false;
                    }

                    if (!IsPostBack)
                    {
                        InitExpedialasPanel();
                        FillGridViewSearchResult(false);
                    }

                    // m�dos�tjuk a fejl�c c�m�t is:
                    LovListHeader1.HeaderTitle = Resources.LovList.PldIratPeldanyokLovListHeaderTitle_Filtered_Expedialas;

                    panelExpedialas.Visible = true;
                    panelSearch.Visible = false;
                    break;
                }
            // nekrisz CR 2960 post�z�s iratp�ld�ny-v�laszt�
            case Constants.Postazas:
                {
                    filterForPostazas = true;
                    filterForExpedialas = false;

                    IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

                    ButtonSearch.Visible = true;
                    ButtonAdvancedSearch.Visible = true;

                    // CR 3048 Post�z�sn�l Iratp�ld�nyv�laszt�skor nincs automatikus sz�r�s
                    // nekrisz
                    //if (!IsPostBack)
                    //{
                    //    FillGridViewSearchResult(false);
                    //}

                    // m�dos�tjuk a fejl�c c�m�t is:
                    LovListHeader1.HeaderTitle = Resources.LovList.PldIratPeldanyokLovListHeaderTitle_Filtered_Postazas;

                    panelExpedialas.Visible = false;
                    panelSearch.Visible = true;
                    break;
                }
        }
        if (!IsPostBack)
        {
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvTol = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvIg = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefault();
        }

        filterByObjektumKapcsolat = Request.QueryString.Get(QueryStringVars.ObjektumKapcsolatTipus);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("PldIratPeldanyokForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokSearch.aspx", ""
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);


        if (!IsPostBack)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, Iktatas_EvIntervallum_SearchFormControl.EvTol, Iktatas_EvIntervallum_SearchFormControl.EvIg,
                true, false, LovListHeader1.ErrorPanel);

        }

    }

    private void InitExpedialasPanel()
    {
        if (String.IsNullOrEmpty(IratPeldanyId))
        {
            // hiba, hi�nyzik az iratp�ld�nyId
            ResultError.DisplayNoIdParamError(LovListHeader1.ErrorPanel);
            return;
        }
        else
        {
            // C�mzett, c�m, k�ld�s m�dj�nak meg�llap�t�sa, mert erre kell sz�rni:
            EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //execParam.Record_Id = IratPeldanyId;

            EREC_PldIratPeldanyokSearch search_pldFilter = new EREC_PldIratPeldanyokSearch();
            search_pldFilter.Id.Value = IratPeldanyId;
            search_pldFilter.Id.Operator = Query.Operators.equals;

            Result result_pldGet = service_pld.GetAllWithExtension(execParam, search_pldFilter);
            if (!String.IsNullOrEmpty(result_pldGet.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_pldGet);
                return;
            }
            else
            {
                if (result_pldGet.Ds.Tables[0].Rows.Count != 1)
                {
                    // hiba:
                    ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, "", Resources.Error.ErrorText_Query);
                    return;
                }
                //EREC_PldIratPeldanyok pld = (EREC_PldIratPeldanyok)result_pldGet.Record;

                DataRow row = result_pldGet.Ds.Tables[0].Rows[0];

                string NevSTR_Cimzett = row["NevSTR_Cimzett"].ToString();
                string CimSTR_Cimzett = row["CimSTR_Cimzett"].ToString();
                string KuldesMod_Nev = row["KuldesMod_Nev"].ToString();
                string KuldesMod = row["KuldesMod"].ToString();

                Label_Cimzett.Text = NevSTR_Cimzett;
                Label_Cim.Text = CimSTR_Cimzett;
                Label_KuldesModja.Text = KuldesMod_Nev;

                Cimzett_PartnerTextBox.Text = NevSTR_Cimzett;
                Cim_CimekTextBox.Text = CimSTR_Cimzett;
                KuldesModja_Hiddenfield.Value = KuldesMod;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        //if (IsPostBack)
        //{
        //    String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
        //    RefreshOnClientClicks(selectedRowId);
        //}
    }


    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
        disable_refreshLovList = true;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject(Page, new EREC_PldIratPeldanyokSearch(true));
        }
        else
        {
         
            search = new EREC_PldIratPeldanyokSearch(true);

            //     if (!filterForExpedialas)
            if (!filterForExpedialas)
            {
                if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    IraIktatoKonyvekDropDownList1.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
                }

                Iktatas_EvIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                if (!FoSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
                {
                    FoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);
                }

                if (!AlSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
                {
                    AlSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_IraIratokSearch.Alszam);
                }

                if (!SorSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
                {
                    SorSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Sorszam);
                }
                if (!String.IsNullOrEmpty(VonalKodTextBox1.Text))
                {
                    search.BarCode.Value = VonalKodTextBox1.Text;
                    search.BarCode.Operator = Search.GetOperatorByLikeCharater(VonalKodTextBox1.Text);
                }


            }

        }

        if (filterForExpedialas)
        {
            // TODO:
            // default sz�r�s: felhaszn�l� az �rz�, stb...

            search.FelhasznaloCsoport_Id_Orzo.Value = FelhasznaloProfil.GetFelhasznaloCsoport(UI.SetExecParamDefault(Page, new ExecParam()));
            search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

            // CR 1944: Sztorn�zott  iratp�ld�nyok ne j�jjenek a list�n, s�t, m�r itt kisz�rj�k
            // (egy�b, nem expedi�lhat� �llapotok is el�fordulhatnak, ezeket majd csak a Hozz�ad�s-n�l sz�rj�k ki)

            // Expedi�lhat� �llapotok kisz�r�se m�r itt - kiadm�nyozhat�s�gra vonatkoz� vizsg�lat majd a hozz�ad�sn�l
            search.Allapot.Value = Search.GetSqlInnerString(IratPeldanyok.GetExpedialhatoAllapotokFilter()); //KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
            search.Allapot.Operator = Query.Operators.inner; // Query.Operators.noteuqlas;

            // CR3394
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Name = "isnull(EREC_IraIratok.KiadmanyozniKell,'0')";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Value = "1";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Operator = Query.Operators.notequals;
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Group = "101";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.GroupOperator = Query.Operators.or;

            search.Extended_EREC_IraIratokSearch.Allapot.Value = KodTarak.IRAT_ALLAPOT.Kiadmanyozott;
            search.Extended_EREC_IraIratokSearch.Allapot.Operator = Query.Operators.equals;
            search.Extended_EREC_IraIratokSearch.Allapot.Group = "101";

            if (!String.IsNullOrEmpty(Cimzett_PartnerTextBox.Text))
            {
                search.NevSTR_Cimzett.Value = Cimzett_PartnerTextBox.Text;
                search.NevSTR_Cimzett.Operator = Search.GetOperatorByLikeCharater(Cimzett_PartnerTextBox.Text);
            }

            if (!String.IsNullOrEmpty(Cim_CimekTextBox.Text))
            {
                search.CimSTR_Cimzett.Value = Cim_CimekTextBox.Text;
                search.CimSTR_Cimzett.Operator = Search.GetOperatorByLikeCharater(Cim_CimekTextBox.Text);
            }

            if (!String.IsNullOrEmpty(KuldesModja_Hiddenfield.Value))
            {
                search.KuldesMod.Value = KuldesModja_Hiddenfield.Value;
                search.KuldesMod.Operator = Query.Operators.equals;
            }
        }
        else
        // nekrisz CR 2960 post�z�s iratp�ld�ny-v�laszt�
        if (filterForPostazas)
        {

            // TODO:
            // default sz�r�s: felhaszn�l� az �rz�, stb...

            // CR 3048 �rz�re nem sz�r�nk
            // nekrisz

            //search.FelhasznaloCsoport_Id_Orzo.Value = FelhasznaloProfil.GetFelhasznaloCsoport(UI.SetExecParamDefault(Page, new ExecParam()));
            //search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

            // Sztorn�zott  iratp�ld�nyok ne j�jjenek a list�n, s�t, m�r itt kisz�rj�k
            // (egy�b, nem post�zhat� �llapotok is el�fordulhatnak, ezeket majd csak a Hozz�ad�s-n�l sz�rj�k ki)

            // Post�zhat� �llapotok kisz�r�se m�r itt - kiadm�nyozhat�s�gra vonatkoz� vizsg�lat majd a hozz�ad�sn�l
            search.Allapot.Value = Search.GetSqlInnerString(IratPeldanyok.GetPostazhatoAllapotokFilter()); //KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
            search.Allapot.Operator = Query.Operators.inner; // Query.Operators.noteuqlas;

            // CR3394
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Name = "isnull(EREC_IraIratok.KiadmanyozniKell,'0')";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Value = "1";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Operator = Query.Operators.notequals;
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.Group = "101";
            search.Extended_EREC_IraIratokSearch.KiadmanyozniKell.GroupOperator = Query.Operators.or;

            search.Extended_EREC_IraIratokSearch.Allapot.Value = KodTarak.IRAT_ALLAPOT.Kiadmanyozott;
            search.Extended_EREC_IraIratokSearch.Allapot.Operator = Query.Operators.equals;
            search.Extended_EREC_IraIratokSearch.Allapot.Group = "101";

            // CR3306 Kimen� email eset�n automatikus postazas jav�t�s
            search.KuldesMod.Value = KodTarak.KULDEMENY_KULDES_MODJA.E_mail;
            // Jav�tva, a nullokat is hozza (CR3394) 
            search.KuldesMod.Operator = Query.Operators.isnullornotinner;

            // if (!String.IsNullOrEmpty(IratPeldanyId))
            // {

            //     // C�mzett, c�m, k�ld�s m�dj�nak meg�llap�t�sa, mert erre kell sz�rni:
            //     EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            //     ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //     //execParam.Record_Id = IratPeldanyId;
            //     EREC_PldIratPeldanyokSearch search_pldFilter = new EREC_PldIratPeldanyokSearch();
            //     search_pldFilter.Id.Value = IratPeldanyId;
            //     search_pldFilter.Id.Operator = Query.Operators.equals;

            //     Result result_pldGet = service.GetAllWithExtension(execParam, search_pldFilter);
            //     if (!String.IsNullOrEmpty(result_pldGet.ErrorCode))
            //     {
            //         // hiba:
            //         ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_pldGet);
            //         return;
            //     }

            //     else
            //     {
            //         if (result_pldGet.Ds.Tables[0].Rows.Count != 1)
            //         {
            //             // hiba:
            //             ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, "", Resources.Error.ErrorText_Query);
            //             return;
            //         }
            //         //EREC_PldIratPeldanyok pld = (EREC_PldIratPeldanyok)result_pldGet.Record;

            //         DataRow row = result_pldGet.Ds.Tables[0].Rows[0];

            //         string NevSTR_Cimzett = row["NevSTR_Cimzett"].ToString();
            //         string CimSTR_Cimzett = row["CimSTR_Cimzett"].ToString();
            //         string KuldesMod_Nev = row["KuldesMod_Nev"].ToString();
            //         string KuldesMod = row["KuldesMod"].ToString();


            //         if (!String.IsNullOrEmpty(NevSTR_Cimzett))
            //         {
            //             search.NevSTR_Cimzett.Value = NevSTR_Cimzett;
            //             search.NevSTR_Cimzett.Operator = Query.Operators.equals;
            //         }

            //         if (!String.IsNullOrEmpty(CimSTR_Cimzett))
            //         {
            //             search.CimSTR_Cimzett.Value = CimSTR_Cimzett;
            //             search.CimSTR_Cimzett.Operator = Query.Operators.equals;
            //         }

            //         if (!String.IsNullOrEmpty(KuldesMod))
            //         {
            //             search.KuldesMod.Value = KuldesMod;
            //             search.KuldesMod.Operator = Query.Operators.equals;
            //         }

            //         Label_Cimzett.Text = NevSTR_Cimzett;
            //         Label_Cim.Text = CimSTR_Cimzett;
            //         Label_KuldesModja.Text = KuldesMod_Nev;
            //     }


            // }
        }

        if (!String.IsNullOrEmpty(filterByObjektumKapcsolat) && !String.IsNullOrEmpty(ObjektumId))
        {
            switch (filterByObjektumKapcsolat)
            {
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz:
                    ExecParam xpmIrat = UI.SetExecParamDefault(Page, new ExecParam());
                    Iratok.Statusz iratStatusz = Iratok.GetAllapotById(ObjektumId, xpmIrat, LovListHeader1.ErrorPanel, true);
                    if (!String.IsNullOrEmpty(iratStatusz.UgyiratId))
                    {
                        search.Extended_EREC_UgyUgyiratokSearch.Id.Value = iratStatusz.UgyiratId;
                        search.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;
                    }
                    search.IraIrat_Id.Value = ObjektumId;
                    search.IraIrat_Id.Operator = Query.Operators.notequals;
                    IratPeldanyok.SetCsatolhatoSearchObject(search);
                    break;
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany:
                    search.Extended_EREC_UgyUgyiratokSearch.Id.Value = ObjektumId;
                    search.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.notequals;
                    IratPeldanyok.SetCsatolhatoSearchObject(search);
                    break;
            }
        }

       

        // CR 3048: Iratp�ld�nyok szerint rendezve
        //search.OrderBy = "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam";
        search.OrderBy = "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam,EREC_IraIratok.Alszam,EREC_PldIratPeldanyok.Azonosito";


        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                if (filterForExpedialas || filterForPostazas)
                {
                    // A kimen� k�ldem�nyben l�v�ket hozzuk, de ki kell �ket sz�rk�teni:

                    List<string> disabledValueList = new List<string>();
                    disabledValueList.Add(KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben);

                    JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                        , "labelAllapot", disabledValueList);
                }

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
