using System;
using System.Data;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;

public partial class PldIratPeldanyokPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iratPeldanyId = String.Empty;
    private string iratId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        iratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

        if (String.IsNullOrEmpty(iratPeldanyId))
        {
            // nincs Id megadva:
        }

        //LZS - BUG_4776
        if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
        {
            BoritoA3.Enabled =
            BoritoA3.Visible =
            BoritoA4.Enabled =
            BoritoA4.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.Id.Filter(iratPeldanyId);

        return erec_PldIratPeldanyokSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

            iratId = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            table.Rows.Add(row);

            //string xml = "";
            //string xsd = result.Ds.GetXmlSchema();
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");

            if (BoritoA4.Checked)
            {
                Response.Redirect("IratPeldanyokSSRSBorito.aspx?Iratpeldanyid=" + iratPeldanyId);
            }

            if (BoritoA3.Checked)
            {
                Response.Redirect("IratPeldanyokSSRSBoritoA3.aspx?Iratpeldanyid=" + iratPeldanyId);
            }

            if (Kisero.Checked)
            {
                Response.Redirect("KuldKuldemenyekPrintFormSSRSIratpeldanyKisero.aspx?Iratpeldanyid=" + iratPeldanyId);
            }
        }
    }
}
