﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;


public partial class PldIratPeldanyokSSSRS : Contentum.eUtility.UI.PageBase
{
    GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.PldIratPeldanyokSSSRS];
        visibilityState.LoadFromCustomString(visibilityFilter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_PldIratPeldanyokSearch search = null;

                search = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject(Page, new EREC_PldIratPeldanyokSearch(true));

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_PldIratPeldanyok.LetrehozasIdo DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_EREC_HataridosFeladatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_HataridosFeladatok"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;
                        case "Filter_KimenoKuldemenyId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Filter_KimenoKuldemenyIds")))
                            {
                                ReportParameters[i].Values.Add(null);
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Filter_KimenoKuldemenyIds"));
                            }
                            break;
                        case "CsnyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Csny));
                            break;
                        case "FVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.F));

                            break;
                        case "TVVisibility": // Tértivevény - jelenleg nem szerepel a riportban
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Tv));
                            break;
                        case "IktatokonyvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.IktatoHely));
                            break;
                        case "FoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Foszam));
                            break;
                        case "AlszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Alszam));
                            break;
                        case "EvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Ev));
                            break;
                        case "SorszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Sorszam));
                            break;
                        case "IktatoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.IktatoSzam_Merge));
                            break;
                        case "VonalkodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.BarCode));
                            break;
                        case "TargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.EREC_IraIratok_Targy));
                            break;
                        case "HataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.VisszaerkezesiHatarido));
                            break;
                        case "AllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Allapot_Nev));
                            break;
                        case "CimzettVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.NevSTR_Cimzett));
                            break;
                        case "CimzettCimeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.CimSTR_Cimzett));
                            break;
                        case "KuldesmodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.KuldesMod_Nev));
                            break;
                        case "PeldanyHelyeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.FelhasznaloCsoport_Id_Orzo_Nev));
                            break;
                        case "PeldanyKezelojeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Csoport_Id_Felelos_Nev));
                            break;
                        case "LetrehozasIdejeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.LetrehozasIdo_Rovid));
                            break;
                        case "Letrehozo_NevVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Letrehozo_Nev));
                            break;
                        case "PostazasIdejeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.PostazasDatuma));
                            break;
                        case "PostaiAzonositoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.RagSzam));
                            break;
                        case "IratpeldanyJellegeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Eredet_Nev));
                            break;
                        case "ZarolasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratPeldanyokListColumnName.Zarolas));
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
