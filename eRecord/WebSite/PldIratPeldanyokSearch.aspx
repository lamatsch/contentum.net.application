<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PldIratPeldanyokSearch.aspx.cs" Inherits="PldIratPeldanyokSearch" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc21" %>

<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc20" %>

<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc19" %>
<%@ Register Src="eRecordComponent/UgyiratDarabTextBox.ascx" TagName="UgyiratDarabTextBox"
    TagPrefix="uc18" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc6" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc17" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc16" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl"
    TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc15" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc12" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc10" %>

<%@ Register Src="eRecordComponent/RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc11" %>

<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>

<%@ Register Src="eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            font-weight: bold;
            padding-right: 1px;
            vertical-align: middle;
            text-align: right;
            color: #335674;
            width: 115px;
            min-width: 115px;
            white-space: nowrap;
        }

        body, html {
            overflow: auto;
        }
    </style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <br />

    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 1159px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label28" runat="server" Text="Saj�t:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 3px">
                                <asp:CheckBox ID="Sajat_CheckBox" runat="server" /></td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 3px">
                                <uc15:IraIktatoKonyvekDropDownList ID="UgyUgyir_IraIktatoKonyvekDropDownList" runat="server"
                                    Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="IraIkt_Ev_EvIntervallum_SearchFormControl"
                                    Filter_IdeIktathat="false" IsMultiSearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="�v:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="IraIkt_Ev_EvIntervallum_SearchFormControl" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="F�sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 5px">
                                <uc20:SzamIntervallum_SearchFormControl ID="UgyIratDarab_Foszam_SzamIntervallum_SearchFormControl1" runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Alsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc20:SzamIntervallum_SearchFormControl ID="IraIrat_Alszam_SzamIntervallum_SearchFormControl2" runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label29" runat="server" Text="Sorsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc20:SzamIntervallum_SearchFormControl ID="Sorszam_SzamIntervallum_SearchFormControl3" runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanel2" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">&nbsp;</td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <asp:CheckBox ID="Ugyirat_NemSajat_Cb" runat="server"
                                    Text="Nem saj�t �gyirat" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">&nbsp;</td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="IraIrat_Postazasiranya_csakBelso_CheckBox7" runat="server"
                                    Text="Csak bels� keletkez�s�ek" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label26" runat="server" Text="�gyirat:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc9:UgyiratTextBox ID="UgyUgyIr_Id_UgyiratTextBox" runat="server"
                                    SearchMode="true" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label1" runat="server" Text="K�ld�/felad� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <%--<uc18:UgyiratDarabTextBox ID="IraIrat_UgyiratDarabTextBox1" runat="server" 
                                    SearchMode="true" Validate="false" />--%>
                                <uc19:PartnerTextBox ID="Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox"
                                    runat="server" SearchMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label3" runat="server" Text="Irat t�rgya:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <asp:TextBox ID="IraIrat_Targy_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label8" runat="server" Text="Iratkateg�ria:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:KodtarakDropDownList ID="IraIrat_Kategoria_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label9" runat="server" Text="Iratp�ld�ny l�trehoz�s�nak d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="LetrehozasIdo_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Enabled="true" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label10" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="IraIrat_HivatkozasiSzam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label15" runat="server" Text="Iktat�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="IraIrat_IktatasDatuma_DatumIntervallum_SearchCalendarControl2"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label11" runat="server" Text="Iratp�ld�ny jellege:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:KodtarakDropDownList ID="Eredet_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label12" runat="server" Text="�gyint�z�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc21:FelhasznaloCsoportTextBox ID="fcstbUgyintezo" runat="server" SearchMode="true" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanel3" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label14" runat="server" Text="Pld. kezel�je:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc8:CsoportTextBox ID="Csoport_Id_Felelos_CsoportTextBox" runat="server" SearchMode="true" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label16" runat="server" Text="Pld. helye:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 456px">
                                <uc21:FelhasznaloCsoportTextBox ID="Felh_Csop_Id_Orzo_FelhasznaloCsoportTextBox" runat="server" SearchMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label2" runat="server" Text="C�mzett:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc19:PartnerTextBox ID="PartnerId_Cimzett_PartnerTextBox" runat="server" Validate="false" SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label13" runat="server" Text="Szervezetn�l j�rt:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 456px">
                                <uc8:CsoportTextBox ID="CsoportId_Felelos_Elozo_CsoportTextBox" runat="server" Validate="false" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label19" runat="server" Text="K�ld�sm�d:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc12:KodtarakDropDownList ID="KuldesMod_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label20" runat="server" Text="Elv�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:KodtarakDropDownList ID="Visszavarolag_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label32" runat="server" Text="Tov�bb�t�:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc12:KodtarakDropDownList ID="Tovabbito_KodtarakDropDownList" runat="server" />
                                &nbsp;</td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label30" runat="server" Text="Elv�rt hat�rid�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="VisszaerkezesiHatarido_DatumIntervallum_SearchCalendarControl" runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="width: 150px">
                                <asp:Label ID="Label31" runat="server" Text="Ir�ny:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="width: 400px">
                                <uc12:KodtarakDropDownList ID="IraIrat_Postazasiranya_KodtarakDropDownList" runat="server" />
                                &nbsp;</td>
                            <td class="mrUrlapCaption" style="width: 150px">
                                <%--<asp:Label ID="Label35" runat="server" Text="�tv�tel d�tuma:"></asp:Label>--%>
                            </td>
                            <td class="mrUrlapMezo">
                                <%-- <uc7:DatumIntervallum_SearchCalendarControl ID="AtvetelDatuma_DatumIntervallum_SearchCalendarControl" runat="server" Validate="false" ValidateDateFormat="true" /> --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label33" runat="server" Text="Postai azonos�t�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:RagszamTextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInput" Validate="false"></uc11:RagszamTextBox>
                                &nbsp;
                            </td>
                            <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label36" runat="server" Text="T�ny:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="VisszaerkezesDatuma_DatumIntervallum_SearchCalendarControl" runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label38" runat="server" Text="Kezel�si feljegyz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:KodtarakDropDownList ID="IraKezFeljegyz_KezelesTipus_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label37" runat="server" Text="Post�z�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="PostazasDatuma_DatumIntervallum_SearchCalendarControl" runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label39" runat="server" Text="Kezel�si feljegyz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="IraKezFeljegyz_Note_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label21" runat="server" Text="Vonalk�d:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc10:VonalKodTextBox ID="BarCode_TextBox" runat="server" CssClass="mrUrlapInput" Validate="false"></uc10:VonalKodTextBox>
                                &nbsp;</td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label18" runat="server" Text="�llapot:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" />
                                <br />
                                <asp:CheckBox ID="cbExcludeTovabbitasAlatt" Text="Tov�bb�t�s alattiak n�lk�l" Checked="true" runat="server" />
                            </td>
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="NemCsoporttagokkal_CheckBox" runat="server" Text="Saj�t jogon l�that� t�telek" />
                                <asp:CheckBox ID="CsakAktivIrat_CheckBox" runat="server" Text="Akt�v �llapotban l�v� t�telek" />
                            </td>
                        </tr>
                        <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">                            
                            <td class="mrUrlapCaption_short">                                
                                <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>

                    <table cellspacing="0" cellpadding="0" width="800">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapMezoJobb"></td>
                            <td class="mrUrlapCaptionBal" style="width: 150px">
                                <asp:CheckBox ID="Manual_Kezelhetok_CheckBox" runat="server" Text="Kezelhet�k" /></td>
                            <td class="mrUrlapMezoJobb"></td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="Manual_Elkuldottek_CheckBox" runat="server" Text="Elk�ld�ttek" />&nbsp;</td>
                            <td class="mrUrlapMezoJobb"></td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="Manual_Sztornozottak_CheckBox" runat="server" Text="Sztorn�zottak" /></td>
                            <td class="mrUrlapMezoJobb"></td>
                            <td class="mrUrlapCaptionBal" style="width: 150px">
                                <asp:CheckBox ID="Manual_Munkapeldanyok_CheckBox" runat="server" Text="Munkap�ld�nyok" /></td>
                            <td style="width: 40%"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="text-align: right;">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                        <td style="text-align: left;">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

