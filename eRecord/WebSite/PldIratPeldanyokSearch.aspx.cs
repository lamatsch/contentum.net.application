using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Collections.Generic;
using System.Data;

public partial class PldIratPeldanyokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_PldIratPeldanyokSearch);

    private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_POSTAZAS_IRANYA = "POSTAZAS_IRANYA";
    private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = KodTarak.FELADAT_ALTIPUS.kcsNev;
    private const string kcs_TOVABBITO_SZERVEZET = "TOVABBITO_SZERVEZET";
    //private const string kcs_IRAT_ALLAPOT = "IRAT_ALLAPOT";
    private const string kcs_UGYIRATDARAB_ALLAPOT = "UGYIRATDARAB_ALLAPOT";
    private const string kcs_IRATPELDANY_ALLAPOT = "IRATPELDANY_ALLAPOT";
    private const string kcs_IRAT_FAJTA = "IRAT_FAJTA";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";

    private String Startup = String.Empty;
    private bool isTUKRendszer = false;


    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        SearchHeader1.TemplateObjectType = _type;

        if (Startup == Constants.Startup.FromKimenoIratPeldany)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch;
        }

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            // Jelezz�k az iktat�k�nyv komponensnek, hogy Id-kat kell belet�lteni, nem iktat�hely �rt�ket: (BUG#4290)
            UgyUgyir_IraIktatoKonyvekDropDownList.ValuesFilledWithId = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.PldIratPeldanyokSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_PldIratPeldanyokSearch searchObject = null;
            if (Startup == Constants.Startup.FromKimenoIratPeldany)
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                {
                    searchObject = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_PldIratPeldanyokSearch(true)
                        , SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            else
            {
                if (Search.IsSearchObjectInSession(Page, _type))
                {
                    searchObject = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject(Page, new EREC_PldIratPeldanyokSearch(true));
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            LoadComponentsFromSearchObject(searchObject);

            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Validate = false;
                tr_fizikaihely.Visible = true;
                IrattariHelyLevelekDropDownTUK.Visible = true;
                //JavaScripts.SetFocus(IrattariHelyLevelekDropDownTUK);                  
            }

        }
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = null;
        if (searchObject != null) erec_PldIratPeldanyokSearch = (EREC_PldIratPeldanyokSearch)searchObject;

        if (erec_PldIratPeldanyokSearch != null)
        {
            Sajat_CheckBox.Checked = !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Manual_Sajat.Value);

            IraIkt_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
            if (isTUK)
            {
                //T�K eset�n az id lesz az �rt�k mez�nek felt�ltve, mivel itt Id szerinti keres�s lesz
                string id = erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id == null ? null : erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id.Value;
                UgyUgyir_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
                , false, IraIkt_Ev_EvIntervallum_SearchFormControl.EvTol, IraIkt_Ev_EvIntervallum_SearchFormControl.EvIg, true, false, id, SearchHeader1.ErrorPanel);
            }
            else
            {
                // Iktatokonyvek megk�l�nb�ztet� jelz�s alapj�n val� felt�lt�se:
                UgyUgyir_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, IraIkt_Ev_EvIntervallum_SearchFormControl.EvTol, IraIkt_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
            }

            UgyIratDarab_Foszam_SzamIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);

            IraIrat_Alszam_SzamIntervallum_SearchFormControl2.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);

            Sorszam_SzamIntervallum_SearchFormControl3.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Sorszam);

            // Nem saj�t �gyirat:
            Ugyirat_NemSajat_Cb.Checked = (erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch != null
                && !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Manual_Sajat.Value));

            UgyUgyIr_Id_UgyiratTextBox.Id_HiddenField =
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value;
            UgyUgyIr_Id_UgyiratTextBox.SetUgyiratTextBoxById(SearchHeader1.ErrorPanel, null);

            //IraIrat_UgyiratDarabTextBox1.Id_HiddenField =
            //    erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.UgyUgyIratDarab_Id.Value;
            //IraIrat_UgyiratDarabTextBox1.SetUgyiratDarabTextBoxById(SearchHeader1.ErrorPanel);

            Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Id_HiddenField =
               erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_Partner_Id_Bekuldo.Value;
            // BUG_7469
            // if (erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_NevSTR_Bekuldo.Value != String.Empty)
            Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Text = erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_NevSTR_Bekuldo.Value;

            IraIrat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA,
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Kategoria.Value, true, SearchHeader1.ErrorPanel);

            IraIrat_Targy_TextBox.Text = erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Value;

            IraIrat_HivatkozasiSzam_TextBox.Text = erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.HivatkozasiSzam.Value;

            LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Manual_LetrehozasIdo);

            Eredet_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_FAJTA,
                erec_PldIratPeldanyokSearch.Eredet.Value, true, SearchHeader1.ErrorPanel);

            fcstbUgyintezo.Id_HiddenField = erec_PldIratPeldanyokSearch.Manual_Ugyintezo.Value;
            fcstbUgyintezo.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            IraIrat_IktatasDatuma_DatumIntervallum_SearchCalendarControl2.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.IktatasDatuma);

            IraIrat_Postazasiranya_csakBelso_CheckBox7.Checked = erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value == KodTarak.POSTAZAS_IRANYA.Belso;

            Csoport_Id_Felelos_CsoportTextBox.Id_HiddenField = erec_PldIratPeldanyokSearch.Csoport_Id_Felelos.Value;
            Csoport_Id_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Felh_Csop_Id_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Value;
            Felh_Csop_Id_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = erec_PldIratPeldanyokSearch.Partner_Id_Cimzett.Value;
            PartnerId_Cimzett_PartnerTextBox.Text = erec_PldIratPeldanyokSearch.NevSTR_Cimzett.Value;

            CsoportId_Felelos_Elozo_CsoportTextBox.Id_HiddenField = erec_PldIratPeldanyokSearch.Csoport_Id_Felelos_Elozo.Value;
            CsoportId_Felelos_Elozo_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
                erec_PldIratPeldanyokSearch.KuldesMod.Value, true, SearchHeader1.ErrorPanel);

            Visszavarolag_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VISSZAVAROLAG,
                erec_PldIratPeldanyokSearch.Visszavarolag.Value, true, SearchHeader1.ErrorPanel);

            Tovabbito_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TOVABBITO_SZERVEZET,
                erec_PldIratPeldanyokSearch.Tovabbito.Value, true, SearchHeader1.ErrorPanel);

            VisszaerkezesiHatarido_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.VisszaerkezesiHatarido);

            IraIrat_Postazasiranya_KodtarakDropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA,
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value, true, SearchHeader1.ErrorPanel);

            //AtvetelDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
            //    erec_PldIratPeldanyokSearch.AtvetelDatuma);

            Ragszam_TextBox.Text = erec_PldIratPeldanyokSearch.Kuldemeny_Ragszam.Value;

            VisszaerkezesDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.VisszaerkezesDatuma);

            // kezel�si feljegyz�s r�sz
            if (erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch != null)
            {
                IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KEZELESI_FELJEGYZESEK_TIPUSA,
                    erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.Altipus.Value, true, SearchHeader1.ErrorPanel);

                IraKezFeljegyz_Note_TextBox.Text = erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.Leiras.Value;
            }
            else
            {
                // Extended_EREC_HataridosFeladatokSearch == null
                IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA,
                    true, SearchHeader1.ErrorPanel);
            }

            cbExcludeTovabbitasAlatt.Checked = !erec_PldIratPeldanyokSearch.IsTovabbitasAlattAllapotIncluded;


            PostazasDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_PldIratPeldanyokSearch.PostazasDatuma);

            BarCode_TextBox.Text = erec_PldIratPeldanyokSearch.BarCode.Value;

            Allapot_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATPELDANY_ALLAPOT,
                erec_PldIratPeldanyokSearch.Allapot.Value, true, SearchHeader1.ErrorPanel);

            Manual_Kezelhetok_CheckBox.Checked = !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Manual_Kezelhetok.Value);

            Manual_Elkuldottek_CheckBox.Checked = !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Manual_Elkuldottek.Value);

            //Manual_Lezart_iratok_CheckBox.Checked = !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Manual_Lezart_iratok.Value);

            Manual_Sztornozottak_CheckBox.Checked = !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Manual_Sztornozottak.Value);

            // Az oper�tort kell figyelni (isNull elvileg)
            Manual_Munkapeldanyok_CheckBox.Checked =
                !String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Manual_Alszam_MunkaanyagFilter.Operator);

            NemCsoporttagokkal_CheckBox.Checked = !erec_PldIratPeldanyokSearch.Csoporttagokkal;
            CsakAktivIrat_CheckBox.Checked = erec_PldIratPeldanyokSearch.CsakAktivIrat;

            if (isTUKRendszer)
            {
                ReloadIrattariHelyLevelekDropDownTUK(erec_PldIratPeldanyokSearch.IrattarId.Value);
            }
        }
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string value)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = ExecParam.Felhasznalo_Id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode) || result_csoportok.Ds.Tables[0].Rows.Count < 1)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }

                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, SearchHeader1.ErrorPanel);
                    if (!string.IsNullOrEmpty(value))
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(value);

                }
            }
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private EREC_PldIratPeldanyokSearch SetSearchObjectFromComponents()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = (EREC_PldIratPeldanyokSearch)SearchHeader1.TemplateObject;
        if (erec_PldIratPeldanyokSearch == null)
        {
            erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);
        }

        if (Sajat_CheckBox.Checked)
        {
            erec_PldIratPeldanyokSearch.Manual_Sajat.Filter(
                Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page)));

            // Nem kell:
            // ((Lez�rt jegyz�ken, Jegyz�kre helyezett, C�mzett �tvette, Post�zott, Sztorn�zott)
            var allapotok = new string[]
            {
                KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken,
                KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett,
                // KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette,
                // KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at,
                // KodTarak.IRATPELDANY_ALLAPOT.Postazott,
                KodTarak.IRATPELDANY_ALLAPOT.Sztornozott
            };
            erec_PldIratPeldanyokSearch.Manual_Sajat_Allapot.NotIn(allapotok);

            erec_PldIratPeldanyokSearch.Manual_Sajat_TovabbitasAlattAllapot.IsNullOrNotIn(allapotok);
        }

        if (!String.IsNullOrEmpty(UgyUgyir_IraIktatoKonyvekDropDownList.SelectedValue))
        {
            UgyUgyir_IraIktatoKonyvekDropDownList.SetSearchObject(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch);
        }

        IraIkt_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        UgyIratDarab_Foszam_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);

        IraIrat_Alszam_SzamIntervallum_SearchFormControl2.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);

        Sorszam_SzamIntervallum_SearchFormControl3.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Sorszam);

        if (Ugyirat_NemSajat_Cb.Checked)
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Manual_Sajat.NotEquals(
                Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page)));
        }

        if (!String.IsNullOrEmpty(UgyUgyIr_Id_UgyiratTextBox.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(
                UgyUgyIr_Id_UgyiratTextBox.Id_HiddenField);
        }
        //if (!String.IsNullOrEmpty(IraIrat_UgyiratDarabTextBox1.Id_HiddenField))
        //{
        //    erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.UgyUgyIratDarab_Id.Filter(
        //        IraIrat_UgyiratDarabTextBox1.Id_HiddenField);
        //}

        if (!String.IsNullOrEmpty(Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_Partner_Id_Bekuldo.Filter(
                Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Id_HiddenField);
        }
        // BUG_7469
        //else
        //{
        if (!String.IsNullOrEmpty(Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Text))
        {
            erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_NevSTR_Bekuldo.Value = Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Text;
            // BUG_7468
            erec_PldIratPeldanyokSearch.Manual_KuldKuldemenyek_NevSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuldemeny_Partner_Id_Bekuldo_PartnerTextBox.Text);
        }
        //}

        if (!String.IsNullOrEmpty(IraIrat_Kategoria_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Kategoria.Filter(
                IraIrat_Kategoria_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(IraIrat_Targy_TextBox.Text))
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Value =
                IraIrat_Targy_TextBox.Text;
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Operator = Query.Operators.contains;  //Search.GetOperatorByLikeCharater(IraIrat_Targy_TextBox.Text);  // BUG_3944 kapcs�n visszarakva

        }
        if (!String.IsNullOrEmpty(IraIrat_HivatkozasiSzam_TextBox.Text))
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.HivatkozasiSzam.Filter(
                IraIrat_HivatkozasiSzam_TextBox.Text);
        }

        LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Manual_LetrehozasIdo);

        if (!String.IsNullOrEmpty(Eredet_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.Eredet.Filter(Eredet_KodtarakDropDownList.SelectedValue);
        }

        if (!String.IsNullOrEmpty(fcstbUgyintezo.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.Manual_Ugyintezo.Filter(fcstbUgyintezo.Id_HiddenField);
        }

        IraIrat_IktatasDatuma_DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.IktatasDatuma);

        if (IraIrat_Postazasiranya_csakBelso_CheckBox7.Checked)
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Filter(KodTarak.POSTAZAS_IRANYA.Belso);
        }

        if (!String.IsNullOrEmpty(Csoport_Id_Felelos_CsoportTextBox.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.Csoport_Id_Felelos.Filter(Csoport_Id_Felelos_CsoportTextBox.Id_HiddenField);
        }
        if (!String.IsNullOrEmpty(Felh_Csop_Id_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Filter(
                Felh_Csop_Id_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField);
        }
        if (!String.IsNullOrEmpty(PartnerId_Cimzett_PartnerTextBox.Text))
        {
            //erec_PldIratPeldanyokSearch.Partner_Id_Cimzett.Filter(
            //    PartnerId_Cimzett_PartnerTextBox.Id_HiddenField);

            erec_PldIratPeldanyokSearch.NevSTR_Cimzett.Value = PartnerId_Cimzett_PartnerTextBox.Text;
            erec_PldIratPeldanyokSearch.NevSTR_Cimzett.Operator = Search.GetOperatorByLikeCharater(PartnerId_Cimzett_PartnerTextBox.Text);
        }
        if (!String.IsNullOrEmpty(CsoportId_Felelos_Elozo_CsoportTextBox.Id_HiddenField))
        {
            erec_PldIratPeldanyokSearch.Csoport_Id_Felelos_Elozo.Filter(
                CsoportId_Felelos_Elozo_CsoportTextBox.Id_HiddenField);
        }
        if (!String.IsNullOrEmpty(KuldesMod_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.KuldesMod.Filter(KuldesMod_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(Visszavarolag_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.Visszavarolag.Filter(Visszavarolag_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(Tovabbito_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.Tovabbito.Filter(Tovabbito_KodtarakDropDownList.SelectedValue);
        }

        VisszaerkezesiHatarido_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.VisszaerkezesiHatarido);

        if (!String.IsNullOrEmpty(IraIrat_Postazasiranya_KodtarakDropDownList.SelectedValue))
        {
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Filter(
                IraIrat_Postazasiranya_KodtarakDropDownList.SelectedValue);
        }

        //AtvetelDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
        //    erec_PldIratPeldanyokSearch.AtvetelDatuma);

        if (!String.IsNullOrEmpty(Ragszam_TextBox.Text))
        {
            erec_PldIratPeldanyokSearch.Kuldemeny_Ragszam.Value = Ragszam_TextBox.Text;
            erec_PldIratPeldanyokSearch.Kuldemeny_Ragszam.Operator = Search.GetOperatorByLikeCharater(Ragszam_TextBox.Text);
        }

        VisszaerkezesDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.VisszaerkezesDatuma);

        if (!String.IsNullOrEmpty(IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue))
        {
            if (erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch == null)
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();
            }
            erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.Altipus.Filter(
                IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue);
        }

        if (!String.IsNullOrEmpty(IraKezFeljegyz_Note_TextBox.Text))
        {
            if (erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch == null)
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();
            }
            erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.Leiras.Value =
                IraKezFeljegyz_Note_TextBox.Text;
            erec_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.Leiras.Operator =
                Search.GetOperatorByLikeCharater(IraKezFeljegyz_Note_TextBox.Text);
        }

        PostazasDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_PldIratPeldanyokSearch.PostazasDatuma);


        if (!String.IsNullOrEmpty(BarCode_TextBox.Text))
        {
            erec_PldIratPeldanyokSearch.BarCode.Value = BarCode_TextBox.Text;
            erec_PldIratPeldanyokSearch.BarCode.Operator = Search.GetOperatorByLikeCharater(BarCode_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Allapot_KodtarakDropDownList.SelectedValue))
        {
            // CR#1729: V�laszthat�, hogy a tov�bb�t�s alatti adott �llapot�ak is beker�ljenek a list�ba, vagy sem
            if (cbExcludeTovabbitasAlatt.Checked == false)
            {
                // A Tovabbitasalattallapot mez�ben is kell keresni: (VAGY kapcsolat)

                erec_PldIratPeldanyokSearch.Allapot.Filter(Allapot_KodtarakDropDownList.SelectedValue);
                erec_PldIratPeldanyokSearch.Allapot.OrGroup("531");

                // csak akkor keres�nk a tov�bb�t�s alatti mez�re, ha az �llapt tov�bb�t�s alatt
                string tovabbitasAlattAllapot = String.Format("(case when EREC_PldIratPeldanyok.Allapot = '{0}' then '{1}' else 'xx' end)",
                    KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt, erec_PldIratPeldanyokSearch.Allapot.Value);
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Value = tovabbitasAlattAllapot;
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.inner;
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.OrGroup("531");
            }
            else
            {

                erec_PldIratPeldanyokSearch.Allapot.Filter(Allapot_KodtarakDropDownList.SelectedValue);
                erec_PldIratPeldanyokSearch.Allapot.AndGroup("0");

                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Value = "";
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Operator = "";
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.AndGroup("0");
            }

        }
        erec_PldIratPeldanyokSearch.IsTovabbitasAlattAllapotIncluded = !cbExcludeTovabbitasAlatt.Checked;

        if (Manual_Kezelhetok_CheckBox.Checked)
        {
            IratPeldanyok.SetSearchObjectTo_Kezelhetok(erec_PldIratPeldanyokSearch);
        }
        if (Manual_Elkuldottek_CheckBox.Checked)
        {
            erec_PldIratPeldanyokSearch.Manual_Elkuldottek.In(new string[] {
                KodTarak.IRATPELDANY_ALLAPOT.Postazott,
                //KodTarak.IRATPELDANY_ALLAPOT.Azonositott,
                KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo,
                KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette,
                KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at
            });
        }
        //if (Manual_Lezart_iratok_CheckBox.Checked)
        //{
        //    // Az �gyiratdarab �llapot�ra vonatkozik, hogy az lez�rt-e
        //    erec_PldIratPeldanyokSearch.Manual_Lezart_iratok.Filter(KodTarak.UGYIRATDARAB_ALLAPOT.Lezart);
        //}
        if (Manual_Sztornozottak_CheckBox.Checked)
        {
            erec_PldIratPeldanyokSearch.Manual_Sztornozottak.Filter(KodTarak.IRATPELDANY_ALLAPOT.Sztornozott);
        }
        if (Manual_Munkapeldanyok_CheckBox.Checked)
        {
            // Munkap�ld�ny: ahol az iratok Alszam mez�je null
            erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Manual_Alszam_MunkaanyagFilter.IsNull();
        }

        erec_PldIratPeldanyokSearch.Csoporttagokkal = !NemCsoporttagokkal_CheckBox.Checked;
        erec_PldIratPeldanyokSearch.CsakAktivIrat = CsakAktivIrat_CheckBox.Checked;
        Search.SetFilter_NemLezartUgyirat(erec_PldIratPeldanyokSearch);

        if (isTUKRendszer)
        {
            if (!String.IsNullOrEmpty(IrattariHelyLevelekDropDownTUK.SelectedValue))
            {
                erec_PldIratPeldanyokSearch.IrattarId.Value = IrattariHelyLevelekDropDownTUK.SelectedValue;
                erec_PldIratPeldanyokSearch.IrattarId.Operator = Query.Operators.equals;                
            }
        }

        return erec_PldIratPeldanyokSearch;
    }



    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added: Tal�lati list�k ment�se
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_PldIratPeldanyokSearch searchObject = SetSearchObjectFromComponents();

            if (Search.IsDefaultSearchObject(_type, searchObject, GetDefaultSearchObject())) // �sszehasonl�t�s az "�res" default objektummal
            {
                // default searchobject
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_PldIratPeldanyokSearch GetDefaultSearchObject()
    {
        if (Startup == Constants.Startup.FromKimenoIratPeldany)
        {
            return Search.CreateSearchObjWithDefFilter_KimenoIratPeldany(Page);
        }
        else
        {
            return (EREC_PldIratPeldanyokSearch)Search.GetDefaultSearchObject(_type);
        }
    }

}
