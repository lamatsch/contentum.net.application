﻿using System;
using Microsoft.Reporting.WebForms;
using System.Text;
using Contentum.eRecord.BaseUtility;

public partial class PldIratPeldanyokAutoPrint : Contentum.eUtility.ReportPageBase
{
    private string[] ids = { };
    private string docId = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["ids"] != null)
        {
            ids = Request.QueryString["ids"].ToString().Split(',');
            executor = FelhasznaloProfil.FelhasznaloId(this);
            szervezet = FelhasznaloProfil.FelhasznaloSzerverzetId(this);
        }

        if (Request.QueryString["docid"] != null)
        {
            docId = Request.QueryString["docid"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var riport = "";
            var fn = "";
            switch (docId)
            {
                case "1":
                    fn = "Tértivevény kis boríték";
                    riport = "/TertivevenyKisBoritekTomeges";
                    break;
                case "2":
                    fn = "Tértivevény közepes boríték";
                    riport = "/TertivevenyKozepesBoritekTomeges";
                    break;
                case "3":
                    fn = "Tértivevény nagy boríték";
                    riport = "/TertivevenyNagyBoritekTomeges";
                    break;
                case "4":
                    fn = "Etikett";
                    riport = "/EtikettTomeges";
                    break;
                case "5":
                    fn = "Tértivevény hivatalos irathoz";
                    riport = "/TertivevenyHivatalosIrathozTomeges";
                    break;
                //case "6":
                //    riport = "/FeladovevenyTomeges";
                //    break;
                default:
                    return;
            }

            if (ids != null && ids.Length > 0)
            {
                ReportViewer1.ServerReport.ReportPath += riport;
                InitReport(ReportViewer1, false, "", "");
            
                //LZS - BUG_9561
                //Etikett az a régi módon megy
                if (docId != "4")
                {
                    RenderFileToDownload("WORD", fn);
                    //RenderFileToDownload("IMAGE", fn);
                    //Print();
                }
            }
        }
    }

    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];
                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(executor);
                            break;

                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(szervezet);
                            break;

                        case "Where":
                            break;

                        case "ids":
                            ReportParameters[i].Values.AddRange(ids);
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}

