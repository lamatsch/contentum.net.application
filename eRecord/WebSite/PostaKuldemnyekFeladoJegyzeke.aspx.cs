﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Contentum.eRecord.Service;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;

public partial class PostaKuldemnyekFeladoJegyzeke : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, new Contentum.eUIControls.eErrorPanel());
        DatumIntervallum_SearchCalendarControl1.DatumKezd = System.DateTime.Today.ToString();
        DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Today.ToString();
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

            #region szervezet adatainak default beállítása
            string customerCode = "10101309";
            string agreementID = "19708347";
            string organizationName = "Igazgatási és Hatósági Főosztály";
            // CR 3029 Adó főosztály egyedi cím hozzáadás
            string organizationAddress = "1052 Budapest, Városház utca 9-11.";
            DateTime postingDate = DateTime.Now;
            #endregion

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Guid PostaKonyv_Id = new Guid();
            if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue))
            {
               PostaKonyv_Id = new Guid(IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue);
            }
            DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(search.ErvKezd);
            Result res = new Result();
            if (PostaKonyv_Id == Guid.Empty)
            {
                res = service.KimenoKuldemenyekFeladoJegyzek(execParam, null, Convert.ToDateTime(search.ErvKezd.Value), Convert.ToDateTime(search.ErvKezd.ValueTo));
            }
            else
            {
                res = service.KimenoKuldemenyekFeladoJegyzek(execParam, PostaKonyv_Id, Convert.ToDateTime(search.ErvKezd.Value), Convert.ToDateTime(search.ErvKezd.ValueTo));
                EREC_IraIktatoKonyvekService servicePostakonyv = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam exParam_Iktato = UI.SetExecParamDefault(Page);
                exParam_Iktato.Record_Id = PostaKonyv_Id.ToString();

                Result iktatoResult = servicePostakonyv.Get(exParam_Iktato);
                if (!iktatoResult.IsError)
                {
                    EREC_IraIktatoKonyvek iktatokonyvRecord = (EREC_IraIktatoKonyvek)iktatoResult.Record;
                    customerCode = iktatokonyvRecord.PostakonyvVevokod;
                    agreementID = iktatokonyvRecord.PostakonyvMegallapodasAzon;
                    postingDate = Convert.ToDateTime(search.ErvKezd.Value);
                    // CR 3029 Adó főosztály egyedi cím hozzáadás
                    switch (agreementID)
                    {
                        case "19708350":
                            organizationName = "Adó Főosztály";
                            organizationAddress = "1052 Budapest, Városház utca 9-11.,   1364 Budapest, Pf.269";
                            break;
                        default:
                            organizationName = "Igazgatási és Hatósági Főosztály";
                            organizationAddress = "1052 Budapest, Városház utca 9-11.";
                            break;
                    }
                }
            }

            if (!res.IsError)
            {
                DataSet ds = res.Ds;

                //LZS- BUG_9045
                //A tárolt eljárás már 1-el több oszlopot ad vissza. (Iktatószám)
                //Mivel erre itt nincs szükség, ezért kiszedem a dateset-ből ezt az oszlopot.
                if (ds.Tables[0].Columns.Contains("Iktatoszam"))
                {
                    ds.Tables[0].Columns.Remove("Iktatoszam");
                }

                if (ds.Tables[1].Columns.Contains("Iktatoszam"))
                {
                    ds.Tables[1].Columns.Remove("Iktatoszam");
                }

                // BUG_11856
                if (ds.Tables[0].Columns.Contains("IratPeldanyIktatoszam"))
                {
                    ds.Tables[0].Columns.Remove("IratPeldanyIktatoszam");
                }

                if (ds.Tables[1].Columns.Contains("IratPeldanyIktatoszam"))
                {
                    ds.Tables[1].Columns.Remove("IratPeldanyIktatoszam");
                }

                try
                {
                    // CR 3029 Adó főosztály egyedi cím hozzáadás
                    SaveToExcel(ds, "Postakuldemenyek_feladojegyzeke", customerCode, agreementID, organizationName, organizationAddress, postingDate);
                }
                catch (Exception ex)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba a dokumentum előállítása során!", ex.Message);
                    ErrorUpdatePanel1.Update();
                }
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba a dokumentum előállítása során!", res.ErrorMessage);
                ErrorUpdatePanel1.Update();
            }
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    public void SaveToExcel(DataSet data, String fileName, string customerCode, string agreementID, string organizationName, string organizationAddress, DateTime postingDate)
    {
        fileName += ".xls";
        HSSFWorkbook workbook = new HSSFWorkbook();

        #region cellastílusok
        ICellStyle tBodyCellStyle = workbook.CreateCellStyle();
        tBodyCellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyCellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyCellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyCellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyCellStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyCellStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyCellStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyCellStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;

        ICellStyle tBodyEndCellStyle = workbook.CreateCellStyle();
        tBodyEndCellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyEndCellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyEndCellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
        tBodyEndCellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Medium;
        tBodyEndCellStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyEndCellStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyEndCellStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyEndCellStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Black.Index;

        ICellStyle tBodyEndCellStyle_GREY = workbook.CreateCellStyle();
        tBodyEndCellStyle_GREY.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyEndCellStyle_GREY.FillPattern = FillPattern.SolidForeground;
        tBodyEndCellStyle_GREY.BorderTop = NPOI.SS.UserModel.BorderStyle.Medium;
        tBodyEndCellStyle_GREY.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Black.Index;

        ICellStyle tBodyCellStyle_GREY = workbook.CreateCellStyle();
        tBodyCellStyle_GREY.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        tBodyCellStyle_GREY.FillPattern = FillPattern.SolidForeground;
        #endregion
        
        string sheetName = "Postaküldemények feladójegyzéke";

        // CR 3029 Adó főosztály egyedi cím hozzáadás
        ISheet sheet = BuildHeader(sheetName, workbook, organizationName, organizationAddress, agreementID, customerCode, postingDate.ToShortDateString());

        DataTable tableNyilvanTartott = data.Tables[0];
        int count = tableNyilvanTartott.Rows.Count;
        BuildWorkSheet(tableNyilvanTartott, sheetName, workbook, 11);

        //sum
        int sumBermentesitesiDij = tableNyilvanTartott.Rows.Cast<DataRow>().Select(r => Convert.ToInt32(r["BermentesitesiDij"])).Sum();
        IRow row = sheet.CreateRow(count + 11);
        row.Height = 300;
        ICell cell;
        for (int i = 1; i < 12; i++)
        {
            if (i != 10)
            {
                cell = row.CreateCell(i);
                if (i != 7)
                {
                    cell.CellStyle = tBodyEndCellStyle;
                }
                else
                {
                    cell.CellStyle = tBodyEndCellStyle_GREY;
                }
            }
        }

        cell = row.CreateCell(10);
        cell.CellStyle = tBodyEndCellStyle;

        cell.SetCellValue(sumBermentesitesiDij);

        IFont font = workbook.CreateFont();
        font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
        font.FontName = "Tahoma";
        cell.CellStyle.SetFont(font);

        row = sheet.CreateRow(count + 12);
        row.Height = 300;
        for (int i = 1; i < 12; i++)
        {
            cell = row.CreateCell(i);
            if (i != 7)
            {
                cell.CellStyle = tBodyCellStyle;
            }
            else
            {
                cell.CellStyle = tBodyCellStyle_GREY;
            }
        }
        DataTable tableKozonseges = data.Tables[1];

        foreach (DataRow r in tableKozonseges.Rows)
        {
            r[0] = Convert.ToInt32(r[0]) + count;
        }

        int lastRow = 11 + tableNyilvanTartott.Rows.Count + 2;
        BuildWorkSheet(tableKozonseges, sheetName, workbook, lastRow);

        //sum
        count = tableKozonseges.Rows.Count;
        lastRow = lastRow + count;
        int sumBermentesitesiDij2 = tableKozonseges.Rows.Cast<DataRow>().Select(r => Convert.ToInt32(r["BermentesitesiDij"])).Sum();
        row = sheet.CreateRow(lastRow);

        for (int i = 1; i < 12; i++)
        {
            if (i != 10)
            {
                cell = row.CreateCell(i);
                if (i != 7)
                {
                    cell.CellStyle = tBodyEndCellStyle;
                }
                else
                {
                    cell.CellStyle = tBodyEndCellStyle_GREY;
                }
            }
        }
        cell = row.CreateCell(10);
        cell.CellStyle = tBodyEndCellStyle;

        cell.SetCellValue(sumBermentesitesiDij2);
        cell.CellStyle.SetFont(font);

        row = sheet.CreateRow(lastRow+1);
        for (int i = 1; i < 12; i++)
        {
            cell = row.CreateCell(i);
            if (i != 7)
            {
                cell.CellStyle = tBodyCellStyle;
            }
            else
            {
                cell.CellStyle = tBodyCellStyle_GREY;
            }
        }

        int finalSum = sumBermentesitesiDij + sumBermentesitesiDij2;

        row = sheet.CreateRow(lastRow + 2);

        for (int i = 1; i < 12; i++)
        {
            if (i != 2 && i != 10)
            {
                cell = row.CreateCell(i);
                if (i != 7)
                {
                    cell.CellStyle = tBodyCellStyle;
                }
                else
                {
                    cell.CellStyle = tBodyCellStyle_GREY;
                }
            }
        }
        cell = row.CreateCell(2);
        cell.CellStyle = tBodyCellStyle;
        cell.SetCellValue("ÖSSZESEN:");
        cell.CellStyle.SetFont(font);

        cell = row.CreateCell(10);
        cell.CellStyle = tBodyCellStyle;
        cell.SetCellValue(finalSum);
        cell.CellStyle.SetFont(font);
        /* NPOI Oldalbeállítás
         * 
         * worksheet_Body.FitToPage = false; <- ezzel lehet teljesen kikapcsolni azt, hogy az oldal magasságához vagy hosszúságához méretezze az excelt.
         * 
         * worksheet_Body.PrintSetup.FitHeight = 1 <- ez a default, a szám az oldalak számát jelöli, ahova el kell férnie hosszában.
         * worksheet_Body.PrintSetup.FitWidth = 1 <- ugyanaz, mint felette, csak ez a szélességhez kötődik.
        */
        sheet.PrintSetup.FitHeight = 9999;
        sheet.PrintSetup.FitWidth = 1;
        sheet.PrintSetup.Landscape = true;
        sheet.PrintSetup.PaperSize = (short)PaperSize.A4+1;
        sheet.Autobreaks = true;

        MemoryStream memoryStream = new MemoryStream();
        workbook.Write(memoryStream);
        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.Buffer = true;
        response.AddHeader("Accept-Header", "0");
        response.AddHeader("Content-Length", memoryStream.Length.ToString());
        string contentDispositon = "inline" + (!String.IsNullOrEmpty(fileName) ? "; filename=" + fileName : "");
        response.AddHeader("Content-Disposition", contentDispositon);
        response.AddHeader("Expires", "0");
        response.AddHeader("Pragma", "cache");
        response.AddHeader("Cache-Control", "private");
        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Accept-Ranges", "bytes");
        response.BinaryWrite(memoryStream.GetBuffer());
        response.Flush();
        try
        {
            response.End();
        }
        catch
        {
        }
    }

    private ISheet BuildHeader(string sheetName, HSSFWorkbook mainBook, string feladoNeve, string feladoCime, string megallapodasAzonosito, string vevokod, string datum)
    {
        ISheet _tempWorksheet = mainBook.CreateSheet(sheetName);

        #region Általános beállítások

            //alap betűtípus
            string generalFont = "Tahoma";
            //alap fejléc betűméret
            short hDataFontSize = 12;
            //alap oszlop mező betűméret
            short tHeaderFontSize = 11;

            #region alap stílus beállítása (SHEETRE, látszatmegoldás)
            ICellStyle defaultStyle = mainBook.CreateCellStyle();
            defaultStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            defaultStyle.FillPattern = FillPattern.SolidForeground;
            for (int i = 0; i < 50; i++)
            {
                _tempWorksheet.SetDefaultColumnStyle(i, defaultStyle);  
            }
            #endregion

            #region fejléc adatsor stílus
            ICellStyle hDataStyle = mainBook.CreateCellStyle();
            IFont hDataFont = mainBook.CreateFont();
            hDataStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            hDataStyle.FillPattern = FillPattern.SolidForeground;
            hDataFont.FontName = generalFont;
            hDataFont.FontHeightInPoints = hDataFontSize;
            hDataStyle.SetFont(hDataFont);
            #endregion

            #region oszlop mező stílus
            ICellStyle HeaderCell_Style = mainBook.CreateCellStyle();
            IFont HeaderFont = mainBook.CreateFont();
            HeaderCell_Style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            HeaderCell_Style.FillPattern = FillPattern.SolidForeground;
            HeaderFont.FontName = generalFont;
            HeaderFont.FontHeightInPoints = tHeaderFontSize;
            HeaderCell_Style.SetFont(HeaderFont);
            HeaderCell_Style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.Alignment = HorizontalAlignment.Center;
            HeaderCell_Style.VerticalAlignment = VerticalAlignment.Top;
            HeaderCell_Style.WrapText = true;

            IFont italicFont = mainBook.CreateFont();
            italicFont.FontName = generalFont;
            italicFont.FontHeightInPoints = tHeaderFontSize;
            #endregion

            #region oszlop mező stílus SZÜRKE
            ICellStyle HeaderCell_Style_GREY = mainBook.CreateCellStyle();
            HeaderCell_Style_GREY.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_GREY.FillPattern = FillPattern.SolidForeground;
            HeaderCell_Style_GREY.SetFont(HeaderFont);
            HeaderCell_Style_GREY.Alignment = HorizontalAlignment.Center;
            HeaderCell_Style_GREY.VerticalAlignment = VerticalAlignment.Top;
            HeaderCell_Style_GREY.WrapText = true;
            #endregion

            #region oszlop mező stílus 90 fokkal elforgatva
            ICellStyle HeaderCell_Style_90 = mainBook.CreateCellStyle();
            HeaderCell_Style_90.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            HeaderCell_Style_90.FillPattern = FillPattern.SolidForeground;
            HeaderCell_Style_90.SetFont(HeaderFont);
            HeaderCell_Style_90.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style_90.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style_90.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style_90.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;
            HeaderCell_Style_90.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_90.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_90.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_90.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_90.Alignment = HorizontalAlignment.Center;
            HeaderCell_Style_90.VerticalAlignment = VerticalAlignment.Center;
            HeaderCell_Style_90.WrapText = true;
            HeaderCell_Style_90.Rotation = (short)90;
            #endregion

            #region oszlop mező stílus 90 fokkal elforgatva SZÜRKE
            ICellStyle HeaderCell_Style_90_GREY = mainBook.CreateCellStyle();
            HeaderCell_Style_90_GREY.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style_90_GREY.FillPattern = FillPattern.SolidForeground;
            HeaderCell_Style_90_GREY.SetFont(HeaderFont);
            HeaderCell_Style_90_GREY.Alignment = HorizontalAlignment.Center;
            HeaderCell_Style_90_GREY.VerticalAlignment = VerticalAlignment.Center;
            HeaderCell_Style_90_GREY.WrapText = true;
            HeaderCell_Style_90_GREY.Rotation = (short)90;
            #endregion

            #region Séma alapján oszlopszélességek beállítása
            _tempWorksheet.DefaultRowHeight = 340;
            _tempWorksheet.SetColumnWidth(0, 200);
            _tempWorksheet.SetColumnWidth(1, Convert_sizeToInt(3.14));
            _tempWorksheet.SetColumnWidth(2, Convert_sizeToInt(30.86));
            _tempWorksheet.SetColumnWidth(3, Convert_sizeToInt(23.5));
            _tempWorksheet.SetColumnWidth(4, Convert_sizeToInt(7));
            _tempWorksheet.SetColumnWidth(5, Convert_sizeToInt(12.86));
            _tempWorksheet.SetColumnWidth(6, Convert_sizeToInt(7.43));
            _tempWorksheet.SetColumnWidth(7, Convert_sizeToInt(3));
            _tempWorksheet.SetColumnWidth(8, Convert_sizeToInt(5));
            _tempWorksheet.SetColumnWidth(9, Convert_sizeToInt(8.5));
            _tempWorksheet.SetColumnWidth(10, Convert_sizeToInt(13.5));
            _tempWorksheet.SetColumnWidth(11, Convert_sizeToInt(11));
            #endregion

        #endregion

        #region fejléc
        //1. sor
        IRow row = _tempWorksheet.CreateRow(0);
        row.Height = 560;
        ICell cell = row.CreateCell(1);

        cell.SetCellValue("Postaküldemények feladójegyzéke");

        cell.CellStyle = mainBook.CreateCellStyle();
        IFont font = mainBook.CreateFont();
        cell.CellStyle.Alignment = HorizontalAlignment.Center;
        cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        cell.CellStyle.FillPattern = FillPattern.SolidForeground;
        font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
        font.FontHeightInPoints = 20;
        font.FontName = generalFont;
        cell.CellStyle.SetFont(font);

        NPOI.SS.Util.CellRangeAddress cra = new NPOI.SS.Util.CellRangeAddress(0, 0, 1, 11);
        _tempWorksheet.AddMergedRegion(cra);
        #endregion

        //3. sor

        row = _tempWorksheet.CreateRow(2);
        row.Height = 340;
        cell = row.CreateCell(1);
        cell.CellStyle = hDataStyle;
        // CR 3029 Adó főosztály egyedi cím hozzáadás
        //  cell.SetCellValue(String.Format("Feladó (neve, címe): {0}, 1052 Budapest, Városház utca 9-11.", feladoNeve));
        cell.SetCellValue(String.Format("Feladó: {0}, {1}", feladoNeve, feladoCime));
        cra = new NPOI.SS.Util.CellRangeAddress(2, 2, 1, 5);
        _tempWorksheet.AddMergedRegion(cra);

        cell = row.CreateCell(6);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue("Fizetési mód: átutalással");

        cra = new NPOI.SS.Util.CellRangeAddress(2, 2, 6, 11);
        _tempWorksheet.AddMergedRegion(cra);

        //4. sor
        row = _tempWorksheet.CreateRow(3);
        row.Height = 340;
        cell = row.CreateCell(1);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue(String.Format("Megállapodás azonosító: {0}", megallapodasAzonosito));

        cra = new NPOI.SS.Util.CellRangeAddress(3, 3, 1, 5);
        _tempWorksheet.AddMergedRegion(cra);

        cell = row.CreateCell(6);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue(String.Format("Dátum: {0}", datum));

        cra = new NPOI.SS.Util.CellRangeAddress(3, 3, 6, 11);
        _tempWorksheet.AddMergedRegion(cra);

        //5. sor
        row = _tempWorksheet.CreateRow(4);
        row.Height = 340;
        cell = row.CreateCell(1);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue(String.Format("Vevőkód: {0}", vevokod));

        cra = new NPOI.SS.Util.CellRangeAddress(4, 4, 1, 5);
        _tempWorksheet.AddMergedRegion(cra);

        cell = row.CreateCell(6);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue("Bérm. gép kezdőállása: …………………………………………");

        cra = new NPOI.SS.Util.CellRangeAddress(4, 4, 6, 11);
        _tempWorksheet.AddMergedRegion(cra);

        //6. sor
        row = _tempWorksheet.CreateRow(5);
        row.Height = 340;
        cell = row.CreateCell(1);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue(String.Format("Bevizsgálási jegyzőkönyvek száma:"));

        cra = new NPOI.SS.Util.CellRangeAddress(5, 5, 1, 5);
        _tempWorksheet.AddMergedRegion(cra);

        cell = row.CreateCell(6);
        cell.CellStyle = hDataStyle;

        cell.SetCellValue("Bérm. gép záróállása: ……………………………………………");

        cra = new NPOI.SS.Util.CellRangeAddress(5, 5, 6, 11);
        _tempWorksheet.AddMergedRegion(cra);

        //8. sor
        row = _tempWorksheet.CreateRow(7);
        row.Height = 340;
        for (int i = 1; i < 12; i++)
        {
            if (i != 2)
            {
                cell = row.CreateCell(i);
                cell.CellStyle = HeaderCell_Style;
            }
        }
        cell = row.CreateCell(2);

        cell.SetCellValue("Küldemény");

        cell.CellStyle = mainBook.CreateCellStyle();
        cell.CellStyle.Alignment = HorizontalAlignment.Center;
        cell.CellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
        cell.CellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
        cell.CellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
        cell.CellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;
        cell.CellStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        cell.CellStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        cell.CellStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
        cell.CellStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;

        font = mainBook.CreateFont();
        font.FontName = generalFont;
        font.FontHeightInPoints = tHeaderFontSize;
        cell.CellStyle.Alignment = HorizontalAlignment.Center;
        font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
        cell.CellStyle.SetFont(font);

        cra = new NPOI.SS.Util.CellRangeAddress(7, 7, 2, 9);
        _tempWorksheet.AddMergedRegion(cra);

        //9. sor
        row = _tempWorksheet.CreateRow(8);
        row.Height = 340;
        for (int i = 1; i < 12; i++)
        {
            if (i != 4)
            {
                cell = row.CreateCell(i);
                if (i != 7)
                {
                    cell.CellStyle = HeaderCell_Style;
                }
                else
                {
                    cell.CellStyle = HeaderCell_Style_GREY;
                }
            }
        }
        cell = row.CreateCell(4);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Rendeltetési hely");

        cra = new NPOI.SS.Util.CellRangeAddress(8, 8, 4, 5);
        _tempWorksheet.AddMergedRegion(cra);

        //10. sor
        row = _tempWorksheet.CreateRow(9);
        row.Height = 1995;
        cell = row.CreateCell(1);
        cell.CellStyle = HeaderCell_Style_90;

        cell.SetCellValue("Folyószám");

        cell = row.CreateCell(2);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Küldemény azonosító \n (közönséges levélküldeménynél küldemény fajta)");

        cell = row.CreateCell(3);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Címzett neve \n (közönséges levélküldeménynél darabszám)");

        cell = row.CreateCell(4);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Irányítószám/ország");

        cell = row.CreateCell(5);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("település, címhely");

        cell = row.CreateCell(6);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Tömeg (g)");

        cell = row.CreateCell(7);
        cell.CellStyle = HeaderCell_Style_90_GREY;

        cell.SetCellValue("Gépi feld. alkalmas");

        cell = row.CreateCell(8);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Érték(Ft)");

        cell = row.CreateCell(9);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Fizetést követő kézbesítés összege/Utánvétel/Árufizetés");

        cell = row.CreateCell(10);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Bérmente-sítésidíj (Ft)");

        cell = row.CreateCell(11);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("Igénybevett többletszolgáltatások, küldemény típusok, megjegyzések");

        //11. sor
        row = _tempWorksheet.CreateRow(10);
        row.Height = 340;
        cell = row.CreateCell(1);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("1.");

        cell = row.CreateCell(2);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("2.");

        cell = row.CreateCell(3);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("3.");

        cell = row.CreateCell(4);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("4.");

        cell = row.CreateCell(5);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("5.");

        cell = row.CreateCell(6);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("6.");

        cell = row.CreateCell(7);
        cell.CellStyle = HeaderCell_Style_GREY;

        cell.SetCellValue("7.");

        cell = row.CreateCell(8);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("8.");

        cell = row.CreateCell(9);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("9.");

        cell = row.CreateCell(10);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("10.");

        cell = row.CreateCell(11);
        cell.CellStyle = HeaderCell_Style;

        cell.SetCellValue("11.");

       
        return _tempWorksheet;
    }

    private ISheet BuildWorkSheet(DataTable table, string sheetName, HSSFWorkbook mainBook, int startRow)
    {
        ISheet _tempWorksheet = mainBook.GetSheet(sheetName);

        #region Általános beállítások
        #region Állítható kezdőpont koordináták a Munkafüzeten
        int col_StartNumber = 1;
        int row_StartNumber = startRow;
        #endregion

        //alap betűtípus
        string generalFont = "Tahoma";
        //alap sor betűméret
        short tBodyFontSize = 10;

        #region CellaStílusok
            #region sor stílus
            ICellStyle tBodyStyle = mainBook.CreateCellStyle();
            IFont tBodyFont = mainBook.CreateFont();
            tBodyStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            tBodyStyle.FillPattern = FillPattern.SolidForeground;
            tBodyFont.FontName = generalFont;
            tBodyFont.FontHeightInPoints = tBodyFontSize;
            tBodyStyle.SetFont(tBodyFont);
            tBodyStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
            tBodyStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
            tBodyStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
            tBodyStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;
            tBodyStyle.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            tBodyStyle.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            tBodyStyle.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            tBodyStyle.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            tBodyStyle.WrapText = true;
            tBodyStyle.VerticalAlignment = VerticalAlignment.Top;
            #endregion

            #region sor szürke stílus
            ICellStyle tBodyGreyStyle = mainBook.CreateCellStyle();
            IFont tBodyGreyFont = mainBook.CreateFont();
            tBodyGreyStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            tBodyGreyStyle.FillPattern = FillPattern.SolidForeground;
            tBodyGreyFont.FontName = generalFont;
            tBodyGreyFont.FontHeightInPoints = tBodyFontSize;
            tBodyGreyStyle.SetFont(tBodyGreyFont);
            #endregion

        #endregion


            #region Páratlan formázások

            #region Páratlan DateTime formázás
            ICellStyle BodyCell_StyleOdd_DateTime = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleOdd_DateTime = mainBook.CreateDataFormat();
        BodyCell_StyleOdd_DateTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
        BodyCell_StyleOdd_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
        BodyCell_StyleOdd_DateTime.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páratlan DateTime formázás (Ha nincs idő)
        ICellStyle BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateDataFormat();
        BodyCell_StyleOdd_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
        BodyCell_StyleOdd_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
        BodyCell_StyleOdd_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páratlan Integer formázás
        ICellStyle BodyCell_StyleOdd_Number = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleOdd_Number = mainBook.CreateDataFormat();
        BodyCell_StyleOdd_Number.DataFormat = dataFormat_BodyCell_StyleOdd_Number.GetFormat("0");
        BodyCell_StyleOdd_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
        BodyCell_StyleOdd_Number.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páratlan Double formázás
        ICellStyle BodyCell_StyleOdd_Double = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleOdd_Double = mainBook.CreateDataFormat();
        BodyCell_StyleOdd_Double.DataFormat = dataFormat_BodyCell_StyleOdd_Double.GetFormat("#,##0.###");
        BodyCell_StyleOdd_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
        BodyCell_StyleOdd_Double.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páratlan String formázás
        ICellStyle BodyCell_StyleOdd_String = mainBook.CreateCellStyle();
        BodyCell_StyleOdd_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
        BodyCell_StyleOdd_String.FillPattern = FillPattern.SolidForeground;
        #endregion

        #endregion

        #region Páros formázások

        #region Páros DateTime formázás
        ICellStyle BodyCell_StyleDuo_DateTime = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleDuo_DateTime = mainBook.CreateDataFormat();
        BodyCell_StyleDuo_DateTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
        BodyCell_StyleDuo_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        BodyCell_StyleDuo_DateTime.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páros DateTime formázás (Ha nincs idő)
        ICellStyle BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateDataFormat();
        BodyCell_StyleDuo_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
        BodyCell_StyleDuo_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        BodyCell_StyleDuo_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páros Integer formázás
        ICellStyle BodyCell_StyleDuo_Number = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleDuo_Number = mainBook.CreateDataFormat();
        BodyCell_StyleDuo_Number.DataFormat = dataFormat_BodyCell_StyleDuo_Number.GetFormat("0");
        BodyCell_StyleDuo_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        BodyCell_StyleDuo_Number.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páros Double formázás
        ICellStyle BodyCell_StyleDuo_Double = mainBook.CreateCellStyle();
        IDataFormat dataFormat_BodyCell_StyleDuo_Double = mainBook.CreateDataFormat();
        BodyCell_StyleDuo_Double.DataFormat = dataFormat_BodyCell_StyleDuo_Double.GetFormat("#,##0.###");
        BodyCell_StyleDuo_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        BodyCell_StyleDuo_Double.FillPattern = FillPattern.SolidForeground;
        #endregion

        #region Páros String formázás
        ICellStyle BodyCell_StyleDuo_String = mainBook.CreateCellStyle();
        BodyCell_StyleDuo_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
        BodyCell_StyleDuo_String.FillPattern = FillPattern.SolidForeground;
        #endregion

        #endregion

        #endregion

        #region Tábla adatainak betöltése az Excelbe
        int rowIndex_Incremental = row_StartNumber;
        foreach (DataRow rowItem in table.Rows)
        {
            IRow new_Row_Body = _tempWorksheet.CreateRow(rowIndex_Incremental);
            foreach (DataColumn colItem in table.Columns)
            {
                ICell new_Cell_Body = new_Row_Body.CreateCell(colItem.Ordinal + col_StartNumber);
                if (colItem.Ordinal != 0 && colItem.Ordinal != 9)
                {
                    new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                }
                else
                {
                    int value = 0;
                    if (Int32.TryParse(rowItem[colItem.ColumnName].ToString(), out value))
                    {
                        new_Cell_Body.SetCellValue(value);
                    }
                    else
                    {
                        new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                    }
                }
                if (colItem.Ordinal != 6)
                {
                    new_Cell_Body.CellStyle = tBodyStyle;
                }
                else
                {
                    new_Cell_Body.CellStyle = tBodyGreyStyle;
                }
            }

            rowIndex_Incremental++;
        }
        #endregion
        return _tempWorksheet;
    }

    private TypeCode excel_ObjectParser(string needToParse)
    {
        DateTime parser_DateTime = new DateTime();
        Double parser_Double = new Double();
        Char parser_Char = new Char();
        Int64 parser_Int = new Int64();

        bool isAzonosito = Regex.IsMatch(needToParse.Replace(" ", ""), @"\d{1,4}\/\d{1,4}\/\d{1,4}");

        if (DateTime.TryParse(needToParse, out parser_DateTime) && !isAzonosito)
        {
            return Type.GetTypeCode(parser_DateTime.GetType());
        }
        if (Char.TryParse(needToParse, out parser_Char))
        {
            return Type.GetTypeCode(parser_Char.GetType());
        }
        if (Int64.TryParse(needToParse, out parser_Int))
        {
            return Type.GetTypeCode(parser_Int.GetType());
        }
        if (Double.TryParse(needToParse, out parser_Double))
        {
            return Type.GetTypeCode(parser_Double.GetType());
        }

        return Type.GetTypeCode(needToParse.GetType());
    }

    private int Convert_sizeToInt(double input)
    {
        return Convert.ToInt32(Math.Round(input * 310));
    }
}