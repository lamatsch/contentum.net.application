﻿<%@ Page Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true" 
    CodeFile="PostakonyvekPrintFormSSRS.aspx.cs" Inherits="IraPostakonyvekPrintForm" 
    Title="Untitled Page"  %>   

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl" TagPrefix="uc4" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
</asp:ScriptManager>

 <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IraPostaKonyvekFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPostazasIdopontja" runat="server" Text="Postázás időpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
							<td>
			                   <asp:Button runat = "server" Text = "Lekérdez" ID = "btnLekerdez" OnClick="btnLekerdez_Click" />
			                </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                
            </td>
			  
			
        </tr>
    </table>



<asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
        <table cellpadding="0" cellspacing="0" width="95%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" 
                                    Width="1200px" Height="1000px" EnableTelemetry="false">
                                   <ServerReport ReportPath = "/Sablonok/Postakonyvek" />
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
    </table>
</asp:Content>