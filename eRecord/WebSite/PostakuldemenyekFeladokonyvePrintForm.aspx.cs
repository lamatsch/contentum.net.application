﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class PostakuldemenyekFeladokonyvePrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";
        // (Postakönyv jelenleg nem zárható le)
        IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, false, new Contentum.eUIControls.eErrorPanel());
        DatumCalendarControl1.Text = System.DateTime.Today.ToString();
    }

   

    private EREC_KuldKuldemenyekSearch _GetSearchObjectFromComponents_sima_level(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        // BUG_1353
        //erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta.Value = "'19','20'";
        erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta.Value = "'08','09','10','11','19','20'";
        erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta.Operator = Query.Operators.notinner;
        erec_KuldKuldemenyekSearch.Elsobbsegi.Value = "0";
        erec_KuldKuldemenyekSearch.Elsobbsegi.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.Ajanlott.Value = "0";
        erec_KuldKuldemenyekSearch.Ajanlott.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.Tertiveveny.Value = "0";
        erec_KuldKuldemenyekSearch.Tertiveveny.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.SajatKezbe.Value = "0";
        erec_KuldKuldemenyekSearch.SajatKezbe.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.E_ertesites.Value = "0";
        erec_KuldKuldemenyekSearch.E_ertesites.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.E_elorejelzes.Value = "0";
        erec_KuldKuldemenyekSearch.E_elorejelzes.Operator = Query.Operators.equals;
        erec_KuldKuldemenyekSearch.PostaiLezaroSzolgalat.Value = "0";
        erec_KuldKuldemenyekSearch.PostaiLezaroSzolgalat.Operator = Query.Operators.equals;
        
        return erec_KuldKuldemenyekSearch;
    }
    
    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            

            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            
            //search.WhereByManual = " (EREC_KuldKuldemenyek.KimenoKuldemenyFajta in ('08','09','10','11','19','20')" +
            //                      "or EREC_KuldKuldemenyek.Elsobbsegi = '1' or EREC_KuldKuldemenyek.Ajanlott = '1' " +
            //                      "or EREC_KuldKuldemenyek.Tertiveveny = '1' or EREC_KuldKuldemenyek.SajatKezbe = '1' " +
            //                      "or EREC_KuldKuldemenyek.E_ertesites = '1' or EREC_KuldKuldemenyek.E_elorejelzes = '1' " +
            //                      "or EREC_KuldKuldemenyek.PostaiLezaroSzolgalat = '1')";
            //search.OrderBy = " order_RagSzam,EREC_KuldKuldemenyek.BeerkezesIdeje ";

            //Result result = service.KimenoKuldGetAllWithExtension(execParam, search);

            //sima_level_search = _GetSearchObjectFromComponents_sima_level(sima_level_search);

            //sima_level_search.OrderBy = " order_RagSzam,EREC_KuldKuldemenyek.BeerkezesIdeje ";

            //Result sima_level_result = service.KimenoKuldGetAllWithExtension(execParam, sima_level_search);

            

            //DataTable table = new DataTable("ParentTable");
            //DataColumn column;
            //DataRow row;
            //column = new DataColumn();
            //column.DataType = System.Type.GetType("System.Int32");
            //column.ColumnName = "id";
            //column.ReadOnly = true;
            //column.Unique = true;
            //table.Columns.Add(column);
            //column = new DataColumn();
            //column.DataType = System.Type.GetType("System.String");
            //column.ColumnName = "VevoKod";
            //column.AutoIncrement = false;
            //column.Caption = "VevoKod";
            //column.ReadOnly = false;
            //column.Unique = false;
            //table.Columns.Add(column);
            //column = new DataColumn();
            //column.DataType = System.Type.GetType("System.String");
            //column.ColumnName = "MegallapodasAzon";
            //column.AutoIncrement = false;
            //column.Caption = "MegallapodasAzon";
            //column.ReadOnly = false;
            //column.Unique = false;
            //table.Columns.Add(column);
            //column = new DataColumn();
            //column.DataType = System.Type.GetType("System.String");
            //column.ColumnName = "Date";
            //column.AutoIncrement = false;
            //column.Caption = "Date";
            //column.ReadOnly = false;
            //column.Unique = false;
            //table.Columns.Add(column);
            //DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            //PrimaryKeyColumns[0] = table.Columns["id"];
            //table.PrimaryKey = PrimaryKeyColumns;
            //result.Ds.Tables.Add(table);

            //EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            //EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            //erec_IraIktatoKonyvekSearch.Id.Value = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
            //erec_IraIktatoKonyvekSearch.Id.Operator = Query.Operators.equals;

            //Result posta_result = erec_IraIktatoKonyvekService.GetAll(execParam, erec_IraIktatoKonyvekSearch);

            //row = table.NewRow();
            //row["id"] = 0;
            //if (string.IsNullOrEmpty(posta_result.ErrorCode))
            //{
            //    row["VevoKod"] = posta_result.Ds.Tables[0].Rows[0]["PostakonyvVevokod"];
            //    row["MegallapodasAzon"] = posta_result.Ds.Tables[0].Rows[0]["PostakonyvMegallapodasAzon"];
            //}
            //row["Date"] = System.DateTime.Now.ToString();
            //table.Rows.Add(row);

            Response.Redirect("PostakuldemenyekFeladokonyvePrintFormSSRS.aspx?Iktatokonyvid="+ IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue+"&datum="+ DatumCalendarControl1.Text.Replace(" ", "").Remove(10).Replace(".", "-"));
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
