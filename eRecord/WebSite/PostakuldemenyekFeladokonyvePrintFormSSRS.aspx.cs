using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class PostakuldemenyekFeladokonyvePrintForm : Contentum.eUtility.ReportPageBase
{
    private string iktatokonyvId = String.Empty;
    private string datum = String.Empty;

    private string executor = String.Empty;
    private string szervezet = String.Empty;
    private string kornyezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        kornyezet = FelhasznaloProfil.OrgKod(Page);
        if (!kornyezet.Equals("NMHH"))
        {
            kornyezet = "";
        }
        else // BUG_3840
        {
            if (!ReportViewer1.ServerReport.ReportPath.EndsWith("NMHH"))
            {
                ReportViewer1.ServerReport.ReportPath += "NMHH";
            }
        }
        iktatokonyvId = Request.QueryString["iktatokonyvid"].ToString();
        
        datum = Request.QueryString["datum"].ToString();

        if (String.IsNullOrEmpty(iktatokonyvId) || String.IsNullOrEmpty(datum))
        {
            // nincs Id megadva:
           ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitReport(ReportViewer1, true, "Postaküldemények feladókönyve", kornyezet); // NOTE: pdf-ben nincsenek elcsúszva az oszlopok
            
            //InitReport(ReportViewer1, false, "", kornyezet);
            //RenderFileToDownload("WORDOPENXML", "Postaküldemények feladókönyve");
            //RenderFileToDownload("WORD", "Postaküldemények feladókönyve");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        //  FormFooter1.ImageButton_Save.Visible = true;
    }

    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                executor = execParam.LoginUser_Id;
                szervezet = execParam.Org_Id;
                execParam.Fake = true;
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "ExecutorUserId":
                            if (!string.IsNullOrEmpty(executor))
                            {
                                ReportParameters[i].Values.Add(executor);
                            }
                            break;

                        case "FelhasznaloSzervezet_Id":
                            if (!string.IsNullOrEmpty(szervezet))
                            {
                                ReportParameters[i].Values.Add(szervezet);
                            }
                            break;

                        case "IktatokonyvId":
                            if (!string.IsNullOrEmpty(iktatokonyvId))
                            {
                                ReportParameters[i].Values.Add(iktatokonyvId);
                            }
                            break;

                        case "BelyegzoDatuma":
                            if (!string.IsNullOrEmpty(datum))
                            {
                                ReportParameters[i].Values.Add(datum);
                            }
                            break;

                        case "Where":
                            ReportParameters[i].Values.Add("EREC_KuldKuldemenyek.ErvKezd <= getdate() and EREC_KuldKuldemenyek.ErvVege >= getdate() and EREC_KuldKuldemenyek.IraIktatokonyv_Id = '"+iktatokonyvId+ "' and EREC_KuldKuldemenyek.PostazasIranya=2 and EREC_KuldKuldemenyek.allapot <>90 and cast(EREC_KuldKuldemenyek.BelyegzoDatuma as date) = '"+datum+"'");
                            break;

                        case "WhereId":
                            ReportParameters[i].Values.Add("EREC_IraIktatokonyvek.ErvKezd <= getdate() and EREC_IraIktatokonyvek.ErvVege >= getdate() and EREC_IraIktatoKonyvek.Id = '"+iktatokonyvId+"'");
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
