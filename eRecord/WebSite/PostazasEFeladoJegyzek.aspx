﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PostazasEFeladoJegyzek.aspx.cs" Inherits="PostazasEFeladoJegyzek" Title="Postázás - E-Feladójegyzék" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/MultiGridViewPager.ascx" TagName="MultiGridViewPager"
    TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc15" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/EFeladoJegyzekUgyfelAdatokPanel.ascx" TagName="EFeladoJegyzekUgyfelAdatokPanel"
    TagPrefix="uap" %>
<%@ Register Src="eRecordComponent/EFeladoJegyzekStatisztikaPanel.ascx" TagName="EFeladoJegyzekStatisztikaPanel"
    TagPrefix="stat" %>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup"
    TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <%--    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>--%>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Postázás - E-Feladójegyzék" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanelKereses" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelPostakonyv" runat="server" Text="Postakönyv:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList_Postakonyv" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelIdoszak" runat="server" Text="Időszak:" />
                            </td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="true" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelKikuldoStar" runat="server" Text="*" CssClass="ReqStar" />
                                <asp:Label ID="labelKikuldo" runat="server" Text="Kiküldő:" />
                            </td>
                            <td class="mrUrlapMezo" width="0%" colspan="1">
                                <uc15:CsoportTextBox ID="KimenoKuldemeny_Kikuldo_CsoportTextBox" runat="server" Validate="false"
                                    LabelRequiredIndicatorID="labelKikuldoStar" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelTetelTipus" runat="server" Text="Tételek típusa:" />
                            </td>
                            <td class="mrUrlapMezo" width="0%" colspan="1">
                                <asp:CheckBox ID="cbKozonseges" runat="server" Checked="true" Text="közönséges" />
                                <asp:CheckBox ID="cbNyilvantartott" runat="server" Checked="true" Text="nyilvántartott" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short" colspan="2">
                                <%--                            </td>
                            <td colspan="1">--%>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor_kicsi">
                                        <td class="mrUrlapCaption_short">
                                            <div class="DisableWrap">
                                                <asp:Label ID="labelTetelPerOldalStar" runat="server" Text="*" CssClass="ReqStar" />
                                                <asp:Label ID="labelTetelPerOldal" runat="server" Text="Tétel/oldal(fájl):" />
                                            </div>
                                        </td>
                                        <td class="mrUrlapMezo" style="white-space: nowrap;">
                                            <uc4:RequiredNumberBox ID="RequiredNumberBoxTetelPerOldal" runat="server" Text="100000"
                                                Validate="true" LabelRequiredIndicatorID="labelMaxTetelPerOldalStar" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <div class="DisableWrap">
                                                <asp:Label ID="labelKezdooldalStar" runat="server" Text="*" CssClass="ReqStar" />
                                                <asp:Label ID="labelKezdooldal" runat="server" Text="Kezdőoldal:" />
                                            </div>
                                        </td>
                                        <td class="mrUrlapMezo" style="white-space: nowrap;">
                                            <uc4:RequiredNumberBox ID="RequiredNumberBoxKezdooldal" runat="server" Text="1" Validate="true"
                                                LabelRequiredIndicatorID="labelKezdooldalStar" />
                                        </td>
                                        <%--                                        <td class="mrUrlapCaption_short">
                                            <div class="DisableWrap">
                                                <asp:Label ID="labelOldalakOsszesenStar" runat="server" Text="*" CssClass="ReqStar" />
                                                <asp:Label ID="labelOldalakOsszesen" runat="server" Text="Oldalak összesen:" />
                                            </div>
                                        </td>--%>
                                        <%--                                        <td class="mrUrlapMezo">
                                            <uc4:RequiredNumberBox ID="RequiredNumberBoxOldalakOsszesen" runat="server" Text=""
                                                Validate="false" LabelRequiredIndicatorID="labelOldalakOsszesenStar" />
                                        </td>--%>
                                        <td class="mrUrlapMezo" width="100%" />
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td colspan="2">
                                <br />
                                <asp:ImageButton ID="imgUgyfelAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/ugyfel_adatok.jpg"
                                    onmouseover="swapByName(this.id,'ugyfel_adatok2.jpg')" onmouseout="swapByName(this.id,'ugyfel_adatok.jpg')"
                                    OnClick="ImageButton_Click" CommandName="UgyfelAdatok" />
                                <asp:ImageButton ID="imgStatisztika" runat="server" ImageUrl="~/images/hu/ovalgomb/statisztika.jpg"
                                    onmouseover="swapByName(this.id,'statisztika2.jpg')" onmouseout="swapByName(this.id,'statisztika.jpg')"
                                    OnClick="ImageButton_Click" CommandName="Statistics" />
                                <asp:ImageButton ID="imgKereses" runat="server" ImageUrl="~/images/hu/ovalgomb/kereses.jpg"
                                    onmouseover="swapByName(this.id,'kereses2.jpg')" onmouseout="swapByName(this.id,'kereses.jpg')"
                                    OnClick="ImageButton_Click" CommandName="Search" />
                                <%--                                <asp:ImageButton ID="imgExport" runat="server" ImageUrl="~/images/hu/ovalgomb/export.jpg"
                                    onmouseover="swapByName(this.id,'export2.jpg')" onmouseout="swapByName(this.id,'export.jpg')"
                                    OnClick="ImageButton_Click" CommandName="Export" />--%>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanelUgyfelAdatok" runat="server" Visible="false">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td>
                                <uap:EFeladoJegyzekUgyfelAdatokPanel ID="UgyfelAdatokPanel" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td>
                                <br />
                                <asp:ImageButton ID="imgUgyfelAdatokSave" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                    onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                    OnClick="ImageButton_Click" CommandName="UgyfelAdatokSave" />
                                <asp:ImageButton ID="imgUgyfelAdatokCancel" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                    onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                    OnClick="ImageButton_Click" CommandName="UgyfelAdatokCancel" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <asp:UpdatePanel ID="StatisztikaUpdatePanel" runat="server" UpdateMode="Conditional"
                    OnLoad="StatisztikaUpdatePanel_Load">
                    <Triggers>
                       <asp:AsyncPostBackTrigger ControlID="KimenoKuldemenyekGridView_Kozonseges" EventName="Load" />
                    </Triggers>
                    <ContentTemplate>
                        <eUI:eFormPanel ID="EFormPanelStatisztika" runat="server" Visible="false">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor_kicsi">
                                    <td style="text-align: center;">
                                        <stat:EFeladoJegyzekStatisztikaPanel ID="StatisztikaPanel" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor_kicsi">
                                    <td>
                                        <br />
                                        <asp:ImageButton ID="imgStatisztikaClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                            onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                            OnClick="ImageButton_Click" CommandName="StatisztikaClose" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="KimenoKuldemenyekUpdatePanel" runat="server" UpdateMode="Conditional"
                    OnLoad="KimenoKuldemenyekUpdatePanel_Load">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="EFormPanelLista" runat="server" Visible="false">
                            <uc2:MultiGridViewPager ID="MultiGridViewPager_KimenoKuldemenyek" runat="server"
                                Title="Kimenő küldemények" TitleCssClass="GridViewCaptionStyle" />
                            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
                            <ajaxToolkit:CollapsiblePanelExtender ID="KimenoKuldemenyekCPE" runat="server" TargetControlID="KuldemenyekListPanel"
                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ExpandedSize="0">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="KuldemenyekListPanel" runat="server" Visible="true">
                                <table cellpadding="0" cellspacing="0" width="90%">
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                          <%--  <asp:Panel ID="KuldemenyekListPanel_Kozonseges" runat="server" Visible="false">--%>
                                                <asp:GridView ID="KimenoKuldemenyekGridView_Kozonseges" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Közönséges küldemények</div>" CellSpacing="0"
                                                    BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <%--                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                    <%--                                                                <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                    onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                    AlternateText="Javítás..." Visible="false" />--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--BLG_1938--%>
                                                 <%--       <asp:BoundField DataField="viszonylat" HeaderText="Viszonylat" SortExpression="viszonylat">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="darab" HeaderText="Darab" SortExpression="darab">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                         <%--BLG_1938--%>
                                                         <asp:BoundField DataField="vakok_irasa" HeaderText="Vakok írása" SortExpression="vakok_irasa">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                         <%--BLG_1938--%>
                                                        <%--  <asp:BoundField DataField="gepre_alkalmassag" HeaderText="Gépre alkalmasság" SortExpression="gepre_alkalmassag">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                <%--BLG_1938--%>
                                                <asp:GridView ID="KimenoKuldemenyekGridView_NemzKozonseges" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Nemzetközi közönséges küldemények</div>" CellSpacing="0"
                                                    BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <%--                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                    <%--                                                                <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                    onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                    AlternateText="Javítás..." Visible="false" />--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                     <%-- BLG_1938--%>
                                                     <%--  <asp:BoundField DataField="viszonylat" HeaderText="Viszonylat" SortExpression="viszonylat">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="darab" HeaderText="Darab" SortExpression="darab">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                         <%--BLG_1938--%>
                                                         <asp:BoundField DataField="vakok_irasa" HeaderText="Vakok írása" SortExpression="vakok_irasa">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                         <%--BLG_1938--%>
                                                        <asp:BoundField DataField="gepre_alkalmassag" HeaderText="Gépre alkalmasság" SortExpression="gepre_alkalmassag">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView> 
                                                 <asp:GridView ID="KimenoKuldemenyekGridView_KozonsegesAzon" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Közönséges azonosított küldemények</div>"
                                                    CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <%--                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                    <%--                                                                <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                    onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                    AlternateText="Javítás..." Visible="false" />--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="azonosito" HeaderText="Azonosító" SortExpression="azonosito">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_nev" HeaderText="Címzett neve" SortExpression="cimzett_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                       <%-- <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="cimzett_irsz" HeaderText="IRSZ" SortExpression="cimzett_irsz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_hely" HeaderText="Címzett helység" SortExpression="cimzett_hely">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                             
                                                        <%--BUG_963--%>
                                                       <asp:BoundField DataField="cimzett_kozterulet_nev" HeaderText="Címzett közterület" SortExpression="cimzett_kozterulet_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_kozterulet_jelleg" HeaderText="Címzett közterület típus" SortExpression="cimzett_kozterulet_jelleg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_hazszam" HeaderText="Címzett házszám" SortExpression="cimzett_hazszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_epulet" HeaderText="Címzett épület" SortExpression="cimzett_epulet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_lepcsohaz" HeaderText="Címzett lépcsőház" SortExpression="cimzett_lepcsohaz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_emelet" HeaderText="Címzett emelet" SortExpression="cimzett_emelet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_ajto" HeaderText="Címzett ajtó" SortExpression="cimzett_ajto">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_postafiok" HeaderText="Címzett postafiók" SortExpression="cimzett_postafiok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--BUG_963
                                                        HRSZ esetén a közelebbi címet (is) töltjük--%>
                                                         <asp:BoundField DataField="cimzett_kozelebbi_cim" HeaderText="Közelebbi cím (HRSZ esetén)" SortExpression="cimzett_kozelebbi_cim">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                                      <asp:GridView ID="KimenoKuldemenyekGridView_Nyilvantartott" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Nyilvántartott küldemények</div>"
                                                    CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <%--                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                    <%--                                                                <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                    onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                    AlternateText="Javítás..." Visible="false" />--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="azonosito" HeaderText="Azonosító" SortExpression="azonosito">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--                                                    <asp:BoundField DataField="vakok_irasa" HeaderText="Vakok írása jelzés" SortExpression="vakok_irasa">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>--%>
                                                        <%--                                            <asp:BoundField DataField="uv_osszeg" HeaderText="Utánvétel összege" SortExpression="uv_osszeg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="uv_lapid" HeaderText="Utánvétel biz. azon." SortExpression="uv_lapid">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="eny_osszeg" HeaderText="Értéknyilv. összege" SortExpression="eny_osszeg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                                        <%--                                                    <asp:BoundField DataField="kezelesi_mod" HeaderText="Kezelési mód" SortExpression="kezelesi_mod">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>--%>
                                                        <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_nev" HeaderText="Címzett neve" SortExpression="cimzett_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                       <%-- <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="cimzett_irsz" HeaderText="IRSZ" SortExpression="cimzett_irsz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_hely" HeaderText="Címzett helység" SortExpression="cimzett_hely">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--                                            <asp:BoundField DataField="cimzett_ertcsat" HeaderText="Címzett e-értesítés fajta"
                                                SortExpression="cimzett_ertcsat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cimzett_ertcim" HeaderText="Címzett e-értesítés cím" SortExpression="cimzett_ertcim">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="felado_ertcsat" HeaderText="Feladó e-értesítés fajta"
                                                SortExpression="felado_ertcsat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="felado_ertcim" HeaderText="Feladó e-értesítés cím" SortExpression="felado_ertcim">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                                      
                                                        <%--                                                    <asp:BoundField DataField="potlapszam" HeaderText="Pótlapszám" SortExpression="potlapszam">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    </asp:BoundField>--%>
                                                        <%--BUG_963--%>
                                                       <asp:BoundField DataField="cimzett_kozterulet_nev" HeaderText="Címzett közterület" SortExpression="cimzett_kozterulet_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_kozterulet_jelleg" HeaderText="Címzett közterület típus" SortExpression="cimzett_kozterulet_jelleg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_hazszam" HeaderText="Címzett házszám" SortExpression="cimzett_hazszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_epulet" HeaderText="Címzett épület" SortExpression="cimzett_epulet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_lepcsohaz" HeaderText="Címzett lépcsőház" SortExpression="cimzett_lepcsohaz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_emelet" HeaderText="Címzett emelet" SortExpression="cimzett_emelet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_ajto" HeaderText="Címzett ajtó" SortExpression="cimzett_ajto">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_postafiok" HeaderText="Címzett postafiók" SortExpression="cimzett_postafiok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--BUG_963
                                                        HRSZ esetén a közelebbi címet (is) töltjük--%>
                                                         <asp:BoundField DataField="cimzett_kozelebbi_cim" HeaderText="Közelebbi cím (HRSZ esetén)" SortExpression="cimzett_kozelebbi_cim">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
<%--                                                        <asp:BoundField DataField="megjegyzes" HeaderText="Megjegyzés" SortExpression="megjegyzes">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <%--                                            <asp:BoundField DataField="feldolg" HeaderText="Feldolgozottság mértéke" SortExpression="feldolg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="sajat_azonosito" HeaderText="Saját azonosító" SortExpression="sajat_azonosito">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                        
                                                 <asp:GridView ID="KimenoKuldemenyekGridView_NemzKozonsegesAzon" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Nemzetközi közönséges azonosított küldemények</div>"
                                                    CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <%--                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                    <%--                                                                <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                    onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                    AlternateText="Javítás..." Visible="false" />--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="azonosito" HeaderText="Azonosító" SortExpression="azonosito">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_nev" HeaderText="Címzett neve" SortExpression="cimzett_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_irsz" HeaderText="IRSZ" SortExpression="cimzett_irsz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_hely" HeaderText="Címzett helység" SortExpression="cimzett_hely">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                             
                                                        <%--BUG_963--%>
                                                   <%--    <asp:BoundField DataField="cimzett_kozterulet_nev" HeaderText="Címzett közterület" SortExpression="cimzett_kozterulet_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_kozterulet_jelleg" HeaderText="Címzett közterület típus" SortExpression="cimzett_kozterulet_jelleg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_hazszam" HeaderText="Címzett házszám" SortExpression="cimzett_hazszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_epulet" HeaderText="Címzett épület" SortExpression="cimzett_epulet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_lepcsohaz" HeaderText="Címzett lépcsőház" SortExpression="cimzett_lepcsohaz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_emelet" HeaderText="Címzett emelet" SortExpression="cimzett_emelet">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_ajto" HeaderText="Címzett ajtó" SortExpression="cimzett_ajto">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="cimzett_postafiok" HeaderText="Címzett postafiók" SortExpression="cimzett_postafiok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <%--BUG_963
                                                        HRSZ esetén a közelebbi címet (is) töltjük--%>
                                                         <asp:BoundField DataField="cimzett_kozelebbi_cim" HeaderText="Közelebbi cím" SortExpression="cimzett_kozelebbi_cim">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>

                                          <%--  </asp:Panel>--%>
                               <%--         </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">--%>
                                       <%--     <asp:Panel ID="KuldemenyekListPanel_Nyilvantartott" runat="server" Visible="false">--%>
                                              <%--BLG_1938--%>
                                                <asp:GridView ID="KimenoKuldemenyekGridView_NemzNyilvantartott" runat="server" CellPadding="0"
                                                    Caption="<div class='GridViewCaptionStyle'>Nemzetközi nyilvántartott küldemények</div>"
                                                    CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="true" PagerSettings-Visible="false"
                                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Ids" OnRowDataBound="KimenoKuldemenyekGridView_RowDataBound"
                                                    OnPreRender="KuldKuldemenyekGridView_PreRender" OnRowCommand="KimenoKuldemenyekGridViewGridView_RowCommand"
                                                    OnSelectedIndexChanging="KimenoKuldemenyekGridViewGridView_SelectedIndexChanging">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("sorszam") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                            SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="sorszam" HeaderText="Sorsz." SortExpression="sorszam">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("ids") %>' OnClick="ImageButton_Click" />
                                                                 </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="azonosito" HeaderText="Azonosító" SortExpression="azonosito">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="alapszolg" HeaderText="Alapszolg." SortExpression="alapszolg">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:BoundField DataField="suly" HeaderText="Súly" SortExpression="suly">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="kulonszolgok" HeaderText="Különszolg." SortExpression="kulonszolgok">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                       <asp:BoundField DataField="meret" HeaderText="Méret" SortExpression="meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                          <asp:BoundField DataField="dij" HeaderText="Díj" SortExpression="dij">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_nev" HeaderText="Címzett neve" SortExpression="cimzett_nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="orszagkod" HeaderText="Orsz. kód" SortExpression="orszagkod">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_irsz" HeaderText="IRSZ" SortExpression="cimzett_irsz">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="cimzett_hely" HeaderText="Címzett helység" SortExpression="cimzett_hely">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                      
                                                         <asp:BoundField DataField="cimzett_kozelebbi_cim" HeaderText="Közelebbi cím" SortExpression="cimzett_kozelebbi_cim">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ids" HeaderText="Ids" SortExpression="ids">
                                                            <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                         <%--   </asp:Panel>--%>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor_kicsi">
                                        <td>
                                            <br />
                                            <asp:ImageButton ID="imgListaClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                                OnClick="ImageButton_Click" CommandName="ListaClose" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </eUI:eFormPanel>
                        <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" CssClass="mrResultPanel">
                            <div class="mrResultPanelText">
                                Az e-feladójegyzék létrehozása sikeres volt.</div>
                            <table cellspacing="0" cellpadding="0" width="700px" class="mrResultTable">
                                <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                    <td class="mrUrlapCaptionSubTitle_Bal" colspan="3">
                                        <asp:Label ID="lbMentes" runat="server" Text="A létrehozott XML fájlok mentése:" />
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="lbResultUgyfelAdatok" runat="server" Text="Ügyfél adatok:" />
                                    </td>
                                    <td class="mrUrlapMezoBigSize" style="padding-left: 10px;">
                                        <asp:Label ID="lbResultFajlNevUgyfelAdatok" runat="server" Text="" />
                                    </td>
                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                        <asp:HyperLink ID="linkResultDownLoadUgyfelAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/export.jpg"
                                            NavigateUrl="PostazasEFeladoJegyzekDownLoadForm.aspx?FileName=UGYFEL.xml&Mode=File"
                                            Target="_blank" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="lbResultAdatok" runat="server" Text="E-feladójegyzék adatok:" />
                                    </td>
                                    <td class="mrUrlapMezoBigSize" style="padding-left: 10px;">
                                        <asp:Label ID="lbResultFajlNevAdatok" runat="server" Text="" />
                                    </td>
                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                        <asp:HyperLink ID="linkResultDownLoadAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/export.jpg"
                                            NavigateUrl="PostazasEFeladoJegyzekDownLoadForm.aspx?FileName=ADAT.xml&Mode=Result"
                                            Target="_blank" />
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
