﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// TODO:
/// XML-ek mentése adatbázisba v. fájlrendszerbe
/// XSD betöltés adatbázisból v. fájlrendszerből
/// XSD kiegészítése
/// További szabályellenőrző szkriptek
/// Validálás C# oldalon
/// Statisztika bővítése (hibás formátumú mezők)
/// XmlHelper DLL-be
/// XML formázás (max. egy behúzás, sorvége, fájlvége...)
/// Lapozás (MultiGridViewPager) SelectedId kezelés, Scroll kezelés
/// </summary>


public partial class PostazasEFeladoJegyzek : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();

    private const string normalReadonlyRowStyle = "GridViewReadOnlyRowStyle";
    private const string selectedReadOnlyRowStyle = "GridViewReadOnlySelectedRowStyle";
    private const string normalRowStyle = "GridViewRowStyle";
    private const string selectedRowStyle = "GridViewSelectedRowStyle";

    private const string xsd_verzio = "8.5";

    private Dictionary<string, List<string>> mandatoryFields = null;
    public Dictionary<string, List<string>> MandatoryFields
    {
        get
        {
            if (this.mandatoryFields == null)
            {
                this.mandatoryFields = Contentum.eUtility.XmlHelper.GetMandatoryFieldsFromXmlSchema(XmlSchemaEFeladojegyzek);
            }
            return this.mandatoryFields;
        }
    }

    private Dictionary<string, Contentum.eUtility.XmlHelper.SimpleTypeRestriction> simpleTypeRestrictions = null;
    public Dictionary<string, Contentum.eUtility.XmlHelper.SimpleTypeRestriction> SimpleTypeRestrictions
    {
        get
        {
            if (this.simpleTypeRestrictions == null)
            {
                this.simpleTypeRestrictions = Contentum.eUtility.XmlHelper.GetSimpleTypeRestrictionsFromXmlSchema(this.XmlSchemaEFeladojegyzek);
            }
            return this.simpleTypeRestrictions;
        }
    }

    private System.Xml.Schema.XmlSchema xmlSchemaEFeladojegyzek = null;
    public System.Xml.Schema.XmlSchema XmlSchemaEFeladojegyzek
    {
        get
        {
            if (this.xmlSchemaEFeladojegyzek == null)
            {
                //System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
                //this.xmlSchemaEFeladojegyzek = GetXsdSchemaFromFile(PathToAdatokXsd, validationEventHandler);
                this.xmlSchemaEFeladojegyzek = Contentum.eUtility.XmlHelper.GetXsdSchemaFromFile(Contentum.eUtility.EFeladoJegyzek.Constants.PathToAdatokXsd);
            }
            return this.xmlSchemaEFeladojegyzek;
        }
    }

    // XSD/XML mezők leképezése mezőnevekre
    private Dictionary<string, string> dictMezoMegnevezes = null;
    public Dictionary<string, string> DictMezoMegnevezes
    {
        get
        {
            if (dictMezoMegnevezes == null)
            {
                dictMezoMegnevezes = new Dictionary<string, string>();
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek, "közönséges küldemény");
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetelek, "nyilvántartott küldemény");

                dictMezoMegnevezes.Add("sorszam", "sorszám");
				dictMezoMegnevezes.Add("alapszolg", "alapszolgáltatás");
                //dictMezoMegnevezes.Add("viszonylat", "viszonylat"); // nem kell feloldani
                dictMezoMegnevezes.Add("orszagkod", "országkód");
				dictMezoMegnevezes.Add("suly","súly/súlykategória");
				dictMezoMegnevezes.Add("kulonszolgok","különszolgáltatások");
				dictMezoMegnevezes.Add("vakok_irasa","vakok írása jelzés");
				dictMezoMegnevezes.Add("meret", "méret");
				dictMezoMegnevezes.Add("dij", "feladási díj");
                dictMezoMegnevezes.Add("gepre_alkalmassag", "feldolgozottság");
                //dictMezoMegnevezes.Add("szall_mod", "szállítás módja"); //? nincs specifikálva

                dictMezoMegnevezes.Add("azonosito", "küldemény azonosító");
				dictMezoMegnevezes.Add("uv_osszeg", "utánvétel összege");
				dictMezoMegnevezes.Add("uv_lapid","utánvételi bizonylat azonosító");
				dictMezoMegnevezes.Add("eny_osszeg", "értéknyilvánítás összege");
				dictMezoMegnevezes.Add("kezbesites_ideje" , "kézbesítés ideje"); // ? nincs specifikálva
				dictMezoMegnevezes.Add("kezelesi_mod", "kezelési mód");
				//dictMezoMegnevezes.Add("alapdij", "alapdíj"); // ? nincs specifikálva
				//dictMezoMegnevezes.Add("tobbletdij", "többletdíj"); // ? nincs specifikálva
				//dictMezoMegnevezes.Add("arufizdij", "árufizetési díj"); // ? nincs specifikálva
				dictMezoMegnevezes.Add("cimzett_nev", "címzett neve");
				dictMezoMegnevezes.Add("cimzett_hely", "címzett helység");
				//dictMezoMegnevezes.Add("cimzett_ertcsat", "címzett e-értesítés fajtája");
				//dictMezoMegnevezes.Add("cimzett_ertcim", "címzett e-értesítés címe");
				//dictMezoMegnevezes.Add("felado_ertcsat", "feladó e-értesítés fajtája");
				//dictMezoMegnevezes.Add("felado_ertcim", "feladó e-értesítés címe");
				dictMezoMegnevezes.Add("potlapszam", "pótlapszám");
                //dictMezoMegnevezes.Add("orz_ido", "őrzési idő"); // ? nincs specifikálva
                dictMezoMegnevezes.Add("cimzett_kozelebbi_cim", "közelebbi cím");
                dictMezoMegnevezes.Add("megjegyzes", "megjegyzés");

            }
            return this.dictMezoMegnevezes;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        MultiGridViewPager_KimenoKuldemenyek.RowCount_Changed += new EventHandler(MultiGridViewPager_RowCount_Changed);

        MultiGridViewPager_KimenoKuldemenyek.PagingButtonsClick += new EventHandler(MultiGridViewPager_PagingChanged);

        // már itt hozzá kell adni, hogy lefussanak az események (selectedindexchanged...)
        // BLG_1938
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_Kozonseges);
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_NemzKozonseges);
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_KozonsegesAzon);
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_Nyilvantartott);
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_NemzKozonsegesAzon);
        MultiGridViewPager_KimenoKuldemenyek.AddAttachedGridViewToPager(KimenoKuldemenyekGridView_NemzNyilvantartott);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
        if (!IsPostBack)
        {
            DatumIntervallum_SearchCalendarControl1.DatumKezd = System.DateTime.Today.ToShortDateString();
            DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Today.ToShortDateString();
            FillIktatoKonyvekList();
        }

        if (IsPostBack)
        {
            if (FormHeader1.ErrorPanel.Visible == true)
            {
                FormHeader1.ErrorPanel.Visible = false;
            }
        }

        //ne lehessen többször kattintani szkript
        ImageButton[] imageButtons = new ImageButton[] {imgUgyfelAdatok, imgStatisztika, imgKereses}; //, imgExport
        foreach (ImageButton imgBtn in imageButtons)
        {
            // fontos a sorrend! (return false; miatt)
            imgBtn.OnClientClick += " $get('" + CustomUpdateProgress1.UpdateProgress.ClientID + "').style.display = 'block'; ";
            imgBtn.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, imgBtn);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = "Export";
        FormFooter1.Command = CommandName.Modify;

        //FormFooter1.ImageButton_Save.Visible = false;
        //FormFooter1.ImageButton_Close.Visible = true;
        //FormFooter1.ImageButton_Back.Visible = false;
        //FormFooter1.ImageButton_Cancel.Visible = false;
    }

    private void FillIktatoKonyvekList()
    {
        IraIktatoKonyvekDropDownList_Postakonyv.DropDownList.Items.Clear();

        // (Postakönyv jelenleg nem zárható le)
        IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv, false, FormHeader1.ErrorPanel);

    }

    private EPostazasiParameterek GetSearchObjectFromComponents_EPostazasiParameterek()
    {
        EPostazasiParameterek ePostazasiParameterek = new EPostazasiParameterek();

        ePostazasiParameterek.IgnoreKozonsegesTetelek = !cbKozonseges.Checked;
        ePostazasiParameterek.IgnoreNyilvantartottTetelek = !cbNyilvantartott.Checked;

        ePostazasiParameterek.StartDate = DatumIntervallum_SearchCalendarControl1.DatumKezd;
        ePostazasiParameterek.EndDate = DatumIntervallum_SearchCalendarControl1.DatumVege;

        ePostazasiParameterek.Postakonyv_Id = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
        // Kiküldő!
        ePostazasiParameterek.Kikuldo_Id = KimenoKuldemeny_Kikuldo_CsoportTextBox.Id_HiddenField;

        int tetelszamPerOldal;
        if (Int32.TryParse(RequiredNumberBoxTetelPerOldal.Text, out tetelszamPerOldal))
        {
            ePostazasiParameterek.WindowSize = tetelszamPerOldal;
        }

        int kezdoOldal;
        if (Int32.TryParse(RequiredNumberBoxKezdooldal.Text, out kezdoOldal))
        {
            ePostazasiParameterek.StartPage = kezdoOldal;
        }

        //int oldalSzam;
        //if (Int32.TryParse(RequiredNumberBoxOldalakOsszesen.Text, out oldalSzam))
        //{
        //    ePostazasiParameterek.PageCount = oldalSzam;
        //}
        return ePostazasiParameterek;
    }

    /// <summary>
    /// A keresési form footer funkciógomjainak eseményeinek kezelése
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgbtn = sender as ImageButton;
        if (imgbtn == null) return;

        if (imgbtn.CommandName == CommandName.Search)
        {
            EFormPanelLista.Visible = true;
            Result result = FillKimenoKuldemenyek();
            FillStatistics();
        }
        //else if (imgbtn.CommandName == "Export")
        //{
        //    Result result = ExportToEFeladojegyzek();
        //    if (result.IsError)
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        //    }
        //}
        else if (imgbtn.CommandName == "Statistics")
        {
            EFormPanelStatisztika.Visible = true;
            FillStatistics();
            FillKimenoKuldemenyek();
        }
        else if (imgbtn.CommandName == "UgyfelAdatok")
        {
            EFormPanelUgyfelAdatok.Visible = true;

            UgyfelAdatokPanel.PostakonyvId = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
            Result result = UgyfelAdatokPanel.LoadUgyfelAdatokComponentsFromXml();

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        else if (imgbtn.CommandName == "UgyfelAdatokSave")
        {
            SaveUgyfelAdatok();
        }
        else if (imgbtn.CommandName == "UgyfelAdatokCancel")
        {
            EFormPanelUgyfelAdatok.Visible = false;
        }
        else if (imgbtn.CommandName == "StatisztikaClose")
        {
            EFormPanelStatisztika.Visible = false;
        }
        else if (imgbtn.CommandName == "ListaClose")
        {
            EFormPanelLista.Visible = false;
        }
        else if (imgbtn.CommandName == "BackToFilteredList")
        {
            if (!String.IsNullOrEmpty(imgbtn.CommandArgument))
            {
                EREC_KuldKuldemenyekSearch searchObject = new EREC_KuldKuldemenyekSearch(true);
                searchObject.Id.Value = imgbtn.CommandArgument.Replace(";", ",");
                searchObject.Id.Operator = Query.Operators.equals;

                searchObject.ReadableWhere = "E-Feladójegyzék tétel";

                Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch);
            }
            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        }
        //else if (imgbtn.CommandName == ...)
        //{
        //// TODO
        //}
        else
        {
            // TODO
        }

    }

    protected Result ExportToEFeladojegyzek()
    {
        Result result = new Result();

        EPostazasiParameterek ePostazasiParameterek = GetSearchObjectFromComponents_EPostazasiParameterek();

        result = GetKimenoKuldemenyekForEFeladoJegyzek(ePostazasiParameterek, false);

        if (result.IsError)
        {
            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return result;
        }

        try
        {
            string efj_adatok = GetEfjAdatok();

            // BLG_1938
            //string ugyfel_adatok = GetUgyfelAdatok();

             string tomeges_adatok = GetXmlFromResultOverStringManipulation(result); // vagy: GetXmlFromResultOverDataDocument(result);

            // BLG_1938
            //string xmlEPostaKonyv = String.Format("<jegyzek_adatok>{0}<jegyzek_adat>{1}{2}</jegyzek_adat></jegyzek_adatok>", 
            //efj_adatok, ugyfel_adatok, tomeges_adatok);
            string xmlEPostaKonyv = String.Format("<jegyzek_adatok>{0}{1}</jegyzek_adatok>",
                            efj_adatok, tomeges_adatok);

            System.Xml.XmlDocument xmlDocument = Contentum.eUtility.XmlHelper.GetXmlDocumentFromText(xmlEPostaKonyv);
            Contentum.eUtility.XmlHelper.RemoveEmptyNodes(xmlDocument);

            System.Xml.Schema.XmlSchema xmlSchema = Contentum.eUtility.XmlHelper.GetXsdSchemaFromFile(Contentum.eUtility.EFeladoJegyzek.Constants.PathToAdatokXsd);

            //TODO: nincs séma
            Contentum.eUtility.XmlHelper.ValidateXmlAgainstXmlSchema(xmlDocument, xmlSchema);

            string fileName = Contentum.eUtility.EFeladoJegyzek.Xml.WriteFormattedStringFromXmlDocument(xmlDocument, Contentum.eUtility.EFeladoJegyzek.Constants.XmlType.Adatok);
            result.Record = fileName;
        }
        catch (Exception ex)
        {
            Logger.Error(ex.Message);
            // BUG_4737
            //result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            result = ResultError.CreateNewResultWithErrorCode(80415);
            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_exception);
        }

        return result;
    }

    protected void SaveUgyfelAdatok()
    {
        if (EFormPanelUgyfelAdatok.Visible)
        {
            if (UgyfelAdatokPanel.Megallapodas.Length != 8)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Érvénytelen mező", "A 'Megállapodás száma' mezőnek pontosan 8 karakter hosszúnak kell lennie.");
                return;
            }

            Result result = UgyfelAdatokPanel.UgyfelAdatokSave();
            //string path = UgyfelAdatokPanel.PathToUgyfelAdatokXml;

            if (result.IsError)
            {
                string exmsg = result.ErrorMessage.Replace(Environment.NewLine, @"\n");
                string msg = null;

                if (result.ErrorType == typeof(Contentum.eUtility.XmlHelper.ValidationException).ToString())
                {
                    //msg = String.Format(@"Az ügyfél adatok validálása sikertelen!\nFájl: {0}\nHibaüzenet: {1}", path.Replace(@"\", @"\\"), exmsg);
                    msg = String.Format(@"Az ügyfél adatok validálása sikertelen!\nHibaüzenet: {0}", exmsg);
                }
                else
                {
                    //msg = String.Format(@"Az ügyfél adatok mentése sikertelen!\nFájl: {0}\nHibaüzenet: {1}", path.Replace(@"\", @"\\"), exmsg);
                    msg = String.Format(@"Az ügyfél adatok mentése sikertelen!\nHibaüzenet: {0}", exmsg);
                }
                Logger.Error(msg);

                //LZS- BUG_1301
                if (result.ErrorCode == "[66666]")
                {
                    if (exmsg.Contains("azonosito"))
                    {
                        msg = String.Format(@"{0}: {1}", "Azonosító ", Resources.Error.ErrorCode_66666);
                    }
                }

                string js = String.Format("alert('{0}');", msg);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);

                result.ErrorMessage = msg;
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
            else
            {
                //string js = String.Format(@"alert('Az ügyfél adatok mentése sikeres!\nFájl: {0}');", path.Replace(@"\", @"\\"));
                string js = @"alert('Az ügyfél adatok mentése sikeres!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);

                EFormPanelUgyfelAdatok.Visible = false;
            }
        }
    }

    protected Result FillStatistics()
    {
        if (EFormPanelStatisztika.Visible)
        {
            EPostazasiParameterek ePostazasiParameterek = GetSearchObjectFromComponents_EPostazasiParameterek();

            Result result = GetKimenoKuldemenyekForEFeladoJegyzek(ePostazasiParameterek, false);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                EFormPanelStatisztika.Visible = false;
            }
            else
            {
                EFormPanelStatisztika.Visible = true;
                StatisztikaPanel.FillFromResult(result, ePostazasiParameterek);
            }
            return result;
        }
        return null;
    }

    protected Result FillKimenoKuldemenyek()
    {
        if (EFormPanelLista.Visible)
        {
            MultiGridViewPager_KimenoKuldemenyek.RowCount = RequiredNumberBoxTetelPerOldal.Text;
            return KimenoKuldemenyekGridViewBind();
        }
        return null;
    }

    protected Result GetKimenoKuldemenyekForEFeladoJegyzek(EPostazasiParameterek ePostazasiParameterek, bool usePaging)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        if (usePaging)
        {
            // Lapozás beállítása:
            UI.SetPaging(execParam, MultiGridViewPager_KimenoKuldemenyek);
        }

        Result result = service.KimenoKuldGetAllWithExtensionAndJogosultakForEFeladoJegyzek(execParam
            , ePostazasiParameterek, true);

        return result;

    }

    protected void KimenoKuldemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FillKimenoKuldemenyek();
                    break;
            }
        }
    }

    protected void StatisztikaUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FillStatistics();
                    break;
            }

        }
    }

    protected Result KimenoKuldemenyekGridViewBind()
    {
        EPostazasiParameterek ePostazasiParameterek = GetSearchObjectFromComponents_EPostazasiParameterek();
        Result result = GetKimenoKuldemenyekForEFeladoJegyzek(ePostazasiParameterek, true);

        ui.GridViewClear(KimenoKuldemenyekGridView_Kozonseges);
        ui.GridViewClear(KimenoKuldemenyekGridView_Nyilvantartott);
        // BLG_1938
        ui.GridViewClear(KimenoKuldemenyekGridView_KozonsegesAzon);
        ui.GridViewClear(KimenoKuldemenyekGridView_NemzKozonseges);
        ui.GridViewClear(KimenoKuldemenyekGridView_NemzKozonsegesAzon);
        ui.GridViewClear(KimenoKuldemenyekGridView_NemzNyilvantartott);
        //if (ePostazasiParameterek.IgnoreKozonsegesTetelek)
        //{
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_Kozonseges);
        //    // BLG_1938
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_KozonsegesAzon);
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_NemzKozonseges);
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_NemzKozonsegesAzon);

        //}

        //if (ePostazasiParameterek.IgnoreNyilvantartottTetelek)
        //{
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_Nyilvantartott);
        //    //BLG_1938
        //    MultiGridViewPager_KimenoKuldemenyek.RemoveAttachedGridViewFromPager(KimenoKuldemenyekGridView_NemzNyilvantartott);

        //}

        // lapozási adatok (Id... pageNumber, RecordNumber) miatt nem megy, mert több mint 1 eredménytáblánk van
        //ui.GridViewFill(KimenoKuldemenyekGridView, result, "", FormHeader1.ErrorPanel, null);
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            ui.GridViewClear(KimenoKuldemenyekGridView_Kozonseges);
            ui.GridViewClear(KimenoKuldemenyekGridView_Nyilvantartott);
            // BLG_1938
            ui.GridViewClear(KimenoKuldemenyekGridView_KozonsegesAzon);
            ui.GridViewClear(KimenoKuldemenyekGridView_NemzKozonseges);
            ui.GridViewClear(KimenoKuldemenyekGridView_NemzKozonsegesAzon);
            ui.GridViewClear(KimenoKuldemenyekGridView_NemzNyilvantartott);

            // BLG_1938
            //KuldemenyekListPanel_Kozonseges.Visible = false;
            //KuldemenyekListPanel_Nyilvantartott.Visible = false;
            //return null;
        }
        else
        {
            #region Inline lapozás - komment
            /*
            int recordNumber = 0; // az összes visszakapott rekord száma: lapozáshoz
            DataTable dtKozonseges = null;
            DataTable dtNyilvantartott = null;
            // TODO: Hibakezelés
            int rowCount = Int32.Parse(MultiGridViewPager_KimenoKuldemenyek.RowCount);
            int pageIndex = MultiGridViewPager_KimenoKuldemenyek.PageIndex;

            if (pageIndex < 0)
            {
                pageIndex = 0;
            }

            int startPosition = pageIndex * rowCount;
            int afterEndPosition = startPosition + rowCount;

            if (result.Ds.Tables.Contains("kozonseges_tetel") && result.Ds.Tables["kozonseges_tetel"].Rows.Count > 0)
            {

                if (startPosition == 0 && result.Ds.Tables["kozonseges_tetel"].Rows.Count == rowCount)
                {
                    dtKozonseges = result.Ds.Tables["kozonseges_tetel"].Copy();
                }
                else
                {
                    dtKozonseges = result.Ds.Tables["kozonseges_tetel"].Clone();
                    int maxRow = Math.Min(afterEndPosition, result.Ds.Tables["kozonseges_tetel"].Rows.Count);
                    for (int i = startPosition; i < maxRow; i++)
                    {
                        dtKozonseges.ImportRow(result.Ds.Tables["kozonseges_tetel"].Rows[i]);
                    }
                }

                KimenoKuldemenyekGridView_Kozonseges.DataSource = dtKozonseges;//result.Ds.Tables["kozonseges_tetel"];
                KimenoKuldemenyekGridView_Kozonseges.DataBind();
                KuldemenyekListPanel_Kozonseges.Visible = true;
                EFormPanelLista.Visible = true;

                recordNumber += result.Ds.Tables["kozonseges_tetel"].Rows.Count;
            }
            else
            {
                ui.GridViewClear(KimenoKuldemenyekGridView_Kozonseges);
                KuldemenyekListPanel_Kozonseges.Visible = false;
            }

            if (dtKozonseges != null && afterEndPosition >= dtKozonseges.Rows.Count)
            {
                startPosition = Math.Max(startPosition - dtKozonseges.Rows.Count, 0);
                afterEndPosition -= dtKozonseges.Rows.Count;
            }

            if (result.Ds.Tables.Contains("nyilvantartott_tetel") && result.Ds.Tables["nyilvantartott_tetel"].Rows.Count > 0)
            {
                if (startPosition == 0 && result.Ds.Tables["nyilvantartott_tetel"].Rows.Count == afterEndPosition)
                {
                    dtNyilvantartott = result.Ds.Tables["nyilvantartott_tetel"].Copy();
                }
                else
                {
                    dtNyilvantartott = result.Ds.Tables["nyilvantartott_tetel"].Clone();
                    int maxRow = Math.Min(afterEndPosition, result.Ds.Tables["nyilvantartott_tetel"].Rows.Count);
                    for (int i = startPosition; i < maxRow; i++)
                    {
                        dtNyilvantartott.ImportRow(result.Ds.Tables["nyilvantartott_tetel"].Rows[i]);
                    }
                }

                KimenoKuldemenyekGridView_Nyilvantartott.DataSource = dtNyilvantartott;//result.Ds.Tables["nyilvantartott_tetel"];
                KimenoKuldemenyekGridView_Nyilvantartott.DataBind();
                KuldemenyekListPanel_Nyilvantartott.Visible = true;
                EFormPanelLista.Visible = true;

                recordNumber += result.Ds.Tables["nyilvantartott_tetel"].Rows.Count;
            }
            else
            {
                ui.GridViewClear(KimenoKuldemenyekGridView_Nyilvantartott);
                KuldemenyekListPanel_Nyilvantartott.Visible = false;
            }
            //DataSet ds = result.Ds;

            //return ds;
            MultiGridViewPager_KimenoKuldemenyek.RecordNumber = recordNumber;
             * */

            #endregion Inline lapozás - komment

            // BLG_1938
            //KuldemenyekListPanel_Kozonseges.Visible = true;
            //KuldemenyekListPanel_Nyilvantartott.Visible = true;
            //GridViewFill(MultiGridViewPager_KimenoKuldemenyek.AttachedGridViews, result, MultiGridViewPager_KimenoKuldemenyek, FormHeader1.ErrorPanel, null);
            MultiGridViewPager_KimenoKuldemenyek.FillAttachedGridViews(result, FormHeader1.ErrorPanel, null, false);
        }

        return result;
    }

    //public string GetXmlFromResultOverDataDocument(Result result)
    //{
    //    if (result == null) return null;

    //    string xmlEPostakonyv = null;

    //    if (result.IsError == false)
    //    {
    //        try
    //        {
    //            System.Xml.Schema.XmlSchema schema = this.XmlSchemaEFeladojegyzek;

    //            //System.Xml.Schema.XmlSchemaCollection schemaCollection = new System.Xml.Schema.XmlSchemaCollection();
    //            //schemaCollection.Add("", xmlReader);

    //            System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
    //            xmlDocument.Schemas.Add(schema);

    //            // 1. szint DocumentElement (root)
    //            System.Xml.XmlElement xmlElement_tomeges_adatok = xmlDocument.CreateElement(Contentum.eUtility.EFeladoJegyzek.Constants.tomeges_adatok);
    //            xmlDocument.AppendChild(xmlElement_tomeges_adatok);

    //            System.Xml.XmlElement xmlElement_kozonseges_tetelek = xmlDocument.CreateElement(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek);
    //            System.Xml.XmlElement xmlElement_nyilvantartott_tetelek = xmlDocument.CreateElement(Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetelek);

    //            // 2. szint
    //            xmlElement_tomeges_adatok.AppendChild(xmlElement_kozonseges_tetelek);
    //            xmlElement_tomeges_adatok.AppendChild(xmlElement_nyilvantartott_tetelek);

    //            // 3. szint + 4. szint
    //            System.Xml.XmlDataDocument xmlDataDocument = new System.Xml.XmlDataDocument(result.Ds);

    //            System.Xml.XmlNodeList xmlNodeList_kozonseges_tetelek = xmlDataDocument.SelectNodes("NewDataSet/kozonseges_tetel/.");
    //            System.Xml.XmlNodeList xmlNodeList_nyilvantartott_tetelek = xmlDataDocument.SelectNodes("NewDataSet/nyilvantartott_tetel/.");

    //            foreach (System.Xml.XmlNode nd in xmlNodeList_kozonseges_tetelek)
    //            {
    //                xmlElement_kozonseges_tetelek.AppendChild(xmlDocument.ImportNode(nd, true));
    //            }
    //            foreach (System.Xml.XmlNode nd in xmlNodeList_nyilvantartott_tetelek)
    //            {
    //                xmlElement_kozonseges_tetelek.AppendChild(xmlDocument.ImportNode(nd, true));
    //            }

    //            xmlEPostakonyv = xmlDocument.OuterXml;
    //        }
    //        catch (Exception ex)
    //        {
    //            // TODO: hibakezelés
    //            Logger.Error("Hiba az Xml DataDocumentből való előállításakor: " + ex.Message);
    //        }

    //    }

    //    return xmlEPostakonyv;
    //}

    public string GetXmlFromResultOverStringManipulation(Result result)
    {
        if (result == null) return null;

        string xmlEPostakonyv = null;

        if (result.IsError == false)
        {
            string xml_kozonseges = null;
            string xml_nyilvantartott = null;
            // BLG_1938
            string xml_kozonseges_azon = null;
            string xml_nemz_kozonseges = null;
            string xml_nemz_kozonseges_azon = null;
            string xml_nemz_nyilvantartott = null;

            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel))
                {
                    DataTable tbKozonseges = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel].Copy();
                    if (tbKozonseges.Columns.Contains("ids"))
                    {
                        tbKozonseges.Columns.Remove("ids");
                    }
                    tbKozonseges.WriteXml(sw);
                    xml_kozonseges = sw.ToString();
                    sw.Flush();
                }
            }

            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel))
                {
                    DataTable tbNyilvantartott = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel].Copy();
                    if (tbNyilvantartott.Columns.Contains("ids"))
                    {
                        tbNyilvantartott.Columns.Remove("ids");
                    }
                    tbNyilvantartott.WriteXml(sw);
                    xml_nyilvantartott = sw.ToString();
                    sw.Flush();
                }
            }

            // BLG_1938
            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel))
                {
                    DataTable tbKozonsegesAzon = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel].Copy();
                    if (tbKozonsegesAzon.Columns.Contains("ids"))
                    {
                        tbKozonsegesAzon.Columns.Remove("ids");
                    }
                    tbKozonsegesAzon.WriteXml(sw);
                    xml_kozonseges_azon = sw.ToString();
                    sw.Flush();
                }
            }

            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel))
                {
                    DataTable tbNemzKozonseges = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel].Copy();
                    if (tbNemzKozonseges.Columns.Contains("ids"))
                    {
                        tbNemzKozonseges.Columns.Remove("ids");
                    }
                    tbNemzKozonseges.WriteXml(sw);
                    xml_nemz_kozonseges = sw.ToString();
                    sw.Flush();
                }
            }

            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel))
                {
                    DataTable tbNemzKozonsegesAzon = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel].Copy();
                    if (tbNemzKozonsegesAzon.Columns.Contains("ids"))
                    {
                        tbNemzKozonsegesAzon.Columns.Remove("ids");
                    }
                    tbNemzKozonsegesAzon.WriteXml(sw);
                    xml_nemz_kozonseges_azon = sw.ToString();
                    sw.Flush();
                }
            }

            using (StringWriter sw = new StringWriter())
            {
                if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel))
                {
                    DataTable tbNemzNyilvantartott = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel].Copy();
                    if (tbNemzNyilvantartott.Columns.Contains("ids"))
                    {
                        tbNemzNyilvantartott.Columns.Remove("ids");
                    }
                    tbNemzNyilvantartott.WriteXml(sw);
                    xml_nemz_nyilvantartott = sw.ToString();
                    sw.Flush();
                }
            }

            // BLG_1938
            string ugyfel_adatok = GetUgyfelAdatok();

            if (!String.IsNullOrEmpty(xml_kozonseges))
            {
                xml_kozonseges = String.Concat(xml_kozonseges.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);
            }
            if (!String.IsNullOrEmpty(xml_nyilvantartott))
            {
                xml_nyilvantartott = String.Concat(xml_nyilvantartott.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetelek), "\n"); //String.Format("<nyilvantartott_tetelek>\n{0}\n</nyilvantartott_tetelek>", xml_nyilvantartott);
            }

            // BLG_1938
            if (!String.IsNullOrEmpty(xml_kozonseges_azon))
            {
                //xml_kozonseges_azon = String.Concat(xml_kozonseges_azon.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);
                // Közönséges azonositott tételeknek nincs külön tagjük (Kozonseges_tetelek-nek kell lenni)
                xml_kozonseges_azon = String.Concat(xml_kozonseges_azon.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);

            }

            if (!String.IsNullOrEmpty(xml_nemz_kozonseges))
            {
                xml_nemz_kozonseges = String.Concat(xml_nemz_kozonseges.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);
            }

            if (!String.IsNullOrEmpty(xml_nemz_kozonseges_azon))
            {
                //xml_nemz_kozonseges_azon = String.Concat(xml_nemz_kozonseges_azon.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);
                // Közönséges azonositott tételeknek nincs külön tagjük (Kozonseges_tetelek-nek kell lenni)
                xml_nemz_kozonseges_azon = String.Concat(xml_nemz_kozonseges_azon.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetelek), "\n"); //String.Format("<kozonseges_tetelek>\n{0}\n</kozonseges_tetelek>", xml_kozonseges);

            }

            if (!String.IsNullOrEmpty(xml_nemz_nyilvantartott))
            {
                xml_nemz_nyilvantartott = String.Concat(xml_nemz_nyilvantartott.Replace("DocumentElement", Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetelek), "\n"); //String.Format("<nyilvantartott_tetelek>\n{0}\n</nyilvantartott_tetelek>", xml_nyilvantartott);
            }

            // BLG_1938
            #region Jegyzék_1 : Közönséges adatok
            string xmljegyzek1 = String.Format("<belf_tetelek>\n{0}\n</belf_tetelek>\n<nemz_tetelek>\n{1}\n</nemz_tetelek>",
                                    xml_kozonseges, xml_nemz_kozonseges);
            #endregion Jegyzék_1 : Közönséges adatok

            #region Jegyzék_2 : Közönséges_azonosított és nyilvántartott adatok
            string xmljegyzek2 = String.Format("<belf_tetelek>\n{0}\n{1}\n</belf_tetelek>\n<nemz_tetelek>{2}\n{3}\n</nemz_tetelek>",
                            xml_kozonseges_azon, xml_nyilvantartott, xml_nemz_kozonseges_azon, xml_nemz_nyilvantartott);

            //xmlEPostakonyv = String.Format("<{0}>\n{1}{2}</{0}>", Contentum.eUtility.EFeladoJegyzek.Constants.tomeges_adatok, xml_kozonseges, xml_nyilvantartott);
            xmlEPostakonyv = String.Format("<jegyzek_adat>\n{0}\n<tomeges_adatok>\n{1}\n</tomeges_adatok>\n</jegyzek_adat>\n<jegyzek_adat>\n{0}\n<tomeges_adatok>\n{2}\n</tomeges_adatok>\n</jegyzek_adat>", 
                    ugyfel_adatok, xmljegyzek1, xmljegyzek2);
            #endregion Jegyzék_2 

        }
        return xmlEPostakonyv;
    }

    private string GetTextFromFile(string pathToFile)
    {
        string txt = null;
        try
        {
            txt = File.ReadAllText(pathToFile);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a fájl ({0}) betöltésénél: {1}", pathToFile, ex.Message));
        }
        return txt;
    }

    //GridView RowCommand eseménykezelője, amit valamelyik sorban fellépett parancs vált ki
    protected void KimenoKuldemenyekGridViewGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiválasztása (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            GridView gv = (GridView)sender;
            //TODO
            //if (gv == KimenoKuldemenyekGridView_Kozonseges)
            //{
            //    SetRowStyleOnSelectedIndexChanging(KimenoKuldemenyekGridView_Nyilvantartott, KimenoKuldemenyekGridView_Nyilvantartott.SelectedIndex, -1);
            //    KimenoKuldemenyekGridView_Nyilvantartott.SelectedIndex = -1;
            //}
            //else
            //{
            //    SetRowStyleOnSelectedIndexChanging(KimenoKuldemenyekGridView_Kozonseges, KimenoKuldemenyekGridView_Kozonseges.SelectedIndex, -1);
            //    KimenoKuldemenyekGridView_Kozonseges.SelectedIndex = -1;
            //}

            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(gv, selectedRowNumber, "check");

            //string id = gv.DataKeys[selectedRowNumber].Value.ToString();

            string strIds = UI.GetGridViewColumnText(gv.Rows[selectedRowNumber], "ids");
            string[] arrayIds = strIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            string formName = null;
            string queryString = null;
            if (arrayIds.Length == 1)
            {
                formName = "PostazasForm.aspx";
                queryString = QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.KuldemenyId + "=" + arrayIds[0];
            }
            else
            {
                formName = "PostazasEFeladoJegyzekJavitasForm.aspx";
                queryString = QueryStringVars.KuldemenyId + "=" + strIds;
            }

            string js = "OpenNewWindow(); function OpenNewWindow() { "
                + JavaScripts.SetOnClientClick_DisplayUpdateProgress(formName, queryString
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KimenoKuldemenyekUpdatePanel.ClientID
                        , EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID)
                + "}";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
        }

    }

    protected void SetRowStyleOnSelectedIndexChanging(GridView gv, int oldSelectedIndex, int newSelectedIndex)
    {
        if (gv == null) return;

        if (oldSelectedIndex > -1 && oldSelectedIndex < gv.Rows.Count)
        {
            GridViewRow oldRow = gv.Rows[oldSelectedIndex];

            if (oldRow.ControlStyle.CssClass == selectedReadOnlyRowStyle || oldRow.ControlStyle.CssClass == normalReadonlyRowStyle)
            {
                oldRow.ControlStyle.CssClass = normalReadonlyRowStyle;
            }
            else
            {
                oldRow.ControlStyle.CssClass = normalRowStyle;
            }
        }

        if (newSelectedIndex > -1 && newSelectedIndex < gv.Rows.Count)
        {
            GridViewRow newRow = gv.Rows[newSelectedIndex];
            if (newRow.ControlStyle.CssClass == normalReadonlyRowStyle || newRow.ControlStyle.CssClass == selectedReadOnlyRowStyle)
            {
                newRow.ControlStyle.CssClass = selectedReadOnlyRowStyle;
            }
            else
            {
                newRow.ControlStyle.CssClass = selectedRowStyle;
            }
        }
    }

    //GridView eseménykezelői(3-4)
    ////specialis, nem mindig kell kell
    protected void KimenoKuldemenyekGridViewGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        // sorok színezése
        GridView gv = (GridView)sender;
        SetRowStyleOnSelectedIndexChanging(gv, gv.SelectedIndex, e.NewSelectedIndex);
        
    }

    protected void KimenoKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ErrorDetails errorDetail;
        GridView gv = sender as GridView;
        if (gv == null) return;

        string name = gv.ID;
        string sourcetable = null;
        
        if (name == KimenoKuldemenyekGridView_Kozonseges.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel;
        }
        else if (name == KimenoKuldemenyekGridView_Nyilvantartott.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel;
        }
        else if (name == KimenoKuldemenyekGridView_NemzNyilvantartott.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel;
        }
        else if (name == KimenoKuldemenyekGridView_NemzKozonseges.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel;
        }
        else if (name == KimenoKuldemenyekGridView_KozonsegesAzon.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel;
        }
        else if (name == KimenoKuldemenyekGridView_NemzKozonsegesAzon.ID)
        {
            sourcetable = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel;
        }


        //List<string> mandatoryFieldList = null;
        //if (MandatoryFields.TryGetValue(sourcetable, out mandatoryFieldList))
        //{
        //    // debug
        //    //KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(XmlSchemaEFeladojegyzek, e, Page, sourcetable, mandatoryFieldList, KimenoKuldemenyekUpdatePanel.ClientID, out errorDetail);
        //    //KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(e, Page, sourcetable, MandatoryFields[sourcetable], KimenoKuldemenyekUpdatePanel.ClientID, out errorDetail);
        //}
        KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(SimpleTypeRestrictions, MandatoryFields, DictMezoMegnevezes, e, Page, sourcetable, KimenoKuldemenyekUpdatePanel.ClientID, out errorDetail);
        //KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(XmlSchemaEFeladojegyzek, e, Page, sourcetable, KimenoKuldemenyekUpdatePanel.ClientID, out errorDetail); 

    }


    protected void KuldKuldemenyekGridView_PreRender(object sender, EventArgs e)
    {
        MultiGridViewPager_KimenoKuldemenyek.SetScrollable(KimenoKuldemenyekCPE);
        MultiGridViewPager_KimenoKuldemenyek.RefreshPagerLabel();
    }

    protected void MultiGridViewPager_RowCount_Changed(object sender, EventArgs e)
    {
        KimenoKuldemenyekGridViewBind();
    }

    protected void MultiGridViewPager_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, KimenoKuldemenyekCPE);
        KimenoKuldemenyekGridViewBind();
    }

    public static void ShowCompileErrors(object sender, System.Xml.Schema.ValidationEventArgs args)
    {
        if (args.Severity == System.Xml.Schema.XmlSeverityType.Warning)
        {
            Logger.Warn(args.Message);
        }
        else if (args.Severity == System.Xml.Schema.XmlSeverityType.Error)
        {
            throw new System.Xml.XmlException(args.Message);
        }
    }

    private static bool CheckDataRowViewByRulesAndRestrictions(System.Data.DataRowView drv, string sourcetable, out Dictionary<string, List<string>> errors)
    {
        bool isValid = true;
        errors = null;
        if (!String.IsNullOrEmpty(sourcetable) && drv != null)
        {
            isValid = Contentum.eUtility.EFeladoJegyzek.Validation.Validate(drv.Row, sourcetable, out errors);
        }

        return isValid;
    }

    private static bool CheckDataRowViewByXmlSchema(System.Xml.Schema.XmlSchema xmlSchema, System.Data.DataRowView drv, out string msg)
    {
        string xmlFrag = null;
        msg = null;
        bool isValid = true;
        try
        {
            using (StringWriter sw = new StringWriter())
            {
                DataTable table = drv.Row.Table.Clone();
                table.ImportRow(drv.Row);
                if (table.Columns.Contains("ids"))
                {
                    table.Columns.Remove("ids");
                }
                table.WriteXml(sw);
                xmlFrag = sw.ToString().Replace("<DocumentElement>", "").Replace("</DocumentElement>", "").Replace("<DocumentElement/>", "");
                sw.Flush();
            }

            Contentum.eUtility.XmlHelper.ValidateXmlFragmentAgainstXmlSchema(xmlFrag, xmlSchema);
        }
        catch (Contentum.eUtility.XmlHelper.ValidationException valex)
        {
            System.Diagnostics.Debug.WriteLine(valex.Message);
            msg = valex.Message;
            isValid = false;
        }
        //catch (System.Xml.XmlException XmlExp)
        //{
        //    System.Diagnostics.Debug.WriteLine(XmlExp.Message);
        //    msg = XmlExp.Message;
        //    isValid = false;

        //}
        //catch (System.Xml.Schema.XmlSchemaException XmlSchExp)
        //{
        //    System.Diagnostics.Debug.WriteLine(XmlSchExp.Message);
        //    msg = XmlSchExp.Message;
        //    isValid = false;
        //}

        return isValid;
    }

    public static void SetValidationInfoForRow(GridViewRowEventArgs e, Page page, bool postazhato, string msg, string UpdatePanelClientID, out ErrorDetails errorDetail)
    {
        errorDetail = null;
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            // CheckBox lekérése:
            CheckBox checkBox = (CheckBox)e.Row.FindControl("check");
            if (postazhato == false)
            {
                string strIds = drv["Ids"].ToString();
                string[] arrayIds = strIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                errorDetail = new ErrorDetails(msg, "", "", "", "");

                //e.Row.BackColor = System.Drawing.Color.Coral;
                if (e.Row.RowIndex == ((GridView)e.Row.NamingContainer).SelectedIndex)
                {
                    e.Row.ControlStyle.CssClass = selectedReadOnlyRowStyle;
                }
                else
                {
                    e.Row.ControlStyle.CssClass = normalReadonlyRowStyle;
                }
                if (checkBox != null)
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = false;
                }

                // Info Image állítása, hogy miért nem lehet postázni
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null && errorDetail != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('"
                        + infoMessage.Replace("\n", "\\n") + "','','','',''); return false;";
                }


                //ImageButton link = (ImageButton)e.Row.FindControl("imgKimenoKuldemenyLink");
                //if (link != null)
                //{
                //    string strIds = drv["Ids"].ToString();
                //    string[] arrayIds = strIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                //    link.Visible = true;
                    
                //    if (arrayIds.Length == 1)
                //    {
                //        link.OnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx"
                //            , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.KuldemenyId + "=" + arrayIds[0]
                //            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UpdatePanelClientID
                //            , EventArgumentConst.refreshMasterList);
                //    }
                //    else
                //    {
                //        link.OnClientClick = JavaScripts.SetOnClientClick("PostazasEFeladoJegyzekJavitasForm.aspx",
                //            QueryStringVars.KuldemenyId + "=" + strIds
                //            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UpdatePanelClientID, EventArgumentConst.refreshMasterList);
                //    }


                //}

                return;
            }
            else
            {
                checkBox.Checked = true;
                checkBox.Enabled = true;
            }
        }
    }


    // XmlSchema Validation
    public static bool KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(System.Xml.Schema.XmlSchema xmlSchema, GridViewRowEventArgs e, Page page, string sourcetable, string UpdatePanelClientID, out ErrorDetails errorDetail)
    {
        errorDetail = null;
        bool postazhato = true;
        if (e == null || page == null) return false;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string msg;

            postazhato = CheckDataRowViewByXmlSchema(xmlSchema, drv, out msg);
            SetValidationInfoForRow(e, page, postazhato, msg, UpdatePanelClientID, out errorDetail);
            
        }
        return postazhato;

    }

    #region Field/TableName Format
    private static string GetFormattedFieldName(Dictionary<string, string> dictMezoMegnevezes, string field)
    {
        if (dictMezoMegnevezes != null && !String.IsNullOrEmpty(field) && dictMezoMegnevezes.ContainsKey(field))
        {
            return String.Format("{0} ({1})", dictMezoMegnevezes[field], field);
        }

        return field;
    }

    private static string GetFormattedFieldNameWithFacetValue(Dictionary<string, string> dictMezoMegnevezes, string field, string facetvalue)
    {
        if (String.IsNullOrEmpty(facetvalue))
        {
            return GetFormattedFieldName(dictMezoMegnevezes, field);
        }

        if (dictMezoMegnevezes != null && !String.IsNullOrEmpty(field))
        {
            if (dictMezoMegnevezes.ContainsKey(field))
            {
                return String.Format("{0} ({1}: {2})", dictMezoMegnevezes[field], field, facetvalue);
            }
        }

        if (!String.IsNullOrEmpty(field))
        {
            return String.Format("{0} ({1})", field, facetvalue);
        }

        return String.Format("??? ({0})", facetvalue);
    }

    private static string GetFormattedTableName(Dictionary<string, string> dictMezoMegnevezes, string tablename)
    {
        if (dictMezoMegnevezes != null && !String.IsNullOrEmpty(tablename) && dictMezoMegnevezes.ContainsKey(tablename))
        {
            return dictMezoMegnevezes[tablename];
        }
        return tablename;
    }
    #endregion Field/TableName Format

    // SimpleTypeRestriction Validation
    public static bool KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(Dictionary<string, Contentum.eUtility.XmlHelper.SimpleTypeRestriction> dictSimpleTypeRestrictions, Dictionary<string, List<string>> mantatoryFields, Dictionary<string, string> dictMezoMegnevezes, GridViewRowEventArgs e, Page page, string sourcetable, string UpdatePanelClientID, out ErrorDetails errorDetail)
    {
        errorDetail = null;
        bool postazhato = true;
        if (e == null || page == null) return false;
        //if (mandatoryFieldList == null) return false;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string msg;
            Dictionary<string, List<string>> errors;
            //List<string> mandatoryFieldList = null;
            
            //if (mantatoryFields.ContainsKey(sourcetable))
            //{
            //    mandatoryFieldList = mantatoryFields[sourcetable];
            //}
            postazhato = CheckDataRowViewByRulesAndRestrictions(drv, sourcetable, out errors);

            if (errors != null)
            {
                List<string> errorFields = new List<string>(errors.Keys.Count);
                foreach (string errorCategory in errors.Keys)
                {
                    string errorCategoryHeader = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(errorCategory);
                    
                    List<string> fieldnames_with_facets = errors[errorCategory];
                    List<string> errorFieldNames = new List<string>(errors[errorCategory].Count);
                    foreach (string fieldnames_with_facet in fieldnames_with_facets)
                    {
                        string[] fieldname_items = fieldnames_with_facet.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        string fieldname = fieldname_items[0];
                        string facetValue = (fieldname_items.Length < 2 ? null : fieldname_items[1]);
                        errorFieldNames.Add(GetFormattedFieldNameWithFacetValue(dictMezoMegnevezes, fieldname, facetValue));
                    }

                    errorFields.Add(String.Format("{0}: {1}", errorCategoryHeader, String.Join(";", errorFieldNames.ToArray())));
                }

                string strErrorFields = String.Join("\n", errorFields.ToArray());
                msg = String.Format("Hibás mezők ({0}):\n{1}", GetFormattedTableName(dictMezoMegnevezes, sourcetable), strErrorFields);
                
                SetValidationInfoForRow(e, page, postazhato, msg, UpdatePanelClientID, out errorDetail);
            }

            
        }
        return postazhato;

    }

    //// MandatoryFields Validation
    //public static bool KimenoKuldemenyekGridView_RowDataBound_CheckForEFeladoJegyzek(GridViewRowEventArgs e, Page page, string sourcetable, List<string> mandatoryFieldList, string UpdatePanelClientID, out ErrorDetails errorDetail)
    //{
    //    errorDetail = null;
    //    bool postazhato = true;
    //    if (e == null || page == null) return false;
    //    if (mandatoryFieldList == null) return false;

    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

    //        // Adattartalom teljes-e?

    //        //Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(drv);

    //        //string Id = null;
    //        //if (drv != null && drv.Row.Table.Columns.Contains("Id"))
    //        //{
    //        //    Id = drv["Id"].ToString();
    //        //}

    //        List<string> missingMandatoryFields = new List<string>(mandatoryFieldList.Count);

    //        foreach (string fieldname in mandatoryFieldList)
    //        {
    //            object mandatory = null;
    //            if (drv != null && drv.Row.Table.Columns.Contains(fieldname))
    //            {
    //                mandatory = drv[fieldname];
    //                if (mandatory == DBNull.Value)
    //                {
    //                    //missingMandatoryFields.Add(String.Concat(sourcetable, ".", fieldname));
    //                    missingMandatoryFields.Add(fieldname);
    //                }
    //            }
    //        }

    //        postazhato = (missingMandatoryFields.Count == 0);

    //        string strMandatoryFields = String.Join(",", missingMandatoryFields.ToArray());
    //        string msg = String.Format("Hiányzó kötelező mezők ({0}): {1}", sourcetable, strMandatoryFields);
    //        SetValidationInfoForRow(e, page, postazhato, msg, UpdatePanelClientID, out errorDetail);
    //    }

    //    // TODO: hibakezelés
    //    return postazhato;
    //}


    private bool CheckDates()
    {
        DateTime dtKezd;
        DateTime dtVege;
        if (!DateTime.TryParse(DatumIntervallum_SearchCalendarControl1.DatumKezd, out dtKezd)
            || !DateTime.TryParse(DatumIntervallum_SearchCalendarControl1.DatumVege, out dtVege))
        {
            // hiba:
            //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIFormatErrorHeader, Resources.Error.UIDateTimeFormatError);
            return false;
        }

        return true;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (CheckDates() == false)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIFormatErrorHeader, Resources.Error.UIDateTimeFormatError);
            }

            Result result = ExportToEFeladojegyzek();
            if (!result.IsError)
            {
                SetResultPanel(result);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        else if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    
    private void SetResultPanel(Result result)
    {
        if (!result.IsError)
        {

            //Panel-ek beállítása
            EFormPanelKereses.Visible = false;
            EFormPanelStatisztika.Visible = false;
            EFormPanelUgyfelAdatok.Visible = false;
            EFormPanelLista.Visible = false;
            ResultPanel.Visible = true;

            //FormFooter beállítása
            FormFooter1.ImageButton_Close.Visible = true;
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            FormFooter1.ImageButton_Save.Visible = false;
            FormFooter1.ImageButton_Cancel.Visible = false;

            UgyfelAdatokPanel.PostakonyvId = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
            string fileNameADAT = UgyfelAdatokPanel.GetXmlFieldValue("adatfajlnev");
            string fileNameUGYFEL = UgyfelAdatokPanel.FileNameXml;
            lbResultFajlNevAdatok.Text = fileNameADAT ?? "???";
            lbResultFajlNevUgyfelAdatok.Text = fileNameUGYFEL ?? "???";
            linkResultDownLoadUgyfelAdatok.NavigateUrl = String.Format("PostazasEFeladoJegyzekDownLoadForm.aspx?FileName={0}&Mode=Result&Type=UGYFEL&PostakonyvId={1}&{2}", fileNameUGYFEL, UgyfelAdatokPanel.PostakonyvId, UI.GetLoginDataQueryString(Page));

            string fileNameADATSource = Convert.ToString(result.Record);
            linkResultDownLoadAdatok.NavigateUrl = String.Format("PostazasEFeladoJegyzekDownLoadForm.aspx?FileName={0}&Mode=File&SourceFileName={1}&{2}", fileNameADAT, fileNameADATSource, UI.GetLoginDataQueryString(Page));
        }
        else
        {
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
    }

    public string GetUgyfelAdatok()
    {
        UgyfelAdatokPanel.PostakonyvId = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;

        Result result = UgyfelAdatokPanel.GetUgyfelAdatokXml();

        System.Xml.XmlDocument xmlDocument = result.Record as System.Xml.XmlDocument;

        System.Xml.XmlElement root = xmlDocument.DocumentElement;


        System.Xml.XmlNodeList childs = root.ChildNodes;

        List<System.Xml.XmlNode> newChilds = new List<System.Xml.XmlNode>();

        foreach (string key in UgyfelAdatMapping.Keys)
        {
            foreach (System.Xml.XmlNode child in childs)
            {
                if (child.Name == key)
                {
                    System.Xml.XmlElement newChild = xmlDocument.CreateElement(UgyfelAdatMapping[key]);
                    newChild.InnerText = child.InnerText;
                    newChilds.Add(newChild);
                }
            }
        }

        root.RemoveAll();

        foreach (System.Xml.XmlNode newChild in newChilds)
        {
            root.AppendChild(newChild);
        }

        return root.OuterXml;
    }

    private Dictionary<string, string> _UgyfelAdatMapping;

    Dictionary<string, string> UgyfelAdatMapping
    {
        get
        {
            if (_UgyfelAdatMapping == null)
            {
                _UgyfelAdatMapping = new Dictionary<string, string>();

                _UgyfelAdatMapping.Add("azonosito", "felado_vevokod");
                _UgyfelAdatMapping.Add("megallapodas", "felado_megallapodas");
                _UgyfelAdatMapping.Add("nev", "felado_nev");
                _UgyfelAdatMapping.Add("iranyitoszam", "felado_irsz");
                _UgyfelAdatMapping.Add("helyseg", "felado_hely");
                _UgyfelAdatMapping.Add("utca", "felado_kozelebbi_cim");
                _UgyfelAdatMapping.Add("kezdoallas", "kezdoallas");
                _UgyfelAdatMapping.Add("zaroallas", "zaroallas");
                _UgyfelAdatMapping.Add("engedelyszam", "engedelyszam");
            }

            return _UgyfelAdatMapping;
        }
    }

    string GetEfjAdatok()
    {
        //<efj_adatok>
        //<efj_zaras>170426_162533</efj_zaras>
        //<efj_szoftver>EDOK</efj_szoftver>
        //<xsd_verzio>8.3</xsd_verzio>
        //</efj_adatok>

        //ééhhnn_óóppmm
        string efj_zaras = DateTime.Now.ToString("yyMMdd_HHmmsss");
        string efj_szoftver = GetSzoftver();

        return String.Format(@"<efj_adatok>
<efj_zaras>{0}</efj_zaras>
<efj_szoftver>{1}</efj_szoftver>
<xsd_verzio>{2}</xsd_verzio>
</efj_adatok>", efj_zaras, efj_szoftver, xsd_verzio);

    }

    private string GetSzoftver()
    {
        string appVersion = Rendszerparameterek.Get(Page, Rendszerparameterek.APPLICATION_VERSION);

        Regex regVer = new Regex(@"v[\d\.]*");
        Match m = regVer.Match(appVersion);

        return String.Format("Contentum.NET {0}", m.Value);
    }


}