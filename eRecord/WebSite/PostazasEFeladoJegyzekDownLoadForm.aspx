﻿<%@ Page Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true"
    CodeFile="PostazasEFeladoJegyzekDownLoadForm.aspx.cs" Inherits="PostazasEFeladoJegyzekDownLoadForm"
    Title="E-Feladójegyzék mentés" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
<%--    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="E-feladójegyzék mentés" />
    <br />
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
