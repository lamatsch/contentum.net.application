﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Xml;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

public partial class PostazasEFeladoJegyzekDownLoadForm : Contentum.eUtility.UI.PageBase
{
    private const string pathForDownLoad = Contentum.eUtility.EFeladoJegyzek.Constants.DefaultXmlDirPath; //@"C:\temp\ContentumNet\EFeladoJegyzek";

    protected void Page_Load(object sender, EventArgs e)
    {
        //FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        string mode = Request.QueryString.Get("Mode");
        string type = Request.QueryString.Get("Type");
        string postakonyvId = Request.QueryString.Get("PostakonyvId");
        string fileName = Request.QueryString.Get("FileName");
        string sourceFileName = Request.QueryString.Get("SourceFileName");

        if (String.IsNullOrEmpty(fileName))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: nincs megadva fájlnév!");
        }
        else
        {
            switch (mode)
            {
                case "File":
                    ProcessDownLoadFile(fileName, sourceFileName);
                    break;
                case "Result":
                    ProcessResult(fileName, type, postakonyvId);
                    break;
                default:
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: nincs megadva végrehajtási mód!");
                    break;
            }
        }
    }

    public void ProcessDownLoadFile(string fileName, string sourceFileName)
    {
        if (!String.IsNullOrEmpty(sourceFileName))
        {
            string path = System.IO.Path.Combine(pathForDownLoad, sourceFileName);
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                Response.Clear();
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(1250);
                Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", fileName ?? ""));
                Response.ContentType = "text/plain";
                //Response.WriteFile(path);
                Response.TransmitFile(path);
                Response.End();
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: a megadott fájl nem található!");
            }

        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: nincs megadva fájlnév!");
        }
    }


    public void ProcessResult(string fileName, string type, string postakonyvId)
    {
        if (!String.IsNullOrEmpty(fileName))
        {
            Result result = null;
            if (type.ToUpperInvariant() == "UGYFEL")
            {
                result = GetContentUgyfelAdatok(postakonyvId);
            }
            else if (type.ToUpperInvariant() == "ADAT")
            {
                result = GetContentAdatok(postakonyvId);
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: nincs megadva a tartalom típusa!");
                return;
            }

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
            else
            {
                Response.Clear();
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(1250);
                Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", fileName));
                Response.ContentType = "text/plain";
                Response.Write(result.Record);
                Response.End();
            }
            
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a fájl kliensre mentésekor: nincs megadva fájlnév!");
        }
    }

    // dummy
    // TODO: paraméterezés? StartDate, EndDate, Postakonyv_Id, Kikuldo_Id ...
    private Result GetContentAdatok(string postakonyvId)
    {
        throw new NotImplementedException();

        // dummy
        Result result = new Result();
        EREC_KuldKuldemenyekService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

        EPostazasiParameterek ePostazasiParameterek = new EPostazasiParameterek();
        ePostazasiParameterek.Postakonyv_Id = postakonyvId;
        ePostazasiParameterek.StartDate = DateTime.Today.ToShortDateString();
        ePostazasiParameterek.EndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59).ToShortDateString();
 

        ExecParam execParam = UI.SetExecParamDefault(Page);
        result = service.KimenoKuldGetAllWithExtensionAndJogosultakForEFeladoJegyzek(execParam, ePostazasiParameterek, true);

        if (result.IsError == false)
        {
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                result.Ds.Tables[0].WriteXml(sw);
                sw.Flush();
                result.Record = sw.ToString();
            }
        }

        return result;
    }

    // TODO: paraméterezés?
    private Result GetContentUgyfelAdatok(string PostakonyvId)
    {
        Result result = new Result();
        EREC_IraIktatoKonyvekService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = PostakonyvId;
        Result result_get = service.Get(execParam);

        if (result_get.IsError)
        {
            return result_get;
        }

        if (result_get.IsError == false)
        {
            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_get.Record;

            if (erec_IraIktatoKonyvek != null && !String.IsNullOrEmpty(erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok);

                string formattedXml = Contentum.eUtility.EFeladoJegyzek.Xml.GetFormattedStringFromXmlDocument(xmlDocument);

                result.Record = formattedXml;
            }
        }

        return result;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        //FormFooter1.ImageButton_Save.Visible = true;
        //FormFooter1.ImageButton_Close.Visible = false;
        //FormFooter1.ImageButton_Back.Visible = true;
        //FormFooter1.ImageButton_Save.OnClientClick = "";

        FormFooter1.ImageButton_Save.Visible = false;
        FormFooter1.ImageButton_Close.Visible = true;
        FormFooter1.ImageButton_Back.Visible = false;

    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Save)
        //{


        //}
        //else if (e.CommandName == CommandName.Back)
        //{
        //    if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
        //    {
        //        Response.Redirect("Login.aspx", true);
        //    }
        //    if (Page.PreviousPage != null)
        //    {
        //        Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
        //    }
        //    else
        //    {
        //        Page.Response.Redirect("Default.aspx", true);
        //    }
        //}
    }
}