<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PostazasEFeladoJegyzekJavitasForm.aspx.cs" Inherits="PostazasEFeladoJegyzekJavitasForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup"
    TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <div>
        <asp:Panel ID="MainPanel" runat="server">
            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
            <table cellpadding="0" cellspacing="0" width="90%">
                <tr>
                    <td style="text-align: left; vertical-align: top; width: 0px;">
                        <asp:ImageButton ImageUrl="../images/hu/Grid/minus.gif" runat="server" ID="btnCpeKuldemenyek"
                            OnClientClick="return false;" />
                    </td>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <asp:UpdatePanel ID="KuldKuldemenyekUpdatePanel" runat="server" UpdateMode="Conditional"
                            OnLoad="KuldKuldemenyekUpdatePanel_Load">
                            <ContentTemplate>
                                <%-- SubListHeader --%>
                                <uc1:SubListHeader ID="SubListHeader1" runat="server" />
                                <%-- /SubListHeader --%>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeKuldemenyek" runat="server" TargetControlID="KuldemenyekListPanel"
                                    CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeKuldemenyek" CollapseControlID="btnCpeKuldemenyek"
                                    ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="~/images/hu/Grid/plus.gif"
                                    ExpandedImage="~/images/hu/Grid/minus.gif" ImageControlID="btnCpeKuldemenyek"
                                    ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja elrejt" CollapsedText="K�ldem�nyek list�ja mutat">
                                </ajaxToolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="KuldemenyekListPanel" runat="server" Visible="false" Width="100%">
                                    <asp:Label ID="Label_Kuldemenyek" runat="server" Text="K�ldem�nyek:" Visible="false"
                                        CssClass="GridViewTitle"></asp:Label>
                                    <br />
                                    <asp:GridView ID="KuldKuldemenyekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                        BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                        AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="KuldKuldemenyekGridView_RowDataBound"
                                        OnPreRender="KuldKuldemenyekGridView_PreRender"
                                        OnRowCommand="KuldKuldemenyekGridView_RowCommand">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <%--                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />--%>
                                                <HeaderStyle CssClass="GridViewInvisibleColumnStyle" />
                                                <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:Buttons,Modositas%>"
                                                SelectImageUrl="~/images/hu/egyeb/modositas.gif">
                                                <HeaderStyle Width="25px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                        Visible="false" OnClientClick="return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
<%--                                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                <ItemTemplate>
                                                    <div class="DisableWrap" style="text-align: left;">
                                                        <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                            onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                            AlternateText="Jav�t�s..." Visible="false" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:BoundField DataField="EREC_IraIktatoKonyvek_Nev" HeaderText="Postak�nyv" SortExpression="EREC_IraIktatoKonyvek_Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--                                            <asp:BoundField DataField="EREC_IraIktatoKonyvek_Ev" HeaderText="�v" SortExpression="EREC_IraIktatoKonyvek_Ev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <asp:BoundField DataField="KuldesMod_Nev" HeaderText="K�ld�sm�d" SortExpression="KRT_KodTarakKuldesMod.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cimzett_Nev_Partner" HeaderText="C�mzett" SortExpression="EREC_KuldKuldemenyek.NevSTR_Bekuldo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cimzett_Cim_Partner" HeaderText="C�mzett&nbsp;c�me" SortExpression="EREC_KuldKuldemenyek.CimSTR_Bekuldo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d" SortExpression="EREC_KuldKuldemenyek.BarCode">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RagSzam" HeaderText="Postai azonos�t�" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BelyegzoDatuma_rovid" HeaderText="Felad�s&nbsp;ideje"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="EREC_KuldKuldemenyek.BelyegzoDatuma">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Ar" HeaderText="�r" ItemStyle-HorizontalAlign="Center"
                                                SortExpression="EREC_KuldKuldemenyek.Ar">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PeldanySzam" HeaderText="Mennyis�g" Visible="false" ItemStyle-HorizontalAlign="Center"
                                                SortExpression="EREC_KuldKuldemenyek.PeldanySzam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="40px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="EREC_KuldKuldemenyek.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--                        <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="KRT_KodTarakAllapot.Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>
                                            <%--                                            <asp:TemplateField HeaderText="�llapot" SortExpression="KRT_KodTarakAllapot.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelAllapotNev" runat="server" Text='<%# string.Concat(Eval("AllapotNev"),                                          
	                              (Eval("Allapot") as string) == "03" ?
			                            "<br/>(" + (Eval("TovabbitasAlattAllapotNev") as string) + ")" : "")  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye"
                                                SortExpression="KRT_CsoportokOrzo.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--                                            <asp:BoundField DataField="FelhasznaloCsoport_Id_ExpedialNev" HeaderText="Expedi�l�"
                                                SortExpression="KRT_CsoportokExpedialo.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ExpedialasIdeje_rovid" HeaderText="Expedi�l�s id�pontja"
                                                SortExpression="EREC_KuldKuldemenyek.ExpedialasIdeje">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="Expedi�l�s" SortExpression="KRT_CsoportokExpedialo.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelExpedialoNev" runat="server" Text='<%# string.IsNullOrEmpty(Eval("FelhasznaloCsoport_Id_ExpedialNev") as string) ? "" :
                                                    string.Concat(Eval("FelhasznaloCsoport_Id_ExpedialNev"),
			                                        string.Format("<br/>({0})", Eval("ExpedialasIdeje_rovid")))  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Csoport_Id_KikuldoNev" HeaderText="Kik�ld�" SortExpression="Csoportok_KikuldoNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <uc2:FormFooter ID="FormFooter1" runat="server" />
    </div>
</asp:Content>
