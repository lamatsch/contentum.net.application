using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class PostazasEFeladoJegyzekJavitasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    private String KuldemenyId = ""; // id-k �sszef�zve

    private String[] KuldemenyekArray;

    // figyeli, hogy volt-e friss�t�s
    protected bool IsDirty
    {
        get
        {
            if (ViewState["IsDirty"] != null)
            {
                return (bool)ViewState["IsDirty"];
            }
            return false;
        }
        set { ViewState["IsDirty"] = value; }
    }


    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string FunkcioKod_Postazas = "Postazas"; // kimen� k�ldem�ny m�dos�t�si jog


    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_Postazas);
                break;
        }

        SubListHeader1.RowCount_Changed += new EventHandler(SubListHeader1_RowCount_Changed);

        SubListHeader1.PagingButtonsClick += new EventHandler(SubListHeader1_PagingChanged);

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.KuldemenyId)))
        {
            if (Session["SelectedKuldemenyIds"] != null)
                KuldemenyId = Session["SelectedKuldemenyIds"].ToString();
        }
        else KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);


        if (!String.IsNullOrEmpty(KuldemenyId))
        {
            KuldemenyekArray = KuldemenyId.Split(';');
        }
        if (!IsPostBack)
        {
            LoadFormComponents();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // TODO: Resources.Form
        string PostazasEFeladoJegyzekJavitasFormHeaderTitle = "E-Felad�jegyz�k - Jav�t�s";
        FormHeader1.HeaderTitle = PostazasEFeladoJegyzekJavitasFormHeaderTitle;
        //FormHeader1.ModeText = "";

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        //ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        //F� lista gombjainak l�that�s�g�nak be�ll�t�sa
        SubListHeader1.NewVisible = false;
        SubListHeader1.ModifyVisible = false;
        SubListHeader1.ViewVisible = false;
        SubListHeader1.InvalidateVisible = false;
        SubListHeader1.ValidFilterVisible = false;

        //Keres�si objektum t�pus�nak megad�sa

        //F� lista megjelen�sek testreszab�sa

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa

        //selectedRecordId kezel�se
        SubListHeader1.AttachedGridView = KuldKuldemenyekGridView;


        JavaScripts.RegisterPopupWindowClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // elt�ntetj�k, hogy friss�lj�n a h�v� lista
        // nem lehet kor�bban, mert a FormFooter fel�l�rn�
        FormFooter1.ImageButton_Cancel.Visible = false;
    }

    private void LoadFormComponents()
    {
        #region KuldemenyJavitas
        if (String.IsNullOrEmpty(KuldemenyId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
            return;
        }
        else
        {
            KuldemenyekListPanel.Visible = true;
            FillKuldemenyGridView();
        }
        #endregion
    }

    private void FillKuldemenyGridView()
    {
        DataSet ds = KuldKuldemenyekGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != KuldemenyekArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }
    }

    protected void KuldKuldemenyekGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(SubListHeader1.Scrollable, cpeKuldemenyek);
        SubListHeader1.RefreshPagerLabel();
    }

    protected void SubListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeKuldemenyek);
        KuldKuldemenyekGridViewBind();
    }

    void SubListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KuldKuldemenyekGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void KuldKuldemenyekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KuldKuldemenyekGridView, selectedRowNumber, "check");

            string id = KuldKuldemenyekGridView.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewEsemenyek_SelectRowCommand(id);

            string js = "OpenNewWindow(); function OpenNewWindow() { "
                + JavaScripts.SetOnClientClick_DisplayUpdateProgress("PostazasForm.aspx",
                        QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.KuldemenyId + "=" + id
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID
                        , EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID)
                + "}";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
        }

    }

    protected DataSet KuldKuldemenyekGridViewBind()
    {
        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            search.Id.Value = Search.GetSqlInnerString(KuldemenyekArray);
            search.Id.Operator = Query.Operators.inner;

            // Lapoz�s be�ll�t�sa:
            UI.SetPaging(ExecParam, SubListHeader1);

            Result res = service.KimenoKuldGetAllWithExtensionAndJogosultak(ExecParam, search, true);

            UI.GridViewFill(KuldKuldemenyekGridView, res, SubListHeader1, FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void KuldKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckModosithatoPostazashoz(e, Page);
    }

    
            //UpdatePanel Load esem�nykezel�je
    protected void KuldKuldemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KuldKuldemenyekGridViewBind();
                    IsDirty = true;
                    break;
            }
        }
    }


    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        // ...
        if (e.CommandName == CommandName.Save)
        {
            // csak akkor friss�tj�k a f�list�t/h�v�oldalt, ha feltehet�en volt v�ltoz�s
            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, IsDirty);        
        }
    }
}
