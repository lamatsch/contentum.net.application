﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PostazasForm.aspx.cs" Inherits="PostazasForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/PostazasiAdatokPanel.ascx" TagName="PostazasiAdatokPanel"
    TagPrefix="uc3" %>
    
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc" %>

<%@ Register Src="~/eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="uc" %>
        
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatePanelFormHeader" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <uc1:FormHeader ID="FormHeader1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Frissítés jelzése-->
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:Panel ID="MainPanel" runat="server">
        <asp:UpdatePanel ID="PostazasiAdatokUpdatePanel" runat="server">
        <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <uc3:PostazasiAdatokPanel ID="PostazasiAdatokPanel1" runat="server" />
                        &nbsp;
                    </eUI:eFormPanel>
                    <asp:HiddenField ID="KuldemenyId_HiddenField" runat="server" />
                    <eUI:eFormPanel ID="TipusosTargyszavakPanel_Kuldemenyek" runat="server" Visible="false">
                         <uc:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_Kuldemenyek" RepeatColumns="2" RepeatDirection="Horizontal"
                          runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                     </eUI:eFormPanel>	
                    <eUI:eFormPanel ID="TipusosTargyszavakPanel_KimenoKuldemenyek" runat="server" Visible="false">
                         <uc:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_KimenoKuldemenyek" RepeatColumns="2" RepeatDirection="Horizontal"
                          runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                     </eUI:eFormPanel>	
                </td>
            </tr>
            <tr>
                <table cellpadding="0" cellspacing="0" width="90%">
                    <tr>
                        <td style="width: 20%">
                        </td>
                        <td>
                            <uc2:FormFooter ID="FormFooter1" runat="server" />
                        </td>
                        <td style="width: 20%">
                        </td>
                    </tr>
                </table>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
