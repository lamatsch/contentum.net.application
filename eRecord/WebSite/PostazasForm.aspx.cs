﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PostazasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    // BLG_1721
    //private PageView pageView = null;
    Contentum.eUtility.PageView pageView = null;
    //  private ComponentSelectControl compSelector = null;

    private string KuldemenyId = "";
    private string IratPeldanyId = "";

    private const string FunkcioKod_Postazas = "Postazas";
    private const string FunkcioKod_KimenoKuldemenyFelvitel = "KimenoKuldemenyFelvitel";

    private string Startup = String.Empty;
    private string Mode = String.Empty;

    private Type _type = typeof(EREC_KuldKuldemenyek);

    protected void Page_Init(object sender, EventArgs e)
    {
        // BLG_1721
        //pageView = new PageView(Page, ViewState);
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        // Command == Postazas vagy KimenoKuldemenyFelvitel
        Command = Request.QueryString.Get(CommandName.Command);

        Startup = Request.QueryString.Get(QueryStringVars.Startup); // PostazasTomeges
        Mode = Request.QueryString.Get(QueryStringVars.Mode); // Sima, Ajanlott, Tertivevenyes, HivatalosIrat


        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosultságellenõrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                // BLG_1721
                //compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                //FormFooter1.Controls.Add(compSelector);
                break;
            case CommandName.KimenoKuldemenyFelvitel:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_KimenoKuldemenyFelvitel);
                break;
            case CommandName.Modify:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_Postazas);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_Postazas);
                break;
        }

        // Az egyik kell a kettõ közül: (ha nem KimenoKuldemenyFelvitel van)
        KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
        IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

        PostazasiAdatokPanel1.ErrorPanel = FormHeader1.ErrorPanel;

        if (!IsPostBack)
        {
            LoadFormComponents();
        }

        FormHeader1.TemplateObjectType = _type;
        FormHeader1.CustomTemplateTipusNev = "KimenoKuldemeny";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        FormHeader1.ButtonsClick +=
            new CommandEventHandler(FormHeader1_ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            //      Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        // Cím állítása:

        FormHeader1.DisableModeLabel = true;
        if (Command == CommandName.KimenoKuldemenyFelvitel)
        {
            FormHeader1.HeaderTitle = Resources.Form.KimenoKuldemenyLetrehozas;

            //LZS - BLG_5341 - CommandName.KimenoKuldemenyFelvitelnél legyen szürke ez a rész.
            //otpTipusosTargyszavak_KimenoKuldemenyek.ReadOnly = true;
            //otpTipusosTargyszavak_KimenoKuldemenyek.ViewMode = true;
        }
        else if (Command == CommandName.Modify)
        {
            FormHeader1.HeaderTitle = Resources.Form.KimenoKuldemenyModositas;
        }
        else
        {
            FormHeader1.HeaderTitle = Resources.Form.PostazasFormHeaderTitle;
        }

        string js_postaKonyvCheck = "var ddl_postakonyv = $get('" + PostazasiAdatokPanel1.PostaKonyvekDropDownList.ClientID + @"');
            if (ddl_postakonyv)
            {
                if (ddl_postakonyv.options.length == 0 || ddl_postakonyv.options[ddl_postakonyv.selectedIndex].value == '')
                { alert('" + Resources.Error.UINincsMegadvaPostakonyv + @"'); return false; }
            }
            ";

        FormFooter1.ImageButton_Save.OnClientClick = js_postaKonyvCheck;
        FormFooter1.ImageButton_SaveAndNew.OnClientClick = js_postaKonyvCheck;

        string js_pldselected = @"var hf=$get('" + PostazasiAdatokPanel1.IratPeldanyTextBoxClientID + "'); if (hf && hf.value && hf.value != '') { return confirm('Kiválaszott egy iratpéldányt, de nem adta hozzá a fához. Ha folytatja a mûveletet, a kiválasztott iratpéldány nem kerül bele a kimenõ küldeménybe. Kívánja folytatni a mûveletet?'); }";
        FormFooter1.ImageButton_Save.OnClientClick += js_pldselected;
        FormFooter1.ImageButton_SaveAndNew.OnClientClick += js_pldselected;

        //if (UgykorDropDown.selectedIndex >= 0)
        //    { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //CR3132 Template megjelenítése az iratpéldéldánylista oldalról is
        if ((Command == CommandName.KimenoKuldemenyFelvitel) || (String.IsNullOrEmpty(Command)))
        {
            FormHeader1.FormTemplateLoader1_Visibility = true;
        }
    }

    // BLG_1721
    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            List<Control> componentList = new List<Control>();
            componentList.Add(PostazasiAdatokPanel1);
            componentList.AddRange(PageView.GetSelectableChildComponents(PostazasiAdatokPanel1.Controls));
            pageView.CompSelector.Add_ComponentOnClick(componentList);
            FormFooter1.SaveEnabled = false;
        }
    }
    private void LoadFormComponents()
    {
        // Kimenõ küldemény felvitel:
        if (Command == CommandName.KimenoKuldemenyFelvitel)
        {
            PostazasiAdatokPanel1.SetAblakosBoritekComponents();
            PostazasiAdatokPanel1.SetComponentsForKimenoKuldemenyFelvitel(FormHeader1.ErrorPanel, null);
            // Rendben és új gomb megjelenítése:
            FormFooter1.ImageButton_SaveAndNew.Visible = true;
            FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(KuldemenyId);
        }
        else if (Command == CommandName.Modify)
        {
            if (!String.IsNullOrEmpty(KuldemenyId))
            {
                PostazasiAdatokPanel1.SetComponentsForKimenoKuldemenyModositas(KuldemenyId, FormHeader1.ErrorPanel, null);
                FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(KuldemenyId);
                FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(KuldemenyId);
            }
            else if (!String.IsNullOrEmpty(IratPeldanyId))
            {
                // IratPeldany Id-ból küldemény meghatározása:

                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                // Szûrni kell az iratpéldány id-ra, és a Küldemény állapotára (expediált lehet csak)
                EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch(true);
                search.Extended_EREC_PldIratPeldanyokSearch.Id.Value = IratPeldanyId;
                search.Extended_EREC_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

                search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
                search.Allapot.Operator = Query.Operators.equals;

                Result result = service.KimenoKuldGetAllWithExtension(execParam, search);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    return;
                }

                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataRow row = result.Ds.Tables[0].Rows[0];

                    KuldemenyId = row["Id"].ToString();
                    // KuldemenyId-t letesszük hiddenfieldbe:
                    KuldemenyId_HiddenField.Value = KuldemenyId;

                    PostazasiAdatokPanel1.SetComponentsForKimenoKuldemenyModositas(KuldemenyId, FormHeader1.ErrorPanel, null);
                    FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(KuldemenyId);
                    FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(KuldemenyId);
                }

                #region RAGSZAM
                Result resultKuldTerti = GetKuldTertiFromKuldemeny(KuldemenyId);
                if (!String.IsNullOrEmpty(resultKuldTerti.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    return;
                }
                FillRagszamParameters(resultKuldTerti);
                #endregion
            }

        }
        else
        {
            if (Startup == "PostazasTomeges")
            {
                //EFormPanelVonalkodRagszam.Visible = true;
                PostazasiAdatokPanel1.RogzitesTipus = eRecordComponent_PostazasiAdatokPanel.Mode.Multiple;

                switch (Mode)
                {
                    case "Sima":
                        PostazasiAdatokPanel1.KuldesMod = eRecordComponent_PostazasiAdatokPanel.KuldesModType.Sima;
                        break;
                    case "Ajanlott":
                        PostazasiAdatokPanel1.KuldesMod = eRecordComponent_PostazasiAdatokPanel.KuldesModType.Ajanlott;
                        break;
                    case "Tertivevenyes":
                        PostazasiAdatokPanel1.KuldesMod = eRecordComponent_PostazasiAdatokPanel.KuldesModType.Tertivevenyes;
                        break;
                    case "HivatalosIrat":
                        PostazasiAdatokPanel1.KuldesMod = eRecordComponent_PostazasiAdatokPanel.KuldesModType.HivatalosIrat;
                        break;
                }
                PostazasiAdatokPanel1.SetComponentsForPostazas("", FormHeader1.ErrorPanel, null);
            }
            else
            {

                // Postázás:
                if (!String.IsNullOrEmpty(KuldemenyId))
                {
                    PostazasiAdatokPanel1.SetComponentsForPostazas(KuldemenyId, FormHeader1.ErrorPanel, null);
                    FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(KuldemenyId);
                    FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(KuldemenyId);

                }
                else if (!String.IsNullOrEmpty(IratPeldanyId))
                {
                    // IratPeldany Id-ból küldemény meghatározása:

                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    // Szûrni kell az iratpéldány id-ra, és a Küldemény állapotára (expediált lehet csak)
                    EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch(true);
                    search.Extended_EREC_PldIratPeldanyokSearch.Id.Value = IratPeldanyId;
                    search.Extended_EREC_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

                    search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt; //nekrisz itt csak az expediáltak jók, nem az összes ami postázható????
                    search.Allapot.Operator = Query.Operators.equals;

                    Result result = service.KimenoKuldGetAllWithExtension(execParam, search);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        System.Data.DataRow row = result.Ds.Tables[0].Rows[0];

                        KuldemenyId = row["Id"].ToString();
                        // KuldemenyId-t letesszük hiddenfieldbe:
                        KuldemenyId_HiddenField.Value = KuldemenyId;

                        PostazasiAdatokPanel1.SetComponentsForPostazas(KuldemenyId, FormHeader1.ErrorPanel, null);
                    }

                    #region RAGSZAM
                    Result resultKuldTerti = GetKuldTertiFromKuldemeny(KuldemenyId);
                    if (!String.IsNullOrEmpty(resultKuldTerti.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    FillRagszamParameters(resultKuldTerti);
                    #endregion
                }
            }
        }
    }


    private bool CheckKimenoKuldemeny(EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod)
    {
        bool bOK = true;
        List<string> messages = new List<string>(10);
        if (erec_KuldKuldemenyek != null)
        {
            if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    messages.Add("Ajánlott küldeménynél a ragszám megadása kötelezõ!");
                    bOK = false;
                }
            }
            if (erec_KuldKuldemenyek.Tertiveveny == "1")
            {
                if (erec_KuldKuldemenyek.Ajanlott != "1")
                {
                    messages.Add("Tértivevény különszolgáltatás csak ajánlott szolgáltatással együtt vehetõ igénybe!");
                    bOK = false;
                }

                bool isKulfoldi = erec_KuldKuldemenyek.KimenoKuldemenyFajta.StartsWith(Constants.OrszagViszonylatKod.Europai)
                    || erec_KuldKuldemenyek.KimenoKuldemenyFajta.StartsWith(Constants.OrszagViszonylatKod.EgyebKulfoldi);


                //külföldi küldeményeknél nem kell a tértitevény vonalkódja
                // CR 3088 :  BOPMH-nál, CSBO-nál nem kötelezõ a tértivevény kitöltése (csak FPH-nál)
                // ORG függõ megjelenítés
                // BLG_591
                //if ((!isKulfoldi) && (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH))
                if ((!isKulfoldi) && (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TERTIVONALKOD_KELL, false)))

                {
                    if (String.IsNullOrEmpty(tertivevenyVonalkod))
                    {
                        messages.Add("Tértivevényes küldeménynél a tértivevény vonalkód megadása kötelezõ!");
                        bOK = false;
                    }
                }
            }

            if (erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat
                || erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat_sajat_kezbe)
            {
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    messages.Add("Hivatalos iratnál a ragszám megadása kötelezõ!");
                    bOK = false;
                }
                // CR 3088 :  BOPMH-nál, CSBO-nál nem kötelezõ a tértivevény kitöltése (csak FPH-nál)
                // ORG függõ megjelenítés
                // BLG_591
                //if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH))
                if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TERTIVONALKOD_KELL, false)))

                {
                    messages.Add("Hivatalos iratnál a tértivevény vonalkód megadása kötelezõ!");
                    bOK = false;
                }
            }
            //Ha kimenő küldemény és a kódtárfüggőség összerendelés szerint hivatalos küldemény akkor kötelező a ragszám!
            if (Contentum.eUtility.ePosta.ePostaUtility.IsHivatalosKuldemeny(UI.SetExecParamDefault(Page), erec_KuldKuldemenyek.KimenoKuldemenyFajta) && string.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
            {
                messages.Add("Hivatalos levélnél a ragszám megadása kötelezõ!");
                bOK = false;
            }

            // modified by nekrisz: NaN ár ellenörzés
            // BLG_1721
            bool isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);
            if (!isTUK)
            {
                decimal d_ar;

                if (!decimal.TryParse(erec_KuldKuldemenyek.Ar, out d_ar))
                {
                    messages.Add("Az adott küldemény ára nem meghatározható, konzultáljon az alkalmazás gazdával!");
                    bOK = false;
                }
            }
        }

        if (!bOK)
        {
            string js = String.Format(@"alert('{0}');", String.Join(@"\n", messages.ToArray()));
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
        }

        return bOK;
    }

  
    // CR3150 Cím szegmentáltság ellenörzés
    private bool CheckPartnerCim(EREC_KuldKuldemenyek kuldemeny)
    {
        // CR3284 : Tömeges postázáskor nem kell szegmentáltság-ellenörzés
        if (PostazasiAdatokPanel1.RogzitesTipus == eRecordComponent_PostazasiAdatokPanel.Mode.Multiple) return true;

        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.CIM_SZEGMENTALTSAG_ELLENORZES, false))
        {
            if (String.IsNullOrEmpty(kuldemeny.Cim_Id))
            {
                FormHeader1.ErrorPanel.Header = Resources.Error.ErrorLabel;
                FormHeader1.ErrorPanel.Body = Resources.Error.ErrorCode_52799; //A postázási cím nem struktúrált!
                FormHeader1.ErrorPanel.Visible = true;
                return false;
            }
        }
        return true;
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save
            || e.CommandName == CommandName.SaveAndNew)
        {

            if ((Command == CommandName.KimenoKuldemenyFelvitel && FunctionRights.GetFunkcioJog(Page, FunkcioKod_KimenoKuldemenyFelvitel))
                || (Command != CommandName.KimenoKuldemenyFelvitel && FunctionRights.GetFunkcioJog(Page, FunkcioKod_Postazas)))
            {
                if (Command == CommandName.KimenoKuldemenyFelvitel)
                {

                    if (!Page.IsValid)
                    {
                        // nem érvényes az oldal

                        return;
                    }

                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    EREC_IraIktatoKonyvekService iktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                    ExecParam execParamIktatoKonyvek = new ExecParam();



                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    // CR 3107 : Postázásnál Postazas funkciokod-ot is néz hogy enged-e új iratpéldányt létrehozni
                    // CR kapcsán készült, de végül nem ezt használjuk
                    execParam.FunkcioKod = FunkcioKod_Postazas;

                    EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanel1.GetBusinessObjectFromComponents(true);

                    execParamIktatoKonyvek.Record_Id = kuldemeny.IraIktatokonyv_Id;
                    Result iktatoKonyvekResult = iktatoKonyvekService.Get(execParamIktatoKonyvek);
                    EREC_IraIktatoKonyvek iktatoKonyv = iktatoKonyvekResult.Record as EREC_IraIktatoKonyvek;
                    int newFoszam = int.Parse(iktatoKonyv.UtolsoFoszam) + 1;

                    iktatoKonyv.UtolsoFoszam = newFoszam.ToString();


                    if (String.IsNullOrEmpty(iktatoKonyvekResult.ErrorCode))
                    {
                        kuldemeny.Erkezteto_Szam = newFoszam.ToString();
                    }


                    kuldemeny.Azonosito = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullKuldemenyAzonosito(execParam, (EREC_IraIktatoKonyvek)iktatoKonyvekResult.Record, kuldemeny);
                    kuldemeny.Updated.Azonosito = true;
                    kuldemeny.Updated.Erkezteto_Szam = true;

                    iktatoKonyv.Updated.UtolsoFoszam = true;
                    Result updateResult = iktatoKonyvekService.Update(execParamIktatoKonyvek, iktatoKonyv);



                    // CR3150 Cím szegmentáltság ellenörzés
                    if (!CheckPartnerCim(kuldemeny))
                    {

                        return;
                    }


                    if (!CheckKimenoKuldemeny(kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod))
                    {
                        return;
                    }

                    // nekrisz CR 2960
                    // Postázáskor Iratpéldányok kiválasztása, hozzárendelése a küldeményekhez 
                    // Iratpéldányokat Expediálja és Postázza, Küldeményt létrehozza (az expediált iratpéldányokkal) és postázza


                    Result resultKuldFelv;
                    String[] iratPld_Array;
                    // CR3108 : Postázott iratpéldányok Ügyiratba helyezése 
                    // CR kapcsán elõjött hiba
                    // a GetIratPeldanyok által visszaadott hibát elnyelte, ezért ha false, akkor hiba van és ellenõrizni kell hogy van-e eleme a tömbnek
                    if (PostazasiAdatokPanel1.GetIratPeldanyok(out iratPld_Array))
                    {
                        if (iratPld_Array == null)
                        {
                            resultKuldFelv = service.KimenoKuldemenyFelvitel(execParam, kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod);
                        }
                        else
                        {
                            resultKuldFelv = service.KimenoKuldemenyFelvitelWithExpedialas(execParam, kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod, iratPld_Array);
                        }


                        if (!resultKuldFelv.IsError)
                        {

                            bool voltHiba = false;
                            #region típusos objektumfüggõ tárgyszavak mentése

                            Result result_ot_tipusos_kimeno = Save_TipusosObjektumTargyszavai_KimenoKuldemenyek(resultKuldFelv.Uid);

                            if (!String.IsNullOrEmpty(result_ot_tipusos_kimeno.ErrorCode))
                            {
                                // hiba
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot_tipusos_kimeno);
                                voltHiba = true;
                            }

                            #endregion típusos objektumfüggõ tárgyszavak mentése

                            if (!voltHiba)
                            {
                                PostazasiAdatokPanel1.SetSessionData();

                                if (e.CommandName == CommandName.SaveAndNew)
                                {
                                    //                            string js_refreshKuldemenyekList = @"                             
                                    //                            Utility.Popup.getParentWindow().refreshMasterList();";

                                    // JavaScripts.RegisterSelectedRecordIdToParent(Page, resultKuldFelv.Uid);
                                    //CR 2067 fix: kimenõ küldemény sorozatos felvétel lassú
                                    //Rendben és új funkciónál háttérben történõ listafrissítés (Kimenõ küldemények lista) kikapcsolva
                                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "refreshKuldemenyekList", js_refreshKuldemenyekList, true);

                                    // Oldal újratöltése:
                                    //Response.Redirect(Request.Url.OriginalString, true);

                                    PostazasiAdatokPanel1.ClearForm();

                                    string templateId = FormHeader1.CurrentTemplateId;

                                    if (!String.IsNullOrEmpty(templateId))
                                    {
                                        FormHeader1.LoadTemplateObjectById(templateId);
                                        LoadComponentsFromTemplate();
                                    }

                                    LoadFormComponents();
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, resultKuldFelv.Uid);
                                    //JavaScripts.RegisterCloseWindowClientScript(Page);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                                    MainPanel.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            // CR 3048: Felvitelbõl visszaérkezõ hiba szöveges kiírása
                            resultKuldFelv.ErrorMessage = ResultError.GetErrorMessageFromResultObject(resultKuldFelv);
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultKuldFelv);
                        }
                    }

                }
                else if (Command == CommandName.Modify)
                {
                    if (String.IsNullOrEmpty(KuldemenyId))
                    {
                        // hátha letettük hiddenfieldbe:
                        KuldemenyId = KuldemenyId_HiddenField.Value;
                    }

                    if (String.IsNullOrEmpty(KuldemenyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(KuldemenyId))
                        {
                            // hátha letettük hiddenfieldbe:
                            KuldemenyId = KuldemenyId_HiddenField.Value;
                        }

                        if (String.IsNullOrEmpty(KuldemenyId))
                        {
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            return;
                        }
                        else
                        {
                            if (!Page.IsValid)
                            {
                                // nem érvényes az oldal
                                // PostazasiAdatokPanel1.SetValidateFields(false);
                                return;
                            }
                            // Módosítás:

                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanel1.GetBusinessObjectFromComponents(true);

                            if (!CheckKimenoKuldemeny(kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod))
                            {
                                return;
                            }
                            kuldemeny.Id = KuldemenyId;

                            // CR 3028 Kimenõ küldemény módosítás hiba: ha nincs iratpéldáy, nem lehet elmenteni a módosítást
                            //  String[] iratPld_Array;
                            //  if (PostazasiAdatokPanel1.GetIratPeldanyok(out iratPld_Array))
                            //  {
                            execParam.Record_Id = KuldemenyId;

                            Result result = service.KimenoKuldemenyModositas(execParam, kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                bool isError = false;
                                #region típusos objektumfüggõ tárgyszavak mentése

                                Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Kuldemenyek(kuldemeny.Id);

                                if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot_tipusos);
                                    isError = true;
                                }

                                Result result_ot_tipusos_kimeno = Save_TipusosObjektumTargyszavai_KimenoKuldemenyek(kuldemeny.Id);

                                if (!String.IsNullOrEmpty(result_ot_tipusos_kimeno.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot_tipusos_kimeno);
                                    isError = true;
                                }

                                #endregion típusos objektumfüggõ tárgyszavak mentése

                                if (!isError)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyId);
                                    //JavaScripts.RegisterCloseWindowClientScript(Page);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                                    MainPanel.Visible = false;
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            // }
                            // else return;

                        }

                    }
                }
                else
                {
                    if (Startup == "PostazasTomeges")
                    {
                        string[] VonalkodArray = PostazasiAdatokPanel1.VonalkodArray;
                        string[] RagszamArray = PostazasiAdatokPanel1.RagszamArray;
                        string[] TertivevenyRagszamArray = PostazasiAdatokPanel1.TertivevenyVonalkodArray;
                        #region Tömeges postázás
                        // Postázás:

                        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanel1.GetBusinessObjectFromComponents();

                        Result result = service.Postazas_Tomeges(execParam, kuldemeny, VonalkodArray, RagszamArray, TertivevenyRagszamArray);

                        if (!result.IsError)
                        {
                            //JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyId);
                            //JavaScripts.RegisterCloseWindowClientScript(Page);
                            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                            MainPanel.Visible = false;
                        }
                        else
                        {
                            if (result.Record != null)
                            {
                                PostazasiAdatokPanel1.MarkFailedRecords(new List<string>((string[])(result.Record)));
                            }
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            PostazasiAdatokPanel1.RegisterArKalkulatorJs();
                        }
                        #endregion Tömeges postázás
                    }
                    else
                    {

                        #region Postázás
                        if (String.IsNullOrEmpty(KuldemenyId))
                        {
                            // hátha letettük hiddenfieldbe:
                            KuldemenyId = KuldemenyId_HiddenField.Value;
                        }

                        if (String.IsNullOrEmpty(KuldemenyId))
                        {
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            return;
                        }
                        else
                        {
                            // Postázás:

                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                           // BUG_14234
                           // EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanel1.GetBusinessObjectFromComponents();
                           EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanel1.GetBusinessObjectFromComponents(true);
                           
                            // CR3150 Cím szegmentáltság ellenörzés
                            if (!CheckPartnerCim(kuldemeny))
                            {

                                return;
                            }
                            if (!CheckKimenoKuldemeny(kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod))
                            {
                                return;
                            }
                            kuldemeny.Id = KuldemenyId;

                            Result result = service.Postazas(execParam, kuldemeny, PostazasiAdatokPanel1.TertivevenyVonalkod);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                bool isError = false;
                                #region típusos objektumfüggõ tárgyszavak mentése

                                Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Kuldemenyek(kuldemeny.Id);

                                if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot_tipusos);
                                    isError = true;
                                }

                                Result result_ot_tipusos_kimeno = Save_TipusosObjektumTargyszavai_KimenoKuldemenyek(kuldemeny.Id);

                                if (!String.IsNullOrEmpty(result_ot_tipusos_kimeno.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot_tipusos_kimeno);
                                    isError = true;
                                }

                                #endregion típusos objektumfüggõ tárgyszavak mentése

                                if (!isError)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyId);
                                    //JavaScripts.RegisterCloseWindowClientScript(Page);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                                    MainPanel.Visible = false;
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                PostazasiAdatokPanel1.RegisterArKalkulatorJs();
                            }

                        }
                        #endregion
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
        }
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromTemplate();
            //CR3132 Template megjelenítése az iratpéldéldánylista oldalról is
            //if (!IsPostBack)
            //{
            //    LoadFormComponents();
            //}
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormHeader1.NewTemplate(GetTempateBusinessObjects());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTempateBusinessObjects());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    private EREC_KuldKuldemenyek GetTempateBusinessObjects()
    {
        EREC_KuldKuldemenyek kuld = PostazasiAdatokPanel1.GetBusinessObjectFromComponents(true);
        kuld.BarCode = "<null>";
        kuld.RagSzam = "<null>";
        kuld.BelyegzoDatuma = "<null>";
        return kuld;
    }

    private void LoadComponentsFromTemplate()
    {
        EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)FormHeader1.TemplateObject;
        PostazasiAdatokPanel1.LoadComponentsFromTemplate(kuld, true, FormHeader1.ErrorPanel, null);
    }

    private Result GetKuldTertiFromKuldemeny(string kuldemenyId)
    {
        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Kuldemeny_Id.Filter(kuldemenyId);

        Result result = service.GetAll(execParam, search);
        return result;
    }
    private Result GetRagszam(string ragszam)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Filter(ragszam);

        Result result = service.GetAll(execParam, search);
        return result;
    }
    private Result GetRagszamSav(string id)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        return result;
    }
    private bool FillRagszamParameters(Result resultTerti)
    {
        if (resultTerti == null || resultTerti.Ds.Tables.Count < 1 || resultTerti.Ds.Tables[0].Rows.Count < 1)
            return false;

        System.Data.DataRow rowTerti = resultTerti.Ds.Tables[0].Rows[0];
        string KuldTertiId = rowTerti["Id"].ToString();
        string barcode = rowTerti["barCode"].ToString();
        string ragszam = rowTerti["ragSzam"].ToString();
        if (string.IsNullOrEmpty(ragszam))
            return false;

        Result resultRagszam = GetRagszam(ragszam);
        if (resultRagszam == null || resultRagszam.IsError || resultRagszam.Ds.Tables.Count < 1 || resultRagszam.Ds.Tables[0].Rows.Count < 1)
            return false;

        System.Data.DataRow rowRagszam = resultRagszam.Ds.Tables[0].Rows[0];
        string postakonyv = rowRagszam["Postakonyv_Id"].ToString();
        string ragszamsav = rowRagszam["RagszamSav_Id"].ToString();

        Result resultragSzamSav = GetRagszamSav(ragszamsav);
        if (resultragSzamSav == null || resultragSzamSav.IsError)
            return false;

        KRT_RagszamSavok itemRagsazmSav = (KRT_RagszamSavok)resultragSzamSav.Record;
        if (itemRagsazmSav == null)
            return false;

        string kuldFajta = itemRagsazmSav.SavType;

        PostazasiAdatokPanel1.FillFixRagSzamParameters(postakonyv, ragszam, barcode, kuldFajta);

        return true;
    }

    #region típusos metaadatok
    protected void FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(String id)
    {
        TipusosTargyszavakPanel_Kuldemenyek.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_Kuldemenyek.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_KuldKuldemenyek, "KuldesMod", new string[] { PostazasiAdatokPanel1.KuldesModja }, false
            , null, false, true, FormHeader1.ErrorPanel);

        TipusosTargyszavakPanel_Kuldemenyek.Visible = (otpTipusosTargyszavak_Kuldemenyek.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_Kuldemenyek.Count > 0)
            {
                otpTipusosTargyszavak_Kuldemenyek.SetDefaultValues();
                FormFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_Kuldemenyek.GetCheckErtekJavaScript() + FormFooter1.ImageButton_Save.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_Kuldemenyek.ReadOnly = true;
            otpTipusosTargyszavak_Kuldemenyek.ViewMode = true;
        }
    }

    Result Save_TipusosObjektumTargyszavai_Kuldemenyek(string kuldemenyId)
    {
        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpTipusosTargyszavak_Kuldemenyek.GetEREC_ObjektumTargyszavaiList(true);
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                        , EREC_ObjektumTargyszavaiList
                        , kuldemenyId
                        , null
                        , Constants.TableNames.EREC_KuldKuldemenyek
                        , "*"
                        , null
                        , false
                        , null
                        , false
                        , false
                        );
        }

        return result_ot;
    }

    #endregion

    #region kimenõ küldemény típusos metaadatok
    protected void FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(String id)
    {
        TipusosTargyszavakPanel_KimenoKuldemenyek.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_KimenoKuldemenyek.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_KuldKuldemenyek, "PostazasIranya", new string[] { KodTarak.POSTAZAS_IRANYA.Kimeno }, false
            , null, false, true, FormHeader1.ErrorPanel);
        TipusosTargyszavakPanel_KimenoKuldemenyek.Visible = (otpTipusosTargyszavak_KimenoKuldemenyek.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_KimenoKuldemenyek.Count > 0)
            {
                otpTipusosTargyszavak_KimenoKuldemenyek.SetDefaultValues();
                FormFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_KimenoKuldemenyek.GetCheckErtekJavaScript() + FormFooter1.ImageButton_Save.OnClientClick;
            }
        }

        //LZS - BLG_5341 - CommandName.KimenoKuldemenyFelvitel hozzáadása. Ebben az esetben is legyen szürke ez a rész.
        if (Command == CommandName.View || Command == CommandName.KimenoKuldemenyFelvitel)
        {
            //
            //    otpTipusosTargyszavak_KimenoKuldemenyek.ReadOnly = true;
            //    otpTipusosTargyszavak_KimenoKuldemenyek.ViewMode = true;
            //
        }
    }

    Result Save_TipusosObjektumTargyszavai_KimenoKuldemenyek(string kuldemenyId)
    {
        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpTipusosTargyszavak_KimenoKuldemenyek.GetEREC_ObjektumTargyszavaiList(true);
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                        , EREC_ObjektumTargyszavaiList
                        , kuldemenyId
                        , null
                        , Constants.TableNames.EREC_KuldKuldemenyek
                        , "*"
                        , null
                        , false
                        , null
                        , false
                        , false
                        );
        }

        return result_ot;
    }

    #endregion
}
