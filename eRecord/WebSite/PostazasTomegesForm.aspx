<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PostazasTomegesForm.aspx.cs"
    Inherits="PostazasTomegesForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<%@ Register Src="eRecordComponent/PostazasiAdatokPanel.ascx" TagName="PostazasiAdatokPanel" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" />

    <div>
        <asp:Panel ID="MainPanel" runat="server">

            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />

            <table cellpadding="0" cellspacing="0" width="90%">
                <tr>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <asp:Panel ID="PostazListPanel" runat="server" Visible="false">
                            <br />
                            <asp:Label ID="Label_Postaz" runat="server" Text="K�ldem�nyek:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />

                            <asp:GridView ID="PostazGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                OnRowDataBound="PostazGridView_RowDataBound"
                                AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" Checked="true"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:BoundField DataField="Azonosito" HeaderText="Azonosito" SortExpression="Azonosito">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>--%>
                                    <%--<asp:BoundField DataField="Csoport_Id_CimzettNev" HeaderText="C�mzett&nbsp;neve" SortExpression="Csoportok_CimzettNev.Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="NevSTR_Bekuldo" HeaderText="C�mzett" SortExpression="EREC_KuldKuldemenyek.NevSTR_Bekuldo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CimSTR_Bekuldo" HeaderText="C�mzett c�me" SortExpression="EREC_KuldKuldemenyek.CimSTR_Bekuldo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="KuldesModNev" HeaderText="K�ld�s m�dja" Visible="true" SortExpression="EREC_KuldKuldemenyek.KuldesMod">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RagSzam" HeaderText="Postai azonos�t�" Visible="true" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <%--   <asp:BoundField DataField="�llapotNev" HeaderText="�llapot" Visible="true" SortExpression="�llapotNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>--%>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>

                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                        <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc6:PostazasiAdatokPanel ID="PostazasiAdatokPanelTomeges" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Panel ID="Panel_Warning_Postaz" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Postaz" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_Kuldemeny" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Kuldemeny" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:FormFooter ID="FormFooter1" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upResult">
            <ContentTemplate>
                <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
                    <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
                    <asp:HiddenField ID="hfSelectedPostaz" runat="server" />
                    <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
                    <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
                    <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                        <div class="mrResultPanelText">A kijel�lt t�telek post�z�sa sikeresen v�grehajt�dott.</div>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                        onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                        CommandName="Close" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
