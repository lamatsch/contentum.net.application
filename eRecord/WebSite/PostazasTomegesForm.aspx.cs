using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PostazasTomegesForm : Contentum.eUtility.UI.PageBase
{
    #region PROPERTIES
    private UI ui = new UI();
    private string Command = "";

    //private string kuldemenyIds = null;
    private string[] kuldemenyIdsArray = null;

    private string SessionIratPldIds = "SelectedIratPldIds";
    private string SessionKuldemenyIds = "SelectedKuldemenyIds";

    private const string FunkcioKod_Postazas = "Postazas";

    public string maxTetelszam = "0";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ResultError.ResetErrorPanel(FormHeader1.ErrorPanel);
        this.FormHeader1.HeaderTitle = "Post�z�s";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_Postazas);

        SetSelectedIdsFromSession();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadFormComponents();

        FormHeader1.DisableModeLabel = true;
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(PostazGridView, FormHeader1.ErrorPanel, null).Count.ToString());
    }

    private void SetSelectedIdsFromSession()
    {
        if (Session[SessionIratPldIds] != null)
        {
            string selectedIratPldIds = Session[SessionIratPldIds].ToString();
            if (string.IsNullOrEmpty(selectedIratPldIds))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageErrorInData, Resources.Error.MessageNoSelectedItem);
                return;
            }
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch(true);
            search.Extended_EREC_PldIratPeldanyokSearch.Id.Value = selectedIratPldIds;
            search.Extended_EREC_PldIratPeldanyokSearch.Id.Operator = Query.Operators.inner;
            search.Allapot.Filter(KodTarak.KULDEMENY_ALLAPOT.Expedialt);
            Result result = service.KimenoKuldGetAllWithExtension(execParam, search);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            var ids = new List<string>();
            if (result.GetCount > 0)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    var kuldemenyId = row["Id"].ToString();
                    ids.Add(kuldemenyId);
                }
                if (ids.Count > 0)
                    kuldemenyIdsArray = ids.ToArray();
            }
        }
        else if (Session[SessionKuldemenyIds] != null)
        {
            string selectedKuldemenyIds = Session[SessionKuldemenyIds].ToString();
            if (string.IsNullOrEmpty(selectedKuldemenyIds))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageErrorInData, Resources.Error.MessageNoSelectedItem);
                return;
            }

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch(true);
            search.Id.Value = selectedKuldemenyIds;
            search.Id.Operator = Query.Operators.inner;
            search.Allapot.Filter(KodTarak.KULDEMENY_ALLAPOT.Expedialt);
            Result result = service.KimenoKuldGetAllWithExtension(execParam, search);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            List<string> ids = new List<string>();
            if (result.GetCount > 0)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    var kuldemenyId = row["Id"].ToString();
                    ids.Add(kuldemenyId);
                }
                if (ids.Count > 0)
                    kuldemenyIdsArray = ids.ToArray();
            }
        }
    }
    private void LoadFormComponents()
    {
        if (kuldemenyIdsArray != null && kuldemenyIdsArray.Length > 0)
        {
            PostazListPanel.Visible = true;
            Label_Postaz.Visible = true;

            // Post�z�si panel be�ll�t�sa, hogy t�megesk�nt kezelje + nem kell a vonalk�d-beolvas�s r�sz: (BUG#4759)
            PostazasiAdatokPanelTomeges.RogzitesTipus = eRecordComponent_PostazasiAdatokPanel.Mode.Multiple;
            PostazasiAdatokPanelTomeges.FillAndSetKuldesModja(GetKuldesMod());
            PostazasiAdatokPanelTomeges.SetComponentsForPostazas("", FormHeader1.ErrorPanel, null);
            PostazasiAdatokPanelTomeges.HideVonalkodRagszamPanel();

            FillPostazGridView();
        }
    }

    private void FillPostazGridView()
    {
        if (!CheckKuldemenyek(kuldemenyIdsArray, false))
        {
            FormFooter1.SaveEnabled = false;
            PostazasiAdatokPanelTomeges.Visible = false;
            return;
        }

        Result res = PostazGridViewBind();

        // ellen�rz�s:
        if (res.GetCount != kuldemenyIdsArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        #region HIBASAK SZAMA
        try
        {
            int osszesSzama = kuldemenyIdsArray == null ? 0 : kuldemenyIdsArray.Length;
            int jokSzama = res.GetCount;
            if (osszesSzama > jokSzama)
            {
                Label_Warning_Postaz.Text = string.Format(Resources.List.UI_Postaz_KijeloltekWarning, osszesSzama - jokSzama);
                Panel_Warning_Postaz.Visible = true;
            }
        }
        catch (Exception)
        {
        }
        #endregion
    }

    protected Result PostazGridViewBind()
    {
        if (kuldemenyIdsArray != null && kuldemenyIdsArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.Id.In(kuldemenyIdsArray);

            Result res = service.GetAllWithExtension(ExecParam, search);
            ui.GridViewFill(PostazGridView, res, "", FormHeader1.ErrorPanel, null);
            SetPostazasiAdatokpanel(res);

            return res;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }
    private void SetPostazasiAdatokpanel(Result res)
    {
        PostazasiAdatokPanelTomeges.SetRagszamReadOnly();
        if (res != null && res.GetCount > 0)
        {
            PostazasiAdatokPanelTomeges.Visible = true;
            PostazasiAdatokPanelTomeges.SetComponentsForPostazas(res.Ds.Tables[0].Rows[0]["Id"].ToString(), FormHeader1.ErrorPanel, null);
        }
        else
        {
            PostazasiAdatokPanelTomeges.Visible = false;
        }
    }
    protected void PostazGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }

        // postai azonos�t� ellen�rz�se
        var kuldemeny = GetKuldemenyFromRow(e);
        var messages = GetKimenoKuldemenyErrors(kuldemeny, false);
        SetKimenoKuldemenyErrors(e, messages);
    }

    private EREC_KuldKuldemenyek GetKuldemenyFromRow(GridViewRowEventArgs e)
    {
        if (e == null || e.Row == null || e.Row.RowType != DataControlRowType.DataRow)
        {
            return null;
        }

        var row = (e.Row.DataItem as DataRowView).Row;
        var kuldemeny = PostazasiAdatokPanelTomeges.GetBusinessObjectFromComponents();
        if (String.IsNullOrEmpty(kuldemeny.Ar))
        {
            kuldemeny.Ar = !row.Table.Columns.Contains("Ar") || String.IsNullOrEmpty(row["Ar"].ToString()) ? "0" : row["Ar"].ToString();
        }
        if (String.IsNullOrEmpty(kuldemeny.RagSzam) && row.Table.Columns.Contains("RagSzam"))
        {
            kuldemeny.RagSzam = row["RagSzam"].ToString();
        }
        return kuldemeny;
    }

    private void SetKimenoKuldemenyErrors(GridViewRowEventArgs e, List<string> messages)
    {
        if (e != null && e.Row != null && e.Row.RowType == DataControlRowType.DataRow && messages != null)
        { 
            var row = e.Row.DataItem as DataRowView;
            var statusz = Kuldemenyek.GetAllapotFromDataRowView(row);

            ErrorDetails errorDetail = null;
            var kuldemenyOk = messages.Count == 0;
            if (!kuldemenyOk)
            {
                errorDetail = new ErrorDetails(String.Join("<br/>", messages.ToArray()), Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, "", "");
            }
            UI.SetRowCheckboxAndInfo(kuldemenyOk, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, Page);
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        SaveProcedure(e);
    }

    private void SaveProcedure(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_Postazas))
            {
                EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanelTomeges.GetBusinessObjectFromComponents();

                // CR3150 C�m szegment�lts�g ellen�rz�s
                if (!CheckPartnerCim(kuldemeny))
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.ErrorCode_80413);
                    return;
                }
                //if (!CheckKimenoKuldemeny(kuldemeny, true))
                //{
                //    //ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.ErrorCode_80414);
                //    return;
                //}

                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                List<string> selectedItemsList = ui.GetGridViewSelectedRows(PostazGridView, FormHeader1.ErrorPanel, null);

                if (selectedItemsList.Count == 0)
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                    return;
                }
                string[] selectedKimenoKuldIds = selectedItemsList.ToArray();

                if (!CheckKuldemenyek(selectedKimenoKuldIds, true))
                {
                    return;
                }

                Result result = service.Postazas_Tomegesen(execParam.Clone(), kuldemeny, selectedKimenoKuldIds, PostazasiAdatokPanelTomeges.TertivevenyVonalkod);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridView(PostazGridView, result);
                }
                else
                {
                    MainPanel.Visible = false;
                    ResultPanel.Visible = true;
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    /// <summary>
    /// CheckKuldemenyek
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    private bool CheckKuldemenyek(string[] selectedKuldemenyIds, bool checkKimenoAr)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        ExecParam execParam_kuldGet = null;
        Result result_kuldGet = null;

        EREC_KuldKuldemenyekService service_Kuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        string kuldesMod = null;
        string postaKonyv = null;
        for (int i = 0; i < selectedKuldemenyIds.Length; i++)
        {
            var id = selectedKuldemenyIds[i];
            execParam_kuldGet = execParam.Clone();
            execParam_kuldGet.Record_Id = id;

            result_kuldGet = service_Kuld.Get(execParam_kuldGet);
            if (!String.IsNullOrEmpty(result_kuldGet.ErrorCode))
            {
                Logger.Error("CheckKuldemenyek: ", execParam.Clone(), result_kuldGet);
                return false;
            }
            EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)result_kuldGet.Record;

            // k�ld�s m�d
            if (string.IsNullOrEmpty(kuldesMod))
            {
                kuldesMod = kuld.KuldesMod;
            }
            else if (kuldesMod != kuld.KuldesMod)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, Resources.Error.ErrorCode_80412);
                Logger.Error("CheckKuldemenyek: K�ld�s m�d nem egyeznek.");
                return false;
            }

            // postak�nyvek
            if (string.IsNullOrEmpty(postaKonyv))
            {
                postaKonyv = kuld.IraIktatokonyv_Id;
            }
            else if (postaKonyv != kuld.IraIktatokonyv_Id)
            {
                Logger.Error("CheckKuldemenyek: Postak�nyvek nem egyeznek.");
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, Resources.Error.ErrorCode_80417);
                return false;
            }

            // kimen� k�ldem�ny
            //EREC_KuldKuldemenyek kuldemeny = PostazasiAdatokPanelTomeges.GetBusinessObjectFromComponents();
            //kuldemeny.Id = id; // hiba�zenethez kell
            //kuldemeny.RagSzam = kuld.RagSzam;
            //CheckKimenoKuldemeny(kuldemeny, checkKimenoAr);
        }
        return true;
    }

    // CR3150 C�m szegment�lts�g ellen�rz�s
    private bool CheckPartnerCim(EREC_KuldKuldemenyek kuldemeny)
    {
        // CR3284 : T�meges post�z�skor nem kell szegment�lts�g-ellen�rz�s
        if (PostazasiAdatokPanelTomeges.RogzitesTipus == eRecordComponent_PostazasiAdatokPanel.Mode.Multiple) return true;

        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.CIM_SZEGMENTALTSAG_ELLENORZES, false))
        {
            if (String.IsNullOrEmpty(kuldemeny.Cim_Id))
            {
                FormHeader1.ErrorPanel.Header = Resources.Error.ErrorLabel;
                FormHeader1.ErrorPanel.Body = Resources.Error.ErrorCode_52799; //A post�z�si c�m nem strukt�r�lt!
                FormHeader1.ErrorPanel.Visible = true;
                return false;
            }
        }
        return true;
    }
    private bool CheckKimenoKuldemeny(EREC_KuldKuldemenyek erec_KuldKuldemenyek, bool checkKimenoAr)
    {
        var messages = GetKimenoKuldemenyErrors(erec_KuldKuldemenyek, checkKimenoAr);
        var bOK = messages.Count == 0;
        if (!bOK && erec_KuldKuldemenyek != null)
        {
            var row = PostazGridView.Rows.Cast<GridViewRow>().FirstOrDefault(r => r.FindControl("check") is CheckBox && (r.FindControl("check") as CheckBox).Text.ToLower() == erec_KuldKuldemenyek.Id.ToLower());
            if (row != null)
            {
                var e = new GridViewRowEventArgs(row);
                SetKimenoKuldemenyErrors(e, messages);
            }
            else // Id alapj�n nem tal�lhat� a fenti list�ban, vagy az Id �res
            {
                var allMsg = String.Join(@"<br/>", messages.ToArray());
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, allMsg);
                //string js = String.Format(@"alert('{0}');", String.Join(@"\n", messages.ToArray()));
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "errorMsg", js, true);
            }

            FormHeader1.ErrorPanel.Focus();
        }

        if (bOK)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }

        return bOK;
    }

    private List<string> GetKimenoKuldemenyErrors(EREC_KuldKuldemenyek erec_KuldKuldemenyek, bool checkKimenoAr)
    {
        var messages = new List<string>();
        if (erec_KuldKuldemenyek != null)
        {
            var tertivevenyVonalkod = PostazasiAdatokPanelTomeges.TertivevenyVonalkod;
            if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    messages.Add("Aj�nlott k�ldem�nyn�l a ragsz�m megad�sa k�telez�!");
                }
            }
            if (erec_KuldKuldemenyek.Tertiveveny == "1")
            {
                if (erec_KuldKuldemenyek.Ajanlott != "1")
                {
                    messages.Add("T�rtivev�ny k�l�nszolg�ltat�s csak aj�nlott szolg�ltat�ssal egy�tt vehet� ig�nybe!");
                }

                bool isKulfoldi = erec_KuldKuldemenyek.KimenoKuldemenyFajta.StartsWith(Constants.OrszagViszonylatKod.Europai)
                    || erec_KuldKuldemenyek.KimenoKuldemenyFajta.StartsWith(Constants.OrszagViszonylatKod.EgyebKulfoldi);

                //k�lf�ldi k�ldem�nyekn�l nem kell a t�rtitev�ny vonalk�dja
                // CR 3088 :  BOPMH-n�l, CSBO-n�l nem k�telez� a t�rtivev�ny kit�lt�se (csak FPH-n�l)
                // ORG f�gg� megjelen�t�s
                // BLG_591
                //if ((!isKulfoldi) && (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH))
                if ((!isKulfoldi) && (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TERTIVONALKOD_KELL, false)))
                {
                    if (String.IsNullOrEmpty(tertivevenyVonalkod))
                    {
                        messages.Add("T�rtivev�nyes k�ldem�nyn�l a t�rtivev�ny vonalk�d megad�sa k�telez�!");
                    }
                }
            }

            if (erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat
                || erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat_sajat_kezbe)
            {
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    messages.Add("Hivatalos iratn�l a ragsz�m megad�sa k�telez�!");
                }
                // CR 3088 :  BOPMH-n�l, CSBO-n�l nem k�telez� a t�rtivev�ny kit�lt�se (csak FPH-n�l)
                // ORG f�gg� megjelen�t�s
                // BLG_591
                //if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH))
                if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TERTIVONALKOD_KELL, false)))
                {
                    messages.Add("Hivatalos iratn�l a t�rtivev�ny vonalk�d megad�sa k�telez�!");
                }
            }

            // modified by nekrisz: NaN �r ellen�rz�s
            if (checkKimenoAr) // oldalbet�lt�skor mindig �res lesz az �r, mivel a javascript �ll�tja majd be, ez�rt csak a SaveProcedure-b�l h�vva kell ellen�rizni
            {
                decimal d_ar;
                if (!decimal.TryParse(erec_KuldKuldemenyek.Ar, out d_ar))
                {
                    messages.Add("Az adott k�ldem�ny �ra nem meghat�rozhat�, konzult�ljon az alkalmaz�s gazd�val!");
                }
            }
        }
        return messages;
    }

    string GetKuldesMod()
    {
        if (kuldemenyIdsArray != null && kuldemenyIdsArray.Length > 0)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = kuldemenyIdsArray[0];
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

            Result res = service.Get(execParam);

            if (!res.IsError)
            {
                EREC_KuldKuldemenyek kuldemeny = res.Record as EREC_KuldKuldemenyek;
                return kuldemeny.KuldesMod;
            }
        }

        return String.Empty;
    }
}