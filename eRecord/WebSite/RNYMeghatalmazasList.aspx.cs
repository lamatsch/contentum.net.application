﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.Service.NAP.Results;
using Contentum.eIntegrator.Service.NAP;

public partial class RNYMeghatalmazasList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MeghatalmazasokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = "RNY meghatalmazások lekérdezése";
        //ListHeader1.SearchObjectType = typeof(EREC_MeghatalmazasokSearch);
        ListHeader1.CustomSearchObjectSessionName = "RNYMeghatalmazasok";
        // A baloldali, alapból invisible ikonok megjelenítése:
        ListHeader1.PrintVisible = false;// TODO: printform
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);


        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(MeghatalmazasokGridView.ClientID);
        ListHeader1.DefaultFilterVisible = false;

        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = MeghatalmazasokGridView;
        ui.SetClientScriptToGridViewSelectDeSelectButton(MeghatalmazasokGridView);

        if (!IsPostBack)
        {
            MeghatalmazasokGridViewEmptyBind();
        }

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!IsPostBack)
        {
            ddownSzereplo.Items.Add(new ListItem() { Text = "Meghatalmazott", Value = ((int)SzereploTipusok.Kit).ToString() });
            ddownSzereplo.Items.Add(new ListItem() { Text = "Képviselt", Value = ((int)SzereploTipusok.Kire).ToString() });

            foreach (MeghatalmazasTipusok meghatalmazasTipus in MeghatalmazasTipusokHelper.GetOsszesMeghatalmazasTipus())
            {
                ddownMeghatalmasTipusa.Items.Add(new ListItem()
                {
                    Text = RendelkezesTipusNevek.GetNev(meghatalmazasTipus),
                    Value = ((int)meghatalmazasTipus).ToString()
                });
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MeghatalmazasokGridView);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
    }


    #endregion

    #region Master List

    protected void MeghatalmazasokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MeghatalmazasokGridView", ViewState, "Kod");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MeghatalmazasokGridView", ViewState);

        MeghatalmazasokGridViewBind(sortExpression, sortDirection);
    }

    protected void MeghatalmazasokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(MeghatalmazasokGridView);

        INT_NAPService service = eIntegratorService.ServiceFactory.GetINT_NAPService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        Szemely4TAdatok szemelyAdatok = SzemelyAdatokPanel.Get4TAdatok();

        MeghatalmazasokRequest request = new MeghatalmazasokRequest();
        request.AzonositoAdat = new AzonositoAdat
        {
            SzemelyAdatok = szemelyAdatok
        };

        request.Szereplo = (SzereploTipusok)Enum.Parse(typeof(SzereploTipusok), ddownSzereplo.SelectedValue);
        request.Tipus = (MeghatalmazasTipusok)Enum.Parse(typeof(MeghatalmazasTipusok), ddownMeghatalmasTipusa.SelectedValue);

        MeghatalmazasokResult res = service.GetMeghatalmazasok(execParam, request);

        GridViewFill(MeghatalmazasokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void MeghatalmazasokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void MeghatalmazasokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = MeghatalmazasokGridView.PageIndex;

        MeghatalmazasokGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = MeghatalmazasokGridView.PageCount;

        if (prev_PageIndex != MeghatalmazasokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, MeghatalmazasokCPE);
            MeghatalmazasokGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, MeghatalmazasokCPE);
        }

        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        MeghatalmazasokGridViewBind();
    }

    protected void MeghatalmazasokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MeghatalmazasokGridView, selectedRowNumber, "check");
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {

        }
    }

    protected void MeghatalmazasokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    MeghatalmazasokGridViewBind();
                    break;
            }
        }

    }


    protected void MeghatalmazasokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MeghatalmazasokGridViewBind(e.SortExpression, UI.GetSortToGridView("MeghatalmazasokGridView", ViewState, e.SortExpression));
    }

    void GridViewFill(GridView ParentGridView, MeghatalmazasokResult result, Component_ListHeader ListHeader, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }
        else
        {
            string selectedRecordId = ListHeader1.SelectedRecordId;
            List<Meghatalmazas> meghatalmazasok = result.MeghatalmazasokList;

            if (meghatalmazasok != null)
            {
                ParentGridView.SelectedIndex = -1;
                ListHeader.RecordNumber = meghatalmazasok.Count;
                if (!String.IsNullOrEmpty(ListHeader.RowCount) && ListHeader.RowCount != "0")
                {
                    ParentGridView.PageSize = Int32.Parse(ListHeader.RowCount);
                }
                else
                {
                    ParentGridView.PageSize = meghatalmazasok.Count;
                }

                bool isEmpty = false;
                if (meghatalmazasok.Count == 0)
                {
                    isEmpty = true;
                    meghatalmazasok.Add(new Meghatalmazas());
                }

                ParentGridView.DataSource = meghatalmazasok;
                ParentGridView.DataBind();

                if (isEmpty)
                {
                    ParentGridView.Rows[0].Visible = false;
                }

                if (ParentGridView.SelectedIndex > -1)
                {
                    UI.SetGridViewCheckBoxesToSelectedRow(ParentGridView, ParentGridView.SelectedIndex, "check");
                }
            }
        }

    }

    #endregion

    protected void ImageSearch_Click(object sender, ImageClickEventArgs e)
    {
        MeghatalmazasokGridViewBind();
    }

    protected void MeghatalmazasokGridViewEmptyBind()
    {
        MeghatalmazasokResult result = new MeghatalmazasokResult();
        result.MeghatalmazasokList = new List<Meghatalmazas>();
        GridViewFill(MeghatalmazasokGridView, result, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }
}