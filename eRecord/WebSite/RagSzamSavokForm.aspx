<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="RagszamSavokForm.aspx.cs"
    Inherits="RagszamSavokForm" Title="ragsz�ms�vok" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="~/eRecordComponent/RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,RagszamSavokNewHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label0" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelFelelos" runat="server" Text="S�vtulajdonos:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="FelelosCsoportTextBox1" runat="server" ViewMode="true" Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="TRKovetkezoCsoportFelelos" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelKovetkezoCsoportFelelos0" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelKovetkezoCsoportFelelos1" runat="server" Text="K�vetkez� csoport felel�s:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="FelelosCsoportTextBox2" runat="server" Validate="false" />
                        </td>
                    </tr>

                    <tr class="urlapSor" id="TRSavKezdete" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelSavKezdete1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelSavKezdete2" runat="server" Text="S�v kezdete:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">                            
                            <uc5:RagszamTextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" />
                        </td>
                    </tr>

                    <%--START: RAGSZ�M IG�NYL�S--%>
                    <tr class="urlapSor" id="TRIgenylendoDarab" runat="server" visible="true">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelIgenylendoDarab" runat="server" Text="Ig�nylend� darab:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc9:RequiredNumberBox ID="Igenylendo_Darab_RequiredNumberBox" runat="server" Validate="true" />
                        </td>
                    </tr>
                    <%--STOP: RAGSZ�M IG�NYL�S--%>

                    <%--<tr class="urlapSor" id="TRSavVege" runat="server" visible="false">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text=""></asp:Label>
                            <asp:Label ID="labelSavVege" runat="server" Text="S�v v�ge:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc8:VonalKodTextBox ID="SavVege" runat="server" ReadOnly="true" Validate="false" />
                        </td>
                    </tr>--%>

                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label runat="server" CssClass="ReqStar" Text="*" />
                            <asp:Label runat="server" Text="Postak�nyv" />
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="DropDownListPostakonyv" runat="server" Width="255px"/>
                        </td>
                    </tr>


                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelSavAllapot" runat="server" Text="S�v �llapot:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="ddownSavAllapot" runat="server" Width="255px">
                                <asp:ListItem Text="Allok�lt" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Felhaszn�lt" Value="F"></asp:ListItem>
                                <asp:ListItem Text="Haszn�lhat�" Value="H"></asp:ListItem>
                                <asp:ListItem Text="T�r�lt" Value="T"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                      <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="label4" runat="server" Text="K�ldem�ny fajta:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="DropDownListKuldemenyFajta" runat="server" Width="255px">
                                <%--<asp:ListItem Text="Aj�nlott k�ldem�ny" Value="40"/>
                                <asp:ListItem Text="�rt�knyilv�n�t�sos lev�lk�ldem�ny" Value="41"/>
                                <asp:ListItem Text="Hivatalos irat" Value="42"/>
                                <asp:ListItem Text="�rt�knyilv�n�t�sos hivatalos irat" Value="43"/>
                                <asp:ListItem Text="�C�mzett kez�be� lev�l" Value="44"/>--%>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="Ervenyesseg_tr">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" Enabled="false" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
