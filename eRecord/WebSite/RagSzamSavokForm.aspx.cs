using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;

public partial class RagszamSavokForm : Contentum.eUtility.UI.PageBase
{
    private const string RagszamSavTitleMain = "Ragsz�m s�v";
    private const string RagszamSavTitleRagszam = "Ragsz�m";
    private const string RagszamSavTitleNew = "L�trehoz�s";
    private const string RagszamSavTitleView = "Megtekint�s";
    private const string RagszamSavTitleModify = "M�dos�t�s";
    private const string RagszamSavTitleNeed = "Ig�nyl�s";

    private const string RagszamSavMessageNewOnlyAllocated = "Csak 'Allok�lt' ragsz�ms�vb�l lehet ragsz�mokat ig�nyelni";
    private const string RagszamSavMessageInvalidFormat = "Nem megfelel� ragsz�m form�tum";
    private const string RagszamSavMessageInvalidCount = "Nem megfelel� darabsz�m �rt�k!";

    private const string ConstFoglalasAllapotaHasznalhato = "H";
    private const string ConstFoglalasAllapotaAllokalt = "A";

    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        FelelosCsoportTextBox1.Enabled = false;
        Ragszam_TextBox.ViewEnabled = false;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                ErvenyessegCalendarControl1.Enabled = true;
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KRT_RagszamSavokList");
                break;
        }
        FormHeader1.HeaderTitle = RagszamSavTitleMain;
        fillPostakonyvek();
        FillKuldemenyFajta();
        String id = Request.QueryString.Get(QueryStringVars.Id);

        if (Command == CommandName.New)
        {
            #region NEW
            FormHeader1.ModeText = RagszamSavTitleNew;
            FelelosCsoportTextBox1.Enabled = false;
            TRKovetkezoCsoportFelelos.Visible = false;
            TRSavKezdete.Visible = true;
            //BLG 2166
            //TRSavVege.Visible = true;
            TRIgenylendoDarab.Visible = true;
            ddownSavAllapot.SelectedValue = "A";
            ddownSavAllapot.Enabled = true;
            ddownSavAllapot.Items.RemoveAt(1);
            ddownSavAllapot.Items.RemoveAt(1);
            ddownSavAllapot.Items.RemoveAt(1);
            DropDownListPostakonyv.Enabled = true;
            DropDownListKuldemenyFajta.Enabled = true;
            #endregion
        }
        else if (Command == CommandName.View)
        {
            registerJavascripts();
            #region VIEW
            FormHeader1.ModeText = RagszamSavTitleView;
            TRKovetkezoCsoportFelelos.Visible = false;
            FelelosCsoportTextBox1.Enabled = false;
            Ragszam_TextBox.ReadOnly = true;
            Ragszam_TextBox.ReadOnly = true;
            //BLG 2166
            Igenylendo_Darab_RequiredNumberBox.ReadOnly = true;
            ddownSavAllapot.Enabled = false;
            DropDownListPostakonyv.Enabled = false;
            DropDownListKuldemenyFajta.Enabled = false;
            #endregion

            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            KRT_RagszamSavok item = Get(id);
            FillComponentsFromService(item);
        }
        else if (Command == CommandName.Modify)
        {
            registerJavascripts();

            FormHeader1.ModeText = RagszamSavTitleModify;
            FelelosCsoportTextBox1.Enabled = false;
            Igenylendo_Darab_RequiredNumberBox.ReadOnly = true;
            Ragszam_TextBox.ReadOnly = true;
            Ragszam_TextBox.ReadOnly = true;
            ddownSavAllapot.Enabled = false;
            DropDownListPostakonyv.Enabled = false;
            DropDownListKuldemenyFajta.Enabled = false;

            TRKovetkezoCsoportFelelos.Visible = false;
            FelelosCsoportTextBox2.Visible = false;
            FelelosCsoportTextBox2.Validate = false;

            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }

            KRT_RagszamSavok item = Get(id);
            FillComponentsFromService(item);

            if (item.SavAllapot == "H")
            {
                Result ragszamokResult = GetAllRagszam(id);

                if (ragszamokResult.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, ragszamokResult);
                    return;
                }

                if (ragszamokResult.Ds.Tables[0].Rows.Count > 0)
                {
                    var rows = ragszamokResult.Ds.Tables[0].AsEnumerable();

                    if (!rows.Any(x => x["Allapot"] != DBNull.Value && (x["Allapot"].ToString() == "A" || x["Allapot"].ToString() == "F")))
                    {
                        DropDownListPostakonyv.Enabled = true;
                        DropDownListKuldemenyFajta.Enabled = true;
                    }
                }
            }
        }
        else if (Command == CommandName.RagszamIgenyles)
        {
            #region IGENYLES
            FormHeader1.ModeText = RagszamSavTitleNeed;
            TRKovetkezoCsoportFelelos.Visible = false;
            FelelosCsoportTextBox1.Enabled = false;
            Ragszam_TextBox.ReadOnly = true;
            Ragszam_TextBox.ReadOnly = true;
            ddownSavAllapot.Enabled = false;
            DropDownListPostakonyv.Enabled = false;
            DropDownListKuldemenyFajta.Enabled = false;
            Igenylendo_Darab_RequiredNumberBox.ReadOnly = true;
            #endregion

            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            KRT_RagszamSavok item = Get(id);
            if (item.SavAllapot != "A")
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageNewOnlyAllocated);
            else
                FillComponentsFromService(item);
        }

    }

    private void FillComponentsFromService(KRT_RagszamSavok item)
    {
        if (item == null)
            return;

        LoadComponentsFromBusinessObject(item);
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        Ragszam_TextBox.OnServerValidate += new ServerValidateEventHandler(CheckRagszam);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            if (Command == CommandName.New)
            {
                FormHeader1.FullManualHeaderTitle = RagszamSavTitleMain + " - " + RagszamSavTitleNew;
                // felel�s defaultb�l be�ll�tva a saj�t csoportra:
                FelelosCsoportTextBox1.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
                    FelhasznaloProfil.FelhasznaloId(Page));
                FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
            if (Command == CommandName.View)
            {
                FormHeader1.FullManualHeaderTitle = RagszamSavTitleMain + " - " + RagszamSavTitleView;
                FormFooter1.Command = CommandName.New;
            }
            if (Command == CommandName.Modify)
            {
                FormHeader1.FullManualHeaderTitle = RagszamSavTitleMain + " - " + RagszamSavTitleModify;
            }
            if (Command == CommandName.RagszamIgenyles)
            {
                FormHeader1.FullManualHeaderTitle = RagszamSavTitleRagszam + " - " + RagszamSavTitleNeed;
                FormFooter1.Command = CommandName.New;
                FormFooter1.SaveEnabled = true;

            }
            pageView.SetViewOnPage(Command);
        }
    }

    protected void CheckRagszam(object source, ServerValidateEventArgs args)
    {
        string errormsg = null;

        bool isValid = args.IsValid;
        if (isValid)
        {
            bool isUnique = true;
            if (!String.IsNullOrEmpty(Ragszam_TextBox.Text))
            {
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

                search.RagSzam.Value = Ragszam_TextBox.Text;
                search.RagSzam.Operator = Query.Operators.equals;

                //if (!String.IsNullOrEmpty(KuldemenyId_HiddenField.Value))
                //{
                //    search.Id.Value = KuldemenyId_HiddenField.Value;
                //    search.Id.Operator = Query.Operators.notequals;
                //}

                search.TopRow = 1;

                Result result = service.GetAll(execParam, search);
                if (!result.IsError)
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        isUnique = false;
                    }
                }
            }

            if (!isUnique)
            {
                errormsg = Resources.Form.CustomValidatorNonUniqueValueMessage;
                if (source is BaseValidator)
                {
                    ((BaseValidator)source).ErrorMessage = errormsg;
                }
            }

            isValid = isUnique;
        }
        args.IsValid = isValid;
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_KodTarak"></param>
    private void LoadComponentsFromBusinessObject(KRT_RagszamSavok krt_RagszamSavok)
    {
        FelelosCsoportTextBox1.Id_HiddenField = krt_RagszamSavok.Csoport_Id_Felelos;
        FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Igenylendo_Darab_RequiredNumberBox.Text = krt_RagszamSavok.IgenyeltDarabszam;
        Ragszam_TextBox.TextBox.Text = krt_RagszamSavok.SavKezd;
        //Ragszam_TextBox.TextBox.Text = krt_RagszamSavok.SavVege;

        ddownSavAllapot.SelectedValue = krt_RagszamSavok.SavAllapot;
        DropDownListKuldemenyFajta.SelectedValue = krt_RagszamSavok.SavType;
        ErvenyessegCalendarControl1.ErvKezd = krt_RagszamSavok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_RagszamSavok.ErvVege;

        FormHeader1.Record_Ver = krt_RagszamSavok.Base.Ver;
        FormPart_CreatedModified1.SetComponentValues(krt_RagszamSavok.Base);

        DropDownListPostakonyv.SelectedValue = krt_RagszamSavok.Postakonyv_Id;
    }

    private string getSavVegeData(string darabszamStr, KRT_RagszamSavok krt_RagszamSavok)
    {
        #region LETREHOZAS
        string retVal = string.Empty;

        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        int darab;
        if (int.TryParse(darabszamStr, out darab))
        {
            if (darab < 1)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageInvalidCount);
                return "";
            }

            long? savKezdNum = GetRagszamNumber(krt_RagszamSavok.SavKezd);

            Result result = service.RagszamSav_Igenyles_WithoutInsert(
                execParam,
                Guid.NewGuid(),
                krt_RagszamSavok.SavKezd,
                savKezdNum.Value,
                new Guid(krt_RagszamSavok.Postakonyv_Id),
                darab,
                ConstFoglalasAllapotaHasznalhato
                );

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return "";
            }

            if (result.GetCount > 0)
            {
                retVal = result.Ds.Tables[0].Rows[0]["I_Kod"].ToString();
            }
        }
        return retVal;
        #endregion
    }

    private string getSavVegeData(string savkezdeteStr, string darabszamStr)
    {
        string retvalue = string.Empty;
        try
        {
            long? savKezdNum = GetRagszamNumber(savkezdeteStr);
            if (savKezdNum != null && savKezdNum > long.MinValue)
            {
                int darabszam = 0;
                if (int.TryParse(darabszamStr, out darabszam))
                {
                    //mivel az utols� sz�mjegy checksum, ez�rt azt levessz�k a darabsz�m sz�m�t�shoz, majd egy '9' -est tesz�nk a v�geredm�ny ut�n
                    long savVeg = (savKezdNum.Value / 10 + darabszam) * 10 + 9;
                    //hozz�adjuk a prefixhez, �s mehet vissza
                    return savkezdeteStr.Replace(savKezdNum.ToString(), savVeg.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("getSavVegeData", ex);
        }

        return retvalue;
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_RagszamSavok GetBusinessObjectFromComponents()
    {
        KRT_RagszamSavok krt_RagszamSavok = new KRT_RagszamSavok();
        krt_RagszamSavok.Updated.SetValueAll(false);
        krt_RagszamSavok.Base.Updated.SetValueAll(false);
        krt_RagszamSavok.Csoport_Id_Felelos = FelelosCsoportTextBox1.Id_HiddenField;
        krt_RagszamSavok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(FelelosCsoportTextBox1.TextBox);

        //if (Command == CommandName.View)
        //{
        //    krt_RagszamSavok.SavKezd = SavKezdete.TextBox.Text;
        //    krt_RagszamSavok.Updated.SavKezd = pageView.GetUpdatedByView(SavKezdete.TextBox);

        //    krt_RagszamSavok.SavVege = SavVege.TextBox.Text;
        //    krt_RagszamSavok.Updated.SavVege = pageView.GetUpdatedByView(SavVege.TextBox);
        //}
        //else
        //{
        //    krt_RagszamSavok.SavKezd = SavKezdete.TextBox.Text;
        //    krt_RagszamSavok.Updated.SavKezd = pageView.GetUpdatedByView(SavKezdete.TextBox);

        //    krt_RagszamSavok.SavVege = SavVege.TextBox.Text;
        //    krt_RagszamSavok.Updated.SavVege = pageView.GetUpdatedByView(SavVege.TextBox);
        //}
        krt_RagszamSavok.SavKezd = Ragszam_TextBox.TextBox.Text;
        krt_RagszamSavok.Updated.SavKezd = pageView.GetUpdatedByView(Ragszam_TextBox.TextBox);

        krt_RagszamSavok.IgenyeltDarabszam = Igenylendo_Darab_RequiredNumberBox.TextBox.Text;
        krt_RagszamSavok.Updated.IgenyeltDarabszam = pageView.GetUpdatedByView(Igenylendo_Darab_RequiredNumberBox.TextBox);

        krt_RagszamSavok.Postakonyv_Id = DropDownListPostakonyv.SelectedValue;
        krt_RagszamSavok.Updated.Postakonyv_Id = pageView.GetUpdatedByView(DropDownListPostakonyv);

        krt_RagszamSavok.SavType = DropDownListKuldemenyFajta.SelectedValue;
        krt_RagszamSavok.Updated.SavType = pageView.GetUpdatedByView(DropDownListKuldemenyFajta);

        krt_RagszamSavok.SavAllapot = ddownSavAllapot.SelectedValue;
        krt_RagszamSavok.Updated.SavAllapot = pageView.GetUpdatedByView(ddownSavAllapot);

        if (Command == CommandName.New)
        {
            //BLG2166 s�v v�g�t sz�m�tjuk
            string savVege = getSavVegeData(Igenylendo_Darab_RequiredNumberBox.TextBox.Text, krt_RagszamSavok);
            //krt_RagszamSavok.SavVege = getSavVegeData(Ragszam_TextBox.TextBox.Text, Igenylendo_Darab_RequiredNumberBox.TextBox.Text);
            krt_RagszamSavok.SavVege = savVege;
            krt_RagszamSavok.Updated.SavVege = pageView.GetUpdatedByView(Ragszam_TextBox.TextBox) || pageView.GetUpdatedByView(Igenylendo_Darab_RequiredNumberBox.TextBox);

        }
        else if (Command==CommandName.Modify)
        {
            String recordId = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(recordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = recordId;
            KRT_RagszamSavok r = (KRT_RagszamSavok)service.Get(execParam).Record;
            krt_RagszamSavok.SavVege = r.SavVege;
            krt_RagszamSavok.SavVegeNum = r.SavVegeNum;
        }
        krt_RagszamSavok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        krt_RagszamSavok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        krt_RagszamSavok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        krt_RagszamSavok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_RagszamSavok.Base.Ver = FormHeader1.Record_Ver;
        krt_RagszamSavok.Base.Updated.Ver = true;

        return krt_RagszamSavok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            //compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            switch (Command)
            {
                case CommandName.New:
                    {
                        KRT_RagszamSavok krt_RagszamSavok = GetBusinessObjectFromComponents();
                        long? savKezdNum = GetRagszamNumber(krt_RagszamSavok.SavKezd);
                        float? igenyeltDarabszam = GetRagszamInt(krt_RagszamSavok.IgenyeltDarabszam);
                        if (!savKezdNum.HasValue || !igenyeltDarabszam.HasValue)
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageInvalidFormat);
                            return;
                        }

                        if (!Insert_RagszamSav(krt_RagszamSavok))
                            return;
                        break;
                    }
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            KRT_RagszamSavok krt_RagszamSavok = GetBusinessObjectFromComponents();
                            krt_RagszamSavok.Id = recordId;
                            Update_RagszamSav(krt_RagszamSavok.Id, krt_RagszamSavok);
                        }
                        break;
                    }
                case CommandName.RagszamIgenyles:
                    {
                        int count;
                        if (int.TryParse(Igenylendo_Darab_RequiredNumberBox.Text, out count))
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_RagszamSavok krt_RagszamSavok = GetBusinessObjectFromComponents();
                                krt_RagszamSavok.Id = recordId;
                                Igeyles_RagszamSav(count, krt_RagszamSavok);
                            }
                        }
                        else
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageInvalidCount);
                        }
                        break;
                    }
            }
        }
    }

    private void registerJavascripts()
    {
    }
    #region FILL
    private void fillPostakonyvek()
    {
        DataRowCollection items = LoadPostakonyvek();
        if (items == null)
            return;

        ListItem li = null;
        foreach (DataRow item in items)
        {
            li = new ListItem();
            li.Text = item["Nev"].ToString();
            li.Value = item["Id"].ToString();
            DropDownListPostakonyv.Items.Add(li);
        }
    }
    private void FillKuldemenyFajta()
    {
        Dictionary<string, string> items = LoadPostaiKimenoKuldemenyFajta();
        if (items == null)
            return;

        ListItem li = null;
        foreach (KeyValuePair<string, string> item in items)
        {
            li = new ListItem();
            li.Text = item.Value;
            li.Value = item.Key;
            DropDownListKuldemenyFajta.Items.Add(li);
        }
    }
    #endregion

    #region SERVICE
    private DataRowCollection LoadPostakonyvek()
    {
        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
        Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
        Search.IktatoErkezteto.Value = Constants.IktatoErkezteto.Postakonyv;
        Result erec_IraIktatoKonyvek_result = service.GetAllWithIktathat(execParam, Search);
        if (erec_IraIktatoKonyvek_result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, erec_IraIktatoKonyvek_result);
            return null;
        }
        if (erec_IraIktatoKonyvek_result.Ds.Tables.Count < 1 || erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count < 1)
            return null;

        return erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows;
    }
    private Dictionary<string, string> LoadPostaiKimenoKuldemenyFajta()
    {
        Dictionary<string, string> allElements = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.kcsNev, Page);
        if (allElements == null || allElements.Count < 1)
            return null;

        Dictionary<string, string> filteredElements = new Dictionary<string, string>();
        foreach (KeyValuePair<string, string> item in allElements)
        {
            if (item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Ajanlott_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Cimzett_Kezebe_Level
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Hivatalos_Irat
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Level_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Hivatalos_Irat)
            {
                filteredElements.Add(item.Key, item.Value);
            }
        }
        return filteredElements;
    }
    private KRT_RagszamSavok Get(string id)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        Result result = service.Get(execParam);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            KRT_RagszamSavok krt_RagszamSavok = (KRT_RagszamSavok)result.Record;
            return krt_RagszamSavok;
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return null;
        }
    }

    private bool Update_RagszamSav(string recordId, KRT_RagszamSavok krt_RagszamSavok)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = recordId;
        float? savKezdNum = GetRagszamNumber(krt_RagszamSavok.SavKezd);
        float? savVegNun = GetRagszamNumber(krt_RagszamSavok.SavVege);
        float? igenyeltDarabszam = GetRagszamInt(krt_RagszamSavok.IgenyeltDarabszam);        

        Result result = service.RagszamSav_Modositas(
                            execParam,
                            new Guid(krt_RagszamSavok.Id),
                            new Guid(krt_RagszamSavok.Postakonyv_Id),
                            krt_RagszamSavok.SavKezd,
                            krt_RagszamSavok.SavVege,
                            savKezdNum.Value,
                            savVegNun.Value,
                            igenyeltDarabszam.Value,
                            krt_RagszamSavok.Csoport_Id_Felelos,
                            krt_RagszamSavok.SavAllapot,
                            krt_RagszamSavok.SavType
                            );

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
            return true;
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
    }
    private bool Insert_RagszamSav(KRT_RagszamSavok krt_RagszamSavok)
    {
        #region LETREHOZAS
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        long? savKezdNum = GetRagszamNumber(krt_RagszamSavok.SavKezd);
        long? savVegNun = GetRagszamNumber(krt_RagszamSavok.SavVege);
        float? igenyeltDarabszam = GetRagszamInt(krt_RagszamSavok.IgenyeltDarabszam);
        if (!savKezdNum.HasValue || !savVegNun.HasValue)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageInvalidFormat);
            return false;
        }

        Result result = service.RagszamSav_Letrehozas(
            execParam,
            Guid.NewGuid(),
            new Guid(krt_RagszamSavok.Postakonyv_Id),
            krt_RagszamSavok.SavKezd,
            krt_RagszamSavok.SavVege,
            savKezdNum.Value,
            savVegNun.Value,
            igenyeltDarabszam.Value,
            DropDownListKuldemenyFajta.SelectedValue
            );

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
        #endregion
    }



    private bool Igeyles_RagszamSav(int darab, KRT_RagszamSavok krt_RagszamSavok)
    {
        #region LETREHOZAS
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = krt_RagszamSavok.Id;
        if (darab < 1)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", RagszamSavMessageInvalidCount);
            return false;
        }
        service.Timeout = 600000;

        Result result = service.RagszamSav_Igenyles(
            execParam,
            new Guid(krt_RagszamSavok.Id),
            new Guid(krt_RagszamSavok.Postakonyv_Id),
            darab,
            ConstFoglalasAllapotaHasznalhato
            );

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
        #endregion
    }

    private Result GetAllRagszam(string ragszamSavId)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();

        search.RagszamSav_Id.Value = ragszamSavId;
        search.RagszamSav_Id.Operator = Query.Operators.equals;

        Result res = service.GetAll(ExecParam, search);

        return res;
    }

    private Result GetAllRagszamsavAllokalt(string postakonyvId, string exceptId)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Id.Value = exceptId;

        search.Postakonyv_Id.Value = postakonyvId;
        search.Postakonyv_Id.Operator = Query.Operators.and;
        search.SavAllapot.Value = "A";
        search.SavAllapot.Operator = Query.Operators.and;
        Result result = service.GetAll(execParam, search);
        return result;
    }
    #endregion

    #region HELPER
    public long? GetRagszamNumber(string value)
    {
        int index = int.MinValue;
        for (int i = 0; i < value.Length; i++)
        {
            byte val;
            if (byte.TryParse(value[i].ToString(), out val))
            {
                index = i;
                break;
            }
        }
        if (index != int.MinValue)
        {
            string numString = value.Substring(index);
            long ret = long.MinValue;
            if (long.TryParse(numString, out ret))
            {
                return ret;
            }
        }
        return null;
    }

    public float? GetRagszamInt(string value)
    {
        float ret = float.MinValue;
        if (float.TryParse(value, out ret))
        {
            return ret;
        }
        return null;
    }

    #endregion
}
