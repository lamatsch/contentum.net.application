<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RagszamSavokList.aspx.cs" Inherits="RagszamSavokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <%--HiddenFields--%>
    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeRagszamSavok" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" TabIndex="0">
                        <HeaderTemplate>
                            <asp:Label ID="label1" runat="server" Text="Ragsz�ms�vok"></asp:Label>
                        </HeaderTemplate>
                        <ContentTemplate>

                            <asp:UpdatePanel ID="updatePanelRagszamSavok"
                                runat="server"
                                OnLoad="updatePanelRagszamSavok_Load">
                                <ContentTemplate>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeRagszamSavok"
                                        runat="server"
                                        TargetControlID="panelRagszamSavok"
                                        CollapsedSize="20"
                                        Collapsed="False"
                                        ExpandControlID="btnCpeRagszamSavok"
                                        CollapseControlID="btnCpeRagszamSavok"
                                        ExpandDirection="Vertical"
                                        AutoCollapse="false"
                                        AutoExpand="false"
                                        CollapsedImage="images/hu/Grid/plus.gif"
                                        ExpandedImage="images/hu/Grid/minus.gif"
                                        ImageControlID="btnCpeRagszamSavok"
                                        ExpandedSize="0"
                                        ExpandedText="Vonalk�ds�vok list�ja"
                                        CollapsedText="Vonalk�ds�vok list�ja">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                    <asp:Panel ID="panelRagszamSavok" runat="server" Width="100%">
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                    <asp:GridView ID="gridViewRagszamSavok"
                                                        runat="server"
                                                        GridLines="None"
                                                        BorderWidth="1px"
                                                        AllowSorting="True"
                                                        PagerSettings-Visible="false"
                                                        CellPadding="0"
                                                        DataKeyNames="Id"
                                                        AutoGenerateColumns="False"
                                                        AllowPaging="true"
                                                        OnRowCommand="gridViewRagszamSavok_RowCommand"
                                                        OnPreRender="gridViewRagszamSavok_PreRender"
                                                        OnRowDataBound="gridViewRagszamSavok_RowDataBound"
                                                        OnSorting="gridViewRagszamSavok_Sorting">
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                    &nbsp;&nbsp;
														<asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                <HeaderStyle Width="25px" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                            <asp:BoundField DataField="Csoport_Felelos" HeaderText="Csoport felel�s" SortExpression="Csoport_Felelos">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Postakonyv_Nev" HeaderText="Postak�nyv" SortExpression="Postakonyv_Nev">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="SzervezetId" runat="server" CssClass="ReqStar" Text='<%# Eval("Csoport_Id_Felelos") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="SavKezd" HeaderText="S�v kezdete" SortExpression="SavKezd">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SavVege" HeaderText="S�v v�ge" SortExpression="SavVege">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IgenyeltDarab" HeaderText="Darabsz�m" SortExpression="IgenyeltDarab">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="75px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FelhasznaltDarab" HeaderText="Felhaszn�lt" SortExpression="FelhasznaltDarab">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="75px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <%--   <asp:BoundField DataField="SavTypeName" HeaderText="S�v t�pus" SortExpression="SavTypeName">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="90px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                            <asp:BoundField DataField="SavAllapotName" HeaderText="S�v �llapot" SortExpression="SavAllapotName">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="90px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SavTypeName" HeaderText="K�ldem�ny fajta" SortExpression="SavTypeName">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="90px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Labelx" runat="server" CssClass="ReqStar" Text='<%# Eval("SavAllapot") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <PagerSettings Visible="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <br />
                <ajaxToolkit:TabContainer ID="TabContainerDetail" runat="server" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanelPartner" runat="server" TabIndex="0">
                        <HeaderTemplate>
                            <asp:Label ID="labelPartnerHeader" runat="server" Text="Ragsz�mS�vhoz tartoz� ragsz�mok"></asp:Label>
                        </HeaderTemplate>
                        <ContentTemplate>

                            <asp:UpdatePanel ID="RagszamokUpdatePanel" runat="server" OnLoad="ragszamokUpdatePanel_Load">
                                <ContentTemplate>
                                    <asp:Panel ID="ragszamokPanel" runat="server" Visible="false" Width="100%">
                                        <uc1:SubListHeader ID="RagszamokSubListHeader" runat="server" InvalidateEnabled="false" ModifyEnabled="false"
                                            InvalidateVisible="false" ModifyVisible="false" NewEnabled="false" NewVisible="false" ViewEnabled="false" ViewVisible="false" ValidFilterVisible="false" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="RagszamokCPE" runat="server" TargetControlID="panelRagszamokList"
                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                                        AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                    <asp:Panel ID="panelragszamokList" runat="server">
                                                        <asp:GridView ID="RagszamokGridView" runat="server"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            AutoGenerateColumns="false" OnSorting="RagszamokGridView_Sorting" OnPreRender="RagszamokGridView_PreRender"
                                                            OnRowCommand="RagszamokGridView_RowCommand" DataKeyNames="Id" OnRowDataBound="RagszamokGridView_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                        &nbsp;&nbsp;
                                                                             <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="Kod">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="AllapotName" HeaderText="�llapot" SortExpression="AllapotName">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>




            </td>
        </tr>
    </table>

</asp:Content>
