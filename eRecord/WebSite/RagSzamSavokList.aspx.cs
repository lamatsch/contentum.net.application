using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class RagszamSavokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    readonly string RagszamSavListJavaScriptMessageNemAllokaltRagszamsav = "alert('Csak Allok�lt Ragsz�mS�vb�l lehet ig�nyelni ragsz�mot !');return false;";
    readonly string RagszamSavListJavaScriptMessageNincsKivalasztottElem = "alert('Nincs kiv�lasztott elem !');return false;";
    readonly string RagszamSavListJavaScriptMessageAlertError = "alert('Hiba'); return false;";
    readonly string RagszamSavListJavaScriptMessageAlertNoRight = "alert('Nem jogosult'); return false;";
    readonly string RagszamSavListJavaScriptMessageFelhasznaloNemJogosult = "alert('�n nem jogosult a m�velet v�grehajt�s�ra!\\n\\nA m�veletet csak a Ragsz�ms�v�rt felel�s szervezet tagjai, vagy ha a felel�s egy szem�ly, ezen szem�ly szervezet�nek tagjai hajthatj�k v�gre.');return false;";

    readonly string RagszamSavListTooltipNew = "Ragsz�m s�v l�trehoz�s";
    readonly string RagszamSavListTooltipView = "Ragsz�m s�v megtekint�s";
    readonly string RagszamSavListTooltipModify = "Ragsz�m m�dos�t�s";
    readonly string RagszamSavListTooltipExport = "A kiv�lasztott s�v ragsz�mjainak export�l�sa";
    readonly string RagszamSavListTooltipAdd = "Ragsz�m ig�nyl�s";

    #region Utils
    static class BoolString
    {
        public const string YesOld = "I";
        public const string Yes = "1";
        public const string NoOld = "N";
        public const string No = "0";
    }

    //Az I/N �rt�k �tkonvert�l�sa checkbox �llapotaira
    protected void SetCheckBox(CheckBox cb, string value)
    {
        if (value == BoolString.Yes || value == BoolString.YesOld)
        {
            cb.Checked = true;
        }
        else
        {
            cb.Checked = false;
        }
    }

    protected string getSavAllapot(GridView gridView)
    {
        return getSavAllapot(gridView, -1);
    }

    protected string getSavAllapot(GridView gridView, int rowIndex)
    {
        string SavAllapot = String.Empty;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            Label x = (Label)selectedRow.FindControl("Labelx");
            if (x != null)
            {
                SavAllapot = x.Text;
            }
        }
        return SavAllapot;
    }

    protected string getSavFelelos(GridView gridView)
    {
        return getSavFelelos(gridView, -1);
    }

    protected string getSavFelelos(GridView gridView, int rowIndex)
    {
        string SavFelelos = String.Empty;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            Label x = (Label)selectedRow.FindControl("SzervezetId");
            if (x != null)
            {
                SavFelelos = x.Text;
            }
        }
        return SavFelelos;
    }

    //readonly string jsNemModosithato = "alert('Ragsz�ms�v sz�toszt�sa csak felt�lt�s ut�n lehets�ges.');return false;";
    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }
    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        bool modosithato = false;

        if (getSavAllapot(gridView, rowIndex) == "H")
        {
            modosithato = true;
        }

        return modosithato;
    }

    //readonly string jsNemFeltoltheto = "alert('" + Resources.List.UI_NotLoadable + "');return false;";
    protected bool getFeltoltheto(GridView gridView)
    {
        return getFeltoltheto(gridView, -1);
    }
    protected bool getFeltoltheto(GridView gridView, int rowIndex)
    {
        bool feltoltheto = false;

        if (getSavAllapot(gridView, rowIndex) == "A")
        {
            feltoltheto = true;
        }

        return feltoltheto;
    }

    //readonly string jsNemListazhato = "alert('Ragsz�ms�v csak allok�lt �llapotban list�zhat�.');return false;";
    protected bool getListazhato(GridView gridView)
    {
        return getListazhato(gridView, -1);
    }
    protected bool getListazhato(GridView gridView, int rowIndex)
    {
        bool listazhato = false;

        if (getSavAllapot(gridView, rowIndex) == "A")
        {
            listazhato = true;
        }
        return listazhato;
    }

    protected bool getFelhasznaloJogosult(GridView gridView)
    {
        return getFelhasznaloJogosult(gridView, -1);
    }
    protected bool getFelhasznaloJogosult(GridView gridView, int rowIndex)
    {
        bool felhasznalo_jogosult = false;

        string savFelelos = getSavFelelos(gridView, rowIndex);
        if (savFelelos == FelhasznaloProfil.FelhasznaloId(Page)
            || (ListOfCsoportTagIds != null && ListOfCsoportTagIds.Contains(savFelelos)))
        {
            felhasznalo_jogosult = true;
        }

        return felhasznalo_jogosult;
    }
    #endregion

    #region csoporttagok
    private List<string> ListOfCsoportTagIds = null;

    private List<string> GetCsoportTagokListToSzervezet(string Szervezet_Id)
    {
        List<string> CsoporttagIds = new List<string>();

        if (!String.IsNullOrEmpty(Szervezet_Id))
        {
            CsoporttagIds.Add(Szervezet_Id);
            ExecParam execParam = UI.SetExecParamDefault(Page);
            KRT_CsoportokSearch search = new KRT_CsoportokSearch();

            string[] ids = Contentum.eUtility.Csoportok.GetAllSubCsoport(execParam, Szervezet_Id, true, search);

            if (ids != null)
            {
                foreach (string id in ids)
                {
                    if (!String.IsNullOrEmpty(id))
                    {
                        CsoporttagIds.Add(id);
                    }
                }
            }
        }

        return CsoporttagIds;
    }
    #endregion csoporttagok

    #region BASE PAGE
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KRT_RagszamSavokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.SelectedRecordChanged += new EventHandler(ListHeader1_SelectedRecordChanged);

        #region RAGSZAM
        RagszamokSubListHeader.RowCount_Changed += new EventHandler(RagszamokSubListHeader_RowCount_Changed);
        RagszamokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(RagszamokSubListHeader_ErvenyessegFilter_Changed);
        #endregion
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_ParameterekSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.SearchVisible = false;
        ListHeader1.HeaderLabel = Resources.List.RagszamSavokLisHeader;
        ListHeader1.ExportVisible = true;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.AddVisible = true;
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        ListHeader1.PrintVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.AddOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ExportOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewRagszamSavok.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewRagszamSavok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewRagszamSavok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewRagszamSavok.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("RagszamSavokListajaPrintForm.aspx");

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewRagszamSavok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewRagszamSavok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewRagszamSavok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_RagszamSavokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            RagszamSavokGridViewBind();

        RagszamGridViewBind(GetSelectedMasterRecordID());
        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        // jogosults�g vizsg�lathoz csoporttagok lek�r�se
        if (ListOfCsoportTagIds == null)
        {
            if (ViewState["Csoporttagok"] != null)
            {
                ListOfCsoportTagIds = (List<string>)ViewState["Csoporttagok"];
            }
            else
            {
                ListOfCsoportTagIds = GetCsoportTagokListToSzervezet(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                ViewState["Csoporttagok"] = ListOfCsoportTagIds;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewToolTip = RagszamSavListTooltipNew;
        ListHeader1.ViewToolTip = RagszamSavListTooltipView;
        ListHeader1.ModifyToolTip = RagszamSavListTooltipModify;
        ListHeader1.ExportToolTip = RagszamSavListTooltipExport;
        ListHeader1.AddToolTip = RagszamSavListTooltipAdd;

        ListHeader1.ExportEnabled = false;
        ListHeader1.ExportVisible = false;

        ListHeader1.PrintEnabled = false;
        ListHeader1.PrintVisible = false;

        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;

        //if (IsPostBack)
        //{
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewRagszamSavok);
        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        //}
    }
    #endregion Base Page

    #region RAGSZAM
    protected void RagszamGridViewBind(string id)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("RagszamokGridView", ViewState, "Kod");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("RagszamokGridView", ViewState);

        RagszamGridViewBind(id, sortExpression, sortDirection);
    }
    protected void RagszamGridViewBind(string id, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(id))
        {
            KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
            search.RagszamSav_Id.Value = id;
            search.RagszamSav_Id.Operator = Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("RagszamokGridView", ViewState, SortExpression, SortDirection);

            Result res = service.GetAll(ExecParam, search);

            UI.GridViewFill(RagszamokGridView, res, RagszamokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            TabContainerDetail.Visible = true;
            ragszamokPanel.Visible = true;
            ui.SetClientScriptToGridViewSelectDeSelectButton(RagszamokGridView);
        }
        else
        {
            ui.GridViewClear(RagszamokGridView);
        }
    }
    void RagszamokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        RagszamGridViewBind(GetSelectedMasterRecordID());
    }
    void RagszamokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        RagszamokGridViewBind(GetSelectedMasterRecordID());
    }
    protected void RagszamokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        RagszamGridViewBind(GetSelectedMasterRecordID()
            , e.SortExpression, UI.GetSortToGridView("RagszamGridView", ViewState, e.SortExpression));
    }
    protected void RagszamokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(RagszamokGridView, selectedRowNumber, "check");
        }
    }
    protected void RagszamokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cb_UtolsoHasznalatSikeres", "UtolsoHasznalatSiker");
    }
    protected void ragszamokUpdatePanel_Load(object sender, EventArgs e)
    {

    }
    #endregion

    void ListHeader1_SelectedRecordChanged(object sender, EventArgs e)
    {
        SetSelectedId();
    }
    public string RagszamSavokSelectedId
    {
        get
        {
            object o = ViewState["RagszamSavokSelectedId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["RagszamSavokSelectedId"] = value;
        }
    }
    public void SetSelectedId()
    {
        RagszamSavokSelectedId = ListHeader1.SelectedRecordId;
        if (!string.IsNullOrEmpty(ListHeader1.SelectedRecordId))
            RagszamGridViewBind(ListHeader1.SelectedRecordId);
    }

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void RagszamSavokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewRagszamSavok", ViewState, "");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewRagszamSavok", ViewState);

        RagszamSavokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void RagszamSavokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_RagszamSavokSearch search = (KRT_RagszamSavokSearch)Search.GetSearchObject(Page, new KRT_RagszamSavokSearch());

        search.OrderBy = Search.GetOrderBy("gridViewRagszamSavok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtensions(ExecParam, search);

        UI.GridViewFill(gridViewRagszamSavok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewRagszamSavok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewRagszamSavok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewRagszamSavok.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewRagszamSavok.PageIndex = ListHeader1.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewRagszamSavok.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            //----JavaScripts.ResetScroll(Page, cpeRagszamSavok);
            RagszamSavokGridViewBind();
        }
        else
        {
            //----UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeRagszamSavok);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewRagszamSavok);

        //oldalsz�mok be�ll�t�sa
        ListHeader1.PageCount = gridViewRagszamSavok.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewRagszamSavok);

    }
    protected void RagszamokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = RagszamokGridView.PageIndex;

        RagszamokGridView.PageIndex = RagszamokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != RagszamokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, RagszamokCPE);
            RagszamokGridViewBind(GetSelectedMasterRecordID());
        }
        else
        {
            UI.GridViewSetScrollable(RagszamokSubListHeader.Scrollable, RagszamokCPE);
        }
        RagszamokSubListHeader.PageCount = RagszamokGridView.PageCount;
        RagszamokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(RagszamokGridView);
    }
    protected void RagszamokGridViewBind(string id)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("RagszamokGridView", ViewState, "Kod");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("RagszamokGridView", ViewState);

        RagszamokGridViewBind(id, sortExpression, sortDirection);
    }
    private string GetSelectedMasterRecordID()
    {
        return UI.GetGridViewSelectedRecordId(gridViewRagszamSavok);
    }
    protected void RagszamokGridViewBind(string id, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(id))
        {
            KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_RagSzamok cim = new KRT_RagSzamok();
            KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
            search.RagszamSav_Id.Value = id;
            search.RagszamSav_Id.Operator = Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("RagszamokGridView", ViewState, SortExpression, SortDirection);
            RagszamokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAll(ExecParam, search);

            UI.GridViewFill(RagszamokGridView, res, RagszamokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            ui.SetClientScriptToGridViewSelectDeSelectButton(RagszamokGridView);
        }
        else
        {
            ui.GridViewClear(RagszamokGridView);
        }
    }
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        RagszamSavokGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewRagszamSavok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewRagszamSavok, selectedRowNumber, "check");

            string id = gridViewRagszamSavok.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewRagszamSavok_SelectRowCommand(id);
        }
    }
    private void SetModifyIconClick(bool jogosult, bool modosithato)
    {
        string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewRagszamSavok);
        if (string.IsNullOrEmpty(MasterListSelectedRowId))
        {
            ListHeader1.ModifyOnClientClick = RagszamSavListJavaScriptMessageNincsKivalasztottElem;
            return;
        }
        if (!jogosult)
        {
            ListHeader1.ModifyOnClientClick = RagszamSavListJavaScriptMessageFelhasznaloNemJogosult;
            return;
        }

        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokForm.aspx"
        , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + MasterListSelectedRowId
        , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID, EventArgumentConst.refreshMasterList);

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            bool felhasznalo_jogosult = getFelhasznaloJogosult(gridViewRagszamSavok);
            //m�dos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(gridViewRagszamSavok);

            #region MODIFY
            SetModifyIconClick(felhasznalo_jogosult, modosithato);
            #endregion

            #region VIEW
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokForm.aspx"
                          , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                          , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID, EventArgumentConst.refreshMasterList);
            #endregion

            #region EXPORT
            bool listazhato = getListazhato(gridViewRagszamSavok);
            if (!felhasznalo_jogosult)
            {
                ListHeader1.ExportOnClientClick = RagszamSavListJavaScriptMessageFelhasznaloNemJogosult;
            }
            else if (listazhato)
            {
                /*ListHeader1.ExportOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokListazas.aspx"
					 , QueryStringVars.Id + "=" + id
					 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID);
				*/
                ListHeader1.ExportOnClientClick =
                    "window.open('RagszamSavokListazas.aspx?Id=" + id + "','external');return false;";
            }
            #endregion

            #region ADD RAGSZAM

            bool feltoltheto = getFeltoltheto(gridViewRagszamSavok);
            if (feltoltheto)
            {
                ListHeader1.AddOnClientClick = JavaScripts.SetOnClientClick("RagszamSavokForm.aspx"
                     , CommandName.Command + "=" + CommandName.RagszamIgenyles + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRagszamSavok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.AddOnClientClick = RagszamSavListJavaScriptMessageNemAllokaltRagszamsav;
            }
            #endregion
        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelRagszamSavok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    RagszamSavokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewRagszamSavok));                    
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedRagszamSavok();
            RagszamSavokGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedRagszamSavok()
    {

        if (FunctionRights.GetFunkcioJog(Page, "RagszamSavokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewRagszamSavok, EErrorPanel1, ErrorUpdatePanel);

            KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.SendObjects:
                SendMailSelectedRagszamSavok();
                break;
        }
    }

    /// <summary>
    /// Elkuldi emailben a gridViewRagszamSavok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedRagszamSavok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewRagszamSavok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_RagszamSavok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewRagszamSavok_Sorting(object sender, GridViewSortEventArgs e)
    {
        RagszamSavokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewRagszamSavok", ViewState, e.SortExpression));
    }

    #endregion

}
