using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;

public partial class RagszamSavokListazas : Contentum.eUtility.UI.PageBase
{
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
    	FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KRT_RagszamSavokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_RagszamSavokSearch search = (KRT_RagszamSavokSearch)Search.GetSearchObject(Page, new KRT_RagszamSavokSearch());

        String id = Request.QueryString.Get(QueryStringVars.Id);
     

        if (String.IsNullOrEmpty(id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(EErrorPanel1);
        }
        else
        {
            Result result = service.RagszamSav_Listazas(execParam, id, execParam.Felhasznalo_Id);
            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                string filename = "KRT_Ragszamok.txt";

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                //Response.ContentType = "application/octet-stream";
                Response.ContentType = "text/html";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string barkod = row["Kod"].ToString();
                    Response.Output.WriteLine(barkod);
                }
                Response.End();
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
	}
}
