﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="RagszamOsztasForm.aspx.cs" Inherits="RagszamOsztasForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="eRecordComponent/PldIratPeldanyokTextBox.ascx" TagName="PldIratPeldanyokTextBox"
    TagPrefix="uc3" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc10" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc6" %>

<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>

<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>

<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox"
    TagPrefix="uc" %>

<%@ Register Src="~/Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl"
    TagPrefix="uc" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc13" %>

<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript" src="JavaScripts/jquery-3.2.1.min.js"></script>
    <style type="text/css">
        .panelHivataliKapu .mrUrlapCaption {
            width: 170px;
        }

        .panelHivataliKapu {
            margin-top: 5px;
        }

        .panelIratPeldany {
            margin-top: 5px;
        }
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>

    <uc10:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                function HivatalKapuBehavior() {

                    if (this.rbHivatal) {
                        $addHandler(this.rbHivatal, "click", Function.createDelegate(this, this.OnCimzettTipusChanged));
                    }

                    if (this.rbSzemely) {
                        $addHandler(this.rbSzemely, "click", Function.createDelegate(this, this.OnCimzettTipusChanged));
                    }


                    if (this.kapcsolatiKod) {
                        $addHandler(this.kapcsolatiKod, "change", Function.createDelegate(this, this.OnKapcsolatiKodChanged));
                    }

                    this.init();
                }

                HivatalKapuBehavior.prototype.init = function () {
                    if (this.ddownKuldesMod)
                        this.OnKuldesModChanged();
                    if (this.rbHivatal && this.rbSzemely)
                        this.OnCimzettTipusChanged();
                }

                HivatalKapuBehavior.prototype.OnKuldesModChanged = function (ev) {

                    var index = this.ddownKuldesMod.selectedIndex;

                    if (index > -1) {
                        var opt = this.ddownKuldesMod.options[index];

                        //Hivatali kapu
                        if (opt.value == '<%=Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu %>') {
                            this.panelHivataliKapu.style.display = '';
                            this.panelIratPeldany.style.display = 'none';
                            this.cimzettValidator.enabled = false;
                        }
                        else {
                            this.panelHivataliKapu.style.display = 'none';
                            this.panelIratPeldany.style.display = '';
                            this.cimzettValidator.enabled = true;
                        }
                    }
                }

                HivatalKapuBehavior.prototype.OnCimzettTipusChanged = function (ev) {
                    var selectedIndex = this.GetSelectedCimzettTipus();

                    if (selectedIndex == 1) {
                        this.panelHivatal.style.display = "";
                        this.panelSzemely.style.display = "none";
                    }

                    if (selectedIndex == 2) {
                        this.panelSzemely.style.display = "";
                        this.panelHivatal.style.display = "none";
                    }
                }

                HivatalKapuBehavior.prototype.GetSelectedCimzettTipus = function () {
                    if (this.rbHivatal.checked) {
                        return 1;
                    }

                    if (this.rbSzemely.checked) {
                        return 2;
                    }

                    return 0;
                }

                HivatalKapuBehavior.prototype.OnKRIDChanged = function (ev) {
                    if (this.hivatalNeve)
                        this.hivatalNeve.value = '';

                    if (this.hivatalRovidNeve)
                        this.hivatalRovidNeve.value = '';

                    if (this.hivatalMAKKod)
                        this.hivatalMAKKod.value = '';
                }

                HivatalKapuBehavior.prototype.OnKapcsolatiKodChanged = function (ev) {
                    if (this.szemelyNeve)
                        this.szemelyNeve.value = '';

                    if (this.szemelyEmail)
                        this.szemelyEmail.value = '';
                }

                function pageLoad() {
                    var hkp = new HivatalKapuBehavior();
                }

                //--------------------------------------------------------
                //RAGSZAM IGENYLES
                //--------------------------------------------------------
                function CheckRagszamIgenyles() {

                    try {
                        var mode = $("#ctl00_ContentPlaceHolder1_HiddenFieldRagszamIgenylesModja").val();
                        if (mode == "A") {
                            CheckRagszamIgenylesAuto();
                        }
                        else if (mode == "M") {
                            CheckRagszamIgenylesManual()
                        }
                    } catch (e) {
                        // console.log(e);
                    }
                }

                function CheckRagszamIgenylesManual() {

                    try {
                        var tbRagszam = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxPostaiRagszam');

                        var tbRagszamValue = tbRagszam.value;
                        if (tbRagszamValue == "")
                            return;

                        dataValue = JSON.stringify('M|' + tbRagszamValue);
                        $.ajax({
                            type: "POST",
                            url: "ExpedialasForm.aspx/CheckRagszamIgenyles",
                            data: '{allParameters: ' + dataValue + ' }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: OnSuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    } catch (e) {
                        // console.log(e);
                    }
                }
                function CheckRagszamIgenylesAuto() {
                    try {
                        var ddPostakonyv = document.getElementById('ctl00_ContentPlaceHolder1_DropDownListPostaiRagszamPostakonyv');
                        var ddKuldfajta = document.getElementById('ctl00_ContentPlaceHolder1_DropDownListPostaiRagszamKuldemenyFajtaja');

                        var ddPostakonyvValue = ddPostakonyv.options[ddPostakonyv.selectedIndex].value;
                        var ddKuldfajtaValue = ddKuldfajta.options[ddKuldfajta.selectedIndex].value;

                        dataValue = JSON.stringify('A|' + ddPostakonyvValue + '|' + ddKuldfajtaValue);
                        $.ajax({
                            type: "POST",
                            url: "ExpedialasForm.aspx/CheckRagszamIgenyles",
                            data: '{allParameters: ' + dataValue + ' }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: OnSuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    } catch (e) {
                        // console.log(e);
                    }
                }
                function OnSuccess(response) {
                    if (response["d"] != "") {
                        $("#ctl00_ContentPlaceHolder1_LabelErrorMessageRagszamCheck").html(response["d"]);
                    }
                    else {
                        $("#ctl00_ContentPlaceHolder1_LabelOkMessageRagszamCheck").html("OK");
                    }
                    //alert(response.d);
                }
                //--------------------------------------------------------

            </script>

            <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,RagszamosztasFormHeaderTitle %>" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- CR3152 - Expediálás folyamat tömegesítése --%>
    <asp:Panel ID="TomegesPanel" runat="server" Visible="false">
        <!--Frissítés jelzése-->
        <uc10:CustomUpdateProgress ID="CustomUpdateProgress2" runat="server" />

        <uc1:FormHeader ID="FormHeader2" runat="server" />

        <div>
            <uc13:InfoModalPopup ID="InfoModalPopup1" runat="server" />
            <table cellpadding="0" cellspacing="0" border="1" width="90%">
                <tr>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <br />
                        <asp:Panel ID="IratPeldanyokListPanel" runat="server">
                            <asp:Label ID="Label_Grid" runat="server" Text="Iratpéldányok:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />
                            <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Sorrend" Visible="false">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlSorrend" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám"
                                        SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok_Targy">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Határidő" SortExpression="VisszaerkezesiHatarido">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="NevSTR_Cimzett">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="KuldesMod_Nev" HeaderText="Küldésmód" SortExpression="KuldesMod_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>


                            <asp:GridView ID="KuldemenyekGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                OnRowDataBound="KuldemenyekGridView_RowDataBound">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sorrend" Visible="false">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlSorrend" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EREC_IraIktatoKonyvek_MegkulJelzes" HeaderText="Érk.könyv"
                                        SortExpression="EREC_IraIktatoKonyvek_MegkulJelzes">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Erkezteto_Szam" HeaderText="Érk.szám" SortExpression="Erkezteto_Szam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREC_IraIktatoKonyvek_Ev" HeaderText="Év" SortExpression="EREC_IraIktatoKonyvek_Ev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Küldő&nbsp;ikt.száma" SortExpression="HivatkozasiSzam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Bekuldo" HeaderText="Küldő/feladó neve" SortExpression="NevSTR_Bekuldo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezelő" SortExpression="Csoport_Id_FelelosNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="125px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BeerkezesIdeje" HeaderText="Érk.dátuma" ItemStyle-HorizontalAlign="Center"
                                        SortExpression="ElsoAtvetIdeje">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllapotNev" HeaderText="Állapot" SortExpression="AllapotNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IktatniKellNev" HeaderText="Iktatás" SortExpression="IktatniKellNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_OrzoNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_CimzettNev" HeaderText="Címzett" SortExpression="Csoport_Id_CimzettNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                        <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="PostaKönyv:" />
                        <asp:DropDownList runat="server" ID="DropDownListPostaiRagszamPostakonyv" Width="300px"
                            AutoPostBack="true" />
                        <!--<input type="button" onclick="CheckRagszamIgenyles();" value="Ragszámsáv ellenőrzés" />-->
                        &nbsp;
                        <asp:Label runat="server" Text="Küldemény fajtája:" />
                        <asp:DropDownList runat="server" ID="DropDownListPostaiRagszamKuldemenyFajtaja" Width="300px"
                            AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:FormFooter ID="FormFooter2" runat="server" />
                    </td>
                </tr>

            </table>

        </div>
    </asp:Panel>
    <%--CR3152 - Expediálás folyamat tömegesítése --%>
</asp:Content>
