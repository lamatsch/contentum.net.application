﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RagszamOsztasForm : Contentum.eUtility.UI.PageBase
{
    #region PrivateVariables
    private string Command = "";
    private PageView pageView = null;
    //private ComponentSelectControl compSelector = null;

    private String[] IratPeldanyokArray;
    private String[] KuldemenyekArray;
    private UI ui = new UI();

    //mivel a funkció az expediálás során is elérhető, ezért ugyanazzal a joggal futtatható
    private const string funkciokod_RagszamGeneralas = "Expedialas";
    public string maxTetelszam = "0";

    private string IratPeldany_Ids = "";
    private string Kuldemeny_Ids = "";
    private string[] Kuldemeny_Ids_ClickOrder;
    private string[] IratPeldany_Ids_ClickOrder;

    //több listáról is hívható, a FormSource mutatja meg, honnan került meghívásra
    private string FormSource = "";

    //konstansok a lehetséges forrás értékekkel
    private const string SourceIratPld = "IratpeldanyList";
    private const string SourceKuldemeny = "KuldemenyList";

    private string POSTAI_RAGSZAM_KIOSZTAS_SORREND = "A";
    private int sorrendCounter = 0;

    private string IratPeldany_Vonalkod
    {
        set { ViewState["PldVonalkod"] = value; }
        get
        {
            if (ViewState["PldVonalkod"] == null)
            {
                return String.Empty;
            }
            else
            {
                return ViewState["PldVonalkod"].ToString();
            }
        }
    }
    #endregion

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
        POSTAI_RAGSZAM_KIOSZTAS_SORREND = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "POSTAI_RAGSZAM_KIOSZTAS_SORREND");
    }

    public bool IsSorrendDropDownColumnVisible
    {
        get { return POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K"; }
    }

    /// <summary>
    /// Az oldal Init eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["Tomeges"] != null && Session["Tomeges"].Equals("1"))
        {
            if (Session["RagszamSource"] != null)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkciokod_RagszamGeneralas);
                // FormFooternek beállítjuk a Modify üzemmódot:
                FormFooter2.Command = CommandName.Modify;
                FormHeader2.DisableModeLabel = true;
                FormHeader2.HeaderTitle = Resources.Form.RagszamOsztasFormHeaderTitle;
                FormSource = Session["RagszamSource"].Equals("IratPeldanyokList") ? SourceIratPld : SourceKuldemeny;
                if (FormSource == SourceIratPld && Session["SelectedIratPeldanyIds"] != null)
                {
                    IratPeldany_Ids = Session["SelectedIratPeldanyIds"].ToString();
                    FormSource = SourceIratPld;

                    if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
                    {
                        IratPeldany_Ids_ClickOrder = Session["SelectedIratPeldanyIdsClickOrder"] == null ? null : (string[])Session["SelectedIratPeldanyIdsClickOrder"];

                        foreach(DataControlField column in PldIratPeldanyokGridView.Columns)
                        {
                            if (column.HeaderText == "Sorrend")
                            {
                                column.Visible = true;
                            }
                        }                        
                    }
                }
                if (FormSource == SourceKuldemeny && Session["SelectedKuldemenyIds"] != null)
                {
                    Kuldemeny_Ids = Session["SelectedKuldemenyIds"].ToString();
                    FormSource = SourceKuldemeny;

                    if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
                    {
                        Kuldemeny_Ids_ClickOrder = Session["SelectedKuldemenyIdsClickOrder"] == null ? null : (string[])Session["SelectedKuldemenyIdsClickOrder"];
                        foreach (DataControlField column in KuldemenyekGridView.Columns)
                        {
                            if (column.HeaderText == "Sorrend")
                            {
                                column.Visible = true;
                            }
                        }
                    }
                }
                PldIratPeldanyokGridView.Visible = FormSource == SourceIratPld;
                KuldemenyekGridView.Visible = FormSource == SourceKuldemeny;
            }
        }

        Command = Request.QueryString.Get(CommandName.Command);

        FormHeader1.DisableModeLabel = true;

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkciokod_RagszamGeneralas);
                break;
        }
    }

    /// <summary>
    /// Oldal Load eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        if (IsPostBack)
        {
            if (FormHeader1.ErrorPanel.Visible == true)
            {
                FormHeader1.ErrorPanel.Visible = false;
            }
        }

        if (Session["Tomeges"] != null && Session["Tomeges"].Equals("1"))
        {
            UpdatePanel1.Visible = false;
            TomegesPanel.Visible = true;
            Label_Grid.Text = FormSource == SourceIratPld ? "Iratpéldányok:" : "Küldemények:";
            Label_Grid.Visible = true;
            FormFooter2.Visible = true;
            FormFooter2.ButtonsClick += new
                System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

            Boolean isKuldemenyList = false;
            if (Session["RagszamSource"] != null && Session["RagszamSource"].Equals("IratPeldanyokList"))
            {
                if (!String.IsNullOrEmpty(IratPeldany_Ids))
                {
                    IratPeldanyokArray = IratPeldany_Ids.Split(',');
                }
            }
            if (Session["RagszamSource"] != null && Session["RagszamSource"].Equals("KimenoKuldemenyekList"))
            {
                if (!String.IsNullOrEmpty(Kuldemeny_Ids))
                {
                    KuldemenyekArray = Kuldemeny_Ids.Split(',');
                }
                isKuldemenyList = true;
            }
            if (!IsPostBack)
            {
                InitTomegesRagszamGeneralas();
            }
            else
            {
                if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
                {
                    if (FormSource == SourceKuldemeny)
                    {
                        Kuldemeny_Ids_ClickOrder = CalculateOrder(KuldemenyekGridView);
                    }
                    else if (FormSource == SourceIratPld)
                    {
                        IratPeldany_Ids_ClickOrder = CalculateOrder(PldIratPeldanyokGridView);
                    }
                    InitTomegesRagszamGeneralas();
                }
            }

            //küldemény listánál a küldemény grid -et figyelje, egyébként az irat grid -et
            int cntKijeloltSorok = isKuldemenyList ? ui.GetGridViewSelectedRows(KuldemenyekGridView, FormHeader2.ErrorPanel, null).Count : ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader2.ErrorPanel, null).Count;
            labelTetelekSzamaDb.Text = cntKijeloltSorok.ToString();
        }

        FillKuldemenyFajta();
        FillPostakonyvek();
    }

    private string[] CalculateOrder(GridView grid)
    {
        int a = 0; int b = 0;
        List<int> sorrend = new List<int>();
        for (int i = 0; i < grid.Rows.Count; i++)
        {
            var row = grid.Rows[i];

            if (row.RowType == DataControlRowType.DataRow)
            {
                int pos = int.Parse(((DropDownList)row.FindControl("ddlSorrend")).SelectedValue);
                if (sorrend.Contains(pos))
                {
                    a = sorrend.IndexOf(pos);
                    b = sorrend.Count;
                }
                else
                {
                    sorrend.Add(pos);
                }
            }
        }

        List<string> keys = new List<string>();

        for (int i = 0; i < grid.DataKeys.Count; i++)
        {
            keys.Add(grid.DataKeys[i].Value.ToString());
        }

        Swap(keys, a, b);

        return keys.ToArray();
    }

    public static void Swap<T>(IList<T> list, int indexA, int indexB)
    {
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
    }


    private void LoadComponentsFromBusinessObject(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        if (erec_PldIratPeldanyok != null)
        {
            IratPeldany_Vonalkod = erec_PldIratPeldanyok.BarCode;

            FormHeader1.Record_Ver = erec_PldIratPeldanyok.Base.Ver;
        }
    }

    private EREC_PldIratPeldanyok GetBusinessObjectFromComponents()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        erec_PldIratPeldanyok.Base.Ver = FormHeader1.Record_Ver;
        erec_PldIratPeldanyok.Base.Updated.Ver = true;

        return erec_PldIratPeldanyok;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    private Result PostaiRagSzamKiosztasProcedure(EREC_KuldKuldemenyek kuldemeny, string mode, string postakonyvId, string kuldesModja, string ragszamSavId, string ragszam)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.PostaiRagszamKiosztas(execParam, kuldemeny, mode, postakonyvId, kuldesModja, ragszamSavId, ragszam);
        return result;
    }

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes", "Javascripts/CheckBoxes.js");
    }

    #region IratPeldanyGridView
    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            for (int i = 0; i < IratPeldanyokArray.Length; i++)
            {
                if (i == 0)
                {
                    search.Id.Value = "'" + IratPeldanyokArray[i] + "'";
                }
                else
                {
                    search.Id.Value += ",'" + IratPeldanyokArray[i] + "'";
                }
            }
            search.Id.Value = Search.GetSqlInnerString(IratPeldanyokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K" && IratPeldany_Ids_ClickOrder != null)
            {                
                if (res.IsError)
                {
                    // Hiba kezeles
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader2.ErrorPanel, res, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
                    return null;
                }

                DataTable sortedTable = res.Ds.Tables[0].Clone();

                for (int i = 0; i < IratPeldany_Ids_ClickOrder.Length; i++)
                {
                    DataRow row = res.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Id"].ToString().Trim().ToLower() == IratPeldany_Ids_ClickOrder[i].Trim().ToLower());
                    if (row != null)
                    {
                        sortedTable.Rows.Add(row.ItemArray);
                    }
                }

                ui.GridViewFill(PldIratPeldanyokGridView, sortedTable, "", FormHeader2.ErrorPanel, null);
            }
            else
            {
                ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader2.ErrorPanel, null);
            }

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader2.ErrorPanel);
            return null;
        }
    }

    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        EREC_PldIratPeldanyok obj_iratPeldany = null;
        EREC_IraIratok obj_irat = null;
        EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
        EREC_UgyUgyiratok obj_ugyirat = null;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;
            bool success_objectsGet;
            if (!string.IsNullOrEmpty(drv.Row["Id"].ToString()))
                success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(drv.Row["Id"].ToString(), out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                    , Page, FormHeader2.ErrorPanel);
        }
        IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckExpedialt(e, Page);

        if (e.Row.RowType == DataControlRowType.DataRow && POSTAI_RAGSZAM_KIOSZTAS_SORREND=="K")
        {
            DropDownList ddlSorrend = (DropDownList)e.Row.FindControl("ddlSorrend");
            ddlSorrend.Items.AddRange(GetListItems(IratPeldanyokArray.Length));
            ddlSorrend.SelectedIndex = sorrendCounter;
            sorrendCounter++;
        }
    }

    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader2.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }
    }
    #endregion

    #region KuldemenyGridView
    protected DataSet KuldemenyekGridViewBind()
    {
        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            for (int i = 0; i < KuldemenyekArray.Length; i++)
            {
                if (i == 0)
                {
                    search.Id.Value = "'" + KuldemenyekArray[i] + "'";
                }
                else
                {
                    search.Id.Value += ",'" + KuldemenyekArray[i] + "'";
                }
            }
            search.Id.Value = Search.GetSqlInnerString(KuldemenyekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K" && Kuldemeny_Ids_ClickOrder != null)
            {
                DataTable sortedTable = res.Ds.Tables[0].Clone();

                for (int i = 0; i < Kuldemeny_Ids_ClickOrder.Length; i++)
                {
                    DataRow row = res.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Id"].ToString().Trim().ToLower() == Kuldemeny_Ids_ClickOrder[i].Trim().ToLower());
                    if (row != null)
                    {
                        sortedTable.Rows.Add(row.ItemArray);
                    }
                }

                ui.GridViewFill(KuldemenyekGridView, sortedTable, "", FormHeader2.ErrorPanel, null);
            }
            else
            {
                ui.GridViewFill(KuldemenyekGridView, res, "", FormHeader2.ErrorPanel, null);
            }

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader2.ErrorPanel);
            return null;
        }
    }

    protected void KuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (e.Row.RowType == DataControlRowType.DataRow && POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
        {
            DropDownList ddlSorrend = (DropDownList)e.Row.FindControl("ddlSorrend");
            ddlSorrend.Items.AddRange(GetListItems(KuldemenyekArray.Length));
            ddlSorrend.SelectedIndex = sorrendCounter;
            sorrendCounter++;
        }
        Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckExpedialt(e, Page);
    }

    private ListItem[] GetListItems(int count)
    {
        ListItem[] items = new ListItem[count];
        for (int i = 0; i < count; i++)
        {
            ListItem item = new ListItem((i + 1).ToString());
            items[i] = item;
        }
        return items;
    }

    private void FillKuldemenyGridView()
    {
        DataSet ds = KuldemenyekGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != KuldemenyekArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader2.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }
    }
    #endregion

    private void InitTomegesRagszamGeneralas()
    {
        //Felelős ellenőrzéshez kell
        try
        {
            if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
            {
                FillIratPeldanyGridView();
            }
            if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
            {
                FillKuldemenyGridView();
            }
        }
        catch (Contentum.eUtility.ResultException e)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader2.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            int selectedIds = FormSource == SourceIratPld ? ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader2.ErrorPanel, null).Count : ui.GetGridViewSelectedRows(KuldemenyekGridView, FormHeader2.ErrorPanel, null).Count;

            if (selectedIds > int.Parse(maxTetelszam))
            {
                // ha a JavaScript végén "return false;" van, nem jelenik meg az üzenet...
                string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "');"; // return false; } ";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                return;
            }
            if (selectedIds == 0)
            {
                ResultError.DisplayNoIdParamError(FormHeader2.ErrorPanel);
                return;
            }
            Dictionary<String, EREC_KuldKuldemenyek> kuldemenyekListWithIratPldAzon = new Dictionary<string, EREC_KuldKuldemenyek>();
            if (FormSource == SourceIratPld)
            {
                #region Iratpéldány ágon tömeges ragszám kioasztás
                List<string> selectedIratPeldanyokList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader2.ErrorPanel, null);
                foreach (string IratPeldanyId in selectedIratPeldanyokList)
                {
                    EREC_PldIratPeldanyokService serviceIratPld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam execParamIratPld = UI.SetExecParamDefault(Page, new ExecParam());
                    execParamIratPld.Record_Id = IratPeldanyId;
                    EREC_PldIratPeldanyok pld = (EREC_PldIratPeldanyok)serviceIratPld.Get(execParamIratPld).Record;

                    //pld.
                    if (pld.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
                    {
                        EREC_KuldKuldemenyekService serviceKuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                        ExecParam execParamKuld = UI.SetExecParamDefault(Page, new ExecParam());

                        EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch(true);
                        search.Extended_EREC_PldIratPeldanyokSearch.Id.Value = IratPeldanyId;
                        search.Extended_EREC_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

                        //search.a

                        search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
                        search.Allapot.Operator = Query.Operators.equals;

                        Result result = serviceKuld.KimenoKuldGetAllWithExtension(execParamKuld, search);
                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                System.Data.DataRow row = result.Ds.Tables[0].Rows[0];

                                EREC_KuldKuldemenyekService serviceKuldById = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                ExecParam execParamKuldById = UI.SetExecParamDefault(Page, new ExecParam());
                                execParamKuldById.Record_Id = row["Id"].ToString();
                                EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)serviceKuldById.Get(execParamKuldById).Record;

                                //generateRagszam(kuld);
                                if (!kuldemenyekListWithIratPldAzon.ContainsKey(pld.Azonosito))
                                    kuldemenyekListWithIratPldAzon.Add(pld.Azonosito, kuld);
                            }

                        }
                    }

                }

                #endregion

            }
            else if (FormSource == SourceKuldemeny)
            {
                #region Küldemény ágon tömeges ragszám kioasztás
                List<string> selectedkuldemenyekList = ui.GetGridViewSelectedRows(KuldemenyekGridView, FormHeader2.ErrorPanel, null);
                foreach (string KuldemenyId in selectedkuldemenyekList)
                {
                    EREC_KuldKuldemenyekService serviceKuldById = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParamKuldById = UI.SetExecParamDefault(Page, new ExecParam());
                    execParamKuldById.Record_Id = KuldemenyId;
                    EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)serviceKuldById.Get(execParamKuldById).Record;
                    //get Pld azon to kuldemeny
                    String pldAzon = String.Empty;

                    EREC_PldIratPeldanyokService iratPldService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam execParamIratPld = UI.SetExecParamDefault(Page, new ExecParam());

                    EREC_PldIratPeldanyokSearch iratPldSearch = new EREC_PldIratPeldanyokSearch(true);

                    Result resIratPld = iratPldService.GetAllWithExtensionByKimenoKuldemeny(execParamIratPld, iratPldSearch, kuld.Id);
                    if (String.IsNullOrEmpty(resIratPld.ErrorCode))
                    {
                        if (resIratPld.Ds.Tables[0].Rows.Count > 0)
                        {
                            System.Data.DataRow row = resIratPld.Ds.Tables[0].Rows[0];
                            EREC_PldIratPeldanyokService iratPldServiceGet = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParamIratPldGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParamIratPldGet.Record_Id = row["Id"].ToString();
                            EREC_PldIratPeldanyok iratPld = (EREC_PldIratPeldanyok)iratPldServiceGet.Get(execParamIratPldGet).Record;

                            pldAzon = iratPld.Azonosito;
                        }
                    }
                    if (!kuldemenyekListWithIratPldAzon.ContainsKey(pldAzon))
                        kuldemenyekListWithIratPldAzon.Add(pldAzon, kuld);
                }
                #endregion
            }
            //csak akkor regisztráljuk a scripteket, ha valóban képződött ragszám, és minden esetben sierkes volt
            //Alapértelmezett false -> ha nincsenek kuldemenyeink, nem lehe true az értéke
            bool successFullGeneration = false;
            if (kuldemenyekListWithIratPldAzon != null && kuldemenyekListWithIratPldAzon.Count > 0)
                successFullGeneration = generateRagszamByOrder(kuldemenyekListWithIratPldAzon);
            if (successFullGeneration)
            {
                //FormSource -on múlik, mit adunk vissza a hívó oldalra
                if (FormSource == SourceKuldemeny)
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, Kuldemeny_Ids);
                else
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, IratPeldany_Ids);
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader2.ErrorPanel, null);
        }

    }

    #region POSTAI RAGSZAM

    private bool generateRagszamByOrder(Dictionary<String, EREC_KuldKuldemenyek> kuldemenyekListWithIratPldAzon)
    {
        //TODO - sorrendet véglegesíteni!
        //kuldemenyekList = kuldemenyekList.OrderBy(X => X.KimenoKuld_Sorszam).ToList();
        try
        {
            if (POSTAI_RAGSZAM_KIOSZTAS_SORREND != "K")
            {
                List<EREC_KuldKuldemenyek> sortedList = kuldemenyekListWithIratPldAzon.OrderBy(X => X.Key).Select(X => X.Value).ToList();
                foreach (EREC_KuldKuldemenyek kuld in sortedList)
                {
                    bool returnvalue = generateRagszam(kuld);
                    //sikertelen generálás esetén nem megyünk tovább
                    if (!returnvalue) return false;
                }
            }
            else
            {
                foreach (EREC_KuldKuldemenyek kuld in kuldemenyekListWithIratPldAzon.Values)
                {
                    bool returnvalue = generateRagszam(kuld);
                    //sikertelen generálás esetén nem megyünk tovább
                    if (!returnvalue) return false;
                }
            }

        }
        catch (Exception e)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader2.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
            return false;
        }
        return true;
    }

    private bool generateRagszam(EREC_KuldKuldemenyek kuld)
    {
        string rszPostakonyvId = null;
        string rszkuldemenyFajta = null;
        string rszCode = null;
        string rszRagszamSavId = null;
        string outError = null;
        var pk = DropDownListPostaiRagszamPostakonyv.SelectedValue;
        var kf = DropDownListPostaiRagszamKuldemenyFajtaja.SelectedValue;

        if (!CheckPostaiRagszamParameters(out rszPostakonyvId, out rszkuldemenyFajta, out rszCode, out rszRagszamSavId, out outError))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, outError);
            return false;
        }
        else
        {
            //BUG 2128 - generálás módja ttömeges ("T") esetén külön ágon működik a webservice, nem állít küldésmódot
            Result resultPostaiRagszam = PostaiRagSzamKiosztasProcedure(kuld, "T", rszPostakonyvId, rszkuldemenyFajta, rszRagszamSavId, null);
            if (resultPostaiRagszam != null)
            {
                if (!String.IsNullOrEmpty(resultPostaiRagszam.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultPostaiRagszam);
                    return false;
                }
            }
        }

        return true;
    }
    #region FILL
    private void FillPostakonyvek()
    {
        if (DropDownListPostaiRagszamPostakonyv.Items.Count > 0)
            return;

        //BUG3845
        Result result;

        List<IktatoKonyvek.IktatokonyvItem> IktatokonyvekList = null;

        result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, Constants.IktatoErkezteto.Postakonyv, false, false, out IktatokonyvekList);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            DropDownListPostaiRagszamPostakonyv.Items.Clear();

            foreach (IktatoKonyvek.IktatokonyvItem item in IktatokonyvekList)
            {
                try
                {
                    DropDownListPostaiRagszamPostakonyv.Items.Add(new ListItem(item.Text, item.Id));
                }
                catch
                {
                    DropDownListPostaiRagszamPostakonyv.Items.Clear();
                    DropDownListPostaiRagszamPostakonyv.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                    break;
                }
            }
        }
    }
    private void FillKuldemenyFajta()
    {
        if (DropDownListPostaiRagszamKuldemenyFajtaja.Items.Count > 0)
            return;

        Dictionary<string, string> items = LoadPostaiKimenoKuldemenyFajta();
        if (items == null)
            return;

        DropDownListPostaiRagszamKuldemenyFajtaja.Items.Clear();
        ListItem li = null;
        foreach (KeyValuePair<string, string> item in items)
        {
            li = new ListItem();
            li.Text = item.Value;
            li.Value = item.Key;
            DropDownListPostaiRagszamKuldemenyFajtaja.Items.Add(li);
        }
    }
    #endregion

    private Dictionary<string, string> LoadPostaiKimenoKuldemenyFajta()
    {
        Dictionary<string, string> allElements = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.kcsNev, Page);
        if (allElements == null || allElements.Count < 1)
            return null;

        Dictionary<string, string> filteredElements = new Dictionary<string, string>();
        foreach (KeyValuePair<string, string> item in allElements)
        {
            if (item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Ajanlott_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Cimzett_Kezebe_Level
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Hivatalos_Irat
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Level_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Hivatalos_Irat)
            {
                filteredElements.Add(item.Key, item.Value);
            }
        }
        return filteredElements;
    }

    #region PARAMETEREK

    private bool CheckPostaiRagszamParameters(out string postakonyvId, out string kuldemenyFajta, out string ragszam, out string outRagszamSavId, out string outError)
    {
        postakonyvId = null;
        kuldemenyFajta = null;
        ragszam = null;
        outRagszamSavId = null;
        outError = null;
        postakonyvId = DropDownListPostaiRagszamPostakonyv.SelectedValue;
        kuldemenyFajta = DropDownListPostaiRagszamKuldemenyFajtaja.SelectedValue;
        var res = IsValidRagszamParameters("A", postakonyvId, kuldemenyFajta, ragszam, out outError);
        if (!res)
            return false;

        if (!CheckRagszam(postakonyvId, kuldemenyFajta, out outRagszamSavId, out outError))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader2.ErrorPanel, Resources.Error.MessageRagszamValidationError, outError);
            return false;
        }

        return true;
    }

    private bool IsValidRagszamParameters(string mode, string postakonyv, string kuldemenyFajta, string ragszam, out string outError)
    {
        outError = null;

        if (string.IsNullOrEmpty(kuldemenyFajta))
        {
            outError = Resources.Error.UI_ValidationMessage_EmptyKudlemenyFajta;
            return false;
        }

        if (string.IsNullOrEmpty(postakonyv))
        {
            outError = Resources.Error.UI_ValidationMessage_EmptyPostakonyv;
            return false;
        }

        return true;
    }
    #endregion
    #endregion

    private static bool CheckRagszam(string postakonyv, string kuldFajta, out string outRagszamSavId, out string outError)
    {
        outError = null;
        string errorString;
        outRagszamSavId = null;
        DataRow ragszamSavData;
        bool ret = CheckRagszamSavInService(postakonyv, kuldFajta, out errorString, out outRagszamSavId, out ragszamSavData);

        if (!ret)
        {
            outError = errorString;
            return false;
        }

        return true;
    }

    private static bool CheckRagszamSavInService(string postakonyv, string kuldFajta, out string errorString, out string outRagSzamSavId, out DataRow outRagSzamSav)
    {
        outRagSzamSavId = null;
        errorString = null;
        outRagSzamSav = null;
        Result result = GetAllRagszamSavFilteredByPkKf(postakonyv, kuldFajta);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds == null || result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
        {
            errorString = Resources.Error.MessageRagszamSavNotFound;
            return false;
        }

        //if (result.Ds.Tables[0].Rows.Count > 1)
        //{
        //    errorString = Resources.Error.MessageRagszamSavMoreThanOne;
        //    return false;
        //}

        outRagSzamSavId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        outRagSzamSav = result.Ds.Tables[0].Rows[0];
        return true;
    }
    private static bool CheckRagszamExistingInService(string ragszam, string postakonyv, string ragSzamSavId, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        Result result = FindRagszamByCode(ragszam, ragszamNum.ToString(), postakonyv, ragSzamSavId);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count < 1)
        {
            errorString = "Nem található ragszám";
            return false;
        }
        return true;
    }

    private static bool CheckRagszamExistingInService(string ragszam, out string postakonyv, out string ragSzamSavId, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        ragSzamSavId = null;
        postakonyv = null;
        Result result = FindRagszamByCode(ragszam, ragszamNum.ToString());
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count >= 1)
        {
            postakonyv = result.Ds.Tables[0].Rows[0]["Postakonyv_Id"].ToString();
            ragSzamSavId = result.Ds.Tables[0].Rows[0]["RagszamSav_Id"].ToString();
            errorString = Resources.Error.MessageRagszamExist;
            return false;
        }

        return true;
    }

    #region SERVICE CALL
    /// <summary>
    /// Get all allokalt ragszamsav by postakonyv and kuldemeny fajta.
    /// </summary>
    /// <param name="postakonyv"></param>
    /// <param name="kuldFajta"></param>
    /// <returns></returns>
    private static Result GetAllRagszamSavFilteredByPkKf(string postakonyv, string kuldFajta)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Postakonyv_Id.Value = postakonyv;
        search.Postakonyv_Id.Operator = Query.Operators.equals;

        search.SavType.Value = kuldFajta;
        search.SavType.Operator = Query.Operators.equals;

        search.SavAllapot.Value = "H";
        search.SavAllapot.Operator = Query.Operators.equals;

        return service.GetAll(execParam, search);
    }

    private static Result FindRagszamByCode(string ragszam, string ragszamNum, string postakonyv, string ragszamSavId)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Value = ragszam;
        search.Kod.Operator = Query.Operators.equals;

        search.KodNum.Value = ragszamNum;
        search.KodNum.Operator = Query.Operators.equals;

        search.Postakonyv_Id.Value = postakonyv;
        search.Postakonyv_Id.Operator = Query.Operators.equals;

        search.KodType.Value = "H";
        search.KodType.Operator = Query.Operators.equals;

        search.RagszamSav_Id.Value = ragszamSavId;
        search.RagszamSav_Id.Operator = Query.Operators.equals;

        return service.GetAll(execParam, search);
    }
    private static Result FindRagszamByCode(string ragszam, string ragszamNum)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Value = ragszam;
        search.Kod.Operator = Query.Operators.equals;

        return service.GetAll(execParam, search);
    }

    private Result FindKimenoKuldemeny(string id)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        return service.Get(execParam);
    }
    #endregion

    #region HELPER
    public static float? GetRagszamNumber(string value)
    {
        int index = int.MinValue;
        for (int i = 0; i < value.Length; i++)
        {
            byte val;
            if (byte.TryParse(value[i].ToString(), out val))
            {
                index = i;
                break;
            }
        }
        if (index != int.MinValue)
        {
            string numString = value.Substring(index);
            float ret = float.MinValue;
            if (float.TryParse(numString, out ret))
            {
                return ret;
            }
        }
        return null;
    }
    #endregion

}