using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;

public partial class RecordHistory : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();

    protected void Page_Init(object sender, EventArgs e)
    {
        /// Jogosultságellenőrzés:

        KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string tableName = Request.QueryString.Get(QueryStringVars.TableName);
        string MuveletKod_ViewHistory = "ViewHistory";

        Result result = service.GetByTableAndOperation(execParam, tableName, MuveletKod_ViewHistory);

        if (result.IsError)
        {
            Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
            UI.RedirectToErrorPageByErrorMsg(Page, resultError.ErrorMessage);
        }
        else
        {
            KRT_Funkciok funkcio = (KRT_Funkciok)result.Record;
            String funkcioKod = funkcio.Kod;
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        //HistoryListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("HistorySearch.aspx", ""
        //    , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HistoryListHeader1.HeaderLabel = Resources.List.RecordHistoryHeaderTitle;

        HistoryListHeader1.SearchEnabled = false;
        HistoryListHeader1.ViewEnabled = false;
        // BLG_2970
        HistoryListHeader1.ExportEnabled = true;
        HistoryListHeader1.PrintEnabled = false;

        // BLG_2970
        HistoryListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ScriptManager1.RegisterPostBackControl(HistoryListHeader1.ExportButton);

        if (!IsPostBack) HistoryGridViewBind();

        Page.Title = Page.Title + " - " + Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.APPLICATION_VERSION) + " ";
    }

    // BLG_2970
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            // List<double> columnSizeList = new List<double>() { 2.57, 4.71, 5.29, 2.8, 16.14, 13.57, 13.86, 14.86, 10.57, 22.29, 11, 13.3, 18 };
            ex_Export.SaveGridView_ToExcel(exParam, HistoryGridView, HistoryListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
    }

    protected void HistoryGridViewBind()
    {
        string recordId = Request.QueryString.Get(QueryStringVars.Id);
        string tableName = Request.QueryString.Get(QueryStringVars.TableName);

        if (!String.IsNullOrEmpty(recordId) && !String.IsNullOrEmpty(tableName))
        {
            RecordHistoryService service = eAdminService.ServiceFactory.GetRecordHistoryService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            ExecParam.Record_Id = recordId;
            Result res = service.GetAllByRecord(ExecParam, tableName);

            // BUG_10349 - Számlák history-ja is kell, nem csak a küldeményeké
            var startup = Request.QueryString.Get(QueryStringVars.Startup);
            if (startup == Contentum.eRecord.Utility.Constants.Startup.Szamla && tableName == "EREC_KuldKuldemenyek")
            {
                var szamlakService = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_SzamlakService();
                var szamlakSearch = new EREC_SzamlakSearch();
                szamlakSearch.KuldKuldemeny_Id.Filter(recordId);
                var execParam_szamlaSearch = UI.SetExecParamDefault(Page, new ExecParam());

                var resultSzamlak = szamlakService.GetAll(execParam_szamlaSearch, szamlakSearch);
                if (!resultSzamlak.IsError)
                {
                    foreach (DataRow szamlaRow in resultSzamlak.Ds.Tables[0].Rows)
                    {
                        ExecParam execParam_szamla = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam_szamla.Record_Id = szamlaRow["Id"].ToString();
                        Result resultSzamla = service.GetAllByRecord(execParam_szamla, "EREC_Szamlak");

                        if (!resultSzamla.IsError)
                        {
                            var dt = res.Ds.Tables[0];
                            dt.Merge(resultSzamla.Ds.Tables[0]);
                            dt.DefaultView.Sort = "ExecutionTime desc";
                            res.Ds.Tables.RemoveAt(0);
                            res.Ds.Tables.Add(dt.DefaultView.ToTable());
                        }
                    }
                }
            }

            //if (res.Ds != null)
            //{
            ui.GridViewFill(HistoryGridView, res, EErrorPanel1, ErrorUpdatePanel);
            //}
        }
        else
        {
            ResultError.DisplayNoIdParamError(EErrorPanel1);
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {

    }
    protected void HistoryGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = HistoryGridView.PageIndex;

        HistoryGridView.PageIndex = (HistoryListHeader1.PageIndex < 0 ? 0 : HistoryListHeader1.PageIndex);
        HistoryListHeader1.PageCount = HistoryGridView.PageCount;

        if (prev_PageIndex != HistoryGridView.PageIndex)
        {
            HistoryGridViewBind();
            UI.ClearGridViewRowSelection(HistoryGridView);
            //ActiveTabClear();
        }

        HistoryListHeader1.PagerLabel = (HistoryGridView.PageIndex + 1).ToString() + "/" + HistoryGridView.PageCount.ToString();
    }

    protected void HistoryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}
