﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Redirect : System.Web.UI.Page
{
    private string RedirectTo
    {
        get
        {
            string qs = Request.QueryString.Get("RedirectTo");

            if (!String.IsNullOrEmpty(qs))
            {
                return HttpUtility.UrlDecode(qs);
            }

            return String.Empty;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(RedirectTo))
        {
            string js = String.Format("window.location = '{0}';", ResolveClientUrl(RedirectTo));
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "redirect", js, true);
        }
    }
}