﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="RefreshCache.aspx.cs" Inherits="RefreshCache" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="cache-control" content="no-store" />
    <title>Untitled Page</title>
    <base target="_self" />
</head>
<body runat="server" id="masterBody">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <uc:FormHeader ID="FormHeader1" runat="server" FullManualHeaderTitle="Cache frissítése" />
        <ul style="text-align: left">
            <li>
                <asp:LinkButton ID="ForditasokCache" Text="Fordítások cache" runat="server" OnClick="ForditasokCache_Click" />
            </li>
        </ul>
    </form>
</body>
</html>


