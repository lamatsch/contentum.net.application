﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RefreshCache : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string command = Request.QueryString.Get(QueryStringVars.Command);

            if ("RefreshForditasokCache".Equals(command))
            {
                RefreshForditasokCache();
            }
        }
    }

    protected void ForditasokCache_Click(object sender, EventArgs e)
    {
        RefreshForditasokCache();
    }

    void RefreshForditasokCache()
    {
        ForditasokManager.ClearCache();
    }
}