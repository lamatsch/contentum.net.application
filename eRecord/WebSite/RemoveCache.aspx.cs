using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Contentum.eAdmin.Utility;


public partial class _Default : Contentum.eUtility.UI.PageBase
{

    //Authentication auth = new Authentication();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);

    }

    protected void Page_Load(object sender, EventArgs e)
       {
        
        if (String.IsNullOrEmpty(Request.QueryString["name"]))
        {
            foreach (DictionaryEntry item in Cache)
            {
                if (item.Value.ToString().Contains("Dictionary`2[System.String,System.DateTime]"))
                {
                    Response.Write("<u><a href='./removecache.aspx?name=" + item.Key.ToString() + "'>Remove " + item.Key.ToString() + " = " + Cache[item.Key.ToString()] + "</a></u><br />");
                    Dictionary<string, DateTime> Dictionary = null;
                    Dictionary = (Dictionary<string, DateTime>)Cache[item.Key.ToString()];
                    foreach (KeyValuePair<string, DateTime> hi in Dictionary)
                    {
                        Response.Write(hi.Key.ToString() + " �rt�ke: " + hi.Value.ToString() + "<br />");
                    }
                }                
                else {
                    Response.Write("<a href='./removecache.aspx?name=" + item.Key.ToString() + "'>Remove " + item.Key.ToString() + " = " + Cache[item.Key.ToString()] + "</a><br />");
                }
            }
            Response.Write("<a href='./removecache.aspx?name=ALL'>Remove ALL</a><br />");
        }
        else
        {
            if (Request.QueryString["name"] == "ALL")
            {
                foreach (DictionaryEntry item in Cache)
                {
                    Cache.Remove(item.Key.ToString());
                    Response.Write(item.Key.ToString() + " removed<br />");
                }
            }
            else
            {
                Cache.Remove(Request.QueryString["name"]);
                Response.Write(Request.QueryString["name"] + " removed<br />");
            }            
        }
    }

}
