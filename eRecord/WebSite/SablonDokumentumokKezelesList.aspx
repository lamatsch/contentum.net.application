<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SablonDokumentumokKezelesList.aspx.cs" Inherits="SablonDokumentumokKezelesList" Title="Untitled Page"%>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="~/eRecordComponent/SablonDokumentumokList.ascx" TagPrefix="dl" TagName="SablonDokumentumokList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<uc2:ListHeader ID="ListHeader1" runat="server" />
<!--Hiba megjelen�t�se-->
<asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<!--Friss�t�s jelz�se-->
<uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--HiddenFields--%> 
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeSablonok" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="SablonokUpdatePanel" runat="server" OnLoad="SablonokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeSablonok" runat="server" TargetControlID="panelSablonok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeSablonok" CollapseControlID="btnCpeSablonok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeSablonok"
                            ExpandedSize="0" ExpandedText="K�dcsoportok list�ja" CollapsedText="K�dcsoportok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        
                        <asp:Panel ID="panelSablonok" runat="server" Width="100%" printable="true"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="SablonokGridView" runat="server" OnSorting="SablonokGridView_Sorting"
                                        GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                        DataKeyNames="Id" AutoGenerateColumns="False" OnPreRender="SablonokGridView_PreRender" 
                                        AllowSorting="True" PagerSettings-Visible="false" AllowPaging="True" 
                                        CellPadding="0" OnRowCommand="SablonokGridView_RowCommand" OnRowDataBound="SablonokGridView_RowDataBound">        
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                            <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="KRT_Kodtarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="KRT_Kodtarak.Kod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Egyeb" HeaderText="Egy�b" SortExpression="KRT_Kodtarak.Egyeb">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>           
                                        </Columns>
                                    <PagerSettings Visible="False" />
                                   </asp:GridView>
                                   <div class="MediaPrintBreak" />
                                   </td>
                                 </tr>
                             </table>
                        </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>                            
          </tr>      
          <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
          </tr>
          <!--Allist�k--> 
          <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeDetail" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;"> 
            <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                 CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeDetail" CollapseControlID="btnCpeDetail" ExpandDirection="Vertical"
                 AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeDetail">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="panelDetail" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%" ActiveTabIndex="0">
                        <!-- T�K adatok -->
                        <ajaxToolkit:TabPanel ID="DokumentumTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelDokumentum" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="headerDokumentum" runat="server" Text="Sablon dokumentum"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="DokumentumUpdatePanel" runat="server" OnLoad="DokumentumUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="DokumentumPanel" runat="server" Visible="true" Width="100%">
                                            <%--BLG_2521--%> 
                                            <uc2:ListHeader ID="ListHeader2" runat="server" />
                                            <uc1:SubListHeader ID="DokumentumSubListHeader" runat="server" />
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="DokumentumCPE" runat="server" TargetControlID="panelDokumentumList"
                                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <asp:Panel ID="panelDokumentumList" runat="server" printable="true">
                                                            <dl:SablonDokumentumokList runat="server" ID="DokumentumLista" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>  
                 </asp:Panel> 
                </td>
            </tr>
        </table>
       </td>
      </tr>
   </table>
   <br /> 
</asp:Content>