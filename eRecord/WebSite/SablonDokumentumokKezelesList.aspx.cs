using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Data;
using Contentum.eRecord.Service;

public partial class SablonDokumentumokKezelesList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    public string Sablonok_KodCsoportKod = "SABLONOK";

    #region Base Page

    private string DokumentumId
    {
        get
        {
            return Contentum.eRecord.Utility.QueryStringVars.Get<Guid>(Page, Contentum.eRecord.Utility.QueryStringVars.DokumentumId);
        }
    }

    private bool IsVersionView
    {
        get
        {
            return (!String.IsNullOrEmpty(DokumentumId));
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SablonokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        ListHeader1.SearchEnabled = false;


        ListHeader1.META_ListButtonEnabled = true;
        ListHeader1.META_ListButtonVisible = true;
        ListHeader1.META_ListButtonOnClientClick = JavaScripts.SetOnClientClick("IratMETA_List.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.FuggosegButtonEnabled = true;
        ListHeader1.FuggosegButtonVisible = true;
        ListHeader1.FuggosegButtonOnClientClick = JavaScripts.SetOnClientClick("KodtarFuggosegForm.aspx?Command=Modify&Id=e8c8a9ca-8369-4004-8a3f-ed47121609bd&WindowType=Popup&_lgdLoginType=Basic", "", Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        DokumentumSubListHeader.RowCount_Changed += new EventHandler(DokumentumSubListHeader_RowCount_Changed);
        DokumentumSubListHeader.ErvenyessegFilter_Changed += new EventHandler(DokumentumSubListHeader_ErvenyessegFilter_Changed);

        DokumentumLista.ScriptManager = ScriptManager1;
        // BLG_2521
        //DokumentumokLista.ListHeader = ListHeader1;
        DokumentumLista.ListHeader = ListHeader2;
        DokumentumLista.ListHeader.Visible = false;

        DokumentumLista.SubListHeader = DokumentumSubListHeader;
        DokumentumLista.DokumentumId = DokumentumId;
        DokumentumLista.IsVersionView = IsVersionView;
        DokumentumLista.IsPartnerMode = true;


        DokumentumSubListHeader.InvalidateEnabled = false;
        DokumentumSubListHeader.InvalidateVisible = false;
        DokumentumSubListHeader.ModifyEnabled = false;
        DokumentumSubListHeader.ModifyVisible = false;
        DokumentumSubListHeader.ValidFilterVisible = false;


        DokumentumLista.InitPage();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);

        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        // BLG_2521
        ListHeader1.HeaderLabel = "Sablonok list�ja";
        ListHeader1.SearchObjectType = typeof(KRT_KodTarakSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.PrintHtmlVisible = false;
        ListHeader1.RevalidateVisible = true;
        // BLG_1132
        ExecParam execParam = UI.SetExecParamDefault(Page);

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
        //    , Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("SablonokForm.aspx", "Command=New"
            , Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(SablonokGridView.ClientID, "", "check");
        
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(SablonokGridView.ClientID);


        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = SablonokGridView;

        DokumentumSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        DokumentumSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        DokumentumSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        DokumentumSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(DokumentumLista.GridView.ClientID);
        DokumentumSubListHeader.ButtonsClick += new CommandEventHandler(DokumentumSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        DokumentumSubListHeader.AttachedGridView = DokumentumLista.GridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_PartnerekSearch());

        if (!IsPostBack) SablonokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        ListHeader1.RevalidateOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(SablonokGridView.ClientID);

        DokumentumLista.LoadPage();

        DokumentumLista.GridView.PreRender += DokumentumokGridView_PreRender;
        DokumentumLista.GridView.Sorting += DokumentumokGridView_Sorting;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.Invalidate);
        ListHeader1.RevalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.Revalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Sablonok" + CommandName.ViewHistory);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(SablonokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(SablonokGridView);

        DokumentumLista.PreRenderPage();
        
        //Checkboxok elt�ntet�se
        DokumentumLista.GridView.Columns[0].Visible = false;

    }

    #endregion

    #region Master List

    protected void SablonokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SablonokGridView", ViewState, "KRT_Kodtarak.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SablonokGridView", ViewState);

        SablonokGridViewBind(sortExpression, sortDirection);
    }

    protected void SablonokGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        //KRT_KodTarakSearch search = (KRT_KodTarakSearch)Search.GetSearchObject(Page, new KRT_KodTarakSearch());
        //search.OrderBy = Search.GetOrderBy("SablonokGridView", ViewState, SortExpression, SortDirection);

        //// BLG_2521
        //search.KodCsoport_Id.Value = Sablonok_KodCsoportKod;
        //search.KodCsoport_Id.Operator = Query.Operators.equals;

        //if (Search.IsSearchObjectInSession(Page, typeof(KRT_KodTarakSearch)) == false)
        //{
        //    Search.SetSearchObject(Page, search);
        //}

        //search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        //List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
        //    Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);
        Result res = service.GetAllByKodcsoportKod(ExecParam, Sablonok_KodCsoportKod);

        UI.GridViewFill(SablonokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void SablonokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbBelso", "Belso");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void SablonokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeSablonok);

        ListHeader1.RefreshPagerLabel();
    }

    private void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        SablonokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeSablonok);
        SablonokGridViewBind();
    }

    protected void SablonokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SablonokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("SablonokForm.aspx"
               , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("SablonokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SablonokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            string tableName = "KRT_KodTarak";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, SablonokGridView.ClientID);

            DokumentumSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Contentum.eRecord.Utility.Constants.Startup.Sablonok
                , Defaults.PopupWidth, Defaults.PopupHeight, DokumentumUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }


    protected void SablonokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    SablonokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(SablonokGridView));
                    break;
            }
        }

    }

    private void deleteSelectedSablon()
    {
        if (FunctionRights.GetFunkcioJog(Page, "SablonokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SablonokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void SablonokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SablonokGridViewBind(e.SortExpression, UI.GetSortToGridView("SablonokGridView", ViewState, e.SortExpression));
    }

    //private void RevalidateSelectedSablonok()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "SablonokRevalidate"))
    //    {
    //        List<string> selectedItemsList = ui.GetGridViewSelectedRows(SablonokGridView, EErrorPanel1, ErrorUpdatePanel);
    //        KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();

    //        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
    //        Result res = new Result();
    //        Result resError = new Result();
    //        int errorCount = 0;
    //        for (int i = 0; i < selectedItemsList.Count; i++)
    //        {
    //            xpm.Record_Id = selectedItemsList[i];
    //            res = service.Revalidate(xpm);
    //            if (res.IsError)
    //            {
    //                resError = res;
    //                errorCount++;
    //            }
    //        }

    //        if (errorCount > 0)
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
    //            if (errorCount > 1)
    //            {
    //                EErrorPanel1.Header = EErrorPanel1.Header + " (Hib�k sz�ma: " + errorCount.ToString() + ")";
    //            }
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    #endregion

    #region Mapping ListHeader Function Button Events

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedSablon();
            SablonokGridViewBind();
        }
        else if (e.CommandName == CommandName.Revalidate)
        {
            //RevalidateSelectedSablonok();
            SablonokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(SablonokGridView));
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (SablonokGridView.SelectedIndex == -1)
        {
            return;
        }

        string DokumentumId = UI.GetGridViewSelectedRecordId(SablonokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, DokumentumId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string SablonId)
    {
        //BUG 6198
        //Az ActiveTab -ban val�j�ban az adott poz�ci�n l�v� tab adatai vannak, teh�t az elrejtett tabok miatt l�tre kell hozni egy list�t a l�that� tabokr�l, �s azok k�z�tt kell megkeresni a tabindexet
        switch (sender.GetRealActiveTab().TabIndex)
        {
            case 0:
                DokumentumokGridViewBind(SablonId);
                DokumentumLista.PartnerId = SablonId;
                DokumentumPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.GetRealActiveTab().TabIndex)
        {
            case 0:
                ui.GridViewClear(DokumentumLista.GridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        DokumentumSubListHeader.RowCount = RowCount;
    }


    #endregion

    #region Sablonok Detail

   

    //Data Binding(2)
    protected void DokumentumokGridViewBind(String SablonId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("DokumentumokLista.GridView", ViewState, "KRT_Dokumentumok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("DokumentumokLista.GridView", ViewState, SortDirection.Descending);

        DokumentumokGridViewBind(SablonId, sortExpression, sortDirection);
    }

    protected void DokumentumokGridViewBind(String SablonId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(SablonId))
        {
            Contentum.eAdmin.Service.KRT_KodTarakService krt_KodtarakService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam execParamSablon = UI.SetExecParamDefault(Page);
            execParamSablon.Record_Id = SablonId;

            //LZS - BLG_51 - Sablon
            KRT_KodTarak Sablon = (KRT_KodTarak)krt_KodtarakService.Get(execParamSablon).Record;
            string SablonDokumentumId = Sablon.Obj_Id;

            ExecParam execParamDokumentum = UI.SetExecParamDefault(Page);
            //execParamDokumentum.Record_Id = SablonDokumentumId;

            Contentum.eAdmin.Service.KRT_DokumentumokService krt_DokumentumokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch krt_DokumentumSearch = new KRT_DokumentumokSearch();
            krt_DokumentumSearch.Id.Value = SablonDokumentumId;
            krt_DokumentumSearch.Id.Operator = Query.Operators.equals;


            //KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)krt_DokumentumokService.Get(execParamDokumentum).Record;
            Result res = krt_DokumentumokService.GetAll(execParamDokumentum, krt_DokumentumSearch);

            if (!res.IsError)
            {
                DokumentumLista.ClearDokumentElements();
                UI.GridViewFill(DokumentumLista.GridView, res, DokumentumSubListHeader, EErrorPanel1, ErrorUpdatePanel);
                UI.SetTabHeaderRowCountText(res, headerDokumentum);

                ui.SetClientScriptToGridViewSelectDeSelectButton(DokumentumLista.GridView);
            }
        }
        else
        {
            ui.GridViewClear(DokumentumLista.GridView);
        }
    }

    //SubListHeader f�ggv�nyei(4)
    private void DokumentumSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedDokumentum();
            DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView));
        }
    }

    //private void deleteSelectedDokumentum()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "SablonDokumentumInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(DokumentumLista.GridView, EErrorPanel1, ErrorUpdatePanel);
    //        KRT_Partner_DokumentumokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_Partner_DokumentumokService();
    //        List<ExecParam> execParams = new List<ExecParam>();
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            string dokumentumId = deletableItemsList[i];
    //            if (Partner_Dokumentumok_Ids.ContainsKey(dokumentumId))
    //            {
    //                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //                execParam.Record_Id = Partner_Dokumentumok_Ids[dokumentumId];
    //                execParams.Add(execParam);
    //            }
    //        }
    //        Result result = service.MultiInvalidate(execParams.ToArray());
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    private void DokumentumSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView));
    }

    private void DokumentumSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(DokumentumSubListHeader.RowCount);
        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void DokumentumUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainer1.ActiveTab.Equals(DokumentumTabPanel))
                    {
                        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView));
                    }
                    ActiveTabRefreshDetailList(DokumentumTabPanel);
                    break;
            }
        }
    }

    protected void DokumentumokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = DokumentumLista.GridView.PageIndex;

        DokumentumLista.GridView.PageIndex = DokumentumSubListHeader.PageIndex;

        if (prev_PageIndex != DokumentumLista.GridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, DokumentumCPE);
            DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(DokumentumSubListHeader.Scrollable, DokumentumCPE);
        }

        DokumentumSubListHeader.PageCount = DokumentumLista.GridView.PageCount;
        DokumentumSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(DokumentumLista.GridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(DokumentumLista.GridView);
    }

    protected void DokumentumokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (sortExpression == "TipusKodTarak.Nev")
        {
            sortExpression = "Tipus_Nev";
        }
        else if (sortExpression == "FormatumKodTarak.Nev")
        {
            sortExpression = "Formatum_Nev";
        }

        DokumentumokGridViewBind(UI.GetGridViewSelectedRecordId(SablonokGridView), sortExpression, UI.GetSortToGridView("DokumentumokLista.GridView", ViewState, sortExpression));
    }

    #endregion
}
