<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="SablonokForm.aspx.cs" Inherits="SablonokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KodtarakFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelKodcsoport" runat="server" Text="K�dcsoport:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc7:KodcsoportokTextBox ID="KodcsoportokTextBox1" runat="server"></uc7:KodcsoportokTextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelKod" runat="server" Text="K�d:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxKod" runat="server" MaxLength="64" />
                        </td>
                    </tr>
                    <%--<tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelRovidNev" runat="server" Text="R�vid n�v:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <asp:TextBox ID="textRovidNev"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                            </td>
                    </tr>--%>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelEgyeb" runat="server" Text="Egy�b:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="TextBoxEgyeb" CssClass="mrUrlapInput" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelSorrend" runat="server" Text="Sorrend:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textSorrend" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="filteredExtenderSorrend" runat="server"
                                FilterType="Numbers" TargetControlID="textSorrend">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelPartner" runat="server" Text="Szervezet:" Visible="false"></asp:Label>
                            <asp:Label ID="labelFelhasznalo" runat="server" Text="Felhaszn�l�:" Visible="false"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc8:CsoportTextBox ID="CsoportTextBox" runat="server" AjaxEnabled="true" Visible="false"></uc8:CsoportTextBox>
                            <uc8:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox1" runat="server" Visible="false"></uc8:FelhasznaloCsoportTextBox>
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>

