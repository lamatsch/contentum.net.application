using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Web.UI;

public partial class SablonokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        KodcsoportokTextBox1.ReadOnly = true;
        CsoportTextBox.ReadOnly = true;
        FelhasznaloCsoportTextBox1.ReadOnly = true;
        requiredTextBoxNev.ReadOnly = true;
        requiredTextBoxKod.ReadOnly = true;
        //textRovidNev.ReadOnly= true;
        textSorrend.ReadOnly = true;
        TextBoxEgyeb.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;


        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelKodcsoport.CssClass = "mrUrlapInputWaterMarked";
        labelKod.CssClass = "mrUrlapInputWaterMarked";
        //labelRovidNev.CssClass = "mrUrlapInputWaterMarked";
        labelSorrend.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelFelhasznalo.CssClass = "mrUrlapInputWaterMarked";
        labelEgyeb.CssClass = "mrUrlapInputWaterMarked";

    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        KodcsoportokTextBox1.ReadOnly = true;
        requiredTextBoxKod.TextBox.ReadOnly = true;

        labelKodcsoport.CssClass = "mrUrlapInputWaterMarked";
        labelKod.CssClass = "mrUrlapInputWaterMarked";;
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Sablonok" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_KodTarak krt_KodTarak = (KRT_KodTarak)result.Record;
                    LoadComponentsFromBusinessObject(krt_KodTarak);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

            }
        }
        else if (Command == CommandName.New)
        {
            //string kodcsoportID = Request.QueryString.Get("KodcsoportID");
            string SABLONOK_kodcsoportID = "5B9EDE81-895B-40A6-AB75-1B6A7DD766D4";

            if (!String.IsNullOrEmpty(SABLONOK_kodcsoportID))
            {
                KodcsoportokTextBox1.Id_HiddenField = SABLONOK_kodcsoportID;
                KodcsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
                KodcsoportokTextBox1.Enabled = false;
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_KodTarak"></param>
    private void LoadComponentsFromBusinessObject(KRT_KodTarak krt_KodTarak)
    {   
        KodcsoportokTextBox1.Id_HiddenField = krt_KodTarak.KodCsoport_Id;
        KodcsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        requiredTextBoxKod.Text = krt_KodTarak.Kod;
        requiredTextBoxNev.Text = krt_KodTarak.Nev;
        //textRovidNev.Text = krt_KodTarak.RovidNev;
        textSorrend.Text = krt_KodTarak.Sorrend;
        TextBoxEgyeb.Text = krt_KodTarak.Egyeb;
        ErvenyessegCalendarControl1.ErvKezd = krt_KodTarak.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_KodTarak.ErvVege;


        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_KodTarak.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_KodTarak.Base);
        KRT_KodCsoportokService kcsService = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
        Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch kcsSearch = new Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch();
        kcsSearch.Kod.Value = "SPEC_SZERVEK";
        kcsSearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result reskcs = kcsService.GetAll(execParam, kcsSearch);
        labelPartner.Visible = false;
        CsoportTextBox.Visible = false;
        if (reskcs.Ds.Tables[0].Rows.Count == 1)
        {
            System.Data.DataSet ds = reskcs.Ds;
            String KodcsoportId = ds.Tables[0].Rows[0]["Id"].ToString();
            if (KodcsoportId.CompareTo(krt_KodTarak.KodCsoport_Id) == 0)
            {
                if (krt_KodTarak.Kod ==  KodTarak.SPEC_SZERVEK.SKONTROIRATTAROS)
                {
                    CsoportTextBox.Visible = false;
                    labelPartner.Visible = false;
                    FelhasznaloCsoportTextBox1.Visible = true;
                    labelFelhasznalo.Visible = true;

                    FelhasznaloCsoportTextBox1.Id_HiddenField = krt_KodTarak.Obj_Id;
                    FelhasznaloCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);                
                }
                else
                {
                    CsoportTextBox.Visible = true;
                    labelPartner.Visible = true;
                    FelhasznaloCsoportTextBox1.Visible = false;
                    labelFelhasznalo.Visible = false ;

                    CsoportTextBox.SzervezetCsoport = true;
                    CsoportTextBox.Id_HiddenField = krt_KodTarak.Obj_Id;
                    CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                }
            }
        }

    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_KodTarak GetBusinessObjectFromComponents()
    {
        KRT_KodTarak krt_KodTarak = new KRT_KodTarak();
        krt_KodTarak.Updated.SetValueAll(false);
        krt_KodTarak.Base.Updated.SetValueAll(false);

        krt_KodTarak.KodCsoport_Id = KodcsoportokTextBox1.Id_HiddenField;
        krt_KodTarak.Updated.KodCsoport_Id = pageView.GetUpdatedByView(KodcsoportokTextBox1);
        if (FelhasznaloCsoportTextBox1.Visible)
        {
            krt_KodTarak.Obj_Id = FelhasznaloCsoportTextBox1.Id_HiddenField;
            krt_KodTarak.Updated.Obj_Id = pageView.GetUpdatedByView(FelhasznaloCsoportTextBox1);
        }
        else
        {
            if (string.IsNullOrEmpty(CsoportTextBox.Id_HiddenField) && !string.IsNullOrEmpty(CsoportTextBox.Text))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, /*Resources.Error.Error*/Resources.Error.ErrorCode, Resources.Error.ErrorCode/*Resources.Error.UI_Error_Szervezet_Kivalasztas_Soran*/);
                return null;
            }

            krt_KodTarak.Obj_Id = CsoportTextBox.Id_HiddenField;
            krt_KodTarak.Updated.Obj_Id = pageView.GetUpdatedByView(CsoportTextBox);
        }
        krt_KodTarak.Kod = requiredTextBoxKod.Text;
        krt_KodTarak.Updated.Kod = pageView.GetUpdatedByView(requiredTextBoxKod);
        krt_KodTarak.Nev = requiredTextBoxNev.Text;
        krt_KodTarak.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        //krt_KodTarak.RovidNev = textRovidNev.Text;
        //krt_KodTarak.Updated.RovidNev = pageView.GetUpdatedByView(textRovidNev);
        krt_KodTarak.Sorrend = textSorrend.Text;
        krt_KodTarak.Egyeb = TextBoxEgyeb.Text;
        krt_KodTarak.Updated.Sorrend = pageView.GetUpdatedByView(textSorrend);
        krt_KodTarak.Updated.Egyeb = pageView.GetUpdatedByView(TextBoxEgyeb);
        
        //krt_KodTarak.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_KodTarak.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_KodTarak.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_KodTarak.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_KodTarak, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_KodTarak.Modosithato = BoolString.Yes;
        krt_KodTarak.Updated.Modosithato = true;

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_KodTarak.Base.Ver = FormHeader1.Record_Ver;
        krt_KodTarak.Base.Updated.Ver = true;

        return krt_KodTarak;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(KodcsoportokTextBox1);
            compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //compSelector.Add_ComponentOnClick(textRovidNev);
            compSelector.Add_ComponentOnClick(textSorrend);
            compSelector.Add_ComponentOnClick(TextBoxEgyeb);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Kodtar" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                            KRT_KodTarak krt_KodTarak = GetBusinessObjectFromComponents();

                            if (krt_KodTarak == null)
                                return;

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_KodTarak);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                //Cache friss�t�s:
                                Contentum.eUtility.KodTar_Cache.RefreshKodCsoportokCacheByKodcsoportKod(execParam, KodcsoportokTextBox1.Kodcsoport_Kod, Page.Cache);
                                // v�ltoz�s jelz�se eRecordba (Cache friss�t�shez):                                
                                //Contentum.eUtility.KodTarak.RefreshKodcsoportCacheByKodcsoportId_eRecordWebSite(Page, krt_KodTarak.KodCsoport_Id);


                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                                KRT_KodTarak krt_KodTarak = GetBusinessObjectFromComponents();

                                if (krt_KodTarak == null)
                                    return;

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_KodTarak);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    //Cache friss�t�s:
                                    Contentum.eUtility.KodTar_Cache.RefreshKodCsoportokCacheByKodcsoportKod(execParam, KodcsoportokTextBox1.Kodcsoport_Kod, Page.Cache);
                                    // v�ltoz�s jelz�se eRecordba (Cache friss�t�shez):                                
                                    //Contentum.eAdmin.Utility.KodTarak.RefreshKodcsoportCacheByKodcsoportId_eRecordWebSite(Page, krt_KodTarak.KodCsoport_Id);

                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page,true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
