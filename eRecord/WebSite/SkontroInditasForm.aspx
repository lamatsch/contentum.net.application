<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="SkontroInditasForm.aspx.cs" Inherits="SkontroInditasForm" Title="Skontro" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
    <%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc10" %>

<%@ Register Src="~/eRecordComponent/TeljessegEllenorzesResultPanel.ascx" TagName="TeljessegEllenorzesReultPanel" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>        
    <input type="hidden" id="skontroVegeHiddenField" runat="server"/>
    <script type="text/javascript">
        function GetCurrentDate_LikeCSharpFormat()
        {
            var curDate = new Date();
            return curDate.getFullYear().toString() + '.' + ((curDate.getMonth()+1).toString().length==1?'0'+(curDate.getMonth()+1).toString():(curDate.getMonth()+1).toString()) + '.' + (curDate.getDate().toString().length==1?'0'+curDate.getDate().toString():curDate.getDate().toString()) + '.';
        }
        
        function Validate_SkontrobanMarad_Dates_ClientSide()
        {
            var skontroVegeDate = document.getElementById('ctl00_ContentPlaceHolder1_SkontroVege_CalendarControl_DateTextBox').value.replace(/ /g,'');
            var curDate = GetCurrentDate_LikeCSharpFormat();

            if (skontroVegeDate <= curDate)
            {
                var origSkontroVege = document.getElementById('ctl00_ContentPlaceHolder1_skontroVegeHiddenField').value.replace(/ /g,'');
                if (origSkontroVege > curDate)
                {
                    if (confirm('A d�tum nem lehet a mai nap, vagy ann�l kor�bbi! Megtartja az eredeti skontr� v�ge d�tum�t (' + origSkontroVege + ')?'))
                    {
                        document.getElementById('ctl00_ContentPlaceHolder1_SkontroVege_CalendarControl_DateTextBox').value = origSkontroVege;
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    alert('A d�tum nem lehet a mai nap, vagy ann�l kor�bbi!');
                    return false;
                }
            }
            else
                return true;
        }
        
        function Validate_SkontrobolKivesz_Dates_ClientSide()
        {
            var skontroVegeDate = document.getElementById('ctl00_ContentPlaceHolder1_SkontroVege_CalendarControl_DateTextBox').value.replace(/ /g,'');;
            var curDate = GetCurrentDate_LikeCSharpFormat();
            
            if (skontroVegeDate > curDate)
            {
                alert('A d�tum nem lehet a mai napn�l k�s�bbi!');
                return false;
            }
            else
                return true;
        }
        
        
    </script>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <uc:TeljessegEllenorzesReultPanel runat="server" ID="TeljessegEllenorzesResultPanel1" />    
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                        <br />
                        <asp:Label ID="Label_Ugyiratok" runat="server" Text="�gyiratok:" Visible="false"
                            CssClass="GridViewTitle"></asp:Label>
                        <br />
                        <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            OnRowDataBound="UgyUgyiratokGridView_RowDataBound" AllowSorting="False" AutoGenerateColumns="False"
                            DataKeyNames="Id">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    <%-- <HeaderTemplate>
                                    <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                    </div>
                                </HeaderTemplate>--%>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                <HeaderStyle Width="25px" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:CommandField>--%>
                                <asp:BoundField DataField="Foszam_Merge" HeaderText="F�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SkontrobaDat" HeaderText="Skontr�ba" SortExpression="SkontrobaDat">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <%--<asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                    
                                </asp:BoundField>--%>
                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>
                    
                    <uc10:UgyiratMasterAdatok id="UgyiratMasterAdatok1" runat="server">
                                </uc10:UgyiratMasterAdatok>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr class="urlapSor" runat="server" id="tr_skontroKezdete">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="label5" runat="server" Text="Skontro kezdete:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <uc5:CalendarControl ID="SkontrobaDat_CalendarControl" runat="server" Validate="false"
                                                    ReadOnly="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="label2" runat="server" Text="Skontro v�ge:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <uc5:CalendarControl ID="SkontroVege_CalendarControl" runat="server" Validate="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr class="urlapSor" runat="server" id="tr_skontroOka">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="label7" runat="server" Text="Skontro oka:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="SkontroOka_TextBox" runat="server" CssClass="mrUrlapInput" Rows="3"
                                                    TextMode="MultiLine"></asp:TextBox>
                                                <%--CR3407--%>
                                                  <uc10:KodtarakDropDownList ID="SkontroOka_KodtarakDropDownList" runat="server"></uc10:KodtarakDropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <asp:Panel runat="server" ID="panelMegjegyzes" Visible="false">
                        <eUI:eFormPanel ID="eFormPanelMegjegyzes" runat="server" style="padding-top:10px;">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textMegjegyzes" runat="server" CssClass="mrUrlapInput" Rows="3"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </asp:Panel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                    <table style="width: 60%;">
                        <tr>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageApprove" runat="server" ImageUrl="~/images/hu/ovalgomb/jovahagyas.gif"
                                    onmouseover="swapByName(this.id,'jovahagyas2.gif')" onmouseout="swapByName(this.id,'jovahagyas.gif')"
                                    CommandName="Approve" Visible="False" OnClick="ImageApprove_Click" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageReject" runat="server" ImageUrl="~/images/hu/ovalgomb/elutasitas.gif"
                                    onmouseover="swapByName(this.id,'elutasitas2.gif')" onmouseout="swapByName(this.id,'elutasitas.gif')"
                                    CommandName="Reject" Visible="False" OnClick="ImageReject_Click" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageRevoke" runat="server" ImageUrl="~/images/hu/ovalgomb/visszavonas.gif"
                                    onmouseover="swapByName(this.id,'visszavonas2.gif')" onmouseout="swapByName(this.id,'visszavonas.gif')"
                                    CommandName="Revoke" Visible="False" OnClick="ImageRevoke_Click" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="1" ID="ImageSkontrobolKiad" runat="server" ImageUrl="~/images/hu/ovalgomb/skontrobol_kiad.gif"
                                    onmouseover="swapByName(this.id,'skontrobol_kiad2.gif')" onmouseout="swapByName(this.id,'skontrobol_kiad.gif')"
                                    OnClick="ImageSkontrobolKivesz_Click" CommandName="SkontrobolKiad" Visible="False" />
                            </td>                            
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="1" ID="ImageSkontrobanMarad" runat="server" ImageUrl="~/images/hu/ovalgomb/skontroban_marad.gif"
                                    onmouseover="swapByName(this.id,'skontroban_marad2.gif')" onmouseout="swapByName(this.id,'skontroban_marad.gif')"
                                    OnClick="ImageSkontrobanMarad_Click" CommandName="SkontrobanMarad" Visible="False" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="1" ID="ImageSkontrobolKivesz" runat="server" ImageUrl="~/images/hu/ovalgomb/skontrobol_kivesz.gif"
                                    onmouseover="swapByName(this.id,'skontrobol_kivesz2.gif')" onmouseout="swapByName(this.id,'skontrobol_kivesz.gif')"
                                    OnClick="ImageSkontrobolKivesz_Click" CommandName="SkontrobolKivesz" Visible="False" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="5" ID="ImageCancel" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                    onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                    OnClientClick="window.close(); return false;" CommandName="Cancel" Visible="False" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
