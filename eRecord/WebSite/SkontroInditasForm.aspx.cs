/* $Header: SkontroInditasForm.aspx.cs, 25, 2017. 08. 01. 9:55:53, N�meth Krisztina$ 
 */
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class SkontroInditasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private SkontroTipus Mode = SkontroTipus.SkontrobaAdas;

    private string UgyiratId = "";
    private string[] UgyiratokArray;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private UI ui = new UI();

    private const string FunkcioKod_UgyiratSkontroKezeles = "UgyiratSkontroKezeles";
    private const string FunkcioKod_UgyiratSkontrobaHelyezes = "UgyiratSkontrobaHelyezes";
    private const string FunkcioKod_UgyiratSkontrobolKivetel = "UgyiratSkontrobolKivetel";

    // CR3407
    private const string kcsKod_SKONTRO_OKA = "SKONTRO_OKA";
    private bool isSkontroOkaValaszthato = false;

    private enum SkontroTipus
    {
        SkontrobaAdas,
        SkontrobolKivetel,
        SkontroJovahagyasa,
        SkontoVisszavonasa,
        KiadasSkontroIrattarbol
    }

    private string CheckMegjegyzesJavaScript(TextBox tb, string msgIfEmpty)
    {
        string js = @"
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };

var megjegyzes = $get('" + tb.ClientID + @"');
if (megjegyzes && megjegyzes.value.trim() == '')
{
    alert('" + msgIfEmpty + @"');
    return false;
}
";
        return js;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        
        Command = Request.QueryString.Get(CommandName.Command);
        isSkontroOkaValaszthato=Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.SKONTRO_OKA_VALASZTHATO, false);
        string strMode = Request.QueryString.Get(QueryStringVars.Mode);

        if (strMode == CommandName.SkontrobolKivesz)
        {
            Mode = SkontroTipus.SkontrobolKivetel;
        }
        else if (strMode == CommandName.KiadasSkontroIrattarbol)
        {
            Mode = SkontroTipus.KiadasSkontroIrattarbol;
        }
        else
        {
            Mode = SkontroTipus.SkontrobaAdas;
        }
        
       
        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            case CommandName.UgyiratSkontrobaHelyezesJovahagyasa:
                if (!FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesJovahagyasa) &&
                    !FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesElutasitasa))
                {
                    FunctionRights.RedirectErrorPage(Page);
                }
                Mode = SkontroTipus.SkontroJovahagyasa;
                break;
            case CommandName.UgyiratSkontrobaHelyezesVisszavonasa:
                if (!FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesVisszavonasa) &&
                    !FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontroKezeles))
                {
                    FunctionRights.RedirectErrorPage(Page);
                }
                Mode = SkontroTipus.SkontoVisszavonasa;
                break;
            case CommandName.KiadasSkontroIrattarbol:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KiadasSkontroIrattarbol");
                Mode = SkontroTipus.KiadasSkontroIrattarbol;
                break;
            default:
                if (Mode == SkontroTipus.SkontrobolKivetel && !FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontrobolKivetel)
                    || Mode == SkontroTipus.SkontrobaAdas && !FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontrobaHelyezes))
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratSkontroKezeles);
                }
                break;
        }



        //UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session[Constants.SelectedUgyiratIds] != null)
            {
                UgyiratId = Session[Constants.SelectedUgyiratIds].ToString();
            }
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);


        if (!String.IsNullOrEmpty(UgyiratId))
        {
            if (Session[Constants.SelectedUgyiratIds] != null && Session[Constants.SelectedUgyiratIds].GetType() == new List<string>().GetType())
            {
                UgyiratokArray = (Session[Constants.SelectedUgyiratIds] as List<string>).ToArray();
            }
            else
            {
                UgyiratokArray = UgyiratId.Split(',');
            }
        }


        if (!IsPostBack)
        {
            LoadFormComponents();            
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;
         
        // Fejl�c c�m be�ll�t�s:
        switch (Mode)
        {
            case SkontroTipus.SkontrobolKivetel:
                FormHeader1.HeaderTitle = Resources.Form.SkontrobolKivetelFormHeaderTitle;
                break;
            case SkontroTipus.SkontroJovahagyasa:
                FormHeader1.HeaderTitle = Resources.Form.SkontroJovahagyasaFormHeaderTitle;
                // elutas�t�sn�l k�telez� a vezet�i megjegyz�s
                ImageReject.OnClientClick = CheckMegjegyzesJavaScript(textMegjegyzes, Resources.Form.UI_ElutasitasMegjegyzesKotelezo);
                break;
            case SkontroTipus.SkontoVisszavonasa:
                FormHeader1.HeaderTitle = Resources.Form.SkontroVisszavonasFormHeaderTitle;
                break;
            case SkontroTipus.KiadasSkontroIrattarbol:
                FormHeader1.HeaderTitle = Resources.Form.KiadasFormSkontroIrattarHeaderTitle;
                break;
            default:
                FormHeader1.HeaderTitle = Resources.Form.SkontroFormHeaderTitle;
                break;
        }
                
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        // skontr� iratt�rb�l kiad�sn�l nincs skontr�ban marad gomb
        if (Mode == SkontroTipus.KiadasSkontroIrattarbol)
        {
            ImageSkontrobanMarad.Visible = false;

            if (this.SkontrobolKiadasUgyintezesre())
            {
                ImageSkontrobolKivesz.Visible = true;
            }
            else
            {
                ImageSkontrobolKivesz.Visible = false;
                ImageSkontrobolKiad.Visible = true;

                EFormPanel1.Visible = false;
            }
            
            SkontroVege_CalendarControl.ReadOnly = true;
            
        }
        else
        {
            ImageSkontrobanMarad.OnClientClick = " if (!Validate_SkontrobanMarad_Dates_ClientSide()) {return false;} ";
            ImageSkontrobanMarad.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageSkontrobanMarad);
        }

        // Javascriptes ellen�rz�s: skontr� v�ge k�telez� a skontr�b�l kiv�telhez
        ImageSkontrobolKivesz.OnClientClick = " if ($get('" + SkontroVege_CalendarControl.TextBox.ClientID
            + "').value == '') { alert('" + Resources.Error.UISkontroVegeNincsMegadva + "');return false;} ";
        ImageSkontrobolKivesz.OnClientClick += " if (!Validate_SkontrobolKivesz_Dates_ClientSide()) {return false;} ";
        ImageSkontrobolKivesz.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageSkontrobolKivesz);

    }


    private bool SkontrobolKiadasUgyintezesre()
    {
        List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

        // ha nincs kiv�lasztva �gyirat, nem h�vjuk meg a webszerv�zt
        if (selectedItemsList_ugyirat.Count > 0)
        {
            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();

            EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();

            search.UgyUgyirat_Id.Value = Search.GetSqlInnerString(selectedItemsList_ugyirat.ToArray());
            search.UgyUgyirat_Id.Operator = Query.Operators.inner;

            search.FelhasznalasiCel.Value = "U";
            search.FelhasznalasiCel.Operator = Query.Operators.equals;

            search.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto + "," + KodTarak.IRATTARIKIKEROALLAPOT.Nyitott;
            search.Allapot.Operator = Query.Operators.inner;

            Result result = service.GetAll(UI.SetExecParamDefault(Page), search);

            if (!result.IsError && result.Ds.Tables[0].Rows.Count > 0)
                return true;
        }

        return false;
    }

    private void LoadFormComponents()
    {
        if (String.IsNullOrEmpty(UgyiratId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            // SkontrobaAdas m�g nem t�meges, m�shogy kell kezelni:
            switch (Mode)
            {
                case SkontroTipus.SkontrobaAdas:
                    {
                        #region SkontrobaAdas (nem t�meges)
                        // CR3407
                        if (isSkontroOkaValaszthato)
                        {
                            SkontroOka_TextBox.Visible = false;
                            SkontroOka_KodtarakDropDownList.Visible = true;
                            SkontroOka_KodtarakDropDownList.FillAndSetEmptyValue(kcsKod_SKONTRO_OKA, FormHeader1.ErrorPanel);
                        } else
                        {
                            SkontroOka_TextBox.Visible = true;
                            SkontroOka_KodtarakDropDownList.Visible = false;
                        }
                        UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                        EREC_UgyUgyiratok erec_UgyUgyiratok =
                            UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                        // �gyirat verzi� elment�se:
                        FormHeader1.Record_Ver = erec_UgyUgyiratok.Base.Ver;

                        if (erec_UgyUgyiratok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            ExecParam execParam = UI.SetExecParamDefault(Page);
                            ErrorDetails errorDetail = null;

                            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);

                            //if (Mode == SkontroTipus.SkontrobaAdas)
                            //{
                            #region Skontr�ba helyez�s
                            // Ellen�rz�s, Skontr�ba helyezhet�-e:
                            if (Ugyiratok.SkontrobaHelyezheto(ugyiratStatusz, execParam, out errorDetail) == false)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                    ResultError.CreateNewResultWithErrorCode(52171, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }

                            SkontrobaDat_CalendarControl.Text = DateTime.Now.ToShortDateString();

                            #region TeljessegEllenorzes
                            
                            // Rendszerparam�ter szerint k�telez�-e teljesnek lennie az �gyiratnak skontr�ba ad�s el�tt?
                            bool kotelezoaTeljessegEllenorzes =
                                Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page),Rendszerparameterek.TELJESSEGELLENORZES_SKONTROELOTT_ENABLED);
                            if (kotelezoaTeljessegEllenorzes)
                            {
                                bool passed = TeljessegEllenorzesResultPanel1.Ellenorzes(UgyiratId, false);
                                if (!passed)
                                {
                                    MainPanel.Visible = false;
                                    FormFooter1.SaveEnabled = false;
                                    FormFooter1.Visible = false;
                                }
                            }
                            else
                            {
                                // csak figyelmeztet� �zenet, ha nem ok:
                                ASP.erecordcomponent_teljessegellenorzesresultpanel_ascx.DisplayWarningMessage(Page, erec_UgyUgyiratok, FormHeader1.ErrorPanel, null);
                            }

                            #endregion
                            #endregion
                            //}

                        }
                        #endregion
                        break;
                    }
                case SkontroTipus.SkontrobolKivetel:
                case SkontroTipus.KiadasSkontroIrattarbol:
                    {
                        UgyiratokListPanel.Visible = true;
                        UgyiratMasterAdatok1.Visible = false;

                        DataSet ds = FillUgyiratokGridView();

                        SkontroVege_CalendarControl.Text = DateTime.Now.ToShortDateString();

                        // ha csak egy rekordr�l van sz�:
                        if (ds != null && ds.Tables[0].Rows.Count == 1)
                        {
                            DataRow row = ds.Tables[0].Rows[0];

                            SkontrobaDat_CalendarControl.Text = row["SkontrobaDat"].ToString();

                            //// Skontr�b�l kiv�teln�l a mai napot �rjuk, nem pedig az eredeti skontr� v�g�t.
                            string skontroVege = row["SkontroVege"].ToString();
                            if (!String.IsNullOrEmpty(skontroVege))
                            {
                                //SkontroVege_CalendarControl.Text = skontroVege;
                                skontroVegeHiddenField.Value = DateTime.Parse(skontroVege).ToShortDateString();
                            }

                            // CR3407
                            //SkontroOka_TextBox.Text = row["SkontroOka"].ToString();
                            if (isSkontroOkaValaszthato)
                            {
                                SkontroOka_TextBox.Visible = false;
                                SkontroOka_KodtarakDropDownList.Visible = true;
                                SkontroOka_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_SKONTRO_OKA, row["SkontroOka_Kod"].ToString(),FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                SkontroOka_TextBox.Visible = true;
                                SkontroOka_KodtarakDropDownList.Visible = false;
                                SkontroOka_TextBox.Text = row["SkontroOka"].ToString();
                            }

                            // A Rendben, skontr�ban marad csak akkor enged�lyezett, ha egy �gyirat van csak
                            ImageSkontrobanMarad.Visible = true;

                            // �gyirat verzi� elment�se:
                            FormHeader1.Record_Ver = row["Ver"].ToString();
                        }
                        else
                        {
                            // skontr�ba ad�s d�tum�t, skontr� ok�t elt�ntetj�k:
                            tr_skontroOka.Visible = false;
                            tr_skontroKezdete.Visible = false;
                        }


                        FormFooter1.Visible = false;
                        ImageSkontrobolKivesz.Visible = true;
                        ImageCancel.Visible = true;
                        break;
                    }
                case SkontroTipus.SkontroJovahagyasa:
                    {
                        UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                        EREC_UgyUgyiratok erec_UgyUgyiratok =
                            UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                        // �gyirat verzi� elment�se:
                        FormHeader1.Record_Ver = erec_UgyUgyiratok.Base.Ver;

                        if (erec_UgyUgyiratok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            ExecParam execParam = UI.SetExecParamDefault(Page);
                            ErrorDetails errorDetail = null;

                            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);

                            //Ellen�rz�s, J�v�hagyhat�/elutas�that�:
                            if (!Ugyiratok.SkontroJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                    ResultError.CreateNewResultWithErrorCode(52175, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }

                            SkontrobaDat_CalendarControl.Text = erec_UgyUgyiratok.SkontrobaDat;
                            SkontrobaDat_CalendarControl.ReadOnly = true;
                            SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
                            SkontroVege_CalendarControl.ReadOnly = true;
                            // CR3407
                            //SkontroOka_TextBox.Text = erec_UgyUgyiratok.SkontroOka;
                            //SkontroOka_TextBox.ReadOnly = true;
                            if (isSkontroOkaValaszthato)
                            {
                                SkontroOka_TextBox.Visible = false;
                                SkontroOka_KodtarakDropDownList.Visible = true;
                                SkontroOka_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_SKONTRO_OKA, erec_UgyUgyiratok.SkontroOka_Kod.ToString(), FormHeader1.ErrorPanel);
                                SkontroOka_KodtarakDropDownList.ReadOnly = true;
                            }
                            else
                            {
                                SkontroOka_TextBox.Visible = true;
                                SkontroOka_KodtarakDropDownList.Visible = false;
                                SkontroOka_TextBox.Text = erec_UgyUgyiratok.SkontroOka;
                                SkontroOka_TextBox.ReadOnly = true;
                            }
                            FormFooter1.Visible = false;
                            ImageApprove.Visible = true;
                            ImageReject.Visible = true;
                            ImageCancel.Visible = true;
                            panelMegjegyzes.Visible = true;

                        }
                        break;
                    }

                case SkontroTipus.SkontoVisszavonasa:
                    {
                        UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                        EREC_UgyUgyiratok erec_UgyUgyiratok =
                            UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                        // �gyirat verzi� elment�se:
                        FormHeader1.Record_Ver = erec_UgyUgyiratok.Base.Ver;

                        if (erec_UgyUgyiratok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            ExecParam execParam = UI.SetExecParamDefault(Page);
                            ErrorDetails errorDetail = null;

                            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);

                            //Ellen�rz�s, J�v�hagyhat�/elutas�that�:
                            if (!Ugyiratok.SkontroVisszavonhato(ugyiratStatusz, execParam, out errorDetail))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                    ResultError.CreateNewResultWithErrorCode(52174, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }

                            SkontrobaDat_CalendarControl.Text = erec_UgyUgyiratok.SkontrobaDat;
                            SkontrobaDat_CalendarControl.ReadOnly = true;
                            SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
                            SkontroVege_CalendarControl.ReadOnly = true;
                            // CR3407
                            //SkontroOka_TextBox.Text = erec_UgyUgyiratok.SkontroOka;
                            //SkontroOka_TextBox.ReadOnly = true;
                            if (isSkontroOkaValaszthato)
                            {
                                SkontroOka_TextBox.Visible = false;
                                SkontroOka_KodtarakDropDownList.Visible = true;
                                SkontroOka_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_SKONTRO_OKA, erec_UgyUgyiratok.SkontroOka_Kod.ToString(), FormHeader1.ErrorPanel);
                                SkontroOka_KodtarakDropDownList.ReadOnly = true;
                            }
                            else
                            {
                                SkontroOka_TextBox.Visible = true;
                                SkontroOka_KodtarakDropDownList.Visible = false;
                                SkontroOka_TextBox.Text = erec_UgyUgyiratok.SkontroOka;
                                SkontroOka_TextBox.ReadOnly = true;
                            }
                            FormFooter1.Visible = false;
                            ImageRevoke.Visible = true;
                            ImageCancel.Visible = true;

                        }
                        break;
                    }
            }
        }

       
    }

    private DataSet FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_skontrobolKivehetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_skontrobolNEMkivehetok = UgyiratokArray.Length - count_skontrobolKivehetok;

        if (count_skontrobolNEMkivehetok > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_SkontrobolKivehetokWarning, count_skontrobolNEMkivehetok);
            Panel_Warning_Ugyirat.Visible = true;
        }

        return ds;
    }


    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < UgyiratokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + UgyiratokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + UgyiratokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }



    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (Mode == SkontroTipus.KiadasSkontroIrattarbol)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKiadasSkontroIrattarbol(e, Page);
        }
        else
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckSkontrobolKiveheto(e, Page);
        }

        //CheckBox cb = (CheckBox)e.Row.FindControl("check");
        //if (cb != null)
        //    cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
        //        "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
        //        "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
        //        "; return true;");
    }



    public override void Load_ComponentSelectModul()
    {
        // TODO:

        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    protected void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontroKezeles)
                || Mode == SkontroTipus.SkontrobaAdas && FunctionRights.GetFunkcioJog(Page,FunkcioKod_UgyiratSkontrobaHelyezes)
                || Mode == SkontroTipus.SkontrobolKivetel && FunctionRights.GetFunkcioJog(Page,FunkcioKod_UgyiratSkontrobolKivetel))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    try
                    {
                        this.ValidateInputParameters();
                        String skontroHatarido = SkontroVege_CalendarControl.Text;
                        // CR3407
                        String skontroOka = String.Empty;
                        if (isSkontroOkaValaszthato)
                        {
                            skontroOka = SkontroOka_KodtarakDropDownList.SelectedValue;
                        } else
                            skontroOka = SkontroOka_TextBox.Text;

                        
                        // Skontr� ind�t�s:
                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result result = service.SkontroInditas(execParam, UgyiratId, skontroHatarido, skontroOka);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                    }
                    catch(FormatException fe)
                    {
                        ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                    }

                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }

    // Skontr�ban marad gombra kattint�s
    protected void ImageSkontrobanMarad_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontroKezeles)
            || FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontrobaHelyezes))
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            else
            {
                try
                {
                    this.ValidateInputParameters();
                    // Skontr� ok�nak �s skontr� v�g�nek elment�se:
                    EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
                    ugyirat.Updated.SetValueAll(false);
                    ugyirat.Base.Updated.SetValueAll(false);

                    ugyirat.Base.Ver = FormHeader1.Record_Ver;
                    ugyirat.Base.Updated.Ver = true;

                    // Skontro oka:
                    // CR3407
                    
                    if (isSkontroOkaValaszthato)
                    {
                        ugyirat.SkontroOka_Kod = SkontroOka_KodtarakDropDownList.SelectedValue;
                        ugyirat.Updated.SkontroOka_Kod = pageView.GetUpdatedByView(SkontroOka_KodtarakDropDownList);
                    }
                    else
                    {
                        ugyirat.SkontroOka = SkontroOka_TextBox.Text;
                        ugyirat.Updated.SkontroOka = pageView.GetUpdatedByView(SkontroOka_TextBox);
                    }
                    

                    // Skontro v�ge:
                    ugyirat.SkontroVege = SkontroVege_CalendarControl.Text;
                    ugyirat.Updated.SkontroVege = pageView.GetUpdatedByView(SkontroVege_CalendarControl);

                    // �gyirat UPDATE:
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = UgyiratId;

                    Result result = service.Update(execParam, ugyirat);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                    else
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
                catch (FormatException fe)
                {
                    ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
        
    }

    // Skontr�b�l kivesz gombra kattint�s
    // (Skontr�b�l kiv�tel t�meges m�velet)
    protected void ImageSkontrobolKivesz_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontroKezeles)
            || FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontrobolKivetel)
            || (Mode == SkontroTipus.KiadasSkontroIrattarbol && FunctionRights.GetFunkcioJog(Page, "KiadasSkontroIrattarbol")) )
        {
            List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
            //string[] UgyiratId_Array = new String[selectedItemsList_ugyirat.Count];
            //for (int i = 0; i < UgyiratId_Array.Length; i++)
            //{
            //    UgyiratId_Array[i] = selectedItemsList_ugyirat[i];
            //}
            string[] UgyiratId_Array = selectedItemsList_ugyirat.ToArray();

            if (selectedItemsList_ugyirat.Count == 0)
            {
                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINoSelectedRow);
                //ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            else
            {
                try
                {
                    this.ValidateInputParameters();
                    // Ellen�rz�s, skontr� v�ge ki van-e t�ltve, �s az a mai napn�l nem k�s�bbi:
                    if (String.IsNullOrEmpty(SkontroVege_CalendarControl.Text))
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINincsMegadvaMindenAdat);
                        return;
                    }

                    try
                    {
                        DateTime skontroVege_DateTime = DateTime.Parse(SkontroVege_CalendarControl.Text);
                        // ha a megadott skontr� v�ge nagyobb a mai d�tumn�l, hiba:
                        if (DateTime.Compare(skontroVege_DateTime, DateTime.Now) > 0)
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UIMaiNapUtaniDatum);
                            return;
                        }
                    }
                    catch
                    {
                        // hiba, rossz form�tum
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UIDateTimeFormatError);
                        return;
                    }

                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    //Result result = service.SkontrobolKivetel(execParam, UgyiratId, SkontroVege_CalendarControl.Text);

                    Result result = new Result();
                    if (Mode == SkontroTipus.KiadasSkontroIrattarbol)
                    {
                        EREC_IrattariKikeroService kikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();

                        result = kikeroService.KiadasSkontroIrattarbol_Tomeges(execParam, UgyiratId_Array, SkontroVege_CalendarControl.Text);
                    }
                    else
                    {
                        result = service.SkontrobolKivetel_Tomeges(execParam, UgyiratId_Array, SkontroVege_CalendarControl.Text);
                    }

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    else
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
                catch (FormatException fe)
                {
                    ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }




    protected void ValidateInputParameters()
    {
        string sSkontroVege = SkontroVege_CalendarControl.Text.Trim();
        string sSkontroKezdete = SkontrobaDat_CalendarControl.Text.Trim();
        // skontr� kezdete csak skontr�ba ad�sn�l kell:
        if (String.IsNullOrEmpty(sSkontroKezdete) && Mode == SkontroTipus.SkontrobaAdas)
        {
            throw new FormatException(UI.GetRequeredTimeMessage("skontr� kezdete"));
        }

        // skontr�ba ad�skor k�telez� a skontr� v�ge
        if (String.IsNullOrEmpty(sSkontroVege) && Mode == SkontroTipus.SkontrobaAdas)
        {
            throw new FormatException(UI.GetRequeredTimeMessage("skontr� v�ge"));
        }

        if (!String.IsNullOrEmpty(sSkontroVege))
        {
            DateTime dtSkontroVege = UI.GetDateTimeFromString(sSkontroVege, "skontr� v�ge");
            if (Mode == SkontroTipus.SkontrobaAdas)
            {
                DateTime dtSkontroKezdete = UI.GetDateTimeFromString(sSkontroKezdete, "skontr� kezdete");
                if (dtSkontroKezdete > dtSkontroVege)
                {
                    throw new FormatException("A skontr� v�ge d�tum nem lehet kor�bbi a skontr� kezdeti d�tumn�l!");
                }
            }
        }
    }

    protected void ImageApprove_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesJovahagyasa))
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.SkontroJovahagyas(execParam, UgyiratId, textMegjegyzes.Text);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }

    }

    protected void ImageReject_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesElutasitasa))
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.SkontroElutasitas(execParam, UgyiratId, textMegjegyzes.Text);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    protected void ImageRevoke_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratSkontrobaHelyezesVisszavonasa) ||
            FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSkontroKezeles))
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.SkontroVisszavonas(execParam, UgyiratId, String.Empty);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }
}
