﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="StandaloneDokumentumFelvitel.aspx.cs" Inherits="StandaloneDokumentumFelvitel" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/FileUploadComponent.ascx" TagName="FileUploadComponent" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc7" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <asp:Panel ID="AllomanyAdatokPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="LabelAllomany_Csillag" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="LabelAllomany" runat="server" Text="Állomány:"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <input id="fileUpload" type="file" runat="server" />
                                        </td>
                                    </tr>
                                    <%-- 
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="lbDokumemtumSzerepStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="lbDokumemtumSzerep" runat="server" Text="Szerep:"></asp:Label>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_DokumentumSzerep" runat="server" CssClass="mrUrlapInput" />
                                            <asp:HiddenField ID="hfDokumentumSzerep" runat="server" />
                                            <asp:HiddenField ID="hfIsFodokument" runat="server" />
                                            <asp:CustomValidator ID="validatorDokumentumSzerep" runat="server" EnableClientScript="true" ErrorMessage="<%$Resources:Error,UIAlreadyExistsFodokumentumInCsatolmanyok %>" SetFocusOnError="true" Display="None"></asp:CustomValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="validatorCalloutDokumentumSzerep" runat="server"
                                                TargetControlID="validatorDokumentumSzerep">
                                                <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                    <HideAction Visible="true" />
                                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                                </Animations>
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="LabelCsatolmanyMegjegyzes" runat="server" Text="Megjegyzés:"></asp:Label>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:TextBox ID="CsatolmanyMegjegyzes" runat="server" CssClass="mrUrlapInput" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="height: 30px">
                                        </td>
                                        <td class="mrUrlapMezo" style="height: 30px">
                                            <asp:CheckBox ID="cbMegnyithato" Text="Megnyitható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbOlvashato" Text="Olvasható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbTitkositott" Text="Titkosított" TextAlign="Right" runat="server" />
                                        </td>
                                    </tr>
                                    --%>
                                </table>
                            </asp:Panel>
                            <%-- 
                            <asp:Panel ID="ElektronikusAlairasPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAlairas" runat="server" Text="Aláírás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="AlairasManualis_KodtarakDropDownList" runat="server" Enabled="false" Visible="false" />
                                            <asp:UpdatePanel ID="AlairasUpdatePanel" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:CheckBox ID="cb_Alairas" runat="server" Checked="false" />
                                                    <asp:DropDownList ID="ddl_AlairasSzabalyok" runat="server" CssClass="mrUrlapInputComboBox_minWidth"
                                                        Enabled="false" Visible="true" style="display: none;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <object classid="clsid:0E9BF42E-38BC-4711-B1D4-D6A77A5AD96A" height="1" id="sdxsigner"
                                                width="1" codebase="http://pc-psenak2/SDX_SignAx.cab#Version=1,0,0,1">
                                                <param name="Easz" value="">
                                                <param name="Thumbprint" value="">
                                            </object>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            --%>
                        <asp:HiddenField ID="hf_confirmUploadByDocumentHash" runat="server" Value="" />
                    </eUI:eFormPanel>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

