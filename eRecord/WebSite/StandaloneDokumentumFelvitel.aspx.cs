﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eDocument.Service;


public partial class StandaloneDokumentumFelvitel : System.Web.UI.Page
{
    private const string SITE_PATH = "standalone";
    private const string DOC_LIB_PATH = "docs";
    private const string FOLDER_PATH = "fph";



    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.StandaloneDokumentumokFormHeaderTitle;
    }


    private void FormFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "StandaloneDokumentumokNew"))
            {

                if (fileUpload.PostedFile == null || string.IsNullOrEmpty(fileUpload.PostedFile.FileName))
                {
                    return;
                }

                #region Feltöltött file beolvasása byte[]-ba

                System.IO.Stream fs = fileUpload.PostedFile.InputStream;
                byte[] buf = new byte[fs.Length];

                fs.Read(buf, 0, System.Convert.ToInt32(fs.Length));

                #endregion Feltöltött file beolvasása byte[]-ba

                ExecParam ep = UI.SetExecParamDefault(Page);

                string fileName = fileUpload.PostedFile.FileName.Substring(fileUpload.PostedFile.FileName.LastIndexOf('\\') + 1);


                #region Feltöltötték-e már a fájlt

                DocumentService docService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

                Result res = docService.FileExists(SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName);

                if (!res.IsError &&(bool)res.Record)
                {
                    //A fájlt már feltöltötték, nem engedjük felülírni
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, ResultError.CreateNewResultWithErrorCode(52755));
                    return;
                }

                #endregion Feltöltötték-e már a fájlt


                #region FileUpload



                res = docService.DirectUploadAndDocumentInsert(ep, "SharePoint", SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName, buf, "1", null);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                    return;
                }

                #endregion FileUpload

                JavaScripts.RegisterSelectedRecordIdToParent(Page, (res.Record as KRT_Dokumentumok).Id);
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
        }
        else
        {
            Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
        }


        #region OldUpload
        /*
        String SubCommand = (String)e.CommandArgument;

        // Az AlairasEllenorzes es az ElektronikusAlairas funkciojogokat
        // jelenleg a csatolmanyok modositasi jogahoz kotjuk
        String CheckRightCommand = "";
        switch (SubCommand)
        {
            case CommandName.AlairasEllenorzes:
            case CommandName.ElektronikusAlairas:
                CheckRightCommand = CommandName.Modify;
                break;
            default:
                CheckRightCommand = SubCommand;
                break;
        }




        if (e.CommandName == CommandName.Save)
        {
            Result result = new Result();

            bool fileIsAlreadyOnTheServer = false;
            string randomTempDirectoryName = String.Empty;

            if (SubCommand == CommandName.New)
            {
                #region File ellenőrzése

                // A fájl már fel lett-e töltve, vagy ez egy új feltöltés:
                if (FileUploadComponent1.Visible
                    && !string.IsNullOrEmpty(FileUploadComponent1.HiddenField.Value)
                    && !FileUpload1.HasFile)
                {
                    fileIsAlreadyOnTheServer = true;
                }

                // ellenőrzés, van-e feltöltött fájl
                if (!fileIsAlreadyOnTheServer && FileUpload1.HasFile == false)
                {
                    ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UINoSelectedFile);
                    ErrorUpdatePanel.Update();
                    return;
                }

                #endregion

                bool kliensoldaliAlairas = false;

                try
                {
                    #region Fájltartalom kiolvasása

                    if (fileIsAlreadyOnTheServer)
                    {
                        // FileUploadComponent1.HiddenField-ben van letéve a directory és file neve
                        string[] tomb = FileUploadComponent1.Value_HiddenField.Split(';');
                        if (tomb == null || tomb.Length < 2)
                        {
                            // hiba:
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, Resources.Error.UINoFile);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        randomTempDirectoryName = tomb[0];
                        string fileName = tomb[1];

                        csatolmany.Tartalom = FileFunctions.Upload.GetFileContentFromTempDirectory(randomTempDirectoryName, fileName);
                        csatolmany.Nev = fileName;
                    }
                    else
                    {
                        csatolmany.Tartalom = FileUpload1.FileBytes;
                        csatolmany.Nev = FileUpload1.FileName;
                    }

                    #endregion

                    #region Hash számítás és ellenőrzés, van-e már ilyen feltöltött fájl

                    // ha egy ellenőrzés utáni confirm-ra 'ok' jött, nem kell újból ellenőrzés
                    if (hf_confirmUploadByDocumentHash.Value == "ok")
                    {
                        hf_confirmUploadByDocumentHash.Value = "";

                        // MS fileoknál a tartalom hash-t meg kell képezni:
                        if (FileFunctions.NeedTartalomHashCheck(csatolmany.Nev))
                        {
                            csatolmany.TartalomHash = FileFunctions.GetMSFileContentSHA1(csatolmany.Tartalom, csatolmany.Nev);
                        }
                    }
                    else if (csatolmany.Tartalom != null && csatolmany.Tartalom.Length > 0)
                    {
                        bool tartalomHashEllenorzesIsKell = false;
                        string tartalomHash = String.Empty;
                        if (FileFunctions.NeedTartalomHashCheck(csatolmany.Nev))
                        {
                            tartalomHash = FileFunctions.GetMSFileContentSHA1(csatolmany.Tartalom, csatolmany.Nev);
                            if (!String.IsNullOrEmpty(tartalomHash))
                            {
                                tartalomHashEllenorzesIsKell = true;
                                // továbbadjuk a tartalomHash-et:
                                csatolmany.TartalomHash = tartalomHash;
                            }
                        }

                        string fileHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);

                        // Ellenőrzés, van-e már ilyen dokumentum feltöltve?
                        EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                        EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
                        csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Value = fileHash;
                        csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Operator = Query.Operators.equals;

                        if (tartalomHashEllenorzesIsKell)
                        {
                            // tartalomHash-re is ellenőrzünk (VAGY kapcsolat a kivonathash-sel)
                            csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Value = tartalomHash;
                            csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Operator = Query.Operators.equals;
                            csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Group = "975";
                            csatolmanyokSearch.Manual_Dokumentum_TartalomHash.GroupOperator = Query.Operators.or;

                            csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Group = "975";
                            csatolmanyokSearch.Manual_Dokumentum_KivonatHash.GroupOperator = Query.Operators.or;
                        }

                        Result result_csatSearch = service_csatolmanyok.GetAllWithExtension(
                            UI.SetExecParamDefault(Page, new ExecParam()), csatolmanyokSearch);
                        if (result_csatSearch.IsError)
                        {
                            // hiba:
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_csatSearch);
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        else
                        {
                            // ha van már ilyen dokumentum:
                            if (result_csatSearch.Ds != null && result_csatSearch.Ds.Tables[0].Rows.Count > 0)
                            {
                                // File lementése a szerverre, hogy ne kelljen a felhasználónak újra feltöltenie:
                                string randomDirName;
                                try
                                {
                                    FileFunctions.Upload.SaveFileToTempDirectory(csatolmany.Tartalom, csatolmany.Nev, out randomDirName);
                                }
                                catch (Exception exc)
                                {
                                    // hiba a file elmentése során:
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
                                    ErrorUpdatePanel.Update();
                                    return;
                                }

                                // FileUpload1 helyett FileUploadComponent1 megjelenítése, ami mutatja a feltöltött fájlt:
                                FileUpload1.Visible = false;

                                FileUploadComponent1.HiddenField.Value = randomDirName + ";" + csatolmany.Nev;
                                FileUploadComponent1.Label.Text = csatolmany.Nev;
                                FileUploadComponent1.Visible = true;
                                FileUploadComponent1.Enabled = false;

                                #region Figyelmeztető üzenet kell a felhasználónak: (javascriptes confirm)

                                // 'Ez a dokumentum már feltöltésre került az alábbi elem(ek) csatolmányaként:'
                                string confirmText = Resources.Form.DokumentumMarFeltoltve + " \\n";
                                int felsoroltElemekMaxSzama = 3;
                                for (int i = 0; i < result_csatSearch.Ds.Tables[0].Rows.Count && i < felsoroltElemekMaxSzama; i++)
                                {
                                    DataRow row = result_csatSearch.Ds.Tables[0].Rows[i];
                                    if (!string.IsNullOrEmpty(row["IraIrat_Id"].ToString()))
                                    {
                                        // irat:
                                        string iktatoSzam = row["IktatoSzam_Merge"].ToString();
                                        confirmText += "\\n" + iktatoSzam;
                                    }
                                    else
                                    {
                                        // küldemény:
                                        string erkeztetoSzam = row["ErkeztetoSzam_Merge"].ToString();
                                        confirmText += "\\n" + erkeztetoSzam;
                                    }
                                }

                                if (result_csatSearch.Ds.Tables[0].Rows.Count > felsoroltElemekMaxSzama)
                                {
                                    confirmText += "\\n...";
                                }

                                // 'Biztosan feltölti a dokumentumot?'
                                confirmText += "\\n\\n" + Resources.Question.UIConfirmHeader_DokumentumUpload;

                                string js = @"
                                 function ConfirmUploadByDocumentHash()  {
                                    if (confirm('" + confirmText + "') == true) { $get('"
                                    + hf_confirmUploadByDocumentHash.ClientID + "').value = 'ok';  "
                                    + "window.setTimeout(\"" + Page.ClientScript.GetPostBackEventReference(TabFooter1.ImageButton_Save, "") + "\",500);" +
                                   @"} else {}
                                 }"
                                    //+ " window.setTimeout(\"ConfirmUploadByDocumentHash()\",2000); ";

                                    + " function pageLoad() { if(ConfirmUploadByDocumentHash != null){ConfirmUploadByDocumentHash();ConfirmUploadByDocumentHash = null;} } ";

                                //+ " Sys.Application.add_load(ConfirmUploadByDocumentHash); ";


                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "documentHashConfirmScript", js, true);

                                #endregion

                                // Kilépés innen, még nem töltjük fel
                                return;
                            }
                        }
                    }

                    #endregion

                    #region Aláírás:

                    if (cb_Alairas.Checked && !string.IsNullOrEmpty(ddl_AlairasSzabalyok.SelectedValue))
                    {
                        // beállítjuk az aláírásra vonatkozó infókat a csatolmany objektumban:

                        string alairasSzabaly_Id = ddl_AlairasSzabalyok.SelectedValue;

                        // Aláírásszabályhoz tartozó adatok a lementett dictionary-ből:
                        Dictionary<string, Csatolmany.Alairas> alairasAdatok_Dict =
                            (Dictionary<string, Csatolmany.Alairas>)ViewState[viewState_AlairasAdatok_Dict];

                        if (alairasAdatok_Dict != null && alairasAdatok_Dict.ContainsKey(alairasSzabaly_Id))
                        {
                            Csatolmany.Alairas elmentettAlairasAdatok = alairasAdatok_Dict[alairasSzabaly_Id];

                            csatolmany.AlairasAdatok = elmentettAlairasAdatok;
                        }

                        /// A csatolmány aláírandó:
                        /// Ha kliens oldali aláírásszabály volt választva, akkor ez nem kell,
                        /// különben a szerveren még egyszer alá lenne írva
                        List<string> kliensoldaliAlairasSzabalyok = (List<string>)ViewState[viewState_KliensoldaliAlairasSzabalyok];
                        if (kliensoldaliAlairasSzabalyok != null
                              && kliensoldaliAlairasSzabalyok.Contains(alairasSzabaly_Id))
                        {
                            // kliens oldali aláírás volt választva, nem kell már szerveren aláírni:
                            csatolmany.AlairasAdatok.CsatolmanyAlairando = false;
                            csatolmany.ElektronikusAlairas = csatolmany.AlairasAdatok.AlairasSzint.ToString();
                            kliensoldaliAlairas = true;
                        }
                        else
                        {
                            csatolmany.AlairasAdatok.CsatolmanyAlairando = true;
                        }

                        csatolmany.AlairasAdatok.AlairasSzabaly_Id = alairasSzabaly_Id;


                        //string signdata = System.Convert.ToBase64String(csatolmany.Tartalom);
                        //int ConfigId = 0;

                        ////string contentToSign = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        ////        + "<xml>"
                        ////        + "<config_item>" + ConfigId + "</config_item>"
                        ////        + "<filename>" + csatolmany.Nev + "</filename>"
                        ////        + "<signdata>" + signdata + "</signdata>"
                        ////        + "</xml>";

                        //eSign.SDXM.Url = UI.GetAppSetting("eASZBusinessServiceUrl");

                        ////eSign.common.SignResult sr = eSign.SDXM.Sign(contentToSign);
                        //eSign.common.SignResult sr = eSign.SDXM.SignDirectly(ConfigId, csatolmany.Nev, signdata);

                        //csatolmany.Tartalom = System.Convert.FromBase64String(sr.Result);
                        //csatolmany.Nev += ".sdxm";
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    result.ErrorCode = "222222";
                    result.ErrorMessage = "hiba a file beolvasasakor! \n" + ex.ToString();
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return;
                }

                if (csatolmany.Tartalom == null)
                {
                    result.ErrorCode = "222222";
                    result.ErrorMessage = "hiba a file beolvasásakor: null a tartalom!";
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return;
                }

                csatolmany.Megnyithato = cbMegnyithato.Checked ? "1" : "0";
                csatolmany.Olvashato = cbOlvashato.Checked ? "1" : "0";
                csatolmany.Titkositas = cbTitkositott.Checked ? "1" : "0";

                switch (krt_Parameterek_PKI_INTEGRACIO)
                {
                    case "Nem":
                        csatolmany.ElektronikusAlairas = AlairasManualis_KodtarakDropDownList.SelectedValue;
                        break;
                    case "Igen":
                        // csatolmany.AlairasAdatok -ban adjuk meg az aláírásos cuccokat
                        // kliensoldali aláírásnál már beállítottuk:
                        if (!kliensoldaliAlairas)
                        {
                            csatolmany.ElektronikusAlairas = "<null>";
                        }
                        break;
                    default:
                        csatolmany.ElektronikusAlairas = "<null>";
                        break;
                }
            }

            ExecParam execParam = UI.SetExecParamDefault(Page);

            // dokumentum vizsgálathoz minden típus esetén aktualizálni kell
            // a KRT_Dokumentumok tábla rekordját is, módosítás esetén
            String krtDokumentumId = String.Empty;

            // nyilvántartjuk, hogy sikerült-e beolvasni a fő (küldemény, irat, iratpéldány) rekordot
            bool voltHiba = false;

            //// a dokumentum megnyithatósági vizsgálathoz szükséges
            //// a KRT_Dokumentumok típusú paraméter továbbítása is
            //// ez független attól, hogy mihez csatolunk
            //KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents_DokumentumVizsgalat();

            // Kuldemeny
            if (_ParentForm == Constants.ParentForms.Kuldemeny)
            {
                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CheckRightCommand))
                {
                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fődokumentum ellenőrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, ParentId))
                                {
                                    return;
                                }
                                #endregion Fődokumentum ellenőrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                execParam.Record_Id = ParentId;
                                result = erec_KuldKuldemenyekService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (result.IsError == false)
                                {
                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fődokumentum ellenőrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, ParentId))
                                    {
                                        return;
                                    }
                                    #endregion Fődokumentum ellenőrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
                }
            }
            // IraIrat
            else if (_ParentForm == Constants.ParentForms.IraIrat)
            {

                if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CheckRightCommand))
                {
                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fődokumentum ellenőrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, ParentId))
                                {
                                    return;
                                }
                                #endregion Fődokumentum ellenőrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                execParam.Record_Id = ParentId;
                                result = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (result.IsError == false)
                                {
                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fődokumentum ellenőrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, ParentId))
                                    {
                                        return;
                                    }
                                    #endregion Fődokumentum ellenőrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.RedirectToAccessDeniedErrorPage(Page);
                }
            }
            // IratPeldany
            else if (_ParentForm == Constants.ParentForms.IratPeldany)
            {

                if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CheckRightCommand))
                {

                    EREC_PldIratPeldanyok erec_PldIratPeldanyok = null;

                    if (SubCommand == CommandName.New
                        || SubCommand == CommandName.Modify)
                    {
                        // Iratpéldányból irat lekérése:
                        EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam_PldGet.Record_Id = ParentId;

                        Result result_PldGet = service_Pld.Get(execParam_PldGet);
                        if (!String.IsNullOrEmpty(result_PldGet.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldGet);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;
                    }

                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fődokumentum ellenőrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, erec_PldIratPeldanyok.IraIrat_Id))
                                {
                                    return;
                                }
                                #endregion Fődokumentum ellenőrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents_IratPeldany(erec_PldIratPeldanyok);

                                // erec_IraIratokDokumentumok.IraIrat_Id ParentId-ra volt állítva
                                erec_Csatolmanyok.IraIrat_Id = erec_PldIratPeldanyok.IraIrat_Id;


                                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                                execParam.Record_Id = erec_PldIratPeldanyok.IraIrat_Id;
                                result = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fődokumentum ellenőrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, erec_PldIratPeldanyok.IraIrat_Id))
                                    {
                                        return;
                                    }
                                    #endregion Fődokumentum ellenőrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents_IratPeldany(erec_PldIratPeldanyok);

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.RedirectToAccessDeniedErrorPage(Page);
                }
            }

            if (!voltHiba &&
                    (SubCommand == CommandName.Modify
                     || SubCommand == CommandName.AlairasEllenorzes
                     )
                )
            {
                KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents_DokumentumVizsgalat(SubCommand);
                ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                dokExecParam.Record_Id = krtDokumentumId;

                result = dokService.Update(dokExecParam, krt_Dokumentumok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    // 
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }

                ReLoadTab();
            }

        }
        else if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
            SubListPanel.Visible = true;
        }
         * */

        #endregion OldUpload
    }

    #endregion Base Page


}
