﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class SzamlakSSRS : Contentum.eUtility.UI.ListSSRSPageBase //Contentum.eUtility.UI.PageBase
{
    protected override ReportViewer ReportViewer { get { return ReportViewer1;  } }
    protected override String DefaultOrderBy { get { return " order by EREC_KuldKuldemenyek.LetrehozasIdo"; } }
    protected override Contentum.eUIControls.eErrorPanel ErrorPanel { get { return EErrorPanel1; } }

    protected override Result GetData()
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        ExecParam.Fake = true;

        EREC_KuldKuldemenyekSearch search = (EREC_KuldKuldemenyekSearch)this.SearchObject;
        
        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        return result;
    }

    protected override BaseSearchObject GetSearchObject()
    {
        EREC_KuldKuldemenyekSearch search = null;

        if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SzamlaSearch))
        {
            search = (EREC_KuldKuldemenyekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_KuldKuldemenyekSearch), Page);

            if (Search.IsDefaultSearchObject(typeof(EREC_KuldKuldemenyekSearch), search) == false)
            {
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.SzamlaSearch);
            }
        }

        search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_KuldKuldemenyekSearch(true), Constants.CustomSearchObjectSessionNames.SzamlaSearch);
        search.Tipus.Value = KodTarak.KULDEMENY_TIPUS.Szamla;
        search.Tipus.Operator = Query.Operators.equals;

        Kuldemenyek.SetDefaultFilter(search);

        search.TopRow = 10000;

        return search;
    }

    protected override Dictionary<string, int> CreateVisibilityColumnsInfoDictionary()
    {
        Dictionary<string, int> dict = new Dictionary<string, int>();

        dict.Add("CsnyVisibility", 2);
        dict.Add("KVisibility", 3);
        dict.Add("FVisibility", 4);
        dict.Add("ErkeztetokonyvVisibility", 5);
        dict.Add("ErkeztetoszamVisibility", 6);
        dict.Add("EvVisibility", 7);
        dict.Add("ErkszamVisibility", 8);
        dict.Add("VonalkodVisibility", 9);
        dict.Add("KuldoIktSzamaVisibility", 10);
        dict.Add("BekuldoVIsibility", 11);
        dict.Add("KuldoCimeVIsibility", 12);
        dict.Add("BelsoCimzettVisibility", 13);
        dict.Add("KezeloVisibility", 14);
        dict.Add("TargyVisibility", 15);
        dict.Add("ErkezesDatumaVisibility", 16);
        dict.Add("ErkeztetoVisibility", 17);
        dict.Add("BeerkezesModVisibility", 18);
        dict.Add("TartalomVisibility", 19); ;
        dict.Add("RagszamVisibility", 20);
        dict.Add("KezbesitesPrioritasVisibility", 21);
        dict.Add("AdathordozoTipusaVisibility", 22);
		dict.Add("MunkaallomasVisibility", 23);
        dict.Add("AllapotVisibility", 24);
        dict.Add("IktatasVisibility", 25);
        dict.Add("IrathelyeVisibility", 26);
        dict.Add("PIRVisibility", 27);
        dict.Add("ZarolasVisibility", 28);

        return dict;
    }

    protected override void SetSpecificReportParameter(ReportParameter rp)
    {
        if (rp != null && !String.IsNullOrEmpty(rp.Name) && ResultOfDataQuery != null)
        {
            switch (rp.Name)
            {
                case "Where_Dosszie":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_Dosszie"));
                    break;
            }
        }
    }


    protected override bool IsGetAllParameter(String pname)
    {
        switch (pname)
        {
            case "FelhasznaloSzervezet_Id":
                return true;
            default:
                return base.IsGetAllParameter(pname);
        }

    }
}
