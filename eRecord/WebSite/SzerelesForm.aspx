<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" EnableEventValidation="true"
    CodeFile="SzerelesForm.aspx.cs" Inherits="SzerelesForm" Title="Szereles" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,SzerelesFormHeaderTitle %>" />

    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <asp:RadioButtonList ID="RadioButtonList_SzerelesTipus" runat="server" Visible="false"
                    RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList_SzerelesTipus_SelectedIndexChanged">
                    <asp:ListItem Text="Előkészített szerelés végrehajtása" Value="ElokeszitettSzereles"
                        Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Előkészített szerelés felbontása" Value="ElokeszitettSzerelesFelbontasa"></asp:ListItem>
                    <asp:ListItem Text="Új szerelés" Value="UjSzereles"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>

    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <uc10:UgyiratMasterAdatok ID="UgyiratMasterAdatok1" runat="server"></uc10:UgyiratMasterAdatok>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="label7" runat="server" Text="Szerelendő ügyirat:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc6:UgyiratTextBox ID="Szerelendo_Ugyirat_UgyiratTextBox" runat="server" Filter="Szereles" MigralasVisible="True" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" ValidateIfFeladatIsNotEmpty="true" />

                    <asp:Panel ID="panel_ConfirmMsg_MegorzIdo" runat="server" BorderColor="#FEEFB3" BorderWidth="2px"
                        Wrap="true" HorizontalAlign="Center" Style="margin: 5px;"
                        Visible="False">

                        <div style="color: #9F6000; background-color: #FEEFB3; padding: 5px;">
                            <asp:Literal runat="server" Text="<%$Resources:Question,SzereltUgyiratMegorzesiIdoFigyelmeztetes%>" />
                        </div>
                        <p>
                            <asp:ImageButton TabIndex="1" ID="button_ConfirmMsg_MegorzIdo_Yes" runat="server"
                                ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                OnClick="button_ConfirmMsg_MegorzIdo_Click" CommandName="true" />
                            <asp:ImageButton TabIndex="6" ID="button_ConfirmMsg_MegorzIdo_No" runat="server"
                                ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                CommandName="false"
                                OnClick="button_ConfirmMsg_MegorzIdo_Click" />
                        </p>
                    </asp:Panel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
