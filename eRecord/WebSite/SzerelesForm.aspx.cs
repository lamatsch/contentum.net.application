using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SzerelesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string UgyiratId = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string FunkcioKod_UgyiratSzereles = "UgyiratSzereles";
    private const string FunkcioKod_UgyiratSzerelesVisszavonasa = "UgyiratSzerelesVisszavonasa";

    private const string const_ElokeszitettSzereles = "ElokeszitettSzereles";
    private const string const_ElokeszitettSzerelesFelbontasa = "ElokeszitettSzerelesFelbontasa";
    private const string const_UjSzereles = "UjSzereles";

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratSzereles);
                break;
        }

        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        FeljegyzesPanel.ErrorPanel = FormHeader1.ErrorPanel;

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);
        FormFooter1.ImageButton_Save.OnClientClick = String.Empty;

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
        //HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
    }


    private void LoadFormComponents()
    {
        LoadFormComponents(false);
    }

    private void LoadFormComponents(bool setComponentsForElokeszitettSzerelesFelbontas)
    {
        if (String.IsNullOrEmpty(UgyiratId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            /// �gyirat elemz�se, van-e r� el�k�sz�tett szerel�s

            #region �gyirat Get

            EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page);
            execParam_ugyiratGet.Record_Id = UgyiratId;

            Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyiratGet);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
                MainPanel.Visible = false;
                return;
            }

            EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            #endregion

            #region Van-e hozz� el�k�sz�tett szerel�s?

            bool vanElokeszitettSzereles = false;

            /// lek�rj�k azokat az �gyiratokat, ahol ez az �gyirat a sz�l� (ut�irat)
            /// 
            EREC_UgyUgyiratokSearch search_eloiratok = new EREC_UgyUgyiratokSearch();

            search_eloiratok.UgyUgyirat_Id_Szulo.Value = UgyiratId;
            search_eloiratok.UgyUgyirat_Id_Szulo.Operator = Query.Operators.equals;

            Result result_eloiratokGetAll = service_ugyiratok.GetAll(UI.SetExecParamDefault(Page), search_eloiratok);
            if (result_eloiratokGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_eloiratokGetAll);
                MainPanel.Visible = false;
                return;
            }

            foreach (DataRow row in result_eloiratokGetAll.Ds.Tables[0].Rows)
            {
                string row_UgyUgyirat_Id_Szulo = row["UgyUgyirat_Id_Szulo"].ToString();
                string row_Allapot = row["Allapot"].ToString();
                string row_TovabbitasAlattAllapot = row["TovabbitasAlattAllapot"].ToString();

                if (row_UgyUgyirat_Id_Szulo == UgyiratId
                    && row_Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                    && row_TovabbitasAlattAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                {
                    vanElokeszitettSzereles = true;

                    string row_Id = row["Id"].ToString();
                    string row_Azonosito = row["Azonosito"].ToString();

                    SetComponentsForElokeszitettSzereles(erec_UgyUgyiratok, row_Id, row_Azonosito);

                    if (setComponentsForElokeszitettSzerelesFelbontas)
                    {
                        CheckElokeszitettSzerelesFelbontas(erec_UgyUgyiratok, Ugyiratok.GetAllapotByDataRow(row), row_UgyUgyirat_Id_Szulo);
                    }
                    else
                    {
                        CheckSzerelhetoseg(erec_UgyUgyiratok);
                    }

                    break;
                }
            }

            if (vanElokeszitettSzereles == false)
            {
                #region Vizsg�lat, ez az �gyirat van-e esetleg el�k�sz�tve szerel�sre

                /// ha ki van t�ltve az UgyUgyiratIdSzulo mez�, de nincs Szerelt �llapotban
                if (!String.IsNullOrEmpty(erec_UgyUgyiratok.UgyUgyirat_Id_Szulo)
                    && erec_UgyUgyiratok.Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                    && erec_UgyUgyiratok.TovabbitasAlattAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                {
                    vanElokeszitettSzereles = true;

                    EREC_UgyUgyiratok masterUgyiratObj =
                        SetComponentsForElokeszitettSzereles(erec_UgyUgyiratok.UgyUgyirat_Id_Szulo, erec_UgyUgyiratok);

                    if (setComponentsForElokeszitettSzerelesFelbontas)
                    {
                        CheckElokeszitettSzerelesFelbontas(masterUgyiratObj, erec_UgyUgyiratok);
                    }
                    else
                    {
                        CheckSzerelhetoseg(masterUgyiratObj);
                    }
                }

                #endregion
            }

            #endregion

            // ha sima szerel�s van:
            if (vanElokeszitettSzereles == false)
            {
                // ha nem tal�ltunk el�k�sz�tett szerel�st, de erre kellett volna be�ll�tani a formot, hiba
                if (setComponentsForElokeszitettSzerelesFelbontas)
                {
                    // hiba, az �gyirat nincs szerel�sre el�k�sz�tve
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                        ResultError.CreateNewResultWithErrorCode(52774));
                    MainPanel.Visible = false;
                }
                else
                {
                    UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                    UgyiratMasterAdatok1.SetUgyiratMasterAdatokByBusinessObject(erec_UgyUgyiratok, FormHeader1.ErrorPanel, null);

                    CheckSzerelhetoseg(erec_UgyUgyiratok);
                }
            }

            if (vanElokeszitettSzereles)
            {
                /// r�di�gombok be�ll�t�sa �s megjelen�t�se:
                RadioButtonList_SzerelesTipus.Visible = true;

                // ha nincs joga visszavonni az el�k�sz�tett szerel�st, letiltjuk azt a r�di�gombot:
                if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSzerelesVisszavonasa) == false)
                {
                    RadioButtonList_SzerelesTipus.Items[1].Enabled = false;
                }
            }

        }
    }


    /// <summary>
    /// Ellen�rz�s, szerelhet�-e bele �gyirat
    /// </summary>
    private bool CheckSzerelhetoseg(EREC_UgyUgyiratok masterUgyiratObj)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);

        // Ellen�rz�s, Szerelhet�-e bele �gyirat:
        Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(masterUgyiratObj);
        ErrorDetails errorDetail = null;

        if (Ugyiratok.SzerelhetoBeleUgyirat(ugyiratStatusz, execParam, out errorDetail) == false)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                ResultError.CreateNewResultWithErrorCode(52271, errorDetail));
            MainPanel.Visible = false;
            return false;
        }

        if (!Ugyiratok.SzerelhetoBeleRegiUgyirat(ugyiratStatusz, execParam))
        {
            Szerelendo_Ugyirat_UgyiratTextBox.MigralasVisible = false;
        }

        return true;
    }


    /// <summary>
    /// Ellen�rz�s, az el�k�sz�tett szerel�s felbonthat�-e?
    /// </summary>
    private bool CheckElokeszitettSzerelesFelbontas(EREC_UgyUgyiratok masterUgyiratObj, EREC_UgyUgyiratok szerelendoUgyiratObj)
    {
        Ugyiratok.Statusz szerelendoUgyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(szerelendoUgyiratObj);

        return CheckElokeszitettSzerelesFelbontas(masterUgyiratObj, szerelendoUgyiratStatusz, szerelendoUgyiratObj.UgyUgyirat_Id_Szulo);
    }

    /// <summary>
    /// Ellen�rz�s, az el�k�sz�tett szerel�s felbonthat�-e?
    /// </summary>
    private bool CheckElokeszitettSzerelesFelbontas(EREC_UgyUgyiratok masterUgyiratObj, Ugyiratok.Statusz szerelendoUgyiratStatusz, string szerelendoUgyirat_UgyUgyirat_Id_Szulo)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);

        // Ellen�rz�s, az el�k�sz�tett szerel�s felbonthat�-e?
        Ugyiratok.Statusz masterugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(masterUgyiratObj);
        //Ugyiratok.Statusz szerelendoUgyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(szerelendoUgyiratObj);
        ErrorDetails errorDetail = null;

        if (Ugyiratok.ElokeszitettSzerelesFelbonthato(masterugyiratStatusz, szerelendoUgyiratStatusz, szerelendoUgyirat_UgyUgyirat_Id_Szulo
             , execParam, out errorDetail) == false)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                ResultError.CreateNewResultWithErrorCode(52773, errorDetail));
            MainPanel.Visible = false;
            return false;
        }

        return true;
    }

    private void SetComponentsForElokeszitettSzereles(EREC_UgyUgyiratok master_UgyiratObj, string szerelendoUgyiratId, string szerelendoUgyiratIktatoSzam)
    {
        UgyiratMasterAdatok1.UgyiratId = master_UgyiratObj.Id;
        UgyiratMasterAdatok1.SetUgyiratMasterAdatokByBusinessObject(master_UgyiratObj, FormHeader1.ErrorPanel, null);

        Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField = szerelendoUgyiratId;
        Szerelendo_Ugyirat_UgyiratTextBox.Text = szerelendoUgyiratIktatoSzam;

        Szerelendo_Ugyirat_UgyiratTextBox.ReadOnly = true;
        Szerelendo_Ugyirat_UgyiratTextBox.MigralasVisible = false;
    }

    private EREC_UgyUgyiratok SetComponentsForElokeszitettSzereles(string masterUgyiratId, EREC_UgyUgyiratok szerelendoUgyiratObj)
    {
        UgyiratMasterAdatok1.UgyiratId = masterUgyiratId;
        EREC_UgyUgyiratok masterUgyiratObj = UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

        Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField = szerelendoUgyiratObj.Id;
        Szerelendo_Ugyirat_UgyiratTextBox.Text = szerelendoUgyiratObj.Azonosito;

        Szerelendo_Ugyirat_UgyiratTextBox.ReadOnly = true;
        Szerelendo_Ugyirat_UgyiratTextBox.MigralasVisible = false;

        return masterUgyiratObj;
    }

    //private EREC_UgyKezFeljegyzesek GetBusinessObjectFromComponents_UgyKezFeljegyzesek()
    //{
    //    EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = new EREC_UgyKezFeljegyzesek();
    //    erec_UgyKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_UgyKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_UgyKezFeljegyzesek.Leiras = Megjegyzes_TextBox.Text;
    //    erec_UgyKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(Megjegyzes_TextBox);

    //    return erec_UgyKezFeljegyzesek;
    //}

    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        SaveProcedure(e.CommandName, false);
    }

    private void SaveProcedure(string commandName, bool confirmed)
    {
        if (commandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSzereles))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }

                // Szerel�s

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);

                EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                string szerelendo_ugyiratId = Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField;
                string szerelendo_ugyiratAzon = Szerelendo_Ugyirat_UgyiratTextBox.Text;
                string cel_ugyiratId = UgyiratMasterAdatok1.UgyiratId;
                string cel_ugyiratAzon = UgyiratMasterAdatok1.UgyiratFoszam;

                if (!confirmed && CheckSzereltUgyIratMegorzesiIdoFigyelmeztetes(execParam, szerelendo_ugyiratId, cel_ugyiratId))
                {
                    panel_ConfirmMsg_MegorzIdo.Visible = true;
                    return;
                }
                else
                {
                    panel_ConfirmMsg_MegorzIdo.Visible = false;
                }

                // el�k�sz�tett szerel�s v�gleges�t�s, vagy sima szerel�s:
                if (RadioButtonList_SzerelesTipus.Visible
                    && RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzereles)
                {
                    Result result = service.ElokeszitettSzerelesVegrehajtasa(execParam, szerelendo_ugyiratId, cel_ugyiratId, erec_HataridosFeladatok);
                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    else
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
                else if (RadioButtonList_SzerelesTipus.Visible
                    && RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzerelesFelbontasa)
                {
                    #region El�k�sz�tett szerel�s felbont�sa

                    // Funkci�jog-ellen�rz�s:
                    if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSzerelesVisszavonasa) == false)
                    {
                        UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                        return;
                    }

                    Result result = service.ElokeszitettSzerelesFelbontasa(execParam, szerelendo_ugyiratId, cel_ugyiratId, erec_HataridosFeladatok);
                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    else
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }

                    #endregion
                }
                else
                {
                    Result result = new Result();

                    if (Szerelendo_Ugyirat_UgyiratTextBox.IsMigraltSelected)
                    {
                        result = service.RegiAdatSzereles(execParam, cel_ugyiratId, cel_ugyiratAzon, szerelendo_ugyiratId, szerelendo_ugyiratAzon, erec_HataridosFeladatok);
                    }
                    else
                    {
                        result = service.Szereles(execParam, szerelendo_ugyiratId, cel_ugyiratId, erec_HataridosFeladatok);
                    }

                    if (!result.IsError)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }


    protected void RadioButtonList_SzerelesTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        // hibapanel elt�ntet�se
        if (FormHeader1.ErrorPanel.Visible == true)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }
        // ha a f�panel el lett esetleg t�ntetve:
        MainPanel.Visible = true;

        if (RadioButtonList_SzerelesTipus.SelectedValue == const_UjSzereles)
        {
            ResetFormComponentsForUjUgyirat();

            FormHeader1.HeaderTitle = Resources.Form.SzerelesFormHeaderTitle;
        }
        else if (RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzereles)
        {
            // ha l�tezik el�k�sz�tett szerel�s, arra fognak be�llni a komponensek
            LoadFormComponents();

            FormHeader1.HeaderTitle = Resources.Form.SzerelesFormHeaderTitle;
        }
        else if (RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzerelesFelbontasa)
        {
            /// ugyanaz l�tsz�dik, mint az el�k�sz�tett szerel�sn�l
            /// formheader c�m v�ltozik, illetve Rendben gombra meger�s�t� (confirm) js k�d

            LoadFormComponents(true);

            FormHeader1.HeaderTitle = Resources.Form.ElokeszitettSzerelesFelbontasaFormHeader;

            FormFooter1.ImageButton_Save.OnClientClick = " if (confirm('" + Resources.Question.UIConfirmHeader_ElokeszitettSzerelesFelbontasa
                + "')==false) { return false; } " + FormFooter1.ImageButton_Save.OnClientClick;
        }
    }


    private void ResetFormComponentsForUjUgyirat()
    {
        UgyiratMasterAdatok1.UgyiratId = UgyiratId;
        EREC_UgyUgyiratok masterUgyiratObj = UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

        Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField = String.Empty;
        Szerelendo_Ugyirat_UgyiratTextBox.Text = String.Empty;
        Szerelendo_Ugyirat_UgyiratTextBox.ReadOnly = false;
        Szerelendo_Ugyirat_UgyiratTextBox.ImageButton_Lov.CssClass = "mrUrlapInputImageButton";

        CheckSzerelhetoseg(masterUgyiratObj);
    }

    #region 515 - SZEREL - MEGORZESI IDO
    //private void SetSzereltUgyIratMegorzesiIdoFigyelmeztetes(bool append)
    //{
    //    if (append)
    //        FormFooter1.ImageButton_Save.OnClientClick += " checkSzereltUgyIratMegorzesiIdo();";
    //    else
    //        FormFooter1.ImageButton_Save.OnClientClick = " checkSzereltUgyIratMegorzesiIdo();";
    //}

    /// <summary>
    /// CheckSzereltUgyIratMegorzesiIdo
    /// </summary>
    /// <param name="szerelendoUgyId"></param>
    /// <param name="celUgyId"></param>
    private bool CheckSzereltUgyIratMegorzesiIdoFigyelmeztetes(ExecParam execParam, string szerelendoUgyId, string celUgyId)
    {
        EREC_UgyUgyiratok ugySzerelendo = GetUgyirat(execParam, szerelendoUgyId);
        EREC_UgyUgyiratok ugyCel = GetUgyirat(execParam, celUgyId);
        return CheckSzereltUgyIratMegorzesiIdoFigyelmeztetes(ugySzerelendo, ugyCel);
    }
    /// <summary>
    /// CheckSzereltUgyIratMegorzesiIdo
    /// </summary>
    /// <param name="szerelendoUgy"></param>
    /// <param name="celUgy"></param>
    private bool CheckSzereltUgyIratMegorzesiIdoFigyelmeztetes(EREC_UgyUgyiratok szerelendoUgy, EREC_UgyUgyiratok celUgy)
    {
        if (szerelendoUgy == null || celUgy == null)
            return false;
        if (szerelendoUgy.IraIrattariTetel_Id != celUgy.IraIrattariTetel_Id)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Get ugyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    public EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string ugyiratId)
    {
        ;
        #region GET UGY
        Contentum.eRecord.Service.EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        execParam.Record_Id = ugyiratId;
        Result iratResultGet = service.Get(execParam);

        if (!String.IsNullOrEmpty(iratResultGet.ErrorCode))
            return null;
        return (EREC_UgyUgyiratok)iratResultGet.Record;
        #endregion  GET UGY
    }

    protected void button_ConfirmMsg_MegorzIdo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton bt = (ImageButton)sender;
        switch (bt.CommandName)
        {
            case "true":
                SaveProcedure(CommandName.Save, true);
                break;
            case "false":
                panel_ConfirmMsg_MegorzIdo.Visible = false;
                break;
            default:
                break;
        }
    }
    #endregion 515 - SZEREL - MEGORZESI IDO



}
