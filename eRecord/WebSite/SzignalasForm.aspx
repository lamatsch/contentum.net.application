<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="SzignalasForm.aspx.cs" Inherits="SzignalasForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="eRecordComponent/KuldemenyMasterAdatok.ascx" TagName="KuldemenyMasterAdatok"
    TagPrefix="uc3" %>

<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%--  bernat.laszlo added: További ügyintézők--%>
<%@ Register Src="Component/FelhasznalokMultiSelectPanel.ascx" TagName="FelhasznaloMultiSelect" TagPrefix="uc150" %>

<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp"%>
<%@ Register Src="~/eRecordComponent/IratMasterAdatok.ascx" TagPrefix="uc16" TagName="IratMasterAdatok" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
                
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,SzignalasFormHeaderTitle %>" />
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%" style="margin:auto;">
            <tr>
                <td>
                    <uc10:UgyiratMasterAdatok ID="UgyiratMasterAdatok1" runat="server"></uc10:UgyiratMasterAdatok>
                    <%-- <uc3:KuldemenyMasterAdatok ID="KuldemenyMasterAdatok1" runat="server" />        
                                --%>
                    <uc16:IratMasterAdatok runat="server" ID="IratMasterAdatok1" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <asp:RadioButtonList ID="RadioButtonList_SzignalasTipus" runat="server" Visible="false"
                        RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList_SzignalasTipus_SelectedIndexChanged">
                        <asp:ListItem Text="Szignálás szervezetre" Value="SzignalasSzervezetre" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Szignálás ügyintézőre" Value="SzignalasUgyintezore"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel_Ugyirat" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr id="tr_szervezet_UgyiratSzign" runat="server" class="urlapSor" visible="false">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label2" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="label6" runat="server" Text="Szervezet:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="Szervezet_UgySzign_CsoportTextBox" SzervezetCsoport="true"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                </td>
                                <td class="mrUrlapMezo">
                                </td>
                            </tr>
                            <tr id="tr_kovFelelos" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelUserNev" runat="server" Text="Ügyintéző:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloCsoportTextBox ID="Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat" runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <%--<div class="DisableWrap">
                                    &nbsp;<asp:Label ID="label2" runat="server" Text="Szignáló:"></asp:Label>
                                </div>--%>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%--<uc5:FelhasznaloCsoportTextBox ID="Szignalo_FelhasznaloCsoportTextBox" runat="server" ReadOnly="true"
                                    ViewMode="true" />--%>
                                </td>
                            </tr>
                            <%-- bernat.laszlo added: További ügyintézők--%>
                            <tr id="tr_TovabbiUgyintezok_Labels" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="label5" runat="server" Text="További ügyintézők:"></asp:Label>
                                </td>
                                </tr>
                            <tr id="tr_TovabbiUgyintezok" runat="server" class="urlapSor">
                                <td class="mrUrlapMezo" colspan=3>
                                    <uc150:FelhasznaloMultiSelect ID="Felhasznalo_MultiSelect1" runat="server" />
                                </td>
                            </tr>
                            <%-- bernat.laszlo eddig --%>
                        </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="EFormPanel_Irat" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">       
                            <tr id="tr_szervezet_IratSzign" runat="server" class="urlapSor" visible="false">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label3" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="label8" runat="server" Text="Szervezet:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="Szervezet_IratSzign_CsoportTextBox" SzervezetCsoport="true"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                </td>
                                <td class="mrUrlapMezo">
                                </td>
                            </tr>
                            <tr id="tr_kovFelelos_Irat" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label9" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelUserNevIrat" runat="server" Text="Ügyintéző:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloCsoportTextBox ID="Ugyintezo_FelhasznaloCsoportTextBox_Irat" runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <%--<div class="DisableWrap">
                                    &nbsp;<asp:Label ID="label2" runat="server" Text="Szignáló:"></asp:Label>
                                </div>--%>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%--<uc5:FelhasznaloCsoportTextBox ID="Szignalo_FelhasznaloCsoportTextBox" runat="server" ReadOnly="true"
                                    ViewMode="true" />--%>
                                </td>
                            </tr>                            
                        </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="EFormPanel_Kuldemeny" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr id="tr_iktato" runat="server" class="urlapSor" visible="false">
                                <td class="mrUrlapCaption">
                                    <div class="DisableWrap">
                                       <asp:Label ID="labelIktato" runat="server" Text="Iktató:"></asp:Label>
                                    </div>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="Iktato_CsoportTextBox" runat="server" Validate="true"
                                        SzervezetCsoport="true" />
                                </td>
                            </tr>
                            <tr id="tr_szervezet" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="label_Szervezet" runat="server" Text="Szervezet:"></asp:Label>
                                    <asp:Label ID="label_Ugyfelelos" runat="server" Text="Felelős:" Visible="false"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="Szervezet_CsoportTextBox_Kuld" runat="server" Validate="true"
                                        SzervezetCsoport="true" />
                                    <uc8:CsoportTextBox ID="Ugyfelelos_CsoportTextBox_Kuld" runat="server" Validate="true"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <div class="DisableWrap">
                                        &nbsp;<asp:Label ID="label7" runat="server" Text="Ügyintéző:"></asp:Label>
                                    </div>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloCsoportTextBox ID="Ugyintezo_FelhasznaloCsoportTextBox_Kuld" runat="server"
                                        Validate="false" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" />
                    <asp:HiddenField ID="SzervezetreSzignalas_HiddenField" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
