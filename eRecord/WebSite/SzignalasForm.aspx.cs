﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Newtonsoft.Json;
using System;
using System.Web.UI.WebControls;

public partial class SzignalasForm : Contentum.eUtility.UI.PageBase
{
    private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";
    //private const string kcs_KEZELESI_FELJEGYZES_KULDEMENY = "KEZELESI_FELJEGYZES_KULDEMENY";

    private string Command = "";
    private SzignalasTipus Mode = SzignalasTipus.UgyiratSzignalas;

    private string UgyiratId = "";
    private string KuldemenyId = "";
    private string IratId = "";

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string FunkcioKod_UgyiratSzignalas = "UgyiratSzignalas";
    private const string FunkcioKod_KuldemenySzignalas = "KuldemenySzignalas";
    private const string FunkcioKod_IratSzignalas = "IratSzignalas";

    private const string Const_SzignalasSzervezetre = "SzignalasSzervezetre";
    private const string Const_SzignalasUgyintezore = "SzignalasUgyintezore";


    private enum SzignalasTipus
    {
        UgyiratSzignalas,
        KuldemenySzignalas,
        IratSzignalas
    }

    // BUG_8765
    private bool IsTUK_SZIGNALAS_ENABLED
    {
        get
        {
            return Rendszerparameterek.GetBoolean(Page, "TUK_SZIGNALAS_ENABLED", false);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
        KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
        IratId = Request.QueryString.Get(QueryStringVars.IratId);


        if (!String.IsNullOrEmpty(KuldemenyId))
        {
            // Küldemény szignálás:
            Mode = SzignalasTipus.KuldemenySzignalas;
            FormHeader1.CustomTemplateTipusNev = "SzignalasTemplateObject_Kuldemeny";
        }
        else if (!String.IsNullOrEmpty(UgyiratId))
        {
            // Ügyiratszignálás:
            Mode = SzignalasTipus.UgyiratSzignalas;
            FormHeader1.CustomTemplateTipusNev = "SzignalasTemplateObject_Ugyirat";
        }
        else if (!String.IsNullOrEmpty(IratId))
        {
            // Iratszignálás:
            Mode = SzignalasTipus.IratSzignalas;
            FormHeader1.CustomTemplateTipusNev = "SzignalasTemplateObject_Irat";
        }
        else
        {

        }

        FormHeader1.TemplateObjectType = typeof(SzignalasTemplateObject);
        FormHeader1.FormTemplateLoader1_Visibility = true;
        FormHeader1.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);

        // Jogosultságellenõrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Mode == SzignalasTipus.KuldemenySzignalas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_KuldemenySzignalas);
                }
                else if (Mode == SzignalasTipus.IratSzignalas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratSzignalas);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratSzignalas);
                }
                break;
        }


        FeljegyzesPanel.ErrorPanel = FormHeader1.ErrorPanel;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Mode == SzignalasTipus.KuldemenySzignalas)
        {
            FormHeader1.HeaderTitle = Resources.Form.SzignalasFormHeaderTitle_Kuldemeny;
        }
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            FormHeader1.HeaderTitle = Resources.Form.SzignalasFormHeaderTitle_Irat;
        }
        else
        {
            FormHeader1.HeaderTitle = Resources.Form.SzignalasFormHeaderTitle;
        }

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (IsPostBack)
        {
            // errorPanel eltüntetése, ha ki volt rakva:
            FormHeader1.ErrorPanel.Visible = false;
            // FormFooter vissza, ha el volt tüntetve:
            FormFooter1.Visible = true;
        }

        if (!IsPostBack)
        {
            LoadFormComponents();

            #region Template betöltés, ha kell

            string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);
            if (!String.IsNullOrEmpty(templateId))
            {
                FormHeader1.LoadTemplateObjectById(templateId);
                LoadComponentsFromTemplate((SzignalasTemplateObject)FormHeader1.TemplateObject);
            }

            #endregion
        }

        // figyelmeztetés fizikai átadásra
        FormFooter1.ImageButton_Save.OnClientClick = " alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');";

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        // csak szignálható csoportok megjelenítése
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            // szervezetre szûrés levéve (2009.01.06)
            //Szervezet_UgySzign_CsoportTextBox.FilterBySzervezetId(null);

            Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.FilterBySzervezetId(null);
        }
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            Ugyintezo_FelhasznaloCsoportTextBox_Irat.FilterBySzervezetId(null);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // csak szignálható csoportok megjelenítése
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            //string orig = Szervezet_UgySzign_CsoportTextBox.OnClick_Lov;
            //string insert =
            //        QueryStringVars.CsoportFilter.HierarchiabanLefele + "=true&" +
            //        QueryStringVars.CsoportFilter.KozvetlenulHozzarendelt + "=false&" +
            //        QueryStringVars.CsoportFilter.SzervezetId +
            //        "=&";

            //Szervezet_UgySzign_CsoportTextBox.OnClick_Lov = orig.Insert(orig.IndexOf("CsoportokLovList.aspx?") + "CsoportokLovList.aspx?".Length, insert);

            //Contentum.eAdmin.Service.KRT_CsoportTagokService csoportTagokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            //ExecParam szervezetExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            //szervezetExecParam.Record_Id = Ugyfelelos_CsoportTextBox_Kuld.Id_HiddenField;
            //Result szervezetResult = csoportTagokService.GetSzervezet(szervezetExecParam);

            //string origUI = Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.OnClick_Lov;
            //string insertUI =
            //        QueryStringVars.CsoportFilter.HierarchiabanLefele + "=true&" +
            //        QueryStringVars.CsoportFilter.KozvetlenulHozzarendelt + "=false&" +
            //        QueryStringVars.CsoportFilter.SzervezetId +
            //        "=" + szervezetResult.Uid + "&";

            //Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.OnClick_Lov = origUI.Insert(origUI.IndexOf("CsoportokLovList.aspx?") + "CsoportokLovList.aspx?".Length, insertUI);
        }

    }
    private void LoadFormComponents()
    {
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            #region Ügyirat szignálás
            if (String.IsNullOrEmpty(UgyiratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratMasterAdatok1.Visible = true;
                //KuldemenyMasterAdatok1.Visible = false;
                IratMasterAdatok1.Visible = false;
                EFormPanel_Kuldemeny.Visible = false;
                EFormPanel_Irat.Visible = false;

                UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                EREC_UgyUgyiratok erec_UgyUgyiratok =
                    UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                if (erec_UgyUgyiratok == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    MainPanel.Visible = false;
                    return;
                }
                else
                {
                    ExecParam execParam_szignalhato = UI.SetExecParamDefault(Page);

                    // Ellenõrzés, szignálható-e:
                    Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
                    ErrorDetails errorDetail = null;
                    if (Ugyiratok.Szignalhato(ugyiratStatusz, execParam_szignalhato, out errorDetail) == false)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                            ResultError.CreateNewResultWithErrorCode(52152, errorDetail));
                        MainPanel.Visible = false;
                        return;
                    }

                    // Szervezetre, vagy ügyintézõre szignálás kell?
                    bool szervezetreSzignalas = false;

                    if (String.IsNullOrEmpty(erec_UgyUgyiratok.Csoport_Id_Ugyfelelos))
                    {
                        // ha üres az ügyfelelõs, mindig szervezetre szignálás van:

                        szervezetreSzignalas = true;


                        //// Szignálástípus meghatározása:
                        //string szignalasTipusa = "";

                        //EREC_SzignalasiJegyzekekService service_szignJegyzekek = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();

                        //if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IraIrattariTetel_Id))
                        //{
                        //    Result result_szignTipus = service_szignJegyzekek.GetSzignalasTipusaByUgykorUgytipus(
                        //        execParam_szignalhato, erec_UgyUgyiratok.IraIrattariTetel_Id, erec_UgyUgyiratok.UgyTipus);
                        //    if (!String.IsNullOrEmpty(result_szignTipus.ErrorCode))
                        //    {
                        //        // hiba:
                        //        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_szignTipus);
                        //        MainPanel.Visible = false;
                        //        return;
                        //    }
                        //    else if (result_szignTipus.Record != null)
                        //    {
                        //        szignalasTipusa = result_szignTipus.Record.ToString();
                        //    }
                        //}

                        //// csak a 3-as esetben lehet elvileg szervezetre szignálás:
                        //if (szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._3_Iktatas_Szervezetre_Ugyintezore)
                        //{
                        //    szervezetreSzignalas = true;
                        //}
                    }

                    if (szervezetreSzignalas == true)
                    {
                        SetComponents_SzervezetreSzignalas();
                    }
                    else
                    {
                        // Ügyintézõre szignálás

                        /// Szervezetre is szignálhat (rádiógombot kitesszük)                                               

                        RadioButtonList_SzignalasTipus.Visible = true;
                        RadioButtonList_SzignalasTipus.SelectedValue = Const_SzignalasUgyintezore;

                        SetComponents_UgyintezoreSzignalas();
                        // Szignálhat-e ügyintézõre?
                        string ugyintezoreSzignalhatoErrorMessage;
                        bool ugyintezoreSzignalhato = Ugyiratok.UgyintezoreSzignalhato(UI.SetExecParamDefault(Page), erec_UgyUgyiratok, FelhasznaloProfil.FelhasznaloSzerverzetId(Page), out ugyintezoreSzignalhatoErrorMessage);
                        if (!ugyintezoreSzignalhato)
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, ugyintezoreSzignalhatoErrorMessage);
                            // panel eltüntetése:
                            EFormPanel_Ugyirat.Visible = false;
                            FormFooter1.Visible = false;
                        }
                    }
                }
            }
            #endregion
        }
        else if (Mode == SzignalasTipus.KuldemenySzignalas)
        {
            #region Küldemény szignálás

            if (String.IsNullOrEmpty(KuldemenyId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {

                UgyiratMasterAdatok1.Visible = false;
                IratMasterAdatok1.Visible = false;
                EFormPanel_Ugyirat.Visible = false;
                EFormPanel_Irat.Visible = false;
                //KuldemenyMasterAdatok1.Visible = true;

                // Küldemény lekérése:
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execparam_kuldGet = UI.SetExecParamDefault(Page, new ExecParam());
                execparam_kuldGet.Record_Id = KuldemenyId;

                Result result_kuldGet = service.Get(execparam_kuldGet);
                if (!String.IsNullOrEmpty(result_kuldGet.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_kuldGet);
                    return;
                }

                EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)result_kuldGet.Record;

                // szervezetre szignálás kell?
                bool szervezetreSzignalas = true;

                // tartozik-e már hozzá irat:
                if (!String.IsNullOrEmpty(kuldemeny.IraIratok_Id))
                {
                    // Irat lekérése:
                    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_iratGet.Record_Id = kuldemeny.IraIratok_Id;

                    Result result_iratGet = service_iratok.Get(execParam_iratGet);
                    if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratGet);
                        return;
                    }

                    EREC_IraIratok iraIrat = (EREC_IraIratok)result_iratGet.Record;

                    // ha már meg van adva az ügyfelelõs, akkor már szignálták szervezetre
                    if (!String.IsNullOrEmpty(iraIrat.Csoport_Id_Ugyfelelos))
                    {
                        szervezetreSzignalas = false;

                        Ugyfelelos_CsoportTextBox_Kuld.Id_HiddenField = iraIrat.Csoport_Id_Ugyfelelos;
                        Ugyfelelos_CsoportTextBox_Kuld.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                    }
                }


                string erkeztetoSzam = kuldemeny.Azonosito;

                if (szervezetreSzignalas == true)
                {
                    FormHeader1.FullManualHeaderTitle = erkeztetoSzam + " " + Resources.Form.SzignalasSzervezetreFormHeaderTitle_Kuldemeny;
                    SzervezetreSzignalas_HiddenField.Value = "1";
                    Ugyintezo_FelhasznaloCsoportTextBox_Kuld.ReadOnly = true;
                    label_Szervezet.Visible = true;
                    label_Ugyfelelos.Visible = false;
                    tr_iktato.Visible = false;
                }
                else
                {
                    FormHeader1.FullManualHeaderTitle = erkeztetoSzam + " " + Resources.Form.SzignalasUgyintezoreFormHeaderTitle_Kuldemeny;

                    Szervezet_CsoportTextBox_Kuld.Visible = false;
                    Ugyfelelos_CsoportTextBox_Kuld.Visible = true;
                    Ugyfelelos_CsoportTextBox_Kuld.ReadOnly = true;
                    label_Szervezet.Visible = false;
                    label_Ugyfelelos.Visible = true;
                    Ugyintezo_FelhasznaloCsoportTextBox_Kuld.FilterBySzervezetId(Ugyfelelos_CsoportTextBox_Kuld.Id_HiddenField);

                    if (FelhasznaloProfil.OrgIsBOPMH(Page))
                    {
                        tr_iktato.Visible = true;

                        Iktato_CsoportTextBox.Id_HiddenField = GetDefaultIktatoCsoportId();
                        Iktato_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                    }
                    else
                    {
                        tr_iktato.Visible = false;
                    }
                }
            }
            #endregion
        }
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            if (String.IsNullOrEmpty(IratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                IratMasterAdatok1.Visible = true;
                EFormPanel_Irat.Visible = true;
                UgyiratMasterAdatok1.Visible = false;
                EFormPanel_Ugyirat.Visible = false;
                EFormPanel_Kuldemeny.Visible = false;

                tr_kovFelelos_Irat.Visible = true;

                FeljegyzesPanel.FelelosControlID = Ugyintezo_FelhasznaloCsoportTextBox_Irat.ID;

                // BUG#7575: Szervezetre is szignálhat (rádiógombot kitesszük)                                   
                RadioButtonList_SzignalasTipus.Visible = true;
                RadioButtonList_SzignalasTipus.SelectedValue = Const_SzignalasUgyintezore;

                IratMasterAdatok1.IratId = IratId;
                EREC_IraIratok erec_iraIratok =
                    IratMasterAdatok1.SetIratMasterAdatokById(FormHeader1.ErrorPanel);

                if (erec_iraIratok == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    MainPanel.Visible = false;
                    return;
                }
                else
                {
                    ExecParam execParam_szignalhato = UI.SetExecParamDefault(Page);

                    // Ellenõrzés, szignálható-e:
                    Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(erec_iraIratok);
                    ErrorDetails errorDetail;
                    if (!Iratok.Szignalhato(iratStatusz, execParam_szignalhato, out errorDetail))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                            ResultError.CreateNewResultWithErrorCode(52152, errorDetail));
                        MainPanel.Visible = false;
                        return;
                    }
                }
            }
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //Szignalo_FelhasznaloCsoportTextBox.Id_HiddenField = FelhasznaloProfil.GetFelhasznaloCsoport(execParam);
        //Szignalo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);


        // kezelési feljegyzés rész:
        //if (Mode == SzignalasTipus.KuldemenySzignalas)
        //{
        //    KezFelj_KezelesTipus_Kuldemeny_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZES_KULDEMENY, true, FormHeader1.ErrorPanel);
        //}
        //else
        //{
        //    KezFelj_KezelesTipus_Ugyirat_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, FormHeader1.ErrorPanel);
        //}

    }

    private void SetComponents_UgyintezoreSzignalas()
    {
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            FormHeader1.HeaderTitle = Resources.Form.SzignalasFormHeaderTitle;
        }

        tr_kovFelelos.Visible = true;
        tr_kovFelelos_Irat.Visible = true;
        tr_szervezet_UgyiratSzign.Visible = false;
        tr_szervezet_IratSzign.Visible = false;
        //bernat.laszlo added
        tr_TovabbiUgyintezok.Visible = true;
        tr_TovabbiUgyintezok_Labels.Visible = true;
        //bernat.laszlo eddig
        SzervezetreSzignalas_HiddenField.Value = String.Empty;

        FeljegyzesPanel.FelelosControlID = Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.ID;
    }

    private void SetComponents_SzervezetreSzignalas()
    {
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.SzignalasSzervezetreFormHeaderTitle_Ugyirat;

            EFormPanel_Ugyirat.Visible = true;
        }

        tr_kovFelelos.Visible = false;
        tr_kovFelelos_Irat.Visible = false;
        tr_szervezet_UgyiratSzign.Visible = true;
        tr_szervezet_IratSzign.Visible = true;
        //bernat.laszlo added
        tr_TovabbiUgyintezok.Visible = false;
        tr_TovabbiUgyintezok_Labels.Visible = false;
        //bernat.laszlo eddig

        SzervezetreSzignalas_HiddenField.Value = "1";

        FeljegyzesPanel.FelelosControlID = Szervezet_UgySzign_CsoportTextBox.ID;
    }


    //private EREC_UgyKezFeljegyzesek GetBusinessObjectFromComponents_UgyKezFeljegyzesek()
    //{
    //    EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = new EREC_UgyKezFeljegyzesek();
    //    erec_UgyKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_UgyKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_UgyKezFeljegyzesek.KezelesTipus = KezFelj_KezelesTipus_Ugyirat_KodtarakDropDownList.SelectedValue;
    //    erec_UgyKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezFelj_KezelesTipus_Ugyirat_KodtarakDropDownList);

    //    erec_UgyKezFeljegyzesek.Leiras = KezFelj_Leiras_Ugyirat_TextBox.Text;
    //    erec_UgyKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(KezFelj_Leiras_Ugyirat_TextBox);

    //    return erec_UgyKezFeljegyzesek;
    //}

    //private EREC_KuldKezFeljegyzesek GetBusinessObjectFromComponents_KuldKezFeljegyzesek()
    //{
    //    EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = new EREC_KuldKezFeljegyzesek();
    //    erec_KuldKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_KuldKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_KuldKezFeljegyzesek.KezelesTipus = KezFelj_KezelesTipus_Kuldemeny_KodtarakDropDownList.SelectedValue;
    //    erec_KuldKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezFelj_KezelesTipus_Kuldemeny_KodtarakDropDownList);

    //    erec_KuldKezFeljegyzesek.Leiras = KezFelj_Leiras_Kuldemeny_TextBox.Text;
    //    erec_KuldKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(KezFelj_Leiras_Kuldemeny_TextBox);

    //    return erec_KuldKezFeljegyzesek;
    //}


    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    protected void RadioButtonList_SzignalasTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList_SzignalasTipus.SelectedValue == Const_SzignalasSzervezetre)
        {
            SetComponents_SzervezetreSzignalas();
        }
        else
        {
            SetComponents_UgyintezoreSzignalas();

            LoadFormComponents();
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //LZS - BUG_11081
            //Ide áthelyeztem, mert itt is kell.
            EREC_UgyUgyiratokService serviceUgyUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgyUgyiratok = UI.SetExecParamDefault(Page, new ExecParam());

            if ((Mode == SzignalasTipus.UgyiratSzignalas && FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratSzignalas))
                || (Mode == SzignalasTipus.KuldemenySzignalas && FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenySzignalas))
                || (Mode == SzignalasTipus.IratSzignalas && FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratSzignalas))
                )
            {
                if (Mode == SzignalasTipus.UgyiratSzignalas)
                {
                    if (String.IsNullOrEmpty(UgyiratId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        // Szervezetre szignálás:
                        if (SzervezetreSzignalas_HiddenField.Value == "1")
                        {
                            string szervezet_Id = Szervezet_UgySzign_CsoportTextBox.Id_HiddenField;
                            if (String.IsNullOrEmpty(szervezet_Id))
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINincsMegadvaMindenAdat);
                                return;
                            }

                            //LZS - BUG_11081
                            if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
                            {
                                if (!String.IsNullOrEmpty(UgyiratId))
                                {
                                    execParamUgyUgyiratok.Record_Id = UgyiratId;
                                    EREC_UgyUgyiratok eREC_UgyUgyirat = (EREC_UgyUgyiratok)serviceUgyUgyiratok.Get(execParamUgyUgyiratok).Record;

                                    Note_JSON note;

                                    try
                                    {
                                        note = JsonConvert.DeserializeObject<Note_JSON>(eREC_UgyUgyirat.Base.Note);
                                    }
                                    catch (Exception)
                                    {
                                        note = null;
                                    }


                                    bool SZK_Update = note == null;

                                    if (note != null)
                                    {
                                        SZK_Update = string.IsNullOrEmpty(note.SZK);
                                    }

                                    //LZS - BUG_11081
                                    //Amennyiben a FELELOS_SZERV_KOD_MEGJELENITES = 1, akkor vizsgálni kell, hogy a Note mezőben már van-e SZK element (és ki van töltve az értéke).
                                    //Amennyiben nincs SZK element, vagy nincs érték megadva hozzá(NULL vagy empty string), 
                                    if (SZK_Update)
                                    {
                                        if (!String.IsNullOrEmpty(szervezet_Id))
                                        {
                                            //szervezet_Id alapján lekérjük a csoportot, majd használjuk a kódját:
                                            KRT_CsoportokService krtKodCsoportService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                                            ExecParam execParamKRT = UI.SetExecParamDefault(Page, new ExecParam());
                                            execParamKRT.Record_Id = szervezet_Id;

                                            KRT_Csoportok krt_csoport = (KRT_Csoportok)krtKodCsoportService.Get(execParamKRT).Record;

                                            //akkor a Note mezőbe is fel kell venni a CsoportId_Ugyfelelos mezőbe megadott csoport kódját(KRT_Csoportok.Kod)
                                            string SZK = krt_csoport.Kod;

                                            if (note == null)
                                                note = new Note_JSON();

                                            note.SZK = SZK;

                                            eREC_UgyUgyirat.Base.Note = JsonConvert.SerializeObject(note);
                                            eREC_UgyUgyirat.Base.Updated.Note = true;

                                            Result resUpdate = serviceUgyUgyiratok.Update(execParamUgyUgyiratok, eREC_UgyUgyirat);

                                            //Hibakezelés, megjelenítés
                                            if (resUpdate.IsError)
                                            {
                                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resUpdate);
                                            }
                                        }

                                    }
                                }
                            }

                            // Szervezetre szignálás:


                            // BUG_8765
                            // BUG_11085
                            EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            //EREC_HataridosFeladatok erec_HataridosFeladatok = null;
                            //if (!IsTUK_SZIGNALAS_ENABLED)
                            //    erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            Result result = serviceUgyUgyiratok.SzignalasSzervezetre(execParamUgyUgyiratok, UgyiratId, szervezet_Id, erec_HataridosFeladatok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }

                        }
                        else
                        {
                            String kovFelelosId = Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.Id_HiddenField;
                            if (String.IsNullOrEmpty(kovFelelosId))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                                    , Resources.Error.UINincsMegadvaFelelos);
                                LoadFormComponents();
                                return;
                            }

                            // Szignálás
                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            //bernat.laszlo added
                            RightsService RightService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
                            //CR 3063 Szignálás hiba
                            if (Felhasznalo_MultiSelect1.FelhasznaloIds != null)
                            {
                                if (Felhasznalo_MultiSelect1.FelhasznaloIds.Length != 0)
                                {
                                    foreach (String _felhasznaloId in Felhasznalo_MultiSelect1.FelhasznaloIds)
                                    {
                                        string kuldemenyId = Request.QueryString.Get(QueryStringVars.UgyiratId);
                                        Result right_result = RightService.AddCsoportToJogtargy(execParam, kuldemenyId, _felhasznaloId, 'I');
                                        if (!string.IsNullOrEmpty(right_result.ErrorCode))
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, right_result);
                                        }
                                    }
                                }
                            }
                            //bernat.laszlo eddig

                            // BUG_8765
                            // BUG_11085
                            EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            //EREC_HataridosFeladatok erec_HataridosFeladatok = null;
                            //if (!IsTUK_SZIGNALAS_ENABLED)
                            //    erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            Result result = service.Szignalas(execParam, UgyiratId, kovFelelosId, erec_HataridosFeladatok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // BUG_8765
                                if (!IsTUK_SZIGNALAS_ENABLED)
                                    Notify.SendAnswerEmail(
                                        execParam,
                                        "00000000-0000-0000-0000-000000000000",
                                        UgyiratId
                                    );

                                JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                        }
                    }
                }
                else if (Mode == SzignalasTipus.KuldemenySzignalas)
                {
                    if (String.IsNullOrEmpty(KuldemenyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        // Szervezetre szignálás:
                        if (SzervezetreSzignalas_HiddenField.Value == "1")
                        {
                            string szervezet_Id = Szervezet_CsoportTextBox_Kuld.Id_HiddenField;
                            if (String.IsNullOrEmpty(szervezet_Id))
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINincsMegadvaMindenAdat);
                                return;
                            }

                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            Result result = service.SzignalasSzervezetre(execParam, KuldemenyId, szervezet_Id, erec_HataridosFeladatok);
                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // hiba:
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            else
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyId);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                        }
                        else
                        {
                            // Ügyintézõre szignálás:

                            string ugyintezo_Id = Ugyintezo_FelhasznaloCsoportTextBox_Kuld.Id_HiddenField;
                            if (String.IsNullOrEmpty(ugyintezo_Id))
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINincsMegadvaMindenAdat);
                                return;
                            }

                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            //bernat.laszlo added
                            RightsService RightService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
                            //CR 3063 Szignálás hiba
                            if (Felhasznalo_MultiSelect1.FelhasznaloIds != null)
                            {
                                if (Felhasznalo_MultiSelect1.FelhasznaloIds.Length != 0)
                                {
                                    foreach (String _felhasznaloId in Felhasznalo_MultiSelect1.FelhasznaloIds)
                                    {
                                        string kuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
                                        Result right_result = RightService.AddCsoportToJogtargy(execParam, kuldemenyId, _felhasznaloId, 'I');
                                        if (!string.IsNullOrEmpty(right_result.ErrorCode))
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, right_result);
                                        }
                                    }
                                }
                            }
                            //bernat.laszlo eddig
                            EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                            string csoportId_Iktato = String.Empty;

                            if (FelhasznaloProfil.OrgIsBOPMH(Page))
                            {
                                csoportId_Iktato = Iktato_CsoportTextBox.Id_HiddenField;
                            }

                            Result result = service.SzignalasUgyintezore(execParam, KuldemenyId, ugyintezo_Id, erec_HataridosFeladatok, csoportId_Iktato);
                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // hiba:
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            else
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyId);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }

                        }

                    }
                }
                else if (Mode == SzignalasTipus.IratSzignalas)
                {
                    String kovFelelosId;

                    // Szervezetre szignálás:
                    if (SzervezetreSzignalas_HiddenField.Value == "1")
                    {
                        kovFelelosId = Szervezet_IratSzign_CsoportTextBox.Id_HiddenField;
                    }
                    else
                    {
                        // Ügyintézõre szignálás:

                        //String kovFelelosId = IratMasterAdatok1.UgyintezoId;
                        kovFelelosId = Ugyintezo_FelhasznaloCsoportTextBox_Irat.Id_HiddenField;
                    }

                    if (String.IsNullOrEmpty(kovFelelosId))
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                            , Resources.Error.UINincsMegadvaFelelos);
                        LoadFormComponents();
                        return;
                    }

                    // Szignálás

                    EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    Result result = service.Szignalas(execParam, IratId, kovFelelosId, null);

                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                    else
                    {
                        // BUG_7316
                        EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();
                        if (erec_HataridosFeladatok != null)
                        {
                            erec_HataridosFeladatok.Obj_Id = IratId;
                            erec_HataridosFeladatok.Updated.Obj_Id = true;

                            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
                            erec_HataridosFeladatok.Updated.Obj_type = true;

                            result = FeljegyzesPanel.InsertTaskForMoreUsers(null, erec_HataridosFeladatok);
                            if (result.IsError)
                            {
                                // log error
                                Log.Error.Page(Page, result);
                            }
                        }
                        //
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, IratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }

    }


    string GetDefaultIktatoCsoportId()
    {
        string defaultIktatoCsoportNev = Rendszerparameterek.Get(Page, "DEFAULT_IKTATO_NEV");

        if (!String.IsNullOrEmpty(defaultIktatoCsoportNev))
        {
            KRT_CsoportokService csopService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam csopXpm = UI.SetExecParamDefault(Page);
            KRT_CsoportokSearch csopSearch = new KRT_CsoportokSearch();

            csopSearch.Nev.Value = defaultIktatoCsoportNev;
            csopSearch.Nev.Operator = Query.Operators.equals;

            Result csopRes = csopService.GetAll(csopXpm, csopSearch);

            if (!csopRes.IsError)
            {
                if (csopRes.Ds.Tables[0].Rows.Count == 1)
                    return csopRes.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
        }

        return String.Empty;
    }

    #region Template  

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromTemplate((SzignalasTemplateObject)FormHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormHeader1.NewTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
    }

    private void LoadComponentsFromTemplate(SzignalasTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            #region hataridos feladat

            if (templateObject.HataridosFeladat != null)
            {
                FeljegyzesPanel.SetFeladatByBusinessObject(templateObject.HataridosFeladat);
            }
            #endregion

            if (Mode == SzignalasTipus.UgyiratSzignalas)
            {
                if (!String.IsNullOrEmpty(templateObject.SzignalasTipus))
                {
                    SetSelectedValue(RadioButtonList_SzignalasTipus, templateObject.SzignalasTipus);
                }

                Szervezet_UgySzign_CsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.FelhCsoport_Id_Selejtezo;
                Szervezet_UgySzign_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.Id_HiddenField = templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez;
                Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            }
            else if (Mode == SzignalasTipus.KuldemenySzignalas)
            {
                Iktato_CsoportTextBox.Id_HiddenField = templateObject.KuldemenyComponents.Csoport_Id_Felelos;
                Iktato_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                Szervezet_CsoportTextBox_Kuld.Id_HiddenField = templateObject.KuldemenyComponents.CsoportFelelosEloszto_Id;
                Szervezet_CsoportTextBox_Kuld.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                Ugyfelelos_CsoportTextBox_Kuld.Id_HiddenField = templateObject.IratComponents.Csoport_Id_Ugyfelelos;
                Ugyfelelos_CsoportTextBox_Kuld.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                Ugyintezo_FelhasznaloCsoportTextBox_Kuld.Id_HiddenField = templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez;
                Ugyintezo_FelhasznaloCsoportTextBox_Kuld.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
            else if (Mode == SzignalasTipus.IratSzignalas)
            {
                Ugyintezo_FelhasznaloCsoportTextBox_Irat.Id_HiddenField =
                     templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez;
                Ugyintezo_FelhasznaloCsoportTextBox_Irat.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }

        }
    }

    private void SetSelectedValue(RadioButtonList list, String selectedValue)
    {
        ListItem selectedListItem = list.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            list.SelectedValue = selectedValue;
        }
    }

    private SzignalasTemplateObject GetTemplateObjectFromComponents()
    {
        SzignalasTemplateObject templateObject = new SzignalasTemplateObject();

        //templateObject.Megjegyzes = Megjegyzes_TextBox.Text;

        templateObject.HataridosFeladat = FeljegyzesPanel.GetBusinessObject();

        if (RadioButtonList_SzignalasTipus.Visible)
        {
            templateObject.SzignalasTipus = RadioButtonList_SzignalasTipus.SelectedValue;
        }

        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            templateObject.UgyiratComponents.FelhCsoport_Id_Selejtezo = Szervezet_UgySzign_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez = Ugyintezo_FelhasznaloCsoportTextBox_Ugyirat.Id_HiddenField;
        }
        else if (Mode == SzignalasTipus.KuldemenySzignalas)
        {
            templateObject.KuldemenyComponents.Csoport_Id_Felelos = Iktato_CsoportTextBox.Id_HiddenField;
            templateObject.KuldemenyComponents.CsoportFelelosEloszto_Id = Szervezet_CsoportTextBox_Kuld.Id_HiddenField;
            templateObject.IratComponents.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox_Kuld.Id_HiddenField;
            templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez = Ugyintezo_FelhasznaloCsoportTextBox_Kuld.Id_HiddenField;
        }
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez = Ugyintezo_FelhasznaloCsoportTextBox_Irat.Id_HiddenField;
        }

        return templateObject;
    }

    #endregion
}
