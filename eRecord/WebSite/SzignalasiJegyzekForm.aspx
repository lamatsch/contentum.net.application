<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="SzignalasiJegyzekForm.aspx.cs" Inherits="SzignalasiJegyzekForm" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBoxTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label11" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label2" runat="server" Text="�gyt�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:RequiredTextBoxTextBox ID="UgyTipusRequiredTextBox" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                         <tr class="urlapSor" id="trLezaras" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Szign�l�si elj�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc9:KodtarakDropDownList ID="EljarasiSzakaszKodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trVegrehajto" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="�gyint�z�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                &nbsp;<uc5:CsoportTextBox ID="CsoportTextBox1" runat="server" />
                            </td>                            
                        </tr>
                                                
                    </table>
                    </eUI:eFormPanel>
              <%--      <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" OnLoad="FormPart_CreatedModified1_Load" /> --%>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
        
</asp:Content>

