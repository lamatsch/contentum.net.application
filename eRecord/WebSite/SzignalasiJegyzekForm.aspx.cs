using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class SzignalasiJegyzekForm : System.Web.UI.Page
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    private void SetNewControls()
    {
        //CsoportTextBoxFelelos.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        //CsoportTextBoxFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        //trVegrehajto.Visible = false;
        //trAtvevo.Visible = false;
        //trLezaras.Visible = false;
        //trMegsemmesites.Visible = false;
        //trSztornozas.Visible = false;
    }

    private void SetViewControls()
    {
        //Nev_TextBox.ReadOnly = true;
        //TipusDropDownList.Enabled = false;
        //CsoportTextBoxFelelos.ReadOnly = true;
        //CsoportTextBoxVegrehajto.ReadOnly = true;
        //PartnerTextBoxAtvevo.ReadOnly = true;
        //CalendarControlMegsemmisites.ReadOnly = true;
        //RovidNevTextBox.ReadOnly = true;
        IraIrattariTetelTextBox1.ReadOnly = true;
        UgyTipusRequiredTextBox.ReadOnly = true;
        EljarasiSzakaszKodtarakDropDownList.ReadOnly = true;
        //IratTipusKodtarakDropDownList.ReadOnly = true;
        //GeneraltTargyTextBox.ReadOnly = true;
        //IntezesiIdoTextBox.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        //TipusDropDownList.Enabled = false;
        //CsoportTextBoxFelelos.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetel" + Command);
                break;
        }

        if (!IsPostBack)
        {
            //UI.JegyzekTipus.FillDropDownList(TipusDropDownList);
            EljarasiSzakaszKodtarakDropDownList.FillDropDownList("ELJARASI_SZAKASZ", FormHeader1.ErrorPanel);
            //IratTipusKodtarakDropDownList.FillDropDownList("IRAT_FAJTA", FormHeader1.ErrorPanel);
            //EljarasiSzakaszKodtarakDropDownList.ReadOnly = true;
            //IratTipusKodtarakDropDownList.ReadOnly = true;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                //EREC_SzignalasiJegyzekekService service = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                //execParam.Record_Id = id;

                //Result result = service.Get(execParam);
                //if (String.IsNullOrEmpty(result.ErrorCode))
                //{
                //    EREC_SzignalasiJegyzekek EREC_SzignalasiJegyzek = (EREC_SzignalasiJegyzekek)result.Record;
                //    LoadComponentsFromBusinessObject(EREC_SzignalasiJegyzekek);
                //}
                //else
                //{
                //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                //}
            }
        }
        else if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.SzignalasiJegyzekekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    //private void LoadComponentsFromBusinessObject(EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek)
    //{

    //    RovidNevTextBox.Text = erec_SzignalasiJegyzek.Rovidnev;

    //    IraIrattariTetelTextBox1.Id_HiddenField = erec_SzignalasiJegyzek.Ugykor_Id;
    //    IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

    //    UgyTipusRequiredTextBox.Text = erec_SzignalasiJegyzek.Ugytipus;

    //    EljarasiSzakaszKodtarakDropDownList.SetSelectedValue(erec_SzignalasiJegyzek.EljarasiSzakasz);

    //    IratTipusKodtarakDropDownList.SetSelectedValue(erec_SzignalasiJegyzek.Irattipus);

    //    GeneraltTargyTextBox.Text = erec_SzignalasiJegyzek.GeneraltTargy;
    //    IntezesiIdoTextBox.Text = erec_SzignalasiJegyzek.UgyiratIntezesiIdo;

    //    //TipusDropDownList.SelectedValue = erec_SzignalasiJegyzekk.Tipus;


    //    FormHeader1.Record_Ver = erec_SzignalasiJegyzek.Base.Ver;
    //}

    //private EREC_SzignalasiJegyzek GetBusinessObjectFromComponents()
    //{
    //    EREC_SzignalasiJegyzek erec_SzignalasiJegyzek = new EREC_SzignalasiJegyzek();

    //    erec_SzignalasiJegyzek.Updated.SetValueAll(false);
    //    erec_SzignalasiJegyzek.Base.Updated.SetValueAll(false);

    //    erec_SzignalasiJegyzek.Rovidnev = RovidNevTextBox.Text;
    //    erec_SzignalasiJegyzek.Updated.Rovidnev = pageView.GetUpdatedByView(RovidNevTextBox);

    //    erec_SzignalasiJegyzek.Ugykor_Id = IraIrattariTetelTextBox1.Id_HiddenField;
    //    erec_SzignalasiJegyzek.Updated.Ugykor_Id = pageView.GetUpdatedByView(IraIrattariTetelTextBox1);

    //    erec_SzignalasiJegyzek.Ugytipus = UgyTipusRequiredTextBox.Text;
    //    erec_SzignalasiJegyzek.Updated.Ugytipus = pageView.GetUpdatedByView(UgyTipusRequiredTextBox);

    //    erec_SzignalasiJegyzek.EljarasiSzakasz = EljarasiSzakaszKodtarakDropDownList.SelectedValue;
    //    erec_SzignalasiJegyzek.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(EljarasiSzakaszKodtarakDropDownList);

    //    erec_SzignalasiJegyzek.Irattipus = IratTipusKodtarakDropDownList.SelectedValue;
    //    erec_SzignalasiJegyzek.Updated.Irattipus = pageView.GetUpdatedByView(IratTipusKodtarakDropDownList);

    //    erec_SzignalasiJegyzek.GeneraltTargy = GeneraltTargyTextBox.Text;
    //    erec_SzignalasiJegyzek.Updated.GeneraltTargy = pageView.GetUpdatedByView(GeneraltTargyTextBox);

    //    erec_SzignalasiJegyzek.UgyiratIntezesiIdo = IntezesiIdoTextBox.Text;
    //    erec_SzignalasiJegyzek.Updated.UgyiratIntezesiIdo = pageView.GetUpdatedByView(IntezesiIdoTextBox);


    //    erec_SzignalasiJegyzek.Base.Ver = FormHeader1.Record_Ver;
    //    erec_SzignalasiJegyzek.Base.Updated.Ver = true;

    //    return erec_SzignalasiJegyzek;
    //}

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(RovidNevTextBox);
            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(UgyTipusRequiredTextBox);
            compSelector.Add_ComponentOnClick(EljarasiSzakaszKodtarakDropDownList);
            //compSelector.Add_ComponentOnClick(IratTipusKodtarakDropDownList);
            //compSelector.Add_ComponentOnClick(GeneraltTargyTextBox);
            //compSelector.Add_ComponentOnClick(IntezesiIdoTextBox);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            //EREC_SzignalasiJegyzekekService service = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                            //EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek = GetBusinessObjectFromComponents();

                            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            //    Result result = service.Insert(execParam, erec_SzignalasiJegyzekek);

                            //if (String.IsNullOrEmpty(result.ErrorCode))
                            //{
                            // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                            // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                            //if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                            //{
                            //    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                            //}
                            //else
                            //{
                            //    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                            //    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            //}
                            //}
                            //else
                            //{
                            //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            //}
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                //EREC_SzignalasiJegyzekekService service = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                                //EREC_SzignalasiJegyzekek erec_SzignalasiJegyzek = GetBusinessObjectFromComponents();

                                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                //execParam.Record_Id = recordId;

                                //Result result = service.Update(execParam, erec_SzignalasiJegyzekek);

                                //if (String.IsNullOrEmpty(result.ErrorCode))
                                //{
                                //    JavaScripts.RegisterCloseWindowClientScript(Page);
                                //}
                                //else
                                //{
                                //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                //}
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
