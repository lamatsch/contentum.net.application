﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="SztornoPage.aspx.cs" Inherits="SztornoPage"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc15" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Szornó oldal" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short" colspan="2" style="text-align: left;">Vagy adja meg az iratok azonosítóját 'Iktatókönyv/főszám-alszám/év' formátumban vesszővel (,) elválasztva (Pl: 'XIV/39-1/2017')
                vagy pedig adja meg a főszám intervallumot.
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <asp:RadioButtonList ID="rblType" runat="server" Style="text-align: left" AutoPostBack="true" OnSelectedIndexChanged="rblType_SelectedIndexChanged">
                                <asp:ListItem Text="Irat azonosítók" Value="iratidk" Selected="True" />
                                <asp:ListItem Text="Főszám intervallum" Value="foszamintervallum" />
                            </asp:RadioButtonList>
                        </tr>
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label1" runat="server" Text="Iratok azonosítói:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox runat="server" ID="txtIratAzonositok" TextMode="MultiLine" Rows="10" Width="600px" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label8" runat="server" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="IktKonyv_Ev_EvIntervallum_SearchFormControl1"
                                    runat="server" Enabled="false"></uc11:EvIntervallum_SearchFormControl>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label7" runat="server" Text="Iktatókönyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList"
                                    runat="server" Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="IktKonyv_Ev_EvIntervallum_SearchFormControl1"
                                    Filter_IdeIktathat="false" IsMultiSearchMode="false" Enabled="false"></uc15:IraIktatoKonyvekDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label12" runat="server" Text="Fõszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc14:SzamIntervallum_SearchFormControl ID="Foszam_SzamIntervallum_SearchFormControl"
                                    runat="server" Enabled="false" />
                            </td>
                        </tr>

                        <tr class="urlapSor_kicsi" runat="server" id="trSztornozottak" visible="false">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label2" runat="server" Text="A sztornózás az alábbi iratokra futott le:"></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="text-align: left;">
                                <asp:TextBox runat="server" ID="lblSztornotottIratok" TextMode="MultiLine" Rows="25" Width="600px" Style="text-align: left;" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" runat="server" id="trMegmaradtUgyiratok" visible="false">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label4" runat="server" Text="A megmaradt ügyiratok:"></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="text-align: left;">
                                <asp:TextBox runat="server" ID="txtMegmaradtUgyiratok" TextMode="MultiLine" Rows="25" Width="600px" Style="text-align: left;" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" runat="server" id="trMegmaradIratok" visible="false">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label3" runat="server" Text="A sztornózási művelet után az alábbi iratok még nem lettek sztornózva:"></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="text-align: left;">
                                <asp:TextBox runat="server" ID="txtMegmaradtIratok" TextMode="MultiLine" Rows="25" Width="600px" Style="text-align: left;" />
                                <asp:HiddenField runat="server" ID="hiddenMegmaradtIratok" Value="" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" runat="server" id="trMegmaradtIratPeldanyok" visible="false">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label5" runat="server" Text="A sztornózási művelet után az alábbi iratpéldányok még nem lettek sztornózva:"></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="text-align: left;">
                                <asp:TextBox runat="server" ID="txtMegmaradtIratPeldanyok" TextMode="MultiLine" Rows="25" Width="600px" Style="text-align: left;" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" runat="server" id="trUpdate" visible="false">
                            <td class="mrUrlapCaption" style="vertical-align: top;">
                                <asp:Label ID="Label6" runat="server" Text="A gomb megnyomásával sztornózhatóak a kilistázott ügyiratok, iratok, és iratpéldányok:"></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="text-align: left;">
                                <asp:Button ID="btnSztorno" runat="server" Text="Sztornó" OnClick="btnSztorno_Click" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
