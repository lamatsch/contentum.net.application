﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.IO;
using System.Xml;
using System.Net;
using System.Collections.Generic;

public partial class SztornoPage : Contentum.eUtility.UI.PageBase
{
    public List<string> IratIds = new List<string>();
    public List<string> MegmaradtIratIds = new List<string>();
    public List<string> MegmaradtUgyiratIds = new List<string>();
    public List<string> MegmaradtIratPeldanyokIds = new List<string>();
    public string IratIdsStr = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
        EREC_IraIratokSearch defaultSearch = (EREC_IraIratokSearch)Search.GetDefaultSearchObject(typeof(EREC_IraIratokSearch));

        IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
               , false, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvTol, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvIg
               , true, false, IktatoKonyvek.GetIktatokonyvValue(defaultSearch.Extended_EREC_IraIktatoKonyvekSearch), null);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            SztornoMethode();
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    private void SztornoMethode()
    {
        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
        ExecParam execParamIratok = UI.SetExecParamDefault(Page, new ExecParam());


        if (rblType.SelectedValue == "iratidk")
        {
            IratIdsStr = txtIratAzonositok.Text.Replace(" ", "");
        }
        else if (rblType.SelectedValue == "foszamintervallum")
        {
            searchIratok.Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
            searchIratok.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            searchIratok.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Between(Foszam_SzamIntervallum_SearchFormControl.SzamTol, Foszam_SzamIntervallum_SearchFormControl.SzamIg);
            searchIratok.Extended_EREC_IraIktatoKonyvekSearch.Ev.Between(IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvTol, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvIg);
            searchIratok.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(IraIktatoKonyvekDropDownList.SelectedValue);

            searchIratok.Munkaallomas.Filter("ASP.ADO", Query.Operators.equals);
            searchIratok.WhereByManual = "(EREC_UgyUgyiratok.Allapot <> '09')";


            Result iratokResult = iratokService.GetAllWithExtensionAndJogosultak(execParamIratok, searchIratok, true);

            if (!iratokResult.IsError)
            {
                foreach (DataRow row in iratokResult.Ds.Tables[0].Rows)
                {
                    IratIds.Add(String.Format("'{0}'", row["IktatoSzam_Merge"].ToString()));
                }

                IratIdsStr = String.Join(", ", IratIds.ToArray()).Replace(" ", "");

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, iratokResult);
            }
        }

        if (!string.IsNullOrEmpty(IratIdsStr))
        {
            Result sztornoResult = iratokService.SztornoKiadmanyozottIratIds(IratIdsStr);

            if (!sztornoResult.IsError)
            {
                lblSztornotottIratok.Text = IratIdsStr;

                Result sztornoGet_ASPADO_IratokResult = null;

                if (rblType.SelectedValue == "iratidk")
                {
                    sztornoGet_ASPADO_IratokResult = iratokService.Get_ASPADO_Iratok_IratIds(execParamIratok, IratIdsStr);
                }
                else if (rblType.SelectedValue == "foszamintervallum")
                {
                    //Maradt-e ki valami. Vannak olyan esetek, amiket nem fed le a service, ezeket kiírjuk:
                    int FoszamFrom = Convert.ToInt32(Foszam_SzamIntervallum_SearchFormControl.SzamTol);
                    int FoszamTo = Convert.ToInt32(Foszam_SzamIntervallum_SearchFormControl.SzamIg);

                    sztornoGet_ASPADO_IratokResult = iratokService.Get_ASPADO_Iratok(execParamIratok, FoszamFrom, FoszamTo);
                }

                if (!sztornoGet_ASPADO_IratokResult.IsError)
                {
                    foreach (DataRow row in sztornoGet_ASPADO_IratokResult.Ds.Tables[0].Rows)
                    {
                        MegmaradtIratIds.Add(String.Format("'{0}'", row["IratAzonosito"].ToString()));
                    }

                    if (MegmaradtIratIds.Count > 0)
                    {
                        string strMegmaradtIratIds = String.Join(", ", MegmaradtIratIds.ToArray()).Replace(" ", "");
                        txtMegmaradtIratok.Text = "Megmaradt iratok száma: " + MegmaradtIratIds.Count.ToString() + " db" + Environment.NewLine + Environment.NewLine;
                        txtMegmaradtIratok.Text += strMegmaradtIratIds;



                        trMegmaradIratok.Visible = true;
                        //trMegmaradtIratPeldanyok.Visible = true;


                        Result sztornoGet_ASPADOResult = iratokService.ASPADOSztorno_Get(execParamIratok, strMegmaradtIratIds.Replace("'", ""));
                        hiddenMegmaradtIratok.Value = strMegmaradtIratIds.Replace("'", "");

                        if (!sztornoGet_ASPADOResult.IsError)
                        {
                            foreach (DataRow row in sztornoGet_ASPADOResult.Ds.Tables[0].Rows)
                            {
                                MegmaradtUgyiratIds.Add(String.Format("'{0}'", row["Azonosito"].ToString()));
                            }

                            if (MegmaradtUgyiratIds.Count > 0)
                            {
                                string strMegmaradtUgyiratIds = String.Join(", ", MegmaradtUgyiratIds.ToArray()).Replace(" ", "");
                                txtMegmaradtUgyiratok.Text = "Megmaradt ügyiratok száma: " + MegmaradtUgyiratIds.Count.ToString() + " db" + Environment.NewLine + Environment.NewLine;
                                txtMegmaradtUgyiratok.Text += strMegmaradtUgyiratIds;
                                trMegmaradtUgyiratok.Visible = true;
                            }

                            foreach (DataRow row in sztornoGet_ASPADOResult.Ds.Tables[1].Rows)
                            {
                                MegmaradtIratPeldanyokIds.Add(String.Format("'{0}'", row["Azonosito"].ToString()));
                            }

                            if (MegmaradtIratPeldanyokIds.Count > 0)
                            {
                                string strMegmaradtIratPeldanyokIds = String.Join(", ", MegmaradtIratPeldanyokIds.ToArray()).Replace(" ", "");
                                txtMegmaradtIratPeldanyok.Text = "Megmaradt iratpéldányokszáma: " + MegmaradtIratPeldanyokIds.Count.ToString() + " db" + Environment.NewLine + Environment.NewLine;
                                txtMegmaradtIratPeldanyok.Text += strMegmaradtIratPeldanyokIds;
                                trMegmaradtIratPeldanyok.Visible = true;
                            }

                            trUpdate.Visible = true;
                        }


                    }
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, sztornoResult);
            }
        }
    }

    protected void rblType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblType.SelectedValue == "iratidk")
        {
            Foszam_SzamIntervallum_SearchFormControl.Enabled = false;
            IraIktatoKonyvekDropDownList.Enabled = false;
            IktKonyv_Ev_EvIntervallum_SearchFormControl1.Enabled = false;

            trSztornozottak.Visible = false;
            txtIratAzonositok.Enabled = true;
        }
        else if (rblType.SelectedValue == "foszamintervallum")
        {
            Foszam_SzamIntervallum_SearchFormControl.Enabled = true;
            IraIktatoKonyvekDropDownList.Enabled = true;
            IktKonyv_Ev_EvIntervallum_SearchFormControl1.Enabled = true;

            trSztornozottak.Visible = true;
            txtIratAzonositok.Enabled = false;
        }

    }

    protected void btnSztorno_Click(object sender, EventArgs e)
    {
        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParamIratok = UI.SetExecParamDefault(Page, new ExecParam());

        Result sztornoUpdate_ASPADOResult = iratokService.ASPADOSztorno_Update(execParamIratok, hiddenMegmaradtIratok.Value);

        if (!sztornoUpdate_ASPADOResult.IsError)
        {
            txtMegmaradtIratok.Text =
            txtMegmaradtUgyiratok.Text =
            txtMegmaradtIratPeldanyok.Text = string.Empty;

            trMegmaradtUgyiratok.Visible =
            trMegmaradIratok.Visible =
            trMegmaradtIratPeldanyok.Visible =
            trUpdate.Visible = false;

            SztornoMethode();


        }
    }
}
