using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class TUKFelhasznalokLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        LovListHeader1.HeaderTitle = "TÜK Felhasználó kiválasztása";

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
             , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick =
              JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
             + JavaScripts.SetOnClientClick("PartnerekForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'"
             , Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
            ScriptManager1.SetFocus(TextBoxSearch);
        }
    }


    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        KRT_PartnerekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_PartnerekSearch)Search.GetSearchObject(Page, new KRT_PartnerekSearch());
        }
        else
        {
            search = new KRT_PartnerekSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
        search.Tipus.Operator = Query.Operators.equals;
        search.Belso.Value = Constants.Database.Yes;
        search.Belso.Operator = Query.Operators.equals;

        search.OrderBy = "Nev";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, "dr.");

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;
                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, false, tryFireChangeEvent);

                if (Request.QueryString.Get(QueryStringVars.RefreshCallingWindow) == "1")
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                     , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }

        string js = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}";

        //TextBoxSearch.Attributes.Add("onkeypress", js);

        string js2 = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(LovListFooter1.ImageButton_Ok, "") + ";}";

        ListBoxSearchResult.Attributes.Add("onkeypress", js2);

    }

}
