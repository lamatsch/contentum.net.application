<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TargySzavakList.aspx.cs" Inherits="TargySzavakList" Title="T�rgyszavak karbantart�sa" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="TargySzavakCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="TargySzavakUpdatePanel" runat="server" OnLoad="TargySzavakUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="TargySzavakCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="TargySzavakCPEButton" CollapseControlID="TargySzavakCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="TargySzavakCPEButton"
                            ExpandedSize="0" ExpandedText="T�rgyszavak/metaadatok list�ja" CollapsedText="T�rgyszavak/metaadatok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="TargySzavakGridView" runat="server" OnRowCommand="TargySzavakGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                            OnPreRender="TargySzavakGridView_PreRender" AutoGenerateColumns="False" DataKeyNames="Id"
                                            OnSorting="TargySzavakGridView_Sorting" OnRowDataBound="TargySzavakGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="TargySzavak" HeaderText="T�rgysz�" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                    SortExpression="TargySzavak" HeaderStyle-Width="200px" HeaderStyle-CssClass="GridViewBorderHeader" />
                                                <asp:TemplateField HeaderText="�rt�k tartozik hozz�" SortExpression="Tipus">
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbTipus" runat="server" Checked='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                                                            Enabled="false" Text='<%# Eval("Tipus") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CsoportNevTulaj" HeaderText="Tulajdonos csoport" SortExpression="CsoportNevTulaj">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
<%--                                                <asp:BoundField DataField="AlapertelmezettErtek" HeaderText="Alap�rtelmezett �rt�k"
                                                    SortExpression="AlapertelmezettErtek">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField AccessibleHeaderText="AlapertelmezettErtek" HeaderText="Alap�rtelmezett �rt�k"
                                                    SortExpression="AlapertelmezettErtek_Lekepezett">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%-- A Labeleket nem akarjuk besz�rk�teni, ez�rt csak a checkboxra �ll�tjuk be az Enabled="false"-t --%>
                                                        <eUI:DynamicValueControl ID="DVC_AlapertelmezettErtek" runat="server"
                                                        DefaultControlTypeSource='<%# Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label %>'
                                                        ControlTypeSource='<%# (Eval("ControlTypeSource") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox : Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label  %>'
                                                        Value='<%# Eval("AlapertelmezettErtek_Lekepezett") %>' Validate="false" Enabled='<%# (Eval("ControlTypeSource") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? false : true  %>'
                                                        />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RegExp" HeaderText="Regul�ris kif." SortExpression="RegExp">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BelsoAzonosito" HeaderText="Bels� azonos�t�" SortExpression="BelsoAzonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="Note">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Szinkroniz�lt" SortExpression="SPSSzinkronizalt">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbInvalidateEnabled" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                                CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton"
                                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ScrollContents="false"
                                CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                                ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <%-- panelDetail --%>
                            <asp:Panel ID="panelDetail" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="TabPanelIratMetaDefinicio" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="IratMetaDefinicioHeaderUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerIratMetaDefinicio" runat="server" Text="Irat metadefin�ci�k"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="IratMetaDefinicioUpdatePanel" runat="server" OnLoad="IratMetaDefinicioUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="IratMetaDefinicioPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="IratMetaDefinicioSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="IratMetaDefinicioCPE" runat="server" TargetControlID="panelIratMetaDefinicioList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelIratMetaDefinicioList" runat="server">
                                                                        <asp:GridView ID="IratMetaDefinicioGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                            OnSorting="IratMetaDefinicioGridView_Sorting" OnPreRender="IratMetaDefinicioGridView_PreRender"
                                                                            OnRowCommand="IratMetaDefinicioGridView_RowCommand" DataKeyNames="Id" OnRowDataBound="IratMetaDefinicioGridView_RowDataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Iratt�ri t�telsz�m"
                                                                                    SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" Wrap="false" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <%--
																	<asp:BoundField DataField="UgykorKod" HeaderText="Iratt�ri t�telsz�m" SortExpression="UgykorKod">
																		<HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
																		<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
																	</asp:BoundField>
																	--%>
                                                                                <asp:BoundField DataField="UgytipusNev" HeaderText="�gyt�pus" SortExpression="UgytipusNev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="EljarasiSzakasz" HeaderText="Elj�r�si szakasz" SortExpression="EljarasiSzakasz">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Irattipus" HeaderText="Iratt�pus" SortExpression="Irattipus">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TabPanelObjMetaDefinicio" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="ObjMetaDefinicioHeaderUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerObjMetaDefinicio" runat="server" Text="Objektum metadefin�ci�k"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="ObjMetaDefinicioUpdatePanel" runat="server" OnLoad="ObjMetaDefinicioUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="ObjMetaDefinicioPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="ObjMetaDefinicioSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="ObjMetaDefinicioCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <asp:GridView ID="ObjMetaDefinicioGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                            OnSorting="ObjMetaDefinicioGridView_Sorting" OnPreRender="ObjMetaDefinicioGridView_PreRender"
                                                                            OnRowCommand="ObjMetaDefinicioGridView_RowCommand" DataKeyNames="Id" OnRowDataBound="ObjMetaDefinicioGridView_RowDataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
<%--                                                                                <asp:BoundField DataField="ContentType" HeaderText="ContentType" SortExpression="ContentType">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                               <asp:TemplateField HeaderText="ContentType" SortExpression="ContentType">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Left" />
                                                                                    <ItemTemplate>
                                                                                            <asp:Label ID="labelContentType" runat="server" Text='<%#
                                                                                                string.IsNullOrEmpty(Eval("ContentType") as string) ? "" : "<a href=\"ObjMetaDefinicioList.aspx?Id="+ Eval("Id") as string + "\" style=\"text-decoration:underline\"> "
                                                                                                + Eval("ContentType") as string +"<a />"%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="ObjTipusok_Tabla" HeaderText="T�bla" SortExpression="ObjTipusok_Tabla">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ObjTipusok_Oszlop" HeaderText="Oszlop" SortExpression="ObjTipusok_Oszlop">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ColumnValue_Lekepezett" HeaderText="�rt�k" SortExpression="ColumnValue">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DefinicioTipus_Nev" HeaderText="Def. t�pus" SortExpression="DefinicioTipus_Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelGridHeaderSPSSzinkronizalt" runat="server" Text="Szinkroniz�lt" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" Enabled="false" AutoPostBack="false"
                                                                                            Checked='<%# (Eval("SPSSzinkronizalt") as string) == "1" ? true : false  %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                            <%-- /panelDetail --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
