
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class TargySzavakLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TargyszavakList");        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
                
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("TargySzavakForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("TargySzavakSearch.aspx", ""
            , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            FillGridViewSearchResult(false);
        }

        CsoportTextBoxTulaj.Validate = false;
        CsoportTextBoxTulaj.SzervezetCsoport = true;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, TargySzavakCPE,15);
    }



    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, TargySzavakCPE);
        FillGridViewSearchResult(false);
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbErtekTartozikHozza", "Tipus");
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_TargySzavakService service = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
        EREC_TargySzavakSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_TargySzavakSearch)Search.GetSearchObject(Page, new EREC_TargySzavakSearch());
        }
        else
        {
            search = new EREC_TargySzavakSearch();

            if (!String.IsNullOrEmpty(TargySzavak_TextBoxSearch.Text))
            {
                search.TargySzavak.Value = TargySzavak_TextBoxSearch.Text;
                search.TargySzavak.Operator = Search.GetOperatorByLikeCharater(TargySzavak_TextBoxSearch.Text);                
            }

            if (!String.IsNullOrEmpty(CsoportTextBoxTulaj.Text))
            {
                search.Csoport_Id_Tulaj.Value = CsoportTextBoxTulaj.Id_HiddenField;
                search.Csoport_Id_Tulaj.Operator = Query.Operators.equals;
            }
        }

        search.OrderBy = "TargySzavak";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(ExecParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        
     }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string TargySzavak = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string CsoportId = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 9);

                    string TipusValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);
                    string RegExpValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 6);
                    string AlapertelmezettErtekValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4);
                    string ToolTipValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 7);
                    string SPSSzinkronizaltValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 8);

                    string ControlTypeSourceValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 9);
                    string ControlTypeDataSourceValue = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 10);

                    string selectedText = TargySzavak;

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                    String TipusHiddenFieldId = Request.QueryString.Get(QueryStringVars.TipusHiddenFieldId);
                    if (!String.IsNullOrEmpty(TipusHiddenFieldId))
                    { 
                        Dictionary<string, string> dictionaryTipus = new Dictionary<string,string>();
                        dictionaryTipus.Add(TipusHiddenFieldId, TipusValue);

                        String RegExpHiddenFieldId = Request.QueryString.Get(QueryStringVars.RegExpHiddenFieldId);
                        if (!String.IsNullOrEmpty(RegExpHiddenFieldId))
                        {
                            dictionaryTipus.Add(RegExpHiddenFieldId, RegExpValue);
                            
                            String AlapertelmezettErtekHiddenFieldId = Request.QueryString.Get(QueryStringVars.AlapertelmezettErtekHiddenFieldId);
                            if (!String.IsNullOrEmpty(AlapertelmezettErtekHiddenFieldId))
                            {
                                dictionaryTipus.Add(AlapertelmezettErtekHiddenFieldId, AlapertelmezettErtekValue);

                                String ToolTipHiddenFieldId = Request.QueryString.Get(QueryStringVars.ToolTipHiddenFieldId);
                                if (!String.IsNullOrEmpty(ToolTipHiddenFieldId))
                                {
                                    dictionaryTipus.Add(ToolTipHiddenFieldId, ToolTipValue);
                                }
                            }

                            String ControlTypeSourceHiddenFieldId = Request.QueryString.Get(QueryStringVars.ControlTypeSourceHiddenFieldId);
                            if (!String.IsNullOrEmpty(ControlTypeSourceHiddenFieldId))
                            {
                                dictionaryTipus.Add(ControlTypeSourceHiddenFieldId, ControlTypeSourceValue);

                                String ControlTypeDataSource = Request.QueryString.Get(QueryStringVars.ControlTypeDataSourceHiddenFieldId);
                                if (!String.IsNullOrEmpty(ControlTypeDataSource))
                                {
                                    dictionaryTipus.Add(ControlTypeDataSource, ControlTypeDataSourceValue);
                                }
                            }
                        }


                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, dictionaryTipus, "ReturnValuesToParentWindowTipus", false);
                    }


                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    

    

    
    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }


}
