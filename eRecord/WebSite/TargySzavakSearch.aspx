<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" CodeFile="TargySzavakSearch.aspx.cs"
    Inherits="TargySzavakSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
    
    
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="labelTargySzavak" runat="server" Text="T�rgyszavak:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="requiredTextBoxTargySzavak" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
						    <td class="mrUrlapCaption">
						        <asp:Label ID="labelBelsoAzonosito" runat="server" Text="Bels� azonos�t�:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="TextBoxBelsoAzonosito" CssClass="mrUrlapInput" runat="server" />
						    </td>
					    </tr>
		                <tr class="urlapSor">
			                  <td class="mrUrlapCaption">
			                  </td>
			                  <td class="mrUrlapMezo">
			                      <asp:RadioButtonList ID="rblSPSSzinkronizalt" runat="server" >
				                        <asp:ListItem  Value="1" Selected="false" Text="Szinkroniz�lt" />
				                        <asp:ListItem Value="0" Selected="false" Text="Nem szinkroniz�lt" />
				                        <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
				                    </asp:RadioButtonList>
			                  </td>
		                </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
						        <asp:Label id="labelCsoportNevTulaj" runat="server" Text="Tulajdonos szervezet:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">		
					            <uc7:CsoportTextBox id="CsoportTextBoxTulaj" runat="server"></uc7:CsoportTextBox>
                            </td>
                        </tr>
		                <tr class="urlapSor">
			                  <td class="mrUrlapCaption">
			                  </td>
			                  <td class="mrUrlapMezo">
			                      <asp:RadioButtonList ID="rblTipus" runat="server" >
				                        <asp:ListItem  Value="1" Selected="false" Text="�rt�k tartozik hozz�" />
				                        <asp:ListItem Value="0" Selected="false" Text="�rt�k n�lk�l" />
				                        <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
				                    </asp:RadioButtonList>
			                  </td>
		                </tr>
					    <tr class="urlapSor">
						    <td class="mrUrlapCaption">
						        <asp:Label ID="labelAlapertelmezettErtek" runat="server" Text="Alap�rtelmezett �rt�k:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="TextBoxAlapertelmezettErtek" CssClass="mrUrlapInput" runat="server" />
						    </td>
					    </tr>
					    <tr class="urlapSor">
						    <td class="mrUrlapCaption">
						        <asp:Label ID="labelRegexp" runat="server" Text="Regul�ris kifejez�s:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="TextBoxRegexp" CssClass="mrUrlapInput" runat="server" />
						    </td>
					    </tr>
					    <tr class="urlapSor">
						    <td class="mrUrlapCaption">
						        <asp:Label ID="labelToolTip" runat="server" Text="Reg. kif. eszk�ztipp:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="TextBoxToolTip" CssClass="mrUrlapInput" runat="server" />
						    </td>
					    </tr>
	                    <tr class="urlapSor">
		                    <td class="mrUrlapCaption">
		                        <asp:Label ID="labelControlTypeSource" runat="server" Text="Vez�rl�elem forr�sa:"></asp:Label>
		                    </td>
		                    <td class="mrUrlapMezo">
			                    <ktddl:KodtarakDropDownList ID="KodTarakDropDownList_ControlTypeSource" CssClass="mrUrlapInputComboBox" runat="server" />
		                    </td>
	                    </tr>
	                    <tr class="urlapSor">
		                    <td class="mrUrlapCaption">
		                        <asp:Label ID="labelControlTypeDataSource" runat="server" Text="Vez�rl�elem adatforr�sa:"></asp:Label>
		                    </td>
		                    <td class="mrUrlapMezo">
			                    <asp:TextBox ID="TextBoxControlTypeDataSource" CssClass="mrUrlapInput" runat="server" />
		                    </td>
	                    </tr>
                        <tr class="urlapSor">
                            <td colspan="2" >
                                <uc9:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc9:Ervenyesseg_SearchFormComponent>
                                &nbsp;</td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                &nbsp; &nbsp;
                    
                    &nbsp; &nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
