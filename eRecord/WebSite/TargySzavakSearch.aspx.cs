using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class TargySzavakSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_TargySzavakSearch);

    private const string kcs_CONTROLTYPE_SOURCE = "CONTROLTYPE_SOURCE";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.TargySzavakSearchHeaderTitle;
       
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_TargySzavakSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_TargySzavakSearch)Search.GetSearchObject(Page, new EREC_TargySzavakSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            KodTarakDropDownList_ControlTypeSource.FillAndSetEmptyValue(kcs_CONTROLTYPE_SOURCE, SearchHeader1.ErrorPanel);
            LoadComponentsFromSearchObject(searchObject);
        }

        // Csoport kiv�laszt�sn�l sz�r�s a szervezetre:
        CsoportTextBoxTulaj.Validate = false;
        CsoportTextBoxTulaj.SzervezetCsoport = true;
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_TargySzavakSearch EREC_TargySzavakSearch = null;
        if (searchObject != null) EREC_TargySzavakSearch = (EREC_TargySzavakSearch)searchObject;

        if (EREC_TargySzavakSearch != null)
        {
            requiredTextBoxTargySzavak.Text = EREC_TargySzavakSearch.TargySzavak.Value;

            CsoportTextBoxTulaj.Id_HiddenField = EREC_TargySzavakSearch.Csoport_Id_Tulaj.Value;
            CsoportTextBoxTulaj.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            TextBoxRegexp.Text = EREC_TargySzavakSearch.RegExp.Value;
            TextBoxToolTip.Text = EREC_TargySzavakSearch.ToolTip.Value;
            TextBoxAlapertelmezettErtek.Text = EREC_TargySzavakSearch.AlapertelmezettErtek.Value;
            TextBoxBelsoAzonosito.Text = EREC_TargySzavakSearch.BelsoAzonosito.Value;

            KodTarakDropDownList_ControlTypeSource.SelectedValue = EREC_TargySzavakSearch.ControlTypeSource.Value;
            TextBoxControlTypeDataSource.Text = EREC_TargySzavakSearch.ControlTypeDataSource.Value;

            switch (EREC_TargySzavakSearch.SPSSzinkronizalt.Value)
            {
                case "0":
                    rblSPSSzinkronizalt.SelectedValue = "0";
                    break;
                case "1":
                    rblSPSSzinkronizalt.SelectedValue = "1";
                    break;
                default:
                    rblSPSSzinkronizalt.SelectedValue = "NotSet";
                    break;
            }

            switch (EREC_TargySzavakSearch.Tipus.Value)
            {
                case "0":
                    rblTipus.SelectedValue = "0";
                    break;
                case "1":
                    rblTipus.SelectedValue = "1";
                    break;
                default:
                    rblTipus.SelectedValue = "NotSet";
                    break;
            }

            Ervenyesseg_SearchFormComponent1.SetDefault(
                EREC_TargySzavakSearch.ErvKezd, EREC_TargySzavakSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private EREC_TargySzavakSearch SetSearchObjectFromComponents()
    {
        EREC_TargySzavakSearch EREC_TargySzavakSearch = (EREC_TargySzavakSearch)SearchHeader1.TemplateObject;
        if (EREC_TargySzavakSearch == null)
        {
            EREC_TargySzavakSearch = new EREC_TargySzavakSearch();
        }

        if (!String.IsNullOrEmpty(requiredTextBoxTargySzavak.Text))
        {
            EREC_TargySzavakSearch.TargySzavak.Value = requiredTextBoxTargySzavak.Text;
            EREC_TargySzavakSearch.TargySzavak.Operator = Search.GetOperatorByLikeCharater(requiredTextBoxTargySzavak.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxRegexp.Text))
        {
            EREC_TargySzavakSearch.RegExp.Value = TextBoxRegexp.Text;
            EREC_TargySzavakSearch.RegExp.Operator = Search.GetOperatorByLikeCharater(TextBoxRegexp.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxToolTip.Text))
        {
            EREC_TargySzavakSearch.ToolTip.Value = TextBoxToolTip.Text;
            EREC_TargySzavakSearch.ToolTip.Operator = Search.GetOperatorByLikeCharater(TextBoxToolTip.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxAlapertelmezettErtek.Text))
        {
            EREC_TargySzavakSearch.AlapertelmezettErtek.Value = TextBoxAlapertelmezettErtek.Text;
            EREC_TargySzavakSearch.AlapertelmezettErtek.Operator = Search.GetOperatorByLikeCharater(TextBoxAlapertelmezettErtek.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxBelsoAzonosito.Text))
        {
            EREC_TargySzavakSearch.BelsoAzonosito.Value = TextBoxBelsoAzonosito.Text;
            EREC_TargySzavakSearch.BelsoAzonosito.Operator = Search.GetOperatorByLikeCharater(TextBoxBelsoAzonosito.Text);
        }

        if (!String.IsNullOrEmpty(KodTarakDropDownList_ControlTypeSource.SelectedValue))
        {
            EREC_TargySzavakSearch.ControlTypeSource.Value = KodTarakDropDownList_ControlTypeSource.SelectedValue;
            EREC_TargySzavakSearch.ControlTypeSource.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(TextBoxControlTypeDataSource.Text))
        {
            EREC_TargySzavakSearch.ControlTypeDataSource.Value = TextBoxControlTypeDataSource.Text;
            EREC_TargySzavakSearch.ControlTypeDataSource.Operator = Search.GetOperatorByLikeCharater(TextBoxControlTypeDataSource.Text);
        }

        if (!String.IsNullOrEmpty(CsoportTextBoxTulaj.Id_HiddenField))
        {
            EREC_TargySzavakSearch.Csoport_Id_Tulaj.Value = CsoportTextBoxTulaj.Id_HiddenField;
            EREC_TargySzavakSearch.Csoport_Id_Tulaj.Operator = Query.Operators.equals;
        }

        switch (rblSPSSzinkronizalt.SelectedValue)
        {
            case "0":
                EREC_TargySzavakSearch.SPSSzinkronizalt.Value = "0";
                EREC_TargySzavakSearch.SPSSzinkronizalt.Operator = Query.Operators.equals;
                break;
            case "1":
                EREC_TargySzavakSearch.SPSSzinkronizalt.Value = "1";
                EREC_TargySzavakSearch.SPSSzinkronizalt.Operator = Query.Operators.equals;
                break;
            case "NotSet":
            default:
                break;
        }

        switch (rblTipus.SelectedValue)
        {
            case "0":
                EREC_TargySzavakSearch.Tipus.Value = "0";
                EREC_TargySzavakSearch.Tipus.Operator = Query.Operators.equals;
                break;
            case "1":
                EREC_TargySzavakSearch.Tipus.Value = "1";
                EREC_TargySzavakSearch.Tipus.Operator = Query.Operators.equals;
                break;
            case "NotSet":
            default:
                break;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            EREC_TargySzavakSearch.ErvKezd, EREC_TargySzavakSearch.ErvVege);        
              
        return EREC_TargySzavakSearch;
    }

  

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_TargySzavakSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_TargySzavakSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_TargySzavakSearch();
    }

}
