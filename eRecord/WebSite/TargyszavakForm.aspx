<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TargyszavakForm.aspx.cs" Inherits="TargyszavakForm" Title="T�rgyszavak" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
	TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
	TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
	TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>	
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagName="FuggoKodtarakDropDownList"
    TagPrefix="ktddl" %>	
<%@ Register Src="~/eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox"
    TagPrefix="tsztb" %>
<%@ Register Src="~/Component/MultiLineTextBox.ascx" TagName="MultiLineTextBox"
    TagPrefix="uc" %>	
<%--BLG_608--%>
<%@ Register Src="~/Component/KodtarakListBox.ascx" TagName="KodtarakListBox"
    TagPrefix="ut" %>	
<%@ Register Src="~/Component/KodtarakCheckBoxList.ascx" TagName="KodtarakCheckBoxList"
    TagPrefix="ut" %>	
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
	EnableScriptLocalization="true" >
	</asp:ScriptManager>
	<uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,TargyszavakFormHeaderTitle %>" />
	<br />
	<div class="popupBody">
	<eUI:eFormPanel ID="EFormPanel1" runat="server">
		<asp:HiddenField ID="Record_Csoport_Id_Tulaj_HiddenField" runat="server" />
			<table cellspacing="0" cellpadding="0" width="100%">
				<tbody>
					<tr class="urlapNyitoSor">
						<td class="mrUrlapCaption">
							<img alt=" " src="images/hu/design/spacertrans.gif" /></td>
						<td class="mrUrlapMezo">
							<img alt=" " src="images/hu/design/spacertrans.gif" /></td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelTargySzavak" runat="server" Text="T�rgyszavak:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc5:RequiredTextBox ID="requiredTextBoxTargySzavak" runat="server" />
						</td>
					</tr>
					<tr id="trBelsoAzonosito" class="urlapSor" runat="server">
						<td class="mrUrlapCaption">
						    <asp:Label ID="labelBelsoAzonosito" runat="server" Text="Bels� azonos�t�:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxBelsoAzonosito" CssClass="mrUrlapInput" ReadOnly="true" runat="server" />
						</td>
					</tr>
					<tr id="trSPSSzinkronizalt"  class="urlapSor" runat="server">
						<td class="mrUrlapCaption">
						</td>
						<td class="mrUrlapMezo">
							<asp:CheckBox ID="cbSPSSzinkronizalt" TextAlign="Right" Text="SPS-szinkroniz�lt" Enabled="false" runat="server" />
						</td>
					</tr>								
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
						    <asp:Label id="labelCsoportNevTulaj" runat="server" Text="Tulajdonos szervezet:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">		
					        <uc7:CsoportTextBox id="CsoportTextBoxTulaj" runat="server"></uc7:CsoportTextBox>
						</td>
					</tr>
					</tbody>
					</table>
<%-- Metaadatok jellemzoi --%>
<%--					<tr id="tr_MetaAdatok" class="urlapSor" runat="server">
						<td colspan="2">--%>
						    <asp:UpdatePanel ID="MetaAdatokUpdatePanel" runat="server" UpdateMode="Conditional" OnLoad="MetaAdatokUpdatePanel_Load">
						        <ContentTemplate>
						            <table cellspacing="0" cellpadding="0" width="100%" id="tr_MetaAdatok" runat="server">
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                    </td>
						                    <td class="mrUrlapMezo">
							                    <asp:CheckBox ID="cbTipus" checked="false" Text="�rt�k tartozik hozz�" TextAlign="Right"
							                    runat="server" AutoPostBack="false" />
						                    </td>
					                    </tr>
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelControlTypeSource" runat="server" Text="Vez�rl�elem forr�sa:"></asp:Label>
						                    </td>
						                    <td class="mrUrlapMezo">
							                    <ktddl:KodtarakDropDownList ID="KodTarakDropDownList_ControlTypeSource" CssClass="mrUrlapInputComboBox" runat="server" />
						                    </td>
					                    </tr>
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelControlTypeDataSource" runat="server" Text="Vez�rl�elem adatforr�sa:"></asp:Label>
						                    </td>
						                    <td class="mrUrlapMezo">
						                        <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
							                        ID="DynamicValueControl_ControlTypeDataSource" CssClass="mrUrlapInput" runat="server" />
						                    </td>
					                    </tr>
                                        <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelParentTargyszo" runat="server" Text="Vez�rl� t�rgysz�:"></asp:Label>
						                    </td>
						                    <td class="mrUrlapMezo">
						                        <tsztb:TargySzavakTextBox ID="ParentTargyszo" CustomValueEnabled="false" runat="server" Validate="false"/>
						                    </td>
					                    </tr>
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelAlapertelmezettErtek" runat="server" Text="Alap�rtelmezett �rt�k:"></asp:Label>
						                    </td>
						                    <%--<td class="mrUrlapMezo">
							                    <asp:TextBox ID="TextBoxAlapertelmezettErtek" CssClass="mrUrlapInput" runat="server" />
						                    </td>--%>
						                    <td class="mrUrlapMezo">
							                    <eUI:DynamicValueControl DefaultControlTypeSource="Contentum.eUIControls.CustomTextBox;Contentum.eUIControls"
							                            ID="DynamicValueControl_AlapertelmezettErtek" CssClass="mrUrlapInput" runat="server" />
							                </td>
					                    </tr>	
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelRegexp" runat="server" Text="Regul�ris kifejez�s:"></asp:Label>
						                    </td>
						                    <td class="mrUrlapMezo">
							                    <asp:TextBox ID="TextBoxRegexp" CssClass="mrUrlapInput" TextMode="MultiLine" Rows="3" runat="server" />
						                    </td>
					                    </tr>
					                    <tr class="urlapSor">
						                    <td class="mrUrlapCaption">
						                        <asp:Label ID="labelToolTip" runat="server" Text="Reg.kif./�rt�k eszk�ztipp:"></asp:Label>
						                    </td>
						                    <td class="mrUrlapMezo">
							                    <asp:TextBox ID="TextBoxToolTip" CssClass="mrUrlapInput" TextMode="MultiLine" Rows="3" runat="server" />
						                    </td>
					                    </tr>
					                </table>
					            </ContentTemplate>
					        </asp:UpdatePanel>
<%--					    </td>
					</tr>--%>
<%-- /Metaadatok jellemzoi --%>
			<table cellspacing="0" cellpadding="0" width="100%">
				<tbody>																		
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
						</td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
						<td class="mrUrlapMezo">
						<asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
						</td>
					</tr>					
				</tbody>
			</table>
		</eUI:eFormPanel>
	<uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
	<uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
	</div>
</asp:Content>