<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TelepulesekForm.aspx.cs" Inherits="TelepulesekForm" Title="Untitled Page" %>


<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/OrszagokTextBox.ascx" TagName="OrszagokTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/TelepulesekTextBox.ascx" TagName="TelepulesekTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc10" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,TelepulesekFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <uc7:OrszagokTextBox id="OrszagokTextBox1" runat="server"></uc7:OrszagokTextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                    </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                         <asp:Label ID="labelFoTelepules" runat="server" Text="F� telep�l�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <uc9:TelepulesekTextBox id="TelepulesekTextBoxFoTelepules" runat="server" Validate="false"></uc9:TelepulesekTextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <uc8:RequiredNumberBox ID="requiredNumberIrsz" runat="server" CssClass="mrUrlapInput"/>
                    </td>
                    </tr>
                   <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelRegio" runat="server" Text="R�gi�:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc10:KodtarakDropDownList ID="KodtarakDropDownListRegio" runat="server" />
                            </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelMegye" runat="server" Text="Megye:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc10:KodtarakDropDownList ID="KodtarakDropDownListMegye" runat="server" />
                            </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <asp:TextBox ID="textNote"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                            </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" >
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
     </div>
</asp:Content>

