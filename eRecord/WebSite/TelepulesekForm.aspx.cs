using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class TelepulesekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kodcsoportMegye = "MEGYE";
    private const string kodcsoportRegio = "REGIO";
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        TelepulesekTextBoxFoTelepules.ReadOnly = true;
        requiredTextBoxNev.ReadOnly = true;
        OrszagokTextBox1.ReadOnly = true;
        requiredNumberIrsz.ReadOnly = true;
        textNote.ReadOnly = true;
        //KodtarakDropDownListMegye.Enabled = false;
        //KodtarakDropDownListRegio.Enabled = false;
        KodtarakDropDownListMegye.ReadOnly = true;
        KodtarakDropDownListRegio.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelOrszag.CssClass = "mrUrlapInputWaterMarked";
        labelIranyitoszam.CssClass = "mrUrlapInputWaterMarked";
        labelFoTelepules.CssClass = "mrUrlapInputWaterMarked";
        labelMegye.CssClass = "mrUrlapInputWaterMarked";
        labelRegio.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Telepules" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Telepulesek krt_Telepulesek = (KRT_Telepulesek)result.Record;
                    LoadComponentsFromBusinessObject(krt_Telepulesek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string orszagID = Request.QueryString.Get("OrszagID");
            if (!String.IsNullOrEmpty(orszagID))
            {
                OrszagokTextBox1.Id_HiddenField = orszagID;
                OrszagokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
                OrszagokTextBox1.Enabled = false;
            }
            KodtarakDropDownListMegye.FillAndSetEmptyValue(kodcsoportMegye, FormHeader1.ErrorPanel);
            KodtarakDropDownListRegio.FillAndSetEmptyValue(kodcsoportRegio, FormHeader1.ErrorPanel);
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Telepulesek"></param>
    private void LoadComponentsFromBusinessObject(KRT_Telepulesek krt_Telepulesek)
    {
        OrszagokTextBox1.Id_HiddenField = krt_Telepulesek.Orszag_Id;
        OrszagokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        TelepulesekTextBoxFoTelepules.Id_HiddenField = krt_Telepulesek.Telepules_Id_Fo;
        TelepulesekTextBoxFoTelepules.SetTextBoxById(FormHeader1.ErrorPanel);
        requiredTextBoxNev.Text = krt_Telepulesek.Nev;
        requiredNumberIrsz.Text = krt_Telepulesek.IRSZ;
        if (Command == CommandName.View)
        {
            KodtarakDropDownListMegye.FillWithOneValue(kodcsoportMegye, krt_Telepulesek.Megye, FormHeader1.ErrorPanel);
            KodtarakDropDownListRegio.FillWithOneValue(kodcsoportRegio, krt_Telepulesek.Regio, FormHeader1.ErrorPanel);
        }
        else
        {
            KodtarakDropDownListMegye.FillAndSetSelectedValue(kodcsoportMegye, krt_Telepulesek.Megye, true, FormHeader1.ErrorPanel);
            KodtarakDropDownListRegio.FillAndSetSelectedValue(kodcsoportRegio, krt_Telepulesek.Regio, true,  FormHeader1.ErrorPanel);
        }
        textNote.Text = krt_Telepulesek.Base.Note;
        ErvenyessegCalendarControl1.ErvKezd = krt_Telepulesek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Telepulesek.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_Telepulesek.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_Telepulesek.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_Telepulesek GetBusinessObjectFromComponents()
    {
        KRT_Telepulesek krt_Telepulesek = new KRT_Telepulesek();
        krt_Telepulesek.Updated.SetValueAll(false);
        krt_Telepulesek.Base.Updated.SetValueAll(false);

        krt_Telepulesek.Orszag_Id = OrszagokTextBox1.Id_HiddenField;
        krt_Telepulesek.Updated.Orszag_Id = pageView.GetUpdatedByView(OrszagokTextBox1);
        krt_Telepulesek.Telepules_Id_Fo = TelepulesekTextBoxFoTelepules.Id_HiddenField;
        krt_Telepulesek.Updated.Telepules_Id_Fo = pageView.GetUpdatedByView(TelepulesekTextBoxFoTelepules);
        krt_Telepulesek.Nev = requiredTextBoxNev.Text;
        krt_Telepulesek.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        krt_Telepulesek.IRSZ = requiredNumberIrsz.Text;
        krt_Telepulesek.Updated.IRSZ = pageView.GetUpdatedByView(requiredNumberIrsz);
        krt_Telepulesek.Megye = KodtarakDropDownListMegye.SelectedValue;
        krt_Telepulesek.Updated.Megye = pageView.GetUpdatedByView(KodtarakDropDownListMegye);
        krt_Telepulesek.Regio = KodtarakDropDownListRegio.SelectedValue;
        krt_Telepulesek.Updated.Regio = pageView.GetUpdatedByView(KodtarakDropDownListRegio);
        krt_Telepulesek.Base.Note = textNote.Text;
        krt_Telepulesek.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        //krt_Telepulesek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_Telepulesek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_Telepulesek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_Telepulesek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Telepulesek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Telepulesek.Base.Ver = FormHeader1.Record_Ver;
        krt_Telepulesek.Base.Updated.Ver = true;

        return krt_Telepulesek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(OrszagokTextBox1);
            compSelector.Add_ComponentOnClick(TelepulesekTextBoxFoTelepules);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(requiredNumberIrsz);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListMegye);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListRegio);
            compSelector.Add_ComponentOnClick(textNote);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Telepules" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
                            KRT_Telepulesek krt_Telepulesek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Telepulesek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
                                KRT_Telepulesek krt_Telepulesek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Telepulesek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
