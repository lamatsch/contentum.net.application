using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;


public partial class TelepulesekLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;
    private const string beginSeperator = " (";
    private const string endSeperator = ")";

    private string getTelepulesNev()
    {
        string text = ListBoxSearchResult.SelectedItem.Text;
        int index = text.IndexOf(beginSeperator);
        if (index > -1)
        {
            return  text.Substring(0, index);
        }
        else
        {
            return text;
        }
    }

    private string getIrsz()
    {
        string text = ListBoxSearchResult.SelectedItem.Text;
        int index = text.IndexOf(beginSeperator);
        if (index > -1)
        {
            return text.Substring(index + beginSeperator.Length,text.Length - index - beginSeperator.Length - endSeperator.Length);
        }
        else
        {
            return String.Empty;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TelepulesekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("TelepulesekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
        + JavaScripts.SetOnClientClick("TelepulesekForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
        KRT_TelepulesekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_TelepulesekSearch)Search.GetSearchObject(Page, new KRT_TelepulesekSearch());
            TextBoxSearch.Text = "";
        }
        else
        {
            search = new KRT_TelepulesekSearch();
            search.Nev.Value = SearchKey;
            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }
        search.OrderBy = "Nev, IRSZ";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page,Rendszerparameterek.LOVLIST_MAX_ROW);
        
        Result result = service.GetAll(execParam, search);

        List<string> columnsName = new List<string>(2);
        columnsName.Add("Nev");
        columnsName.Add("IRSZ");

        UI.ListBoxFill(ListBoxSearchResult, result,columnsName,beginSeperator,")", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = getTelepulesNev();

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                //ha kell az ir�ny�t�sz�mot is visszaadjuk a h�v� formnak
                string irszTextBoxClientId = Request.QueryString.Get(QueryStringVars.IrszTextBoxClientId);
                string selectedIrsz = getIrsz();

                if (!String.IsNullOrEmpty(irszTextBoxClientId) && !String.IsNullOrEmpty(selectedIrsz))
                {
                    Dictionary<string, string> returnParams = new Dictionary<string, string>();
                    returnParams.Add(irszTextBoxClientId, selectedIrsz);
                    JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnParams, "IrszReturnValue");
                }

                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }

}
