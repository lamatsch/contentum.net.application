<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TelepulesekSearch.aspx.cs" Inherits="TelepulesekSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc5" %>
<%@ Register Src="Component/OrszagokTextBox.ascx" TagName="OrszagokTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/TelepulesekTextBox.ascx" TagName="TelepulesekTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc9" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,TelepulesekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label></td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <uc6:OrszagokTextBox ID="OrszagokTextBox1" runat="server" SearchMode="true"/>
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                               </tr>
                               <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:RequiredNumberBox ID="requiredNumberIrsz"  CssClass="mrUrlapInput" runat="server" Validate="false" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFoTelepules" runat="server" Text="F� telep�l�s:"></asp:Label></td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <uc7:TelepulesekTextBox ID="TelepulesekTextBoxFoTelepules" runat="server" Validate="false"/>
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelRegio" runat="server" Text="R�gi�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList runat="server" ID="KodtarakDropDownListRegio" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelMegye" runat="server" Text="Megye:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList runat="server" ID="KodtarakDropDownListMegye" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                     <uc5:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                    </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                               </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                               </tr>
                               <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                <tr class="urlapSor">
                                     <td colspan="5">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

