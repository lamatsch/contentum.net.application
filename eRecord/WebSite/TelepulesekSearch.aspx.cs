using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class TelepulesekSearch : Contentum.eUtility.UI.PageBase
{
    private const string kodcsoportMegye = "MEGYE";
    private const string kodcsoportRegio = "REGIO";

    private Type _type = typeof(KRT_TelepulesekSearch);


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_TelepulesekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_TelepulesekSearch)Search.GetSearchObject(Page, new KRT_TelepulesekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_TelepulesekSearch _KRT_TelepulesekSearch = (KRT_TelepulesekSearch)searchObject;

        if (_KRT_TelepulesekSearch != null)
        {
            OrszagokTextBox1.Id_HiddenField = _KRT_TelepulesekSearch.Orszag_Id.Value;
            OrszagokTextBox1.SetTextBoxById(null);
            TelepulesekTextBoxFoTelepules.Id_HiddenField = _KRT_TelepulesekSearch.Telepules_Id_Fo.Value;
            TelepulesekTextBoxFoTelepules.SetTextBoxById(null);
            textNev.Text = _KRT_TelepulesekSearch.Nev.Value;
            requiredNumberIrsz.Text = _KRT_TelepulesekSearch.IRSZ.Value;
            KodtarakDropDownListMegye.FillAndSetSelectedValue(kodcsoportMegye, _KRT_TelepulesekSearch.Megye.Value, true, null);
            KodtarakDropDownListRegio.FillAndSetSelectedValue(kodcsoportRegio, _KRT_TelepulesekSearch.Regio.Value, true, null);

            Ervenyesseg_SearchFormComponent1.SetDefault(
               _KRT_TelepulesekSearch.ErvKezd, _KRT_TelepulesekSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_TelepulesekSearch SetSearchObjectFromComponents()
    {
        KRT_TelepulesekSearch _KRT_TelepulesekSearch = (KRT_TelepulesekSearch)SearchHeader1.TemplateObject;

        if (_KRT_TelepulesekSearch == null)
        {
            _KRT_TelepulesekSearch = new KRT_TelepulesekSearch();
        }

        if (OrszagokTextBox1.Id_HiddenField != "")
        {
            _KRT_TelepulesekSearch.Orszag_Id.Value = OrszagokTextBox1.Id_HiddenField;
            _KRT_TelepulesekSearch.Orszag_Id.Operator = Query.Operators.equals;
        }

        if (TelepulesekTextBoxFoTelepules.Id_HiddenField != "")
        {
            _KRT_TelepulesekSearch.Telepules_Id_Fo.Value = TelepulesekTextBoxFoTelepules.Id_HiddenField;
            _KRT_TelepulesekSearch.Telepules_Id_Fo.Operator = Query.Operators.equals;
        }

        _KRT_TelepulesekSearch.Nev.Value = textNev.Text;
        _KRT_TelepulesekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _KRT_TelepulesekSearch.IRSZ.Value = requiredNumberIrsz.Text;
        if(_KRT_TelepulesekSearch.IRSZ.Value != "")
            _KRT_TelepulesekSearch.IRSZ.Operator = Query.Operators.equals;
        _KRT_TelepulesekSearch.Regio.Value = KodtarakDropDownListRegio.SelectedValue;
        if (_KRT_TelepulesekSearch.Regio.Value != "")
            _KRT_TelepulesekSearch.Regio.Operator = Query.Operators.equals;
        _KRT_TelepulesekSearch.Megye.Value = KodtarakDropDownListMegye.SelectedValue;
        if (_KRT_TelepulesekSearch.Megye.Value != "")
            _KRT_TelepulesekSearch.Megye.Operator = Query.Operators.equals;

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_TelepulesekSearch.ErvKezd, _KRT_TelepulesekSearch.ErvVege);

        return _KRT_TelepulesekSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_TelepulesekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            //kiv�lasztott rekord t�rl�se
            ////JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_TelepulesekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_TelepulesekSearch();
    }
}
