﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TeljessegEllenorzes.aspx.cs" Inherits="TeljessegEllenorzes" EnableEventValidation="false" %>

<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/VonalKodListBox.ascx" TagName="VonalKodListBox" TagPrefix="uc4" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc7" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>

<%@ Register src="eRecordComponent/UgyiratMasterAdatok.ascx" tagname="UgyiratMasterAdatok" tagprefix="uc3" %>
<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="infoPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional" OnLoad="ErrorUpdatePanel1_Load"
        RenderMode="Inline">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>
                            
    <asp:Panel ID="MainPanel" runat="server" Visible="true">
        
        <table cellpadding="0" cellspacing="0" align="left">
            <tr>
                <td colspan="3">
                    <asp:UpdatePanel ID="UgyiratMasterAdatok_UpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ajaxToolkit:CollapsiblePanelExtender ID="UgyiratMasterAdatokCPE" runat="server"
                                TargetControlID="UgyiratMasterAdatokPanel" CollapsedSize="0" Collapsed="true"
                                AutoCollapse="false" AutoExpand="false" ScrollContents="true" ExpandedText="<%$Resources:Form,UI_UgyiratAdatok_ExpandedText%>"
                                CollapsedText="<%$Resources:Form,UI_UgyiratAdatok_CollapsedText%>" TextLabelID="labelUgyiratMasterAdatokShowHide"
                                ExpandControlID="UgyiratMasterAdatokHeader" CollapseControlID="UgyiratMasterAdatokHeader"
                                ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                ImageControlID="UgyiratMasterAdatokCPEButton" >
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr class="tabExtendableListFejlec">
                                    <td style="text-align: left;">
                                        <asp:Panel runat="server" ID="UgyiratMasterAdatokHeader" CssClass="cursor_Hand" Visible="false">
                                            <asp:ImageButton runat="server" ID="UgyiratMasterAdatokCPEButton" ImageUrl="images/hu/Grid/plus.gif"
                                                OnClientClick="return false;" />
                                            &nbsp;
                                            <asp:Label ID="labelUgyiratMasterAdatokShowHide" runat="server" Text="<%$Resources:Form,UI_UgyiratAdatok_CollapsedText%>" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="UgyiratMasterAdatokPanel" runat="server" Visible="false">
                                            <uc3:UgyiratMasterAdatok ID="UgyiratMasterAdatok1" runat="server" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:UpdatePanel ID="VonalkodosBeolvasoUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <span style="position: absolute; left: 31px;">
                                <asp:ImageButton runat="server" ID="SearchCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                                    OnClientClick="return false;" />
                            </span>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeSearch" runat="server" TargetControlID="SearchPanel"
                                CollapsedSize="10" Collapsed="False" ExpandControlID="SearchCPEButton" CollapseControlID="SearchCPEButton"
                                ExpandDirection="Horizontal" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="SearchResultCPEButton"
                                ExpandedSize="0" ExpandedText="Összecsuk" CollapsedText="Kinyit">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="SearchPanel" runat="server">
                                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                    <table>
                                        <tr>
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelVonalkod" runat="server" Text="Ügyirat vonalkód:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <uc1:vonalkodtextbox id="UgyiratVonalkodTextBox" runat="server" validate="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="mrUrlapCaptionBal_indent">
                                                <asp:Label ID="label1" runat="server" Text="Ügyirat elemek beolvasása:"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <uc4:VonalKodListBox runat="server" ID="VonalKodListBoxSearch" Rows="6" />
                                            </td>
                                        </tr>
                                    </table>
                                </eUI:eFormPanel>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="text-align: center;">
                                            <br />
                                            <table style="width: 60%;">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <asp:ImageButton ID="ImageRendben" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                                            onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                                             CommandName="Save" onclick="ImageRendben_Click" />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:ImageButton ID="ImageAlapallapot" runat="server" ImageUrl="~/images/hu/ovalgomb/alapallapot.jpg"
                                                            onmouseover="swapByName(this.id,'alapallapot2.jpg')" onmouseout="swapByName(this.id,'alapallapot.jpg')"
                                                            OnClick="ImageButton_Alapallapot_Click" CommandName="Default" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td style="text-align: left; vertical-align: top; width: 60%;">
                    <asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server">
                        <ContentTemplate>
                        
                            <infoPopup:InfoModalPopup ID="InfoModalPopup1" runat="server" HeaderText=""  />
                        
                            <asp:ImageButton runat="server" ID="SearchResultCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                                OnClientClick="return false;" CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ajaxtoolkit:collapsiblepanelextender id="SearchResultCPE" runat="server" targetcontrolid="SearchResultPanel"
                                collapsedsize="20" collapsed="False" expandcontrolid="SearchResultCPEButton"
                                collapsecontrolid="SearchResultCPEButton" expanddirection="Vertical" autocollapse="false"
                                autoexpand="false" collapsedimage="images/hu/Grid/plus.gif" expandedimage="images/hu/Grid/minus.gif"
                                imagecontrolid="SearchResultCPEButton" expandedsize="0" expandedtext="Keresés eredménye"
                                collapsedtext="Kézbesítési tételek listája">
                            </ajaxtoolkit:collapsiblepanelextender>
                            <eui:eformpanel id="eformPanelSearchResult" runat="server" visible="false">
                                <table style="width: 60%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="color: #335674; font-weight: bold; padding-bottom: 15px; font-size: 14px;">
                                            Az ellenőrzés eredménye
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: top;">
                                            <asp:Panel ID="ResultPanel_MindenRendben" runat="server" Visible="false">
                                                <asp:Label ID="label5" runat="server" Text="Nincs hiányzó irat, minden az ügyiratban van."
                                                    Font-Bold="true"></asp:Label>                                                
                                            </asp:Panel>
                                        
                                            <asp:Panel ID="ResultPanel_hianyzoIratok" runat="server" Visible="false" HorizontalAlign="Center">
                                              <asp:Panel ID="ResultPanel_hianyzoIratok_Header" runat="server" CssClass="cursor_Hand">
                                                <asp:ImageButton runat="server" ID="HianyzoIratok_VonalkodokList_CPEButton"
                                                    ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                                                &nbsp;
                                                <asp:Label ID="labelHianyzoIratok" runat="server" Text="Hiányzó iratok: "
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                                <asp:Label ID="labelHianyzoIratok_Darab" runat="server" Text="0 darab"
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                              </asp:Panel>      
                                                <ajaxToolkit:CollapsiblePanelExtender ID="HianyzoIratok_VonalkodokList_CPE" runat="server"
                                                    TargetControlID="PanelHianyzoIratok_VonalkodokList" CollapsedSize="0" Collapsed="true"
                                                    AutoCollapse="false" AutoExpand="false" ScrollContents="true"
                                                    ExpandControlID="ResultPanel_hianyzoIratok_Header" CollapseControlID="ResultPanel_hianyzoIratok_Header"
                                                    ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                                    TextLabelID="labelHianyzoIratok"
                                                    ImageControlID="HianyzoIratok_VonalkodokList_CPEButton">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="PanelHianyzoIratok_VonalkodokList" runat="server">
                                                    <asp:Label ID="labelHianyzoIratok_VonalkodokList" runat="server" Text="" CssClass="warningHeader"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>
                                        <asp:Panel ID="ResultPanel_nincsAzUgyiratban" runat="server" Visible="false" HorizontalAlign="Center">                                                
                                            <asp:Panel ID="ResultPanel_nincsAzUgyiratban_Header" runat="server" CssClass="cursor_Hand">
                                                 <asp:ImageButton runat="server" ID="NincsAzUgyiratban_VonalkodokList_CPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                                                  &nbsp; 
                                                  <asp:Label ID="label2" runat="server" Text="Nincs az ügyiratban: "
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                                <asp:Label ID="labelNincsAzUgyiratban_Darab" runat="server" Text="0 darab"
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                              </asp:Panel> 
                                                                                                      
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="NincsAzUgyiratban_VonalkodokList_CPE" runat="server"
                                                    TargetControlID="PanelNincsAzUgyiratban_VonalkodokList" CollapsedSize="0" Collapsed="true"
                                                    AutoCollapse="false" AutoExpand="false" ScrollContents="true"
                                                    ExpandControlID="ResultPanel_nincsAzUgyiratban_Header" CollapseControlID="ResultPanel_nincsAzUgyiratban_Header"
                                                    ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                                    ImageControlID="NincsAzUgyiratban_VonalkodokList_CPEButton">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="PanelNincsAzUgyiratban_VonalkodokList" runat="server">
                                                    <asp:Label ID="labelNincsAzUgyiratban_VonalkodokList" runat="server" Text="" CssClass="warningHeader"></asp:Label>
                                                </asp:Panel>
                                                   
                                                 </asp:Panel>
                                            <asp:Panel ID="ResultPanel_kezeloNemEgyezik" runat="server" Visible="false" HorizontalAlign="Center">         
                                                <asp:Panel ID="ResultPanel_kezeloNemEgyezik_Header" runat="server" CssClass="cursor_Hand">
                                                <asp:ImageButton runat="server" ID="KezeloNemEgyezik_VonalkodokList_CPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                                                  &nbsp;
                                                    <asp:Label ID="label3" runat="server" Text="Kezelő nem egyezik: "
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                                <asp:Label ID="labelKezeloNemEgyezik_Darab" runat="server" Text="0 darab"
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                                </asp:Panel>
                                                    
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="KezeloNemEgyezik_VonalkodokList_CPE" runat="server"
                                                    TargetControlID="PanelKezeloNemEgyezik_VonalkodokList" CollapsedSize="0" Collapsed="true"
                                                    AutoCollapse="false" AutoExpand="false" ScrollContents="true"
                                                    ExpandControlID="ResultPanel_kezeloNemEgyezik_Header" CollapseControlID="ResultPanel_kezeloNemEgyezik_Header"
                                                    ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                                    ImageControlID="KezeloNemEgyezik_VonalkodokList_CPEButton">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="PanelKezeloNemEgyezik_VonalkodokList" runat="server">
                                                    <asp:Label ID="labelKezeloNemEgyezik_VonalkodokList" runat="server" Text="" CssClass="warningHeader"></asp:Label>
                                                </asp:Panel>
                                                                                                  
                                            </asp:Panel>
                                            <asp:Panel ID="ResultPanel_NemAzonositottVonalkodok" runat="server" Visible="false" HorizontalAlign="Center">
                                                <asp:Panel ID="ResultPanel_NemAzonositottVonalkodok_Header" runat="server" CssClass="cursor_Hand">
                                                <asp:ImageButton runat="server" ID="NemAzonositottVonalkodok_VonalkodokList_CPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                                                  &nbsp;
                                                <asp:Label ID="label4" runat="server" Text="Nem azonosított vonalkódok: "
                                                    Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                                                <asp:Label ID="labelNemAzonositottak_Darab" runat="server" Text="0 darab" Font-Bold="true" CssClass="warningHeader"></asp:Label>                                                    
                                                </asp:Panel>                                                
                                                
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="NemAzonositottak_VonalkodokList_CPE" runat="server"
                                                    TargetControlID="PanelNemAzonositottak_VonalkodokList" CollapsedSize="0" Collapsed="true"
                                                    AutoCollapse="false" AutoExpand="false" ScrollContents="true"
                                                    ExpandControlID="ResultPanel_NemAzonositottVonalkodok_Header" CollapseControlID="ResultPanel_NemAzonositottVonalkodok_Header"
                                                    ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                                    ImageControlID="NemAzonositottVonalkodok_VonalkodokList_CPEButton">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                
                                                <asp:Panel ID="PanelNemAzonositottak_VonalkodokList" runat="server">
                                                    <asp:Label ID="labelNemAzonositottak_VonalkodokList" runat="server" Text="" Font-Bold="True" CssClass="warningBody"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>

                                            <asp:Panel ID="ResultPanel_Munkapeldanyok" runat="server" Visible="false" HorizontalAlign="Center">
                                                <asp:Panel ID="ResultPanel_Munkapeldanyok_Header" runat="server" CssClass="cursor_Hand">
                                                <asp:ImageButton runat="server" ID="ResultPanel_Munkapeldanyok_CPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                                                  &nbsp;
                                                <asp:Label ID="label6" runat="server" Text="Munkairatok: "
                                                    Font-Bold="true" CssClass="warningHeader"></asp:Label>
                                                <asp:Label ID="labelMunkapeldanyok_Darab" runat="server" Text="0 darab" Font-Bold="true" CssClass="warningHeader"></asp:Label>                                                    
                                                </asp:Panel>                                                         
                                                
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="Munkapeldanyok_CPE" runat="server"
                                                    TargetControlID="PanelMunkapeldanyok_VonalkodokList" CollapsedSize="0" Collapsed="true"
                                                    AutoCollapse="false" AutoExpand="false" ScrollContents="true"
                                                    ExpandControlID="ResultPanel_Munkapeldanyok_Header" CollapseControlID="ResultPanel_Munkapeldanyok_Header"
                                                    ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                                                    ImageControlID="ResultPanel_Munkapeldanyok_CPEButton">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                
                                                <asp:Panel ID="PanelMunkapeldanyok_VonalkodokList" runat="server">
                                                    <span class="warningHeader">A munkairatok az irattárba küldéskor törlésre kerülnek!</span>
                                                    <asp:Label ID="labelMunkapeldanyok_VonalkodokList" runat="server" Text="" Font-Bold="True" CssClass="warningBody"></asp:Label>
                                                </asp:Panel>
                                            </asp:Panel>

                                            <br />
                                            <asp:Panel ID="SearchResultPanel" runat="server" CssClass="searchResultPanel">
                                                <asp:GridView ID="SearchResultGridView" runat="server" OnRowCommand="SearchResultGridView_RowCommand"
                                                    CellPadding="0" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                    AllowPaging="True" PagerSettings-Visible="false" AllowSorting="False" OnPreRender="SearchResultGridView_PreRender"
                                                    AutoGenerateColumns="False" DataKeyNames="Id"
                                                    >
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                            SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                                                                                
                                                        <asp:TemplateField HeaderText="Alszám">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" /> 
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label_Alszam" runat="server" Text='<% #Eval("Alszam") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy">
                                                             <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                                            
                                                        </asp:BoundField>
                                                        
                                                        <asp:BoundField DataField="BarCode" HeaderText="Vonalkód">
                                                             <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                                    Visible="false" OnClientClick="return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </asp:Panel>                                            
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
</asp:Content>
