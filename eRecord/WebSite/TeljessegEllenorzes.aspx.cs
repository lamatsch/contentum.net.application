﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class TeljessegEllenorzes : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();    
    private List<string> VonalkodokList_Beolvasott;


    #region Page Base

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Jogosultság-ellenőrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TeljessegEllenorzes");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadVonalkodListBoxState();

        ListHeader1.HeaderLabel = Resources.Form.UgyiratTeljessegEllenorzesHeaderTitle;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;

        #region Baloldali funkciógombok kiszedése
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        ListHeader1.RefreshVisible = false;
        #endregion

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(SearchResultUpdatePanel.ClientID);

        ListHeader1.CustomSearchObjectSessionName = "kérdőjelek ne jelenjenek meg";

        ListHeader1.AttachedGridView = SearchResultGridView;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        registerJavascripts();
    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(SearchResultGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        // Ne legyen lapozás:
        ListHeader1.PagerVisible = false;
    }

    #endregion


    // Rendben gombra kattintás
    protected void ImageRendben_Click(object sender, ImageClickEventArgs e)
    {
        if (string.IsNullOrEmpty(UgyiratVonalkodTextBox.Text))
        {
            // Nincs megadva az ügyirat vonalkódja:
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, "Nincs megadva az ügyirat vonalkódja!");
            ErrorUpdatePanel.Update();
            return;
        }

        #region Ügyirat Id meghatározás, master adatok megjelenítése

        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        EREC_UgyUgyiratokSearch searchUgyiratok = new EREC_UgyUgyiratokSearch();
        searchUgyiratok.BARCODE.Value = UgyiratVonalkodTextBox.Text;
        searchUgyiratok.BARCODE.Operator = Query.Operators.equals;

        Result result_ugyiratGetAllByBarCode = service_ugyiratok.GetAll(UI.SetExecParamDefault(Page), searchUgyiratok);
        if (result_ugyiratGetAllByBarCode.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGetAllByBarCode);
            ErrorUpdatePanel.Update();
            return;
        }

        if (result_ugyiratGetAllByBarCode.Ds.Tables[0].Rows.Count == 0)
        {
            // hiba: Nem található a megadott vonalkódú ügyirat
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, "Nem található a megadott vonalkódú ügyirat");
            ErrorUpdatePanel.Update();
            return;
        }

        string ugyiratId = result_ugyiratGetAllByBarCode.Ds.Tables[0].Rows[0]["Id"].ToString();

        // Master adatok feltöltése:
        UgyiratMasterAdatok1.UgyiratId = ugyiratId;
        EREC_UgyUgyiratok erec_ugyugyiratok = UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(EErrorPanel1, ErrorUpdatePanel);
        // Panel megjelenítése:
        UgyiratMasterAdatokHeader.Visible = true;
        UgyiratMasterAdatokPanel.Visible = true;
        UgyiratMasterAdatok_UpdatePanel.Update();

        #endregion

        Analyze();

    }


    private void Analyze()
    {
        /// - Ügyirat iratpéldányainak lekérdezése        
        /// - Elemzés: melyek azok az iratpéldányok, amik nem lettek beolvasva; illetve a beolvasottak között melyek nincsenek bent az ügyiratban
        ///  (azokat külön gyűjteni, amiket beolvasott, az ügyirat iratpéldánya, de nem elsődleges. Ezeket majd hozzáadjuk a listához)
        /// - GridView-ban az ügyirat iratainak megjelenítése, színezés a nem beolvasott iratpéldányokra,
        ///  illetve másfajta színezés azokra az iratpéldányokra, amik nincsenek az ügyiratban (más a kezelő vagy az őrző)

        string ugyiratId = UgyiratMasterAdatok1.UgyiratId;

        if (string.IsNullOrEmpty(ugyiratId))
        {
            return;
        }

        #region Ügyirat iratpéldányainak lekérése

        EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

        EREC_PldIratPeldanyokSearch search_ugyiratPeldanyai = new EREC_PldIratPeldanyokSearch(true);

        // Ügyirat-ra szűrünk, és az 1-es sorszámú példányokat hozzuk:
        search_ugyiratPeldanyai.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        search_ugyiratPeldanyai.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        search_ugyiratPeldanyai.Sorszam.Value = "1";
        search_ugyiratPeldanyai.Sorszam.Operator = Query.Operators.equals;

        // rendezés alszám szerint
        search_ugyiratPeldanyai.OrderBy = "EREC_IraIratok.Alszam";

        Result result_ugyiratPeldanyai = service_pld.GetAllWithExtension(UI.SetExecParamDefault(Page), search_ugyiratPeldanyai);

        if (result_ugyiratPeldanyai.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratPeldanyai);
            ErrorUpdatePanel.Update();
            return;
        }

        #endregion
        
        List<string> VonalkodokList_UgyiratPeldanyai = new List<string>();

        #region Elemzés - melyek azok az iratpéldányok, amik nem lettek beolvasva

        // amiket nem csippantott be, de az ügyiratban benne vannak:
        List<string> hianyzoVonalkodok = new List<string>();

        foreach (DataRow row in result_ugyiratPeldanyai.Ds.Tables[0].Rows)
        {
            string row_BarCode = row["BarCode"].ToString();

            // ügyirat iratpéldány vonalkódok kigyűjtése:
            if (VonalkodokList_UgyiratPeldanyai.Contains(row_BarCode) == false && !String.IsNullOrEmpty(row_BarCode))
            {
                VonalkodokList_UgyiratPeldanyai.Add(row_BarCode);
            }

            // ha nincs a beolvasottak között:
            if (VonalkodokList_Beolvasott.Contains(row_BarCode) == false)
            {
                hianyzoVonalkodok.Add(row_BarCode);
            }
        }

        // hiányzó vonalkódokat lementjük ViewState-be
        ViewState[viewStateName_hianyzoVonalkodok] = hianyzoVonalkodok;

        #endregion

        #region Elemzés: A beolvasottak között melyek nincsenek bent az ügyiratban

        List<string> VonalkodokList_nemAzonositottak = new List<string>();
        // in-es kereséshez össze kell fűzni őket:
        string nemAzonositottVonalkodokStr = String.Empty;

        foreach (string vonalkod in VonalkodokList_Beolvasott)
        {
            // ha nincs az ügyirat elsődleges példányai között, felvesszük a listába:
            if (VonalkodokList_UgyiratPeldanyai.Contains(vonalkod) == false)
            {
                VonalkodokList_nemAzonositottak.Add(vonalkod);

                // 
                if (String.IsNullOrEmpty(nemAzonositottVonalkodokStr))
                {
                    nemAzonositottVonalkodokStr += "'" + vonalkod + "'";
                }
                else
                {
                    nemAzonositottVonalkodokStr += ",'" + vonalkod + "'";
                }
            }
        }        

        #endregion

        #region Nem azonosított vonalkódok elemzése 
        
        if (VonalkodokList_nemAzonositottak.Count > 0)
        {
            // Ami az ügyiratban van (csak nem elsődleges példányok), azokat kigyűjtjük

            EREC_PldIratPeldanyokSearch search_NemAzonositottPeldanyok = new EREC_PldIratPeldanyokSearch(true);

            search_NemAzonositottPeldanyok.BarCode.Value = nemAzonositottVonalkodokStr;
            search_NemAzonositottPeldanyok.BarCode.Operator = Query.Operators.inner;

            search_NemAzonositottPeldanyok.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
            search_NemAzonositottPeldanyok.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

            Result result_NemAzonositottPeldanyokGetAll = service_pld.GetAllWithExtension(UI.SetExecParamDefault(Page), search_NemAzonositottPeldanyok);
            if (result_NemAzonositottPeldanyokGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_NemAzonositottPeldanyokGetAll);
                ErrorUpdatePanel.Update();
                return;
            }

            foreach (DataRow row in result_NemAzonositottPeldanyokGetAll.Ds.Tables[0].Rows)
            {
                string row_BarCode = row["BarCode"].ToString();

                if (VonalkodokList_UgyiratPeldanyai.Contains(row_BarCode) == false)
                {
                    // Az adatsort felvesszük az ügyirat példányai közé, hogy majd ezt is megjeleníthessük:
                    DataTable table = result_ugyiratPeldanyai.Ds.Tables[0];

                    int position = GetRowPosition(row, table);

                    // A megfelelő pozícióba beszúrjuk:
                    
                    DataRow newRow = table.NewRow();
                    newRow.ItemArray = row.ItemArray;
                    table.Rows.InsertAt(newRow, position);
                }

                // Ki kell szedni a nem azonosított vonalkódok közül:
                if (VonalkodokList_nemAzonositottak.Contains(row_BarCode))
                {
                    VonalkodokList_nemAzonositottak.Remove(row_BarCode);
                }
            }
                        
        }

        #endregion

        #region Munkairat van-e

        List<string> munkapeldanyVonalkodok = new List<string>();
        foreach (DataRow row in result_ugyiratPeldanyai.Ds.Tables[0].Rows)
        {
            string row_BarCode = row["BarCode"].ToString();

            string iratAlszam = row["Alszam"].ToString();
            if (String.IsNullOrEmpty(iratAlszam) || iratAlszam == "0")
            {
                munkapeldanyVonalkodok.Add(row_BarCode);
            }
        }

        #endregion

        #region GridView feltöltése, megfelelő sorok megjelölése:

        // hibás sorok számolásához inicializálás:
        iratPeldanyok_hianyzoVonalkodok.Clear();
        iratPeldanyok_nincsenekAzUgyiratban.Clear();
        iratPeldanyok_kezeloNemEgyezik.Clear();
        
        FillGridView(result_ugyiratPeldanyai, hianyzoVonalkodok);

        #endregion

        #region Eredmény megjelenítése

        bool mindenRendben = true;

        // Hiányzó vonalkódok (amiket nem olvasott be):
        if (hianyzoVonalkodok.Count > 0)
        {
            ResultPanel_hianyzoIratok.Visible = true;
            labelHianyzoIratok_Darab.Text = hianyzoVonalkodok.Count.ToString() + " darab";

            labelHianyzoIratok_VonalkodokList.Text = CreateResultPanelHtmlList(iratPeldanyok_hianyzoVonalkodok);
            mindenRendben = false;
        }
        else
        {
            ResultPanel_hianyzoIratok.Visible = false;
        }

        // Amik nincsenek az ügyiratban:
        if (iratPeldanyok_nincsenekAzUgyiratban.Count > 0)
        {
            ResultPanel_nincsAzUgyiratban.Visible = true;
            labelNincsAzUgyiratban_Darab.Text = iratPeldanyok_nincsenekAzUgyiratban.Count.ToString() + " darab";

            labelNincsAzUgyiratban_VonalkodokList.Text = CreateResultPanelHtmlList(iratPeldanyok_nincsenekAzUgyiratban);
            mindenRendben = false;
        }
        else
        {
            ResultPanel_nincsAzUgyiratban.Visible = false;
        }

        // Ahol a kezelő nem egyezik:
        if (iratPeldanyok_kezeloNemEgyezik.Count > 0)
        {
            ResultPanel_kezeloNemEgyezik.Visible = true;
            labelKezeloNemEgyezik_Darab.Text = iratPeldanyok_kezeloNemEgyezik.Count.ToString() + " darab";

            labelKezeloNemEgyezik_VonalkodokList.Text = CreateResultPanelHtmlList(iratPeldanyok_kezeloNemEgyezik);
            mindenRendben = false;
        }
        else
        {
            ResultPanel_kezeloNemEgyezik.Visible = false;
        }

        // Nem azonosított vonalkódok megjelenítése:
        if (VonalkodokList_nemAzonositottak.Count > 0)
        {
            ResultPanel_NemAzonositottVonalkodok.Visible = true;
            labelNemAzonositottak_Darab.Text = VonalkodokList_nemAzonositottak.Count.ToString() + " darab";

            labelNemAzonositottak_VonalkodokList.Text = CreateResultPanelHtmlList(VonalkodokList_nemAzonositottak);
            mindenRendben = false;
        }
        else
        {
            ResultPanel_NemAzonositottVonalkodok.Visible = false;
        }

        // Munkapéldányok megjelenítése:
        if (munkapeldanyVonalkodok.Count > 0)
        {
            ResultPanel_Munkapeldanyok.Visible = true;
            labelMunkapeldanyok_Darab.Text = munkapeldanyVonalkodok.Count.ToString() + " darab";

            labelMunkapeldanyok_VonalkodokList.Text = CreateResultPanelHtmlList(munkapeldanyVonalkodok);
            mindenRendben = false;
        }
        else
        {
            ResultPanel_Munkapeldanyok.Visible = false;
        }

        // Ha minden ok:
        if (mindenRendben)
        {
            ResultPanel_MindenRendben.Visible = true;
        }
        else
        {
            ResultPanel_MindenRendben.Visible = false;
        }
        
        #endregion

    }

    // Alszám alapján megkeressük a helyét a táblában:
    private int GetRowPosition(DataRow row, DataTable table)
    {
        int alszam;
        Int32.TryParse(row["Alszam"].ToString(), out alszam);

        for (int i=0;i<table.Rows.Count;i++)
        {
            DataRow tableRow = table.Rows[i];

            int alszam_TableRow;
            Int32.TryParse(tableRow["Alszam"].ToString(), out alszam_TableRow);

            // Alszámok növekvő sorrendjén megyünk végig, ha meghaladjuk a megadott alszámot, megvan a pozíció
            if (alszam_TableRow > alszam)
            {
                return i;
            }
        }

        // ha a megadott alszám a legnagyobb, akkor az utolsó pozíciót adjuk vissza:
        return table.Rows.Count;        
    }

    private const string viewStateName_hianyzoVonalkodok = "hianyzoVonalkodok";

    private List<DataRowView> iratPeldanyok_hianyzoVonalkodok = new List<DataRowView>();
    private List<DataRowView> iratPeldanyok_nincsenekAzUgyiratban = new List<DataRowView>();
    private List<DataRowView> iratPeldanyok_kezeloNemEgyezik = new List<DataRowView>();


    private void FillGridView(Result result_ugyiratPeldanyai, List<string> hianyzoVonalkodok)
    {
        SearchResultGridView.RowDataBound +=new GridViewRowEventHandler(SearchResultGridView_RowDataBound);

        ui.GridViewFill(SearchResultGridView, result_ugyiratPeldanyai, EErrorPanel1, ErrorUpdatePanel);
        //UI.GridViewFill(SearchResultGridView, result_ugyiratPeldanyai, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

        // panel megjelenítése:
        eformPanelSearchResult.Visible = true;
    }



    protected void SearchResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        List<string> hianyzoVonalkodok = null;
        if (ViewState[viewStateName_hianyzoVonalkodok] != null)
        {
            hianyzoVonalkodok = (List<string>)ViewState[viewStateName_hianyzoVonalkodok];
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;


            // Ha nem egyes sorszámú az iratpéldány, kiírjuk:
            if (drv["Sorszam"] != null)
            {
                string sorszam = drv["Sorszam"].ToString();
                if (!String.IsNullOrEmpty(sorszam))
                {
                    int sorszam_int;
                    Int32.TryParse(sorszam, out sorszam_int);
                    if (sorszam_int > 1)
                    {
                        Label label_Alszam = (Label)e.Row.FindControl("Label_Alszam");
                        if (label_Alszam != null)
                        {
                            label_Alszam.Text += "&nbsp;(" + sorszam + ".&nbsp;példány)";
                        }
                    }
                }
            }

            bool hianyzoVonalkod = false;
            bool nincsAzUgyiratban = false;
            bool kezeloNemEgyezik = false;

            ErrorDetails errorDetail_orzo = null;
            ErrorDetails errorDetail_felelos = null;

            // hiányzó vonalkódok megjelölése:
            if (hianyzoVonalkodok != null)
            {
                if (drv["BarCode"] != null)
                {
                    string vonalkod = drv["BarCode"].ToString();

                    if (hianyzoVonalkodok.Contains(vonalkod))
                    {
                        hianyzoVonalkod = true;
                        // listába gyűjtés:
                        iratPeldanyok_hianyzoVonalkodok.Add(drv);
                    }
                }
            }

            string pld_Csoport_Id_Felelos = String.Empty;
            if (drv["Csoport_Id_Felelos"] != null)
            {
                pld_Csoport_Id_Felelos = drv["Csoport_Id_Felelos"].ToString();
            }

            string pld_FelhasznaloCsoport_Id_Orzo = String.Empty;
            if (drv["FelhasznaloCsoport_Id_Orzo"] != null)
            {
                pld_FelhasznaloCsoport_Id_Orzo = drv["FelhasznaloCsoport_Id_Orzo"].ToString();
            }

            string ugyirat_orzo_Id = String.Empty;
            if (drv["EREC_UgyUgyiratok_FelhCsopIdOrzo"] != null)
            {
                ugyirat_orzo_Id = drv["EREC_UgyUgyiratok_FelhCsopIdOrzo"].ToString();
            }

            string ugyirat_felelos_Id = String.Empty;
            if (drv["EREC_UgyUgyiratok_CsopIdFelelos"] != null)
            {
                ugyirat_felelos_Id = drv["EREC_UgyUgyiratok_CsopIdFelelos"].ToString();
            }

            string iratpeldany_Id = String.Empty;
            if (drv["Id"]!= null)
            {
                iratpeldany_Id = drv["Id"].ToString();
            }

            // Felelős nem egyezik:
            if (!String.IsNullOrEmpty(ugyirat_felelos_Id)
               && pld_Csoport_Id_Felelos != ugyirat_felelos_Id)
            {
                kezeloNemEgyezik = true;
                // kigyűjtjük egy listába:
                iratPeldanyok_kezeloNemEgyezik.Add(drv);

                errorDetail_felelos = new ErrorDetails("Nem egyezik az ügyirat és az iratpéldány kezelője!"
                    , Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, iratpeldany_Id
                    , Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos, pld_Csoport_Id_Felelos);
            }

            // nem ügyiratban lévő iratok (első iratpéldány alapján) megjelölése:
            // (ha az iratpéldánynak más az őrzője)
            if (!String.IsNullOrEmpty(ugyirat_orzo_Id) &&
                pld_FelhasznaloCsoport_Id_Orzo != ugyirat_orzo_Id)
            {
                nincsAzUgyiratban = true;
                // kigyűjtjük egy listába:
                iratPeldanyok_nincsenekAzUgyiratban.Add(drv);

                errorDetail_orzo = new ErrorDetails("Nem egyezik az ügyirat és az iratpéldány őrzője!"
                     , Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, iratpeldany_Id
                     , Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo, pld_FelhasznaloCsoport_Id_Orzo);
            }
                        

            // Szín állítása:
            if (hianyzoVonalkod)
            {   
                e.Row.BackColor = System.Drawing.Color.Yellow;

                //if (nincsAzUgyiratban || kezeloNemEgyezik)
                //{
                //    e.Row.BackColor = System.Drawing.Color.Orange;
                //}
            }
            else if (nincsAzUgyiratban || kezeloNemEgyezik)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
            }


            #region Info image állítása
            ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");

            if (infoImageButton != null && (nincsAzUgyiratban || kezeloNemEgyezik))
            {
                string infoMessage = String.Empty;
                if (nincsAzUgyiratban)
                {
                    infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail_orzo, Page);
                }
                else if (kezeloNemEgyezik)
                {
                    infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail_felelos, Page);
                }

                if (!string.IsNullOrEmpty(infoMessage))
                {
                    infoImageButton.Visible = true;
                    infoImageButton.ToolTip = infoMessage;                    
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                            + Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok + "','" + iratpeldany_Id + "',null,null); return false;";
                }
            }
            #endregion


        }

    }

    private void LoadVonalkodListBoxState()
    {
        VonalKodListBoxSearch.ListBox.Items.Clear();
        VonalkodokList_Beolvasott = new List<string>();
        string[] items;
        if (String.IsNullOrEmpty(VonalKodListBoxSearch.VonalkodClientList))
        {
            items = new string[0];
        }
        else
        {
            items = VonalKodListBoxSearch.VonalkodClientList.Split(',');
        }

        foreach (string item in items)
        {
            VonalKodListBoxSearch.ListBox.Items.Add(new ListItem(item, item));
            VonalkodokList_Beolvasott.Add(item);
        }
    }


    private string CreateResultPanelHtmlList(List<DataRowView> iratPeldanyokDrvList)
    {
        // Megjelenítendő lista előállítása:
        List<string> displayedList = new List<string>();

        foreach (DataRowView drv in iratPeldanyokDrvList)
        {
            string BarCode = (drv["BarCode"] != null) ? drv["BarCode"].ToString() : String.Empty;
            string Alszam = (drv["Alszam"] != null) ? drv["Alszam"].ToString() : String.Empty;

            displayedList.Add(BarCode + "&nbsp;&nbsp;&nbsp; (Alszám: " + Alszam + ")");
        }

        return CreateResultPanelHtmlList(displayedList);
    }

    private string CreateResultPanelHtmlList(List<string> list)
    {
        string text = "<ul>";

        foreach (string listItem in list)
        {
            text += "<li>";
            text += listItem;
            text += "</li>";
        }

        text += "</ul>";

        return text;
    }


    private void ResetForm()
    {
        UgyiratVonalkodTextBox.Text = String.Empty;

        VonalKodListBoxSearch.ListBox.Items.Clear();
        VonalKodListBoxSearch.TextBox.Text = String.Empty;
        VonalKodListBoxSearch.VonalkodClientList = String.Empty;

        eformPanelSearchResult.Visible = false;

        // ügyirat master adatok eltüntetése:
        UgyiratMasterAdatokHeader.Visible = false;
        UgyiratMasterAdatokPanel.Visible = false;
        UgyiratMasterAdatok_UpdatePanel.Update();
    }
    

    private void SetSearchFieldFromArray(Field Idfield, List<string> IdArray)
    {
        bool elso = true;
        Idfield.Operator = Query.Operators.inner;
        Idfield.Value = String.Empty;
        foreach (string id in IdArray)
        {
            if (!elso)
            {
                Idfield.Value += ",";
            }
            Idfield.Value += "'" + id + "'";

            elso = false;
        }
    }

   

    private void RemoveVonalkodFromArray(DataRow row)
    {
        string kod = row["BarCode"].ToString();

        if (!String.IsNullOrEmpty(kod))
        {

            if (!String.IsNullOrEmpty(kod))
            {
                if (VonalkodokList_Beolvasott.Contains(kod))
                {
                    VonalkodokList_Beolvasott.Remove(kod);
                }
            }
        }
    }

    protected void SearchResultGridView_PreRender(object sender, EventArgs e)
    {
        //int prev_PageIndex = SearchResultGridView.PageIndex;

        //SearchResultGridView.PageIndex = ListHeader1.PageIndex;
        //ListHeader1.PageCount = SearchResultGridView.PageCount;

        //if (prev_PageIndex != SearchResultGridView.PageIndex)
        //{
        //    //scroll állapotának törlése
        //    JavaScripts.ResetScroll(Page, SearchResultCPE);
        //    //SearchResultGridViewBind();
        //}
        //else
        //{
        //    UI.GridViewSetScrollable(ListHeader1.Scrollable, SearchResultCPE);
        //}

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(SearchResultGridView);

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        //SearchResultGridViewBind();
    }

    protected void SearchResultGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            //int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(SearchResultGridView, selectedRowNumber, "check");
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, SearchResultPanel.ClientID);


            EREC_PldIratPeldanyok obj_iratPeldany = null;
            EREC_IraIratok obj_irat = null;
            EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
            EREC_UgyUgyiratok obj_ugyirat = null;

            bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                , Page, EErrorPanel1);
            //if (success_objectsGet == false)
            //{
            //    return;
            //}

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);

            // irathoz első iratpéldány őrzőjét csak akkor állítjuk be, ha ez az iratpéldány volt az első
            // TODO: minden esetben be kéne állítani az első iratpéldány őrzőjét
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);

            ErrorDetails errorDetail;

            // Módosítás
            if (!IratPeldanyok.Modosithato(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_IratPeldanyMegtekintes
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" +
                    JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, SearchResultPanel.ClientID, EventArgumentConst.refreshMasterList)
                    + "} else return false;";
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, SearchResultPanel.ClientID, EventArgumentConst.refreshMasterList);

            }

        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        JavaScripts.SetFocus(UgyiratVonalkodTextBox.TextBox);
    }

    protected void ErrorUpdatePanel1_Load(object sender, EventArgs e)
    {
    }


    // Alapállapot gombra kattintás
    protected void ImageButton_Alapallapot_Click(object sender, ImageClickEventArgs e)
    {
        ResetForm();
    }


}
