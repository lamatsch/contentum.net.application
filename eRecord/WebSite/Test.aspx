﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<%@ Register Src="Component/BizottsagTextBox.ascx" TagName="BizottsagTextBox"
    TagPrefix="uc" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc9" %>

<%@ Register Src="Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr id="Tr2" class="urlapSor" runat="server">
                    <td class="mrUrlapCaption"></td>
                    <td class="mrUrlapMezo">
                          <uc1:EditablePartnerTextBoxWithTypeFilter ID="EditablePartnerTextBoxWithTypeFilter1" runat="server" />
                    </td>
                </tr>

                <tr id="Tr1" class="urlapSor" runat="server">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelBizottsag" runat="server" Text="Bizottság:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                      
                        <uc:BizottsagTextBox ID="BizottsagTextBox" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>Tabfooter
                    </td>
                    <td>
                        <uc9:TabFooter ID="Footer" runat="server" Visible="true" Command="New" />
                        <%--<uc9:TabFooter ID="TabFooter1666" runat="server" Visible="true" Command="Modify" />--%>
                    </td>
                </tr>
            </tbody>
        </table>
        <div>
        </div>
    </eUI:eFormPanel>
</asp:Content>

