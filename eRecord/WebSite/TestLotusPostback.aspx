﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestLotusPostback.aspx.cs" Inherits="TestLotusPostback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title></title>
    <script src="JavaScripts/jquery-3.2.1.min.js"></script>
    <script src="JavaScripts/json2.js"></script>

    <script type="text/javascript">
        // Create IE + others compatible event handler
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

        // Listen to message from child window
        eventer(messageEvent, function (e) {
            //alert(e.data);
            alert(JSON.stringify(e.data));
            console.log('Parent received message!:  ', e.data);
        }, false);
    </script>

</head>
<body>
    <iframe src="<%=Page.ResolveUrl("~/UgyUgyiratokList.aspx?Lotus=1")%>" width="1280" height="768"></iframe>
    <iframe src="<%=Page.ResolveUrl("~/IraIratokList.aspx?Lotus=1")%>" width="1280" height="768"></iframe>
</body>
</html>
