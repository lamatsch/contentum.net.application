﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="TestPage.aspx.cs" Inherits="TestPage"
    Title="Teszt Oldal" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox"
    TagPrefix="uc14" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc1" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/StandardObjektumTargyszavak.ascx" TagName="StandardObjektumTargyszavak"
    TagPrefix="sot" %>
 <%@ Register src="Component/EvIntervallum_SearchFormControl.ascx" tagname="EvIntervallum_SearchFormControl" tagprefix="uc11" %>
<%@ Register src="Component/RequiredNumberBox.ascx" tagname="RequiredNumberBox" tagprefix="uc11" %>
 <%@ Register src="eRecordComponent/UgyiratSzerelesiLista.ascx" tagname="UgyiratSzerelesiLista" tagprefix="uc17" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" FullManualHeaderTitle="<%$Resources:Form,EgyszerusitettIktatasFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>
    <asp:UpdatePanel ID="IraIratUpdatePanel" runat="server" OnLoad="IraIratUpdatePanel_Load">
        <ContentTemplate>
            <asp:Panel ID="MainPanel" runat="server">
                <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
                <table cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="FoPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor_kicsi">
                                        <td colspan="6">
                                            <asp:RadioButtonList ID="ErkeztetesVagyIktatasRadioButtonList" runat="server" 
                                                AutoPostBack="True" RepeatDirection="Horizontal" Visible="False">
                                                <asp:ListItem Selected="True" Value="0">Érkeztetés és iktatás</asp:ListItem>
                                                <asp:ListItem Value="1">Csak érkeztetés</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <eUI:eFormPanel ID="KuldemenyPanel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelVonalkodPirosCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label10" runat="server" Text="Vonalkód:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <uc10:VonalKodTextBox ID="VonalkodTextBoxVonalkod" runat="server"/>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="cbMegorzesJelzo" runat="server" Checked="false" ToolTip="Borító típusának megőrzése"
                                                                    TabIndex="-1" />
                                                                <asp:Label ID="label11" runat="server" Text="Borító típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="KodtarakDropDownListBoritoTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label19" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label20" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                                                <uc11:IraIktatoKonyvekDropDownList ID="ErkeztetoKonyvekDropDrownList" runat="server" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label21" runat="server" Text="Küldő adószáma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="textAdoszam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label5" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label22" runat="server" Text="Küldő/feladó neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="100%" colspan="5">
                                                                <uc2:PartnerTextBox ID="Bekuldo_PartnerTextBox" runat="server"></uc2:PartnerTextBox>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label3" runat="server" Text="Küldő/feladó címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="100%" colspan="5">
                                                                <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" Validate="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label12" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label62" runat="server" Text="Beérkezés módja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <ktddl:KodtarakDropDownList ID="KuldesMod_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="cbBeerkezesIdopontjaMegorzes" runat="server" ToolTip="Beérkezés időpontjának megőrzése" TabIndex="-1" />
                                                                <asp:Label ID="Label14" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label82" runat="server" Text="Beérkezés időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="BeerkezesIdeje_CalendarControl" runat="server" TimeVisible="true">
                                                                </cc:CalendarControl>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label15" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label4" runat="server" Text="Bontó:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                                                <uc15:FelhasznaloCsoportTextBox ID="Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox"
                                                                    runat="server" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label16" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label6" runat="server" Text="Bontás időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Kuld_FelbontasDatuma_CalendarControl" runat="server" TimeVisible="true" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label102" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <asp:TextBox ID="HivatkozasiSzam_Kuldemeny_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label17" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label112" runat="server" Text="Küldemény típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="UgyintezesModja_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="CheckBox_CimzettMegorzes" runat="server" TabIndex="-1" 
                                                                    ToolTip="Címzett megőrzése" />
                                                                <asp:Label ID="Label13" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label122" runat="server" Text="Címzett:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <uc5:CsoportTextBox ID="CsoportId_CimzettCsoportTextBox1" runat="server"></uc5:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label18" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label132" runat="server" Text="Kézbesítés prioritása:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="Surgosseg_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="EmailKuldemenyPanel" runat="server">
                                                <asp:HiddenField ID="eMailBoritekok_Ver_HiddenField" runat="server" />
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label23" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label24" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc11:IraIktatoKonyvekDropDownList id="Email_IraIktatoKonyvekDropDownList1" runat="server">
                                                                </uc11:IraIktatoKonyvekDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label25" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label26" runat="server" Text="Iktatási kötelezettség:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <ktddl:KodtarakDropDownList ID="Email_IktatasiKotelezettsegKodtarakDropDownList"
                                                                    runat="server" Width="150"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label29" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label32" runat="server" Text="Küldő/feladó neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:PartnerTextBox ID="Email_Bekuldo_PartnerTextBox" runat="server"></uc2:PartnerTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label33" runat="server" Text="Küldő szervezete:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:PartnerTextBox ID="Email_BekuldoSzervezete_PartnerTextBox" runat="server" ReadOnly="true" ViewMode="true"></uc2:PartnerTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label35" runat="server" Text="Küldő e-mail címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc3:CimekTextBox ID="Email_KuldCim_CimekTextBox" runat="server" Validate="false"></uc3:CimekTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label115" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label37" runat="server" Text="E-mail tárgya:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="Email_Targy_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label40" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label123" runat="server" Text="Címzett:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:CsoportTextBox ID="Email_CsoportIdCimzett_CsoportTextBox" runat="server"></uc5:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label124" runat="server" Text="Címzett szervezete:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:CsoportTextBox ID="Email_CsoportId_CimzettSzervezete" runat="server" ReadOnly="true" ViewMode="true"></uc5:CsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label_cimzett" runat="server" Text="Címzett e-mail címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc3:CimekTextBox ID="Email_CimzettEmailcime" runat="server" Validate="false" ReadOnly="true" ViewMode="true"></uc3:CimekTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label42" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label43" runat="server" Text="Érkeztető:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc15:FelhasznaloCsoportTextBox ID="Email_ErkeztetoFelhasznaloCsoportTextBox1" runat="server" ReadOnly="true" ViewMode="true"></uc15:FelhasznaloCsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label46" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label47" runat="server" Text="Beérkezés időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Email_BeerkezesIdeje_CalendarControl" runat="server" TimeVisible="true" ReadOnly="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label48" runat="server" Text="Beérkezés módja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="Email_BeerkezesMod_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label52" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label53" runat="server" Text="Feladás időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Email_FeladasIdeje_CalendarControl" runat="server" TimeVisible="true" ReadOnly="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label54" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="Email_HivatkozasiSzam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </eUI:eFormPanel>
                                            
                                            <%-- Rejtett textbox tárolja a régi ügyirat irattári tételszámát és megkülönböztető jelzését --%>
                                            <asp:TextBox ID="IrattariTetelTextBox" style="display:none;" Text="" runat="server" />
                                            <asp:TextBox ID="MegkulJelzesTextBox" style="display:none;" Text="" runat="server" />                                            
                                            
                                            <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_AgazatiJel" runat="server" UseContextKey="true"
                                                ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetAgazatiJelek"
                                                Category="AgazatiJel" Enabled="False" TargetControlID="AgazatiJelek_DropDownList"
                                                EmptyText="---" EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]"
                                                PromptText="<Ágazati jel kiválasztása>" PromptValue="">
                                            </ajaxToolkit:CascadingDropDown>
                                            <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_Ugykor" runat="server" TargetControlID="Ugykor_DropDownList"
                                                ParentControlID="AgazatiJelek_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                                ServiceMethod="GetUgykorokByAgazatiJel" Category="Ugykor" Enabled="False" EmptyText="---"
                                                EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Irattári tételszám kiválasztása>"
                                                PromptValue="">
                                            </ajaxToolkit:CascadingDropDown>
                                            <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="UgyUgyirat_Ugytipus_DropDownList"
                                                ParentControlID="Ugykor_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                                ServiceMethod="GetUgytipusokByUgykor" Category="Ugytipus" Enabled="False" EmptyText="---"
                                                EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Ügytípus kiválasztása>"
                                                PromptValue="">
                                            </ajaxToolkit:CascadingDropDown>
                                            <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_IktatoKonyv" runat="server"
                                                TargetControlID="Iktatokonyvek_DropDownLis_Ajax" ParentControlID="Ugykor_DropDownList"
                                                UseContextKey="true" Category="Iktatokonyv" Enabled="false" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                                ServiceMethod="GetIktatokonyvekByIrattaritetel" EmptyText="---" EmptyValue=""
                                                LoadingText="[Feltöltés folyamatban...]" SelectedValue="" PromptText="<Iktatókönyv kiválasztása>"
                                                PromptValue="">
                                            </ajaxToolkit:CascadingDropDown>
                                            <uc17:UgyiratSzerelesiLista runat="server" ID="UgyiratSzerelesiLista1" />
                                            <eUI:eFormPanel ID="Ugyirat_Panel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr id="tr_lezartazUgyirat" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td colspan="2" style="text-align: center; color: Red; font-weight: bold;">
                                                            <asp:Label ID="Label2" runat="server" Text="Figyelem: Lezárt az előzmény ügyirat!"></asp:Label>
                                                        </td>
                                                        <td colspan="2" style="text-align: left;">
                                                            <asp:RadioButtonList ID="RadioButtonList_Lezartbaiktat_vagy_Ujranyit" runat="server"
                                                                RepeatDirection="Horizontal">
                                                                <asp:ListItem Selected="True" Value="0">Lez&#225;rt &#252;gyiratba iktat&#225;s</asp:ListItem>
                                                                <asp:ListItem Value="1">&#220;gyirat &#250;jranyit&#225;sa</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_lezartazIktatokonyv" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td colspan="2" style="text-align: center; color: Red; font-weight: bold;">
                                                            <asp:Label ID="Label7" runat="server" Text="Figyelem: Az előzmény lezárt iktatókönyvben van! Az előzmény az új ügyiratba lesz szerelve!"></asp:Label>
                                                        </td>
                                                        <td colspan="2" style="text-align: left;">
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trFoszam" visible="false" class="foszamRow">
                                                        <td class="foszamLabel" colspan="2">
                                                            <asp:Label ID="labelFoszam" runat="server" CssClass="DisableWrap" Text="Ügyirat főszáma:">
                                                            </asp:Label>
                                                        </td>
                                                        <td class="foszamText" colspan="2">
                                                            <asp:Label ID="UgyiratFoszam_Label" runat="server" CssClass="DisableWrap" Text="[Főszám]">
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trRegiAdatAzonosito" visible="false" class="foszamRow">
                                                        <td class="foszamLabel" colspan="2">
                                                            <asp:Label ID="labelRegiAdatAzonosito" runat="server" CssClass="DisableWrap" Text="Régi azonosító:">
                                                            </asp:Label>
                                                        </td>
                                                        <td class="foszamText" colspan="2">
                                                            <asp:Label ID="RegiAdatAzonosito_Label" runat="server" CssClass="DisableWrap" Text="[Régi azonosító]">
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trElozmenySearch" style="background-color: #F2E3FB;">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                                        </td>
                                                        <td colspan="3" class="mrUrlapMezo" style="text-align: left;">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left">
                                                                <tr>
                                                                    <td class="mrUrlapMezo" style="width: 90px;">
                                                                        <uc11:EvIntervallum_SearchFormControl ID="evIktatokonyvSearch" IsIntervallumMode="false"
                                                                            runat="server" />
                                                                    </td>
                                                                    <td class="mrUrlapCaption_nowidth" style="width: 80px;">
                                                                        <asp:Label ID="labelIktatokonyvSearch" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="width: 110px">
                                                                        <uc11:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList_Search" runat="server"
                                                                            Mode="IktatokonyvekWithRegiAdatok" EvIntervallum_SearchFormControlId="evIktatokonyvSearch" />
                                                                    </td>
                                                                    <td class="mrUrlapCaption_nowidth" style="width: 80px">
                                                                        <asp:Label ID="labelFoszamSearch" runat="server" Text="Főszám:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="width: 120px">
                                                                        <uc11:RequiredNumberBox ID="numberFoszamSearch" Validate="false" runat="server" />
                                                                    </td>
                                                                    <td style="padding-top: 5px">
                                                                        <%--<asp:ImageButton ID="ImageButtonElozmenySearch" runat="server" CausesValidation="false"
                                                        CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif"                                            
                                                        ToolTip="Előzmény" Visible="True" CssClass="highlightit" OnClick="ImageButtonElozmenySearch_Click" />--%>
                                                                        <asp:ImageButton ID="ImageButton_Elozmeny" runat="server" CausesValidation="false"
                                                                            CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif" ToolTip="Előzmény"
                                                                            Visible="True" CssClass="highlightit" OnClick="ImageButton_Elozmeny_Click" />
                                                                    </td>
                                                                    <td style="padding-top: 5px">
                                                                        <asp:ImageButton ID="ImageButton_MigraltKereses" runat="server" CausesValidation="false"
                                                                            CommandName="" ImageUrl="~/images/hu/trapezgomb/migraltkereses_trap1.jpg" ToolTip="Régi adatok"
                                                                            Visible="True" CssClass="highlightit" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_agazatiJel" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label8" runat="server" Text="Ágazati jel:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label39" runat="server" Text="Irattári tételszám:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                                                            </asp:DropDownList>
                                                            <uc12:IraIrattariTetelTextBox ID="UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox"
                                                                runat="server" Validate="false" Visible="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*">&nbsp;</asp:Label><asp:Label
                                                                ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc11:IraIktatoKonyvekDropDownList ID="ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1"
                                                                runat="server" />
                                                            <asp:DropDownList ID="Iktatokonyvek_DropDownLis_Ajax" runat="server" CssClass="mrUrlapInputComboBox"
                                                                Visible="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_ugytipus" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label27" runat="server" Text="Ügytípus:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label55" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label30" runat="server" Text="Ügyirat tárgya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc8:RequiredTextBox ID="UgyUgyirat_Targy_RequiredTextBox" Width="95%" runat="server"
                                                                Rows="2" TextBoxMode="MultiLine" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short" colspan="2">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="padding-left: 2px;">
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                            CommandName="" AlternateText="Megtekintés" CssClass="highlightit" />
                                                                        <asp:HiddenField ID="ElozmenyUgyiratID_HiddenField" runat="server" />
                                                                        <asp:HiddenField ID="MigraltUgyiratID_HiddenField" runat="server" />
                                                                    </td>
                                                                    <td style="padding-left: 2px;">
                                                                        <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Modositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                            CommandName="" AlternateText="Módosítás" CssClass="highlightit" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImageButton_Ugyiratterkep" runat="server" CausesValidation="false"
                                                                            CssClass="highlightit" ImageUrl="~/images/hu/trapezgomb/ugyirat_fa_trap.jpg"
                                                                            ToolTip="Ügyirat-fa" Visible="True" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label28" runat="server" Text="Intézési határidő:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                             <cc:CalendarControl ID="UgyUgyirat_Hatarido_CalendarControl" runat="server" Visible="true"
                                                                Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label38" runat="server" Text="Skontró vége dátum:" Visible="False"></asp:Label>
                                                            <asp:Label ID="Label57" runat="server" Text="Irattárba helyezés dátuma:" Visible="False"></asp:Label></td>
						                                <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="UgyUgyirat_SkontroVege_CalendarControl" runat="server" 
                                                                Visible="false" Validate="false" ReadOnly="True" />
                                                            <cc:CalendarControl ID="UgyUgyirat_IrattarbaHelyezes_CalendarControl" 
                                                                runat="server" Visible="false" Validate="false" ReadOnly="True" /></td>
                                                    </tr>
                                                    <tr id="migraltUgyiratAzonositoMezo" class="urlapSor_kicsi" runat="server" style="display: none;">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="regiAzonositoLabel" runat="server" Text="Régi azonosító"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="regiAzonositoTextBox" runat="server" Validate="false" CssClass="ReadOnlyWebControl" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label9" runat="server" Text="Ügyfelelős:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc5:CsoportTextBox ID="Ugyfelelos_CsoportTextBox" runat="server" 
                                                                Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label34" runat="server" Text="Ügyintéző:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <uc15:FelhasznaloCsoportTextBox ID="UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox" 
                                                                runat="server" Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label51" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label31" runat="server" Text="Kezelő:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc5:CsoportTextBox ID="UgyUgyiratok_CsoportId_Felelos" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label36" runat="server" Text="Ügyfél, ügyindító:"></asp:Label></td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                           <uc2:PartnerTextBox ID="Ugy_PartnerId_Ugyindito_PartnerTextBox" Validate="false" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUgyinditoCime" runat="server" Text="Ügyindító címe:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <uc3:CimekTextBox ID="CimekTextBoxUgyindito" runat="server" Validate="false"></uc3:CimekTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </eUI:eFormPanel>
                                            <%-- Standard objektumfüggő tárgyszavak ügyirathoz --%>
                                            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                                                <sot:StandardObjektumTargyszavak ID="standardObjektumTargyszavak" runat="server" />
                                            </eUI:eFormPanel>
                                           
                                            <eUI:eFormPanel ID="IratPanel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label41" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label50" runat="server" Text="Irat tárgya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <uc8:RequiredTextBox ID="IraIrat_Targy_RequiredTextBox" runat="server" Rows="2" TextBoxMode="MultiLine"
                                                                Width="95%" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short" width="0%">
                                                            <%--<asp:Label ID="Label38" runat="server" Text="Irat kategória:"></asp:Label>--%>
                                                            <asp:Label ID="Label56" runat="server" Text="Irat típus:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <%--<ktddl:KodtarakDropDownList ID="IraIrat_Kategoria_KodtarakDropDownList" runat="server" />--%>
                                                            <ktddl:KodtarakDropDownList ID="IraIrat_Irattipus_KodtarakDropDownList" runat="server" />
                                                        </td>
                                                    </tr>   
                                                    <tr id="tr_Kiadmanyozas" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:CheckBox ID="IraIrat_KiadmanyozniKell_CheckBox" runat="server" Text="Kiadmányozni kell" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Kiadmanyozo" runat="server" Text="Kiadmányozó:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <uc15:FelhasznaloCsoportTextBox ID="IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox"
                                                                runat="server" Validate="false" ReadOnly="true" ViewMode="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr1" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelIratMinosites" runat="server" Text="Kezelési utasítások"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="ktDropDownListIratMinosites" runat="server" Width="97%" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label133" runat="server" Text="Ügyintéző:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc15:FelhasznaloCsoportTextBox ID="Irat_Ugyintezo_FelhasznaloCsoportTextBox" runat="server"
                                                                Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_EgyebMuvelet" runat="server" class="urlapSor_kicsi" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_EgyebMuvelet" runat="server" Text="Egyéb művelet:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:RadioButtonList ID="RadioButtonList_BejovoIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;b&#243;l kivesz</asp:ListItem>
                                                                <asp:ListItem Value="2">Lez&#225;r</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:RadioButtonList ID="RadioButtonList_BelsoIratIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;ba helyez</asp:ListItem>
                                                                <asp:ListItem Value="2">Elint&#233;z</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_kezelesifeljegyzes" runat="server" class="urlapSor_kicsi">
                                                        <td colspan="5">
                                                        <div style="margin:0 -3px -8px -3px">
                                                            <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" />
                                                        </div>
                                                        </td>
                                                    </tr>
                                                   
                                                </table>
                                            </eUI:eFormPanel>
                                            
                                            <eUI:eFormPanel ID="ErkeztetesResultPanel" runat="server" Visible="false" CssClass="mrResultPanelErkeztetes">
                                                <div class="mrResultPanelText">
                                                    Az érkeztetés sikeresen végrehajtódott.</div>
                                                <table cellspacing="0" cellpadding="0" width="700px" class="mrResultTableErkeztetes">
                                                    <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                                        <td class="mrUrlapCaptionBigSize">
                                                            <asp:Label ID="labelKuldemeny" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezoBigSize">
                                                            <asp:Label ID="labelKuldemenyErkSzam" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapCaption" style="padding-top: 5px">
                                                            <asp:ImageButton ID="imgKuldemenyMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        </td>
                                                        <td class="mrUrlapMezo" style="padding-top: 5px">
                                                            <span style="padding-right: 80px">
                                                                <asp:ImageButton ID="imgKuldemenyModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </eUI:eFormPanel>
                                            
                                            <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" CssClass="mrResultPanel">
                                                <div class="mrResultPanelText">
                                                    Az iktatás sikeresen végrehajtódott.</div>
                                                <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
                                                    <tr class="urlapSorBigSize">
                                                        <td class="mrUrlapCaptionBigSize">
                                                            <asp:Label ID="labelUgyirat" runat="server" Text="Ügyirat iktatószáma:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezoBigSize">
                                                            <asp:Label ID="labelUgyiratFoszam" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapCaption" style="padding-top: 5px">
                                                            <asp:ImageButton ID="imgUgyiratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        </td>
                                                        <td class="mrUrlapMezo" style="padding-top: 5px">
                                                            <span style="padding-right: 80px">
                                                                <asp:ImageButton ID="imgUgyiratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                                <asp:ImageButton ID="imgEloadoiIv" runat="server" ImageUrl="~/images/hu/trapezgomb/eloadoi_iv_trap.jpg" 
                                                                    onmouseover="swapByName(this.id,'eloadoi_iv_trap2.jpg');" onmouseout="swapByName(this.id,'eloadoi_iv_trap.jpg');"/>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                                        <td class="mrUrlapCaptionBigSize">
                                                            <asp:Label ID="labelIrat" runat="server" Text="Irat iktatószáma:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezoBigSize">
                                                            <asp:Label ID="labelIratFoszam" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapCaption" style="padding-top: 5px">
                                                            <asp:ImageButton ID="imgIratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        </td>
                                                        <td class="mrUrlapMezo" style="padding-top: 5px;">
                                                            <span style="padding-right: 80px">
                                                                <asp:ImageButton ID="imgIratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </eUI:eFormPanel>

                                        </td>
                                    </tr>
                                    <tr class="urlapSor_kicsi">
                                        <td class="mrUrlapCaption_short" valign="top">
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:TabFooter ID="TabFooter1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

