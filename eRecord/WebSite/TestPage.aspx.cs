﻿/* $Header: TestPage.aspx.cs, 10, 2009.05.19. 14:26:09, Lamatsch Andr?s$ */
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class TestPage : Contentum.eUtility.Test.BaseTestPage
{
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    //private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";
    //private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";

    private const string kcs_ErkeztetoKonyv = "ERKEZTETOKONYV";
    private const string kcs_KuldesModja = "KULDEMENY_KULDES_MODJA";
    private const string kcs_Surgosseg = "SURGOSSEG";
    private const string kcs_BoritoTipus = "KULDEMENY_BORITO_TIPUS";
    private const string kcs_IKTATASI_KOTELEZETTSEG = "IKTATASI_KOTELEZETTSEG";

    private const string funkcio_EgyszerusitettIktatas = "EgyszerusitettIktatas";
    private const string funkcio_BejovoIratIktatas = "BejovoIratIktatas";
    private const string funkcio_Erkeztetes = "KuldemenyNew";
    private const string funkcio_BejovoIratIktatasCsakAlszamra = "BejovoIratIktatasCsakAlszamra";

    private const string session_CimzettChecked = "CimzettChecked";
    private const string session_CimzettId = "CimzettId";

    private PageView pageView = null;
    private string Command;
    //private const string migralasUrl = "/eMigration/FoszamLOVList.aspx";
    private Boolean _Load = false;

    protected bool IsRequireOverwriteTemplateDatas()
    {
        if (!IsPostBack && Command == CommandName.New && _Load)
        {
            return true;
        }

        return false;
    }

    private string EmailBoritekokId = "";

    private ErkeztetesTipus Mode = ErkeztetesTipus.KuldemenyErkeztetes;

    private enum ErkeztetesTipus
    {
        KuldemenyErkeztetes,
        EmailErkeztetes
    }

    private IktatasiParameterek iktatasiParameterek = null;

    protected IktatasiParameterek IktatasiParameterek
    {
        get
        {
            if (iktatasiParameterek == null)
                iktatasiParameterek = new IktatasiParameterek();
            return iktatasiParameterek;
        }
        set { iktatasiParameterek = value; }
    }

    #region page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Funkciójog ellenőrzés: legalább érkeztetés jognak kell lennie:
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_Erkeztetes);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = CommandName.New;

        EmailBoritekokId = Request.QueryString.Get(QueryStringVars.EmailBoritekokId);

        if (!String.IsNullOrEmpty(EmailBoritekokId))
        {
            Mode = ErkeztetesTipus.EmailErkeztetes;
        }
        else
        {
            Mode = ErkeztetesTipus.KuldemenyErkeztetes;
        }

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        ImageButton_Elozmeny.OnClientClick = GetElozmenyKeresesOnClientClick();

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
           "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_MigraltKereses.OnClientClick = GetElozmenyKeresesOnClientClick();

        #region Cascading DropDown
        // Contextkey-ben megadjuk a felhasználó id-t
        CascadingDropDown_AgazatiJel.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugykor.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugytipus.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);

        #region CR956 fix, ügyirat tárgy ne másolódjon át irathoz
        //        UgyUgyirat_Targy_RequiredTextBox.TextBox.Attributes["onblur"] +=
        //               @" var iratTargyObj = $get('" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID + @"');
        //                if (iratTargyObj.value == '') 
        //                {                  
        //                    iratTargyObj.value = this.value;
        //                }  ";
        #endregion


        CascadingDropDown_AgazatiJel.Enabled = true;
        CascadingDropDown_Ugykor.Enabled = true;
        CascadingDropDown_Ugytipus.Enabled = true;

        CascadingDropDown_IktatoKonyv.Enabled = true;

        Iktatokonyvek_DropDownLis_Ajax.Visible = true;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;

        #endregion

        //FormHeader1.TemplateObjectType = typeof(EREC_UgyUgyiratok);
        //FormHeader1.TemplateObjectType = typeof(IktatasFormTemplateObject);
        //FormHeader1.FormTemplateLoader1_Visibility = true;


        //Partner és cím összekötése
        Bekuldo_PartnerTextBox.CimTextBox = Kuld_CimId_CimekTextBox.TextBox;
        Bekuldo_PartnerTextBox.CimHiddenField = Kuld_CimId_CimekTextBox.HiddenField;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimTextBox = CimekTextBoxUgyindito.TextBox;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimHiddenField = CimekTextBoxUgyindito.HiddenField;

        Bekuldo_PartnerTextBox.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(80);
        Kuld_CimId_CimekTextBox.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(80);

        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            Email_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.AutoPostBack = true;
            Email_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(EmailIktatasiKotelezettseg_SelectedIndexChanged);

            ErkeztetesVagyIktatasRadioButtonList.Visible = true;
            ErkeztetesVagyIktatasRadioButtonList.SelectedIndexChanged += new EventHandler(ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged);
        }

        //DropDownList ListSearchExtender konfigurálása
        AgazatiJelek_DropDownList.LSEX_RaiseImmediateOnChange = true;
        Ugykor_DropDownList.LSEX_RaiseImmediateOnChange = true;

        RegisterJavascripts();

        FeljegyzesPanel.ErrorPanel = EErrorPanel1;
        FeljegyzesPanel.ErrorUpdatePanel = ErrorUpdatePanel1;
    }

    private void RegisterJavascripts()
    {

        if (Command == CommandName.New)
        {
            #region GombCsere

            string jsOnEvKeyUp = @"function OnEvKeyUp(ev)
            {
               var k= null;
               if(ev) var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
               if (k == null || k != Sys.UI.Key.enter)
               {
                   var textBoxEv = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + @"');
                   if(textBoxEv)
                    {
                       var evValue = textBoxEv.value;
                       var elozmenyVisible = true;
                       if(evValue != '')
                       {
                          evValue = parseInt(evValue);
                          if(!isNaN(evValue) && evValue < " + MigralasEve + @")
                          {
                             elozmenyVisible = false;
                          }
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               if(elozmenyVisible)
                               {
                                   imageElozmeny.style.display = '';
                                   imageMigralt.style.display = 'none';
                               }
                               else
                               {
                                   imageElozmeny.style.display = 'none';
                                   imageMigralt.style.display = '';
                               }
                           }  
                       }
                       else
                       {
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               imageElozmeny.style.display = '';
                               imageMigralt.style.display = '';
                           } 
                       }
                   }
               }     
             };OnEvKeyUp(null);";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnEvKeyUp", jsOnEvKeyUp, true);

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeyup", "OnEvKeyUp(event)");

            #endregion

            #region Enter figyelese

            string jsOnElozmenyKeyDown = @"function OnElozmenyKeyDown(ev)
            {
                var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
                if (k === Sys.UI.Key.enter)
                {
                    " + ImageButton_Elozmeny.OnClientClick + @"
                }
            return false;
            }";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnElozmenyKeyDown", jsOnElozmenyKeyDown, true);

            numberFoszamSearch.TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            IktatoKonyvekDropDownList_Search.DropDownList.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            #endregion
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.ButtonsClick +=
            new CommandEventHandler(FormHeader1_ButtonsClick);

        // Cím:
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.EmailErkeztetesIktatasFormHeaderTitle;
        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

        if (!IsPostBack)
        {
            ReLoadTab();
        }

    }

    protected void IraIratUpdatePanel_Load(object sender, EventArgs e)
    {
        // csak iktatáskor van értelme ezt figyelni (nehogy feleslegesen lefusson a tabfülek kattintgatásakor)

        if (Command == CommandName.New)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshUgyiratokPanel:
                        if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyUgyirat();
                        }
                        else if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyRegiUgyirat();
                        }
                        break;
                }
            }
        }

    }

    #region Elozmeny Ugyirat
    /// <summary>
    /// Ügyirat adatainak feltöltése adatbázisból
    /// </summary>
    private void SetElozmenyUgyirat()
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        if (!String.IsNullOrEmpty(UgyUgyirat_Id))
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = UgyUgyirat_Id;
            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                SetElozmenyUgyirat(erec_UgyUgyiratok);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }

        }
    }

    private void SetElozmenyUgyirat(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = UgyUgyirat_Id;
        if (erec_UgyUgyiratok != null)
        {
            /// Ellenőrzés, hogy lehet-e alszámra iktatni az ügyiratba
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
            ErrorDetails errorDetail = null;
            if (Ugyiratok.LehetAlszamraIktatni(execParam, ugyiratStatusz, out errorDetail) == false)
            {
                // nem lehet alszámra iktatni, hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(52109, errorDetail));
                ErrorUpdatePanel1.Update();

                // hiddenfield értékének törlése
                ElozmenyUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();
                return;
            }


            //// Egyéb műveletek sor megjelenítése alszámra iktatásnál:
            //tr_EgyebMuvelet.Visible = true;

            EREC_IraIktatoKonyvek erec_IraIktatokonyvek = LoadUgyiratComponents(erec_UgyUgyiratok);

            // Mégsem kell (CR#1052)
            //// Ha üres az Iratok tárgy, segítségül beírjuk oda az Ügyiratok tárgyat
            //if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
            //{
            //    IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
            //}


            // Iktatóköny vizsgálata, lezárt-e esetleg:
            bool iktatoKonyvLezart = false;
            if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatokonyvek) == true)
            {
                // Lezárt az iktatókönyv:
                iktatoKonyvLezart = true;
            }

            // Ügyirat lezárt-e?
            bool ugyiratLezart = false;
            if (Ugyiratok.Lezart(ugyiratStatusz) == true)
            {
                ugyiratLezart = true;
            }



            // ReadOnly tulajdonságok beállítása:                    
            //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.ReadOnly = true;
            //UgyUgyirat_UgyiratTextBox.ReadOnly = true;                    
            //UgyUgyirat_Ugytipus_KodtarakDropDownList.ReadOnly = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
            UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;
            UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;
            UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;

            if (erec_UgyUgyiratok.Allapot == "07")
            {
                Label36.Visible = true;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
                Label24.Visible = false;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            }
            if (erec_UgyUgyiratok.Allapot == "10")
            {
                Label24.Visible = true;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
                Label36.Visible = false;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            }

            // Felelős, Kezelő sor látszódjon:
            UgyUgyiratok_CsoportId_Felelos.Visible = true;
            //Label_KezeloSzignJegyzekAlapjan.Visible = false;
            //tr_felelos_kezelo.Visible = true;

            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
            Ugyfelelos_CsoportTextBox.ReadOnly = true;

            // ügyindító, ügyfél
            Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
            CimekTextBoxUgyindito.ReadOnly = true;
            #region Cascading cuccok letiltása:
            CascadingDropDown_AgazatiJel.Enabled = false;
            CascadingDropDown_Ugykor.Enabled = false;
            CascadingDropDown_Ugytipus.Enabled = false;
            CascadingDropDown_IktatoKonyv.Enabled = false;

            tr_agazatiJel.Visible = false;
            AgazatiJelek_DropDownList.Visible = false;
            Ugykor_DropDownList.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = true;
            tr_ugytipus.Visible = false;
            UgyUgyirat_Ugytipus_DropDownList.Visible = false;

            Iktatokonyvek_DropDownLis_Ajax.Visible = false;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = true;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = true;

            #endregion

            #region Módosítás gomb állítása

            if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = "alert('" + Resources.Error.UIUgyiratNemModosithato
                   + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }
            else
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + UgyUgyirat_Id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, ImageButton_ElozmenyUgyirat_Modositas.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }

            #endregion

            // iktatókönyv dropdown readonly, kivéve ha lezárt az előzmény iktatókönyve
            if (iktatoKonyvLezart == true)
            {
                // iktatokonyvek újratöltése:
                // TODO: itt majd a Cascading cuccot kéne újra engedélyezni
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Iktato, EErrorPanel1);
            }
            else
            {
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = true;
            }

            // Figyelmeztető üzenetek megjelenítése:
            if (iktatoKonyvLezart == true)
            {
                tr_lezartazIktatokonyv.Visible = true;
                tr_lezartazUgyirat.Visible = false;
            }
            else if (ugyiratLezart == true)
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = true;
            }
            else
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = false;
            }

            #region standard objektumfüggő tárgyszavak ügyirathoz

            standardObjektumTargyszavak.FillStandardTargyszavak(UgyUgyirat_Id, null, Constants.TableNames.EREC_UgyUgyiratok, EErrorPanel1);
            standardObjektumTargyszavak.ReadOnly = true;

            if (standardObjektumTargyszavak.Count > 0)
            {
                StandardTargyszavakPanel.Visible = true;
            }
            else
            {
                StandardTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumfüggő tárgyszavak ügyirathoz

            SetElozmenyUgyiratUI();

        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "Rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
        }
    }

    private void SetElozmenyUgyiratUI()
    {
        // előzményezés után már nem lehet régi adatot keresni
        //ImageButton_MigraltKereses.Enabled = false;
        //UI.SetImageButtonStyleToDisabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

    }

    private void ClearElozmenyUgyirat()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;
        MigraltUgyiratID_HiddenField.Value = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = false;
        UgyUgyirat_Targy_RequiredTextBox.ReadOnly = false;
        UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
        UgyUgyiratok_CsoportId_Felelos.ReadOnly = false;

        Label36.Visible = false;
        UgyUgyirat_SkontroVege_CalendarControl.Visible = false;

        Label24.Visible = false;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;

        // Felelős, Kezelő sor látszódjon:
        UgyUgyiratok_CsoportId_Felelos.Visible = true;

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = false;
        Ugyfelelos_CsoportTextBox.ReadOnly = false;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = false;
        CimekTextBoxUgyindito.ReadOnly = false;

        #region Cascading cuccok letiltása:
        CascadingDropDown_AgazatiJel.Enabled = true;
        CascadingDropDown_Ugykor.Enabled = true;
        CascadingDropDown_Ugytipus.Enabled = true;
        CascadingDropDown_IktatoKonyv.Enabled = true;

        tr_agazatiJel.Visible = true;
        AgazatiJelek_DropDownList.Visible = true;
        Ugykor_DropDownList.Visible = true;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
        tr_ugytipus.Visible = true;
        UgyUgyirat_Ugytipus_DropDownList.Visible = true;

        Iktatokonyvek_DropDownLis_Ajax.Visible = true;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        #endregion

        // Figyelmeztető üzenetek megjelenítése:
        tr_lezartazIktatokonyv.Visible = false;
        tr_lezartazUgyirat.Visible = false;

        #region standard objektumfüggő tárgyszavak ügyirathoz

        standardObjektumTargyszavak.ClearTextBoxValues();
        standardObjektumTargyszavak.ReadOnly = false;
        StandardTargyszavakPanel.Visible = true;

        #endregion standard objektumfüggő tárgyszavak ügyirathoz

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = true;
        // előzményezés után már nem lehet régi adatot keresni
        ImageButton_MigraltKereses.Enabled = true;
        UI.SetImageButtonStyleToEnabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;
        RegiAdatAzonosito_Label.Text = String.Empty;
        IrattariTetelTextBox.Text = String.Empty;
        MegkulJelzesTextBox.Text = String.Empty;
        UgyiratSzerelesiLista1.Reset();

        ClearUgyiratComponents();
    }

    private void SetElozmenyRegiUgyirat()
    {
        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                                    eMigration.migralasFormUrl, QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, MigraltUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(eMigration.migralasFormUrl, "", MigraltUgyiratID_HiddenField.ClientID,
                          Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = false;

        SetRegiadatAzonositoRow();

        SetIrattariTetelIktatokonyvRegiUgyirat();
    }

    private void SetRegiadatAzonositoRow()
    {
        trRegiAdatAzonosito.Visible = true;
        RegiAdatAzonosito_Label.Text = regiAzonositoTextBox.Text;
    }

    private void SetIrattariTetelIktatokonyvRegiUgyirat()
    {
        // előlkészítés: töröljük a mezőértékeket
        CascadingDropDown_AgazatiJel.SelectedValue = "";
        CascadingDropDown_IktatoKonyv.SelectedValue = "";


        if (!String.IsNullOrEmpty(IrattariTetelTextBox.Text))
        {
            #region irattári tétel
            string irattaritetelszam = IrattariTetelTextBox.Text;

            // Ágazati jel és azonosító meghatározása az irattári tételből
            EREC_IraIrattariTetelekService service_itsz = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search_itsz = new EREC_IraIrattariTetelekSearch();
            ExecParam execParam_itsz = UI.SetExecParamDefault(Page, new ExecParam());
            search_itsz.IrattariTetelszam.Value = irattaritetelszam;
            search_itsz.IrattariTetelszam.Operator = Query.Operators.equals;
            Result result_itsz = service_itsz.GetAll(execParam_itsz, search_itsz);

            if (String.IsNullOrEmpty(result_itsz.ErrorCode))
            {
                if (result_itsz.Ds.Tables[0].Rows.Count == 1)
                {
                    string Id_itsz = result_itsz.Ds.Tables[0].Rows[0]["Id"].ToString();
                    string AgazatiJel_Id = result_itsz.Ds.Tables[0].Rows[0]["AgazatiJel_Id"].ToString();

                    CascadingDropDown_AgazatiJel.SelectedValue = AgazatiJel_Id;
                    CascadingDropDown_Ugykor.SelectedValue = Id_itsz;

                    #region iktatókönyv
                    if (!String.IsNullOrEmpty(MegkulJelzesTextBox.Text))
                    {
                        string megkuljelzes = MegkulJelzesTextBox.Text;

                        // Iktatókönyv
                        EREC_IraIktatoKonyvekService service_iktato = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                        EREC_IraIktatoKonyvekSearch search_iktato = new EREC_IraIktatoKonyvekSearch();
                        ExecParam execParam_iktato = UI.SetExecParamDefault(Page, new ExecParam());
                        search_iktato.Iktatohely.Value = megkuljelzes;
                        search_iktato.Iktatohely.Operator = Query.Operators.likestart;
                        search_iktato.Ev.Value = System.DateTime.Now.Year.ToString();
                        search_iktato.Ev.Operator = Query.Operators.equals;
                        Result result_iktato = service_iktato.GetAll(execParam_iktato, search_iktato);

                        if (String.IsNullOrEmpty(result_iktato.ErrorCode))
                        {
                            if (result_iktato.Ds.Tables[0].Rows.Count == 1)
                            {
                                string Id_iktato = result_iktato.Ds.Tables[0].Rows[0]["Id"].ToString();

                                CascadingDropDown_IktatoKonyv.SelectedValue = Id_iktato;
                            }
                        }

                        // végül kiürítjük a textboxot
                        MegkulJelzesTextBox.Text = "";
                    }

                    #endregion iktatókönyv
                }
            }

            // végül kiürítjük a textboxot
            IrattariTetelTextBox.Text = "";
            #endregion irattári tétel

        }

    }

    #endregion

    public void ReLoadTab()
    {
        if (Command == CommandName.New)
        {
            #region (Iktatás)

            // Components Init
            InitElozmenySearchComponent();
            DateTime _30DaysAfterToday = DateTime.Now.AddDays(30);
            UgyUgyirat_Hatarido_CalendarControl.Text = _30DaysAfterToday.ToShortDateString();
            // Kezelő kitöltve alapból a felhasználóra:
            UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
            UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);


            if (!IsPostBack)
            {
                #region Template betöltés, ha kell

                if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
                {
                    KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(kcs_BoritoTipus, KodTarak.BoritoTipus.Irat, EErrorPanel1);
                }

                string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);
                if (!String.IsNullOrEmpty(templateId))
                {
                    _Load = true;
                    FormHeader1.LoadTemplateObjectById(templateId);
                    LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader1.TemplateObject);
                    _Load = false;

                }

                #endregion


                bool vanUgyirat = false;
                //bool vanUgyiratDarab = false;

                // Van-e megadva ügyirat alszámra iktatáshoz?
                String ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    ElozmenyUgyiratID_HiddenField.Value = ugyiratId;
                    SetElozmenyUgyirat();
                    vanUgyirat = true;
                }



                // komponensek inicializálása:

                if (vanUgyirat == false)
                {
                    //UgyUgyirat_Ugytipus_KodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, true, EErrorPanel1);
                    //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.FillDropDownList(kcs_IKTATOSZAM_KIEG, true, EErrorPanel1);

                }
                else
                {
                    ImageButton_Elozmeny.Enabled = false;
                    UI.SetImageButtonStyleToDisabled(ImageButton_Elozmeny);
                }

                #region standard objektumfüggő tárgyszavak ügyirathoz
                if (vanUgyirat == true)
                {
                    standardObjektumTargyszavak.FillStandardTargyszavak(ugyiratId, null, Constants.TableNames.EREC_UgyUgyiratok, EErrorPanel1);
                    standardObjektumTargyszavak.ReadOnly = true;

                    if (standardObjektumTargyszavak.Count > 0)
                    {
                        StandardTargyszavakPanel.Visible = true;
                    }
                    else
                    {
                        StandardTargyszavakPanel.Visible = false;
                    }
                }
                else
                {
                    standardObjektumTargyszavak.FillStandardTargyszavak(null, null, Constants.TableNames.EREC_UgyUgyiratok, EErrorPanel1);
                    standardObjektumTargyszavak.ReadOnly = false;

                    if (standardObjektumTargyszavak.Count > 0)
                    {
                        StandardTargyszavakPanel.Visible = true;
                        standardObjektumTargyszavak.SetDefaultValues();
                        TabFooter1.ImageButton_Save.OnClientClick = standardObjektumTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                    }
                    else
                    {
                        StandardTargyszavakPanel.Visible = false;
                    }
                }
                #endregion standard objektumfüggő tárgyszavak ügyirathoz

                // ha feltöltöttük az ügyirat részt, ezeket már nem kell:
                if (vanUgyirat == false)
                {
                    // Cascading dropdown miatt a másikat használjuk:
                    //UgyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, Constants.IktatoErkezteto.Iktato
                    //    , false, EErrorPanel1);
                    //IktatoKonyvek.FillDropDownList(Iktatokonyvek_DropDownLis_Ajax,true, Constants.IktatoErkezteto.Iktato, Page, EErrorPanel1);

                    //UgyUgyiratDarab_EljarasiSzakasz_KodtarakDropDownList.FillDropDownList(kcs_ELJARASI_SZAKASZ, true, EErrorPanel1);
                }

                //IraIrat_Kategoria_KodtarakDropDownList.FillDropDownList(kcs_IRATKATEGORIA, true, EErrorPanel1);

                IraIrat_Irattipus_KodtarakDropDownList.FillDropDownList(kcs_IRATTIPUS, true, EErrorPanel1);

                //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, EErrorPanel1);

                ktDropDownListIratMinosites.FillDropDownList(kcs_IRATMINOSITES, true, EErrorPanel1);

            }

            #endregion


            if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
            {
                #region küldemény

                ErkeztetoKonyvekDropDrownList.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, EErrorPanel1);
                KuldesMod_DropDownList.FillDropDownList(kcs_KuldesModja, EErrorPanel1);

                UgyintezesModja_DropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);

                Surgosseg_DropDownList.FillDropDownList(kcs_Surgosseg, EErrorPanel1);
                Surgosseg_DropDownList.SelectedValue = KodTarak.SURGOSSEG.Normal;

                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyFizikaiBontas"))
                {
                    Kuld_FelbontasDatuma_CalendarControl.SetToday();
                    Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                    Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                }

                BeerkezesIdeje_CalendarControl.SetToday();

                LoadSessionDatas();

                #endregion
            }
            else if (Mode == ErkeztetesTipus.EmailErkeztetes)
            {
                #region Email

                Email_IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, true, true, EErrorPanel1);

                Email_IktatasiKotelezettsegKodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATASI_KOTELEZETTSEG, KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando
                    , false, EErrorPanel1);

                // Emailboríték lekérdezése:
                EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
                ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
                execparam_emailGet.Record_Id = EmailBoritekokId;

                Result result_emailGet = erec_eMailBoritekokService.Get(execparam_emailGet);
                if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                    ErrorUpdatePanel1.Update();
                    return;
                }

                EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

                #region Ellenőrzés, lehet-e érkeztetni:

                if (!String.IsNullOrEmpty(erec_eMailBoritekok.KuldKuldemeny_Id))
                {
                    // Már érkeztetve van:
                    ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_52651);
                    ErrorUpdatePanel1.Update();
                    MainPanel.Visible = false;

                    return;
                }

                #endregion

                // verzió lementése:
                eMailBoritekok_Ver_HiddenField.Value = erec_eMailBoritekok.Base.Ver;

                // cimzett és feladóból a partneradatok lekérése:
                Contentum.eAdmin.Service.KRT_PartnerekService service_partnerek =
                    Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                ExecParam execParam_partnerAdatok = UI.SetExecParamDefault(Page, new ExecParam());

                Result result_partnerAdatok = service_partnerek.GetEmailPartnerAdatok(execParam_partnerAdatok,
                    erec_eMailBoritekok.Felado, erec_eMailBoritekok.Cimzett);
                if (!String.IsNullOrEmpty(result_partnerAdatok.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partnerAdatok);
                    ErrorUpdatePanel1.Update();
                    return;
                }



                // Beküldő megállapítása az emailborítékból
                DataTable table_felado = result_partnerAdatok.Ds.Tables["Felado"];
                if (table_felado != null && table_felado.Rows.Count > 0)
                {
                    DataRow row = table_felado.Rows[0];

                    Email_Bekuldo_PartnerTextBox.Id_HiddenField = row["Id"].ToString();
                    Email_Bekuldo_PartnerTextBox.Text = row["Nev"].ToString();

                    // Beküldő címe:
                    Email_KuldCim_CimekTextBox.ID = row["CimId"].ToString();
                    Email_KuldCim_CimekTextBox.Text = row["CimNev"].ToString();
                }

                // Beküldő szervezetének megállapítása:
                DataTable table_feladoSzervezet = result_partnerAdatok.Ds.Tables["FeladoSzervezet"];
                if (table_feladoSzervezet != null && table_feladoSzervezet.Rows.Count > 0)
                {
                    DataRow row = table_feladoSzervezet.Rows[0];

                    Email_BekuldoSzervezete_PartnerTextBox.Id_HiddenField = row["Partner_id_kapcsolt"].ToString();
                    Email_BekuldoSzervezete_PartnerTextBox.Text = row["Nev"].ToString();
                }

                // Címzett megállapítása:
                DataTable table_cimzett = result_partnerAdatok.Ds.Tables["Cimzett"];
                if (table_cimzett != null && table_cimzett.Rows.Count > 0)
                {
                    DataRow row = table_cimzett.Rows[0];

                    Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = row["Id"].ToString();
                    Email_CsoportIdCimzett_CsoportTextBox.Text = row["Nev"].ToString();
                }

                // Címzett szervezetének megállapítása
                DataTable table_cimzettSzervezet = result_partnerAdatok.Ds.Tables["CimzettSzervezet"];
                if (table_cimzettSzervezet != null && table_cimzettSzervezet.Rows.Count > 0)
                {
                    DataRow row = table_cimzettSzervezet.Rows[0];

                    Email_CsoportId_CimzettSzervezete.Id_HiddenField = row["Id"].ToString();
                    Email_CsoportId_CimzettSzervezete.Text = row["Nev"].ToString();
                }

                // ha még nem töltöttük ki:
                if (string.IsNullOrEmpty(Email_KuldCim_CimekTextBox.Text))
                {
                    Email_KuldCim_CimekTextBox.Text = erec_eMailBoritekok.Felado;
                }

                Email_Targy_TextBox.Text = erec_eMailBoritekok.Targy;

                // Tárgy bemásolása az irat tárgyához is:
                IraIrat_Targy_RequiredTextBox.Text = erec_eMailBoritekok.Targy;

                Email_CimzettEmailcime.Text = erec_eMailBoritekok.Cimzett;

                Email_ErkeztetoFelhasznaloCsoportTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                Email_ErkeztetoFelhasznaloCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

                Email_BeerkezesIdeje_CalendarControl.SetTodayAndTime();

                Email_BeerkezesMod_DropDownList.FillWithOneValue(kcs_KuldesModja, KodTarak.KULDEMENY_KULDES_MODJA.E_mail, EErrorPanel1);

                Email_FeladasIdeje_CalendarControl.Text = erec_eMailBoritekok.FeladasDatuma;

                #endregion
            }

        }

        // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
        TabFooter1.CommandArgument = Command;


        if (Command == CommandName.DesignView)
        {
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        // Komponensek láthatóságának beállítása:
        SetComponentsVisibility(Command);
    }

    private void InitElozmenySearchComponent()
    {
        //evIktatokonyvSearch.SetDefaultEvTol = true;
        //evIktatokonyvSearch.SetDefaultEvIg = true;
        //evIktatokonyvSearch.SetDefault();
        IktatoKonyvekDropDownList_Search.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , evIktatokonyvSearch.EvTol, evIktatokonyvSearch.EvIg
                , true, false, EErrorPanel1);
    }

    private void LoadSessionDatas()
    {
        if (UI.GetSession(Page, "BoritoTipusChecked") == "true")
        {
            KodtarakDropDownListBoritoTipus.SetSelectedValue(UI.GetSession(Page, "BoritoTipusValue"));
            cbMegorzesJelzo.Checked = true;
        }

        // Címzett megőrzése:
        if (UI.GetSession(Page, session_CimzettChecked) == "true")
        {
            CsoportId_CimzettCsoportTextBox1.Id_HiddenField = UI.GetSession(Page, session_CimzettId);
            CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
            CheckBox_CimzettMegorzes.Checked = true;
        }

        if (Command == CommandName.New)
        {
            //Beerkezes Ido
            if (UI.IsStoredValueInSession(Page, BeerkezesIdeje_CalendarControl.ID))
            {
                BeerkezesIdeje_CalendarControl.Text = UI.GetSessionStoredValue(Page, BeerkezesIdeje_CalendarControl.ID, cbBeerkezesIdopontjaMegorzes);
            }
        }
    }

    #endregion

    #region private methods

    /// <summary>
    /// Komponensek láthatóságának beállítása
    /// </summary>
    /// <param name="_command">New, Modify, View</param>
    /// <param name="_mode">BejovoIratIktatas, BelsoIratIktatas, stb...</param>
    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok állítása

        if (_command == CommandName.Modify || _command == CommandName.New)
        {
            // A tomeges felvitelt segito gombok megjelenitese:
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            // ez csak New-nál kell
            if (_command == CommandName.New)
            {
                TabFooter1.ImageButton_SaveAndNew.Visible = false;
                TabFooter1.ImageButton_SaveAndClose.Visible = false;
                TabFooter1.ImageButton_Cancel.Visible = true;
                TabFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            }
            else
            {
                // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
                TabFooter1.ImageButton_Cancel.Visible = false;
            }
        }

        #endregion

        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            KuldemenyPanel.Visible = false;
            EmailKuldemenyPanel.Visible = true;
            ImageButton_MigraltKereses.Visible = false;
        }
        else
        {
            KuldemenyPanel.Visible = true;
            EmailKuldemenyPanel.Visible = false;
        }
    }

    private void SetAllPanelVisibility(bool value)
    {
        KuldemenyPanel.Visible = value;
        EmailKuldemenyPanel.Visible = value;
        Ugyirat_Panel.Visible = value;
        StandardTargyszavakPanel.Visible = value;
        IratPanel.Visible = value;
        ResultPanel.Visible = value;
        ErkeztetesResultPanel.Visible = value;
    }


    /// <summary>
    /// Ha nem Iktatandó-t választ ki, csak az érkeztetőpanel látszódik:
    /// </summary>    
    private void EmailIktatasiKotelezettseg_SelectedIndexChanged(object sender, EventArgs e)
    {
        // csak email érkeztetésnél:
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            if (Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
            {
                // minden látszódik:
                EmailKuldemenyPanel.Visible = true;
                Ugyirat_Panel.Visible = true;
                StandardTargyszavakPanel.Visible = true;
                IratPanel.Visible = true;
            }
            else
            {
                // csak az email érkeztetés panel látszódik:
                SetAllPanelVisibility(false);
                EmailKuldemenyPanel.Visible = true;
            }
        }
    }

    /// <summary>
    /// Érkeztetés és iktatás közti váltás a felületen
    /// </summary>    
    private void ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        // csak email érkeztetésnél (egyelőre)
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            if (ErkeztetesVagyIktatasRadioButtonList.SelectedValue == "0")
            {
                // Érkeztetés és iktatás:
                EmailKuldemenyPanel.Visible = true;
                Ugyirat_Panel.Visible = true;
                StandardTargyszavakPanel.Visible = true;
                IratPanel.Visible = true;
            }
            else
            {
                // csak az email érkeztetés panel látszódik:
                SetAllPanelVisibility(false);
                EmailKuldemenyPanel.Visible = true;
            }
        }
    }


    // form --> business object
    private EREC_IraIratok GetBusinessObjectFromComponents()
    {
        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        erec_IraIratok.Targy = IraIrat_Targy_RequiredTextBox.Text;
        erec_IraIratok.Updated.Targy = pageView.GetUpdatedByView(IraIrat_Targy_RequiredTextBox);

        //erec_IraIratok.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;
        //erec_IraIratok.Updated.Kategoria = pageView.GetUpdatedByView(IraIrat_Kategoria_KodtarakDropDownList);

        erec_IraIratok.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.Irattipus = pageView.GetUpdatedByView(IraIrat_Irattipus_KodtarakDropDownList);

        erec_IraIratok.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";
        erec_IraIratok.Updated.KiadmanyozniKell = pageView.GetUpdatedByView(IraIrat_KiadmanyozniKell_CheckBox);

        erec_IraIratok.Minosites = ktDropDownListIratMinosites.SelectedValue;
        erec_IraIratok.Updated.Minosites = pageView.GetUpdatedByView(ktDropDownListIratMinosites);

        erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(Irat_Ugyintezo_FelhasznaloCsoportTextBox);

        //erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany = IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Id_HiddenField;
        //erec_IraIratok.Updated.FelhasznaloCsoport_Id_Kiadmany = pageView.GetUpdatedByView(IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox);        

        erec_IraIratok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IraIratok.Base.Updated.Ver = true;

        return erec_IraIratok;
    }

    // kezelési feljegyzések businessobject összeálllítása:
    //private EREC_IraKezFeljegyzesek GetBusinessObjectFromComponents_EREC_IraKezFeljegyzesek()
    //{
    //    EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = new EREC_IraKezFeljegyzesek();
    //    // összes mező update-elhetőségét kezdetben letiltani:
    //    erec_IraKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_IraKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_IraKezFeljegyzesek.KezelesTipus = IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue;
    //    erec_IraKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(IraKezFeljegyz_KezelesTipus_KodtarakDropDownList);

    //    erec_IraKezFeljegyzesek.Leiras = IraKezFeljegyz_Leiras_TextBox.Text;
    //    erec_IraKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(IraKezFeljegyz_Leiras_TextBox);

    //    return erec_IraKezFeljegyzesek;
    //}

    // Az ügyiratokra vonatkozó néhány komponensből egy EREC_UgyUgyiratok objektum felépítése
    private EREC_UgyUgyiratok GetBusinessObjectFromComponents_EREC_UgyUgyiratok()
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_UgyUgyiratok.Updated.SetValueAll(false);
        erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

        // csak New -nál van értelme, és ha nincs előzmény (nincs betöltve ügyirat)
        if (Command == CommandName.New
            && String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            //erec_UgyUgyiratok.IktatoszamKieg = UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.IktatoszamKieg = pageView.GetUpdatedByView(UgyUgyirat_IktatoszamKieg_KodtarakDropDownList);

            //erec_UgyUgyiratok.IraIrattariTetel_Id = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField;
            //erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox);

            erec_UgyUgyiratok.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(Ugykor_DropDownList);

            //erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_KodtarakDropDownList);

            erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_DropDownList);

            erec_UgyUgyiratok.Hatarido = UgyUgyirat_Hatarido_CalendarControl.Text;
            erec_UgyUgyiratok.Updated.Hatarido = pageView.GetUpdatedByView(UgyUgyirat_Hatarido_CalendarControl);

            erec_UgyUgyiratok.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;
            erec_UgyUgyiratok.Updated.Targy = pageView.GetUpdatedByView(UgyUgyirat_Targy_RequiredTextBox);

            // kezelő:
            erec_UgyUgyiratok.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(UgyUgyiratok_CsoportId_Felelos);

            //ügyintéző
            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox);

            // ügyfelelős:
            erec_UgyUgyiratok.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = pageView.GetUpdatedByView(Ugyfelelos_CsoportTextBox);

            // ügyindító/ügyfél
            erec_UgyUgyiratok.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            erec_UgyUgyiratok.Updated.NevSTR_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Cim_Id_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);
            erec_UgyUgyiratok.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;
            erec_UgyUgyiratok.Updated.CimSTR_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);

            // régi rendszer iktatószáma:
            if (!string.IsNullOrEmpty(regiAzonositoTextBox.Text))
            {
                erec_UgyUgyiratok.RegirendszerIktatoszam = regiAzonositoTextBox.Text;
                erec_UgyUgyiratok.Updated.RegirendszerIktatoszam = true;
                IktatasiParameterek.RegiAdatAzonosito = regiAzonositoTextBox.Text;
                IktatasiParameterek.RegiAdatId = MigraltUgyiratID_HiddenField.Value;
            }
        }

        return erec_UgyUgyiratok;
    }


    private EREC_KuldKuldemenyek GetBusinessObjectFromComponents_EREC_KuldKuldemenyek()
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        // webservice-en állítva:
        //erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
        //    FelhasznaloProfil.FelhasznaloId(Page));
        //erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;

        if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        {
            #region Küldemény

            erec_KuldKuldemenyek.IraIktatokonyv_Id = ErkeztetoKonyvekDropDrownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(ErkeztetoKonyvekDropDrownList);

            erec_KuldKuldemenyek.Partner_Id_Bekuldo = Bekuldo_PartnerTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.NevSTR_Bekuldo = Bekuldo_PartnerTextBox.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.Cim_Id = Kuld_CimId_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(Kuld_CimId_CimekTextBox);

            erec_KuldKuldemenyek.CimSTR_Bekuldo = Kuld_CimId_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(Kuld_CimId_CimekTextBox);

            erec_KuldKuldemenyek.KuldesMod = KuldesMod_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KuldesMod = pageView.GetUpdatedByView(KuldesMod_DropDownList);

            if (!_Load)
            {
                DateTime beerkezes;
                if (!DateTime.TryParse(BeerkezesIdeje_CalendarControl.Text, out beerkezes))
                {
                    throw new FormatException("A beérkezés időpontjának megadott érték nem megfelelő formátumú!");
                }
                if (beerkezes > DateTime.Now)
                {
                    throw new FormatException("A beérkezés időpontja nem lehet a jelenlegi időpontnál későbbi!");
                }
                erec_KuldKuldemenyek.BeerkezesIdeje = BeerkezesIdeje_CalendarControl.Text;
                erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(BeerkezesIdeje_CalendarControl);
            }

            erec_KuldKuldemenyek.HivatkozasiSzam = HivatkozasiSzam_Kuldemeny_TextBox.Text;
            erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(HivatkozasiSzam_Kuldemeny_TextBox);

            erec_KuldKuldemenyek.UgyintezesModja = UgyintezesModja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);

            erec_KuldKuldemenyek.Csoport_Id_Cimzett = CsoportId_CimzettCsoportTextBox1.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(CsoportId_CimzettCsoportTextBox1);

            erec_KuldKuldemenyek.Surgosseg = Surgosseg_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.Surgosseg = pageView.GetUpdatedByView(Surgosseg_DropDownList);

            erec_KuldKuldemenyek.BarCode = VonalkodTextBoxVonalkod.Text;
            erec_KuldKuldemenyek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBoxVonalkod);

            erec_KuldKuldemenyek.BoritoTipus = KodtarakDropDownListBoritoTipus.SelectedValue;
            erec_KuldKuldemenyek.Updated.BoritoTipus = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            switch (KodtarakDropDownListBoritoTipus.SelectedValue)
            {
                case KodTarak.BoritoTipus.Boritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.Irat:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.EldobhatoBoritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.No;
                    break;
            }

            erec_KuldKuldemenyek.Updated.MegorzesJelzo = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            erec_KuldKuldemenyek.Updated.IktatniKell = true;

            GetFelbontoPanelDatas(erec_KuldKuldemenyek);

            #endregion
        }
        else if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            #region Email

            erec_KuldKuldemenyek.IraIktatokonyv_Id = Email_IraIktatoKonyvekDropDownList1.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(Email_IraIktatoKonyvekDropDownList1);

            erec_KuldKuldemenyek.Partner_Id_Bekuldo = Email_Bekuldo_PartnerTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(Email_Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.NevSTR_Bekuldo = Email_Bekuldo_PartnerTextBox.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(Email_Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.Cim_Id = Email_KuldCim_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(Email_KuldCim_CimekTextBox);

            erec_KuldKuldemenyek.CimSTR_Bekuldo = Email_KuldCim_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(Email_KuldCim_CimekTextBox);

            erec_KuldKuldemenyek.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.E_mail;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            if (!_Load)
            {
                DateTime beerkezes;
                if (!DateTime.TryParse(Email_BeerkezesIdeje_CalendarControl.Text, out beerkezes))
                {
                    throw new FormatException("A beérkezés időpontjának megadott érték nem megfelelő formátumú!");
                }
                if (beerkezes > DateTime.Now)
                {
                    throw new FormatException("A beérkezés időpontja nem lehet a jelenlegi időpontnál későbbi!");
                }

                erec_KuldKuldemenyek.BeerkezesIdeje = Email_BeerkezesIdeje_CalendarControl.Text;
                erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(Email_BeerkezesIdeje_CalendarControl);
            }

            erec_KuldKuldemenyek.HivatkozasiSzam = Email_HivatkozasiSzam_TextBox.Text;
            erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(Email_HivatkozasiSzam_TextBox);

            //erec_KuldKuldemenyek.UgyintezesModja = UgyintezesModja_DropDownList.SelectedValue;
            //erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);

            erec_KuldKuldemenyek.Csoport_Id_Cimzett = Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(Email_CsoportIdCimzett_CsoportTextBox);

            // Sürgősség: kézzel beállítva Normál-ra
            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            //erec_KuldKuldemenyek.BarCode = VonalkodTextBoxVonalkod.Text;
            //erec_KuldKuldemenyek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBoxVonalkod);

            //erec_KuldKuldemenyek.BoritoTipus = KodtarakDropDownListBoritoTipus.SelectedValue;
            //erec_KuldKuldemenyek.Updated.BoritoTipus = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            //erec_KuldKuldemenyek.MegorzesJelzo = (cbMegorzesJelzo.Checked) ? Constants.Database.Yes : Constants.Database.No;
            //erec_KuldKuldemenyek.Updated.MegorzesJelzo = pageView.GetUpdatedByView(cbMegorzesJelzo);

            erec_KuldKuldemenyek.IktatniKell = Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IktatniKell = pageView.GetUpdatedByView(Email_IktatasiKotelezettsegKodtarakDropDownList);

            // Kezelés módja: elektronikus
            erec_KuldKuldemenyek.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = true;

            #endregion
        }

        erec_KuldKuldemenyek.Csoport_Id_Felelos = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

        // WS-en állítva:
        //erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
        //erec_KuldKuldemenyek.Updated.Allapot = true;



        erec_KuldKuldemenyek.Base.Ver = Record_Ver_HiddenField.Value;
        erec_KuldKuldemenyek.Base.Updated.Ver = true;

        // Áttéve a webservice-be:
        //erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        //erec_KuldKuldemenyek.Updated.PostazasIranya = true;

        erec_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
        erec_KuldKuldemenyek.Updated.Erkeztetes_Ev = true;


        // TODO: nem tudjuk miert kellenek, de kotelezoek
        erec_KuldKuldemenyek.PeldanySzam = "1";
        erec_KuldKuldemenyek.Updated.PeldanySzam = true;


        return erec_KuldKuldemenyek;
    }

    private void GetFelbontoPanelDatas(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (!_Load)
        {
            DateTime beerkezes;
            if (!DateTime.TryParse(BeerkezesIdeje_CalendarControl.Text, out beerkezes))
            {
                throw new FormatException("A beérkezés időpontjának megadott érték nem megfelelő formátumú!");
            }
            DateTime felbontas;
            if (!DateTime.TryParse(Kuld_FelbontasDatuma_CalendarControl.Text, out felbontas))
            {
                throw new FormatException("A bontás időpontjának megadott érték nem megfelelő formátumú!!");
            }
            if (beerkezes > DateTime.Now)
            {
                throw new FormatException("A beérkezés időpontja nem lehet a jelenlegi időpontnál későbbi!");
            }
            if (felbontas > DateTime.Now)
            {
                throw new FormatException("A bontás időpontja nem lehet a jelenlegi időpontnál későbbi!");
            }
            if (felbontas < beerkezes)
            {
                throw new FormatException("A bontás időpontja nem lehet a beérkezés időpontjánál korábbi!");
            }

            erec_KuldKuldemenyek.FelbontasDatuma = Kuld_FelbontasDatuma_CalendarControl.Text;
            erec_KuldKuldemenyek.Updated.FelbontasDatuma = pageView.GetUpdatedByView(Kuld_FelbontasDatuma_CalendarControl);
        }
        erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto = Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Bonto = pageView.GetUpdatedByView(Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox);
    }

    private void LoadKuldemenyComponents(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        {
            #region Küldemény

            if (_Load)
            {
                ListItem item =
                ErkeztetoKonyvekDropDrownList.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);
                if (item != null)
                {
                    ErkeztetoKonyvekDropDrownList.DropDownList.SelectedValue = item.Value;
                }
            }

            Bekuldo_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_KuldKuldemenyek.Partner_Id_Bekuldo, erec_KuldKuldemenyek.NevSTR_Bekuldo, EErrorPanel1);

            Kuld_CimId_CimekTextBox.SetCimekTextBoxByStringOrId(erec_KuldKuldemenyek.Cim_Id, erec_KuldKuldemenyek.CimSTR_Bekuldo, EErrorPanel1);

            KuldesMod_DropDownList.SelectedValue = erec_KuldKuldemenyek.KuldesMod;

            //BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

            HivatkozasiSzam_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

            UgyintezesModja_DropDownList.SelectedValue = erec_KuldKuldemenyek.UgyintezesModja;

            CsoportId_CimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
            CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

            Surgosseg_DropDownList.SelectedValue = erec_KuldKuldemenyek.Surgosseg;

            //VonalkodTextBoxVonalkod.Text = erec_KuldKuldemenyek.BarCode;

            KodtarakDropDownListBoritoTipus.SelectedValue = erec_KuldKuldemenyek.BoritoTipus;

            LoadBontoPanel(erec_KuldKuldemenyek);

            #endregion
        }
        else if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            #region Email

            if (_Load)
            {
                ListItem item =
                Email_IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);
                if (item != null)
                {
                    Email_IraIktatoKonyvekDropDownList1.DropDownList.SelectedValue = item.Value;
                }
            }

            Email_Bekuldo_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_KuldKuldemenyek.Partner_Id_Bekuldo, erec_KuldKuldemenyek.NevSTR_Bekuldo, EErrorPanel1);

            Email_KuldCim_CimekTextBox.SetCimekTextBoxByStringOrId(erec_KuldKuldemenyek.Cim_Id, erec_KuldKuldemenyek.CimSTR_Bekuldo, EErrorPanel1);

            Email_BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

            Email_HivatkozasiSzam_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

            Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
            Email_CsoportIdCimzett_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue = erec_KuldKuldemenyek.IktatniKell;

            #endregion
        }
    }

    private void LoadBontoPanel(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        //Kuld_FelbontasDatuma_CalendarControl.Text = erec_KuldKuldemenyek.FelbontasDatuma;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //#region Megőrzést jelző checkboxok állítása
        //if (cbMegorzesJelzo.Checked)
        //{
        //    Session["BoritoTipusValue"] = KodtarakDropDownListBoritoTipus.SelectedValue;
        //    Session["BoritoTipusChecked"] = "true";
        //}
        //else
        //{
        //    Session.Remove("BoritoTipusValue");
        //    Session.Remove("BoritoTipusChecked");
        //}

        //if (CheckBox_CimzettMegorzes.Checked)
        //{
        //    Session[session_CimzettId] = CsoportId_CimzettCsoportTextBox1.Id_HiddenField;
        //    Session[session_CimzettChecked] = "true";
        //}
        //else
        //{
        //    Session.Remove(session_CimzettId);
        //    Session.Remove(session_CimzettChecked);
        //}

        //UI.StoreValueInSession(Page, BeerkezesIdeje_CalendarControl.ID, BeerkezesIdeje_CalendarControl.Text, cbBeerkezesIdopontjaMegorzes);
        //#endregion

        //if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        //{

        //    if ((Command == CommandName.New && FunctionRights.GetFunkcioJog(Page, funkcio_EgyszerusitettIktatas) && Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        //        || Command == CommandName.New && FunctionRights.GetFunkcioJog(Page, funkcio_Erkeztetes) && Mode == ErkeztetesTipus.EmailErkeztetes)
        //    {
        //        bool voltHiba = false;
        //        bool ujUgyirat = false; // annak nyilvántartásához, hogy kell-e beszúrni standard tárgyszó rekordokat (csak újhoz kell)

        //        try
        //        {
        //            switch (Command)
        //            {
        //                case CommandName.New:
        //                    {
        //                        Result result_iktatas = null;
        //                        bool csakKuldemenyElokeszitesVolt = false;
        //                        bool csakerkeztetes = false;
        //                        string kuldemenyId = "";

        //                        #region Iktatáshoz szükséges adatok:

        //                        // (Alszámra iktatásnál) ha lezárt az ügyirat, felhasználótól megkérdezni valahogy, hogy most akkor 
        //                        // újra kéne nyitni az ügyiratot, vagy beleiktassunk a lezárt ügyiratba
        //                        // defaultból most az utóbbi eset lesz
        //                        IktatasiParameterek.UgyiratUjranyitasaHaLezart = false;

        //                        if (RadioButtonList_Lezartbaiktat_vagy_Ujranyit.SelectedValue == "1")
        //                        {
        //                            IktatasiParameterek.UgyiratUjranyitasaHaLezart = true;
        //                        }
        //                        else
        //                        {
        //                            IktatasiParameterek.UgyiratUjranyitasaHaLezart = false;
        //                        }

        //                        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        //                        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

        //                        // objektumok feltöltése a form komponenseiből:
        //                        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;

        //                        // A cascading dropdown cuccból vesszük:
        //                        String Erec_IraIktatokonyvek_Id = Iktatokonyvek_DropDownLis_Ajax.SelectedValue;

        //                        EREC_UgyUgyiratok erec_UgyUgyirat = GetBusinessObjectFromComponents_EREC_UgyUgyiratok();
        //                        EREC_IraIratok erec_IraIrat = GetBusinessObjectFromComponents();
        //                        EREC_IraKezFeljegyzesek erec_kezFeljegyzes = GetBusinessObjectFromComponents_EREC_IraKezFeljegyzesek();

        //                        EREC_KuldKuldemenyek kuldemenyAdatok = null;
        //                        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        //                        {
        //                            kuldemenyAdatok = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
        //                        }
        //                        else
        //                        {
        //                            kuldemenyAdatok = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
        //                        }
        //                        IktatasiParameterek.Adoszam = textAdoszam.Text;

        //                        #endregion


        //                        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        //                        {
        //                            #region Email érkeztetés/iktatás

        //                            EREC_KuldKuldemenyekService service_kuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        //                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //                            // Id-t és a verziót ebben adjuk át:
        //                            EREC_eMailBoritekok emailBoritek = new EREC_eMailBoritekok();
        //                            emailBoritek.Updated.SetValueAll(false);
        //                            emailBoritek.Base.Updated.SetValueAll(false);
        //                            emailBoritek.Id = EmailBoritekokId;
        //                            emailBoritek.Base.Ver = eMailBoritekok_Ver_HiddenField.Value;
        //                            emailBoritek.Base.Updated.Ver = true;

        //                            // Csak érkeztetés, vagy iktatás is?
        //                            if (ErkeztetesVagyIktatasRadioButtonList.SelectedValue == "0")
        //                            {
        //                                #region Érkeztetés/Iktatás

        //                                // Funkciójog ellenőrzés:
        //                                if (FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatas) == false)
        //                                {
        //                                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        //                                    return;
        //                                }

        //                                // Ha van megadva ügyirat id, akkor alszámra iktatás van
        //                                if (!String.IsNullOrEmpty(UgyUgyirat_Id))
        //                                {
        //                                    // Alszámra iktatás

        //                                    // !!! Alszámra iktatásnál nem az ajax-os dropdownból vesszük az iktatókönyvet!!! (IDEIGLENES MEGOLDÁS)
        //                                    Erec_IraIktatokonyvek_Id = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.SelectedValue;

        //                                    //alszamraIktatas = true;

        //                                    result_iktatas = service.EmailErkeztetesIktatas_Alszamra(execparam, kuldemenyAdatok, emailBoritek, Erec_IraIktatokonyvek_Id,
        //                                        UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_kezFeljegyzes, IktatasiParameterek);
        //                                    if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
        //                                    {
        //                                        // hiba
        //                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
        //                                        ErrorUpdatePanel1.Update();
        //                                        voltHiba = true;
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    // Normál iktatás

        //                                    // Funkciójog ellenőrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
        //                                    if (FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra))
        //                                    {
        //                                        // Hiba: Csak alszámra iktathat!
        //                                        // "A jogosultsága alapján Ön csak alszámra iktathat! Válasszon előzmény ügyiratot!"
        //                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UICsakAlszamraIktathat);
        //                                        ErrorUpdatePanel1.Update();
        //                                        voltHiba = true;
        //                                    }
        //                                    else
        //                                    {

        //                                        IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
        //                                        IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

        //                                        result_iktatas = service.EmailErkeztetesIktatas(execparam, kuldemenyAdatok, emailBoritek,
        //                                            Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(),
        //                                            erec_IraIrat, erec_kezFeljegyzes, IktatasiParameterek);
        //                                        if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
        //                                        {
        //                                            // hiba
        //                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
        //                                            ErrorUpdatePanel1.Update();
        //                                            voltHiba = true;
        //                                        }
        //                                        else if (result_iktatas.ErrorType == "NincsIktatva")
        //                                        {
        //                                            // Küldemény nem lett iktatva, csak rögzítve lettek bizonyos iktatási adatok

        //                                            csakKuldemenyElokeszitesVolt = true;
        //                                        }
        //                                        else
        //                                        {
        //                                            ujUgyirat = true;
        //                                        }
        //                                    }

        //                                }

        //                                #endregion
        //                            }
        //                            else
        //                            {
        //                                #region Csak érkeztetés

        //                                result_iktatas = service_kuldemenyek.ErkeztetesEmailbol(execParam, kuldemenyAdatok, emailBoritek);
        //                                if (!string.IsNullOrEmpty(result_iktatas.ErrorCode))
        //                                {
        //                                    // hiba:
        //                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
        //                                    ErrorUpdatePanel1.Update();
        //                                    voltHiba = true;
        //                                }

        //                                kuldemenyId = result_iktatas.Uid;

        //                                csakerkeztetes = true;

        //                                #endregion
        //                            }



        //                            #endregion
        //                        }
        //                        else
        //                        {
        //                            #region Küldemény érkeztetés/iktatás


        //                            // Ha van megadva ügyirat id, akkor alszámra iktatás van
        //                            if (!String.IsNullOrEmpty(UgyUgyirat_Id))
        //                            {
        //                                // Alszámra iktatás

        //                                // !!! Alszámra iktatásnál nem az ajax-os dropdownból vesszük az iktatókönyvet!!! (IDEIGLENES MEGOLDÁS)
        //                                Erec_IraIktatokonyvek_Id = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.SelectedValue;

        //                                //alszamraIktatas = true;

        //                                result_iktatas = service.EgyszerusitettIktatasa_Alszamra(execparam, Erec_IraIktatokonyvek_Id,
        //                                    UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_kezFeljegyzes, kuldemenyAdatok, IktatasiParameterek);
        //                                if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
        //                                {
        //                                    // hiba
        //                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
        //                                    ErrorUpdatePanel1.Update();
        //                                    voltHiba = true;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                // Normál iktatás

        //                                // Funkciójog ellenőrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
        //                                if (FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra))
        //                                {
        //                                    // Hiba: Csak alszámra iktathat!
        //                                    // "A jogosultsága alapján Ön csak alszámra iktathat! Válasszon előzmény ügyiratot!"
        //                                    ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UICsakAlszamraIktathat);
        //                                    ErrorUpdatePanel1.Update();
        //                                    voltHiba = true;
        //                                }
        //                                else
        //                                {

        //                                    IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
        //                                    IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

        //                                    result_iktatas = service.EgyszerusitettIktatasa(execparam,
        //                                        Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(),
        //                                        erec_IraIrat, erec_kezFeljegyzes, kuldemenyAdatok, IktatasiParameterek);
        //                                    if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
        //                                    {
        //                                        // hiba
        //                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
        //                                        ErrorUpdatePanel1.Update();
        //                                        voltHiba = true;
        //                                    }
        //                                    else if (result_iktatas.ErrorType == "NincsIktatva")
        //                                    {
        //                                        // Küldemény nem lett iktatva, csak rögzítve lettek bizonyos iktatási adatok

        //                                        csakKuldemenyElokeszitesVolt = true;
        //                                    }
        //                                    else
        //                                    {
        //                                        ujUgyirat = true;
        //                                    }
        //                                }
        //                            }


        //                            #endregion
        //                        }

        //                        if (!voltHiba && result_iktatas != null)
        //                        {
        //                            if (csakKuldemenyElokeszitesVolt == true)
        //                            {
        //                            }
        //                            else
        //                            {
        //                                // Iktatás sikeres volt:

        //                                String UjUgyirat_Id = "";
        //                                //String UjUgyiratDarab_Id = "";
        //                                String UjIrat_Id = "";
        //                                //String UjIratPeldany_Id = "";
        //                                try
        //                                {
        //                                    //// TODO: Nem Ok egyelőre, az osztály serializációját még meg kell csinálni
        //                                    //Contentum.eRecord.BaseUtility.Iratok.IktatasResult iktatasResult =
        //                                    //    (Contentum.eRecord.BaseUtility.Iratok.IktatasResult)result_iktatas.Record;
        //                                    //if (iktatasResult != null)
        //                                    //{
        //                                    //    UjUgyirat_Id = iktatasResult.UjUgyirat_Id;
        //                                    //    UjUgyiratDarab_Id = iktatasResult.UjUgyiratDarab_Id;
        //                                    //    UjIrat_Id = iktatasResult.UjIrat_Id;
        //                                    //    UjIratPeldany_Id = iktatasResult.UjIratPeldany_Id;
        //                                    //}

        //                                    // egyelőre csak az ügyirat id-ját küldjük vissza
        //                                    UjUgyirat_Id = (String)result_iktatas.Record;
        //                                    UjIrat_Id = result_iktatas.Uid;

        //                                    if (csakerkeztetes == false)
        //                                    {
        //                                        #region standard objektumfüggő tárgyszavak mentése ügyirathoz
        //                                        // csak új ügyiratra
        //                                        if (ujUgyirat == true)
        //                                        {
        //                                            EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = standardObjektumTargyszavak.GetEREC_ObjektumTargyszavaiList(true);

        //                                            if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
        //                                            {
        //                                                ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
        //                                                EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        //                                                Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
        //                                                            , EREC_ObjektumTargyszavaiList
        //                                                            , UjUgyirat_Id
        //                                                            , null
        //                                                            , Constants.TableNames.EREC_UgyUgyiratok
        //                                                            , "B1"
        //                                                            , false
        //                                                            );

        //                                                if (!String.IsNullOrEmpty(result_ot.ErrorCode))
        //                                                {
        //                                                    // hiba
        //                                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
        //                                                    ErrorUpdatePanel1.Update();
        //                                                    //voltHiba = true; // TODO: ??? true or false ???
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion standard objektumfüggő tárgyszavak mentése ügyirathoz
        //                                    }
        //                                }
        //                                catch
        //                                {
        //                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        //                                    return;
        //                                }

        //                                /// TODO majd a hívó listától függően beállítani az új rekord id-t,
        //                                /// egyelőre az új ügyirat id-ja lesz megadva
        //                                String returnId = UjUgyirat_Id;

        //                                if (e.CommandName == CommandName.Save)
        //                                {
        //                                    if (Command == CommandName.New)
        //                                    {
        //                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);

        //                                        if (csakerkeztetes)
        //                                        {
        //                                            // Érkeztetés eredménye:
        //                                            SetErkeztetesResultPanel(kuldemenyId);
        //                                        }
        //                                        else
        //                                        {
        //                                            // Iktatás eredménye: (+ Érkeztetés eredménye:)
        //                                            this.SetResultPanel(UjUgyirat_Id, UjIrat_Id);
        //                                        }
        //                                    }
        //                                }
        //                                else if (e.CommandName == CommandName.SaveAndClose)
        //                                {
        //                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);
        //                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        //                                }
        //                            }

        //                        }
        //                        else
        //                        {
        //                            // ha be volt töltve küldemény iktatási adat,
        //                            // egy hibajelenség leküzdésére:
        //                            //LoadKuldemenyComponents();

        //                        }



        //                        break;


        //                    }
        //            }
        //        }
        //        catch (FormatException fe)
        //        {
        //            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", fe.Message);
        //            ErrorUpdatePanel1.Update();
        //        }
        //    }
        //    else
        //    {
        //        UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        //    }
        //}

    }

    private EREC_IraIktatoKonyvek LoadUgyiratComponents(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        ElozmenyUgyiratID_HiddenField.Value = erec_UgyUgyiratok.Id;

        EREC_IraIktatoKonyvek iktatoKonyv = null;

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            iktatoKonyv = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillWithOneValue(
               Constants.IktatoErkezteto.Iktato, erec_UgyUgyiratok.IraIktatokonyv_Id, EErrorPanel1);
        }
        else
        {
            // iktatásnál
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillAndSetSelectedValue(true, true, Constants.IktatoErkezteto.Iktato
                , erec_UgyUgyiratok.IraIktatokonyv_Id, false, EErrorPanel1);
        }

        //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATOSZAM_KIEG
        //    , erec_UgyUgyiratok.IktatoszamKieg, true, EErrorPanel1);

        //UgyUgyirat_UgyiratTextBox.SetUgyiratTextBoxById(EErrorPanel1);

        trFoszam.Visible = true;
        //UgyiratFoszam_Label.Enabled = true;

        string fullFoszam = "";

        #region Iktatókönyv lekérése:
        if (iktatoKonyv == null)
        {
            EREC_IraIktatoKonyvekService IktatoKonyvekservice = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;

            Result IktatoKonyvekresult = IktatoKonyvekservice.Get(execParam);
            if (!string.IsNullOrEmpty(IktatoKonyvekresult.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, IktatoKonyvekresult);
                if (ErrorUpdatePanel1 != null)
                {
                    ErrorUpdatePanel1.Update();
                }
                return null;
            }
            else
            {
                iktatoKonyv = (EREC_IraIktatoKonyvek)IktatoKonyvekresult.Record;
            }
        }

        #endregion

        // BLG_292
        ExecParam execParamFoszam = UI.SetExecParamDefault(Page, new ExecParam());
        //fullFoszam = Ugyiratok.GetFullFoszam(erec_UgyUgyiratok, iktatoKonyv);
        fullFoszam = Ugyiratok.GetFullFoszam(execParamFoszam, erec_UgyUgyiratok, iktatoKonyv);


        //fullFoszam = Ugyiratok.GetFullFoszam(erec_UgyUgyiratok, Page, EErrorPanel1, ErrorUpdatePanel1);

        UgyiratFoszam_Label.Text = fullFoszam;

        if (!_Load)
        {
            UgyUgyirat_Hatarido_CalendarControl.Text = erec_UgyUgyiratok.Hatarido;
        }

        //UgyUgyirat_Ugytipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYTIPUS
        //    , erec_UgyUgyiratok.UgyTipus, true, EErrorPanel1);

        UgyUgyirat_SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = erec_UgyUgyiratok.IrattarbaKuldDatuma;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField =
            erec_UgyUgyiratok.IraIrattariTetel_Id;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(EErrorPanel1);

        UgyUgyirat_Targy_RequiredTextBox.Text = erec_UgyUgyiratok.Targy;

        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Felelos;
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        Ugyfelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Ugyfelelos;
        Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
        CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);

        regiAzonositoTextBox.Text = erec_UgyUgyiratok.RegirendszerIktatoszam;

        return iktatoKonyv;
    }

    private void ClearUgyiratComponents()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;

        //// iktatásnál
        //ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, Constants.IktatoErkezteto.Iktato
        //    , false, EErrorPanel1);

        trFoszam.Visible = false;

        UgyiratFoszam_Label.Text = String.Empty;

        DateTime _30DaysAfterToday = DateTime.Now.AddDays(30);
        UgyUgyirat_Hatarido_CalendarControl.Text = _30DaysAfterToday.ToShortDateString();

        UgyUgyirat_SkontroVege_CalendarControl.Text = String.Empty;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = String.Empty;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text = String.Empty;

        UgyUgyirat_Targy_RequiredTextBox.Text = String.Empty;

        //Kezelő kitöltve alapból a felhasználóra:
        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text = String.Empty;

        Ugyfelelos_CsoportTextBox.Id_HiddenField = String.Empty;
        Ugyfelelos_CsoportTextBox.Text = String.Empty;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField = String.Empty;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = String.Empty;
        CimekTextBoxUgyindito.Id_HiddenField = String.Empty;
        CimekTextBoxUgyindito.Text = String.Empty;

        regiAzonositoTextBox.Text = String.Empty;

    }

    private void SetErkeztetesResultPanel(string kuldemenyID)
    {
        SetErkeztetesResultPanel(kuldemenyID, false);
    }

    // ha volt iktatás, a küldemény nem módosítható, eltüntetjük a gombot
    private void SetErkeztetesResultPanel(string kuldemenyID, bool voltIktatas)
    {
        if (String.IsNullOrEmpty(kuldemenyID)) return;
        try
        {
            //Panel-ek beállítása
            KuldemenyPanel.Visible = false;
            EmailKuldemenyPanel.Visible = false;
            Ugyirat_Panel.Visible = false;
            IratPanel.Visible = false;
            StandardTargyszavakPanel.Visible = false;
            ErkeztetesVagyIktatasRadioButtonList.Visible = false;

            ErkeztetesResultPanel.Visible = true;

            //Étkeztetőszám beállítása
            ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_KuldKuldemenyekService srvKuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_KuldKuldemenyekSearch searchKuldemenyek = new EREC_KuldKuldemenyekSearch();
            searchKuldemenyek.Id.Value = kuldemenyID;
            searchKuldemenyek.Id.Operator = Query.Operators.equals;
            Result res = srvKuldemenyek.GetAllWithExtension(xcParam, searchKuldemenyek);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new Contentum.eUtility.ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Contentum.eUtility.ResultException(52106);
            }
            labelKuldemenyErkSzam.Text = res.Ds.Tables[0].Rows[0]["FullErkeztetoSzam"].ToString();

            //TabFooter beállítása
            TabFooter1.ImageButton_Close.Visible = true;
            TabFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            TabFooter1.Link_SaveAndNew.Visible = true;
            TabFooter1.ImageButton_Save.Visible = false;
            TabFooter1.ImageButton_Cancel.Visible = false;

            //string saveAndNewUrl = Request.Url.OriginalString;
            string saveAndNewUrl = "~/KuldKuldemenyekForm.aspx?"
                    + QueryStringVars.Command + "=" + CommandName.New;

            // ha volt betöltött template:
            if (!String.IsNullOrEmpty(FormHeader1.CurrentTemplateId))
            {
                saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader1.CurrentTemplateId;
            }
            TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

            //Módosítás,megtekintés gombok beállítása
            imgKuldemenyMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            if (voltIktatas == false)
            {
                imgKuldemenyModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                                "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            }
            else
            {
                imgKuldemenyModosit.Visible = false;
            }
            ////Funkciógombsor beállítása: átvételi elismervény meg hasonlól
            //divFunkcioGombsor.Visible = true;
            //FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
            //FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            //FunkcioGombsor.KiserolapVisible = true;
            //FunkcioGombsor.AtveteliElismervenyVisible = true;
            //FunkcioGombsor.XMLExportVisible = true;
            //FunkcioGombsor.Borito_nyomtatasa_A3Enabled = true;
            //FunkcioGombsor.Borito_nyomtatasa_A4Enabled = true;
            //FunkcioGombsor.KiserolapEnabled = true;
            //FunkcioGombsor.AtveteliElismervenyEnabled = true;
            //FunkcioGombsor.XMLExportEnabled = true;
            //FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = "javascript:window.open('KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA3');return false;";
            //FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = "javascript:window.open('KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA4');return false;";
            //FunkcioGombsor.KiserolapOnClientClick = "javascript:window.open('KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kisero');return false;";
            //FunkcioGombsor.AtveteliElismervenyOnClientClick = "javascript:window.open('KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Atveteli');return false;";
            //FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + kuldemenyID + "&tipus=Kuldemeny');return false;";

        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }
    }

    // a küldemény Id meghatározása után után meghívja a SetErkeztetesResultPanelt is
    private void SetResultPanel(string ugyiratId, string iratId)
    {
        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(iratId)) return;
        try
        {
            //Panel-ek beállítása
            KuldemenyPanel.Visible = false;
            EmailKuldemenyPanel.Visible = false;
            Ugyirat_Panel.Visible = false;
            //UgyiratDarab_Panel.Visible = false;
            IratPanel.Visible = false;
            ResultPanel.Visible = true;
            StandardTargyszavakPanel.Visible = false;
            ErkeztetesVagyIktatasRadioButtonList.Visible = false;

            //Főszámok beállítása
            ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
            //Ugyirat
            EREC_UgyUgyiratokService srvUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch searchUgyiratok = new EREC_UgyUgyiratokSearch(); ;
            searchUgyiratok.Id.Value = ugyiratId;
            searchUgyiratok.Id.Operator = Query.Operators.equals;
            Result res = srvUgyiratok.GetAllWithExtension(xcParam, searchUgyiratok);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new Contentum.eUtility.ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Contentum.eUtility.ResultException(52106);
            }
            labelUgyiratFoszam.Text = res.Ds.Tables[0].Rows[0]["Foszam_Merge"].ToString();


            //Irat
            EREC_IraIratokService srvIratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
            searchIratok.Id.Value = iratId;
            searchIratok.Id.Operator = Query.Operators.equals;
            res = srvIratok.GetAllWithExtension(xcParam, searchIratok);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new Contentum.eUtility.ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Contentum.eUtility.ResultException(52118);
            }
            labelIratFoszam.Text = res.Ds.Tables[0].Rows[0]["IktatoSzam_Merge"].ToString();

            // küldemény lekérése, hogy be lehessen állítani a KuldemenyResultPanel-t
            #region Kuldemeny

            string kuldemenyId = res.Ds.Tables[0].Rows[0]["KuldKuldemenyek_Id"].ToString();
            SetErkeztetesResultPanel(kuldemenyId, true);

            #endregion Kuldemeny
            //TabFooter beállítása
            TabFooter1.ImageButton_Close.Visible = true;
            TabFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            TabFooter1.Link_SaveAndNew.Visible = true;
            //TabFooter1.Link_SaveAndAlszam.Visible = true;
            TabFooter1.Link_SaveAndAlszamBejovo.Visible = true;
            TabFooter1.Link_SaveAndAlszamBelso.Visible = true;
            TabFooter1.ImageButton_Save.Visible = false;
            TabFooter1.ImageButton_Cancel.Visible = false;


            string saveAndNewUrl = String.Empty;

            saveAndNewUrl = "~/EgyszerusitettIktatasForm.aspx?"
                    + QueryStringVars.Command + "=" + CommandName.New;

            // ha volt betöltve template:
            if (!String.IsNullOrEmpty(FormHeader1.CurrentTemplateId))
            {
                saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader1.CurrentTemplateId;
            }


            TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

            //string saveAndAlszamUrl = String.Empty;
            //saveAndAlszamUrl = "~/EgyszerusitettIktatasForm.aspx?"
            //    + QueryStringVars.Command + "=" + CommandName.New
            //    + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;

            //TabFooter1.Link_SaveAndAlszam.NavigateUrl = saveAndAlszamUrl;

            string saveAndAlszamBejovoUrl = "~/EgyszerusitettIktatasForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;
            TabFooter1.Link_SaveAndAlszamBejovo.NavigateUrl = saveAndAlszamBejovoUrl;

            string saveAndAlszamBelsoUrl = String.Empty;
            saveAndAlszamBelsoUrl = "~/IraIratokForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New
                + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromEgyszerusitettIktatas;

            TabFooter1.Link_SaveAndAlszamBelso.NavigateUrl = saveAndAlszamBelsoUrl;


            //Módosítás,megtekintés gombok beállítása
            //ügyirat
            imgUgyiratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgUgyiratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            imgEloadoiIv.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("EloadoiIv_PrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId);
            //irat
            imgIratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            VonalkodTextBoxVonalkod.Validate = false;
            labelVonalkodPirosCsillag.Visible = false;
        }
        else
        {
            VonalkodTextBoxVonalkod.Validate = true;
            labelVonalkodPirosCsillag.Visible = true;
        }
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            _Load = true;

            LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader1.TemplateObject);

            _Load = false;
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            try
            {
                FormHeader1.NewTemplate(GetTemplateObjectFromComponents());
            }
            catch (FormatException fe)
            {

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", fe.Message);
                ErrorUpdatePanel1.Update();
            }
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    private void LoadComponentsFromTemplate(IktatasFormTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            // csak ha nincs megadva előzmény:
            if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                #region Ügyirat komponensek

                CascadingDropDown_AgazatiJel.SelectedValue = templateObject.AgazatiJel_Id;
                CascadingDropDown_Ugykor.SelectedValue = templateObject.UgyiratComponents.IraIrattariTetel_Id;
                CascadingDropDown_IktatoKonyv.SelectedValue = templateObject.UgyiratComponents.IraIktatokonyv_Id;
                CascadingDropDown_Ugytipus.SelectedValue = templateObject.UgyiratComponents.UgyTipus;

                UgyUgyirat_Targy_RequiredTextBox.Text = templateObject.UgyiratComponents.Targy;

                Ugyfelelos_CsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos;
                Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Felelos;
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
                    templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez;
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                // ügyindító/ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(templateObject.UgyiratComponents.Partner_Id_Ugyindito, templateObject.UgyiratComponents.NevSTR_Ugyindito, EErrorPanel1);
                CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(templateObject.UgyiratComponents.Cim_Id_Ugyindito, templateObject.UgyiratComponents.CimSTR_Ugyindito, EErrorPanel1);

                #endregion
            }

            #region Irat komponensek

            //IraIrat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA, templateObject.IratComponents.Kategoria
            //    , true, EErrorPanel1);

            IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, templateObject.IratComponents.Irattipus
                , true, EErrorPanel1);

            IraIrat_KiadmanyozniKell_CheckBox.Checked = (templateObject.IratComponents.KiadmanyozniKell == "1") ? true : false;

            IraIrat_Targy_RequiredTextBox.Text = templateObject.IratComponents.Targy;

            //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KEZELESI_FELJEGYZESEK_TIPUSA,
            //  templateObject.IraKezFeljegyz_KezelesTipus, true, EErrorPanel1);

            //IraKezFeljegyz_Leiras_TextBox.Text = templateObject.IraKezFeljegyz_Leiras;

            #endregion

            #region Kuldemeny komponensek

            if (templateObject.KuldemenyComponents != null)
            {
                LoadKuldemenyComponents(templateObject.KuldemenyComponents);
            }

            #endregion

            #region elozmeny search

            if (templateObject.ElozmenySearch != null)
            {
                evIktatokonyvSearch.EvTol = templateObject.ElozmenySearch.Ev;
                IktatoKonyvekDropDownList_Search.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, templateObject.ElozmenySearch.Ev, templateObject.ElozmenySearch.Ev, true,
                    false, templateObject.ElozmenySearch.Iktatokonyv, EErrorPanel1);
                numberFoszamSearch.Text = templateObject.ElozmenySearch.Foszam;
            }

            #endregion

            #region hataridos feladat

            if (templateObject.HataridosFeladat != null)
            {
                FeljegyzesPanel.SetFeladatByBusinessObject(templateObject.HataridosFeladat);
            }
            #endregion

            if (IsRequireOverwriteTemplateDatas())
            {
                LoadSessionDatas();
            }

        }
    }

    private IktatasFormTemplateObject GetTemplateObjectFromComponents()
    {
        //EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();      
        IktatasFormTemplateObject templateObject = new IktatasFormTemplateObject();

        // csak ha nincs megadva előzmény:
        if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            #region Ügyirat komponensek

            templateObject.AgazatiJel_Id = AgazatiJelek_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIktatokonyv_Id = Iktatokonyvek_DropDownLis_Ajax.SelectedValue; ;

            templateObject.UgyiratComponents.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;

            // intézési határidő nem kell

            //templateObject.UgyiratComponents.IktatoszamKieg = UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.SelectedValue;

            templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;

            // ügyindító/ügyfél
            templateObject.UgyiratComponents.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            templateObject.UgyiratComponents.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            templateObject.UgyiratComponents.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            templateObject.UgyiratComponents.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;

            #endregion
        }

        #region Irat komponensek

        templateObject.IratComponents.Targy = IraIrat_Targy_RequiredTextBox.Text;

        //templateObject.IratComponents.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;

        templateObject.IratComponents.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;

        //templateObject.IraKezFeljegyz_KezelesTipus = IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue;

        //templateObject.IraKezFeljegyz_Leiras = IraKezFeljegyz_Leiras_TextBox.Text;

        templateObject.IratComponents.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";

        #endregion

        #region Kuldemeny komponensek

        _Load = true;
        templateObject.KuldemenyComponents = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
        _Load = false;

        #endregion

        #region elozmeny search

        templateObject.ElozmenySearch = new IktatasFormTemplateObject.ElozmenySearchcomponent();
        templateObject.ElozmenySearch.Ev = evIktatokonyvSearch.EvTol;
        templateObject.ElozmenySearch.Iktatokonyv = IktatoKonyvekDropDownList_Search.SelectedValue;
        templateObject.ElozmenySearch.Foszam = numberFoszamSearch.Text;

        #endregion

        #region hataridos feladat
        templateObject.HataridosFeladat = FeljegyzesPanel.GetBusinessObject();
        #endregion

        return templateObject;
    }

    #endregion

    #region Kuldemeny, Elozmeny gyors kereses

    protected void ImageButton_Elozmeny_Click(object sender, ImageClickEventArgs e)
    {
        UgyiratSzerelesiLista1.Reset();

        #region Keresesi mezok ellenorzese
        string errorHeader = "Előzmény keresés hiba!";
        if (String.IsNullOrEmpty(evIktatokonyvSearch.EvTol))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva év!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (IktatoKonyvekDropDownList_Search.DropDownList.SelectedIndex == -1 || String.IsNullOrEmpty(IktatoKonyvekDropDownList_Search.SelectedValue))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs kiválasztva iktatókönyv!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (String.IsNullOrEmpty(numberFoszamSearch.Text))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva főszám!");
            ErrorUpdatePanel1.Update();
            return;
        }
        #endregion

        try
        {
            if (IsRegiAdatSearch())
            {
                SearchRegiElozmenyUgyirat();
            }
            else
            {
                SearchElozmenyUgyirat();
            }
        }
        catch (FormatException fe)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, fe.Message);
            ErrorUpdatePanel1.Update();
        }
    }

    private void SearchRegiElozmenyUgyirat()
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch search = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
        evIktatokonyvSearch.SetSearchObjectFields(search.UI_YEAR);
        search.UI_SAV.Value = IktatoKonyvekDropDownList_Search.GetSelectedSav();
        search.UI_SAV.Operator = Query.Operators.equals;
        search.UI_NUM.Value = numberFoszamSearch.Text;
        search.UI_NUM.Operator = Query.Operators.equals;
        Contentum.eMigration.Service.MIG_FoszamService service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetFoszamWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            DisplayElozmenySearchError(res, false);
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

        Contentum.eMigration.BusinessDocuments.MIG_Foszam foszam = (Contentum.eMigration.BusinessDocuments.MIG_Foszam)res.Record;

        if (foszam != null && !String.IsNullOrEmpty(foszam.Id))
        {
            //Edok-os ügyiratba van szerelve
            string utoiratId = foszam.Edok_Utoirat_Id;
            if (!String.IsNullOrEmpty(utoiratId))
            {
                SetSzerelesiLista(res, true, true);
                SearchElozmenyUgyirat(utoiratId);
                return;
            }

            if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }

            MigraltUgyiratID_HiddenField.Value = foszam.Id;
            UgyUgyirat_Targy_RequiredTextBox.Text = foszam.Conc;

            DataRow foszamRow = res.Ds.Tables[0].Rows[res.Ds.Tables[0].Rows.Count - 1];
            regiAzonositoTextBox.Text = foszamRow["Foszam_Merge"].ToString();
            IrattariTetelTextBox.Text = foszamRow["IrattariTetelszam"].ToString();
            MegkulJelzesTextBox.Text = foszamRow["MegkulJelzes"].ToString();

            SetElozmenyRegiUgyirat();
            SetSzerelesiLista(res, true);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }
    }

    private void SearchElozmenyUgyirat(string ugyiratId)
    {
        bool searchUtoirat = !String.IsNullOrEmpty(ugyiratId);
        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

        if (!searchUtoirat)
        {
            evIktatokonyvSearch.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);
            IktatoKonyvekDropDownList_Search.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
            search.Foszam.Value = numberFoszamSearch.Text;
            search.Foszam.Operator = Query.Operators.equals;
        }
        else
        {
            search.Id.Value = ugyiratId;
            search.Id.Operator = Query.Operators.equals;
        }

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetUgyiratWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            if (searchUtoirat)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A keresett régi ügyirat szerelve van. A szülő ügyirat lekérése sikertelen: " + ugyiratId);
            }
            else
            {
                DisplayElozmenySearchError(res, true);
                ClearElozmenyUgyirat();
            }
            ErrorUpdatePanel1.Update();
            return;
        }



        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;

        if (ugyirat != null && !String.IsNullOrEmpty(ugyirat.Id))
        {
            if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }
            ElozmenyUgyiratID_HiddenField.Value = ugyirat.Id;
            SetElozmenyUgyirat(ugyirat);
            SetSzerelesiLista(res, false, searchUtoirat);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

    }

    private void DisplayElozmenySearchError(Result res, bool IsElozmenyHiba)
    {
        if (res.ErrorCode == "57000")
        {
            string onclick;
            if (IsElozmenyHiba)
                onclick = GetElozmenyOnClientClick();
            else
                onclick = GetMigraltOnClientClick();
            string hiba = "A keresett ügyirat nem található. <span onclick=\"" + onclick + "\" class=\"linkStyle\">Kereső képernyő megnyitása</span>";
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", hiba);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        }
    }

    private void SearchElozmenyUgyirat()
    {
        SearchElozmenyUgyirat(String.Empty);
    }

    private void SetSzerelesiLista(Result result)
    {
        SetSzerelesiLista(result, false, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat)
    {
        SetSzerelesiLista(result, RegiAdat, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat, bool IsUtoirat)
    {
        string SelectedId = String.Empty;

        if (!RegiAdat)
            SelectedId = ElozmenyUgyiratID_HiddenField.Value;
        else
            SelectedId = MigraltUgyiratID_HiddenField.Value;

        DataTable ugyiratokTable = result.Ds.Tables[0];

        if (ugyiratokTable != null)
        {
            if (ugyiratokTable.Rows.Count > 1 || IsUtoirat)
            {
                UgyiratSzerelesiLista1.SelectedId = SelectedId;
                if (RegiAdat)
                {
                    UgyiratSzerelesiLista1.DataSourceRegiAdat = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBindRegiAdat();
                }
                else
                {
                    UgyiratSzerelesiLista1.DataSource = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBind();
                }
            }
        }
    }

    private int MigralasEve
    {
        get
        {
            int MigralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE);
            if (MigralasEve == 0) MigralasEve = 2008;
            return MigralasEve;
        }
    }

    private bool IsRegiAdatSearch()
    {
        int Ev = evIktatokonyvSearch.EvTol_Integer;
        Logger.Debug("Migrálás Éve: " + MigralasEve);
        if (Ev < MigralasEve)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public string GetElozmenyKeresesOnClientClick()
    {
        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            return "return false;";
        }

        string jsQuery = "var query = ''; if(ev) query = '&" + QueryStringVars.ElozmenySearchParameters.Ev + "=' + ev.value;";
        jsQuery += "if(selectedValue.trim() != '') query += '&" + QueryStringVars.ElozmenySearchParameters.IktatokonyvValue + "=' + selectedValue;";
        jsQuery += "if(foszam && foszam.value != '') query += '&" + QueryStringVars.ElozmenySearchParameters.Foszam + "=' + foszam.value;";

        string js = "var ev = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + "');var iktatokonyv = $get('" + IktatoKonyvekDropDownList_Search.DropDownList.ClientID + "');var index = -1; if(iktatokonyv) index = iktatokonyv.selectedIndex;";
        js += "var selectedValue = ''; if(index > -1) selectedValue = iktatokonyv.options[index].value;var foszam = $get('" + numberFoszamSearch.TextBox.ClientID + "');if(ev && foszam && ev.value.trim() != '' && foszam.value.trim() != '' && selectedValue.trim() != '')";
        js += "{" + Page.ClientScript.GetPostBackEventReference(ImageButton_Elozmeny, "") + ";this.disabled = true;ev.disabled = true;foszam.disabled = true;iktatokonyv.disabled = true;iktatokonyv.options[iktatokonyv.selectedIndex].text = 'Keresés folyamatban...';return false;}";

        string OnClientClick = js + jsQuery + "if(ev && (ev.value== '' && this.id == '" + ImageButton_MigraltKereses.ClientID + "' || !isNaN(parseInt(ev.value)) && parseInt(ev.value) < " + MigralasEve + ")){";
        OnClientClick += GetMigraltOnClientClick("' + query + '");
        OnClientClick += "} else {";
        OnClientClick += GetElozmenyOnClientClick("' + query + '");
        OnClientClick += "}";

        return OnClientClick;
    }

    public string GetMigraltOnClientClick()
    {
        return GetMigraltOnClientClick(String.Empty);
    }

    public string GetMigraltOnClientClick(string dynamicQuery)
    {
        string OnClientClick = JavaScripts.SetOnClientClick(eMigration.migralasLovListUrl,
            QueryStringVars.HiddenFieldId + "=" + MigraltUgyiratID_HiddenField.ClientID + "&" +
            QueryStringVars.AzonositoTextBox + "=" + regiAzonositoTextBox.ClientID + "&" +
            QueryStringVars.IrattariTetelTextBox + "=" + IrattariTetelTextBox.ClientID + "&" +
            QueryStringVars.MegkulJelzesTextBox + "=" + MegkulJelzesTextBox.ClientID + "&" +
            QueryStringVars.TextBoxId + "=" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID + "&" + QueryStringVars.RefreshCallingWindow + "=1"
            + "&" + QueryStringVars.Filter + "=" + Constants.FilterType.Ugyiratok.Szereles + dynamicQuery,
            Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, Ugyirat_Panel.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    public string GetElozmenyOnClientClick()
    {
        return GetElozmenyOnClientClick(String.Empty);
    }

    public string GetElozmenyOnClientClick(string dynamicQuery)
    {
        string OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokLovList.aspx"
            , QueryStringVars.HiddenFieldId + "=" + ElozmenyUgyiratID_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=1"
            + "&" + QueryStringVars.Filter + "=" + Constants.AlszamraIktathatoak + dynamicQuery
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, ImageButton_Elozmeny.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    #endregion
}
