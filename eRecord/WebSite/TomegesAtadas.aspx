﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TomegesAtadas.aspx.cs" Inherits="TomegesAtadas" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="~/Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="ff" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="JavaScripts/CheckBoxes.js" type="text/javascript"></script>
    <asp:ScriptManager runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>

    <fh:FormHeader ID="FormHeader1" runat="server" DisableModeLabel="true" HeaderTitle="<%$Resources:Form,TomegesAtadasHeaderTitle %>" />

    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="MainPanel" runat="server">
        <br />
        <asp:GridView ID="tomegesAtadasGridView" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="false"
            AllowSorting="true" AutoGenerateColumns="false" DataKeyNames="Id" OnRowDataBound="TomegesAtadasGridView_RowDataBound" OnRowCreated="TomegesAtadasGridView_RowCreated" OnSorting="TomegesAtadasGridView_Sorting">
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" Enabled='<%# Eval("Atadhato") %>' Checked='<%# Eval("Atadhato") %>' CssClass="HideCheckBoxText" />

                        <asp:Panel ID="pnlErrorDetail" runat="server" Style="display: none;" CssClass="InfoPopupPanel"><%# Eval("ErrorDetail") %></asp:Panel>
                        <ajaxToolkit:HoverMenuExtender ID="hmeErrorDetail" runat="server" PopupControlID="pnlErrorDetail" Enabled='<%# !(bool)Eval("Atadhato") %>' PopupPosition="Center">
                        </ajaxToolkit:HoverMenuExtender>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Azonosito" HeaderText="Azonosító">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyzés">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="210px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Jegyzékre helyezés">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Sztornó" SortExpression="ErvVege"
                    ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Label ID="labelSztornoDate" runat="server" Text='<%# (!Convert.IsDBNull(Eval("ErvVege")) && DateTime.Compare((DateTime)Eval("ErvVege"), DateTime.Now) < 0) ?  Eval("ErvVege") : "" %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Note" HeaderText="Átvevõ iktatószáma" SortExpression="Note">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="AtadasDatuma" HeaderText="Átadás dátuma" SortExpression="AtadasDatuma" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                    HeaderStyle-Width="120px" />
            </Columns>
        </asp:GridView>

        <br />

        <eUI:eFormPanel ID="EFormPanelJegyzekTetel" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr id="trAtadas" runat="server" class="urlapSor" visible="true">
                    <td class="mrUrlapCaption_shorter">
                        <asp:Label runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label runat="server" Text="Átadás dátuma:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:CalendarControl ID="CalendarControlAtadas" runat="server" ReadOnly="false" Validate="true" TimeVisible="true" />                        
                    </td>
                </tr>
            </table>
        </eUI:eFormPanel>

        <ff:FormFooter ID="FormFooter1" runat="server" Command="Modify" OnButtonsClick="FormFooter1_ButtonsClick" />
    </asp:Panel>
</asp:Content>
