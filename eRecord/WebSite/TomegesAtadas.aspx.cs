﻿using AjaxControlToolkit;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TomegesAtadas : System.Web.UI.Page
{

    private string[] SelectedIds
    {
        get
        {
            return Session[Constants.SelectedJegyzekTetelIds].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }
    }

    private IEnumerable<string> CheckedIds
    {
        get
        {
            foreach (GridViewRow row in tomegesAtadasGridView.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                    continue;
                CheckBox check = row.FindControl("check") as CheckBox;
                if (check.Checked)
                    yield return tomegesAtadasGridView.DataKeys[row.DataItemIndex].Value.ToString();
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekTetelAtadas");
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //FormFooter1.ImageButton_Save.OnClientClick = string.Format("{0} alert('{1}');", JavaScripts.SetOnClientClickIsSelectedRow(tomegesAtadasGridView.ClientID), Resources.List.UI_Atadas_AtadasFizikailag);
        FormHeader1.HeaderTitle = Resources.Form.TomegesAtadasHeaderTitle;

        CalendarControlAtadas.Validate = true;
        CalendarControlAtadas.Validator.ErrorMessage = "A mező kitöltése kötelező";

        if (IsPostBack)
        {
            // errorPanel eltüntetése, ha ki volt rakva:
            FormHeader1.ErrorPanel.Visible = false;
        }
        else
        {
            CalendarControlAtadas.SetTodayAndTime();
            TomegesAtadasGridViewBind();
        }
    }

    private void TomegesAtadasGridViewBind()
    {
        string sortExpression;

        SortDirection sortDirection = Search.GetSortDirectionFromViewState(tomegesAtadasGridView.ID, ViewState, SortDirection.Descending);

        sortExpression = Search.GetSortExpressionFromViewState(tomegesAtadasGridView.ID, ViewState, "Azonosito");
        TomegesAtadasGridViewBind(sortExpression, sortDirection);
    }


    private void TomegesAtadasGridViewBind(string sortExpression, SortDirection sortDirection)
    {
        Result result;
        result = GetJegyzekTetelekData(sortExpression, sortDirection);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            ErrorUpdatePanel.Update();
        }
        else
        {
            SetEnabled(result);
            tomegesAtadasGridView.DataSource = result.Ds;
            tomegesAtadasGridView.DataBind();
        }
    }

    private Result GetJegyzekTetelekData(string sortExpression, SortDirection sortDirection)
    {
        EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraJegyzekTetelekSearch search = new EREC_IraJegyzekTetelekSearch();
        search.WhereByManual = string.Format(" and EREC_IraJegyzekTetelek.Id IN ({0}) ", string.Join(", ", SelectedIds.Select(id => string.Format("'{0}'", id)).ToArray()));
        search.OrderBy = Search.GetOrderBy(tomegesAtadasGridView.ID, ViewState, sortExpression, sortDirection);
        Result res = service.GetAllWithExtension(ExecParam, search);
        return res;
    }

    private void SetEnabled(Result result)
    {
        result.Ds.Tables[0].Columns.Add("Atadhato", typeof(bool));
        result.Ds.Tables[0].Columns.Add("ErrorDetail", typeof(string));
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            String errorDetail = "";

            bool atadhato = true;


            string objType = row["Obj_Type"].ToString();
            string objId = row["Obj_Id"].ToString();
            if (!String.IsNullOrEmpty(objType))
            {
                if (objType == Constants.TableNames.EREC_PldIratPeldanyok)
                {
                    EREC_PldIratPeldanyokService iratPledanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam iratPeldanyExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    iratPeldanyExecParam.Record_Id = objId;
                    Result iratPeldanyResult = iratPledanyService.Get(iratPeldanyExecParam);
                    if (!iratPeldanyResult.IsError)
                    {
                        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)iratPeldanyResult.Record;
                        if (iratPeldany.UgyintezesModja== KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
                        {
                            atadhato = false;
                            errorDetail = "Elektronikus iratpéldány nem adható át. ";
                        }
                    }

                }
                else if (objType == Constants.TableNames.EREC_UgyUgyiratok)
                {
                    EREC_UgyUgyiratokService ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam ugyiratExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    ugyiratExecParam.Record_Id = objId;
                    Result ugyiratResult = ugyiratService.Get(ugyiratExecParam);
                    if (!ugyiratResult.IsError)
                    {
                        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratResult.Record;

                        if (ugyirat.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus)
                        {
                            atadhato = false;
                            errorDetail = "Elektronikus ügyirat nem adható át. ";
                        }

                    }
                }
            }

            if (!String.IsNullOrEmpty(row["AtadasDatuma"].ToString()))
            {
                atadhato = false;
                errorDetail += "A tétel már át van adva. ";
            }

            string jegyzekId = row["Jegyzek_Id"].ToString();

            EREC_IraJegyzekekService jegyzekService = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = jegyzekId;
            Result res = jegyzekService.Get(execParam);

            if (!res.IsError)
            {
                EREC_IraJegyzekek jegyzek = (EREC_IraJegyzekek)res.Record;

                if (String.IsNullOrEmpty(jegyzek.LezarasDatuma))
                {
                    atadhato = false;
                    errorDetail += "A tétel jegyzéke nem zárolt. ";
                }
            }

            row["Atadhato"] = atadhato;

            if (!atadhato)
                row["ErrorDetail"] = errorDetail;

        }
    }


    protected void TomegesAtadasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = e.Row.DataItem as DataRowView;
            if (!(bool)dataRowView["Atadhato"])
                e.Row.CssClass += " ViewDisabledWebControl";
        }
    }

    protected void TomegesAtadasGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HoverMenuExtender hmeErrorDetail = e.Row.FindControl("hmeErrorDetail") as HoverMenuExtender;
            e.Row.ID = "TomegesAtadas_" + e.Row.RowIndex.ToString();
            hmeErrorDetail.TargetControlID = e.Row.ID;
        }
    }

    protected void TomegesAtadasGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TomegesAtadasGridViewBind(e.SortExpression, UI.GetSortToGridView(tomegesAtadasGridView.ID, ViewState, e.SortExpression));
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
            Result result = null;

            foreach (string id in CheckedIds)
            {
                execParam.Record_Id = id;
                EREC_IraJegyzekTetelek tetel = (EREC_IraJegyzekTetelek)service.Get(execParam).Record;
                tetel.Updated.SetValueAll(false);
                tetel.Base.Updated.SetValueAll(false);
                tetel.AtadasDatuma = CalendarControlAtadas.Text;
                tetel.Updated.AtadasDatuma = true;
                tetel.Base.Updated.Ver = true;

                result = service.Update(execParam, tetel);

                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

            if (result != null && !result.IsError)
            {
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
        }
    }

}