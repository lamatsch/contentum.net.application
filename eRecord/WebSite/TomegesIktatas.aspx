﻿<%@ Page Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TomegesIktatas.aspx.cs" 
Inherits="TomegesIktatas"  Async="true" %>

<%@ Register Src="~/eRecordComponent/TomegesIktatasFileInput.ascx" TagName="UploadFile" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <uc3:UploadFile ID="TomegesIktatasInput" runat="server" CssClass="mrUrlap" />
            </td>
        </tr>
    </table>
    				
</asp:Content>


