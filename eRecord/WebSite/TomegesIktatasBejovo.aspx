﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TomegesIktatasBejovo.aspx.cs" Inherits="TomegesIktatasBejovo"
    Async="true" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc8" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc8:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="Tömeges iktatás (Bejövő)" />

    <div>
        <%--Hiba megjelenites--%>
        <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <eUI:eErrorPanel ID="ErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--/Hiba megjelenites--%>

        <%--<asp:UpdatePanel ID="MainUpdatePanel" runat="server">
        <ContentTemplate>--%>
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <contenttemplate>
							<table>
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption">Import sablon:</td>
                                    <td class="mrUrlapMezo" style="text-align: left;">                             
                                        <asp:LinkButton runat="server" ID="btnGetImportSablon" Text="Letöltés" OnClick="btnGetImportSablon_Click" />
                                    </td>
                                </tr>

                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption">
                                        Import fájl:
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:FileUpload id="FileUpload1" runat="server"  accept=".xlsx"></asp:FileUpload>
                                        <%--<asp:RequiredFieldValidator id="Validator1" runat="server" ErrorMessage="<%$Resources:LovList,UploadLovHasNoFile%>" Display="None" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
							            <ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1"></ajaxToolkit:ValidatorCalloutExtender>--%>    
                                    </td>
                                </tr>
                               
							</table>

						</contenttemplate>
            &nbsp;
                    &nbsp;
        </eUI:eFormPanel>
        <br />
        <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false">
            <h3>Iktatás eredménye</h3>
            <table style="width: 100%;">
                <tr class="urlapSor_kicsi">
                    <td class="mrUrlapCaption">Napló:</td>
                    <td class="mrUrlapMezo" style="text-align: left; width: 95%;">
                        <%--<asp:Literal runat="server" ID="resultText" />--%>
                        <asp:TextBox runat="server" ID="txtBoxIktatasResult" TextMode="MultiLine" Width="95%" Height="150" />
                    </td>
                </tr>
                <tr id="trLinkResultCsv" class="urlapSor_kicsi" style="display: none;">
                    <td class="mrUrlapCaption">Eredmény fájl:</td>
                    <td class="mrUrlapMezo" style="text-align: left;">
                        <asp:HyperLink runat="server" ID="linkResultCsv" Text="Letöltés" />
                    </td>
                </tr>
                <tr id="trLinkResultLog" class="urlapSor_kicsi" style="display: none;">
                    <td class="mrUrlapCaption">Napló fájl:</td>
                    <td class="mrUrlapMezo" style="text-align: left;">
                        <asp:LinkButton runat="server" ID="linkResultLog" Text="Letöltés" OnClick="linkResultLog_Click" />
                    </td>
                </tr>
            </table>

            <div id="divInProgressMsg" style="font-weight: bold;">
                A művelet végrehajtása folyamatban...
            </div>

        </eUI:eFormPanel>

        <table style="width: 70%;">
            <tr>
                <td style="text-align: center;">
                    <asp:ImageButton TabIndex="1" ID="ImageOk" runat="server"
                        ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                        onmouseover="swapByName(this.id,'rendben2.jpg')"
                        onmouseout="swapByName(this.id,'rendben.jpg')"
                        OnClick="UploadBtn_Click" CommandName="Ok" Visible="True" />

                    <asp:ImageButton TabIndex="1" ID="ImageButtonBack" runat="server"
                        ImageUrl="~/images/hu/ovalgomb/vissza.jpg"
                        onmouseover="swapByName(this.id,'vissza2.jpg')"
                        onmouseout="swapByName(this.id,'vissza.jpg')"
                        OnClientClick="window.location.href='TomegesIktatasBejovo.aspx'; return false;"
                        Visible="False" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hfBackgroundProcessId" runat="server" Value="" />

        <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
    </div>

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>

    <script type="text/javascript">
        // namespace:
        var TomegesIktatas = {};

        TomegesIktatas.lastStatusGetTime = '';

        TomegesIktatas.checkBgProcessStatus = function () {

            var bgProcessId = $('#<%=hfBackgroundProcessId.ClientID %>').val();

            if (bgProcessId) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: 'TomegesIktatasBejovo.aspx/GetTomegesIktatasStatusz',
                    data: '{"processId":"' + bgProcessId + '", "lastStatusGetTime":"' + TomegesIktatas.lastStatusGetTime + '"}',
                    success:
                        function (response) {
                            TomegesIktatas.processStatusResult(response.d);
                        }
                    , error: function (request, status, error) {
                        TomegesIktatas.addNewLogMsg(error);

                        // Timer leállítása:
                        clearInterval(TomegesIktatas.Timer);
                    }
                });
            }
        };

        TomegesIktatas.processStatusResult = function (statusResult) {
            if (statusResult) {
                TomegesIktatas.lastStatusGetTime = statusResult.StatusGetTime;

                if (statusResult.NewLogItems) {
                    for (var i = 0; i < statusResult.NewLogItems.length; i++) {
                        var logMsg = statusResult.NewLogItems[i];
                        TomegesIktatas.addNewLogMsg(logMsg);
                    }
                }

                if (statusResult.BackgroundProcessError) {
                    TomegesIktatas.addNewLogMsg(statusResult.BackgroundProcessError);
                }

                if (statusResult.BackgroundProcessEnded) {
                    // Eredmény fájlra link:
                    if (statusResult.ResultTempDirName && statusResult.ResultTempFileName) {
                        var resultCsvUrl = "TomegesIktatasBejovo.aspx?Command=DownloadResult&TempDir=" + statusResult.ResultTempDirName
                            + "&TempFile=" + statusResult.ResultTempFileName;

                        $('#<%=linkResultCsv.ClientID %>').attr("href", resultCsvUrl);

                        $('#trLinkResultCsv').show(500);
                        $('#trLinkResultLog').show(500);
                    }

                    $('#divInProgressMsg').hide();

                    // Timer leállítása:
                    clearInterval(TomegesIktatas.Timer);
                }
            }
        };

        TomegesIktatas.addNewLogMsg = function (logMsg) {
            var txtBox = $('#<%=txtBoxIktatasResult.ClientID %>');
            txtBox.val(txtBox.val() + '\n' + logMsg);

            txtBox.scrollTop(txtBox[0].scrollHeight);
        };

        // Periodikus státusz-ellenőrzés indítása
        TomegesIktatas.startStatusCheck = function () {
            TomegesIktatas.Timer = setInterval(TomegesIktatas.checkBgProcessStatus, 1000);
        };

    </script>


</asp:Content>
