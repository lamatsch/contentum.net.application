﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;
using OfficeOpenXml;
using Newtonsoft.Json;
using Contentum.eRecord.UtilityExcel;
using Contentum.eRecord.TomegesIktatas;
using Contentum.eIntegrator.Service.Helper;

public partial class TomegesIktatasBejovo : Contentum.eUtility.UI.PageBase
{
    private const string funkcio_BejovoIratIktatas = "BejovoIratIktatas";
    private const string funkcio_Erkeztetes = "KuldemenyNew";

    private UtilityTomegesIktatas _util;

    #region Page methods

    protected void Page_Init(object sender, EventArgs e)
    {
        // Funkciójog-ellenőrzések: (Érkeztetés + Bejövő irat iktatás)
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_Erkeztetes);
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_BejovoIratIktatas);

        _util = new UtilityTomegesIktatas(Page, ErrorPanel1);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (ErrorPanel1.Visible)
            {
                ErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
            if (ResultPanel.Visible)
            {
                ResultPanel.Visible = false;
            }
        }

        #region Eredmény fájl letöltése

        string command = Request.QueryString.Get(QueryStringVars.Command);
        if (command == "DownloadResult")
        {
            string tempDirName = Request.QueryString.Get("TempDir");
            string tempFileName = Request.QueryString.Get("TempFile");

            if (!String.IsNullOrEmpty(tempDirName)
                && !String.IsNullOrEmpty(tempFileName)
                && tempFileName.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase))
            {
                _util.DownloadResultFile(tempDirName, tempFileName);
            }
        }

        #endregion
    }

    #endregion

    #region Import sablon fájl előállítása

    protected void btnGetImportSablon_Click(object sender, EventArgs e)
    {
        try
        {
            bool ignoreOutputColumns = true;
            byte[] excelHeaderContent = EgyszerusitettIktatasAdatok.GetExcleHeaderContent(ignoreOutputColumns);
			_util.DownloadExcel("BejovoTomegesIktatasSablon.xlsx", excelHeaderContent);
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader, exc.Message);
        }
    }

    #endregion

    protected void linkResultLog_Click(object sender, EventArgs e)
    {
        _util.DownloadLog(txtBoxIktatasResult.Text);
    }

    #region Tömeges iktatás

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        try
        {
            if (FileUpload1.HasFile)
            {
                //if (FileUpload1.PostedFile.ContentType != "text/xml" && FileUpload1.PostedFile.ContentType != "application/octet-stream")
                //{
                //    ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                //        "Csak xml vagy dbf file tölthető fel.");
                //    return;
                //}

                //// Fájl lementése a temp mappába:
                //string uploadedFilePath = this.SaveUploadedFileToTempDir();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                TomegesIktatasCsv tomegesIktatas = new TomegesIktatasCsv(FileUpload1.FileBytes, execParam, this.Session, this.Cache, _util, new EgyszerusitettIktatasAdatokCsvMap());
                // Feldolgozás és importálás (iktatások) indítása:
                var bgItem = tomegesIktatas.StartImportOnBackground();

                // Hiddenfieldbe letesszük a processId-t, hogy majd lehessen kérdezgetni a folyamat állapotát:
                this.hfBackgroundProcessId.Value = bgItem.BackgroundProcessId.ToString();

                this.SetResultPanel(tomegesIktatas);

                // Javascriptes folyamatos státuszellenőrzés indítása:
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "jsStartProcessStatusCheck", "TomegesIktatas.startStatusCheck();", true);
            }
            else
            {
                throw new ResultException("Nincs fájl kiválasztva!");
            }
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader, exc.Message);
        }
    }

    public void SetResultPanel(TomegesIktatasCsv tomegesIktatas)
    {
        this.txtBoxIktatasResult.Text = "1. sor: Fejléc" + Environment.NewLine;

        //foreach (var iktatasAdatok in tomegesIktatas.IktatasAdatokList)
        //{
        //    this.txtBoxIktatasResult.Text += iktatasAdatok.RowNumber + ". sor: ";
        //    if (!String.IsNullOrEmpty(iktatasAdatok.IktatasHibaUzenet))
        //    {
        //        this.txtBoxIktatasResult.Text += "Hiba: " + iktatasAdatok.IktatasHibaUzenet;
        //    }
        //    else if (iktatasAdatok.MarIktatvaVolt)
        //    {
        //        this.txtBoxIktatasResult.Text += String.Format("Nem kellett iktatni ({0})", iktatasAdatok.ImportRowObj.ResultIktatoszam);
        //    }
        //    else
        //    {
        //        this.txtBoxIktatasResult.Text += String.Format("OK, irat iktatószáma: {0}", iktatasAdatok.ImportRowObj.ResultIktatoszam);
        //    }

        //    // Csatolmány feltöltés hiba:
        //    if (!String.IsNullOrEmpty(iktatasAdatok.CsatolmanyUploadHibaUzenet))
        //    {
        //        this.txtBoxIktatasResult.Text += " Csatolmány feltöltés hiba: " + iktatasAdatok.CsatolmanyUploadHibaUzenet;
        //    }
        //    // Csatolmány feltöltés státuszok:
        //    else if (!String.IsNullOrEmpty(iktatasAdatok.ImportRowObj.CsatolmanyUploadStatus))
        //    {
        //        this.txtBoxIktatasResult.Text += " Csatolmány státuszok: " + iktatasAdatok.ImportRowObj.CsatolmanyUploadStatus;
        //    }

        //    // Soremelés:
        //    this.txtBoxIktatasResult.Text += Environment.NewLine;
        //}

        //// Eredmény fájl link:
        //this.linkResultCsv.NavigateUrl = String.Format("TomegesIktatasBejovo.aspx?Command={0}&TempDir={1}&TempFile={2}"
        //                , "DownloadResult", tomegesIktatas.ResultTempDirName, tomegesIktatas.ResultTempFileName);

        this.ShowResultPanel();
    }

    public void ShowResultPanel()
    {
        this.ResultPanel.Visible = true;

        // A főpanelt elrejtjük, illetve a gombok láthatóságát is állítjuk:
        this.EFormPanel1.Visible = false;
        this.ImageOk.Visible = false;
        this.ImageButtonBack.Visible = true;
    }
    #endregion

    #region Háttérszálas működés

    [System.Web.Services.WebMethod]
    public static TomegesIktatasStatusz GetTomegesIktatasStatusz(string processId, string lastStatusGetTime)
    {
        return UtilityTomegesIktatas.GetTomegesIktatasStatusz(processId, lastStatusGetTime);
    }
    #endregion
}

#region CSV segéd

public class TomegesIktatasCsv : TomegesIktatasCsvBase
{
    private EgyszerusitettIktatasAdatokCsvMap _egyszerusitettIktatasAdatokCsvMap;

    private TomegesIktatasBackgroundItem _tomegesIktatasBackgroundItem;

    #region Constructor

    //public TomegesIktatasCsv(string uploadedFilePath)
    //{
    //    this._uploadedFilePath = uploadedFilePath;
    //}

    public TomegesIktatasCsv(byte[] csvFileContent, ExecParam execParam, System.Web.SessionState.HttpSessionState session
            , System.Web.Caching.Cache cache, UtilityTomegesIktatas util, EgyszerusitettIktatasAdatokCsvMap egyszerusitettIktatasAdatokCsvMap)
    {
        this._csvFileContent = csvFileContent;
        this._execParam = execParam;
        this._util = util;
        this._egyszerusitettIktatasAdatokCsvMap = egyszerusitettIktatasAdatokCsvMap;
        this.Session = session;
        this.Cache = cache;
    }

    #endregion

    #region Public methods

    protected override void SetUgyintezesKezdete(EgyszerusitettIktatasAdatokBase impBase, EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        var imp = impBase as EgyszerusitettIktatasAdatok;

        // Beérkezés ideje:
        if (imp != null && !String.IsNullOrEmpty(imp.BeerkezesIdopontja))
        {
            DateTime beerkezesIdopontja;
            if (DateTime.TryParse(imp.BeerkezesIdopontja, out beerkezesIdopontja))
            {
                erec_UgyUgyiratok.UgyintezesKezdete = beerkezesIdopontja.ToString();
                erec_UgyUgyiratok.Updated.UgyintezesKezdete = true;
            }
        }
    }

    /// <summary>
    /// Import CSV fájl feldolgozása, és tömeges iktatás indítása - háttérszálon
    /// </summary>
    /// <returns></returns>
    public TomegesIktatasBackgroundItem StartImportOnBackground()
    {
        IktatasAdatokList = new List<IktatasImportCsvItemBase>();

        this._tomegesIktatasBackgroundItem = new TomegesIktatasBackgroundItem(IktatasAdatokList, _helper, _execParam, _priority);
        // Beregisztrálás a cache-be, hogy majd lehessen kérdezgetni a státuszt:
        _tomegesIktatasBackgroundItem.RegisterBackgroundProcess();

        System.Threading.Thread thread = new System.Threading.Thread(
                    new System.Threading.ThreadStart(StartImportDoWork));

        // Háttérszál indítása:
        thread.Start();

        return this._tomegesIktatasBackgroundItem;
    }

    /// <summary>
    /// Import CSV fájl feldolgozásának indítása, majd soronként iktatás hívása
    /// </summary>
    private void StartImportDoWork()
    {
        try
        {
            // CSV fájl sorai objektumként:
            List<EgyszerusitettIktatasAdatok> importRowObjects;
            try
            {
                importRowObjects = this.ProcessImportCsv(this._csvFileContent, false);
            }
            catch (CsvHelper.CsvMissingFieldException)
            {
                /// A beolvasást engedélyezzük az eredmény oszlopokat nem tartalmazó CSV-re is:
                /// (Amiben nincs benne az Iktatószám és Hibaüzenet mező)
                /// 
                importRowObjects = this.ProcessImportCsv(this._csvFileContent, true);
            }

            #region Mezőnév visszafejtések (szöveges nevek alapján Id-k lekérése)

            // Érkeztetőkönyvek:
            FillErkeztetokonyvNevek();

            // Iktatókönyvek:
            FillIktatokonyvNevek();

            // CsoportNevek:
            List<string> csoportNevek = new List<string>();
            FillCsoportNevekAll();

            // Irattári tételszámok:
            //List<String> irattariTetelszamok = new List<string>();
            //irattariTetelszamok.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.IrattariTetelszam))
            //                        .Select(e => e.IrattariTetelszam).Distinct().ToList());
            FillIrattariTetelszamokCache();

            // Előzmény ügyiratok:
            List<string> elozmenyUgyiratNevek = new List<string>();
            elozmenyUgyiratNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.ElozmenyUgyirat))
                                    .Select(e => e.ElozmenyUgyirat).Distinct().ToList());
            FillElozmenyUgyiratok(elozmenyUgyiratNevek);

            // Ügytípusok feltöltése:
            FillUgyTipusNevekAll();

            // Partnernevek:
            List<string> partnerNevek = new List<string>();
            // Küldő partner:
            partnerNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.KuldoFeladoNeve))
                                    .Select(e => e.KuldoFeladoNeve).Distinct().ToList());
            // Ügyindító:
            partnerNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.UgyfelUgyindito))
                                    .Select(e => e.UgyfelUgyindito).Distinct().ToList());

            FillPartnerNevekCache(partnerNevek);

            #endregion

            #region Iktatáshoz szükséges objektumok feltöltése, adatok ellenőrzése

            // Csv sorszám (fejléc az 1.)
            int rowNumber = 1;
            foreach (var importRowObj in importRowObjects)
            {
                rowNumber++;

                IktatasImportCsvItem iktatasAdatok = new IktatasImportCsvItem(rowNumber, importRowObj, this._execParam);
                IktatasAdatokList.Add(iktatasAdatok);

                // Ha ki van töltve az iktatószám, akkor erre a sorra nem kell iktatás (max. csatolmány feltöltés)
                if (!String.IsNullOrEmpty(importRowObj.ResultIktatoszam))
                {
                    iktatasAdatok.MarIktatvaVolt = true;
                    continue;
                }

                // Csatolmányok létezésének ellenőrzése:
                iktatasAdatok.CheckCsatolmanyok();

                try
                {
                    // Iktatáshoz szükséges objektumok feltöltése:
                    FillIktatasBusinessObjects(iktatasAdatok, importRowObj);
                }
                catch (Exception exc)
                {
                    Logger.Error(exc.ToString());

                    // A hibaüzenetet kiegészítjük a sorszámmal:
                    throw new ResultException(String.Format("{0} ({1}. sor)", exc.Message, rowNumber));
                }
            }

            #endregion

            this._tomegesIktatasBackgroundItem.StartIktatas(null);

        }
        catch (Exception exc)
        {
            this._tomegesIktatasBackgroundItem.BackgroundProcessError = exc.Message;
        }
        finally
        {
            this._tomegesIktatasBackgroundItem.BackgroundProcessEnded = true;
        }
    }

    #endregion

    #region Private methods

    #region FillIktatasBusinessObjects

    /// <summary>
    /// Az iktatáshoz szükséges objektumok feltöltése
    /// </summary>
    /// <param name="iktatasAdatok"></param>
    private void FillIktatasBusinessObjects(IktatasImportCsvItem iktatasAdatok, EgyszerusitettIktatasAdatok importRowObj)
    {
        // Előzmény ügyirat van-e:        
        Guid? elozmenyUgyiratId = null;
        if (!String.IsNullOrEmpty(importRowObj.ElozmenyUgyirat))
        {
            if (_elozmenyUgyiratAzonDict.ContainsKey(importRowObj.ElozmenyUgyirat))
            {
                elozmenyUgyiratId = _elozmenyUgyiratAzonDict[importRowObj.ElozmenyUgyirat];
            }
            else
            {
                // Hiba:
                throw new ResultException(String.Format("Nem található a megadott előzmény ügyirat: {0}", importRowObj.ElozmenyUgyirat));
            }
        }

        iktatasAdatok.KuldemenyObj = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek(importRowObj);
        iktatasAdatok.UgyiratObj = GetBusinessObjectFromComponents_EREC_UgyUgyiratok(importRowObj);
        iktatasAdatok.IratObj = GetBusinessObjectFromComponents_EREC_IraIratok(importRowObj);

        bool isAlszamraIktatas;
        Guid? iktatokonyvId;

        iktatasAdatok.IktatasiParameterek = GetIktatasiParameterek(importRowObj, elozmenyUgyiratId
            , iktatasAdatok.UgyiratObj.UgyTipus, iktatasAdatok.UgyiratObj.IraIrattariTetel_Id
            , out isAlszamraIktatas, out iktatokonyvId);

        iktatasAdatok.IsAlszamraIktatas = isAlszamraIktatas;
        iktatasAdatok.IktatokonyvId = iktatokonyvId;
        iktatasAdatok.ElozmenyUgyiratId = elozmenyUgyiratId;
    }

    /// <summary>
    /// EREC_KuldKuldemenyek objektum létrehozása és feltöltése (csv egy adatsorából)
    /// </summary>
    /// <returns></returns>
    private EREC_KuldKuldemenyek GetBusinessObjectFromComponents_EREC_KuldKuldemenyek(EgyszerusitettIktatasAdatok importRowObj)
    {
        #region Kötelező mezők ellenőrzése

        if (String.IsNullOrEmpty(importRowObj.BeerkezesModja))
        {
            _util.ThrowRequiredFieldException("Beérkezés módja");
        }

        #endregion

        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        // webservice-en állítva:
        //erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
        //    FelhasznaloProfil.FelhasznaloId(Page));
        //erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;

        // Érkeztetőkönyv kötelező:
        if (String.IsNullOrEmpty(importRowObj.ErkeztetoKonyv))
        {
            throw new ResultException("Érkeztetőkönyv nincs kitöltve!");
        }
        else
        {
            if (this._erkeztetokonyvNevekDict.ContainsKey(importRowObj.ErkeztetoKonyv))
            {
                erec_KuldKuldemenyek.IraIktatokonyv_Id = this._erkeztetokonyvNevekDict[importRowObj.ErkeztetoKonyv].ToString();
                erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = true;
            }
            else
            {
                throw new ResultException(String.Format("Nincs ilyen érkeztetőkönyv: '{0}'", importRowObj.ErkeztetoKonyv));
            }
        }

        if (!String.IsNullOrEmpty(importRowObj.KuldoFeladoNeve))
        {
            if (this._partnerNevekDict.ContainsKey(importRowObj.KuldoFeladoNeve))
            {
                erec_KuldKuldemenyek.Partner_Id_Bekuldo = this._partnerNevekDict[importRowObj.KuldoFeladoNeve].ToString();
                erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = true;
            }
            else
            {
                // Ha nincs ilyen partner a partnertörzsben, attól még hibát nem dobunk, szövegesen úgyis le lesz tárolva...
            }
        }

        erec_KuldKuldemenyek.NevSTR_Bekuldo = importRowObj.KuldoFeladoNeve;
        erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = true;

        /// TODO: Cim_Id lekérése a cím szövege alapjá, ha az egyértelmű. Ha nem egyértelmű, nem állítjuk be.
        /// 
        //erec_KuldKuldemenyek.Cim_Id = Kuld_CimId_CimekTextBox.Id_HiddenField;
        //erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(Kuld_CimId_CimekTextBox);

        erec_KuldKuldemenyek.CimSTR_Bekuldo = importRowObj.KuldoFeladoCime;
        erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = true;

        //BUG 7067
        //erec_KuldKuldemenyek.KuldesMod = this.GetKodtarKodByKodtarNev(kcs_KULDEMENY_KULDES_MODJA, importRowObj.BeerkezesModja);
        erec_KuldKuldemenyek.KuldesMod = _util.GetKodtarKodByKodtarNev(kcs_KULD_KEZB_MODJA, importRowObj.BeerkezesModja);

        erec_KuldKuldemenyek.Updated.KuldesMod = true;

        erec_KuldKuldemenyek.Targy = importRowObj.IratTargya;
        erec_KuldKuldemenyek.Updated.Targy = true;

        if (String.IsNullOrEmpty(importRowObj.BeerkezesIdopontja))
        {
            importRowObj.BeerkezesIdopontja = DateTime.Now.ToString();
        }

        DateTime beerkezes;
        if (!DateTime.TryParse(importRowObj.BeerkezesIdopontja, out beerkezes))
        {
            throw new FormatException("Az érkeztetés időpontjának megadott érték nem megfelelő formátumú!");
        }
        if (beerkezes != DateTime.MinValue && beerkezes > DateTime.Now)
        {
            throw new FormatException("Az érkeztetés időpontja nem lehet a jelenlegi időpontnál későbbi!");
        }

        erec_KuldKuldemenyek.BeerkezesIdeje = beerkezes.ToString();
        erec_KuldKuldemenyek.Updated.BeerkezesIdeje = true;

        // Érkeztetés ideje
        erec_KuldKuldemenyek.Base.LetrehozasIdo = importRowObj.ErkeztetesIdopontja;
        erec_KuldKuldemenyek.Base.Updated.LetrehozasIdo = true;

        erec_KuldKuldemenyek.HivatkozasiSzam = importRowObj.HivatkozasiSzam;
        erec_KuldKuldemenyek.Updated.HivatkozasiSzam = true;

        erec_KuldKuldemenyek.AdathordozoTipusa = _util.GetKodtarKodByKodtarNev(kcs_UGYINTEZES_ALAPJA, importRowObj.KuldemenyTipusa);
        erec_KuldKuldemenyek.Updated.AdathordozoTipusa = true;

        erec_KuldKuldemenyek.UgyintezesModja = _util.GetKodtarKodByKodtarNev(kcs_ELSODLEGES_ADATHORDOZO, importRowObj.ElsodlegesAdathordozoTipusa);
        erec_KuldKuldemenyek.Updated.UgyintezesModja = true;

        erec_KuldKuldemenyek.RagSzam = importRowObj.PostaiAzonosito;
        erec_KuldKuldemenyek.Updated.RagSzam = true;

        if (!String.IsNullOrEmpty(importRowObj.CimzettNeve))
        {
            if (this._csoportNevekDict.ContainsKey(importRowObj.CimzettNeve))
            {
                erec_KuldKuldemenyek.Csoport_Id_Cimzett = this._csoportNevekDict[importRowObj.CimzettNeve].ToString();
                erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = true;
            }
            else
            {
                // Nincs ilyen nevű csoport:
                throw new ResultException(String.Format("Nincs ilyen nevű csoport: '{0}'", importRowObj.CimzettNeve));
            }
        }

        // Ha nincs kitöltve, állítsuk Normál-ra:
        if (String.IsNullOrEmpty(importRowObj.KezbesitesPrioritasa))
        {
            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
        }
        erec_KuldKuldemenyek.Surgosseg = _util.GetKodtarKodByKodtarNev(kcs_SURGOSSEG, importRowObj.KezbesitesPrioritasa);
        erec_KuldKuldemenyek.Updated.Surgosseg = true;

        erec_KuldKuldemenyek.BarCode = importRowObj.Vonalkod;
        erec_KuldKuldemenyek.Updated.BarCode = true;

        erec_KuldKuldemenyek.BoritoTipus = _util.GetKodtarKodByKodtarNev(kcs_BoritoTipus, importRowObj.BoritoTipus);
        erec_KuldKuldemenyek.Updated.BoritoTipus = true;

        erec_KuldKuldemenyek.IktatastNemIgenyel = "1";
        erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = true;

        erec_KuldKuldemenyek.KezbesitesModja = _util.GetKodtarKodByKodtarNev(kcs_KULD_KEZB_MODJA, importRowObj.KezbesitesModja);
        erec_KuldKuldemenyek.Updated.KezbesitesModja = true;

        erec_KuldKuldemenyek.CimzesTipusa = _util.GetKodtarKodByKodtarNev(kcs_KULD_CIMZES_TIPUS, importRowObj.CimzesTipusa);
        erec_KuldKuldemenyek.Updated.CimzesTipusa = true;

        erec_KuldKuldemenyek.Munkaallomas = this._execParam.UserHostAddress;
        erec_KuldKuldemenyek.Updated.Munkaallomas = true;

        erec_KuldKuldemenyek.SerultKuldemeny = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.SerultKuldemeny);
        erec_KuldKuldemenyek.Updated.SerultKuldemeny = true;

        erec_KuldKuldemenyek.TevesCimzes = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.TevesCimzes);
        erec_KuldKuldemenyek.Updated.TevesCimzes = true;

        erec_KuldKuldemenyek.TevesErkeztetes = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.TevesErkeztetes);
        erec_KuldKuldemenyek.Updated.TevesErkeztetes = true;

        string boritoTipusKod = _util.GetKodtarKodByKodtarNev(kcs_BoritoTipus, importRowObj.BoritoTipus);
        switch (boritoTipusKod)
        {
            case KodTarak.BoritoTipus.Boritek:
                erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                break;
            case KodTarak.BoritoTipus.Irat:
                erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                break;
            case KodTarak.BoritoTipus.EldobhatoBoritek:
                erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.No;
                break;
        }
        erec_KuldKuldemenyek.Updated.MegorzesJelzo = true;

        erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
        erec_KuldKuldemenyek.Updated.IktatniKell = true;

        if (string.IsNullOrEmpty(importRowObj.BontasIdopontja))
        {
            importRowObj.BontasIdopontja = DateTime.Now.ToString();
        }

        DateTime felbontas;
        if (!DateTime.TryParse(importRowObj.BontasIdopontja, out felbontas))
        {
            throw new FormatException("A bontás időpontjának megadott érték nem megfelelő formátumú!!");
        }
        if (beerkezes > DateTime.Now)
        {
            throw new FormatException("Az érkeztetés időpontja nem lehet a jelenlegi időpontnál későbbi!");
        }
        if (felbontas > DateTime.Now)
        {
            throw new FormatException("A bontás időpontja nem lehet a jelenlegi időpontnál későbbi!");
        }
        if (felbontas < beerkezes)
        {
            throw new FormatException("A bontás időpontja nem lehet a beérkezés időpontjánál korábbi!");
        }

        erec_KuldKuldemenyek.FelbontasDatuma = felbontas.ToString();
        erec_KuldKuldemenyek.Updated.FelbontasDatuma = true;

        if (!String.IsNullOrEmpty(importRowObj.Bonto))
        {
            if (this._csoportNevekDict.ContainsKey(importRowObj.Bonto))
            {
                erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto = this._csoportNevekDict[importRowObj.Bonto].ToString();
                erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Bonto = true;
            }
            else
            {
                // Nincs ilyen nevű fehasználó/csoport:
                throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", importRowObj.Bonto));
            }
        }

        erec_KuldKuldemenyek.BontasiMegjegyzes = importRowObj.BontassalKapcsolatosMegjegyzes;
        erec_KuldKuldemenyek.Updated.BontasiMegjegyzes = true;

        erec_KuldKuldemenyek.Csoport_Id_Felelos = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(this.Session));
        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

        // WS-en állítva:
        //erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
        //erec_KuldKuldemenyek.Updated.Allapot = true;

        // iratnál beállított minősítést vesszük át
        erec_KuldKuldemenyek.Minosites = _util.GetKodtarKodByKodtarNev(kcs_IRATMINOSITES, importRowObj.IratMinositese);
        erec_KuldKuldemenyek.Updated.Minosites = true;

        //erec_KuldKuldemenyek.Base.Ver = Record_Ver_HiddenField.Value;
        //erec_KuldKuldemenyek.Base.Updated.Ver = true;

        // Áttéve a webservice-be:
        //erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        //erec_KuldKuldemenyek.Updated.PostazasIranya = true;

        erec_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
        erec_KuldKuldemenyek.Updated.Erkeztetes_Ev = true;

        // TODO: nem tudjuk miert kellenek, de kotelezoek
        erec_KuldKuldemenyek.PeldanySzam = "1";
        erec_KuldKuldemenyek.Updated.PeldanySzam = true;

        return erec_KuldKuldemenyek;
    }

    private EREC_IraIratok GetBusinessObjectFromComponents_EREC_IraIratok(EgyszerusitettIktatasAdatok importRowObj)
    {
        #region Kötelező mezők ellenőrzése

        if (String.IsNullOrEmpty(importRowObj.IratTargya))
        {
            _util.ThrowRequiredFieldException("Irat tárgya");
        }

        #endregion

        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        erec_IraIratok.Targy = importRowObj.IratTargya;
        erec_IraIratok.Updated.Targy = true;

        erec_IraIratok.Irattipus = _util.GetKodtarKodByKodtarNev(kcs_IRATTIPUS, importRowObj.IratTipus);
        erec_IraIratok.Updated.Irattipus = true;

        // TODO: nincs kint a felületen, kell ez?
        //erec_IraIratok.KiadmanyozniKell = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.ki (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";
        //erec_IraIratok.Updated.KiadmanyozniKell = pageView.GetUpdatedByView(IraIrat_KiadmanyozniKell_CheckBox);

        erec_IraIratok.Minosites = _util.GetKodtarKodByKodtarNev(kcs_IRATMINOSITES, importRowObj.IratMinositese);
        erec_IraIratok.Updated.Minosites = true;

        if (!String.IsNullOrEmpty(importRowObj.IratUgyintezo))
        {
            if (this._csoportNevekDict.ContainsKey(importRowObj.IratUgyintezo))
            {
                erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = this._csoportNevekDict[importRowObj.IratUgyintezo].ToString();
                erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
            }
            else
            {
                // Nincs ilyen nevű fehasználó/csoport:
                throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", importRowObj.IratUgyintezo));
            }
        }

        /// Irat intézési idejét csv-ben így kell megadni pl.: "22 nap"
        /// --> szétbontjuk két részre: IntezesiIdo + IntezesiIdoegyseg
        /// 

        if (!String.IsNullOrEmpty(importRowObj.IratUgyintezesiIdeje))
        {
            string[] intezesiIdoParts = importRowObj.IratUgyintezesiIdeje.Split(' ');
            string intezesiIdoStr = intezesiIdoParts[0];
            string intezesiIdoEgysegStr = (intezesiIdoParts.Length > 1) ? intezesiIdoParts[1] : String.Empty;

            erec_IraIratok.IntezesiIdo = _util.GetKodtarKodByKodtarNev(kcs_UGYIRAT_INTEZESI_IDO, intezesiIdoStr);
            erec_IraIratok.Updated.IntezesiIdo = true;

            if (!String.IsNullOrEmpty(intezesiIdoEgysegStr))
            {
                erec_IraIratok.IntezesiIdoegyseg = _util.GetKodtarKodByKodtarNev(kcs_IDOEGYSEG, intezesiIdoEgysegStr);
                erec_IraIratok.Updated.IntezesiIdoegyseg = true;
            }
        }

        if (!String.IsNullOrEmpty(importRowObj.IratUgyintezesiHatarideje))
        {
            DateTime dat;
            if (DateTime.TryParse(importRowObj.IratUgyintezesiHatarideje, out dat))
            {
                erec_IraIratok.Hatarido = dat.ToString();
                erec_IraIratok.Updated.Hatarido = true;
            }
            else
            {
                throw new ResultException(String.Format("A megadott 'Irat ügyintézési határideje' nem megfelelő formátumú: '{0}'", importRowObj.IratUgyintezesiHatarideje));
            }
        }

        erec_IraIratok.AdathordozoTipusa = _util.GetKodtarKodByKodtarNev(kcs_UGYINTEZES_ALAPJA, importRowObj.KuldemenyTipusa);
        erec_IraIratok.Updated.AdathordozoTipusa = true;

        // Ez már le van véve felületről:
        //erec_IraIratok.Jelleg = IratJellegKodtarakDropDown.SelectedValue;
        //erec_IraIratok.Updated.Jelleg = true;

        // nekrisz : munkaállomás mentése
        erec_IraIratok.Munkaallomas = this._execParam.UserHostAddress;
        erec_IraIratok.Updated.Munkaallomas = true;

        return erec_IraIratok;
    }

    private IktatasiParameterek GetIktatasiParameterek(EgyszerusitettIktatasAdatok importRowObj
                , Guid? elozmenyUgyiratId, string ugytipus, string irattariTetelszamId
                , out bool isAlszamraIktatas, out Guid? iktatoKonyvId)
    {
        var iktatasiParameterek = CreateIktatasiParameterek(importRowObj, elozmenyUgyiratId, ugytipus, irattariTetelszamId, out isAlszamraIktatas, out iktatoKonyvId);

        // Adószám:
        iktatasiParameterek.Adoszam = importRowObj.KuldoAdoszama;

        return iktatasiParameterek;
    }

    #endregion

    /// <summary>
    /// A feltöltött import csv fájl feldolgozása
    /// </summary>
    /// <param name="uploadedFilePath"></param>
    /// <returns>A csv fájlból beolvasott 'EgyszerusitettIktatasAdatok' rekordok</returns>
    private List<EgyszerusitettIktatasAdatok> ProcessImportCsv(byte[] csvFileContent, bool ignoreOutputColumns)
    {
        var classMap = this._egyszerusitettIktatasAdatokCsvMap; // new EgyszerusitettIktatasAdatokCsvMap();

        if (ignoreOutputColumns)
        {
            // Kikapcsoljuk a beolvasásból az eredmény oszlopokat:
            classMap.Map(e => e.ResultHibaUzenet).Ignore();
            classMap.Map(e => e.ResultIktatoszam).Ignore();
            classMap.Map(e => e.CsatolmanyUploadStatus).Ignore();
        }

        using (MemoryStream memStream = new MemoryStream(csvFileContent))
        using (var package = new ExcelPackage(memStream))
        {
            ExcelWorksheet ws = package.Workbook.Worksheets.First();

            var items = ws.ConvertSheetToObjects<EgyszerusitettIktatasAdatok>(classMap);

            return items.ToList();
        }
    }

    #endregion
}

/// <summary>
/// Egyszerűsített iktatás (érkeztetés+iktatás) képernyő adatai (Ezek a mezők lesznek az import CSV fájlban.) 
/// </summary>
public class EgyszerusitettIktatasAdatok : EgyszerusitettIktatasAdatokBase
{
    #region Properties

    #region Küldemény adatok

    public string Vonalkod { get; set; }
    public string BoritoTipus { get; set; }
    public string ErkeztetoKonyv { get; set; }
    public string KuldoAdoszama { get; set; }
    public string KuldoFeladoNeve { get; set; }
    public string KuldoFeladoCime { get; set; }
    public string BeerkezesModja { get; set; }
    public string KezbesitesModja { get; set; }
    public string BeerkezesIdopontja { get; set; }
    public string ErkeztetesIdopontja { get; set; }
    public string Bonto { get; set; }
    public string BontasIdopontja { get; set; }
    public string HivatkozasiSzam { get; set; }
    public string PostaiAzonosito { get; set; }
    public string KuldemenyTipusa { get; set; }
    public string BontassalKapcsolatosMegjegyzes { get; set; }
    public string CimzettNeve { get; set; }
    public string KezbesitesPrioritasa { get; set; }
    public string CimzesTipusa { get; set; }
    public string IktatastIgenyel { get; set; }
    public string SerultKuldemeny { get; set; }
    public string TevesCimzes { get; set; }
    public string TevesErkeztetes { get; set; }

    #endregion

    #endregion

    /// <summary>
    /// Fejlécet tartalmazó csv fájl tartalom
    /// </summary>
    /// <param name="ignoreResultColumns">Az output oszlopokat kihagyja-e (pl. Iktatószám, Hibaüzenet)</param>
    public static byte[] GetExcleHeaderContent(bool ignoreOutputColumns)
    {
        var classMap = new EgyszerusitettIktatasAdatokCsvMap();

        if (ignoreOutputColumns)
        {
            classMap.Map(e => e.ResultHibaUzenet).Ignore();
            classMap.Map(e => e.ResultIktatoszam).Ignore();
            classMap.Map(e => e.CsatolmanyUploadStatus).Ignore();
        }

        using (var p = new ExcelPackage())
        {
            var ws = p.Workbook.Worksheets.Add("Iktatas");
            ws.WriteHeader<EgyszerusitettIktatasAdatok>(classMap);
            byte[] csvContent = p.GetAsByteArray();
            return csvContent;
        }
    }
}

public class EgyszerusitettIktatasAdatokCsvMap : CsvHelper.Configuration.CsvClassMap<EgyszerusitettIktatasAdatok>
{
    public EgyszerusitettIktatasAdatokCsvMap()
    {
        AutoMap(false, false);

        Map(m => m.ResultIktatoszam).Name("Iktatószám");
        Map(m => m.ResultHibaUzenet).Name("Hibaüzenet");

        #region Küldemény adatok

        Map(m => m.Vonalkod).Name("Vonalkód");
        // BUG#4742:
        string colNameBoritoTipusa = ForditasExpressionBuilder.GetForditas("labelBoritoTipus", "Borító típusa", "~/EgyszerusitettIktatasForm.aspx")
                // A végén van egy : karakter a szótárban lévő szövegben, azt levesszük:
                    .TrimEnd(':');
        Map(m => m.BoritoTipus).Name(colNameBoritoTipusa);
        Map(m => m.ErkeztetoKonyv).Name("Érkeztetőkönyv");
        Map(m => m.KuldoAdoszama).Name("Küldő adószáma");
        Map(m => m.KuldoFeladoNeve).Name("Küldő/feladó neve");
        Map(m => m.KuldoFeladoCime).Name("Küldő/feladó címe");
        Map(m => m.BeerkezesModja).Name("Beérkezés módja");
        Map(m => m.KezbesitesModja).Name("Kézbesítés módja");
        Map(m => m.BeerkezesIdopontja).Name("Beérkezés időpontja");
        Map(m => m.ErkeztetesIdopontja).Name("Érkeztetés időpontja");
        Map(m => m.Bonto).Name("Bontó");
        Map(m => m.BontasIdopontja).Name("Bontás időpontja");
        Map(m => m.HivatkozasiSzam).Name("Hivatkozási szám");
        Map(m => m.PostaiAzonosito).Name("Postai azonosító");
        Map(m => m.KuldemenyTipusa).Name("Küldemény típusa");
        Map(m => m.BontassalKapcsolatosMegjegyzes).Name("Bontással kapcsolatos megjegyzés");
        Map(m => m.ElsodlegesAdathordozoTipusa).Name("Elsődleges adathordozó típusa");
        Map(m => m.CimzettNeve).Name("Címzett neve");
        Map(m => m.KezbesitesPrioritasa).Name("Kézbesítés prioritása");
        Map(m => m.CimzesTipusa).Name("Címzés típusa");
        Map(m => m.IktatastIgenyel).Name("Iktatást igényel?");
        Map(m => m.SerultKuldemeny).Name("Sérült küldemény?");
        Map(m => m.TevesCimzes).Name("Téves címzés?");
        Map(m => m.TevesErkeztetes).Name("Téves érkeztetés?");

        #endregion

        #region Ügyirat adatok

        Map(m => m.ElozmenyUgyirat).Name("Előzmény ügyirat");
        //Map(m => m.ElozmenyUgyiratEv).Name("Év (Előzmény ügyirat)");
        //Map(m => m.ElozmenyUgyiratIktatokonyv).Name("Iktatókönyv (Előzmény ügyirat)");
        //Map(m => m.ElozmenyUgyiratFoszam).Name("Főszám (Előzmény ügyirat)");
        Map(m => m.UgyFajtaja).Name("Ügy fajtája");
        Map(m => m.IrattariTetelszam).Name("Irattári tételszám");
        Map(m => m.Iktatokonyv).Name("Iktatókönyv");
        Map(m => m.UgyTipusa).Name("Ügy típusa");
        Map(m => m.UgyiratTargya).Name("Ügyirat tárgya");
        Map(m => m.UgyiratUgyintezesiIdeje).Name("Ügyirat ügyintézési ideje");
        Map(m => m.UgyiratUgyintezesiHatarideje).Name("Ügyirat ügyintézési határideje");
        Map(m => m.SzervezetiEgyseg).Name("Szervezeti egység");
        Map(m => m.UgyiratUgyintezo).Name("Ügyirat ügyintéző");
        Map(m => m.Kezelo).Name("Kezelő");
        Map(m => m.UgyfelUgyindito).Name("Ügyfél, ügyindító");
        Map(m => m.UgyinditoCime).Name("Ügyindító címe");

        #endregion

        #region Irat adatok

        Map(m => m.IratTargya).Name("Irat tárgya");
        Map(m => m.IratTipus).Name("Irat típus");
        Map(m => m.IratMinositese).Name("Irat minősítése");
        Map(m => m.IratUgyintezo).Name("Irat ügyintéző");
        Map(m => m.IratUgyintezesiIdeje).Name("Irat ügyintézési ideje");
        Map(m => m.IratUgyintezesiHatarideje).Name("Irat ügyintézési határideje");

        #endregion

        Map(m => m.CsatolmanyUrls).Name("Csatolmány URL");
        Map(m => m.CsatolmanyUploadStatus).Name("Csatolmány feltöltés eredmény");
    }
}

/// <summary>
/// A tömeges iktatásra feltöltött import fájl egy eleme
/// </summary>
public class IktatasImportCsvItem : IktatasImportCsvItemBase
{
    #region Properties

    public EREC_KuldKuldemenyek KuldemenyObj { get; set; }

    public bool IktatasInProgress { get; set; }

    #endregion

    #region Constructor

    public IktatasImportCsvItem(int rowNumber, EgyszerusitettIktatasAdatok importRowObj, ExecParam execParam)
    {
        this.RowNumber = rowNumber;
        this.ImportRowObj = importRowObj;
        this.ExecParam = execParam;
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Iktatás indítása
    /// </summary>
    public void CallIktatasService()
    {
        // Ha ki van töltve az iktatószám, akkor erre a sorra nem kell iktatás:
        if (!String.IsNullOrEmpty(ImportRowObj.ResultIktatoszam))
        {
            Logger.Debug(String.Format("{0}. sor: Van iktatószám, nem kell iktatni.", this.RowNumber));

            return;
        }

        try
        {
            this.IktatasInProgress = true;

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            // TODO:
            EREC_HataridosFeladatok erec_HataridosFeladatok = null; //FeljegyzesPanel.GetBusinessObject();

            ErkeztetesParameterek ep = new ErkeztetesParameterek();
            //ep.Mellekletek = mellekletekPanel.GetMellekletek();

            Result result_iktatas;

            if (IsAlszamraIktatas)
            {
                result_iktatas = service.EgyszerusitettIktatasa_Alszamra(this.ExecParam, (this.IktatokonyvId != null) ? this.IktatokonyvId.ToString() : String.Empty
                    , this.ElozmenyUgyiratId != null ? this.ElozmenyUgyiratId.ToString() : String.Empty
                    , new EREC_UgyUgyiratdarabok(), this.IratObj, erec_HataridosFeladatok, this.KuldemenyObj
                    , this.IktatasiParameterek, ep);
            }
            else
            {
                // Normál iktatás
                result_iktatas = service.EgyszerusitettIktatasa(this.ExecParam, (this.IktatokonyvId != null) ? this.IktatokonyvId.ToString() : String.Empty
                        , this.UgyiratObj, new EREC_UgyUgyiratdarabok()
                        , this.IratObj, erec_HataridosFeladatok, this.KuldemenyObj, this.IktatasiParameterek, ep);
            }

            //this.IktatasResult = result_iktatas;

            if (result_iktatas.IsError)
            {
                this.IktatasHibaUzenet = Contentum.eUtility.ResultError.GetErrorMessageFromResultObject(result_iktatas);
            }
            else
            {
                // Iktatószám visszaírása a CSV objektumba:
                ErkeztetesIktatasResult iktatasResultObj = result_iktatas.Record as ErkeztetesIktatasResult;
                if (iktatasResultObj == null)
                {
                    // Valami hiba van:
                    this.ImportRowObj.ResultIktatoszam = "??";
                    this.IktatasHibaUzenet = "Iktatás OK, Iktatószám: ??";
                }
                else
                {
                    this.ImportRowObj.ResultIktatoszam = iktatasResultObj.IratAzonosito;
                }
            }

            this.IktatasCompleted = true;
        }
        catch (Exception exc)
        {
            this.IktatasCompleted = true;

            this.IktatasHibaUzenet = exc.Message;
        }

        // Hibaüzenet visszaírása a CSV objektumba:
        this.ImportRowObj.ResultHibaUzenet = this.IktatasHibaUzenet;
    }

    #endregion
}

/// <summary>
/// A háttérszálon futtatott iktatási adatok
/// </summary>
public class TomegesIktatasBackgroundItem : TomegesIktatasBackgroundItemBase
{
    public TomegesIktatasBackgroundItem(List<IktatasImportCsvItemBase> iktatasAdatokList, TomegesIktatasHelper helper,
             ExecParam execParam, int prio) : base(iktatasAdatokList, helper, execParam, prio)
    {
    }

    /// <summary>
    /// Iktatás (és csatolmány feltöltés) indítása
    /// </summary>
    public override void StartIktatas(byte[] csvContent)
    {
        try
        {
            #region Iktatás

            this.Logs.Add(new LogItem("Iktatás elkezdődött..."));

            // Iktatás hívása soronként:
            foreach (var iktatasAdatok in IktatasAdatokList.Cast<IktatasImportCsvItem>())
            {
                iktatasAdatok.CallIktatasService();

                string logMsg = iktatasAdatok.GetIktatasLogMsg();
                this.Logs.Add(new LogItem(logMsg));
            }

            #endregion

            #region Csatolmányok feltöltése

            this.Logs.Add(new LogItem("Csatolmányok feltöltése elkezdődött..."));

            // Csatolmányfeltöltés hívása soronként:
            foreach (var iktatasAdatok in IktatasAdatokList)
            {
                iktatasAdatok.CallCsatolmanyFeltoltes();

                string logMsg = iktatasAdatok.GetCsatolmanyFeltoltesLogMsg();
                this.Logs.Add(new LogItem(logMsg));
            }

            #endregion

            Finish("TomegesIktatasBejovo");
        }
        catch (Exception exc)
        {
            Logger.Error(exc.ToString());
            this.BackgroundProcessError = exc.Message;
        }
        finally
        {
            this.BackgroundProcessEnded = true;
        }
    }

    /// <summary>
    /// Eredmény CSV fájl tartalma
    /// </summary>
    public override byte[] GetResultCsvContent()
    {
        var recordsToWrite = this.IktatasAdatokList.Select(e => e.ImportRowObj as EgyszerusitettIktatasAdatok).ToList();
        var classMap = new EgyszerusitettIktatasAdatokCsvMap();

        using (var p = new ExcelPackage())
        {
            var ws = p.Workbook.Worksheets.Add("Iktatas");
            ws.WriteHeader<EgyszerusitettIktatasAdatok>(classMap);
            ws.Cells[2, 1].LoadFromCollection(recordsToWrite);
            byte[] csvContent = p.GetAsByteArray();
            return csvContent;
        }
    }
}

#endregion
