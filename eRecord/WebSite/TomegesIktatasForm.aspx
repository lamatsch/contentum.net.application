<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TomegesIktatasForm.aspx.cs" Inherits="TomegesIktatasForm" %>


<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc15" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,TomegesIktatasFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxForrasNev" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelFelelosSzervezetReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="LabelFelelosSzervezet" runat="server" Text="Szervezeti egys�g:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc7:CsoportTextBox ID="Felelos_CsoportTextBox" SzervezetCsoport="true"
                                Validate="false" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelAgazatiJelReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="LabelAgazatiJel" runat="server" Text="�gazati jel:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%">
                            </asp:DropDownList>
                            <uc5:RequiredTextBox ID="ViewTextBoxAgazatiJel" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                            <asp:RequiredFieldValidator ID="AgazatiJelek_RequiredFieldValidator" runat="server" ControlToValidate="AgazatiJelek_DropDownList"
                                Display="None" SetFocusOnError="true" Enabled="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" />
                            <ajaxToolkit:ValidatorCalloutExtender
                                ID="AgazatiJelek_ValidatorCalloutExtender" runat="server" TargetControlID="AgazatiJelek_RequiredFieldValidator">
                                <Animations>
                                    <OnShow>
                                    <Sequence>
                                        <HideAction Visible="true" />
                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                    </Sequence>    
                                    </OnShow>
                                </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender>
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelUgykorKodReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelUgykorKod" runat="server" Text="�gyk�r k�d:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%">
                            </asp:DropDownList>
                            <uc5:RequiredTextBox ID="ViewTextBoxUgykor" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                            <asp:RequiredFieldValidator ID="Ugykor_RequiredFieldValidator" runat="server" ControlToValidate="Ugykor_DropDownList"
                                Display="None" SetFocusOnError="true" Enabled="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" />
                            <ajaxToolkit:ValidatorCalloutExtender
                                ID="Ugykor_ValidatorCalloutExtender" runat="server" TargetControlID="Ugykor_RequiredFieldValidator">
                                <Animations>
                                    <OnShow>
                                    <Sequence>
                                        <HideAction Visible="true" />
                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                    </Sequence>    
                                    </OnShow>
                                </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender>
                        </td>
                    </tr>
                    <tr runat="server" class="urlapSor_kicsi">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelUgytipusReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="LabelUgytipus" runat="server" Text="�gyt�pus:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"
                                Width="97%">
                            </asp:DropDownList>
                            <uc5:RequiredTextBox ID="ViewTextBoxUgytipus" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                            <asp:RequiredFieldValidator ID="UgyUgyirat_Ugytipus_RequiredFieldValidator" runat="server" ControlToValidate="UgyUgyirat_Ugytipus_DropDownList"
                                Display="None" SetFocusOnp="true" Enabled="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="UgyUgyirat_Ugytipus_ValidatorCalloutExtender" runat="server"
                                TargetControlID="UgyUgyirat_Ugytipus_RequiredFieldValidator">
                                <Animations>
                                       <OnShow>
                                       <Sequence>
                                           <HideAction Visible="true" />
                                           <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                       </Sequence>    
                                       </OnShow>
                                </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender>
                        </td>
                        <td class="mrUrlapCaption"></td>
                        <td class="mrUrlapMezo"></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelIrattipusReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelIrattipus" runat="server" Text="Iratt�pus:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodtarakDropDownList ID="IraIrat_Irattipus_KodtarakDropDownList" runat="server" Validate="true"></ktddl:KodtarakDropDownList>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="LabelTargyReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelTargy" runat="server" Text="T�rgy:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxTargy" runat="server" />
                        </td>
                    </tr>

                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox runat="server" Text="Al��r�s kell?" Checked="false" AutoPostBack="true" ID="AlairasKellCheckBox" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox runat="server" Text="Automatikus expedi�l�s?" Checked="false" AutoPostBack="true" ID="autoExpedialasCheckBox" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelAdathordozoTipusa">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label25" runat="server" Text="Adathordoz� t�pusa:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" runat="server">
                            <ktddl:KodtarakDropDownList ID="AdathordozoTipusa_KodtarakDropDownList" runat="server" />
                            <uc5:RequiredTextBox ID="AdathordozoTipusaViewTextbox" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="trUgyintezesAlapja">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" Text="�gyint�z�s alapja:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodtarakDropDownList ID="UgyintezesAlapja_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                            <uc5:RequiredTextBox ID="UgyintezesAlapjaViewTextbox" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="trExpedialasModja">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" Text="Expedi�l�s m�dja:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodtarakDropDownList ID="ExpedialasModjaKodtarakDropDownList" runat="server" />
                            <uc5:RequiredTextBox ID="ExpedialasModjaViewTextBox" runat="server" Visible="false" Validate="false" ReadOnly="true" />
                        </td>
                    </tr>
                    <%--<td class="mrUrlapCaption_short">
							<asp:Label ID="Label19" runat="server" Text="Els�dleges adathordoz� t�pusa:"></asp:Label>
						</td>
						<td class="mrUrlapMezo" id="valami" runat="server" width="0%">
							<ktddl:KodtarakDropDownList ID="Pld_UgyintezesModja_KodtarakDropDownList" runat="server" />
						</td>--%>

                    <tr class="urlapSor">
                        <td class="mrUrlapCaption"></td>
                        <td class="mrUrlapMezo">
                            <eUI:eFormPanel ID="AlairasForm" runat="server">

                                <table runat="server">
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAlairoReqStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="labelAlairo" runat="server" Text="Al��r�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc15:FelhasznaloCsoportTextBox ID="Alairo_CsoportTextBox"
                                                runat="server" Validate="true" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelHelyettes" runat="server" Text="Helyettes:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc15:FelhasznaloCsoportTextBox ID="Helyettes_CsoportTextBox"
                                                runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr id="AlairasSzerepSor" visible="true" class="urlapSor_kicsi">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="LabelAlairoSzerepReqStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="LabelAlairoSzerep" runat="server" Text="Al��r� szerepe:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="AlairoSzerep_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAlairasTipusReqStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="labelAlairasTipus" runat="server" Text="Al��r�s t�pus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <asp:DropDownList ID="AlairasTipusDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                                <%-- al��r�st�pus dopdown --%>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="alairasSzabalySor" visible="false" runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAlairasSzabalyReqStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="labelAlairasSzabaly" runat="server" Text="Al��r�s szab�ly:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <asp:DropDownList ID="AlairasSzabalyDropDown" Enabled="false" runat="server">
                                                <%-- al��r�sszab�ly dopdown --%>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox runat="server" Text="Hat�s�gi adatlap kell?" Checked="false" AutoPostBack="true" ID="HatosagiAdatlapKellCheckBox" />
                        </td>
                    </tr>
                    <tr class="urlapSor">

                        <td class="mrUrlapCaption"></td>
                        <td class="mrUrlapMezo">
                            <eUI:eFormPanel ID="HatosagiAdatlapForm" runat="server">
                                <table runat="server">
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUgyFajtaja" runat="server" Text="�gy fajt�ja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc10:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelDontestHozta" runat="server" Text="D�nt�st hozta:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc10:KodtarakDropDownList ID="DONTEST_HOZTA_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelDontesFormaja" runat="server" Text="D�nt�s form�ja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc10:KodtarakDropDownList ID="DONTES_FORMAJA_KodtarakDropDownList" runat="server" Enabled="False" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUGYINTEZES_IDOTARTAMA" runat="server" Text="�gyint�z�s id�tartama:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                            <uc10:KodtarakDropDownList ID="UGYINTEZES_IDOTARTAMA_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="HATARIDO_TULLEPES_felirat" runat="server" Text="Hat�rid�-t�ll�p�s id�tartama napokban:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="HATARIDO_TULLEPES_NumberBox" runat="server" MaxLength="3"
                                                Validate="false" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="HatosagiEllenorzes_Felirat" runat="server" Text="Sor ker�lt-e hat�s�gi ellen�rz�s lefolytat�s�ra az adott �gyben:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="HatosagiEllenorzes_RadioButtonList" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Text="Igen" />
                                                <asp:ListItem Value="0" Text="Nem" />
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="HatosagiEllenorzes_RadioButtonList"
                                                Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender
                                                ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                                </Animations>
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="MunkaorakSzama_Felirat" runat="server" Text="Munka�r�k sz�ma (f�l�ra pontoss�ggal):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <uc8:RequiredNumberBox ID="MunkaorakSzamaOra_NumberBox" runat="server" MaxLength="3"
                                                            Validate="false" Width="50px" RightAlign="True" />
                                                    </td>
                                                    <td style="vertical-align: bottom;">
                                                        <span style="margin-left: 3px; padding-right: 3px; color: Black; font-weight: bold;">,</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="MunkaorakSzamaPerc_DropDownList" runat="server">
                                                            <asp:ListItem Text="0" Value="0" Selected="True" />
                                                            <asp:ListItem Text="5" Value="5" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="EljarasiKoltseg_Felirat" runat="server" Text="Elj�r�si k�lts�g �sszege (Ft-ban):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="EljarasiKoltseg_NumberBox" runat="server"
                                                Validate="false" RightAlign="True" Width="100px" MaxLength="7" FormatNumber="True" NumberMaxLength="6"
                                                FilterType="NumbersWithGroupSeparator" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="KozigazgatasiBirsagMerteke_Felirat" runat="server" Text="K�zigazgat�si b�rs�g �sszege (Ft-ban):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="KozigazgatasiBirsagMerteke_NumberBox" runat="server"
                                                Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                                FilterType="NumbersWithGroupSeparator" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="SommasEljDontes_Felirat" runat="server" Text="Somm�s elj�r�sban hozott d�nt�s:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="SOMMAS_DONTES_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="NyolcnaponBelulNemSommas_Felirat" runat="server" Text="8 napon bel�l lez�rt, nem somm�s elj�r�sban hozott d�nt�s:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="FUGGO_HATALYU_HATAROZAT_Felirat" runat="server" Text="F�gg� hat�ly� hat�rozat:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="FUGGO_HATALYU_VEGZES_Felirat" runat="server" Text="F�gg� hat�ly� v�gz�s:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="FUGGO_HATALYU_VEGZES_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="HatosagAltalVisszafizetettOsszeg_Felirat" runat="server" Text="Hat�s�g �ltal visszafizetett �sszeg (Ft-ban):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="HatosagAltalVisszafizetettOsszeg_NumberBox" runat="server"
                                                Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                                FilterType="NumbersWithGroupSeparator" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="HatosagotTerheloEljKtsg_Felirat" runat="server" Text="Hat�s�got terhel� elj�r�si k�lts�g �sszege (Ft-ban):"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="HatosagotTerheloEljKtsg_NumberBox" runat="server"
                                                Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                                FilterType="NumbersWithGroupSeparator" />
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="FELFUGGESZTETT_HATAROZAT" runat="server" Text="Felf�ggesztett hat�rozat:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textNote" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <eUI:CustomCascadingDropDown ID="CascadingDropDown_AgazatiJel" runat="server" UseContextKey="true"
            ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetAllAgazatiJelek"
            Category="AgazatiJel" Enabled="False" TargetControlID="AgazatiJelek_DropDownList"
            EmptyText="---" EmptyValue="" SelectedValue="" LoadingText="[Felt�lt�s folyamatban...]"
            PromptText="<�gazati jel kiv�laszt�sa>" PromptValue="">
        </eUI:CustomCascadingDropDown>
        <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugykor" runat="server" TargetControlID="Ugykor_DropDownList"
            ParentControlID="AgazatiJelek_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
            ServiceMethod="GetAllUgykorokByAgazatiJel" Category="Ugykor" Enabled="False" EmptyText="---"
            EmptyValue="" SelectedValue="" LoadingText="[Felt�lt�s folyamatban...]" PromptText="<�gyk�r k�d kiv�laszt�sa>"
            PromptValue="">
        </eUI:CustomCascadingDropDown>
        <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="UgyUgyirat_Ugytipus_DropDownList"
            ParentControlID="Ugykor_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
            ServiceMethod="GetUgytipusokByUgykor" Category="Ugytipus" Enabled="False" EmptyText="---"
            EmptyValue="" SelectedValue="" LoadingText="[Felt�lt�s folyamatban...]" PromptText="<�gyt�pus kiv�laszt�sa>"
            PromptValue="">
        </eUI:CustomCascadingDropDown>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>

