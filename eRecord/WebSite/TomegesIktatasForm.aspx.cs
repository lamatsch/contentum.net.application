using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Text;
using System.Data;

public partial class TomegesIktatasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_ALAIRO_SZEREP = "ALAIRO_SZEREP";
    private const string viewState_AlairasModDictionary = "AlairasModDictionary";
    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
    private const string kcs_DONTEST_HOZTA = "DONTEST_HOZTA";
    private const string kcs_DONTES_FORMAJA_ONK = "DONTES_FORMAJA_ONK";
    private const string kcs_DONTES_FORMAJA_ALLAMIG = "DONTES_FORMAJA_ALLAMIG";
    private const string kcs_UGYINTEZES_IDOTARTAMA = "UGYINTEZES_IDOTARTAMA";
    private const string kcs_SOMMAS_DONTES = "SOMMAS_DONTES";
    private const string kcs_NYOLCNAPON_BELUL_NEM_SOMMAS = "8NAPON_BELUL_NEM_SOMMAS";
    private const string kcs_FUGGO_HATALYU_HATAROZAT = "FUGGO_HATALYU_HATAROZAT";
    private const string kcs_FUGGO_HATALYU_VEGZES = "FUGGO_HATALYU_VEGZES";
    private const string kcs_FELFUGGESZTETT_HATAROZAT = "FELFUGGESZTETT_HATAROZAT";

    #region LZS
    private const string kcs_ADATHORDOZO_TIPUSA = "UGYINTEZES_ALAPJA";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";

    #endregion



    #region priv�t met�dusok
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxForrasNev.ReadOnly = true;
        Felelos_CsoportTextBox.ReadOnly = true;

        AgazatiJelek_DropDownList.Visible = false;
        AgazatiJelek_DropDownList.Enabled = false;
        CascadingDropDown_AgazatiJel.Enabled = false;
        Ugykor_DropDownList.Visible = false;
        Ugykor_DropDownList.Enabled = false;
        CascadingDropDown_Ugykor.Enabled = false;
        UgyUgyirat_Ugytipus_DropDownList.Visible = false;
        UgyUgyirat_Ugytipus_DropDownList.Enabled = false;
        CascadingDropDown_Ugytipus.Enabled = false;

        autoExpedialasCheckBox.Enabled = false;
        AdathordozoTipusa_KodtarakDropDownList.Visible = false;
        AdathordozoTipusa_KodtarakDropDownList.Enabled = false;

        UgyintezesAlapja_DropDownList.Visible = false;
        UgyintezesAlapja_DropDownList.Enabled = false;

        ExpedialasModjaKodtarakDropDownList.Visible = false;
        ExpedialasModjaKodtarakDropDownList.Enabled = false;


        ViewTextBoxAgazatiJel.Visible = true;
        ViewTextBoxUgykor.Visible = true;
        ViewTextBoxUgytipus.Visible = true;
        #region LZS
        AdathordozoTipusaViewTextbox.Visible = true;
        AdathordozoTipusaViewTextbox.Visible = true;
        UgyintezesAlapjaViewTextbox.Visible = true;
        ExpedialasModjaViewTextBox.Visible = true;
        #endregion

        using (Contentum.eRecord.Service.EREC_AgazatiJelekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_AgazatiJelekService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = CascadingDropDown_AgazatiJel.SelectedValue;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                ViewTextBoxAgazatiJel.Text = ((EREC_AgazatiJelek)result.Record).Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }

        using (Contentum.eRecord.Service.EREC_IraIrattariTetelekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = CascadingDropDown_Ugykor.SelectedValue;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                ViewTextBoxUgykor.Text = ((EREC_IraIrattariTetelek)result.Record).Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }

        }
        GetUgytipusIdByKod(CascadingDropDown_Ugykor.SelectedValue, CascadingDropDown_Ugytipus.SelectedValue);

        IraIrat_Irattipus_KodtarakDropDownList.ReadOnly = true;
        requiredTextBoxTargy.ReadOnly = true;
        AlairasKellCheckBox.Enabled = false;
        AlairasKellCheckBox.Visible = true;
        HatosagiAdatlapKellCheckBox.Enabled = false;
        HatosagiAdatlapKellCheckBox.Visible = true;
        textNote.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;



    }

    private void SetModifyControls()
    {
        requiredTextBoxForrasNev.Validate = true;
        requiredTextBoxForrasNev.Enabled = true;
        requiredTextBoxForrasNev.Visible = true;
        Felelos_CsoportTextBox.Validate = true;
        Felelos_CsoportTextBox.Enabled = true;
        Felelos_CsoportTextBox.Visible = true;
        AgazatiJelek_DropDownList.Enabled = true;
        AgazatiJelek_DropDownList.Visible = true;
        Ugykor_DropDownList.Enabled = true;
        Ugykor_DropDownList.Visible = true;
        UgyUgyirat_Ugytipus_DropDownList.Enabled = true;
        UgyUgyirat_Ugytipus_DropDownList.Visible = true;

        IraIrat_Irattipus_KodtarakDropDownList.Visible = true;
        IraIrat_Irattipus_KodtarakDropDownList.Enabled = true;
        IraIrat_Irattipus_KodtarakDropDownList.Validate = true;
        requiredTextBoxTargy.Visible = true;
        requiredTextBoxTargy.Enabled = true;
        requiredTextBoxTargy.Validate = true;
        AlairasKellCheckBox.Enabled = true;
        AlairasKellCheckBox.Visible = true;
        HatosagiAdatlapKellCheckBox.Enabled = true;
        HatosagiAdatlapKellCheckBox.Visible = true;
        textNote.ReadOnly = false;

    }
    private void SetAlairasHatosagFormsControls()
    {
        if (Command == CommandName.View)
        {
            if (AlairasKellCheckBox.Checked)
            {
                AlairasForm.Visible = true;
                alairasSzabalySor.Visible = true;

                Alairo_CsoportTextBox.Enabled = false;
                Helyettes_CsoportTextBox.Enabled = false;
                AlairoSzerep_KodtarakDropDownList.Enabled = false;
                AlairasTipusDropDown.Enabled = false;
            }
            else
            {
                AlairasForm.Visible = false;

                Alairo_CsoportTextBox.Validate = false;
                Helyettes_CsoportTextBox.Validate = false;
                AlairoSzerep_KodtarakDropDownList.Validate = false;
                AlairasTipusDropDown.Enabled = false;
            }
            if (HatosagiAdatlapKellCheckBox.Checked)
            {
                HatosagiAdatlapForm.Visible = true;
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
                DONTEST_HOZTA_KodtarakDropDownList.ReadOnly = true;
                DONTES_FORMAJA_KodtarakDropDownList.ReadOnly = true;
                UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.ReadOnly = true;
                HATARIDO_TULLEPES_NumberBox.ReadOnly = true;
                HatosagiEllenorzes_RadioButtonList.Enabled = false;
                MunkaorakSzamaOra_NumberBox.ReadOnly = true;
                MunkaorakSzamaPerc_DropDownList.ReadOnly = true;
                EljarasiKoltseg_NumberBox.ReadOnly = true;
                KozigazgatasiBirsagMerteke_NumberBox.ReadOnly = true;
                SOMMAS_DONTES_KodtarakDropDownList.ReadOnly = true;
                NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.ReadOnly = true;
                FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.ReadOnly = true;
                FUGGO_HATALYU_VEGZES_KodtarakDropDownList.ReadOnly = true;
                HatosagAltalVisszafizetettOsszeg_NumberBox.ReadOnly = true;
                HatosagotTerheloEljKtsg_NumberBox.ReadOnly = true;
                FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.ReadOnly = true;
            }
            else
            {
                HatosagiAdatlapForm.Visible = false;
                UGY_FAJTAJA_KodtarakDropDownList.Enabled = false;
                DONTEST_HOZTA_KodtarakDropDownList.Enabled = false;
                DONTES_FORMAJA_KodtarakDropDownList.Enabled = false;
                UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.Enabled = false;
                HATARIDO_TULLEPES_NumberBox.Enabled = false;
                HatosagiEllenorzes_RadioButtonList.Enabled = false;
                MunkaorakSzamaOra_NumberBox.Enabled = false;
                MunkaorakSzamaPerc_DropDownList.Enabled = false;
                EljarasiKoltseg_NumberBox.Enabled = false;
                KozigazgatasiBirsagMerteke_NumberBox.Enabled = false;
                SOMMAS_DONTES_KodtarakDropDownList.Enabled = false;
                NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.Enabled = false;
                FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.Enabled = false;
                FUGGO_HATALYU_VEGZES_KodtarakDropDownList.Enabled = false;
                HatosagAltalVisszafizetettOsszeg_NumberBox.Enabled = false;
                HatosagotTerheloEljKtsg_NumberBox.Enabled = false;
                FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.Enabled = false;
            }
        }
        else
        {
            if (AlairasKellCheckBox.Checked)
            {
                AlairasForm.Visible = true;

                Alairo_CsoportTextBox.Enabled = true;
                Alairo_CsoportTextBox.Validate = true;
                Helyettes_CsoportTextBox.Enabled = true;
                if (String.IsNullOrEmpty(Alairo_CsoportTextBox.Id_HiddenField))
                {
                    AlairoSzerep_KodtarakDropDownList.Enabled = false;
                    AlairoSzerep_KodtarakDropDownList.Validate = false;
                    AlairasTipusDropDown.Enabled = false;
                    AlairasSzabalyDropDown.Enabled = false;
                }
                else
                {
                    AlairoSzerep_KodtarakDropDownList.Enabled = true;
                    AlairoSzerep_KodtarakDropDownList.Validate = true;
                    AlairasTipusDropDown.Enabled = true;
                }
            }
            else
            {
                AlairasForm.Visible = false;

                Alairo_CsoportTextBox.Enabled = false;
                Helyettes_CsoportTextBox.Enabled = false;
                AlairoSzerep_KodtarakDropDownList.Enabled = false;
                AlairasTipusDropDown.Enabled = false;
            }
            if (HatosagiAdatlapKellCheckBox.Checked)
            {
                HatosagiAdatlapForm.Visible = true;
                UGY_FAJTAJA_KodtarakDropDownList.Enabled = true;
                UGY_FAJTAJA_KodtarakDropDownList.Validate = true;
                DONTEST_HOZTA_KodtarakDropDownList.Enabled = true;
                DONTEST_HOZTA_KodtarakDropDownList.Validate = true;
                UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.Enabled = true;
                UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.Validate = true;
                if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue.ToString().Equals("1") || UGY_FAJTAJA_KodtarakDropDownList.SelectedValue.ToString().Equals("2"))
                {
                    DONTES_FORMAJA_KodtarakDropDownList.Enabled = true;
                    DONTES_FORMAJA_KodtarakDropDownList.Validate = true;
                }
                HATARIDO_TULLEPES_NumberBox.Enabled = true;
                HATARIDO_TULLEPES_NumberBox.Validate = true;
                HatosagiEllenorzes_RadioButtonList.Enabled = true;
                RequiredFieldValidator1.Enabled = true;
                MunkaorakSzamaOra_NumberBox.Enabled = true;
                MunkaorakSzamaPerc_DropDownList.Enabled = true;
                EljarasiKoltseg_NumberBox.Enabled = true;
                EljarasiKoltseg_NumberBox.Validate = true;
                KozigazgatasiBirsagMerteke_NumberBox.Enabled = true;
                KozigazgatasiBirsagMerteke_NumberBox.Validate = true;
                SOMMAS_DONTES_KodtarakDropDownList.Enabled = true;
                SOMMAS_DONTES_KodtarakDropDownList.Validate = true;
                NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.Enabled = true;
                NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.Validate = true;
                FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.Enabled = true;
                FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.Validate = true;
                FUGGO_HATALYU_VEGZES_KodtarakDropDownList.Enabled = true;
                FUGGO_HATALYU_VEGZES_KodtarakDropDownList.Validate = true;
                HatosagAltalVisszafizetettOsszeg_NumberBox.Enabled = true;
                HatosagAltalVisszafizetettOsszeg_NumberBox.Validate = true;
                HatosagotTerheloEljKtsg_NumberBox.Enabled = true;
                HatosagotTerheloEljKtsg_NumberBox.Validate = true;
                FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.Enabled = true;
                FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.Validate = true;
            }
            else
            {
                HatosagiAdatlapForm.Visible = false;
                UGY_FAJTAJA_KodtarakDropDownList.Enabled = false;
                DONTEST_HOZTA_KodtarakDropDownList.Enabled = false;
                DONTES_FORMAJA_KodtarakDropDownList.Enabled = false;
                UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.Enabled = false;
                HATARIDO_TULLEPES_NumberBox.Enabled = false;
                HatosagiEllenorzes_RadioButtonList.Enabled = false;
                RequiredFieldValidator1.Enabled = false;
                MunkaorakSzamaOra_NumberBox.Enabled = false;
                MunkaorakSzamaPerc_DropDownList.Enabled = false;
                EljarasiKoltseg_NumberBox.Enabled = false;
                KozigazgatasiBirsagMerteke_NumberBox.Enabled = false;
                SOMMAS_DONTES_KodtarakDropDownList.Enabled = false;
                NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.Enabled = false;
                FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.Enabled = false;
                FUGGO_HATALYU_VEGZES_KodtarakDropDownList.Enabled = false;
                HatosagAltalVisszafizetettOsszeg_NumberBox.Enabled = false;
                HatosagotTerheloEljKtsg_NumberBox.Enabled = false;
                FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.Enabled = false;
            }
        }

    }
    #endregion
    #region Page
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        if (Command == CommandName.Modify || Command == CommandName.New || Command == CommandName.View)
        {
            CascadingDropDown_AgazatiJel.Enabled = true;
            CascadingDropDown_Ugykor.Enabled = true;
            CascadingDropDown_Ugytipus.Enabled = true;

            #region Cascading DropDown
            // Contextkey-ben megadjuk a felhaszn�l� id-t
            CascadingDropDown_AgazatiJel.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
            CascadingDropDown_Ugykor.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
            CascadingDropDown_Ugytipus.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);

            #endregion
            AgazatiJelek_DropDownList.LSEX_RaiseImmediateOnChange = true;
            Ugykor_DropDownList.LSEX_RaiseImmediateOnChange = true;
            UgyUgyirat_Ugytipus_DropDownList.LSEX_RaiseImmediateOnChange = true;
            IraIrat_Irattipus_KodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, EErrorPanel1);

            #region LZS
            AdathordozoTipusa_KodtarakDropDownList.FillDropDownList(kcs_ADATHORDOZO_TIPUSA, false, EErrorPanel1);
            UgyintezesAlapja_DropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);
            ExpedialasModjaKodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, true, EErrorPanel1);
            #endregion


            //RegisterJavascripts();
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                // BUG_1592
                if (!IsPostBack)
                {
                    using (var service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
                    {
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = id;

                        Result result = service.Get(execParam);
                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            EREC_TomegesIktatas erec_TomegesIktatas = (EREC_TomegesIktatas)result.Record;
                            LoadComponentsFromBusinessObject(erec_TomegesIktatas);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                    }
                }
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UgyUgyirat_Ugytipus_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            Ugykor_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            AgazatiJelek_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            //Iktatokonyvek_DropDownLis_Ajax.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
        }
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        SetAlairasHatosagFormsControls();
        if (!IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
            }
        }
        if (Command == CommandName.Modify || Command == CommandName.New)
        {
            // Al��r�szerep v�ltoztat�s�ra �jra kell t�lteni az al��r�s t�pus dropdownlist�t is:
            AlairoSzerep_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(AlairoSzerep_KodtarakDropDownList_SelectedIndexChanged);
            AlairoSzerep_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        }
        #region __doPostBack -kel k�ld�tt esem�nyek figyel�se:
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshAlairoSzabalyok:
                    RefreshAlairoSzabalyok();
                    break;
            }
        }
        if (Request.Params["__EVENTTARGET"] != null)
        {
            string eventTarget = Request.Params["__EVENTTARGET"].ToString();
            if (eventTarget.Contains(UGY_FAJTAJA_KodtarakDropDownList.ID))
            {
                DONTES_FORMAJA_KodtarakDropDownList.Enabled = true;
                if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue == "1")
                {
                    DONTES_FORMAJA_KodtarakDropDownList.FillDropDownList(kcs_DONTES_FORMAJA_ALLAMIG, true, EErrorPanel1);
                }
                else if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue == "2")// UgyFajtaja = '2'
                {
                    DONTES_FORMAJA_KodtarakDropDownList.FillDropDownList(kcs_DONTES_FORMAJA_ONK, true, EErrorPanel1);
                }
                else
                {
                    DONTES_FORMAJA_KodtarakDropDownList.Clear();
                    DONTES_FORMAJA_KodtarakDropDownList.Enabled = false;
                    DONTES_FORMAJA_KodtarakDropDownList.FillAndSetEmptyValue("", EErrorPanel1);
                }
            }
        }
        #endregion
        Alairo_CsoportTextBox.TryFireChangeEvent = true;
        pageView.SetViewOnPage(Command);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        #region Javascriptes cuccok beregisztr�l�sa

        if (Command == CommandName.Modify || Command == CommandName.New)
        {
            #region al��r�sform
            if (AlairasKellCheckBox.Checked)
            {
                /// Al��r�k v�ltoz�s�nak figyel�se:
                /// Ha v�ltozott az al��r�, �s al��r� szerep ki volt t�ltve --> postBack, al��r�s t�pusok felt�lt�se
                /// Ha az al��r� szerep disabled volt, enged�lyezni kell
                if (Request.Params["__EVENTTARGET"] != null && IsPostBack)
                {
                    string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                    if (eventTarget.Contains(AlairasKellCheckBox.ID) && String.IsNullOrEmpty(AlairoSzerep_KodtarakDropDownList.SelectedValue))
                    {
                        AlairoSzerep_KodtarakDropDownList.FillDropDownList(kcs_ALAIRO_SZEREP, true, EErrorPanel1);
                    }
                }

                StringBuilder sb_alairoChanged = new StringBuilder();//document.getElementById('<%= hidBT.ClientID %>')
                sb_alairoChanged.Append(" var alairoSzerep_ddl = $get('" + AlairoSzerep_KodtarakDropDownList.DropDownList.ClientID + @"');
                    if (alairoSzerep_ddl) 
                    {        
                        if (alairoSzerep_ddl.disabled == true) { alairoSzerep_ddl.disabled = false; }
                        // ha van kiv�lasztva al��r�szerep is, akkor postBack --> al��r�s t�pusok felt�lt�se
                        if (alairoSzerep_ddl.selectedIndex > 0)
                        {
                            // BUG_1592
                            window.setTimeout(function () { " + Page.ClientScript.GetPostBackEventReference(Alairo_CsoportTextBox.TextBox, EventArgumentConst.refreshAlairoSzabalyok) + @"; }, 500);
                        }
                    }");

                Alairo_CsoportTextBox.TextBox.Attributes["onchange"] = sb_alairoChanged.ToString();



                StringBuilder sb_alairasModok = new StringBuilder();

                // al��r�sm�d dictionary a viewstate-b�l:
                Dictionary<string, string> alairasModDict = (Dictionary<string, string>)ViewState[viewState_AlairasModDictionary];
                if (alairasModDict != null)
                {
                    sb_alairasModok.Append(" var alairasSzabalyok = new Array(); var alairasModok = new Array(); ");
                    int i = 0;
                    foreach (KeyValuePair<string, string> kvp in alairasModDict)
                    {
                        sb_alairasModok.Append(" alairasSzabalyok[" + i + "]= '" + kvp.Key + @"'; 
                                             alairasModok[" + i + "]= '" + kvp.Value + "'; ");
                        i++;
                    }

                    sb_alairasModok.Append(@"
            
                                var ddl_AlairasTipus = document.getElementById('" + AlairasTipusDropDown.ClientID + @"');
            
                                function CheckAlairasAdatok()
                                {                        
                                    if (ddl_AlairasTipus)
                                    { 
                                        if (ddl_AlairasTipus.selectedIndex >= 0)
                                        {
                                            
                                        }
                                        else
                                        {
                                            alert('Nincs kiv�lasztva al��r�s t�pus!'); return false;
                                        }
                                    }
            
                                    return true;
                                }

                                function GetAlairasModByAlairasSzabalyId(alairasSzabalyId)
                                {                                    
                                    var i =0;
                                    for (i=0;i<alairasSzabalyok.length;i++)
                                    {                                        
                                        if (alairasSzabalyok[i] == alairasSzabalyId)
                                        {
                                            return alairasModok[i];
                                        }
                                    }
                                    return '';
                                }                    
            
                                function AlairasTipusDropDownChanged()
                                {                                    
                                    var alairasMod = '';
                                    if (ddl_AlairasTipus && ddl_AlairasTipus.selectedIndex >= 0)
                                    {
                                        var alairasSzabalyId = ddl_AlairasTipus.options[ddl_AlairasTipus.selectedIndex].value;
                                        alairasMod = GetAlairasModByAlairasSzabalyId(alairasSzabalyId);                                        
                                    }
                                }
            
                                ");

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "js_CheckAlairasAdatok", sb_alairasModok.ToString(), true);

                    AlairasTipusDropDown.Attributes["onchange"] = " AlairasTipusDropDownChanged(); ";

                    FormFooter1.ImageButton_Save.OnClientClick = " if (CheckAlairasAdatok()==false){return false;} "
                             + FormFooter1.ImageButton_Save.OnClientClick;
                }
            }
            #endregion
            #region hat adatlap
            if (HatosagiAdatlapKellCheckBox.Checked)
            {
                if (Request.Params["__EVENTTARGET"] != null && IsPostBack)
                {
                    string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                    if (eventTarget.Contains(HatosagiAdatlapKellCheckBox.ID))
                    {
                        if (String.IsNullOrEmpty(UGY_FAJTAJA_KodtarakDropDownList.SelectedValue))
                            UGY_FAJTAJA_KodtarakDropDownList.FillDropDownList(kcs_UGY_FAJTAJA, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(DONTEST_HOZTA_KodtarakDropDownList.SelectedValue))
                            DONTEST_HOZTA_KodtarakDropDownList.FillDropDownList(kcs_DONTEST_HOZTA, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(DONTES_FORMAJA_KodtarakDropDownList.SelectedValue))
                        {
                            if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue == "1")
                            {
                                DONTES_FORMAJA_KodtarakDropDownList.FillDropDownList(kcs_DONTES_FORMAJA_ALLAMIG, true, EErrorPanel1);
                            }
                            else if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue == "2")  // UgyFajtaja = '2'
                            {
                                DONTES_FORMAJA_KodtarakDropDownList.FillDropDownList(kcs_DONTES_FORMAJA_ONK, true, EErrorPanel1);
                            }
                            else
                            {
                                DONTES_FORMAJA_KodtarakDropDownList.Clear();
                                DONTES_FORMAJA_KodtarakDropDownList.Enabled = false;
                                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetEmptyValue("", EErrorPanel1);
                            }
                        }
                        if (String.IsNullOrEmpty(UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.SelectedValue))
                            UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_IDOTARTAMA, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(SOMMAS_DONTES_KodtarakDropDownList.SelectedValue))
                            SOMMAS_DONTES_KodtarakDropDownList.FillDropDownList(kcs_SOMMAS_DONTES, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.SelectedValue))
                            NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.FillDropDownList(kcs_NYOLCNAPON_BELUL_NEM_SOMMAS, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.SelectedValue))
                            FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.FillDropDownList(kcs_FUGGO_HATALYU_HATAROZAT, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(FUGGO_HATALYU_VEGZES_KodtarakDropDownList.SelectedValue))
                            FUGGO_HATALYU_VEGZES_KodtarakDropDownList.FillDropDownList(kcs_FUGGO_HATALYU_VEGZES, true, EErrorPanel1);
                        if (String.IsNullOrEmpty(FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.SelectedValue))
                            FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.FillDropDownList(kcs_FELFUGGESZTETT_HATAROZAT, true, EErrorPanel1);
                    }
                }
            }
            #endregion
        }
        #endregion


    }
    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="EREC_TomegesIktatas"></param>
    private void LoadComponentsFromBusinessObject(EREC_TomegesIktatas erec_TomegesIktatas)
    {
        requiredTextBoxForrasNev.Text = erec_TomegesIktatas.ForrasTipusNev;
        Felelos_CsoportTextBox.Id_HiddenField = erec_TomegesIktatas.FelelosCsoport_Id;
        Felelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        string irattipusKod = GetKodtarKodById(erec_TomegesIktatas.Irattipus_Id);
        IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, irattipusKod, true, EErrorPanel1);

        CascadingDropDown_AgazatiJel.SelectedValue = erec_TomegesIktatas.AgazatiJelek_Id;
        CascadingDropDown_Ugykor.SelectedValue = erec_TomegesIktatas.IraIrattariTetelek_Id;
        string ugytipusKod = GetUgytipusKodById(erec_TomegesIktatas.Ugytipus_Id);
        CascadingDropDown_Ugytipus.SelectedValue = ugytipusKod;

        requiredTextBoxTargy.Text = erec_TomegesIktatas.TargyPrefix;
        AlairasKellCheckBox.Checked = erec_TomegesIktatas.AlairasKell.Equals("1") ? true : false;
        HatosagiAdatlapKellCheckBox.Checked = erec_TomegesIktatas.HatosagiAdatlapKell.Equals("1") ? true : false;
        //SetAlairasHatosagFormsControls();
        #region al��r�s kell
        if (AlairasKellCheckBox.Checked)
        {
            Alairo_CsoportTextBox.Id_HiddenField = erec_TomegesIktatas.FelhasznaloCsoport_Id_Alairo;
            Alairo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            Helyettes_CsoportTextBox.Id_HiddenField = erec_TomegesIktatas.FelhaszCsoport_Id_Helyettesito;
            Helyettes_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            AlairoSzerep_KodtarakDropDownList.FillDropDownList(kcs_ALAIRO_SZEREP, true, EErrorPanel1);
            RefreshAlairoSzabalyok();
            SetDropdownsSelectedValue(erec_TomegesIktatas);

        }
        #endregion
        #region Hat. adatlap kell
        if (HatosagiAdatlapKellCheckBox.Checked)
        {
            UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_TomegesIktatas.UgyFajtaja, EErrorPanel1);
            DONTEST_HOZTA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTEST_HOZTA, erec_TomegesIktatas.DontestHozta, EErrorPanel1);
            if (erec_TomegesIktatas.UgyFajtaja == "1")
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ALLAMIG, erec_TomegesIktatas.DontesFormaja, EErrorPanel1);
            }
            else if (UGY_FAJTAJA_KodtarakDropDownList.SelectedValue == "2")// UgyFajtaja = '2'
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ONK, erec_TomegesIktatas.DontesFormaja, EErrorPanel1);
            }
            else
            {
                DONTES_FORMAJA_KodtarakDropDownList.Clear();
                DONTES_FORMAJA_KodtarakDropDownList.Enabled = false;
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetEmptyValue("", EErrorPanel1);
            }
            UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_IDOTARTAMA, erec_TomegesIktatas.UgyintezesHataridore, EErrorPanel1);
            HATARIDO_TULLEPES_NumberBox.Text = erec_TomegesIktatas.HataridoTullepes;
            SetHatosagiEllenorzesValue(erec_TomegesIktatas.HatosagiEllenorzes);
            SetMunkaorakSzama(erec_TomegesIktatas.MunkaorakSzama);
            EljarasiKoltseg_NumberBox.Text = erec_TomegesIktatas.EljarasiKoltseg;
            KozigazgatasiBirsagMerteke_NumberBox.Text = erec_TomegesIktatas.KozigazgatasiBirsagMerteke;
            SOMMAS_DONTES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SOMMAS_DONTES, erec_TomegesIktatas.SommasEljDontes, EErrorPanel1);
            NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.FillAndSetSelectedValue(kcs_NYOLCNAPON_BELUL_NEM_SOMMAS, erec_TomegesIktatas.NyolcNapBelulNemSommas, EErrorPanel1);
            FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_HATAROZAT, erec_TomegesIktatas.FuggoHatalyuHatarozat, EErrorPanel1);
            FUGGO_HATALYU_VEGZES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_VEGZES, erec_TomegesIktatas.FuggoHatalyuVegzes, EErrorPanel1);
            HatosagAltalVisszafizetettOsszeg_NumberBox.Text = erec_TomegesIktatas.HatAltalVisszafizOsszeg;
            HatosagotTerheloEljKtsg_NumberBox.Text = erec_TomegesIktatas.HatTerheloEljKtsg;
            FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FELFUGGESZTETT_HATAROZAT, erec_TomegesIktatas.FelfuggHatarozat, EErrorPanel1);
        }
        #endregion

        textNote.Text = erec_TomegesIktatas.Base.Note;
        ErvenyessegCalendarControl1.ErvKezd = erec_TomegesIktatas.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_TomegesIktatas.ErvVege;

        #region LZS - kieg�sz�t�s
        autoExpedialasCheckBox.Checked = erec_TomegesIktatas.AutoExpedialasKell.Equals("1") ? true : false;
        AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ADATHORDOZO_TIPUSA, erec_TomegesIktatas.AdathordozoTipusa, EErrorPanel1);
        AdathordozoTipusaViewTextbox.Text = AdathordozoTipusa_KodtarakDropDownList.Text;

        UgyintezesAlapja_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_TomegesIktatas.UgyintezesAlapja, EErrorPanel1);
        UgyintezesAlapjaViewTextbox.Text = UgyintezesAlapja_DropDownList.Text;

        ExpedialasModjaKodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA, erec_TomegesIktatas.ExpedialasModja, EErrorPanel1);
        ExpedialasModjaViewTextBox.Text = ExpedialasModjaKodtarakDropDownList.Text;
        #endregion

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = erec_TomegesIktatas.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(erec_TomegesIktatas.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private EREC_TomegesIktatas GetBusinessObjectFromComponents()
    {
        EREC_TomegesIktatas erec_TomegesIktatas = new EREC_TomegesIktatas();
        erec_TomegesIktatas.Updated.SetValueAll(false);
        erec_TomegesIktatas.Base.Updated.SetValueAll(false);

        erec_TomegesIktatas.ForrasTipusNev = requiredTextBoxForrasNev.Text;
        erec_TomegesIktatas.Updated.ForrasTipusNev = pageView.GetUpdatedByView(requiredTextBoxForrasNev);


        erec_TomegesIktatas.FelelosCsoport_Id = !string.IsNullOrEmpty(Felelos_CsoportTextBox.HiddenField.Value) ? Felelos_CsoportTextBox.HiddenField.Value : Session["SzervezetiEgysegID" + Session.SessionID].ToString();


        erec_TomegesIktatas.Updated.FelelosCsoport_Id = pageView.GetUpdatedByView(Felelos_CsoportTextBox);
        erec_TomegesIktatas.Irattipus_Id = GetKodtarIdByKod(IraIrat_Irattipus_KodtarakDropDownList.SelectedValue);
        erec_TomegesIktatas.Updated.Irattipus_Id = pageView.GetUpdatedByView(IraIrat_Irattipus_KodtarakDropDownList);

        erec_TomegesIktatas.AgazatiJelek_Id = AgazatiJelek_DropDownList.SelectedValue;
        erec_TomegesIktatas.Updated.AgazatiJelek_Id = pageView.GetUpdatedByView(AgazatiJelek_DropDownList);
        erec_TomegesIktatas.IraIrattariTetelek_Id = Ugykor_DropDownList.SelectedValue;
        erec_TomegesIktatas.Updated.IraIrattariTetelek_Id = pageView.GetUpdatedByView(Ugykor_DropDownList);
        erec_TomegesIktatas.Ugytipus_Id = GetUgytipusIdByKod(Ugykor_DropDownList.SelectedValue, UgyUgyirat_Ugytipus_DropDownList.SelectedValue);
        erec_TomegesIktatas.Updated.Ugytipus_Id = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_DropDownList);

        erec_TomegesIktatas.TargyPrefix = requiredTextBoxTargy.Text;
        erec_TomegesIktatas.Updated.TargyPrefix = pageView.GetUpdatedByView(requiredTextBoxTargy);
        erec_TomegesIktatas.AlairasKell = AlairasKellCheckBox.Checked == true ? "1" : "0";
        erec_TomegesIktatas.Updated.AlairasKell = pageView.GetUpdatedByView(AlairasKellCheckBox);
        erec_TomegesIktatas.HatosagiAdatlapKell = HatosagiAdatlapKellCheckBox.Checked == true ? "1" : "0";
        erec_TomegesIktatas.Updated.HatosagiAdatlapKell = pageView.GetUpdatedByView(HatosagiAdatlapKellCheckBox);

        #region al��r�s kell
        if (AlairasKellCheckBox.Checked)
        {
            erec_TomegesIktatas.FelhasznaloCsoport_Id_Alairo = Alairo_CsoportTextBox.Id_HiddenField;
            erec_TomegesIktatas.Updated.FelhasznaloCsoport_Id_Alairo = pageView.GetUpdatedByView(Alairo_CsoportTextBox);
            erec_TomegesIktatas.FelhaszCsoport_Id_Helyettesito = Helyettes_CsoportTextBox.Id_HiddenField;
            erec_TomegesIktatas.Updated.FelhaszCsoport_Id_Helyettesito = pageView.GetUpdatedByView(Helyettes_CsoportTextBox);
            erec_TomegesIktatas.AlairasMod = AlairasTipusDropDown.SelectedValue;
            erec_TomegesIktatas.Updated.AlairasMod = pageView.GetUpdatedByView(AlairasTipusDropDown);
            // erec_TomegesIktatas.AlairasSzabaly_Id = AlairasSzabalyDropDown.SelectedValue;
            // erec_TomegesIktatas.Updated.AlairasSzabaly_Id = pageView.GetUpdatedByView(AlairasSzabalyDropDown);
        }
        #endregion
        #region Hat. adatlap kell
        if (HatosagiAdatlapKellCheckBox.Checked)
        {
            erec_TomegesIktatas.UgyFajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.UgyFajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);
            erec_TomegesIktatas.DontestHozta = DONTEST_HOZTA_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.DontestHozta = pageView.GetUpdatedByView(DONTEST_HOZTA_KodtarakDropDownList);
            erec_TomegesIktatas.DontesFormaja = DONTES_FORMAJA_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.DontesFormaja = pageView.GetUpdatedByView(DONTES_FORMAJA_KodtarakDropDownList);
            erec_TomegesIktatas.UgyintezesHataridore = UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.UgyintezesHataridore = pageView.GetUpdatedByView(UGYINTEZES_IDOTARTAMA_KodtarakDropDownList);
            erec_TomegesIktatas.HataridoTullepes = HATARIDO_TULLEPES_NumberBox.Text;
            erec_TomegesIktatas.Updated.HataridoTullepes = pageView.GetUpdatedByView(HATARIDO_TULLEPES_NumberBox);
            erec_TomegesIktatas.HatosagiEllenorzes = GetHatosagiEllenorzesValue();
            erec_TomegesIktatas.Updated.HatosagiEllenorzes = pageView.GetUpdatedByView(HatosagiEllenorzes_RadioButtonList);
            erec_TomegesIktatas.MunkaorakSzama = GetMunkaorakSzama();
            erec_TomegesIktatas.Updated.MunkaorakSzama = pageView.GetUpdatedByView(MunkaorakSzamaOra_NumberBox) || pageView.GetUpdatedByView(MunkaorakSzamaPerc_DropDownList);
            erec_TomegesIktatas.EljarasiKoltseg = GetMaskedNumber(EljarasiKoltseg_NumberBox.Text);
            erec_TomegesIktatas.Updated.EljarasiKoltseg = pageView.GetUpdatedByView(EljarasiKoltseg_NumberBox);
            erec_TomegesIktatas.KozigazgatasiBirsagMerteke = GetMaskedNumber(KozigazgatasiBirsagMerteke_NumberBox.Text);
            erec_TomegesIktatas.Updated.KozigazgatasiBirsagMerteke = pageView.GetUpdatedByView(KozigazgatasiBirsagMerteke_NumberBox);
            erec_TomegesIktatas.SommasEljDontes = SOMMAS_DONTES_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.SommasEljDontes = pageView.GetUpdatedByView(SOMMAS_DONTES_KodtarakDropDownList);
            erec_TomegesIktatas.NyolcNapBelulNemSommas = NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.NyolcNapBelulNemSommas = pageView.GetUpdatedByView(NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList);
            erec_TomegesIktatas.FuggoHatalyuHatarozat = FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.FuggoHatalyuHatarozat = pageView.GetUpdatedByView(FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList);
            erec_TomegesIktatas.FuggoHatalyuVegzes = FUGGO_HATALYU_VEGZES_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.FuggoHatalyuVegzes = pageView.GetUpdatedByView(FUGGO_HATALYU_VEGZES_KodtarakDropDownList);

            erec_TomegesIktatas.HatAltalVisszafizOsszeg = GetMaskedNumber((HatosagAltalVisszafizetettOsszeg_NumberBox.Text));
            erec_TomegesIktatas.Updated.HatAltalVisszafizOsszeg = pageView.GetUpdatedByView(HatosagAltalVisszafizetettOsszeg_NumberBox);
            erec_TomegesIktatas.HatTerheloEljKtsg = GetMaskedNumber((HatosagotTerheloEljKtsg_NumberBox.Text));
            erec_TomegesIktatas.Updated.HatTerheloEljKtsg = pageView.GetUpdatedByView(HatosagotTerheloEljKtsg_NumberBox);
            erec_TomegesIktatas.FelfuggHatarozat = FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.SelectedValue;
            erec_TomegesIktatas.Updated.FelfuggHatarozat = pageView.GetUpdatedByView(FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList);
        }
        #endregion

        erec_TomegesIktatas.Base.Note = textNote.Text;
        erec_TomegesIktatas.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_TomegesIktatas, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        erec_TomegesIktatas.Base.Ver = FormHeader1.Record_Ver;
        erec_TomegesIktatas.Base.Updated.Ver = true;


        #region LZS - Kieg�sz�t�s

        //erec_TomegesIktatas.Base.LetrehozasIdo = DateTime.Now.ToString ( );

        #region automata expedialas kell
        erec_TomegesIktatas.AutoExpedialasKell = autoExpedialasCheckBox.Checked == true ? "1" : "0";
        erec_TomegesIktatas.Updated.AutoExpedialasKell = pageView.GetUpdatedByView(autoExpedialasCheckBox);
        #endregion

        #region �gyint�z�s alapja
        erec_TomegesIktatas.UgyintezesAlapja = UgyintezesAlapja_DropDownList.SelectedValue;
        erec_TomegesIktatas.Updated.UgyintezesAlapja = pageView.GetUpdatedByView(UgyintezesAlapja_DropDownList);
        #endregion

        #region Adathordoz� t�pusa
        erec_TomegesIktatas.AdathordozoTipusa = AdathordozoTipusa_KodtarakDropDownList.SelectedValue;
        erec_TomegesIktatas.Updated.AdathordozoTipusa = pageView.GetUpdatedByView(AdathordozoTipusa_KodtarakDropDownList);
        #endregion

        #region Expedialas m�dja
        erec_TomegesIktatas.ExpedialasModja = ExpedialasModjaKodtarakDropDownList.SelectedValue;
        erec_TomegesIktatas.Updated.ExpedialasModja = pageView.GetUpdatedByView(ExpedialasModjaKodtarakDropDownList);
        #endregion

        #endregion

        //LZS - BUG_5417 - �rv�nyess�g kezdete nem volt lementve.
        erec_TomegesIktatas.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;

        return erec_TomegesIktatas;
    }
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            using (EREC_TomegesIktatasService service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
                            {
                                EREC_TomegesIktatas erec_TomegesIktatas = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = service.Insert(execParam, erec_TomegesIktatas, AlairoSzerep_KodtarakDropDownList.SelectedValue);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {

                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                using (EREC_TomegesIktatasService service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
                                {
                                    EREC_TomegesIktatas erec_TomegesIktatas = GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.Update(execParam, erec_TomegesIktatas, AlairoSzerep_KodtarakDropDownList.SelectedValue);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
    #endregion
    #region priv�t met�dusok
    private string GetHatosagiEllenorzesValue()
    {
        if (!String.IsNullOrEmpty(HatosagiEllenorzes_RadioButtonList.SelectedValue))
        {
            if (HatosagiEllenorzes_RadioButtonList.SelectedValue == "1" || HatosagiEllenorzes_RadioButtonList.SelectedValue == "0")
            {
                return HatosagiEllenorzes_RadioButtonList.SelectedValue;
            }
        }

        return String.Empty;
    }

    private void SetHatosagiEllenorzesValue(string value)
    {
        if (!String.IsNullOrEmpty(value))
        {
            if (value == "0" || value == "1")
            {
                HatosagiEllenorzes_RadioButtonList.SelectedValue = value;
            }
        }
    }

    private string GetMunkaorakSzama()
    {
        string text = String.Empty;

        if (!String.IsNullOrEmpty(MunkaorakSzamaOra_NumberBox.Text) || MunkaorakSzamaPerc_DropDownList.SelectedIndex > 0)
        {
            decimal number = 0;

            if (!String.IsNullOrEmpty(MunkaorakSzamaOra_NumberBox.Text))
            {
                number = MunkaorakSzamaOra_NumberBox.Number;
            }

            if (MunkaorakSzamaPerc_DropDownList.SelectedIndex == 1)
            {
                int selectedValue = 5;
                number = number + selectedValue / 10m;
            }

            text = number.ToString();
        }

        return text;
    }

    private void SetMunkaorakSzama(string text)
    {
        string textOra = String.Empty;
        string textPerc = String.Empty;

        if (!String.IsNullOrEmpty(text))
        {
            decimal number;
            if (Decimal.TryParse(text, out number))
            {
                decimal intPart = Math.Truncate(number);
                textOra = intPart.ToString();
                if (intPart != number)
                {
                    textPerc = Math.Truncate((number - intPart) * 10).ToString();
                }
            }
        }

        MunkaorakSzamaOra_NumberBox.Text = textOra;

        if (textPerc == "5")
        {
            MunkaorakSzamaPerc_DropDownList.SelectedIndex = 1;
        }
        else
        {
            MunkaorakSzamaPerc_DropDownList.SelectedIndex = 0;
        }

    }
    private string GetMaskedNumber(string text)
    {
        if (!String.IsNullOrEmpty(text))
        {
            return text.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator, "");
        }

        return String.Empty;
    }
    private string GetKodtarKodById(string id)
    {
        string irattipusKod = "";
        using (Contentum.eAdmin.Service.KRT_KodTarakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodTarakService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                irattipusKod = ((KRT_KodTarak)result.Record).Kod;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        return irattipusKod;
    }
    private string GetKodtarIdByKod(string kod)
    {
        string irattipusId = "";
        using (Contentum.eAdmin.Service.KRT_KodTarakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodTarakService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = service.GetByKodcsoportKod(execParam, kcs_IRATTIPUS, kod);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                irattipusId = ((KRT_KodTarak)result.Record).Id;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        return irattipusId;
    }
    private string GetUgytipusKodById(string id)
    {
        string ugytipusKod = "";
        using (Contentum.eRecord.Service.EREC_IratMetaDefinicioService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                ugytipusKod = ((EREC_IratMetaDefinicio)result.Record).Ugytipus;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        return ugytipusKod;
    }
    private string GetUgytipusIdByKod(string ugykorId, string ugytipus)
    {
        string ugytipusKod = "";
        if (!String.IsNullOrEmpty(ugykorId))
        {
            using (Contentum.eRecord.Service.EREC_IratMetaDefinicioService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService())
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


                Result result = service.GetIratMetaDefinicioByUgykorUgytipus(execParam, ugykorId, ugytipus);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Record != null)
                    {
                        ugytipusKod = ((EREC_IratMetaDefinicio)result.Record).Id;
                        if (Command == CommandName.View)
                        {
                            ViewTextBoxUgytipus.Text = ((EREC_IratMetaDefinicio)result.Record).UgytipusNev;
                        }
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        return ugytipusKod;
    }
    #endregion
    #region Al��r�sok
    private void SetDropdownsSelectedValue(EREC_TomegesIktatas erec_tomegesIktatas)
    {
        Result result = new Result();
        #region Adatok lek�rdez�se adatb�zisb�l:
        using (var service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
        {
            var execParam = UI.SetExecParamDefault(Page);
            execParam.Record_Id = erec_tomegesIktatas.AlairasSzabaly_Id;
            result = service.GetAlairoSzabylById(execParam);
        }
        #endregion
        DataRow dr = result.Ds.Tables[0].Rows[0];
        AlairoSzerep_KodtarakDropDownList.SelectedValue = dr["AlairoSzerep"].ToString();
        RefreshAlairoSzabalyok();
        AlairasTipusDropDown.SelectedValue = erec_tomegesIktatas.AlairasMod;
        if (Command == CommandName.View)
            AlairasSzabalyDropDown.Items.Add(new ListItem() { Value = dr["Id"].ToString(), Text = dr["Nev"].ToString() });
    }
    protected void AlairoSzerep_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshAlairoSzabalyok();
    }

    private void RefreshAlairoSzabalyok()
    {
        // t�r�lj�k a dropdownlista elemeit:
        AlairasTipusDropDown.Items.Clear();

        string alairoSzerep = AlairoSzerep_KodtarakDropDownList.SelectedValue;
        string felhasznaloId_Alairo = Alairo_CsoportTextBox.Id_HiddenField;

        #region Enabled �ll�t�sa
        if (!String.IsNullOrEmpty(felhasznaloId_Alairo))
        {
            AlairoSzerep_KodtarakDropDownList.Enabled = true;

            if (!String.IsNullOrEmpty(alairoSzerep))
            {
                AlairasTipusDropDown.Enabled = true;
            }
        }

        // Felhaszn�l� textbox letilt�sa:
        //AlairoSzerep_KodtarakDropDownList.Enabled = false;

        #endregion

        if (!string.IsNullOrEmpty(alairoSzerep) && !string.IsNullOrEmpty(felhasznaloId_Alairo))
        {
            Dictionary<string, DataRowCollection> alairoSzabalyokDict = null;


            // Objektum l�trehoz�sa
            alairoSzabalyokDict = new Dictionary<string, DataRowCollection>();
            Result result = new Result();
            #region Adatok lek�rdez�se adatb�zisb�l:

            if (!string.IsNullOrEmpty(AlairoSzerep_KodtarakDropDownList.SelectedValue))
                using (var service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
                {
                    result = service.GetAlairasTipusokSzerepAlapjan(UI.SetExecParamDefault(Page), AlairoSzerep_KodtarakDropDownList.SelectedValue);
                }
            //AlairasokService service = eRecordService.ServiceFactory.GetAlairasokService();

            //string objektumTip = "Irat";
            //string objektumId = Request.QueryString.Get(QueryStringVars.Id);
            //string folyamatKod = "IratJovahagyas";
            //string muveletKod = null;

            //Result result = service.GetAll_PKI_IratAlairoSzabalyok(UI.SetExecParamDefault(Page)
            //   , felhasznaloId_Alairo, null, objektumTip, null, folyamatKod, muveletKod, alairoSzerep);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                // komponensek Enabled �rt�k�nek vissza�ll�t�sa:
                //InitComponentsForAlairasFelvitel();
                return;
            }
            #endregion

            alairoSzabalyokDict.Add(alairoSzerep, result.Ds.Tables[0].Rows);


            // Felt�ltj�k a dropdownList-et:
            if (alairoSzabalyokDict.ContainsKey(alairoSzerep))
            {
                // Az al��r�sszab�lyhoz tartoz� al��r�sm�dot lementj�k ViewState-be, hogy ne kelljen k�l�n lek�rni
                Dictionary<string, string> alairasModDict = new Dictionary<string, string>();

                foreach (DataRow row in alairoSzabalyokDict[alairoSzerep])
                {
                    string row_AlairasNev = row["Nev"].ToString();
                    string row_AlairasTipus_Id = row["Id"].ToString();
                    string row_AlairasMod = row["AlairasMod"].ToString();
                    // dropdownlist�ba felv�tel:
                    AlairasTipusDropDown.Items.Add(new ListItem(row_AlairasNev, row_AlairasTipus_Id));
                    // al��r�sm�d dictionary-be:
                    if (alairasModDict.ContainsKey(row_AlairasTipus_Id) == false)
                    {
                        alairasModDict.Add(row_AlairasTipus_Id, row_AlairasMod);
                    }
                }

                // al��r�sm�dok viewstate-be ment�se:
                ViewState[viewState_AlairasModDictionary] = alairasModDict;

                // Ha az al��r�st�pus dropdown kiv�lasztott eleme 'M_UTO', akkor enged�lyezni kell a d�tumkomponenst
                if (!String.IsNullOrEmpty(AlairasTipusDropDown.SelectedValue) && alairasModDict.ContainsKey(AlairasTipusDropDown.SelectedValue))
                {
                    string alairasMod_selected = alairasModDict[AlairasTipusDropDown.SelectedValue];

                }
            }
        }

    }
    #endregion
}