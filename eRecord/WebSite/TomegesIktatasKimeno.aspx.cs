﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.TomegesIktatas;
using Contentum.eRecord.UtilityExcel;
using Contentum.eUtility;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.UI;

namespace KimenoIktatas
{

    public partial class TomegesIktatasKimeno : Contentum.eUtility.UI.PageBase
    {
        private const string funkcio_BelsoIratIktatas = "BelsoIratIktatas";
        private const string funkcio_Erkeztetes = "KuldemenyNew";

        private UtilityTomegesIktatas _util;
        private TomegesIktatasHelper _helper;
        private ExecParam _globalExecParam;

        #region Page methods

        protected void Page_Init(object sender, EventArgs e)
        {
            // Funkciójog-ellenőrzések: (Érkeztetés + Bejövő irat iktatás)
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_Erkeztetes);
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_BelsoIratIktatas);
            // BUG_6058
            KDDLPrioritas.FillDropDownList(TomegesIktatasFolyamat.kcs_TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS, false, ErrorPanel1, false);

            _util = new UtilityTomegesIktatas(Page, ErrorPanel1);
            _globalExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            _helper = new TomegesIktatasHelper(_globalExecParam, null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (ErrorPanel1.Visible)
                {
                    ErrorPanel1.Visible = false;
                    ErrorUpdatePanel1.Update();
                }
                if (ResultPanel.Visible)
                {
                    ResultPanel.Visible = false;
                }
            }

            #region Eredmény fájl letöltése

            string command = Request.QueryString.Get(QueryStringVars.Command);
            if (command == "DownloadResult")
            {
                string tempDirName = Request.QueryString.Get("TempDir");
                string tempFileName = Request.QueryString.Get("TempFile");

                if (!String.IsNullOrEmpty(tempDirName)
                    && !String.IsNullOrEmpty(tempFileName)
                    && tempFileName.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase))
                {
                    _util.DownloadResultFile(tempDirName, tempFileName);
                }
            }

            #endregion
        }

        #endregion

        #region Import sablon fájl előállítása

        protected void btnGetImportSablon_Click(object sender, EventArgs e)
        {
            try
            {
                bool ignoreOutputColumns = true;
                byte[] excelHeaderContent = EgyszerusitettIktatasAdatok.GetExcleHeaderContent(ignoreOutputColumns);
                _util.DownloadExcel("KimenoTomegesIktatasSablon.xlsx", excelHeaderContent);
            }
            catch (Exception exc)
            {
                ResultError.DisplayErrorOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader, exc.Message);
            }
        }

        #endregion

        protected void linkResultLog_Click(object sender, EventArgs e)
        {
            _util.DownloadLog(txtBoxIktatasResult.Text);
        }

        #region Tömeges iktatás

        protected void UploadBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload1.HasFile)
                {
                    //if (FileUpload1.PostedFile.ContentType != "text/xml" && FileUpload1.PostedFile.ContentType != "application/octet-stream")
                    //{
                    //    ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                    //        "Csak xml vagy dbf file tölthető fel.");
                    //    return;
                    //}

                    var priority = Int32.Parse(KDDLPrioritas.SelectedValue);
                    var execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    var tomegesIktatas = new TomegesIktatasCsv(FileUpload1.FileBytes, execParam, Session, Cache, priority, _util, _helper);
                    // Feldolgozás és importálás (iktatások) indítása:
                    var bgItem = tomegesIktatas.StartImportOnBackground();

                    // Hiddenfieldbe letesszük a processId-t, hogy majd lehessen kérdezgetni a folyamat állapotát:
                    hfBackgroundProcessId.Value = bgItem.BackgroundProcessId.ToString();

                    SetResultPanel(tomegesIktatas);

                    // Javascriptes folyamatos státuszellenőrzés indítása:
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "jsStartProcessStatusCheck", "TomegesIktatas.startStatusCheck();", true);
                }
                else
                {
                    throw new ResultException("Nincs fájl kiválasztva!");
                }
            }
            catch (Exception exc)
            {
                ResultError.DisplayErrorOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader, exc.Message);
            }
        }

        public void SetResultPanel(TomegesIktatasCsv tomegesIktatas)
        {
            txtBoxIktatasResult.Text = "1. sor: Fejléc" + Environment.NewLine;
            ShowResultPanel();
        }

        public void ShowResultPanel()
        {
            ResultPanel.Visible = true;

            // A főpanelt elrejtjük, illetve a gombok láthatóságát is állítjuk:
            EFormPanel1.Visible = false;
            ImageOk.Visible = false;
            ImageButtonBack.Visible = true;
        }

        #endregion

        #region Háttérszálas működés

        [System.Web.Services.WebMethod]
        public static TomegesIktatasStatusz GetTomegesIktatasStatusz(string processId, string lastStatusGetTime)
        {
            return UtilityTomegesIktatas.GetTomegesIktatasStatusz(processId, lastStatusGetTime);
        }

        #endregion
    }

    #region CSV segéd

    public class IktatoObjektum : IktatoObjektumBase
    {
        public readonly IktatasImportCsvItem CsvItem;
        private readonly UtilityTomegesIktatas _util;

        public IktatoObjektum(IktatasImportCsvItem iktatasAdatok, UtilityTomegesIktatas util)
        {
            CsvItem = iktatasAdatok;
            _util = util;
            IktatoKonyvId = iktatasAdatok.IktatokonyvId.HasValue ? iktatasAdatok.IktatokonyvId.Value.ToString() : "-1";
            CsoportId = -1;
        }

        public override int GetFoszam()
        {
            int foszam;
            if (Int32.TryParse(CsvItem.UgyiratObj.Foszam, out foszam))
            {
                return foszam;
            }
            return 0;
        }

        public override int GetIratIrany()
        {
            if (!String.IsNullOrEmpty(CsvItem.ImportRowObj.IratIranya))
            {
                var kod = _util.GetKodtarKodByKodtarNev("POSTAZAS_IRANYA", CsvItem.ImportRowObj.IratIranya);
                switch (kod)
                {
                    case "0": return 0;
                    case "1": return 1;
                    default: return 2;
                }
            }
            else
            {
                return 2;
            }
        }
    }

    public class TomegesIktatasCsv : TomegesIktatasCsvBase
    {
        private TomegesIktatasBackgroundItem _tomegesIktatasBackgroundItem;

        #region Constructor

        public TomegesIktatasCsv(byte[] csvFileContent, ExecParam execParam, System.Web.SessionState.HttpSessionState session
                , System.Web.Caching.Cache cache, int priority, UtilityTomegesIktatas util, TomegesIktatasHelper helper)
        {
            _helper = helper;
            _priority = priority;
            _util = util;
            _csvFileContent = csvFileContent;
            _execParam = execParam;
            Session = session;
            Cache = cache;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Import CSV fájl feldolgozása, és tömeges iktatás indítása - háttérszálon
        /// </summary>
        /// <returns></returns>
        public TomegesIktatasBackgroundItem StartImportOnBackground()
        {
            IktatasAdatokList = new List<IktatasImportCsvItemBase>();

            _tomegesIktatasBackgroundItem = new TomegesIktatasBackgroundItem(IktatasAdatokList, _helper, _util, _execParam, _priority);
            // Beregisztrálás a cache-be, hogy majd lehessen kérdezgetni a státuszt:
            _tomegesIktatasBackgroundItem.RegisterBackgroundProcess();

            System.Threading.Thread thread = new System.Threading.Thread(
                        new System.Threading.ThreadStart(StartImportDoWork));

            // Háttérszál indítása:
            thread.Start();

            return _tomegesIktatasBackgroundItem;
        }

        /// <summary>
        /// Import CSV fájl feldolgozásának indítása, majd soronként iktatás hívása
        /// </summary>
        private void StartImportDoWork()
        {
            try
            {
                // CSV fájl sorai objektumként:
                List<EgyszerusitettIktatasAdatok> importRowObjects;
                try
                {
                    importRowObjects = ProcessImportCsv(_csvFileContent, false);
                }
                catch (CsvHelper.CsvMissingFieldException)
                {
                    /// A beolvasást engedélyezzük az eredmény oszlopokat nem tartalmazó CSV-re is:
                    /// (Amiben nincs benne az Iktatószám és Hibaüzenet mező)
                    /// 
                    importRowObjects = ProcessImportCsv(_csvFileContent, true);
                }

                #region Mezőnév visszafejtések (szöveges nevek alapján Id-k lekérése)

                // Érkeztetőkönyvek:
                FillErkeztetokonyvNevek();

                // Iktatókönyvek:
                FillIktatokonyvNevek();

                // CsoportNevek:
                List<string> csoportNevek = new List<string>();
                FillCsoportNevekAll();

                // Irattári tételszámok:
                //List<String> irattariTetelszamok = new List<string>();
                //irattariTetelszamok.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.IrattariTetelszam))
                //                        .Select(e => e.IrattariTetelszam).Distinct().ToList());
                FillIrattariTetelszamokCache();

                // Előzmény ügyiratok:
                List<string> elozmenyUgyiratNevek = new List<string>();
                elozmenyUgyiratNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.ElozmenyUgyirat))
                                        .Select(e => e.ElozmenyUgyirat).Distinct().ToList());
                FillElozmenyUgyiratok(elozmenyUgyiratNevek);

                // Ügytípusok feltöltése:
                FillUgyTipusNevekAll();

                // Partnernevek:
                List<string> partnerNevek = new List<string>();
                // Küldő partner:
                partnerNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.CimzettNeve))
                                        .Select(e => e.CimzettNeve).Distinct().ToList());
                // Ügyindító:
                partnerNevek.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.UgyfelUgyindito))
                                        .Select(e => e.UgyfelUgyindito).Distinct().ToList());

                FillPartnerNevekCache(partnerNevek);

                // Adószámok:
                var adoszamok = new List<string>();
                adoszamok.AddRange(importRowObjects.Where(e => !String.IsNullOrEmpty(e.Adoszam))
                                        .Select(e => e.Adoszam).Distinct().ToList());

                FillPartnerAdoszamokCache(adoszamok);

                #endregion

                #region Iktatáshoz szükséges objektumok feltöltése, adatok ellenőrzése

                // Csv sorszám (fejléc az 1.)
                int rowNumber = 1;
                foreach (var importRowObj in importRowObjects)
                {
                    rowNumber++;

                    IktatasImportCsvItem iktatasAdatok = new IktatasImportCsvItem(rowNumber, importRowObj, _execParam, _helper, IsNMHH());
                    IktatasAdatokList.Add(iktatasAdatok);

                    // Ha ki van töltve az iktatószám, akkor erre a sorra nem kell iktatás (max. csatolmány feltöltés)
                    if (!String.IsNullOrEmpty(importRowObj.ResultIktatoszam))
                    {
                        iktatasAdatok.MarIktatvaVolt = true;
                        continue;
                    }

                    // Csatolmányok létezésének ellenőrzése:
                    iktatasAdatok.CheckCsatolmanyok();

                    try
                    {
                        // Iktatáshoz szükséges objektumok feltöltése:
                        FillIktatasBusinessObjects(iktatasAdatok, importRowObj);
                    }
                    catch (Exception exc)
                    {
                        Logger.Error(exc.ToString());

                        // A hibaüzenetet kiegészítjük a sorszámmal:
                        throw new ResultException(String.Format("{0} ({1}. sor)", exc.Message, rowNumber));
                    }
                }

                #endregion

                _tomegesIktatasBackgroundItem.StartIktatas(_csvFileContent);

            }
            catch (Exception exc)
            {
                _tomegesIktatasBackgroundItem.BackgroundProcessError = exc.Message;
            }
            finally
            {
                _tomegesIktatasBackgroundItem.BackgroundProcessEnded = true;
            }
        }

        #endregion

        #region Private methods

        private bool IsNMHH()
        {
            return Constants.OrgKod.NMHH.Equals(FelhasznaloProfil.OrgKod(Session), StringComparison.InvariantCultureIgnoreCase);
        }

        #region FillIktatasBusinessObjects

        /// <summary>
        /// Az iktatáshoz szükséges objektumok feltöltése
        /// </summary>
        /// <param name="iktatasAdatok"></param>
        private void FillIktatasBusinessObjects(IktatasImportCsvItem iktatasAdatok, EgyszerusitettIktatasAdatok importRowObj)
        {
            // Előzmény ügyirat van-e:        
            Guid? elozmenyUgyiratId = null;
            var elozmenyUgyiratFoszam = "";
            if (!String.IsNullOrEmpty(importRowObj.ElozmenyUgyirat) && importRowObj.ElozmenyUgyirat.Trim().Length > 0)
            {
                if (_elozmenyUgyiratAzonDict.ContainsKey(importRowObj.ElozmenyUgyirat))
                {
                    elozmenyUgyiratId = _elozmenyUgyiratAzonDict[importRowObj.ElozmenyUgyirat];
                }
                else
                {
                    // Hiba:
                    throw new ResultException(String.Format("Nem található a megadott előzmény ügyirat: {0}", importRowObj.ElozmenyUgyirat));
                }

                // főszám ellenőrzés
                elozmenyUgyiratFoszam = _elozmenyUgyiratFoszamDict[importRowObj.ElozmenyUgyirat];
                if (String.IsNullOrEmpty(elozmenyUgyiratFoszam))
                {
                    throw new ResultException(String.Format("A megadott előzmény ügyirat főszáma nincs beállítva: {0}", importRowObj.ElozmenyUgyirat));
                }
            }

            // KuldemenyAutomatikusAtadasa
            if (!String.IsNullOrEmpty(importRowObj.KuldemenyAutomatikusAtadasa) && importRowObj.KuldemenyAutomatikusAtadasa.Trim().Length > 0)
            {
                if (_csoportNevekDict.ContainsKey(importRowObj.KuldemenyAutomatikusAtadasa))
                {
                    iktatasAdatok.KuldemenyAutomatikusAtadasaCsoportId = _csoportNevekDict[importRowObj.KuldemenyAutomatikusAtadasa].ToString();
                }
                else
                {
                    // Nincs ilyen nevű fehasználó/csoport:
                    throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", importRowObj.KuldemenyAutomatikusAtadasa));
                }
            }

            // iktatás objektumok
            iktatasAdatok.UgyiratObj = GetBusinessObjectFromComponents_EREC_UgyUgyiratok(importRowObj);
            iktatasAdatok.IratObj = GetBusinessObjectFromComponents_EREC_IraIratok(importRowObj);
            iktatasAdatok.PeldanyObj = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok(importRowObj);
            iktatasAdatok.AlairokObj = GetBusinessObjectFromComponents_EREC_IratAlairok(importRowObj);

            // iktatás paraméterek
            bool isAlszamraIktatas;
            Guid? iktatokonyvId;
 
            iktatasAdatok.IktatasiParameterek = GetIktatasiParameterek(importRowObj, elozmenyUgyiratId
                , iktatasAdatok.UgyiratObj.UgyTipus, iktatasAdatok.UgyiratObj.IraIrattariTetel_Id
                , out isAlszamraIktatas, out iktatokonyvId);

            iktatasAdatok.IsAlszamraIktatas = isAlszamraIktatas;
            iktatasAdatok.IktatokonyvId = iktatokonyvId;
            iktatasAdatok.ElozmenyUgyiratId = elozmenyUgyiratId;
            if (isAlszamraIktatas && elozmenyUgyiratId.HasValue && iktatasAdatok.UgyiratObj != null)
            {
                iktatasAdatok.UgyiratObj.Id = elozmenyUgyiratId.Value.ToString();
                iktatasAdatok.UgyiratObj.Foszam = elozmenyUgyiratFoszam;
            }
            iktatasAdatok.ExpedialniKell = IsNMHH() ? "0" : UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.ExpedialniKell);
        }

        private EREC_IraIratok GetBusinessObjectFromComponents_EREC_IraIratok(EgyszerusitettIktatasAdatok importRowObj)
        {
            #region Kötelező mezők ellenőrzése

            #endregion

            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_IraIratok.Updated.SetValueAll(false);
            erec_IraIratok.Base.Updated.SetValueAll(false);

            // Ha üres az Irat_Targy mező akkor bemásoljuk az ügyirat tárgyát
            if (String.IsNullOrEmpty(importRowObj.IratTargya))
            {
                erec_IraIratok.Targy = importRowObj.UgyiratTargya;
            }
            else
            {
                erec_IraIratok.Targy = importRowObj.IratTargya;
            }
            erec_IraIratok.Updated.Targy = true;

            erec_IraIratok.Irattipus = _util.GetKodtarKodByKodtarNev(kcs_IRATTIPUS, importRowObj.IratTipus);
            erec_IraIratok.Updated.Irattipus = true;

            erec_IraIratok.KiadmanyozniKell = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.KiadmanyozniKell);
            erec_IraIratok.Updated.KiadmanyozniKell = true;

            erec_IraIratok.Minosites = _util.GetKodtarKodByKodtarNev(kcs_IRATMINOSITES, importRowObj.IratMinositese);
            erec_IraIratok.Updated.Minosites = true;

            if (!String.IsNullOrEmpty(importRowObj.IratUgyintezo))
            {
                if (_csoportNevekDict.ContainsKey(importRowObj.IratUgyintezo))
                {
                    erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = _csoportNevekDict[importRowObj.IratUgyintezo].ToString();
                    erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                }
                else
                {
                    // Nincs ilyen nevű fehasználó/csoport:
                    throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", importRowObj.IratUgyintezo));
                }
            }

            /// Irat intézési idejét csv-ben így kell megadni pl.: "22 nap"
            /// --> szétbontjuk két részre: IntezesiIdo + IntezesiIdoegyseg
            /// 

            if (!String.IsNullOrEmpty(importRowObj.IratUgyintezesiIdeje) &&
                !Rendszerparameterek.GetBoolean(_execParam, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true) // PROD ügyfeleknél nics meg az UGYIRAT_INTEZESI_IDO kódcsoport
                )
            {
                string[] intezesiIdoParts = importRowObj.IratUgyintezesiIdeje.Split(' ');
                string intezesiIdoStr = intezesiIdoParts[0];
                string intezesiIdoEgysegStr = (intezesiIdoParts.Length > 1) ? intezesiIdoParts[1] : String.Empty;

                erec_IraIratok.IntezesiIdo = _util.GetKodtarKodByKodtarNev(kcs_UGYIRAT_INTEZESI_IDO, intezesiIdoStr);
                erec_IraIratok.Updated.IntezesiIdo = true;

                if (!String.IsNullOrEmpty(intezesiIdoEgysegStr))
                {
                    erec_IraIratok.IntezesiIdoegyseg = _util.GetKodtarKodByKodtarNev(kcs_IDOEGYSEG, intezesiIdoEgysegStr);
                    erec_IraIratok.Updated.IntezesiIdoegyseg = true;
                }
            }

            if (!String.IsNullOrEmpty(importRowObj.IratUgyintezesiHatarideje))
            {
                DateTime dat;
                if (DateTime.TryParse(importRowObj.IratUgyintezesiHatarideje, out dat))
                {
                    erec_IraIratok.Hatarido = dat.ToString();
                    erec_IraIratok.Updated.Hatarido = true;
                }
                else
                {
                    throw new ResultException(String.Format("A megadott 'Irat ügyintézési határideje' nem megfelelő formátumú: '{0}'", importRowObj.IratUgyintezesiHatarideje));
                }
            }

            erec_IraIratok.AdathordozoTipusa = _util.GetKodtarKodByKodtarNev(kcs_UGYINTEZES_ALAPJA, importRowObj.AdathordozoTipusa);
            erec_IraIratok.Updated.AdathordozoTipusa = true;

            erec_IraIratok.UgyintezesAlapja = _util.GetKodtarKodByKodtarNev(kcs_ELSODLEGES_ADATHORDOZO, importRowObj.ElsodlegesAdathordozoTipusa);
            erec_IraIratok.Updated.UgyintezesAlapja = true;

            // Ez már le van véve felületről:
            //erec_IraIratok.Jelleg = IratJellegKodtarakDropDown.SelectedValue;
            //erec_IraIratok.Updated.Jelleg = true;

            // nekrisz : munkaállomás mentése
            erec_IraIratok.Munkaallomas = _execParam.UserHostAddress;
            erec_IraIratok.Updated.Munkaallomas = true;

            erec_IraIratok.UgyintezesModja = _util.GetKodtarKodByKodtarNev(kcs_IRAT_UGYINT_MODJA, importRowObj.UgyintezesModja);
            erec_IraIratok.Updated.UgyintezesModja = true;

            if (!String.IsNullOrEmpty(importRowObj.IratIranya))
            {
                erec_IraIratok.PostazasIranya = _util.GetKodtarKodByKodtarNev(kcs_POSTAZAS_IRANYA, importRowObj.IratIranya);
            }
            else
            {
                erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
            }
            erec_IraIratok.Updated.PostazasIranya = true;

            if (!String.IsNullOrEmpty(importRowObj.IratElintezesiIdopontja))
            {
                DateTime dat;
                if (DateTime.TryParse(importRowObj.IratElintezesiIdopontja, out dat))
                {
                    erec_IraIratok.IntezesIdopontja = dat.ToString();
                    erec_IraIratok.Updated.IntezesIdopontja = true;
                }
                else
                {
                    throw new ResultException(String.Format("A megadott 'Irat elintézési időpontja' nem megfelelő formátumú: '{0}'", importRowObj.IratElintezesiIdopontja));
                }
            }

            if (!String.IsNullOrEmpty(importRowObj.FelelosSzervezet))
            {
                if (_csoportNevekDict.ContainsKey(importRowObj.FelelosSzervezet))
                {
                    erec_IraIratok.Csoport_Id_Ugyfelelos = _csoportNevekDict[importRowObj.FelelosSzervezet].ToString();
                    erec_IraIratok.Updated.Csoport_Id_Ugyfelelos = true;
                }
                else
                {
                    // Nincs ilyen nevű fehasználó/csoport:
                    throw new ResultException(String.Format("Nincs ilyen nevű csoport: '{0}'", importRowObj.FelelosSzervezet));
                }
            }

            erec_IraIratok.IratHatasaUgyintezesre = _util.GetKodtarKodByKodtarNev(kcs_IRAT_HATASA_UGYINTEZESRE, importRowObj.IratHatasaAzUgyintezesre);
            erec_IraIratok.Updated.IratHatasaUgyintezesre = true;

            return erec_IraIratok;
        }

        private EREC_PldIratPeldanyok GetBusinessObjectFromComponents_EREC_PldIratPeldanyok(EgyszerusitettIktatasAdatok importRowObj)
        {
            EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_PldIratPeldanyok.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

            if (!String.IsNullOrEmpty(importRowObj.CimzettNeve))
            {
                if (_partnerNevekDict.ContainsKey(importRowObj.CimzettNeve))
                {
                    erec_PldIratPeldanyok.Partner_Id_Cimzett = _partnerNevekDict[importRowObj.CimzettNeve].ToString();
                    erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;
                }
                else
                {
                    // Ha nincs ilyen partner a partnertörzsben, attól még hibát nem dobunk, szövegesen úgyis le lesz tárolva...
                }
            }

            erec_PldIratPeldanyok.NevSTR_Cimzett = importRowObj.CimzettNeve;
            erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

            erec_PldIratPeldanyok.KuldesMod = _util.GetKodtarKodByKodtarNev(kcs_KULDEMENY_KULDES_MODJA, importRowObj.ExpedialasModja);
            erec_PldIratPeldanyok.Updated.KuldesMod = true;

            // BUG_11059
            var adathordozoTipusa = _util.GetKodtarKodByKodtarNev(kcs_UGYINTEZES_ALAPJA, importRowObj.AdathordozoTipusa);
            var isElektronikus = adathordozoTipusa == // iratpéldány ügyintézés módja
                KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

            var cimzettCime = new TomegesIktatasCim
            {
                OrszagNev = importRowObj.OrszagNev,
                IRSZ = importRowObj.IRSZ,
                TelepulesNev = importRowObj.TelepulesNev,
                KozteruletNev = importRowObj.KozteruletNev,
                KozteruletTipusNev = importRowObj.KozteruletTipusNev,
                Hazszam = importRowObj.Hazszam,
                Hazszamig = importRowObj.Hazszamig,
                HazszamBetujel = importRowObj.HazszamBetujel,
                Lepcsohaz = importRowObj.Lepcsohaz,
                Emelet = importRowObj.Emelet,
                Ajto = importRowObj.Ajto,
                AjtoBetujel = importRowObj.AjtoBetujel,
                HRSZ = importRowObj.HRSZ,
                Postafiok = importRowObj.Postafiok,
                ElektronikusCim = importRowObj.ElektronikusCim
            };

            var cimzettKrtCim = GetCimzettCim(cimzettCime, erec_PldIratPeldanyok.KuldesMod, isElektronikus);
            if (cimzettKrtCim != null)
            {
                erec_PldIratPeldanyok.Cim_id_Cimzett = cimzettKrtCim.Id;
                erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                erec_PldIratPeldanyok.CimSTR_Cimzett = Contentum.eUtility.CimUtility.MergeCim(cimzettKrtCim, true);
                erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;
            }

            erec_PldIratPeldanyok.Visszavarolag = _util.GetKodtarKodByKodtarNev(kcs_VISSZAVAROLAG, importRowObj.Elvaras);
            erec_PldIratPeldanyok.Updated.Visszavarolag = true;

            if (!String.IsNullOrEmpty(importRowObj.ElvartHatarido))
            {
                DateTime dat;
                if (DateTime.TryParse(importRowObj.ElvartHatarido, out dat))
                {
                    erec_PldIratPeldanyok.VisszaerkezesiHatarido = dat.ToString();
                    erec_PldIratPeldanyok.Updated.VisszaerkezesiHatarido = true;
                }
                else
                {
                    throw new ResultException(String.Format("A megadott 'Elvárt határidő' nem megfelelő formátumú: '{0}'", importRowObj.ElvartHatarido));
                }
            }

            erec_PldIratPeldanyok.BarCode = importRowObj.Vonalkod;
            erec_PldIratPeldanyok.Updated.BarCode = true;

            // BUG#6627:
            erec_PldIratPeldanyok.UgyintezesModja = _util.GetKodtarKodByKodtarNev(kcs_UGYINTEZES_ALAPJA, importRowObj.AdathordozoTipusa);
            erec_PldIratPeldanyok.Updated.UgyintezesModja = true;

            return erec_PldIratPeldanyok;
        }

        private IktatasiParameterek GetIktatasiParameterek(EgyszerusitettIktatasAdatok importRowObj
                    , Guid? elozmenyUgyiratId, string ugytipus, string irattariTetelszamId
                    , out bool isAlszamraIktatas, out Guid? iktatoKonyvId)
        {
            var iktatasiParameterek = CreateIktatasiParameterek(importRowObj, elozmenyUgyiratId, ugytipus, irattariTetelszamId, out isAlszamraIktatas, out iktatoKonyvId);

            string ugyiratpeldanySzukseges = UtilityExcel.ConvertIgenNemErtekToDbIgenNem(importRowObj.UgyiratpeldanySzukseges);

            if (String.IsNullOrEmpty(ugyiratpeldanySzukseges))
                ugyiratpeldanySzukseges = "1";

            iktatasiParameterek.UgyiratPeldanySzukseges = ugyiratpeldanySzukseges == "1";

            return iktatasiParameterek;
        }

        private EREC_IratAlairok GetBusinessObjectFromComponents_EREC_IratAlairok(EgyszerusitettIktatasAdatok importRowObj)
        {
            if (String.IsNullOrEmpty(importRowObj.Alairo))
                return null;

            // kötelező mezők ellenőrzése
            if (String.IsNullOrEmpty(importRowObj.AlairoSzerepe))
            {
                _util.ThrowRequiredFieldException("Aláíró szerepe");
            }
            if (String.IsNullOrEmpty(importRowObj.AlairasTipusa))
            {
                _util.ThrowRequiredFieldException("Aláírás típusa");
            }

            // irat aláíró
            EREC_IratAlairok erec_IratAlairok = new EREC_IratAlairok();
            erec_IratAlairok.Updated.SetValueAll(false);
            erec_IratAlairok.Base.Updated.SetValueAll(false);

            if (_csoportNevekDict.ContainsKey(importRowObj.Alairo))
            {
                erec_IratAlairok.FelhasznaloCsoport_Id_Alairo = _csoportNevekDict[importRowObj.Alairo].ToString();
                erec_IratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;
            }
            else
            {
                // Nincs ilyen nevű fehasználó/csoport:
                throw new ResultException(String.Format("Nincs ilyen nevű csoport: '{0}'", importRowObj.Alairo));
            }

            erec_IratAlairok.AlairoSzerep = _util.GetKodtarKodByKodtarNev(kcs_ALAIRO_SZEREP, importRowObj.AlairoSzerepe);
            erec_IratAlairok.Updated.AlairoSzerep = true;

            erec_IratAlairok.AlairasMod = importRowObj.AlairasTipusa;
            erec_IratAlairok.Updated.AlairasMod = true;

            erec_IratAlairok.AlairasSorrend = "1";
            erec_IratAlairok.Updated.AlairasSorrend = true;

            erec_IratAlairok.AlairasDatuma = DateTime.Now.ToString();
            erec_IratAlairok.Updated.AlairasDatuma = true;

            return erec_IratAlairok;
        }

        private KRT_Cimek GetCimzettCim(TomegesIktatasCim cim, string kuldesMod, bool isElektronikus)
        {
            if (!cim.IsValid(isElektronikus))
            {
                var krtCimek = cim.GetKRT_Cimek(kuldesMod, isElektronikus);
                throw new ResultException("A címzett címe érvénytelen: " + CimUtility.MergeCim(krtCimek, true));
            }

            var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
            var cimSearch = cim.GetKRT_Cimek_Search();

            Result res = cimekService.GetAll(_execParam, cimSearch);
            res.CheckError();

            string cimId;
            if (res.GetCount > 0)
            {
                cimId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else // új cím felvétele feloldással
            {
                var krt_cim = cim.GetKRT_Cimek(kuldesMod, isElektronikus);
                var resultInsert = cimekService.InsertWithFKResolution(_execParam, krt_cim);
                resultInsert.CheckError();
                cimId = resultInsert.Uid;
            }

            // cím lekérése
            if (!String.IsNullOrEmpty(cimId))
            {
                var execParam = _execParam.Clone();
                execParam.Record_Id = cimId;
                var resultGet = cimekService.Get(execParam);
                resultGet.CheckError();
                return resultGet.Record as KRT_Cimek;
            }

            return null;
        }

        #endregion

        /// <summary>
        /// A feltöltött import csv fájl feldolgozása
        /// </summary>
        /// <param name="uploadedFilePath"></param>
        /// <returns>A csv fájlból beolvasott 'EgyszerusitettIktatasAdatok' rekordok</returns>
        private List<EgyszerusitettIktatasAdatok> ProcessImportCsv(byte[] csvFileContent, bool ignoreOutputColumns)
        {
            var classMap = new EgyszerusitettIktatasAdatokCsvMap();

            if (ignoreOutputColumns)
            {
                // Kikapcsoljuk a beolvasásból az eredmény oszlopokat:
                classMap.Map(e => e.ResultHibaUzenet).Ignore();
                classMap.Map(e => e.ResultIktatoszam).Ignore();
                classMap.Map(e => e.CsatolmanyUploadStatus).Ignore();
                classMap.Map(e => e.AlairasStatus).Ignore();
                classMap.Map(e => e.ExpedialasStatus).Ignore();
            }

            using (MemoryStream memStream = new MemoryStream(csvFileContent))
            using (var package = new ExcelPackage(memStream))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.First();

                var items = ws.ConvertSheetToObjects<EgyszerusitettIktatasAdatok>(classMap);

                return items.Where(s => !s.IsEmpty()).ToList();
            }
        }

        #endregion
    }

    /// <summary>
    /// Egyszerűsített iktatás (érkeztetés+iktatás) képernyő adatai (Ezek a mezők lesznek az import CSV fájlban.) 
    /// </summary>
    public class EgyszerusitettIktatasAdatok : EgyszerusitettIktatasAdatokBase
    {
        #region Properties

        #region Irat adatok

        public string KiadmanyozniKell { get; set; }

        #endregion

        #region példány adatok

        public string CimzettNeve { get; set; }
        public string ExpedialasModja { get; set; }
        public string Elvaras { get; set; }
        public string ElvartHatarido { get; set; }
        public string Vonalkod { get; set; }
        public string UgyiratpeldanySzukseges { get; set; }

        // BUG_11059, CimzettCime helyett:
        public string OrszagNev { get; set; }
        public string IRSZ { get; set; }
        public string TelepulesNev { get; set; }
        public string KozteruletNev { get; set; }
        public string KozteruletTipusNev { get; set; }
        public string Hazszam { get; set; }
        public string Hazszamig { get; set; }
        public string HazszamBetujel { get; set; }
        public string Lepcsohaz { get; set; }
        public string Emelet { get; set; }
        public string Ajto { get; set; }
        public string AjtoBetujel { get; set; }
        public string HRSZ { get; set; }
        public string Postafiok { get; set; }
        public string ElektronikusCim { get; set; }

        #endregion

        #region Aláírás

        public string Alairo { get; set; }
        public string AlairoSzerepe { get; set; }
        public string AlairasTipusa { get; set; }
        public string AlairasStatus { get; set; }
        #endregion

        public string ExpedialniKell { get; set; }
        public string ExpedialasStatus { get; set; }

        // BUG_6058
        public string AzonosCimzettnekEgyKuldemeny { get; set; }
        public string KuldemenyAutomatikusAtadasa { get; set; }
        #endregion

        /// <summary>
        /// Fejlécet tartalmazó csv fájl tartalom
        /// </summary>
        /// <param name="ignoreResultColumns">Az output oszlopokat kihagyja-e (pl. Iktatószám, Hibaüzenet)</param>
        public static byte[] GetExcleHeaderContent(bool ignoreOutputColumns)
        {
            var classMap = new EgyszerusitettIktatasAdatokCsvMap();

            if (ignoreOutputColumns)
            {
                classMap.Map(e => e.ResultHibaUzenet).Ignore();
                classMap.Map(e => e.ResultIktatoszam).Ignore();
                classMap.Map(e => e.CsatolmanyUploadStatus).Ignore();
                classMap.Map(e => e.AlairasStatus).Ignore();
                classMap.Map(e => e.ExpedialasStatus).Ignore();
            }

            using (var p = new ExcelPackage())
            {
                var ws = p.Workbook.Worksheets.Add("Iktatas");

                ws.WriteHeader<EgyszerusitettIktatasAdatok>(classMap);
                ws.Cells.AutoFitColumns(100);

                byte[] csvContent = p.GetAsByteArray();

                return csvContent;
            }
        }
    }

    public class EgyszerusitettIktatasAdatokCsvMap : CsvHelper.Configuration.CsvClassMap<EgyszerusitettIktatasAdatok>
    {
        private readonly string[] _order;

        private void MapNameOrder(Expression<Func<EgyszerusitettIktatasAdatok, object>> o, string name)
        {
            var index = _order.ToList().IndexOf(name);
            if (index >= 0) {
                Map(o).Name(name).Index(index);
            }
        }

        public int GetOrder(string name)
        {
            var propertyMap = PropertyMaps.FirstOrDefault(m => !m.Data.Ignore && m.Data.Property.Name == name);
            return propertyMap != null ? propertyMap.Data.Index : 0;
        }

        public EgyszerusitettIktatasAdatokCsvMap()
        {
            // excelből másolva
            var tabSeparatedOrder = "Iktatószám	Hibaüzenet	Kiadmányozni kell?	Címzett neve	Expediálás módja	Ország	Irányítószám	Település	Közterület neve	Közterület típusa	Házszám(-tól)	Házszám-ig	Házszám betűjel	Lépcsőház	Emelet	Ajtó	Ajtó betűjele	Helyrajzi szám	Postafiók száma	Elektronikus cím	Elvárás	Elvárt határidő	Vonalkód	Ügyiratpéldány szükséges?	Aláíró	Aláíró szerepe	Aláírás típusa	Aláíró felvétele	Expediálni kell?	Expediálás eredménye	Adószám/Adó azonosító	Azonos címzettnek egy küldemény	Küldemény automatikus átadása	Előzmény ügyirat	Ügy fajtája	Irattári tételszám	Iktatókönyv	Ügy típusa	Ügyirat tárgya	Ügyirat ügyintézési ideje	Ügyirat ügyintézési határideje	Szervezeti egység	Ügyirat ügyintéző	Kezelő	Ügyfél, ügyindító	Ügyindító címe	Irat tárgya	Irat típus	Irat minősítése	Irat ügyintéző	Adathordozó típusa	Elsődleges adathordozó típusa	Irat elintézési időpontja	Irat ügyintézési ideje	Irat ügyintézési határideje	Felelős szervezet	Ügyintézés módja	Irat iránya	Irat hatása az ügyintézésre (sakkóra)	Csatolmány URL	Csatolmány feltöltés eredmény";
            _order = tabSeparatedOrder.Split('\t');

            //
            AutoMap(false, false);

            MapNameOrder(m => m.ResultIktatoszam, "Iktatószám");
            MapNameOrder(m => m.ResultHibaUzenet, "Hibaüzenet");

            #region Ügyirat adatok

            MapNameOrder(m => m.ElozmenyUgyirat, "Előzmény ügyirat");
            //Map(m => m.ElozmenyUgyiratEv).Name("Év (Előzmény ügyirat)");
            //Map(m => m.ElozmenyUgyiratIktatokonyv).Name("Iktatókönyv (Előzmény ügyirat)");
            //Map(m => m.ElozmenyUgyiratFoszam).Name("Főszám (Előzmény ügyirat)");
            MapNameOrder(m => m.UgyFajtaja, "Ügy fajtája");
            MapNameOrder(m => m.IrattariTetelszam, "Irattári tételszám");
            MapNameOrder(m => m.Iktatokonyv, "Iktatókönyv");
            MapNameOrder(m => m.UgyTipusa, "Ügy típusa");
            MapNameOrder(m => m.UgyiratTargya, "Ügyirat tárgya");
            MapNameOrder(m => m.UgyiratUgyintezesiIdeje, "Ügyirat ügyintézési ideje");
            MapNameOrder(m => m.UgyiratUgyintezesiHatarideje, "Ügyirat ügyintézési határideje");
            MapNameOrder(m => m.SzervezetiEgyseg, "Szervezeti egység");
            MapNameOrder(m => m.UgyiratUgyintezo, "Ügyirat ügyintéző");
            MapNameOrder(m => m.Kezelo, "Kezelő");
            MapNameOrder(m => m.UgyfelUgyindito, "Ügyfél, ügyindító");
            MapNameOrder(m => m.UgyinditoCime, "Ügyindító címe");

            #endregion

            #region Irat adatok

            MapNameOrder(m => m.IratTargya, "Irat tárgya");
            MapNameOrder(m => m.IratTipus, "Irat típus");
            MapNameOrder(m => m.KiadmanyozniKell, "Kiadmányozni kell?");
            MapNameOrder(m => m.IratMinositese, "Irat minősítése");
            MapNameOrder(m => m.IratUgyintezo, "Irat ügyintéző");
            MapNameOrder(m => m.AdathordozoTipusa, "Adathordozó típusa");
            MapNameOrder(m => m.ElsodlegesAdathordozoTipusa, "Elsődleges adathordozó típusa");
            MapNameOrder(m => m.IratElintezesiIdopontja, "Irat elintézési időpontja");
            MapNameOrder(m => m.IratUgyintezesiIdeje, "Irat ügyintézési ideje");
            MapNameOrder(m => m.IratUgyintezesiHatarideje, "Irat ügyintézési határideje");
            MapNameOrder(m => m.FelelosSzervezet, "Felelős szervezet");
            MapNameOrder(m => m.UgyintezesModja, "Ügyintézés módja");
            MapNameOrder(m => m.IratIranya, "Irat iránya");
            MapNameOrder(m => m.IratHatasaAzUgyintezesre, "Irat hatása az ügyintézésre (sakkóra)");

            #endregion

            #region Példány adatok

            MapNameOrder(m => m.CimzettNeve, "Címzett neve");
            MapNameOrder(m => m.ExpedialasModja, "Expediálás módja");

            MapNameOrder(m => m.OrszagNev, "Ország");
            MapNameOrder(m => m.IRSZ, "Irányítószám");
            MapNameOrder(m => m.TelepulesNev, "Település");
            MapNameOrder(m => m.KozteruletNev, "Közterület neve");
            MapNameOrder(m => m.KozteruletTipusNev, "Közterület típusa");
            MapNameOrder(m => m.Hazszam, "Házszám(-tól)");
            MapNameOrder(m => m.Hazszamig, "Házszám-ig");
            MapNameOrder(m => m.HazszamBetujel, "Házszám betűjel");
            MapNameOrder(m => m.Lepcsohaz, "Lépcsőház");
            MapNameOrder(m => m.Emelet, "Emelet");
            MapNameOrder(m => m.Ajto, "Ajtó");
            MapNameOrder(m => m.AjtoBetujel, "Ajtó betűjele");
            MapNameOrder(m => m.HRSZ, "Helyrajzi szám");
            MapNameOrder(m => m.Postafiok, "Postafiók száma");
            MapNameOrder(m => m.ElektronikusCim, "Elektronikus cím");

            MapNameOrder(m => m.Elvaras, "Elvárás");
            MapNameOrder(m => m.ElvartHatarido, "Elvárt határidő");
            MapNameOrder(m => m.Vonalkod, "Vonalkód");
            MapNameOrder(m => m.UgyiratpeldanySzukseges, "Ügyiratpéldány szükséges?");

            #endregion

            MapNameOrder(m => m.CsatolmanyUrls, "Csatolmány URL");
            MapNameOrder(m => m.CsatolmanyUploadStatus, "Csatolmány feltöltés eredmény");

            #region Aláírás

            MapNameOrder(m => m.Alairo, "Aláíró");
            MapNameOrder(m => m.AlairoSzerepe, "Aláíró szerepe");
            MapNameOrder(m => m.AlairasTipusa, "Aláírás típusa");
            MapNameOrder(m => m.AlairasStatus, "Aláíró felvétele");

            #endregion

            MapNameOrder(m => m.ExpedialniKell, "Expediálni kell?");
            MapNameOrder(m => m.ExpedialasStatus, "Expediálás eredménye");

            // BUG_6058
            MapNameOrder(m => m.Adoszam, "Adószám/Adó azonosító");
            MapNameOrder(m => m.AzonosCimzettnekEgyKuldemeny, "Azonos címzettnek egy küldemény");
            MapNameOrder(m => m.KuldemenyAutomatikusAtadasa, "Küldemény automatikus átadása");
        }
    }

    /// <summary>
    /// A tömeges iktatásra feltöltött import fájl egy eleme
    /// </summary>
    public class IktatasImportCsvItem : IktatasImportCsvItemBase
    {
        #region Properties

        public EgyszerusitettIktatasAdatok Imp { get { return ImportRowObj as EgyszerusitettIktatasAdatok; } }

        public EREC_PldIratPeldanyok PeldanyObj { get; set; }

        public EREC_IratAlairok AlairokObj { get; set; }

        public string ExpedialniKell { get; set; }

        public string KuldemenyAutomatikusAtadasaCsoportId { get; set; }

        #endregion

        private bool _isNMHH;

        #region Constructor

        public IktatasImportCsvItem(int rowNumber, EgyszerusitettIktatasAdatok importRowObj, ExecParam execParam, TomegesIktatasHelper helper, bool isNMHH)
        {
            RowNumber = rowNumber;
            ImportRowObj = importRowObj;
            ExecParam = execParam;
            _helper = helper;
            _isNMHH = isNMHH;
        }

        #endregion

        #region Public methods

        string Validate(IktatoObjektum iktatoObjektum)
        {
            if (iktatoObjektum == null || Imp == null)
            {
                return "Az iktatóobjektum nincs megadva.";
            }

            if (!String.IsNullOrEmpty(Imp.ResultIktatoszam))
            {
                var s = String.Format("{0}. sor: Van iktatószám, nem kell iktatni: {1}", RowNumber, Imp.ResultIktatoszam);
                Logger.Debug(s);

                return s;
            }

            return "";
        }

        private T[] Remove<T>(List<T> list, int index)
        {
            list.RemoveAt(index);
            return list.ToArray();
        }

        private string Prepare(IktatoObjektum iktatoObjektum)
        {
            if (iktatoObjektum == null || IratObj == null)
            {
                return "Az iktatóobjektum nincs megadva.";
            }

            IktatoCsoport csoport = _helper.IktatoCsoportok.FirstOrDefault(x => x.Id == iktatoObjektum.CsoportId);
            if (csoport == null)
            {
                return "Az iktatócsoport nem található.";
            }

            if (csoport.kapottIratId == null || csoport.kapottAlszam == null)
            {
                return "Az iktatócsoport nincs inicializálva.";
            }

            int foszam = iktatoObjektum.GetFoszam();
            string alszam;
            int index;

            index = foszam < 1 ? 0 : Array.FindIndex(csoport.kapottFoszam, x => x == foszam);
            if (index < 0 || index >= csoport.kapottFoszam.Length)
            {
                return "A főszám nem található.";
            }

            foszam = csoport.kapottFoszam[index];
            alszam = csoport.kapottAlszam[index];

            IratObj.Id = csoport.kapottIratId[index].ToString();
            IratObj.Azonosito = csoport.kapottAzonosito[index];

            csoport.kapottAlszam = Remove(csoport.kapottAlszam.ToList(), index);
            csoport.kapottIratId = Remove(csoport.kapottIratId.ToList(), index);
            csoport.kapottAzonosito = Remove(csoport.kapottAzonosito.ToList(), index);
            csoport.kapottFoszam = Remove(csoport.kapottFoszam.ToList(), index);

            return "";
        }

        public EREC_TomegesIktatasTetelek GetTomegesIktatasTetel(IktatoObjektum iktatoObjektum)
        {
            // validálás
            var validateErr = Validate(iktatoObjektum);
            if (!String.IsNullOrEmpty(validateErr))
            {
                IktatasHibaUzenet = "A validálás nem sikerült: " + validateErr;
                IktatasCompleted = true;
                return null;
            }

            // előkészítés
            var prepareErr = Prepare(iktatoObjektum);
            if (!String.IsNullOrEmpty(prepareErr))
            {
                IktatasHibaUzenet = "Az előkészítés nem sikerült: " + prepareErr;
                IktatasCompleted = true;
                return null;
            }

            var result = new EREC_TomegesIktatasTetelek()
            {
                Azonosito = IratObj.Azonosito,
                Irat_Id = IratObj == null ? "" : IratObj.Id,
                Iktatokonyv_Id = IktatokonyvId.HasValue ? IktatokonyvId.Value.ToString() : "",
                Ugyirat_Id = UgyiratObj == null ? "" : UgyiratObj.Id,

                Forras_Azonosito = TomegesIktatasFolyamat.Forras_Excel,

                IktatasTipus = IratObj != null && IratObj.PostazasIranya == "1" ? "0" : "1", // StartInitTetelek, ASPADOHelper.BuildDocumentFromIratok
                Alszamra = IsAlszamraIktatas ? "1" : "0",

                IktatasiParameterek = JsonConvert.SerializeObject(IktatasiParameterek),
                ErkeztetesParameterek = JsonConvert.SerializeObject(new ErkeztetesParameterek()), // bejövő irat iktatáshoz kell csak

                EREC_IraIratok = JsonConvert.SerializeObject(IratObj),
                EREC_UgyUgyiratok = JsonConvert.SerializeObject(UgyiratObj),
                EREC_PldIratPeldanyok = JsonConvert.SerializeObject(PeldanyObj),
                EREC_UgyUgyiratdarabok = JsonConvert.SerializeObject(new EREC_UgyUgyiratdarabok()),
                EREC_IratAlairok = JsonConvert.SerializeObject(AlairokObj),
                ExecParam = JsonConvert.SerializeObject(ExecParam),

                //EREC_HataridosFeladatok = 
                //EREC_KuldKuldemenyek = // bejövő irat iktatáshoz kell csak
                //Kuldemeny_Id = // bejövő irat iktatáshoz kell csak

                Dokumentumok = ImportRowObj.CsatolmanyUrls,

                Expedialas = ExpedialniKell,
                KuldemenyAtadas = KuldemenyAutomatikusAtadasaCsoportId,
                TobbIratEgyKuldemenybe = Imp.AzonosCimzettnekEgyKuldemeny,
                // NMHH: irat statisztika nélkül nem lehet iratot elintézni, csak az aláíró rögzítéséig lehet elmenni a folyamattal
                Lezarhato = _isNMHH || String.IsNullOrEmpty(Imp.CsatolmanyUrls) || Imp.CsatolmanyUrls.Trim().Length == 0 ? "0" : "1" // csatolmány kell az elintézéshez
            };

            // updated
            result.Updated.SetValueAll(false);
            result.Base.Updated.SetValueAll(false);

            result.Updated.Azonosito = true;
            result.Updated.Irat_Id = true;
            result.Updated.Iktatokonyv_Id = true;
            result.Updated.Ugyirat_Id = true;
            result.Updated.Forras_Azonosito = true;
            result.Updated.IktatasTipus = true;
            result.Updated.Alszamra = true;
            result.Updated.IktatasiParameterek = true;
            result.Updated.ErkeztetesParameterek = true;
            result.Updated.EREC_IraIratok = true;
            result.Updated.EREC_UgyUgyiratok = true;
            result.Updated.EREC_UgyUgyiratdarabok = true;
            result.Updated.EREC_PldIratPeldanyok = true;
            result.Updated.EREC_IratAlairok = true;
            result.Updated.ExecParam = true;
            result.Updated.Expedialas = true;
            result.Updated.KuldemenyAtadas = true;
            result.Updated.TobbIratEgyKuldemenybe = true;
            result.Updated.Lezarhato = true;

            return result;
        }

        /// <summary>
        /// Aláírás log bejegyzése
        /// </summary>
        /// <returns></returns>
        public string GetAlairasLogMsg()
        {
            string logMsg = RowNumber + ". sor: ";

            if (String.IsNullOrEmpty(Imp.Alairo))
            {
                logMsg += "Nincs aláíró";
            }
            else
            {
                logMsg += "Aláíró felvétele: " + Imp.AlairasStatus;
            }

            return logMsg;
        }

        /// <summary>
        /// Expediálás log bejegyzése
        /// </summary>
        /// <returns></returns>
        public string GetExpedialasLogMsg()
        {
            string logMsg = RowNumber + ". sor: ";

            if (ExpedialniKell != "1")
            {
                logMsg += "Nem kell expediálni";
            }
            else
            {
                logMsg += "Expediálás eredménye: " + Imp.ExpedialasStatus;
            }

            return logMsg;
        }

        /// <summary>
        /// Hiba naplózása
        /// </summary>
        /// <returns></returns>
        public string GetResultLogMsg()
        {
            if (!String.IsNullOrEmpty(Imp.ResultHibaUzenet))
            {
                return RowNumber + ". sor: Hiba: " + Imp.ResultHibaUzenet;
            }

            return "";
        }


        #endregion
    }

    /// <summary>
    /// A háttérszálon futtatott iktatási adatok
    /// </summary>
    public class TomegesIktatasBackgroundItem : TomegesIktatasBackgroundItemBase
    {
        private UtilityTomegesIktatas _util;
        private Dictionary<string, IktatasImportCsvItem> _TomegesIktatasTetel_csvItem_Map;

        public TomegesIktatasBackgroundItem(List<IktatasImportCsvItemBase> iktatasAdatokList, TomegesIktatasHelper helper, UtilityTomegesIktatas util,
            ExecParam execParam, int prio) : base(iktatasAdatokList, helper, execParam, prio)
        {
            _util = util;
            _TomegesIktatasTetel_csvItem_Map = new Dictionary<string, IktatasImportCsvItem>();
        }

        private IktatasImportCsvItem GetTetelCsvItem(EREC_TomegesIktatasTetelek tetel)
        {
            if (tetel == null || !_TomegesIktatasTetel_csvItem_Map.ContainsKey(tetel.Id))
            {
                return null;
            }

            return _TomegesIktatasTetel_csvItem_Map[tetel.Id];
        }

        private List<IktatasImportCsvItem> TetelhezRendeles(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            var kimenoIktatasAdatok = IktatasAdatokList.Cast<IktatasImportCsvItem>().ToList();

            // tétel id-khoz Excel sorok rendelése
            int idx = 0;
            foreach (var tetel in tetelek)
            {
                var csvItem = kimenoIktatasAdatok[idx];
                if (tetel != null)
                {
                    _TomegesIktatasTetel_csvItem_Map[tetel.Id] = csvItem;
                }
                idx++;
            }

            return kimenoIktatasAdatok;
        }

        private void JobValidalas(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            var kimenoIktatasAdatok = TetelhezRendeles(tetelek);
            foreach (var tetel in tetelek)
            {
                var csvItem = GetTetelCsvItem(tetel);
                if (csvItem != null)
                {
                    if (csvItem.AlairokObj != null && !String.IsNullOrEmpty(csvItem.Imp.Alairo))
                    {
                        csvItem.Imp.AlairasStatus = "OK";
                    }
                }
            }
        }

        private void AzonnaliVegrehajtas(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            Log("Iktatás...");
            var kimenoIktatasAdatok = TetelhezRendeles(tetelek);
            _helper.StartExec();

            // sikeres iktatás állapot beállítása
            var xp = _execParam.Clone();
            tetelek = _helper.Folyamat.GetTetelek(xp, KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Iktatott);

            tetelek.ToList().ForEach(t =>
            {
                var csvItem = GetTetelCsvItem(t);
                if (csvItem != null)
                {
                    var hasId = !String.IsNullOrEmpty(t.Azonosito);
                    csvItem.Imp.ResultIktatoszam = hasId ? t.Azonosito : IktatasImportCsvItemBase.ResultIktatoszam_Hiba;
                    if (!hasId)
                    {
                        csvItem.IktatasHibaUzenet = "Iktatás OK, Iktatószám: ??";
                    }
                    csvItem.IktatasCompleted = true;
                }
            }
            );

            // iktatás hibaüzenet beállítása a hibás tételekre
            var hibasTetelek = _helper.Folyamat.GetTetelek(xp, KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Hibas);
            hibasTetelek.ToList().ForEach(t =>
            {
                var csvItem = GetTetelCsvItem(t);
                if (csvItem != null)
                {
                    csvItem.IktatasHibaUzenet = t.Hiba;
                    Log(csvItem.GetIktatasLogMsg());
                }
            }
            );

            // utóműveletek
            _helper.SetTetelek(tetelek);

            CsatolmanyFeltoltes();

            if (tetelek.ToList().Count > 0)
            {
                Log("Utóműveletek...");

                var postOperationErrors = _helper.PostOperations();

                // utóműveletek hibáinak logolása
                //var sb = new StringBuilder();
                //hibasTetelek.ForEach(hibasTetel => { if (!String.IsNullOrEmpty(hibasTetel.Hiba)) { sb.AppendLine(hibasTetel.Hiba); } });

                var hibasUtomuveletTetelek = postOperationErrors.Where(e => e.Tetel != null).Select(e => e.Tetel);

                postOperationErrors.ForEach(hiba =>
                {
                    var hibasTetel = hiba == null ? null : hiba.Tetel;
                    if (hibasTetel != null && !String.IsNullOrEmpty(hibasTetel.Hiba))
                    {
                        // hiba visszaírása az excel sorba
                        var csvItem = GetTetelCsvItem(hibasTetel);
                        if (csvItem != null)
                        {
                            // őrző hiba
                            if (hibasTetel.Hiba == "[80012]" || hibasTetel.Hiba == "[800120]")
                            {
                                hibasTetel.Hiba += " Hiba az elintézés alatt. A dokumentumnak nem őrzője.";
                            }
                            // többi üzenet feloldása
                            else if (hibasTetel.Hiba.StartsWith("[") && hibasTetel.Hiba.EndsWith("]"))
                            {
                                hibasTetel.Hiba += " " + ResultError.ErrorMessageCodeToDetailedText(hibasTetel.Hiba);
                            }

                            var hibauz = "HIBA: " + hibasTetel.Hiba;
                            //Log(hibauz);

                            if (hiba.TomegesIktatasMuvelet == TomegesIktatasMuvelet.Expedialas)
                            {
                                csvItem.Imp.ExpedialasStatus = hibauz;
                            }
                            else if (hiba.TomegesIktatasMuvelet == TomegesIktatasMuvelet.Alairas)
                            {
                                csvItem.Imp.AlairasStatus = hibauz;
                            }
                            else if (hiba.TomegesIktatasMuvelet == TomegesIktatasMuvelet.IntezkedesreAtvetel || hiba.TomegesIktatasMuvelet == TomegesIktatasMuvelet.Lezaras)
                            {
                                if (String.IsNullOrEmpty(csvItem.Imp.ResultHibaUzenet))
                                {
                                    csvItem.Imp.ResultHibaUzenet = hibauz;
                                }
                                else
                                {
                                    csvItem.Imp.ResultHibaUzenet += " - " + hibasTetel.Hiba;
                                }
                            }
                        }
                    }
                });

                // sikeres állapot beállítása
                tetelek.ToList().ForEach(t =>
                {
                    if (!hibasTetelek.Any(ht => ht.Id == t.Id) && !hibasUtomuveletTetelek.Any(ht => ht.Id == t.Id))
                    {
                        var csvItem = GetTetelCsvItem(t);
                        if (csvItem != null)
                        {
                            if (csvItem.AlairokObj != null && !String.IsNullOrEmpty(csvItem.Imp.Alairo)) { csvItem.Imp.AlairasStatus = "OK"; }
                            // BUG_15596
                            if (t.Expedialas == "1" && !String.IsNullOrEmpty(t.Hiba))
                            {
                                csvItem.Imp.ExpedialasStatus = t.Hiba;
                            }
                            else
                            {
                                csvItem.Imp.ExpedialasStatus = "OK";
                            }
                        }
                    }
                }
                );
            }

            // utóművelet logok összegyűjtése
            kimenoIktatasAdatok.ForEach(i => Log(i.GetAlairasLogMsg()));
            kimenoIktatasAdatok.ForEach(i => Log(i.GetExpedialasLogMsg()));
            kimenoIktatasAdatok.ForEach(i => Log(i.GetResultLogMsg()));

            kimenoIktatasAdatok.ForEach(i => i.IktatasCompleted = true);
            //IktatasHibaUzenet = sb.ToString();
            //Imp.ResultHibauzenet = sb.ToString();
        }

        private void CsatolmanyFeltoltes()
        {
            Log("Csatolmányok feltöltése...");

            // Csatolmányfeltöltés hívása soronként:
            foreach (var iktatasAdatok in IktatasAdatokList)
            {
                if (iktatasAdatok.CallCsatolmanyFeltoltes())
                {
                    string logMsg = iktatasAdatok.GetCsatolmanyFeltoltesLogMsg();
                    Log(logMsg);
                }
            }
        }

        /// <summary>
        /// Iktatás (és csatolmány feltöltés) indítása
        /// </summary>
        public override void StartIktatas(byte[] csvContent)
        {
            try
            {
                Log("Iktatás előkészítése...");
                var azonnali = Prio < 3; // ha nem job indítja

                // dokumentum feltöltés
                Result uploadResult;
                using (var ms = new MemoryStream(csvContent))
                {
                    uploadResult = _helper.FileUpload(_execParam, ms, "RQ_Dokumentum", "");
                }

                // csatolmány feltöltés
                if (!azonnali)
                {
                    CsatolmanyFeltoltes();
                }

                // folyamat
                Result resultFolyamat = _helper.Folyamat.Insert(_execParam, TomegesIktatasFolyamat.Forras_Excel, Prio, uploadResult);
                if (resultFolyamat.IsError)
                {
                    _util.DisplayError(resultFolyamat.ErrorMessage);
                    return;
                }

                // iktatócsoportok létrehozása
                _helper.SetKuldemenyIktatoKonyv();

                var iktatoObjektumok = IktatasAdatokList.Cast<IktatasImportCsvItem>().Select(
                    iktatasImportCsvItem => new IktatoObjektum(iktatasImportCsvItem, _util)).ToList();
                _helper.CreateGroups(iktatoObjektumok.Cast<IktatoObjektumBase>());
                _helper.CallTomegesIktatasInitForGroupsSync();

                // tételek mentése a folyamatba
                var tetelek = iktatoObjektumok.Select(iktatoObjektum =>
                {
                    var tetel = iktatoObjektum == null || iktatoObjektum.CsvItem == null ? null : iktatoObjektum.CsvItem.GetTomegesIktatasTetel(iktatoObjektum);
                    if (tetel == null && iktatoObjektum != null && iktatoObjektum.CsvItem != null)
                    {
                        Log(iktatoObjektum.CsvItem.GetIktatasLogMsg());
                    }
                    return tetel;
                }).ToList();
                _helper.InsertItemsForProcess(tetelek);

                _helper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Inicializalt);

                // végrehajtás vagy job validálás
                if (azonnali)
                {
                    AzonnaliVegrehajtas(tetelek);
                }
                else // utóműveletekhez validálás
                {
                    JobValidalas(tetelek);
                }

                Finish("TomegesIktatasKimeno");
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString());
                BackgroundProcessError = exc.Message;
            }
            finally
            {
                BackgroundProcessEnded = true;
            }
        }

        /// <summary>
        /// Eredmény CSV fájl tartalma
        /// </summary>
        public override byte[] GetResultCsvContent()
        {
            var recordsToWrite = IktatasAdatokList.Select(e => e.ImportRowObj as EgyszerusitettIktatasAdatok).ToList();
            var classMap = new EgyszerusitettIktatasAdatokCsvMap();

            using (var p = new ExcelPackage())
            {
                var membersInfos = typeof(EgyszerusitettIktatasAdatok)
                       .GetProperties()
                       .OrderBy(pi => classMap.GetOrder(pi.Name))
                       .Cast<MemberInfo>()
                       .ToArray();

                var ws = p.Workbook.Worksheets.Add("Iktatas");
                ws.WriteHeader<EgyszerusitettIktatasAdatok>(classMap);
                ws.Cells[2, 1].LoadFromCollection(recordsToWrite, false, OfficeOpenXml.Table.TableStyles.None, BindingFlags.Public | BindingFlags.Instance, membersInfos);
                ws.Cells.AutoFitColumns(100);
                byte[] csvContent = p.GetAsByteArray();
                return csvContent;
            }
        }
    }

    #endregion
}
