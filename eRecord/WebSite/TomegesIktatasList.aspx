<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TomegesIktatasList.aspx.cs" Inherits="TomegesIktatasList" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

        <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeTomegesIktatas" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelTomegesIktatas" runat="server" OnLoad="updatePanelTomegesIktatas_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeTomegesIktatas" runat="server" TargetControlID="panelTomegesIktatas"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeTomegesIktatas" CollapseControlID="btnCpeTomegesIktatas"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeTomegesIktatas"
                            ExpandedSize="0" ExpandedText="T�meges iktat�s forr�sok list�ja" CollapsedText="K�dt�rak list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelTomegesIktatas" runat="server" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewTomegesIktatas" runat="server" GridLines="None" BorderWidth="1px" OnRowCommand="gridViewTomegesIktatas_RowCommand" 
                                     OnPreRender="gridViewTomegesIktatas_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                     DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewTomegesIktatas_RowDataBound" OnSorting="gridViewTomegesIktatas_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
                                            <asp:BoundField DataField="ForrasTipusNev" HeaderText="N�v" SortExpression="EREC_TomegesIktatas.ForrasTipusNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FelelosNev" HeaderText="Szervezeti egys�g" SortExpression="KRT_CsoportokFelelos.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AgazatiJelNev" HeaderText="�gazati jel" SortExpression="EREC_AgazatiJelek.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IraIrattariTetelNev" HeaderText="Iratt�ri t�tel" SortExpression="EREC_IraIrattariTetelek.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UgytipusNev" HeaderText="�gyt�pus" SortExpression="EREC_IratMetaDefinicio.UgytipusNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IrattipusNev" HeaderText="Iratt�pus" SortExpression="KRT_KodTarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TargyPrefix" HeaderText="T�rgy" SortExpression="EREC_TomegesIktatas.TargyPrefix">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkAlairasKell" CommandArgument="AlairasKell" CommandName="Sort"
                                                        runat="server">Al��r�s kell</asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbAlairasKell" runat="server" Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField Visible="false"  DataField="AlairasModNev" HeaderText="al��r�s m�d" SortExpression="KRT_AlairasTipusok.AlairasMod">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="AlairoNev" HeaderText="Al��r�" SortExpression="KRT_CsoportokAlairo.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="HelyettesNev" HeaderText="Helyettes" SortExpression="KRT_CsoportokHelyettes.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="AlairasSzabalyNev" HeaderText="Al��r�s szab�ly" SortExpression="KRT_AlairasSzabalyok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkHatosagiAdatlapKell" CommandArgument="HatosagiAdatlapKell" CommandName="Sort"
                                                        runat="server">Hat�s�gi adatlap kell</asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbHatosagiAdatlapKell" runat="server" Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField Visible="false" DataField="UgyFajtaja" HeaderText="�gy fajta" SortExpression="EREC_TomegesIktatas.UgyFajtaja">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="DontestHozta" HeaderText="D�nt�st hozta" SortExpression="EREC_TomegesIktatas.DontestHozta">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                           <asp:BoundField Visible="false" DataField="DontesFormaja" HeaderText="D�nt�s form�ja" SortExpression="EREC_TomegesIktatas.DontesFormaja">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="UgyintezesHataridore" HeaderText="�gyint�z�s hat�rid�re" SortExpression="EREC_TomegesIktatas.UgyintezesHataridore">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="MunkaorakSzama" HeaderText="Munka�r�k sz�ma" SortExpression="EREC_TomegesIktatas.MunkaorakSzama">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="EljarasiKoltseg" HeaderText="Elj�r�si k�lts�g" SortExpression="EREC_TomegesIktatas.EljarasiKoltseg">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="HatosagiEllenorzes" HeaderText="Hat�s�gi ellen�rz�s" SortExpression="EREC_TomegesIktatas.HatosagiEllenorzes">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="KozigazgatasiBirsagMerteke" HeaderText="K�zigazgat�si b�r�sg m�rt�ke" SortExpression="EREC_TomegesIktatas.KozigazgatasiBirsagMerteke">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="SommasEljDontes" HeaderText="Sommas elj�r�si d�nt�s" SortExpression="EREC_TomegesIktatas.SommasEljDontes">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="NyolcNapBelulNemSommas" HeaderText="Nyolc napon bel�l nem sommas" SortExpression="EREC_TomegesIktatas.NyolcNapBelulNemSommas">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="FuggoHatalyuHatarozat" HeaderText="F�gg� hat�ly� hat�rozat" SortExpression="EREC_TomegesIktatas.FuggoHatalyuHatarozat">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="FuggoHatalyuVegzes" HeaderText="F�gg� hat�ly� v�gz�s" SortExpression="EREC_TomegesIktatas.FuggoHatalyuVegzes">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="HatAltalVisszafizOsszeg" HeaderText="Hat�s�g �ltal visszafiz. �sszeg" SortExpression="EREC_TomegesIktatas.HatAltalVisszafizOsszeg">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="HatTerheloEljKtsg" HeaderText="Hat�s�got terhel� elj�r�si k�lts�g" SortExpression="EREC_TomegesIktatas.HatTerheloEljKtsg">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField Visible="false" DataField="FelfuggHatarozat" HeaderText="Felf�gg. hat�rozat" SortExpression="EREC_TomegesIktatas.FelfuggHatarozat">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>                                                                              
                                            <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="EREC_TomegesIktatas.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                       <PagerSettings Visible="False" />
                                     </asp:GridView>
                                   </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>
         </tr>
       </table>  

</asp:Content>

