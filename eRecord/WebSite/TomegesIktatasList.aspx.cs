using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord;

public partial class TomegesIktatasList : System.Web.UI.Page
{
    UI ui = new UI();
    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TomegesIktatasList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(EREC_TomegesIktatasSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.TomegesIktatasListHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.SearchVisible = false;
        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        //ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("TomegesIktatasSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTomegesIktatas.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("TomegesIktatasForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTomegesIktatas.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewTomegesIktatas.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewTomegesIktatas.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewTomegesIktatas;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewTomegesIktatas);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_TomegesIktatasSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            TomegesIktatasGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesIktatas" + CommandName.ViewHistory);

        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewTomegesIktatas);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void TomegesIktatasGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewTomegesIktatas", ViewState, "EREC_TomegesIktatas.ForrasTipusNev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewTomegesIktatas", ViewState);

        TomegesIktatasGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void TomegesIktatasGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_TomegesIktatasSearch search = (EREC_TomegesIktatasSearch)Search.GetSearchObject(Page, new EREC_TomegesIktatasSearch());

        search.OrderBy = Search.GetOrderBy("gridViewTomegesIktatas", ViewState, SortExpression, SortDirection);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        using (var service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
        {
            Result res = service.GetAllWithFK(ExecParam, search);

            UI.GridViewFill(gridViewTomegesIktatas, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewTomegesIktatas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbAlairasKell", "AlairasKell");
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbHatosagiAdatlapKell", "HatosagiAdatlapKell");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewTomegesIktatas_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeTomegesIktatas);
        ListHeader1.RefreshPagerLabel();

    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        TomegesIktatasGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeTomegesIktatas);
        TomegesIktatasGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewTomegesIktatas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewTomegesIktatas, selectedRowNumber, "check");

            //string id = gridViewTomegesIktatas.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
        }

    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("TomegesIktatasForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTomegesIktatas.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("TomegesIktatasForm.aspx"
            , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTomegesIktatas.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_TomegesIktatas";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelTomegesIktatas.ClientID);
        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelTomegesIktatas_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    TomegesIktatasGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewTomegesIktatas));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTomegesIktatas();
            TomegesIktatasGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedTomegesIktatas()
    {

        if (FunctionRights.GetFunkcioJog(Page, "TomegesIktatasInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewTomegesIktatas, EErrorPanel1, ErrorUpdatePanel);

            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            using (var service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
            {
                Result result = service.MultiInvalidate(execParams);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }


    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

    }



    //GridView Sorting esm�nykezel�je
    protected void gridViewTomegesIktatas_Sorting(object sender, GridViewSortEventArgs e)
    {
        TomegesIktatasGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewTomegesIktatas", ViewState, e.SortExpression));
    }

    #endregion
}