﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TomegesOlvasasiJog.aspx.cs" Inherits="TomegesOlvasasiJog" %>

<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>
<%@ Register src="~/eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc" %>
<%@ Register Src="~/Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>
<%@ Register Src="~/Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>
        
    <!--Frissítés jelzése-->
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <asp:UpdatePanel ID="MainPanelUpdatePanel" runat="server">
        <ContentTemplate>

            <uc:FormHeader ID="FormHeader1" runat="server" />

            <uc:InfoModalPopup ID="InfoModalPopup1" runat="server" />

            <div style="margin: 10px">
                <div>

                    <div style="text-align:left">
                        <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                            <asp:Label ID="UgyiratokGridHeader" runat="server" Text="Ügyiratok:" style="font-weight: bold; text-decoration: underline;"/>
                            <div style="margin-top:3px; max-height:250px;overflow-y:auto">
                                <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                    OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="GridViewBorder">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                    Visible="false" OnClientClick="return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám"
                                            SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezelő" SortExpression="Felelos_Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField SortExpression="Csoportok_UgyintezoNev.Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderTemplate>
                                                <asp:HyperLink ID="headerUgyintezo" runat="server" Text='<%#Contentum.eUtility.ForditasExpressionBuilder.GetForditas("BoundField_UgyiratUgyintezo", "Ügyintéző", "~/eRecordComponent/UgyiratokList.ascx") %>' />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="labelUgyintezo" runat="server" Text='<%#Eval("Ugyintezo_Nev") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Alszám/Db" SortExpression="EREC_UgyUgyiratok.UtolsoAlszam">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                <asp:Label ID="labelUtolsoAlszam" runat="server"
                                                    Text='<%#String.Format("{0} / {1}",Eval("UtolsoAlszam"),Eval("IratSzam"))%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="IratokListPanel" runat="server" Visible="false">
                            <asp:Label ID="IratokGridHeader" runat="server" Text="Iratok:" style="font-weight: bold; text-decoration: underline;"/>
                            <div style="margin-top:3px; max-height:250px;overflow-y:auto">
                                <asp:GridView ID="IraIratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                    OnRowDataBound="IraIratokGridView_RowDataBound"
                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="GridViewBorder">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                    Visible="false" OnClientClick="return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam, EREC_IraIratok.Alszam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Targy1" HeaderText="Irat&nbsp;tárgy" SortExpression="EREC_IraIratok.Targy">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ElsoIratPeldanyFelelos_Nev" HeaderText="Kezelő" SortExpression="Csoportok_Peldany_FelelosNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        </asp:BoundField>
                                        <asp:TemplateField SortExpression="Csoportok_UgyintezoNev.Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderTemplate>
                                                <asp:HyperLink ID="headerUgyintezo" runat="server" Text='<%#Contentum.eUtility.ForditasExpressionBuilder.GetForditas("BoundField_IratUgyintezo", "Ügyintéző", "~/IraIratokList.aspx") %>' />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="labelUgyintezo" runat="server" Text='<%#Eval("FelhasznaloCsoport_Id_Ugyintezo_Nev") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                <%-- Átiktatott állapot esetén (06) átiktatás utáni új iktatószám megjelenítése --%>
                                                <asp:Label ID="labelAllapotNev" runat="server" Text='<%# string.Concat(Eval("Allapot_Nev"), (Eval("Allapot") as string) == "06" && (Eval("UjIktatoSzam") as string) != null ? "<br />(" + (Eval("UjIktatoSzam") as string).Replace(" ", "&nbsp;") + ")" : "") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ElsoIratPeldanyOrzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_Peldany_OrzoNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        </asp:BoundField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                    </div>

                    <div style="margin: 10px">
                        <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                        <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                    </div>

                    <div>
                        <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        </asp:Panel>
                    </div>

                    <div>
                        <asp:UpdatePanel runat="server" ID="ListUpdatePanel">
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 10px; margin-bottom: 10px;">
                                    <tr>
                                        <td style="width: 50%; padding: 0px 10px;">
                                            <div style="text-align: left; padding-bottom: 2px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: auto">
                                                            <span style="font-weight: bold; text-decoration: underline;">Jogosultak:</span>
                                                        </td>
                                                        <td style="width: 100%; padding-left: 5px; padding-right: 8px">
                                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="100%" CausesValidation="false" OnTextChanged="TextBoxSearch_TextChanged" AutoPostBack="true" />
                                                        </td>
                                                        <td style="width: auto">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" AlternateText="Keresés" ImageUrl="~/images/hu/egyeb/kereses.gif" CausesValidation="false"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                        <td></td>
                                        <td style="width: 50%; padding: 0px 10px;">
                                            <%--<div style="text-align: left; padding-bottom: 2px;">
                                            <span style="font-weight: bold; text-decoration: underline;">Partner kapcsolatok:</span>
                                        </div>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%; padding: 0px 10px;">
                                            <asp:ListBox ID="CsoportokListSource" runat="server" Rows="15" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="AddButton" Text=">" Width="50px" Style="margin-bottom: 5px;" ToolTip="Hozzáad" CausesValidation="false" OnClick="AddButton_Click" />
                                            <asp:Button runat="server" ID="RemoveButton" Text="<" Width="50px" Style="margin-top: 5px;" ToolTip="Töröl" CausesValidation="false" OnClick="RemoveButton_Click" />
                                        </td>
                                        <td style="width: 50%; padding: 0px 10px;">
                                            <asp:ListBox ID="CsoportokListTarget" runat="server" Rows="15" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td colspan="3" style="padding: 10px;">
                                            <asp:Label ID="label5" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div style="width: 600px; margin: 0 auto; padding-left: 100px">
                        <uc:FormFooter ID="FormFooter1" runat="server" />
                    </div>
                </div>
            </div>
            </
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

