﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class TomegesOlvasasiJog : System.Web.UI.Page
{
    private UI ui = new UI();
    private String[] UgyiratokArray;
    private String[] IratokArray;
    public string maxTetelszam = "0";
    string ObjektumType { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TomegesOlvasasiJog");

        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
        FormFooter1.Command = CommandName.Modify;

        ObjektumType = Request.QueryString.Get(QueryStringVars.ObjektumType);

        if (ObjektumType == Constants.TableNames.EREC_UgyUgyiratok)
        {
            if (Session[Constants.SelectedUgyiratIds] != null)
                UgyiratokArray = Session[Constants.SelectedUgyiratIds].ToString().Split(',');
        }
        else if (ObjektumType == Constants.TableNames.EREC_IraIratok)
        {
            if (Session[Constants.SelectedIratIds] != null)
                IratokArray = Session[Constants.SelectedIratIds].ToString().Split(',');
        }
        else
        {
            //hiba
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);
        FormHeader1.HeaderTitle = Resources.Form.TomegesOlvasasiJogHeaderTitle;
        FormHeader1.DisableModeLabel = true;
        FormHeader1.DesignViewVisible = false;
        FormHeader1.ErrorPanel.Visible = false;

        FormFooter1.ButtonsClick += FormFooter1_ButtonsClick;

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        if (!IsPostBack)
        {
            LoadFormComponents();
            FillCsoportokListSource();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int cntKijeloltUgyiratok = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count;
        int cntKijeloltIratok = ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null).Count;
        int cntKijelolt = cntKijeloltUgyiratok + cntKijeloltIratok;
        labelTetelekSzamaDb.Text = cntKijelolt.ToString();
    }

    private void LoadFormComponents()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            FillUgyiratokGridView();
        }
        else if (IratokArray != null && IratokArray.Length > 0)
        {
            IratokListPanel.Visible = true;
            FillIratokGridView();
        }

    }

    #region ügyirat

    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, amin a művelet nem hajtható végre
        int count_vegrehajthato = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_atadasraNEMVegrehajthato = UgyiratokArray.Length - count_vegrehajthato;

        if (count_atadasraNEMVegrehajthato > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_TomegesOlvasasiJogWarning, count_atadasraNEMVegrehajthato);
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            FillJogosultUgyiratok();

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        Ugyiratok.UgyiratokGridView_RowDataBound_CheckTomegesOlvasasiJog(e, Page, JogosultUgyiratok);
    }

    List<string> JogosultUgyiratok { get; set; }

    void FillJogosultUgyiratok()
    {
        JogosultUgyiratok = new List<string>();

        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result res = service.GetTomegesOlvasasiJog(ExecParam, UgyiratokArray);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            }
            else
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    JogosultUgyiratok.Add(row["Id"].ToString());
                }
            }
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    #endregion

    #region irat

    private void FillIratokGridView()
    {
        DataSet ds = IratokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != IratokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, amin a művelet nem hajtható végre
        int count_vegrehajthato = UI.GetGridViewSelectedCheckBoxesCount(IraIratokGridView, "check");

        int count_atadasraNEMVegrehajthato = IratokArray.Length - count_vegrehajthato;

        if (count_atadasraNEMVegrehajthato > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_TomegesOlvasasiJogWarning, count_atadasraNEMVegrehajthato);
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet IratokGridViewBind()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            FillJogosultIratok();

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            search.Id.Value = Search.GetSqlInnerString(IratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(IraIratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void IraIratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        Iratok.IratokGridView_RowDataBound_CheckTomegesOlvasasiJog(e, Page, JogosultIratok);
    }

    List<string> JogosultIratok { get; set; }

    void FillJogosultIratok()
    {
        JogosultIratok = new List<string>();

        if (IratokArray != null && IratokArray.Length > 0)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result res = service.GetTomegesOlvasasiJog(ExecParam, IratokArray);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            }
            else
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    JogosultIratok.Add(row["Id"].ToString());
                }
            }
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    #endregion

    #region jogosultak

    void FillCsoportokListSource()
    {
        CsoportokListSource.Items.Clear();

        KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);
        KRT_CsoportokSearch search = new KRT_CsoportokSearch();
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.OrderBy = "Nev";
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);
        UI.ListBoxFill(CsoportokListSource, result, "Nev", FormHeader1.ErrorPanel, null);
        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(CsoportokListSource);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = CsoportokListTarget.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                CsoportokListTarget.Items.Add(new ListItem(item.Text, item.Value));
            }
        }
    }

    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(CsoportokListTarget);
        foreach (ListItem item in selectedItems)
        {
            CsoportokListTarget.Items.Remove(item);
        }
    }

    /// <summary>
    /// Az adott listboxban kijelölt elemeket adja vissza.
    /// </summary>
    /// <param name="listBox"></param>
    /// <returns></returns>
    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    /// <summary>
    /// Szűr a partnerek között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokListSource();
    }

    /// <summary>
    /// Szűr a partnerek között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokListSource();
    }

    List<string> GetAddedCsoportIds()
    {
        List<string> csoportIds = new List<string>();

        foreach (ListItem item in CsoportokListTarget.Items)
        {
            csoportIds.Add(item.Value);
        }

        return csoportIds;
    }
    #endregion

    List<string> GetSelectedRows()
    {
        if (ObjektumType == Constants.TableNames.EREC_UgyUgyiratok)
        {
            return ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
        }
        else if (ObjektumType == Constants.TableNames.EREC_IraIratok)
        {
            return ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null);
        }
        else
        {
            return new List<string>();
        }
    }

    private void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page);
            RightsService rightsService = Contentum.eUtility.eAdminService.ServiceFactory.getRightsService();

            List<string> jogtargyIds = GetSelectedRows();
            List<string> csoportIds = GetAddedCsoportIds();

            if (jogtargyIds.Count == 0)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Nincs kijelölve tétel!");
                return;
            }

            if (csoportIds.Count == 0)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Nincs megadva jogosult!");
                return;
            }

            Result result = rightsService.AddCsoportToJogtargyTomeges(execParam, jogtargyIds.ToArray(), csoportIds.ToArray(), ObjektumType);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
            else
            {
                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
            }
        }
    }
}