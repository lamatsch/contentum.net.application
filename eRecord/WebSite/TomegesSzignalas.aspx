﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TomegesSzignalas.aspx.cs" Inherits="TomegesSzignalas" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="~/Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="ff" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp"%>
<%@ Register Src="~/Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="cstb" %>
<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="fcstb" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kfp"%>
<%@ Register Src="~/Component/FelhasznalokMultiSelectPanel.ascx" TagName="FelhasznalokMultiSelectPanel" TagPrefix="fmsp" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="JavaScripts/CheckBoxes.js" type="text/javascript"></script>
    <asp:ScriptManager runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>

    <fh:FormHeader ID="FormHeader1" runat="server" DisableModeLabel="true" HeaderTitle="<%$Resources:Form,TomegesSzignalasHeaderTitle %>" />

    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="MainPanel" runat="server">
        <eUI:eFormPanel ID="EFormPanel_SzignalasTipus" runat="server">
            <asp:RadioButtonList ID="RadioButtonList_SzignalasTipus" runat="server"
                RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList_SzignalasTipus_SelectedIndexChanged">
                <asp:ListItem Text="Szignálás szervezetre" Value="SzignalasSzervezetre" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Szignálás ügyintézőre" Value="SzignalasUgyintezore"></asp:ListItem>
            </asp:RadioButtonList>
        </eUI:eFormPanel>
        <br />
        <asp:GridView ID="tomegesSzignalasGridView" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="false"
            AllowSorting="true" AutoGenerateColumns="false" DataKeyNames="Id" OnRowDataBound="TomegesSzignalasGridView_RowDataBound" OnRowCreated="TomegesSzignalasGridView_RowCreated" OnSorting="TomegesSzignalasGridView_Sorting">
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" Enabled='<%# Eval("Szignalhato") %>' Checked='<%# Eval("Szignalhato") %>' CssClass="HideCheckBoxText" />

                        <asp:Panel ID="pnlErrorDetail" runat="server" style="display:none;" CssClass="InfoPopupPanel"><%# Eval("ErrorDetail") %></asp:Panel>
                        <ajaxToolkit:HoverMenuExtender ID="hmeErrorDetail" runat="server" PopupControlID="pnlErrorDetail" Enabled='<%# !(bool)Eval("Szignalhato") %>' PopupPosition="Center">
                        </ajaxToolkit:HoverMenuExtender>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="EREC_UgyUgyiratok.Targy">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="210px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Felelos_Nev" HeaderText="Felelős">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Ugyintezo_Nev" HeaderText="Ügyintéző">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Felelos_Nev" HeaderText="Felelős">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Ügyirat helye">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="tomegesSzignalasIratGridView" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="false"
            AllowSorting="true" AutoGenerateColumns="false" DataKeyNames="Id" OnRowDataBound="TomegesSzignalasIratGridView_RowDataBound" OnRowCreated="TomegesSzignalasIratGridView_RowCreated" OnSorting="TomegesSzignalasIratGridView_Sorting">
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" Enabled='<%# Eval("Szignalhato") %>' Checked='<%# Eval("Szignalhato") %>' CssClass="HideCheckBoxText" />

                        <asp:Panel ID="pnlErrorDetail" runat="server" style="display:none;" CssClass="InfoPopupPanel"><%# Eval("ErrorDetail") %></asp:Panel>
                        <ajaxToolkit:HoverMenuExtender ID="hmeErrorDetail" runat="server" PopupControlID="pnlErrorDetail" Enabled='<%# !(bool)Eval("Szignalhato") %>' PopupPosition="Center">
                        </ajaxToolkit:HoverMenuExtender>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIratok.Iktatohely, EREC_IraIratok.Foszam">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok.Targy">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="210px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <%--<asp:BoundField DataField="Felelos_Nev" HeaderText="Felelős">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>--%>
                <asp:BoundField DataField="Ugyintezo_Nev" HeaderText="Ügyintéző">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <%--<asp:BoundField DataField="Felelos_Nev" HeaderText="Felelős">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>--%>
                <%--<asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Ügyirat helye">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>--%>
                <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <br />
        <eUI:eFormPanel ID="EFormPanel_Ugyirat" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr id="trSzervezet" runat="server" class="urlapSor">
                    <td class="mrUrlapCaption_shorter">
                        <asp:Label runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label runat="server" Text="Szervezet:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <cstb:CsoportTextBox ID="SzervezetCsoportTextBox" SzervezetCsoport="true" runat="server" TryFireChangeEvent="false" />
                    </td>
                </tr>
                <tr id="trUgyintezo" runat="server" class="urlapSor" visible="false">
                    <td class="mrUrlapCaption_shorter">
                        <asp:Label runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label runat="server" Text="Ügyintéző:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <fcstb:FelhasznaloCsoportTextBox ID="UgyintezoFelhasznaloCsoportTextBox" runat="server" />
                    </td>
                </tr>
                <tr id="trTovabbiUgyintezok" runat="server" class="urlapSor" visible="false">
                    <td class="mrUrlapCaption_shorter">
                        <asp:Label runat="server" Text="További ügyintézők:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <fmsp:FelhasznalokMultiSelectPanel ID="FelhasznalokMultiSelect" runat="server" />
                    </td>
                </tr>
            </table>
        </eUI:eFormPanel> 
         <eUI:eFormPanel ID="EFormPanelIrat" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr id="trUgyintezoIrat" runat="server" class="urlapSor" visible="false">
                    <td class="mrUrlapCaption_shorter">
                        <asp:Label runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label runat="server" Text="Ügyintéző:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <fcstb:FelhasznaloCsoportTextBox ID="UgyintezoFelhasznaloCsoportTextBoxIrat" runat="server" />
                    </td>
                </tr>
            </table>
        </eUI:eFormPanel> 
        <kfp:KezelesiFeljegyzesPanel ID="KezelesiFeljegyzesPanel" runat="server" />

        <ff:FormFooter ID="FormFooter1" runat="server" Command="Modify" OnButtonsClick="FormFooter1_ButtonsClick" />
    </asp:Panel>
</asp:Content>