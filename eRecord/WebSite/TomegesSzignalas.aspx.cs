﻿using AjaxControlToolkit;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TomegesSzignalas : System.Web.UI.Page
{
    private enum SzignalasTipus
    {
        UgyiratSzignalas,
        IratSzignalas
    }

    private const string FunkcioKod_UgyiratSzignalas = "UgyiratSzignalas";
    private const string FunkcioKod_KuldemenySzignalas = "KuldemenySzignalas";
    private const string FunkcioKod_IratSzignalas = "IratSzignalas";

    private SzignalasTipus Mode = SzignalasTipus.UgyiratSzignalas;

    private string[] SelectedIds
    {
        get
        {
            string type = "";
            if (Mode == SzignalasTipus.UgyiratSzignalas)
            {
                type = Constants.SelectedUgyiratIds;
            }
            else
            {
                type = Constants.SelectedIratIds;
            }

            return Session[type].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }
    }

    private IEnumerable<string> CheckedIds
    {
        get
        {
            foreach (GridViewRow row in tomegesSzignalasGridView.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                    continue;
                CheckBox check = row.FindControl("check") as CheckBox;
                if (check.Checked)
                    yield return tomegesSzignalasGridView.DataKeys[row.DataItemIndex].Value.ToString();
            }
        }
    }

    private IEnumerable<string> CheckedIdsIrat
    {
        get
        {
            foreach (GridViewRow row in tomegesSzignalasIratGridView.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                    continue;
                CheckBox check = row.FindControl("check") as CheckBox;
                if (check.Checked)
                    yield return tomegesSzignalasIratGridView.DataKeys[row.DataItemIndex].Value.ToString();
            }
        }
    }

    private bool SzignalasUgyintezore
    {
        get { return RadioButtonList_SzignalasTipus.SelectedValue == "SzignalasUgyintezore"; }
    }

    // BUG_8765
    private bool IsTUK_SZIGNALAS_ENABLED
    {
        get
        {
            return Rendszerparameterek.GetBoolean(Page, "TUK_SZIGNALAS_ENABLED", false);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        string mode = Request.QueryString.Get("Mode");

        if (!string.IsNullOrEmpty(mode))
        {
            if (mode == "Irat")
            {
                Mode = SzignalasTipus.IratSzignalas;
            }
        }

        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            FormHeader1.CustomTemplateTipusNev = "SzignalasTemplateObject_Ugyirat_Tomeges";
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratSzignalas);
        }
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            FormHeader1.CustomTemplateTipusNev = "SzignalasTemplateObject_Irat_Tomeges";
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratSzignalas);
        }

        FormHeader1.TemplateObjectType = typeof(SzignalasTemplateObject);
        FormHeader1.FormTemplateLoader1_Visibility = true;
        FormHeader1.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);

        KezelesiFeljegyzesPanel.ErrorPanel = FormHeader1.ErrorPanel;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            FormFooter1.ImageButton_Save.OnClientClick = string.Format("{0} alert('{1}');", JavaScripts.SetOnClientClickIsSelectedRow(tomegesSzignalasGridView.ClientID), Resources.List.UI_Atadas_AtadasFizikailag);
            UgyintezoFelhasznaloCsoportTextBox.FilterBySzervezetId(null);
            FormHeader1.HeaderTitle = Resources.Form.TomegesSzignalasHeaderTitle;
        }
        else
        {
            FormFooter1.ImageButton_Save.OnClientClick = string.Format("{0} alert('{1}');", JavaScripts.SetOnClientClickIsSelectedRow(tomegesSzignalasIratGridView.ClientID), Resources.List.UI_Atadas_AtadasFizikailag);
            UgyintezoFelhasznaloCsoportTextBoxIrat.FilterBySzervezetId(null);
            FormHeader1.HeaderTitle = Resources.Form.TomegesSzignalasHeaderTitleIrat;
        }

        if (IsPostBack)
        {
            // errorPanel eltüntetése, ha ki volt rakva:
            FormHeader1.ErrorPanel.Visible = false;
        }
        else
        {
            if (Mode == SzignalasTipus.UgyiratSzignalas)
            {
                RadioButtonList_SzignalasTipus_SelectedIndexChanged(this, EventArgs.Empty);
            }
            else
            {
                TomegesSzignalasIratGridViewBind();
                KezelesiFeljegyzesPanel.FelelosControlID = UgyintezoFelhasznaloCsoportTextBoxIrat.ID;
            }

            #region Template betöltés, ha kell

            string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);
            if (!String.IsNullOrEmpty(templateId))
            {
                FormHeader1.LoadTemplateObjectById(templateId);
                LoadComponentsFromTemplate((SzignalasTemplateObject)FormHeader1.TemplateObject);
            }

            #endregion

            EnableDisableControls();
        }
    }

    private void EnableDisableControls()
    {
        bool enable = Mode == SzignalasTipus.UgyiratSzignalas;

        EFormPanel_Ugyirat.Visible = enable;
        tomegesSzignalasGridView.Visible = enable;
        EFormPanel_SzignalasTipus.Visible = enable;

        EFormPanelIrat.Visible = !enable;
        tomegesSzignalasIratGridView.Visible = !enable;        
        trUgyintezoIrat.Visible = !enable;
    }

    private void TomegesSzignalasGridViewBind()
    {
        string sortExpression;

        SortDirection sortDirection = Search.GetSortDirectionFromViewState(tomegesSzignalasGridView.ID, ViewState, SortDirection.Descending);

        sortExpression = Search.GetSortExpressionFromViewState(tomegesSzignalasGridView.ID, ViewState, "EREC_UgyUgyiratok.LetrehozasIdo");
        TomegesSzignalasGridViewBind(sortExpression, sortDirection);
    }

    private void TomegesSzignalasIratGridViewBind()
    {
        string sortExpression;

        SortDirection sortDirection = Search.GetSortDirectionFromViewState(tomegesSzignalasGridView.ID, ViewState, SortDirection.Descending);

        sortExpression = Search.GetSortExpressionFromViewState(tomegesSzignalasGridView.ID, ViewState, "EREC_IraIratok.LetrehozasIdo");
        TomegesSzignalasIratGridViewBind(sortExpression, sortDirection);

    }

    private void TomegesSzignalasGridViewBind(string sortExpression, SortDirection sortDirection)
    {
        Result result;
        result = GetUgyUgyIratokData(sortExpression, sortDirection);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            ErrorUpdatePanel.Update();
        }
        else
        {
            SetEnabled(result);
            tomegesSzignalasGridView.DataSource = result.Ds;
            tomegesSzignalasGridView.DataBind();
        }
    }

    private void TomegesSzignalasIratGridViewBind(string sortExpression, SortDirection sortDirection)
    {
        Result result;

        result = GetIratokData(sortExpression, sortDirection);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            ErrorUpdatePanel.Update();
        }
        else
        {
            SetEnabledIratok(result);
            tomegesSzignalasIratGridView.DataSource = result.Ds;
            tomegesSzignalasIratGridView.DataBind();
        }
    }

    private Result GetUgyUgyIratokData(string sortExpression, SortDirection sortDirection)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        // jav. nekrisz (patch tesztelés NMHH_2-1-032_P9-0-4)
        //EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));
        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
        search.WhereByManual = string.Format("EREC_UgyUgyiratok.Id IN ({0})", string.Join(", ", SelectedIds.Select(id => string.Format("'{0}'", id)).ToArray()));
        search.OrderBy = Search.GetOrderBy(tomegesSzignalasGridView.ID, ViewState, sortExpression, sortDirection);
        Result result = service.GetAllWithExtension(execParam, search);
        return result;
    }

    private Result GetIratokData(string sortExpression, SortDirection sortDirection)
    {
        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        // jav. nekrisz (patch tesztelés NMHH_2-1-032_P9-0-4)
        // EREC_IraIratokSearch search = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
        EREC_IraIratokSearch search = new EREC_IraIratokSearch();
         search.WhereByManual = string.Format("EREC_IraIratok.Id IN ({0})", string.Join(", ", SelectedIds.Select(id => string.Format("'{0}'", id)).ToArray()));
        search.OrderBy = Search.GetOrderBy(tomegesSzignalasGridView.ID, ViewState, sortExpression, sortDirection);
        Result result = service.GetAllWithExtension(execParam, search);
        return result;
    }

    private void SetEnabled(Result result)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        result.Ds.Tables[0].Columns.Add("Szignalhato", typeof(bool));
        result.Ds.Tables[0].Columns.Add("ErrorDetail", typeof(string));
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotByDataRow(row);
            ErrorDetails errorDetail;
            bool szignalhato = Ugyiratok.Szignalhato(statusz, execParam, out errorDetail);
            row["Szignalhato"] = szignalhato;
            if (!szignalhato)
                row["ErrorDetail"] = errorDetail.Message;

            if (szignalhato && SzignalasUgyintezore)
            {
                string ugyintezoreSzignalhatoErrorMessage;
                bool ugyintezoreSzignalhato = Ugyiratok.UgyintezoreSzignalhato(UI.SetExecParamDefault(Page), row, FelhasznaloProfil.FelhasznaloSzerverzetId(Page), out ugyintezoreSzignalhatoErrorMessage);
                if (!ugyintezoreSzignalhato)
                {
                    row["Szignalhato"] = false;
                    row["ErrorDetail"] = ugyintezoreSzignalhatoErrorMessage;
                }
            }
        }
    }

    private void SetEnabledIratok(Result result)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        result.Ds.Tables[0].Columns.Add("Szignalhato", typeof(bool));
        result.Ds.Tables[0].Columns.Add("ErrorDetail", typeof(string));
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            Iratok.Statusz statusz = Iratok.GetAllapotByDataRow(row);
            ErrorDetails errorDetail;
            bool szignalhato = Iratok.Szignalhato(statusz, execParam, out errorDetail);
            row["Szignalhato"] = szignalhato;
            if (!szignalhato)
                row["ErrorDetail"] = errorDetail.Message;
        }
    }

    protected void TomegesSzignalasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = e.Row.DataItem as DataRowView;
            if (!(bool)dataRowView["Szignalhato"])
                e.Row.CssClass += " ViewDisabledWebControl";
        }
    }

    protected void TomegesSzignalasGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HoverMenuExtender hmeErrorDetail = e.Row.FindControl("hmeErrorDetail") as HoverMenuExtender;
            e.Row.ID = "TomegesSzignalasRow_" + e.Row.RowIndex.ToString();
            hmeErrorDetail.TargetControlID = e.Row.ID;
        }
    }

    protected void TomegesSzignalasIratGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = e.Row.DataItem as DataRowView;
            if (!(bool)dataRowView["Szignalhato"])
                e.Row.CssClass += " ViewDisabledWebControl";
        }
    }

    protected void TomegesSzignalasIratGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HoverMenuExtender hmeErrorDetail = e.Row.FindControl("hmeErrorDetail") as HoverMenuExtender;
            e.Row.ID = "TomegesSzignalasRow_" + e.Row.RowIndex.ToString();
            hmeErrorDetail.TargetControlID = e.Row.ID;
        }
    }

    protected void TomegesSzignalasGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TomegesSzignalasGridViewBind(e.SortExpression, UI.GetSortToGridView(tomegesSzignalasGridView.ID, ViewState, e.SortExpression));
    }

    protected void TomegesSzignalasIratGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TomegesSzignalasIratGridViewBind(e.SortExpression, UI.GetSortToGridView(tomegesSzignalasIratGridView.ID, ViewState, e.SortExpression));
    }

    protected void RadioButtonList_SzignalasTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        TomegesSzignalasGridViewBind();
        if (SzignalasUgyintezore)
        {
            KezelesiFeljegyzesPanel.FelelosControlID = UgyintezoFelhasznaloCsoportTextBox.ID;
            trUgyintezo.Visible = true;
            trTovabbiUgyintezok.Visible = true;
            trSzervezet.Visible = false;
        }
        else
        {
            KezelesiFeljegyzesPanel.FelelosControlID = SzervezetCsoportTextBox.ID;
            trSzervezet.Visible = true;
            trUgyintezo.Visible = false;
            trTovabbiUgyintezok.Visible = false;
        }
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {

            if ((Mode == SzignalasTipus.UgyiratSzignalas && FunctionRights.GetFunkcioJog(Page, "UgyiratSzignalas"))
                || ((Mode == SzignalasTipus.IratSzignalas && FunctionRights.GetFunkcioJog(Page, "IratSzignalas"))))
            {
                if (Mode == SzignalasTipus.UgyiratSzignalas)
                {
                    if (SzignalasUgyintezore)
                    {
                        string kovFelelosId = UgyintezoFelhasznaloCsoportTextBox.Id_HiddenField;
                        if (string.IsNullOrEmpty(kovFelelosId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINincsMegadvaFelelos);
                            return;
                        }

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        if (FelhasznalokMultiSelect.FelhasznaloIds != null && FelhasznalokMultiSelect.FelhasznaloIds.Any())
                        {
                            RightsService rightsService = Contentum.eUtility.eAdminService.ServiceFactory.getRightsService();
                            foreach (string felhasznaloId in FelhasznalokMultiSelect.FelhasznaloIds)
                                foreach (string ugyiratId in CheckedIds)
                                {
                                    Result rightsResult = rightsService.AddCsoportToJogtargy(execParam, ugyiratId, felhasznaloId, 'I');
                                    if (rightsResult.IsError)
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, rightsResult);
                                }
                        }
                        // BUG_8765
                        EREC_HataridosFeladatok erec_HataridosFeladatok = null;
                        if (!IsTUK_SZIGNALAS_ENABLED)
                            erec_HataridosFeladatok = KezelesiFeljegyzesPanel.GetBusinessObject();

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        Result result = service.TomegesSzignalas(execParam, CheckedIds.ToArray(), kovFelelosId, erec_HataridosFeladatok);
                        if (result.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        else
                        {
                            // BUG_8765
                            if (!IsTUK_SZIGNALAS_ENABLED)
                            {
                                foreach (string checkedId in CheckedIds)
                                    Notify.SendAnswerEmail(execParam, Guid.Empty.ToString(), checkedId);
                            }
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                        }
                    }
                    else // Szervezet
                    {
                        string szervezetId = SzervezetCsoportTextBox.Id_HiddenField;
                        if (string.IsNullOrEmpty(szervezetId))
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINincsMegadvaMindenAdat);
                            return;
                        }

                        // BUG_8765
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        EREC_HataridosFeladatok erec_HataridosFeladatok = null;
                        if (!IsTUK_SZIGNALAS_ENABLED)
                            erec_HataridosFeladatok = KezelesiFeljegyzesPanel.GetBusinessObject();

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result result = service.TomegesSzignalasSzervezetre(execParam, CheckedIds.ToArray(), szervezetId, erec_HataridosFeladatok);
                        if (result.IsError)
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        else
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
                else
                {
                    string kovFelelosId = UgyintezoFelhasznaloCsoportTextBoxIrat.Id_HiddenField;

                    if (String.IsNullOrEmpty(kovFelelosId))
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                            , Resources.Error.UINincsMegadvaFelelos);
                        return;
                    }

                    // Szignálás

                    EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    EREC_HataridosFeladatok erec_HataridosFeladatok = KezelesiFeljegyzesPanel.GetBusinessObject();

                    Result result = service.TomegesSzignalas(execParam, CheckedIdsIrat.ToArray(), kovFelelosId, erec_HataridosFeladatok);

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                    else
                    {
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
        }
    }

    #region Template

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromTemplate((SzignalasTemplateObject)FormHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormHeader1.NewTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
    }

    private void LoadComponentsFromTemplate(SzignalasTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            #region hataridos feladat

            if (templateObject.HataridosFeladat != null)
            {
                KezelesiFeljegyzesPanel.SetFeladatByBusinessObject(templateObject.HataridosFeladat);
            }
            #endregion

            if (Mode == SzignalasTipus.UgyiratSzignalas)
            {
                if (!String.IsNullOrEmpty(templateObject.SzignalasTipus))
                {
                    SetSelectedValue(RadioButtonList_SzignalasTipus, templateObject.SzignalasTipus);
                    RadioButtonList_SzignalasTipus_SelectedIndexChanged(this, EventArgs.Empty);
                }

                UgyintezoFelhasznaloCsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.FelhCsoport_Id_Selejtezo;
                UgyintezoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                SzervezetCsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez;
                SzervezetCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            }            
            else if (Mode == SzignalasTipus.IratSzignalas)
            {
                UgyintezoFelhasznaloCsoportTextBoxIrat.Id_HiddenField =
                     templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez;
                UgyintezoFelhasznaloCsoportTextBoxIrat.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }

        }
    }

    private void SetSelectedValue(RadioButtonList list, String selectedValue)
    {
        ListItem selectedListItem = list.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            list.SelectedValue = selectedValue;
        }
    }

    private SzignalasTemplateObject GetTemplateObjectFromComponents()
    {
        SzignalasTemplateObject templateObject = new SzignalasTemplateObject();

        //templateObject.Megjegyzes = Megjegyzes_TextBox.Text;

        templateObject.HataridosFeladat = KezelesiFeljegyzesPanel.GetBusinessObject();

        if (RadioButtonList_SzignalasTipus.Visible)
        {
            templateObject.SzignalasTipus = RadioButtonList_SzignalasTipus.SelectedValue;            
        }

        if (Mode == SzignalasTipus.UgyiratSzignalas)
        {
            templateObject.UgyiratComponents.FelhCsoport_Id_Selejtezo= UgyintezoFelhasznaloCsoportTextBox.Id_HiddenField;
            templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez= SzervezetCsoportTextBox.Id_HiddenField;
        }        
        else if (Mode == SzignalasTipus.IratSzignalas)
        {
            templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez= UgyintezoFelhasznaloCsoportTextBoxIrat.Id_HiddenField;
        }

        return templateObject;
    }

    #endregion

}