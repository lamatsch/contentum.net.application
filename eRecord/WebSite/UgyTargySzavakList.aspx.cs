using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class UgyTargySzavakList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    private string selectedTab = "";

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
    }

    //readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }

    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        return getModosithatosag(selectedRow);
    }

    protected bool getModosithatosag(GridViewRow selectedRow)
    {
        bool modosithato = false;
        if (selectedRow != null)
        {
            CheckBox cbSystem = (CheckBox)selectedRow.FindControl("cbSPSSzinkronizalt");
            if (cbSystem != null)
            {
                modosithato = !cbSystem.Checked;
            }
        }
        return modosithato;
    }

    protected bool isMetaadat(GridView gridView)
    {
        return isMetaadat(gridView, -1);
    }

    protected bool isMetaadat(GridView gridView, int rowIndex)
    {
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        return isMetaadat(selectedRow);
    }

    protected bool isMetaadat(GridViewRow selectedRow)
    {
        bool metaadat = false;
        if (selectedRow != null)
        {
            CheckBox cbSystem = (CheckBox)selectedRow.FindControl("cbTipus");
            if (cbSystem != null)
            {
                metaadat = cbSystem.Checked;
            }
        }
        return metaadat;
    }

    #region Utils

    private String GetIdListFromTable(DataTable table, string Separator, bool bQuoteId)
    {
        List<String> Ids = new List<string>();
        if (table == null) return "";

        foreach (DataRow row in table.Rows)
        {
            string row_Id = row["Id"].ToString();
            if (bQuoteId)
            {
                row_Id = "'" + row_Id + "'";
            }

            Ids.Add(row_Id);
        }

        return String.Join(Separator, Ids.ToArray());
    }

    #endregion Utils

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TargyszavakList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        IratMetaDefinicioSubListHeader.RowCount_Changed += new EventHandler(IratMetaDefinicioSubListHeader_RowCount_Changed);       
        IratMetaDefinicioSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IratMetaDefinicioSubListHeader_ErvenyessegFilter_Changed);

        ObjMetaDefinicioSubListHeader.RowCount_Changed += new EventHandler(ObjMetaDefinicioSubListHeader_RowCount_Changed);
        ObjMetaDefinicioSubListHeader.ErvenyessegFilter_Changed += new EventHandler(ObjMetaDefinicioSubListHeader_ErvenyessegFilter_Changed);

        #region Tabf�lek megjelen�t�se jogosults�g szerint

        bool hasRightForIMD = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioList");
        bool hasRightForOMD = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioList");
        if (hasRightForIMD || hasRightForOMD)
        {           
            if (!hasRightForIMD && hasRightForOMD)
            {
                IratMetaDefinicioUpdatePanel.Visible = false;
                TabPanelIratMetaDefinicio.Enabled = false;

                ObjMetaDefinicioUpdatePanel.Visible = true;
                TabPanelObjMetaDefinicio.Enabled = true;
            }
            else if (hasRightForIMD && !hasRightForOMD)
            {
                IratMetaDefinicioUpdatePanel.Visible = true;
                TabPanelIratMetaDefinicio.Enabled = true;

                ObjMetaDefinicioUpdatePanel.Visible = false;
                TabPanelObjMetaDefinicio.Enabled = false;
            }
        }

        #endregion


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.TargySzavakListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_TargySzavakSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainerDetail);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerDetail);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("TargySzavakSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("TargySzavakForm.aspx",
            QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(TargySzavakGridView.ClientID, "check", "cbInvalidateEnabled", true);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TargySzavakGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(TargySzavakGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(TargySzavakGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(TargySzavakGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
          QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                  , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakUpdatePanel.ClientID, "", true);


        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = TargySzavakGridView;

        IratMetaDefinicioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IratMetaDefinicioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IratMetaDefinicioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IratMetaDefinicioSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IratMetaDefinicioGridView.ClientID);
        IratMetaDefinicioSubListHeader.ButtonsClick += new CommandEventHandler(IratMetaDefinicioSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        IratMetaDefinicioSubListHeader.AttachedGridView = IratMetaDefinicioGridView;

        ObjMetaDefinicioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ObjMetaDefinicioSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ObjMetaDefinicioGridView.ClientID);
        ObjMetaDefinicioSubListHeader.ButtonsClick += new CommandEventHandler(ObjMetaDefinicioSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        ObjMetaDefinicioSubListHeader.AttachedGridView = ObjMetaDefinicioGridView;


        ui.SetClientScriptToGridViewSelectDeSelectButton(TargySzavakGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(IratMetaDefinicioGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_TargySzavakSearch());

        if (!IsPostBack) TargySzavakGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!IsPostBack)
        {
            bool hasRightForIMD = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioList");
            bool hasRightForOMD = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioList");
            if (!hasRightForIMD && hasRightForOMD)
            {
                selectedTab = "TabPanelObjMetaDefinicio";
            }
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in TabContainerDetail.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        TabContainerDetail.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    TabContainerDetail.ActiveTab = TabPanelIratMetaDefinicio;
                }
            }
            else
            {
                TabContainerDetail.ActiveTab = TabPanelIratMetaDefinicio;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Lock);

        // funkci�jog
        panelDetail.Visible = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioList") || FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioList");
        if (panelDetail.Visible)
        {
            IratMetaDefinicioPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioList");
            ObjMetaDefinicioPanel.Visible = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioList");

            //IratMetaDefinicioSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.New);
            //IratMetaDefinicioSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.View);
            //IratMetaDefinicioSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Modify);
            //IratMetaDefinicioSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Invalidate);

            IratMetaDefinicioSubListHeader.NewVisible = false;
            IratMetaDefinicioSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.View);
            IratMetaDefinicioSubListHeader.ModifyVisible = false;
            IratMetaDefinicioSubListHeader.InvalidateVisible = false;

            //ObjMetaDefinicioSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.New);
            //ObjMetaDefinicioSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.View);
            //ObjMetaDefinicioSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Modify);
            //ObjMetaDefinicioSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.Invalidate);

            ObjMetaDefinicioSubListHeader.NewVisible = false;
            ObjMetaDefinicioSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicio" + CommandName.View);
            ObjMetaDefinicioSubListHeader.ModifyVisible = false;
            ObjMetaDefinicioSubListHeader.InvalidateVisible = false;

        }
        else
        {
            DetailCPEButton.Visible = false;
        }


        if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(TargySzavakGridView);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (panelDetail.Visible)
            {
                if (String.IsNullOrEmpty(MasterListSelectedRowId))
                {
                    ActiveTabClear();
                }
                ActiveTabRefreshOnClientClicks();
            }
        }

    }

    #endregion


    #region Master List

    protected void TargySzavakGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("TargySzavakGridView", ViewState, "TargySzavak");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("TargySzavakGridView", ViewState);

        TargySzavakGridViewBind(sortExpression, sortDirection);
    }

    protected void TargySzavakGridViewBind(String SortExpression, SortDirection SortDirection)    
    {
        EREC_TargySzavakService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_TargySzavakSearch search = (EREC_TargySzavakSearch)Search.GetSearchObject(Page, new EREC_TargySzavakSearch());
        search.OrderBy = Search.GetOrderBy("TargySzavakGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(TargySzavakGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void TargySzavakGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbSPSSzinkronizalt", "SPSSzinkronizalt");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        CheckBox cbInvalidateEnabled = (CheckBox)e.Row.FindControl("cbInvalidateEnabled");
        if (cbInvalidateEnabled != null)
        {
            bool bCheck = getModosithatosag(e.Row);
            bool meta = isMetaadat(e.Row);
            if (meta)
            {
                bCheck = bCheck && FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese");
            }

            cbInvalidateEnabled.Checked = bCheck;
        }
    }

    
    protected void TargySzavakGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = TargySzavakGridView.PageIndex;

        TargySzavakGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = TargySzavakGridView.PageCount;

        if (prev_PageIndex != TargySzavakGridView.PageIndex )
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, TargySzavakCPE);
            TargySzavakGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, TargySzavakCPE);
        }


        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(TargySzavakGridView);
        
        ListHeader1.RefreshPagerLabel();
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        TargySzavakGridViewBind();
        ActiveTabClear();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, TargySzavakCPE);
        TargySzavakGridViewBind();
    }

    protected void TargySzavakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(TargySzavakGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainerDetail, id);
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(TargySzavakGridView);
            bool metaadat = isMetaadat(TargySzavakGridView);
            //if (modosithato)
            //{
            //    ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    + "&" + QueryStringVars.RefreshCallingWindow + "=" + "1"
            //     , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.ModifyOnClientClick = jsNemModosithato;
            //}

            if (!modosithato)
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_TargyszavakMegtekintes
                   + "')) {" + JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";
            }
            else if (metaadat && !FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese"))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_MetaadatMegtekintes
                   + "')) {" + JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.RefreshCallingWindow + "=" + "1"
                 , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakUpdatePanel.ClientID);

            string tableName = "EREC_TargySzavak";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, TargySzavakUpdatePanel.ClientID);


            IratMetaDefinicioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                , "Command=" + CommandName.New
                , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            ObjMetaDefinicioSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , "Command=" + CommandName.New
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

       
        }
    }

    protected void TargySzavakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    TargySzavakGridViewBind();
                    break;
            }
        }        
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTargySzavak();
            TargySzavakGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedTargySzavakRecords();
                TargySzavakGridViewBind();
                break;
            case CommandName.Unlock:
                UnlockSelectedTargySzavakRecords();
                TargySzavakGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedTargySzavak();
                break;
        }
    }

    private void LockSelectedTargySzavakRecords()
    {
        LockManager.LockSelectedGridViewRecords(TargySzavakGridView, "EREC_TargySzavak"
            , "TargyszavakLock", "TargyszavakForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedTargySzavakRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(TargySzavakGridView, "EREC_TargySzavak"
            , "TargyszavakLock", "TargyszavakForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// T�rli a TargySzavakGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedTargySzavak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "TargyszavakInvalidate"))
        {
            List<string[]> deletableItemsList = ui.GetGridViewSelectedRowsWithIndex(TargySzavakGridView, EErrorPanel1, ErrorUpdatePanel);

            EREC_TargySzavakService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(TargySzavakGridView, Int32.Parse(deletableItemsList[i][1]));
                bool metaadat = isMetaadat(TargySzavakGridView, Int32.Parse(deletableItemsList[i][1]));
                if (metaadat)
                {
                    modosithato = modosithato && FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese");
                }

                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i][0];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a TargySzavakGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedTargySzavak()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(TargySzavakGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_TargySzavak");
        }
    }

    protected void TargySzavakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TargySzavakGridViewBind(e.SortExpression, UI.GetSortToGridView("TargySzavakGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }


    #endregion

    #region Detail Tab

    protected void TabContainerDetail_ActiveTabChanged(object sender, EventArgs e)
    {
        if (TargySzavakGridView.SelectedIndex == -1)
        {
            return;
        }
        string id = UI.GetGridViewSelectedRecordId(TargySzavakGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, id);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string id)
    {
        //switch (sender.ActiveTab.TabIndex)
        //{
        //    case 0:
        //        ObjMetaDefinicioGridViewBind(id);
        //        ObjMetaDefinicioPanel.Visible = true;
        //        break;
        //    case 1:
        //        IratMetaDefinicioGridViewBind(id);
        //        IratMetaDefinicioPanel.Visible = true;
        //        break;

        //}
        if (sender.ActiveTab.Equals(TabPanelObjMetaDefinicio))
        {
            ObjMetaDefinicioGridViewBind(id);
            ObjMetaDefinicioPanel.Visible = true;
        }
        else if (sender.ActiveTab.Equals(TabPanelIratMetaDefinicio))
        {
            IratMetaDefinicioGridViewBind(id);
            IratMetaDefinicioPanel.Visible = true;
        }
    }

    private void ActiveTabClear()
    {
        //switch (TabContainerDetail.ActiveTabIndex)
        //{
        //    case 0:
        //        ui.GridViewClear(ObjMetaDefinicioGridView);
        //         break;
        //    case 1:
        //        ui.GridViewClear(IratMetaDefinicioGridView);
        //        break;
        //}

        if (TabContainerDetail.ActiveTab.Equals(TabPanelObjMetaDefinicio))
        {
            ui.GridViewClear(ObjMetaDefinicioGridView);
        }
        else if (TabContainerDetail.ActiveTab.Equals(TabPanelIratMetaDefinicio))
        {
            ui.GridViewClear(IratMetaDefinicioGridView);
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        IratMetaDefinicioSubListHeader.RowCount = RowCount;
        ObjMetaDefinicioSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainerDetail.ActiveTab.Equals(TabPanelIratMetaDefinicio))
        {
            IratMetaDefinicioGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IratMetaDefinicioGridView));
        }
        else if (TabContainerDetail.ActiveTab.Equals(TabPanelObjMetaDefinicio))
        {
            ObjMetaDefinicioGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(ObjMetaDefinicioGridView));
        }
    }

    #endregion

    #region IratMetaDefinicio Detail

    private void IratMetaDefinicioSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IratMetaDefinicio();
            IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
        }
    }

    protected void IratMetaDefinicioGridViewBind(string id)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IratMetaDefinicioGridView", ViewState, "UgykorKod");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IratMetaDefinicioGridView", ViewState);

        IratMetaDefinicioGridViewBind(id, sortExpression, sortDirection);
    }

    protected void IratMetaDefinicioGridViewBind(string id, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(id))
        {
            #region el�sz�r�s

            //string[] imd_Ids = null;

            //// 1. l�p�s: EREC_Obj_MetaAdatai, ahol Targyszavak_Id = id
            //EREC_Obj_MetaAdataiService service_oma = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            //ExecParam ExecParam_oma = UI.SetExecParamDefault(Page, new ExecParam());

            //EREC_Obj_MetaAdataiSearch search_oma = new EREC_Obj_MetaAdataiSearch();
            //search_oma.Targyszavak_Id.Value = id;
            //search_oma.Targyszavak_Id.Operator = Query.Operators.equals;

            //search_oma.Obj_MetaDefinicio_Id.Value = "";
            //search_oma.Obj_MetaDefinicio_Id.Operator = Query.Operators.notnull;

            //IratMetaDefinicioSubListHeader.SetErvenyessegFields(search_oma.ErvKezd, search_oma.ErvVege);
            //search_oma.WhereByManual += " and DefinicioTipus = 'C2'";

            //Result res_oma = service_oma.GetAllWithExtension(ExecParam_oma, search_oma);

            //if (String.IsNullOrEmpty(res_oma.ErrorCode))
            //{
            //    if (res_oma.Ds.Tables[0].Rows.Count > 0)
            //    {
            //        string[] omd_Ids = new string[res_oma.Ds.Tables[0].Rows.Count];

            //        int i = 0;

            //        foreach (DataRow row in res_oma.Ds.Tables[0].Rows)
            //        {
            //            string omd_Id = row["Obj_MetaDefinicio_Id"].ToString();
            //            if (!String.IsNullOrEmpty(omd_Id))
            //            {
            //                omd_Ids[i] = "'" + omd_Id + "'";
            //                i++;
            //            }
            //        }

            //        // 2. l�p�s: EREC_Obj_MetaDefinicio, ahol DefinicioTipus = 'C2' �s Id in EREC_Obj_MetaAdatai->Obj_MetaDefinicio_Id[]
            //        if (omd_Ids.Length > 0)
            //        {
            //            string omd_Ids_joined = String.Join(",", omd_Ids);

            //            EREC_Obj_MetaDefinicioService service_omd = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            //            ExecParam ExecParam_omd = UI.SetExecParamDefault(Page, new ExecParam());

            //            EREC_Obj_MetaDefinicioSearch search_omd = new EREC_Obj_MetaDefinicioSearch();
            //            search_omd.Id.Value = omd_Ids_joined;
            //            search_omd.Id.Operator = Query.Operators.inner;

            //            IratMetaDefinicioSubListHeader.SetErvenyessegFields(search_omd.ErvKezd, search_omd.ErvVege);

            //            Result res_omd = service_omd.GetAllWithExtension(ExecParam_omd, search_omd);

            //            if (String.IsNullOrEmpty(res_omd.ErrorCode))
            //            {
            //                if (res_omd.Ds.Tables[0].Rows.Count > 0)
            //                {
            //                    imd_Ids = new string[res_omd.Ds.Tables[0].Rows.Count];

            //                    i = 0;
            //                    foreach (DataRow row in res_omd.Ds.Tables[0].Rows)
            //                    {
            //                        string imd_Id = row["ColumnValue"].ToString();
            //                        if (!String.IsNullOrEmpty(imd_Id))
            //                        {
            //                            imd_Ids[i] = "'" + imd_Id + "'";
            //                            i++;
            //                        }
            //                    }
            //                }

            //            }
            //        }
            //    }
            //}
            #endregion el�sz�r�s

            EREC_Obj_MetaDefinicioService service_omd = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam execParam_metafilter = UI.SetExecParamDefault(Page, new ExecParam());

            MetaAdatHierarchiaSearch search_metafilter = new MetaAdatHierarchiaSearch();

            search_metafilter.EREC_TargySzavakSearch.ErvKezd.Clear();
            search_metafilter.EREC_TargySzavakSearch.ErvVege.Clear();

            search_metafilter.EREC_Obj_MetaAdataiSearch.Targyszavak_Id.Filter(id);

            IratMetaDefinicioSubListHeader.SetErvenyessegFields(search_metafilter.EREC_IratMetaDefinicioSearch.ErvKezd
                , search_metafilter.EREC_IratMetaDefinicioSearch.ErvVege);

            Result result_metafilter = service_omd.GetAllMetaAdatokHierarchiaFilter(execParam_metafilter, search_metafilter, false);

            if (String.IsNullOrEmpty(result_metafilter.ErrorCode))
            {

                bool IsFilteredWithNoHit = (bool)result_metafilter.Record;
                if (!IsFilteredWithNoHit)
                {
                    String IratMetaDefinicio_Ids = GetIdListFromTable(result_metafilter.Ds.Tables[3], ",", true);
                    
                    //if (imd_Ids != null && imd_Ids.Length > 0)
                    if (!String.IsNullOrEmpty(IratMetaDefinicio_Ids))
                    {
                        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

                        //string imd_ids_joined = String.Join(",", imd_Ids);
                        //search.Id.Value = imd_ids_joined;
                        search.Id.Value = IratMetaDefinicio_Ids;
                        search.Id.Operator = Query.Operators.inner;

                        search.OrderBy = Search.GetOrderBy("IratMetaDefinicioGridView", ViewState, SortExpression, SortDirection);

                        //IratMetaDefinicioSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

                        Result res = service.GetAllWithExtension(ExecParam, search);

                        UI.GridViewFill(IratMetaDefinicioGridView, res, IratMetaDefinicioSubListHeader, EErrorPanel1, ErrorUpdatePanel);

                        ui.SetClientScriptToGridViewSelectDeSelectButton(IratMetaDefinicioGridView);
                    }
                    else
                    {
                        ui.GridViewClear(IratMetaDefinicioGridView);
                    }
                }
                else
                {
                    ui.GridViewClear(IratMetaDefinicioGridView);
                }
            }
            else
            {
                ui.GridViewClear(IratMetaDefinicioGridView);
            }
        }
        else
        {
            ui.GridViewClear(IratMetaDefinicioGridView);
        }
    }

    void IratMetaDefinicioSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
    }

    protected void IratMetaDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainerDetail.ActiveTab.Equals(TabPanelIratMetaDefinicio))
                    {
                        IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a IratMetaDefinicioGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_IratMetaDefinicio()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IratMetaDefinicioGridView, EErrorPanel1, ErrorUpdatePanel);
            
            EREC_TargySzavakService service = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void IratMetaDefinicioGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IratMetaDefinicioGridView, selectedRowNumber, "check");
        }
    }

    private void IratMetaDefinicioGridView_RefreshOnClientClicks(string  id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            IratMetaDefinicioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            IratMetaDefinicioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void IratMetaDefinicioGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbTipus", "Tipus");
    }

    protected void IratMetaDefinicioGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IratMetaDefinicioGridView.PageIndex;

        IratMetaDefinicioGridView.PageIndex = IratMetaDefinicioSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != IratMetaDefinicioGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IratMetaDefinicioCPE);
            IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
        }
        else
        {
            UI.GridViewSetScrollable(IratMetaDefinicioSubListHeader.Scrollable, IratMetaDefinicioCPE);
        }
        IratMetaDefinicioSubListHeader.PageCount = IratMetaDefinicioGridView.PageCount;
        IratMetaDefinicioSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IratMetaDefinicioGridView);
    }


    void IratMetaDefinicioSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(IratMetaDefinicioSubListHeader.RowCount);
        IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
    }

    protected void IratMetaDefinicioGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IratMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView)
            , e.SortExpression, UI.GetSortToGridView("IratMetaDefinicioGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region ObjMetaDefinicio Detail

    private void ObjMetaDefinicioSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_ObjMetaDefinicio();
            ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
        }
    }

    protected void ObjMetaDefinicioGridViewBind(string id)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ObjMetaDefinicioGridView", ViewState, "ContentType");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ObjMetaDefinicioGridView", ViewState);

        ObjMetaDefinicioGridViewBind(id, sortExpression, sortDirection);
    }

    protected void ObjMetaDefinicioGridViewBind(string id, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(id))
        {
            #region el�sz�r�s

            string[] omd_Ids = null;

            // 1. l�p�s: EREC_Obj_MetaAdatai, ahol Targyszavak_Id = id
            EREC_Obj_MetaAdataiService service_oma = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            ExecParam ExecParam_oma = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_Obj_MetaAdataiSearch search_oma = new EREC_Obj_MetaAdataiSearch();
            search_oma.Targyszavak_Id.Value = id;
            search_oma.Targyszavak_Id.Operator = Query.Operators.equals;

            search_oma.Obj_MetaDefinicio_Id.Value = "";
            search_oma.Obj_MetaDefinicio_Id.Operator = Query.Operators.notnull;

            ObjMetaDefinicioSubListHeader.SetErvenyessegFields(search_oma.ErvKezd, search_oma.ErvVege);

            Result res_oma = service_oma.GetAllWithExtension(ExecParam_oma, search_oma);

            if (String.IsNullOrEmpty(res_oma.ErrorCode))
            {
                if (res_oma.Ds.Tables[0].Rows.Count > 0)
                {
                    omd_Ids = new string[res_oma.Ds.Tables[0].Rows.Count];

                    int i = 0;

                    foreach (DataRow row in res_oma.Ds.Tables[0].Rows)
                    {
                        string omd_Id = row["Obj_MetaDefinicio_Id"].ToString();
                        if (!String.IsNullOrEmpty(omd_Id))
                        {
                            omd_Ids[i] = "'" + omd_Id + "'";
                            i++;
                        }
                    }
                }
            }
            #endregion el�sz�r�s

            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();
            if (omd_Ids != null && omd_Ids.Length > 0)
            {
                string omd_ids_joined = String.Join(",", omd_Ids);
                search.Id.Value = omd_ids_joined;
                search.Id.Operator = Query.Operators.inner;

                search.OrderBy = Search.GetOrderBy("ObjMetaDefinicioGridView", ViewState, SortExpression, SortDirection);

                ObjMetaDefinicioSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

                Result res = service.GetAllWithExtension(ExecParam, search);

                UI.GridViewFill(ObjMetaDefinicioGridView, res, ObjMetaDefinicioSubListHeader, EErrorPanel1, ErrorUpdatePanel);

                ui.SetClientScriptToGridViewSelectDeSelectButton(ObjMetaDefinicioGridView);
            }
            else
            {
                ui.GridViewClear(ObjMetaDefinicioGridView);
            }
        }
        else
        {
            ui.GridViewClear(ObjMetaDefinicioGridView);
        }
    }

    void ObjMetaDefinicioSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
    }

    protected void ObjMetaDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainerDetail.ActiveTab.Equals(TabPanelObjMetaDefinicio))
                    {
                        ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a ObjMetaDefinicioGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_ObjMetaDefinicio()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ObjMetaDefinicioGridView, EErrorPanel1, ErrorUpdatePanel);

            EREC_TargySzavakService service = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void ObjMetaDefinicioGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ObjMetaDefinicioGridView, selectedRowNumber, "check");
        }
    }

    private void ObjMetaDefinicioGridView_RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ObjMetaDefinicioSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            ObjMetaDefinicioSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ObjMetaDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void ObjMetaDefinicioGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbSPSSzinkronizalt", "SPSSzinkronizalt");
    }

    protected void ObjMetaDefinicioGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ObjMetaDefinicioGridView.PageIndex;

        ObjMetaDefinicioGridView.PageIndex = ObjMetaDefinicioSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != ObjMetaDefinicioGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ObjMetaDefinicioCPE);
            ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
        }
        else
        {
            UI.GridViewSetScrollable(ObjMetaDefinicioSubListHeader.Scrollable, ObjMetaDefinicioCPE);
        }
        ObjMetaDefinicioSubListHeader.PageCount = ObjMetaDefinicioGridView.PageCount;
        ObjMetaDefinicioSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(ObjMetaDefinicioGridView);
    }


    void ObjMetaDefinicioSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(ObjMetaDefinicioSubListHeader.RowCount);
        ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView));
    }

    protected void ObjMetaDefinicioGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ObjMetaDefinicioGridViewBind(UI.GetGridViewSelectedRecordId(TargySzavakGridView)
            , e.SortExpression, UI.GetSortToGridView("ObjMetaDefinicioGridView", ViewState, e.SortExpression));
    }

    #endregion
}
