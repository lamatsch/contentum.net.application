<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="UgyTargyszavakForm.aspx.cs" Inherits="UgyTargyszavakForm" Title="Tárgyszavak" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
	TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
	TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
	TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
	EnableScriptLocalization="true" >
	</asp:ScriptManager>
	<uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,TargyszavakFormHeaderTitle %>" />
	<br />
	<%-- HiddenFields --%>
	<asp:HiddenField ID="ErtekRequired_HiddenField" runat="server" />
	<%-- /HiddenFields --%>
	<div class="popupBody">
	<eUI:eFormPanel ID="EFormPanel1" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tbody>
					<tr class="urlapNyitoSor">
						<td class="mrUrlapCaption">
							<img alt=" " src="images/hu/design/spacertrans.gif" /></td>
						<td class="mrUrlapMezo">
							<img alt=" " src="images/hu/design/spacertrans.gif" /></td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelTargySzavak" runat="server" Text="Tárgyszavak:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc5:RequiredTextBox ID="requiredTextBoxTargyszo" runat="server" />
						</td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
						    <asp:Label ID="labelErtekStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErtek" runat="server" Text="Érték:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxErtek" runat="server" Text=""></asp:TextBox>
						</td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="labelSorszam" runat="server" Text="Sorszám:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxSorszam" runat="server" Text="" Width="35"></asp:TextBox>
							<ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server"
							    Minimum="0" Maximum="100" TargetControlID="TextBoxSorszam" Width="60"/>
						</td>
					</tr>													
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="labelForrasCaption" runat="server" Text="Forrás:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:Label ID="labelForras" runat="server" Text=""></asp:Label>
						</td>
					</tr>					
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
						</td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="labelNote" runat="server" Text="Megjegyzés:"></asp:Label></td>
						<td class="mrUrlapMezo">
						<asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
						</td>
					</tr>					
				</tbody>
			</table>
		</eUI:eFormPanel>
	<uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
	<uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
	</div>
</asp:Content>