using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class UgyTargyszavakForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    //private string ObjTip_Id = "";

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private int MaxSorszam = 0;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxTargyszo.TextBox.Enabled = false;
        labelForras.Enabled = false;
        TextBoxErtek.Enabled = false;
        TextBoxSorszam.Enabled = false;
        nupeSorszam.Enabled = false;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        textNote.Enabled = false;

        labelTargySzavak.CssClass = "mrUrlapInputWaterMarked";
        labelErtek.CssClass = "mrUrlapInputWaterMarked";
        labelSorszam.CssClass = "mrUrlapInputWaterMarked";
        labelForrasCaption.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);
        pageView = new PageView(Page, ViewState);

        labelErtekStar.Visible = false;
        ErtekRequired_HiddenField.Value = "";

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:

                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Targyszavak" + Command);

                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;
 
                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
               {
                   EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = (EREC_ObjektumTargyszavai)result.Record;
                   LoadComponentsFromBusinessObject(erec_ObjektumTargyszavai);

                   // Forras n�v kiv�tele adatb�zisb�l
                   string forrasNev;
                   if ((Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("TARGYSZO_FORRAS", Page)).TryGetValue(erec_ObjektumTargyszavai.Forras, out forrasNev))
                   {
                       labelForras.Text = forrasNev;
                   }

                   if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Listas) // szervezeti v. k�zponti
                   {
                       requiredTextBoxTargyszo.Enabled = false;

                   }
                   else // k�zi
                   {
                       requiredTextBoxTargyszo.Enabled = true;
                   }
               }
               else
               {
                   ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
               }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        if (Command == CommandName.Modify)
        {
            SetModifyControls();

            if (int.TryParse(Request.QueryString.Get(QueryStringVars.MaxSorszam), out MaxSorszam))
            {
                nupeSorszam.Maximum = MaxSorszam + 1;
            }

            String ErtekRequired = Request.QueryString.Get(QueryStringVars.Tipus);
            if (!String.IsNullOrEmpty(ErtekRequired))
            {
                ErtekRequired_HiddenField.Value = ErtekRequired;
            }
            else
            {
                ErtekRequired_HiddenField.Value = "";
            }

           // az �rt�k mez� enged�lyez�se, k�telez�v� t�tele v. letilt�sa
           if (ErtekRequired_HiddenField.Value == "1")
           { // k�telez� �rt�k
               labelErtekStar.Visible = true;
           }
           else if (ErtekRequired_HiddenField.Value == "0")
           {
               TextBoxErtek.ReadOnly = true;
           }
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="erec_ObjektumTargyszavai"></param>
    private void LoadComponentsFromBusinessObject(EREC_ObjektumTargyszavai erec_ObjektumTargyszavai)
    {
        requiredTextBoxTargyszo.Text = erec_ObjektumTargyszavai.Targyszo;
        TextBoxErtek.Text = erec_ObjektumTargyszavai.Ertek;
        TextBoxSorszam.Text = erec_ObjektumTargyszavai.Sorszam;
        ErvenyessegCalendarControl1.ErvKezd = erec_ObjektumTargyszavai.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_ObjektumTargyszavai.ErvVege;
        textNote.Text = erec_ObjektumTargyszavai.Base.Note;

        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = erec_ObjektumTargyszavai.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(erec_ObjektumTargyszavai.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private EREC_ObjektumTargyszavai GetBusinessObjectFromComponents()
    {
        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);
        
        erec_ObjektumTargyszavai.Targyszo = requiredTextBoxTargyszo.Text;
        erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(requiredTextBoxTargyszo);
        erec_ObjektumTargyszavai.Ertek = TextBoxErtek.Text;
        erec_ObjektumTargyszavai.Updated.Ertek = pageView.GetUpdatedByView(TextBoxErtek);

        erec_ObjektumTargyszavai.Sorszam = TextBoxSorszam.Text;
        erec_ObjektumTargyszavai.Updated.Sorszam = pageView.GetUpdatedByView(TextBoxSorszam);

        erec_ObjektumTargyszavai.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_ObjektumTargyszavai.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_ObjektumTargyszavai.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.Base.Note = textNote.Text;
        erec_ObjektumTargyszavai.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        erec_ObjektumTargyszavai.Base.Ver = FormHeader1.Record_Ver;
        erec_ObjektumTargyszavai.Base.Updated.Ver = true;

        return erec_ObjektumTargyszavai;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxTargyszo);
            compSelector.Add_ComponentOnClick(TextBoxErtek);
            compSelector.Add_ComponentOnClick(TextBoxSorszam);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textNote);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            // TODO: funkci�jog
            if (FunctionRights.GetFunkcioJog(Page, "Targyszavak" + Command))
            //if (true)   // -- end TODO
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, erec_ObjektumTargyszavai);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxTargyszo.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                if (ErtekRequired_HiddenField.Value == "1" && TextBoxErtek.Text == "")
                                {
                                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Ehhez a t�rgysz�hoz k�telez� az �rt�k megad�sa!");
                                    return;
                                }

                                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_ObjektumTargyszavai);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
