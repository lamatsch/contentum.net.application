<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="UgyUgyiratdarabokForm.aspx.cs" Inherits="UgyUgyiratdarabokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc11" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc10" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %> 
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <asp:Panel ID="MainPanel" runat="server">
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Azonos�t�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Azonosito_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label1" runat="server" Text="�gyirat:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc7:UgyiratTextBox ID="Ugy_UgyiratTextBox" runat="server"></uc7:UgyiratTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label7" runat="server" Text="Elj�r�si szakasz:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc10:KodtarakDropDownList ID="EljarasiSzakasz_KodtarakDropDownList" runat="server" ReadOnly="true">
                                    </uc10:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label9" runat="server" Text="Le�r�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Leiras_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label5" runat="server" Text="�llapot:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc10:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label15" runat="server" Text="Hat�rid�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <span style="color: red">
                                        <uc5:CalendarControl ID="HatarIdo_CalendarControl" runat="server" Validate="false"></uc5:CalendarControl>
                                    </span>
                                </td>
                            </tr>
                            <%--<tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="Label2" runat="server" Text="Felel�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="CsoportID_Felelos_CsoportTextBox" runat="server" FelhasznaloCsoport="true">
                                    </uc8:CsoportTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label16" runat="server" Text="�gyint�z�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc11:FelhasznaloCsoportTextBox ID="FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox"
                                        runat="server"></uc11:FelhasznaloCsoportTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label11" runat="server" Text="Elint�z�s m�dja"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc10:KodtarakDropDownList ID="ElintezesMod_KodtarakDropDownList" runat="server"></uc10:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label10" runat="server" Text="�gyirat elint�z�si id�pontja:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <span style="color: red"></span>
                                    <uc5:CalendarControl ID="ElintezesDat_CalendarControl" runat="server" Validate="false" TimeVisible="true">
                                    </uc5:CalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label13" runat="server" Text="Lez�r�s oka"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc10:KodtarakDropDownList ID="LezarasOka_KodtarakDropDownList" runat="server"></uc10:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label3" runat="server" Text="Lez�r�s d�tuma:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc5:CalendarControl ID="LezarasDat_CalendarControl" runat="server" Validate="false">
                                    </uc5:CalendarControl>
                                </td>
                            </tr>--%>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

