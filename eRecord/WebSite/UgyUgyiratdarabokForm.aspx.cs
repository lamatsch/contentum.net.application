using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UgyUgyiratdarabokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_ELINTEZESMOD = "ELINTEZESMOD";
    private const string kcs_LEZARAS_OKA = "LEZARAS_OKA";
    private const string kcs_UGYIRATDARAB_ALLAPOT = "UGYIRATDARAB_ALLAPOT";

    private EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok = null;
    private EREC_UgyUgyiratok _EREC_UgyUgyiratok = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Ugyiratdarab" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                char jogszint = Command == CommandName.View ? 'O' : 'I';

                Result result = service.GetWithRightCheck(execParam,jogszint);
                //Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    _EREC_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result.Record;

                    // �gyirat lek�r�se:
                    EREC_UgyUgyiratokService service_Ugyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_ugyiratGet.Record_Id = _EREC_UgyUgyiratdarabok.UgyUgyirat_Id;

                    Result result_ugyiratGet = service_Ugyirat.Get(execParam_ugyiratGet);
                    if (String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                    {
                        _EREC_UgyUgyiratok = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                        if (Command == CommandName.Modify)
                        {
                            // M�dos�that�s�g ellen�rz�se:
                            UgyiratDarabok.Statusz ugyiratDarabStatusz =
                                UgyiratDarabok.GetAllapotByBusinessDocument(_EREC_UgyUgyiratdarabok);
                            Ugyiratok.Statusz ugyiratStatusz =
                                Ugyiratok.GetAllapotByBusinessDocument(_EREC_UgyUgyiratok);
                            ErrorDetails errorDetail;

                            if (UgyiratDarabok.Modosithato(ugyiratDarabStatusz,ugyiratStatusz, out errorDetail) == false)
                            {
                                // Nem m�dos�that� a rekord, �tir�ny�t�s a megtekint�s oldalra

                                Response.Redirect(Page.Request.Url.AbsolutePath + "?Command=View&Id=" + id);
                            }
                        }

                        LoadComponentsFromBusinessObject(_EREC_UgyUgyiratdarabok);

                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
                        MainPanel.Visible = false;
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    MainPanel.Visible = false;
                }
            }
        }

        SetComponentsVisibility(Command);

        //else if (Command == CommandName.New)
        //{
        //    EljarasiSzakasz_KodtarakDropDownList.FillDropDownList(kcs_ELJARASI_SZAKASZ, false, FormHeader1.ErrorPanel);
        //    ElintezesMod_KodtarakDropDownList.FillDropDownList(kcs_ELINTEZESMOD, true, FormHeader1.ErrorPanel);
        //    LezarasOka_KodtarakDropDownList.FillDropDownList(kcs_LEZARAS_OKA, true, FormHeader1.ErrorPanel);
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.UgyUgyiratdarabokFormHeaderTitle;
        
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok)
    {
        if (_EREC_UgyUgyiratok != null)
        {
            Ugy_UgyiratTextBox.SetUgyiratTextBoxByBusinessObject(_EREC_UgyUgyiratok, FormHeader1.ErrorPanel, null);
        }
        else
        {
            Ugy_UgyiratTextBox.Id_HiddenField = erec_UgyUgyiratdarabok.UgyUgyirat_Id;
            Ugy_UgyiratTextBox.SetUgyiratTextBoxById(FormHeader1.ErrorPanel, null);
        }

        Azonosito_TextBox.Text = erec_UgyUgyiratdarabok.Azonosito;

        EljarasiSzakasz_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELJARASI_SZAKASZ
                , erec_UgyUgyiratdarabok.EljarasiSzakasz, true, FormHeader1.ErrorPanel);

        Leiras_TextBox.Text = erec_UgyUgyiratdarabok.Leiras;
        HatarIdo_CalendarControl.Text = erec_UgyUgyiratdarabok.Hatarido;

        Allapot_KodtarakDropDownList.FillWithOneValue(kcs_UGYIRATDARAB_ALLAPOT, erec_UgyUgyiratdarabok.Allapot, FormHeader1.ErrorPanel);

        //CsoportID_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabok.Csoport_Id_Felelos;
        //CsoportID_Felelos_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        //FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez;
        //FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        //ElintezesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELINTEZESMOD
        //    ,erec_UgyUgyiratdarabok.ElintezesMod, true, FormHeader1.ErrorPanel);

        //ElintezesDat_CalendarControl.Text = erec_UgyUgyiratdarabok.ElintezesDat;

        //LezarasOka_KodtarakDropDownList.FillAndSetSelectedValue(kcs_LEZARAS_OKA,
        //        erec_UgyUgyiratdarabok.LezarasOka, true, FormHeader1.ErrorPanel);

        //LezarasDat_CalendarControl.Text = erec_UgyUgyiratdarabok.LezarasDat;

        //// F�sz�m kijelz�se a Headerben:
        //String fullFoszam = UgyiratDarabok.GetFullFoszam(erec_UgyUgyiratdarabok, Page, FormHeader1.ErrorPanel, null);
        //if (Command == CommandName.View)
        //{
        //    FormHeader1.FullManualHeaderTitle = fullFoszam + " " + Resources.Form.Ugyiratdarabmegtekintese_ManualFormHeaderTitle;
        //}
        //else if (Command == CommandName.Modify)
        //{
        //    FormHeader1.FullManualHeaderTitle = fullFoszam + " " + Resources.Form.Ugyiratdarabmodositasa_ManualFormHeaderTitle;
        //}

        FormHeader1.Record_Ver = erec_UgyUgyiratdarabok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_UgyUgyiratdarabok.Base);
                
    }

    private void SetComponentsVisibility(String _command)
    {
        if (_command == CommandName.View || _command == CommandName.Modify)
        {
            // amit csak a View-n�l kell tiltani, Modify-n�l nem:
            if (_command == CommandName.View)
            {
                Azonosito_TextBox.ReadOnly = true;
                Leiras_TextBox.ReadOnly = true;
                EljarasiSzakasz_KodtarakDropDownList.ReadOnly = true;            
            }

            
            Ugy_UgyiratTextBox.ReadOnly = true;
            Allapot_KodtarakDropDownList.ReadOnly = true;
            HatarIdo_CalendarControl.ReadOnly = true;
            EljarasiSzakasz_KodtarakDropDownList.ReadOnly = true;
            //CsoportID_Felelos_CsoportTextBox.ReadOnly = true;
            //FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;
            //ElintezesMod_KodtarakDropDownList.ReadOnly = true;
            //ElintezesDat_CalendarControl.ReadOnly = true;
            //LezarasOka_KodtarakDropDownList.ReadOnly = true;
            //LezarasDat_CalendarControl.ReadOnly = true;
        }
    }


    //form --> business object
    private EREC_UgyUgyiratdarabok GetBusinessObjectFromComponents()
    {
        EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
        //�sszes mez� update-elhet�s�g�t kezdetben letiltani:
        erec_UgyUgyiratdarabok.Updated.SetValueAll(false);
        erec_UgyUgyiratdarabok.Base.Updated.SetValueAll(false);

        // kikommentezve, amelyeket v�g�lis nem lehet update-elni; (New meg nincs itt)

        //erec_UgyUgyiratdarabok.UgyUgyirat_Id = Ugy_UgyiratTextBox.Id_HiddenField;
        //erec_UgyUgyiratdarabok.Updated.UgyUgyirat_Id = pageView.GetUpdatedByView(Ugy_UgyiratTextBox);


        erec_UgyUgyiratdarabok.Azonosito = Azonosito_TextBox.Text;
        erec_UgyUgyiratdarabok.Updated.Azonosito = pageView.GetUpdatedByView(Azonosito_TextBox);

        //erec_UgyUgyiratdarabok.EljarasiSzakasz = EljarasiSzakasz_KodtarakDropDownList.SelectedValue;
        //erec_UgyUgyiratdarabok.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(EljarasiSzakasz_KodtarakDropDownList);

        erec_UgyUgyiratdarabok.Leiras = Leiras_TextBox.Text;
        erec_UgyUgyiratdarabok.Updated.Leiras = pageView.GetUpdatedByView(Leiras_TextBox);

        //erec_UgyUgyiratdarabok.Hatarido = HatarIdo_CalendarControl.Text;
        //erec_UgyUgyiratdarabok.Updated.Hatarido = pageView.GetUpdatedByView(HatarIdo_CalendarControl);

        //erec_UgyUgyiratdarabok.Csoport_Id_Felelos = CsoportID_Felelos_CsoportTextBox.Id_HiddenField;
        //erec_UgyUgyiratdarabok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(CsoportID_Felelos_CsoportTextBox);

        //erec_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
        //erec_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox);

        //erec_UgyUgyiratdarabok.ElintezesMod = ElintezesMod_KodtarakDropDownList.SelectedValue;
        //erec_UgyUgyiratdarabok.Updated.ElintezesMod = pageView.GetUpdatedByView(ElintezesMod_KodtarakDropDownList);

        //erec_UgyUgyiratdarabok.ElintezesDat = ElintezesDat_CalendarControl.Text;
        //erec_UgyUgyiratdarabok.Updated.ElintezesDat = pageView.GetUpdatedByView(ElintezesDat_CalendarControl);

        //erec_UgyUgyiratdarabok.LezarasOka = LezarasOka_KodtarakDropDownList.SelectedValue;
        //erec_UgyUgyiratdarabok.Updated.LezarasOka = pageView.GetUpdatedByView(LezarasOka_KodtarakDropDownList);

        //erec_UgyUgyiratdarabok.LezarasDat = LezarasDat_CalendarControl.Text;
        //erec_UgyUgyiratdarabok.Updated.LezarasDat = pageView.GetUpdatedByView(LezarasDat_CalendarControl);
        
        ////erec_UgyUgyiratdarabok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        ////erec_UgyUgyiratdarabok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ////erec_UgyUgyiratdarabok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        ////erec_UgyUgyiratdarabok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        erec_UgyUgyiratdarabok.Base.Ver = FormHeader1.Record_Ver;
        erec_UgyUgyiratdarabok.Base.Updated.Ver = true;

        return erec_UgyUgyiratdarabok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Azonosito_TextBox);
            compSelector.Add_ComponentOnClick(Ugy_UgyiratTextBox);
            compSelector.Add_ComponentOnClick(EljarasiSzakasz_KodtarakDropDownList);
            compSelector.Add_ComponentOnClick(Leiras_TextBox);
            compSelector.Add_ComponentOnClick(HatarIdo_CalendarControl);
            //compSelector.Add_ComponentOnClick(CsoportID_Felelos_CsoportTextBox);
            //compSelector.Add_ComponentOnClick(FelhasznaloCsop_Ugyintezo_FelhasznaloCsoportTextBox);
            //compSelector.Add_ComponentOnClick(ElintezesMod_KodtarakDropDownList);
            //compSelector.Add_ComponentOnClick(ElintezesDat_CalendarControl);
            //compSelector.Add_ComponentOnClick(LezarasOka_KodtarakDropDownList);
            //compSelector.Add_ComponentOnClick(LezarasDat_CalendarControl);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + Command))
            {
                switch (Command)
                {
                    //case CommandName.New:
                    //    {
                    //        EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                    //        EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = GetBusinessObjectFromComponents();

                    //        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    //        Result result = null;
                    //        result = service.Insert(execParam, erec_UgyUgyiratdarabok);

                    //        if (String.IsNullOrEmpty(result.ErrorCode))
                    //        {
                    //            // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                    //            // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                    //            String stringVissza = StringFormatter.GetUgyiratDarabMegnevezes(
                    //                Ugy_UgyiratTextBox.Text, EljarasiSzakasz_KodtarakDropDownList.Text);
                    //            if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, stringVissza))
                    //            {
                    //                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    //            }
                    //            else
                    //            {
                    //                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                    //            }

                    //        }
                    //        else
                    //        {
                    //            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    //        }
                    //        break;
                    //    }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                                EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_UgyUgyiratdarabok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
