using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;

public partial class UgyUgyiratdarabokList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    //private const string defaultSortExpression = "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratdarabok.Foszam,EREC_IraIktatokonyvek.Ev";

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratdarabokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.UgyUgyiratdarabokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratdarabokSearch);

        ListHeader1.NewVisible = false;        
        ListHeader1.InvalidateVisible = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        //ListHeader1.ElintezetteNyilvanitasVisible = true;
        //ListHeader1.LezarasVisible = true;

        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;        

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokSearch.aspx", ""
            , 800, 650, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                
        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(UgyUgyiratdarabokGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UgyUgyiratdarabokGridView.ClientID);

        //ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(UgyUgyiratdarabokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(UgyUgyiratdarabokGridView.ClientID);

    //    ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratdarabokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratdarabokGridView.ClientID) + JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, "", true);

        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyUgyiratdarabokistajaPrintForm.aspx");

        string column_v = "";
        for (int i = 0; i < UgyUgyiratdarabokGridView.Columns.Count; i++)
        {
            if (UgyUgyiratdarabokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratdarabokSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        
        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = UgyUgyiratdarabokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(UgyUgyiratdarabokGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratdarabokSearch());

        if (!IsPostBack) UgyUgyiratdarabokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.New);
        ListHeader1.NewEnabled = false;

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        //ListHeader1.ElintezetteNyilvanitasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratdarabElintezes");
        //ListHeader1.LezarasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratdarabLezaras");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyiratdarab" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(UgyUgyiratdarabokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        string column_v = "";
        for (int i = 0; i < UgyUgyiratdarabokGridView.Columns.Count; i++)
        {
            if (UgyUgyiratdarabokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratdarabokSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
    }


    #endregion


    #region Master List

    protected void UgyUgyiratdarabokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratdarabokGridView", ViewState, "EREC_UgyUgyiratdarabok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("UgyUgyiratdarabokGridView", ViewState,SortDirection.Descending);

        UgyUgyiratdarabokGridViewBind(sortExpression, sortDirection);
    }

    protected void UgyUgyiratdarabokGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratdarabokSearch search = (EREC_UgyUgyiratdarabokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratdarabokSearch());
        search.OrderBy = Search.GetOrderBy("UgyUgyiratdarabokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search,true);
        //Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(UgyUgyiratdarabokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void UgyUgyiratdarabokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void UgyUgyiratdarabokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, UgyUgyiratdarabokCPE);

        ListHeader1.RefreshPagerLabel();
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        UgyUgyiratdarabokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, UgyUgyiratdarabokCPE);
        UgyUgyiratdarabokGridViewBind();
    }


    protected void UgyUgyiratdarabokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(UgyUgyiratdarabokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //UgyUgyiratdarabokGridView_SelectRowCommand(id);
        }
    }

    //private void UgyUgyiratdarabokGridView_SelectRowCommand(string felhasznaloId)
    //{
    //    ActiveTabRefresh(TabContainer1, felhasznaloId);
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID);
            string tableName = "EREC_UgyUgyiratdarabok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID);
            
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotById(id, execParam, EErrorPanel1);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(ugyiratDarabStatusz.Ugyirat_Id,execParam,EErrorPanel1);
            ErrorDetails errorDetail;

            // M�dos�t�s
            if (!UgyiratDarabok.Modosithato(ugyiratDarabStatusz, execParam, out errorDetail))
            {

                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratDarabMegtekintes 
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail,Page) + "')) {" +
                    JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                    + "} else return false;";
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
                
            //// Elint�zett� nyilv�n�t�s:
            //if (UgyiratDarabok.ElintezetteNyilvanithato(ugyiratDarabStatusz,ugyiratStatusz,execParam))
            //{
            //    ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
            //        , QueryStringVars.UgyiratDarabId + "=" + id
            //        , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.ElintezetteNyilvanitasOnClientClick = "alert('" + Resources.Error.ErrorCode_52253 + "'); return false;";
            //}

            //// Lez�r�s
            //if (UgyiratDarabok.Lezarhato(ugyiratDarabStatusz, ugyiratStatusz, execParam))
            //{
            //    ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClick("LezarasForm.aspx"
            //        , QueryStringVars.UgyiratDarabId + "=" + id
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratdarabokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.LezarasOnClientClick = "alert('" + Resources.Error.ErrorCode_52371 + "'); return false;";
            //}

        }
    }

    protected void UgyUgyiratdarabokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    UgyUgyiratdarabokGridViewBind();
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedUgyUgyiratdarabok();
        //    UgyUgyiratdarabokGridViewBind();
        //}
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedUgyiratdarabRecords();
                UgyUgyiratdarabokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedUgyiratdarabRecords();
                UgyUgyiratdarabokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedUgyUgyiratdarabok();
                break;
        }
    }

    private void LockSelectedUgyiratdarabRecords()
    {
        LockManager.LockSelectedGridViewRecords(UgyUgyiratdarabokGridView, "EREC_UgyUgyiratdarabok"
            , "UgyiratdarabLock", "UgyiratdarabForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedUgyiratdarabRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(UgyUgyiratdarabokGridView, "EREC_UgyUgyiratdarabok"
            , "UgyiratdarabLock", "UgyiratdarabForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

   
    /// <summary>
    /// Elkuldi emailben a UgyUgyiratdarabokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    
    private void SendMailSelectedUgyUgyiratdarabok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(UgyUgyiratdarabokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_UgyUgyiratdarabok");
        }
    }

    protected void UgyUgyiratdarabokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        UgyUgyiratdarabokGridViewBind(e.SortExpression, UI.GetSortToGridView("UgyUgyiratdarabokGridView", ViewState, e.SortExpression));
    }

    #endregion


}
