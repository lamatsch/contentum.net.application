using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class UgyUgyiratdarabokLovList : Contentum.eUtility.UI.PageBase
{

    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";    
    private const string defaultSortExpression = "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratdarabok.Foszam,EREC_IraIktatokonyvek.Ev";


    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratdarabokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("UgyUgyiratdarabokForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            //IraIktatoKonyvekDropDownList1.FillAndSetEmptyValue(false, false, Constants.IktatoErkezteto.Iktato, LovListHeader1.ErrorPanel);
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , "", "", true, false, LovListHeader1.ErrorPanel);

            EljarasiSzakasz_KodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASI_SZAKASZ, LovListHeader1.ErrorPanel);
            //FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        //if (IsPostBack)
        //{
        //    String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
        //    RefreshOnClientClicks(selectedRowId);
        //}
    }

    //private void RefreshOnClientClicks(string id)
    //{
    //    if (!String.IsNullOrEmpty(id))
    //    {

    //        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
    //            "UgyUgyiratdarabokForm.aspx", QueryStringVars.Command + "=" + CommandName.View
    //            + "&" + QueryStringVars.Id + "=" + id
    //        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, null);
    //    }
    //}

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page,GridViewCPE );
        FillGridViewSearchResult(false);
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
        EREC_UgyUgyiratdarabokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_UgyUgyiratdarabokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratdarabokSearch());
        }
        else
        {
            search = new EREC_UgyUgyiratdarabokSearch();
            //if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            //{
            //    search.IraIktatokonyv_Id.Value = IraIktatoKonyvekDropDownList1.SelectedValue;
            //    search.IraIktatokonyv_Id.Operator = Query.Operators.equals;
            //}
            if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue)
                || !String.IsNullOrEmpty(Iktatokonyv_EvIntervallum_SearchFormControl.EvTol)
                || !String.IsNullOrEmpty(Iktatokonyv_EvIntervallum_SearchFormControl.EvIg))
            {
                if (search.Extended_EREC_IraIktatoKonyvekSearch == null)
                {
                    search.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                }
                //Search.ClearErvenyessegFilter(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvKezd
                //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvVege);
                if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    IraIktatoKonyvekDropDownList1.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
                }

                Iktatokonyv_EvIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);
            }

            /// Foszam be�ll�t�sa
            if (!FoSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
            {
                FoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Foszam);
            }

            if (!String.IsNullOrEmpty(EljarasiSzakasz_KodtarakDropDownList.SelectedValue))
            {
                search.EljarasiSzakasz.Value = EljarasiSzakasz_KodtarakDropDownList.SelectedValue;
                search.EljarasiSzakasz.Operator = Query.Operators.equals;
            }
        }

        search.OrderBy = defaultSortExpression;

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search,true);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string ugyiratMegnevezes = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string hatarido = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string lezarasDat = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 5);

                    string resultText = StringFormatter.GetUgyiratDarabMegnevezes(ugyiratMegnevezes, hatarido, lezarasDat);

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, resultText);
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
