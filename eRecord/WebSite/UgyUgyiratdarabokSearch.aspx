<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="UgyUgyiratdarabokSearch.aspx.cs" Inherits="UgyUgyiratdarabokSearch" Title="�gyiratdarabok keres�se" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc12" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc5" %>
    <%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>  
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Azonos�t�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Azonosito_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="�gyirat:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:UgyiratTextBox ID="UgyUgyiratTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label7" runat="server" Text="Elj�r�si szakasz:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="EljarasiSzakasz_KodtarakDropDownList" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label9" runat="server" Text="Le�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Leiras_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label15" runat="server" Text="Hat�rid�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <span style="color: red">
                                <uc12:DatumIntervallum_SearchCalendarControl ID="Hatarido_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                                </span>
                            </td>
                        </tr>   
                                                                     
                      <%--  <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label2" runat="server" Text="Kezel�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="CsoportID_Felelos_CsoportTextBox" runat="server" FelhasznaloCsoport="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="�gyint�z�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="FelhasznaloCsop_Ugyintezo_CsoportTextBox" runat="server"
                                    FelhasznaloCsoport="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label11" runat="server" Text="Elint�z�s m�dja"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="ElintezesMod_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label10" runat="server" Text="�gyirat elint�z�si id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:DatumIntervallum_SearchCalendarControl ID="ElintezesDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label13" runat="server" Text="Lez�r�s oka"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="LezarasOka_KodtarakDropDownList" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                &nbsp;<asp:Label ID="Label3" runat="server" Text="Lez�r�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:DatumIntervallum_SearchCalendarControl ID="LezarasDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                                &nbsp;</td>
                        </tr>     --%>   
                        
                        <tr class="urlapSor">
                                <td colspan="2">
                                    &nbsp; &nbsp; &nbsp;&nbsp;
                                    <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />                                    
                                </td>
                            </tr>                                                                
                    </table>
                    </eUI:eFormPanel>  
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">                            
                            </td>
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                    </table>
                </td>
            </tr>
        </table>
</asp:Content>

