using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;


public partial class UgyUgyiratdarabokSearch : Contentum.eUtility.UI.PageBase
{
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_ELINTEZESMOD = "ELINTEZESMOD";
    private const string kcs_LEZARAS_OKA = "LEZARAS_OKA";

    private Type _type = typeof(EREC_UgyUgyiratdarabokSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.UgyUgyiratdarabokSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_UgyUgyiratdarabokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_UgyUgyiratdarabokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratdarabokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = null;
        if (searchObject != null) erec_UgyUgyiratdarabokSearch = (EREC_UgyUgyiratdarabokSearch)searchObject;

        if (erec_UgyUgyiratdarabokSearch != null)
        {
            Azonosito_TextBox.Text = erec_UgyUgyiratdarabokSearch.Azonosito.Value;

            UgyUgyiratTextBox.Id_HiddenField = erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value;
            UgyUgyiratTextBox.SetUgyiratTextBoxById(SearchHeader1.ErrorPanel, null);

            EljarasiSzakasz_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELJARASI_SZAKASZ
                , erec_UgyUgyiratdarabokSearch.EljarasiSzakasz.Value, true, SearchHeader1.ErrorPanel);

            Leiras_TextBox.Text = erec_UgyUgyiratdarabokSearch.Leiras.Value;

            Hatarido_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                    erec_UgyUgyiratdarabokSearch.Hatarido);

            //CsoportID_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabokSearch.Csoport_Id_Felelos.Value;
            //CsoportID_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //FelhasznaloCsop_Ugyintezo_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabokSearch.FelhasznaloCsoport_Id_Ugyintez.Value;
            //FelhasznaloCsop_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //ElintezesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELINTEZESMOD,
            //        erec_UgyUgyiratdarabokSearch.ElintezesMod.Value, true, SearchHeader1.ErrorPanel);

            //ElintezesDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
            //        erec_UgyUgyiratdarabokSearch.ElintezesDat);

            //LezarasOka_KodtarakDropDownList.FillAndSetSelectedValue(kcs_LEZARAS_OKA,
            //        erec_UgyUgyiratdarabokSearch.LezarasOka.Value, true, SearchHeader1.ErrorPanel);

            //LezarasDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
            //        erec_UgyUgyiratdarabokSearch.LezarasDat);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_UgyUgyiratdarabokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = (EREC_UgyUgyiratdarabokSearch)SearchHeader1.TemplateObject;
        if (erec_UgyUgyiratdarabokSearch == null)
        {
            erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
        }

        if (!String.IsNullOrEmpty(Azonosito_TextBox.Text))
        {
            erec_UgyUgyiratdarabokSearch.Azonosito.Value = Azonosito_TextBox.Text;
            erec_UgyUgyiratdarabokSearch.Azonosito.Operator = Search.GetOperatorByLikeCharater(Azonosito_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(UgyUgyiratTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = UgyUgyiratTextBox.Id_HiddenField;
            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(EljarasiSzakasz_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratdarabokSearch.EljarasiSzakasz.Value = EljarasiSzakasz_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratdarabokSearch.EljarasiSzakasz.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Leiras_TextBox.Text))
        {
            erec_UgyUgyiratdarabokSearch.Leiras.Value = Leiras_TextBox.Text;
            erec_UgyUgyiratdarabokSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(Leiras_TextBox.Text);
        }

        /// erec_UgyUgyiratdarabokSearch.Hatarido
        Hatarido_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratdarabokSearch.Hatarido);

        //if (!String.IsNullOrEmpty(CsoportID_Felelos_CsoportTextBox.Id_HiddenField))
        //{
        //    erec_UgyUgyiratdarabokSearch.Csoport_Id_Felelos.Value = CsoportID_Felelos_CsoportTextBox.Id_HiddenField;
        //    erec_UgyUgyiratdarabokSearch.Csoport_Id_Felelos.Operator = Query.Operators.equals;
        //}
        //if (!String.IsNullOrEmpty(FelhasznaloCsop_Ugyintezo_CsoportTextBox.Id_HiddenField))
        //{
        //    erec_UgyUgyiratdarabokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = FelhasznaloCsop_Ugyintezo_CsoportTextBox.Id_HiddenField;
        //    erec_UgyUgyiratdarabokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;
        //}
        //if (!String.IsNullOrEmpty(ElintezesMod_KodtarakDropDownList.SelectedValue))
        //{
        //    erec_UgyUgyiratdarabokSearch.ElintezesMod.Value = ElintezesMod_KodtarakDropDownList.SelectedValue;
        //    erec_UgyUgyiratdarabokSearch.ElintezesMod.Operator = Query.Operators.equals;
        //}

        ///// erec_UgyUgyiratdarabokSearch.ElintezesDat
        //ElintezesDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratdarabokSearch.ElintezesDat);

        //if (!String.IsNullOrEmpty(LezarasOka_KodtarakDropDownList.SelectedValue))
        //{
        //    erec_UgyUgyiratdarabokSearch.LezarasOka.Value = LezarasOka_KodtarakDropDownList.SelectedValue;
        //    erec_UgyUgyiratdarabokSearch.LezarasOka.Operator = Query.Operators.equals;
        //}

        //LezarasDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratdarabokSearch.LezarasDat);

        return erec_UgyUgyiratdarabokSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_UgyUgyiratdarabokSearch searchObject = SetSearchObjectFromComponents();

            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_UgyUgyiratdarabokSearch GetDefaultSearchObject()
    {
        return new EREC_UgyUgyiratdarabokSearch();
    }

}
