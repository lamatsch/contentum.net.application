﻿<%@ Page Title="Határidő figyelő" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UgyUgyiratokCalendarView.aspx.cs" Inherits="UgyUgyiratokCalendarView" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>
<%@ Register Src="~/Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="JavaScripts/tooltipster.bundle.min.css" rel="stylesheet" />
    <link href="JavaScripts/tooltipster-sideTip-light.min.css" rel="stylesheet" />
    <style>
        .UgyiratGrid {
            width:100%;
        }

        .NaptarGrid th {
            width: 20px;
        }

        .NaptarGrid td, .UgyiratGrid td {
        }

        .GridViewWeekendItem {
            background-color: rgb(83,141,213) !important;
        }

        .GridViewTodayItem {
            background-color: yellow !important;
        }

        .SakkoraNincs {
           background-color: rgb(238,236,225);
        }

        .SakkoraMegall {
           background-color: rgb(255,239,189);
        }

        .SakkoraVegeJogerositesreVar {
           background-color: rgb(200,255,179);
        }

        .SakkoraVege {
           background-color: rgb(146,208,80);
        }

        .Hatarido {
            border: 1px solid red;
        }

        .HataridonTuli {
            background-color:red !important;
            color:white;
        }

        .GridViewHeaderStyle {
            background-image:none;
        }

        .tooltip_templates { display: none; }

        .tooltipster-sidetip.tooltipster-light.tooltipster-light-customized .tooltipster-box {
        }

        .UgyiratSulyosHiba{
            background-color: red;
        }

        .UgyiratHiba
        {
            background-color:yellow;
        }

        .UgyiratOk
        {
            background-color:green;
        }

        .UgyiratSulyosHiba, .UgyiratHiba, .UgyiratOk {
            padding: 5px;
        }

        .tooltip_template ul {
              margin-bottom: 2px;
              margin-top: 2px;
            }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/jquery-1.12.4.min.js" />
            <asp:ScriptReference Path="~/JavaScripts/tooltipster.bundle.min.js" />
        </Scripts>
    </asp:ScriptManager>
    <uc:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="UgyiratokUpdatePanel" runat="server" OnLoad="UgyiratokUpdatePanel_Load">
                    <ContentTemplate>
                        <div style="margin-bottom: 5px;">
                            <eUI:eFormPanel ID="UgyiratokSearchPanel" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelUgyFajta" runat="server" Text="Ügy fajtája:" />
                                </td>
                                <td class="mrUrlapMezo" style="padding-right: 10px">
                                     <uc:KodtarakDropDownList ID="UgyFajtajaDropDownList" runat="server">
                                      </uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelUgyintezo" runat="server" Text="Ügyintéző" />
                                </td>
                                <td class="mrUrlapMezo" style="padding-right: 10px">
                                    <uc:CsoportTextBox ID="UgyintezoCsoportTexBox" runat="server" SearchMode="true" Validate="true" TryFireChangeEvent="false"/>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelDatumIntervallum" runat="server" Text="Ügyintézési határidő:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" style="padding-right: 10px">
                                    <uc:DatumIntervallum_SearchCalendarControl ID="DatumIntervallumSearch" runat="server" Validate="true" />
                                </td>
                                <td class="mrUrlapCaption">

                                </td>
                                <td class="mrUrlapMezo">
                                     <asp:CheckBox ID="CsakAktivIrat_CheckBox" runat="server" Checked="true" Text="Aktív állapotban lévő tételek"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="4">
                                    <asp:ImageButton TabIndex="1" ID="ImageSave" runat="server"
                                        ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                        onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                        CommandName="Save" OnClick="ImageSave_Click"/>
                                </td>
                            </tr>
                        </table>
                            </eUI:eFormPanel>
                       </div>
                        <div>
                            <asp:Panel ID="Panel1" runat="server" style="width:100%;overflow:hidden;">
                                <div style="float:left; width:50%;overflow-x:scroll">
                                    <asp:GridView ID="UgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="UgyiratokGridView_RowCommand" OnPreRender="UgyiratokGridView_PreRender"
                                            OnSorting="UgyiratokGridView_Sorting" OnRowDataBound="UgyiratokGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id"
                                            CssClass="UgyiratGrid" OnDataBound="UgyiratokGridView_DataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:BoundField DataField="Ev" HeaderText="Év" SortExpression="Ev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Eloszam" HeaderText="Iktatókönyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely" > 
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
<%--                                                <asp:BoundField DataField="Foszam" HeaderText="Főszám" SortExpression="Foszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Főszám" SortExpression="Foszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="FoszamLink" runat="server" NavigateUrl="#" Text='<%# Eval("Foszam") %>' onclick='<%# GetUgyiratLink(Eval("Id") as Guid?) %>' style="text-decoration:underline;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Ugyirat_helye" HeaderText="Ügyirat helye" SortExpression="Csoportok_UgyiratHelye.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyirat_ugyintezo_neve" HeaderText="Ügyintéző" SortExpression="Csoportok_UgyintezoNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Hatarido" HeaderText="Ügyintézési határidő" SortExpression="erec_ugyugyiratok.Hatarido">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugy_fajtaja" HeaderText="Ügyfajta" SortExpression="Ugy_fajtaja">
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewBorderHeader"/>
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                            <%# GetUgyiratHiba(Eval("Id") as Guid?) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                </div>
                                <div style="width:50%; float:left; overflow-x:scroll; text-align:left">
                                    <asp:GridView ID="NaptarGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id"
                                                CssClass="NaptarGrid" OnDataBound="NaptarGridView_DataBound">
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                <PagerSettings Visible="False" />
                                            </asp:GridView>
                                </div>
                        </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <script>
        function SetRowHeights() {
            var copyRows = $('table.NaptarGrid tr');
            $('table.UgyiratGrid tr').each(function (i, tr) {
                var copyTr = copyRows.eq(i);
                var $tr = $(tr);
                var height1 = $tr.height();
                var height2 = copyTr.height();

                if (height1 != height2) {
                    var height = Math.max(height1, height2);
                    copyTr.height(height);
                    $tr.height(height);
                }
            });
        }

        function SetTooltip() {
            $('.tooltip').tooltipster(
                {
                    delay: 200,
                    theme: ['tooltipster-light', 'tooltipster-light-customized'],
                    animationDuration: 0
                });
        }

        SetRowHeights();

        function pageLoad() {
            SetRowHeights();
            SetTooltip();
        }

    </script>
</asp:Content>

