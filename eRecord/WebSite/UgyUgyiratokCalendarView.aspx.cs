﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using System.Globalization;
using System.Text;
using Contentum.eAdmin.Service;

public partial class UgyUgyiratokCalendarView : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    CalendarViewResult _CalendarViewResult;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = "Határidő figyelő";
        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokCalendarViewSearch);
        // A baloldali, alapból invisible ikonok megjelenítése:
        ListHeader1.ViewVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.SearchVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UgyiratokGridView.ClientID);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = UgyiratokGridView;

        //ui.SetClientScriptToGridViewSelectDeSelectButton(UgyiratokGridView);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!IsPostBack)
        {
            UgyintezoCsoportTexBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
            UgyintezoCsoportTexBox.SetCsoportTextBoxById(EErrorPanel1);
            UgyFajtajaDropDownList.FillDropDownList("UGY_FAJTAJA", true, EErrorPanel1);
            DatumIntervallumSearch.DatumVege = DateTime.Today.ToShortDateString();
            DatumIntervallumSearch.DatumKezd = DateTime.Today.AddMonths(-3).ToShortDateString();
            //UgyiratokGridViewBind();
        }
        else
        {
            if (Request.Params["__EVENTARGUMENT"] != null && Request.Params["__EVENTTARGET"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                if (eventTarget == ListHeader1.DefaultFilterButton.ClientID)
                {
                    SetDefaultFilter();
                }

            }
        }
    }

    void SetDefaultFilter()
    {
        UgyintezoCsoportTexBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        UgyintezoCsoportTexBox.SetCsoportTextBoxById(EErrorPanel1);
        UgyFajtajaDropDownList.SelectedValue = String.Empty;
        DatumIntervallumSearch.DatumVege = DateTime.Today.ToShortDateString();
        DatumIntervallumSearch.DatumKezd = DateTime.Today.AddMonths(-3).ToShortDateString();
        CsakAktivIrat_CheckBox.Checked = true;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);

        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(UgyiratokGridView);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
    }


    #endregion

    #region Master List

    protected void UgyiratokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("UgyiratokGridView", ViewState, "EREC_UgyUgyiratok.Hatarido");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("UgyiratokGridView", ViewState, SortDirection.Descending);

        UgyiratokGridViewBind(sortExpression, sortDirection);
    }

    protected void UgyiratokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(UgyiratokGridView);

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokCalendarViewSearch search = GetSearchObject();

        search.OrderBy = Search.GetOrderBy("UgyiratokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.CalendarView(ExecParam, search);
        _CalendarViewResult = new CalendarViewResult(res, this);

        int rowCount = 0;

        if (!res.IsError)
        {
            if (res.Ds.Tables.Count > 1)
            {
                res.Ds.Tables.RemoveAt(1);
            }

            rowCount = res.Ds.Tables[0].Rows.Count;
        }

        UI.GridViewFill(UgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

        if (!res.IsError && rowCount > 0)
        {
            DataTable ugyiratokTable = res.Ds.Tables[0];

            DateTime datumtol = search.DatumTol;
            DateTime? ugyiratKezdoDatum = _CalendarViewResult.Ugyiratok.Select(d => d.Value).Min(u => u.KezdoDatum);
            if (ugyiratKezdoDatum != null)
            {
                datumtol = ugyiratKezdoDatum.Value.Date;
            }
            DateTime datumig = search.DatumIg.AddDays(14);
            FillExtraNapok(datumtol, datumig);
            NaptarGridViewBind(datumtol, datumig, ugyiratokTable);
        }
        else
        {
            NaptarGridView.DataSource = null;
            NaptarGridView.DataBind();
        }

    }

    protected void UgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void UgyiratokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = UgyiratokGridView.PageIndex;

        UgyiratokGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = UgyiratokGridView.PageCount;

        if (prev_PageIndex != UgyiratokGridView.PageIndex)
        {
            //scroll állapotának törlése
            //JavaScripts.ResetScroll(Page, UgyiratokCPE);
            UgyiratokGridViewBind();
        }
        else
        {
            //UI.GridViewSetScrollable(ListHeader1.Scrollable, UgyiratokCPE);
        }

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(UgyiratokGridView);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        UgyiratokGridViewBind();
    }

    protected void UgyiratokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(UgyiratokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight, UgyiratokUpdatePanel.ClientID);
        }
    }

    protected void UgyiratokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    UgyiratokGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedUgyiratok();
            UgyiratokGridViewBind();
        }
        else if (e.CommandName == "DefaultFilter")
        {
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedAgazatiJelRecords();
                UgyiratokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedAgazatiJelRecords();
                UgyiratokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedUgyiratok();
                break;
        }
    }

    private void LockSelectedAgazatiJelRecords()
    {
        LockManager.LockSelectedGridViewRecords(UgyiratokGridView, "EREC_Ugyiratok"
            , "AgazatiJelLock", "AgazatiJelForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedAgazatiJelRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(UgyiratokGridView, "EREC_Ugyiratok"
            , "AgazatiJelLock", "AgazatiJelForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Törli a UgyiratokGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedUgyiratok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "AgazatiJelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a UgyiratokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedUgyiratok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_Ugyiratok");
        }
    }

    protected void UgyiratokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        UgyiratokGridViewBind(e.SortExpression, UI.GetSortToGridView("UgyiratokGridView", ViewState, e.SortExpression));
    }

    #endregion

    protected void ImageSave_Click(object sender, ImageClickEventArgs e)
    {
        UgyiratokGridViewBind();
    }

    EREC_UgyUgyiratokCalendarViewSearch GetSearchObject()
    {
        EREC_UgyUgyiratokCalendarViewSearch search = new EREC_UgyUgyiratokCalendarViewSearch();
        if (!String.IsNullOrEmpty(UgyFajtajaDropDownList.SelectedValue))
        {
            search.Ugy_Fajtaja.Value = UgyFajtajaDropDownList.SelectedValue;
            search.Ugy_Fajtaja.Operator = Query.Operators.equals;
        }
        search.UgyintezoId = new Guid(UgyintezoCsoportTexBox.Id_HiddenField);
        search.DatumTol = DateTime.Parse(DatumIntervallumSearch.DatumKezd);
        search.DatumIg = DateTime.Parse(DatumIntervallumSearch.DatumVege);
        search.CsakAktivIrat = CsakAktivIrat_CheckBox.Checked;
        return search;
    }

    #region naptár
    protected void NaptarGridViewBind(DateTime datumtol, DateTime datumIg, DataTable ugyiratokTable)
    {
        NaptarGridView.Columns.Clear();
        Result res = GetNaptarAdatok(datumtol, datumIg, ugyiratokTable);
        NaptarGridView.PageIndex = UgyiratokGridView.PageIndex;
        UI.GridViewFill(NaptarGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    private Result GetNaptarAdatok(DateTime datumtol, DateTime datumIg, DataTable ugyiratokTable)
    {
        Result res = new Result();
        DataSet ds = new DataSet();

        DataTable table = new DataTable();
        table.Columns.Add("Id", typeof(Guid));

        var dates = GetIntervallum(datumtol, datumIg);

        foreach (DateTime date in dates)
        {
            table.Columns.Add(date.ToShortDateString(), typeof(DateTime));
            TemplateField field = new TemplateField();
            field.HeaderText = date.Day.ToString();
            

            string itemClass = String.Empty;
            if (IsToday(date))
            {
                itemClass = "GridViewTodayItem";
            }
            else if (IsWeekend(date))
            {
                itemClass = "GridViewWeekendItem";
            }

            field.HeaderStyle.CssClass = "GridViewBorderHeader" + " " + itemClass;
            field.ItemStyle.CssClass = "GridViewBoundFieldItemStyle" + " " + itemClass;

            NaptarItemTemplate template = new NaptarItemTemplate(DataControlRowType.DataRow, date, this, field);

            field.ItemTemplate = template;

            NaptarGridView.Columns.Add(field);
        }


        foreach (DataRow row in ugyiratokTable.Rows)
        {
            DataRow naptarRow = table.NewRow();
            naptarRow["Id"] = row["Id"];
            table.Rows.Add(naptarRow);
        }

        ds.Tables.Add(table);

        res.Ds = ds;
        return res;
    }

    List<DateTime> GetIntervallum(DateTime datumTol, DateTime datumIg)
    {
        var dates = Enumerable.Range(0, 1 + datumIg.Subtract(datumTol).Days)
          .Select(i => datumTol.AddDays(i));

        return dates.ToList();

    }
    #endregion

    protected void NaptarGridView_DataBound(object sender, EventArgs e)
    {
        if (NaptarGridView.Controls.Count > 0)
        {
            // Creating a Row
            GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            int month = -1;
            int days = -1;

            foreach (DataControlField col in NaptarGridView.Columns)
            {
                TemplateField field = col as TemplateField;

                if (field != null)
                {
                    NaptarItemTemplate naptarItem = field.ItemTemplate as NaptarItemTemplate;
                    if (naptarItem != null)
                    {
                        DateTime date = naptarItem.Date;
                        //új hónap
                        if (date.Month != month)
                        {
                            if (month > -1)
                            {
                                TableCell HeaderCell = new TableCell();
                                HeaderCell.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                                HeaderCell.ColumnSpan = days;
                                HeaderCell.CssClass = "GridViewBorderHeader";
                                HeaderRow.Cells.Add(HeaderCell);
                            }
                            month = date.Month;
                            days = 1;
                        }
                        else
                        {
                            days++;
                        }

                    }
                }
            }

            if (month > -1)
            {
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.ColumnSpan = days;
                HeaderCell.CssClass = "GridViewBorderHeader";
                HeaderRow.Cells.Add(HeaderCell);
            }


            NaptarGridView.Controls[0].Controls.AddAt(0, HeaderRow);
        }
    }

    protected void UgyiratokGridView_DataBound(object sender, EventArgs e)
    {
        // Creating a Row
        GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

        TableCell HeaderCell = new TableCell();
        HeaderCell.Text = "&nbsp;";
        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        HeaderCell.ColumnSpan = UgyiratokGridView.Columns.Count;
        HeaderCell.CssClass = "GridViewBorderHeader";
        HeaderRow.Cells.Add(HeaderCell);

        UgyiratokGridView.Controls[0].Controls.AddAt(0, HeaderRow);
    }

    bool IsWeekend(DateTime date)
    {
        if (ExtraNapok.ContainsKey(date))
        {
            if (ExtraNapok[date] == -1)
                return true;

            if (ExtraNapok[date] == 1)
                return false;
        }

        if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool IsToday(DateTime date)
    {
        return date == DateTime.Today;
    }

    public class NaptarItemTemplate : ITemplate
    {
        private DataControlRowType templateType;

        public DateTime Date { get; private set; }

        UgyUgyiratokCalendarView Page { get; set; }

        TemplateField field;

        public NaptarItemTemplate(DataControlRowType type, DateTime date, UgyUgyiratokCalendarView page, TemplateField field)
        {
            templateType = type;
            this.Date = date;
            this.Page = page;
            this.field = field;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    Literal lc = new Literal();
                    container.Controls.Add(lc);
                    break;
                case DataControlRowType.DataRow:
                    Panel pnl = new Panel();
                    pnl.ID = "pnl1" + "_" + this.Date.ToString("yyyyMMdd");
                    pnl.DataBinding += new EventHandler(this.ctl_OnDataBinding);
                    container.Controls.Add(pnl);
                    (container as WebControl).CssClass = field.ItemStyle.CssClass;
                    break;
                default:
                    break;
            }
        }

        public void ctl_OnDataBinding(object sender, EventArgs e)
        {
            if (sender.GetType().Name == "Panel")
            {
                Panel panel = (Panel)sender;
                GridViewRow container = (GridViewRow)panel.NamingContainer;
                Guid ugyiratId = (Guid)((DataRowView)container.DataItem)["Id"];
                var ugyirat = Page._CalendarViewResult.Ugyiratok[ugyiratId];
                int sakkoraIdo = ugyirat.GetSakkoraIdo(Date);
                string tooltip;

                if (ugyirat.IsBejovoIrat(this.Date) && ugyirat.IsPostazottIrat(this.Date))
                {
                    Image img = new Image();
                    img.ImageUrl = "~/images/hu/egyeb/treeview_bejovo_kimeno_irat.gif";
                    panel.Controls.Add(img);

                    Page.AddTooltip(img, this.Date, ugyirat);
                }
                else if (ugyirat.IsBejovoIrat(this.Date))
                {
                    Image img = new Image();
                    img.ImageUrl = "~/images/hu/egyeb/treeview_bejovo_irat.gif";
                    panel.Controls.Add(img);

                    Page.AddTooltip(img, this.Date, ugyirat);

                }
                else if (ugyirat.IsPostazottIrat(this.Date))
                {
                    Image img = new Image();
                    img.ImageUrl = "~/images/hu/egyeb/treeview_kimeno_irat.gif";
                    panel.Controls.Add(img);

                    Page.AddTooltip(img, this.Date, ugyirat);
                }
                else if (ugyirat.IsKimenoIrat(this.Date))
                {
                    Image img = new Image();
                    img.ImageUrl = "~/images/hu/egyeb/treeview_iktatott_irat.gif";
                    panel.Controls.Add(img);

                    Page.AddTooltip(img, this.Date, ugyirat);
                }
                else if (ugyirat.Tertivevenyek(this.Date, out tooltip))
                {
                    Image img = new Image();
                    img.ImageUrl = "~/images/hu/ikon/tertiveveny.gif";
                    panel.Controls.Add(img);

                    Page.AddTooltip(img, tooltip, String.Format("tooltip_{0}_{1}", ugyiratId, this.Date.ToString("yyyyMMhh")));
                }
                else
                {
                    if (sakkoraIdo > 0 && ugyirat.SakkoraAllapot.Progress)
                    {
                        Label lbl = new Label();
                        lbl.Text = sakkoraIdo.ToString();
                        panel.Controls.Add(lbl);
                    }
                }

                string itemCssClass = String.Empty;

                if (ugyirat.SakkoraAllapot == null)
                {
                    itemCssClass = "SakkoraNincs";
                }
                else
                {
                    if (ugyirat.SakkoraAllapot.Pause)
                    {
                        itemCssClass = "SakkoraMegall";
                    }
                    else if (ugyirat.SakkoraAllapot.Stop)
                    {
                        if (ugyirat.JogerositesreVar(this.Date))
                        {
                            itemCssClass = "SakkoraVegeJogerositesreVar";
                        }
                        else
                        {
                            itemCssClass = "SakkoraVege";
                        }
                    }
                }

                if (sakkoraIdo > 0  && ugyirat.Hatarido != null && ugyirat.SakkoraAllapot.Progress)
                {
                    if (this.Date == ugyirat.Hatarido.Value.Date)
                    {
                        itemCssClass = "Hatarido";
                    }
                    else
                    if (this.Date > ugyirat.Hatarido)
                    {
                        itemCssClass = "HataridonTuli";
                    }
                }

                (panel.Parent as WebControl).CssClass += " " + itemCssClass;

            }
        }
    }

    private Panel GetTooltip(DateTime date,CalendarViewResult.UgyiratRow ugyirat, out Panel tooltip)
    {
        string tooltipId = String.Format("tooltip_{0}_{1}", ugyirat.Id, date.ToString("yyyyMMdd"));

        Panel tooltipWrapper = new Panel();
        tooltipWrapper.CssClass = "tooltip_templates";

        tooltip = new Panel();
        tooltip.ID = tooltipId;
        tooltip.CssClass = "tooltip_template";

        Literal li = new Literal();

        StringBuilder sb = new StringBuilder();
        sb.Append(date.ToShortDateString());
        if (ugyirat.SakkoraAllapot != null)
        {
            if (ugyirat.SakkoraAllapot.Progress)
            {
                sb.Append(" - Indul");
            }

            if (ugyirat.SakkoraAllapot.Pause)
            {
                sb.Append(" - Megáll");
            }

            if (ugyirat.SakkoraAllapot.Stop)
            {
                sb.Append(" - Vége");
            }
        }
        sb.Append("<ul>");
        foreach (var irat in ugyirat.GetIratok(date))
        {
            sb.Append(String.Format("<li>{0} {1} {2} - {3} - {4}</li>", 
                irat.Alszam, irat.Irat_iranya_nev, irat.Irat_statisztikaT1, irat.Irat_statisztikaT2, irat.Irat_statisztikaT3));
        }
        sb.Append("</ul>");

        li.Text = sb.ToString();

        tooltip.Controls.Add(li);
        tooltipWrapper.Controls.Add(tooltip);

        return tooltipWrapper;
    }

    private Panel GetTooltip(string tooltipText, string tooltipId, out Panel tooltip)
    {
        Panel tooltipWrapper = new Panel();
        tooltipWrapper.CssClass = "tooltip_templates";

        tooltip = new Panel();
        tooltip.ID = tooltipId;
        tooltip.CssClass = "tooltip_template";

        Literal li = new Literal();
        li.Text = tooltipText;

        tooltip.Controls.Add(li);
        tooltipWrapper.Controls.Add(tooltip);

        return tooltipWrapper;
    }


    private void AddTooltip(WebControl control, DateTime date, CalendarViewResult.UgyiratRow ugyirat)
    {
        control.CssClass = "tooltip";

        Panel tooltip;
        Panel tooltipWrapper = GetTooltip(date, ugyirat, out tooltip);
        control.Parent.Controls.Add(tooltipWrapper);


        control.Attributes.Add("data-tooltip-content", "#" + tooltip.ClientID);
    }

    private void AddTooltip(WebControl control, string tooltipText, string tooltipId)
    {
        control.CssClass = "tooltip";

        Panel tooltip;
        Panel tooltipWrapper = GetTooltip(tooltipText, tooltipId, out tooltip);
        control.Parent.Controls.Add(tooltipWrapper);


        control.Attributes.Add("data-tooltip-content", "#" + tooltip.ClientID);
    }

    public string GetUgyiratHiba(Guid? ugyiratId)
    {
        if (ugyiratId != null)
        {
            Panel wrapper = new Panel();
            Panel pnl = new Panel();
            wrapper.Controls.Add(pnl);

            var ugyirat = _CalendarViewResult.Ugyiratok[ugyiratId.Value];

            string error;
            if (ugyirat.IsSulyosHiba(out error))
            {
                AddTooltip(pnl, error, ugyiratId.ToString());
                pnl.CssClass += " UgyiratSulyosHiba";
            }
            else if (ugyirat.IsHiba(out error))
            {
                AddTooltip(pnl, error, ugyiratId.ToString());
                pnl.CssClass += " UgyiratHiba";
            }
            else
            {
                pnl.CssClass = "UgyiratOk";
            }

            return UI.RenderControl(wrapper);
        }

        return String.Empty;
    }

    public class CalendarViewResult
    {
        public class UgyiratRow
        {
            public Guid Id { get; set; }
            public int Ev { get; set; }
            public int? Foszam { get; set; }
            public string Ugyirat_helye { get; set; }
            public string Ugyirat_ugyintezo_neve { get; set; }
            public string Targy { get; set; }
            public DateTime? Hatarido { get; set; }
            public string ElteltIdo { get; set; }
            public string Ugy_fajtaja { get; set; }
            public string Szignalt_ugyirat { get; set; }
            public string Ugyirat_statisztikaT1 { get; set; }

            public List<IratRow> Iratok = new List<IratRow>();

            public bool IsBejovoIrat(DateTime date)
            {
                return Iratok.Exists(i => i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Bejovo 
                && i.Kuldemeny_Beerkezes_datuma.HasValue 
                && i.Kuldemeny_Beerkezes_datuma.Value.Date == date);
            }

            public bool IsKimenoIrat(DateTime date)
            {
                return Iratok.Exists(i => i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Belso
                && i.Irat_Iktatas_datuma.HasValue
                && i.Irat_Iktatas_datuma.Value.Date == date
                && i.Postazas_datuma == null);
            }

            public bool IsPostazottIrat(DateTime date)
            {
                return Iratok.Exists(i => i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Belso
                && i.Postazas_datuma.HasValue
                && i.Postazas_datuma.Value.Date == date);
            }

            public List<IratRow> GetIratok(DateTime date)
            {
                return Iratok.Where(i => (i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Bejovo
                && i.Kuldemeny_Beerkezes_datuma.HasValue
                && i.Kuldemeny_Beerkezes_datuma.Value.Date == date)
                || (i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Belso
                && i.Irat_Iktatas_datuma.HasValue
                && i.Irat_Iktatas_datuma.Value.Date == date
                && i.Postazas_datuma == null)
                || (i.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Belso
                && i.Postazas_datuma.HasValue
                && i.Postazas_datuma.Value.Date == date)).ToList();
            }

            public bool IsHiba(out string error)
            {
                bool isHiba = false;
                error = String.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append("<ul>");

                foreach(var irat in Iratok)
                {
                    if (String.IsNullOrEmpty(irat.Irat_hatasa_sakkorara))
                    {
                        sb.Append("<li>");
                        sb.Append(String.Format("Az {0}. alszámhoz nincs sakkóra!", irat.Alszam));
                        sb.Append("</li>");
                        isHiba = true;
                    }

                    if (String.IsNullOrEmpty(irat.Irat_statisztikaT1))
                    {
                        sb.Append("<li>");
                        sb.Append(String.Format("Az {0}. alszámhoz nincs statisztikai adat!", irat.Alszam));
                        sb.Append("</li>");
                        isHiba = true;
                    }
                }

                sb.Append("</ul>");

                if (isHiba)
                {
                    error = sb.ToString();
                }

                return isHiba;
            }

            public bool IsSulyosHiba(out string error)
            {
                bool isHiba = false;
                error = String.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append("<ul>");
                if (String.IsNullOrEmpty(Ugyirat_statisztikaT1))
                {
                    sb.Append("<li>");
                    sb.Append("Ügyirat statisztika nincs kitöltve!");
                    sb.Append("</li>");
                    isHiba = true;
                }

                if (Szignalt_ugyirat != "1")
                {
                    sb.Append("<li>");
                    sb.Append("Nincs szignálva az ügyirat!");
                    sb.Append("</li>");
                    isHiba = true;
                }

                var elsoAlszam = Iratok.Where(i => i.Alszam == 1).FirstOrDefault();
                if (elsoAlszam != null && String.IsNullOrEmpty(elsoAlszam.Irat_hatasa_sakkorara))
                {
                    sb.Append("<li>");
                    sb.Append("Az első alszámhoz nincs sakkóra!");
                    sb.Append("</li>");
                    isHiba = true;
                }

                sb.Append("</ul>");

                if (isHiba)
                {
                    error = sb.ToString();
                }

                return isHiba;
            }

            public DateTime? KezdoDatum
            {
                get
                {
                    return Iratok.Min(i => i.Datum);
                }
            }

            int sakkoraIdo = 0;
            SakkoraManager.SakkoraLeiras sakkoraAllapot;

            public SakkoraManager.SakkoraLeiras SakkoraAllapot
            {
                get { return sakkoraAllapot; }
            }

            public int GetSakkoraIdo(DateTime date)
            {
                if (sakkoraAllapot != null)
                {
                    if (sakkoraAllapot.Progress)
                        sakkoraIdo++;
                }

                var napiIratok = GetIratok(date);

                foreach (var irat in napiIratok.OrderBy(i => i.Alszam))
                {
                    if (irat.Sakkora != null && !irat.Sakkora.None)
                    {
                        if (irat.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Bejovo
                            || irat.Postazas_datuma != null)
                        {
                            this.sakkoraAllapot = irat.Sakkora;
                        }
                    }
                }

                return sakkoraIdo;
            }

            public bool JogerositesreVar(DateTime date)
            {
                var jogerositendoiratok = Iratok.Where(i => i.JogerositesKell 
                && (i.JogerositesDatuma == null || i.JogerositesDatuma.Value > date)).ToList();

                return jogerositendoiratok.Count > 0;
            }

            public bool Tertivevenyek(DateTime date, out string tootlip)
            {
                bool ret = false;
                StringBuilder sb = new StringBuilder();
                //mai nap utáni nap
                if (date == DateTime.Today.AddDays(1))
                {
                    var jogerositesreVaroIratok = Iratok.Where(i => i.JogerositesKell
                    && (i.JogerositesDatuma == null)).ToList();

                    
                    foreach (var irat in jogerositesreVaroIratok)
                    {
                        if (irat.Tertiveveny_NemJottVisszaDB > 0)
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append("<br/>");
                            }
                            sb.Append(String.Format("-- {0}. alszám jogerőre vár --", irat.Alszam));
                            sb.Append("<br/>");
                            sb.Append(String.Format("Vissza nem érkezett tértivevények száma: {0} ({1})", 
                                irat.Tertiveveny_NemJottVisszaDB, irat.Tertiveveny_NemJottVisszaDB + irat.Tertiveveny_VisszaJottDB));
                            ret = true;
                        }
                    }
                }

                tootlip = sb.ToString();
                return ret;
            }

            public UgyiratRow(DataRow row)
            {
                this.Id = (Guid)row["Id"];
                this.Ev = (int)row["Ev"];
                this.Foszam = row["Foszam"] as int?;
                this.Ugyirat_helye = row["Ugyirat_helye"].ToString();
                this.Ugyirat_ugyintezo_neve = row["Ugyirat_ugyintezo_neve"].ToString();
                this.Targy = row["Targy"].ToString();
                this.Hatarido = row["Hatarido"] as DateTime?;
                this.ElteltIdo = row["ElteltIdo"].ToString();
                this.Ugy_fajtaja = row["Ugy_fajtaja"].ToString();
                this.Szignalt_ugyirat = row["Szignalt_ugyirat"].ToString();
                this.Ugyirat_statisztikaT1 = row["Ugyirat_statisztikaT1"].ToString();
            }
        }

        public class IratRow
        {
            public Guid Ugyirat_ID { get; set; }
            public Guid Id { get; set; }
            public int? Alszam { get; set; }
            public string Irat_iranya { get; set; }
            public string Irat_iranya_nev { get; set; }
            public string Irat_tipusa { get; set; }
            public string Irat_statisztikaT1 { get; set; }
            public string Irat_statisztikaT2 { get; set; }
            public string Irat_statisztikaT3 { get; set; }
            public DateTime? Kuldemeny_Beerkezes_datuma { get; set; }
            public DateTime? Irat_Iktatas_datuma { get; set; }
            public DateTime? Postazas_datuma { get; set; }
            public string Irat_hatasa_sakkorara { get; set; }
            public string Irat_jogerositendo { get; set; }
            public string Jogerore_emelkedes_datuma { get; set; }
            public int Tertiveveny_NemJottVisszaDB { get; set; }
            public int Tertiveveny_VisszaJottDB { get; set; }
            public DateTime? Tertiveveny_UtolsoVisszaDatum { get; set; }

            public bool JogerositesKell
            {
                get
                {
                    return "true".Equals(this.Irat_jogerositendo, StringComparison.InvariantCultureIgnoreCase);
                }
            }

            public DateTime? JogerositesDatuma
            {
                get
                {
                    DateTime dt;

                    if (!String.IsNullOrEmpty(this.Jogerore_emelkedes_datuma))
                    {
                        if (DateTime.TryParse(this.Jogerore_emelkedes_datuma, out dt))
                        {
                            return dt;
                        }
                    }

                    return null;
                }
            }

            public SakkoraManager.SakkoraLeiras Sakkora { get; set; }

            public DateTime? Datum
            {
                get
                {
                    if (this.Irat_iranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                        return this.Kuldemeny_Beerkezes_datuma;
                    else if (this.Postazas_datuma != null)
                        return this.Postazas_datuma;
                    else
                        return this.Irat_Iktatas_datuma;
                }
            }

            public IratRow(DataRow row, SakkoraManager sakkoraManager)
            {
                this.Ugyirat_ID = (Guid)row["Ugyirat_ID"];
                this.Id = (Guid)row["Id"];
                this.Alszam = row["Alszam"] as int?;
                this.Irat_iranya = row["Irat_iranya"].ToString();
                this.Irat_iranya_nev = row["Irat_iranya_nev"].ToString();
                this.Irat_tipusa = row["Irat_tipusa"].ToString();
                this.Irat_statisztikaT1 = row["Irat_statisztikaT1"].ToString();
                this.Irat_statisztikaT2 = row["Irat_statisztikaT2"].ToString();
                this.Irat_statisztikaT3 = row["Irat_statisztikaT3"].ToString();
                this.Kuldemeny_Beerkezes_datuma = row["Kuldemeny_Beerkezes_datuma"] as DateTime?;
                this.Irat_Iktatas_datuma = row["Irat_Iktatas_datuma"] as DateTime?;
                this.Postazas_datuma = row["Postazas_datuma"] as DateTime?;
                this.Irat_hatasa_sakkorara = row["Irat_hatasa_sakkorara"].ToString();
                this.Irat_jogerositendo = row["Irat_jogerositendo"].ToString();
                this.Jogerore_emelkedes_datuma = row["Jogerore_emelkedes_datuma"].ToString();
                this.Tertiveveny_NemJottVisszaDB = (int)row["Tertiveveny_NemJottVisszaDB"];
                this.Tertiveveny_VisszaJottDB = (int)row["Tertiveveny_VisszaJottDB"];
                this.Tertiveveny_UtolsoVisszaDatum = row["Tertiveveny_UtolsoVisszaDatum"] as DateTime?;

                this.Sakkora = sakkoraManager.GetSakkoraLeiras(this.Irat_hatasa_sakkorara);
            }
         
              
        }

        public Dictionary<Guid, UgyiratRow> Ugyiratok = new Dictionary<Guid, UgyiratRow>();

        public CalendarViewResult(Result result, Page page)
        {
            if (!result.IsError)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    UgyiratRow ugyirat = new UgyiratRow(row);
                    Ugyiratok.Add(ugyirat.Id, ugyirat);
                }

                SakkoraManager sakkoraManager = new SakkoraManager(page);

                foreach (DataRow row in result.Ds.Tables[1].Rows)
                {
                    IratRow irat = new IratRow(row, sakkoraManager);
                    Ugyiratok[irat.Ugyirat_ID].Iratok.Add(irat);
                }
            }
        }
    }

    public class SakkoraManager
    {
        public class SakkoraLeiras
        {
            public List<string> Tipusok { get; set; }

            public bool None
            {
                get
                {
                    return Tipusok.Contains("NONE");
                }
            }

            public bool Progress
            {
                get
                {
                    return Tipusok.Contains("ON_PROGRESS");
                }
            }

            public bool Pause
            {
                get
                {
                    return Tipusok.Contains("PAUSE");
                }
            }

            public bool Stop
            {
                get
                {
                    return Tipusok.Contains("STOP");
                }
            }
        }

        Page page;

        public Dictionary<string, SakkoraLeiras> SakkoraTipusok = new Dictionary<string, SakkoraLeiras>();

        public SakkoraManager(Page page)
        {
            this.page = page;

            var kodtarak = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList("IRAT_HATASA_UGYINTEZESRE", page, null);

            foreach (var kodtar in kodtarak)
            {
                string egyeb = kodtar.Egyeb;

                if (!String.IsNullOrEmpty(egyeb))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    SakkoraLeiras leiras = serializer.Deserialize<SakkoraLeiras>(egyeb);

                    if (!SakkoraTipusok.ContainsKey(kodtar.Kod))
                    {
                        SakkoraTipusok.Add(kodtar.Kod, leiras);
                    }
                }
            }
        }

        public SakkoraLeiras GetSakkoraLeiras(string kod)
        {
            if (!String.IsNullOrEmpty(kod) && SakkoraTipusok.ContainsKey(kod))
            {
                return SakkoraTipusok[kod];
            }

            return null;
        }
    }

    Dictionary<DateTime, int> ExtraNapok;

    void FillExtraNapok(DateTime kezd, DateTime veg)
    {
        ExtraNapok = new Dictionary<DateTime, int>();

        KRT_Extra_NapokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
        ExecParam xpm = UI.SetExecParamDefault(Page);

        KRT_Extra_NapokSearch search = new KRT_Extra_NapokSearch();
        search.Datum.Value = kezd.ToShortDateString();
        search.Datum.ValueTo = veg.ToShortDateString();
        search.Datum.Operator = Query.Operators.between;

        Result res = service.GetAll(xpm, search);

        if (!res.IsError)
        {
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                DateTime? datum = row["Datum"] as DateTime?;
                int? jelzo = row["Jelzo"] as int?;

                if (datum != null && jelzo != null)
                {
                    if (!ExtraNapok.ContainsKey(datum.Value))
                        ExtraNapok.Add(datum.Value, jelzo.Value);
                }
            }
        }
    }

    public string GetUgyiratLink(Guid? id)
    {
        if (id != null)
        {
            return JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                  , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyiratokUpdatePanel.ClientID);
        }

        return String.Empty;
    }
}

