<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
CodeFile="UgyUgyiratokFTSearch.aspx.cs" Inherits="UgyUgyiratokFTSearch" Title="Teljes szöveges keresés" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader"
    TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter"
    TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc10" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<uc1:SearchHeader id="SearchHeader1" runat="server" >
	</uc1:SearchHeader>
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true" >
	</asp:ScriptManager>
	<table cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td>
				&nbsp;
				<eUI:eFormPanel ID="EFormPanel3" runat="server">
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr class="urlapSor_kicsi">
							<td class="mrUrlapCaption">
								<asp:Label ID="Label14" runat="server" Text="Tárgyszavak:"></asp:Label>
							</td>
							<td class="mrUrlapMezo">
								<asp:TextBox ID="Targyszavak_TextBox" runat="server" Width="600"></asp:TextBox>
							</td>
						</tr>
					</table>
				</eUI:eFormPanel>
				<table cellpadding="0" cellspacing="0">
					<tr class="urlapSor">
						<td>
							<uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
						</td>
					</tr>
					<tr class="urlapSor">
						<td>
							<uc2:SearchFooter ID="SearchFooter1" runat="server" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</asp:Content>

