using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class UgyUgyiratokFTSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_UgyUgyiratokSearch);

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.UgyUgyiratFTSearchHeaderTitle;
        SearchHeader1.TemplateObjectType = _type;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {

            EREC_UgyUgyiratokSearch searchObject = null;

            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            
            LoadComponentsFromSearchObject(searchObject);

        }
    }
    #endregion


    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = null;
        if (searchObject != null) erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)searchObject;

        if (erec_UgyUgyiratokSearch != null)
        {
           if (erec_UgyUgyiratokSearch.fts_targyszavak != null)
           {
               Targyszavak_TextBox.Text = erec_UgyUgyiratokSearch.fts_targyszavak.Filter;
           }
           else
           {
               Targyszavak_TextBox.Text = "";
           }
        }
    }

    private EREC_UgyUgyiratokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)SearchHeader1.TemplateObject;
        if (erec_UgyUgyUgyiratokSearch == null)
        {
            erec_UgyUgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);
        }

        #region Manuálisan kezeltek (WhereByManual állítva)

        String WhereByManual = "";

        erec_UgyUgyUgyiratokSearch.WhereByManual = WhereByManual;

        if (!String.IsNullOrEmpty(Targyszavak_TextBox.Text))
        {
            erec_UgyUgyUgyiratokSearch.fts_targyszavak = new FullTextSearchField();
            erec_UgyUgyUgyiratokSearch.fts_targyszavak.Filter = Targyszavak_TextBox.Text;
        }

        #endregion
        return erec_UgyUgyUgyiratokSearch;
    }



    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_UgyUgyiratokSearch searchObject = SetSearchObjectFromComponents();
                       
            EREC_UgyUgyiratokSearch defaultSearchObject = GetDefaultSearchObject();
            if (Search.IsIdentical(searchObject, 
                	defaultSearchObject, 
                	searchObject.WhereByManual, 
                	defaultSearchObject.WhereByManual
                )
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch,defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch,defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                && ((searchObject.fts_targyszavak == null)
                || String.IsNullOrEmpty(searchObject.fts_targyszavak.Filter)) // idáig nem szabad eljutnia
                )
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type); 
            }
            else
            {
               //Keresési feltételek elmentése kiiratható formában
               searchObject.ReadableWhere = Search.GetReadableWhere(Form);
               Search.SetSearchObject(Page, searchObject);  
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }
  

    private EREC_UgyUgyiratokSearch GetDefaultSearchObject()
    {
        return new EREC_UgyUgyiratokSearch(true);
    }
}

