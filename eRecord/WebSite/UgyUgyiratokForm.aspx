<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokForm.aspx.cs" Inherits="UgyUgyiratokForm" Title="�gyiratok karbantart�sa" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/UgyUgyiratFormTab.ascx" TagName="UgyUgyiratFormTab"
    TagPrefix="tp1" %>
<%@ Register Src="eRecordComponent/MellekletekTab.ascx" TagName="MellekletekTab"
    TagPrefix="tp2" %>
<%@ Register Src="eRecordComponent/CsatolmanyokTab.ascx" TagName="CsatolmanyokTab"
    TagPrefix="tp3" %>
<%@ Register Src="eRecordComponent/TargyszavakTab.ascx" TagName="TargyszavakTab"
    TagPrefix="tp4" %>
<%@ Register Src="eRecordComponent/FeljegyzesekTab.ascx" TagName="FeljegyzesekTab"
    TagPrefix="tp5" %>
<%@ Register Src="eRecordComponent/CsatolasokTab.ascx" TagName="CsatolasokTab" TagPrefix="tp6" %>
<%@ Register Src="eRecordComponent/JogosultakTab.ascx" TagName="JogosultakTab" TagPrefix="tp7" %>
<%@ Register Src="eRecordComponent/TortenetTab.ascx" TagName="TortenetTab" TagPrefix="tp8" %>
<%@ Register Src="eRecordComponent/FeladatokTab.ascx" TagName="FeladatokTab" TagPrefix="tp9" %>
<%-- 
<%@ Register Src="eRecordComponent/OnkormanyzatTab.ascx" TagName="OnkormanyzatTab"
    TagPrefix="tp10" %> --%>
<%@ Register Src="eRecordComponent/UgyiratTerkepTab.ascx" TagName="UgyiratTerkepTab"
    TagPrefix="tp11" %>
<%@ Register Src="eRecordComponent/KikeroTab.ascx" TagName="KikeroTab" TagPrefix="tp12" %>

<%@ Register Src="~/eRecordComponent/UgyUgyiratJellemzokFormTab.ascx" TagName="UgyUgyiratJellemzokFormTab" TagPrefix="tp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        body, html {
            /*overflow: auto;*/
            /*overflow-y: scroll;*/
            overflow-x: hidden;
        }
    </style>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyiratokFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/json2.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/libs/jquery/jquery.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/esign_v2.js" />
            <asp:ScriptReference Path="~/JavaScripts/Microsec.js" />
        </Scripts>
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/MicroSignerCallbackService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:Panel ID="UgyUgyiratFormPanel" runat="server">
        <ajaxToolkit:TabContainer ID="UgyUgyiratTabContainer" runat="server" Width="100%"
            OnActiveTabChanged="UgyUgyiratokTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged"
            ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="TabUgyiratPanel" runat="server" TabIndex="0">
                <HeaderTemplate>
                    <asp:Label ID="TabUgyPanelHeader" runat="server" Text="�gy" />
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- �gy --%>
                    <tp1:UgyUgyiratFormTab ID="UgyUgyiratFormTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabUgyiratJellemzoPanel" runat="server" TabIndex="1">
                <HeaderTemplate>
                    <asp:Label ID="Label2" runat="server" Text="�gyirat jellemz�k" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp:UgyUgyiratJellemzokFormTab ID="UgyUgyiratJellemzokFormTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabUgyiratTerkepPanel" runat="server" TabIndex="2">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="�gyiratt�rk�p" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp11:UgyiratTerkepTab ID="UgyiratTerkepTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>


            <%-- <ajaxToolkit:TabPanel ID="TabMellekletekPanel" runat="server" TabIndex="1">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Mell�kletek" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp2:MellekletekTab ID="MellekletekTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>--%>
            <ajaxToolkit:TabPanel ID="TabCsatolmanyokPanel" runat="server" TabIndex="2">
                <HeaderTemplate>
                    <asp:Label ID="LabelCsatolmanyokHeader" runat="server" Text="Csatolm�nyok" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp3:CsatolmanyokTab ID="CsatolmanyokTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabTargyszavakPanel" runat="server" TabIndex="3">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="LabelUpdatePanelTargyszavak" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label3" runat="server" Text="T�rgyszavak" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- T�rgyszavak --%>
                    <tp4:TargyszavakTab ID="TargyszavakTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabCsatolasPanel" runat="server" TabIndex="5">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="LabelUpdatePanelKapcsolatok" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Kapcsolatok" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- Kapcsolatok --%>
                    <tp6:CsatolasokTab ID="CsatolasokTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabJogosultakPanel" runat="server" TabIndex="6">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label6" runat="server" Text="Jogosultak" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <tp7:JogosultakTab ID="JogosultakTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabTortenetPanel" runat="server" TabIndex="7">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="LabelUpdatePanelTortenet" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label7" runat="server" Text="T�rt�net" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- T�rt�net --%>
                    <tp8:TortenetTab ID="TortenetTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <%-- K�lcs�nz�s tab --%>
            <ajaxToolkit:TabPanel ID="TabKikeroPanel" runat="server" TabIndex="8">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="LabelUpdatePanelKolcsonzes" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label9" runat="server" Text="K�lcs�nz�s" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <tp12:KikeroTab ID="KikeroTab" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="TabFeladatokPanel" runat="server" TabIndex="9">
                <HeaderTemplate>
                    <asp:UpdatePanel ID="updatePanelFeladatokHeader" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelFeladatok" runat="server" Text="Feladatok/Kezel�si feljegyz�sek" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- Feladatok --%>
                    <tp9:FeladatokTab ID="FeladatokTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--<ajaxToolkit:TabPanel ID="TabOnkormanyzatPanel" runat="server" TabIndex="10">
                <HeaderTemplate>
                    <asp:Label ID="Label10" runat="server" Text="Hat�s�gi adatok" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp10:OnkormanyzatTab ID="OnkormanyzatTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel> --%>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
