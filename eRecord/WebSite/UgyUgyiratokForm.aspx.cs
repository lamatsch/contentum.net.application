using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;

public partial class UgyUgyiratokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string StartUp = String.Empty;

    private bool UgyiratTerkepMode = false;

    private EREC_UgyUgyiratok obj_Ugyirat = null;

    Contentum.eUtility.PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, UgyUgyiratTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(UgyUgyiratTabContainer);

        Command = Request.QueryString.Get(CommandName.Command);
        StartUp = Request.QueryString.Get(Constants.Startup.StartupName);

        string mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (mode == Constants.UgyiratTerkep)
        {
            UgyiratTerkepMode = true;
        }

        // FormHeader be�ll�t�sa az UgyUgyiratFormTab1 -nak:
        UgyUgyiratFormTab1.FormHeader = FormHeader1;

        string SubCommand = Request.QueryString.Get("SubCommand");
        if (Command != CommandName.View || SubCommand == "IrattarbolModositas")
        {
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        }

        // New tiltva, csak iktat�ssal j�het l�tre �j �gyirat, ez meg nem az...
        if (Command == CommandName.New)
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        //jogosults�g ellen�rz�s
        if (Command == CommandName.DesignView)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
        }
        else
        {
            // ha nincs Modify joga, de van View joga, akkor egyes esetekben automatikus �tir�ny�t�s
            bool hasModifyRightAndIsModifyCommand = false;
            bool hasViewRightAndRedirectRequired = false;
            if (Command == CommandName.Modify)
            {
                switch (StartUp)
                {
                    case Constants.Startup.FromAtmenetiIrattar:
                        hasModifyRightAndIsModifyCommand = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + Command);
                        if (!hasModifyRightAndIsModifyCommand)
                        {
                            hasViewRightAndRedirectRequired = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.View);
                        }
                        break;
                    case Constants.Startup.FromKozpontiIrattar:
                        hasModifyRightAndIsModifyCommand = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + Command);
                        if (!hasModifyRightAndIsModifyCommand)
                        {
                            hasViewRightAndRedirectRequired = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.View);
                        }
                        break;
                    case Constants.Startup.FromJegyzekTetel:
                        hasModifyRightAndIsModifyCommand = FunctionRights.GetFunkcioJog(Page, "JegyzekTetelView");
                        break;
                    case Constants.Startup.FromFelulvizsgalat:
                        hasModifyRightAndIsModifyCommand = FunctionRights.GetFunkcioJog(Page, "FelulvizsgalatList");
                        break;
                    default:
                        hasModifyRightAndIsModifyCommand = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + Command);
                        if (!hasModifyRightAndIsModifyCommand)
                        {
                            hasViewRightAndRedirectRequired = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
                        }
                        break;
                }
            }

            if (hasViewRightAndRedirectRequired)
            {
                // �tir�ny�t�s View m�dba
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
            }
            else if (!hasModifyRightAndIsModifyCommand)
            {
                // Modify Command �s jog eset�n m�r nem sz�ks�ges m�g egyszer vizsg�lni
                switch (StartUp)
                {
                    case Constants.Startup.FromAtmenetiIrattar:
                        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtmenetiIrattar" + Command);
                        break;
                    case Constants.Startup.FromKozpontiIrattar:
                        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattar" + Command);
                        break;
                    case Constants.Startup.FromJegyzekTetel:
                        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekTetelView");
                        break;
                    case Constants.Startup.FromFelulvizsgalat:
                        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelulvizsgalatList");
                        break;
                    default:
                        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Ugyirat" + Command);
                        break;
                }
            }
        }

        SetEnabledAllTabsByFunctionRights();


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            //EREC_UgyUgyiratok erec_UgyUgyirat = null;

            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                UgyUgyiratFormPanel.Visible = false;
            }
            else
            {
                // Rekord lek�r�se jogosults�gellen�rz�ssel egy�tt

                if (!IsPostBack)
                {
                    obj_Ugyirat = CheckRights(id);
                }

                #region Felesleges tabf�lek elt�ntet�se

                if (UgyiratTerkepMode == true)
                {
                    // Csak az �gyiratt�rk�p-panelt jelen�tj�k meg?
                    SetAllTabsInvisible();

                    UgyiratTerkepTab1.Visible = true;
                    TabUgyiratTerkepPanel.Enabled = true;
                }
               
                // K�lcs�nz�s TAB nem l�tszik, ha m�g nem volt lez�rva �s nincs tartalma a k�lcs�nz�s list�nak
                if (obj_Ugyirat != null && string.IsNullOrEmpty(obj_Ugyirat.LezarasDat))
                {
                    if (!HasKikeroTabElements())
                    {
                        KikeroTab.Visible = false;
                        TabKikeroPanel.Enabled = false;
                    }                   
                }

                bool statisztika = Rendszerparameterek.GetBoolean(Page, "UGYTIPUS_STATISZTIKA", false);

                if (!statisztika)
                {
                    UgyUgyiratJellemzokFormTab1.Visible = false;
                    TabUgyiratJellemzoPanel.Enabled = false;
                }

                #endregion

            }
        }

        UgyUgyiratFormTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        UgyiratTerkepTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);


        UgyUgyiratFormTab1.Active = true;
        UgyUgyiratFormTab1.ParentForm = Constants.ParentForms.Ugyirat;

        UgyiratTerkepTab1.Active = false;
        UgyiratTerkepTab1.ParentForm = Constants.ParentForms.Ugyirat;

        //MellekletekTab1.Active = false;
        //MellekletekTab1.ParentForm = Constants.ParentForms.Ugyirat;

        CsatolmanyokTab1.Active = true;
        CsatolmanyokTab1.ParentForm = Constants.ParentForms.Ugyirat;

        TargyszavakTab1.Active = false;
        TargyszavakTab1.ParentForm = Constants.ParentForms.Ugyirat;
        TargyszavakTab1.TabHeader = Label3;

        CsatolasokTab1.Active = false;
        CsatolasokTab1.ParentForm = Constants.ParentForms.Ugyirat;
        CsatolasokTab1.TabHeader = Label5;

        JogosultakTab1.Active = false;
        JogosultakTab1.ParentForm = Constants.ParentForms.Ugyirat;
        JogosultakTab1.TabHeader = Label6;

        TortenetTab1.Active = false;
        TortenetTab1.ParentForm = Constants.ParentForms.Ugyirat;
        TortenetTab1.TabHeader = Label7;

        FeladatokTab1.Active = false;
        FeladatokTab1.ParentForm = Constants.ParentForms.Ugyirat;
        FeladatokTab1.TabHeader = LabelFeladatok;
        //OnkormanyzatTab1.Active = false;
        //OnkormanyzatTab1.ParentForm = Constants.ParentForms.Ugyirat;

        UgyUgyiratJellemzokFormTab1.Active = false;
        UgyUgyiratJellemzokFormTab1.ParentForm = Constants.ParentForms.Ugyirat;

        if (UgyiratTerkepMode == true)
        {
            UgyUgyiratFormTab1.Active = false;

            UgyiratTerkepTab1.Active = true;
        }
    }

    private bool HasKikeroTabElements()
    {
        Contentum.eQuery.BusinessDocuments.EREC_IrattariKikeroSearch search = new Contentum.eQuery.BusinessDocuments.EREC_IrattariKikeroSearch();
        var result = eRecordComponent_KikeroTab.GetKikeroResult(search, Page, Request.QueryString.Get("Id"));
        // BUG_12208
        if (result.IsError)
        {
            string msg = result.ErrorMessage;
            result.ErrorMessage = "Hiba a K�lcs�nz�s panel megjelen�t�sn�l! Hiba oka: " + msg;
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return true;
        }
        if (result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
        {
            return false;
        }
        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            // Default panel beallitasa:
            if (UgyiratTerkepMode == true)
            {
                selectedTab = "UgyiratTerkep";
            }
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in UgyUgyiratTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        UgyUgyiratTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    UgyUgyiratTabContainer.ActiveTab = TabUgyiratPanel;
                }
            }
            else
            {
                UgyUgyiratTabContainer.ActiveTab = TabUgyiratPanel;
            }

            ActiveTabRefresh(UgyUgyiratTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Ha v�ltozott valamilyen tulajdons�ga az �gyiratnak, jogosults�gellen�rz�s
    /// </summary>    
    void CallBack_OnChangedObjectProperties(object sender, EventArgs e)
    {
        string id = Request.QueryString.Get(QueryStringVars.Id);
        CheckRights(id);
    }


    private EREC_UgyUgyiratok CheckRights(string id)
    {
        EREC_UgyUgyiratok erec_UgyUgyirat = null;

        char jogszint = Command == CommandName.View ? 'O' : 'I';
        Contentum.eRecord.Service.EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
        ep.Record_Id = id;
        Result result = service.GetWithRightCheck(ep, jogszint);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            UgyUgyiratFormPanel.Visible = false;
            return null;
        }
        else
        {
            erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;
        }


        #region �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
        if (Command == CommandName.Modify || Command == CommandName.View)
        {
            bool bUgyiratModosithato = false;
            String linkURL = String.Empty;

            #region �gyirat m�dos�that�-e
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            ErrorDetails errorDetail;
            bUgyiratModosithato = Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail);
            #endregion �gyirat m�dos�that�-e

            // M�dos�that�s�g ellen�rz�se
            if (Command == CommandName.Modify)
            {
                if (bUgyiratModosithato == false)
                {
                    string qs = QueryStringVars.Id + "=" + id + "&"
                        + Contentum.eUtility.Utils.GetViewRedirectQs(Page, new string[] { QueryStringVars.Id });

                    // Nem m�dos�that� a rekord, �tir�ny�t�s a megtekint�s oldalra
                    //string startup = Request.QueryString.Get(Constants.Startup.StartupName);
                    //if (!String.IsNullOrEmpty(startup) && (startup == Constants.Startup.FromAtmenetiIrattar || startup == Constants.Startup.FromKozpontiIrattar))
                    if (!String.IsNullOrEmpty(StartUp) && (StartUp == Constants.Startup.FromAtmenetiIrattar || StartUp == Constants.Startup.FromKozpontiIrattar))

                    {
                        string modositas = string.Empty;
                        if (Ugyiratok.IrattarbolModosithato(ugyiratStatusz, execParam))
                        {
                            modositas += "&SubCommand=IrattarbolModositas";
                        }
                        if (!IsPostBack)
                        {
                            Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs + modositas);
                        }
                        else
                        {
                            UI.popupRedirect(Page, "UgyUgyIratokForm.aspx?" + qs + modositas);
                        }
                    }
                    else
                    {
                        if (!IsPostBack)
                        {
                            Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                        }
                        else
                        {
                            UI.popupRedirect(Page, "UgyUgyIratokForm.aspx?" + qs);
                        }
                    }
                }
            }
            else if (Command == CommandName.View)
            {
                if (bUgyiratModosithato == true)
                {
                    String funkcioJog = String.Empty;
                    //jogosults�g ellen�rz�s
                    switch (StartUp)
                    {
                        case Constants.Startup.FromAtmenetiIrattar:
                            funkcioJog = "AtmenetiIrattar" + CommandName.Modify;
                            break;
                        case Constants.Startup.FromKozpontiIrattar:
                            funkcioJog = "KozpontiIrattar" + CommandName.Modify;
                            break;
                        case Constants.Startup.FromJegyzekTetel:
                            funkcioJog = "JegyzekTetel" + CommandName.Modify;
                            break;
                        //case Constants.Startup.FromFelulvizsgalat:
                        //    funkcioJog = "FelulvizsgalatList";
                        //    break;
                        default:
                            funkcioJog = "Ugyirat" + CommandName.Modify;
                            break;
                    }

                    if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
                    {
                        // Original QueryString without Command and Id and SelectedTab
                        string qsOriginalParts = QueryStringVars.Id + "=" + id + "&"
                            + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                        linkURL = "UgyUgyIratokForm.aspx?"
                                                + QueryStringVars.Command + "=" + CommandName.Modify
                                                + "&" + qsOriginalParts
                                                + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + UgyUgyiratTabContainer.ClientID + "') + '";
                    }

                }
                else
                {
                    if (!String.IsNullOrEmpty(StartUp) && (StartUp == Constants.Startup.FromAtmenetiIrattar || StartUp == Constants.Startup.FromKozpontiIrattar))
                    {
                        String SubCommand = Request.QueryString.Get("SubCommand");
                        if (SubCommand != "IrattarbolModositas" && Ugyiratok.IrattarbolModosithato(ugyiratStatusz, execParam))
                        {
                            // Original QueryString without Command and Id and SelectedTab
                            string qsOriginalParts = QueryStringVars.Id + "=" + id + "&"
                                + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                            linkURL = "UgyUgyIratokForm.aspx?"
                                                    + QueryStringVars.Command + "=" + CommandName.View
                                                    + "&" + qsOriginalParts + "&SubCommand=IrattarbolModositas"
                                                    + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + UgyUgyiratTabContainer.ClientID + "') + '";
                        }
                    }
                }
            }

            // ModifyLink be�ll�t�sa
            JavaScripts.RegisterSetLinkOnForm(Page, (String.IsNullOrEmpty(linkURL) ? false : true), linkURL, FormHeader1.ModifyLink);
        }
        #endregion �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa

        return erec_UgyUgyirat;
    }



    #region Forms Tab

    protected void UgyUgyiratokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        UgyUgyiratFormTab1.Active = false;
        //MellekletekTab1.Active = false;
        CsatolmanyokTab1.Active = true;
        UgyiratTerkepTab1.Active = false;
        TargyszavakTab1.Active = false;
        CsatolasokTab1.Active = false;
        JogosultakTab1.Active = false;
        TortenetTab1.Active = false;
        KikeroTab.Active = false;
        FeladatokTab1.Active = false;
        //OnkormanyzatTab1.Active = false;

        if (UgyiratTerkepMode == true)
        {
            // Csak az �gyiratt�rk�p l�tsz�dik:
            UgyiratTerkepTab1.Active = true;
            UgyiratTerkepTab1.ReLoadTab();
            return;
        }
        else
        {

            if (UgyUgyiratTabContainer.ActiveTab.Equals(TabUgyiratPanel))
            {
                UgyUgyiratFormTab1.Active = true;

                // a m�r esetleg lek�rt �gyirat objektum �tad�sa
                UgyUgyiratFormTab1.obj_ugyirat = this.obj_Ugyirat;

                UgyUgyiratFormTab1.ReLoadTab();
            }
            //else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabMellekletekPanel))
            //{
            //    MellekletekTab1.Active = true;
            //    MellekletekTab1.ReLoadTab();
            //}
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabCsatolmanyokPanel))
            {
                CsatolmanyokTab1.Active = true;
                CsatolmanyokTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabUgyiratTerkepPanel))
            {
                UgyiratTerkepTab1.Active = true;
                UgyiratTerkepTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabTargyszavakPanel))
            {
                TargyszavakTab1.Active = true;
                TargyszavakTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabCsatolasPanel))
            {
                CsatolasokTab1.Active = true;
                CsatolasokTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabJogosultakPanel))
            {
                JogosultakTab1.Active = true;
                JogosultakTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabTortenetPanel))
            {
                TortenetTab1.Active = true;
                TortenetTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabKikeroPanel))
            {
                KikeroTab.Active = true;
                KikeroTab.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabFeladatokPanel))
            {
                FeladatokTab1.Active = true;
                FeladatokTab1.ReLoadTab();
            }
            else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabUgyiratJellemzoPanel))
            {
                UgyUgyiratJellemzokFormTab1.Active = true;
                UgyUgyiratJellemzokFormTab1.ReLoadTab();
            }
            //else if (UgyUgyiratTabContainer.ActiveTab.Equals(TabOnkormanyzatPanel))
            //{
            //    OnkormanyzatTab1.Active = true;
            //    OnkormanyzatTab1.ReLoadTab();
            //}
        }
    }

    #endregion


    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    //// (Itt nem kell)
    //private void SetEnabledAllTabs(Boolean value)
    //{

    //}

    private void SetAllTabsInvisible()
    {
        UgyUgyiratFormTab1.Visible = false;
        TabUgyiratPanel.Enabled = false;

        UgyiratTerkepTab1.Visible = false;
        TabUgyiratTerkepPanel.Enabled = false;

        TargyszavakTab1.Visible = false;
        TabTargyszavakPanel.Enabled = false;

        CsatolasokTab1.Visible = false;
        TabCsatolasPanel.Enabled = false;

        JogosultakTab1.Visible = false;
        TabJogosultakPanel.Enabled = false;

        TortenetTab1.Visible = false;
        TabTortenetPanel.Enabled = false;

        KikeroTab.Visible = false;
        TabKikeroPanel.Enabled = false;

        FeladatokTab1.Visible = false;
        TabFeladatokPanel.Enabled = false;

        UgyUgyiratJellemzokFormTab1.Visible = false;
        TabUgyiratJellemzoPanel.Enabled = false;

        //OnkormanyzatTab1.Visible = false;
        //TabOnkormanyzatPanel.Enabled = false;
    }


    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni, azt majd ott helyben
        TabUgyiratPanel.Enabled = true;

        // TODO: Funkci�jogosults�gok!!
        //TabMellekletekPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratMelleklet" + CommandName.List);
        //TabCsatolmanyokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolmany" + CommandName.List);
        TabUgyiratTerkepPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);

        TabTargyszavakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratTargyszo" + CommandName.List);
        TabTargyszavakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratTargyszo" + CommandName.List);
        TabCsatolasPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolas" + CommandName.List);
        //TabJogosultakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratJogosult" + CommandName.List);
        TabTortenetPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratTortenet" + CommandName.List);
        TabFeladatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratFeladat" + CommandName.List);

        TabUgyiratJellemzoPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        //TabOnkormanyzatPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratOnkormanyzatiAdat" + CommandName.List);
        //// TODO: funkci�jog
        //TabOnkormanyzatPanel.Enabled = true;
    }

    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            SetAllTabsInvisible();
            TabUgyiratPanel.Visible = TabUgyiratPanel.Enabled = true;
            pageView.CompSelector.Add_ComponentOnClick(TabUgyiratPanel);
            pageView.CompSelector.Add_ComponentOnClick(UgyUgyiratFormTab1);
            pageView.CompSelector.Add_ComponentOnClick(TabUgyiratTerkepPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTargyszavakPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolasPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabJogosultakPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTortenetPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabKikeroPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabFeladatokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabUgyiratJellemzoPanel);
            FormFooter1.SaveEnabled = false;
        }
    }

}
