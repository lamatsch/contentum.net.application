﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class UgyUgyiratokIrattarbolKiadottListaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        IraIktatoKonyvekDropDownList1.FillAndSetEmptyValue(true, false, Constants.IktatoErkezteto.Iktato, EErrorPanel1);
        CalendarControl1.DatumKezd = System.DateTime.Today.ToString();
        CalendarControl1.DatumVege = System.DateTime.Today.AddDays(3).ToString();
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //LZS - BUG_9177
            string dateFrom = string.Empty;
            string dateTo = string.Empty;
            string TypeText = string.Empty;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            if (AMaiNaponMiVanKolcsonozve.Checked)
            {
                if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = IraIktatoKonyvekDropDownList1.SelectedValue;
                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
                }

                //LZS - BUG_9177
                dateFrom = DateTime.Now.ToString().Remove(10).Replace(".", "-") + " 00:00:00";
                dateTo = DateTime.Now.ToString().Remove(10).Replace(".", "-") + "  23:59:59";
                TypeText = AMaiNaponMiVanKolcsonozve.Text;

                erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.KikerKezd IS NOT NULL and EREC_IrattariKikero.VisszaadasDatuma IS NULL and EREC_IrattariKikero.KiadasDatuma BETWEEN '" + DateTime.Now.ToString().Remove(10).Replace(".", "-") + " 00:00:00' and '" + DateTime.Now.ToString().Remove(10).Replace(".", "-") + " 23:59:59' ";
            }

            if (AdottIdoszakbanMiJarLe.Checked)
            {
                //LZS - BUG_9177
                dateFrom = CalendarControl1.DatumKezd.ToString().Remove(10).Replace(".", "-");
                dateTo = CalendarControl1.DatumVege.ToString().Remove(10).Replace(".", "-");
                TypeText = AdottIdoszakbanMiJarLe.Text;

                erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.KikerKezd IS NOT NULL and EREC_IrattariKikero.KikerVege BETWEEN '" + CalendarControl1.DatumKezd.ToString().Remove(10).Replace(".", "-") + "' and '" + CalendarControl1.DatumVege.ToString().Remove(10).Replace(".", "-") + "' and EREC_IrattariKikero.Allapot = '04'";
            }

            if (AzAdottNaponKiMitKolcsonzottKi.Checked)
            {
                if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = IraIktatoKonyvekDropDownList1.SelectedValue;
                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
                }

                dateFrom = CalendarControl1.DatumKezd.ToString().Remove(10).Replace(".", "-");
                dateTo = CalendarControl1.DatumVege.ToString().Remove(10).Replace(".", "-");
                TypeText = AzAdottNaponKiMitKolcsonzottKi.Text;

                erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.KikerKezd IS NOT NULL and EREC_IrattariKikero.KiadasDatuma BETWEEN '" + CalendarControl1.DatumKezd.ToString().Remove(10).Replace(".", "-") + "' and '" + CalendarControl1.DatumVege.ToString().Remove(10).Replace(".", "-") + "'";
            }

            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            //string orderby = erec_UgyUgyiratokSearch.OrderBy;
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            Query query = new Query();
            query.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            Session["Where"] = query.Where + erec_UgyUgyiratokSearch.WhereByManual;

            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "DateFrom=" + dateFrom + '&' + "DateTo=" + dateTo + '&' + "Iktatokonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue + '&' + "TypeText=" + TypeText;

            //Meghívjuk az UgyUgyiratokIrattarbolKiadottListaPrintFormSSRS.aspx nyomtatóoldalt, .
            string js = "javascript:window.open('UgyUgyiratokIrattarbolKiadottListaPrintFormSSRS.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyUgyiratokIrattarbolKiadottListaPrintFormSSRS", js, true);
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected void AMaiNaponMiVanKolcsonozve_CheckedChanged(object sender, EventArgs e)
    {
        if (AMaiNaponMiVanKolcsonozve.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = false;
        }

        if (AdottIdoszakbanMiJarLe.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = false;
            CalendarControl1.Enabled = true;
            Label5.Text = "Lejárat dátuma:";
        }

        if (AzAdottNaponKiMitKolcsonzottKi.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = true;
            Label5.Text = "Kiadás dátuma:";
        }
    }

    protected void AdottIdoszakbanMiJarLe_CheckedChanged(object sender, EventArgs e)
    {
        if (AMaiNaponMiVanKolcsonozve.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = false;
        }

        if (AdottIdoszakbanMiJarLe.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = false;
            CalendarControl1.Enabled = true;
            Label5.Text = "Lejárat dátuma:";
        }

        if (AzAdottNaponKiMitKolcsonzottKi.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = true;
            Label5.Text = "Kiadás dátuma:";
        }
    }

    protected void AzAdottNaponKiMitKolcsonzottKi_CheckedChanged(object sender, EventArgs e)
    {
        if (AMaiNaponMiVanKolcsonozve.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = false;
        }

        if (AdottIdoszakbanMiJarLe.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = false;
            CalendarControl1.Enabled = true;
            Label5.Text = "Lejárat dátuma:";
        }

        if (AzAdottNaponKiMitKolcsonzottKi.Checked)
        {
            IraIktatoKonyvekDropDownList1.Enabled = true;
            CalendarControl1.Enabled = true;
            Label5.Text = "Kiadás dátuma:";
        }
    }
}
