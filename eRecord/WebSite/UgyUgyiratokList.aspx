<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokList.aspx.cs" Inherits="UgyUgyiratokList" Title="&uuml;gyiratok list&aacute;ja" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="eRecordComponent/UgyiratokList.ascx" TagName="UgyiratokList" TagPrefix="ul" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <ul:UgyiratokList ID="UgyiratokList" runat="server" />
   
</asp:Content>
