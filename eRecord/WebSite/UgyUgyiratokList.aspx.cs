using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Reflection;
using Contentum.eQuery;
using System.Text;

public partial class UgyUgyiratokList : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
    }
    
    protected void Page_Init(object sender, EventArgs e)
    {
        UgyiratokList.ScriptManager = ScriptManager1;
        UgyiratokList.ListHeader = ListHeader1;

        UgyiratokList.InitPage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UgyiratokList.LoadPage();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        UgyiratokList.PreRenderPage();
    }

    
}



