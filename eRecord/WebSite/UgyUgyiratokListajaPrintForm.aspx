<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokListajaPrintForm.aspx.cs" Inherits="UgyUgyiratokListajaPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyUgyiratokListFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelLezarasIdopontja" runat="server" Text="�gyirat lez�r�si id�pontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                       </tr>
                        <tr class="urlapSor">
                        <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelCsakLezartak" runat="server" Text="Csak lez�rtak"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:CheckBox ID="csaklezartak_check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                        <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
