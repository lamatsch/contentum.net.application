using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class UgyUgyiratokListajaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_UgyUgyiratokSearch.LezarasDat);
        if (csaklezartak_check.Checked)
        {
            erec_UgyUgyiratokSearch.Allapot.Value = "09";
            erec_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.equals;
        }
        erec_UgyUgyiratokSearch.OrderBy = "EREC_IraIktatokonyvek.Ev, EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratok.Foszam ";

        return erec_UgyUgyiratokSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Megj";
            column.AutoIncrement = false;
            column.Caption = "Megj";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "count";
            column.AutoIncrement = false;
            column.Caption = "count";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Csop_Nev";
            column.AutoIncrement = false;
            column.Caption = "Csop_Nev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            Contentum.eAdmin.Service.KRT_CsoportokService csop_service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            execParam.Record_Id = execParam.FelhasznaloSzervezet_Id;
            Result csop_result = csop_service.Get(execParam);

            KRT_Csoportok csop = (KRT_Csoportok)csop_result.Record;

            string csoport_nev = "";

            if (!(csop == null))
            {
                csoport_nev = csop.Nev;
            }

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            row["Megj"] = Megjegyzes_TextBox.Text;
            row["count"] = result.Ds.Tables[0].Rows.Count.ToString();
            row["Csop_Nev"] = csoport_nev;
            table.Rows.Add(row);

            //string xml = result.Ds.GetXml();
            //string xsd = result.Ds.GetXmlSchema();
            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            WebRequest wr = WebRequest.Create(SP_TM_site_url + "Ugyiratok listaja.xml");
            wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
            StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
            templateText = template.ReadToEnd();
            template.Close();
            //string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns2=\"pl\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>kiss.gergely</o:Author><o:LastAuthor>kiss.gergely</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>0</o:TotalTime><o:Created>2008-01-07T14:07:00Z</o:Created><o:LastSaved>2008-01-07T14:07:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>27</o:Words><o:Characters>189</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>215</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"0086119A\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"lfej\"><w:name w:val=\"header\"/><wx:uiName wx:val=\"�l�fej\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"lfejChar\"/><w:rsid w:val=\"008F17A0\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"lfejChar\"><w:name w:val=\"�l�fej Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"lfej\"/><w:rsid w:val=\"008F17A0\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"llb\"><w:name w:val=\"footer\"/><wx:uiName wx:val=\"�l�l�b\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"llbChar\"/><w:rsid w:val=\"008F17A0\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"llbChar\"><w:name w:val=\"�l�l�b Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"llb\"/><w:rsid w:val=\"008F17A0\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"R�csos t�bl�zat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00421AD4\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"5122\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:footnotePr><w:footnote w:type=\"separator\"><w:p wsp:rsidR=\"00A81B80\" wsp:rsidRDefault=\"00A81B80\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:footnote><w:footnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"00A81B80\" wsp:rsidRDefault=\"00A81B80\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:footnote></w:footnotePr><w:endnotePr><w:endnote w:type=\"separator\"><w:p wsp:rsidR=\"00A81B80\" wsp:rsidRDefault=\"00A81B80\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:endnote><w:endnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"00A81B80\" wsp:rsidRDefault=\"00A81B80\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:endnote></w:endnotePr><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"008F17A0\"/><wsp:rsid wsp:val=\"0003325C\"/><wsp:rsid wsp:val=\"000968CF\"/><wsp:rsid wsp:val=\"00130A88\"/><wsp:rsid wsp:val=\"00244807\"/><wsp:rsid wsp:val=\"00251F7B\"/><wsp:rsid wsp:val=\"002941D9\"/><wsp:rsid wsp:val=\"002B69C4\"/><wsp:rsid wsp:val=\"00421AD4\"/><wsp:rsid wsp:val=\"00473A0E\"/><wsp:rsid wsp:val=\"006355A6\"/><wsp:rsid wsp:val=\"007802AD\"/><wsp:rsid wsp:val=\"00817DF4\"/><wsp:rsid wsp:val=\"00821AA8\"/><wsp:rsid wsp:val=\"0086119A\"/><wsp:rsid wsp:val=\"008960D7\"/><wsp:rsid wsp:val=\"008B5454\"/><wsp:rsid wsp:val=\"008F17A0\"/><wsp:rsid wsp:val=\"009B083D\"/><wsp:rsid wsp:val=\"009F4689\"/><wsp:rsid wsp:val=\"00A54A31\"/><wsp:rsid wsp:val=\"00A81B80\"/><wsp:rsid wsp:val=\"00B444C7\"/><wsp:rsid wsp:val=\"00D65996\"/><wsp:rsid wsp:val=\"00EE2E37\"/><wsp:rsid wsp:val=\"00F71EF2\"/><wsp:rsid wsp:val=\"00F8646C\"/><wsp:rsid wsp:val=\"00FB3FA5\"/></wsp:rsids></w:docPr><w:body><ns2:NewDataSet><w:p wsp:rsidR=\"008F17A0\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr><w:t>�gyiratok list�ja</w:t></w:r></w:p><w:p wsp:rsidR=\"008F17A0\" wsp:rsidRPr=\"008F17A0\" wsp:rsidRDefault=\"008F17A0\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008F17A0\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�sz�t� megjegyz�se a list�hoz:</w:t></w:r><w:r wsp:rsidR=\"00421AD4\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> </w:t></w:r><ParentTable><Megj/></ParentTable></w:p><w:p wsp:rsidR=\"008F17A0\" wsp:rsidRPr=\"008F17A0\" wsp:rsidRDefault=\"008F17A0\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"1535\"/><w:gridCol w:w=\"1535\"/><w:gridCol w:w=\"1535\"/><w:gridCol w:w=\"1535\"/><w:gridCol w:w=\"1536\"/><w:gridCol w:w=\"1536\"/></w:tblGrid><w:tr wsp:rsidR=\"00EE2E37\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidTr=\"00817DF4\"><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>F�sz�m</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>T�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Felel�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1536\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Hat�rid�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1536\" w:type=\"dxa\"/><w:tcBorders><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00EE2E37\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>Lez�r�s</w:t></w:r></w:p></w:tc></w:tr><w:tr wsp:rsidR=\"00EE2E37\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidTr=\"00817DF4\"><w:trPr><w:cantSplit/><w:trHeight w:h-rule=\"exact\" w:val=\"57\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1535\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1536\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1536\" w:type=\"dxa\"/><w:tcBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tcBorders></w:tcPr><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"00EE2E37\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p></w:tc></w:tr><Table ns2:repeater=\"true\"><w:tr wsp:rsidR=\"00EE2E37\" wsp:rsidRPr=\"00EE2E37\" wsp:rsidTr=\"00817DF4\"><Foszam_Merge><w:tc><w:p></w:p></w:tc></Foszam_Merge><Targy><w:tc><w:p></w:p></w:tc></Targy><Felelos_Nev><w:tc><w:p></w:p></w:tc></Felelos_Nev><Allapot_Nev><w:tc><w:p></w:p></w:tc></Allapot_Nev><Hatarido><w:tc><w:p></w:p></w:tc></Hatarido><LezarasDat><w:tc><w:p></w:p></w:tc></LezarasDat></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00421AD4\" wsp:rsidRDefault=\"00421AD4\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00421AD4\" wsp:rsidRDefault=\"00421AD4\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"00421AD4\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>List�zott t�telek sz�ma: </w:t></w:r><count/></w:p><w:p wsp:rsidR=\"00251F7B\" wsp:rsidRDefault=\"00251F7B\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00421AD4\" wsp:rsidRDefault=\"00421AD4\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�sz�tette</w:t></w:r><w:r wsp:rsidR=\"002941D9\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> (felh./szervezet)</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>: </w:t></w:r><FelhNev/><w:r wsp:rsidR=\"002941D9\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t> / </w:t></w:r><Csop_Nev/></w:p><w:p wsp:rsidR=\"008F17A0\" wsp:rsidRPr=\"008F17A0\" wsp:rsidRDefault=\"00421AD4\" wsp:rsidP=\"008F17A0\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr><w:t>K�sz�lt: </w:t></w:r><Date/></w:p></ParentTable></ns2:NewDataSet><w:sectPr wsp:rsidR=\"008F17A0\" wsp:rsidRPr=\"008F17A0\" wsp:rsidSect=\"0086119A\"><w:hdr w:type=\"odd\"><w:p wsp:rsidR=\"008F17A0\" wsp:rsidRDefault=\"008F17A0\" wsp:rsidP=\"008F17A0\"><w:pPr><w:pStyle w:val=\"lfej\"/><w:tabs><w:tab w:val=\"clear\" w:pos=\"4536\"/><w:tab w:val=\"left\" w:pos=\"6804\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>Lapsz�m:</w:t></w:r><w:r><w:tab/></w:r><w:fldSimple w:instr=\" PAGE   \\* MERGEFORMAT \"><w:r wsp:rsidR=\"00817DF4\"><w:rPr><w:noProof/></w:rPr><w:t>1</w:t></w:r></w:fldSimple></w:p></w:hdr><w:pgSz w:w=\"11906\" w:h=\"16838\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

            string filename = "";

            if (pdf)
            {
                filename = "Ugyiratok_listaja_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = "Ugyiratok_listaja_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService _csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch _krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            _krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
            _krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result _csop_result = _csop_service.GetAll(execParam, _krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(_csop_result.ErrorCode))
            {
                foreach (DataRow _row in _csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

            byte[] res = (byte[])result.Record;

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                if (pdf)
                {
                    Response.ContentType = "application/pdf";
                }
                else
                {
                    Response.ContentType = "application/msword";
                }
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else
            {
                if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                {
                    string js = "alert('A dokumentum elk�sz�l�s�r�l e-mail �rtes�t�st fog kapni!');";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
