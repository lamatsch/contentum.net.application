<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" Title="�gyiratok kiv�laszt�sa"
    AutoEventWireup="true" CodeFile="UgyUgyiratokLovList.aspx.cs" Inherits="UgyUgyiratokLovList" EnableEventValidation="false" %>

<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>

<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc5" %>

<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/StandardObjektumTargyszavak.ascx" TagName="StandardObjektumTargyszavak" TagPrefix="sot" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>

<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc4" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="csp" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }

        .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
            width: 205px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,UgyUgyiratokLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                                <td colspan="4">
                                                    <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label2" runat="server" CssClass="mrUrlapCaption" Text="�v:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <uc3:EvIntervallum_SearchFormControl ID="Iktatas_EvIntervallum_SearchFormControl"
                                                        runat="server" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label4" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" EvIntervallum_SearchFormControlId="Iktatas_EvIntervallum_SearchFormControl"
                                                        Mode="Iktatokonyvek" runat="server" IsMultiSearchMode="true" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label3" runat="server" Text="F�sz�m:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <uc5:SzamIntervallum_SearchFormControl ID="FoSzam_SzamIntervallum_SearchFormControl"
                                                        runat="server" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label1" runat="server" Text="T�rgy:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="Targy_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox></td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label5" runat="server" Text="K�ld�/felad� neve:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <uc4:PartnerTextBox ID="Kuld_PartnerId_Bekuldo_PartnerTextBox" runat="server" SearchMode="true" AjaxEnabled="false" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label8" runat="server" Text="K�ld�/felad� c�me:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" SearchMode="true" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="EredetiCimzettLabel" runat="server" CssClass="mrUrlapCaption"
                                                        Text="C�mzett neve:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <csp:CsoportTextBox ID="EredetiCimzett_CsoportTextBox" runat="server"
                                                        SearchMode="true" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label7" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="Kuld_HivatkozasiSzam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelAltalanos" runat="server" CssClass="mrUrlapCaption"
                                                        Text="�gy/irat/k�ld.t�rgy, �gyind�t�, bek�ld� tartalmaz:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="Altalanos_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption"></td>
                                                <td class="mrUrlapMezo"></td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td colspan="4">
                                                    <eUI:eFormPanel ID="StandardUgyiratTargyszavakPanel" runat="server" Visible="true">
                                                        <sot:StandardObjektumTargyszavak ID="sotUgyiratTargyszavak" runat="server" SearchMode="true" />
                                                    </eUI:eFormPanel>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td colspan="4">
                                                    <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                                    <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img src="images/hu/design/spacertrans.gif" alt="" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                        <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" />

                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="�llapot">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%-- skontr�ban eset�n (07) skontr� v�ge megjelen�t�se --%>
                                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%#                                        
                                                                        string.Concat(                                          
                                                                          Eval("Allapot_Nev")
                                                                         , (Eval("Allapot") as string) == "07" ? "<br />(" + Eval("SkontroVege_Rovid") + ")" : ""
                                                                         , (Eval("Allapot") as string) == "60" ? "<a href=\"javascript:void(0)\" onclick=\"__doPostBack("+ this.apostrof + Panel1.ClientID + this.apostrof + "," + this.apostrof + "SelectId_" + Eval("UgyUgyirat_Id_Szulo") as string + this.apostrof + ")\" style=\"text-decoration:underline\"> ("
                                                                                + Eval("Foszam_Merge_Szulo") as string +")<a />" : "") %>' />

                                                                        <asp:Label ID="labelAllapot" runat="server" CssClass="GridViewLovListInvisibleCoulumnStyle"
                                                                            Text='<%# Eval("Allapot") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>--%>
                                                                <asp:BoundField DataField="UtolsoAlszam" HeaderText="Alsz�mok">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <%--BUG_7920--%>
                                                                <asp:BoundField DataField="UgyTipus_Nev" HeaderText="�gyt�pus">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyf�l, �gyind�t� neve">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">&nbsp;
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server"
                                            ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                            onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                            AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
