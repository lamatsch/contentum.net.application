using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery.FullTextSearch;
using System.Collections.Generic;

public partial class UgyUgyiratokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterByObjektumKapcsolat;
    private string ObjektumId;
    private string selectedId = "";
    // az .aspx oldalon kellett
    protected string apostrof = "'";

    private const string funkcio_BejovoIratIktatasCsakAlszamra = "BejovoIratIktatasCsakAlszamra";
    private const string funkcio_BelsoIratIktatasCsakAlszamra = "BelsoIratIktatasCsakAlszamra";

    /// <summary>
    /// Ha true, mindenk�ppen csak azokat az �gyiratokat fogjuk hozni, amibe lehet alsz�mra iktatni
    /// </summary>
    private bool filterForAlszamraIktathatoak = false;

    private string filter = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        // Sz�r�si felt�tel van-e?
        String filterParam = Request.QueryString.Get(QueryStringVars.Filter);
        filter = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterParam == Constants.AlszamraIktathatoak)
        {
            filterForAlszamraIktathatoak = true;

            // m�dos�tjuk a fejl�c c�m�t is:
            LovListHeader1.HeaderTitle = Resources.LovList.UgyUgyiratokLovListHeaderTitle_Filtered_AlszamraIktathatoak;
        }

        //Partner �s cim �sszek�t�se
        Kuld_PartnerId_Bekuldo_PartnerTextBox.CimTextBox = Kuld_CimId_CimekTextBox.TextBox;
        Kuld_PartnerId_Bekuldo_PartnerTextBox.CimHiddenField = Kuld_CimId_CimekTextBox.HiddenField;

        filterByObjektumKapcsolat = Request.QueryString.Get(QueryStringVars.ObjektumKapcsolatTipus);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);

        //Template enged�lyez�s
        LovListHeader1.FormTemplateLoader1_Visibility = true;
        LovListHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.UgyiratokLovList;
        LovListHeader1.TemplateObjectType = typeof(EREC_UgyUgyiratokSearch);

        // BUG 5037
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            IraIktatoKonyvekDropDownList1.ValuesFilledWithId = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Template
        LovListHeader1.ButtonsClick += new CommandEventHandler(LovListHeader1_ButtonsClick);

        //LZS - BUG_3782
        //if (!string.IsNullOrEmpty(Iktatas_EvIntervallum_SearchFormControl.EvTol_TextBox.Text) || !string.IsNullOrEmpty(Iktatas_EvIntervallum_SearchFormControl.EvIg_TextBox.Text))
        //    ButtonSearch_Click(ButtonSearch, null);


        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("UgyUgyiratokForm.aspx", "", GridViewSelectedId.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", ""
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        if (!IsPostBack)
        {
            //IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, Iktatas_EvIntervallum_SearchFormControl.EvTol, Iktatas_EvIntervallum_SearchFormControl.EvIg,
            //    true, false, LovListHeader1.ErrorPanel);
            //FillGridViewSearchResult(false);

            #region standard objektumf�gg� t�rgyszavak
            // �gyirat
            sotUgyiratTargyszavak.FillStandardTargyszavak(null, null, Constants.TableNames.EREC_UgyUgyiratok, LovListHeader1.ErrorPanel);

            if (sotUgyiratTargyszavak.Count > 0)
            {
                StandardUgyiratTargyszavakPanel.Visible = true;
            }
            else
            {
                StandardUgyiratTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumf�gg� t�rgyszavak

            InitComponentsFromQueryString();

        }

    }

    private void InitComponentsFromQueryString()
    {
        string ev = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.Ev);

        if (ev != null)
        {
            Iktatas_EvIntervallum_SearchFormControl.EvTol = ev;
            Iktatas_EvIntervallum_SearchFormControl.EvIg = ev;
        }
        else
        {
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvTol = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvIg = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefault();
        }

        string iktatokonyvValue = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.IktatokonyvValue);
        IktatoKonyv_FillAndSelectValue(null, iktatokonyvValue == null ? "" : iktatokonyvValue, Iktatas_EvIntervallum_SearchFormControl);

        //IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, Iktatas_EvIntervallum_SearchFormControl.EvTol, Iktatas_EvIntervallum_SearchFormControl.EvIg,
        //       true, false, LovListHeader1.ErrorPanel);

        //string iktatokonyvValue = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.IktatokonyvValue);

        //if (!String.IsNullOrEmpty(iktatokonyvValue))
        //{
        //    IraIktatoKonyvekDropDownList1.SetSelectedValue(iktatokonyvValue); ;
        //}



        string foszam = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.Foszam);

        if (!String.IsNullOrEmpty(foszam))
        {
            FoSzam_SzamIntervallum_SearchFormControl.SzamTol = foszam;
            FoSzam_SzamIntervallum_SearchFormControl.SzamIg = foszam;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();

            if (!string.IsNullOrEmpty(eventArgument) && eventArgument.StartsWith("SelectId_"))
            {
                // kinyerj�k az id-t:
                selectedId = eventArgument.Replace("SelectId_", "");
                if (disable_refreshLovList != true)
                {
                    FillGridViewSearchResult(false);
                }
            }
            else
            {
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            FillGridViewSearchResult(true);
                        }
                        break;
                }
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        //if (IsPostBack)
        //{
        //    String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
        //    RefreshOnClientClicks(selectedRowId);
        //}
    }

    //private void RefreshOnClientClicks(string id)
    //{
    //    if (!String.IsNullOrEmpty(id))
    //    {

    //        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
    //            "UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View
    //            + "&" + QueryStringVars.Id + "=" + id
    //        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, null);
    //    }
    //}

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
        disable_refreshLovList = true;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch());
        }
        else
        {
            search = SetSearchObjectFromComponents();

        }

        // Ha r�kattintott egy sz�l� �gyiratra, csak azt hozzuk: (Szereltekn�l lehets�ges)
        if (!String.IsNullOrEmpty(selectedId))
        {
            search = new EREC_UgyUgyiratokSearch(true);
            search.Id.Value = selectedId;
            search.Id.Operator = Query.Operators.equals;
        }

        // ha kell sz�rni az alsz�mra iktathat�kra:
        if (filterForAlszamraIktathatoak == true)
        {
            Ugyiratok.SetSearchObjectTo_AlszamraIktathatok(search, this);
        }
        else
        {
            switch (filter)
            {
                case Constants.FilterType.Ugyiratok.Szereles:
                    search.Manual_Lezartak.Value = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                    search.Manual_Lezartak.Operator = Query.Operators.equals;
                    break;
                case Constants.FilterType.Ugyiratok.SzerelesKikeressel:
                    // egyel�re azokat vessz�k, amelyeket az iktat� form lila keres�je is enged�lyez
                    Ugyiratok.SetSearchObjectTo_AlszamraIktathatok(search, this);
                    break;
                default:
                    break;
            }
        }

        if (!String.IsNullOrEmpty(filterByObjektumKapcsolat) && !String.IsNullOrEmpty(ObjektumId))
        {
            switch (filterByObjektumKapcsolat)
            {
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt:
                    search.Id.Value = ObjektumId;
                    search.Id.Operator = Query.Operators.notequals;
                    Ugyiratok.SetCsatolhatoSearchObject(search);
                    break;
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany:
                    ExecParam xpmIratPeldany = UI.SetExecParamDefault(Page, new ExecParam());
                    IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotById(ObjektumId, xpmIratPeldany, LovListHeader1.ErrorPanel, true);
                    if (!String.IsNullOrEmpty(iratPeldanyStatusz.UgyiratId))
                    {
                        search.Id.Value = iratPeldanyStatusz.UgyiratId;
                        search.Id.Operator = Query.Operators.notequals;
                    }
                    Ugyiratok.SetCsatolhatoSearchObject(search);
                    break;
            }
        }


        search.OrderBy = "EREC_UgyUgyiratok.LetrehozasIdo";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);
        search.TopRow = Rendszerparameterek.Get_LOVLIST_MAX_ROW(Page);

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    private EREC_UgyUgyiratokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)LovListHeader1.TemplateObject;

        if (search == null)
        {
            search = new EREC_UgyUgyiratokSearch(true);
        }

        if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
        {
            IraIktatoKonyvekDropDownList1.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
        }

        Iktatas_EvIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField))
        {
            search.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value =
                Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField;
            search.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Operator =
                Query.Operators.equals;
        }
        else
        {
            if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text))
            {
                search.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = Kuld_PartnerId_Bekuldo_PartnerTextBox.Text;
                search.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text);
            }
        }

        if (!String.IsNullOrEmpty(Kuld_CimId_CimekTextBox.Id_HiddenField))
        {
            search.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Value =
                Kuld_CimId_CimekTextBox.Id_HiddenField;
            search.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Operator =
                Query.Operators.equals;
        }
        else
        {
            if (!String.IsNullOrEmpty(Kuld_CimId_CimekTextBox.Text))
            {
                search.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value = Kuld_CimId_CimekTextBox.Text;
                search.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuld_CimId_CimekTextBox.Text);
            }
        }

        if (!String.IsNullOrEmpty(EredetiCimzett_CsoportTextBox.Id_HiddenField))
        {
            search.Extended_EREC_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value = EredetiCimzett_CsoportTextBox.Id_HiddenField;
            search.Extended_EREC_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Kuld_HivatkozasiSzam_TextBox.Text))
        {
            search.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Value =
                Kuld_HivatkozasiSzam_TextBox.Text;
            search.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Operator =
                Search.GetOperatorByLikeCharater(Kuld_HivatkozasiSzam_TextBox.Text);
        }



        /// Foszam be�ll�t�sa
        if (!FoSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
        {
            FoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Foszam);
        }

        #region FTS

        if (!String.IsNullOrEmpty(Targy_TextBox.Text))
        {
            //search.fts_targy = new FullTextSearchField();
            //search.fts_targy.Filter = Targy_TextBox.Text;

            search.Targy.Value = Targy_TextBox.Text;
            // BUG_3944
            search.Targy.Operator = Query.Operators.contains;
            //search.Targy.Operator = Search.GetOperatorByLikeCharater(Targy_TextBox.Text);

        }

        if (!String.IsNullOrEmpty(Altalanos_TextBox.Text))
        {
            search.fts_altalanos = new FullTextSearchField();
            search.fts_altalanos.Filter = Altalanos_TextBox.Text;
        }


        #endregion

        #region standard objektumf�gg� t�rgyszavak
        // �gyiratt�l �r�k�lt
        try
        {
            FullTextSearchTree FTSTree = sotUgyiratTargyszavak.BuildFTSTreeFromList(LovListHeader1.ErrorPanel);
            if (FTSTree != null)
            {
                search.fts_tree_ugyirat_auto = FTSTree;
                search.ObjektumTargyszavai_ObjIdFilter = FTSTree.TransformToFTSContainsConditions();
            }

        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion standard objektumf�gg� t�rgyszavak

        return search;
    }

    private void IktatoKonyv_FillAndSelectValue(EREC_UgyUgyiratokSearch search, string selectedId, ASP.component_evintervallum_searchformcontrol_ascx component_Evintervallum)
    {

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
        if (isTUK) // BUG 5037
        {
            //T�K eset�n az id lesz az �rt�k mez�nek felt�ltve, mivel itt Id szerinti keres�s lesz
            string id = search != null ? search.Extended_EREC_IraIktatoKonyvekSearch.Id == null ? null : search.Extended_EREC_IraIktatoKonyvekSearch.Id.Value : selectedId;
            IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
            , true, component_Evintervallum.EvTol, component_Evintervallum.EvIg, true, false, id, LovListHeader1.ErrorPanel);
        }
        else {
            IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
            , component_Evintervallum.EvTol, component_Evintervallum.EvIg
            , true, false, search != null ? IktatoKonyvek.GetIktatokonyvValue(search.Extended_EREC_IraIktatoKonyvekSearch) : selectedId, LovListHeader1.ErrorPanel);
        }
    }

    private void LoadComponentsFromSearchObject(EREC_UgyUgyiratokSearch search)
    {
        if (search != null)
        {
            if (search.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                Iktatas_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                IktatoKonyv_FillAndSelectValue(search, null, Iktatas_EvIntervallum_SearchFormControl);
            }

            if (search.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                if (!String.IsNullOrEmpty(search.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value))
                {
                    Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField = search.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value;
                    Kuld_PartnerId_Bekuldo_PartnerTextBox.SetPartnerTextBoxById(LovListHeader1.ErrorPanel);
                }
                else
                {
                    Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField = String.Empty;
                    Kuld_PartnerId_Bekuldo_PartnerTextBox.Text = search.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;
                }


                if (!String.IsNullOrEmpty(search.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Value))
                {
                    Kuld_CimId_CimekTextBox.Id_HiddenField = search.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Value;
                    Kuld_CimId_CimekTextBox.SetCimekTextBoxById(LovListHeader1.ErrorPanel);
                }
                else
                {
                    Kuld_CimId_CimekTextBox.Id_HiddenField = String.Empty;
                    Kuld_CimId_CimekTextBox.Text = search.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value;
                }

                if (!String.IsNullOrEmpty(search.Extended_EREC_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value))
                {
                    EredetiCimzett_CsoportTextBox.Id_HiddenField = search.Extended_EREC_KuldKuldemenyekSearch.Csoport_Id_Cimzett.Value;
                    EredetiCimzett_CsoportTextBox.SetCsoportTextBoxById(LovListHeader1.ErrorPanel);
                }
                else
                {
                    EredetiCimzett_CsoportTextBox.Id_HiddenField = String.Empty;
                }

                if (!String.IsNullOrEmpty(search.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Value))
                {
                    Kuld_HivatkozasiSzam_TextBox.Text = search.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Value;
                }
                else
                {
                    Kuld_HivatkozasiSzam_TextBox.Text = String.Empty;
                }
            }

            FoSzam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(search.Foszam);

            #region FTS

            //if (search.fts_targy != null)
            //{
            //    Targy_TextBox.Text = search.fts_targy.Filter;
            //}
            //else
            //{
            //    Targy_TextBox.Text = String.Empty;
            //}
            Targy_TextBox.Text = search.Targy.Value;

            if (search.fts_altalanos != null)
            {
                Altalanos_TextBox.Text = search.fts_altalanos.Filter;
            }
            else
            {
                Altalanos_TextBox.Text = String.Empty;
            }

            #endregion



            #region standard objektumf�gg� t�rgyszavak

            if (sotUgyiratTargyszavak.Count > 0)
            {
                StandardUgyiratTargyszavakPanel.Visible = true;
                //a postback sor�n visszakapott �rt�kek t�rl�se
                sotUgyiratTargyszavak.ClearTextBoxValues();

                //// vigy�zat, felt�telezz�k, hogy felv�ltva lett hozz�adva t�rgysz� �s �rt�k...!
                //if (search.fts_tree_ugyirat_auto != null)
                //{
                //    List<string> items = search.fts_tree_ugyirat_auto.GetLeafList();
                //    if (items != null)
                //    {
                //        int i = 0;
                //        while (i + 1 < items.Count)
                //        {
                //            string items_targyszo = items[i];
                //            string items_ertek = items[i + 1];
                //            string[] item_elements_targyszo = items_targyszo.Split(FullTextSearchTree.FieldValueSeparator);
                //            string[] item_elements_ertek = items_ertek.Split(FullTextSearchTree.FieldValueSeparator);
                //            // t�rgysz� �rt�k�nek be�r�sa a mez�be
                //            sotUgyiratTargyszavak.TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

                //            i += 2;
                //        }
                //    }
                //}
                sotUgyiratTargyszavak.FillFromFTSTree(search.fts_tree_ugyirat_auto);
            }
            else
            {
                StandardUgyiratTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumf�gg� t�rgyszavak

        }
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    string jsCallbackFunctionName = Request.QueryString.Get(QueryStringVars.ParentWindowCallbackFunction);
                    if (!String.IsNullOrEmpty(jsCallbackFunctionName))
                    {
                        JavaScripts.RegisterParentWindowCallbackFunction(Page, jsCallbackFunctionName);
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                if (filterForAlszamraIktathatoak)
                {
                    // A szerelteket hozzuk, de ki kell �ket sz�rk�teni:

                    List<string> disabledValueList = new List<string>();
                    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
                    // CR#2580 (2010.12.27): Iratt�rba k�ld�ttbe nem szabad iktatni
                    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);

                    // CR#1305: Aki csak alsz�mra iktathat (FPH_TITKARSAG), az el�zm�nyk�nt ne v�laszthasson iratt�rban vagy skontr�ban l�v� �gyiratot
                    if (FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra)
                        || FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra))
                    {
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Skontroban);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                        //disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
                    }
                    #region CR#3245 DEPRICATED   (CR3081 : BOPMH param�terezetten kellene megoldani, hogy ne lehessen kiv�lasztani a migr�ltb�l iratt�rban lev�t el�zm�nyk�nt)
                    //if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
                    //{
                    //    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                    //    //disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
                    //    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa);
                    //    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
                    //    JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                    //        , "labelAllapot", disabledValueList);

                    //}
                    #endregion
                    JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                        , "labelAllapot", disabledValueList);
                }
                else if (filter == Constants.FilterType.Ugyiratok.SzerelesKikeressel)
                {
                    // A szerelteket hozzuk, de ki kell �ket sz�rk�teni:

                    List<string> disabledValueList = new List<string>();
                    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
                    // CR#2580 (2010.12.27): Iratt�rba k�ld�ttbe nem szabad iktatni
                    // egyel�re azokat vessz�k, amelyeket az iktat� form lila keres�je is enged�lyez, ez�rt ezt itt is letiltjuk
                    disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
                    #region CR3081 : BOPMH param�terezetten kellene megoldani, hogy ne lehessen kiv�lasztani a migr�ltb�l iratt�rban lev�t el�zm�nyk�nt
                    if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
                    {
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                        //disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
                        JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                            , "labelAllapot", disabledValueList);

                    }
                    #endregion

                    JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                        , "labelAllapot", disabledValueList);
                }
                else
                {
                    List<string> disabledValueList = new List<string>();
                    #region CR3081 : BOPMH param�terezetten kellene megoldani, hogy ne lehessen kiv�lasztani a migr�ltb�l iratt�rban lev�t el�zm�nyk�nt
                    if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
                    {
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                        //disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa);
                        disabledValueList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);


                    }
                    #endregion
                    JavaScripts.SetLovListGridViewRowScripts_disableIfValueIsInList(row, GridViewSelectedIndex, GridViewSelectedId
                                                , "labelAllapot", disabledValueList);
                    //JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
                }



            }
        }

        base.Render(writer);

    }


    /// <summary>
    /// FormTemplate gombjainak esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void LovListHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject((EREC_UgyUgyiratokSearch)LovListHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            LovListHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            LovListHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }
}
