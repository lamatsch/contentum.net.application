<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokPrintForm.aspx.cs" Inherits="UgyUgyiratokPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyiratokFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:RadioButton id="AtmenetiBiroto" Text="�tmeneti bor�t�" GroupName="sablonok" runat="server"/>
                                <br />
                                <asp:RadioButton id="UgyiratKisero" Text="�gyiratk�s�r�" GroupName="sablonok" runat="server"/>
                                <br />
                                <asp:RadioButton id="EloadoiIv" Text="El�ad�i �v" Checked="true" GroupName="sablonok" runat="server"/>
                                <br />
                                <asp:RadioButton id="PeresEloadoiIv" Text="Peres el�ad�i �v" GroupName="sablonok" runat="server"/>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
