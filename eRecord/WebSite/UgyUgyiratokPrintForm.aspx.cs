using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using System.Collections.Generic;
 
public partial class UgyUgyiratokPrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;

    private int Ev = -1;

    private bool IsAfter2011
    {
        get
        {
            return (Ev == -1 || Ev > 2011);
        }
    }

    private string EloadoIvSablonNev
    {
        get
        {
            if (IsAfter2011)
                return "Eloadoi iv_2012.xml";

            return "Eloadoi iv.xml";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (String.IsNullOrEmpty(ugyiratId))
        {
            // nincs Id megadva:
        }

        string qs = Request.QueryString.Get(QueryStringVars.Ev);
        if (!String.IsNullOrEmpty(qs))
        {
            int i;
            if (Int32.TryParse(qs, out i))
            {
                Ev = i;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;

        //BUG_3606 - LZS -
        PeresEloadoiIv.Visible = FunctionRights.GetFunkcioJog(Page, "PeresEloadoiIvNyomtatas");

        FormFooter1.ImageButton_Save.OnClientClick = null;
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
         
        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_i()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = ugyiratId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_ip()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_uk()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = ugyiratId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = true;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    //private EREC_IraKezFeljegyzesekSearch GetSearchObjectFromComponents_ik()
    //{
    //    EREC_IraKezFeljegyzesekSearch erec_IraKezFeljegyzesekSearch = new EREC_IraKezFeljegyzesekSearch();

    //    erec_IraKezFeljegyzesekSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
    //    erec_IraKezFeljegyzesekSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

    //    return erec_IraKezFeljegyzesekSearch;
    //}

    //private EREC_KuldKezFeljegyzesekSearch GetSearchObjectFromComponents_kk()
    //{
    //    EREC_KuldKezFeljegyzesekSearch erec_KuldKezFeljegyzesekSearch = new EREC_KuldKezFeljegyzesekSearch();

    //    erec_KuldKezFeljegyzesekSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
    //    erec_KuldKezFeljegyzesekSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

    //    return erec_KuldKezFeljegyzesekSearch;
    //}

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_id()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_es()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.ObjTip_Id.Value = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.Obj_Id.Value = ugyiratId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }

    private EREC_UgyiratKapcsolatokSearch GetSearchObjectFromComponents_csat()
    {
        EREC_UgyiratKapcsolatokSearch erec_UgyiratKapcsolatokSearch = new EREC_UgyiratKapcsolatokSearch();

        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Value = "01";
        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Value = ugyiratId;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.equals;

        return erec_UgyiratKapcsolatokSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

            //ügy fajtájának feloldása
            if (!result.IsError)
            {
                Dictionary<string, string> ugyFajtajaDict = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.UGY_FAJTAJA.KodcsoportKod, Page, null);
                foreach (DataRow _row in result.Ds.Tables[0].Rows)
                {
                    // BUG_2433
                    //string ugyFajtaja = _row["UgyFajtaja"].ToString();
                    string ugyFajtaja = _row["Ugy_Fajtaja"].ToString();
                
                    if (!String.IsNullOrEmpty(ugyFajtaja))
                    {
                        if (ugyFajtajaDict.ContainsKey(ugyFajtaja))
                            _row["Ugy_Fajtaja"] = ugyFajtajaDict[ugyFajtaja];
                    }
                }
            }

            Result eloirat_result = service.GetSzereltekFoszam(execParam, erec_UgyUgyiratokSearch);

            string eloirat = "";

            foreach (DataRow _row in eloirat_result.Ds.Tables[0].Rows)
            {
                eloirat = eloirat + _row["Foszam"].ToString() + ", ";
            }

            if (eloirat.Length > 2)
            {
                eloirat = eloirat.Remove(eloirat.Length - 2);
            }

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "EloIrat";
            column.AutoIncrement = false;
            column.Caption = "EloIrat";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            row["EloIrat"] = eloirat;
            table.Rows.Add(row);

            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            
            string filename = "";

            if (AtmenetiBiroto.Checked)
            {

                //LZS - BLG_5887
                //Response.Redirect("UgyUgyiratokPrintFormSSRSAtmenetiBorito.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId);
                string js = "window.open('UgyUgyiratokPrintFormSSRSAtmenetiBorito.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "','popUpWindow','height=300,width=700,left=50,top=50,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes' );";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openUgyUgyiratokPrintFormSSRSAtmenetiBoritoScript", js, true);

                return;
            }
            else if (UgyiratKisero.Checked)
            {
                //LZS - BLG_5887
                //Response.Redirect("UgyUgyiratokPrintFormSSRSUgyiratKisero.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId);
                string js = "window.open('UgyUgyiratokPrintFormSSRSUgyiratKisero.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "','popUpWindow','height=300,width=700,left=50,top=50,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes' );";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openUgyUgyiratokPrintFormSSRSUgyiratKiseroScript", js, true);

                return;
            }
            else if (EloadoiIv.Checked)
            {
                //LZS - BLG_5887
                //Response.Redirect("UgyUgyiratokPrintFormSSRSEloadoiIv.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId);
                string js = "window.open('UgyUgyiratokPrintFormSSRSEloadoiIv.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "','popUpWindow','height=300,width=700,left=50,top=50,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes' );";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openUgyUgyiratokPrintFormSSRSEloadoiIvScript", js, true);

                return;
            }
            else if (PeresEloadoiIv.Checked)
            {
                EREC_PldIratPeldanyokService ip_service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents_ip();

                erec_PldIratPeldanyokSearch.WhereByManual = " and EREC_PldIratPeldanyok.Sorszam = '1' ";
                erec_PldIratPeldanyokSearch.OrderBy = "  EREC_IraIratok.Alszam";

                Result ip_result = ip_service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

                table = new DataTable("ip_Table");
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "id";
                column.ReadOnly = true;
                column.Unique = true;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IktatasDatuma";
                column.AutoIncrement = false;
                column.Caption = "IktatasDatuma";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                PrimaryKeyColumns = new DataColumn[1];
                PrimaryKeyColumns[0] = table.Columns["id"];
                table.PrimaryKey = PrimaryKeyColumns;
                result.Ds.Tables.Add(table);

                row = table.NewRow();
                row["id"] = 0;
                for (int i = 0; i < 1; i++)
                {
                    row["IktatasDatuma"] = ip_result.Ds.Tables[0].Rows[i]["IktatasDatuma"].ToString();
                }
                table.Rows.Add(row);

                EREC_UgyiratObjKapcsolatokService kap_service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
                EREC_UgyiratObjKapcsolatokSearch erec_UgyiratObjKapcsolatokSearch = new EREC_UgyiratObjKapcsolatokSearch();

                erec_UgyiratObjKapcsolatokSearch.Obj_Id_Elozmeny.Value = ugyiratId;
                erec_UgyiratObjKapcsolatokSearch.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
                erec_UgyiratObjKapcsolatokSearch.Obj_Type_Kapcsolt.Value = "EREC_UgyUgyiratok";
                erec_UgyiratObjKapcsolatokSearch.Obj_Type_Kapcsolt.Operator = Query.Operators.equals;
                erec_UgyiratObjKapcsolatokSearch.KapcsolatTipus.Value = "01";
                erec_UgyiratObjKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
                erec_UgyiratObjKapcsolatokSearch.WhereByManual = " AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege ";

                Result kap_result = kap_service.GetAllWithExtension(execParam, erec_UgyiratObjKapcsolatokSearch);

                string kap_xml = kap_result.Ds.GetXmlSchema();

                DataTable tempTable = new DataTable();
                tempTable = kap_result.Ds.Tables[0].Copy();
                tempTable.TableName = "kap_Table";
                result.Ds.Tables.Add(tempTable);

                table = new DataTable("IartariTetelszamTable");
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "id";
                column.ReadOnly = true;
                column.Unique = true;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Itsz1";
                column.AutoIncrement = false;
                column.Caption = "Itsz1";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Itsz2";
                column.AutoIncrement = false;
                column.Caption = "Itsz2";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Itsz3";
                column.AutoIncrement = false;
                column.Caption = "Itsz3";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Itsz4";
                column.AutoIncrement = false;
                column.Caption = "Itsz4";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "selejtkod";
                column.AutoIncrement = false;
                column.Caption = "selejtkod";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "megorzesi_ido";
                column.AutoIncrement = false;
                column.Caption = "megorzesi_ido";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "szervezeti_egyseg";
                column.AutoIncrement = false;
                column.Caption = "szervezeti_egyseg";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                PrimaryKeyColumns = new DataColumn[1];
                PrimaryKeyColumns[0] = table.Columns["id"];
                table.PrimaryKey = PrimaryKeyColumns;
                result.Ds.Tables.Add(table);

                string itsz = "";
                string szerv = "";
                string irattari_jel = "";
                string megorzesi_ido = "";
                string megorzesi_ido_regi = "";
                string letrehozas_ido = "";
                string agjel = "";

                foreach (DataRow _row in result.Ds.Tables[0].Rows)
                {
                    itsz = _row["IrattariTetelszam"].ToString();
                    szerv = _row["Iktatokonyv_Nev"].ToString();
                    irattari_jel = _row["IrattariJel"].ToString();
                    megorzesi_ido = _row["MegorzesiIdo1"].ToString();
                    megorzesi_ido_regi = _row["MegorzesiIdo"].ToString();
                    letrehozas_ido = _row["LetrehozasIdo_Rovid"].ToString();
                    agjel = _row["AgazatiJel"].ToString();
                }

                if (szerv.IndexOf("iktatókönyve") >= 0)
                {
                    szerv = szerv.Remove(szerv.IndexOf("iktatókönyve"));
                }

                row = table.NewRow();
                row["id"] = 0;

                if (Convert.ToInt32(letrehozas_ido.Substring(0, 4)) < 2010)
                {
                    if (itsz.Length >= 3)
                    {
                        row["Itsz2"] = itsz.Substring(0, 1);
                        row["Itsz3"] = itsz.Substring(1, 1);
                        row["Itsz4"] = itsz.Substring(2, 1);
                    }
                    if (agjel.Length >= 1)
                    {
                        row["selejtkod"] = agjel.Substring(0, 1);
                    }
                    row["megorzesi_ido"] = megorzesi_ido_regi;
                }
                else
                {
                    if (itsz.Length >= 3)
                    {
                        row["Itsz1"] = "";
                        row["Itsz2"] = itsz.Substring(0, 1);
                        row["Itsz3"] = itsz.Substring(1, 1);
                        row["Itsz4"] = itsz.Substring(2, 1);
                    }
                    if (itsz.Length >= 4)
                    {
                        row["Itsz1"] = itsz.Substring(0, 1);
                        row["Itsz2"] = itsz.Substring(1, 1);
                        row["Itsz3"] = itsz.Substring(2, 1);
                        row["Itsz4"] = itsz.Substring(3, 1);
                    }
                    if (irattari_jel.Equals("L") || irattari_jel.Equals("N"))
                    {
                        row["selejtkod"] = "NS";

                        if (irattari_jel.Equals("N"))
                        {
                            row["megorzesi_ido"] = "-";
                        }
                        else
                        {
                            row["megorzesi_ido"] = megorzesi_ido;
                        }
                    }
                    else
                    {
                        row["megorzesi_ido"] = "";

                        if (irattari_jel.Equals("N"))
                        {
                            row["selejtkod"] = "-";
                        }
                        else
                        {
                            row["selejtkod"] = megorzesi_ido;
                        }
                    }
                }
                row["szervezeti_egyseg"] = szerv;
                table.Rows.Add(row);

                //xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                WebRequest wr = WebRequest.Create(SP_TM_site_url + "Peres Eloadoi Iv.xml");
                wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                templateText = template.ReadToEnd();
                template.Close();

                filename = "Peres_eloadoi_iv_";
            }


            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

            if (pdf)
            {
                filename = filename +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = filename +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(csop_result.ErrorCode))
            {
                foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

            byte[] res = (byte[])result.Record;

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                if (pdf)
                {
                    Response.ContentType = "application/pdf";
                }
                else
                {
                    Response.ContentType = "application/msword";
                }
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else
            {
                if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                {
                    string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
    }
}
