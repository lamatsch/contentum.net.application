﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class UgyUgyiratokFormTabPrintForm : Contentum.eUtility.ReportPageBase
{
    private string ugyiratId = String.Empty;
    private string tipus = String.Empty;
    private string proba = String.Empty;
    private string kornyezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {

        kornyezet = FelhasznaloProfil.OrgKod(Page);
        if (kornyezet.Equals("CSPH")) { kornyezet = "CSEPEL"; }

        ugyiratId = Request.QueryString.Get("UgyiratId");


        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(tipus))
        {
            // nincs Id megadva:
        }

        ;
    }



    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    

    private EREC_IraIratokSearch GetSearchObjectFromComponents_i()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = ugyiratId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_ip()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_uk()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = ugyiratId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = true;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_id()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_es()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.ObjTip_Id.Value = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.Obj_Id.Value = ugyiratId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }

    private EREC_UgyiratKapcsolatokSearch GetSearchObjectFromComponents_csat()
    {
        EREC_UgyiratKapcsolatokSearch erec_UgyiratKapcsolatokSearch = new EREC_UgyiratKapcsolatokSearch();

        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Value = "01";
        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Value = ugyiratId;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.equals;

        return erec_UgyiratKapcsolatokSearch;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        InitReport(ReportViewer1, Rendszerparameterek.GetBoolean(this, Rendszerparameterek.SSRS_AUTO_PDF_RENDER), GetReportFileName("előadói ív"), kornyezet);
    }


    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {



        ReportParameter[] ReportParameters = null;



        if (rpis != null)
        {
            if (rpis.Count > 0)

            {

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);
               


                Result eloirat_result = service.GetSzereltekFoszam(execParam, erec_UgyUgyiratokSearch);



                string eloirat = "";
               

                foreach (DataRow _row in eloirat_result.Ds.Tables[0].Rows)
                {
                    eloirat = eloirat + _row["Foszam"].ToString() + ", ";
                   
                }

                if (eloirat.Length > 2)
                {
                    eloirat = eloirat.Remove(eloirat.Length - 2);
                }

                if (eloirat.Length == 0)
                {
                    eloirat = " ";
                }   

                EREC_HataridosFeladatokService uk_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponents_uk();

                Result uk_result = uk_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "Eloirat":

                            ReportParameters[i].Values.Add(eloirat);
                           // ReportParameters[i].Values.Add(kornyezet);
                            break;  
                        case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;

                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;



                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;
                        case "UgyiratId":
                            // ReportParameters[i].Values.Add(iratId);
                            ReportParameters[i].Values.Add(ugyiratId);
                            break;
                        case "WhereHatFel":
                            ReportParameters[i].Values.Add("( erec_hataridosfeladatok.obj_id = '"+ugyiratId+"' ) ");
                            break;

                        case "WhereEsemenyek":
                            ReportParameters[i].Values.Add("((((KRT_Esemenyek.Funkcio_Id not in ('d5fd9dc2-19f9-4d3d-971f-1053fdf2013d','ae131060-8552-42c7-97a0-59526f88c2f5','ff760ce5-be6e-4c60-b373-75d2817c9998','56cc5b77-bf80-407f-a27a-ddad1f504832','c948a74c-1613-4bf6-9b29-38b7cefbe84b','e2422e74-9465-4910-a696-10f2044f0136','a5bdc3bd-58ec-42c8-9aba-0011ec48d9c0','8d84174e-671c-49f1-a18c-a4db97ad0e16','3d4d0a42-6a71-4810-873b-70d3ff27b403','2b8e71d6-278c-4ab0-b485-623b1c2c585c')) and (KRT_Esemenyek.ErvKezd <= getdate()) and (KRT_Esemenyek.ErvVege >= getdate()))))");
                            break;

                        case "WhereUgyirat":
                            ReportParameters[i].Values.Add("((((EREC_UgyiratObjKapcsolatok.KapcsolatTipus <> '07') and (EREC_UgyiratObjKapcsolatok.ErvKezd <= getdate()) and (EREC_UgyiratObjKapcsolatok.ErvVege >= getdate())) and ((EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt = '" + ugyiratId + "'))))");
                            break;
                    }
                }
            }

        }
        return ReportParameters;
    }
}

