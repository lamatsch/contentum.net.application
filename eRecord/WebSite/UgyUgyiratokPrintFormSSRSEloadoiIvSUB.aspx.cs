﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;
using BarcodeLib;


public partial class UgyUgyiratokFormTabPrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;
    private string tipus = String.Empty;
    private string proba = String.Empty;
    private string kornyezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {


        // ugyiratId = Request.QueryString.Get("UgyiratId");
        kornyezet = FelhasznaloProfil.OrgKod(Page);

        if (kornyezet.Equals("CSPH")) { kornyezet = "CSEPEL"; }

        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(tipus))
        {
            // nincs Id megadva:
        }
    }



    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_i()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = ugyiratId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_ip()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_uk()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = ugyiratId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = true;

        erec_HataridosFeladatokSearch.Jogosultak = true;

        return erec_HataridosFeladatokSearch;
    }

    private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_id()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_es()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.ObjTip_Id.Value = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.Obj_Id.Value = ugyiratId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;

        return krt_EsemenyekSearch;
    }

    private EREC_UgyiratKapcsolatokSearch GetSearchObjectFromComponents_csat()
    {
        EREC_UgyiratKapcsolatokSearch erec_UgyiratKapcsolatokSearch = new EREC_UgyiratKapcsolatokSearch();

        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Value = "01";
        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Value = ugyiratId;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.equals;

        return erec_UgyiratKapcsolatokSearch;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            if (kornyezet.Equals("FPH") || kornyezet.Equals("CSEPEL") || kornyezet.Equals("BOPMH"))
            {
                ReportViewer1.ServerReport.ReportPath += kornyezet;
            }

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ugyiratId = rpis["UgyiratId"].ToString();

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {



        ReportParameter[] ReportParameters = null;



        if (rpis != null)
        {
            if (rpis.Count > 0)

            {

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);
                //	execParam.Fake = false;

                Result eloirat_result = service.GetSzereltekFoszam(execParam, erec_UgyUgyiratokSearch);

                string eloirat = "";

                foreach (DataRow _row in eloirat_result.Ds.Tables[0].Rows)
                {
                    eloirat = eloirat + _row["Foszam"].ToString() + ", ";
                }

                if (eloirat.Length > 2)
                {
                    eloirat = eloirat.Remove(eloirat.Length - 2);
                }

                if (eloirat.Length == 0)
                {
                    eloirat = " ";
                }

                EREC_HataridosFeladatokService uk_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponents_uk();

                Result uk_result = uk_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);





                ReportParameters = new ReportParameter[rpis.Count];


                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;




                        case "Eloirat":

                            ReportParameters[i].Values.Add(eloirat);

                            break;
                        /* 
						case "WhereFelj":
						    ReportParameters[i].Values.Add(uk_result.SqlCommand.GetParamValue("@Where"));
						break; */




                        case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;

                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;



                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;


                        case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;



                        case "UgyiratId":


                            //	ReportParameters[i].Values.Add(ugyiratId);


                            break;



                    }
                }




            }

        }
        return ReportParameters;
    }


}

