﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Diagnostics;

public partial class UgyUgyiratokFormTabPrintForm : Contentum.eUtility.ReportPageBase
{
    private string ugyiratId = String.Empty;
    private string[] uid = { };
    private string kornyezet = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        kornyezet = FelhasznaloProfil.OrgKod(Page);
        if (kornyezet.Equals("CSPH")) { kornyezet = "CSEPEL"; }

        if (!IsPostBack && Session["SelectedUgyiratok"] == null)
        {
            //a nyitó oldalról érkező paramétereket kiolvasó script regisztrálása
            string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldID))
            {
                Dictionary<string, string> ParentChildControl = new Dictionary<string, string>(1);
                ParentChildControl.Add(hiddenFieldID, hfSelectedUgyiratok.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl, "", EventArgumentConst.refreshValuesFromParent);
            }
        }
        else
        {
            if (Session["SelectedUgyiratok"] != null)
            {
                ugyiratId = Session["SelectedUgyiratok"].ToString();
            }
        }

        ugyiratId = ugyiratId.Replace("$", "");
        uid = ugyiratId.Split(',');
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        var erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
        erec_UgyUgyiratokSearch.Id.In(uid);
        return erec_UgyUgyiratokSearch;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitReport(ReportViewer1, Rendszerparameterek.GetBoolean(this, Rendszerparameterek.SSRS_AUTO_PDF_RENDER), GetReportFileName("előadói ív"), kornyezet);
    }

    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

                Result eloirat_result = service.GetSzereltekFoszam(execParam, erec_UgyUgyiratokSearch);
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "UgyiratId":
                            ReportParameters[i].Values.AddRange(uid);
                            break;
                        //case "WhereEsemenyek":
                        //    ReportParameters[i].Values.Add("( KRT_ESEMENYEK.Funkcio_Id not in ('8D84174E-671C-49F1-A18C-A4DB97AD0E16','D5FD9DC2-19F9-4D3D-971F-1053FDF2013D'))");
                        //    break;
                        case "WhereUgyirat":
                            ReportParameters[i].Values.Add("((((EREC_UgyiratObjKapcsolatok.KapcsolatTipus <> ''07'') and (EREC_UgyiratObjKapcsolatok.ErvKezd <= getdate()) and (EREC_UgyiratObjKapcsolatok.ErvVege >= getdate())) and ((EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt = ''" + uid + "''))))");
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}

