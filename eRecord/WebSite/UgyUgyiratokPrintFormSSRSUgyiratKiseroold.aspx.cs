﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class UgyUgyiratokFormTabPrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;
    private string tipus = String.Empty;
	private string proba = String.Empty;
	
    protected void Page_Init(object sender, EventArgs e)
    {
        

         ugyiratId = Request.QueryString.Get("UgyiratId");
        tipus = Request.QueryString.Get("tipus");

        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(tipus))
        {
            // nincs Id megadva:
        }
    }
	
	
    
    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_i()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = ugyiratId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;
        
        return erec_IraIratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_ip()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;
        
        return erec_PldIratPeldanyokSearch;
    }

    private EREC_HataridosFeladatokSearch GetSearchObjectFromComponents_uk()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();

        erec_HataridosFeladatokSearch.Obj_Id.Value = ugyiratId;
        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;

        erec_HataridosFeladatokSearch.UseUgyiratHierarchy = true;

        erec_HataridosFeladatokSearch.Jogosultak = true;
        
        return erec_HataridosFeladatokSearch;
    }
    
	private EREC_CsatolmanyokSearch GetSearchObjectFromComponents_id()
    {
        EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Value = ugyiratId;
        erec_CsatolmanyokSearch.Manual_Ugyiratok_Id.Operator = Query.Operators.equals;

        return erec_CsatolmanyokSearch;
    }

    private KRT_EsemenyekSearch GetSearchObjectFromComponents_es()
    {
        KRT_EsemenyekSearch krt_EsemenyekSearch = new KRT_EsemenyekSearch();

        krt_EsemenyekSearch.ObjTip_Id.Value = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";
        krt_EsemenyekSearch.ObjTip_Id.Operator = Query.Operators.equals;
        krt_EsemenyekSearch.Obj_Id.Value = ugyiratId;
        krt_EsemenyekSearch.Obj_Id.Operator = Query.Operators.equals;
        
        return krt_EsemenyekSearch;
    }

    private EREC_UgyiratKapcsolatokSearch GetSearchObjectFromComponents_csat()
    {
        EREC_UgyiratKapcsolatokSearch erec_UgyiratKapcsolatokSearch = new EREC_UgyiratKapcsolatokSearch();

        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Value = "01";
        erec_UgyiratKapcsolatokSearch.KapcsolatTipus.Operator = Query.Operators.equals;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Value = ugyiratId;
        erec_UgyiratKapcsolatokSearch.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.equals;
        
        return erec_UgyiratKapcsolatokSearch;
    }
	
        
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }
	
	
	protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        
		
		
		ReportParameter[] ReportParameters = null;

        
         
        if (rpis != null)
        {
            if (rpis.Count > 0)
				
            {
				
		EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
		execParam.Fake = true;

        Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Date";
        column.AutoIncrement = false;
        column.Caption = "Date";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FelhNev";
        column.AutoIncrement = false;
        column.Caption = "FelhNev";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Date"] = System.DateTime.Now.ToString();
        row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        table.Rows.Add(row);
		
		        EREC_UgyiratKapcsolatokService csat_service = eRecordService.ServiceFactory.GetEREC_UgyiratKapcsolatokService();
                EREC_UgyiratKapcsolatokSearch erec_UgyiratKapcsolatokSearch = GetSearchObjectFromComponents_csat();

                Result csat_result = csat_service.GetAllWithExtension(execParam, erec_UgyiratKapcsolatokSearch);

                
                EREC_IraIratokService i_service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                EREC_IraIratokSearch erec_IraIratokSearch = GetSearchObjectFromComponents_i();
				
				

                Result i_result = i_service.GetAllWithExtension(execParam, erec_IraIratokSearch);

                
                EREC_PldIratPeldanyokService ip_service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents_ip();

                Result ip_result = ip_service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

				               
                
                EREC_HataridosFeladatokService uk_service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = GetSearchObjectFromComponents_uk();

                Result uk_result = uk_service.GetAllWithExtension(execParam, erec_HataridosFeladatokSearch);

                				
				

                

                EREC_CsatolmanyokService id_service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = GetSearchObjectFromComponents_id();
				
				
                Result id_result = id_service.GetAllWithExtension(execParam, erec_CsatolmanyokSearch);
				
				
				

                

                Contentum.eAdmin.Service.KRT_EsemenyekService es_service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                KRT_EsemenyekSearch krt_EsemenyekSearch = GetSearchObjectFromComponents_es();

                Result es_result = es_service.GetAllWithExtension(execParam, krt_EsemenyekSearch);

                				
				 ReportParameters = new ReportParameter[rpis.Count];
					
 
                for (int i = 0; i < rpis.Count; i++)
                {
                    
                     
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);
					
					

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                           if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            { 
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
							
						case "Where_Iratok":
						    ReportParameters[i].Values.Add(i_result.SqlCommand.GetParamValue("@Where"));
						    break;
							
						case "Where_IratPeldanyok":
						    ReportParameters[i].Values.Add(ip_result.SqlCommand.GetParamValue("@Where"));
						    break;
							
						 case "Where_Felj":
						    if (uk_result.Ds.Tables[0].Rows.Count > 0 )
							{
							    ReportParameters[i].Values.Add(uk_result.SqlCommand.GetParamValue("@Where"));
							 
							}
						    
						    break;
							
						  case "Dokumentum_Id":
						    if (id_result.Ds.Tables[0].Rows.Count > 0 )
							{
							 // ReportParameters[i].Values.Add(d_result.SqlCommand.GetParamValue("@DokumentumId"));
							 ReportParameters[i].Values.Add(id_result.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString());
							 
							}
						    
						    break;
						
						case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;
							
						case "Where_Szamlak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Szamlak"));
                            break;
							
							
						case "Where_KuldKuldemenyek_Csatolt":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt")))
                            {
                               ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt"));
                            }
							
							
                            break;
						  case "Where_Csatolasok":
                            if (csat_result.Ds.Tables[0].Rows.Count > 0 )
							{
							    ReportParameters[i].Values.Add(csat_result.SqlCommand.GetParamValue("@Where"));
							 
							}
							
							else
							{
								
							}
                            break;	 
							
							
						
						
							
						case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;
						
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                          case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break; 
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
							
						
						
                          case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break; 
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                               ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                               ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        
							
						case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;
							
						
							
						case "ugyiratId":
						    
                               // ReportParameters[i].Values.Add(iratId);
								ReportParameters[i].Values.Add(ugyiratId);
                            
                            
                            break;
							
						
					
                    }
				}
				
                
				
				
				}
            
        }
        return ReportParameters;
    }
}
	
	