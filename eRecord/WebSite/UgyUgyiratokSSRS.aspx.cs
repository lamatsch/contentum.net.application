﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class UgyUgyiratokSSRS : Contentum.eUtility.UI.PageBase
{
    public string StringNumberToStringBool(string value)
    {
        if (string.IsNullOrEmpty(value)) { return "false"; }
        if (value == "1") { return "true"; }
        return "false";
    }
    private string vis = String.Empty;
    Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.UgyUgyiratokSSRS];
        visibilityState.LoadFromCustomString(visibilityFilter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
            return;

        ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
        ReportViewer1.ServerReport.ReportServerCredentials = rvc;
        System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
        ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
        ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

        ReportViewer1.ShowRefreshButton = false;
        ReportViewer1.ShowParameterPrompts = false;

        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));

                search.TopRow = 0;

                execParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(execParam, search, true);
                //Result res = service.GetAllWithExtension(ExecParam, search);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.LetrehozasIdo desc");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_KuldKuldemenyek":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_UgyUgyiratdarabok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_UgyUgyiratdarabok"));
                            break;
                        case "Where_IraIratok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_IraIratok"));
                            break;
                        case "Where_IraIktatokonyvek":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_IraIktatokonyvek"));
                            break;
                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                        case "Altalanos_FTS":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Altalanos_FTS"));
                            break;
                        case "ForMunkanaplo":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ForMunkanaplo"));
                            break;
                        case "CsakAktivIrat":
                            ReportParameters[i].Values.Add(StringNumberToStringBool(result.SqlCommand.GetParamValue("@CsakAktivIrat")));
                            break;
                        case "Where_EREC_IratMetaDefinicio":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Where_EREC_IratMetaDefinicio")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IratMetaDefinicio"));
                            }
                            break;
                        case "Csoporttagokkal":
                            ReportParameters[i].Values.Add(StringNumberToStringBool(result.SqlCommand.GetParamValue("@Csoporttagokkal")));
                            break;
                        case "SzVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Sz));
                            break;
                        case "CsatVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Csatolmany));
                            break;
                        case "CsnyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Csny));
                            break;
                        case "FVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.F));
                            break;
                        case "IktatokonyvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Iktatohely));
                            break;
                        case "FoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Foszam));
                            break;
                        case "EvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Ev));
                            break;
                        case "UgyFelelos_SzervezetKodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.UgyfeleloSzervezetKod));
                            break;
                        case "Foszam_MergeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Foszam_Merge));
                            break;
                        case "TargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Targy));
                            break;
                        case "UgyTipus_NevVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.UgyTipus_Nev));
                            break;
                        case "UgyfelUgyinditoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.NevSTR_Ugyindito));
                            break;
                        case "UgyinditoCimVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Ugyindito_Cim));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Felelos_Nev));
                            break;
                        case "VonalkodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.BARCODE));
                            break;
                        case "MegjegyzesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Megjegyzes));
                            break;
                        case "LtszVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.ITSZ));
                            break;
                        case "UgyintezoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Ugyintezo_Nev));
                            break;
                        case "IktatoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Iktato));
                            break;
                        case "IktatoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Foszam_Merge));
                            break;
                        case "AllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Allapot));
                            break;
                        case "UgyintezesModja_NevVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.UgyintezesModja_Nev));
                            break;
                        case "LetrehozasIdo_RovidVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.LetrehozasIdo_Rovid));
                            break;
                        case "IktDatumVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.LetrehozasIdo_Rovid));
                            break;
                        case "HatarIdoSakkoraVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Hatarido));
                            break;
                        case "HataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Hatarido));
                            break;
                        case "ElteletUgyintezesiIdoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.ElteltUgyintezesiIdo));
                            break;
                        case "IraHelyeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.KRT_Csoportok_Orzo_Nev));
                            break;
                        case "LezarasDatumVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.LezarasDat));
                            break;
                        case "LezarasOkaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.LezarasOka_Nev));
                            break;
                        case "SztornoDatumVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SztornirozasDat));
                            break;
                        case "SkontroKezdeteVisibility"://SkontrobaDat
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SkontrobaDat));
                            break;
                        case "SkontroHataridejeVisibility"://SkontroVege_Rovid
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SkontroVege_Rovid));
                            break;
                        case "SkontrobanOsszesenVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SkontrobanOsszesen));
                            break;
                        case "IrattarbaKuldesVisibility"://IrattarbaKuldDatuma
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.IrattarbaKuldDatuma));
                            break;
                        case "IrattarbaVetelVisibility"://IrattarbaVetelDat
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.IrattarbaVetelDat));
                            break;
                        case "KolcsonzesKezdeteVisibility"://KolcsonzesDatuma
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.KolcsonzesDatuma));
                            break;
                        case "KolcsonzesHataridejeVisibility"://KolcsonzesiHatarido
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.KolcsonzesiHatarido));
                            break;
                        case "MegorzesiIdoVegeVisibility"://MegorzesiIdoVege
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.MegorzesiIdoVege));
                            break;
                        case "FelulvizsgalatVisibility"://FelulvizsgalatDat
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.FelulvizsgalatDat));
                            break;
                        case "SelejtezveLeveltartbaVisibility"://SelejtezesDat
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SelejtezesDat));
                            break;
                        case "JellegVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.Jelleg));
                            break;
                        case "AlszamDbVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.AlszamDb));
                            break;
                        case "UgyFajtajaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.UgyFajtaja));
                            break;
                        case "ElintezesiHataridoVisibility"://SakkoraAllapotNev
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.SakkoraAllapotNev));
                            break;
                        case "ElintezesiHataridoVisibility2"://SakkoraAllapotTipus
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.IH));
                            break;
                        case "EljarasiSzakaszFokNevVisibility"://EljarasiSzakaszFokNev
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.EljarasiSzakaszFokNev));
                            break;
                        case "SakkOraHatralevoNapokVisibility"://SakkOraHatralevoNapok
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.HatralevoNapok));
                            break;
                        case "SakkoraHatralevoMunkaNapokVisibility"://SakkoraHatralevoMunkaNapok
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.HatralevoMunkanapok));
                            break;
                        case "UgyintezesIdeje_NevVisibility"://UgyintezesIdeje_Nev
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.UgyintezesIdeje_Nev));
                            break;
                        case "KezelesiUtasitasVisibility"://KezelesTipusNev
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.KezelesTipusNev));
                            break;
                        case "FT1Visibility"://FT1
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.FT1));
                            break;
                        case "FT2Visibility"://FT2
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.FT2));
                            break;
                        case "FT3Visibility"://FT3
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.UgyiratokListColumnNames.FT3));
                            break;

                    }
                }
            }
        }
        return ReportParameters;
    }
}
