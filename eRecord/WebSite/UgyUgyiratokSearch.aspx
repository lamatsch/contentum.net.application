﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokSearch.aspx.cs" Inherits="UgyUgyiratokSearch" Title="Ügyiratok keresése"
    EnableEventValidation="false" %>

<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc16" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc9" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc12" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl"
    TagPrefix="uc7" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc8" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc15" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="fhtb" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="kod" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox"
    TagPrefix="tsztb" %>
<%@ Register Src="eRecordComponent/StandardObjektumTargyszavak.ascx" TagName="StandardObjektumTargyszavak"
    TagPrefix="sot" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavakMultiSearch.ascx" TagName="ObjektumTargyszavakMultiSearch"
    TagPrefix="tszms" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc" %>
<%@ Register Src="eRecordComponent/MunkanapSearchControl.ascx" TagName="MunkanapSearchControl"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .mrUrlapCaption_short {
            font-weight: bold;
            padding-right: 1px;
            padding-left: 1px;
            vertical-align: middle;
            text-align: right;
            color: #335674;
            width: 1px;
            min-width: 1px;
            white-space: nowrap;
        }

        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            font-weight: bold;
            padding-right: 1px;
            vertical-align: middle;
            text-align: right;
            color: #335674;
            width: 1px;
            min-width: 1px;
            white-space: nowrap;
        }

        body, html {
            overflow: auto;
        }
    </style>

    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <ajaxToolkit:CollapsiblePanelExtender ID="DatumKeresesCPE" runat="server" TargetControlID="Datumok_eFormPanel"
        CollapsedSize="0" Collapsed="true" AutoExpand="false" AutoCollapse="false" ScrollContents="false"
        ExpandedText="<%$Resources:Search,UI_DatumKereses_ExpandedText%>" CollapsedText="<%$Resources:Search,UI_DatumKereses_CollapsedText%>"
        TextLabelID="labelDatumKeresesShowHide" ExpandControlID="DatumKeresesHeader"
        CollapseControlID="DatumKeresesHeader" ExpandedImage="images/hu/Grid/minus.gif"
        CollapsedImage="images/hu/Grid/plus.gif" ImageControlID="DatumKeresesCPEButton">
    </ajaxToolkit:CollapsiblePanelExtender>
    <ajaxToolkit:CollapsiblePanelExtender ID="KuldemenyAdataiCPE" runat="server" TargetControlID="Kuldemeny_eFormPanel"
        CollapsedSize="0" Collapsed="true" AutoExpand="false" AutoCollapse="false" ScrollContents="false"
        ExpandedText="<%$Resources:Search,UI_KuldemenyAdatai_ExpandedText%>" CollapsedText="<%$Resources:Search,UI_KuldemenyAdatai_CollapsedText%>"
        TextLabelID="labelKuldemenyAdataiShowHide" ExpandControlID="KuldemenyAdataiHeader"
        CollapseControlID="KuldemenyAdataiHeader" ExpandedImage="images/hu/Grid/minus.gif"
        CollapsedImage="images/hu/Grid/plus.gif" ImageControlID="KuldemenyAdataiCPEButton">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="AltalanosKeresesPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="90%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAltalanos" runat="server" CssClass="mrUrlapCaption" Text="Ügy/irat/küld.tárgy, ügyindító, beküldõ tartalmaz:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Altalanos_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                            </td>
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="StandardUgyiratTargyszavakPanel" runat="server" Visible="true">
                    <sot:StandardObjektumTargyszavak ID="sotUgyiratTargyszavak" runat="server" SearchMode="true" />
                </eUI:eFormPanel>
                <%--                    <eUI:eFormPanel ID="EFormPanelFTS" runat="server">
					    <table cellspacing="0" cellpadding="0" width="90%">
						    <tr class="urlapSor_kicsi">
							    <td class="mrUrlapCaption">
								    <asp:Label ID="Label18" runat="server" Text="Tárgyszó:"></asp:Label></td>
							    <td class="mrUrlapMezo">
								    <tsztb:TargySzavakTextBox ID="Targyszavak_TextBoxTargyszo" runat="server"
							            Validate="false" SearchMode="true" /></td>
							    <td class="mrUrlapCaption">
								    <asp:Label ID="Label20" runat="server" Text="Érték:"></asp:Label></td>
							    <td class="mrUrlapMezo">
								    <asp:TextBox ID="Targyszavak_TextBoxErtek" runat="server"></asp:TextBox></td>
							    <td class="mrUrlapCaption">
								    <asp:Label ID="Label31" runat="server" Text="Megjegyzés:"></asp:Label></td>
							    <td class="mrUrlapMezo">
								    <asp:TextBox ID="Targyszavak_TextBoxNote" runat="server"></asp:TextBox></td>														
						    </tr>
					    </table>
				</eUI:eFormPanel>--%>
                <eUI:eFormPanel ID="EFormPanelOTMultiSearch" runat="server">
                    <tszms:ObjektumTargyszavakMultiSearch ID="tszmsEgyebTargyszavak" runat="server" />
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanel3" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label33" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label17" runat="server" Text="Iktatókönyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IraIktatoKonyvekDropDownList ID="UgyDarab_IraIktatoKonyvek_DropDownList" runat="server"
                                    Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    Filter_IdeIktathat="false" IsMultiSearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label12" runat="server" Text="Fõszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc14:SzamIntervallum_SearchFormControl ID="UgyDarab_Foszam_SzamIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label11" runat="server" Text="Vonalkód:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc10:VonalKodTextBox ID="Ugy_BarCode_TextBox" runat="server" Validate="false"></uc10:VonalKodTextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label15" runat="server" Text="Ügyirat állapot:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="Ugy_Allapot_KodtarakDropDownListUgyUgyirat" runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label21" runat="server" Text="Sürgõsség:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="Ugy_Surgosseg_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="cbExcludeTovabbitasAlatt" Text="Továbbítás alattiak nélkül" Checked="true"
                                    runat="server" />
                                <asp:CheckBox ID="cbExcludeSzereltek" Text="Szereltek nélkül" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="Irattári tételszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:IraIrattariTetelTextBox ID="Ugy_IraIrattariTetelTextBox" runat="server" SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label14" runat="server" Text="Tárgy:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Ugy_Targy_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgytipus" runat="server" Text="Ügytípus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="Ugytipus_DropDownList"
                                    UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetUgytipusokByUgykorForModify"
                                    Category="Ugytipus" Enabled="false" EmptyText="---" EmptyValue="" SelectedValue=""
                                    LoadingText="[Feltöltés folyamatban...]" PromptText="<Ügytípus kiválasztása>"
                                    PromptValue="">
                                </ajaxToolkit:CascadingDropDown>
                                <asp:DropDownList ID="Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputFullComboBox">
                                </asp:DropDownList>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyfelelos" runat="server" Text="Felelõs:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CsoportTextBox ID="CsopId_UgyFelelos_CsoportTextBox" runat="server" SzervezetCsoport="true" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelJelleg" runat="server" Text="Ügyirat típusa:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="Ugy_Jelleg_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyintezesModja" runat="server" Text="Elsõdleges adathordozó típusa:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="Ugy_UgyintezesModja_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <%--BLG_1014--%>
                                <%--<asp:Label ID="Label5" runat="server" Text="Ügyintézõ:"></asp:Label>--%>
                                <asp:Label ID="labelUgyUgyirat_Ugyintezo" runat="server" Text="<%$Forditas:labelUgyUgyirat_Ugyintezo|Ügyintézõ:%>"></asp:Label>

                            </td>
                            <td class="mrUrlapMezo">
                                <uc16:FelhasznaloCsoportTextBox ID="Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                    runat="server" SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Kezelõ:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CsoportTextBox ID="Ugy_CsopId_Felelos_CsoportTextBox" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label9" runat="server" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CsoportTextBox ID="Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox" runat="server"
                                    SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label13" runat="server" Text="Ügyindító, ügyfél:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:PartnerTextBox ID="Ugy_PartnerId_Ugyindito_PartnerTextBox" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label22" runat="server" Text="Irattári hely:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Ugy_Irattarihely_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                <br />
                                <asp:CheckBox ID="cbNincsMegadvaIrattariHely" runat="server" Text="Nincs megadva irattári hely" CssClass="urlapCheckbox" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyinditoCime" runat="server" Text="Ügyindító címe:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CimekTextBox ID="CimekTextBoxUgyindito" runat="server" SearchMode="true"></uc3:CimekTextBox>
                            </td>
                        </tr>
                        <tr id="tr_jovahagyo" runat="server" visible="true" class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label_Jovahagyo" runat="server" Text="Jóváhagyó:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CsoportTextBox ID="Jovahagyo_CsoportTextBox" runat="server" SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="UgyFajtaja_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr id="trLezarasOka" runat="server" visible="true" class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label20" runat="server" Text="Lezárás oka:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="LezarasOka_KodtarakDropDownList" runat="server" />
                            </td>
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="NemCsoporttagokkal_CheckBox" runat="server" Text="Saját jogon látható tételek" />
                                <asp:CheckBox ID="CsakAktivIrat_CheckBox" runat="server" Text="Aktív állapotban lévõ tételek" />
                            </td>
                        </tr>
                        <%--LZS - BUG_7099
                        Ügyirathoz tartozó megjegyzés mező. EREC_UgyUgyiratok.Note--%>
                        <tr id="trMegjegyzes" runat="server" visible="false" class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="lblMegjegyzes" runat="server" Text="<%$Forditas:lblMegjegyzes|Megjegyzés:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtMegjegyzes" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Image ID="SpacerImage1" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif"
                                    Visible="true" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr class="tabExtendableListFejlec">
            <td style="text-align: left;">
                <asp:Panel runat="server" ID="DatumKeresesHeader" CssClass="cursor_Hand">
                    <asp:ImageButton runat="server" ID="DatumKeresesCPEButton" ImageUrl="images/hu/Grid/plus.gif"
                        OnClientClick="return false;" />
                    &nbsp;
                    <asp:Label ID="labelDatumKeresesShowHide" runat="server" Text="<%$Resources:Search,UI_DatumKereses_CollapsedText%>" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="Datumok_eFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="90%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label27" runat="server" Text="Iktatás idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_LetrehozasIdo_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <%--<td class="mrUrlapCaption"></td>--%>
                            <td class="mrUrlapMezo" colspan="2" style="padding-left: 85px;">
                                <uc:MunkanapSearchControl ID="MunkanapSearchControl" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo" style="font-weight: bold">
                                <asp:CheckBox runat="server" ID="NaponBelulNincsUjAlszam" Text="6 napon belül nem született új alszám" />
                            </td>
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label23" runat="server" Text="Ügyirat ügyintézési határideje:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_Hatarido_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" TimeVisible="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelHataridoElott" runat="server" Text="Határidõ elõtt"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc9:RequiredNumberBox ID="Ugy_HataridoElottXNappal_RequiredNumberBox" runat="server"
                                    Validate="false" />
                                <asp:Label ID="labelXNappal" runat="server" Text="nappal"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label25" runat="server" Text="Ügyirat elintézési idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_ElintezesDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label30" runat="server" Text="Ügyirat lezárási idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_LezarasDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label24" runat="server" Text="Határidõbe tétel idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_SkontrobaDat_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label29" runat="server" Text="Határidõbe tétel lejárata:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_SkontroVege_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIrattarbaKuldes" runat="server" Text="Irattárba küldés:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_IrattarbaKuldes_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label34" runat="server" Text="Irattárba vétel:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_IrattarbaVetelDat_DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" runat="server" id="trKolcsonzesiHatarido">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label10" runat="server" Text="Kölcsönzés idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="disKolcsonzesDatuma" runat="server"
                                    Validate="false" ValidateDateFormat="true" CalendarPosition="TopLeft" />
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label18" runat="server" Text="Kölcsönzés határideje:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="disKolcsonzesHatarido" runat="server"
                                    Validate="false" ValidateDateFormat="true" CalendarPosition="TopLeft" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr class="tabExtendableListFejlec">
            <td style="text-align: left;">
                <asp:Panel runat="server" ID="KuldemenyAdataiHeader" CssClass="cursor_Hand">
                    <asp:ImageButton runat="server" ID="KuldemenyAdataiCPEButton" ImageUrl="images/hu/Grid/plus.gif"
                        OnClientClick="return false;" />
                    &nbsp;
                    <asp:Label ID="labelKuldemenyAdataiShowHide" runat="server" Text="<%$Resources:Search,UI_KuldemenyAdatai_CollapsedText%>" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="Kuldemeny_EFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKuld_Erkeztetokonyv_Ev" runat="server" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="Kuld_EvIntervallum_SearchFormControl" runat="server" />
                            </td>
                            <td class="mrUrlapCaption"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="Érkeztetõkönyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IraIktatoKonyvekDropDownList ID="Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList"
                                    Mode="ErkeztetoKonyvek" EvIntervallum_SearchFormControlId="Kuld_EvIntervallum_SearchFormControl"
                                    runat="server" IsMultiSearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 187px">
                                <asp:Label ID="Label7" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc14:SzamIntervallum_SearchFormControl ID="Kuld_ErkeztetoSzam_SzamIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption" style="height: 35px">
                                <asp:Label ID="Label1" runat="server" Text="Postai azonosító:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="height: 35px">
                                <uc10:VonalKodTextBox ID="Kuld_Ragszam_TextBox" runat="server" Validate="false"></uc10:VonalKodTextBox>
                            </td>
                            <td class="mrUrlapCaption" style="width: 187px; height: 35px">
                                <asp:Label ID="Label6" runat="server" Text="Hivatkozási szám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="height: 35px">
                                <asp:TextBox ID="Kuld_HivatkozasiSzam_TextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Küldõ/feladó neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:PartnerTextBox ID="Kuld_PartnerId_Bekuldo_PartnerTextBox" runat="server" SearchMode="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 187px">
                                <asp:Label ID="Label8" runat="server" Text="Küldõ/feladó címe:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label19" runat="server" Text="Érkezés dátuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Kuld_Beerkezesideje_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                            <td class="mrUrlapCaption" style="width: 187px"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="CheckBox_EFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="70%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSajat" runat="server" Text="Saját" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxHataridoben" runat="server" Text="Határidõben" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxHataridontul" runat="server" Text="Határidõn túl" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxMunkaugyiratok" runat="server" Text="Munkaügyiratok" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxElo" runat="server" Text="Élõ ügyiratok" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSkontroban" runat="server" Text="Skontróban" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxElintezett" runat="server" Text="Elintézett ügyirat" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSzerelesreElokeszitett" runat="server" Text="Szerelésre&nbsp;elõkészített&nbsp;utóiratok" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxLezart" runat="server" Text="Lezártak" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSztorno" runat="server" Text="Sztornózottak" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSzerelt" runat="server" Text="Szereltek" />
                            </td>
                            <td class="mrUrlapCaptionBal">
                                <asp:CheckBox ID="CheckBoxSzerelendok" runat="server" Text="Szerelendõ&nbsp;elõiratok" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
