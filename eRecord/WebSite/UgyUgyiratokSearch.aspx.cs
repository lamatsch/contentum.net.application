﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.FullTextSearch;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class UgyUgyiratokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_UgyUgyiratokSearch);

    private const string kcs_UGYIRAT_ALLAPOT = "UGYIRAT_ALLAPOT";
    private const string kcs_SURGOSSEG = "SURGOSSEG";
    private const string kcs_UGYIRAT_JELLEG = "UGYIRAT_JELLEG";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_LEZARAS_OKA= "LEZARAS_OKA";

    private String Startup = "";

    bool LezarasOkaVisible { get { return true; } }

    #region ControlCheckUtils

    private bool IsThereAFilledControl_DatumKereses()
    {
        bool bIsThereFilledControl = !Ugy_LetrehozasIdo_DatumIntervallum_SearchCalendarControl.IsEmpty
        || MunkanapSearchControl.Checked == true
        || !Ugy_Hatarido_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !String.IsNullOrEmpty(Ugy_HataridoElottXNappal_RequiredNumberBox.Text)
        || !Ugy_ElintezesDat_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !Ugy_LezarasDat_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !Ugy_SkontrobaDat_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !Ugy_SkontroVege_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !Ugy_IrattarbaVetelDat_DatumIntervallum_SearchCalendarControl1.IsEmpty
        || !Ugy_IrattarbaKuldes_DatumIntervallum_SearchCalendarControl.IsEmpty
        || !disKolcsonzesHatarido.IsEmpty || !disKolcsonzesDatuma.IsEmpty;

        return bIsThereFilledControl;
    }

    private bool IsThereAFilledControl_KuldemenyAdatai()
    {
        bool bIsThereFilledControl = !String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue)
            || !String.IsNullOrEmpty(Kuld_ErkeztetoSzam_SzamIntervallum_SearchFormControl.SzamTol)
            || !String.IsNullOrEmpty(Kuld_ErkeztetoSzam_SzamIntervallum_SearchFormControl.SzamIg)
            || !String.IsNullOrEmpty(Kuld_Ragszam_TextBox.Text)
            || !String.IsNullOrEmpty(Kuld_HivatkozasiSzam_TextBox.Text)
            || !String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField)
            || !String.IsNullOrEmpty(Kuld_CimId_CimekTextBox.Id_HiddenField)
            || !String.IsNullOrEmpty(Kuld_Beerkezesideje_DatumIntervallum_SearchCalendarControl.DatumKezd)
            || !String.IsNullOrEmpty(Kuld_Beerkezesideje_DatumIntervallum_SearchCalendarControl.DatumVege)            
            || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvTol)
            || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvIg);

        return bIsThereFilledControl;
    }

    #endregion ControlCheckUtils

    #region JavaScripts
    public void RegisterJavaScripts()
    {
            string js_AddHandler = @"

        function UgykorChangeHandler()
            {
                //FillUgytipusDropDown();
                var cdde = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
                var hfUgykorId = $get('" + Ugy_IraIrattariTetelTextBox.HiddenField.ClientID + @"');

                if (cdde && hfUgykorId)
                {
                    cdde.set_contextKey(hfUgykorId.value + '||0');
                    cdde._onParentChange(null, true);
                }

            }

        function callCreateItemToolTips(sender, args)
            {
                var dropdown = sender.get_element();
                if (dropdown)
                {
                    Utility.DomElement.Select.createItemToolTips(dropdown);
                }
            }        

        function pageLoad(){//be called automatically.

            var cascadingDropDown_ugytipus = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
            if (cascadingDropDown_ugytipus)
            {
                cascadingDropDown_ugytipus.add_populated(callCreateItemToolTips);
            }
         }
        ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dropDown_handler", js_AddHandler, true);

        Ugy_IraIrattariTetelTextBox.TextBox.Attributes.Add("onchange", "UgykorChangeHandler()");
        Ugytipus_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
}
    #endregion JavaScripts

    private bool IsKolcsonzottSearch
    {
        get
        {
            return Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott;
        }
    }

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.UgyUgyiratSearchHeaderTitle;
        SearchHeader1.TemplateObjectType = _type;

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);

        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch;
        }
        else if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch;
        }
        else if (Startup == Constants.Startup.Skontro)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SkontroSearch;
        }
        else if (Startup == Constants.Startup.FromMunkanaplo)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunaknaploSearch;
        }
        else if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch;
        }

        // irattári tételszám - ügytípus összekapcsolás
        Ugy_IraIrattariTetelTextBox.TryFireChangeEvent = true;
        CascadingDropDown_Ugytipus.Enabled = true;
        RegisterJavaScripts();
        
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            // Jelezzük az iktatókönyv komponensnek, hogy Id-kat kell beletölteni, nem iktatóhely értéket: (BUG#4290)
            UgyDarab_IraIktatoKonyvek_DropDownList.ValuesFilledWithId = true;
            Label22.Text = "Fizikai hely";
            cbNincsMegadvaIrattariHely.Text = "Nincs megadva fizikai hely";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimTextBox = CimekTextBoxUgyindito.TextBox;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimHiddenField = CimekTextBoxUgyindito.HiddenField;

        if (!IsPostBack)
        {

            EREC_UgyUgyiratokSearch searchObject = null;

            switch (Startup)
            {                
                case Constants.Startup.FromAtmenetiIrattar:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page,Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromKozpontiIrattar:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.Skontro:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SkontroSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , Constants.CustomSearchObjectSessionNames.SkontroSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromMunkanaplo:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunaknaploSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , Constants.CustomSearchObjectSessionNames.MunaknaploSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();

                        // BLG_7987
                        var kornyezet = FelhasznaloProfil.OrgKod(Page);
                        if (kornyezet == "CSPH" || kornyezet == "NMHH")
                        {
                            searchObject.CsakAktivIrat = true;
                        }
                    }
                    break;
                case Constants.Startup.FromSkontroIrattar:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                // CR3289 Vezetõi panel kezelés
                case Constants.Startup.FromVezetoiPanel_Ugyirat:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                default:
                    if (Search.IsSearchObjectInSession(Page, _type))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
            }    

            LoadComponentsFromSearchObject(searchObject);

            /* Határidõ elõtt x nap csak akkor tölthetõ ha 'Határidõben' és 'Határidõn túl' unchecked */
            //CheckBoxListHatarido.Attributes["onclick"] = "Javascript: " +
            //    " document.getElementById('RequiredNumberBoxHataridoElott').disabled = " +
            //    " (document.getElementById('" + CheckBoxListHatarido.ClientID + "_0').checked || " +
            //    " document.getElementById('" + CheckBoxListHatarido.ClientID + "_1').checked); " +
            //    " return true;";

        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // BUG_9693
        //if (FelhasznaloProfil.OrgIsBOPMH(Page))
        //{
        //    Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol_TextBox.Focus();
        //}
        //else
        //{
        //    //Fókusz beállítása a vonalkód mezõre
        //    Page.SetFocus(Ugy_BarCode_TextBox.TextBox);
        //}
        if (!IsPostBack)
        {
            if (FelhasznaloProfil.OrgIs(Page, Constants.OrgKod.FPH))
            {
                //Fókusz beállítása a vonalkód mezõre
                Page.SetFocus(Ugy_BarCode_TextBox.TextBox);
            }
            else
            {
                Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol_TextBox.Focus();
            }
        }
        SetCompontentsAccessibility();

        #region összecsukható panelek láthatósága tartalmuk alapján
        // ha van adat, kinyitjuk a panelt
        #region dátum keresõ mezõk
        bool bDatumKeresesPanelCollapsed = true   // alapértelmezésben összecsukva
            && !IsThereAFilledControl_DatumKereses();

        DatumKeresesCPE.Collapsed = bDatumKeresesPanelCollapsed;
        #endregion dátum keresõ mezõk tartalmuk alapján

        #region küldemény adatai mezõk
        bool bKuldemenyAdataiPanelCollapsed = true   // alapértelmezésben összecsukva
            && !IsThereAFilledControl_KuldemenyAdatai();

        KuldemenyAdataiCPE.Collapsed = bKuldemenyAdataiPanelCollapsed;
        #endregion küldemény adatai mezõk

        #endregion összecsukható panelek láthatósága

        string js = disKolcsonzesHatarido.GetJsSetReadonly(true) + disKolcsonzesDatuma.GetJsSetReadonly(true);

        if (!IsKolcsonzottSearch)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "!IsKolcsonzottSearch", js, true);
        }


        js = disKolcsonzesHatarido.GetJsSetReadonly(true) + disKolcsonzesDatuma.GetJsSetReadonly(true);
        js += "var drop = $get('" + Ugy_Allapot_KodtarakDropDownListUgyUgyirat.DropDownList.ClientID + @"');
               if(drop && drop.selectedIndex > -1){
                    var allapot= drop.options[drop.selectedIndex].value;
                    if(allapot == '" + KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott + @"'){
                        " + disKolcsonzesHatarido.GetJsSetReadonly(false) + disKolcsonzesDatuma.GetJsSetReadonly(false) 
                          + disKolcsonzesHatarido.GetJsRestore() + disKolcsonzesDatuma.GetJsRestore() + @"
                    }else { " + disKolcsonzesHatarido.GetJsClear() + disKolcsonzesDatuma.GetJsClear() + @"}
               }";

        Ugy_Allapot_KodtarakDropDownListUgyUgyirat.DropDownList.Attributes["onchange"] = js;

        trLezarasOka.Visible = LezarasOkaVisible;
    }

    #endregion


    private void SetCompontentsAccessibility()
    {
        Ugy_Allapot_KodtarakDropDownListUgyUgyirat.ReadOnly = false;

        var defaultSearch = GetDefaultSearchObject();

        if (defaultSearch.IsSzereltekExcluded)
            cbExcludeSzereltek.Visible = false;
    }


    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = null;
        if (searchObject != null) erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)searchObject;

        if (erec_UgyUgyiratokSearch != null)
        {
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
               erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
            if (isTUK)
            {
                //TÜK esetén az id lesz az érték mezõnek feltöltve, mivel itt Id szerinti keresés lesz
                string id = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id == null ? null : erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id.Value;
                UgyDarab_IraIktatoKonyvek_DropDownList.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg, true, false, id, SearchHeader1.ErrorPanel);
            }
            else
            {
                /// Iktatokonyvek megkülönböztetõ jelzés alapján való feltöltése:
                /// Nem szûrünk a szervezet által láthatókra, hozzuk az összeset (2008.11.10)
                UgyDarab_IraIktatoKonyvek_DropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
            }

            UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Foszam);

            //if (erec_UgyUgyiratokSearch.fts_targy != null)
            //    Ugy_Targy_TextBox.Text = erec_UgyUgyiratokSearch.fts_targy.Filter;
            Ugy_Targy_TextBox.Text = erec_UgyUgyiratokSearch.Targy.Value;

            //Ugy_IktatoszamKieg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATOSZAM_KIEG,
            //    erec_UgyUgyiratokSearch.IktatoszamKieg.Value, true, SearchHeader1.ErrorPanel);

            // Ügyfelelõs
            CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.Csoport_Id_Ugyfelelos.Value;
            CsopId_UgyFelelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            switch (Startup)
            {
                case Constants.Startup.FromKozpontiIrattar:
                    // Irat helye a Központi irattár:                    
                    Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower();
                    Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                    Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.ReadOnly = true;

                    // ezekre az állapotokra szûkítjük a listát:
                    List<string> allapotFilterList_kozp = new List<string>();
                    allapotFilterList_kozp.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                    allapotFilterList_kozp.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
                    allapotFilterList_kozp.Add(KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo);

                    Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
                        erec_UgyUgyiratokSearch.Allapot.Value, allapotFilterList_kozp, true, SearchHeader1.ErrorPanel);
                    //Ugy_Allapot_KodtarakDropDownListUgyUgyirat.Enabled = false;

                    //LZS - BUG_7099
                    trMegjegyzes.Visible = true;
                    break;

                case Constants.Startup.FromAtmenetiIrattar:
                    // Mivel a listán 
                    if (erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value.ToLower() == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower())
                    {
                        Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = "";
                    }
                    else
                    {
                        Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value;
                    }
                    Ugy_CsopId_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                    Ugy_CsopId_Felelos_CsoportTextBox.ReadOnly = false;

                    // ezekre az állapotokra szûkítjük a listát:
                    List<string> allapotFilterList = new List<string>();
                    allapotFilterList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
                    allapotFilterList.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
                    allapotFilterList.Add(KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott);
                    allapotFilterList.Add(KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo);

                    Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
                        erec_UgyUgyiratokSearch.Allapot.Value, allapotFilterList, true, SearchHeader1.ErrorPanel);
                    //Ugy_Allapot_KodtarakDropDownListUgyUgyirat.Enabled = false;

                    // ezen állapotok mellett nem lehet jóváhagyó
                    tr_jovahagyo.Visible = false;
                    break;
                case Constants.Startup.FromSkontroIrattar:
                case Constants.Startup.Skontro:
                    // Kezelõ a Skontró irattár:
                    if (Startup == Constants.Startup.FromSkontroIrattar)
                    {
                        Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(Page).Obj_Id.ToLower();
                        Ugy_CsopId_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                        Ugy_CsopId_Felelos_CsoportTextBox.ReadOnly = true;
                    }

                    // ezekre az állapotokra szûkítjük a listát:
                    List<string> allapotFilterList_skontroirattar = new List<string>();
                    allapotFilterList_skontroirattar.Add(KodTarak.UGYIRAT_ALLAPOT.Skontroban);
                    allapotFilterList_skontroirattar.Add(KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert);

                    Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
                        erec_UgyUgyiratokSearch.Allapot.Value, allapotFilterList_skontroirattar, true, SearchHeader1.ErrorPanel);

                    // ezen állapotok mellett nem lehet jóváhagyó
                    tr_jovahagyo.Visible = false;
                    break;
                default:
                    // ha több állapot van megadva, nem tudjuk kitölteni
                    string Allapot = erec_UgyUgyiratokSearch.Allapot.Value.Replace("'", "");
                    string[] separator = new string[] { "," };
                    int cntIds = Allapot.Split(separator, StringSplitOptions.None).Length;
                    if (cntIds < 2)
                    {
                        Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
                            Allapot, true, SearchHeader1.ErrorPanel);
                    }
                    else
                    {
                        Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetEmptyValue(kcs_UGYIRAT_ALLAPOT,
                            SearchHeader1.ErrorPanel);
                    }

                    //Ugy_Allapot_KodtarakDropDownListUgyUgyirat.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
                    //    erec_UgyUgyiratokSearch.Allapot.Value, true, SearchHeader1.ErrorPanel);

                    //LZS - BUG_7099
                    trMegjegyzes.Visible = true;
                    break;
            }

            cbExcludeTovabbitasAlatt.Checked = !erec_UgyUgyiratokSearch.IsTovabbitasAlattAllapotIncluded;

            cbExcludeSzereltek.Checked = erec_UgyUgyiratokSearch.IsSzereltekExcluded;

            Ugy_Surgosseg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SURGOSSEG,
                erec_UgyUgyiratokSearch.Surgosseg.Value, true, SearchHeader1.ErrorPanel);

            Ugy_IraIrattariTetelTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value;
            Ugy_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);

            CascadingDropDown_Ugytipus.ContextKey = erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value + "|" + erec_UgyUgyiratokSearch.UgyTipus.Value + "|" + "0";
            //CascadingDropDown_Ugytipus.Enabled = true;
            CascadingDropDown_Ugytipus.SelectedValue = erec_UgyUgyiratokSearch.UgyTipus.Value;

            // Ügyirat típusa
            Ugy_Jelleg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYIRAT_JELLEG,
                erec_UgyUgyiratokSearch.Jelleg.Value, true, SearchHeader1.ErrorPanel);
            // Elsõdleges adathordozó típusa
            Ugy_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
                erec_UgyUgyiratokSearch.UgyintezesModja.Value, true, SearchHeader1.ErrorPanel);

            if (erec_UgyUgyiratokSearch.IrattariHely.Operator == Query.Operators.isnull)
            {
                cbNincsMegadvaIrattariHely.Checked = true;
            }
            else
            {
                cbNincsMegadvaIrattariHely.Checked = false;
                Ugy_Irattarihely_TextBox.Text = erec_UgyUgyiratokSearch.IrattariHely.Value;
            }

            Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value;
            Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Ugy_BarCode_TextBox.Text = erec_UgyUgyiratokSearch.BARCODE.Value;

            // Átmeneti és skontró irattár esetén már állítottuk
            if (Startup != Constants.Startup.FromAtmenetiIrattar && Startup != Constants.Startup.FromSkontroIrattar)
            {
                // csak ha tényleg egy Id van benne
                string Csoport_Id_Felelos = erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value.Replace("'", "");
                string[] separator = new string[] { "," };
                int cntIds = Csoport_Id_Felelos.Split(separator, StringSplitOptions.None).Length;
                if (cntIds < 2)
                {
                    Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = Csoport_Id_Felelos;
                }
                else
                {
                    Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = "";
                }

                //Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value;
                Ugy_CsopId_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

                Jovahagyo_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.Kovetkezo_Felelos_Id.Value;
                Jovahagyo_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            }

            // Központi irattárnál már korábban beállítottuk a mezõt:
            if (Startup != Constants.Startup.FromKozpontiIrattar)
            {
                Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value;
                Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            }

            Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratokSearch.Partner_Id_Ugyindito.Value, erec_UgyUgyiratokSearch.NevSTR_Ugyindito.Value, SearchHeader1.ErrorPanel);
            CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratokSearch.Cim_Id_Ugyindito.Value, erec_UgyUgyiratokSearch.CimSTR_Ugyindito.Value, SearchHeader1.ErrorPanel);

            Ugy_Hatarido_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Hatarido);

            // TODO: hogy legyen visszatöltve?
            DateTime dtHatarido;
            if (DateTime.TryParse(erec_UgyUgyiratokSearch.Manual_HataridoElottXNappal.ValueTo, out dtHatarido))
            {
                int dayDiff = (dtHatarido - DateTime.Today).Days;
                Ugy_HataridoElottXNappal_RequiredNumberBox.Text = dayDiff.ToString();
            }
            else
            {
                Ugy_HataridoElottXNappal_RequiredNumberBox.Text = "";
            }

            Ugy_SkontrobaDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.SkontrobaDat);

            Ugy_SkontroVege_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.SkontroVege);

            Ugy_IrattarbaVetelDat_DatumIntervallum_SearchCalendarControl1.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.IrattarbaVetelDat);

            Ugy_ElintezesDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.ElintezesDat);

            Ugy_LezarasDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.LezarasDat);

            //Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
            //    erec_UgyUgyiratokSearch.MegorzesiIdoVege);

            Ugy_IrattarbaKuldes_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.IrattarbaKuldDatuma);

            Ugy_LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);

            if (erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch != null)
            {
                disKolcsonzesDatuma.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.KikerKezd);
                disKolcsonzesHatarido.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.KikerVege);
            }

            //Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillAndSetSelectedValue(
            //    false, false, Constants.IktatoErkezteto.Erkezteto, erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value,
            //    true, SearchHeader1.ErrorPanel);
            if (erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch != null
                && erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                Kuld_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                , Kuld_EvIntervallum_SearchFormControl.EvTol, Kuld_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
            }
            else
            {
                Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                    , "", "", true, false, SearchHeader1.ErrorPanel);
            }

            Kuld_ErkeztetoSzam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Erkezteto_Szam);

            Kuld_Ragszam_TextBox.Text = erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.RagSzam.Value;

            Kuld_HivatkozasiSzam_TextBox.Text = erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Value;

            Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField =
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value;
            if (erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value != String.Empty)
                Kuld_PartnerId_Bekuldo_PartnerTextBox.Text = erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;

            Kuld_Beerkezesideje_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.BeerkezesIdeje);

            Kuld_CimId_CimekTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Value;
            Kuld_CimId_CimekTextBox.SetCimekTextBoxById(SearchHeader1.ErrorPanel);
            if (erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value != String.Empty)
                Kuld_CimId_CimekTextBox.Text = erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value;


            CheckBoxSajat.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Sajat.Value)) ? true : false;

            // Az operátor be van-e állítva (isNull elvileg)
            CheckBoxMunkaugyiratok.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Foszam_MunkaanyagFilter.Operator)) ? true : false;

            CheckBoxElo.Checked = 
                (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.Value)
                 || !String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Value)) ? true : false;

            CheckBoxLezart.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Lezartak.Value)) ? true : false;

            CheckBoxHataridoben.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Hataridoben.Value)) ? true : false;

            CheckBoxSkontroban.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Skontroban.Value)) ? true : false;

            //CheckBoxSztorno.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Sztornozottak.Value)) ? true : false;
            // CR#2034: Alapból szûrjük a sztornózottakat, ezért megfordítjuk a vizsgálatot
            CheckBoxSztorno.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Sztornozottak.Value) && String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Sztornozottak.Operator)) ? true : false;

            CheckBoxHataridontul.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Hataridon_tul.Value)) ? true : false;

            CheckBoxElintezett.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Elintezett.Value)) ? true : false;

            CheckBoxSzerelt.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Szereltek.Value)) ? true : false;

            CheckBoxSzerelesreElokeszitett.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_SzerelesreElokeszitettek.Value)) ? true : false;

            CheckBoxSzerelendok.Checked = (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Szerelendok.Value)) ? true : false;

            MunkanapSearchControl.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch.Manual_IktatasDatuma_Munkanap);

            #region standard objektumfüggõ tárgyszavak
            // ügyirat
            sotUgyiratTargyszavak.FillStandardTargyszavak(null, null, Constants.TableNames.EREC_UgyUgyiratok, SearchHeader1.ErrorPanel);

            if (sotUgyiratTargyszavak.Count > 0)
            {
                StandardUgyiratTargyszavakPanel.Visible = true;

                //// vigyázat, feltételezzük, hogy felváltva lett hozzáadva tárgyszó és érték...!
                //if (erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto != null)
                //{
                //    List<string> items = erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto.GetLeafList();
                //    if (items != null)
                //    {
                //        int i = 0;
                //        while (i + 1 < items.Count)
                //        {
                //            string items_targyszo = items[i];
                //            string items_ertek = items[i + 1];
                //            string[] item_elements_targyszo = items_targyszo.Split(FullTextSearchTree.FieldValueSeparator);
                //            string[] item_elements_ertek = items_ertek.Split(FullTextSearchTree.FieldValueSeparator);
                //            // tárgyszó értékének beírása a mezõbe
                //            sotUgyiratTargyszavak.TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

                //            i += 2;
                //        }
                //    }
                //}
                sotUgyiratTargyszavak.FillFromFTSTree(erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto);
            }
            else
            {
                StandardUgyiratTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumfüggõ tárgyszavak

            #region egyéb tárgyszavak
            tszmsEgyebTargyszavak.FillFromFTSTree(erec_UgyUgyiratokSearch.fts_tree_ugyirat_egyeb);
            #endregion egyéb tárgyszavak

            #region fts
            //if (erec_UgyUgyiratokSearch.fts_targyszavak != null)
            //{
            //    Targyszavak_TextBoxTargyszo.Text = erec_UgyUgyiratokSearch.fts_targyszavak.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxTargyszo.Text = "";
            //}

            //if (erec_UgyUgyiratokSearch.fts_ertek != null)
            //{
            //    Targyszavak_TextBoxErtek.Text = erec_UgyUgyiratokSearch.fts_ertek.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxErtek.Text = "";
            //}

            //if (erec_UgyUgyiratokSearch.fts_note != null)
            //{
            //    Targyszavak_TextBoxNote.Text = erec_UgyUgyiratokSearch.fts_note.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxNote.Text = "";
            //}

            if (erec_UgyUgyiratokSearch.fts_altalanos != null)
            {
                Altalanos_TextBox.Text = erec_UgyUgyiratokSearch.fts_altalanos.Filter;
            }
            else
            {
                Altalanos_TextBox.Text = "";
            }

            //LZS - BUG_7099
            //Megjegyzés mező kiiratása a kereső felületre.
            if (erec_UgyUgyiratokSearch.fts_note != null)
            {
                txtMegjegyzes.Text = erec_UgyUgyiratokSearch.fts_note.Filter;
            }
            else
            {
                txtMegjegyzes.Text = "";
            }
            #endregion fts

            // BLG_1421
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                UgyFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(KodTarak.UGY_FAJTAJA.KodcsoportKod,
                erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch != null ?
                erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.UgyFajta.Value
                : String.Empty,
                true, SearchHeader1.ErrorPanel);
            } else
            {
                UgyFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(KodTarak.UGY_FAJTAJA.KodcsoportKod, 
                    erec_UgyUgyiratokSearch.Ugy_Fajtaja.Value, true, SearchHeader1.ErrorPanel);
            }
            LezarasOka_KodtarakDropDownList.FillAndSetSelectedValue(kcs_LEZARAS_OKA, erec_UgyUgyiratokSearch.LezarasOka.Value, true, SearchHeader1.ErrorPanel);

            NemCsoporttagokkal_CheckBox.Checked = !erec_UgyUgyiratokSearch.Csoporttagokkal;
            CsakAktivIrat_CheckBox.Checked = erec_UgyUgyiratokSearch.CsakAktivIrat;

            if (erec_UgyUgyiratokSearch.NaponBelulNincsUjAlszam > -1)
            {
                NaponBelulNincsUjAlszam.Checked = true;
            }
            else
            {
                NaponBelulNincsUjAlszam.Checked = false;
            }
        }


    }


    private EREC_UgyUgyiratokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)SearchHeader1.TemplateObject;
        if (erec_UgyUgyiratokSearch == null)
        {
            erec_UgyUgyiratokSearch = GetDefaultSearchObject(); //new EREC_UgyUgyiratokSearch(true);
        }

        if (!String.IsNullOrEmpty(UgyDarab_IraIktatoKonyvek_DropDownList.SelectedValue))
        {
            UgyDarab_IraIktatoKonyvek_DropDownList.SetSearchObject(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
        }

        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Foszam);

        // Ügyfelelõs
        if (!String.IsNullOrEmpty(CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Csoport_Id_Ugyfelelos.Value = CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.Csoport_Id_Ugyfelelos.Operator = Query.Operators.equals;
        }

        // áthelyezve a WhereByManual-hoz
        //if (!String.IsNullOrEmpty(Ugy_Targy_TextBox.Text))
        //{
        //    //erec_UgyUgyiratokSearch.Targy.Value = Ugy_Targy_TextBox.Text;
        //    //erec_UgyUgyiratokSearch.Targy.Operator = Search.GetOperatorByLikeCharater(Ugy_Targy_TextBox.Text);

        //    string strContains = new FullTextSearch.SQLContainsCondition(Ugy_Targy_TextBox.Text).Normalized;
        //    if (!string.IsNullOrEmpty(erec_UgyUgyiratokSearch.WhereByManual))
        //    {
        //        erec_UgyUgyiratokSearch.WhereByManual += " and ";
        //    }
        //    erec_UgyUgyiratokSearch.WhereByManual += "contains( EREC_UgyUgyiratok.Targy , '" + strContains + "' )";
        //}
        //if (!String.IsNullOrEmpty(Ugy_IktatoszamKieg_KodtarakDropDownList.SelectedValue))
        //{
        //    erec_UgyUgyiratokSearch.IktatoszamKieg.Value = Ugy_IktatoszamKieg_KodtarakDropDownList.SelectedValue;
        //    erec_UgyUgyiratokSearch.IktatoszamKieg.Operator = Query.Operators.equals;
        //}

        if (!String.IsNullOrEmpty(Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue))
        {
            // BUG_9850 (GetDefaultSearchObject_EREC_UgyUgyiratokSearch miatt alapból nem jelennek meg a sztornózottak)
            if (Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue == KodTarak.UGYIRAT_ALLAPOT.Sztornozott)
            {
                CheckBoxSztorno.Checked = true;
                Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue = "";
            }
            else
            { 
                // CR#1548: Választható, hogy a továbbítás alatti adott állapotúak is bekerüljenek a listába, vagy sem
                if (cbExcludeTovabbitasAlatt.Checked == false)
                {
                    // A Tovabbitasalattallapot mezõben is kell keresni: (VAGY kapcsolat)

                    erec_UgyUgyiratokSearch.Allapot.Value = Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue;
                    erec_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.equals;
                    erec_UgyUgyiratokSearch.Allapot.Group = "531";
                    erec_UgyUgyiratokSearch.Allapot.GroupOperator = Query.Operators.or;

                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Value = erec_UgyUgyiratokSearch.Allapot.Value;
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.equals;
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Group = "531";
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.GroupOperator = Query.Operators.or;
                }
                else
                {

                    erec_UgyUgyiratokSearch.Allapot.Value = Ugy_Allapot_KodtarakDropDownListUgyUgyirat.SelectedValue;
                    erec_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.equals;
                    erec_UgyUgyiratokSearch.Allapot.Group = "0";
                    erec_UgyUgyiratokSearch.Allapot.GroupOperator = Query.Operators.and;

                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Value = "";
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Operator = "";
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Group = "0";
                    erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.GroupOperator = Query.Operators.and;
                }
            }
        }
        erec_UgyUgyiratokSearch.IsTovabbitasAlattAllapotIncluded = !cbExcludeTovabbitasAlatt.Checked;
        erec_UgyUgyiratokSearch.IsSzereltekExcluded = cbExcludeSzereltek.Checked;
        
        if (!String.IsNullOrEmpty(Ugy_Surgosseg_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratokSearch.Surgosseg.Value = Ugy_Surgosseg_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratokSearch.Surgosseg.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Ugy_IraIrattariTetelTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value = Ugy_IraIrattariTetelTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Ugytipus_DropDownList.SelectedValue))
        {
            erec_UgyUgyiratokSearch.UgyTipus.Value = Ugytipus_DropDownList.SelectedValue;
            erec_UgyUgyiratokSearch.UgyTipus.Operator = Query.Operators.equals;
        }

        // Ügyirat típusa
        if (!String.IsNullOrEmpty(Ugy_Jelleg_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratokSearch.Jelleg.Value = Ugy_Jelleg_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratokSearch.Jelleg.Operator = Query.Operators.equals;
        }
        // Elsõdleges adathordozó típusa
        if (!String.IsNullOrEmpty(Ugy_UgyintezesModja_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratokSearch.UgyintezesModja.Value = Ugy_UgyintezesModja_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratokSearch.UgyintezesModja.Operator = Query.Operators.equals;
        }

        if (cbNincsMegadvaIrattariHely.Checked)
        {
            erec_UgyUgyiratokSearch.IrattariHely.Operator = Query.Operators.isnull;
        }
        else
        if (!String.IsNullOrEmpty(Ugy_Irattarihely_TextBox.Text))
        {
            erec_UgyUgyiratokSearch.IrattariHely.Value = Ugy_Irattarihely_TextBox.Text;
            erec_UgyUgyiratokSearch.IrattariHely.Operator = Search.GetOperatorByLikeCharater(Ugy_Irattarihely_TextBox.Text);
        }
        if (!String.IsNullOrEmpty(Jovahagyo_CsoportTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Kovetkezo_Felelos_Id.Value = Jovahagyo_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.Kovetkezo_Felelos_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            //segédváltozó a kereséshez
            erec_UgyUgyiratokSearch.UgyUgyiratSearchUgyintezoGuid = Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value =
                Ugy_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Ugy_BarCode_TextBox.Text))
        {
            erec_UgyUgyiratokSearch.BARCODE.Value = Ugy_BarCode_TextBox.Text;
            erec_UgyUgyiratokSearch.BARCODE.Operator = Search.GetOperatorByLikeCharater(Ugy_BarCode_TextBox.Text);
        }
        if (!String.IsNullOrEmpty(Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value = Ugy_CsopId_Felelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value = Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
        }

        Ugy_PartnerId_Ugyindito_PartnerTextBox.SetStringAndIdSearchFields(erec_UgyUgyiratokSearch.Partner_Id_Ugyindito, erec_UgyUgyiratokSearch.NevSTR_Ugyindito);
        CimekTextBoxUgyindito.SetStringAndIdSearchFields(erec_UgyUgyiratokSearch.Cim_Id_Ugyindito, erec_UgyUgyiratokSearch.CimSTR_Ugyindito);

        Ugy_Hatarido_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Hatarido);

        // határidõ elõtt x nappal
        if (!String.IsNullOrEmpty(Ugy_HataridoElottXNappal_RequiredNumberBox.Text))
        {
            int dayDiff;
            if (Int32.TryParse(Ugy_HataridoElottXNappal_RequiredNumberBox.Text, out dayDiff))
            {
                erec_UgyUgyiratokSearch.Manual_HataridoElottXNappal.Value = DateTime.Today.ToShortDateString();
                erec_UgyUgyiratokSearch.Manual_HataridoElottXNappal.ValueTo = DateTime.Today.AddDays(dayDiff).ToShortDateString();
                erec_UgyUgyiratokSearch.Manual_HataridoElottXNappal.Operator = Query.Operators.between;

                // apró trükk a ReadableWhere szépítésére
                Ugy_HataridoElottXNappal_RequiredNumberBox.Text += " " + labelXNappal.Text;
            }
        }

        Ugy_SkontrobaDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.SkontrobaDat);

        Ugy_SkontroVege_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.SkontroVege);

        Ugy_IrattarbaVetelDat_DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.IrattarbaVetelDat);

        Ugy_ElintezesDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.ElintezesDat);

        Ugy_LezarasDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.LezarasDat);

        //Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
        //    erec_UgyUgyiratokSearch.MegorzesiIdoVege);

        Ugy_IrattarbaKuldes_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.IrattarbaKuldDatuma);

        Ugy_LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);

        if (IsKolcsonzottSearch)
        {
            disKolcsonzesDatuma.SetSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.KikerKezd);
            disKolcsonzesHatarido.SetSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.KikerVege);
            if (!disKolcsonzesDatuma.IsEmpty || !disKolcsonzesHatarido.IsEmpty)
            {
                erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
                erec_UgyUgyiratokSearch.Extended_EREC_IrattariKikeroSearch.Allapot.Operator = Query.Operators.equals;
            }
        }
        else
        {
            disKolcsonzesDatuma.Clear();
            disKolcsonzesHatarido.Clear();
        }

        if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue)
            || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvTol)
            || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvIg))
        {
            if (erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }
            //Search.ClearErvenyessegFilter(erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvKezd
            //    , erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvVege);

            if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue))
            {
                Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SetSearchObject(erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);
            }

            Kuld_EvIntervallum_SearchFormControl.SetSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);
        }

        Kuld_ErkeztetoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Erkezteto_Szam);

        if (!String.IsNullOrEmpty(Kuld_Ragszam_TextBox.Text))
        {
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.RagSzam.Value =
                Kuld_Ragszam_TextBox.Text;
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.RagSzam.Operator =
                Search.GetOperatorByLikeCharater(Kuld_Ragszam_TextBox.Text);
        }
        if (!String.IsNullOrEmpty(Kuld_HivatkozasiSzam_TextBox.Text))
        {
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Value =
                Kuld_HivatkozasiSzam_TextBox.Text;
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.HivatkozasiSzam.Operator =
                Search.GetOperatorByLikeCharater(Kuld_HivatkozasiSzam_TextBox.Text);
        }
        
        if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value =
                Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Operator =
                Query.Operators.equals;
        }
        else
        {
            if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text))
            {
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = Kuld_PartnerId_Bekuldo_PartnerTextBox.Text;
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text);
            }
        }

        Kuld_Beerkezesideje_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.BeerkezesIdeje);

        if (!String.IsNullOrEmpty(Kuld_CimId_CimekTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Value =
                Kuld_CimId_CimekTextBox.Id_HiddenField;
            erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Cim_Id.Operator =
                Query.Operators.equals;
        }
        else
        {
            if (!String.IsNullOrEmpty(Kuld_CimId_CimekTextBox.Text))
            {
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Value = Kuld_CimId_CimekTextBox.Text;
                erec_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.CimSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuld_CimId_CimekTextBox.Text);
            }
        }

        if (CheckBoxSajat.Checked)
        {
            // TODO: majd a felelõst nézni; egyelõre az õrzõt figyeljük
            erec_UgyUgyiratokSearch.Manual_Sajat.Value = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
            erec_UgyUgyiratokSearch.Manual_Sajat.Operator = Query.Operators.equals;

            // Nem kell:
            // (Iktatásra elõkészített, Irattárban õrzött,
            // Jegyzékre helyezett, Lezárt jegyzékben lévõ, Selejtezett,
            // Levéltárba adott, Irattárból elkért, Egedélyezett kikérõn lévõ, Sztornózott)
            erec_UgyUgyiratokSearch.Manual_Sajat_Allapot.Value = "'" + KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Selejtezett
                + "','" + KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                + "','" + KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                + (CheckBoxSztorno.Checked ? "" : "','" + KodTarak.UGYIRAT_ALLAPOT.Sztornozott)
                + "'";
            erec_UgyUgyiratokSearch.Manual_Sajat_Allapot.Operator = Contentum.eQuery.Query.Operators.notinner;
        }
        if (CheckBoxHataridoben.Checked)
        {
            erec_UgyUgyiratokSearch.Manual_Hataridoben.Value = Query.SQLFunction.getdate;
            erec_UgyUgyiratokSearch.Manual_Hataridoben.Operator = Query.Operators.greater;
        }
        if (CheckBoxHataridontul.Checked)
        {
            erec_UgyUgyiratokSearch.Manual_Hataridon_tul.Value = Query.SQLFunction.getdate;
            erec_UgyUgyiratokSearch.Manual_Hataridon_tul.Operator = Query.Operators.less;
        }
        
        MunkanapSearchControl.SetSearchObjectFields(erec_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch.Manual_IktatasDatuma_Munkanap);

        if (CheckBoxMunkaugyiratok.Checked)
        {
            // Munkaügyiratokra szûrés: Foszam is null
            erec_UgyUgyiratokSearch.Manual_Foszam_MunkaanyagFilter.Value = String.Empty;
            erec_UgyUgyiratokSearch.Manual_Foszam_MunkaanyagFilter.Operator = Query.Operators.isnull;
        }


        #region Manuálisan kezeltek (WhereByManual állítva)

        String WhereByManual = "";

        // Élõ == Iktatott vagy Szignált vagy Ügyintézés alatt vagy Skontróban
        if (CheckBoxElo.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += "(" + erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.Name
                + " in ("
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Iktatott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Szignalt
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontroban
                + "') or "
                + erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Name
                + " in ("
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Iktatott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Szignalt 
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontroban
                + "'"
                + ")) ";



            erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.Value = 
                "'" + KodTarak.UGYIRAT_ALLAPOT.Iktatott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Szignalt 
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontroban
                + "'";
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.Operator = Query.Operators.inner;
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.Group = "123";
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_Allapot.GroupOperator = Query.Operators.or;

            erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Value = 
                "'" + KodTarak.UGYIRAT_ALLAPOT.Iktatott
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Szignalt 
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontroban
                + "'";
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Operator = Query.Operators.inner;
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Group = "123";
            //erec_UgyUgyiratokSearch.Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.GroupOperator = Query.Operators.or;
        }
        if (CheckBoxLezart.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_Lezartak.Name + "="
                + "'" + KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart + "'";

            erec_UgyUgyiratokSearch.Manual_Lezartak.Value = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
            //erec_UgyUgyiratokSearch.Manual_Lezartak.Operator = Query.Operators.equals;
        }
       
        if (CheckBoxSkontroban.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_Skontroban.Name + "="
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Skontroban + "'";

            erec_UgyUgyiratokSearch.Manual_Skontroban.Value = KodTarak.UGYIRAT_ALLAPOT.Skontroban;
            //erec_UgyUgyiratokSearch.Manual_Skontroban.Operator = Query.Operators.equals;
        }
        if (CheckBoxSztorno.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_Sztornozottak.Name + "="
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Sztornozott + "'";

            //erec_UgyUgyiratokSearch.Manual_Sztornozottak.Value = KodTarak.UGYIRAT_ALLAPOT.Sztornozott;
            // CR#2034: Alapból szûrjük a sztornózottakat, ezért itt kivesszük a szûrést
            erec_UgyUgyiratokSearch.Manual_Sztornozottak.Value = KodTarak.UGYIRAT_ALLAPOT.Sztornozott; 
            erec_UgyUgyiratokSearch.Manual_Sztornozottak.Operator = String.Empty;
            //erec_UgyUgyiratokSearch.Manual_Sztornozottak.Operator = Query.Operators.equals;
        }
       
        if (CheckBoxElintezett.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_Elintezett.Name + "="
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Elintezett + "'";


            erec_UgyUgyiratokSearch.Manual_Elintezett.Value = KodTarak.UGYIRAT_ALLAPOT.Elintezett;
            //erec_UgyUgyiratokSearch.Manual_Elintezett.Operator = Query.Operators.equals;
        }
        if (CheckBoxSzerelt.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_Szereltek.Name + "="
                + "'" + KodTarak.UGYIRAT_ALLAPOT.Szerelt + "'";

            erec_UgyUgyiratokSearch.Manual_Szereltek.Value = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
            //erec_UgyUgyiratokSearch.Manual_Szereltek.Operator = Query.Operators.equals;
        }
        if (CheckBoxSzerelesreElokeszitett.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += erec_UgyUgyiratokSearch.Manual_SzerelesreElokeszitettek.Name + " > 0";

            erec_UgyUgyiratokSearch.Manual_SzerelesreElokeszitettek.Value = "1";
        }
        if (CheckBoxSzerelendok.Checked)
        {
            if (String.IsNullOrEmpty(WhereByManual))
            {
                WhereByManual += "(";
            }
            else
            {
                WhereByManual += " or ";
            }

            WhereByManual += "(" + erec_UgyUgyiratokSearch.Manual_Szerelendok.Name 
                + " <> " + "'" + KodTarak.UGYIRAT_ALLAPOT.Szerelt + "' and "
                + erec_UgyUgyiratokSearch.UgyUgyirat_Id_Szulo.Name + " is not null)";

            erec_UgyUgyiratokSearch.Manual_Szerelendok.Value = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
        }
        // WhereByManual lezárása:
        if (!string.IsNullOrEmpty(WhereByManual))
        {
            WhereByManual += ") ";
        }

        erec_UgyUgyiratokSearch.WhereByManual = WhereByManual;

        #region FTS
        #region standard objektumfüggõ tárgyszavak
        // ügyirattól örökölt
        try
        {
            //FullTextSearchTree FTSTree = sotUgyiratTargyszavak.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);

            ////if (FTSTree != null)
            ////{
            //    erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto = FTSTree;
            //    //erec_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter = FTSTree.TransformToFTSContainsConditions();
            ////}

            erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto = sotUgyiratTargyszavak.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);

        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion standard objektumfüggõ tárgyszavak

        #region egyéb tárgyszavak
        try
        {           
            erec_UgyUgyiratokSearch.fts_tree_ugyirat_egyeb = tszmsEgyebTargyszavak.BuildFTSTree(SearchHeader1.ErrorPanel);
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion egyéb tárgyszavak

        #region merge standard és egyéb tárgyszavak
        try
        {
            FullTextSearchTree mergedTree = FullTextSearchTree.MergeFTSTrees(erec_UgyUgyiratokSearch.fts_tree_ugyirat_auto
                , erec_UgyUgyiratokSearch.fts_tree_ugyirat_egyeb, FullTextSearchTree.Operator.Intersect);

            if (mergedTree != null)
            {
                erec_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter = mergedTree.TransformToFTSContainsConditions();
            }
            else
            {
                erec_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter = "";
            }
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion merge standard és egyéb tárgyszavak

        if (!String.IsNullOrEmpty(Ugy_Targy_TextBox.Text))
        {
            //erec_UgyUgyiratokSearch.fts_targy = new FullTextSearchField();
            //erec_UgyUgyiratokSearch.fts_targy.Filter = Ugy_Targy_TextBox.Text;

            erec_UgyUgyiratokSearch.Targy.Value = Ugy_Targy_TextBox.Text;
            erec_UgyUgyiratokSearch.Targy.Operator =  Query.Operators.contains; //Search.GetOperatorByLikeCharater(Ugy_Targy_TextBox.Text); // BUG_3944 kapcsán visszarakva

            //string strContains = new SQLContainsCondition(Ugy_Targy_TextBox.Text).Normalized;
            //if (!string.IsNullOrEmpty(erec_UgyUgyiratokSearch.WhereByManual))
            //{
            //    erec_UgyUgyiratokSearch.WhereByManual += " and ";
            //}
            //erec_UgyUgyiratokSearch.WhereByManual += "contains( EREC_UgyUgyiratok.Targy , '" + strContains + "' )";
        }

        //string strObjektumTargyszavai = ""; // kiegészítõ feltétel az EREC_ObjektumTargyszavai táblára
        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxTargyszo.Text))
        //{
        //    erec_UgyUgyiratokSearch.fts_targyszavak = new FullTextSearchField();
        //    erec_UgyUgyiratokSearch.fts_targyszavak.Filter = Targyszavak_TextBoxTargyszo.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxTargyszo.Text).Normalized;
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Targyszo, '" + strContains + "' )";

        //}

        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxErtek.Text))
        //{
        //    erec_UgyUgyiratokSearch.fts_ertek = new FullTextSearchField();
        //    erec_UgyUgyiratokSearch.fts_ertek.Filter = Targyszavak_TextBoxErtek.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxErtek.Text).Normalized;
        //    if (!string.IsNullOrEmpty(strObjektumTargyszavai))
        //    {
        //        strObjektumTargyszavai += " and ";
        //    }
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Ertek, '" + strContains + "' )";

        //}

        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxNote.Text))
        //{
        //    erec_UgyUgyiratokSearch.fts_note = new FullTextSearchField();
        //    erec_UgyUgyiratokSearch.fts_note.Filter = Targyszavak_TextBoxNote.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxNote.Text).Normalized;
        //    if (!string.IsNullOrEmpty(strObjektumTargyszavai))
        //    {
        //        strObjektumTargyszavai += " and ";
        //    }
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Note, '" + strContains + "' )";

        //}

        if (!String.IsNullOrEmpty(Altalanos_TextBox.Text))
        {
            erec_UgyUgyiratokSearch.fts_altalanos = new FullTextSearchField();
            erec_UgyUgyiratokSearch.fts_altalanos.Filter = Altalanos_TextBox.Text;
        }

        //LZS - BUG_7099
        //Megjegyzés szöveg beállítása a search object-nek.
        if (!String.IsNullOrEmpty(txtMegjegyzes.Text))
        {
            erec_UgyUgyiratokSearch.fts_note = new FullTextSearchField();
            erec_UgyUgyiratokSearch.fts_note.Filter = txtMegjegyzes.Text;
        }

        //if (!String.IsNullOrEmpty(strObjektumTargyszavai))
        //{
        //    strObjektumTargyszavai = "SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE " + strObjektumTargyszavai;

        //    if (!String.IsNullOrEmpty(erec_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter))
        //    {
        //        strObjektumTargyszavai = " INTERSECT " + strObjektumTargyszavai;
        //    }

        //    erec_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter += strObjektumTargyszavai;
        //}
        #endregion

        #endregion

        Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        if (!String.IsNullOrEmpty(UgyFajtaja_KodtarakDropDownList.SelectedValue))
        {
            // BLG_1421
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                if (erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch == null)
                {
                    erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();
                    erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.ErvKezd.Clear();
                    erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.ErvVege.Clear();
                }
                erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.UgyFajta.Value = UgyFajtaja_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.UgyFajta.Operator = Query.Operators.equals;
            }
            else
            {
                erec_UgyUgyiratokSearch.Ugy_Fajtaja.Value = UgyFajtaja_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratokSearch.Ugy_Fajtaja.Operator = Query.Operators.equals;
            }
        }

        if (!String.IsNullOrEmpty(LezarasOka_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratokSearch.LezarasOka.Value = LezarasOka_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratokSearch.LezarasOka.Operator = Query.Operators.equals;
        }

        erec_UgyUgyiratokSearch.Csoporttagokkal = !NemCsoporttagokkal_CheckBox.Checked;
        erec_UgyUgyiratokSearch.CsakAktivIrat = CsakAktivIrat_CheckBox.Checked;

        if (NaponBelulNincsUjAlszam.Checked)
        {
            erec_UgyUgyiratokSearch.NaponBelulNincsUjAlszam = 6;
        }
        else
        {
            erec_UgyUgyiratokSearch.NaponBelulNincsUjAlszam = -1;
        }

        return erec_UgyUgyiratokSearch;
    }



    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added: Találati listák mentése
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_UgyUgyiratokSearch searchObject = SetSearchObjectFromComponents();

            //Keresési feltételek elmentése kiiratható formában
            searchObject.ReadableWhere = Search.GetReadableWhere(Form);

            //EREC_UgyUgyiratokSearch defaultSearchObject = GetDefaultSearchObject();

            //if (Search.IsIdentical(searchObject, defaultSearchObject, searchObject.WhereByManual, defaultSearchObject.WhereByManual)
            //    && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch,defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
            //    && ((searchObject.Extended_EREC_KuldKuldemenyekSearch == null && defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch == null) || Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch))
            //    && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch,defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
            //    && Search.IsIdentical(searchObject.Extended_EREC_IraIratokSearch,defaultSearchObject.Extended_EREC_IraIratokSearch,searchObject.Extended_EREC_IraIratokSearch.WhereByManual,defaultSearchObject.Extended_EREC_IraIratokSearch.WhereByManual)
            //    && Search.IsIdentical(searchObject.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch, searchObject.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual)
            //    && ((searchObject.fts_targy == null)
            //    || String.IsNullOrEmpty(searchObject.fts_targy.Filter))
            //    && (searchObject.fts_tree_ugyirat_auto == null) // || searchObject.fts_tree_ugyirat_auto.LeafCount == 0)
            //    && String.IsNullOrEmpty(searchObject.ObjektumTargyszavai_ObjIdFilter) 
            //    && ((searchObject.fts_targyszavak == null)
            //    || String.IsNullOrEmpty(searchObject.fts_targyszavak.Filter)) // idáig nem szabad eljutnia
            //    && (searchObject.fts_altalanos == null)
            //    )
            if (Search.IsDefaultSearchObject(_type, searchObject, GetDefaultSearchObject())) // összehasonlítás az "üres" default objektummal
            {
                // default searchobject
                switch (Startup)
                {                    
                    case Constants.Startup.FromAtmenetiIrattar:                    
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch);
                        break;
                    case Constants.Startup.FromKozpontiIrattar:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
                        break;
                    case Constants.Startup.Skontro:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SkontroSearch);
                        break;
                    case Constants.Startup.FromMunkanaplo:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunaknaploSearch);
                        break;
                    case Constants.Startup.FromSkontroIrattar:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch);
                        break;
                    // CR3289 Vezetõi panel kezelés
                    case Constants.Startup.FromVezetoiPanel_Ugyirat:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                        break;
                    default:
                        Search.RemoveSearchObjectFromSession(Page, _type);
                        break;
                }  
            }
            else
            {
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);

                switch (Startup)
                {
                    case Constants.Startup.FromAtmenetiIrattar:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch);
                        break;
                    case Constants.Startup.FromKozpontiIrattar:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
                        break;
                    case Constants.Startup.Skontro:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.SkontroSearch);
                        break;
                    case Constants.Startup.FromMunkanaplo:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.MunaknaploSearch);
                        break;
                    case Constants.Startup.FromSkontroIrattar:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch);
                        break;
                    // CR3289 Vezetõi Panel kezelés
                    case Constants.Startup.FromVezetoiPanel_Ugyirat:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                        break;
                    default:
                        Search.SetSearchObject(Page, searchObject);
                        break;
                }     
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            #region panelek eltüntetése, hogy ne küldjünk vissza feleslegesen adatokat            
            AltalanosKeresesPanel.Visible = false;
            StandardUgyiratTargyszavakPanel.Visible = false;
            //EFormPanelFTS.Visible = false;
            EFormPanelOTMultiSearch.Visible = false;
            EFormPanel3.Visible = false;
            Datumok_eFormPanel.Visible = false;
            Kuldemeny_EFormPanel.Visible = false;
            CheckBox_EFormPanel.Visible = false;
            #endregion
        }

    }

    private EREC_UgyUgyiratokSearch GetDefaultSearchObject()
    {
        //return new EREC_UgyUgyiratokSearch(true);

        EREC_UgyUgyiratokSearch defaultSearchObject = (EREC_UgyUgyiratokSearch)Search.GetDefaultSearchObject(_type);

        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
            case Constants.Startup.FromKozpontiIrattar:
            case Constants.Startup.Skontro:
            case Constants.Startup.FromSkontroIrattar:
            case Constants.Startup.FromMunkanaplo:
                defaultSearchObject.CsakAktivIrat = false;
                break;
            default:
                break;
        }

        // BUG_3842: skontróban ne jelenjenek meg a szereltek
        if (Startup == Constants.Startup.Skontro
            || Startup == Constants.Startup.FromSkontroIrattar)
        {
            defaultSearchObject.IsSzereltekExcluded = true;
        }

        return defaultSearchObject;
    }
}

