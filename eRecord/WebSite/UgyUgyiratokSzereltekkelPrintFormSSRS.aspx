﻿<%@ Page Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokSzereltekkelPrintFormSSRS.aspx.cs" Inherits="UgyUgyiratokSzereltekkelPrintFormSSRS"
    Title="Irattározott ügyiratok" %>

<%@ MasterType VirtualPath="~/PrintFormMasterPage.master" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyUgyiratokSzereltekkelPrintFormSSRSHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanelKeresesiFeltetelek" runat="server" Visible="true">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelEvStar" runat="server" CssClass="ReqStar" Text="*" />
                                    <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" nowrap="nowrap">
                                    <uc4:RequiredNumberBox ID="Ev_RequiredNumberBox" runat="server" LabelRequiredIndicatorID="labelEvStar"
                                        Validate="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelIktatokonyv" runat="server" Text="Iktatókönyv:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" runat="server">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelAllapot" runat="server" Text="Állapot:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true">
                                    </uc2:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                </td>
                                <td class="mrUrlapMezo" nowrap="nowrap">
                                    <asp:CheckBox ID="cbAlszamNelkuliekIs" Text="0 iratszámú ügyiratokkal" Checked="true"
                                        runat="server" AutoPostBack="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                </td>
                                <td class="mrUrlapMezo">
                                    <div class="DisableWrap">
                                        <asp:ImageButton ID="ImageButton_Refresh" runat="server" ImageUrl="./images/hu/muvgomb/refresh.jpg"
                                            CssClass="highlightit" AlternateText="Riport frissítése" />
                                        <asp:ImageButton ID="ImageButton_Back" runat="server" ImageUrl="./images/hu/ovalgomb/vissza.jpg"
                                            CssClass="highlightit" AlternateText="Vissza" CommandName="Back" CausesValidation="False" />
                                    </div>
                                </td>
                                <%--                                <td style="width: 100%;">
                                </td>--%>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanelReport" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="1200px" Height="500px" EnableTelemetry="false">
                                    <ServerReport ReportPath = "/Lekerdezes/UgyiratokListajaSzereltekkel" />
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
</asp:Content>
