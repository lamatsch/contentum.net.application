﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UgyUgyiratokSzereltekkelPrintFormSSRS : Contentum.eUtility.UI.PageBase
{
    private const string kcs_UGYIRAT_ALLAPOT = "UGYIRAT_ALLAPOT";

    private String mode = null;

    #region Utility

    private String GetUGYHOLFromAllapot(string allapot)
    {
        if (String.IsNullOrEmpty(allapot)) return String.Empty;

        switch (Allapot_KodtarakDropDownList.SelectedValue)
        {
            case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                return Contentum.eUtility.Constants.MIG_IratHelye.Irattarban;
            // ...
            default:
                return Contentum.eUtility.Constants.MIG_IratHelye.Osztalyon;
        }

    }

    private void FillIktatoKonyvekList()
    {
        String selectedValue = IraIktatoKonyvekDropDownList1.SelectedValue;

        IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        //if (EvIntervallum_SearchFormControl1.EvTol_Integer > 0)
        //{
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                    , false, null, null
                    , false, false, FormHeader1.ErrorPanel);
        //}

        if (!String.IsNullOrEmpty(selectedValue))
        {
            ListItem liSelected = IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(selectedValue);
            if (liSelected != null)
            {
                liSelected.Selected = true;
            }
        }
    }
    #endregion Utility

    private int migralasEve = 0;

    private bool IsMigralt
    {
        get
        {
            int ev;
            Int32.TryParse(Ev_RequiredNumberBox.Text, out ev);
            return (this.migralasEve > 0 && ev <= this.migralasEve); }
    }

    private const String DefaultOrderBy_EREC = "EREC_IraIktatokonyvek.Iktatohely, EREC_IraIktatokonyvek.Ev DESC, EREC_UgyUgyiratok.Foszam DESC";
    private const String DefaultOrderBy_MIG = "MIG_Foszam.EdokSav, MIG_Foszam.UI_YEAR DESC, MIG_Foszam.UI_NUM DESC";

    private Result resultOfDataQuery_EREC = null;
    protected Result ResultOfDataQuery_EREC
    {
        get
        {
            if (resultOfDataQuery_EREC == null)
            {
                resultOfDataQuery_EREC = GetData_EREC();

                if (resultOfDataQuery_EREC.IsError)
                {
                    throw new Exception(Contentum.eUtility.ResultError.GetErrorMessageFromResultObject(resultOfDataQuery_EREC));
                }
            }
            return resultOfDataQuery_EREC;
        }
    }

    private Result resultOfDataQuery_MIG = null;
    protected Result ResultOfDataQuery_MIG
    {
        get
        {
            if (resultOfDataQuery_MIG == null)
            {
                resultOfDataQuery_MIG = GetData_MIG();

                if (resultOfDataQuery_MIG.IsError)
                {
                    throw new Exception(Contentum.eUtility.ResultError.GetErrorMessageFromResultObject(resultOfDataQuery_MIG));
                }
            }
            return resultOfDataQuery_MIG;
        }
    }

    protected Result GetData_EREC()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Fake = true;

        EREC_UgyUgyiratokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = SearchObject_EREC;
        return service.GetAllWithExtensionAndSzereltek(execParam, erec_UgyUgyiratokSearch);
    }

    protected Result GetData_MIG()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Fake = true;

        Contentum.eMigration.Service.MIG_FoszamService MIG_service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();

        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch mig_FoszamSearch = SearchObject_MIG;
        return MIG_service.GetAllWithExtensionAndSzereltek(execParam, mig_FoszamSearch);
    }

    protected Contentum.eUtility.ReportViewerCredentials GetReportViewCredentials()
    {
        string app_server_name = System.Environment.MachineName;
        Contentum.eUtility.ReportViewerCredentials rvc = new Contentum.eUtility.ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

        return rvc;
    }

    protected void InitializeReportViewer()
    {
        if (!IsPostBack)
        {
            Contentum.eUtility.ReportViewerCredentials rvc = GetReportViewCredentials();

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;
        }
    }

    protected void SetReportViewer()
    {
        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        //ReportViewer1.ServerReport.Refresh();
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    SetReportParameter(ReportParameters[i]);
                }
            }
        }
        return ReportParameters;
    }

    protected void SetReportParameter(ReportParameter rp)
    {
        if (rp != null && !String.IsNullOrEmpty(rp.Name) && ResultOfDataQuery_EREC != null && ResultOfDataQuery_MIG != null)
        {
            switch (rp.Name)
            {
                case "MIGWhere":
                    rp.Values.Add(ResultOfDataQuery_MIG.SqlCommand.GetParamValue("@Where"));
                    break;
                case "MIGOrderBy":
                    rp.Values.Add(ResultOfDataQuery_MIG.SqlCommand.GetParamValue("@OrderBy"));
                    break;
                case "MIGTopRow":
                    rp.Values.Add(ResultOfDataQuery_MIG.SqlCommand.GetParamValue("@TopRow"));
                    break;

                case "Where_IktatoKonyvek":
                    rp.Values.Add(ResultOfDataQuery_EREC.SqlCommand.GetParamValue("@Where_IktatoKonyvek"));
                    break;
                case "Where":
                    rp.Values.Add(ResultOfDataQuery_EREC.SqlCommand.GetParamValue("@Where"));
                    break;
                case "OrderBy":
                    rp.Values.Add(ResultOfDataQuery_EREC.SqlCommand.GetParamValue("@OrderBy"));
                    break;
                case "TopRow":
                    rp.Values.Add(ResultOfDataQuery_EREC.SqlCommand.GetParamValue("@TopRow"));
                    break;

                case "ExecutorUserId":
                    rp.Values.Add(FelhasznaloProfil.FelhasznaloId(Page));
                    break;
                case "FelhasznaloSzervezet_Id":
                    rp.Values.Add(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                    break;

                case "ReadableWhere":
                    rp.Values.Add(Search.GetReadableWhere(Form));
                    break;
            }
        }
    }

    private Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch searchObject_EREC = null;
    protected Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch SearchObject_EREC
    {
        get
        {
            if (searchObject_EREC == null)
            {
                searchObject_EREC = GetSearchObject_EREC();
            }
            return searchObject_EREC;
        }
    }

    private Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch searchObject_MIG = null;
    protected Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch SearchObject_MIG
    {
        get
        {
            if (searchObject_MIG == null)
            {
                searchObject_MIG = GetSearchObject_MIG();
            }
            return searchObject_MIG;
        }
    }

    protected EREC_UgyUgyiratokSearch GetSearchObject_EREC()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);

        if (IsMigralt)
        {
            // nem kell érvényességi feltétel
            erec_UgyUgyiratokSearch.ErvKezd.Clear();
            erec_UgyUgyiratokSearch.ErvVege.Clear();

            erec_UgyUgyiratokSearch.WhereByManual = " 1=0 "; // lehetetlen feltétel
            erec_UgyUgyiratokSearch.TopRow = 1;
        }
        else
        {
            string Iktatohely = String.Empty;

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                Iktatohely = IraIktatoKonyvekDropDownList1.SelectedValue.Split(new char[] { '|' })[0];   // itt biztosan van legalább 0. elem
            }

            erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(Ev_RequiredNumberBox.Text);//EvIntervallum_SearchFormControl1.EvTol;
            erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Iktato); // "I"

            if (!String.IsNullOrEmpty(Iktatohely))
            {
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(Iktatohely);
            }

            if (!cbAlszamNelkuliekIs.Checked)
            {
                erec_UgyUgyiratokSearch.IratSzam.Greater("0");
            }

            if (!String.IsNullOrEmpty(Allapot_KodtarakDropDownList.SelectedValue))
            {
                erec_UgyUgyiratokSearch.Allapot.Value = Allapot_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.equals;
            }

            erec_UgyUgyiratokSearch.TopRow = 0;
        }
        
        erec_UgyUgyiratokSearch.OrderBy = DefaultOrderBy_EREC;
        return erec_UgyUgyiratokSearch;
    }

    protected Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch GetSearchObject_MIG()
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch mig_FoszamSearch = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();

        if (IsMigralt)
        {
            string Iktatohely = String.Empty;

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                Iktatohely = IraIktatoKonyvekDropDownList1.SelectedValue.Split(new char[] { '|' })[0];   // itt biztosan van legalább 0. elem
            }

            mig_FoszamSearch.UI_YEAR.Filter(Ev_RequiredNumberBox.Text);

            if (!String.IsNullOrEmpty(Iktatohely))
            {
                string sav = IktatoKonyvek.GetSavFromIktatohely(Iktatohely);
                mig_FoszamSearch.EdokSav.Filter(sav);
            }

            string ugyhol = GetUGYHOLFromAllapot(Allapot_KodtarakDropDownList.SelectedValue);
            if (!String.IsNullOrEmpty(ugyhol))
            {

                mig_FoszamSearch.UGYHOL.Filter(ugyhol);
            }

            mig_FoszamSearch.TopRow = 0;
        }
        else
        {
            mig_FoszamSearch.WhereByManual = " 1=0 "; // lehetetlen feltétel
            mig_FoszamSearch.OrderBy = DefaultOrderBy_MIG;
            mig_FoszamSearch.TopRow = 1;
        }

        mig_FoszamSearch.OrderBy = DefaultOrderBy_MIG;

        return mig_FoszamSearch;
    }


    #region Page
    protected void Page_Init(object sender, EventArgs e)
    {
        mode = Request.QueryString.Get(QueryStringVars.Mode);

        if (mode == "KozpontiIrattar")
        {
            Allapot_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYIRAT_ALLAPOT,
            KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott
            , false, EErrorPanel1);
            Allapot_KodtarakDropDownList.ReadOnly = true;
        }
        else
        {
            Allapot_KodtarakDropDownList.FillAndSetEmptyValue(kcs_UGYIRAT_ALLAPOT, EErrorPanel1);
            Allapot_KodtarakDropDownList.ReadOnly = false;
        }

        migralasEve = Contentum.eUtility.Rendszerparameterek.GetInt(Page, Contentum.eUtility.Rendszerparameterek.MIGRALAS_EVE);
        if (!IsPostBack)
        {
            FillIktatoKonyvekList();
        }

        ImageButton_Back.Click += new ImageClickEventHandler(ButtonsClick);
        ImageButton_Refresh.Click += new ImageClickEventHandler(ButtonsClick);

        InitializeReportViewer();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewer1.ShowReportBody = false;
        }
        else
        {
            ReportViewer1.ShowReportBody = true;
        }

        if (IsPostBack)
        {
            try
            {
                SetReportViewer();
            }
            catch (Exception ex)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1
                , Resources.Error.DefaultErrorHeader, ex.Message);
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Master.CloseButtonVisible = false;
    }

    #endregion Page

    protected void ButtonsClick(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        if (btn.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
        else
        {
            // csak postback
        }
    }

}
