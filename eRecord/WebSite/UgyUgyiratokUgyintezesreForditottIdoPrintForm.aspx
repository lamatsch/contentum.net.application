<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyUgyiratokUgyintezesreForditottIdoPrintForm.aspx.cs" Inherits="UgyUgyiratokUgyintezesreForditottIdoPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyUgyiratokUgyintezesreForditottIdoFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label15" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelLetrehozasIdopontja" runat="server" Text="L�trehoz�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="true" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                       </tr>
                        <tr class="urlapSor">
                        <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelElintezesIdopontja" runat="server" Text="�gyirat elint�z�si id�pontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl2"
                                    runat="server" Validate="true" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                        <td>
                            </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
