﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class UgyUgyiratokUgyintezesreForditottIdoPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);
        DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_UgyUgyiratokSearch.ElintezesDat);

        return erec_UgyUgyiratokSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();
            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            Query query = new Query();
            query.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            //OrderBy törlése a Session-ből
            if (Session["OrderBy"] != null)
            {
                Session["OrderBy"] = null;
            }

            Session["Where"] = query.Where;
            Session["OrderBy"] = erec_UgyUgyiratokSearch.OrderBy;

            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "Megjegyzes=" + Megjegyzes_TextBox.Text + '&' + "DateFrom=" + DatumIntervallum_SearchCalendarControl1.DatumKezd + '&' + "DateTo=" + DatumIntervallum_SearchCalendarControl1.DatumVege;

            //Meghívjuk az HatralekListaSSRSPrintForm.aspx nyomtatóoldalt.
            string js = "javascript:window.open('UgyintezesreForditottIdoSSRSPrintForm.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyintezesreForditottIdoSSRSPrintForm", js, true);
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
