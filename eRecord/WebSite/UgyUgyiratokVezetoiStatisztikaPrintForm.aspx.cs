﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class UgyUgyiratokVezetoiStatisztikaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {

        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            string kezdDatum = DatumIntervallum_SearchCalendarControl1.DatumKezd;
            string vegeDatum = DatumIntervallum_SearchCalendarControl1.DatumVege;

            if (String.IsNullOrEmpty(kezdDatum))
            {
                kezdDatum = System.DateTime.Today.ToString();
            }

            if (String.IsNullOrEmpty(vegeDatum))
            {
                vegeDatum = System.DateTime.Now.ToString();
            }

            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            //string orderby = erec_UgyUgyiratokSearch.OrderBy;
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            Query query = new Query();
            query.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            Session["Where"] = query.Where + erec_UgyUgyiratokSearch.WhereByManual;

            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "DateFrom=" + kezdDatum + '&' + "DateTo=" + vegeDatum;
            //Meghívjuk az HatralekListaSSRSPrintForm.aspx nyomtatóoldalt.
            string js = "javascript:window.open('VezetoiStatisztikaSSRSPrintForm.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "VezetoiStatisztikaSSRSPrintForm", js, true);

        }

        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
