<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="UgyiratElintezettForm.aspx.cs" Inherits="UgyiratElintezettForm" Title="Untitled Page" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp"%> 

<%@ Register Src="~/eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
        .mrUrlapCaption_short {
            text-align: left;
            width: 100px;
            min-width: 100px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyiratElintezettFormHeaderTitle %>" />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <uc:UgyiratMasterAdatok ID="UgyiratAdatok" runat="server" />
                    <br />
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label7" runat="server" Text="�gyirat elint�z�si id�pontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" >
                                <uc5:CalendarControl ID="ElintezesDat_CalendarControl" runat="server" Validate="true" TimeVisible="true" />
                            </td>
                        </tr>
                        <%if (IsElintezetteNyilvanitasEnabled)
                          { %>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label5" runat="server" Text="Elint�z�s m�dja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="ElintezesMod_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <%} %>                               
                    </table>
                    </eUI:eFormPanel>
                    <br /> 
                    <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" ValidateIfFeladatIsNotEmpty="true" SmallSize="true"/>                   
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
        
</asp:Content>

