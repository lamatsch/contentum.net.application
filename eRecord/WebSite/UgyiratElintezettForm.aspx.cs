using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class UgyiratElintezettForm : Contentum.eUtility.UI.PageBase
{
    private string Id = String.Empty;
    private string Command = String.Empty;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    
    private const String FunkcioKod_UgyiratElintezes = "UgyiratElintezes";
    private const String FunkcioKod_UgyiratElintezesJovahagyas = "UgyiratElintezesJovahagyas";
    private const String FunkcioKod_UgyiratElintezesVisszautasitas = "UgyiratElintezesVisszautasitas";

    private const String kcs_ELINTEZESMOD = "ELINTEZESMOD";

    private bool IsUgyiratElintezesJovahagyas
    {
        get
        {
            object o = ViewState["IsUgyiratElintezesJovahagyas"];
            if(o!=null)
                return (bool)o;

            return false;
        }
        set
        {
            ViewState["IsUgyiratElintezesJovahagyas"] = value;
        }
    }

    /// <summary>
    /// CERTOP: hagyom�nyos elint�zett� nyilv�n�t�s enged�lyez�se
    /// </summary>
    protected bool IsElintezetteNyilvanitasEnabled
    {
        get
        {
            return Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.ELINTEZETTE_NYILVANITAS_ENABLED, false);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Id = Request.QueryString.Get(QueryStringVars.Id);
        Command = Request.QueryString.Get(CommandName.Command);

        if(String.IsNullOrEmpty(Command))
            Command = CommandName.Modify;

        pageView = new PageView(Page, ViewState);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = Command;
     
        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratElintezes);
                break;
        }

        FeljegyzesPanel.ErrorPanel = FormHeader1.ErrorPanel;

        if (IsElintezetteNyilvanitasEnabled)
        {
            ElintezesMod_KodtarakDropDownList.FillDropDownList(kcs_ELINTEZESMOD, true, FormHeader1.ErrorPanel);
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (FormHeader1.ErrorPanel.Visible)
            FormHeader1.ErrorPanel.Visible = false;

        FormFooter1.ButtonsClick += new
          CommandEventHandler(FormFooter1_ButtonsClick);

        if (Command == CommandName.Modify)
        {
            if (!IsPostBack)
            {
                EREC_UgyUgyiratok erec_UgyUgyiratok;
                if (GetBusinessObject(out erec_UgyUgyiratok))
                {
                    LoadComponentsFromBusinessObject(erec_UgyUgyiratok);
                    SetFormHeader(erec_UgyUgyiratok.Azonosito);
                }
            }
        }

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        SetComponentsView();
        SetFormFooterButtons();
    }

    private bool GetBusinessObject(out EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        erec_UgyUgyirat = null;

        if (String.IsNullOrEmpty(Id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = Id;

        Result result = service.Get(execParam);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }

        erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

        return true;
    }

    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        //kiv�lasztott �gyirat adatainak megjelen�t�se
        UgyiratAdatok.SetUgyiratMasterAdatokByBusinessObject(erec_UgyUgyiratok, FormHeader1.ErrorPanel, null);

        IsUgyiratElintezesJovahagyas = (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa);

        if (!IsUgyiratElintezesJovahagyas)
        {
            ElintezesDat_CalendarControl.Text = DateTime.Now.ToString();
        }
        else
        {
            ElintezesDat_CalendarControl.Text = erec_UgyUgyiratok.ElintezesDat;
        }
        
    }

    private EREC_UgyUgyiratok GetBusinessObjectFromComponents()
    {
        EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
        erec_UgyUgyirat.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.SetValueAll(false);

        erec_UgyUgyirat.ElintezesDat = ElintezesDat_CalendarControl.Text;
        erec_UgyUgyirat.Updated.ElintezesDat = pageView.GetUpdatedByView(ElintezesDat_CalendarControl);

        if (IsElintezetteNyilvanitasEnabled)
        {
            erec_UgyUgyirat.ElintezesMod = ElintezesMod_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyirat.Updated.ElintezesMod = pageView.GetUpdatedByView(ElintezesMod_KodtarakDropDownList);
        }

        return erec_UgyUgyirat;

    }

    private void SetFormHeader(string Azonosito)
    {
        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = Azonosito + " " + Resources.Form.UgyiratElintezettFormHeaderTitle;
    }

    private void SetFormFooterButtons()
    {
        if (IsUgyiratElintezesJovahagyas)
        {
            FormFooter1.ImageButton_Save.Visible = false;
            FormFooter1.ImageButton_Accept.Visible = true;
            FormFooter1.ImageButton_Accept.CommandName = CommandName.ElintezetteNyilvanitasJovahagyasa;
            FormFooter1.ImageButton_Accept.AlternateText = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
            FormFooter1.ImageButton_Accept.ToolTip = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
            FormFooter1.ImageButton_Decline.Visible = true;
            FormFooter1.ImageButton_Decline.CommandName = CommandName.ElintezetteNyilvanitasVisszautasitasa;
            FormFooter1.ImageButton_Decline.AlternateText = Resources.Buttons.ElintezetteNyilvanitasVisszautasitasa;
            FormFooter1.ImageButton_Decline.ToolTip = Resources.Buttons.ElintezetteNyilvanitasVisszautasitasa;
        }
    }

    private void SetComponentsView()
    {
        if (IsUgyiratElintezesJovahagyas)
        {
            ElintezesDat_CalendarControl.ReadOnly = true;
        }
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            FormFooter1.SaveEnabled = false;

        }
    }

    private void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (String.IsNullOrEmpty(Id))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return;
        }

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = Id;

        EREC_UgyUgyiratok erec_UgyUgyirat = GetBusinessObjectFromComponents();

        EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();
        Result result = new Result();

        switch (e.CommandName)
        {
            case CommandName.Save:
                if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratElintezes))
                {
                    result = service.ElintezetteNyilvanitas(execParam, erec_UgyUgyirat.ElintezesDat, erec_UgyUgyirat.ElintezesMod, erec_HataridosFeladatok);
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                    return;
                }
                break;
            case CommandName.ElintezetteNyilvanitasJovahagyasa:
                if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratElintezesJovahagyas))
                {
                    result = service.ElintezetteNyilvanitasJovahagyasa(execParam, erec_HataridosFeladatok);
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                    return;
                }
                break;
            case CommandName.ElintezetteNyilvanitasVisszautasitasa:
                if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratElintezesVisszautasitas))
                {
                    result = service.ElintezetteNyilvanitasVisszautasitasa(execParam, erec_HataridosFeladatok);
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                    return;
                }
                break;
        }

        if (!result.IsError)
        {
            JavaScripts.RegisterSelectedRecordIdToParent(Page, Id);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);

            //Az �gyirat statisztika nincs kit�ltve
            if (result.ErrorCode == "52254")
            {
                string url = String.Format("~/UgyUgyiratokForm.aspx?Command=Modify&Id={0}&selectedTab=TabUgyiratJellemzoPanel", Id);
                url = this.ResolveClientUrl(url);
                string onclick = JavaScripts.SetOnClientClick(url, null, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, EFormPanel1.ClientID);
                string link = "<a href=\"javascript:void(0)\" style=\"text-decoration:underline; font-weight:bold;\" onclick=\"" + onclick + ";\">" + "�gyirat statisztika kit�lt�se" + "</a>";
                FormHeader1.ErrorPanel.Body += "<div style=\"padding:3px;\">" + link + "<div>";
            }
        }
    }

}
