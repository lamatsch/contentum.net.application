﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="UgyiratElintezettTomegesForm.aspx.cs" Inherits="UgyiratElintezettTomegesForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Import Namespace="Contentum.eRecord.Utility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
        
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" />

    <div> 
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
        
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    
                  <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                            <asp:Label ID="UgyiratokGridHeader" runat="server" Text="Ügyiratok:" style="font-weight: bold; text-decoration: underline;"/>
                            <div style="margin-top:3px; max-height:250px;overflow-y:auto">
                                <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                    OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                    
                    <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />                               
                                <ItemTemplate>
                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                </ItemTemplate>
                            </asp:TemplateField>    
                            
                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                <ItemTemplate>                                    
                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                        Visible="false" OnClientClick="return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>      
                         <asp:TemplateField HeaderText="Irattár" Visible="false">
                           <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            <ItemTemplate>
                                <asp:Label ID="Label_IrattarTipus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám" 
                                SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                        <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezelõ" SortExpression="Felelos_Nev" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                                                
                            <%--<asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="Ügyindító" SortExpression="NevSTR_Ugyindito"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>
                            <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <%--<asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;dátum" SortExpression="LetrehozasIdo" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Hatarido" HeaderText="Határidõ" SortExpression="Hatarido" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Lezarasdat" HeaderText="Lezárás" SortExpression="Lezarasdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Elintezesdat" HeaderText="Ügyirat elintézési idõpontja" SortExpression="Elintezesdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                        
                            <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                <HeaderTemplate>
                                    <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                            <HeaderStyle CssClass="GridViewBorderHeader" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            <HeaderTemplate>
                                <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labelJelleg" runat="server"
                                    Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                    ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>                                        
                    <PagerSettings Visible="False" />
                </asp:GridView>
                            </div>
                        </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Panel ID="Panel_Warning" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr id="tr_ElintezesIdopontja" runat="server" visible="false">
                <td>
                    <div>&nbsp;</div>
                    <table>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label7" runat="server" Text="Ügyirat elintézési idõpontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" >
                                <uc5:CalendarControl ID="ElintezesDat_CalendarControl" runat="server" Validate="true" TimeVisible="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:formfooter id="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:UpdatePanel runat="server" ID="upResult">
        <ContentTemplate>
        <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
            <eui:eformpanel id="EFormPanel2" runat="server" cssclass="mrResultPanel">
                    <div class="mrResultPanelText">A kijelölt tételek módosítása sikeresen végrehajtódott.</div>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                    onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                    CommandName="Close" />
                            </td>
                        </tr>
                    </table>
            </eUI:eFormPanel>
        </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>
