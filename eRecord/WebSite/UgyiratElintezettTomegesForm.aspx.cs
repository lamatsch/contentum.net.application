﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Web.UI;
using System.Collections.Generic;

public partial class UgyiratElintezettTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private string Command = "";
    private String UgyiratId = "";
    private String[] UgyiratokArray;

    private string funkcioKod = "UgyiratElintezes";

    public string maxTetelszam = "0";
    
    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Ügyiratok tömeges elintézése";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session[Constants.SelectedUgyiratIds] != null)
                UgyiratId = Session[Constants.SelectedUgyiratIds].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (!String.IsNullOrEmpty(UgyiratId))
        {
            UgyiratokArray = UgyiratId.Split(',');
        }

        // Funkciójog-ellenõrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);
        //ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";
        ImageClose.OnClientClick = JavaScripts.GetOnCloseClientClickScript();

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());

        if (!IsPostBack)
        {
            ElintezesDat_CalendarControl.Text = DateTime.Now.ToString();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (UgyiratokArray.Length > 0 && !Page.IsPostBack)
        {
        }
    }

    private void LoadFormComponents()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            FillIratokGridView();
        }
    }

    private void FillIratokGridView()
    {
        var res = UgyiratokGridViewBind();

        // ellenõrzés:
        if (res != null && res.GetCount != UgyiratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        int count_ok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_NEMok = UgyiratokArray.Length - count_ok;

        if (count_NEMok > 0)
        {
            Label_Warning.Text = Resources.List.UI_NotModifiableItemsCount + count_NEMok.ToString();
            Panel_Warning.Visible = true;
        }

        tr_ElintezesIdopontja.Visible = count_ok > 0;
    }
  
    protected Result UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            var service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            var search = new EREC_UgyUgyiratokSearch();
            
            search.Id.In(UgyiratokArray);
            
            Result res = service.GetAllWithExtension(ExecParam, search);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                tr_ElintezesIdopontja.Visible = false;
            }
            else
            {
                ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);
                tr_ElintezesIdopontja.Visible = true;

                if (!CheckItsz())
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel,
                        "Figyelem! A kijelölt tételek irattári tételszáma nem azonos!");
                }
            }
            return res;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    private bool CheckItsz()
    {
        var itsz = "";
        var set = false;
        for (int i = 0; i < UgyUgyiratokGridView.Rows.Count; i++)
        {
            var row = UgyUgyiratokGridView.Rows[i];
            var checkB = (CheckBox)row.FindControl("check");
            if (checkB != null && checkB.Checked)
            {
                var cell = row.Cells.Cast<DataControlFieldCell>().FirstOrDefault(c => c != null && c.ContainingField is BoundField 
                && ((BoundField)c.ContainingField).DataField.Equals("ITSZ"));
                if (cell != null)
                {
                    if (!set)
                    {
                        itsz = cell.Text;
                        set = true;
                    }
                    else if (itsz != cell.Text)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void UgyiratokGridView_RowDataBound_Check(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow row = ((DataRowView)e.Row.DataItem).Row;

            var statusz = Ugyiratok.GetAllapotByDataRow(row);

            ErrorDetails errorDetails = null;
            bool ok = statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            if (ok)
            {
                ok = Ugyiratok.ElintezetteNyilvanithato(statusz, UI.SetExecParamDefault(Page), out errorDetails);
            }
            else
            {
                errorDetails = new ErrorDetails { Message = "Csak ügyintézés alatti állapotú ügyirat nyilvánítható elintézetté" };
            }
            UI.SetRowCheckboxAndInfo(ok, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UgyiratokGridView_RowDataBound_Check(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }
    }

    private List<string> GetSelectedIds()
    {
        return ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
    }
    
    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {        
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, funkcioKod))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    var service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    var selectedItemsList = GetSelectedIds();

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    var elintezesDatuma = ElintezesDat_CalendarControl.Text;
                    var elintezesMod = "";
                    var ugyiratIds = selectedItemsList.ToArray();

                    var errorResults = new List<Result>();

                    var batchResult = new Result();
                    batchResult.Ds = new DataSet();
                    batchResult.Ds.Tables.Add();
                    batchResult.Ds.Tables[0].Columns.Add("Id");

                    foreach (var ugyirat in ugyiratIds)
                    {
                        execParam.Record_Id = ugyirat;
                        var result = service.ElintezetteNyilvanitas(execParam, elintezesDatuma, elintezesMod, null);

                        if (result.IsError)
                        {
                            errorResults.Add(result);
                            batchResult.Ds.Tables[0].Rows.Add(ugyirat);
                        }
                    }

                    if (errorResults.Count > 0)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, errorResults[0]);
                        UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, batchResult);
                    }
                    else
                    {
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        ResultError.ResetErrorPanel(FormHeader1.ErrorPanel);
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
        }
    }

    #region Jelleg // TODO: also refact in FelulvizsgalatList.aspx.cs, IrattarbaVetelList.aspx.cs, IrattarList.aspx.cs
    protected void GetJelleg(object oJelleg, out string JellegLabel, out string JellegTooltip)
    {
        string jelleg = (oJelleg ?? String.Empty) as string;

        switch (jelleg)
        {
            case KodTarak.UGYIRAT_JELLEG.Elektronikus:
                JellegLabel = "E";
                JellegTooltip = "Elektronikus";
                break;
            case KodTarak.UGYIRAT_JELLEG.Papir:
                JellegLabel = "P";
                JellegTooltip = "Papír";
                break;
            case KodTarak.UGYIRAT_JELLEG.Vegyes:
                JellegLabel = "V";
                JellegTooltip = "Vegyes";
                break;
            default:
                goto case KodTarak.UGYIRAT_JELLEG.Papir;
        }
    }

    protected string GetJellegLabel(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return label;
    }

    protected string GetJellegToolTip(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return tooltip;
    }

    #endregion
}

