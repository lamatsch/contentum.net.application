<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyiratFelfuggesztesForm.aspx.cs" Inherits="UgyiratFelfuggesztesForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <div>
        <asp:Panel ID="MainPanel" runat="server">

            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />

            <table cellpadding="0" cellspacing="0" width="90%">
                <tr>
                    <td style="vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                            <%--<asp:Label ID="Label_Ugyiratok" runat="server" Text="�gyiratok:" Visible="false" CssClass="GridViewTitle"></asp:Label>--%>
                            <br />

                            <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                OnRowDataBound="UgyUgyiratokGridView_RowDataBound" Width="100%"
                                AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />

                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />

                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' Checked="true" CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m"
                                        SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr class="GridViewHeaderBackGroundStyle">
                                <td>
                                    <asp:Label ID="LabelFelfuggesztesOka" runat="server" Text="Felf�ggeszt�s oka" Font-Bold="true" Width="150px" />
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="DropDownListFelfuggesztesOka" Width="250px" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="TextBoxFelfuggesztesOka" Width="250px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:FormFooter ID="FormFooter1" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upResult">
            <ContentTemplate>
                <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
                    <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
                    <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
                    <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
                    <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
                    <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                        <div class="mrResultPanelText">A kijel�lt t�telek �tad�sa sikeresen v�grehajt�dott.</div>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/images/hu/ovalgomb/atadasilistanyomtatas.gif"
                                        onmouseover="swapByName(this.id,'atadasilistanyomtatas2.gif')" onmouseout="swapByName(this.id,'atadasilistanyomtatas.gif')"
                                        CommandName="Print" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                        onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                        CommandName="Close" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" language="javascript" src="JavaScripts/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" language="javascript">
        var ddlId = "ctl00_ContentPlaceHolder1_DropDownListFelfuggesztesOka";
        var tbId = "ctl00_ContentPlaceHolder1_TextBoxFelfuggesztesOka";
        var optionValueEgyebOk = '&EGYEB_OK';
        $('#' + ddlId).on('change', function () {
            setTextBoxShowHide(this.value);
        });

        function setTextBoxShowHide(ddlSelectedValue) {
            var controlTextBox = $('#' + tbId);
            if (ddlSelectedValue.indexOf(optionValueEgyebOk) !== -1) {
                controlTextBox.show();
            }
            else {
                controlTextBox.hide();
            }
        }
    </script>
</asp:Content>

