using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UgyiratFelfuggesztesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private UI ui = new UI();
    private String UgyiratId = "";
    private string FormattedUgyiratIds { get; set; }
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    public string maxTetelszam = "0";
    private const string JavaScriptCodeCloseWindow = "window.returnValue=true; window.close(); return false;";
    private const string FunkcioUgyirateFelfuggesztes = "UgyiratFelfuggesztes";
    private const string KodCsoport_FelfuggesztesOka = "UGYIRAT_FELFUGGESZTES_OKA";
    private const string FelfuggesztesOkaSpecItemContains = "&EGYEB_OK";
    private const string FelfuggesztesOkaCustomtextValue = "Egy�b ok";
    protected bool IsFelfuggesztett
    {
        get
        {
            return Command == CommandName.Felfuggesztes;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        SetValuesFromQueryString();
        CheckRights();
        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    private void SetValuesFromQueryString()
    {
        Command = Request.QueryString.Get(CommandName.Command);

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session["SelectedUgyiratIds"] != null)
                UgyiratId = Session["SelectedUgyiratIds"].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (string.IsNullOrEmpty(Request.QueryString.Get("SelectedFelfuggesztendoUgyiratIds")))
        {
            if (Session["SelectedFelfuggesztendoUgyiratIds"] != null)
                FormattedUgyiratIds = Session["SelectedFelfuggesztendoUgyiratIds"].ToString();
        }
    }

    private void CheckRights()
    {
        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioUgyirateFelfuggesztes);
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.UgyiratFelfuggesztesFormHeaderTitle;
        FormHeader1.DisableModeLabel = true;
        ImageClose.OnClientClick = JavaScriptCodeCloseWindow;
        FormFooter1.ButtonsClick += FormFooter1_ButtonsClick;
        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
        // f�kusz az "�tad�s ide" mez�re
        //JavaScripts.SetFocus(CsoportId_Felelos_Kovetkezo_CsoportTextBox.TextBox);
    }

    private void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        UgyiratFelfuggesztesProcedure();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Save.Visible = true;
    }
    private void LoadFormComponents()
    {
        UgyiratokListPanel.Visible = true;
        FillUgyiratokGridView();
        List<ListItem> felfuggeszteOkai = GetFelfuggesztesOkaListItem();
        if (felfuggeszteOkai != null)
        {
            DropDownListFelfuggesztesOka.Items.AddRange(felfuggeszteOkai.ToArray());
        }
    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (!string.IsNullOrEmpty(FormattedUgyiratIds))
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

            search.Id.Value = FormattedUgyiratIds;
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }
    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        //Ugyiratok.UgyiratokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);


        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");

        //CheckBox cb = (CheckBox)e.Row.FindControl("check");
        //CheckBox cbUgyintezesModja = (CheckBox)e.Row.FindControl("cbUgyintezesModja");
        //if (cb != null)
        //{
        //    if (cbUgyintezesModja != null)
        //    {
        //        // figyelj�k a nem elektronikus kezel�s� t�teleket, �s az �zenetet a pa�ron val� �tad�sr�l majd ennek megfelel�en mutatjuk
        //        // NULL eset�n a checkbox Visible tulajdonsaga false-ra van �ll�tva, ez�rt a JavaSript nem fogja megtal�lni
        //        cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)"
        //            + "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;"
        //            + " var cbUgyintezesModja = document.getElementById('" + cbUgyintezesModja.ClientID + "');if (!cbUgyintezesModja || !cbUgyintezesModja.checked) {document.getElementById('" + labelNemElektronikusTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelNemElektronikusTetelekSzamaDb.ClientID + "').innerHTML)+1;}"
        //            + "} else {"
        //            + "document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;"
        //            + "var cbUgyintezesModja = document.getElementById('" + cbUgyintezesModja.ClientID + "');if (!cbUgyintezesModja || !cbUgyintezesModja.checked) {document.getElementById('" + labelNemElektronikusTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelNemElektronikusTetelekSzamaDb.ClientID + "').innerHTML)-1;}"
        //            + "} return true;");
        //    }
        //    else
        //    {
        //        cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
        //            "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
        //            "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
        //            "; return true;");
        //    }
        //}
    }

    protected void DossziekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Dossziek.DossziekGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");

        if (cb != null)
        {
            //cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
            //    "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
            //    "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
            //    "; return true;");
        }
    }

    #region KODTARAK
    /// <summary>
    /// Visszaadja a k�dt�rb�l a lehets�ges felf�ggeszt�s okokat.
    /// </summary>
    /// <returns></returns>
    private List<ListItem> GetFelfuggesztesOkaListItem()
    {
        List<ListItem> ret = new List<ListItem>();

        using (Contentum.eAdmin.Service.KRT_KodTarakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodTarakService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = service.GetAllByKodcsoportKod(execParam, KodCsoport_FelfuggesztesOka);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    String kodtarNev = row["Nev"].ToString();
                    String kodtarKod = row["Kod"].ToString();
                    String kodtarId = row["Id"].ToString();
                    ret.Add(new ListItem(kodtarNev, kodtarId + "&" + kodtarKod));
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return null;
            }
        }
        return ret;
    }
    #endregion

    #region FELFUGGESZTES
    /// <summary>
    /// Felf�ggeszt�s folyamat
    /// </summary>
    private void UgyiratFelfuggesztesProcedure()
    {
        List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        String[] KuldemenyIds = selectedItemsList.ToArray();

        Result result = service.Felfuggesztes_Tomeges(execParam, KuldemenyIds, GetFelfuggesztesOka());
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
        }

    }
    /// <summary>
    /// Visszaadja a felf�ggeszt�s ok�t.
    /// </summary>
    /// <returns></returns>
    private string GetFelfuggesztesOka()
    {
        if (DropDownListFelfuggesztesOka.SelectedItem.Value.EndsWith(FelfuggesztesOkaSpecItemContains))
            return string.IsNullOrEmpty(TextBoxFelfuggesztesOka.Text) ? FelfuggesztesOkaCustomtextValue : TextBoxFelfuggesztesOka.Text;
        else
            return DropDownListFelfuggesztesOka.SelectedItem.Text;
    }
    #endregion
}
