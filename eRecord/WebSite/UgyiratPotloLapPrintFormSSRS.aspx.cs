﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;

public partial class UgyiratpotloLapPrintForm : Contentum.eUtility.UI.PageBase
{
   // private string fejId = String.Empty;
    private string ugyiratIds = String.Empty;

    private string executor = String.Empty;
    private string szervezet = String.Empty;
    private string ugyiratIds2 = String.Empty;


    protected void Page_Init(object sender, EventArgs e)
    {
       // fejId = Request.QueryString.Get("fejId");
        ugyiratIds = Request.QueryString.Get("UgyiratId");
        ugyiratIds2 = ugyiratIds.Replace(",", "','");

        /* if (!string.IsNullOrEmpty(ugyiratIds))
            {
                ugyiratIds = ugyiratIds.Replace("?", "");
            } */
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                erec_UgyUgyiratokSearch.Id.Value = ugyiratIds;
                erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;
               // erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.Allapot = '04' ";

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                executor = execParam.LoginUser_Id;
                szervezet = execParam.Org_Id;


                execParam.Fake = true;

                Result result = ugyservice.GetAllWithExtensionForUgyiratpotlo(execParam, erec_UgyUgyiratokSearch);

                ReportParameters = new ReportParameter[rpis.Count];

                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    // Response.Write(result.Ds.Tables[0].Rows[0]["Targy"].ToString());

                    for (int i = 0; i < rpis.Count; i++)
                    {
                        ReportParameters[i] = new ReportParameter(rpis[i].Name);

                        switch (rpis[i].Name)
                        {
                            case "Where":
                                  ReportParameters[i].Values.Add("EREC_UgyUgyiratok.ErvKezd <= getdate() and EREC_UgyUgyiratok.ErvVege >= getdate() and EREC_UgyUgyiratok.id in ('"+ugyiratIds2 + "')");
                                break;

                            case "ExecutorUserId":
                                if (!string.IsNullOrEmpty(executor))
                                {
                                    ReportParameters[i].Values.Add(executor);
                                }
                                break;

                            case "FelhasznaloSzervezet_Id":
                                if (!string.IsNullOrEmpty(szervezet))
                                {
                                    ReportParameters[i].Values.Add(szervezet);
                                }
                                break;

                            case "UgyiratId":
                                ReportParameters[i].Values.AddRange(ugyiratIds.Split(','));
                                break;
                        }
                    }
                }

                else
                {
                    // Response.Write("ooo");

                }
            }
        }
        return ReportParameters;
    }


}