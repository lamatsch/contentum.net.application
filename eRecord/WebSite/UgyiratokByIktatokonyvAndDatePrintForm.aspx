﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="UgyiratokByIktatokonyvAndDatePrintForm.aspx.cs" Inherits="UgyiratokByIktatokonyvAndDatePrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyiratokByIktatokonyvAndDatePrintFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="hiddenitem">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label33" runat="server" CssClass="mrUrlapCaption" Text="Iktatókönyv éve:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server"
                                    Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    Filter_IdeIktathat="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label3" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label5" runat="server" Text="Dátum:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <cc:CalendarControl ID="DatumCalendarControl1" runat="server" Validate="true" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
