﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class UgyiratokByIktatokonyvAndDatePrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        DatumCalendarControl1.Text = System.DateTime.Today.ToString();
        Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol = System.DateTime.Today.Year.ToString();
        Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg = System.DateTime.Today.Year.ToString();

        //IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
        //        , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
        //        , true, false, "", EErrorPanel1);

        IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, "", EErrorPanel1);
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);
            Result result = null;
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            string where = "";
            string iktatokonyv = "";
            string date = "";



            if (!string.IsNullOrEmpty(DatumCalendarControl1.Text))
            {
                string _date = DatumCalendarControl1.Text.Replace(" ", "").Remove(10).Replace(".", "-");
                date = DatumCalendarControl1.Text;
                where = " EREC_UgyUgyiratok.Letrehozasido BETWEEN '" + _date + " 00:00:00' and ' " + _date + " 23:59:59' ";
            }

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                IraIktatoKonyvekDropDownList1.SetSearchObject(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                iktatokonyv = IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text;
                string iktatokonyv_Id = IraIktatoKonyvekDropDownList1.SelectedValue;

                where += " and EREC_UgyUgyiratok.IraIktatokonyv_Id='" + iktatokonyv_Id + "' ";
            }

            erec_UgyUgyiratokSearch.WhereByManual = where;

            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            //string orderby = erec_UgyUgyiratokSearch.OrderBy;
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            Session["Where"] = where;

            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "Date=" + DatumCalendarControl1.Text + '&' + "Iktatokonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue;

            //Meghívjuk az UgyiratokByIktatokonyvAndDatePrintFormSSRS.aspx nyomtatóoldalt, .
            string js = "javascript:window.open('UgyiratokByIktatokonyvAndDatePrintFormSSRS.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyiratokByIktatokonyvAndDatePrintFormSSRS", js, true);
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
