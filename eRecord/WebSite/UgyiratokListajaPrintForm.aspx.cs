﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class UgyiratokListajaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));

        int toprow = System.Convert.ToInt32(UI.GetAppSetting("TempMan_toprow"));
        if (toprow > 10000)
        {
            toprow = 10000;
        }

        search.TopRow = toprow;

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Where";
        column.AutoIncrement = false;
        column.Caption = "Where";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Where"] = search.ReadableWhere;
        table.Rows.Add(row);

        //string xml = result.Ds.GetXml();
        //string xsd = result.Ds.GetXmlSchema();
        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        WebRequest wr = WebRequest.Create(SP_TM_site_url + "Ugyiratok_listaja.xml");
        wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
        StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
        templateText = template.ReadToEnd();
        template.Close();
        //string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"ulista\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>Kiss Gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>5</o:Revision><o:TotalTime>17</o:TotalTime><o:Created>2008-03-12T09:46:00Z</o:Created><o:LastSaved>2009-02-18T11:27:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>21</o:Words><o:Characters>151</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>171</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"3ECA7D50\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"698223CE\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:lsid w:val=\"43BD71EC\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"6B02C540\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"2\"><w:lsid w:val=\"5F923F7E\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"2BFE1A5E\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"3\"><w:ilst w:val=\"2\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"00B0580C\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00D0693E\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"00D0693E\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"9218\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00D0693E\"/><wsp:rsid wsp:val=\"000874E3\"/><wsp:rsid wsp:val=\"00103281\"/><wsp:rsid wsp:val=\"00144EFF\"/><wsp:rsid wsp:val=\"0040653A\"/><wsp:rsid wsp:val=\"00474299\"/><wsp:rsid wsp:val=\"004B2016\"/><wsp:rsid wsp:val=\"005A7171\"/><wsp:rsid wsp:val=\"006D14FE\"/><wsp:rsid wsp:val=\"00787483\"/><wsp:rsid wsp:val=\"007B6579\"/><wsp:rsid wsp:val=\"008140A2\"/><wsp:rsid wsp:val=\"0096345B\"/><wsp:rsid wsp:val=\"009C7C2C\"/><wsp:rsid wsp:val=\"00A27C9C\"/><wsp:rsid wsp:val=\"00B0580C\"/><wsp:rsid wsp:val=\"00B306A8\"/><wsp:rsid wsp:val=\"00D0693E\"/><wsp:rsid wsp:val=\"00E853CD\"/><wsp:rsid wsp:val=\"00F35C21\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"00B0580C\" wsp:rsidRDefault=\"00B306A8\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Ügyiratok</w:t></w:r><w:r wsp:rsidR=\"00D0693E\" wsp:rsidRPr=\"00D0693E\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t> listája</w:t></w:r></w:p><w:p wsp:rsidR=\"00D0693E\" wsp:rsidRDefault=\"00787483\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>(</w:t></w:r><ParentTable><Where/></ParentTable><w:r><w:t>)</w:t></w:r></w:p><w:p wsp:rsidR=\"00787483\" wsp:rsidRDefault=\"00787483\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"15593\" w:type=\"dxa\"/><w:tblInd w:w=\"-601\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"392\"/><w:gridCol w:w=\"567\"/><w:gridCol w:w=\"709\"/><w:gridCol w:w=\"425\"/><w:gridCol w:w=\"1417\"/><w:gridCol w:w=\"1701\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1559\"/><w:gridCol w:w=\"1276\"/><w:gridCol w:w=\"1088\"/><w:gridCol w:w=\"1088\"/><w:gridCol w:w=\"1088\"/><w:gridCol w:w=\"1555\"/><w:gridCol w:w=\"1310\"/></w:tblGrid><w:tr wsp:rsidR=\"00E853CD\" wsp:rsidTr=\"00E853CD\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"567\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Sz.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"709\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Csat.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"425\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1417\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Főszám</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1701\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Tárgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Kezelő</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Vonalkód</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1276\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r><w:t>Itsz</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Állapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r><w:t>Ikt</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r><w:t>. dátum</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Határidő</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1555\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>Irat helye</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1310\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r><w:t>Alszám</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r><w:t>/Db</w:t></w:r></w:p></w:tc></w:tr><Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"00E853CD\" wsp:rsidTr=\"00E853CD\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"008140A2\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"3\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"0\" w:hanging=\"11\"/><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc><SzereltCount><w:tc><w:tcPr><w:tcW w:w=\"567\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></SzereltCount><CsatolasCount><w:tc><w:tcPr><w:tcW w:w=\"709\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></CsatolasCount><CsatolmanyCount><w:tc><w:tcPr><w:tcW w:w=\"425\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></CsatolmanyCount><Foszam_Merge><w:tc><w:tcPr><w:tcW w:w=\"1417\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></Foszam_Merge><Targy><w:tc><w:tcPr><w:tcW w:w=\"1701\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></Targy><Felelos_Nev><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></Felelos_Nev><BARCODE><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></BARCODE><Merge_IrattariTetelszam><w:tc><w:tcPr><w:tcW w:w=\"1276\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></Merge_IrattariTetelszam><Allapot_Nev_Print><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\" wsp:rsidP=\"005A7171\"/></w:tc></Allapot_Nev_Print><LetrehozasIdo_Rovid><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></LetrehozasIdo_Rovid><Hatarido><w:tc><w:tcPr><w:tcW w:w=\"1088\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></Hatarido><KRT_Csoportok_Orzo_Nev><w:tc><w:tcPr><w:tcW w:w=\"1555\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"00E853CD\"/></w:tc></KRT_Csoportok_Orzo_Nev><w:tc><w:tcPr><w:tcW w:w=\"1310\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00E853CD\" wsp:rsidRDefault=\"004B2016\"><UtolsoAlszam/><w:r><w:t>/</w:t></w:r><IratSzam/></w:p></w:tc></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00D0693E\" wsp:rsidRDefault=\"00F35C21\" wsp:rsidP=\"00D0693E\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00D0693E\" wsp:rsidSect=\"00D0693E\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }

        result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);

        string filename = "";

        if (pdf)
        {
            filename = "Ugyiratok_listaja_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            filename = "Ugyiratok_listaja_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }

        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();
        
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(ExecParam, krt_CsoportTagokSearch);

        if (string.IsNullOrEmpty(csop_result.ErrorCode))
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals("3"))
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 10);
        //result = tms.GetWordDocument_DataSet(templateText, result, pdf);

        byte[] res = (byte[])result.Record;

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
            }
        }
    }
}
