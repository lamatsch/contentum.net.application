﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="UgyiratokNumericSearch.aspx.cs" Inherits="UgyiratokNumericSearch" Title="A rózsaszín gombra feljövő keresés!" 
EnableEventValidation="false"%>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>  
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc5" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc6" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:searchheader id="SearchHeader1" runat="server">
    </uc1:searchheader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="600px">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEv" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:evintervallum_searchformcontrol id="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                           <td class="mrUrlapCaption">
                                <asp:Label ID="labelIktatokonyv" runat="server" Text="Iktatókönyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="width:400px">
                                <uc4:iraiktatokonyvekdropdownlist id="UgyDarab_IraIktatoKonyvek_DropDownList" runat="server"
                                EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl" Mode="Iktatokonyvek"
                                Filter_IdeIktathat="false" IsMultiSearchMode="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFoszam" runat="server" Text="Főszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:szamintervallum_searchformcontrol id="UgyDarab_Foszam_SzamIntervallum_SearchFormControl"
                                 runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="trAlszam">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label1" runat="server" Text="Alszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:szamintervallum_searchformcontrol id="szamintervallumAlszam"
                                 runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor"> 
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc6:talalatokszama_searchformcomponent id="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                             </td>   
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc2:searchfooter id="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

