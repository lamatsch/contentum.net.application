﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;

public partial class UgyiratokNumericSearch : System.Web.UI.Page
{
    private Type _type = typeof(EREC_UgyUgyiratokSearch);
    private readonly Type _typeUgyirat = typeof(EREC_UgyUgyiratokSearch);
    private readonly Type _typeIrat = typeof(EREC_IraIratokSearch);
    private readonly Type _typeIratPeldany = typeof(EREC_PldIratPeldanyokSearch);

    private ObjektumType _objType = ObjektumType.Ugyirat;
    private enum ObjektumType
    {
        Ugyirat = 1,
        Irat = 2,
        IratPeldany = 3
    }

    private String Startup = String.Empty;

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
        
        
        string title = Resources.Search.UgyiratQuickSearchHeaderTitle;

        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
            case Constants.Startup.FromKozpontiIrattar:
            case Constants.Startup.FromAtmenetiIrattarbaVetel:
            case Constants.Startup.FromKozpontiIrattarbaVetel:
            case Constants.Startup.FromMunkanaplo:
            case Constants.Startup.FromUgyirat:
            case Constants.Startup.Skontro:
            case Constants.Startup.FromSkontroIrattar:
            // CR3289 Vezetői panel kezelés
            case Constants.Startup.FromVezetoiPanel_Ugyirat:
                title = Resources.Search.UgyiratQuickSearchHeaderTitle;
                _type = _typeUgyirat;
                break;
            case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
            case Constants.Startup.FromMunkaUgyiratokIratai:
            case Constants.Startup.FromKozgyulesiEloterjesztesek:
            case Constants.Startup.FromBizottsagiEloterjesztesek:
            case Constants.Startup.FromIrat:
            // CR3221 Szignálás(előkészítés) kezelés
            case Constants.Startup.FromMunkairatSzignalas:
            // CR3289 Vezetői panel kezelés
            case Constants.Startup.FromVezetoiPanel_Irat:
                title = Resources.Search.IratQuickSearchHeaderTitle;
                _type = _typeIrat;
                _objType = ObjektumType.Irat;
                break;
            case Constants.Startup.FromIratPeldany:
            case Constants.Startup.FromKimenoIratPeldany:
                title = Resources.Search.IratPeldanyQuickSearchHeaderTitle;
                _type = _typeIratPeldany;
                _objType = ObjektumType.IratPeldany;
                break;
            default:
                goto case Constants.Startup.FromUgyirat;
        }

        SearchHeader1.HeaderTitle = title;
        Title = title;
        SearchHeader1.TemplateObjectType = _type;
        // Irattárnál CustomTemplateTipusNev kell:

        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch;
                break;
                case Constants.Startup.FromKozpontiIrattar:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch;
                break;
                case Constants.Startup.FromAtmenetiIrattarbaVetel:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch;
                break;
                case Constants.Startup.FromKozpontiIrattarbaVetel:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch;
                break;
                case Constants.Startup.FromMunkanaplo:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunaknaploSearch;
                break;
                case Constants.Startup.Skontro:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SkontroSearch;
                break;
                case Constants.Startup.FromSkontroIrattar:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch;
                break;
                case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch;
                break;
                case Constants.Startup.FromMunkaUgyiratokIratai:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch;
                break;
                case Constants.Startup.FromKozgyulesiEloterjesztesek:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch;
                break;
                case Constants.Startup.FromBizottsagiEloterjesztesek:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch;
                break;
            // CR3221 Szignálás(előkészítés) kezelés
            case Constants.Startup.FromMunkairatSzignalas:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch;
                break;
            // CR3221 Vezetői panel kezelés
            case Constants.Startup.FromVezetoiPanel_Ugyirat:
            case Constants.Startup.FromVezetoiPanel_Irat:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch;
                break;
            case Constants.Startup.FromKimenoIratPeldany:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch;
                break;
        }
        
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            // Jelezzük az iktatókönyv komponensnek, hogy Id-kat kell beletölteni, nem iktatóhely értéket: (BUG#4290)
            UgyDarab_IraIktatoKonyvek_DropDownList.ValuesFilledWithId = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        Page.Title = Page.Title + " - " + Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.APPLICATION_VERSION) + " ";

        if (!IsPostBack)
        {

            BaseSearchObject searchObject = null;

            switch (Startup)
            {
                case Constants.Startup.FromAtmenetiIrattar:
                case Constants.Startup.FromKozpontiIrattar:
                case Constants.Startup.FromAtmenetiIrattarbaVetel:
                case Constants.Startup.FromKozpontiIrattarbaVetel:
                case Constants.Startup.FromMunkanaplo:
                case Constants.Startup.Skontro:
                case Constants.Startup.FromSkontroIrattar:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , SearchHeader1.CustomTemplateTipusNev);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
                case Constants.Startup.FromMunkaUgyiratokIratai:
                // CR3221 Szignálás(előkészítés) kezelés
                case Constants.Startup.FromMunkairatSzignalas:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true)
                            , SearchHeader1.CustomTemplateTipusNev);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromKozgyulesiEloterjesztesek:
                case Constants.Startup.FromBizottsagiEloterjesztesek:
                // CR3289 Vezetői Panel kezelés
                case Constants.Startup.FromVezetoiPanel_Irat:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true)
                            , SearchHeader1.CustomTemplateTipusNev);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                // CR3289 Vezetői Panel kezelés
                case Constants.Startup.FromVezetoiPanel_Ugyirat:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                    {
                        searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , SearchHeader1.CustomTemplateTipusNev);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromKimenoIratPeldany:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                    {
                        searchObject = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                            , SearchHeader1.CustomTemplateTipusNev);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                default:
                    if (Search.IsSearchObjectInSession(Page, _type))
                    {
                        switch (Startup)
                        {
                            case Constants.Startup.FromUgyirat:
                                {
                                    searchObject = (BaseSearchObject)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));
                                    break;
                                }
                            case Constants.Startup.FromIrat:
                                {
                                    searchObject = (BaseSearchObject)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
                                    break;

                                }
                            case Constants.Startup.FromIratPeldany:
                                {
                                    searchObject = (BaseSearchObject)Search.GetSearchObject(Page, new EREC_PldIratPeldanyokSearch(true));
                                    break;
                                }
                            default:
                                goto case Constants.Startup.FromUgyirat;
                        }
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
            }

            LoadComponentsFromSearchObject(searchObject);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        trAlszam.Visible = (_objType == ObjektumType.IratPeldany);
    }

    #endregion


    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraIktatoKonyvekSearch schIktatatokonyv = null;

        switch (_objType)
        {
            case ObjektumType.Ugyirat:
                {
                    EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = null;
                    if (searchObject != null) erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)searchObject;
                    if (erec_UgyUgyiratokSearch != null)
                    {
                        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                            erec_UgyUgyiratokSearch.Foszam);

                        schIktatatokonyv = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch;
                    }
                    break;
                }
            case ObjektumType.Irat:
                {
                    EREC_IraIratokSearch erec_IraIratokSearch = null;
                    if (searchObject != null) erec_IraIratokSearch = (EREC_IraIratokSearch)searchObject;
                    if (erec_IraIratokSearch != null)
                    {
                        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                            erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

                        schIktatatokonyv = erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch;
                    }
                    break;

                }
            case ObjektumType.IratPeldany:
                {
                    EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = null;
                    if (searchObject != null) erec_PldIratPeldanyokSearch = (EREC_PldIratPeldanyokSearch)searchObject;
                    if (erec_PldIratPeldanyokSearch != null)
                    {
                        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                            erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);

                        schIktatatokonyv = erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch;

                        szamintervallumAlszam.SetComponentFromSearchObjectFields(erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);
                    }
                    break;
                }
            default:
                goto case ObjektumType.Ugyirat;
        }

        if (schIktatatokonyv != null)
        {
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                               schIktatatokonyv.Ev);

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
            if (isTUK)
            {
                //TÜK esetén az id lesz az érték mezőnek feltöltve, mivel itt Id szerinti keresés lesz
                string id = schIktatatokonyv.Id == null ? null : schIktatatokonyv.Id.Value;
                UgyDarab_IraIktatoKonyvek_DropDownList.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg, true, false, id, SearchHeader1.ErrorPanel);
            }
            else
            {
                UgyDarab_IraIktatoKonyvek_DropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(schIktatatokonyv), SearchHeader1.ErrorPanel);
            }
        }

    }


    private BaseSearchObject SetSearchObjectFromComponents()
    {
        BaseSearchObject searchObject = null;
        EREC_IraIktatoKonyvekSearch schIktatatokonyv = null;

        switch (_objType)
        {
            case ObjektumType.Ugyirat:
                {
                    EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)SearchHeader1.TemplateObject;
                    if (erec_UgyUgyiratokSearch == null)
                    {
                        erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);
                    }

                    UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                    erec_UgyUgyiratokSearch.Foszam);

                    searchObject = erec_UgyUgyiratokSearch;
                    schIktatatokonyv = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch;
                    break;
                }
            case ObjektumType.Irat:
                {
                    EREC_IraIratokSearch erec_IraIratokSearch = (EREC_IraIratokSearch)SearchHeader1.TemplateObject;
                    if (erec_IraIratokSearch == null)
                    {
                        erec_IraIratokSearch = new EREC_IraIratokSearch(true);
                    }

                    UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                    erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

                    if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
                    {
                        Search.SetSearchObjectSpecFilter_Irat_KozgyulesiEloterjesztes(erec_IraIratokSearch);
                    }

                    if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
                    {
                        Search.SetSearchObjectSpecFilter_Irat_BizottsagiEloterjesztes(erec_IraIratokSearch);
                    }

                    searchObject = erec_IraIratokSearch;
                    schIktatatokonyv = erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch;
                    break;

                }
            case ObjektumType.IratPeldany:
                {
                    EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = (EREC_PldIratPeldanyokSearch)SearchHeader1.TemplateObject;
                    if (erec_PldIratPeldanyokSearch == null)
                    {
                        erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);
                    }

                    UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                    erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);

                    szamintervallumAlszam.SetSearchObjectFields(erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);

                    searchObject = erec_PldIratPeldanyokSearch;
                    schIktatatokonyv = erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch;
                    break;
                }
            default:
                goto case ObjektumType.Ugyirat;
        }

        if (schIktatatokonyv != null)
        {
            if (!String.IsNullOrEmpty(UgyDarab_IraIktatoKonyvek_DropDownList.SelectedValue))
            {
                UgyDarab_IraIktatoKonyvek_DropDownList.SetSearchObject(schIktatatokonyv);
            }

            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            schIktatatokonyv.Ev);
        }

        return searchObject;
    }


    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            BaseSearchObject searchObject = SetSearchObjectFromComponents();

            if (IsDefaultSearch(searchObject))
            {
                // default searchobject
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }                
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }

    }


    private BaseSearchObject GetDefaultSearchObject()
    {
        BaseSearchObject searchObject;
        if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek && _objType == ObjektumType.Irat)
        {
            searchObject = (BaseSearchObject)Search.CreateSearchObjWithSpecFilter_Irat_KozgyulesiEloterjesztes(Page);
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek && _objType == ObjektumType.Irat)
        {
            searchObject = (BaseSearchObject)Search.CreateSearchObjWithSpecFilter_Irat_KozgyulesiEloterjesztes(Page);
        }
        else if (Startup == Constants.Startup.FromKimenoIratPeldany)
        {
            searchObject = (BaseSearchObject)Search.CreateSearchObjWithDefFilter_KimenoIratPeldany(Page);
        }
        else if(Startup == Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai)
        {
            searchObject = new EREC_IraIratokSearch(true);
        }
        else if (Startup == Constants.Startup.FromMunkaUgyiratokIratai)
        {
            searchObject = new EREC_IraIratokSearch(true);
        }
        else if (Startup == Constants.Startup.FromMunkairatSzignalas)
        {
            searchObject = new EREC_IraIratokSearch(true);
        }
        else
        {
            searchObject = (BaseSearchObject)Search.GetDefaultSearchObject(_type);
        }
        
        return searchObject;
    }

    private bool IsDefaultSearch(BaseSearchObject baseSearchObject)
    {
        BaseSearchObject baseDefaultSearchObject = GetDefaultSearchObject();
        bool _ret = false;
        if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek && _objType == ObjektumType.Irat)
        {
            _ret = Search.IsDefaultSearchObject_EREC_IraIratokSearch((EREC_IraIratokSearch)baseSearchObject
                , (EREC_IraIratokSearch)baseDefaultSearchObject);
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek && _objType == ObjektumType.Irat)
        {
            _ret = Search.IsDefaultSearchObject_EREC_IraIratokSearch((EREC_IraIratokSearch)baseSearchObject
                , (EREC_IraIratokSearch)baseDefaultSearchObject);
        }
        else
        {
            _ret = Search.IsDefaultSearchObject(_type, baseSearchObject, baseDefaultSearchObject);
        }
        return _ret;
    }
}
