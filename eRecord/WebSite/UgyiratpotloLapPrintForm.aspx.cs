﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class UgyiratpotloLapPrintForm : Contentum.eUtility.UI.PageBase
{
    private string fejId = String.Empty;
    private string ugyiratIds = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        fejId = Request.QueryString.Get("fejId");
        ugyiratIds = Request.QueryString.Get(QueryStringVars.UgyiratId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(String.IsNullOrEmpty(fejId) && String.IsNullOrEmpty(ugyiratIds)))
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            
            if (!String.IsNullOrEmpty(fejId))
            {
                EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

                erec_IraKezbesitesiTetelekSearch.WhereByManual = " and (EREC_IraKezbesitesiTetelek.KezbesitesFej_Id='" + fejId + "' or EREC_IraKezbesitesiTetelek.AtveteliFej_Id='" + fejId + "')";

                Result tetel_result = service.GetAll(execParam, erec_IraKezbesitesiTetelekSearch);

                foreach (DataRow _row in tetel_result.Ds.Tables[0].Rows)
                {
                    ugyiratIds = ugyiratIds + "'" + _row["Obj_Id"].ToString() + "',";
                }
                ugyiratIds = ugyiratIds.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(ugyiratIds))
            {
                ugyiratIds = ugyiratIds.Replace("?", "'");
            }

            EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            erec_UgyUgyiratokSearch.Id.Value = ugyiratIds;
            erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

            erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.Allapot = '04' ";

            Result result = ugyservice.GetAllWithExtensionForUgyiratpotlo(execParam, erec_UgyUgyiratokSearch);

            //string xml = result.Ds.GetXml();
            //string xsd = result.Ds.GetXmlSchema();
            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            WebRequest wr = WebRequest.Create(SP_TM_site_url + "Ugyiratpotlo lap.V2.xml"); //Ugyiratpotlo lap.xml
            wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
            StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
            templateText = template.ReadToEnd();
            template.Close();
            //string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"ul\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>bognar.laura</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>1</o:TotalTime><o:Created>2008-09-23T14:34:00Z</o:Created><o:LastSaved>2008-09-23T14:34:00Z</o:LastSaved><o:Pages>2</o:Pages><o:Words>40</o:Words><o:Characters>276</o:Characters><o:Company>Axis Consulting 2000 Kft.</o:Company><o:Lines>2</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>315</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"420020EB\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"TimesNewRomanPS-BoldMT\"><w:altName w:val=\"Times New Roman\"/><w:charset w:val=\"00\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"default\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"006136E5\"/><w:pPr><w:overflowPunct w:val=\"off\"/><w:autoSpaceDE w:val=\"off\"/><w:autoSpaceDN w:val=\"off\"/><w:adjustRightInd w:val=\"off\"/><w:textAlignment w:val=\"baseline\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:fareast=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"006136E5\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"5122\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"006136E5\"/><wsp:rsid wsp:val=\"00001FAC\"/><wsp:rsid wsp:val=\"000A0E05\"/><wsp:rsid wsp:val=\"000C6D9B\"/><wsp:rsid wsp:val=\"002D3925\"/><wsp:rsid wsp:val=\"00345D0B\"/><wsp:rsid wsp:val=\"0042651B\"/><wsp:rsid wsp:val=\"004607FD\"/><wsp:rsid wsp:val=\"00465882\"/><wsp:rsid wsp:val=\"005E0268\"/><wsp:rsid wsp:val=\"00600028\"/><wsp:rsid wsp:val=\"006136E5\"/><wsp:rsid wsp:val=\"006A1429\"/><wsp:rsid wsp:val=\"006A1EAE\"/><wsp:rsid wsp:val=\"00706A6F\"/><wsp:rsid wsp:val=\"0072455E\"/><wsp:rsid wsp:val=\"00A9291C\"/><wsp:rsid wsp:val=\"00AB2CCD\"/><wsp:rsid wsp:val=\"00AE120F\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><Table ns0:repeater=\"true\"><w:p wsp:rsidR=\"000A0E05\" wsp:rsidRDefault=\"000A0E05\"/><w:p wsp:rsidR=\"000A0E05\" wsp:rsidRDefault=\"000A0E05\"><w:r><w:br w:type=\"page\"/></w:r></w:p><w:p wsp:rsidR=\"000A0E05\" wsp:rsidRDefault=\"000A0E05\"/><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"4219\"/><w:gridCol w:w=\"5069\"/></w:tblGrid><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>irattári helye </w:t></w:r><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(irattár neve)</w:t></w:r></w:p></w:tc><Irattar_neve><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Irattar_neve></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>ügyirat száma</w:t></w:r></w:p></w:tc><Foszam_Merge><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Foszam_Merge></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>tárgya/megnevezése</w:t></w:r></w:p></w:tc><Targy><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Targy></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>kiemelés ideje </w:t></w:r><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(év, hó, nap)</w:t></w:r></w:p></w:tc><KikerKezd><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></KikerKezd></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>kiemelés célja </w:t></w:r><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(betekintés, kölcsönzés, ügyintézés, szerelés)</w:t></w:r></w:p></w:tc><FelhasznalasiCelNev><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></FelhasznalasiCelNev></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>kiemelést kérő neve</w:t></w:r></w:p></w:tc><Kikero_IdNev><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Kikero_IdNev></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>kiemelést végző neve</w:t></w:r></w:p></w:tc><Kapta_IdNev><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Kapta_IdNev></w:tr><w:tr wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"006136E5\" wsp:rsidTr=\"00345D0B\"><w:trPr><w:trHeight w:val=\"552\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"4219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>kölcsönzési határidő </w:t></w:r><w:r wsp:rsidRPr=\"00345D0B\"><w:rPr><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(év, hó, nap)</w:t></w:r></w:p></w:tc><KikerVege><w:tc><w:tcPr><w:tcW w:w=\"5069\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00345D0B\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"00A9291C\"><w:pPr><w:rPr><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></KikerVege></w:tr></w:tbl><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"009D342C\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009D342C\"><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>A KIEMELT IRAT</w:t></w:r></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:rPr><w:rFonts w:ascii=\"TimesNewRomanPS-BoldMT\" w:h-ansi=\"TimesNewRomanPS-BoldMT\" w:cs=\"TimesNewRomanPS-BoldMT\"/><wx:font wx:val=\"TimesNewRomanPS-BoldMT\"/><w:b/><w:b-cs/><w:color w:val=\"000000\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00641758\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"/><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00641758\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00641758\"><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr><w:t>ÜGYIRATPÓTLÓ LAP</w:t></w:r></w:p><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00641758\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"/><w:p wsp:rsidR=\"006136E5\" wsp:rsidRPr=\"00641758\" wsp:rsidRDefault=\"006136E5\" wsp:rsidP=\"006136E5\"/><w:p wsp:rsidR=\"000A0E05\" wsp:rsidRDefault=\"002D3925\" wsp:rsidP=\"006136E5\"/></Table><w:p wsp:rsidR=\"00AE120F\" wsp:rsidRDefault=\"002D3925\"/></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00AE120F\" wsp:rsidSect=\"00AE120F\"><w:pgSz w:w=\"11906\" w:h=\"16838\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

            string filename = "";

            if (pdf)
            {
                filename = "Ugyiratpotlo_lap_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = "Ugyiratpotlo_lap_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(csop_result.ErrorCode))
            {
                foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

            byte[] res = (byte[])result.Record;

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.AddHeader("content-disposition", @"attachment;filename=" + filename);
                if (pdf)
                {
                    Response.ContentType = "application/pdf";
                }
                else
                {
                    Response.ContentType = "application/msword";
                }
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else
            {
                if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                {
                    string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                }
            }
        }
    }
}
