<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="UploadLovList.aspx.cs" Inherits="UploadLovList" Title="Untitled Page" %>
<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,UploadLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <ContentTemplate>
							<asp:HiddenField id="SelectedFilesNamesHiddenField1" runat="server"></asp:HiddenField>
							<asp:HiddenField id="SelectedFilesCountHiddenField1" runat="server" Value="0"></asp:HiddenField>
							<br />
							<asp:FileUpload id="FileUpload1" runat="server" Width="500px"></asp:FileUpload>
							<asp:ImageButton id="AddImageButton" 
							                 runat="server" 
							                 ImageUrl="~/images/hu/lov/hozzaad.gif" 
							                 onmouseover="swapByName(this.id,'hozzaad_keret.gif')" 
							                 onmouseout="swapByName(this.id,'hozzaad.gif')" 
							                 AlternateText="A list�hoz hozz�ad" 
							                 CssClass="mrUrlapInputImageButton" 
							                 OnClick="AddImageButton_Click">
							</asp:ImageButton>
							&nbsp;
							<asp:RequiredFieldValidator id="Validator1" runat="server" ErrorMessage="<%$Resources:LovList,UploadLovHasNoFile%>" Display="None" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1"></ajaxToolkit:ValidatorCalloutExtender>
							<br />
							<table width="50%" align="center" border="0">
								<tbody>
									<tr>
										<td align="left" valign="top">
											<asp:CheckBoxList CellPadding="0" CellSpacing="0" id="SelectedFilesCheckBoxList1" runat="server" OnSelectedIndexChanged="SelectedFilesCheckBoxList1_SelectedIndexChanged">
											</asp:CheckBoxList>
										</td>
									</tr>
								</tbody>
							</table>
						</ContentTemplate>
                    &nbsp;
                    &nbsp;
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
