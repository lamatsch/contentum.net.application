using Contentum.eAdmin.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UploadLovList : System.Web.UI.Page
{
    #region public properties

    public bool IsValidate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string OnClick_Add
    {
        set { AddImageButton.OnClientClick = value; }
        get { return AddImageButton.OnClientClick; }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        Page.Title = "m����� ����";
        registerJavascripts();
        if (!IsPostBack)
        {
            SetDefault();
        }
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (SelectedFilesCheckBoxList1.Items.Count != 0)
        {
            Validator1.Enabled = false;
        }
        //for (int i = 0; i < SelectedFilesCheckBoxList1.Items.Count; i++)
        //{
        //    SelectedFilesCheckBoxList1.Items[i].Attributes["onclick"] =
        //        "hehehe('" + SelectedFilesCheckBoxList1.ClientID + "_" + i + "')";
        //}
        LovListFooter1.ImageButton_Ok.OnClientClick =
            "var rv = KivalasztasOnClick('" + SelectedFilesCheckBoxList1.ClientID + "', "
                                   + SelectedFilesCheckBoxList1.Items.Count + ", '"
                                   + SelectedFilesNamesHiddenField1.ClientID + "', '"
                                   + SelectedFilesCountHiddenField1.ClientID + "'"
                                   + ");"
                                   + " if(rv == false) {return false;}";
    }

    protected void AddImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            //JavaScripts.SetOnClientClick(
            String fn = FileUpload1.FileName;
            Boolean vanMar = false;
            int n = SelectedFilesCheckBoxList1.Items.Count;
            for (int i = 0; i < n; i++)
            {
                if (SelectedFilesCheckBoxList1.Items[i].Text == fn)
                {
                    vanMar = true;
                    break;
                }
            }
            if (!vanMar)
            {
                SelectedFilesCheckBoxList1.Items.Add(fn);
                SelectedFilesCheckBoxList1.Items[n].Selected = true;
            }
        }
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (SelectedFilesCheckBoxList1.Items.Count != 0 && 
                SelectedFilesCountHiddenField1.Value != "")
            {
                string selectedId = SelectedFilesNamesHiddenField1.ClientID;
                string selectedText = SelectedFilesNamesHiddenField1.Value;
                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel,
                                                     Resources.Error.ErrorLabel, 
                                                     Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "Upload" , "Javascripts/Upload.js");
        AddImageButton.Attributes["onclick"] =
            "AddImageButtonClick('" + FileUpload1.ClientID + "', '" +
                                      SelectedFilesNamesHiddenField1.ClientID+"', '"+
                                      SelectedFilesCountHiddenField1.ClientID+"')";
    }

    public void SetDefault()
    {
    }
    protected void SelectedFilesCheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Console.WriteLine("HIBA:SelectedFilesCheckBoxList1_SelectedIndexChanged");
    }
}
