﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VezetoiPanel.aspx.cs" Inherits="VezetoiPanel" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="eRecordComponent/PieChart.ascx" TagName="PieChart" TagPrefix="Stickler" %>
<%@ Register Src="eRecordComponent/BarChart.ascx" TagName="BarChart" TagPrefix="Stickler" %>
<%@ Register Src="~/Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <asp:Panel ID="MainPanel" runat="server">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" OnActiveTabChanged="UgyUgyiratokTabContainer_ActiveTabChanged"
            OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="SzervezetiStatisztikaTabPanel" runat="server" TabIndex="0">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Szervezeti statisztika" />
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="SzervezetiStatisztikaUpdatePanel" runat="server" OnLoad="SzervezetiStatisztikaUpdatePanel_Load">
                        <ContentTemplate>
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label_FuttatasIdeje" runat="server" Text="<%$Forditas:Label_FuttatasIdeje|Futtatás&nbsp;ideje:%>" CssClass="FuttatasIdejeRowStyle"></asp:Label>
                                        </td>
                                        <td  class="mrUrlapCaption">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" style="z-index:100" bOpenDirectionTop="True" ValidateDateFormat="false"  />
                                        </td>                                        
                                        <!-- excel link -->
                                        <td style="text-align: right; padding-right: 10px;">
                                            <asp:ImageButton ID="imgExcelRiport" runat="server" ImageUrl="~/images/hu/trapezgomb/excelriport_trap.jpg"
                                                OnClientClick="javascript:window.open('ExcelDetailsForLeaders.aspx');" onmouseover="swapByName(this.id,'excelriport_trap2.jpg')"
                                                onmouseout="swapByName(this.id,'excelriport_trap.jpg')" Visible="false" />
                                        </td>
                                    </tr>

                                    <tr class="urlapSor" id="trAlosztalyok" runat="server">
                                         <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAlosztaly" runat="server" Text="<%$Forditas:labelAlosztaly|Alosztály:%>"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="7">
                                            <asp:DropDownList ID="ddListAlosztalyok" runat="server" 
                                            DataTextField="Nev" DataValueField="Id" AutoPostBack="true" AppendDataBoundItems="false"
                                            OnSelectedIndexChanged="ddListAlosztalyok_SelectedIndexChanged">
                                                <%--<asp:ListItem Selected="True" Text="" Value=""></asp:ListItem>--%>
                                            </asp:DropDownList>
                                            <asp:Label ID="labelVezetettAlosztaly" runat="server"></asp:Label>
                                        </td>
                                                                                
                                    </tr>
                                    <tr class="urlapSor" id="tr1" runat="server">                                         
                                        <td>
                                            <asp:ImageButton ID="ImageButton_Refresh" runat="server" ImageUrl="~/images/hu/trapezgomb/frissites_trap.jpg" 
                                                onmouseover="swapByName(this.id,'frissites_trap2.jpg')"
                                                onmouseout="swapByName(this.id,'frissites_trap.jpg')" />
                                        </td>                                        
                                        </tr>
                                </table>
                                <br />
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                    <tr>
                                        <td align="left">
                                            <div style="position: relative; width: 99%">
                                                <eUI:eFormPanel ID="EFormPanel_IratKezelesiFeladataim" runat="server">
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <!-- charts --->
                                                        <tr>
                                                            <td align="left">
                                                                <Stickler:BarChart ID="PostAndFilesBarChart" runat="server" />
                                                            </td>
                                                            <td align="left">
                                                                <Stickler:PieChart ID="FilesByStatusPieChart" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <%-- <Stickler:PieChart id="ExpiredPieChart" runat="server" /> --%>
                                                                <Stickler:BarChart ID="ExpiredBarChart" runat="server" />
                                                            </td>
                                                            <td align="left">
                                                                <%-- <Stickler:PieChart id="TermCriticalPieChart" runat="server" /> --%>
                                                                <Stickler:BarChart ID="TermCriticalBarChart" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapElvalasztoSor">
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tbody>
                                                                        <tr class="urlapNyitoSor">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                            </td>
                                                                            <td class="mrUrlapMezo">
                                                                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            <%-- <td class="mrUrlapMezo">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                <td style="width: 70%;">
                                                                </td> --%>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal">
                                                                                <asp:Label ID="Label_header_kuldemenyek" runat="server" Text="<%$Forditas:Label_ErkeztetettKuldemenyekSzama|Érkezett&nbsp;küldemények&nbsp;száma:%>"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kuldemenyek" runat="server">
                                                                                    <asp:Label ID="Label_kuldemenyek" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kuldemenyek_ma" runat="server" Text="Mai&nbsp;napon:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kuldemenyek_ma" runat="server">
                                                                                    <asp:Label ID="Label_kuldemenyek_ma" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kuldemenyek_1_3nap" runat="server" Text="1-3&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kuldemenyek_1_3nap" runat="server">
                                                                                    <asp:Label ID="Label_kuldemenyek_1_3nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kuldemenyek_4_7nap" runat="server" Text="4-7&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kuldemenyek_4_7nap" runat="server" Font-Underline="true">
                                                                                    <asp:Label ID="Label_kuldemenyek_4_7nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kuldemenyek_7napfelett" runat="server" Text="Több&nbsp;mint&nbsp;7&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kuldemenyek_7napfelett" runat="server">
                                                                                    <asp:Label ID="Label_kuldemenyek_7napfelett" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal">
                                                                                <asp:Label ID="Label_header_kuldemenyek_napi_atlag" runat="server" Text="Küldemények naponta átlagosan:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:Label ID="Label_kuldemenyek_napi_atlag" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td colspan="3">
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal">
                                                                                <asp:Label ID="Label_header_ugyiratok" runat="server" Text="Új&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_ugyiratok_ma" runat="server" Text="Mai&nbsp;napon&nbsp;keletkezett:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok_ma" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok_ma" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_ugyiratok_1_3nap" runat="server" Text="1-3&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok_1_3nap" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok_1_3nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_ugyiratok_4_7nap" runat="server" Text="4-7&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok_4_7nap" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok_4_7nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_ugyiratok_7napfelett" runat="server" Text="Több&nbsp;mint&nbsp;7&nbsp;napja:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok_7napfelett" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok_7napfelett" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td colspan="3">
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal">
                                                                                <asp:Label ID="Label_header_iratok_alszamra_ma" runat="server" Text="Mai&nbsp;napon&nbsp;alszámra&nbsp;iktatott&nbsp;iratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_iratok_alszamra_ma" runat="server">
                                                                                    <asp:Label ID="Label_iratok_alszamra_ma" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_iratok_alszamra_bejovo_ma" runat="server" Text="Bejövõ:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_iratok_alszamra_bejovo_ma" runat="server">
                                                                                    <asp:Label ID="Label_iratok_alszamra_bejovo_ma" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_iratok_alszamra_belso_ma" runat="server" Text="Belsõ:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_iratok_alszamra_belso_ma" runat="server">
                                                                                    <asp:Label ID="Label_iratok_alszamra_belso_ma" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                            <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <%-- 
                                                            <tr class="urlapElvalasztoSor">
                                                                <td></td>
                                                             </tr>
 --%>
                                                                            <%--// CR3135: Vezetõi panelen lejárt és kritikus határidejû iratok megjelenítése--%>
                                                                            <tr class="urlapSor_kicsi">
                                                                                <td class="mrUrlapCaptionBal">
                                                                                    <asp:Label ID="Label_header_lejart_hatarideju_Irat" runat="server" Text="Lejárt&nbsp;határidejû&nbsp;Iratok:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_hatarideju_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_hatarideju_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_1nap_Irat" runat="server" Text="1&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_1nap_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_1nap_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_2_5nap_Irat" runat="server" Text="2-5&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_2_5nap_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_2_5nap_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_6_10nap_Irat" runat="server" Text="6-10&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_6_10nap_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_6_10nap_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_11_15nap_Irat" runat="server" Text="11-15&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_11_15nap_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_11_15nap_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_15napfelett_Irat" runat="server" Text="Több&nbsp;mint&nbsp;15&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_15napfelett_Irat" runat="server">
                                                                                        <asp:Label ID="Label_lejart_15napfelett_Irat" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                                <tr class="urlapElvalasztoSor">
                                                                            <td colspan="3"> </td>
                                                                        </tr>
                                                                             <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_kritikus_hatarideju_irat" runat="server" Text="Kritikus&nbsp;határidejû&nbsp;iratok:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_hatarideju_irat" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_hatarideju_irat" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_0nap_irat" runat="server" Text="0&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_0nap_irat" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_0nap_irat" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_1nap_irat" runat="server" Text="1&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_1nap_irat" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_1nap_irat" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_2_5nap_irat" runat="server" Text="2-5&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_2_5nap_irat" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_2_5nap_irat" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_6_10nap_irat" runat="server" Text="6-10&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_6_10nap_irat" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_6_10nap_irat" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         
                                                                           
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tbody>
                                                                        <tr class="urlapNyitoSor">
                                                                            <td class="mrUrlapCaption">
                                                                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                            </td>
                                                                            <td class="mrUrlapMezo">
                                                                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                            </td>
                                                                            <td class="mrUrlapMezo">
                                                                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                            </td>
                                                                            <td style="width: 70%;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_ugyiratok_osszesen" runat="server" Text="Ügyiratok&nbsp;száma&nbsp;összesen:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyiratok_osszesen" runat="server">
                                                                                    <asp:Label ID="Label_ugyiratok_osszesen" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_ugyintezes_alatt" runat="server" Text="Ügyintézés&nbsp;alatt&nbsp;lévõ&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_ugyintezes_alatt" runat="server">
                                                                                    <asp:Label ID="Label_ugyintezes_alatt" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <%--// CR3133 Szignálásra váró és Szignált ne jelenjen meg--%>
                                                                        <tr class="urlapSor_kicsi" style="display: none;">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_szignalasra_varo" runat="server" Text="Szignálásra&nbsp;váró&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_szignalasra_varo" runat="server">
                                                                                    <asp:Label ID="Label_szignalasra_varo" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_elintezett" runat="server" Text="Elintézett&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_elintezett" runat="server">
                                                                                    <asp:Label ID="Label_elintezett" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                         <%--// CR3133 Szignálásra váró és Szignált ne jelenjen meg--%>
                                                                        <tr class="urlapSor_kicsi"  style="display: none;">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_szignalt" runat="server" Text="Szignált&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_szignalt" runat="server">
                                                                                    <asp:Label ID="Label_szignalt" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_lezart" runat="server" Text="Lezárt&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_lezart" runat="server">
                                                                                    <asp:Label ID="Label_lezart" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_skontroban_levo" runat="server" Text="Skontróban&nbsp;lévõ&nbsp;ügyiratok:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_skontroban_levo" runat="server">
                                                                                    <asp:Label ID="Label_skontroban_levo" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_irattarba_kuldott" runat="server" Text="Irattárba&nbsp;küldött&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_irattarba_kuldott" runat="server">
                                                                                    <asp:Label ID="Label_irattarba_kuldott" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_kolcsonzott" runat="server" Text="Kölcsönzött&nbsp;ügyiratok&nbsp;száma:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kolcsonzott" runat="server">
                                                                                    <asp:Label ID="Label_kolcsonzott" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="uralpElvalasztoSor">
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_ugyiratok_ugyintezonkenti_atlag" runat="server" Text="Átlagos&nbsp;ügyiratszám&nbsp;ügyintézõnként:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:Label ID="Label_ugyiratok_ugyintezonkenti_atlag" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td>
                                                                            </td>
                                                                        </tr>
<%--                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td>
                                                                            </td>
                                                                        </tr> --%>
                                                                            <tr class="urlapSor_kicsi">
                                                                                <td class="mrUrlapCaptionBal">
                                                                                    <asp:Label ID="Label_header_lejart_hatarideju" runat="server" Text="Lejárt&nbsp;határidejû&nbsp;ügyiratok:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_hatarideju" runat="server">
                                                                                        <asp:Label ID="Label_lejart_hatarideju" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_1nap" runat="server" Text="1&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_1nap" runat="server">
                                                                                        <asp:Label ID="Label_lejart_1nap" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_2_5nap" runat="server" Text="2-5&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_2_5nap" runat="server">
                                                                                        <asp:Label ID="Label_lejart_2_5nap" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_6_10nap" runat="server" Text="6-10&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_6_10nap" runat="server">
                                                                                        <asp:Label ID="Label_lejart_6_10nap" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_11_15nap" runat="server" Text="11-15&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_11_15nap" runat="server">
                                                                                        <asp:Label ID="Label_lejart_11_15nap" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                            <tr class="urlapElvalasztoSor">
                                                                                <td class="mrUrlapCaptionBal_indent">
                                                                                    <asp:Label ID="Label_header_lejart_15napfelett" runat="server" Text="Több&nbsp;mint&nbsp;15&nbsp;napja:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezoCenterBold">
                                                                                    <asp:HyperLink ID="HyperLink_lejart_15napfelett" runat="server">
                                                                                        <asp:Label ID="Label_lejart_15napfelett" runat="server" Text="0"></asp:Label>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td width="100%" /> <%-- az érték oszlop közelebb nyomása a címkékhez --%>
                                                                            </tr>
                                                                         <tr class="urlapElvalasztoSor">
                                                                            <td colspan="3"> </td>
                                                                        </tr>                                                                                                                                                
                                                                        <tr class="urlapSor_kicsi">
                                                                            <td class="mrUrlapCaptionBal_wide">
                                                                                <asp:Label ID="Label_header_kritikus_hatarideju" runat="server" Text="Kritikus&nbsp;határidejû&nbsp;ügyiratok:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_hatarideju" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_hatarideju" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_0nap" runat="server" Text="0&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_0nap" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_0nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_1nap" runat="server" Text="1&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_1nap" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_1nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_2_5nap" runat="server" Text="2-5&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_2_5nap" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_2_5nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="urlapElvalasztoSor">
                                                                            <td class="mrUrlapCaptionBal_indent">
                                                                                <asp:Label ID="Label_header_kritikus_6_10nap" runat="server" Text="6-10&nbsp;nap:"></asp:Label>
                                                                            </td>
                                                                            <td class="mrUrlapMezoCenterBold">
                                                                                <asp:HyperLink ID="HyperLink_kritikus_6_10nap" runat="server">
                                                                                    <asp:Label ID="Label_kritikus_6_10nap" runat="server" Text="0"></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                             
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </eUI:eFormPanel>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
</asp:Content>
