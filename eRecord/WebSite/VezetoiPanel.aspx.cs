using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

using System.Drawing;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.IO;
using System.Collections.Generic;
using System.Linq;

public partial class VezetoiPanel : Contentum.eUtility.UI.PageBase
{
    // ha true, a linkekre bal eg�rgombbal kattintva �j ablak ny�lik,
    // ha false, akkor nem - jobb eg�rgombbal kattintva a b�ng�sz� context men�je szerinti v�laszt�s mindk�t esetben
    private const bool bHyperLinkTargetNewWindow = false;

    private string chartDirectoryName = "";

    #region Utils
    protected DateTime GetMaxDate(DateTime firstDate, DateTime secondDate)
    {
        return (firstDate > secondDate) ? firstDate : secondDate;
    }

    // delete images older than 10 minutes from ChartImages folder
    private void deleteOldPlotImages(string ChartDirectoryName)
    {
        //DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath("/" + ChartDirectoryName));
        try
        {
            DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath(".") + "/" + ChartDirectoryName);
            foreach (FileInfo myFileInfo in myDirectoryInfo.GetFiles())
            {
                if (myFileInfo.LastWriteTime.AddMinutes(10) < DateTime.Now)
                {
                    myFileInfo.Delete();
                }
            }
        }
        catch (Exception ex)
        {
            //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Delete", ex);
            Exception e = new Exception("Path: " + ChartDirectoryName + "<br />Operation: File Delete", ex);
            throw e;    // throw it with the additional message info
        }
    }
    #endregion Utils

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "VezetoiPanelView");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        if (!IsPostBack)
        {
            FillAlosztalyok();
        }
    }

    private void FillAlosztalyok()
    {
        KRT_CsoportokService svc = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam xpm = UI.SetExecParamDefault(Page);
        Result res = null;
        try
        {
            var vezetettCsoportok = Contentum.eUtility.Csoportok.GetFelhasznaloVezetettCsoportjai(Page, xpm.Felhasznalo_Id);
            if (vezetettCsoportok.Count > 0)
            {
                KRT_CsoportokSearch sch = new KRT_CsoportokSearch();
                sch.Tipus.NotEquals(KodTarak.CSOPORTTIPUS.Dolgozo);
                sch.Id.NotEquals(xpm.FelhasznaloSzervezet_Id);
                sch.OrderBy = "Krt_Csoportok.Nev ASC";
                sch.Id.AndGroup("111");
                sch.Id.In(vezetettCsoportok);
                res = svc.GetAll(xpm, sch);
            }
        }
        catch (Contentum.eUtility.ResultException rx)
        {
            res = rx.GetResult();
        }

        if (res != null)
        {
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
            else
            {
                if (res.GetCount > 0)
                {
                    List<Osztaly> osztalyok = new List<Osztaly>();
                    for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
                    {
                        // �gyfelel�sre r�gt�n sz�k�t�nk is:
                        KRT_CsoportokSearch search_csoportok = new KRT_CsoportokSearch();
                        search_csoportok.Tipus.Value = "0";
                        search_csoportok.Tipus.Operator = Query.Operators.equals;

                        Result result_subCsoportok = svc.GetAllSubCsoport(xpm, res.Ds.Tables[0].Rows[i]["Id"].ToString(), false, search_csoportok);
                        if (result_subCsoportok.IsError)
                        {
                            continue;
                        }

                        Osztaly osztaly = new Osztaly();
                        osztaly.Id = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                        osztaly.Nev = res.Ds.Tables[0].Rows[i]["Nev"].ToString();
                        osztalyok.Add(osztaly);

                        if (result_subCsoportok.Ds.Tables != null && result_subCsoportok.Ds.Tables[0].Rows.Count > 0)
                        {
                            //DataTable t = result_subCsoportok.Ds.Tables[0].Clone();
                            for (int j = 0; j < result_subCsoportok.Ds.Tables[0].Rows.Count; j++)
                            {
                                if (!osztalyok.Any(x => x.Id == result_subCsoportok.Ds.Tables[0].Rows[j]["Id"].ToString()))
                                {
                                    Osztaly o = new Osztaly();
                                    o.Id = result_subCsoportok.Ds.Tables[0].Rows[j]["Id"].ToString();
                                    o.Nev = HttpUtility.HtmlDecode("&nbsp;&nbsp;" + result_subCsoportok.Ds.Tables[0].Rows[j]["Nev"].ToString());
                                    osztalyok.Add(o);
                                }
                            }
                        }
                    }

                    bool dropdown = osztalyok.Count > 1;
                    if (osztalyok.Count > 1)
                    {
                        dropdown = true;
                        Osztaly ures = new Osztaly();
                        ures.Id = "";
                        ures.Nev = "";//HttpUtility.HtmlDecode("&nbsp;&nbsp;");
                        osztalyok.Insert(0, ures);
                    }

                    ddListAlosztalyok.DataSource = osztalyok;
                    ddListAlosztalyok.DataBind();                    
                    
                    ddListAlosztalyok.Visible = dropdown;

                    labelVezetettAlosztaly.Text = AlSzervezetNev;
                    labelVezetettAlosztaly.Visible = !dropdown;
                }
                else
                {
                    trAlosztalyok.Visible = false;
                }
            }
        }
        else
        {
            ddListAlosztalyok.Visible = false;
        }
    }

    private class Osztaly
    {
        public string Id
        { get; set; }
        public string Nev
        { get; set; }
    }

    protected void ddListAlosztalyok_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReloadDatas_SzervezetiStatisztika();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        chartDirectoryName = "images/TempChartImages";

        ListHeader1.HeaderLabel = Resources.List.VezetoiPanelHeaderTitle;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;
        #region Baloldali funkci�gombok kiszed�se
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        #endregion
        ListHeader1.CustomSearchObjectSessionName = "----------";

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        //TextBox t = new TextBox();
        //t.Attributes.Add("onchanged", UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);


        //ListHeader1.RefreshOnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);

        if (!IsPostBack)
        {
            //TextBox_UtobbiXNap.Text = "90";
            var today = DateTime.Now;
            ErvenyessegCalendarControl1.ErvKezd = today.AddDays(-30).ToString();
            ErvenyessegCalendarControl1.ErvVege = today.ToString();
            ReloadDatas_SzervezetiStatisztika();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (TabContainer1.ActiveTab.Equals(SzervezetiStatisztikaTabPanel))
        {
            ListHeader1.RefreshOnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);
            ImageButton_Refresh.OnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);
        }
    }

    protected void SzervezetiStatisztikaUpdatePanel_Load(object sender, EventArgs e)
    {
        if (TabContainer1.ActiveTab.Equals(SzervezetiStatisztikaTabPanel))
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refresh:
                        ReloadDatas_SzervezetiStatisztika();
                        break;
                }
            }
        }
    }

    protected void UgyUgyiratokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
    }

    public void ReFresh_Charts(DataTable table)
    {
        // Elvileg egy sort tartalmaz a ReloadDatas_SzervezetiStatisztika eredm�ny�b�l
        if (table.Rows.Count == 1)
        {
            // make sure that output directory exists
            //string chartChartDirectoryName = "ChartImages";

            #region Image output directory (check if exists and create if does not)
            string chartPhysicalPath = Server.MapPath(".") + "/" + chartDirectoryName;

            try
            {
                if (!Directory.Exists(chartPhysicalPath))
                {
                    Directory.CreateDirectory(chartPhysicalPath);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("ReFresh_Charts", ex);
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    String.Format(Resources.Error.UI_Chart_CreateDirectoryError, chartPhysicalPath));
                ErrorUpdatePanel.Update();
                // make all charts unvisible if chart image directory does not exists
                PostAndFilesBarChart.Visible = false;
                FilesByStatusPieChart.Visible = false;
                ExpiredBarChart.Visible = false;
                TermCriticalBarChart.Visible = false;
                return;
            }
            #endregion Image output directory (check if exists and create if does not)

            // generate charts with exception handling
            try
            {
                // set directory names
                #region Common base settings
                PostAndFilesBarChart.ChartDirectoryName = chartDirectoryName;
                FilesByStatusPieChart.ChartDirectoryName = chartDirectoryName;
                ExpiredBarChart.ChartDirectoryName = chartDirectoryName;
                TermCriticalBarChart.ChartDirectoryName = chartDirectoryName;
                // format charts and fill them with data
                DataRow row = table.Rows[0];
                double value;
                Random rnd = new Random(4); // to generate chart colors
                // settings for the charts
                int chartImageWidth = 250;
                int chartImageHeight = 170;
                Font chartTitleFont = new Font("Verdana", 8, FontStyle.Bold);
                Font chartLabelFont = new Font("Arial Narrow", 8);
                StringAlignment chartTitleAlignment = StringAlignment.Center;
                int chartPanelPadding = 7;
                int chartPanelPaddingTop = 15;
                Point chartLabelPosition = new Point(15, 20);
                string chartLabelExtension = "db";
                bool chartLabelShowPercent = false;
                int chartLabelDecimalPlaces = 0;
                int chartChartPaddingLeft = 120;
                int chartChartPaddingRight = 10;
                int chartChartPaddingBottom = 10;

                Color chartFromBgColorTopRow = Color.FromArgb(254, 254, 254);
                Color chartFromBgColorBottomRow = Color.FromArgb(233, 233, 233);
                Color chartToBgColor = Color.FromArgb(233, 233, 233);
                SolidBrush chartTitleBrush = new SolidBrush(Color.FromArgb(57, 95, 128));
                float chartGradientAngle = 90.0f;

                int chartBarWidthPercent = 80;

                PostAndFilesBarChart.Visible = true;
                FilesByStatusPieChart.Visible = true;
                ExpiredBarChart.Visible = true;
                TermCriticalBarChart.Visible = true;
                #endregion Common base settings

                #region K�ldem�nyek �s �j �gyiratok (PostAndFilesBarChart)
                PostAndFilesBarChart.ChartType = eRecordComponent_BarChart.ChartTypes.Bar;
                PostAndFilesBarChart.ChartTitle = "Forgalom";
                PostAndFilesBarChart.ImageAlt = "Forgalom";
                PostAndFilesBarChart.ImageWidth = chartImageWidth;
                PostAndFilesBarChart.ImageHeight = chartImageHeight;
                PostAndFilesBarChart.TitleStringFormat.Alignment = chartTitleAlignment;
                PostAndFilesBarChart.TitleFont = chartTitleFont;
                PostAndFilesBarChart.LabelFont = chartLabelFont;
                PostAndFilesBarChart.PanelPadding = chartPanelPadding;
                PostAndFilesBarChart.PanelPaddingTop = chartPanelPaddingTop;
                PostAndFilesBarChart.LabelPosition = chartLabelPosition;
                PostAndFilesBarChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                PostAndFilesBarChart.LabelExtension = chartLabelExtension;
                //PostAndFilesPieChart.LabelShowPercent = chartLabelShowPercent;
                PostAndFilesBarChart.ChartPaddingRight = chartChartPaddingRight + 25;
                PostAndFilesBarChart.ChartPaddingBottom = chartChartPaddingBottom;
                PostAndFilesBarChart.ChartPaddingLeft = 10;

                PostAndFilesBarChart.FromPanelBgColor = chartFromBgColorTopRow;
                PostAndFilesBarChart.ToPanelBgColor = chartToBgColor;
                PostAndFilesBarChart.TitleBrush = chartTitleBrush;
                PostAndFilesBarChart.GradientAngle = chartGradientAngle;

                PostAndFilesBarChart.BarWidthPercent = chartBarWidthPercent;

                PostAndFilesBarChart.ChartType = eRecordComponent_BarChart.ChartTypes.Bar;
                // add chart elements
                if (double.TryParse(row["kuldemenyek"].ToString(), out value))
                {
                    PostAndFilesBarChart.addChartElement("�rkezett k�ldem�nyek", value, Color.Firebrick);
                }

                if (double.TryParse(row["ugyiratok"].ToString(), out value))
                {
                    PostAndFilesBarChart.addChartElement("�j �gyiratok", value, Color.FromArgb(203, 252, 133));
                }
                PostAndFilesBarChart.generateChartImage();
                #endregion K�ldem�nyek �s �j �gyiratok (PostAndFilesBarChart)

                #region �gyiratok �llapot szerint (FilesByStatusPieChart)
                FilesByStatusPieChart.ChartTitle = "�gyiratok �llapot szerint";
                FilesByStatusPieChart.ImageAlt = "�gyiratok �llapot szerint";
                FilesByStatusPieChart.ImageWidth = chartImageWidth;
                FilesByStatusPieChart.ImageHeight = chartImageHeight;
                FilesByStatusPieChart.TitleStringFormat.Alignment = chartTitleAlignment;
                FilesByStatusPieChart.TitleFont = chartTitleFont;
                FilesByStatusPieChart.LabelFont = chartLabelFont;
                FilesByStatusPieChart.PanelPadding = chartPanelPadding;
                FilesByStatusPieChart.PanelPaddingTop = chartPanelPaddingTop;
                FilesByStatusPieChart.LabelPosition = chartLabelPosition;
                FilesByStatusPieChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                FilesByStatusPieChart.LabelExtension = chartLabelExtension;
                FilesByStatusPieChart.LabelShowPercent = chartLabelShowPercent;
                FilesByStatusPieChart.ChartPaddingRight = chartChartPaddingRight;
                FilesByStatusPieChart.ChartPaddingBottom = chartChartPaddingBottom;
                FilesByStatusPieChart.ChartPaddingLeft = chartChartPaddingLeft;

                FilesByStatusPieChart.FromPanelBgColor = chartFromBgColorTopRow;
                FilesByStatusPieChart.ToPanelBgColor = chartToBgColor;
                FilesByStatusPieChart.TitleBrush = chartTitleBrush;
                FilesByStatusPieChart.GradientAngle = chartGradientAngle;
                // add chart elements
                if (double.TryParse(row["ugyintezes_alatt"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("�gyint�z�s alatt", value, Color.FromArgb(203, 252, 133));
                }
                // CR3133: Szign�l�sra v�r� �s a Szign�lt sorok ne jelenjenek meg a Vezet�i panelen
                //if (double.TryParse(row["szignalasra_varo"].ToString(), out value))
                //{
                //    FilesByStatusPieChart.addChartElement("Szign�l�sra v�r�", value, Color.FromArgb(94, 2, 9));
                //}

                if (double.TryParse(row["elintezett"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Elint�zett", value, Color.SlateBlue);
                }
                // CR3133: Szign�l�sra v�r� �s a Szign�lt sorok ne jelenjenek meg a Vezet�i panelen 
                //if (double.TryParse(row["szignalt"].ToString(), out value))
                //{
                //    FilesByStatusPieChart.addChartElement("Szign�lt", value, Color.Peru);
                //}

                if (double.TryParse(row["lezart"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Lez�rt", value, Color.FromArgb(172, 197, 219));
                }

                if (double.TryParse(row["skontroban_levo"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Skontr�ban l�v�", value, Color.Gold);
                }

                if (double.TryParse(row["irattarba_kuldott"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Iratt�rba k�ld�tt", value, Color.Indigo);
                }

                if (double.TryParse(row["kolcsonzott"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("K�lcs�nz�tt", value, Color.SteelBlue);
                }
                FilesByStatusPieChart.generateChartImage();
                #endregion �gyiratok �llapot szerint (FilesByStatusPieChart)

                #region Lej�rt �gyiratok (ExpiredBarChart)
                ExpiredBarChart.ChartType = eRecordComponent_BarChart.ChartTypes.Column;
                ExpiredBarChart.ChartTitle = "Lej�rt hat�ridej� �gyiratok";
                ExpiredBarChart.ImageAlt = "Lej�rt hat�ridej� �gyiratok";
                ExpiredBarChart.ImageWidth = chartImageWidth;
                ExpiredBarChart.ImageHeight = chartImageHeight;
                ExpiredBarChart.TitleStringFormat.Alignment = chartTitleAlignment;
                ExpiredBarChart.TitleFont = chartTitleFont;
                ExpiredBarChart.LabelFont = chartLabelFont;
                ExpiredBarChart.PanelPadding = chartPanelPadding;
                ExpiredBarChart.PanelPaddingTop = chartPanelPaddingTop;
                ExpiredBarChart.LabelPosition = chartLabelPosition;
                ExpiredBarChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                ExpiredBarChart.LabelExtension = chartLabelExtension;

                ExpiredBarChart.ChartPaddingRight = chartChartPaddingRight;
                ExpiredBarChart.ChartPaddingBottom = chartChartPaddingBottom;
                ExpiredBarChart.ChartPaddingLeft = 70;
                ExpiredBarChart.ChartPaddingTop = 30;

                ExpiredBarChart.FromPanelBgColor = chartFromBgColorBottomRow;
                ExpiredBarChart.ToPanelBgColor = chartToBgColor;
                ExpiredBarChart.TitleBrush = chartTitleBrush;
                ExpiredBarChart.GradientAngle = chartGradientAngle;

                ExpiredBarChart.BarWidthPercent = chartBarWidthPercent;

                // add chart elements

                if (double.TryParse(row["lejart_15napfelett"].ToString(), out value))
                {
                    ExpiredBarChart.addChartElement("15 nap felett", value, Color.DarkRed);
                }

                if (double.TryParse(row["lejart_11_15nap"].ToString(), out value))
                {
                    ExpiredBarChart.addChartElement("11-15 napja", value, Color.Firebrick);
                }

                if (double.TryParse(row["lejart_6_10nap"].ToString(), out value))
                {
                    ExpiredBarChart.addChartElement("6-10 napja", value, Color.Orange);
                }

                if (double.TryParse(row["lejart_2_5nap"].ToString(), out value))
                {
                    ExpiredBarChart.addChartElement("2-5 napja", value, Color.Gold);
                }

                if (double.TryParse(row["lejart_1nap"].ToString(), out value))
                {
                    ExpiredBarChart.addChartElement("1 napja", value, Color.FromArgb(255, 255, 153));
                }

                ExpiredBarChart.generateChartImage();
                #endregion Lej�rt �gyiratok (ExpiredBarChart)

                #region Kritikus hat�ridej� �gyiratok (TermCriticalBarChart)
                TermCriticalBarChart.ChartType = eRecordComponent_BarChart.ChartTypes.Column;
                TermCriticalBarChart.ChartTitle = "Kritikus hat�ridej� �gyiratok";
                TermCriticalBarChart.ImageAlt = "Kritikus hat�ridej� �gyiratok";
                TermCriticalBarChart.ImageWidth = chartImageWidth;
                TermCriticalBarChart.ImageHeight = chartImageHeight;
                TermCriticalBarChart.TitleStringFormat.Alignment = chartTitleAlignment;
                TermCriticalBarChart.TitleFont = chartTitleFont;
                TermCriticalBarChart.LabelFont = chartLabelFont;
                TermCriticalBarChart.PanelPadding = chartPanelPadding;
                TermCriticalBarChart.PanelPaddingTop = chartPanelPaddingTop;
                TermCriticalBarChart.LabelPosition = chartLabelPosition;
                TermCriticalBarChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                TermCriticalBarChart.LabelExtension = chartLabelExtension;

                TermCriticalBarChart.ChartPaddingRight = chartChartPaddingRight;
                TermCriticalBarChart.ChartPaddingBottom = chartChartPaddingBottom;
                TermCriticalBarChart.ChartPaddingLeft = 70;
                TermCriticalBarChart.ChartPaddingTop = 30;

                TermCriticalBarChart.FromPanelBgColor = chartFromBgColorBottomRow;
                TermCriticalBarChart.ToPanelBgColor = chartToBgColor;
                TermCriticalBarChart.TitleBrush = chartTitleBrush;
                TermCriticalBarChart.GradientAngle = chartGradientAngle;

                TermCriticalBarChart.BarWidthPercent = chartBarWidthPercent;

                // add chart elements
                if (double.TryParse(row["kritikus_0nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("0 nap", value, Color.DarkRed);
                }

                if (double.TryParse(row["kritikus_1nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("1 nap", value, Color.Firebrick);
                }

                if (double.TryParse(row["kritikus_2_5nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("2-5 nap", value, Color.Orange);
                }

                if (double.TryParse(row["kritikus_6_10nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("6-10 nap", value, Color.Gold);
                }
                TermCriticalBarChart.generateChartImage();
                #endregion Kritikus hat�ridej� �gyiratok (TermCriticalBarChart)

                // TODO: megoldani
                // workaround: a k�p src-ben hib�san megjelenik egy "/" az �tvonal el�tt,
                //             ez�rt a k�p nem jelenik meg
                PostAndFilesBarChart.Src = Page.ResolveUrl(".") + PostAndFilesBarChart.Src.Substring(1);
                FilesByStatusPieChart.Src = Page.ResolveUrl(".") + FilesByStatusPieChart.Src.Substring(1);
                ExpiredBarChart.Src = Page.ResolveUrl(".") + ExpiredBarChart.Src.Substring(1);
                TermCriticalBarChart.Src = Page.ResolveUrl(".") + TermCriticalBarChart.Src.Substring(1);
            }
            catch (Exception ex)
            {
                string message = Resources.Error.UI_Chart_SaveError;
                message += "<br />" + ex.Message;
                Exception innerex = ex.InnerException;
                while (innerex != null)
                {
                    message += "<br />" + innerex.GetType().ToString() + ": " + innerex.Message;
                    innerex = innerex.InnerException;
                }

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, message);
                ErrorUpdatePanel.Update();
                // make all charts unvisible if chart image directory does not exists
                PostAndFilesBarChart.Visible = false;
                FilesByStatusPieChart.Visible = false;
                ExpiredBarChart.Visible = false;
                TermCriticalBarChart.Visible = false;
                return;
            }

            #region Try delete old images
            try
            {
                // possible Exception types
                // System.IO.IOException
                // System.Security.SecurityException
                // System.UnauthorizedAccessException
                deleteOldPlotImages(chartDirectoryName);
            }
            catch (Exception ex)
            {
                string message = Resources.Error.UI_Chart_DeleteError;
                message += "<br />" + ex.Message;
                Exception innerex = ex.InnerException;
                while (innerex != null)
                {
                    message += "<br />" + innerex.GetType().ToString() + ": " + innerex.Message;
                    innerex = innerex.InnerException;
                }

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, message);
                ErrorUpdatePanel.Update();
            }
            #endregion Try delete old images
        }   // if
    }

    private string GetReadableWhere(string readableWhere)
    {
        if (!String.IsNullOrEmpty(AlSzervezetNev))
        {
            if (!String.IsNullOrEmpty(readableWhere))
            {
                return String.Format("{0}&nbsp;({1})", readableWhere, AlSzervezetNev);
            }

            return String.Format("({0})", AlSzervezetNev);
        }

        return readableWhere;
    }

    protected void SetHyperLinks_SzervezetiStatisztika(DateTime startDate, DateTime endDate)
    {
        // flaggel szab�lyozhat�, hogy �j ablakokat nyisson-e a linkekre kattintva
        string target = "";
        if (bHyperLinkTargetNewWindow == true)
        {
            target = "_blank";
        }

        // CR3289 Vezet�i panel kezel�s
        //string QS_Startup = QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel;

        string UgyUgyiratokListPath = "~/UgyUgyiratokList.aspx";
        string KuldKuldemenyekListPath = "~/KuldKuldemenyekList.aspx";
        string IraIratokListPath = "~/IraIratokList.aspx";

        //DateTime today = DateTime.Today;
        //DateTime now = DateTime.Now;

        DateTime now = endDate;
        DateTime today = endDate.Date;
        //DateTime startDate = today.AddDays(-nPastDays);
        // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
        // CR kapcs�n der�lt ki, hogy a lev�logat�sokn�l nem j� a maxdate be�ll�t�s-> sp_EREC_�gyiratokGetAllWithExtension  <= n�z, a hat�rid�k pedig �ra-perc-ben van megadva
        // ha a max �s mindate egyenl�, akkor meg csak egyenl�t n�z �ra-percben
        DateTime todayEnd = endDate.Date.AddDays(1.0).AddMilliseconds(-1.0);

        // el�re kisz�m�tjuk a maximumokat, csak az "�j" k�ldem�nyek �s �gyiratok eset�n �rdekes,
        // ha az id�ablak kisebb, mint a lek�rdezett hat�rok
        string _3DaysBefore = GetMaxDate(today.AddDays(-3.0), startDate).ToString();
        string _7DaysBefore = GetMaxDate(today.AddDays(-7.0), startDate).ToString();

        Kuldemenyek.FilterObject kuldemenyekFilterObject = new Kuldemenyek.FilterObject();
        kuldemenyekFilterObject.AlSzervezetId = AlSzervezetId;
        Ugyiratok.FilterObject ugyiratokFilterObject = new Ugyiratok.FilterObject();
        ugyiratokFilterObject.AlSzervezetId = AlSzervezetId;
        Iratok.FilterObject iratokFilterObject = new Iratok.FilterObject();
        iratokFilterObject.AlSzervezetId = AlSzervezetId;

        //// csak a szervezetbe tartoz� felel�s�k
        //string SzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        //kuldemenyekFilterObject.SzervezetId = SzervezetId;
        //ugyiratokFilterObject.SzervezetId = SzervezetId;
        //iratokFilterObject.SzervezetId = SzervezetId;

        #region k�ldem�nyek
        kuldemenyekFilterObject.MinDate = startDate.ToString();
        kuldemenyekFilterObject.MaxDate = now.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek);
        string QS_kuldemenyek = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = today.ToString();
        kuldemenyekFilterObject.MaxDate = now.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_ma);
        string QS_kuldemenyek_ma = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = _3DaysBefore;
        //        kuldemenyekFilterObject.MaxDate = today.ToString();
        kuldemenyekFilterObject.MaxDate = todayEnd.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_1_3nap);
        string QS_kuldemenyek_1_3nap = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = _7DaysBefore;
        kuldemenyekFilterObject.MaxDate = _3DaysBefore;
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_4_7nap);
        string QS_kuldemenyek_4_7nap = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = startDate.ToString();
        kuldemenyekFilterObject.MaxDate = _7DaysBefore;
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_7napfelett);
        string QS_kuldemenyek_7napfelett = kuldemenyekFilterObject.QsSerialize();

        // linkek
        // CR3289 Vezet�i panel kezel�s
        // QS_Startup 
        HyperLink_kuldemenyek.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek + "&" + GetStartupString("Kuldemeny"); // QS_Startup;
        HyperLink_kuldemenyek_ma.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek_ma + "&" + GetStartupString("Kuldemeny"); // QS_Startup;
        HyperLink_kuldemenyek_1_3nap.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek_1_3nap + "&" + GetStartupString("Kuldemeny"); // QS_Startup;
        HyperLink_kuldemenyek_4_7nap.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek_4_7nap + "&" + GetStartupString("Kuldemeny"); // QS_Startup;
        HyperLink_kuldemenyek_7napfelett.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek_7napfelett + "&" + GetStartupString("Kuldemeny"); // QS_Startup;

        HyperLink_kuldemenyek.Target = target;
        HyperLink_kuldemenyek_ma.Target = target;
        HyperLink_kuldemenyek_1_3nap.Target = target;
        HyperLink_kuldemenyek_4_7nap.Target = target;
        HyperLink_kuldemenyek_7napfelett.Target = target;

        // linkek feliratai
        Label_kuldemenyek.Font.Underline = true;
        Label_kuldemenyek_ma.Font.Underline = true;
        Label_kuldemenyek_1_3nap.Font.Underline = true;
        Label_kuldemenyek_4_7nap.Font.Underline = true;
        Label_kuldemenyek_7napfelett.Font.Underline = true;
        #endregion k�ldem�nyek


        #region �gyiratok �sszesen
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.ClearMaxDate();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere("");
        string QS_ugyiratok_osszesen = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyiratok_osszesen.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_osszesen + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_ugyiratok_osszesen.Target = target;

        // linkek feliratai
        Label_ugyiratok_osszesen.Font.Underline = true;
        #endregion

        #region �gyiratok l�trehoz�s szerint
        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Letrehozas;
        ugyiratokFilterObject.MinDate = startDate.ToString();
        ugyiratokFilterObject.MaxDate = now.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok);
        string QS_ugyiratok = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = now.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_ma);
        string QS_ugyiratok_ma = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = _3DaysBefore;
        ugyiratokFilterObject.MaxDate = today.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_1_3nap);
        string QS_ugyiratok_1_3nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = _7DaysBefore;
        ugyiratokFilterObject.MaxDate = _3DaysBefore;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_4_7nap);
        string QS_ugyiratok_4_7nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = startDate.ToString();
        ugyiratokFilterObject.MaxDate = _7DaysBefore;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_7napfelett);
        string QS_ugyiratok_7napfelett = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyiratok.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_ugyiratok_ma.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_ma + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_ugyiratok_1_3nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_1_3nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_ugyiratok_4_7nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_4_7nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_ugyiratok_7napfelett.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_7napfelett + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_ugyiratok.Target = target;
        HyperLink_ugyiratok_ma.Target = target;
        HyperLink_ugyiratok_1_3nap.Target = target;
        HyperLink_ugyiratok_4_7nap.Target = target;
        HyperLink_ugyiratok_7napfelett.Target = target;

        // linkek feliratai
        Label_ugyiratok.Font.Underline = true;
        Label_ugyiratok_ma.Font.Underline = true;
        Label_ugyiratok_1_3nap.Font.Underline = true;
        Label_ugyiratok_4_7nap.Font.Underline = true;
        Label_ugyiratok_7napfelett.Font.Underline = true;
        #endregion �gyiratok l�trehoz�s szerint

        #region iratok alsz�mra
        iratokFilterObject.MinDate = today.ToString();
        iratokFilterObject.MaxDate = now.ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_ma);
        string QS_iratok_alszamra_ma = iratokFilterObject.QsSerialize();

        iratokFilterObject.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_bejovo_ma);
        string QS_iratok_alszamra_bejovo_ma = iratokFilterObject.QsSerialize();

        iratokFilterObject.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_belso_ma);
        string QS_iratok_alszamra_belso_ma = iratokFilterObject.QsSerialize();

        // linkek
        HyperLink_iratok_alszamra_ma.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_ma + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_iratok_alszamra_bejovo_ma.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_bejovo_ma + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_iratok_alszamra_belso_ma.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_belso_ma + "&" + GetStartupString("Irat"); // QS_Startup;

        HyperLink_iratok_alszamra_ma.Target = target;
        HyperLink_iratok_alszamra_bejovo_ma.Target = target;
        HyperLink_iratok_alszamra_belso_ma.Target = target;

        // linkek feliratai
        Label_iratok_alszamra_ma.Font.Underline = true;
        Label_iratok_alszamra_bejovo_ma.Font.Underline = true;
        Label_iratok_alszamra_belso_ma.Font.Underline = true;
        #endregion iratok alsz�mra

        // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
        #region lej�rt iratok
        iratokFilterObject.Clear();
        iratokFilterObject.ClearAllapot();
        iratokFilterObject.ClearExcludeAllapot();

        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Sztornozott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Atiktatott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat);

        iratokFilterObject.SpecialFilter = Iratok.FilterObject.SpecialFilterType.Lejart;
        iratokFilterObject.ClearMinDate();
        //CR3133 : �sszes�t�sn�l az adott d�tum� t�teleket nem vette bele, m�g a r�szletez� beleveszi...
        //A mai nap-1-re kell lek�rdezni mert a sp_EREC_IraIratokGetAllWithExtension <= -vel �ll�tja �ssze a lek�rdez�st
        //ugyiratokFilterObject.MaxDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_hatarideju);
        string QS_lejart_hatarideju_Irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(-1.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_1nap);
        string QS_lejart_1nap_Irat = iratokFilterObject.QsSerialize();


        iratokFilterObject.MinDate = today.AddDays(-5.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-2.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_2_5nap);
        string QS_lejart_2_5nap_Irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(-10.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-6.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_6_10nap);
        string QS_lejart_6_10nap_Irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(-15.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-11.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_11_15nap);
        string QS_lejart_11_15nap_Irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.ClearMinDate();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-16.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_15napfelett);
        string QS_lejart_15napfelett_Irat = iratokFilterObject.QsSerialize();

        // linkek
        HyperLink_lejart_hatarideju_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_hatarideju_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_lejart_1nap_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_1nap_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_lejart_2_5nap_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_2_5nap_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_lejart_6_10nap_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_6_10nap_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_lejart_11_15nap_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_11_15nap_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_lejart_15napfelett_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_15napfelett_Irat + "&" + GetStartupString("Irat"); // QS_Startup;

        HyperLink_lejart_hatarideju_Irat.Target = target;
        HyperLink_lejart_1nap_Irat.Target = target;
        HyperLink_lejart_2_5nap_Irat.Target = target;
        HyperLink_lejart_6_10nap_Irat.Target = target;
        HyperLink_lejart_11_15nap_Irat.Target = target;
        HyperLink_lejart_15napfelett_Irat.Target = target;

        // linkek feliratai
        Label_lejart_hatarideju_Irat.Font.Underline = true;
        Label_lejart_1nap_Irat.Font.Underline = true;
        Label_lejart_2_5nap_Irat.Font.Underline = true;
        Label_lejart_6_10nap_Irat.Font.Underline = true;
        Label_lejart_11_15nap_Irat.Font.Underline = true;
        Label_lejart_15napfelett_Irat.Font.Underline = true;
        #endregion lej�rt iratok

        #region kritikus hat�ridej� iratok
        iratokFilterObject.Clear();
        iratokFilterObject.ClearAllapot();
        iratokFilterObject.ClearExcludeAllapot();

        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Sztornozott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Atiktatott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat);

        iratokFilterObject.SpecialFilter = Iratok.FilterObject.SpecialFilterType.Kritikus;
        iratokFilterObject.MinDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_hatarideju);
        string QS_kritikus_hatarideju_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_0nap);
        string QS_kritikus_0nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(1.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(1.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_1nap);
        string QS_kritikus_1nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(2.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(5.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_2_5nap);
        string QS_kritikus_2_5nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(6.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_6_10nap);
        string QS_kritikus_6_10nap_irat = iratokFilterObject.QsSerialize();

        // linkek
        HyperLink_kritikus_hatarideju_irat.NavigateUrl = IraIratokListPath + "?" + QS_kritikus_hatarideju_irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_kritikus_0nap_irat.NavigateUrl = IraIratokListPath + "?" + QS_kritikus_0nap_irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_kritikus_1nap_irat.NavigateUrl = IraIratokListPath + "?" + QS_kritikus_1nap_irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_kritikus_2_5nap_irat.NavigateUrl = IraIratokListPath + "?" + QS_kritikus_2_5nap_irat + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_kritikus_6_10nap_irat.NavigateUrl = IraIratokListPath + "?" + QS_kritikus_6_10nap_irat + "&" + GetStartupString("Irat"); // QS_Startup;

        HyperLink_kritikus_hatarideju_irat.Target = target;
        HyperLink_kritikus_0nap_irat.Target = target;
        HyperLink_kritikus_1nap_irat.Target = target;
        HyperLink_kritikus_2_5nap_irat.Target = target;
        HyperLink_kritikus_6_10nap_irat.Target = target;

        // linkek ***
        Label_kritikus_hatarideju_irat.Font.Underline = true;
        Label_kritikus_0nap_irat.Font.Underline = true;
        Label_kritikus_1nap_irat.Font.Underline = true;
        Label_kritikus_2_5nap_irat.Font.Underline = true;
        Label_kritikus_6_10nap_irat.Font.Underline = true;
        #endregion kritikus hat�ridej� iratok

        #region lej�rt �gyiratok
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Lejart;
        ugyiratokFilterObject.ClearMinDate();
        //CR3133 : �sszes�t�sn�l az adott d�tum� t�teleket nem vette bele, m�g a r�szletez� beleveszi...
        //A mai nap-1-re kell lek�rdezni mert a sp_EREC_UgyUgyiratokGetAllWithExtension <= -vel �ll�tja �ssze a lek�rdez�st
        //ugyiratokFilterObject.MaxDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_hatarideju);
        string QS_lejart_hatarideju = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-1.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_1nap);
        string QS_lejart_1nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-5.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-2.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_2_5nap);
        string QS_lejart_2_5nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-10.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-6.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_6_10nap);
        string QS_lejart_6_10nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-15.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-11.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_11_15nap);
        string QS_lejart_11_15nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-16.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_15napfelett);
        string QS_lejart_15napfelett = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_lejart_hatarideju.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_hatarideju + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_1nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_1nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_2_5nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_2_5nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_6_10nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_6_10nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_11_15nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_11_15nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_15napfelett.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_15napfelett + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_lejart_hatarideju.Target = target;
        HyperLink_lejart_1nap.Target = target;
        HyperLink_lejart_2_5nap.Target = target;
        HyperLink_lejart_6_10nap.Target = target;
        HyperLink_lejart_11_15nap.Target = target;
        HyperLink_lejart_15napfelett.Target = target;

        // linkek feliratai
        Label_lejart_hatarideju.Font.Underline = true;
        Label_lejart_1nap.Font.Underline = true;
        Label_lejart_2_5nap.Font.Underline = true;
        Label_lejart_6_10nap.Font.Underline = true;
        Label_lejart_11_15nap.Font.Underline = true;
        Label_lejart_15napfelett.Font.Underline = true;
        #endregion lej�rt �gyiratok

        #region �gyiratok �llapot szerint
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.ClearMaxDate();
        ugyiratokFilterObject.SpecialFilter = Contentum.eRecord.BaseUtility.Ugyiratok.FilterObject.SpecialFilterType.Letrehozas;
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyintezes_alatt);
        string QS_ugyintezes_alatt = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_szignalasra_varo);
        string QS_szignalasra_varo = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Elintezett);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_elintezett);
        string QS_elintezett = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_szignalt);
        string QS_szignalt = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lezart);
        string QS_lezart = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_skontroban_levo);
        string QS_skontroban_levo = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_irattarba_kuldott);
        string QS_irattarba_kuldott = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kolcsonzott);
        string QS_kolcsonzott = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyintezes_alatt.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyintezes_alatt + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_szignalasra_varo.NavigateUrl = UgyUgyiratokListPath + "?" + QS_szignalasra_varo + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_elintezett.NavigateUrl = UgyUgyiratokListPath + "?" + QS_elintezett + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_szignalt.NavigateUrl = UgyUgyiratokListPath + "?" + QS_szignalt + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lezart.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lezart + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_skontroban_levo.NavigateUrl = UgyUgyiratokListPath + "?" + QS_skontroban_levo + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_irattarba_kuldott.NavigateUrl = UgyUgyiratokListPath + "?" + QS_irattarba_kuldott + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kolcsonzott.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kolcsonzott + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_ugyintezes_alatt.Target = target;
        HyperLink_szignalasra_varo.Target = target;
        HyperLink_elintezett.Target = target;
        HyperLink_szignalt.Target = target;
        HyperLink_lezart.Target = target;
        HyperLink_skontroban_levo.Target = target;
        HyperLink_irattarba_kuldott.Target = target;
        HyperLink_kolcsonzott.Target = target;

        // linkek feliratai
        Label_ugyintezes_alatt.Font.Underline = true;
        Label_szignalasra_varo.Font.Underline = true;
        Label_elintezett.Font.Underline = true;
        Label_szignalt.Font.Underline = true;
        Label_lezart.Font.Underline = true;
        Label_skontroban_levo.Font.Underline = true;
        Label_irattarba_kuldott.Font.Underline = true;
        Label_kolcsonzott.Font.Underline = true;
        #endregion �gyiratok �llapotok szerint

        #region kritikus hat�ridej� �gyiratok
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Kritikus;
        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_hatarideju);
        string QS_kritikus_hatarideju = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_0nap);
        string QS_kritikus_0nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(1.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_1nap);
        string QS_kritikus_1nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(2.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(5.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_2_5nap);
        string QS_kritikus_2_5nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(6.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_6_10nap);
        string QS_kritikus_6_10nap = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_kritikus_hatarideju.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_hatarideju + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_0nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_0nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_1nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_1nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_2_5nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_2_5nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_6_10nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_6_10nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_kritikus_hatarideju.Target = target;
        HyperLink_kritikus_0nap.Target = target;
        HyperLink_kritikus_1nap.Target = target;
        HyperLink_kritikus_2_5nap.Target = target;
        HyperLink_kritikus_6_10nap.Target = target;

        // linkek feliratai
        Label_kritikus_hatarideju.Font.Underline = true;
        Label_kritikus_0nap.Font.Underline = true;
        Label_kritikus_1nap.Font.Underline = true;
        Label_kritikus_2_5nap.Font.Underline = true;
        Label_kritikus_6_10nap.Font.Underline = true;
        #endregion kritikus hat�ridej� �gyiratok


    }

    private string AlSzervezetId
    {
        get
        {
            return ddListAlosztalyok.SelectedValue.Trim();
        }
    }

    private string AlSzervezetNev
    {
        get
        {
            return (ddListAlosztalyok.SelectedItem != null) ? ddListAlosztalyok.SelectedItem.Text.Trim() : "";
        }
    }

    public void ReloadDatas_SzervezetiStatisztika()
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


        DateTime today = DateTime.Today;

        string kezdDatum = ErvenyessegCalendarControl1.ErvKezd;
        string vegeDatum = ErvenyessegCalendarControl1.ErvVege;

        DateTime kezdDatumDateTime;
        DateTime vegeDatumDateTime;

        if (!DateTime.TryParse(kezdDatum, out kezdDatumDateTime))
        {
            kezdDatumDateTime = today.AddDays(-30);
        }

        if (!DateTime.TryParse(vegeDatum, out vegeDatumDateTime))
        {
            vegeDatumDateTime = today;
        }

        VezetoiPanelParameterek _VezetoiPanelParameterek = new VezetoiPanelParameterek();
        _VezetoiPanelParameterek.KezdDat = kezdDatum;
        _VezetoiPanelParameterek.VegeDat = vegeDatum;

        if (!String.IsNullOrEmpty(AlSzervezetId))
        {
            _VezetoiPanelParameterek.AlSzervezetId = AlSzervezetId;
            EFormPanel_IratKezelesiFeladataim.Visible = true;
        }
        else
        {
            EFormPanel_IratKezelesiFeladataim.Visible = false;
            return;
        }

        Result result = service.GetSummaryForVezetoiPanel(execParam, _VezetoiPanelParameterek);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else
        {
            try
            {
                // Elvileg egy sor az eredm�ny:
                if (result.GetCount == 1)
                {
                    DataRow row = result.Ds.Tables[0].Rows[0];

                    Label_kuldemenyek.Text = row["kuldemenyek"].ToString();
                    Label_kuldemenyek_ma.Text = row["kuldemenyek_ma"].ToString();
                    Label_kuldemenyek_1_3nap.Text = row["kuldemenyek_1_3nap"].ToString();
                    Label_kuldemenyek_4_7nap.Text = row["kuldemenyek_4_7nap"].ToString();
                    Label_kuldemenyek_7napfelett.Text = row["kuldemenyek_7napfelett"].ToString();
                    Label_kuldemenyek_napi_atlag.Text = row["kuldemenyek_napi_atlag"].ToString();
                    Label_ugyiratok.Text = row["ugyiratok"].ToString();
                    Label_ugyiratok_ma.Text = row["ugyiratok_ma"].ToString();
                    Label_ugyiratok_1_3nap.Text = row["ugyiratok_1_3nap"].ToString();
                    Label_ugyiratok_4_7nap.Text = row["ugyiratok_4_7nap"].ToString();
                    Label_ugyiratok_7napfelett.Text = row["ugyiratok_7napfelett"].ToString();

                    Label_iratok_alszamra_ma.Text = row["iratok_alszamra_ma"].ToString();
                    Label_iratok_alszamra_bejovo_ma.Text = row["iratok_alszamra_bejovo_ma"].ToString();
                    Label_iratok_alszamra_belso_ma.Text = row["iratok_alszamra_belso_ma"].ToString();

                    // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                    Label_lejart_hatarideju_Irat.Text = row["lejart_hatarideju_irat"].ToString();
                    Label_lejart_1nap_Irat.Text = row["lejart_1nap_irat"].ToString();
                    Label_lejart_2_5nap_Irat.Text = row["lejart_2_5nap_irat"].ToString();
                    Label_lejart_6_10nap_Irat.Text = row["lejart_6_10nap_irat"].ToString();
                    Label_lejart_11_15nap_Irat.Text = row["lejart_11_15nap_irat"].ToString();
                    Label_lejart_15napfelett_Irat.Text = row["lejart_15napfelett_irat"].ToString();

                    Label_kritikus_hatarideju_irat.Text = row["kritikus_hatarideju_irat"].ToString();
                    Label_kritikus_0nap_irat.Text = row["kritikus_0nap_irat"].ToString();
                    Label_kritikus_1nap_irat.Text = row["kritikus_1nap_irat"].ToString();
                    Label_kritikus_2_5nap_irat.Text = row["kritikus_2_5nap_irat"].ToString();
                    Label_kritikus_6_10nap_irat.Text = row["kritikus_6_10nap_irat"].ToString();

                    Label_kritikus_hatarideju.Text = row["kritikus_hatarideju"].ToString();
                    Label_kritikus_0nap.Text = row["kritikus_0nap"].ToString();
                    Label_kritikus_1nap.Text = row["kritikus_1nap"].ToString();
                    Label_kritikus_2_5nap.Text = row["kritikus_2_5nap"].ToString();
                    Label_kritikus_6_10nap.Text = row["kritikus_6_10nap"].ToString();

                    Label_ugyiratok_osszesen.Text = row["ugyiratok_osszesen"].ToString();

                    Label_ugyintezes_alatt.Text = row["ugyintezes_alatt"].ToString();
                    Label_szignalasra_varo.Text = row["szignalasra_varo"].ToString();
                    Label_elintezett.Text = row["elintezett"].ToString();
                    Label_lejart_hatarideju.Text = row["lejart_hatarideju"].ToString();
                    Label_lejart_1nap.Text = row["lejart_1nap"].ToString();
                    Label_lejart_2_5nap.Text = row["lejart_2_5nap"].ToString();
                    Label_lejart_6_10nap.Text = row["lejart_6_10nap"].ToString();
                    Label_lejart_11_15nap.Text = row["lejart_11_15nap"].ToString();
                    Label_lejart_15napfelett.Text = row["lejart_15napfelett"].ToString();
                    Label_szignalt.Text = row["szignalt"].ToString();
                    Label_lezart.Text = row["lezart"].ToString();
                    Label_skontroban_levo.Text = row["skontroban_levo"].ToString();
                    Label_irattarba_kuldott.Text = row["irattarba_kuldott"].ToString();
                    Label_kolcsonzott.Text = row["kolcsonzott"].ToString();

                    Label_ugyiratok_ugyintezonkenti_atlag.Text = row["ugyiratok_ugyintezonkenti_atlag"].ToString();

                    //--- call PieChart Refresh
                    ReFresh_Charts(result.Ds.Tables[0]);

                    // linkek aktualiz�l�sa
                    SetHyperLinks_SzervezetiStatisztika(kezdDatumDateTime, vegeDatumDateTime);
                }

            }
            catch
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    Resources.Error.ErrorText_Query);
                ErrorUpdatePanel.Update();
            }

        }
    }

    // CR3289 Vezet�i panel kezel�s
    // T�pust�l f�gg� Startup meghat�roz�s
    private static String GetStartupString(String tipus)
    {

        if (tipus == "Ugyirat")
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Ugyirat;
        else if (tipus == "Kuldemeny")
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Kuldemeny;
        else
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Irat;
    }
}
