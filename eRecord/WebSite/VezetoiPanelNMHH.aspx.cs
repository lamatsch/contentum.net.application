﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class VezetoiPanel : Contentum.eUtility.UI.PageBase
{
    // ha true, a linkekre bal egérgombbal kattintva új ablak nyílik,
    // ha false, akkor nem - jobb egérgombbal kattintva a böngésző context menüje szerinti választás mindkét esetben
    private const bool bHyperLinkTargetNewWindow = false;

    private string chartDirectoryName = "";

    #region Utils
    protected DateTime GetMaxDate(DateTime firstDate, DateTime secondDate)
    {
        return (firstDate > secondDate) ? firstDate : secondDate;
    }

    // delete images older than 10 minutes from ChartImages folder
    private void deleteOldPlotImages(string ChartDirectoryName)
    {
        //DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath("/" + ChartDirectoryName));
        try
        {
            DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath(".") + "/" + ChartDirectoryName);
            foreach (FileInfo myFileInfo in myDirectoryInfo.GetFiles())
            {
                if (myFileInfo.LastWriteTime.AddMinutes(10) < DateTime.Now)
                {
                    myFileInfo.Delete();
                }
            }
        }
        catch (Exception ex)
        {
            //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Delete", ex);
            Exception e = new Exception("Path: " + ChartDirectoryName + "<br />Operation: File Delete", ex);
            throw e;    // throw it with the additional message info
        }
    }
    #endregion Utils

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "VezetoiPanelView");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        if (!IsPostBack)
        {
            FillAlosztalyok();
        }
    }

    private void FillAlosztalyok()
    {
        KRT_CsoportokService svc = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam xpm = UI.SetExecParamDefault(Page);
        Result res = null;
        try
        {
            var vezetettCsoportok = Contentum.eUtility.Csoportok.GetFelhasznaloVezetettCsoportjai(Page, xpm.Felhasznalo_Id);
            if (vezetettCsoportok.Count > 0)
            {
                KRT_CsoportokSearch sch = new KRT_CsoportokSearch();
                sch.Tipus.NotEquals(KodTarak.CSOPORTTIPUS.Dolgozo);
                sch.Id.NotEquals(xpm.FelhasznaloSzervezet_Id);
                sch.OrderBy = "Krt_Csoportok.Nev ASC";
                sch.Id.AndGroup("111");
                sch.Id.In(vezetettCsoportok);
                res = svc.GetAll(xpm, sch);
            }
        }
        catch (Contentum.eUtility.ResultException rx)
        {
            res = rx.GetResult();
        }

        if (res != null)
        {
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
            else
            {
                if (res.GetCount > 0)
                {
                    List<Osztaly> osztalyok = new List<Osztaly>();
                    for (int i = 0; i < res.GetCount; i++)
                    {
                        // ügyfelelősre rögtön szűkítünk is:
                        KRT_CsoportokSearch search_csoportok = new KRT_CsoportokSearch();
                        search_csoportok.Tipus.Filter("0");

                        Result result_subCsoportok = svc.GetAllSubCsoport(xpm, res.Ds.Tables[0].Rows[i]["Id"].ToString(), false, search_csoportok);
                        if (result_subCsoportok.IsError)
                        {
                            continue;
                        }

                        Osztaly osztaly = new Osztaly();
                        osztaly.Id = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                        osztaly.Nev = res.Ds.Tables[0].Rows[i]["Nev"].ToString();
                        osztalyok.Add(osztaly);

                        if (result_subCsoportok.Ds.Tables != null && result_subCsoportok.GetCount > 0)
                        {
                            //DataTable t = result_subCsoportok.Ds.Tables[0].Clone();
                            for (int j = 0; j < result_subCsoportok.GetCount; j++)
                            {
                                if (!osztalyok.Any(x => x.Id == result_subCsoportok.Ds.Tables[0].Rows[j]["Id"].ToString()))
                                {
                                    Osztaly o = new Osztaly();
                                    o.Id = result_subCsoportok.Ds.Tables[0].Rows[j]["Id"].ToString();
                                    o.Nev = HttpUtility.HtmlDecode("&nbsp;&nbsp;" + result_subCsoportok.Ds.Tables[0].Rows[j]["Nev"].ToString());
                                    osztalyok.Add(o);
                                }
                            }
                        }
                    }

                    bool dropdown = osztalyok.Count > 1;
                    if (osztalyok.Count > 1)
                    {
                        dropdown = true;
                        Osztaly ures = new Osztaly();
                        ures.Id = "";
                        ures.Nev = "";//HttpUtility.HtmlDecode("&nbsp;&nbsp;");
                        osztalyok.Insert(0, ures);
                    }

                    ddListAlosztalyok.DataSource = osztalyok;
                    ddListAlosztalyok.DataBind();                    
                    
                    ddListAlosztalyok.Visible = dropdown;

                    labelVezetettAlosztaly.Text = AlSzervezetNev;
                    labelVezetettAlosztaly.Visible = !dropdown;
                }
                else
                {
                    trAlosztalyok.Visible = false;
                }
            }
        }
        else
        {
            ddListAlosztalyok.Visible = false;
        }
    }

    private class Osztaly
    {
        public string Id
        { get; set; }
        public string Nev
        { get; set; }
    }

    protected void ddListAlosztalyok_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReloadDatas_SzervezetiStatisztika();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        chartDirectoryName = "images/TempChartImages";

        ListHeader1.HeaderLabel = Resources.List.VezetoiPanelHeaderTitle;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;
        #region Baloldali funkciógombok kiszedése
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        #endregion
        ListHeader1.CustomSearchObjectSessionName = "----------";

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        //TextBox t = new TextBox();
        //t.Attributes.Add("onchanged", UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);


        //ListHeader1.RefreshOnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);

        if (!IsPostBack)
        {
            //TextBox_UtobbiXNap.Text = "90";
            var today = DateTime.Now;
            ErvenyessegCalendarControl1.ErvKezd = today.AddDays(-30).ToString();
            ErvenyessegCalendarControl1.ErvVege = today.ToString();
            ReloadDatas_SzervezetiStatisztika();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (TabContainer1.ActiveTab.Equals(SzervezetiStatisztikaTabPanel))
        {
            ListHeader1.RefreshOnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);
            ImageButton_Refresh.OnClientClick = UI.DoPostBack(SzervezetiStatisztikaUpdatePanel, EventArgumentConst.refresh);
        }
    }

    protected void SzervezetiStatisztikaUpdatePanel_Load(object sender, EventArgs e)
    {
        if (TabContainer1.ActiveTab.Equals(SzervezetiStatisztikaTabPanel))
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refresh:
                        ReloadDatas_SzervezetiStatisztika();
                        break;
                }
            }
        }
    }

    protected void UgyUgyiratokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
    }

    public void ReFresh_Charts(DataTable table)
    {
        // Elvileg egy sort tartalmaz a ReloadDatas_SzervezetiStatisztika eredményéből
        if (table.Rows.Count == 1)
        {
            // make sure that output directory exists
            //string chartChartDirectoryName = "ChartImages";

            #region Image output directory (check if exists and create if does not)
            string chartPhysicalPath = Server.MapPath(".") + "/" + chartDirectoryName;

            try
            {
                if (!Directory.Exists(chartPhysicalPath))
                {
                    Directory.CreateDirectory(chartPhysicalPath);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("ReFresh_Charts", ex);
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    String.Format(Resources.Error.UI_Chart_CreateDirectoryError, chartPhysicalPath));
                ErrorUpdatePanel.Update();
                // make all charts unvisible if chart image directory does not exists
                FilesByStatusPieChart.Visible = false;
                TermCriticalBarChart.Visible = false;
                return;
            }
            #endregion Image output directory (check if exists and create if does not)

            // generate charts with exception handling
            try
            {
                // set directory names
                #region Common base settings
                FilesByStatusPieChart.ChartDirectoryName = chartDirectoryName;
                TermCriticalBarChart.ChartDirectoryName = chartDirectoryName;
                // format charts and fill them with data
                DataRow row = table.Rows[0];
                double value;
                Random rnd = new Random(4); // to generate chart colors
                // settings for the charts
                int chartImageWidth = 250;
                int chartImageHeight = 170;
                Font chartTitleFont = new Font("Verdana", 8, FontStyle.Bold);
                Font chartLabelFont = new Font("Arial Narrow", 8);
                StringAlignment chartTitleAlignment = StringAlignment.Center;
                int chartPanelPadding = 7;
                int chartPanelPaddingTop = 15;
                Point chartLabelPosition = new Point(15, 20);
                string chartLabelExtension = "db";
                bool chartLabelShowPercent = false;
                int chartLabelDecimalPlaces = 0;
                int chartChartPaddingLeft = 120;
                int chartChartPaddingRight = 10;
                int chartChartPaddingBottom = 10;

                Color chartFromBgColorTopRow = Color.FromArgb(254, 254, 254);
                Color chartFromBgColorBottomRow = Color.FromArgb(233, 233, 233);
                Color chartToBgColor = Color.FromArgb(233, 233, 233);
                SolidBrush chartTitleBrush = new SolidBrush(Color.FromArgb(57, 95, 128));
                float chartGradientAngle = 90.0f;

                int chartBarWidthPercent = 80;

                 FilesByStatusPieChart.Visible = true;
                TermCriticalBarChart.Visible = true;
                #endregion Common base settings            
              

                #region Ügyiratok állapot szerint (FilesByStatusPieChart)
                FilesByStatusPieChart.ChartTitle = "Ügyiratok állapot szerint";
                FilesByStatusPieChart.ImageAlt = "Ügyiratok állapot szerint";
                FilesByStatusPieChart.ImageWidth = chartImageWidth;
                FilesByStatusPieChart.ImageHeight = chartImageHeight;
                FilesByStatusPieChart.TitleStringFormat.Alignment = chartTitleAlignment;
                FilesByStatusPieChart.TitleFont = chartTitleFont;
                FilesByStatusPieChart.LabelFont = chartLabelFont;
                FilesByStatusPieChart.PanelPadding = chartPanelPadding;
                FilesByStatusPieChart.PanelPaddingTop = chartPanelPaddingTop;
                FilesByStatusPieChart.LabelPosition = chartLabelPosition;
                FilesByStatusPieChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                FilesByStatusPieChart.LabelExtension = chartLabelExtension;
                FilesByStatusPieChart.LabelShowPercent = chartLabelShowPercent;
                FilesByStatusPieChart.ChartPaddingRight = chartChartPaddingRight;
                FilesByStatusPieChart.ChartPaddingBottom = chartChartPaddingBottom;
                FilesByStatusPieChart.ChartPaddingLeft = chartChartPaddingLeft;

                FilesByStatusPieChart.FromPanelBgColor = chartFromBgColorTopRow;
                FilesByStatusPieChart.ToPanelBgColor = chartToBgColor;
                FilesByStatusPieChart.TitleBrush = chartTitleBrush;
                FilesByStatusPieChart.GradientAngle = chartGradientAngle;
                // add chart elements
                if (double.TryParse(row["ugyintezes_alatt"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Ügyintézés alatt", value, Color.FromArgb(203, 252, 133));
                }
                // CR3133: Szignálásra váró és a Szignált sorok ne jelenjenek meg a Vezetői panelen
                //if (double.TryParse(row["szignalasra_varo"].ToString(), out value))
                //{
                //    FilesByStatusPieChart.addChartElement("Szignálásra váró", value, Color.FromArgb(94, 2, 9));
                //}

                if (double.TryParse(row["elintezett"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Elintézett", value, Color.SlateBlue);
                }
                // CR3133: Szignálásra váró és a Szignált sorok ne jelenjenek meg a Vezetői panelen 
                //if (double.TryParse(row["szignalt"].ToString(), out value))
                //{
                //    FilesByStatusPieChart.addChartElement("Szignált", value, Color.Peru);
                //}

                if (double.TryParse(row["lezart"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Lezárt", value, Color.FromArgb(172, 197, 219));                    
                }

                if (double.TryParse(row["skontroban_levo"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Skontróban lévő", value, Color.Gold);
                }

                if (double.TryParse(row["irattarba_kuldott"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Irattárba küldött", value, Color.Indigo);
                }

                if (double.TryParse(row["kolcsonzott"].ToString(), out value))
                {
                    FilesByStatusPieChart.addChartElement("Kölcsönzött", value, Color.SteelBlue);
                }
                FilesByStatusPieChart.generateChartImage();
                #endregion Ügyiratok állapot szerint (FilesByStatusPieChart)

               

                #region Kritikus határidejű ügyiratok (TermCriticalBarChart)
                TermCriticalBarChart.ChartType = eRecordComponent_BarChart.ChartTypes.Column;
                TermCriticalBarChart.ChartTitle = "Kritikus határidejű ügyiratok";
                TermCriticalBarChart.ImageAlt = "Kritikus határidejű ügyiratok";
                TermCriticalBarChart.ImageWidth = chartImageWidth;
                TermCriticalBarChart.ImageHeight = chartImageHeight;
                TermCriticalBarChart.TitleStringFormat.Alignment = chartTitleAlignment;
                TermCriticalBarChart.TitleFont = chartTitleFont;
                TermCriticalBarChart.LabelFont = chartLabelFont;
                TermCriticalBarChart.PanelPadding = chartPanelPadding;
                TermCriticalBarChart.PanelPaddingTop = chartPanelPaddingTop;
                TermCriticalBarChart.LabelPosition = chartLabelPosition;
                TermCriticalBarChart.LabelDecimalPlaces = chartLabelDecimalPlaces;
                TermCriticalBarChart.LabelExtension = chartLabelExtension;

                TermCriticalBarChart.ChartPaddingRight = chartChartPaddingRight;
                TermCriticalBarChart.ChartPaddingBottom = chartChartPaddingBottom;
                TermCriticalBarChart.ChartPaddingLeft = 70;
                TermCriticalBarChart.ChartPaddingTop = 30;

                TermCriticalBarChart.FromPanelBgColor = chartFromBgColorBottomRow;
                TermCriticalBarChart.ToPanelBgColor = chartToBgColor;
                TermCriticalBarChart.TitleBrush = chartTitleBrush;
                TermCriticalBarChart.GradientAngle = chartGradientAngle;

                TermCriticalBarChart.BarWidthPercent = chartBarWidthPercent;

                // add chart elements
                if (double.TryParse(row["kritikus_0nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("0 nap", value, Color.DarkRed);
                }

                if (double.TryParse(row["kritikus_1nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("1 nap", value, Color.Firebrick);
                }

                if (double.TryParse(row["kritikus_2_5nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("2-5 nap", value, Color.Orange);
                }

                if (double.TryParse(row["kritikus_6_10nap"].ToString(), out value))
                {
                    TermCriticalBarChart.addChartElement("6-10 nap", value, Color.Gold);
                }
                TermCriticalBarChart.generateChartImage();
                #endregion Kritikus határidejű ügyiratok (TermCriticalBarChart)

                // TODO: megoldani
                // workaround: a kép src-ben hibásan megjelenik egy "/" az útvonal előtt,
                //             ezért a kép nem jelenik meg
                FilesByStatusPieChart.Src = Page.ResolveUrl(".") + FilesByStatusPieChart.Src.Substring(1);
                TermCriticalBarChart.Src = Page.ResolveUrl(".") + TermCriticalBarChart.Src.Substring(1);
            }
            catch (Exception ex)
            {
                string message = Resources.Error.UI_Chart_SaveError;
                message += "<br />" + ex.Message;
                Exception innerex = ex.InnerException;
                while (innerex != null)
                {
                    message += "<br />" + innerex.GetType().ToString() + ": " + innerex.Message;
                    innerex = innerex.InnerException;
                }

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, message);
                ErrorUpdatePanel.Update();
                // make all charts unvisible if chart image directory does not exists
                FilesByStatusPieChart.Visible = false;
               TermCriticalBarChart.Visible = false;
                return;
            }

            #region Try delete old images
            try
            {
                // possible Exception types
                // System.IO.IOException
                // System.Security.SecurityException
                // System.UnauthorizedAccessException
                deleteOldPlotImages(chartDirectoryName);
            }
            catch (Exception ex)
            {
                string message = Resources.Error.UI_Chart_DeleteError;
                message += "<br />" + ex.Message;
                Exception innerex = ex.InnerException;
                while (innerex != null)
                {
                    message += "<br />" + innerex.GetType().ToString() + ": " + innerex.Message;
                    innerex = innerex.InnerException;
                }

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, message);
                ErrorUpdatePanel.Update();
            }
            #endregion Try delete old images
        }   // if
    }

    private string GetReadableWhere(string readableWhere)
    {
        // BUG_8847
        return GetReadableWhere(readableWhere, true);
        //if (!String.IsNullOrEmpty(AlSzervezetNev))
        //{
        //    if (!String.IsNullOrEmpty(readableWhere))
        //    {
        //        return String.Format("{0}&nbsp;({1})", readableWhere, AlSzervezetNev);
        //    }

        //    return String.Format("({0})", AlSzervezetNev);
        //}

        //return readableWhere;
    }

    private string GetReadableWhere(string readableWhere, bool useSzervezetFilter)
    {
        if (useSzervezetFilter)
        {
            if (!String.IsNullOrEmpty(AlSzervezetNev))
            {
                if (!String.IsNullOrEmpty(readableWhere))
                {
                    return String.Format("{0}&nbsp;({1})", readableWhere, AlSzervezetNev);
                }

                return String.Format("({0})", AlSzervezetNev);
            }
        }
        return readableWhere;
    }
    protected void SetHyperLinks_SzervezetiStatisztika(DateTime startDate, DateTime endDate)
    {
        // flaggel szabályozható, hogy új ablakokat nyisson-e a linkekre kattintva
        string target = "";
        if (bHyperLinkTargetNewWindow == true)
        {
            target = "_blank";
        }

        // CR3289 Vezetői panel kezelés
        //string QS_Startup = QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel;

        string UgyUgyiratokListPath = "~/UgyUgyiratokList.aspx";
        string KuldKuldemenyekListPath = "~/KuldKuldemenyekList.aspx";
        string IraIratokListPath = "~/IraIratokList.aspx";

        //DateTime today = DateTime.Today;
        //DateTime now = DateTime.Now;

        DateTime now = endDate;
        DateTime today = endDate.Date;
        //DateTime startDate = today.AddDays(-nPastDays);
        // CR3135: Vezetői panelen lejárt és kritikus határidejű iratok megjelenítése
        // CR kapcsán derült ki, hogy a leválogatásoknál nem jó a maxdate beállítás-> sp_EREC_ÜgyiratokGetAllWithExtension  <= néz, a határidők pedig óra-perc-ben van megadva
        // ha a max és mindate egyenlő, akkor meg csak egyenlőt néz óra-percben
        DateTime todayEnd = endDate.Date.AddDays(1.0).AddMilliseconds(-1.0);

        // előre kiszámítjuk a maximumokat, csak az "új" küldemények és ügyiratok esetén érdekes,
        // ha az időablak kisebb, mint a lekérdezett határok
        string _3DaysBefore = GetMaxDate(today.AddDays(-3.0), startDate).ToString();
        string _7DaysBefore = GetMaxDate(today.AddDays(-7.0), startDate).ToString();

        Kuldemenyek.FilterObject kuldemenyekFilterObject = new Kuldemenyek.FilterObject();
       // kuldemenyekFilterObject.AlSzervezetId = AlSzervezetId;
        Ugyiratok.FilterObject ugyiratokFilterObject = new Ugyiratok.FilterObject();
        ugyiratokFilterObject.AlSzervezetId = AlSzervezetId;
        Iratok.FilterObject iratokFilterObject = new Iratok.FilterObject();
        iratokFilterObject.AlSzervezetId = AlSzervezetId;

        //// csak a szervezetbe tartozó felelősök
        //string SzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        //kuldemenyekFilterObject.SzervezetId = SzervezetId;
        //ugyiratokFilterObject.SzervezetId = SzervezetId;
        //iratokFilterObject.SzervezetId = SzervezetId;

        #region küldemények
        kuldemenyekFilterObject.MinDate = startDate.ToString();
        kuldemenyekFilterObject.MaxDate = now.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek, false);
        string QS_kuldemenyek = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = today.ToString();
        kuldemenyekFilterObject.MaxDate = now.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_ma);
        string QS_kuldemenyek_ma = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = _3DaysBefore;
        //        kuldemenyekFilterObject.MaxDate = today.ToString();
        kuldemenyekFilterObject.MaxDate = todayEnd.ToString();
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_1_3nap);
        string QS_kuldemenyek_1_3nap = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = _7DaysBefore;
        kuldemenyekFilterObject.MaxDate = _3DaysBefore;
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_4_7nap);
        string QS_kuldemenyek_4_7nap = kuldemenyekFilterObject.QsSerialize();

        kuldemenyekFilterObject.MinDate = startDate.ToString();
        kuldemenyekFilterObject.MaxDate = _7DaysBefore;
        kuldemenyekFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Kuldemeny_kuldemenyek_7napfelett);
        string QS_kuldemenyek_7napfelett = kuldemenyekFilterObject.QsSerialize();

        // linkek
        // CR3289 Vezetői panel kezelés
        // QS_Startup 
        HyperLink_kuldemenyek.NavigateUrl = KuldKuldemenyekListPath + "?" + QS_kuldemenyek + "&" + GetStartupString("Kuldemeny"); // QS_Startup;
        HyperLink_kuldemenyek.Target = target;
      
        // linkek feliratai
        Label_kuldemenyek.Font.Underline = true;
        #endregion küldemények


        #region ügyiratok összesen
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.ClearMaxDate();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere("");
        string QS_ugyiratok_osszesen = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyiratok_osszesen.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok_osszesen + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_ugyiratok_osszesen.Target = target;

        // linkek feliratai
        Label_ugyiratok_osszesen.Font.Underline = true;
        #endregion

        #region ügyiratok létrehozás szerint
        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Letrehozas;
        ugyiratokFilterObject.MinDate = startDate.ToString();
        ugyiratokFilterObject.MaxDate = now.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok);
        string QS_ugyiratok = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = now.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_ma);
        string QS_ugyiratok_ma = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = _3DaysBefore;
        ugyiratokFilterObject.MaxDate = today.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_1_3nap);
        string QS_ugyiratok_1_3nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = _7DaysBefore;
        ugyiratokFilterObject.MaxDate = _3DaysBefore;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_4_7nap);
        string QS_ugyiratok_4_7nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = startDate.ToString();
        ugyiratokFilterObject.MaxDate = _7DaysBefore;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyiratok_7napfelett);
        string QS_ugyiratok_7napfelett = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyiratok.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyiratok + "&" + GetStartupString("Ugyirat"); // QS_Startup;
         HyperLink_ugyiratok.Target = target;
      
        // linkek feliratai
        Label_ugyiratok.Font.Underline = true;
        #endregion ügyiratok létrehozás szerint

        #region iratok alszámra
        // BUG_8847
        //iratokFilterObject.MinDate = today.ToString();
        //iratokFilterObject.MaxDate = now.ToString();
        iratokFilterObject.MinDate = startDate.ToString();
        iratokFilterObject.MaxDate = now.ToString();
        // BUG_8847
        iratokFilterObject.AlSzervezetId = AlSzervezetId;

        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra);
        string QS_iratok_alszamra = iratokFilterObject.QsSerialize();

        iratokFilterObject.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_bejovo);
        string QS_iratok_alszamra_bejovo = iratokFilterObject.QsSerialize();

        iratokFilterObject.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_belso);
        string QS_iratok_alszamra_belso = iratokFilterObject.QsSerialize();

        iratokFilterObject.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_iratok_alszamra_kimeno);
        string QS_iratok_alszamra_kimeno = iratokFilterObject.QsSerialize();

        // linkek
        HyperLink_iratok_alszamra.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_iratok_alszamra_bejovo.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_bejovo + "&" + GetStartupString("Irat"); // QS_Startup;
        HyperLink_iratok_alszamra_belso.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_belso + "&" + GetStartupString("Irat"); // QS_Startup;
        // BUG_8847                                                                                                                                         // BUG_8847
        HyperLink_iratok_alszamra_kimeno.NavigateUrl = IraIratokListPath + "?" + QS_iratok_alszamra_kimeno + "&" + GetStartupString("Irat"); // QS_Startup;

        HyperLink_iratok_alszamra.Target = target;
        HyperLink_iratok_alszamra_bejovo.Target = target;
        HyperLink_iratok_alszamra_belso.Target = target;
        // BUG_8847
        HyperLink_iratok_alszamra_kimeno.Target = target;

        // linkek feliratai
        Label_iratok_alszamra.Font.Underline = true;
        Label_iratok_alszamra_bejovo.Font.Underline = true;
        Label_iratok_alszamra_belso.Font.Underline = true;
        // BUG_8847
        Label_iratok_alszamra_kimeno.Font.Underline = true;

        #endregion iratok alszámra

        // CR3135: Vezetői panelen lejárt és kritikus határidejű iratok megjelenítése
        #region lejárt iratok
        iratokFilterObject.Clear();
        iratokFilterObject.ClearAllapot();
        iratokFilterObject.ClearExcludeAllapot();

        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Sztornozott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Atiktatott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat);

        iratokFilterObject.SpecialFilter = Iratok.FilterObject.SpecialFilterType.Lejart;
        iratokFilterObject.ClearMinDate();
        //CR3133 : Összesítésnél az adott dátumú tételeket nem vette bele, míg a részletező beleveszi...
        //A mai nap-1-re kell lekérdezni mert a sp_EREC_IraIratokGetAllWithExtension <= -vel állítja össze a lekérdezést
        //ugyiratokFilterObject.MaxDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        // BUG_8847
        iratokFilterObject.AlSzervezetId = AlSzervezetId;
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_hatarideju);
        string QS_lejart_hatarideju_Irat = iratokFilterObject.QsSerialize();

        //iratokFilterObject.MinDate = today.AddDays(-1.0).ToString();
        //iratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        //iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_1nap);
        //string QS_lejart_1nap_Irat = iratokFilterObject.QsSerialize();


        //iratokFilterObject.MinDate = today.AddDays(-5.0).ToString();
        //iratokFilterObject.MaxDate = todayEnd.AddDays(-2.0).ToString();
        //iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_2_5nap);
        //string QS_lejart_2_5nap_Irat = iratokFilterObject.QsSerialize();

        //iratokFilterObject.MinDate = today.AddDays(-10.0).ToString();
        //iratokFilterObject.MaxDate = todayEnd.AddDays(-6.0).ToString();
        //iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_6_10nap);
        //string QS_lejart_6_10nap_Irat = iratokFilterObject.QsSerialize();

        //iratokFilterObject.MinDate = today.AddDays(-15.0).ToString();
        //iratokFilterObject.MaxDate = todayEnd.AddDays(-11.0).ToString();
        //iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_11_15nap);
        //string QS_lejart_11_15nap_Irat = iratokFilterObject.QsSerialize();

        //iratokFilterObject.ClearMinDate();
        //iratokFilterObject.MaxDate = todayEnd.AddDays(-16.0).ToString();
        //iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_lejart_15napfelett);
        //string QS_lejart_15napfelett_Irat = iratokFilterObject.QsSerialize();

        // linkek
        HyperLink_lejart_hatarideju_Irat.NavigateUrl = IraIratokListPath + "?" + QS_lejart_hatarideju_Irat + "&" + GetStartupString("Irat"); // QS_Startup;
      
        // linkek feliratai
        Label_lejart_hatarideju_Irat.Font.Underline = true;
        #endregion lejárt iratok

        #region kritikus határidejű iratok
        iratokFilterObject.Clear();
        iratokFilterObject.ClearAllapot();
        iratokFilterObject.ClearExcludeAllapot();

        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Sztornozott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Atiktatott);
        iratokFilterObject.AddExcludeAllapot(KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat);

        iratokFilterObject.SpecialFilter = Iratok.FilterObject.SpecialFilterType.Kritikus;
        iratokFilterObject.MinDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_hatarideju);
        string QS_kritikus_hatarideju_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.ToString();
        iratokFilterObject.MaxDate = todayEnd.ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_0nap);
        string QS_kritikus_0nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(1.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(1.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_1nap);
        string QS_kritikus_1nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(2.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(5.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_2_5nap);
        string QS_kritikus_2_5nap_irat = iratokFilterObject.QsSerialize();

        iratokFilterObject.MinDate = today.AddDays(6.0).ToString();
        iratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        iratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Irat_kritikus_6_10nap);
        string QS_kritikus_6_10nap_irat = iratokFilterObject.QsSerialize();

        #endregion kritikus határidejű iratok

        #region lejárt ügyiratok
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Lejart;
        ugyiratokFilterObject.ClearMinDate();
        //CR3133 : Összesítésnél az adott dátumú tételeket nem vette bele, míg a részletező beleveszi...
        //A mai nap-1-re kell lekérdezni mert a sp_EREC_UgyUgyiratokGetAllWithExtension <= -vel állítja össze a lekérdezést
        //ugyiratokFilterObject.MaxDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_hatarideju);
        string QS_lejart_hatarideju = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-1.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_1nap);
        string QS_lejart_1nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-5.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-2.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_2_5nap);
        string QS_lejart_2_5nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-10.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-6.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_6_10nap);
        string QS_lejart_6_10nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(-15.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-11.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_11_15nap);
        string QS_lejart_11_15nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(-16.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lejart_15napfelett);
        string QS_lejart_15napfelett = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_lejart_hatarideju.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lejart_hatarideju + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lejart_hatarideju.Target = target;
       
        // linkek feliratai
        Label_lejart_hatarideju.Font.Underline = true;
        #endregion lejárt ügyiratok

        #region ügyiratok állapot szerint
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.ClearMinDate();
        ugyiratokFilterObject.ClearMaxDate();
        ugyiratokFilterObject.SpecialFilter = Contentum.eRecord.BaseUtility.Ugyiratok.FilterObject.SpecialFilterType.Letrehozas;
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_ugyintezes_alatt);
        string QS_ugyintezes_alatt = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_szignalasra_varo);
        string QS_szignalasra_varo = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Elintezett);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_elintezett);
        string QS_elintezett = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_szignalt);
        string QS_szignalt = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        // BUG_8847
        ugyiratokFilterObject.Aktiv = false;

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_lezart);
        string QS_lezart = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_skontroban_levo);
        string QS_skontroban_levo = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
        // BUG_8847
        ugyiratokFilterObject.Aktiv = false;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_irattarba_kuldott);
        string QS_irattarba_kuldott = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.ClearAllapot();
        //ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott);
        // BUG_8847
        ugyiratokFilterObject.Aktiv = false;
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kolcsonzott);
        string QS_kolcsonzott = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_ugyintezes_alatt.NavigateUrl = UgyUgyiratokListPath + "?" + QS_ugyintezes_alatt + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_elintezett.NavigateUrl = UgyUgyiratokListPath + "?" + QS_elintezett + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_lezart.NavigateUrl = UgyUgyiratokListPath + "?" + QS_lezart + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_skontroban_levo.NavigateUrl = UgyUgyiratokListPath + "?" + QS_skontroban_levo + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_irattarba_kuldott.NavigateUrl = UgyUgyiratokListPath + "?" + QS_irattarba_kuldott + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kolcsonzott.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kolcsonzott + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_ugyintezes_alatt.Target = target;
        HyperLink_elintezett.Target = target;
        HyperLink_lezart.Target = target;
        HyperLink_skontroban_levo.Target = target;
        HyperLink_irattarba_kuldott.Target = target;
        HyperLink_kolcsonzott.Target = target;

        // linkek feliratai
        Label_ugyintezes_alatt.Font.Underline = true;
        Label_elintezett.Font.Underline = true;
        Label_lezart.Font.Underline = true;
        Label_skontroban_levo.Font.Underline = true;
        Label_irattarba_kuldott.Font.Underline = true;
        Label_kolcsonzott.Font.Underline = true;
        #endregion ügyiratok állapotok szerint

        #region kritikus határidejű ügyiratok
        ugyiratokFilterObject.ClearAllapot();
        ugyiratokFilterObject.ClearExcludeAllapot();
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
        ugyiratokFilterObject.AddAllapot(KodTarak.UGYIRAT_ALLAPOT.Skontroban);

        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        ugyiratokFilterObject.AddExcludeAllapot(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);

        ugyiratokFilterObject.SpecialFilter = Ugyiratok.FilterObject.SpecialFilterType.Kritikus;
        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_hatarideju);
        string QS_kritikus_hatarideju = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_0nap);
        string QS_kritikus_0nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(1.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(1.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_1nap);
        string QS_kritikus_1nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(2.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(5.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_2_5nap);
        string QS_kritikus_2_5nap = ugyiratokFilterObject.QsSerialize();

        ugyiratokFilterObject.MinDate = today.AddDays(6.0).ToString();
        ugyiratokFilterObject.MaxDate = todayEnd.AddDays(10.0).ToString();
        ugyiratokFilterObject.ReadableWhere = GetReadableWhere(Resources.Search.ReadableWhere_Ugyirat_kritikus_6_10nap);
        string QS_kritikus_6_10nap = ugyiratokFilterObject.QsSerialize();

        // linkek
        HyperLink_kritikus_hatarideju.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_hatarideju + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_0nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_0nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_1nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_1nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_2_5nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_2_5nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;
        HyperLink_kritikus_6_10nap.NavigateUrl = UgyUgyiratokListPath + "?" + QS_kritikus_6_10nap + "&" + GetStartupString("Ugyirat"); // QS_Startup;

        HyperLink_kritikus_hatarideju.Target = target;
        HyperLink_kritikus_0nap.Target = target;
        HyperLink_kritikus_1nap.Target = target;
        HyperLink_kritikus_2_5nap.Target = target;
        HyperLink_kritikus_6_10nap.Target = target;

        // linkek feliratai
        Label_kritikus_hatarideju.Font.Underline = true;
        Label_kritikus_0nap.Font.Underline = true;
        Label_kritikus_1nap.Font.Underline = true;
        Label_kritikus_2_5nap.Font.Underline = true;
        Label_kritikus_6_10nap.Font.Underline = true;
        #endregion kritikus határidejű ügyiratok


    }

    private string AlSzervezetId
    {
        get
        {
            return ddListAlosztalyok.SelectedValue.Trim();
        }
    }

    private string AlSzervezetNev
    {
        get
        {
            return (ddListAlosztalyok.SelectedItem != null) ? ddListAlosztalyok.SelectedItem.Text.Trim() : "";
        }
    }

    public void ReloadDatas_SzervezetiStatisztika()
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


        DateTime today = DateTime.Today;

        string kezdDatum = ErvenyessegCalendarControl1.ErvKezd;
        string vegeDatum = ErvenyessegCalendarControl1.ErvVege;

        DateTime kezdDatumDateTime;
        DateTime vegeDatumDateTime;

        if (!DateTime.TryParse(kezdDatum, out kezdDatumDateTime))
        {
            kezdDatumDateTime = today.AddDays(-30);
        }

        if (!DateTime.TryParse(vegeDatum, out vegeDatumDateTime))
        {
            vegeDatumDateTime = today;
        }

        VezetoiPanelParameterek _VezetoiPanelParameterek = new VezetoiPanelParameterek();
        _VezetoiPanelParameterek.KezdDat = kezdDatum;
        _VezetoiPanelParameterek.VegeDat = vegeDatum;

        if (!String.IsNullOrEmpty(AlSzervezetId))
        {
            _VezetoiPanelParameterek.AlSzervezetId = AlSzervezetId;
            EFormPanel_IratKezelesiFeladataim.Visible = true;
        }
        else
        {
            EFormPanel_IratKezelesiFeladataim.Visible = false;
            return;
        }

        // BUG_8847
        _VezetoiPanelParameterek.isNMHH = true;

        Result result = service.GetSummaryForVezetoiPanel(execParam, _VezetoiPanelParameterek);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else
        {
            try
            {
                // Elvileg egy sor az eredmény:
                if (result.GetCount == 1)
                {
                    DataRow row = result.Ds.Tables[0].Rows[0];

                    Label_kuldemenyek.Text = row["kuldemenyek"].ToString();
                    Label_kuldemenyek_napi_atlag.Text = row["kuldemenyek_napi_atlag"].ToString();
                    Label_ugyiratok.Text = row["ugyiratok"].ToString();
                   
                    Label_iratok_alszamra.Text = row["iratok_alszamra"].ToString();
                    Label_iratok_alszamra_bejovo.Text = row["iratok_alszamra_bejovo"].ToString();
                    Label_iratok_alszamra_belso.Text = row["iratok_alszamra_belso"].ToString();
                    // BUG_8847
                    Label_iratok_alszamra_kimeno.Text = row["iratok_alszamra_kimeno"].ToString();

                    // CR3135: Vezetői panelen lejárt és kritikus határidejű iratok megjelenítése
                    Label_lejart_hatarideju_Irat.Text = row["lejart_hatarideju_irat"].ToString();
                   
                    Label_kritikus_hatarideju.Text = row["kritikus_hatarideju"].ToString();
                    Label_kritikus_0nap.Text = row["kritikus_0nap"].ToString();
                    Label_kritikus_1nap.Text = row["kritikus_1nap"].ToString();
                    Label_kritikus_2_5nap.Text = row["kritikus_2_5nap"].ToString();
                    Label_kritikus_6_10nap.Text = row["kritikus_6_10nap"].ToString();

                    Label_ugyiratok_osszesen.Text = row["ugyiratok_osszesen"].ToString();

                    Label_ugyintezes_alatt.Text = row["ugyintezes_alatt"].ToString();
                    Label_elintezett.Text = row["elintezett"].ToString();
                    Label_lejart_hatarideju.Text = row["lejart_hatarideju"].ToString();
                  
                    Label_lezart.Text = row["lezart"].ToString();
                    Label_skontroban_levo.Text = row["skontroban_levo"].ToString();
                    Label_irattarba_kuldott.Text = row["irattarba_kuldott"].ToString();
                    Label_kolcsonzott.Text = row["kolcsonzott"].ToString();

                    Label_ugyiratok_ugyintezonkenti_atlag.Text = row["ugyiratok_ugyintezonkenti_atlag"].ToString();

                    //--- call PieChart Refresh
                    ReFresh_Charts(result.Ds.Tables[0]);

                    // linkek aktualizálása
                    SetHyperLinks_SzervezetiStatisztika(kezdDatumDateTime, vegeDatumDateTime);
                }

            }
            catch
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    Resources.Error.ErrorText_Query);
                ErrorUpdatePanel.Update();
            }

        }
    }

    // CR3289 Vezetői panel kezelés
    // Típustól függő Startup meghatározás
    private static String GetStartupString(String tipus)
    {

        if (tipus == "Ugyirat")
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Ugyirat;
        else if (tipus == "Kuldemeny")
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Kuldemeny;
        else
            return QueryStringVars.Startup + "=" + Constants.Startup.FromVezetoiPanel_Irat;
    }
}
