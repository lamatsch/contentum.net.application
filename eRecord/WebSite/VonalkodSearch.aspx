<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VonalkodSearch.aspx.cs" Inherits="VonalkodSearch" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="eRecordComponent/VonalKodListBox.ascx" TagName="VonalKodListBox" TagPrefix="uc4" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional" OnLoad="ErrorUpdatePanel1_Load"
        RenderMode="Inline">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>
    <asp:Panel ID="MainPanel" runat="server" Visible="true">
        <table cellpadding="0" cellspacing="0" align="left">
            <tr>
                <td valign="top">
                    <asp:UpdatePanel ID="VonalkodosAtadasUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <span style="position:absolute;left:31px;">
                            <asp:ImageButton runat="server" ID="SearchCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
                        </span>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeSearch" runat="server" TargetControlID="SearchPanel"
                                CollapsedSize="10" Collapsed="False" ExpandControlID="SearchCPEButton"
                                CollapseControlID="SearchCPEButton" ExpandDirection="Horizontal" AutoCollapse="false"
                                AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                                ImageControlID="SearchResultCPEButton" ExpandedSize="0" ExpandedText="�sszecsuk"
                                CollapsedText="Kinyit">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            
                            <asp:Panel ID="SearchPanel" runat="server">
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                <uc4:VonalKodListBox runat="server" ID="VonalKodListBoxSearch" />
                            </eUI:eFormPanel>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: center;">
                                        <br />
                                        <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td style="text-align: left; vertical-align: top; width: 100%;">
                    <asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" OnLoad="SearchResultUpdatePanel_Load">
                        <ContentTemplate>
                            <asp:ImageButton runat="server" ID="SearchResultCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                                OnClientClick="return false;" CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ajaxToolkit:CollapsiblePanelExtender ID="SearchResultCPE" runat="server" TargetControlID="SearchResultPanel"
                                CollapsedSize="20" Collapsed="False" ExpandControlID="SearchResultCPEButton"
                                CollapseControlID="SearchResultCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                                ImageControlID="SearchResultCPEButton" ExpandedSize="0" ExpandedText="Keres�s eredm�nye"
                                CollapsedText="K�zbes�t�si t�telek list�ja">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <eUI:eFormPanel ID="eformPanelSearchResult" runat="server" Visible="false">
                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="color: #335674; font-weight:bold;padding-bottom:20px; font-size:14px;">
                                            A keres�s eredm�nye
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: top;">
                                            <asp:Panel ID="ResultIndicatorPanel" runat="server" Visible="false" Height="80px"
                                                HorizontalAlign="Center">
                                                <asp:Label ID="labelResultIndicatorText" runat="server" Text="Nincs megadva vonalk�d!" Font-Bold="true"> </asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="SearchResultPanel" runat="server" CssClass="searchResultPanel">
                                            <asp:GridView ID="SearchResultGridView" runat="server" OnRowCommand="SearchResultGridView_RowCommand"
                                                CellPadding="0" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" AllowPaging="True"
                                                PagerSettings-Visible="false" AllowSorting="True" OnPreRender="SearchResultGridView_PreRender"
                                                AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="SearchResultGridView_Sorting"
                                                OnRowDataBound="SearchResultGridView_RowDataBound">
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                <Columns>
                                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                        HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                        SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="headerKod" runat="server" CommandName="Sort" CommandArgument="Kod">Vonalk�d</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelKod" runat="server" Text='<%#Eval("Kod")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="headerAzonosito" runat="server" CommandName="Sort" CommandArgument="Azonosito">Azonos�t�</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelAzonosito" runat="server" Text='<%#Eval("Azonosito")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="headerTipus" runat="server" CommandName="Sort" CommandArgument="Obj_type">T�pus</asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelTipus" runat="server" Text='<%#Eval("Obj_type")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="labelTipusId" runat="server" Text='<%#Eval("Obj_Id")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerSettings Visible="False" />
                                            </asp:GridView>
                                            </asp:Panel>
                                            <asp:Panel ID="NotIdentifiedPanel" runat="server" Visible="false" HorizontalAlign="Center">
                                                <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>" Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                                                <span style="position:relative;top:-10px; left:38%;">
                                                    <asp:Label ID="labelNotIdentified" runat="server" Text="" Font-Bold="True" CssClass="warningBody"></asp:Label>
                                                </span>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

