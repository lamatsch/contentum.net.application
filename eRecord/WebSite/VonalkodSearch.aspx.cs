using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class VonalkodSearch : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    public const string kuldemeny = "K�ldem�ny";
    public const string iratpeldany = "Iratp�ld�ny";
    public const string ugyirat = "�gyirat";
    public const string kuldmelleklet = "K�ldem�ny mell�klet";
    public const string iratmelleklet = "Irat mell�klet";
    public const string dosszie = "Dosszi�";
    public const string notUsedVonalkod = "Nem haszn�lt";

    private List<string> VonalkodokArray;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "VonalkodosKereses");

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadVonalkodListBoxSatate();

        ListHeader1.HeaderLabel = Resources.Search.VonalkodokSearch;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;

        #region Baloldali funkci�gombok kiszed�se
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        #endregion

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(SearchResultUpdatePanel.ClientID);

        ListHeader1.CustomSearchObjectSessionName = "k�rd�jelek ne jelenjenek meg";

        ListHeader1.AttachedGridView = SearchResultGridView;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }


        // TODO: kirakni resource-be
        ListHeader1.RefreshImageButton.ToolTip = "Oldal �jrat�lt�se";

        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);


        registerJavascripts();

    }

    private void LoadVonalkodListBoxSatate()
    {
        VonalKodListBoxSearch.ListBox.Items.Clear();
        VonalkodokArray = new List<string>();
        string[] items;
        if(String.IsNullOrEmpty(VonalKodListBoxSearch.VonalkodClientList))
        {
            items = new string[0];
        }
        else
        {
           items = VonalKodListBoxSearch.VonalkodClientList.Split(',');
        }

        foreach (string item in items)
        {
            VonalKodListBoxSearch.ListBox.Items.Add(new ListItem(item, item));
            VonalkodokArray.Add(item);
        }

    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            ResetSearchForm();
        }
        else if (e.CommandName == CommandName.Search)
        {
            if (VonalKodListBoxSearch.ListBox.Items.Count == 0)
            {
                SetNoSearchEntered();
            }
            else
            {
                ShowSearchResult();
                SetNotIdentifiedResults();
            }
        }
    }

    private void SetNotIdentifiedResults()
    {
        if (VonalkodokArray.Count > 0)
        {
            NotIdentifiedPanel.Visible = true;
            string text = String.Empty;

            text += "<ul>";

            foreach (string kod in VonalkodokArray)
            {
                text += "<li class=\"notIdentifiedText\">";
                text += kod;
                text += "</li>";
            }

            text += "</ul>";

            labelNotIdentified.Text = text;
        }
        else
        {
            NotIdentifiedPanel.Visible = false;
        }
    }

    private void ShowSearchResult()
    {
        eformPanelSearchResult.Visible = true;
        ResultIndicatorPanel.Visible = false;
        SearchResultPanel.Visible= true;
        SearchResultGridViewBind();
    }

    private void SetNoSearchEntered()
    {
        eformPanelSearchResult.Visible = true;
        ResultIndicatorPanel.Visible = true;
        SearchResultPanel.Visible = false;
        NotIdentifiedPanel.Visible = false;
    }

    private void ResetSearchForm()
    {
        VonalKodListBoxSearch.ListBox.Items.Clear();
        VonalKodListBoxSearch.TextBox.Text = String.Empty;
        VonalKodListBoxSearch.VonalkodClientList = String.Empty;
        eformPanelSearchResult.Visible = false;
    }

    protected void SearchResultGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SearchResultGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SearchResultGridView", ViewState);

        SearchResultGridViewBind(sortExpression, sortDirection);
    }

    protected void SearchResultGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (VonalkodokArray.Count > 0)
        {
            KRT_BarkodokService srvBarCode = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            KRT_BarkodokSearch srchBarcode = new KRT_BarkodokSearch();
            ExecParam exec = UI.SetExecParamDefault(Page, new ExecParam());

            SetSearchFieldFromArray(srchBarcode.Kod, VonalkodokArray);

            srchBarcode.OrderBy = Search.GetOrderBy("SearchResultGridView", ViewState, SortExpression, SortDirection);

            Result res = srvBarCode.GetAllWithExtension(exec, srchBarcode);

            if (!res.IsError)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    this.RemoveVonalkodFromArray(row);
                }
            }

            if (!res.IsError && res.Ds.Tables[0].Rows.Count == 0)
            {
                SearchResultPanel.Visible = false;
            }
            else
            {
                UI.GridViewFill(SearchResultGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            SetNoSearchEntered();
        }
    }

    private void SetSearchFieldFromArray(Field Idfield, List<string> IdArray)
    {
        if (IdArray != null && IdArray.Count > 0)
        {
            Idfield.Value = Search.GetSqlInnerString(IdArray.ToArray());
            Idfield.Operator = Query.Operators.inner;
        }
        else
        {
            Idfield.Value = String.Empty;
            Idfield.Operator = String.Empty;
        }

        //bool elso = true;
        //Idfield.Operator = Query.Operators.inner;
        //Idfield.Value = String.Empty;
        //foreach (string id in IdArray)
        //{
        //    if(!elso)
        //    {
        //        Idfield.Value += ",";
        //    }
        //    Idfield.Value += "'" + id + "'";

        //    elso = false;
        //}
    }

    protected void SearchResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //T�pus ki�r�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty)
            {
                switch (labelTipus.Text.Trim())
                {
                    case Constants.TableNames.EREC_KuldKuldemenyek:
                        labelTipus.Text = kuldemeny;
                        break;
                    case Constants.TableNames.EREC_PldIratPeldanyok:
                        labelTipus.Text = iratpeldany;
                        break;
                    case Constants.TableNames.EREC_UgyUgyiratok:
                        labelTipus.Text = ugyirat;
                        break;
                    case Constants.TableNames.EREC_KuldMellekletek:
                        {
                            DataRowView drw = (DataRowView)e.Row.DataItem;
                            Label labelTipusId = (Label)e.Row.FindControl("labelTipusId");
                            if (drw != null && labelTipusId != null)
                            {
                                labelTipusId.Text = drw["MellekletParentID"].ToString();
                                string mellekletTipus = drw["MellekletTipus"].ToString().Trim();
                                if (mellekletTipus == Constants.TableNames.EREC_IratMellekletek)
                                {
                                    labelTipus.Text = iratmelleklet;
                                }
                                else
                                {
                                    labelTipus.Text = kuldmelleklet;
                                }
                            }
                            else
                            {

                                labelTipus.Text = kuldmelleklet;
                            }
                        }
                        break;
                    case Constants.TableNames.EREC_IratMellekletek:
                        {
                            labelTipus.Text = iratmelleklet;

                            DataRowView drw = (DataRowView)e.Row.DataItem;
                            Label labelTipusId = (Label)e.Row.FindControl("labelTipusId");
                            if (drw != null && labelTipusId != null)
                            {
                                labelTipusId.Text = drw["MellekletParentID"].ToString();                                
                            }
                        }
                        break;
                    case Constants.TableNames.KRT_Mappak:
                        labelTipus.Text = dosszie;
                        break;
                }
            }
            else
            {
                labelTipus.Text = notUsedVonalkod;
            }
        }

    }

    private void RemoveVonalkodFromArray(DataRow row)
    {
        string kod = row["Kod"].ToString();

        if (!String.IsNullOrEmpty(kod))
        {

            if (!String.IsNullOrEmpty(kod))
            {
                if (VonalkodokArray.Contains(kod))
                {
                    VonalkodokArray.Remove(kod);
                }
            }
        }
    }

    protected void SearchResultGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = SearchResultGridView.PageIndex;

        SearchResultGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = SearchResultGridView.PageCount;

        if (prev_PageIndex != SearchResultGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, SearchResultCPE);
            SearchResultGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SearchResultCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(SearchResultGridView);

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        SearchResultGridViewBind();
    }

    protected void SearchResultGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            //int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(SearchResultGridView, selectedRowNumber, "check");
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //a megfekek� form bek�t�se
            string url = String.Empty;
            string tipusId = String.Empty;
            string query = String.Empty;
            GridViewRow row = SearchResultGridView.SelectedRow;
            if (row != null)
            {
                Label labelTipus = (Label)row.FindControl("labelTipus");
                Label labelTipusId = (Label)row.FindControl("labelTipusId");
                if (labelTipus != null && labelTipus.Text.Trim() != String.Empty && labelTipusId != null && labelTipusId.Text.Trim() != String.Empty)
                {
                    switch (labelTipus.Text.Trim())
                    {
                        case kuldemeny:
                            url = "KuldKuldemenyekForm.aspx";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Modify);
                            break;
                        case iratpeldany:
                            url = "PldIratPeldanyokForm.aspx";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Modify);
                            break;
                        case ugyirat:
                            url = "UgyUgyiratokForm.aspx";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Modify);
                            break;
                        case kuldmelleklet:
                            url = "KuldKuldemenyekForm.aspx";
                            query = "&" + QueryStringVars.SelectedTab + "=Mellekletek";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Modify);
                            break;
                        case iratmelleklet:
                            url = "IraIratokForm.aspx";
                            query = "&" + QueryStringVars.SelectedTab + "=Mellekletek";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Modify);
                            break;
                        case dosszie:
                            url = "DosszieForm.aspx";
                            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Mappak" + CommandName.View);
                            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Mappak" + CommandName.Modify);
                            break;
                        default:
                            //jogosults�g ellen�rz�s
                            ListHeader1.ViewEnabled = false;
                            ListHeader1.ModifyEnabled = false;
                            break;

                    }

                    tipusId = labelTipusId.Text;
                }
                else
                {
                    //jogosults�g ellen�rz�s
                    ListHeader1.ViewEnabled = false;
                    ListHeader1.ModifyEnabled = false;
                }
            }

            if (!String.IsNullOrEmpty(url))
            {
                ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick(url
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + tipusId + query
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchResultUpdatePanel.ClientID);

                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick(url
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + tipusId + query
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchResultUpdatePanel.ClientID);
            }
        }
    }

    protected void SearchResultGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SearchResultGridViewBind(e.SortExpression, UI.GetSortToGridView("SearchResultGridView", ViewState, e.SortExpression));
    }

    protected void SearchResultUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    SearchResultGridViewBind();
                    break;
            }
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        JavaScripts.SetFocus(VonalKodListBoxSearch.TextBox);
    }

    protected void ErrorUpdatePanel1_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //M�gse gomb letilt�sa
        SearchFooter1.ImageButton_Cancel.Visible = false;

        if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(SearchResultGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        if (eformPanelSearchResult.Visible && SearchResultPanel.Visible)
        {
            ListHeader1.PagerVisible = true;
        }
        else
        {
            ListHeader1.PagerVisible = false;
        }
        
    }
}
