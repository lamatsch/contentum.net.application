﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="VonalkodTabPrintForm.aspx.cs" Inherits="VonalkodTabPrintForm" %>
<%@ Register Src="~/Component/NyomtatokDropdownList.ascx" TagName="NyomtatokDropDown"
    TagPrefix="uc1" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,VonalkodNyomtatoFormTitle%>" />
    <asp:Panel ID="VonalkodNyomtatoFormPanel" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tbody>
                <tr id="Tr1" class="urlapSor" runat="server">
                    <td>
                    </td>
                    <td class="mrUrlapCaption">
                        <uc1:NyomtatokDropDown ID="NyomtatokDropDown"
                            runat="server" IsMultiSearchMode="false"/>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <uc3:FormFooter ID="FormFooter1" runat="server" />
    <eUI:eFormPanel ID="ResultPanel_Nyomtatas" runat="server" Visible="false"
        CssClass="mrResultPanel">
        <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
            <tr class="urlapSor" style="padding-bottom: 20px;">
                <td class="mrUrlapCaption">
                    <asp:Label ID="label3" runat="server" Text="<%$Resources:Form,label_result_vonalkod%>"></asp:Label>
                </td>
            </tr>
            <tr class="urlapSor" style="padding-bottom: 20px;">
                <td>
                    <asp:Label ID="label2" runat="server" Text="<%$Resources:Form,label_result_cim%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="label_result_cim" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr class="urlapSor" style="padding-bottom: 20px;">
                <td>
                    <asp:Label ID="label1" runat="server" Text="<%$Resources:Form,label_result_ertek%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="label_result_ertek" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </eUI:eFormPanel>

    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
