using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Text;


public partial class VonalkodTabPrintForm : System.Web.UI.Page
{
    private string Id = String.Empty;
    private string Tipus = String.Empty;
    private string PrintOutput = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        //Request.QueryString.Set("Command","Modify");
        if (Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.UgyiratId) != null)
        {
            Id = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.UgyiratId);
        }                                
        else if (Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.KuldemenyId) != null)
        {                                
            Id = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.KuldemenyId);
        }                                
        else if (Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.IratId) != null)
        {                                
            Id = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.IratId);
        }                                
        else if (Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.IratPeldanyId) != null)
        {                                
            Id = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.IratPeldanyId);
        }
        else if (Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.Vonalkodok) != null)
        {
            Id = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.Vonalkodok);
        }


        Tipus = Request.QueryString.Get("tipus");

        if (String.IsNullOrEmpty(Id) || String.IsNullOrEmpty(Tipus))
        {
            // nincs Id megadva:
        }
    }
    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_IratPeldanyok()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.IraIrat_Id.Value = Id;
        erec_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }
        }
        Result result = new Result();

        string outputFile = "N\nB140,30,0,1,2,7,120,N,";
        //Vonalk�d update �s k�t�s if a vonalk�dkezel�s azonos�t�s
        //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam ExecParam_RendszerParameter = UI.SetExecParamDefault(Page, new ExecParam());
        ExecParam_RendszerParameter.Record_Id = "96472D53-55BC-E611-80BB-00155D020D34"; //VonalkodPrefix
        //var Result_KRTParam = service_parameterek.Get(ExecParam_RendszerParameter);
        //string barcodePrefix = ((KRT_Parameterek)Result_KRTParam.Record).Ertek.Trim() + "-";
        string barcodePrefix = Rendszerparameterek.Get(ExecParam_RendszerParameter, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_PREFIX).Trim() + "-";
        switch (Tipus)
        {
            case "Ugyirat":
                EREC_UgyUgyiratokService service_ugyirat = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam_ugyirat = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_ugyirat.Record_Id = Id;
                result = service_ugyirat.Get(execParam_ugyirat);
                outputFile += "\"" + ((EREC_UgyUgyiratok)result.Record).BARCODE + "\"\n";
                outputFile += "A140,160,0,4,1,1,N,\"" + ((EREC_UgyUgyiratok)result.Record).BARCODE.Replace(barcodePrefix,"") + "\"\nP\n";
                break;
            case "Irat":
                //irat
                EREC_IraIratokService service_irat = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam execParam_irat = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_irat.Record_Id = Id;
                result = service_irat.Get(execParam_irat);
                //�gyirat
                service_ugyirat = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                execParam_ugyirat = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_ugyirat.Record_Id = ((EREC_IraIratok)result.Record).Ugyirat_Id;
                var Erec_Ugyirat = service_ugyirat.Get(execParam_ugyirat);
                //iktat�k�nyv
                EREC_IraIktatoKonyvekService  service_iktatokonyvek = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam_iktatokonyvek = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iktatokonyvek.Record_Id = ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).IraIktatokonyv_Id;
                var iktatokonyv = service_iktatokonyvek.Get(execParam_iktatokonyvek);

                #region CR 3111 Buda�rs�n a vonalk�d gener�l�s Munkap�ld�ny l�trehoz�sakor mindig 0-s sorsz�mot ad.
                string barcode = "";//iktatott vagy munkaanyag
                if (!((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Allapot.Equals(KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag))
                {
                    barcode = ((EREC_IraIktatoKonyvek)iktatokonyv.Record).Iktatohely + "/" +
                        ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0')
                        + "-" +
                        ((EREC_IraIratok)result.Record).Alszam.PadLeft(6, '0')

                        + "/" + ((EREC_IraIktatoKonyvek)iktatokonyv.Record).Ev;
                }
                else
                {
                    barcode = ((EREC_IraIktatoKonyvek)iktatokonyv.Record).Iktatohely + "/MU" +
                        (String.IsNullOrEmpty(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam) ?
                                    ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0') :
                                    ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                        //("MU" + ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0'))
                        + "-" +
                        ("MI" + ((EREC_IraIratok)result.Record).Sorszam.PadLeft(6, '0'))
                        + "/" + ((EREC_IraIktatoKonyvek)iktatokonyv.Record).Ev;
                }
                #endregion

               
                barcode = barcode.Replace(" ", "");
                outputFile += "\"" + barcode + "\"\n";
                outputFile += "A140,160,0,4,1,1,N,\"" + barcode.Replace(barcodePrefix, "") + "\"\nP\n";
                break;
            case "IratPeldany":
                EREC_PldIratPeldanyokService service_iratpeldany = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam_iratpeldany = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iratpeldany.Record_Id = Id;
                result = service_iratpeldany.Get(execParam_iratpeldany);
                outputFile += "\"" + ((EREC_PldIratPeldanyok)result.Record).BarCode + "\"\n";
                outputFile += "A140,160,0,4,1,1,N,\"" + ((EREC_PldIratPeldanyok)result.Record).BarCode.Replace(barcodePrefix, "") + "\"\nP\n";
                break;
            case "Kuldemeny":
                EREC_KuldKuldemenyekService service_kuldemeny = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam_kuldemeny = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_kuldemeny.Record_Id = Id;
                result = service_kuldemeny.Get(execParam_kuldemeny);
                outputFile += "\"" + ((EREC_KuldKuldemenyek)result.Record).BarCode.Replace("-", "") + "\"\n";
                outputFile += "A140,160,0,4,1,1,N,\"" + ((EREC_KuldKuldemenyek)result.Record).BarCode.Replace(barcodePrefix, "") + "\"\nP\n";
                break;
            case "Irattarihely":                
                outputFile += "\"" + Id.Replace("-", "") + "\"\n";
                outputFile += "A140,160,0,4,1,1,N,\"" + Id.Replace(barcodePrefix, "") + "\"\nP\n";
                break;
            default:
                break;
        }
        PrintOutput = outputFile;
       // Response.Clear();
       // Response.ClearContent();
       // Response.ClearHeaders();
       //// Response.ContentType = "text/xml";
       // Response.Write(outputFile);
        try
        {
            if (!IsPostBack)
            {
                GetPrinters();
            }
            //PrintBarCode(outputFile, GetPrinters());
        }
        catch (Exception exp)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(exp);
            resError.ErrorMessage = exp.Message;

            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
            if (ErrorUpdatePanel != null)
            {
                ErrorUpdatePanel.Update();
            }
        }
        /*Response.OutputStream.Write(res, 0, res.Length);
        Response.OutputStream.Flush();*/

        //Response.Flush();
        //Response.End();
    }
    private void GetPrinters()
    {//ha t�bb van kiv�laszt�s
        ExecParam ExecParam_Printing = UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eAdmin.Service.KRT_HalozatiNyomtatokService service_printer = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService();
        var res = service_printer.GetByFelhasznalo(ExecParam_Printing, ExecParam_Printing.Felhasznalo_Id);
       // if (res.Ds.Tables[0].Rows.Count > 1)
        NyomtatokDropDown.FillDropDownList(res, "Cim", "Nev", EErrorPanel1);
        if (res.Ds.Tables[0].Rows.Count < 2)
            NyomtatokDropDown.Enabled = false;
        //else
        //    PrintBarCode(res.Ds.Tables[0].Rows[0]);
        return ;
    }
    private void PrintBarCode(string cim)
    {
        byte[] tempFileContent = System.Text.Encoding.Default.GetBytes(PrintOutput);
        string tempDirectory = string.Empty;
        string tempFileName = "printVonalkod_" + DateTime.Now.ToString("yyyyMMddHHmmss");
        Contentum.eRecord.Utility.FileFunctions.Upload.SaveFileToTempDirectory(tempFileContent,
            tempFileName, out tempDirectory);
        string dirPath = Path.Combine(FileFunctions.Upload.tempDirectory, tempDirectory);
        string filePath = Path.Combine(dirPath, tempFileName);
        
        File.Copy(filePath, cim, true);

        Contentum.eRecord.Utility.FileFunctions.Upload.TryToDeleteFromTempTable(tempDirectory, tempFileName);
        return;
    }
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        string cim = NyomtatokDropDown.SelectedValue;
        try
        {
            PrintBarCode(cim);
            //PrintBarCode(outputFile, GetPrinters());
        }
        catch (Exception exp)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(exp);
            resError.ErrorMessage = exp.Message;

            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
            if (ErrorUpdatePanel != null)
            {
                ErrorUpdatePanel.Update();
            }
        } 
        label_result_cim.Text = cim;
        label_result_ertek.Text = PrintOutput;
        ResultPanel_Nyomtatas.Visible = true;
        VonalkodNyomtatoFormPanel.Visible = false;
        FormFooter1.ImageButton_Save.Visible = false;
        FormFooter1.ImageButton_Cancel.Visible = false;
        FormFooter1.ImageButton_Close.Visible = true;
    }

}
