using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class VonalkodosAtadas : Contentum.eUtility.UI.PageBase
{
    private string Mode = "";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "VonalkodosAtadas");

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Mode = Request.QueryString.Get(QueryStringVars.Mode);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Mode == CommandName.Atvetel)
        {
            ListHeader1.HeaderLabel = Resources.Form.VonalkodosAtvetelHeaderTitle;
        }
        else if (Mode == CommandName.UgyiratIrattarAtvetel)
        {
            ListHeader1.HeaderLabel = Resources.Form.VonalkodosIrattarbaVetelHeaderTitle;
        }
        else
        {
            ListHeader1.HeaderLabel = Resources.Form.VonalkodosAtadasHeaderTitle;
        }

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;
        #region Baloldali funkci�gombok kiszed�se
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        #endregion
        ListHeader1.CustomSearchObjectSessionName = "-----";

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        ListHeader1.RefreshImageButton.ToolTip = Resources.Form.UI_PageReload;
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(VonalkodosAtadasUpdatePanel.ClientID);


        registerJavascripts();

    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Url be�ll�t�sa
        VonalkodAtadasButton.Attributes.Add("onclick",
                @"var listBox = $get('" + VonalKodListBoxAtadas.ListBox.ClientID + @"');
                  if (listBox && listBox.options.length > 0)
                  {
                  }
                  else 
                  { 
                     alert('"+ Resources.Error.UINoBarCode + @"'); 
                      return false;
                  }
                ");


        // f�kusz a vonalk�d mez�re
        ScriptManager1.SetFocus(VonalKodListBoxAtadas.TextBox);
    }

    protected void ErrorUpdatePanel1_Load(object sender, EventArgs e)
    {
    }

    public void AtadasButtonOnClick(object sender, EventArgs e)
    {
        Session["SelectedBarcodeIds"] = VonalKodListBoxAtadas.VonalkodClientList;

        Session["SelectedKuldemenyIds"] = null;
        Session["SelectedUgyiratIds"] = null;
        Session["SelectedIratPeldanyIds"] = null;
		Session["SelectedDosszieIds"] = null;

        if (Mode == CommandName.Atvetel)
        {
            ScriptManager.RegisterStartupScript(Page,Page.GetType(),"selectedVonalkodokAtvetel",JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx","",Defaults.PopupWidth_Max,Defaults.PopupHeight,VonalkodosAtadasUpdatePanel.ClientID,EventArgumentConst.refreshMasterList,100).TrimEnd("return false;".ToCharArray()), true);
        }
        else if (Mode == CommandName.UgyiratIrattarAtvetel)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedVonalkodokIrattarbaVetel"
                , JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.UgyiratIrattarAtvetel
                , Defaults.PopupWidth_Max, Defaults.PopupHeight, VonalkodosAtadasUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, 100).TrimEnd("return false;".ToCharArray()), true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedVonalkodokAtadas", JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, VonalkodosAtadasUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, 100).TrimEnd("return false;".ToCharArray()), true);
        }
    }
    protected void VonalkodosAtadasUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    VonalKodListBoxAtadas.Reset();
                    VonalkodosAtadasUpdatePanel.Update();
                    break;
            }
        }

    }
}
