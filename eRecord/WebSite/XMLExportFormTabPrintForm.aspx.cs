using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class XMLExportFormTabPrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;
    private string tipus = String.Empty;
    private string jegyzekId = String.Empty;
    private string iktatokonyvId = String.Empty;
    private string irattariTetelId = String.Empty;
    //BLG_4977
    private string erkeztetokonyvId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
        tipus = Request.QueryString.Get("tipus");

        #region BLG_2951
        jegyzekId = Request.QueryString.Get(QueryStringVars.Jegyzek_Id);
        #endregion

        #region BLG_2967
        iktatokonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);
        #endregion

        #region BLG_2968
        irattariTetelId = Request.QueryString.Get(QueryStringVars.IrattariTetelId);
        #endregion

        #region BLG_4977
        erkeztetokonyvId = Request.QueryString.Get(QueryStringVars.ErkeztetokonyvId);
        #endregion

        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(tipus))
        {
            // nincs Id megadva:
        }
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents_Ugyiratok()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_Iratok()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();

        erec_IraIratokSearch.Id.Value = ugyiratId;
        erec_IraIratokSearch.Id.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_IratPeldanyok()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents_Kuldemenyek()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.Id.Value = ugyiratId;
        erec_KuldKuldemenyekSearch.Id.Operator = Query.Operators.equals;

        return erec_KuldKuldemenyekSearch;
    }

    #region BLG_2967
    private EREC_IrattariTetel_IktatokonyvSearch GetSearchObjectFromComponents_Iktatokonyvek()
    {
        EREC_IrattariTetel_IktatokonyvSearch erec_IrattariTetel_IktatokonyvSearch = new EREC_IrattariTetel_IktatokonyvSearch();

        erec_IrattariTetel_IktatokonyvSearch.Id.Value = iktatokonyvId;
        erec_IrattariTetel_IktatokonyvSearch.Id.Operator = Query.Operators.equals;

        return erec_IrattariTetel_IktatokonyvSearch;
    }
    #endregion

    #region BLG_2968
    private EREC_IraIrattariTetelekSearch GetSearchObjectFromComponents_IrattariTetelek()
    {
        EREC_IraIrattariTetelekSearch erec_IrattariTetel_IktatokonyvSearch = new EREC_IraIrattariTetelekSearch();

        erec_IrattariTetel_IktatokonyvSearch.Id.Value = irattariTetelId;
        erec_IrattariTetel_IktatokonyvSearch.Id.Operator = Query.Operators.equals;

        return erec_IrattariTetel_IktatokonyvSearch;
    }
    #endregion

    #region BLG_2951
    //LZS
    private EREC_IraJegyzekekSearch GetSearchObjectFromComponents_Jegyzekek()
    {
        EREC_IraJegyzekekSearch erec_IraJegyzekekSearch = new EREC_IraJegyzekekSearch();

        erec_IraJegyzekekSearch.Id.Value = jegyzekId;
        erec_IraJegyzekekSearch.Id.Operator = Query.Operators.equals;

        return erec_IraJegyzekekSearch;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Result result = new Result();

        string outputFile = "";
        //BLG 2969 - ugyirat eset�ben eleve XML j�n vissza, �gy ott m�s kezel�s sz�ks�ges
        string res = string.Empty;

        switch (tipus)
        {
            case "Ugyirat":
                EREC_UgyUgyiratokService service_ugyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents_Ugyiratok();

                ExecParam execParam_ugyirat = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_ugyirat.Record_Id = ugyiratId;
                //BLG 2969
                //result = service_ugyirat.GetAllWithExtension(execParam_ugyirat, erec_UgyUgyiratokSearch);
                res = service_ugyirat.CreateUgyiratIratokDexXml(execParam_ugyirat, erec_UgyUgyiratokSearch);
                //res = result.IsError ? result.ErrorMessage : result.Ds.GetXml();

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\Ugyirat " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                break;
            case "Irat":
                EREC_IraIratokService service_irat = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                EREC_IraIratokSearch erec_IraIratokSearch = GetSearchObjectFromComponents_Iratok();

                ExecParam execParam_irat = UI.SetExecParamDefault(Page, new ExecParam());

                result = service_irat.GetAllWithExtension(execParam_irat, erec_IraIratokSearch);

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\Irat " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                break;
            case "IratPeldany":
                EREC_PldIratPeldanyokService service_iratpeldany = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents_IratPeldanyok();

                ExecParam execParam_iratpeldany = UI.SetExecParamDefault(Page, new ExecParam());

                result = service_iratpeldany.GetAllWithExtension(execParam_iratpeldany, erec_PldIratPeldanyokSearch);

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\IratPeldany " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                break;
            case "Kuldemeny":
                EREC_KuldKuldemenyekService service_kuldemeny = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = GetSearchObjectFromComponents_Kuldemenyek();

                ExecParam execParam_kuldemeny = UI.SetExecParamDefault(Page, new ExecParam());

                result = service_kuldemeny.GetAllWithExtension(execParam_kuldemeny, erec_KuldKuldemenyekSearch);

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\Kuldemeny " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                break;
            case "Jegyzek":
                #region BLG_2951
                //LZS
                EREC_IraJegyzekekService service_jegyzekek = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                EREC_IraJegyzekekSearch erec_jegyzekekSearch = GetSearchObjectFromComponents_Jegyzekek();

                ExecParam execParam_jegyzek = UI.SetExecParamDefault(Page, new ExecParam());

                result = service_jegyzekek.GetAllWithExtension(execParam_jegyzek, erec_jegyzekekSearch);

                break;
            #endregion
            case "Iktatokonyv":
                #region BLG_2967
                EREC_IrattariTetel_IktatokonyvService service_iktatokonyv = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
                EREC_IrattariTetel_IktatokonyvSearch erec_IktatokonyvSearch = GetSearchObjectFromComponents_Iktatokonyvek();

                ExecParam execParam_iktatokonyv = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iktatokonyv.Record_Id = iktatokonyvId;

                res = service_iktatokonyv.CreateIktatokonyvIrattariTetelDexXml(execParam_iktatokonyv, erec_IktatokonyvSearch);

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\Ugyirat " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                #endregion
                break;
            case "IrattariTetel":
                #region BLG 2968

                EREC_IraIrattariTetelekService service_irattaritetel = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                EREC_IraIrattariTetelekSearch erec_irattaritetelSearch = GetSearchObjectFromComponents_IrattariTetelek();

                ExecParam execParam_irattariTetel = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_irattariTetel.Record_Id = irattariTetelId;

                res = service_irattaritetel.CreateIrattariTetelUgyiratokDexXml(execParam_irattariTetel, erec_irattaritetelSearch);

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\Ugyirat " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xml";

                #endregion
                break;
            case "Erkeztetokonyv":
                #region BLG_4977
                EREC_IrattariTetel_IktatokonyvService service_erkeztetokonyv = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
                EREC_IrattariTetel_IktatokonyvSearch erec_erkeztetokonyvSearch = GetSearchObjectFromComponents_Iktatokonyvek();

                ExecParam execParam_erkeztetokonyv = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_erkeztetokonyv.Record_Id = erkeztetokonyvId;

                res = service_erkeztetokonyv.CreateErkeztetokonyvContentumXml(execParam_erkeztetokonyv, erec_erkeztetokonyvSearch);

                #endregion
                break;
            default:
                break;
        }

        /*TextWriter tw = new StreamWriter(outputFile);

        tw.Write(result.Ds.GetXml());

        tw.Close();*/

        //BLG 2969
        if (string.IsNullOrEmpty(res))
        {
            res = result.Ds.GetXml();
        }

        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "text/xml";
        Response.Write(res);
        Response.Flush();
        /*Response.OutputStream.Write(res, 0, res.Length);
        Response.OutputStream.Flush();*/
        Response.End();
    }
}
