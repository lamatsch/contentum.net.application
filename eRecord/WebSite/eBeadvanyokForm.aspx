<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="eBeadvanyokForm.aspx.cs" Inherits="eBeadvanyokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
        </Scripts>
    </asp:ScriptManager>
    <script type="text/javascript">
        function ToTopOfPage() {
            window.scrollTo(0, 0);
        }
    </script>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0px" cellspacing="0" width="80%">
        <tr>
            <td align="center">
                <eUI:eFormPanel Width="100%" ID="EFormPanel1" runat="server">
                    <center>
                        <table cellspacing="0px" cellpadding="5px" width="100%">
                            <tr>
                                <!--�ZENET ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">�zenet adatok</span></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokUzenetIranyaField">
                                    <asp:Label ID="uzenetAdatokUzenetIranyaLabel" runat="server" Text="�zenet ir�nya:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokUzenetIranyaTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>

                                </td>
                                <td class="mrUrlapCaption" id="uzenetAdatokKuldoRendszerField">
                                    <asp:Label ID="labelKuldoRendszerLabel" runat="server" Text="K�ld� rendszer:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokKuldoRendszerTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokUzenetAllapotaField">
                                    <asp:Label ID="uzenetAdatokUzenetAllapotaLabel" runat="server" Text="�zenet �llapota:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="uzenetAdatokUzenetAllapotaDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="uzenetAdatokUzenetTipusaField">
                                    <asp:Label ID="uzenetAdatokUzenetTipusaLabel" runat="server" Text="�zenet t�pusa:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokUzenetTipusaTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokDokumentumAzonositoField">
                                    <asp:Label ID="uzenetAdatokDokumentumAzonositoLabel" runat="server" Text="<%$Forditas:LabelDokumentumAzonosito*_AddColon|Dokumentum azonos�t�:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokDokumentumAzonositoTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="dokumentumLeirasField">
                                    <asp:Label ID="uzenetAdatokDokumentumLeirasLabel" runat="server" Text="<%$Forditas:LabelDokumentumLeiras*_AddColon|Dokumentum le�r�s:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokDokumentumLeirasTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokDokumentumHivatalField">
                                    <asp:Label ID="uzenetAdataiDokumentumHivatalLabel" runat="server" Text="<%$Forditas:LabelDokumentumHivatal*_AddColon|Dokumentum hivatal:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdataiDokumentumHivatalTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="eredetiCsatolmanyField">
                                    <asp:Label ID="uzenetAdatokEredetiCsatolmanyLabel" runat="server" Text="Eredeti csatolm�ny:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokEredetiCsatolmanyTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokMegjegyzesField">
                                    <asp:Label ID="uzenetAdataiMegjegyzesLabel" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdataiMegjegyzesTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="uzenetAdatokLetoltesIdejeField">
                                    <asp:Label ID="uzenetAdataiLetoltesIdejeLabel" runat="server" Text="Let�lt�s ideje:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdataiLetoltesIdejeTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <!--PARTNER ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">Partner adatok</span></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerTipusField">
                                    <asp:HiddenField ID="partnerAdatokPartnerIDHiddenField" runat="server" Visible="false" />
                                    <asp:Label ID="partnerAdatokPartnerTipusLabel" runat="server" Text="Partner t�pus:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="partnerAdatokPartnerTipusDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerEmailField">
                                    <asp:Label ID="partnerAdatokPartnerEmailLabel" runat="server" Text="Partner email:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerEmailTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerNevField">
                                    <asp:Label ID="partnerAdatokPartnerNevLabel" runat="server" Text="Partner n�v:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerNevTextBox" runat="server" CssClass="mrUrlapInput" Style="width: 220px"></asp:TextBox>
                                    <asp:HiddenField ID="partnerAdatokPartnerNevIdHidden" runat="server" />
                                    <asp:ImageButton TabIndex="-1"
                                        ID="partnerAdatokPartnerNevSearch" runat="server" CssClass="mrUrlapInputImageButton"
                                        ImageUrl="~/images/hu/egyeb/nagyito.gif"
                                        onmouseover="swapByName(this.id,'nagyito_keret.gif')"
                                        onmouseout="swapByName(this.id,'nagyito.gif')"
                                        AlternateText="Megtekint" />
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerRovidNevField">
                                    <asp:Label ID="partnerAdatokPartnerRovidNevLabel" runat="server" Text="Partner r�vid n�v:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerRovidNevTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerCimField">
                                    <asp:Label ID="partnerAdatokPartnerCimLabel" runat="server" Text="<%$Forditas:LabelPartnerCim*_AddColon|Partner c�m:%>"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerCimTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerMAKKodField">
                                    <asp:Label ID="partnerAdatokPartnerMAKKodLabel" runat="server" Text="Partner M�K k�d:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerMAKKodTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                            <!--KAPU RENDSZER ADATOK -->
                            <tr id="kapuRendszerRow1" runat="server">
                                <td colspan="2" runat="server" align="left"><span class="ListHeaderText">Kapu rendszer adatok</span></td>
                            </tr>

                            <tr id="kapuRendszerRow2" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokHivatkozasiSzamField">
                                    <asp:Label ID="kapuRendszerAdatokHivatkozasiSzamLabel" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokHivatkozasiSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErkeztetesiSzamField">
                                    <asp:Label ID="kapuRendszerAdatokeEkeztetesiSzamLabel" runat="server" Text="�rkeztet�si sz�m:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokErkeztetesiSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow3" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErvenyessegField">
                                    <asp:Label ID="kapuRendszerAdatokErvenyessegLabel" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokErvenyessegTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErkeztetesIdejeField">
                                    <asp:Label ID="kapuRendszerAdatokErkeztetesIdejeLabel" runat="server" Text="�rkeztet�si ideje:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokErkeztetesIdejeTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow4" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokIdopecsetField">
                                    <asp:Label ID="kapuRendszerAdatokIdopecsetLabel" runat="server" Text="Id�pecs�t:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokIdopecsetTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokValaszTitkositasField">
                                    <asp:Label ID="kapuRendszerAdatokValaszTitkositasLabel" runat="server" Text="V�lasz titkos�t�s:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokValaszTitkositasDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow5" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokRendszerUzenetField">
                                    <asp:Label ID="kapuRendszerAdatokRendszerUzenetLabel" runat="server" Text="Rendszer �zenet:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokRendszerUzenetDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokValaszUtvonalField">
                                    <asp:Label ID="kapuRendszerAdatokValaszUtvonalLabel" runat="server" Text="V�lasz �tvonal:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokValaszUtvonalDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="K�zvetlen e-mail" Value="1" />
                                        <asp:ListItem Text="�rtes�t�si hely" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow6" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokTarteruletField">
                                    <asp:Label ID="kapuRendszerAdatokTartTeruletLabel" runat="server" Text="T�rter�let:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="kapuRendszerAdatokTartTeruletDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokFiokField">
                                    <asp:Label ID="kapuRendszerAdatokFiokLabel" runat="server" Text="Fi�k:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokFiokTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                               </tr>
                               <tr id="kapuRendszerRow7" class="urlapSor" runat="server" >
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokCelRendszerField" >
                                    <asp:Label ID="kapuRendszerAdatokCelRendszerLabel" runat="server" Text="C�lrendszer:"></asp:Label>
                                    <uc:KodtarakDropDownList  ID="kapuRendszerAdatokCelRendszerDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>   
                                </td>                               
                            </tr>

                            <tr>
                                <!--EGY�B ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">Egy�b adatok</span></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokHivatkozottIktatoszamField">
                                    <asp:Label ID="egyebAdatokHivatkozottIktatoszamLabel" runat="server" Text="Hivatkozott iktat�sz�m:"></asp:Label>
                                    <asp:HyperLink ID="egyebAdatokHivatkozottIktatoszamLabelLink" runat="server">
                                        <asp:Label runat="server" ID="egyebAdatokHivatkozottIktatoszamLabelLinkLabel" CssClass="mrUrlapInput" Text="Contentum hivatkozas szam"></asp:Label>
                                    </asp:HyperLink>
                                </td>
                                <td class="mrUrlapCaption" id="egyebAdatokEErkeztetoAzonositoField">
                                    <asp:Label ID="egyebAdatokEErkeztetoAzonositoLabel" runat="server" Text="E. �rkeztet� azon.:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokEErkeztetoAzonositoTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>


                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokErkeztetoSzamField">
                                    <asp:Label ID="egyebAdatokErkeztetoSzamLabel" runat="server" Text="�rkeztet�sz�m:"></asp:Label>

                                    <asp:HyperLink ID="egyebAdatokErkeztetoSzamLabelLink" runat="server">
                                        <asp:Label ID="egyebAdatokErkeztetoSzamLabelLinkLabel" runat="server" CssClass="mrUrlapInput" Text="K�ldem�ny ID"></asp:Label>
                                    </asp:HyperLink>
                                </td>
                                <td class="mrUrlapCaption" id="egyebAdatokEHivatkozasiAzonositoField">
                                    <asp:Label ID="egyebAdatokEHivatkozasiAzonositoLabel" runat="server" Text="E. hivatkoz�si azon.:"></asp:Label>
                                    <asp:HyperLink ID="egyebAdatokEHivatkozasiAzonositoLink" runat="server">
                                        <asp:Label ID="egyebAdatokEHivatkozasiAzonositoLinkLabel" runat="server" CssClass="mrUrlapInput" Text="K�ldem�ny ID"></asp:Label>
                                    </asp:HyperLink>
                                </td>
                            </tr>


                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokIktatoSzamField">
                                    <asp:Label ID="egyebAdatokIktatoSzamLabel" runat="server" Text="Iktat�sz�m:"></asp:Label>

                                    <asp:HyperLink ID="egyebAdatokIktatoSzamLabelLink" runat="server">
                                        <asp:Label ID="egyebAdatokIktatoSzamLabelLinkLabel" runat="server" CssClass="mrUrlapInput" Text="K�ldem�ny ID"></asp:Label>
                                    </asp:HyperLink>
                                </td>
                                <td class="mrUrlapCaption" runat="server" id="egyebAdatokETertivevenyField">
                                    <asp:Label ID="egyebAdatokETertivevenyLabel" runat="server" Text="ET�rtivev�ny:"></asp:Label>
                                    <asp:DropDownList ID="egyebAdatokETertivevenyDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <!-- 
                        <tr class="urlapSor" runat="server" id="rowFeladoNev">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartnerNev" runat="server" Text="Felad� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PartnerNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                <asp:HiddenField ID="HiddenField_PartnerId" runat="server" />
                                <asp:ImageButton TabIndex="-1"
                                    ID="ViewPartnerNevImageButton" runat="server" CssClass="mrUrlapInputImageButton"
                                    ImageUrl="~/images/hu/egyeb/nagyito.gif"
                                    onmouseover="swapByName(this.id,'nagyito_keret.gif')"
                                    onmouseout="swapByName(this.id,'nagyito.gif')"
                                    AlternateText="Megtekint" />
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="rowFeladoEmail">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartnerEmail" runat="server" Text="Felad� e-mail c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PartnerEmail" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="rowPartnerRovidNev">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartnerRovidNev" runat="server" Text="Hivatal r�vid neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PartnerRovidNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="rowPartnerMakKod">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartnerMAKKod" runat="server" Text="Hivatal M�K k�dja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PartnerMAKKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartnerKRID" runat="server" Text="Hivatal PartnerKRID-je:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PartnerKRID" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_ErkeztetesiSzam" runat="server" Text="�rkeztet�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_ErkeztetesiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusHivatal" runat="server" Text="<%$Forditas:labelKR_DokTipusHivatal|Dokumentum t�pus hivatal:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusHivatal" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusAzonosito" runat="server" Text="<%$Forditas:labelKR_DokTipusAzonosito|Dokumentum t�pus azonos�t�:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusLeiras" runat="server" Text="<%$Forditas:labelKR_DokTipusLeiras|Dokumentum t�pus le�r�s:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusLeiras" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_HivatkozasiSzam" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_HivatkozasiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Megjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_Megjegyzes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_FileNev" runat="server" Text="F�jln�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_FileNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_ErvenyessegiDatum" runat="server" Text="�rv�nyess�gi d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_ErvenyessegiDatum" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_ErkeztetesiDatum" runat="server" Text="�rkeztet�si d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_ErkeztetesiDatum" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Kezbesitettseg" runat="server" Text="K�zbes�tetts�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="KR_Kezbesitettseg" runat="server" />
                            </td>
                        </tr>
                        <%--                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKR_Idopecset" runat="server" Text="Id�pecs�t:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="KR_Idopecset" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Valasztitkositas" runat="server" Text="V�lasz titkos�t�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_Valasztitkositas" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Valaszutvonal" runat="server" Text="V�lasz �tvonal:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="KR_Valaszutvonal" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Rendszeruzenet" runat="server" Text="Rendszer�zenet:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_Rendszeruzenet" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Tarterulet" runat="server" Text="T�rter�let:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="KR_Tarterulet" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_ETertiveveny" runat="server" Text="ET�rtivev�ny:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_ETertiveveny" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label runat="server" ToolTip="Uzenet tipusa" Text="<%$Resources:Form,EREC_eBeadvanyok_UzenetTipusa %>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="UzenetTipusa" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label runat="server" ToolTip="Elektronikus �rkeztet� azonos�t�:" Text="<%$Resources:Form,KuldemenyKulsoAzonosito %>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="PR_ErkeztetesiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label runat="server" ToolTip="�rkez�s m�dja:" Text="<%$Resources:Form,EREC_eBeadvanyok_KuldoRendszer %>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="DropDownListKuldoRendszer" runat="server"></uc:KodtarakDropDownList>

                                <%--<asp:TextBox ID="TextBox_KuldoRendszer" runat="server" CssClass="mrUrlapInput"></asp:TextBox>--%>
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label runat="server" ToolTip="FeladoTipusa:" Text="<%$Resources:Form,EREC_eBeadvanyok_FeladoTipusa %>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="KodtarakDropDownList_FeladoTipusa" runat="server"></uc:KodtarakDropDownList>
                            </td>
                        </tr>-->

                        </table>
                    </center>
                </eUI:eFormPanel>
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
