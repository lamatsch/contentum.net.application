using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using Contentum.eUtility.EKF;
using System.Web.UI.WebControls;

public partial class eBeadvanyokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    /// <summary>
    ///Recursively iterate all controls and modify the interested ones to readonly
    /// </summary>
    private void SetAllControlsReadOnly(ControlCollection controls)
    {
        foreach (Control control in controls)
        {
            if (control.Controls.Count != 0)
            {
                SetAllControlsReadOnly(control.Controls);
            }
            if (control.GetType() == typeof(CustomTextBox))
            {
                ((CustomTextBox)control).ReadOnly = true;
            }

            if (control.GetType() == typeof(Contentum.eUIControls.eDropDownList))
            {
                ((Contentum.eUIControls.eDropDownList)control).ReadOnly = true;
            }

            if (control.GetType() == typeof(CustomTextBox))
            {
                ((CustomTextBox)control).ReadOnly = true;
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "eBeadvanyok" + Command);

        String id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(id))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return;
        }

        EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        Result result = service.Get(execParam);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        EREC_eBeadvanyok EREC_eBeadvanyok = (EREC_eBeadvanyok)result.Record;
        FillFormDataFromDTO(EREC_eBeadvanyok);

        EREC_IraIratok irat = GetIratFromHivatkozasiSzam(EREC_eBeadvanyok.Contentum_HivatkozasiSzam);
        SetContentumHivatkozasiSzamControl(EREC_eBeadvanyok.Contentum_HivatkozasiSzam, irat);

        AdjustPartnerFieldVisibility();

        if (Command == CommandName.View)
        {
            SetAllControlsReadOnly(this.Controls);
        }

        if (IsELHISZUzenetForras(execParam)) // in this case we should hide all ELHISZ related fields "Kapu rendszer"
        {
            HideKapuRendszerRows();
        }
    }

    private void HideKapuRendszerRows()
    {
        kapuRendszerRow1.Visible = false;
        kapuRendszerRow2.Visible = false;
        kapuRendszerRow3.Visible = false;
        kapuRendszerRow4.Visible = false;
        kapuRendszerRow5.Visible = false;
        kapuRendszerRow6.Visible = false;
        kapuRendszerRow7.Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Elektronikus �zenet";
        //LZS - BUG_4366 
        //Force Scroll to Top
        ScriptManager.RegisterClientScriptBlock(this, Page.GetType(), "ToTheTop", "ToTopOfPage();", true);
        //When  other action is required in this form register here
        //FormFooter1.ButtonsClick += new  System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
    }

    public bool IsELHISZUzenetForras(ExecParam param)
    {
        string eUzenetForras = "eUzenetForras";
        string ELHISZ_VALUE = "ELHISZ";

        Contentum.eIntegrator.Service.INT_ParameterekService service = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        INT_ParameterekSearch search = new INT_ParameterekSearch();

        search.Nev.Value = eUzenetForras;
        search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

        Result res = service.GetAll(param, search);
        string eUzenetForrasValue;
        if (res.IsError)
        {
            Logger.Error(String.Format("GetParameter hiba: {0}", eUzenetForras), param, res);
        }

        if (res.Ds.Tables[0].Rows.Count == 1)
        {
            eUzenetForrasValue = res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
        }
        if (eUzenetForras == ELHISZ_VALUE)
        { return true; }

        return false;
    }
    /// <summary>
    ///DataTransferObject --> form
    /// </summary>
    private void FillFormDataFromDTO(EREC_eBeadvanyok beadvany)
    {
        if (beadvany.Irany == EKFConstants.IranyBejovo)
        {
            uzenetAdatokUzenetIranyaTextBox.Text = "Bej�v�";
        }
        else
        {
            uzenetAdatokUzenetIranyaTextBox.Text = "Kimen�";
        }

        uzenetAdatokKuldoRendszerTextBox.Text = beadvany.KuldoRendszer;
        uzenetAdatokUzenetAllapotaDropDown.FillDropDownList(Contentum.eRecord.BaseUtility.KodTarak.EBEADVANY_KODCSOPORTOK.ALLAPOT, true, FormHeader1.ErrorPanel);
        uzenetAdatokUzenetAllapotaDropDown.SelectedValue = beadvany.Allapot;
        uzenetAdatokUzenetTipusaTextBox.Text = beadvany.UzenetTipusa;
        uzenetAdatokDokumentumAzonositoTextBox.Text = beadvany.KR_DokTipusAzonosito;
        uzenetAdatokDokumentumLeirasTextBox.Text = beadvany.KR_DokTipusLeiras;
        uzenetAdataiDokumentumHivatalTextBox.Text = beadvany.KR_DokTipusHivatal;
        uzenetAdatokEredetiCsatolmanyTextBox.Text = beadvany.KR_FileNev;
        uzenetAdataiMegjegyzesTextBox.Text = beadvany.KR_Megjegyzes;
        // this should be beadvany.KR_LetrehozasiIdo instead of KR_IdoPecset
        uzenetAdataiLetoltesIdejeTextBox.Text = beadvany.KR_ErkeztetesiDatum;

        partnerAdatokPartnerTipusDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.FELADO_TIPUSA, true, FormHeader1.ErrorPanel);
        partnerAdatokPartnerTipusDropDown.SelectedValue = beadvany.FeladoTipusa;
        partnerAdatokPartnerIDHiddenField.Value = beadvany.Partner_Id;
        partnerAdatokPartnerEmailTextBox.Text = beadvany.PartnerEmail;
        partnerAdatokPartnerNevTextBox.Text = beadvany.PartnerNev;
        partnerAdatokPartnerNevIdHidden.Value = string.IsNullOrEmpty(beadvany.Partner_Id) ? beadvany.PartnerNev : beadvany.Partner_Id;
        partnerAdatokPartnerNevSearch.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", partnerAdatokPartnerNevIdHidden.ClientID);
        partnerAdatokPartnerMAKKodTextBox.Text = beadvany.PartnerMAKKod;

        partnerAdatokPartnerCimTextBox.Text = Contentum.eRecord.BaseUtility.PartnerHelper.GetCorrectPartnerCim(beadvany);

        kapuRendszerAdatokHivatkozasiSzamTextBox.Text = beadvany.KR_HivatkozasiSzam;
        kapuRendszerAdatokErkeztetesiSzamTextBox.Text = beadvany.KR_ErkeztetesiSzam;
        kapuRendszerAdatokErvenyessegTextBox.Text = beadvany.KR_ErvenyessegiDatum;
        kapuRendszerAdatokErkeztetesIdejeTextBox.Text = beadvany.KR_ErkeztetesiDatum;
        kapuRendszerAdatokIdopecsetTextBox.Text = beadvany.KR_Idopecset;
        kapuRendszerAdatokValaszTitkositasDropDown.Text = beadvany.KR_Valasztitkositas;
        kapuRendszerAdatokRendszerUzenetDropDown.SelectedValue = beadvany.KR_Rendszeruzenet;
        kapuRendszerAdatokValaszUtvonalDropDown.SelectedValue = beadvany.KR_Valaszutvonal;
        kapuRendszerAdatokTartTeruletDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.TARTERULET, true, FormHeader1.ErrorPanel);
        kapuRendszerAdatokTartTeruletDropDown.SelectedValue = beadvany.KR_Tarterulet;
        kapuRendszerAdatokFiokTextBox.Text = beadvany.KR_Fiok;
        kapuRendszerAdatokCelRendszerDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.eBeadvany_CelRendszer, true, FormHeader1.ErrorPanel);
        kapuRendszerAdatokCelRendszerDropDown.SelectedValue = beadvany.Cel;


        egyebAdatokHivatkozottIktatoszamLabelLinkLabel.Text = beadvany.Contentum_HivatkozasiSzam;
        egyebAdatokHivatkozottIktatoszamLabelLinkLabel.Font.Underline = true;
        egyebAdatokHivatkozottIktatoszamLabelLink.NavigateUrl = Contentum.eUtility.Constants.PagePaths.IraIratokForm(beadvany.Contentum_HivatkozasiSzam);

        egyebAdatokEErkeztetoAzonositoTextBox.Text = beadvany.PR_ErkeztetesiSzam;

        egyebAdatokErkeztetoSzamLabelLinkLabel.Text = beadvany.KuldKuldemeny_Id;
        egyebAdatokErkeztetoSzamLabelLinkLabel.Font.Underline = true;
        egyebAdatokErkeztetoSzamLabelLink.NavigateUrl = Contentum.eUtility.Constants.PagePaths.KuldKuldemenyekForm(beadvany.KuldKuldemeny_Id);

        //LZS - BUG_12673
        //Iktat�sz�m iratId alapj�n
        //egyebAdatokIktatoSzamLabelLinkLabel.Text = beadvany.IratPeldany_Id;
        egyebAdatokIktatoSzamLabelLinkLabel.Text = GetIktatoSzam(beadvany.IraIrat_Id);
        egyebAdatokIktatoSzamLabelLinkLabel.Font.Underline = true;
        egyebAdatokIktatoSzamLabelLink.NavigateUrl = Contentum.eUtility.Constants.PagePaths.IraIratokForm(beadvany.IraIrat_Id);
        egyebAdatokETertivevenyDropDown.SelectedValue = string.IsNullOrEmpty(beadvany.KR_ETertiveveny) ? "0" : beadvany.KR_ETertiveveny;
        AdjustPartnerFieldVisibility();

        partnerAdatokPartnerTipusDropDown.SelectedIndexChanged += PartnerAdatokPartnerTipusDropDown_SelectedIndexChanged;
    }

    private void PartnerAdatokPartnerTipusDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        AdjustPartnerFieldVisibility();
    }

    public string GetIktatoSzam(string iratId)
    {
        if (string.IsNullOrEmpty(iratId))
            return null;

        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = iratId;

        Result result = service.Get(execParam);
        if (!result.IsError)
        {
            return ((EREC_IraIratok)result.Record).Azonosito;
        }

        return null;
    }

    public void AdjustPartnerFieldVisibility()
    {
        string currentTipus = partnerAdatokPartnerTipusDropDown.SelectedValue;

        if (Contentum.eRecord.BaseUtility.PartnerHelper.IsNemTermeszetesSzemely(currentTipus))
        {
            partnerAdatokPartnerEmailField.Visible = false;
            partnerAdatokPartnerRovidNevField.Visible = true;
            partnerAdatokPartnerMAKKodField.Visible = true;
        }
        if (Contentum.eRecord.BaseUtility.PartnerHelper.IsTermeszetesSzemely(currentTipus))
        {
            partnerAdatokPartnerRovidNevField.Visible = false;
            partnerAdatokPartnerMAKKodField.Visible = false;
            partnerAdatokPartnerEmailField.Visible = true;
        }
    }


    protected string DisplayContentumHivatkozasiSzam(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return Resources.Form.Automatikusan_nem_azonosithato;
        }
        else
        {
            return value.ToString();
        }
    }
    protected string DisplayContentumHivatkozasiSzamLink(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return "#";
        }
        else
        {
            return GetIratContentumPageLink(value.ToString());
        }
    }
    /// <summary>
    /// GetIratContentumPageLink
    /// </summary>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private string GetIratContentumPageLink(string iratId)
    {
        if (iratId == null)
            return string.Empty;

        string eRecordWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(EKFConstants.AppKey_eRecordBusinessServiceUrl);

        if (string.IsNullOrEmpty(eRecordWsUrl))
            return string.Empty;
        return string.Format("{0}{1}{2}", eRecordWsUrl.Replace(EKFConstants.eRecordWebServiceSiteName, EKFConstants.eRecordWebSiteName), EKFConstants.ContentumIratPageFormLink, iratId);
    }

    private EREC_IraIratok GetIratFromHivatkozasiSzam(string azonosito)
    {
        if (string.IsNullOrEmpty(azonosito))
            return null;

        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = azonosito;

        Result result = service.Get(execParam);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            return (EREC_IraIratok)result.Record;
        }
        return null;
    }

    private void SetContentumHivatkozasiSzamControl(string contentumHivatkozasiSzam, EREC_IraIratok irat)
    {
        if (string.IsNullOrEmpty(contentumHivatkozasiSzam))
        {
            egyebAdatokEHivatkozasiAzonositoLinkLabel.Text = Resources.Form.Automatikusan_nem_azonosithato;
            egyebAdatokEHivatkozasiAzonositoLink.NavigateUrl = "";
        }
        else if (irat != null)
        {
            egyebAdatokEHivatkozasiAzonositoLinkLabel.Text = irat.Azonosito;
            egyebAdatokEHivatkozasiAzonositoLink.NavigateUrl = DisplayContentumHivatkozasiSzamLink(contentumHivatkozasiSzam);
        }
        else
        {
            egyebAdatokEHivatkozasiAzonositoLinkLabel.Text = Resources.Form.Automatikusan_nem_azonosithato;
            egyebAdatokEHivatkozasiAzonositoLink.NavigateUrl = "";
        }
    }


    private void AdjustControlVisibilityByFeladoTipus(string feladoTipus)
    {
        switch (feladoTipus)
        {
            case KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_Ugyfelkapu:
                partnerAdatokPartnerRovidNevField.Visible = false;
                partnerAdatokPartnerMAKKodField.Visible = false;
                break;
            case KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_HivataliKapu:
                partnerAdatokPartnerEmailField.Visible = false;
                break;
            case KodTarak.EBEADVANY_FELADO_TIPUS.Rendszer:
                partnerAdatokPartnerRovidNevField.Visible = false;
                partnerAdatokPartnerEmailField.Visible = false;
                partnerAdatokPartnerMAKKodField.Visible = false;
                break;
            case KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_Cegkapu:
                partnerAdatokPartnerRovidNevField.Visible = false;
                partnerAdatokPartnerEmailField.Visible = false;
                partnerAdatokPartnerMAKKodField.Visible = false;
                break;
            default:
                break;
        }
    }

    /* Not used currently. Use it when other action is required
  private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
  {
      if (e.CommandName == CommandName.Save)
      {
          if (FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + Command))
          {
              switch (Command)
              {
                  case CommandName.New:
                      {

                          EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                          EREC_eBeadvanyok EREC_eBeadvanyok = GetBusinessObjectFromComponents();

                          ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                          Csatolmany csatolmany = null;

                          Result result = service.InsertAndAttachDocument(execParam, EREC_eBeadvanyok, new Csatolmany[] { csatolmany });

                          if (String.IsNullOrEmpty(result.ErrorCode))
                          {
                              JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                              JavaScripts.RegisterCloseWindowClientScript(Page, true);
                          }
                          else
                          {
                              ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                          }
                          break;
                      }
              }
          }
          else
          {
              UI.RedirectToAccessDeniedErrorPage(Page);
          }
      }
  }*/
}
