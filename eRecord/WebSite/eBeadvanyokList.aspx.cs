﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eUtility.EKF;

public partial class eBeadvanyokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "eBeadvanyokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keresési objektum típusának megadása
        ListHeader1.SearchObjectType = typeof(EREC_eBeadvanyokSearch);

        //Fő lista megjelenések testreszabása
        ListHeader1.HeaderLabel = "Elektronikus üzenetek";

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.BejovoIratIktatasVisible = true;
        //ListHeader1.ErkeztetesVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //Fő lista gombokhoz kliens oldali szkriptek regisztrálása
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = "";
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokForm.aspx"
                 , CommandName.Command + "=" + CommandName.New
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = "";
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewEREC_eBeadvanyok.ClientID, "", "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewEREC_eBeadvanyok.ClientID);
        ListHeader1.HistoryOnClientClick = "";

        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.BejovoIratIktatas.ToolTip = "Elektronikus üzenet érkeztetés, iktatás";
        //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEREC_eBeadvanyok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewEREC_eBeadvanyok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEREC_eBeadvanyok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = gridViewEREC_eBeadvanyok;

        //Fő lista összes kiválasztása és az összes kiválasztás megszüntetése checkbox-aihoz szkriptek beállítása
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEREC_eBeadvanyok);

        //Fő lista eseménykezelő függvényei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztrálása
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //selectedRecordId kezelése
        eBeadvanyCsatolmanyokSubListHeader.AttachedGridView = gridVieweBeadvanyCsatolmanyok;

        //szkriptek regisztrálása
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        //TabContainer beregisztrálása a ScriptManager-ben
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //Hibaüzenet panel elüntetése
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, e-mail megtekintés */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_eBeadvanyokSearch());

        //A fő lista adatkötés, ha nem postback a kérés
        if (!IsPostBack)
            EREC_eBeadvanyokGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        //ScriptManager1.RegisterAsyncPostBackControl(btnPostaFiokFeldolgozas);

        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Fő lista jogosultságainak beállítása
        ListHeader1.NewEnabled = false;
        ListHeader1.NewVisible = false;

        //teszt
        //ListHeader1.NewEnabled = true;
        //ListHeader1.NewVisible = true;

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.View);
        ListHeader1.ModifyEnabled = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.ViewHistory);

        // erkeztetes
        //ListHeader1.ErkeztetesEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.New);
        // Email érkeztetés/iktatás:
        ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyokErkeztetes")
                                    || FunctionRights.GetFunkcioJog(Page, "eBeadvanyokIktatas");

        //ListHeader1.BejovoIratIktatasEnabled = false;

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.Lock);

        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyok" + CommandName.ExcelExport);

        //Allisták funkcióinak engedélyezése jogosultságok alapján
        eBeadvanyCsatolmanyokSubListHeader.NewEnabled = false;
        eBeadvanyCsatolmanyokSubListHeader.NewVisible = false;
        eBeadvanyCsatolmanyokSubListHeader.ViewEnabled = false;
        eBeadvanyCsatolmanyokSubListHeader.ViewVisible = false;
        eBeadvanyCsatolmanyokSubListHeader.ModifyEnabled = false;
        eBeadvanyCsatolmanyokSubListHeader.ModifyVisible = false;
        eBeadvanyCsatolmanyokSubListHeader.InvalidateEnabled = false;
        eBeadvanyCsatolmanyokSubListHeader.InvalidateVisible = false;
        eBeadvanyCsatolmanyokSubListHeader.ValidFilterVisible = false;

        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        if (String.IsNullOrEmpty(MasterListSelectedRowId))
        {
            ActiveTabClear();
        }
        ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
    }

    #region Master List

    //adatkötés meghívása default értékekkel
    protected void EREC_eBeadvanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEREC_eBeadvanyok", ViewState, "EREC_eBeadvanyok.KR_ErkeztetesiDatum");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEREC_eBeadvanyok", ViewState, SortDirection.Descending);

        EREC_eBeadvanyokGridViewBind(sortExpression, sortDirection);
    }

    protected void EREC_eBeadvanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        var defaultSearch = Search.GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page,true);
        EREC_eBeadvanyokSearch search = (EREC_eBeadvanyokSearch)Search.GetSearchObject(Page, defaultSearch);
        if ( search == defaultSearch) // A "Státusz: Érkeztetés nélküli" kiírásához kell
        {
            Search.SetSearchObject(Page, search);
        }

        search.OrderBy = Search.GetOrderBy("gridViewEREC_eBeadvanyok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);
        
        UI.SetPaging(execParam, ListHeader1);

        search.Irany.Filter("0"); //Bejövő

        Result res = service.GetAllWithExtension(execParam, search);

        UI.GridViewFill(gridViewEREC_eBeadvanyok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    //A GridView RowDataBound esemény kezelője, ami egy sor adatkötése után hívódik
    protected void gridViewEREC_eBeadvanyok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Érkeztetve checkbox beállítása a kuldemény id alapján
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imageButtonErkeztetve = (ImageButton)e.Row.FindControl("imageButtonErkeztetve");
            ImageButton imageButtonIktatva = (ImageButton)e.Row.FindControl("ImageButtonIktatva");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            #region Érkeztetve ikon beállítása
            if (drw != null && drw["KuldKuldemeny_Id"] != null)
            {
                string kuldemenyId = drw["KuldKuldemeny_Id"].ToString();

                if (String.IsNullOrEmpty(kuldemenyId))
                {
                    imageButtonErkeztetve.Visible = false;
                    imageButtonErkeztetve.OnClientClick = String.Empty;
                }
                else
                {
                    string erkeztetoSzam = String.Empty;
                    if (drw["ErkeztetoSzam"] != null)
                    {
                        erkeztetoSzam = drw["ErkeztetoSzam"].ToString();
                    }

                    imageButtonErkeztetve.Visible = true;
                    imageButtonErkeztetve.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + kuldemenyId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(erkeztetoSzam))
                    {
                        // Érkeztetőszám megjelenítése a Tooltip-ben
                        imageButtonErkeztetve.ToolTip = erkeztetoSzam + " küldemény megtekintése";
                        imageButtonErkeztetve.AlternateText = imageButtonErkeztetve.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonErkeztetve.Visible = false;
                imageButtonErkeztetve.OnClientClick = String.Empty;
            }
            #endregion

            #region Iktatva ikon beállítása

            if (drw != null && drw["IratIktatoszamEsId"] != null)
            {
                string IratIktatoszamEsId = drw["IratIktatoszamEsId"].ToString();

                if (String.IsNullOrEmpty(IratIktatoszamEsId))
                {
                    imageButtonIktatva.Visible = false;
                    imageButtonIktatva.OnClientClick = String.Empty;
                }
                else
                {
                    string iktatoszam = this.GetIktatoszamFrom_IratIktatoszamEsId(IratIktatoszamEsId);
                    string iratId = this.GetIdFrom_IratIktatoszamEsId(IratIktatoszamEsId);

                    imageButtonIktatva.Visible = true;
                    imageButtonIktatva.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(iktatoszam))
                    {
                        // Érkeztetőszám megjelenítése a Tooltip-ben
                        imageButtonIktatva.ToolTip = iktatoszam + " irat megtekintése";
                        imageButtonIktatva.AlternateText = imageButtonIktatva.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonIktatva.Visible = false;
                imageButtonIktatva.OnClientClick = String.Empty;
            }

            #endregion

            #region cbIsIktathatoKuldemeny beállítása (érkeztetett, nem iktatott)
            CheckBox cbIsIktathatoKuldemeny = (CheckBox)e.Row.FindControl("cbIsIktathatoKuldemeny");
            if (cbIsIktathatoKuldemeny != null && imageButtonErkeztetve != null && imageButtonIktatva != null)
            {
                // érkeztetett, nem iktatott:
                cbIsIktathatoKuldemeny.Checked = imageButtonErkeztetve.Visible && !imageButtonIktatva.Visible;
            }
            #endregion cbIsIktathatoKuldemeny beállítása (érkeztetett, nem iktatott)
        }

        GridView_RowDataBound_SetCsatolmanyInfo(e);
        //Lockolás jelzése
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetCsatolmanyInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drw = (DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "return false;";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            //DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }
            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }
            }
        }
    }

    //GridView PreRender eseménykezelője
    protected void gridViewEREC_eBeadvanyok_PreRender(object sender, EventArgs e)
    {
        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEREC_eBeadvanyok);

        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEREC_eBeadvanyok);
        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EREC_eBeadvanyokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEREC_eBeadvanyok);
        EREC_eBeadvanyokGridViewBind();
    }

    //GridView RowCommand eseménykezelője, amit valamelyik sorban fellépett parancs vált ki
    protected void gridViewEREC_eBeadvanyok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiválasztása (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEREC_eBeadvanyok, selectedRowNumber, "check");

            string id = gridViewEREC_eBeadvanyok.DataKeys[selectedRowNumber].Value.ToString();
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //Fő lista kiválasztott elem esetén érvényes funkciók regisztrálása
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("eBeadvanyokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEREC_eBeadvanyok.ClientID);
            ListHeader1.ModifyOnClientClick = "";
            string tableName = "EREC_eBeadvanyok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelEREC_eBeadvanyok.ClientID);


            EREC_eBeadvanyokService EREC_eBeadvanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailGet.Record_Id = id;

            Result result_emailGet = EREC_eBeadvanyokService.Get(execparam_emailGet);
            if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                ErrorUpdatePanel.Update();
                return;
            }

            EREC_eBeadvanyok EREC_eBeadvanyok = (EREC_eBeadvanyok)result_emailGet.Record;

            // ha lehet érkeztetni:
            if (String.IsNullOrEmpty(EREC_eBeadvanyok.KuldKuldemeny_Id) && String.IsNullOrEmpty(EREC_eBeadvanyok.IraIrat_Id))
            {
                //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                //    , CommandName.Command + "=" + CommandName.New + "&" + QueryStringVars.EREC_eBeadvanyokId + "=" + id
                //    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);

                ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New + "&" + "eBeadvanyokId" + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // alert('Az email érkeztetett');
                //ListHeader1.ErkeztetesOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                if (!String.IsNullOrEmpty(EREC_eBeadvanyok.IraIrat_Id))
                {
                    ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az üzenet már iktatva van!'); return false;";
                }
                else
                {
                    if (gridViewEREC_eBeadvanyok.SelectedIndex > -1 && gridViewEREC_eBeadvanyok.SelectedIndex < gridViewEREC_eBeadvanyok.Rows.Count)
                    {
                        GridViewRow gvrow = gridViewEREC_eBeadvanyok.Rows[gridViewEREC_eBeadvanyok.SelectedIndex];

                        if (gvrow != null)
                        {
                            CheckBox cbIsIktathatoKuldemeny = (CheckBox)gvrow.FindControl("cbIsIktathatoKuldemeny");

                            if (cbIsIktathatoKuldemeny.Checked)
                            {

                                // Allapot szerint beallitja a headerben a gombok viselkedeset:
                                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(cbIsIktathatoKuldemeny.Text, Page, EErrorPanel1);

                                if (statusz == null)
                                {
                                    ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                                }
                                else
                                {
                                    ExecParam execparam = UI.SetExecParamDefault(Page);
                                    ErrorDetails errorDetail = null;
                                    if (Kuldemenyek.Iktathato(execparam, statusz, out errorDetail))
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                                            , QueryStringVars.Command + "=" + CommandName.New + "&" + "eBeadvanyokId" + "=" + id
                                            + "&" + QueryStringVars.KuldemenyId + "=" + cbIsIktathatoKuldemeny.Text
                                            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEREC_eBeadvanyok.ClientID, EventArgumentConst.refreshMasterList);
                                    }
                                    else
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.UINemIktathatoKuldemeny
                                            + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                                            + "'); return false;";
                                    }
                                }
                            }
                            else
                            {
                                ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az üzenet már iktatva van!'); return false;";
                            }
                        }
                    }
                    else
                    {
                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                    }
                }
            }
        }
    }

    //UpdatePanel Load eseménykezelője
    protected void updatePanelEREC_eBeadvanyok_Load(object sender, EventArgs e)
    {
        //Fő lista frissítése
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    EREC_eBeadvanyokGridViewBind();
                    break;
            }
        }
    }

    //Fő lista bal oldali funkciógombjainak eseménykezelője
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //törlés parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            bool success = deleteSelectedEmailBoritek();
            if (success)
            {
                EREC_eBeadvanyokGridViewBind();
            }
        }

        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, gridViewEREC_eBeadvanyok, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
    }

    //kiválasztott elemek törlése
    private bool deleteSelectedEmailBoritek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "eBeadvanyokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewEREC_eBeadvanyok, EErrorPanel1, ErrorUpdatePanel);

            int markedRows = MarkSelectedRowsWhichCannotInvalidate();

            if (markedRows > 0)
            {
                string errorMessage = (deletableItemsList.Count == 1) ? Resources.Error.UIeBeadvanyNemTorolheto
                    : Resources.Error.UIeBeadvanyokNemTorolhetok;
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, errorMessage);
                ErrorUpdatePanel.Update();
                return false;
            }

            EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return false;
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        return true;
    }

    private int MarkSelectedRowsWhichCannotInvalidate()
    {
        int markedRows = 0;

        foreach (GridViewRow row in gridViewEREC_eBeadvanyok.Rows)
        {
            CheckBox checkB = (CheckBox)row.FindControl("check");
            // ha ki van jelölve a sor:
            if (checkB.Checked)
            {
                // érkeztetve vagy iktatva van-e már?

                Label label_KuldemenyId = (Label)row.FindControl("Label_KuldemenyId");
                Label label_IratId = (Label)row.FindControl("Label_IratId");

                // ha már érkeztetve vagy iktatva van:
                if ((label_KuldemenyId != null && !String.IsNullOrEmpty(label_KuldemenyId.Text))
                    || (label_IratId != null && !String.IsNullOrEmpty(label_IratId.Text)))
                {
                    markedRows++;

                    //row.Attributes["style"] += " background-color: Coral; ";
                    row.BackColor = System.Drawing.Color.Coral;
                }
                else
                {
                    row.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                // Csak hogy eltűnjön a színezés a következő próbálkozáskor:
                row.BackColor = System.Drawing.Color.White;
            }
        }

        return markedRows;
    }



    //Fő lista jobb oldali funkciógombjainak eseménykezelője
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail küldés parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedEmailBoritekRecords();
                EREC_eBeadvanyokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedEmailBoritekRecords();
                EREC_eBeadvanyokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedEREC_eBeadvanyok();
                EREC_eBeadvanyokGridViewBind();
                break;
        }
    }

    //kiválasztott elemek zárolása
    private void lockSelectedEmailBoritekRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewEREC_eBeadvanyok, "EREC_eBeadvanyok"
            , "eBeadvanyokLock", "eBeadvanyokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiválasztott emlemek elengedése
    /// </summary>
    private void unlockSelectedEmailBoritekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewEREC_eBeadvanyok, "EREC_eBeadvanyok"
            , "eBeadvanyokLock", "eBeadvanyokForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEREC_eBeadvanyok -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEREC_eBeadvanyok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEREC_eBeadvanyok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_eBeadvanyok");
        }
    }

    //GridView Sorting esménykezelője
    protected void gridViewEREC_eBeadvanyok_Sorting(object sender, GridViewSortEventArgs e)
    {
        EREC_eBeadvanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewEREC_eBeadvanyok", ViewState, e.SortExpression));
    }

    #endregion

    public string GetDocumentLink(Guid? dokumentumId, string fileNev)
    {
        if (dokumentumId != null)
        {
            return String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('GetDocumentContent.aspx?id={0}'); return false;\" style=\"text-decoration:underline\">{1}</a>", dokumentumId, fileNev);
        }
        else
        {
            return fileNev;
        }
    }

    #region segedFv

    // Segedfv.

    private const string IratIktatoszamEsId_Delimitter = "***";

    protected string GetIktatoszamFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return String.Empty;
        }
    }

    protected string GetIdFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return String.Empty;
            }
        }
        else
        {
            return String.Empty;
        }
    }

    protected string DisplayBoolean(object value)
    {
        if (value != null)
        {
            if ("1".Equals(value.ToString()))
            {
                return "Igen";
            }
        }

        return "Nem";
    }

    #endregion

    protected void btnPostaFiokFeldolgozas_Click(object sender, EventArgs e)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        svc.Timeout = (int)TimeSpan.FromMinutes(60).TotalMilliseconds;
        try
        {
            svc.PostaFiokFeldolgozas();
            EREC_eBeadvanyokGridViewBind();
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", ex.Message);
            ErrorUpdatePanel.Update();
        }
    }

    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok));
        }
    }

    /// <summary>
    /// TabContainer ActiveTabChanged eseménykezelője
    /// A kiválasztott record adataival frissíti az új panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewEREC_eBeadvanyok.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az aktív panel frissítése a fő listában kiválasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JavaScripts.ResetScroll(Page, cpeeBeadvanyCsatolmanyok);
                eBeadvanyCsatolmanyokGridViewBind(masterId);
                paneleBeadvanyCsatolmanyok.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az aktív panel tartalmának törlése
    /// </summary>
    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(gridVieweBeadvanyCsatolmanyok);
                break;
        }
    }

    /// <summary>
    /// Az allisták egy oldalon megjelenített sorok számának beállítása
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        eBeadvanyCsatolmanyokSubListHeader.RowCount = RowCount;
    }

    #endregion

    #region eBeadvanyCsatolmanyok Detail

    /// <summary>
    /// A eBeadvanyCsatolmanyok GridView adatkötése a EmailBoritekID függvényében alapértelmezett
    /// rendezési paraméterekkel
    /// </summary>
    /// <param name="EmailBoritekId"></param>
    protected void eBeadvanyCsatolmanyokGridViewBind(String EmailBoritekId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridVieweBeadvanyCsatolmanyok", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridVieweBeadvanyCsatolmanyok", ViewState);

        eBeadvanyCsatolmanyokGridViewBind(EmailBoritekId, sortExpression, sortDirection);
    }

    /// <summary>
    /// A eBeadvanyCsatolmanyok GridView adatkötése a EmailBoritekID és rendezési paraméterek függvényében
    /// </summary>
    /// <param name="EmailBoritekId"></param>
    /// <param name="SortExpression"></param>
    /// <param name="SortDirection"></param>
    protected void eBeadvanyCsatolmanyokGridViewBind(String EmailBoritekId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(EmailBoritekId))
        {
            EREC_eBeadvanyCsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_eBeadvanyCsatolmanyokSearch _EREC_eBeadvanyCsatolmanyokSearch = (EREC_eBeadvanyCsatolmanyokSearch)Search.GetSearchObject(Page, new EREC_eBeadvanyCsatolmanyokSearch());
            _EREC_eBeadvanyCsatolmanyokSearch.OrderBy = Search.GetOrderBy("gridVieweBeadvanyCsatolmanyok", ViewState, SortExpression, SortDirection);
            _EREC_eBeadvanyCsatolmanyokSearch.eBeadvany_Id.Value = EmailBoritekId;
            _EREC_eBeadvanyCsatolmanyokSearch.eBeadvany_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result res = service.GetAll(ExecParam, _EREC_eBeadvanyCsatolmanyokSearch);
            UI.GridViewFill(gridVieweBeadvanyCsatolmanyok, res, eBeadvanyCsatolmanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {
            ui.GridViewClear(gridVieweBeadvanyCsatolmanyok);
        }
    }

    /// <summary>
    /// eBeadvanyCsatolmanyok updatepanel Load eseménykezelője
    /// eBeadvanyCsatolmanyok panel frissítése, amennyiben aktív
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void updatePaneleBeadvanyCsatolmanyok_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    ActiveTabRefreshDetailList(tabPaneleBeadvanyCsatolmanyok);
                    break;
            }
        }
    }


    /// <summary>
    /// eBeadvanyCsatolmanyok GridView PreRender eseménykezelője
    /// Lapozás kezelése
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridVieweBeadvanyCsatolmanyok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridVieweBeadvanyCsatolmanyok.PageIndex;
        gridVieweBeadvanyCsatolmanyok.PageIndex = eBeadvanyCsatolmanyokSubListHeader.PageIndex;

        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != gridVieweBeadvanyCsatolmanyok.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, cpeeBeadvanyCsatolmanyok);
            eBeadvanyCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok));
        }
        else
        {
            UI.GridViewSetScrollable(eBeadvanyCsatolmanyokSubListHeader.Scrollable, cpeeBeadvanyCsatolmanyok);
        }

        eBeadvanyCsatolmanyokSubListHeader.PageCount = gridVieweBeadvanyCsatolmanyok.PageCount;
        eBeadvanyCsatolmanyokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(gridVieweBeadvanyCsatolmanyok);
    }

    void eBeadvanyCsatolmanyokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(eBeadvanyCsatolmanyokSubListHeader.RowCount);
        eBeadvanyCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok));
    }

    /// <summary>
    /// eBeadvanyCsatolmanyok GridView Sorting eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridVieweBeadvanyCsatolmanyok_Sorting(object sender, GridViewSortEventArgs e)
    {
        eBeadvanyCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEREC_eBeadvanyok)
            , e.SortExpression, UI.GetSortToGridView("gridVieweBeadvanyCsatolmanyok", ViewState, e.SortExpression));
    }

    #endregion
    /// <summary>
    /// UI - DisplayContentumHivatkozasiSzam
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    protected string DisplayContentumHivatkozasiSzam(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return Resources.Form.Automatikusan_nem_azonosithato;
        }
        else
        {
            return value.ToString();
        }
    }
    protected string DisplayContentumHivatkozasiSzamLink(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return "";
        }
        else
        {
            return GetIratContentumPageLink(value.ToString());
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private string GetIratContentumPageLink(string iratId)
    {
        if (iratId == null)
            return string.Empty;

        string eRecordWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(EKFConstants.AppKey_eRecordBusinessServiceUrl);

        if (string.IsNullOrEmpty(eRecordWsUrl))
            return string.Empty;
        return string.Format("{0}{1}{2}", eRecordWsUrl.Replace(EKFConstants.eRecordWebServiceSiteName, EKFConstants.eRecordWebSiteName), EKFConstants.ContentumIratPageFormLink, iratId);
    }


}