<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="eBeadvanyokSearch.aspx.cs" Inherits="eBeadvanyokSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc10" %>

<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="~/Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <eUI:eFormPanel Width="100%" ID="EFormPanel1" runat="server">
                    <center>
                        <table cellspacing="0" cellpadding="0" width="90%">
                            <tr>
                                <!--�ZENET ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">�zenet adatok</span></td>
                            </tr>
                            <tr class="urlapSor" runat="server">
                               <%-- <td class="mrUrlapCaption" id="uzenetAdatokUzenetIranyaField">
                                    <asp:Label ID="uzenetAdatokUzenetIranyaLabel" runat="server" Text="�zenet ir�nya:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="uzenetAdatokUzenetIranyaDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td> --%>
                                <td class="mrUrlapCaption" id="uzenetAdatokKuldoRendszerField">
                                    <asp:Label ID="labelKuldoRendszerLabel" runat="server" Text="K�ld� rendszer:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokKuldoRendszerTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokUzenetAllapotaField">
                                    <asp:Label ID="uzenetAdatokUzenetAllapotaLabel" runat="server" Text="�zenet �llapota:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="uzenetAdatokUzenetAllapotaDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="uzenetAdatokUzenetTipusaField">
                                    <asp:Label ID="uzenetAdatokUzenetTipusaLabel" runat="server" Text="�zenet t�pusa:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokUzenetTipusaTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokDokumentumAzonositoField">
                                    <asp:Label ID="uzenetAdatokDokumentumAzonositoLabel" runat="server" Text="<%$Forditas:LabelDokumentumAzonosito*_AddColon|Dokumentum azonos�t�:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokDokumentumAzonositoTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="dokumentumLeirasField">
                                    <asp:Label ID="uzenetAdatokDokumentumLeirasLabel" runat="server" Text="<%$Forditas:LabelDokumentumLeiras*_AddColon|Dokumentum le�r�s:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokDokumentumLeirasTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokDokumentumHivatalField">
                                    <asp:Label ID="uzenetAdataiDokumentumHivatalLabel" runat="server" Text="<%$Forditas:LabelDokumentumHivatal*_AddColon|Dokumentum hivatal:%>"></asp:Label>
                                    <asp:TextBox ID="uzenetAdataiDokumentumHivatalTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="eredetiCsatolmanyField">
                                    <asp:Label ID="uzenetAdatokEredetiCsatolmanyLabel" runat="server" Text="Eredeti csatolm�ny:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdatokEredetiCsatolmanyTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="uzenetAdatokMegjegyzesField">
                                    <asp:Label ID="uzenetAdataiMegjegyzesLabel" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    <asp:TextBox ID="uzenetAdataiMegjegyzesTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="uzenetAdatokLetoltesIdejeField">
                                    <asp:Label ID="uzenetAdataiLetoltesIdejeLabel" runat="server" Text="Let�lt�s ideje:"></asp:Label>
                                    <uc:DatumIntervallum_SearchCalendarControl ID="uzenetAdataiLetoltesIdejeDateInterval" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>

                            <tr>
                                <!--PARTNER ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">Partner adatok</span></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerTipusField">
                                    <asp:Label ID="partnerAdatokPartnerTipusLabel" runat="server" Text="Partner t�pus:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="partnerAdatokPartnerTipusDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerEmailField">
                                    <asp:Label ID="partnerAdatokPartnerEmailLabel" runat="server" Text="Partner email:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerEmailTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerNevField">
                                    <asp:Label ID="partnerAdatokPartnerNevLabel" runat="server" Text="Partner n�v:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerNevTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerRovidNevField">
                                    <asp:Label ID="partnerAdatokPartnerRovidNevLabel" runat="server" Text="Partner r�vid n�v:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerRovidNevTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerCimField">                                
                                    <asp:Label ID="partnerAdatokPartnerCimLabel" runat="server" Text="<%$Forditas:LabelPartnerCim*_AddColon|Partner c�m:%>"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerCimTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="partnerAdatokPartnerMAKKodField">
                                    <asp:Label ID="partnerAdatokPartnerMAKKodLabel" runat="server" Text="Partner M�K k�d:"></asp:Label>
                                    <asp:TextBox ID="partnerAdatokPartnerMAKKodTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                            <!--KAPU RENDSZER ADATOK -->
                            <tr id="kapuRendszerRow1" runat="server">
                                <td colspan="2" runat="server" align="left"><span class="ListHeaderText">Kapu rendszer adatok</span></td>
                            </tr>

                            <tr id="kapuRendszerRow2" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokHivatkozasiSzamField">
                                    <asp:Label ID="kapuRendszerAdatokHivatkozasiSzamLabel" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokHivatkozasiSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErkeztetesiSzamField">
                                    <asp:Label ID="kapuRendszerAdatokeEkeztetesiSzamLabel" runat="server" Text="�rkeztet�si sz�m:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokErkeztetesiSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow3" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErvenyessegField">
                                    <asp:Label ID="kapuRendszerAdatokErvenyessegLabel" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                    <uc:DatumIntervallum_SearchCalendarControl ID="kapuRendszerAdatokErvenyessegDateInterval" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokErkeztetesIdejeField">
                                    <asp:Label ID="kapuRendszerAdatokErkeztetesIdejeLabel" runat="server" Text="�rkeztet�si ideje:"></asp:Label>
                                    <uc:DatumIntervallum_SearchCalendarControl ID="kapuRendszerAdatokErkeztetesIdejeDateInterval" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow4" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokIdopecsetField">
                                    <asp:Label ID="kapuRendszerAdatokIdopecsetLabel" runat="server" Text="Id�pecs�t:"></asp:Label>
                                    <uc:DatumIntervallum_SearchCalendarControl ID="kapuRendszerAdatokIdopecsetDateInterval" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokValaszTitkositasField">
                                    <asp:Label ID="kapuRendszerAdatokValaszTitkositasLabel" runat="server" Text="V�lasz titkos�t�s:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokValaszTitkositasDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow5" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokRendszerUzenetField">
                                    <asp:Label ID="kapuRendszerAdatokRendszerUzenetLabel" runat="server" Text="Rendszer �zenet:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokRendszerUzenetDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokValaszUtvonalField">
                                    <asp:Label ID="kapuRendszerAdatokValaszUtvonalLabel" runat="server" Text="V�lasz �tvonal:"></asp:Label>
                                    <asp:DropDownList ID="kapuRendszerAdatokValaszUtvonalDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="K�zvetlen e-mail" Value="1" />
                                        <asp:ListItem Text="�rtes�t�si hely" Value="0" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr id="kapuRendszerRow6" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokTarteruletField">
                                    <asp:Label ID="kapuRendszerAdatokTartTeruletLabel" runat="server" Text="T�rter�let:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="kapuRendszerAdatokTarteruletDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>   
                                </td>
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokFiokField">
                                    <asp:Label ID="kapuRendszerAdatokFiokLabel" runat="server" Text="Fi�k:"></asp:Label>
                                    <asp:TextBox ID="kapuRendszerAdatokFiokTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                               <tr id="kapuRendszerRow7" class="urlapSor" runat="server" visible="false">
                                <td class="mrUrlapCaption" id="kapuRendszerAdatokCelRendszerField" >
                                    <asp:Label ID="kapuRendszerAdatokCelRendszerLabel" runat="server" Text="C�lrendszer:"></asp:Label>
                                    <uc:KodtarakDropDownList ID="kapuRendszerAdatokCelRendszerDropDown" runat="server" CssClass="mrUrlapInput"></uc:KodtarakDropDownList>   
                                </td>                               
                            </tr>
                            <tr>
                                <!--EGY�B ADATOK -->
                                <td colspan="2" align="left"><span class="ListHeaderText">Egy�b adatok</span></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokHivatkozottIktatoszamField">
                                    <asp:Label ID="egyebAdatokHivatkozottIktatoszamLabel" runat="server" Text="Hivatkozott iktat�sz�m:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokHivatkozottIktatoszamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="egyebAdatokEErkeztetoAzonositoField">
                                    <asp:Label ID="egyebAdatokEErkeztetoAzonositoLabel" runat="server" Text="E. �rkeztet� azon.:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokEErkeztetoAzonositoTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokErkeztetoSzamField">
                                    <asp:Label ID="egyebAdatokEErkeztetoSzamLabel" runat="server" Text="�rkeztet�sz�m:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokEErkeztetoSzamTextbox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" id="egyebAdatokEHivatkozasiAzonositoField">
                                    <asp:Label ID="egyebAdatokEHivatkozasiAzonositoLabel" runat="server" Text="E. hivatkoz�si azon.:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokEHivatkozasiAzonositoTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" id="egyebAdatokIktatoSzamField">
                                    <asp:Label ID="egyebAdatokIktatoSzamLabel" runat="server" Text="Iktat�sz�m:"></asp:Label>
                                    <asp:TextBox ID="egyebAdatokIktatoSzamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                                <td class="mrUrlapCaption" runat="server" id="egyebAdatokETertivevenyField">
                                    <asp:Label ID="egyebAdatokETertivevenyLabel" runat="server" Text="ET�rtivev�ny:"></asp:Label>
                                    <asp:DropDownList ID="egyebAdatokETertivevenyDropDown" runat="server" CssClass="mrUrlapInputComboBox">
                                        <asp:ListItem Text="Igen" Value="1" />
                                        <asp:ListItem Text="Nem" Value="NULL" />
                                        <asp:ListItem Text="�sszes" Value="" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" colspan="2" id="egyebAdatokStatuszField" runat="server" align="center" style="text-align: center;"></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" colspan="2" id="Td4" runat="server" align="center" style="text-align: center"></td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" colspan="2" id="Td3" runat="server" align="center">
                                    <table>
                                        <tr>
                                            <td class="mrUrlapCaption">
                                                <asp:Label runat="server" Text="St�tusz:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList Style="width: 50%" ID="egyebAdatokStatuszRadioButtonList" runat="server" RepeatDirection="Horizontal" CssClass="" BorderStyle="None" />

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" runat="server" colspan="2" id="Td1">
                                    <center>
                                        <uc:Ervenyesseg_SearchFormComponent style="width: 70%" ID="egyebAdatokErvenyessegSearchFormComponent" runat="server"></uc:Ervenyesseg_SearchFormComponent>
                                    </center>
                                </td>
                            </tr>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" colspan="2" runat="server" id="Td2" align="center" style="text-align: center;">
                                    <center>
                                        <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent" runat="server" />
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </center>
                </eUI:eFormPanel>
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
