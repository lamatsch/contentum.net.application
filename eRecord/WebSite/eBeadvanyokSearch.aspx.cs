﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI.WebControls;

public partial class eBeadvanyokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_eBeadvanyokSearch);
    protected class Constants
    {
        public class Status
        {
            public const string OSSZES = "O";
            public const string ERKEZTETES_NELKULI = "N";
            public const string ERKEZTETETT = "E";
            public const string IKTATOTT = "I";
        }
        public const string WHERE_ERKEZTETETT = "";
    }

    /// <summary>
    /// this search form is used from two source. 1:Erkeztetes/Elektronikus �zenetek and 2:Postazas/Elkuldott Hivatali Kapus Uzenetek 
    /// for kimeno ebeadvany mode we should hide some fields
    /// </summary>
    protected bool IsKimenoEBeadvanyMode
    {
        get
        {
            return "Kimeno".Equals(Request.QueryString.Get(QueryStringVars.Mode));
        }
    }

    const string customSessionName = "KimenoeBeadvanyokSearch";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        if (IsKimenoEBeadvanyMode)
        {
            SearchHeader1.CustomTemplateTipusNev = customSessionName;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = "Elektronikus üzenetek keresése";

        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {

            LoadComponentsFromSearchObject(GetSearchObjectInstanceFromSession());
        }

        /* if (IsKimenoEBeadvanyMode)
         {
             partnerAdatokPartnerNevLabel.Text = "Címzett:";
             kapuRendszerAdatokHivatkozasiSzamField.Visible = false;
             kapuRendszerAdatokErvenyessegField.Visible = false;
             egyebAdatokErvenyessegSearchFormComponent.Visible = false;
         }

         if (!IsKimenoEBeadvanyMode)
         {
             egyebAdatokETertivevenyField.Visible = false;
             partnerAdatokPartnerEmailField.Visible = false;
             partnerAdatokPartnerRovidNevField.Visible = false;
         }*/

        var execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (IsELHISZUzenetForras(execParam)) // in this case we should hide all ELHISZ related fields "Kapu rendszer"
        {
            HideKapuRendszerRows();
        }
    }

    private EREC_eBeadvanyokSearch GetDefaultSearchObject()
    {
        EREC_eBeadvanyokSearch searchObject = searchObject = Search.GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page, false);

        return searchObject;
    }


    private EREC_eBeadvanyokSearch GetSearchObjectInstanceFromSession()
    {
        EREC_eBeadvanyokSearch searchObject = null;
        if (IsKimenoEBeadvanyMode)
        {
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
            {
                searchObject = (EREC_eBeadvanyokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_eBeadvanyokSearch()
                    , SearchHeader1.CustomTemplateTipusNev);
            }
            else
            {
                searchObject = Search.GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page, false);
            }
        }
        else
        {
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_eBeadvanyokSearch)Search.GetSearchObject(Page, new EREC_eBeadvanyokSearch());
            }
            else
            {
                searchObject = Search.GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page, false);
            }
        }

        return searchObject;
    }

    private void LoadComponentsFromSearchObject(object uncastedSearchObject)
    {
        EREC_eBeadvanyokSearch searchObject = null;
        if (uncastedSearchObject != null) searchObject = (EREC_eBeadvanyokSearch)uncastedSearchObject;

        if (searchObject == null)
        {
            return;
        }

        //   uzenetAdatokUzenetIranyaDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.POSTAZAS_IRANYA, true, SearchHeader1.ErrorPanel);
        //   uzenetAdatokUzenetIranyaDropDown.SelectedValue = searchObject.Irany.Value;
        uzenetAdatokKuldoRendszerTextBox.Text = searchObject.KuldoRendszer.Value;
        uzenetAdatokUzenetAllapotaDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.ALLAPOT, true, SearchHeader1.ErrorPanel);
        uzenetAdatokUzenetAllapotaDropDown.SelectedValue = searchObject.Allapot.Value;
        uzenetAdatokUzenetTipusaTextBox.Text = searchObject.UzenetTipusa.Value;
        uzenetAdatokDokumentumAzonositoTextBox.Text = searchObject.KR_DokTipusAzonosito.Value;
        uzenetAdatokDokumentumLeirasTextBox.Text = searchObject.KR_DokTipusLeiras.Value;
        uzenetAdataiDokumentumHivatalTextBox.Text = searchObject.KR_DokTipusHivatal.Value;
        uzenetAdatokEredetiCsatolmanyTextBox.Text = searchObject.KR_FileNev.Value;
        uzenetAdataiMegjegyzesTextBox.Text = searchObject.KR_Megjegyzes.Value;

        uzenetAdataiLetoltesIdejeDateInterval.SetComponentFromSearchObjectFields(searchObject.LetrehozasIdo);

        partnerAdatokPartnerTipusDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.FELADO_TIPUSA, true, SearchHeader1.ErrorPanel);
        partnerAdatokPartnerTipusDropDown.SelectedValue = searchObject.FeladoTipusa.Value;
        partnerAdatokPartnerEmailTextBox.Text = searchObject.PartnerEmail.Value;
        partnerAdatokPartnerNevTextBox.Text = searchObject.PartnerNev.Value;
        partnerAdatokPartnerCimTextBox.Text = Contentum.eRecord.BaseUtility.PartnerHelper.GetCorrectPartnerCim(searchObject).Value;
        partnerAdatokPartnerMAKKodTextBox.Text = searchObject.PartnerMAKKod.Value;

        kapuRendszerAdatokRendszerUzenetDropDown.SelectedValue = searchObject.KR_Rendszeruzenet.Value;

        kapuRendszerAdatokHivatkozasiSzamTextBox.Text = searchObject.KR_HivatkozasiSzam.Value;
        kapuRendszerAdatokErkeztetesiSzamTextBox.Text = searchObject.KR_ErkeztetesiSzam.Value;
        kapuRendszerAdatokErvenyessegDateInterval.SetComponentFromSearchObjectFields(searchObject.KR_ErvenyessegiDatum);
        kapuRendszerAdatokErkeztetesIdejeDateInterval.SetComponentFromSearchObjectFields(searchObject.KR_ErkeztetesiDatum);
        kapuRendszerAdatokIdopecsetDateInterval.SetComponentFromSearchObjectFields(searchObject.KR_Idopecset);
        kapuRendszerAdatokValaszTitkositasDropDown.SelectedValue = searchObject.KR_Valasztitkositas.Value;

        kapuRendszerAdatokValaszUtvonalDropDown.SelectedValue = searchObject.KR_Valaszutvonal.Value;
        kapuRendszerAdatokTarteruletDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.TARTERULET, true, SearchHeader1.ErrorPanel);
        kapuRendszerAdatokTarteruletDropDown.SelectedValue = searchObject.KR_Tarterulet.Value;

        kapuRendszerAdatokCelRendszerDropDown.SetEmptyItemCaption(Resources.List.All);
        uzenetAdatokUzenetAllapotaDropDown.SetEmptyItemCaption(Resources.List.All);
        partnerAdatokPartnerTipusDropDown.SetEmptyItemCaption(Resources.List.All);
        kapuRendszerAdatokTarteruletDropDown.SetEmptyItemCaption(Resources.List.All);

        if (FunctionRights.GetFunkcioJog(Page, "eBeadvanyokOsszesCelRendszer"))
        {
            kapuRendszerRow7.Visible = true;
            kapuRendszerAdatokCelRendszerDropDown.FillDropDownList(KodTarak.EBEADVANY_KODCSOPORTOK.eBeadvany_CelRendszer, true, SearchHeader1.ErrorPanel);

            kapuRendszerAdatokCelRendszerDropDown.SelectedValue = searchObject.Cel.Value;
        }
        else
        {
            kapuRendszerRow7.Visible = false;
            kapuRendszerAdatokCelRendszerDropDown.SelectedValue = "WEB";
        }

        kapuRendszerAdatokFiokTextBox.Text = searchObject.KR_Fiok.Value;

        egyebAdatokHivatkozottIktatoszamTextBox.Text = searchObject.Contentum_HivatkozasiSzam.Value;

        egyebAdatokEErkeztetoAzonositoTextBox.Text = searchObject.PR_ErkeztetesiSzam.Value;
        egyebAdatokEHivatkozasiAzonositoTextBox.Text = searchObject.PR_HivatkozasiSzam.Value;
        egyebAdatokETertivevenyDropDown.SelectedValue = searchObject.KR_ETertiveveny.Value;
        egyebAdatokErvenyessegSearchFormComponent.SetDefault(searchObject.ErvKezd, searchObject.ErvVege);

        FillStatuszRadioList(searchObject);
    }

    private EREC_eBeadvanyokSearch SetSearchObjectFromComponents()
    {
        EREC_eBeadvanyokSearch searchObject = (EREC_eBeadvanyokSearch)SearchHeader1.TemplateObject;

        if (searchObject == null) { searchObject = GetDefaultSearchObject(); }
        if (kapuRendszerAdatokRendszerUzenetDropDown.SelectedValue == "1")
        {
            SetupSearchField(searchObject.KR_Rendszeruzenet, kapuRendszerAdatokRendszerUzenetDropDown.SelectedValue);
        }
        else if (kapuRendszerAdatokRendszerUzenetDropDown.SelectedValue == "0")
        {
            searchObject.KR_Rendszeruzenet.IsNullOrEquals("0");
        }
        else
        {
            searchObject.KR_Rendszeruzenet.Clear();
        }
        SetupSearchField(searchObject.KuldoRendszer, uzenetAdatokKuldoRendszerTextBox.Text);
        SetupSearchField(searchObject.Allapot, uzenetAdatokUzenetAllapotaDropDown.SelectedValue);
        SetupSearchField(searchObject.UzenetTipusa, uzenetAdatokUzenetTipusaTextBox.Text);
        SetupSearchField(searchObject.KR_DokTipusAzonosito, uzenetAdatokDokumentumAzonositoTextBox.Text);
        SetupSearchField(searchObject.KR_DokTipusLeiras, uzenetAdatokDokumentumLeirasTextBox.Text);
        SetupSearchField(searchObject.KR_DokTipusHivatal, uzenetAdataiDokumentumHivatalTextBox.Text);
        SetupSearchField(searchObject.KR_FileNev, uzenetAdatokEredetiCsatolmanyTextBox.Text);
        SetupSearchField(searchObject.KR_Megjegyzes, uzenetAdataiMegjegyzesTextBox.Text);

        SetupDateIntervalSearch(searchObject.LetrehozasIdo, uzenetAdataiLetoltesIdejeDateInterval.DatumKezd, uzenetAdataiLetoltesIdejeDateInterval.DatumVege);

        SetupSearchField(searchObject.FeladoTipusa, partnerAdatokPartnerTipusDropDown.SelectedValue);
        SetupSearchField(searchObject.PartnerEmail, partnerAdatokPartnerEmailTextBox.Text);
        SetupSearchField(searchObject.PartnerNev, partnerAdatokPartnerNevTextBox.Text);
        SetupSearchField(searchObject.PartnerRovidNev, partnerAdatokPartnerRovidNevTextBox.Text);

        SetupSearchField(Contentum.eRecord.BaseUtility.PartnerHelper.GetCorrectPartnerCim(searchObject), partnerAdatokPartnerCimTextBox.Text);
        SetupSearchField(searchObject.KR_HivatkozasiSzam, kapuRendszerAdatokHivatkozasiSzamTextBox.Text);
        SetupSearchField(searchObject.KR_ErkeztetesiSzam, kapuRendszerAdatokErkeztetesiSzamTextBox.Text);

        //BUG_13021
        SetupSearchField(searchObject.PR_ErkeztetesiSzam, egyebAdatokEErkeztetoAzonositoTextBox.Text);
        SetupSearchField(searchObject.PR_HivatkozasiSzam, egyebAdatokEHivatkozasiAzonositoTextBox.Text);
        
        SetupDateIntervalSearch(searchObject.KR_ErvenyessegiDatum, kapuRendszerAdatokErvenyessegDateInterval.DatumKezd, kapuRendszerAdatokErvenyessegDateInterval.DatumVege);
        SetupDateIntervalSearch(searchObject.KR_ErkeztetesiDatum, kapuRendszerAdatokErkeztetesIdejeDateInterval.DatumKezd, kapuRendszerAdatokErkeztetesIdejeDateInterval.DatumVege);
        SetupDateIntervalSearch(searchObject.KR_Idopecset, kapuRendszerAdatokIdopecsetDateInterval.DatumKezd, kapuRendszerAdatokIdopecsetDateInterval.DatumVege);

        SetupSearchField(searchObject.KR_Valasztitkositas, kapuRendszerAdatokValaszTitkositasDropDown.SelectedValue);

        SetupSearchField(searchObject.KR_Valaszutvonal, kapuRendszerAdatokValaszUtvonalDropDown.SelectedValue);
        SetupSearchField(searchObject.KR_Tarterulet, kapuRendszerAdatokTarteruletDropDown.SelectedValue);
        SetupSearchField(searchObject.Cel, kapuRendszerAdatokCelRendszerDropDown.SelectedValue);

        SetupSearchField(searchObject.KR_Fiok, kapuRendszerAdatokFiokTextBox.Text);
        SetupSearchField(searchObject.PartnerRovidNev, partnerAdatokPartnerRovidNevTextBox.Text);
        if (egyebAdatokETertivevenyDropDown.SelectedValue == "1")//We should only search if value = 0
        {
            SetupSearchField(searchObject.KR_ETertiveveny, "1");
        }

        egyebAdatokErvenyessegSearchFormComponent.SetSearchObjectFields(searchObject.ErvKezd, searchObject.ErvVege);

        var statusz = egyebAdatokStatuszRadioButtonList.SelectedValue;
        if (statusz == Constants.Status.OSSZES)
        {
            searchObject.KuldKuldemeny_Id.Clear();
            searchObject.IraIrat_Id.Clear();
            searchObject.WhereByManual = "";
        }
        else if (statusz == Constants.Status.ERKEZTETETT)
        {
            searchObject.KuldKuldemeny_Id.NotNull();
            searchObject.IraIrat_Id.IsNull();
            searchObject.WhereByManual = "*IratIktatoszamEsId_ERKEZTETETT"; // (IratIktatoszamEsId IS NULL OR IratIktatoszamEsId = '')
        }
        else if (statusz == Constants.Status.ERKEZTETES_NELKULI)
        {
            searchObject.KuldKuldemeny_Id.IsNull();
            searchObject.IraIrat_Id.Clear();
            searchObject.WhereByManual = "";
        }
        else if (statusz == Constants.Status.IKTATOTT)
        {
            //LZS - BUG_12673
            //Mivel az IraIrat_Id nem töltődik ki iktatáskor, ezért erre most nem szűdünk még.
            //searchObject.IraIrat_Id.NotNull();
            searchObject.KuldKuldemeny_Id.NotNull();
            searchObject.WhereByManual = "*IratIktatoszamEsId_IKTATOTT"; // (IratIktatoszamEsId IS NOT NULL AND IratIktatoszamEsId <> '')
        }
        else
        {
            searchObject.IraIrat_Id.IsNull();
            // BUG_7466: [sp_EREC_eBeadvanyokGetAllWithExtension] alapján
            searchObject.WhereByManual = " AND (coalesce((select top 1 ir.Id from EREC_IraIratok ir where ir.Allapot not in ('06', '08') and ir.id=EREC_eBeadvanyok.IraIrat_Id), (select top 1 ik.Id from EREC_IraIratok ik WHERE ik.KuldKuldemenyek_Id=EREC_eBeadvanyok.KuldKuldemeny_Id " +
              "AND ik.Allapot NOT IN ('06','08') AND EREC_eBeadvanyok.KuldKuldemeny_Id IS NOT NULL )) is null) ";
        }

        searchObject.UzenetTipusa.LikeWhenNeededIfNotEmpty(uzenetAdatokUzenetTipusaTextBox.Text);

        return searchObject;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(Search.GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page, false));
        }
        else if (e.CommandName == CommandName.Search)
        {
            var search = SetSearchObjectFromComponents();
            SaveSearchObject(search);

            //TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private void RemoveSearchObjectFromSession()
    {
        // default searchobject
        if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
        {
            Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev);
        }
        else
        {
            Search.RemoveSearchObjectFromSession(Page, _type);
        }
    }

    private void SaveSearchObject(EREC_eBeadvanyokSearch searchObject)
    {
        //Keresesi feltetelek elmentese kiirathato formaban
        searchObject.ReadableWhere = Search.GetReadableWhere(Form);

        if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
        {
            Search.SetSearchObject_CustomSessionName(Page, searchObject, SearchHeader1.CustomTemplateTipusNev);
        }
        else
        {
            Search.SetSearchObject(Page, searchObject);
        }
    }

    private void SetupSearchField(Field field, string value)
    {
        if (string.IsNullOrEmpty(value)) { return; }
        field.Value = value;
        field.Operator = Search.GetOperatorByLikeCharater(value);
    }

    private void SetupDateIntervalSearch(Field field, string dateFrom, string dateTo)
    {
        if (string.IsNullOrEmpty(dateFrom)) { return; }

        if (String.IsNullOrEmpty(dateTo))
        {
            field.GreaterOrEqual(dateFrom);
        }
        else
        {
            field.Between(dateFrom, dateTo);
        }
    }
    private void HideKapuRendszerRows()
    {
        kapuRendszerRow1.Visible = false;
        kapuRendszerRow2.Visible = false;
        kapuRendszerRow3.Visible = false;
        kapuRendszerRow4.Visible = false;
        kapuRendszerRow5.Visible = false;
        kapuRendszerRow6.Visible = false;
        kapuRendszerRow7.Visible = false;
    }

    private void FillStatuszRadioList(EREC_eBeadvanyokSearch search)
    {
        var isErkeztetett = search.WhereByManual.Contains("IratIktatoszamEsId_ERKEZTETETT"); //search.KuldKuldemeny_Id.Operator == Query.Operators.notnull;
        var isIktatott = search.WhereByManual.Contains("IratIktatoszamEsId_IKTATOTT"); //search.IraIrat_Id.Operator == Query.Operators.notnull;

        egyebAdatokStatuszRadioButtonList.Items.Clear();
        egyebAdatokStatuszRadioButtonList.Items.Add(new ListItem("Összes", Constants.Status.OSSZES));
        egyebAdatokStatuszRadioButtonList.Items.Add(new ListItem("Érk. nélküli", Constants.Status.ERKEZTETES_NELKULI));
        if (!IsKimenoEBeadvanyMode)
        {
            egyebAdatokStatuszRadioButtonList.Items.Add(new ListItem("Érkeztetett", Constants.Status.ERKEZTETETT));
            egyebAdatokStatuszRadioButtonList.Items.Add(new ListItem("Iktatott", Constants.Status.IKTATOTT));
        }

        var selected = Constants.Status.OSSZES;
        if (String.IsNullOrEmpty(search.KuldKuldemeny_Id.Operator) &&
            String.IsNullOrEmpty(search.IraIrat_Id.Operator) &&
            String.IsNullOrEmpty(search.WhereByManual))
        {
            // STATUSZ_OSSZES
        }
        else
        {
            selected = (isIktatott || isErkeztetett) && IsKimenoEBeadvanyMode ? Constants.Status.OSSZES // 
                : (isIktatott ? Constants.Status.IKTATOTT : (isErkeztetett ? Constants.Status.ERKEZTETETT : Constants.Status.ERKEZTETES_NELKULI));
        }
        egyebAdatokStatuszRadioButtonList.SelectedValue = selected;
    }

    public bool IsELHISZUzenetForras(ExecParam param)
    {
        string eUzenetForras = "eUzenetForras";
        string ELHISZ_VALUE = "ELHISZ";

        Contentum.eIntegrator.Service.INT_ParameterekService service = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        INT_ParameterekSearch search = new INT_ParameterekSearch();

        search.Nev.Filter(eUzenetForras);

        Result res = service.GetAll(param, search);
        string eUzenetForrasValue;
        if (res.IsError)
        {
            Logger.Error(String.Format("GetParameter hiba: {0}", eUzenetForras), param, res);
        }

        if (res.GetCount == 1)
        {
            eUzenetForrasValue = res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
        }
        if (eUzenetForras == ELHISZ_VALUE)
        { return true; }

        return false;
    }
}
