<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" Async="true"
    CodeFile="ePdfTertivevenyKezelesForm.aspx.cs" Inherits="ePdfTertivevenyKezelesForm"
    Title="ePdf t�rtivev�ny kezel�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" HeaderLabel="Fejl�c" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <eUI:eFormPanel ID="eFormPanelMessage" runat="server" Visible="false">
        <asp:Label ID="LabelMessage" ForeColor="Green" runat="server" />
    </eUI:eFormPanel>

    <%--    <asp:UpdatePanel ID="ePdfUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <eUI:eFormPanel ID="EFormPanelePdf" runat="server">
        <h3>K�rem v�lassza ki a Pdf t�rtivev�nyeket, majd nyomja meg a Rendben gombot.</h3>
        <br />
        <asp:FileUpload ID="FileUploadePdf" Multiple="Multiple" runat="server" HorizontalAlign="Left" />
        <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="FileUploadePdf"
            ErrorMessage="Csak Pdf f�jl haszn�lhat� !" Display="Dynamic"
            ValidationExpression="(.+\.pdf|.+\.PDF|.+\.Pdf)" />
        <br />
        <br />
        <br />
        <asp:ImageButton ID="ImageButtonExecute" runat="server" ToolTip="V�grehajt�s"
            ImageUrl="~/images/hu/ovalgomb/rendben.jpg" OnClick="ButtonUpload_Click"
            onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
            CommandName="Execute"
            OnClientClick="showProgress();" />
        <br />
        <img id="loadingImage" src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="" style="visibility: hidden; width: 100px; height: 100px" />
        <br />
        <br />
    </eUI:eFormPanel>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>

    <%--JAVASCRIPT--%>
    <script type="text/javascript">
        function showProgress() {
            document.getElementById('loadingImage').style.visibility = 'visible';
        }
        function hideProgress() {
            document.getElementById('loadingImage').style.visibility = 'hidden';
        }
        function enableSubmitButton(val) {
            if (val == 'true')
                document.getElementById('ContentPlaceHolder1_ImageButtonExecute').disabled = 'false';
            else
                document.getElementById('ContentPlaceHolder1_ImageButtonExecute').disabled = 'true';
        }
    </script>
</asp:Content>
