﻿// <copyright company="Axis Rendszerhaz Kft.">
// Copyright (c) 2017 All Rights Reserved
// <author>Axis Rendszerhaz Kft.</author>
// <date>2016</date>
// </copyright>

using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using eRecordUtility = Contentum.eRecord.Utility;
using Contentum.eScanWebService;
using Contentum.eUtility;
using Contentum.Net.Pdf.Extractor.Common;
using Contentum.Net.Pdf.Extractor.PdfLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

public partial class ePdfTertivevenyKezelesForm : Contentum.eUtility.UI.PageBase
{
    #region PROPERTIES
    private const string ConstNotFoundRagSzamTitle = "Nem talált ragszámok: <br/>";
    private const string ConstFoundRagSzamTitle = "Megtalált és rögzített ragszámok: <br/>";

    private const string ConstErrorMsgNoPdf = "Nincs feldolgozandó Pdf fájl!";
    private const string ConstErrorMsgNoUploadedPdf = "Nincs feltöltött PDF fájl!";

    private const string ConstTertiXmlDataHeader0 = "k adatok";
    private const string ConstTertiXmlDataHeader1 = "Jegyzék adatok";
    private const string ConstTertiXmlDataHeader2 = "JegyzÃ©k adatok";
    private const string ConstTertiXmlDataHeader3 = "JegyzA©k adatok";
    private const string ConstTertiXmlDataHeader4 = "Jegyz??k adatok";


    private const string ConstTertiXmlDataFileExtensionXLS = "XLS";
    private const string ConstTertiXmlDataFileExtensionXML = "XML";
    private const string ConstTertiImageFileExtension = "TIF";
    private const string ConstTertiPdfFileExtension = "PDF";
    #endregion

    private string ErrorMessage { get; set; }
    #region PAGE EVENTS
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ePdfTertivevenyKezeles");
    }

    /// <summary>
    /// Az oldal Init eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Oldal Load eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "close panel", "hideProgress()", true);

        InitHeader();
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    private void InitHeader()
    {
        #region SET HEADER

        ListHeader1.HeaderLabel = Resources.Form.ePdfTertivevenyKezelesFormHeaderTitle;
        ListHeader1.SearchObjectType = typeof(object);
        ListHeader1.CustomSearchObjectSessionName = string.Empty;
        ListHeader1.SearchVisible = false;

        ListHeader1.RefreshVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.PagerVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SearchVisible = false;
        ListHeader1.HistoryVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
        #endregion
    }
    #endregion
    protected void ePdfTertivevenyKezelesFormUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:

                    break;
            }
        }
    }
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        ErrorMessage = string.Empty;
        ResultError.DisplayNoIdParamError(EErrorPanel1);

        eFormPanelMessage.Visible = false;
        LabelMessage.Text = string.Empty;

        ProgressIndicator(true);
        EnableSubmitButton(false);

        try
        {
            MainProcedure();
        }
        catch (Exception) { throw; }
        finally
        {
            ProgressIndicator(false);
            EnableSubmitButton(true);
        }
    }

    #region JAVASCRIPT
    private void ProgressIndicator(bool show)
    {
        if (show)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "close panel", "showProgress()", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "close panel", "hideProgress()", true);
    }
    private void EnableSubmitButton(bool show)
    {
        if (show)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "close panel", "enableSubmitButton(true)", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "close panel", "enableSubmitButton(false)", true);
    }
    #endregion
    /// <summary>
    /// Main procedure
    /// </summary>
    private void MainProcedure()
    {
        HttpFileCollection uploadedFiles = Request.Files;
        if (uploadedFiles == null)
        {
            Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();
            result.ErrorMessage = ConstErrorMsgNoPdf;
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            return;
        }

        List<HttpPostedFile> files = ExtractPostedFiles(uploadedFiles);
        if (files == null || files.Count < 1)
        {
            Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();
            result.ErrorMessage = ConstErrorMsgNoUploadedPdf;
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            return;
        }
        //        #region DEBUG
        //#if DEBUG
        //        foreach (HttpPostedFile item in files)
        //        {
        //            LabelMessage.Text += "PDF File Name: " + item.FileName + "<br>";
        //            LabelMessage.Text += "PDF File Size: " + item.ContentLength + "<br><br>";
        //        }
        //#endif
        //        #endregion

        Dictionary<string, List<PdfFileAttachment>> pdfAttachments = ExtractPdf(files);

        List<EREC_KuldTertivevenyek> resultTertivevenyList = FindTertivevenyProcedure(pdfAttachments);
        UpdateTertivevenyFieldsFromXmlDataProcedure(pdfAttachments, resultTertivevenyList);
        UploadTertivevenyImageProcedure(pdfAttachments, resultTertivevenyList);
    }
    /// <summary>
    /// Extract attached files from collection, and filter by type.
    /// </summary>
    /// <param name="collection"></param>
    /// <returns></returns>
    private List<HttpPostedFile> ExtractPostedFiles(HttpFileCollection collection)
    {
        if (collection.Count < 1)
            return null;

        List<HttpPostedFile> ret = new List<HttpPostedFile>();
        for (int i = 0; i < collection.Count; i++)
        {
            HttpPostedFile userPostedFile = collection[i];
            if (userPostedFile.FileName.ToUpper().EndsWith(ConstTertiPdfFileExtension))
            {
                ret.Add(userPostedFile);
            }
        }
        return ret;
    }
    /// <summary>
    /// Extract all attachments from all pdf
    /// </summary>
    /// <param name="files"></param>
    /// <returns></returns>
    private Dictionary<string, List<PdfFileAttachment>> ExtractPdf(List<HttpPostedFile> files)
    {
        Dictionary<string, List<PdfFileAttachment>> result = new Dictionary<string, List<PdfFileAttachment>>();

        PDFExtractor<byte[]> pdfe = new PDFExtractor<byte[]>();
        foreach (HttpPostedFile item in files)
        {
            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(item.InputStream))
            {
                fileData = binaryReader.ReadBytes(item.ContentLength);
            }
            var res = pdfe.ExtractAttachments(fileData);
            result.Add(item.FileName, res);
        }
        return result;
    }

    private List<EREC_KuldTertivevenyek> FindTertivevenyProcedure(Dictionary<string, List<PdfFileAttachment>> pdfAttachments)
    {
        if (pdfAttachments == null || pdfAttachments.Count < 1)
            return null;

        List<EREC_KuldTertivevenyek> resultTertivevenyList = new List<EREC_KuldTertivevenyek>();
        EREC_KuldTertivevenyek found;
        List<string> hianyzoRagszamok = new List<string>();
        List<string> megtalaltRagszamok = new List<string>();

        foreach (KeyValuePair<string, List<PdfFileAttachment>> keyAttachment in pdfAttachments)
        {
            if (keyAttachment.Value == null || keyAttachment.Value.Count < 1)
                return null;
            foreach (PdfFileAttachment item in keyAttachment.Value)
            {
                if (!item.Name.ToUpper().EndsWith(ConstTertiImageFileExtension))
                    continue;

                found = ServiceFindTertiveveny(item.ShortName);
                if (found == null)
                {
                    hianyzoRagszamok.Add(item.ShortName);
                }
                else
                {
                    resultTertivevenyList.Add(found);
                    megtalaltRagszamok.Add(item.ShortName);
                }
            }
        }
        if (hianyzoRagszamok.Count > 0)
        {
            ErrorMessage += ConstNotFoundRagSzamTitle + ConcatList(hianyzoRagszamok);
            ShowErrorMessage(ErrorMessage);
        }
        if (megtalaltRagszamok.Count > 0)
        {
            eFormPanelMessage.Visible = Visible;
            LabelMessage.Text += ConstFoundRagSzamTitle + ConcatList(megtalaltRagszamok);
            //megtalaltRagszamok.Aggregate((current, next) => current + " , " + next)
        }
        return resultTertivevenyList;
    }
    private void UpdateTertivevenyFieldsFromXmlDataProcedure(Dictionary<string, List<PdfFileAttachment>> pdfAttachments, List<EREC_KuldTertivevenyek> resultTertivevenyList)
    {
        if (pdfAttachments == null || pdfAttachments.Count < 1)
            return;

        List<ePdfXmlDataClass> xmldataCollection = null;
        foreach (KeyValuePair<string, List<PdfFileAttachment>> keyAttachment in pdfAttachments)
        {
            if (keyAttachment.Value == null || keyAttachment.Value.Count < 1)
                return;

            #region FIND TERTI DATA FROM XLS
            foreach (PdfFileAttachment item in keyAttachment.Value)
            {
                if (item.Name.ToUpper().EndsWith(ConstTertiXmlDataFileExtensionXLS))
                {
                    xmldataCollection = ExtractePdfXmlDataFromXls(item);
                    //if (xmldataCollection != null)
                    //    break;

                    //Vannak olyan pdf-ek, amelyekben több excel file-ban vannak az adatok.
                    //Ilyen esetben az eredeti megoldás nem működött. A FindAndUpdateItem() függvénybe lett
                    //áthelyezve az item update, és ezt minden excelre meghívjuk.
                    #region FIND DB ITEM AND UPDATE
                    FindAndUpdateItem(resultTertivevenyList, xmldataCollection);
                    #endregion
                }
                else if (item.Name.ToUpper().EndsWith(ConstTertiXmlDataFileExtensionXML))
                {
                    xmldataCollection = ExtractePdfXmlDataFromXml(item);
                    //if (xmldataCollection != null)
                    //    break;
                    FindAndUpdateItem(resultTertivevenyList, xmldataCollection);
                }
            }
            #endregion

           
        }
    }

    private void FindAndUpdateItem(List<EREC_KuldTertivevenyek> resultTertivevenyList, List<ePdfXmlDataClass> xmldataCollection)
    {
        if (xmldataCollection != null)
        {
            EREC_KuldTertivevenyek element;
            foreach (ePdfXmlDataClass xmldata in xmldataCollection)
            {
                element = resultTertivevenyList.Find(a => a.Ragszam == xmldata.Ragszam);

                //LZS - BUG_8536
                #region BUG_8536
                if (element != null)
                {
                    //LZS - Az átvétel dátuma mezőt csak akkor kell kitölteni, ha a "KezbDatuma" mező nem üres.
                    if (!string.IsNullOrEmpty(xmldata.KezbDatuma))
                    {
                        element.AtvetelDat = xmldata.KezbDatuma;
                        element.Updated.AtvetelDat = true;
                    }
                    else
                    {
                        element.Updated.AtvetelDat = false;
                    }

                    //LZS - Amennyiben a VisszakezbDatuma nem üres , és a VisszakezbOka = "Nem kereste",
                    //akkor a KezbVelelemBeallta true, és a KezbVelelemDatuma = VisszakezbDatuma.
                    if (!string.IsNullOrEmpty(xmldata.VisszakezbDatuma) && xmldata.VisszakezbOka == "Nem kereste")
                    {
                        element.KezbVelelemBeallta = "1";
                        element.KezbVelelemDatuma = xmldata.VisszakezbDatuma;

                        element.Updated.KezbVelelemBeallta = true;
                        element.Updated.KezbVelelemDatuma = true;
                    }
                    else
                    {
                        element.Updated.KezbVelelemBeallta = false;
                        element.Updated.KezbVelelemDatuma = false;
                    }

                    //LZS - BUG_8536
                    //Az element.TertiVisszaKod értékét a függő kódtár alapján kell meghatározni,
                    //úgy hogy a kiolvasott  "Kezbesitve" és "VisszakezbOka" mezők összefűzött értéke alapján (mindig csak az egyik kitöltött).

                    string TertiVisszaPostaKod = GetTertiVisszaPostaKod(xmldata.Kezbesitve + xmldata.VisszakezbOka);
                    ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

                    // Kódtárfüggőség lekérése:
                    KodtarFuggosegDataClass ktFuggosegTertiVissza = GetKodtarFuggoseg("TERTI_VISSZA_KOD_POSTA", "TERTIVEVENY_VISSZA_KOD", execparam);
                    KodTar_Cache.KodTarElem ktTERTIVEVENY_VISSZA_KOD = GetKodtarElem(TertiVisszaPostaKod, ktFuggosegTertiVissza, execparam);

                    element.TertivisszaKod = ktTERTIVEVENY_VISSZA_KOD.Kod;


                    #endregion

                    element.TertivisszaDat = xmldata.AdatatadasNapja;
                    element.Allapot = Contentum.eUtility.KodTarak.TERTIVEVENY_ALLAPOT.Regisztralt;

                    element.Updated.TertivisszaDat = true;
                    element.Updated.TertivisszaKod = true;
                    element.Updated.Allapot = true;

                    element.Updated.ErvKezd = false;
                    element.Updated.ErvVege = false;
                    element.Updated.AtvevoSzemely = false;
                    element.Updated.BarCode = false;
                    element.Updated.Id = false;
                    element.Updated.Kuldemeny_Id = false;
                    element.Updated.Ragszam = false;
                    ServiceUpdateTertiveveny(element);
                }
            }
        }
    }

    //LZS - BUG_8536
    /// <summary>
    /// Kódtárfüggőség lekérése a megadott vezérlő - függő kódcsoportokra.
    /// Ha nincs találat, Exceptiont dobunk.
    /// </summary>
    /// <returns></returns>
    public static KodtarFuggosegDataClass GetKodtarFuggoseg(string kcsKodVezerlo, string kcsKodFuggo, ExecParam execParam)
    {
        Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", kcsKodVezerlo);
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", kcsKodFuggo);
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

        Contentum.eBusinessDocuments.Result res = service.GetAll(execParam, search);
        res.CheckError();

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            throw new ResultException(String.Format("Kódtárfüggőség lekérése sikertelen, nincs találat. ({0} - {1} kódcsoportok)", kcsKodVezerlo, kcsKodFuggo));
        }
        else
        {
            string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
            KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

            return fuggosegek;
        }
    }

    public static KodTar_Cache.KodTarElem GetKodtarElem(string kodtarElem, KodtarFuggosegDataClass ktKodtarFuggoseg, ExecParam execParam)
    {
        var kodtarlistTERTI_VISSZA_KOD_POSTA = KodTar_Cache.GetKodtarakByKodCsoportList("TERTI_VISSZA_KOD_POSTA", execParam, HttpContext.Current.Cache, null);
        var kodtarlistTERTIVEVENY_VISSZA_KOD = KodTar_Cache.GetKodtarakByKodCsoportList("TERTIVEVENY_VISSZA_KOD", execParam, HttpContext.Current.Cache, null);

        var ktTERTI_VISSZA_KOD_POSTA= kodtarlistTERTI_VISSZA_KOD_POSTA.FirstOrDefault(e => e.Kod == kodtarElem);
        if (ktTERTI_VISSZA_KOD_POSTA == null) { return null; }

        // Kódtárfüggőségből kivesszük, hogy melyik kódtárelem(ek) tartozik hozzá:
        List<string> mappedKodtarIdList = null;
        if (ktKodtarFuggoseg.Items != null)
        {
            mappedKodtarIdList = ktKodtarFuggoseg.Items.Where(e => e.VezerloKodTarId == ktTERTI_VISSZA_KOD_POSTA.Id).Select(e => e.FuggoKodtarId).ToList();
        }

        if (mappedKodtarIdList == null || mappedKodtarIdList.Count == 0)
        {
            return null;
        }
        else
        {
            var ktTERTIVEVENY_VISSZA_KOD = kodtarlistTERTIVEVENY_VISSZA_KOD.FirstOrDefault(e => e.Id == mappedKodtarIdList[0]);
            return ktTERTIVEVENY_VISSZA_KOD;
        }
    }

    //LZS - BUG_8536
    //A PDF-ből kiolvasott Postakódot fordítja át a kódtárakban szereplő TERTI_VISSZA_KOD_POSTA kódtárértékre.
    private string GetTertiVisszaPostaKod(string text)
    {
        string result = "";

        switch (text.Trim())
        {
            case "Nem kereste":
                result = KodTarak.TERTI_VISSZA_KOD_POSTA.Nem_kereste;
                break;

            case "Meghatalmazottnak":
                result = KodTarak.TERTI_VISSZA_KOD_POSTA.Meghatalmazottnak;
                break;

            case "Helyettes átvevőnek":
                result = KodTarak.TERTI_VISSZA_KOD_POSTA.Helyettes_atvevonek;
                break;

            case "Közvetett kézbesítőnek":
                result = KodTarak.TERTI_VISSZA_KOD_POSTA.Kozvetett_kezbesitonek;
                break;

            case "Címzettnek":
                result = KodTarak.TERTI_VISSZA_KOD_POSTA.Cimzettnek;
                break;
        }

        return result;
    }

    


    /// <summary>
    /// Upload tertiveveny image by scan service
    /// </summary>
    /// <param name="pdfAttachments"></param>
    /// <param name="resultTertivevenyList"></param>
    private void UploadTertivevenyImageProcedure(Dictionary<string, List<PdfFileAttachment>> pdfAttachments, List<EREC_KuldTertivevenyek> resultTertivevenyList)
    {
        if (pdfAttachments == null || pdfAttachments.Count < 1)
            return;
        EREC_KuldTertivevenyek found;

        foreach (KeyValuePair<string, List<PdfFileAttachment>> keyAttachment in pdfAttachments)
        {
            if (keyAttachment.Value == null || keyAttachment.Value.Count < 1)
                return;
            foreach (PdfFileAttachment item in keyAttachment.Value)
            {
                if (!item.Name.ToUpper().EndsWith(ConstTertiImageFileExtension))
                    continue;

                found = resultTertivevenyList.Find(a => a.Ragszam == item.ShortName);
                if (found != null)
                {
                    ServiceLoadDocumentByScanService(item.Content, item.Name, item.ShortName);
                }
            }
        }
    }

    #region SERVICE
    /// <summary>
    /// Find Tertiveveny
    /// </summary>
    /// <param name="ragSzam"></param>
    /// <returns></returns>
    private EREC_KuldTertivevenyek ServiceFindTertiveveny(string ragSzam)
    {
        EREC_KuldTertivevenyek erec_KuldTertiveveny = new EREC_KuldTertivevenyek();
        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();

        search.Ragszam.Value = ragSzam;
        search.Ragszam.Operator = Query.Operators.equals;

        Contentum.eBusinessDocuments.Result result = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            if (ErrorUpdatePanel != null)
            {
                ErrorUpdatePanel.Update();
            }
            return null;
        }
        if (result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
            return null;

        erec_KuldTertiveveny.Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        erec_KuldTertiveveny.Kuldemeny_Id = result.Ds.Tables[0].Rows[0]["Kuldemeny_Id"].ToString();
        erec_KuldTertiveveny.Ragszam = result.Ds.Tables[0].Rows[0]["Ragszam"].ToString();
        erec_KuldTertiveveny.TertivisszaKod = result.Ds.Tables[0].Rows[0]["TertivisszaKod"].ToString();
        erec_KuldTertiveveny.TertivisszaDat = result.Ds.Tables[0].Rows[0]["TertivisszaDat"].ToString();
        erec_KuldTertiveveny.AtvevoSzemely = result.Ds.Tables[0].Rows[0]["AtvevoSzemely"].ToString();
        erec_KuldTertiveveny.AtvetelDat = result.Ds.Tables[0].Rows[0]["AtvetelDat"].ToString();
        erec_KuldTertiveveny.BarCode = result.Ds.Tables[0].Rows[0]["BarCode"].ToString();
        erec_KuldTertiveveny.Allapot = result.Ds.Tables[0].Rows[0]["Allapot"].ToString();
        erec_KuldTertiveveny.ErvKezd = result.Ds.Tables[0].Rows[0]["ErvKezd"].ToString();
        erec_KuldTertiveveny.ErvVege = result.Ds.Tables[0].Rows[0]["ErvVege"].ToString();
        erec_KuldTertiveveny.Base.Ver = result.Ds.Tables[0].Rows[0]["Ver"].ToString();
        erec_KuldTertiveveny.Base.LetrehozasIdo = result.Ds.Tables[0].Rows[0]["LetrehozasIdo"].ToString();
        erec_KuldTertiveveny.Base.Letrehozo_id = result.Ds.Tables[0].Rows[0]["Letrehozo_id"].ToString();
        erec_KuldTertiveveny.Base.ModositasIdo = result.Ds.Tables[0].Rows[0]["ModositasIdo"].ToString();
        erec_KuldTertiveveny.Base.Modosito_id = result.Ds.Tables[0].Rows[0]["Modosito_id"].ToString();
        erec_KuldTertiveveny.Base.Note = result.Ds.Tables[0].Rows[0]["Note"].ToString();
        erec_KuldTertiveveny.Base.Stat_id = result.Ds.Tables[0].Rows[0]["Stat_id"].ToString();
        erec_KuldTertiveveny.Base.Tranz_id = result.Ds.Tables[0].Rows[0]["Tranz_id"].ToString();
        erec_KuldTertiveveny.Base.UIAccessLog_id = result.Ds.Tables[0].Rows[0]["UIAccessLog_id"].ToString();
        erec_KuldTertiveveny.Base.ZarolasIdo = result.Ds.Tables[0].Rows[0]["ZarolasIdo"].ToString();
        erec_KuldTertiveveny.Base.Zarolo_id = result.Ds.Tables[0].Rows[0]["Zarolo_id"].ToString();

        return erec_KuldTertiveveny;
    }
    /// <summary>
    /// Update item in EREC_KuldTertivevenyekService
    /// </summary>
    /// <param name="item"></param>
    private void ServiceUpdateTertiveveny(EREC_KuldTertivevenyek item)
    {
        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = item.Id;
        Contentum.eBusinessDocuments.Result result = service.Update(execParam, item);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            if (ErrorUpdatePanel != null)
            {
                ErrorUpdatePanel.Update();
            }
        }
    }
    /// <summary>
    /// Load document by scan service
    /// </summary>
    /// <param name="content"></param>
    /// <param name="fileName"></param>
    /// <param name="ragszam"></param>
    private void ServiceLoadDocumentByScanService(byte[] content, string fileName, string ragszam)
    {
        ScanService service = eRecordService.ServiceFactory.GetScanService();
        KRT_Felhasznalok currentUser = GetCurrentUser();
        string userName = currentUser.UserNev;
        string result = service.LoadDocumentDS(userName, ragszam, "-1", "ePdfTertiveveny", fileName, content, string.Empty, null);

        if (!string.IsNullOrEmpty(result) & result != "OK")
        {
            string msg = string.Format("ScanService.LoadDoument Ragszam:{0}. Hiba:{1}", ragszam, result);
            Logger.Error(msg);

            ErrorMessage += string.Format("</br> Hiba a {0} tértivevény képének feltöltésekor:" + result, ragszam);
            ShowErrorMessage(ErrorMessage);
        }
    }
    /// <summary>
    /// Return current user 
    /// </summary>
    /// <returns></returns>
    private KRT_Felhasznalok GetCurrentUser()
    {
        Contentum.eAdmin.Service.KRT_FelhasznalokService krt_FelhasznalokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam execParam = UI.SetExecParamDefault(Session);
        execParam.Record_Id = FelhasznaloProfil.FelhasznaloId(Page);
        Contentum.eBusinessDocuments.Result result = krt_FelhasznalokService.Get(execParam);
        return (KRT_Felhasznalok)result.Record;
    }
    #endregion

    #region HELPER
    private void ShowErrorMessage(string msg)
    {
        Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();
        result.ErrorMessage += msg;
        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    }
    private static List<ePdfXmlDataClass> ExtractePdfXmlDataFromXls(PdfFileAttachment attachment)
    {
        //LZS - BUG_8536
        //Ékezetes szövegek kiolvasása miatt UTF8-ra írtam át.
        string myString = System.Text.Encoding.UTF8.GetString(attachment.Content);
        //string myString = System.Text.Encoding.Default.GetString(attachment.Content);

        if (!myString.Contains(ConstTertiXmlDataHeader1)
            && !myString.Contains(ConstTertiXmlDataHeader2)
            && !myString.Contains(ConstTertiXmlDataHeader3)
            && !myString.Contains(ConstTertiXmlDataHeader4)
            && !myString.Contains(ConstTertiXmlDataHeader0))
            return null;

        int trStartIdx = myString.IndexOf("<tr>");
        int trStopIdx = myString.LastIndexOf("</tr>") + 5;
        string content = myString.Substring(trStartIdx, trStopIdx - trStartIdx);

        string[] trSplitted = content.Split(new string[] { "<tr>" }, StringSplitOptions.RemoveEmptyEntries);
        List<ePdfXmlDataClass> collection = new List<ePdfXmlDataClass>();
        ePdfXmlDataClass tmp;
        for (int i = 1; i < trSplitted.Length; i++)
        {
            string[] tdSplitted = trSplitted[i]
                            .Replace("<tr>", string.Empty)
                            .Replace("</tr>", string.Empty)
                            .Replace("</td>", string.Empty)
                            .Split(new string[] { "<td>" }, StringSplitOptions.None);
            List<string> tdSplittedNew = new List<string>();
            for (int j = 1; j < tdSplitted.Length; j++)
            {
                tdSplittedNew.Add(tdSplitted[j]);
            }

            if (tdSplittedNew.Count < 6 || string.IsNullOrEmpty(tdSplittedNew[5]))
                continue;

            tmp = new ePdfXmlDataClass();
            if (tdSplittedNew.Count >= 1)
                tmp.JegyzekAzonosito = tdSplittedNew[0];
            if (tdSplittedNew.Count >= 2)
                tmp.FelvetelDatuma = tdSplittedNew[1];
            if (tdSplittedNew.Count >= 3)
                tmp.Ragszam = tdSplittedNew[2];
            if (tdSplittedNew.Count >= 4)
                tmp.KezbDatuma = tdSplittedNew[3];
            if (tdSplittedNew.Count >= 5)
                tmp.Kezbesitve = tdSplittedNew[4];
            if (tdSplittedNew.Count >= 6)
                tmp.KepFile = tdSplittedNew[5];
            if (tdSplittedNew.Count >= 7)
                tmp.VisszakezbDatuma = tdSplittedNew[6];
            if (tdSplittedNew.Count >= 8)
                tmp.VisszakezbOka = tdSplittedNew[7];
            if (tdSplittedNew.Count >= 9)
                tmp.AdatatadasNapja = tdSplittedNew[8];

            collection.Add(tmp);
        }
        return collection;
    }

    /// <summary>
    /// Adat atadas sorok xml-bõl.
    /// </summary>
    /// <param name="attachment"></param>
    /// <returns></returns>
    private static List<ePdfXmlDataClass> ExtractePdfXmlDataFromXml(PdfFileAttachment attachment)
    {
        ePdfXmlAdatAtadas result;
        XmlSerializer serializer = new XmlSerializer(typeof(ePdfXmlAdatAtadas));
        try
        {
            using (MemoryStream memoryStream = new MemoryStream(attachment.Content))
            {
                using (XmlReader xmlReader = XmlReader.Create(memoryStream))
                {
                    result = (ePdfXmlAdatAtadas)serializer.Deserialize(xmlReader);
                }
            }
        }
        catch (InvalidOperationException)
        {
            //incorrect xml schema 
            return null;
        }
        catch (Exception)
        {
            return null;
        }

        if (result == null || result.Jegyzek == null || result.Jegyzek.Ragszam.Count < 1)
            return null;

        List<ePdfXmlDataClass> collection = new List<ePdfXmlDataClass>();
        ePdfXmlDataClass tmp;
        for (int i = 0; i < result.Jegyzek.Ragszam.Count; i++)
        {
            tmp = new ePdfXmlDataClass();
            tmp.JegyzekAzonosito = result.Jegyzek.KotegAzonosito[i].Trim();
            tmp.FelvetelDatuma = result.Jegyzek.Datum.Trim();
            tmp.Ragszam = result.Jegyzek.Ragszam[i].Trim();
            tmp.KezbDatuma = result.Jegyzek.KezbesitesDatum[i].Trim();
            tmp.Kezbesitve = result.Jegyzek.Kezbesitve[i].Trim();
            tmp.KepFile = result.Jegyzek.Kepfile[i].Trim();
            tmp.VisszakezbDatuma = result.Jegyzek.VisszakezbesitesDatum[i].Trim();
            tmp.VisszakezbOka = result.Jegyzek.VisszakezbesitesOka[i].Trim();
            tmp.AdatatadasNapja = result.Jegyzek.AtadasDatum[i].Trim();

            collection.Add(tmp);
        }
        return collection;
    }

    /// <summary>
    /// Concatenate list to html string
    /// </summary>
    /// <param name="elements"></param>
    /// <returns></returns>
    private string ConcatList(List<string> elements)
    {
        StringBuilder sb = new StringBuilder();
        int count = 1;
        foreach (string element in elements)
        {
            sb.Append(element + ",");
            if (count % 8 == 0)
                sb.Append("</br>");
            ++count;
        }
        sb.Remove(sb.ToString().Length - 1, 1);
        return sb.ToString();
    }
    #endregion
}