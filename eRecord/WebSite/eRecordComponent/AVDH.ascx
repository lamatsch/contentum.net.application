﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AVDH.ascx.cs" Inherits="eRecordComponent_AVDH" %>

<iframe name="iframe_esign" style="display: none;"></iframe>
<a target="iframe_esign" href="javasript:void(0)" id="a_esign"></a>

<asp:HiddenField ID="hfEsignResponse1" runat="server" />
<asp:HiddenField ID="hfIratIds" runat="server" />
<asp:HiddenField ID="hfExternalLinks" runat="server" />
<asp:HiddenField ID="hfRecordIds" runat="server" />
<asp:HiddenField ID="hfCsatolmanyokIds" runat="server" />
<asp:HiddenField ID="hfFolyamatTetelekIds" runat="server" />
<asp:HiddenField ID="hfFilenames" runat="server" />
<asp:HiddenField ID="hfProcId" runat="server" />

<!-- Dialógus ablakok -->

<div id="divError" title="Hiba"></div>

 <div id="UpdateProgressPanel" class="updateProgress" style="display:none;">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <table>
            <tr>
            <td>
                <img src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="" />
            </td>
            <td class="updateProgressText">
                <span id="updateProgressText">Feldolgozás folyamatban...</span>
            </td>
            </tr>
        </table>
   </eUI:eFormPanel> 
</div>

