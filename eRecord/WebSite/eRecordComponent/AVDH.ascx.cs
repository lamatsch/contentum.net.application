﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Net;

public partial class eRecordComponent_AVDH : System.Web.UI.UserControl
{
    #region properties

    private List<string> _iratIds = new List<string>();

    public List<string> iratIds
    {
        get { return _iratIds; }
        set { _iratIds = value; }
    }

    private List<string> _fileNames = new List<string>();

    public List<string> fileNames
    {
        get { return _fileNames; }
        set { _fileNames = value; }
    }

    private List<string> _externalLinks = new List<string>();

    public List<string> externalLinks
    {
        get { return _externalLinks; }
        set { _externalLinks = value; }
    }

    private List<string> _recordIds = new List<string>();

    public List<string> recordIds
    {
        get { return _recordIds; }
        set { _recordIds = value; }
    }

    private List<string> _csatolmanyokIds = new List<string>();

    public List<string> csatolmanyokIds
    {
        get { return _csatolmanyokIds; }
        set { _csatolmanyokIds = value; }
    }

    private List<string> _folyamatTetelekIds = new List<string>();

    public List<string> folyamatTetelekIds
    {
        get { return _folyamatTetelekIds; }
        set { _folyamatTetelekIds = value; }
    }
    #region BLG 2947

    private string _procId = string.Empty;

    public string procId
    {
        get { return _procId; }
        set { _procId = value; }
    }
    #endregion

    #endregion 

    public void StartNewSigningSession()
    {        
        hfExternalLinks.Value = "";
        foreach (var item in externalLinks)
        {
            hfExternalLinks.Value += item + "|";
        }
        hfFilenames.Value = "";
        foreach (var item in fileNames)
        {
            hfFilenames.Value += item + ";";
        }        
        hfExternalLinks.Value = hfExternalLinks.Value.Substring(0, hfExternalLinks.Value.Length - 1);
        hfRecordIds.Value = "";
        foreach (var item in recordIds)
        {
            hfRecordIds.Value += item + ";";
        }
        hfRecordIds.Value = hfRecordIds.Value.Substring(0, hfRecordIds.Value.Length - 1);
        hfCsatolmanyokIds.Value = "";
        foreach (var item in csatolmanyokIds)
        {
            hfCsatolmanyokIds.Value += item + ";";
        }
        hfCsatolmanyokIds.Value = hfCsatolmanyokIds.Value.Substring(0, hfCsatolmanyokIds.Value.Length - 1);
        hfFolyamatTetelekIds.Value = "";
        foreach (var item in folyamatTetelekIds)
        {
            hfFolyamatTetelekIds.Value += item + ";";
        }
        hfFolyamatTetelekIds.Value = hfFolyamatTetelekIds.Value.Substring(0, hfFolyamatTetelekIds.Value.Length - 1);
        hfIratIds.Value = "";
        foreach (var item in iratIds)
        {
            hfIratIds.Value += item + ";";
        }
        hfProcId.Value = procId;

        hfIratIds.Value = hfIratIds.Value.Substring(0, hfIratIds.Value.Length - 1);
        String startupScript = "startLogin();";

        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", startupScript, true);

    }
}