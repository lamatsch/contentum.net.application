using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class eRecordComponent_AgazatiJelTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            AgazatiJel_TextBox.Enabled = value;
            LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                //NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return AgazatiJel_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            AgazatiJel_TextBox.ReadOnly = value;
            LovImageButton.Enabled = !value;
            //NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                //NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return AgazatiJel_TextBox.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    //public string OnClick_View
    //{
    //    set { ViewImageButton.OnClientClick = value; }
    //    get { return ViewImageButton.OnClientClick; }
    //}

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { AgazatiJel_TextBox.Text = value; }
        get { return AgazatiJel_TextBox.Text; }
    }

    public TextBox TextBox
    {
        get { return AgazatiJel_TextBox; }
    }

    public int Rows
    {
        get { return AgazatiJel_TextBox.Rows; }
        set { AgazatiJel_TextBox.Rows = value; }
    }

    public TextBoxMode TextMode
    {
        get { return AgazatiJel_TextBox.TextMode; }
        set { AgazatiJel_TextBox.TextMode = value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                AgazatiJel_TextBox.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                AgazatiJel_TextBox.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                //NewImageButton.Visible = !value;
                //ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public EREC_AgazatiJelek SetAgazatiJelTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        EREC_AgazatiJelek erec_AgazatiJelek = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_AgazatiJelek = (EREC_AgazatiJelek)result.Record;
                Text = erec_AgazatiJelek.Kod + " (" + erec_AgazatiJelek.Nev + ")";
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
        return erec_AgazatiJelek;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {



    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("AgazatiJelekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + AgazatiJel_TextBox.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        //OnClick_New = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
        //    , CommandName.Command + "=" + CommandName.New
        //    + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelMegnevezes.ClientID
        //    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        //OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
        //        "IraIrattariTetelekForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(AgazatiJel_TextBox);
        componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        //componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        //ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
