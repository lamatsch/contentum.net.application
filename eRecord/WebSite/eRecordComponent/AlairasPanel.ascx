﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlairasPanel.ascx.cs" Inherits="eRecordComponent_AlairasPanel" %>
<asp:Panel ID = "AlairasPanel" runat = "server" 
    style = "display:block" align="center">
    <table style="background-color:White;">
    <tr>
        <td>
            <asp:Label ID ="lbAlairas" runat="server">Aláírandó fájl :</asp:Label>
            <asp:FileUpload ID ="AlairandoFajl" runat="server" style="width:85%"/>            
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID ="Tanúsítvány" runat="server" >Tanúsítvány</asp:Label>
            <asp:DropDownList ID = "Alairasok" runat = "server" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:ImageButton  runat="server" style="display:block;padding-bottom:5px;" 
            ImageUrl="~/images/hu/trapezgomb/alairas_trap.jpg"  ID="ImageSign"  CommandName="Sign"
                                                         AlternateText="Aláírás" 
            onmouseover="swapByName(this.id,'alairas_trap2.jpg')" 
            onmouseout="swapByName(this.id,'alairas_trap.jpg')" 
            onclick="ImageSign_Click" />
        </td>
    </tr>
    </table>  
</asp:Panel>
