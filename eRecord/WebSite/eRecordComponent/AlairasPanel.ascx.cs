﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Security.AccessControl;
using System.Security.Principal;
using Contentum.eRecord.Utility;
using Contentum.eUtility;

public partial class eRecordComponent_AlairasPanel : System.Web.UI.UserControl
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        AlairandoFajl.Attributes["onchange"] = ("if(!this.files[0].type.includes(\"xml\")){alert('Csak XML fájl választható ki.');this.value='';}");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string initResult = AlairasokUtility.InitAlairasokStore();
        if (!String.IsNullOrEmpty(initResult))
        {
            Response.Write(string.Format("A tanúsítvány tároló megnyítása nem sikerült : {0}", initResult));
        }

        if (!IsPostBack)
        {
            if (AlairasokUtility.AlairasLista.Count > 0)
            {
                Alairasok.DataSource = AlairasokUtility.AlairasLista;
                Alairasok.DataTextField = "Subject";
                Alairasok.DataBind();
            }
        }
    }

    protected void ImageSign_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        if (ib.CommandName == "Sign")
        {
            if (IsPostBack)
            {
                if (Alairasok.SelectedIndex >= 0)
                {
                    X509Certificate2 selectedCert = AlairasokUtility.AlairasLista[Alairasok.SelectedIndex];
                    if (string.IsNullOrEmpty(AlairandoFajl.FileName))
                    {
                        throw new ArgumentNullException();
                    }

                    Stream ms;

                    try
                    {
                        ms = AlairasokUtility.SignXml(selectedCert, AlairandoFajl.FileContent);
                        SendFile(ms, AlairandoFajl.FileName);
                    }
                    catch (Exception ex)
                    {
                        Response.OutputStream.Flush();
                        Response.ClearHeaders();
                        Response.Write("Nem sikerült az XML fájl aláírása : " + ex.Message);
                    }
                }
                else
                {
                    SendFile(AlairandoFajl.FileContent, AlairandoFajl.FileName);
                }
                Response.End();
            }
        }
        else
        {
        }
    }


    /// <summary>
    /// Elküldi a klinesnek a paraméterként megadott fájlt a paraméterként megadott fájlnévvel.
    /// </summary>
    /// <param name="fs">Küldésre kijelőlt fájl</param>
    /// <param name="filename">Kliens oldalon megjelenő fájlnév</param>    
    private void SendFile(System.IO.Stream fs, string filename)
    {

        Response.OutputStream.Flush();
        Response.ClearHeaders();
        Response.ContentType = "application/xml";
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\";");
        if (!fs.CanRead)
        {
            return;
        }
        fs.Position = 0;
        byte[] buf = new byte[20480];
        try
        {
            while (true)
            {

                int readBytes = fs.Read(buf, 0, buf.Length);
                Response.OutputStream.Write(buf, 0, readBytes);
                if (readBytes == 0)
                    break;

            }
        }
        catch (Exception e)
        {
            Response.OutputStream.Flush();
            Response.ClearHeaders();
            Response.Write("Hiba a fájl mentése közben : " + e.Message);
        }
        finally
        {
            fs.Close();
            Response.OutputStream.Close();
        }
    }


}