<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlairokTab.ascx.cs" Inherits="eRecordComponent_AlairokTab" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc9" %>
<!--CR3175 - kisherceg alkalmaz�sa-->
<%@ Register Src="../Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc6" %>
<!--CR3175 - kisherceg alkalmaz�sa-->

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="TabStatusUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hf_IsThisTabActive" runat="server" Value="0" />
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="AlairokUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <ajaxToolkit:CollapsiblePanelExtender ID="AlairokCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" AutoCollapse="false" AutoExpand="false"
                ExpandedSize="200" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <%-- TabHeader --%>
                        <uc1:TabHeader ID="TabHeader1" runat="server" />
                        <%-- /TabHeader --%>
                        <%-- CR3175 - kisherceg alkalmaz�sa--%>
                        <uc6:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="true"></uc6:FormTemplateLoader> 
                        <%-- CR3175 - kisherceg alkalmaz�sa--%>

                        <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
                            <asp:GridView ID="AlairokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                                OnRowCommand="AlairokGridView_RowCommand" OnRowDataBound="AlairokGridView_RowDataBound"
                                OnSorting="AlairokGridView_Sorting">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField >
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                CssClass="HideCheckBoxText" />
                                            <%--'<%# Eval("Id") %>'--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                    </asp:CommandField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                        <ItemTemplate>                                    
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="Alairo_Nev" HeaderText="Al��r�" SortExpression="Alairo_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HelyettesAlairo_Nev" HeaderText="Helyettes al��r�" SortExpression="HelyettesAlairo_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_Alairo" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FelhaszCsop_Id_HelyettesAlairo" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>

                                    <%--<asp:BoundField DataField="AlairasSorrend" HeaderText="Sorrend" SortExpression="AlairasSorrend">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="Sorrend" SortExpression="AlairasSorrend">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label_Sorrend" runat="server" Text='<%# Eval("AlairasSorrend") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AlairoSzerep_Nev" HeaderText="Al��r�&nbsp;szerepe" SortExpression="AlairoSzerep_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="AlairoSzerep" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="AlairasTipus" HeaderText="Al��r�s&nbsp;t�pus" SortExpression="AlairasTipus">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="AlairasMod" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AlairasSzabaly_Id" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <%-- %><asp:TemplateField HeaderText="Date Of Birth">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDOB" runat="server" ReadOnly="true" class = "Calender" />
                                            <img src="calender.png" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                     <asp:TemplateField HeaderText="D�tum" SortExpression="AlairasDatuma">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" wrap="false"/>
                                        <ItemTemplate>
                                            <asp:TextBox ID="AlairasDatumaCalendar" runat="server" ReadOnly="true" Visible="false" Text='<%# Eval("AlairasDatuma") %>'></asp:TextBox>
                                            <asp:ImageButton TabIndex = "-1"
                                                ID="AlairasDatumaCalendarImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
                                                OnClientClick="return false;" CausesValidation="false" Visible="false"/>
                                            <ajaxToolkit:CalendarExtender ID="AlairasDatumaCalendarExtender" Enabled="false"
                                                runat="server" PopupButtonID="AlairasDatumaCalendarImageButton" TargetControlID="AlairasDatumaCalendar" PopupPosition="BottomLeft">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:Label ID="AlairasDatumaLabel" runat="server" Visible="true" Text='<%# Eval("AlairasDatuma") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    
                                    <%--<asp:BoundField DataField="AlairasDatuma" HeaderText="D�tum" SortExpression="AlairasDatuma">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" Wrap="false" />
                                    </asp:BoundField>--%>
                                    <%-- CR3175 - kisherceg alkalmaz�sa--%>
                                    <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle">
                                        <ItemTemplate>
                                            <asp:Label ID="Allapot" runat="server" Text='<%# Eval("Allapot") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>                            
                             <%-- CR3175 - kisherceg alkalmaz�sa--%>
                             <asp:ImageButton ID="TemplatesSave" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/rendben.jpg" Visible="false" OnClick="TemplatesSave_OnClick"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" />
                             <%-- CR3175 - kisherceg alkalmaz�sa--%>
                             
                             <%-- CR3204 - M�gse gomb az al��r�k �s jogosultak kisherceg�hez.--%>
                             <asp:ImageButton ID="TemplatesDelete" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/megsem.jpg" Visible="false" OnClick="TemplatesDelete_OnClick"
                                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')" />
                             <%-- CR3204 - M�gse gomb az al��r�k �s jogosultak kisherceg�hez.--%>
                        </asp:Panel> 
                        <br />
                                               

                        <eUI:eFormPanel ID="EFormPanel1" runat="server">                        
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text="Al��r�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc4:FelhasznaloCsoportTextBox ID="Alairo_FelhasznaloCsoportTextBox" runat="server">
                                        </uc4:FelhasznaloCsoportTextBox>
                                    </td>
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label7" runat="server" Text="Sorrend:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBoxSorszam" runat="server" Text="" CssClass="NumberTextBoxShort"></asp:TextBox>
                                        <ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server" Minimum="0" Maximum="100"
                                            TargetControlID="TextBoxSorszam" Width="60" />
                                    </td>                                   
                                </tr>
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label_AlairoSzerep_KodtarakDropDownList_Req" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text="Al��r� szerepe:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="AlairoSzerep_KodtarakDropDownList" LabelRequiredIndicatorID="Label_AlairoSzerep_KodtarakDropDownList_Req" Validate="true" runat="server" />
                                    </td>
                                     <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label6" runat="server" Text="D�tum:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:CalendarControl ID="AlairasDatuma_CalendarControl" runat="server" Validate="false">
                                        </uc5:CalendarControl>
                                    </td>
                                    
                                </tr>
                                <tr class="urlapSor_kicsi">
                                   <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label_AlairasTipus" runat="server" Text="Al��r�s t�pus:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="AlairasTipus_ddl" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label2" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Leiras_TextBox" CssClass="mrUrlapInput" runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label8" runat="server" Text="Helyettes al��r�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc4:FelhasznaloCsoportTextBox ID="HelyettesAlairo_FelhasznaloCsoportTextBox" runat="server" Validate="false" />
                                    </td>
                                    <td class="mrUrlapCaption_short">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>                                   
                                </tr>
                            </table>
                            <%-- TabFooter --%>
                            <uc2:TabFooter ID="TabFooter1" runat="server" />
                            <%-- /TabFooter --%>
                        </eUI:eFormPanel>
                        <br />
                        <asp:CheckBox ID="cbRekordHistory"   runat="server"  
                            Text="Rekord el�zm�nyek mutat�sa" checked="false" AutoPostBack="True" 
                            oncheckedchanged="cbRekordHistory_CheckedChanged" />
                        
                        <asp:Panel ID="AlairokHistoryPanel" runat="server" BackColor="WhiteSmoke" Visible = "false">                        
                        <h4>Rekord el�zm�ny lista</h4>
                        <asp:GridView ID="AlairokHistoryGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="true"
                                AllowSorting="False" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%">                            
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField >
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />                                                                               
                                    </asp:TemplateField>                                                                                                      
                                    <asp:BoundField DataField="OldValue" HeaderText="R�gi&nbsp;�rt�k" SortExpression="OldValue" Visible ="false">
                                        <HeaderStyle CssClass="GridViewBorderHeader"  />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="OldValue" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NewValue" HeaderText="�j&nbsp;�rt�k" SortExpression="NewValue" Visible ="false">
                                        <HeaderStyle CssClass="GridViewBorderHeader"  />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="NewValue" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>                                                                        
                                       <asp:BoundField DataField="Alairo_Nev" HeaderText="Al��r�" SortExpression="Alairo_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"  />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_Alairo" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>                                    
                                    <asp:TemplateField HeaderText="Sorrend" SortExpression="AlairasSorrend">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label_Sorrend" runat="server" Text='<%# Eval("AlairasSorrend") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AlairoSzerep_Nev" HeaderText="Al��r�&nbsp;szerepe" SortExpression="AlairoSzerep_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="AlairoSzerep" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="AlairasTipus" HeaderText="Al��r�s&nbsp;t�pus" SortExpression="AlairasTipus">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="AlairasMod" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AlairasSzabaly_Id" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                     <asp:TemplateField HeaderText="D�tum" SortExpression="AlairasDatuma">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBorder" wrap="true"/>
                                        <ItemTemplate>
                                            <asp:TextBox ID="AlairasDatumaCalendar" runat="server" ReadOnly="true" Visible="false" Text='<%# Eval("AlairasDatuma") %>'></asp:TextBox>
                                            <asp:ImageButton TabIndex = "-1"
                                                ID="AlairasDatumaCalendarImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
                                                OnClientClick="return false;" CausesValidation="false" Visible="false"/>
                                            <ajaxToolkit:CalendarExtender ID="AlairasDatumaCalendarExtender" Enabled="false"
                                                runat="server" PopupButtonID="AlairasDatumaCalendarImageButton" TargetControlID="AlairasDatumaCalendar" PopupPosition="BottomLeft">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:Label ID="AlairasDatumaLabel" runat="server" Visible="true" Text='<%# Eval("AlairasDatuma") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    
                                    <asp:TemplateField HeaderText="M�velet" SortExpression="Operation">
                                        <HeaderStyle CssClass="GridViewBorderHeader"  Width="100px"/>
                                        <ItemStyle CssClass="GridViewBorder" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label_Operation" runat="server" Text='<%# Eval("Operation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Executor" HeaderText="V�grehajt�" SortExpression="RowId">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>                                    

                                     <asp:TemplateField HeaderText="V�grehajt�s ideje" SortExpression="ExecutionTime">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBorder" wrap="true"/>
                                        <ItemTemplate>
                                            <asp:TextBox ID="ExecutionDatumaCalendar" runat="server" ReadOnly="true" Visible="false" Text='<%# Eval("ExecutionTime") %>'></asp:TextBox>
                                            <asp:ImageButton TabIndex = "-1"
                                                ID="ExecutionDatumaCalendarImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
                                                OnClientClick="return false;" CausesValidation="false" Visible="false"/>
                                            <ajaxToolkit:CalendarExtender ID="ExecutionDatumaCalendarExtender" Enabled="false"
                                                runat="server" PopupButtonID="ExecutioDatumaCalendarImageButton" TargetControlID="ExecutionDatumaCalendar" PopupPosition="BottomLeft">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:Label ID="ExecutionDatumaLabel" runat="server" Visible="true" Text='<%# Eval("ExecutionTime") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                                                                                                              
                                </Columns>
                                <PagerSettings Visible="true" />
                            </asp:GridView>  
                        </asp:Panel>

                         <eUI:eFormPanel ID="Visszautasitas_EFormPanel" runat="server" Visible="false">                        
                            <table cellspacing="0" cellpadding="0" width="100%">  
                                <tr class="urlapSor">                                   
                                    <td colspan="2">
                                    <h2 id="header" class="emp_HeaderWrapper">
                                        <asp:Label ID="Label_VisszautasitasFelirat" runat="server" Text="<%$Resources:Form,AlairasVisszautasitasTitle %>" CssClass="emp_Header"></asp:Label>
                                     </h2>
                                    </td>                                    
                                </tr>                 
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>                                        
                                <tr class="urlapSor">                                   
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label13" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="VisszautasitasMegjegyzesTextBox" CssClass="mrUrlapInput" runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>   
                            </table> 
                            <br />                           
                            <uc2:TabFooter ID="TabFooter_Visszautasitas" runat="server" />                            
                        </eUI:eFormPanel>
                        
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
