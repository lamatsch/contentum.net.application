using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Text.RegularExpressions;
using Contentum.eDocument.Service;

public partial class eRecordComponent_AlairokTab : System.Web.UI.UserControl
{
    private bool IsIratKiadmanyozott
    {
        get;
        set;
    }
    private bool IsThisTabActive
    {
        get
        {
            if (hf_IsThisTabActive.Value == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    private bool _ChangedToActive = false;
    public bool Active
    {
        get { return _ChangedToActive; }
        set
        {
            _ChangedToActive = value;
            MainPanel.Visible = value;
            if (value == true)
            {
                hf_IsThisTabActive.Value = "1";
            }
            else
            {
                hf_IsThisTabActive.Value = "0";
            }
        }
    }
    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    private const string kcs_ALAIRO_SZEREP = "ALAIRO_SZEREP";

    // Az objektumban t�rt�n� v�ltoz�sokat jelz� esem�ny, ami pl. kiv�lthat egy jogosults�gellen�rz�st
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");
        Command = Request.QueryString.Get(QueryStringVars.Command);

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        TabFooter_Visszautasitas.ButtonsClick += new CommandEventHandler(TabFooter_Visszautasitas_ButtonsClick);

        #region CR3175 - kisherceg alkalmaz�sa
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
        FormTemplateLoader1.ErrorPanel = EErrorPanel1;
        CustomTemplateTipusNev = "Al��r�kTemplate";
        FormTemplateLoader1.SearchObjectType = _type;
        #endregion CR3175 - kisherceg alkalmaz�sa
        //UI ui = new UI();
        //ui.SetClientScriptToGridViewSelectDeSelectButton(AlairokGridView);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!MainPanel.Visible) return;
        if (!IsThisTabActive) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        #region CR3175 - kisherceg alkalmaz�sa
        if (!IsPostBack)
        {
            TemplatesSave.Visible = false;
            TemplatesDelete.Visible = false;
        }
        #endregion CR3175 - kisherceg alkalmaz�sa
        if (Command == CommandName.Modify)
        {
            // Al��r�szerep v�ltoztat�s�ra �jra kell t�lteni az al��r�s t�pus dropdownlist�t is:
            AlairoSzerep_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(AlairoSzerep_KodtarakDropDownList_SelectedIndexChanged);
            AlairoSzerep_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        }

        #region __doPostBack -kel k�ld�tt esem�nyek figyel�se:
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshAlairoSzabalyok:
                    RefreshAlairoSzabalyok();
                    break;
            }
        }
        #endregion

        Alairo_FelhasznaloCsoportTextBox.TryFireChangeEvent = true;

        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        //TabFooter1.SaveValidationGroup = "Alairok";
        //KezelesTipusKodtarakDropDownList.ValidationGroup = TabFooter1.SaveValidationGroup;
        //LeirasTextBox. = TabFooter1.SaveValidationGroup;                
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //if (!MainPanel.Visible) return;

        if (!IsThisTabActive) return;

        //// UpdatePanel friss�t�se:
        //if (_ChangedToActive)
        //{
        //    AlairokUpdatePanel.Update();
        //}

        #region Javascriptes cuccok beregisztr�l�sa

        if (Command == CommandName.Modify)
        {
            TabHeader1.ElektronikusAlairasResetVisible = true;
            Iratok.Statusz stat = Iratok.GetAllapotById(ParentId, UI.SetExecParamDefault(Page), EErrorPanel1);
            TabHeader1.ElektronikusAlairasResetEnabled = stat.Allapot != KodTarak.IRAT_ALLAPOT.Kiadmanyozott;
            if (TabHeader1.ElektronikusAlairasResetEnabled)
                TabHeader1.ElektronikusAlairasResetOnClientClick = "if(confirm('Biztosan alap�llapotba szeretn� helyezni az irat al��r�sait.')) {} else { return false; }";

            /// Al��r�k v�ltoz�s�nak figyel�se:
            /// Ha v�ltozott az al��r�, �s al��r� szerep ki volt t�ltve --> postBack, al��r�s t�pusok felt�lt�se
            /// Ha az al��r� szerep disabled volt, enged�lyezni kell

            StringBuilder sb_alairoChanged = new StringBuilder();
            sb_alairoChanged.Append(" var alairoSzerep_ddl = $get('" + AlairoSzerep_KodtarakDropDownList.DropDownList.ClientID + @"');
                    if (alairoSzerep_ddl) 
                    {        
                        if (alairoSzerep_ddl.disabled == true) { alairoSzerep_ddl.disabled = false; }
                        // ha van kiv�lasztva al��r�szerep is, akkor postBack --> al��r�s t�pusok felt�lt�se
                        if (alairoSzerep_ddl.selectedIndex > 0)
                        {
                            window.setTimeout(" + Page.ClientScript.GetPostBackEventReference(Alairo_FelhasznaloCsoportTextBox.TextBox, EventArgumentConst.refreshAlairoSzabalyok) + @",500);
                        }
                    }");

            Alairo_FelhasznaloCsoportTextBox.TextBox.Attributes["onchange"] = sb_alairoChanged.ToString();

            StringBuilder sb_alairasModok = new StringBuilder();

            // al��r�sm�d dictionary a viewstate-b�l:
            Dictionary<string, string> alairasModDict = (Dictionary<string, string>)ViewState[viewState_AlairasModDictionary];
            if (alairasModDict != null)
            {
                sb_alairasModok.Append(" var alairasSzabalyok = new Array(); var alairasModok = new Array(); ");
                int i = 0;
                foreach (KeyValuePair<string, string> kvp in alairasModDict)
                {
                    sb_alairasModok.Append(" alairasSzabalyok[" + i + "]= '" + kvp.Key + @"'; 
                                             alairasModok[" + i + "]= '" + kvp.Value + "'; ");
                    i++;
                }

                sb_alairasModok.Append(@"
            
                                var ddl_AlairasTipus = $get('" + AlairasTipus_ddl.ClientID + @"');
                                var alairasDatumaTextBox = $get('" + AlairasDatuma_CalendarControl.TextBox.ClientID + @"');
                                var alairasDatumaImage = $get('" + AlairasDatuma_CalendarControl.CalendarImage.ClientID + @"');
            
                                function CheckAlairasAdatok()
                                {                        
                                    if (ddl_AlairasTipus)
                                    { 
                                        if (ddl_AlairasTipus.selectedIndex >= 0)
                                        {
                                            var alairasSzabalyId = ddl_AlairasTipus.options[ddl_AlairasTipus.selectedIndex].value;
                                            var alairasMod = GetAlairasModByAlairasSzabalyId(alairasSzabalyId);                                            
                                            if (alairasMod == 'M_UTO')
                                            {
                                                // k�telez� kit�ltelni az al��r�s d�tum�t                                    
                                                if (alairasDatumaTextBox && alairasDatumaTextBox.value == '')
                                                {
                                                    alert('Az al��r�s d�tum�nak megad�sa k�telez�!'); return false;
                                                }   
                                            }
                                        }
                                        else
                                        {
                                            alert('Nincs kiv�lasztva al��r�s t�pus!'); return false;
                                        }
                                    }
            
                                    return true;
                                }

                                function GetAlairasModByAlairasSzabalyId(alairasSzabalyId)
                                {                                    
                                    var i =0;
                                    for (i=0;i<alairasSzabalyok.length;i++)
                                    {                                        
                                        if (alairasSzabalyok[i] == alairasSzabalyId)
                                        {
                                            return alairasModok[i];
                                        }
                                    }
                                    return '';
                                }                    
            
                                function AlairasTipusDropDownChanged()
                                {                                    
                                    var alairasMod = '';
                                    if (ddl_AlairasTipus && ddl_AlairasTipus.selectedIndex >= 0)
                                    {
                                        var alairasSzabalyId = ddl_AlairasTipus.options[ddl_AlairasTipus.selectedIndex].value;
                                        alairasMod = GetAlairasModByAlairasSzabalyId(alairasSzabalyId);                                        
                                    }
                                    if (alairasMod == 'M_UTO')
                                    {                                        
                                        // al��r�s d�tum textbox enged�lyez�se:
                                        if (alairasDatumaTextBox)
                                        {
                                            alairasDatumaTextBox.disabled = false;
                                        }
                                        if (alairasDatumaImage)
                                        {
                                            alairasDatumaImage.disabled = false;
                                            alairasDatumaImage.className = '';
                                        }
                                    }
                                    else
                                    {
                                       // al��r�s d�tum textbox letilt�sa:
                                        if (alairasDatumaTextBox)
                                        {
                                            alairasDatumaTextBox.disabled = true;
                                            alairasDatumaTextBox.value = '';
                                        } 
                                        if (alairasDatumaImage)
                                        {
                                            alairasDatumaImage.disabled = true;
                                            alairasDatumaImage.className = 'disabledCalendarImage';                                            
                                        }
                                    }
                                }
            
                                ");

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "js_CheckAlairasAdatok", sb_alairasModok.ToString(), true);

                AlairasTipus_ddl.Attributes["onchange"] = " AlairasTipusDropDownChanged(); ";

                TabFooter1.ImageButton_Save.OnClientClick = " if (CheckAlairasAdatok()==false){return false;} "
                       + TabFooter1.ImageButton_Save.OnClientClick;
            }
        }
        #endregion


        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + CommandName.New);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + CommandName.Invalidate);
        //TabHeader1.VisszaUtasitasEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + CommandName.Modify);
        #region CR3175 - kisherceg alkalmaz�sa
        FormTemplateLoader1.Visible = TabHeader1.NewVisible;
        #endregion CR3175 - kisherceg alkalmaz�sa
        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(AlairokGridView));
        }
    }

    public void ReLoadTab()
    {
        if (!IsThisTabActive) return;

        MainPanel.Visible = true;

        if (ParentForm != Constants.ParentForms.IraIrat)
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
        else
        {
            //if (!MainPanel.Visible) return;

            // itt kell a tiltast megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
            TabHeader1.ViewVisible = false;
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;
            TabHeader1.VisszaUtasitasVisible = false;
            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(AlairokGridView.ClientID);
            TabHeader1.VisszaUtasitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


            ClearForm();
            EFormPanel1.Visible = false;
            Visszautasitas_EFormPanel.Visible = false;

            AlairokGridViewBind();
            pageView.SetViewOnPage(Command);
        }
        #region CR3175  - kisherceg alkalmaz�sa
        TemplatesSave.Visible = false;
        TemplatesDelete.Visible = false;
        #endregion CR3175  - kisherceg alkalmaz�sa
    }

    #region CR3175  - kisherceg alkalmaz�sa

    private Type _type = typeof(DataTable);
    public object TemplateObject
    {
        get { return FormTemplateLoader1.SearchObject; }
        set { FormTemplateLoader1.SearchObject = value; }
    }

    public Type TemplateObjectType
    {
        get { return FormTemplateLoader1.SearchObjectType; }
        set { FormTemplateLoader1.SearchObjectType = value; }
    }

    public string CustomTemplateTipusNev
    {
        get { return FormTemplateLoader1.CustomTemplateTipusNev; }
        set { FormTemplateLoader1.CustomTemplateTipusNev = value; }
    }

    public string CurrentTemplateName
    {
        get { return FormTemplateLoader1.CurrentTemplateName; }
        set { FormTemplateLoader1.CurrentTemplateName = value; }
    }

    public string CurrentTemplateId
    {
        get { return FormTemplateLoader1.CurrentTemplateId; }
        set { FormTemplateLoader1.CurrentTemplateId = value; }
    }

    /// <summary>
    /// �j template l�trehoz�sa
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void NewTemplate(object newSearchObject)
    {
        FormTemplateLoader1.NewTemplate(newSearchObject);
    }

    /// <summary>
    /// Aktu�lis template elment�se
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void SaveCurrentTemplate(object newSearchObject)
    {
        FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    }

    public void TemplateReset()
    {
        FormTemplateLoader1.TemplateReset();
    }

    //public event CommandEventHandler ButtonsClick;

    protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            SetGridViewRowsFromTemplate(TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            NewTemplate(GetGridViewRowsToTemplate());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SaveCurrentTemplate(GetGridViewRowsToTemplate());
        }
        ErrorUpdatePanel1.Update();

        //else if (e.CommandName == CommandName.InvalidateTemplate)
        //{
        //    // FormTemplateLoader lekezeli           
        //}
    }
    #region Template bet�lt�s a gridview-ba
    private void SetGridViewRowsFromTemplate(object TemplateObject)
    {

        // ha nincs megadva a ParentId, nem tudunk sz�rni --> kil�p�s
        if (String.IsNullOrEmpty(ParentId))
        {
            return;
        }

        UI.ClearGridViewRowSelection(AlairokGridView);


        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            String sortExpression = Search.GetSortExpressionFromViewState("AlairokGridView", ViewState, "AlairasSorrend, Alairo_Nev");
            SortDirection sortDirection = Search.GetSortDirectionFromViewState("AlairokGridView", ViewState);
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratAlairokService service = null;
            EREC_IratAlairokSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            search = (EREC_IratAlairokSearch)Search.GetSearchObject(Page, new EREC_IratAlairokSearch());

            search.Obj_Id.Value = ParentId;
            search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("AlairokGridView", ViewState, sortExpression, sortDirection);
            Result res = service.GetAllWithExtension(ExecParam, search);
            DataTable dt = GetDataTableFromTemplate(TemplateObject);

            foreach (DataRow row in dt.Rows)
            {
                DataRow dr = res.Ds.Tables[0].NewRow();

                foreach (DataColumn col in res.Ds.Tables[0].Columns)
                {
                    if (dt.Columns.Contains(col.ColumnName))
                    {
                        try
                        {
                            dr[col.ColumnName] = row[col.ColumnName];
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                res.Ds.Tables[0].Rows.Add(dr);
            }

            ui.GridViewFill(AlairokGridView, res, EErrorPanel1, ErrorUpdatePanel1);
            UI.SetTabHeaderRowCountText(res, TabHeader);
        }
        TemplatesSave.Visible = true;
        TemplatesDelete.Visible = true;
    }
    private DataTable GetDataTableFromTemplate(object TemplateObject)
    {
        DataTable dt = new DataTable();
        if (TemplateObject is DataTable)
            dt = (TemplateObject as DataTable);

        return dt;
    }
    #endregion Template bet�lt�s a gridview-ba
    #region a gridview-ba bet�lt�tt template rekordok esem�nyei
    private bool _allowGridViewFillWhenCheck = true;
    //create checked template rows - delete template rows = postback?
    protected void TemplatesSave_OnClick(object sender, EventArgs e)
    {
        try
        {
            _allowGridViewFillWhenCheck = false;
            // CR3196 Ne lehessen Al��r�t felvinni, ha nincs csatolm�ny
            string errormsg = "";
            if (new Alairo(Page, ParentId).Felveheto(out errormsg))
            {
                DataTable dt = GetGridViewRowsToDatabase();

                SaveBusinessObjectsFromDataTable(dt);
            }
            else
                throw new Exception(errormsg);
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "hiba", ex.Message);
            ErrorUpdatePanel1.Update();
        }
        finally
        {
            _allowGridViewFillWhenCheck = true;
        }
    }

    protected void TemplatesDelete_OnClick(object sender, EventArgs e)
    {
        try
        {
            AlairokGridViewBind();
            TemplatesDelete.Visible = false;
            TemplatesSave.Visible = false;
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "hiba", ex.Message);
            ErrorUpdatePanel1.Update();
        }
    }
    #endregion a gridview-ba bet�lt�tt template rekordok esem�nyei

    #region A gridview template sorok lek�r�se db-be ment�shez
    private DataTable GetGridViewRowsToDatabase()
    {
        DataTable _resultTable = new DataTable("AlairokGridView");

        foreach (DataControlFieldCell cell in AlairokGridView.HeaderRow.Cells)
        {
            int cellIndex = 0;
            if (cell.ContainingField.HeaderStyle.CssClass == "GridViewCheckBoxTemplateHeaderStyle")
            {
                if (cellIndex == 0)
                    _resultTable.Columns.Add("Id");
                ++cellIndex;
                continue;
            }
            if (cell.ContainingField is BoundField && (cell.ContainingField as BoundField).DataField.Equals("Allapot_Nev")) continue;
            else if (cell.ContainingField is BoundField)
            {
                _resultTable.Columns.Add((cell.ContainingField as BoundField).DataField);
            }
            else
                if (cell.ContainingField is TemplateField)
            {
                if (!String.IsNullOrEmpty(cell.ContainingField.SortExpression))
                {
                    _resultTable.Columns.Add(cell.ContainingField.SortExpression);
                }
            }
            cellIndex++;
        }

        foreach (GridViewRow rowItem in AlairokGridView.Rows)
        {
            DataRow _resultRowItem = _resultTable.NewRow();
            if (rowItem.Cells[0].Controls[1].GetType() == typeof(CheckBox) && (!string.IsNullOrEmpty((rowItem.Cells[0].Controls[1] as CheckBox).Text) || !(rowItem.Cells[0].Controls[1] as CheckBox).Checked))
            {
                //if (!(rowItem.Cells[0].Controls[1] as CheckBox).Checked)
                //{
                continue;
                //}
            }
            else
            {
                _resultRowItem["Id"] = "";
            }
            foreach (DataControlFieldCell cell in rowItem.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    if (_resultTable.Columns.Contains((cell.ContainingField as BoundField).DataField))
                    {
                        _resultRowItem[(cell.ContainingField as BoundField).DataField] = HttpUtility.HtmlDecode(cell.Text).Trim();
                    }
                }
                else
                    if (cell.ContainingField is TemplateField && cell.Controls.Count != 0)
                {
                    string controlString = "";
                    for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                    {
                        if (cell.Controls[controlIndex].GetType() == typeof(Label))
                        {
                            controlString += Contentum.eUtility.TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                        }
                        //if (cell.Controls[controlIndex].GetType() == typeof(Image) || cell.Controls[controlIndex].GetType().Name == "CustomFunctionImageButton")
                        //{
                        //    if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                        //    {
                        //        controlString = cell.ContainingField.HeaderText;
                        //    }
                        //}
                        if (cell.Controls[controlIndex].GetType() == typeof(CheckBox))
                        {
                            controlString += (rowItem.Cells[0].Controls[1] as CheckBox).Text;
                        }
                        if (cell.Controls[controlIndex].GetType() == typeof(CustomTextBox))
                        {
                            string date = (cell.Controls[controlIndex] as CustomTextBox).Text;
                            if (!string.IsNullOrEmpty(date))
                            {
                                DateTime datum = Convert.ToDateTime(date);
                                if (datum > DateTime.Now)
                                {
                                    throw new Exception(Resources.Error.SablonDatumJovobeMutato);
                                }
                            }
                            controlString += date;
                        }
                    }
                    if (!String.IsNullOrEmpty(controlString))
                    {
                        if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                        {
                            _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                        }
                        else if (_resultTable.Columns.Contains(cell.ContainingField.SortExpression))
                        {
                            _resultRowItem[cell.ContainingField.SortExpression] = controlString;
                        }
                    }
                }
            }
            _resultTable.Rows.Add(_resultRowItem);
        }
        return _resultTable;
    }
    #endregion A gridview template sorok lek�r�se db-be ment�shez

    #region A kijel�lt template sorok l�trehoz�sa
    // datatable --> business object
    private void SaveBusinessObjectsFromDataTable(DataTable dt)
    {
        using (EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService())
        {
            foreach (DataRow row in dt.Rows)
            {
                EREC_IratAlairok erec_IratAlairok = new EREC_IratAlairok();
                erec_IratAlairok.Updated.SetValueAll(false);
                erec_IratAlairok.Base.Updated.SetValueAll(false);

                erec_IratAlairok.Obj_Id = ParentId;
                erec_IratAlairok.Updated.Obj_Id = true;


                erec_IratAlairok.FelhasznaloCsoport_Id_Alairo = row["FelhasznaloCsoport_Id_Alairo"].ToString();
                erec_IratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                erec_IratAlairok.FelhaszCsop_Id_HelyettesAlairo = row["FelhaszCsop_Id_HelyettesAlairo"].ToString();
                erec_IratAlairok.Updated.FelhaszCsop_Id_HelyettesAlairo = true;

                erec_IratAlairok.AlairoSzerep = row["AlairoSzerep"].ToString();
                erec_IratAlairok.Updated.AlairoSzerep = true;

                erec_IratAlairok.AlairasSzabaly_Id = row["AlairasSzabaly_Id"].ToString();
                erec_IratAlairok.Updated.AlairasSzabaly_Id = true;


                erec_IratAlairok.AlairasMod = row["AlairasMod"].ToString();
                erec_IratAlairok.Updated.AlairasMod = true;


                if (erec_IratAlairok.AlairasMod == "M_UTO")
                {
                    erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    erec_IratAlairok.Updated.Allapot = true;
                    erec_IratAlairok.AlairasDatuma = row["AlairasDatuma"].ToString();
                    erec_IratAlairok.Updated.AlairasDatuma = true;
                }
                else
                {
                    erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                    erec_IratAlairok.Updated.Allapot = true;
                }

                erec_IratAlairok.AlairasSorrend = row["AlairasSorrend"].ToString();
                erec_IratAlairok.Updated.AlairasSorrend = true;

                erec_IratAlairok.Leiras = row["Leiras"].ToString();
                erec_IratAlairok.Updated.Leiras = true;

                erec_IratAlairok.Base.Ver = "1";
                erec_IratAlairok.Base.Updated.Ver = true;

                string errorMsg;
                if (!AlairoFelveheto(erec_IratAlairok.AlairasMod, out errorMsg))
                {
                    ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", errorMsg);
                    ErrorUpdatePanel1.Update();
                    return;
                }

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                Result result = service.Insert(execParam, erec_IratAlairok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    ReLoadTab();
                    //RaiseEvent_OnChangedObjectProperties();
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }

            }
        }
    }
    #endregion A kijel�lt template sorok l�trehoz�sa

    #region A gridview sorok lek�r�se db-be ment�shez (sablon ment�se)
    private DataTable GetGridViewRowsToTemplate()
    {
        DataTable _resultTable = new DataTable("AlairokGridView");

        foreach (DataControlFieldCell cell in AlairokGridView.HeaderRow.Cells)
        {
            int cellIndex = 0;
            if ((cell.ContainingField is BoundField && (cell.ContainingField as BoundField).DataField.Equals("Allapot_Nev")) || cell.ContainingField.SortExpression.Equals("AlairasDatuma")) continue;
            else if (cell.ContainingField is BoundField)
            {
                _resultTable.Columns.Add((cell.ContainingField as BoundField).DataField);
            }
            else
                if (cell.ContainingField is TemplateField)
            {
                if (!String.IsNullOrEmpty(cell.ContainingField.SortExpression))
                {
                    _resultTable.Columns.Add(cell.ContainingField.SortExpression);
                }
            }
            cellIndex++;
        }

        foreach (GridViewRow rowItem in AlairokGridView.Rows)
        {
            DataRow _resultRowItem = _resultTable.NewRow();
            if (rowItem.Cells[0].Controls[1].GetType() == typeof(CheckBox) && string.IsNullOrEmpty((rowItem.Cells[0].Controls[1] as CheckBox).Text))
            {
                continue;
            }
            foreach (DataControlFieldCell cell in rowItem.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    if (_resultTable.Columns.Contains((cell.ContainingField as BoundField).DataField))
                    {
                        _resultRowItem[(cell.ContainingField as BoundField).DataField] = HttpUtility.HtmlDecode(cell.Text).Trim();
                    }
                }
                else
                    if (cell.ContainingField is TemplateField && cell.Controls.Count != 0)
                {
                    string controlString = "";
                    for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                    {
                        if (cell.Controls[controlIndex].GetType() == typeof(Label))
                        {
                            controlString += Contentum.eUtility.TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                        }
                        if (cell.Controls[controlIndex].GetType() == typeof(Image) || cell.Controls[controlIndex].GetType().Name == "CustomFunctionImageButton")
                        {
                            if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                            {
                                controlString = cell.ContainingField.HeaderText;
                            }
                        }
                        //if (cell.Controls[controlIndex].GetType() == typeof(CheckBox) && cell.ContainingField.SortExpression.Equals("AlairasDatuma"))
                        //{
                        //    controlString += (rowItem.Cells[0].Controls[1] as CheckBox).Text;
                        //}
                    }
                    if (!String.IsNullOrEmpty(controlString))
                    {
                        if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                        {
                            _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                        }
                        else if (_resultTable.Columns.Contains(cell.ContainingField.SortExpression))
                        {
                            _resultRowItem[cell.ContainingField.SortExpression] = controlString;
                        }
                    }
                }
            }
            _resultTable.Rows.Add(_resultRowItem);
        }
        return _resultTable;
    }
    #endregion A gridview sorok lek�r�se db-be ment�shez (sablon ment�se)


    #endregion CR3175 - kisherceg alkalmaz�sa

    private EREC_IraIratok GetIrat(string RecordId)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = RecordId;

        Result result = null;

        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraIratok obj_Irat = (EREC_IraIratok)result.Record;
                return obj_Irat;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }

        }
        return null;
    }

    private DateTime getIktatasDatuma(string RecordId)
    {
        EREC_IraIratok irat = GetIrat(RecordId);
        if (irat != null && irat.IktatasDatuma != null && !string.IsNullOrEmpty(irat.IktatasDatuma))
        {
            return DateTime.Parse(irat.IktatasDatuma);
        }
        else
        {
            return DateTime.MinValue;
        }
    }

    private void TabHeaderButtonsClick(object sender, CommandEventArgs e)
    {
        ClearForm();

        //Command = e.CommandName;

        TabFooter1.CommandArgument = e.CommandName;

        if (e.CommandName == CommandName.New)
        {
            #region CommandName.New
            // CR3196 Ne lehessen Al��r�t felvinni, ha nincs csatolm�ny
            string errormsg = "";
            if (AlairoFelveheto(out errormsg))
            {
                EFormPanel1.Visible = true;
                Visszautasitas_EFormPanel.Visible = false;

                /// Az Al��r� szerep, a D�tum, �s az Al��r�s t�pusa mez�k disabled-be t�ve, 
                /// majd csak akkor aktiv�l�dnak, ha ki lett v�lasztva al��r�
                /// 
                Alairo_FelhasznaloCsoportTextBox.Enabled = true;
                AlairoSzerep_KodtarakDropDownList.Enabled = false;
                AlairasDatuma_CalendarControl.Enabled = false;
                AlairasTipus_ddl.Enabled = false;
                Label_AlairasTipus.Visible = true;
                AlairasTipus_ddl.Visible = true;
                //Alairo_FelhasznaloCsoportTextBox.LovListPostBackArgument = EventArgumentConst.refreshAlairoSzabalyok;
                //Alairo_FelhasznaloCsoportTextBox.RefreshCallingWindow = true;

                // Al��r�s sorrend �rt�ke az eddigi legnagyobb + 1
                int maxSorrendValue = GetMaxSorrendFromGridView();

                if (maxSorrendValue < 1)
                {
                    TextBoxSorszam.Text = "1";
                }
                else
                {
                    TextBoxSorszam.Text = (maxSorrendValue + 1).ToString();
                }
            }
            else
            {
                string js = "alert('" + errormsg + "'); ";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CsatolmanycheckScript", js, true);
            }
            #endregion
        }
        else if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            #region CommandName.View || CommandName.Modify

            EFormPanel1.Visible = true;
            Visszautasitas_EFormPanel.Visible = false;

            if (e.CommandName == CommandName.Modify)
            {
                AlairoSzerep_KodtarakDropDownList.ReadOnly = true;
                Alairo_FelhasznaloCsoportTextBox.ReadOnly = true;
                AlairasTipus_ddl.ReadOnly = true;
                AlairasDatuma_CalendarControl.ReadOnly = true;

            }

            // Al��r�s t�pus mez� elt�ntet�se
            Label_AlairasTipus.Visible = false;
            AlairasTipus_ddl.Visible = false;

            // CommandName.View nincs itt

            String RecordId = UI.GetGridViewSelectedRecordId(AlairokGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;

                Result result = null;

                if (ParentForm == Constants.ParentForms.IraIrat)
                {
                    EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                    result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_IratAlairok erec_IratAlairok = (EREC_IratAlairok)result.Record;
                        LoadComponentsFromBusinessObject(erec_IratAlairok);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                    }
                }

            }

            #endregion
        }
        else if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedAlairok();
            AlairokGridViewBind();
        }
        else if (e.CommandName == "ElektronikusAlairasReset")
        {
            #region CommandName.ElektronikusAlairasReset            

            string RecordId = UI.GetGridViewSelectedRecordId(AlairokGridView);

            if (!String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            #region Alaphelyzetbe �ll�t�s

            EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            ExecParam execParam = UI.SetExecParamDefault(Page);
            Result result = service.AlairasAlapallapotbaHelyezes(execParam, ParentId);
            // TODO : Al��r�s lekezel�s

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
            #endregion
            ReLoadTab();

            #endregion
        }

    }

    private void TabFooterButtonsClick(object sender, CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            //BUG 6380 - Ha m�r van kiadm�nyoz� al��r�, m�sik al��r� csak kisebb sorsz�mmal lehet
            string errormsg = "";
            if (CheckKiadmanyozo(out errormsg))
            {
                if (FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + SubCommand))
                {
                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            break;
                        case CommandName.New:
                            {
                                Result result = null;

                                if (ParentForm == Constants.ParentForms.IraIrat)
                                {
                                    EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                                    EREC_IratAlairok erec_IratAlairok = (EREC_IratAlairok)GetBusinessObjectFromComponents(SubCommand);

                                    #region Ellen�rz�sek, minden meg van-e adva
                                    // TODO: hiba�zenetek resource f�jlba
                                    if (string.IsNullOrEmpty(erec_IratAlairok.AlairoSzerep))
                                    {
                                        // Az al��r� szerep megad�sa k�telez�!
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", "Az al��r� szerep�nek megad�sa k�telez�!");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    if (string.IsNullOrEmpty(erec_IratAlairok.AlairasSzabaly_Id))
                                    {
                                        // Az al��r�s t�pus megad�sa k�telez�!
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", "Az al��r�s t�pus�nak megad�sa k�telez�!");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    string errorMsg;
                                    if (!AlairoFelveheto(erec_IratAlairok.AlairasMod, out errorMsg))
                                    {
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", errorMsg);
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    if (erec_IratAlairok.AlairasMod == "M_UTO" && string.IsNullOrEmpty(erec_IratAlairok.AlairasDatuma))
                                    {
                                        // Az al��r�s d�tum�nak megad�sa k�telez�!
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", "Az al��r�s d�tum�nak megad�sa k�telez�!");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    DateTime iktatasdatuma = getIktatasDatuma(ParentId);
                                    if (erec_IratAlairok.AlairasMod == "M_UTO" && DateTime.Parse(erec_IratAlairok.AlairasDatuma).Date < iktatasdatuma.Date)
                                    {
                                        // Az al��r�s d�tum�nak megad�sa k�telez�!
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", "Az al��r�s d�tuma nem lehet korr�bbi az iktat�s d�tum�n�l.");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                // BUG_6209
                                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ELINTEZETTE_NYILV_KIADM_FELTETELE, false) 
                                        && (erec_IratAlairok.AlairasMod == "M_UTO") 
                                        && (erec_IratAlairok.AlairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo))
                                {
                                    ErrorDetails errorDetails = null;
                                    Iratok.Statusz statusz = Iratok.GetAllapotById(ParentId, UI.SetExecParamDefault(Page), EErrorPanel1);
                                    if (!Iratok.isElintezett(statusz, out errorDetails))
                                    {
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", Resources.Error.ErrorCode_52953);
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }
                                }

                                    #endregion

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                    result = service.Insert(execParam, erec_IratAlairok);
                                }


                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                    RaiseEvent_OnChangedObjectProperties();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(AlairokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel1.Update();
                                }
                                else
                                {
                                    Result result = null;

                                    if (ParentForm == Constants.ParentForms.IraIrat)
                                    {
                                        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                                        EREC_IratAlairok erec_IratAlairok = (EREC_IratAlairok)GetBusinessObjectFromComponents(SubCommand);

                                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                        execParam.Record_Id = recordId;

                                        result = service.Update(execParam, erec_IratAlairok);
                                    }


                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        ReLoadTab();
                                        RaiseEvent_OnChangedObjectProperties();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel1.Update();
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
            }
            else
            {
                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", errormsg);
                ErrorUpdatePanel1.Update();
                return;
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    void TabFooter_Visszautasitas_ButtonsClick(object sender, CommandEventArgs e)
    {
        string SubCommand = (string)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IraIratAlairoModify"))
            {
                switch (SubCommand)
                {
                    case CommandName.VisszaUtasitas:
                        {
                            string recordId = UI.GetGridViewSelectedRecordId(AlairokGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                if (ParentForm == Constants.ParentForms.IraIrat)
                                {
                                    EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

                                    EREC_IratAlairok erec_IratAlairok = GetBusinessObjectFromComponentsForVisszaUtasitas();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.Update(execParam, erec_IratAlairok);

                                    if (!result.IsError)
                                    {
                                        ReLoadTab();
                                        //RaiseEvent_OnChangedObjectProperties();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel1.Update();
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
        else if (e.CommandName == CommandName.Cancel)
        {
            Visszautasitas_EFormPanel.Visible = false;
        }
    }


    private void ClearForm()
    {
        Alairo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
        Alairo_FelhasznaloCsoportTextBox.Text = "";
        Alairo_FelhasznaloCsoportTextBox.Enabled = true;
        Alairo_FelhasznaloCsoportTextBox.ReadOnly = false;

        // �res elem legyen alapb�l, de am�gy k�telez� v�lasztania al��r�szerepet
        AlairoSzerep_KodtarakDropDownList.FillDropDownList(kcs_ALAIRO_SZEREP, true, EErrorPanel1);
        AlairoSzerep_KodtarakDropDownList.Enabled = true;
        AlairoSzerep_KodtarakDropDownList.ReadOnly = false;

        AlairasDatuma_CalendarControl.Text = "";

        Leiras_TextBox.Text = "";

        VisszautasitasMegjegyzesTextBox.Text = "";

        HelyettesAlairo_FelhasznaloCsoportTextBox.Id_HiddenField = String.Empty;
        HelyettesAlairo_FelhasznaloCsoportTextBox.Text = String.Empty;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IratAlairok erec_IratAlairok)
    {
        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            Alairo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IratAlairok.FelhasznaloCsoport_Id_Alairo;
            Alairo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            TextBoxSorszam.Text = erec_IratAlairok.AlairasSorrend;

            AlairoSzerep_KodtarakDropDownList.FillWithOneValue(kcs_ALAIRO_SZEREP, erec_IratAlairok.AlairoSzerep, EErrorPanel1);

            //if (!String.IsNullOrEmpty(erec_IratAlairok.AlairasDatuma))
            //{
            //    Alairva_CheckBox.Checked = true;
            //}
            //else
            //{
            //    Alairva_CheckBox.Checked = false;
            //}

            AlairasDatuma_CalendarControl.Text = erec_IratAlairok.AlairasDatuma;

            Leiras_TextBox.Text = erec_IratAlairok.Leiras;

            HelyettesAlairo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IratAlairok.FelhaszCsop_Id_HelyettesAlairo;
            HelyettesAlairo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            // Verzi�sz�m lement�se:
            Record_Ver_HiddenField.Value = erec_IratAlairok.Base.Ver;

        }
    }

    // form --> business object
    private EREC_IratAlairok GetBusinessObjectFromComponents(String subCommand)
    {
        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            EREC_IratAlairok erec_IratAlairok = new EREC_IratAlairok();
            erec_IratAlairok.Updated.SetValueAll(false);
            erec_IratAlairok.Base.Updated.SetValueAll(false);

            erec_IratAlairok.Obj_Id = ParentId;
            erec_IratAlairok.Updated.Obj_Id = true;

            // M�dos�t�sn�l nem kellenek:
            if (subCommand == CommandName.New)
            {
                erec_IratAlairok.FelhasznaloCsoport_Id_Alairo = Alairo_FelhasznaloCsoportTextBox.Id_HiddenField;
                erec_IratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = pageView.GetUpdatedByView(Alairo_FelhasznaloCsoportTextBox);

                erec_IratAlairok.AlairoSzerep = AlairoSzerep_KodtarakDropDownList.SelectedValue;
                erec_IratAlairok.Updated.AlairoSzerep = pageView.GetUpdatedByView(AlairoSzerep_KodtarakDropDownList);

                erec_IratAlairok.AlairasSzabaly_Id = AlairasTipus_ddl.SelectedValue;
                erec_IratAlairok.Updated.AlairasSzabaly_Id = pageView.GetUpdatedByView(AlairasTipus_ddl);

                // Al��r�sm�d kinyer�se a viewstate-be mentett adatokb�l:
                Dictionary<string, string> alairasModDict = (Dictionary<string, string>)ViewState[viewState_AlairasModDictionary];
                if (alairasModDict != null && !string.IsNullOrEmpty(AlairasTipus_ddl.SelectedValue)
                    && alairasModDict.ContainsKey(AlairasTipus_ddl.SelectedValue))
                {
                    erec_IratAlairok.AlairasMod = alairasModDict[AlairasTipus_ddl.SelectedValue];
                    erec_IratAlairok.Updated.AlairasMod = true;
                }

                if (erec_IratAlairok.AlairasMod == "M_UTO")
                {
                    erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    erec_IratAlairok.Updated.Allapot = true;
                }
                else
                {
                    erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                    erec_IratAlairok.Updated.Allapot = true;
                }

                erec_IratAlairok.AlairasDatuma = AlairasDatuma_CalendarControl.Text;
                erec_IratAlairok.Updated.AlairasDatuma = pageView.GetUpdatedByView(AlairasDatuma_CalendarControl);
            }


            erec_IratAlairok.AlairasSorrend = TextBoxSorszam.Text;
            erec_IratAlairok.Updated.AlairasSorrend = pageView.GetUpdatedByView(TextBoxSorszam);

            erec_IratAlairok.Leiras = Leiras_TextBox.Text;
            erec_IratAlairok.Updated.Leiras = pageView.GetUpdatedByView(Leiras_TextBox);

            erec_IratAlairok.FelhaszCsop_Id_HelyettesAlairo = HelyettesAlairo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_IratAlairok.Updated.FelhaszCsop_Id_HelyettesAlairo = pageView.GetUpdatedByView(HelyettesAlairo_FelhasznaloCsoportTextBox);

            erec_IratAlairok.Base.Ver = Record_Ver_HiddenField.Value;
            erec_IratAlairok.Base.Updated.Ver = true;

            return erec_IratAlairok;
        }

        return null;
    }


    private EREC_IratAlairok GetBusinessObjectFromComponentsForVisszaUtasitas()
    {
        EREC_IratAlairok erec_IratAlairok = new EREC_IratAlairok();
        erec_IratAlairok.Updated.SetValueAll(false);
        erec_IratAlairok.Base.Updated.SetValueAll(false);

        // Al��r�s �llapot: Visszautas�tott
        erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Visszautasitott;
        erec_IratAlairok.Updated.Allapot = true;

        erec_IratAlairok.Leiras = VisszautasitasMegjegyzesTextBox.Text;
        erec_IratAlairok.Updated.Leiras = true;

        // D�tum be�ll�t�sa:
        erec_IratAlairok.AlairasDatuma = DateTime.Now.ToString();
        erec_IratAlairok.Updated.AlairasDatuma = true;

        erec_IratAlairok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IratAlairok.Base.Updated.Ver = true;

        return erec_IratAlairok;
    }


    //private const string sessionName_IratAlairoSzabalyok = "IratAlairoSzabalyok";
    private const string viewState_AlairasModDictionary = "AlairasModDictionary";

    /// <summary>
    /// Al��r�szerep v�ltoz�s�ra �jra kell t�lteni az al��r� szab�lyokat
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AlairoSzerep_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshAlairoSzabalyok();
    }

    #region 7471
    private bool HasKrCsatolmany()
    {
        if (!String.IsNullOrEmpty(ParentId))
        {
            try
            {
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch search = (EREC_CsatolmanyokSearch)Search.GetSearchObject(Page, new EREC_CsatolmanyokSearch());
                search.IraIrat_Id.Value = ParentId;
                search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                Result result_csatGetAll = service.GetAll(ExecParam, search);

                if (!result_csatGetAll.IsError)
                {
                    foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                    {
                        String DokId = row["Dokumentum_Id"].ToString();
                        KRT_DokumentumokService dokServ = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                        ExecParam execParamDok = UI.SetExecParamDefault(Page, new ExecParam());
                        execParamDok.Record_Id = DokId;
                        Result resDok = dokServ.Get(execParamDok);
                        if (String.IsNullOrEmpty(resDok.ErrorCode))
                        {
                            KRT_Dokumentumok dok = (KRT_Dokumentumok)resDok.Record;
                            if ("kr".Equals(dok.Formatum, StringComparison.CurrentCultureIgnoreCase) || "krx".Equals(dok.Formatum, StringComparison.CurrentCultureIgnoreCase))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
            
        }
        return false;
    }
    #endregion

    private void RefreshAlairoSzabalyok()
    {
        // t�r�lj�k a dropdownlista elemeit:
        AlairasTipus_ddl.Items.Clear();

        string alairoSzerep = AlairoSzerep_KodtarakDropDownList.SelectedValue;
        string felhasznaloId_Alairo = Alairo_FelhasznaloCsoportTextBox.Id_HiddenField;

        #region Enabled �ll�t�sa
        if (!String.IsNullOrEmpty(felhasznaloId_Alairo))
        {
            AlairoSzerep_KodtarakDropDownList.Enabled = true;

            if (!String.IsNullOrEmpty(alairoSzerep))
            {
                AlairasTipus_ddl.Enabled = true;
            }
        }

        // Felhaszn�l� textbox letilt�sa:
        Alairo_FelhasznaloCsoportTextBox.Enabled = false;
        #region BUG 7471
        if (HasKrCsatolmany())
        {
            Dictionary<string, string> alairasModDict = new Dictionary<string, string>();
            //KRT_AlairasTipusok
        }

        #endregion

        #endregion

        if (!string.IsNullOrEmpty(alairoSzerep) && !string.IsNullOrEmpty(felhasznaloId_Alairo))
        {
            Dictionary<string, DataRowCollection> alairoSzabalyokDict = null;


            // Objektum l�trehoz�sa
            alairoSzabalyokDict = new Dictionary<string, DataRowCollection>();

            #region Adatok lek�rdez�se adatb�zisb�l:

            AlairasokService service = eRecordService.ServiceFactory.GetAlairasokService();

            string objektumTip = "Irat";
            string objektumId = ParentId;
            string folyamatKod = "IratJovahagyas";
            string muveletKod = null;

            Result result = service.GetAll_PKI_IratAlairoSzabalyok(UI.SetExecParamDefault(Page)
               , felhasznaloId_Alairo, null, objektumTip, objektumId, folyamatKod, muveletKod, alairoSzerep);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                // komponensek Enabled �rt�k�nek vissza�ll�t�sa:
                InitComponentsForAlairasFelvitel();
                return;
            }
            #endregion

            alairoSzabalyokDict.Add(alairoSzerep, result.Ds.Tables[0].Rows);


            // Felt�ltj�k a dropdownList-et:
            if (alairoSzabalyokDict.ContainsKey(alairoSzerep))
            {
                // Az al��r�sszab�lyhoz tartoz� al��r�sm�dot lementj�k ViewState-be, hogy ne kelljen k�l�n lek�rni
                Dictionary<string, string> alairasModDict = new Dictionary<string, string>();

                foreach (DataRow row in alairoSzabalyokDict[alairoSzerep])
                {
                    string row_AlairasNev = row["AlairasNev"].ToString();
                    string row_AlairasSzabaly_Id = row["AlairasSzabaly_Id"].ToString();
                    string row_AlairasMod = row["AlairasMod"].ToString();

                    // dropdownlist�ba felv�tel:
                    AlairasTipus_ddl.Items.Add(new ListItem(row_AlairasNev, row_AlairasSzabaly_Id));
                    // al��r�sm�d dictionary-be:
                    if (alairasModDict.ContainsKey(row_AlairasSzabaly_Id) == false)
                    {
                        alairasModDict.Add(row_AlairasSzabaly_Id, row_AlairasMod);
                    }
                }

                // al��r�sm�dok viewstate-be ment�se:
                ViewState[viewState_AlairasModDictionary] = alairasModDict;

                // Ha az al��r�st�pus dropdown kiv�lasztott eleme 'M_UTO', akkor enged�lyezni kell a d�tumkomponenst
                if (!String.IsNullOrEmpty(AlairasTipus_ddl.SelectedValue) && alairasModDict.ContainsKey(AlairasTipus_ddl.SelectedValue))
                {
                    string alairasMod_selected = alairasModDict[AlairasTipus_ddl.SelectedValue];
                    if (alairasMod_selected == "M_UTO")
                    {
                        AlairasDatuma_CalendarControl.Enabled = true;
                    }
                }
            }
        }

    }

    private void InitComponentsForAlairasFelvitel()
    {
        Alairo_FelhasznaloCsoportTextBox.Enabled = true;
        //AlairoSzerep_KodtarakDropDownList.Enabled = false;
        AlairasTipus_ddl.Enabled = false;
        AlairasDatuma_CalendarControl.Enabled = false;

        //AlairoSzerep_KodtarakDropDownList.FillDropDownList(kcs_ALAIRO_SZEREP, true, EErrorPanel1);
    }

    #endregion

    #region List
    protected void AlairokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("AlairokGridView", ViewState, "AlairasSorrend, Alairo_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("AlairokGridView", ViewState);
        AlairokGridViewBind(sortExpression, sortDirection);

    }

    protected void AlairokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        // ha nincs megadva a ParentId, nem tudunk sz�rni --> kil�p�s
        if (String.IsNullOrEmpty(ParentId))
        {
            return;
        }

        UI.ClearGridViewRowSelection(AlairokGridView);


        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratAlairokService service = null;
            EREC_IratAlairokSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            search = (EREC_IratAlairokSearch)Search.GetSearchObject(Page, new EREC_IratAlairokSearch());

            search.Obj_Id.Value = ParentId;
            search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("AlairokGridView", ViewState, SortExpression, SortDirection);
            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(AlairokGridView, res, EErrorPanel1, ErrorUpdatePanel1);

            Result reshistory = service.HistoryGetAllByObjId(ExecParam.Clone(), ParentId);

            ui.GridViewFill(AlairokHistoryGridView, reshistory, EErrorPanel1, ErrorUpdatePanel1);
            UI.SetTabHeaderRowCountText(res, TabHeader);
        }

    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";

            // �llapot lek�r�se a GridViewb�l:
            string allapot = UI.GetGridViewColumnValueOfSelectedRow(AlairokGridView, "Label_Allapot");

            if (allapot == KodTarak.IRATALAIRAS_ALLAPOT.Alairando)
            {
                TabHeader1.VisszaUtasitasOnClientClick = "";
            }
            else
            {
                TabHeader1.VisszaUtasitasOnClientClick = "alert('" + Resources.Error.UIAlairasNemVisszautasithato + "'); return false;";
            }
        }
    }

    protected void AlairokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(AlairokGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;
        }
    }

    /// <summary>
    /// T�rli a AlairokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedAlairok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IraIratAlairoInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(AlairokGridView, EErrorPanel1, ErrorUpdatePanel1);

            //al��rt �llapot�t nem lehet t�r�lni
            int alairtCount = 0;
            for (int i = 0; i < AlairokGridView.Rows.Count; i++)
            {
                string id = AlairokGridView.DataKeys[i].Value.ToString();

                if (deletableItemsList.Contains(id))
                {
                    GridViewRow row = AlairokGridView.Rows[i];
                    Label allapotLabel = row.FindControl("Allapot") as Label;

                    if (allapotLabel != null)
                    {
                        string allapot = allapotLabel.Text;

                        if (allapot == KodTarak.IRATALAIRAS_ALLAPOT.Alairt)
                        {
                            alairtCount++;
                        }
                    }
                }
            }

            if (alairtCount > 0)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.UIAlairtNemTorolheto);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
                for (int i = 0; i < deletableItemsList.Count; i++)
                {
                    execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                    execParams[i].Record_Id = deletableItemsList[i];
                }

                if (ParentForm == Constants.ParentForms.IraIrat)
                {
                    EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                    Result result = service.MultiInvalidate(execParams);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                    }
                }
            }

        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        }
    }

    private int GetMaxSorrendFromGridView()
    {
        int maxSorrendValue = -1;

        foreach (GridViewRow row in AlairokGridView.Rows)
        {
            Label label_sorrend = (Label)row.FindControl("Label_Sorrend");
            if (label_sorrend != null)
            {
                int labelValue;
                if (Int32.TryParse(label_sorrend.Text, out labelValue))
                {
                    if (labelValue > maxSorrendValue)
                    {
                        maxSorrendValue = labelValue;
                    }
                }
            }
        }

        return maxSorrendValue;
    }

    #endregion
    protected void AlairokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        #region CR3175 - kisherceg alkalmaz�sa
        Iratok.AlairokGridView_RowDataBound_CheckTemplatesRowKijeloles(e, Page, ParentId);
        #endregion CR3175 - kisherceg alkalmaz�sa
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void AlairokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        AlairokGridViewBind(e.SortExpression, UI.GetSortToGridView("AlairokGridView", ViewState, e.SortExpression));
    }


    protected void cbRekordHistory_CheckedChanged(object sender, EventArgs e)
    {
        this.AlairokHistoryPanel.Visible = this.cbRekordHistory.Checked;
    }

    #region BUG 6380
    //Ha m�r van kiadm�nyoz� al��r�, m�sik al��r� csak kisebb sorsz�mmal lehet
    private bool CheckKiadmanyozo(out string errormsg)
    {
        errormsg = "";
        try
        {
            if (ParentForm == Constants.ParentForms.IraIrat)
            {
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IratAlairokService service = null;
                EREC_IratAlairokSearch search = null;

                service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                search = (EREC_IratAlairokSearch)Search.GetSearchObject(Page, new EREC_IratAlairokSearch());

                search.Obj_Id.Value = ParentId;
                search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result res = service.GetAllWithExtension(ExecParam, search);
                if (res.IsError)
                {
                    errormsg = res.ErrorMessage;
                    return false;
                }
                DataSet ds = res.Ds;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string szerepNev = row["AlairoSzerep_Nev"].ToString();
                    string AlairoSzerep = row["AlairoSzerep"].ToString();
                    
                    if (AlairoSzerep == Contentum.eUtility.KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
                    {
                        if (!AlairoSzerep_KodtarakDropDownList.Enabled)
                        {
                            return true;
                        }
                        if ( AlairoSzerep_KodtarakDropDownList.SelectedValue == Contentum.eUtility.KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
                        {
                            errormsg = Resources.Error.NoMoreKiadmSignerAllowed;
                            return false;
                        }
                        else
                        {
                            string KiadmSorrend = row["AlairasSorrend"].ToString();
                            string AktualisSorrend = TextBoxSorszam.Text;
                            int KiadmSorrendInt, AktualisSorrendInt;
                            if (Int32.TryParse(KiadmSorrend, out KiadmSorrendInt) && Int32.TryParse(AktualisSorrend, out AktualisSorrendInt)) {
                                if (AktualisSorrendInt >= KiadmSorrendInt)
                                {
                                    errormsg = Resources.Error.NoMoreSignerAllowed;
                                    return false;
                                }
                            }
                        }
                    }
                }

                if (_allowGridViewFillWhenCheck) // BUG_6086: template felvitelnel emiatt felulirodott az AlairokGridView
                {
                    ui.GridViewFill(AlairokGridView, res, EErrorPanel1, ErrorUpdatePanel1);
                    UI.SetTabHeaderRowCountText(res, TabHeader);
                }
            }
        }catch(Exception ex)
        {
            errormsg = ex.Message;
            return false;
        }
        return true;
    }

    //bejelentkezett felhaszn�l� al��rhatja -e?
    public bool EnableSignForLoggedUser()
    {
        try
        {
            if (ParentForm == Constants.ParentForms.IraIrat)
            {
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IratAlairokService service = null;
                EREC_IratAlairokSearch search = null;

                service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                search = (EREC_IratAlairokSearch)Search.GetSearchObject(Page, new EREC_IratAlairokSearch());

                search.Obj_Id.Value = ParentId;
                search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                search.OrderBy = "AlairasSorrend, Alairo_Nev ASC";
                Result res = service.GetAllWithExtension(ExecParam, search);
                if (res.IsError)
                {
                    Logger.Error("IraIratokForm.EnableSignForLoggedUser() error (errorcode: " + res.ErrorCode + "):" + res.ErrorMessage);
                }
                else
                {
                    DataSet ds = res.Ds;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string AlairasDatuma = row["AlairasDatuma"].ToString();
                        //A soron k�vetkez� els� al��r�, aki m�g nem v�gezte el az al��r�st.
                        if (string.IsNullOrEmpty(AlairasDatuma))
                        {
                            string AlairoId = row["FelhasznaloCsoport_Id_Alairo"].ToString();
                            string HelyettesAlairoId = row["FelhaszCsoport_Id_Helyettesito"].ToString();
                            string LoggedUserId = ExecParam.Felhasznalo_Id;
                            if(!string.IsNullOrEmpty(AlairoId) && AlairoId.Equals(LoggedUserId, StringComparison.CurrentCultureIgnoreCase))
                            {
                                return true;
                            }
                            else if (!string.IsNullOrEmpty(HelyettesAlairoId) && HelyettesAlairoId.Equals(LoggedUserId, StringComparison.CurrentCultureIgnoreCase))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }

                    }
                }

            }
        }
        catch (Exception ex)
        {
            Logger.Error("IraIratokForm.EnableSignForLoggedUser() error:" + ex.Message);
        }
        return false;
    }
    #endregion

    public bool AlairoFelveheto(out string errormsg)
    {
        //felv�tel gomb megnyom�sakor nem n�zz�k
        return AlairoFelveheto("M_UTO", out errormsg);
    }

    public bool AlairoFelveheto(string alairasMod, out string errormsg)
    {
        return new Alairo(Page, ParentId).Felveheto(alairasMod, out errormsg);
    }


}
