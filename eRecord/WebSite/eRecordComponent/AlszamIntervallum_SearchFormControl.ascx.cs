using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;


public partial class Component_AlszamIntervallumControl : System.Web.UI.UserControl
{
    /// <summary>
    /// Handles the Init event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Enabled)
        {
            AlszamTol_masol_ImageButton.OnClientClick = "JavaScript: document.getElementById('" + AlszamIgTextBox.ClientID + "').value = " +
               " document.getElementById('" + AlszamTolTextBox.ClientID + "').value; " +
               "return false;";
        }

        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region public properties

    /// <summary>
    /// Gets or sets the erv kezd.
    /// </summary>
    /// <value>The erv kezd.</value>
    public string AlszamTol
    {
        get
        {
            return AlszamTol_TextBox.Text;
        }
        set
        {
            AlszamTol_TextBox.Text = value;
        }
    }

    /// <summary>
    /// Gets or sets the erv vege.
    /// </summary>
    /// <value>The erv vege.</value>
    public string AlszamIg
    {
        get
        {
            return AlszamIg_TextBox.Text;
        }
        set
        {
            AlszamIg_TextBox.Text = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv kezd].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv kezd]; otherwise, <c>false</c>.</value>
    public bool Enable_AlszamTol
    {
        set
        {
            AlszamTol_TextBox.Enabled = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_AlszamIg
    {
        set
        {
            AlszamIg_TextBox.Enabled = value;
        }
    }


    /// <summary>
    /// Gets the erv kezd_ text box.
    /// </summary>
    /// <value>The erv kezd_ text box.</value>
    public TextBox AlszamTol_TextBox
    {
        get { return AlszamTolTextBox; }
    }

    /// <summary>
    /// Gets the erv vege_ text box.
    /// </summary>
    /// <value>The erv vege_ text box.</value>
    public TextBox AlszamIg_TextBox
    {
        get { return AlszamIgTextBox; }
    }

    private Boolean _setDefaultAlszamTol;
    /// <summary>
    /// Kit�ltse-e az �rv�nyess�g kezdete mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv kezd]; otherwise, <c>false</c>.</value>
    public bool SetDefaultAlszamTol
    {
        set
        {
            _setDefaultAlszamTol = value;
        }

    }

    private Boolean _setDefaultAlszamIg;
    /// <summary>
    /// Kit�ltse-e az �rv�nyess�g v�ge mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv vege]; otherwise, <c>false</c>.</value>
    public bool SetDefaultAlszamIg
    {
        set
        {
            _setDefaultAlszamIg = value;
        }

    }

    /// <summary>
    /// Sets the default.
    /// </summary>
    public void SetDefault()
    {
        //if (_setDefaultAlszamTol)
        //    EVTol_TextBox.Text = DateTime.Now.Year.ToString();
        //if (_setDefaultEvIg)
        //    EvIg_TextBox.Text = DateTime.Now.Year.ToString();
    }

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_AlszamTol = _Enabled;
            Enable_AlszamIg = _Enabled;
        }
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;

            if (!_ViewEnabled)
            {
                Enabled = _ViewEnabled;
                AlszamTol_TextBox.CssClass = "ViewReadOnlyWebControl";

                AlszamIg_TextBox.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;

            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                AlszamTol_TextBox.CssClass = "ViewDisabledWebControl";

                AlszamIg_TextBox.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    /// <summary>
    /// Gets the component list.
    /// </summary>
    /// <returns></returns>
    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(AlszamTol_TextBox);
        componentList.Add(AlszamIg_TextBox);

        return componentList;
    }

    #endregion

}
