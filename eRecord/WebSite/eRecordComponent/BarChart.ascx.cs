using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class eRecordComponent_BarChart : System.Web.UI.UserControl
{
    public enum ChartTypes { Column, Bar };

    public ChartTypes ChartType = ChartTypes.Column;

    public string ChartTitle = "";
    public int ImageHeight = 450;
    public int ImageWidth = 700;
    public string ImageAlt = "BarChartImage";

    public int PanelPadding = 25;
    public int PanelPaddingTop = 25; // it can differ from padding because of the title
    public int PanelShadowDistance = 3;
    public int PanelShadowAlpha = 50;

    public int ChartPaddingTop = 50;
    public int ChartPaddingBottom = 20;
    public int ChartPaddingLeft = 140;
    public int ChartPaddingRight = 30;

    public Point LabelPosition = new Point(40, 40);

    public Brush BackgroundBrush = null;
    public Brush TitleBrush = Brushes.White;
    public Brush PanelBackgroundBrush = Brushes.White;

    //-- set display format
    public StringFormat TitleStringFormat = new StringFormat();
    public Font TitleFont = new Font("Verdana", 12, FontStyle.Bold);
    public Font LabelFont = new Font("Verdana", 8, FontStyle.Bold);

    public Color FromPanelBgColor = Color.FromArgb(156, 197, 250);
    public Color ToPanelBgColor = Color.FromArgb(10, 64, 135);
    public float GradientAngle = 0.0f;

    public int LabelDecimalPlaces = 2;       // how many decimal places should be shown in the labels
    public string LabelExtension = "";       // if value shown, extend value by this (e.g. "kg")

    public int BarWidthPercent = 90;   // proportion of the possible bar/column width

    //-- coloring by data points or by categories - only for bar charts
    public bool ColorByCategory = false;

    //-- set directory name for the charts
    public string ChartDirectoryName = "ChartImages";

    public Color CategoryBaseColor = Color.Black;

    public class ChartElement
    {
        public string Name;
        public double Value;
        public Color Color;
    }

    private ArrayList ChartElements;

    public string Src
    {
        get { return BarChartImage.ImageUrl; }
        set
        {
            BarChartImage.ImageUrl = value;
        }
    }

    //-- categories
    public class Category
    {
        public string Name;
        public Color Color;
        public double LowerBound;    // key for sorting
        public double UpperBound;
    }

    private ArrayList Categories;
    private double MinValue = 0.0;
    private double MaxValue = 0.0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // deleteOldPlotPage moved to the generateChartImage,
        // because of the problems rising when the chart image directory
        // cannot be resolved 
    }

    // delete images older than 10 minutes from BarChartImages folder
    private void deleteOldPlotImages()
    {
        //DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath("/" + ChartDirectoryName));
        DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath(".") + "/" + ChartDirectoryName);
        foreach (FileInfo myFileInfo in myDirectoryInfo.GetFiles())
        {
            if (myFileInfo.LastWriteTime.AddMinutes(10) < DateTime.Now)
            {
                myFileInfo.Delete();
            }
        }
    }

    // draw panel
    private void drawPanel(Graphics g)
    {
        // draw background
        Rectangle BackgroundRectangle = new Rectangle(0, 0, ImageWidth, ImageHeight);

        if (BackgroundBrush == null)
        {
            BackgroundBrush = new System.Drawing.Drawing2D.LinearGradientBrush(BackgroundRectangle, FromPanelBgColor, ToPanelBgColor, GradientAngle, false);
        }

        g.FillRectangle(BackgroundBrush, BackgroundRectangle);

        // draw title

        g.DrawString(ChartTitle, TitleFont, TitleBrush, new RectangleF(0, 0, ImageWidth, 20), TitleStringFormat);

        // draw main panel
        Rectangle panelRectangle = new Rectangle(PanelPadding, PanelPaddingTop, ImageWidth - 2 * PanelPadding, ImageHeight - PanelPaddingTop - PanelPadding);
        Rectangle panelShadowRectangle = new Rectangle(PanelPadding + PanelShadowDistance, PanelPaddingTop + PanelShadowDistance, (ImageWidth - 2 * PanelPadding) + PanelShadowDistance, (ImageHeight - PanelPaddingTop - PanelPadding) + PanelShadowDistance);
        SolidBrush transparentSolidBrush = new SolidBrush(Color.FromArgb(PanelShadowAlpha, 0, 0, 0));

        g.FillRectangle(transparentSolidBrush, panelShadowRectangle);
        g.FillRectangle(PanelBackgroundBrush, panelRectangle);
        g.DrawRectangle(Pens.Black, panelRectangle);
    }

    private Color getColorToCategory(double value)
    {   // look up for the color in the category list
        if (value < MinValue || value > MaxValue)
        {
            // value outside the defined categories
            return CategoryBaseColor;
        }
        // between MinValue and MaxValue
        int i = 0;
        while (i < Categories.Count-1 && value > ((Category)Categories[i]).UpperBound) 
        {
            i++;
        }
        return ((Category)Categories[i]).Color;
    }


    // draw columns(vertical)
    private void drawColumn(Graphics g)
    {
        Rectangle ChartRectangle = new Rectangle(PanelPadding + ChartPaddingLeft, PanelPaddingTop + ChartPaddingTop, ImageWidth - 1 - 2 * PanelPadding - ChartPaddingRight - ChartPaddingLeft, ImageHeight - 1 - PanelPaddingTop - PanelPadding - ChartPaddingBottom - ChartPaddingTop);
        float barWidth = (float)ChartRectangle.Width / (float)ChartElements.Count;
        double barHeightUnit = (double)ChartRectangle.Height / MaxValue;
        float hPos = 0.0f;

        foreach (ChartElement myChartElement in ChartElements)
        {
            Brush myBrush;

            if (ColorByCategory)
            {
                // get color by category
                myBrush = new SolidBrush(getColorToCategory(myChartElement.Value));
            }
            else
            {
                // normal color
                myBrush = new SolidBrush(myChartElement.Color);
            }

            if (myChartElement.Value.Equals(0.0)) {
                // draw only a base line
                g.DrawLine(Pens.Black, ChartRectangle.X + hPos, ChartRectangle.Y + ChartRectangle.Height, ChartRectangle.X + hPos + barWidth * BarWidthPercent / 100.0f, ChartRectangle.Y + ChartRectangle.Height);   
            }
            else
            {
                // draw filled rectangle according to value
                g.FillRectangle(myBrush, ChartRectangle.X + hPos, ChartRectangle.Y + ChartRectangle.Height - (float)(myChartElement.Value * barHeightUnit), barWidth * BarWidthPercent / 100.0f, (float)(myChartElement.Value * barHeightUnit));

                // draw border for bar
                g.DrawRectangle(Pens.Black, ChartRectangle.X + hPos, ChartRectangle.Y + ChartRectangle.Height - (float)(myChartElement.Value * barHeightUnit), barWidth * BarWidthPercent / 100.0f, (float)(myChartElement.Value * barHeightUnit));
            }
            // move position along the horizontal axis
            hPos = hPos + barWidth;
        }
    }

    // draw bars (horizontal)
    private void drawBar(Graphics g)
    {
        Rectangle ChartRectangle = new Rectangle(PanelPadding + ChartPaddingLeft, PanelPaddingTop + ChartPaddingTop, ImageWidth - 1 - 2 * PanelPadding - ChartPaddingRight - ChartPaddingLeft, ImageHeight - 1 - PanelPaddingTop - PanelPadding - ChartPaddingBottom - ChartPaddingTop);
        float barWidth = (float)ChartRectangle.Height / (float)ChartElements.Count;
        double barHeightUnit = MaxValue.Equals(0.0) ? 0.0 : (double)ChartRectangle.Width / MaxValue;
        float vPos = 0.0f;

        foreach (ChartElement myChartElement in ChartElements)
        {
            Brush myBrush;

            if (ColorByCategory)
            {
                // get color by category
                myBrush = new SolidBrush(getColorToCategory(myChartElement.Value));
            }
            else
            {
                // normal color
                myBrush = new SolidBrush(myChartElement.Color);
            }
            if (myChartElement.Value.Equals(0.0))
            {
                // draw only a base line
                g.DrawLine(Pens.Black, ChartRectangle.X, ChartRectangle.Y + vPos, ChartRectangle.X, ChartRectangle.Y + vPos + barWidth * BarWidthPercent / 100.0f);
            }
            else
            {
                // draw filled rectangle according to value
                g.FillRectangle(myBrush, ChartRectangle.X, ChartRectangle.Y + vPos, (float)(myChartElement.Value * barHeightUnit), barWidth * BarWidthPercent / 100.0f);

                // draw border for bar
                g.DrawRectangle(Pens.Black, ChartRectangle.X, ChartRectangle.Y + vPos, (float)(myChartElement.Value * barHeightUnit), barWidth * BarWidthPercent / 100.0f);
            }
            // move position along the vertical axis
            vPos = vPos + barWidth;
        }
    }

    private void drawLabels(Graphics g)
    {
        float x = (float)LabelPosition.X;
        float y = (float)LabelPosition.Y;

        if (ColorByCategory)
        {
            foreach (Category myCategory in Categories)
            {
                Brush labelBrush = new SolidBrush(myCategory.Color);
                g.FillRectangle(labelBrush, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
                g.DrawRectangle(Pens.Black, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
                string labelText;

                labelText = myCategory.Name;

                g.DrawString(labelText, LabelFont, Brushes.Black, x + 8, y);
                //y += LabelFont.Height + 5;
                y += 1.3f * LabelFont.Height;
            }

        }
        else // color the data point colors
        {
            foreach (ChartElement myChartElement in ChartElements)
            {
                Brush labelBrush = new SolidBrush(myChartElement.Color);
                g.FillRectangle(labelBrush, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
                g.DrawRectangle(Pens.Black, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
                string labelText;

                labelText = myChartElement.Name;
                g.DrawString(labelText, LabelFont, Brushes.Black, x + 8, y);
                //y += LabelFont.Height + 5;
                y += 1.3f * LabelFont.Height;
            }
        }

        switch (ChartType)
        {
            case ChartTypes.Bar:
                {
                    Rectangle ChartRectangle = new Rectangle(PanelPadding + ChartPaddingLeft, PanelPaddingTop + ChartPaddingTop, ImageWidth - 1 - 2 * PanelPadding - ChartPaddingRight - ChartPaddingLeft, ImageHeight - 1 - PanelPaddingTop - PanelPadding - ChartPaddingBottom - ChartPaddingTop);
                    float barWidth = (float)ChartRectangle.Height / (float)ChartElements.Count;
                    double barHeightUnit = MaxValue.Equals(0.0) ? 0.0 : (double)ChartRectangle.Width / MaxValue;
                    float vPos = barWidth / 2.0f;    // start at the middle of the first bar
                    foreach (ChartElement myChartElement in ChartElements)
                    {
                        string labelText;

                        labelText = myChartElement.Value.ToString(String.Format("N{0}", LabelDecimalPlaces.ToString())) + " " + LabelExtension;
                        g.DrawString(labelText, LabelFont, Brushes.Black, ChartRectangle.X + (float)(myChartElement.Value * barHeightUnit), ChartRectangle.Y + vPos - LabelFont.Height);
                        
                        if (ColorByCategory)
                        {
                            StringFormat stringFormat = new StringFormat();
                            stringFormat.Alignment = StringAlignment.Far;
                            // write element name left to the bar
                            g.DrawString(myChartElement.Name, LabelFont, Brushes.Black, ChartRectangle.X, ChartRectangle.Y + vPos - LabelFont.Height, stringFormat);
                        }

                        // move position along the vertical axis
                        vPos = vPos + barWidth;
                    }
                }
                break;
            case ChartTypes.Column:
            default:
                {
                    Rectangle ChartRectangle = new Rectangle(PanelPadding + ChartPaddingLeft, PanelPaddingTop + ChartPaddingTop, ImageWidth - 1 - 2 * PanelPadding - ChartPaddingRight - ChartPaddingLeft, ImageHeight - 1 - PanelPaddingTop - PanelPadding - ChartPaddingBottom - ChartPaddingTop);
                    float barWidth = (float)ChartRectangle.Width / (float)ChartElements.Count;
                    double barHeightUnit = MaxValue.Equals(0.0) ? 0.0 : (double)ChartRectangle.Height / MaxValue;
                    float hPos = barWidth / 2.0f;    // start at the middle of the first column
                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Center;
                    foreach (ChartElement myChartElement in ChartElements)
                    {
                        string labelText;

                        labelText = myChartElement.Value.ToString(String.Format("N{0}", LabelDecimalPlaces.ToString())) + " " + LabelExtension;
                        g.DrawString(labelText, LabelFont, Brushes.Black, ChartRectangle.X + hPos, ChartRectangle.Y + ChartRectangle.Height - (float)(myChartElement.Value * barHeightUnit) - 2 - LabelFont.Height, stringFormat);
                        if (ColorByCategory) { 
                            // write element name under the column
                            g.DrawString(myChartElement.Name, LabelFont, Brushes.Black, ChartRectangle.X + hPos, ChartRectangle.Y + ChartRectangle.Height, stringFormat);
                        }

                        // move position along the vertical axis
                        hPos = hPos + barWidth;
                    }
                }
                break;
        }
    }

    public void addChartElement(string name, double value, Color color)
    {
        if (this.ChartElements == null) this.ChartElements = new ArrayList();
        ChartElement newChartElement = new ChartElement();
        newChartElement.Name = name;
        newChartElement.Value = value;
        newChartElement.Color = color;
        this.ChartElements.Add(newChartElement);
        if (newChartElement.Value < MinValue)
            MinValue = newChartElement.Value;
        if (newChartElement.Value > MaxValue)
            MaxValue = newChartElement.Value;
    }


    public void addChartElement(ChartElement newChartElement)
    {
        if (this.ChartElements == null) this.ChartElements = new ArrayList();
        this.ChartElements.Add(newChartElement);
        if (newChartElement.Value < MinValue)
            MinValue = newChartElement.Value;
        if (newChartElement.Value > MaxValue)
            MaxValue = newChartElement.Value;
    }

    public void addCategory(Category newCategory)
    {
        if (this.Categories == null) this.Categories = new ArrayList();
        int i = 0;
        while (i < Categories.Count && ((Category)Categories[i]).LowerBound < newCategory.LowerBound)
        {
            i++;
        }

        this.Categories.Insert(i, newCategory);
    }

    public void generateChartImage()
    {
        generateChartImage(false);
    }

    public void generateChartImage(bool cleanUpChartDirectory)
    {
        if (cleanUpChartDirectory)
        {
            try
            {
                // possible Exception types
                // System.IO.IOException
                // System.Security.SecurityException
                // System.UnauthorizedAccessException
                deleteOldPlotImages();  // clean up in the directory
            }
            catch (Exception ex)
            {
                //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Delete", ex);
                Exception e = new Exception("Path: " + ChartDirectoryName + "<br />Operation: File Delete", ex);
                throw e;    // throw it with the additional message info
            }
        }


        Bitmap myBitmap = null;
        Graphics myGraphics = null;

        try
        {
            myBitmap = new Bitmap(ImageWidth, ImageHeight, PixelFormat.Format32bppArgb);

            long ticks = (long)(DateTime.Now.Ticks);
            string filename = "/" + ChartDirectoryName + "/" + ID + "_" + ticks.ToString() + ".png";
            
            BarChartImage.Width = ImageWidth;
            BarChartImage.Height = ImageHeight;
            BarChartImage.ImageUrl = filename;
            BarChartImage.AlternateText = ImageAlt;

            //FileInfo myFileInfo = new FileInfo(Server.MapPath(filename));
            FileInfo myFileInfo = new FileInfo(filename);
            if (myFileInfo.Exists) return;

            myGraphics = Graphics.FromImage(myBitmap);

            drawPanel(myGraphics);
            if (ChartElements != null)
            {
                switch (ChartType)
                {
                    case ChartTypes.Bar: drawBar(myGraphics); break;
                    case ChartTypes.Column: drawColumn(myGraphics); break;
                    default: drawColumn(myGraphics); break;

                }
                drawLabels(myGraphics);
            }

            /*
            MemoryStream memoryStream = new MemoryStream();
            myBitmap.Save(memoryStream, ImageFormat.Png);

            Response.Expires = 0;
            Response.ClearContent();
            Response.ContentType = "image/png";
            Response.BinaryWrite(memoryStream.ToArray());
            Response.End();
            */
            //myBitmap.Save(Server.MapPath(filename), ImageFormat.Png);
            myBitmap.Save(Server.MapPath(".") + filename, ImageFormat.Png);
        }
        catch (Exception ex) {

            // error handling
            //Response.Write(e.ToString());
            //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Save", ex);
            Exception e = new Exception("Path: " + Server.MapPath(".") + ChartDirectoryName + "<br />Operation: File Save", ex);
            throw e;    // throw it with the additional message info
        }
        finally {
            if (myGraphics != null) myGraphics.Dispose();
            if (myBitmap != null) myBitmap.Dispose();
        }
    }
}
