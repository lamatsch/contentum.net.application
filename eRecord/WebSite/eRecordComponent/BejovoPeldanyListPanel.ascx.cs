﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_BejovoPeldanyListPanel : System.Web.UI.UserControl
{
    public Contentum.eUIControls.eErrorPanel ErrorPanel { get; set; }

    public string Partner_Id_Cimzett
    {
        get
        {
            object o = ViewState["Partner_Id_Cimzett"];

            if (o == null)
                return String.Empty;

            return (string)o;
        }
        set
        {
            ViewState["Partner_Id_Cimzett"] = value;
        }
    }

    private string _ParentClonetTextboxClientId;

    public string ParentClonetTextboxClientId
    {
        get { return _ParentClonetTextboxClientId; }
        set { _ParentClonetTextboxClientId = value; }
    }

    private string _ParentCloneHiddenFieldClientId;

    public string ParentCloneHiddenFieldClientId
    {
        get { return _ParentCloneHiddenFieldClientId; }
        set { _ParentCloneHiddenFieldClientId = value; }
    }

    private List<int> PeldanyokPanelList
    {
        get
        {
            object o = ViewState["PeldanyokPanelList"];

            if (o != null)
            {
                return (List<int>)o;
            }

            return null;
        }
        set
        {
            ViewState["PeldanyokPanelList"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PeldanyokPanelList = new List<int>();
            PeldanyokPanelList.Add(1);
        }

        AddControls();

    
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(_fizikaiHelyValue) && MainPanel.Controls.Count > 0)
        {
            MainPanel.Controls.Cast<eRecordComponent_BejovoPeldanyPanel>().First().FizikaiHelyValue = _fizikaiHelyValue;
        }

        int count = MainPanel.Controls.Count;
        int i = 1;
        foreach (eRecordComponent_BejovoPeldanyPanel c in MainPanel.Controls.Cast<eRecordComponent_BejovoPeldanyPanel>())
        {
            if (count == 1)
            {
                c.RemoveButtonVisible = false;
            }
            else
            {
                c.RemoveButtonVisible = true;
            }

            if (i == count)
            {
                c.AddButtonVisible = true;
            }
            else
            {
                c.AddButtonVisible = false;
            }

            i++;

            c.DefaultPartner_Id_Cimzett = this.Partner_Id_Cimzett;
            c.ParentClonetTextboxClientId = this.ParentClonetTextboxClientId;
            c.ParentCloneHiddenFieldClientId = this.ParentCloneHiddenFieldClientId;
        }
    }

    private void BejovoPeldanyPanel_Add(object sender, EventArgs e)
    {
        int last = PeldanyokPanelList.Last();
        int i = last + 1;
        AddControl(i);
        PeldanyokPanelList.Add(i);
    }

    private void BejovoPeldanyPanel_Remove(object sender, EventArgs e)
    {
        Control c = sender as Control;

        if (c != null)
        {
            MainPanel.Controls.Remove(c);
            int i = GetIndexFromId(c.ID);
            PeldanyokPanelList.Remove(i);
        }
    }

    private void AddControls()
    {
        foreach (int i in PeldanyokPanelList)
        {
            AddControl(i);
        }
    }

    private void AddControl(int i)
    {
        var c = (eRecordComponent_BejovoPeldanyPanel)Page.LoadControl("~/eRecordComponent/BejovoPeldanyPanel.ascx");
        c.ID = GetIdFromIndex(i);
        MainPanel.Controls.Add(c);
        c.Add += BejovoPeldanyPanel_Add;
        c.Remove += BejovoPeldanyPanel_Remove;

        if (i > 1)
        {
            c.Sorszam = String.Empty;
        }

        if (i == 1)
        {
            c.DefaultEredet = Contentum.eUtility.KodTarak.IRAT_FAJTA.Eredeti;
        }

        c.DefaultPartner_Id_Cimzett = this.Partner_Id_Cimzett;
    }

    string GetIdFromIndex(int i)
    {
        return String.Format("BejovoPeldanyPanel_{0}", i);
    }

    int GetIndexFromId(string id)
    {
        return Int32.Parse(id.Substring(id.LastIndexOf('_') + 1));
    }

    public List<EREC_PldIratPeldanyok> GetBusinessObjectsFromComponents()
    {
        List<EREC_PldIratPeldanyok> peldanyokList = new List<EREC_PldIratPeldanyok>();
        foreach (eRecordComponent_BejovoPeldanyPanel c in MainPanel.Controls.Cast<eRecordComponent_BejovoPeldanyPanel>())
        {
            peldanyokList.Add(c.GetBusinessObjectFromComponents());
        }
        return peldanyokList;
    }

    private string _fizikaiHelyValue;

    public string FizikaiHelyValue
    {
        get
        {
            return MainPanel.Controls.Cast<eRecordComponent_BejovoPeldanyPanel>().First().FizikaiHelyValue;
        }
        set
        {
            _fizikaiHelyValue = value;
            if (!string.IsNullOrEmpty(value) && MainPanel.Controls.Count > 0)
            {
                MainPanel.Controls.Cast<eRecordComponent_BejovoPeldanyPanel>().First().FizikaiHelyValue = value;
            }
        }
    }

    public bool IsValid(out string error)
    {
        error = String.Empty;

        List<EREC_PldIratPeldanyok> peldanyok = GetBusinessObjectsFromComponents();

        if (peldanyok.Exists(p => p.Typed.Sorszam.IsNull))
        {
            error = "A példány sorszáma nem lehet üres!";
            return false;
        }

        List<int> sorszamok = peldanyok.Select(p => p.Typed.Sorszam.Value).ToList();

        if (sorszamok.Exists(i => i < 1))
        {
            error = "A példány sorszáma csak 0-nál nagyobb szám lehet!";
            return false;
        }

        if (sorszamok.Distinct().Count() != sorszamok.Count())
        {
            error = "A példányok sorszáma nem lehet azonos!";
            return false;
        }

        return true;
    }
}