﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BejovoPeldanyPanel.ascx.cs" Inherits="eRecordComponent_BejovoPeldanyPanel" %>
<%@ Register Src="~/Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>

<eUI:eFormPanel ID="IratPeldanyPanel" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tbody>
            <tr class="urlapSor_kicsi">
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text="Iratpéldány sorszáma:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <uc:RequiredNumberBox ID="Pld_Sorszam" CssClass="mrUrlapInput" runat="server" Text="1" />
                </td>
                <td class="mrUrlapCaption_short">
                    <div class="DisableWrap">
                        <asp:Label ID="Label53" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="Label17" runat="server" Text="Iratpéldány címzettje:"></asp:Label>
                    </div>
                </td>
                <td class="mrUrlapMezo">
                    <uc:CsoportTextBox id="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" withallcimcheckboxvisible="false"></uc:CsoportTextBox>
                    
                </td>
                <td style="width: 80px; text-align:right">
                     <asp:ImageButton runat="server" ID="AddButton" ImageUrl="~/images/hu/egyeb/add.png"  CausesValidation="false" ToolTip="Iratpéldány hozzáadása" OnClick="AddButton_Click"/>
                     <asp:ImageButton runat="server" ID="RemoveButton" ImageUrl="~/images/hu/egyeb/remove.png" CausesValidation="false" ToolTip="Iratpéldány törlése" OnClick="RemoveButton_Click"/>
                </td>
            </tr>
            <tr class="urlapSor_kicsi">
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label_IratPeldanyJellege" runat="server" Text="Iratpéldány jellege:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <uc:KodtarakDropDownList ID="Pld_KodtarakDropDownListIratPeldanyJellege" runat="server" ></uc:KodtarakDropDownList>
                </td>
                <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="label36" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                                            </td>
                <td></td>
            </tr>
        </tbody>
    </table>
</eUI:eFormPanel>