﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUtility;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;

public partial class eRecordComponent_BejovoPeldanyPanel : System.Web.UI.UserControl
{
    private const string kcs_IRAT_FAJTA = "IRAT_FAJTA";

    public Contentum.eUIControls.eErrorPanel ErrorPanel { get; set; }

    public event EventHandler Add;
    public event EventHandler Remove;

    private bool isTUKRendszer = false;

    public string Sorszam
    {
        get
        {
            return Pld_Sorszam.Text;
        }
        set
        {
            Pld_Sorszam.Text = value;
        }
    }

    public string Partner_Id_Cimzett
    {
        get
        {
            return Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
        }
        set
        {
            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = value;
        }
    }

    public string NevSTR_Cimzett
    {
        get
        {
            return Pld_PartnerId_Cimzett_PartnerTextBox.Text;
        }
        set
        {
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = value;
        }
    }

    private string _DefaultPartner_Id_Cimzett = String.Empty;

    public string DefaultPartner_Id_Cimzett
    {
        get
        {
            return _DefaultPartner_Id_Cimzett;
        }
        set
        {
            _DefaultPartner_Id_Cimzett = value;
        }
    }

    public void SetCimzettTextBoxById(string partner_Id_Cimzett)
    {
        Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = partner_Id_Cimzett;
        Pld_PartnerId_Cimzett_PartnerTextBox.SetCsoportTextBoxById(ErrorPanel);
    }

    public string Eredet
    {
        get
        {
            return Pld_KodtarakDropDownListIratPeldanyJellege.SelectedValue;
        }
        set
        {
            Pld_KodtarakDropDownListIratPeldanyJellege.SelectedValue = value;
        }
    }

    private string _DefaultEredet = String.Empty;

    public string DefaultEredet
    {
        get
        {
            return _DefaultEredet;
        }
        set
        {
            _DefaultEredet = value;
        }
    }

    public bool AddButtonVisible
    {
        get
        {
            return AddButton.Visible;
        }
        set
        {
            AddButton.Visible = value;
        }
    }

    public bool RemoveButtonVisible
    {
        get
        {
            return RemoveButton.Visible;
        }
        set
        {
            RemoveButton.Visible = value;
        }
    }

    private string _ParentClonetTextboxClientId;

    public string ParentClonetTextboxClientId
    {
        get { return _ParentClonetTextboxClientId; }
        set { _ParentClonetTextboxClientId = value; }
    }

    private string _ParentCloneHiddenFieldClientId;

    public string ParentCloneHiddenFieldClientId
    {
        get { return _ParentCloneHiddenFieldClientId; }
        set { _ParentCloneHiddenFieldClientId = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
    }

    public string FizikaiHelyValue
    {
        get
        {
            return IrattariHelyLevelekDropDownTUK.SelectedValue;
        }
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                IrattariHelyLevelekDropDownTUK.SetSelectedValue(value);
            }
        }
    }

    

        protected void Page_Load(object sender, EventArgs e)
    {

        Pld_KodtarakDropDownListIratPeldanyJellege.FillAndSetSelectedValue(kcs_IRAT_FAJTA, DefaultEredet, true, ErrorPanel);

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Validate = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
            //tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
            search_csoportok.Csoport_Id_Jogalany.Value = ExecParam.Felhasznalo_Id;
            search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

            Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

            if (!string.IsNullOrEmpty(result_csoportok.ErrorCode) || result_csoportok.Ds.Tables[0].Rows.Count < 1)
                return;

            List<string> Csoportok = new List<string>();

            foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
            {
                Csoportok.Add(row["Csoport_Id"].ToString());
            }

            EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
            search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
             "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
            var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
            IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, ErrorPanel);

        }


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        var val = IrattariHelyLevelekDropDownTUK.SelectedValue;

        if (String.IsNullOrEmpty(this.Partner_Id_Cimzett))
        {
            this.SetCimzettTextBoxById(this.DefaultPartner_Id_Cimzett);
        }

        Pld_PartnerId_Cimzett_PartnerTextBox.ParentClonetTextboxClientId = this.ParentClonetTextboxClientId;
        Pld_PartnerId_Cimzett_PartnerTextBox.ParentCloneHiddenFieldClientId = this.ParentCloneHiddenFieldClientId;
    }

    public EREC_PldIratPeldanyok GetBusinessObjectFromComponents()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        erec_PldIratPeldanyok.Sorszam = Pld_Sorszam.Text;
        erec_PldIratPeldanyok.Updated.Sorszam = true;

        erec_PldIratPeldanyok.Partner_Id_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
        erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

        erec_PldIratPeldanyok.NevSTR_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Text;
        erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

        erec_PldIratPeldanyok.Eredet = Pld_KodtarakDropDownListIratPeldanyJellege.SelectedValue;
        erec_PldIratPeldanyok.Updated.Eredet = true;

        if (isTUKRendszer)
        {
            erec_PldIratPeldanyok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_PldIratPeldanyok.Updated.IrattarId = true;
            erec_PldIratPeldanyok.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
            erec_PldIratPeldanyok.Updated.IrattariHely = true;
        }


        return erec_PldIratPeldanyok;
    }

    protected void AddButton_Click(object sender, ImageClickEventArgs e)
    {
        EventHandler handler = Add;

        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    protected void RemoveButton_Click(object sender, ImageClickEventArgs e)
    {
        EventHandler handler = Remove;

        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }
}