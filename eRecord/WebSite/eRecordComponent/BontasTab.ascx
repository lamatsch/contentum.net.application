<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BontasTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratBontasTab" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>


<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="BontasUpdatePanel" runat="server" OnLoad="BontasUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <ajaxToolkit:CollapsiblePanelExtender ID="BontasCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>                                                  
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		    <td>
				<%-- TabHeader --%>
                
                <uc1:TabHeader ID="TabHeader1" runat="server" />
							    
			    <%-- /TabHeader --%>
                        
                <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
				        <asp:GridView ID="BontasGridView"
				                      runat="server"
                                      CellPadding="0"
				                      CellSpacing="0"
				                      BorderWidth="1"
				                      GridLines="Both"
				                      AllowPaging="True"
				                      PagerSettings-Visible="false"
				                      AllowSorting="True"
				                      DataKeyNames="Id"
				                      AutoGenerateColumns="False" Width="98%"
				                      OnRowCommand="BontasGridView_RowCommand" OnRowDataBound="BontasGridView_RowDataBound" OnSorting="BontasGridView_Sorting"
				                      >
						    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						    <Columns>
							    <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
									    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
									    &nbsp;&nbsp;
									    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
								    </HeaderTemplate>
								    <ItemTemplate>
									    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" /> <%--'<%# Eval("Id") %>'--%>
								    </ItemTemplate>
							    </asp:TemplateField>
							    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							    </asp:CommandField>
									<asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezelő" SortExpression="Csoportok_FelelosNev.Nev">
										<HeaderStyle CssClass="GridViewBorderHeader" Width="550px" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="FullErkeztetoSzam" HeaderText="Érkeztetési azonosító" SortExpression="EREC_KuldKuldemenyek.Erkezteto_Szam">
										<HeaderStyle CssClass="GridViewBorderHeader" Width="250px" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
						    </Columns>
				                      
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>                            
        
			        <eUI:eFormPanel ID="EFormPanel1" runat="server">
					    <table cellspacing="0" cellpadding="0" width="100%">
					        <tr class="urlapSor">
						        <td style="text-align: right; vertical-align: middle;">
						            <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
							        <asp:Label ID="Label2" runat="server" Text="Kezelő:"></asp:Label>
						        </td>
						        <td style="text-align: center; vertical-align: middle;">
							        <%-- asp:TextBox ID="FelelosNeve" runat="server" Width="550"/>
							        <asp:ImageButton ID="SelectingRowsImageButton"
											         runat="server"
											         ImageUrl="~/images/hu/lov/bonto copy.jpg"
											         AlternateText="Felelős kiválasztása"  / --%>
                                    <uc3:csoporttextbox id="CsoportTextBox1" runat="server" Validate="true"></uc3:csoporttextbox>
                                    &nbsp;</td>
					        </tr>
					    </table>
					    <%-- TabFooter --%>
    
                        <uc2:TabFooter ID="TabFooter1" runat="server" />
						
					    <%-- /TabFooter --%>
			    </eUI:eFormPanel>    			
		    </td>
	    </tr>
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
