﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratBontasTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        //        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList"); // TODO: BontasokList
        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        TabFooter1.SaveValidationGroup = "Bontas";
        // TODO:
        //UploadBont.ValidationGroup = TabFooter1.SaveValidationGroup;
        //BontMegjegyzes.ValidationGroup = TabFooter1.SaveValidationGroup;
        //CsatlomanyVerzio.ValidationGroup = TabFooter1.SaveValidationGroup;

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.New);
        TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.View);

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(BontasGridView));
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(BontasGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void BontasUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
        TabHeader1.InvalidateVisible = false;
        TabHeader1.ModifyVisible = false;
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;

        TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ClearForm();
        EFormPanel1.Visible = false;
        BontasGridViewBind();
        pageView.SetViewOnPage(Command);
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Bontas)) // TODO: Bontas
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            //LZS - BUG_481
                            EREC_KuldKuldemenyek erec_kuldemenyek = GetBusinessObjectFromComponents();
                            
                            //A megbontott küldemény Érkeztetőazonosítójával tér vissza a Kuldemenyek.Bontas() 
                            string erkeztetoszam_bontasResult = Kuldemenyek.Bontas(erec_kuldemenyek, Page, EErrorPanel1);

                            if (!String.IsNullOrEmpty(erkeztetoszam_bontasResult))
                            {
                                //Figyelmeztető üzenet
                                string warningMsg = string.Format(Resources.Form.MegbontasWarningMessage, erkeztetoszam_bontasResult);
                                string script = string.Format("alert('{0}');", warningMsg);

                                ScriptManager.RegisterStartupScript(TabFooter1.ImageButton_Save, this.GetType(), "sendPostBack", script, true);

                                ReLoadTab();
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private void ClearForm()
    {
        CsoportTextBox1.Id_HiddenField = "";
        CsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
    }

    // form --> business object
    private EREC_KuldKuldemenyek GetBusinessObjectFromComponents()
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        erec_KuldKuldemenyek.KuldKuldemeny_Id_Szulo = ParentId;
        erec_KuldKuldemenyek.Updated.KuldKuldemeny_Id_Szulo = true;

        erec_KuldKuldemenyek.Csoport_Id_Felelos = CsoportTextBox1.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

        return erec_KuldKuldemenyek;
        //return null;
    }


    #endregion

    #region List
    protected void BontasGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("BontasGridView", ViewState, "EREC_KuldKuldemenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("BontasGridView", ViewState);
        BontasGridViewBind(sortExpression, sortDirection);
    }

    protected void BontasGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (ParentForm.Equals("Kuldemeny"))
        {
            if (!String.IsNullOrEmpty(ParentId))
            {
                UI.ClearGridViewRowSelection(BontasGridView);
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_KuldKuldemenyekService service = null;
                EREC_KuldKuldemenyekSearch search = null;

                service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                //search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject(Page, new EREC_KuldKuldemenyekSearch());
                search = new EREC_KuldKuldemenyekSearch();
                search.KuldKuldemeny_Id_Szulo.Value = ParentId;
                search.KuldKuldemeny_Id_Szulo.Operator = Contentum.eQuery.Query.Operators.equals;

                search.OrderBy = Search.GetOrderBy("BontasGridView", ViewState, SortExpression, SortDirection);
                Result res = service.GetAllWithExtension(ExecParam, search);
                ui.GridViewFill(BontasGridView, res, EErrorPanel1, ErrorUpdatePanel1);
                UI.SetTabHeaderRowCountText(res, TabHeader);
            }
        }
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, BontasUpdatePanel.ClientID);
        }
    }

    protected void BontasGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(BontasGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    #endregion
    protected void BontasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void BontasGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        BontasGridViewBind(e.SortExpression, UI.GetSortToGridView("BontasGridView", ViewState, e.SortExpression));
    }
}
