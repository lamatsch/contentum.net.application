<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CsatolasokTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratCsatolasokTab" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="CsatolasokUpdatePanel" runat="server" OnLoad="CsatolasokUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <ajaxToolkit:CollapsiblePanelExtender ID="CsatolasokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>                                                  
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		    <td>
				<%-- TabHeader --%>
                
                <uc1:TabHeader ID="TabHeader1" runat="server" />
							    
			    <%-- /TabHeader --%>
                        
                <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
				        <asp:GridView ID="CsatolasokGridView"
				                      runat="server"
                                      CellPadding="0"
				                      CellSpacing="0"
				                      BorderWidth="1"
				                      GridLines="Both"
				                      AllowPaging="True"
				                      PagerSettings-Visible="false"
				                      AllowSorting="True"
				                      DataKeyNames="Id"
				                      AutoGenerateColumns="False" Width="98%"
				                      OnRowCommand="CsatolasokGridView_RowCommand" OnRowDataBound="CsatolasokGridView_RowDataBound" OnSorting="CsatolasokGridView_Sorting"
				                      >
						    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						    <Columns>
							    <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
									    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
									    &nbsp;&nbsp;
									    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
								    </HeaderTemplate>
								    <ItemTemplate>
									    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" /> <%--'<%# Eval("Id") %>'--%>
								    </ItemTemplate>
							    </asp:TemplateField>
							    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							    </asp:CommandField>
							    <asp:BoundField DataField="KapcsolatTipusNev" HeaderText="Kapcsolat t�pus" 
                                    SortExpression="KapcsolatTipusNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemTemplate>
                                        <asp:Label ID="labelObjId" runat="server" Text='<%#Eval("Obj_Id_Elozmeny")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Obj_Elozmeny_Azonosito" HeaderText="Azonos�t�" 
                                    SortExpression="Obj_Elozmeny_Azonosito">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemTemplate>
                                        <asp:Label ID="labelObjIdKapcsolt" runat="server" Text='<%#Eval("Obj_Id_Kapcsolt")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Obj_Kapcsolt_Azonosito" HeaderText="Azonos�t�" 
                                    SortExpression="Obj_Kapcsolt_Azonosito">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>
                                        <asp:Label ID="headerOksag" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="labelOksag" runat="server" Text='<%#((Eval("Oksag") as string) == "K" ? "K�vetkezm�ny" : ((Eval("Oksag") as string) == "E" ? "El�zm�ny" : "")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="headerTipus" runat="server" CommandName="Sort" CommandArgument="Obj_Type_Elozmeny">Objektum t�pus</asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="labelTipus" runat="server" Text='<%#Eval("Obj_Type_Elozmeny")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="headerTipusKapcsolt" runat="server" CommandName="Sort" CommandArgument="Obj_Type_Kapcsolt">Objektum t�pus</asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="labelTipusKapcsolt" runat="server" Text='<%#Eval("Obj_Type_Kapcsolt")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Kapcsolat l�trehoz�sa" SortExpression="LetrehozasIdo">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
						    </Columns>
				                      
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>                            &nbsp;
		    </td>
	    </tr>
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
