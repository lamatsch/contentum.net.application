using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_UgyiratCsatolasokTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);
        //TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolasNew");
            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyView");
            TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolasInvalidate");
        }
        else
        {
            TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasNew");
            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratView");
            TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasInvalidate");
        }

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(CsatolasokGridView));
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(CsatolasokGridView);
    }

    protected void CsatolasokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshCsatolasokList:
                    CsatolasokGridViewBind();                    
                    break;
            }
        }
    }

    public void ReLoadTab()
    {
        if (ParentForm != Constants.ParentForms.Ugyirat
            && ParentForm != Constants.ParentForms.IraIrat
            && ParentForm != Constants.ParentForms.IratPeldany
            && ParentForm != Constants.ParentForms.Kuldemeny)
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
        else
        {

            if (!MainPanel.Visible) return;

            // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        

            TabHeader1.ViewVisible = true;
            TabHeader1.ModifyVisible = false;            
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;

            this.SetVisibleColumns();


            string startup = String.Empty;

            switch(ParentForm)
            {
                case Constants.ParentForms.Ugyirat:
                    startup = Constants.Startup.FromUgyirat;
                    break;
                case Constants.ParentForms.IraIrat:
                    startup = Constants.Startup.FromIrat;
                    break;
                case Constants.ParentForms.IratPeldany:
                    startup = Constants.ParentForms.IratPeldany;
                    break;
                case Constants.ParentForms.Kuldemeny:
                    startup = Constants.Startup.FromKuldemeny;
                    break;
                default:
                    break;
            }

            TabHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx"
            , QueryStringVars.Id + "=" + ParentId + "&" + QueryStringVars.Startup + "=" + startup
            , Defaults.PopupWidth_Max, Defaults.PopupHeight, CsatolasokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolasokList);

            TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsatolasokGridView.ClientID);
            //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            CsatolasokGridViewBind();
            pageView.SetViewOnPage(Command);
        }
    }

    private void SetVisibleColumns()
    {
        switch (ParentForm)
        {
            case Constants.ParentForms.Ugyirat:
                //kapcsolt id
                CsatolasokGridView.Columns[5].Visible = false;
                //kapcsolt azonos�t�
                CsatolasokGridView.Columns[6].Visible = false;
                //kapcsolt t�pus
                CsatolasokGridView.Columns[9].Visible = false;
                break;
            case Constants.ParentForms.IraIrat:
                //kapcsolt id
                CsatolasokGridView.Columns[5].Visible = false;
                //kapcsolt azonos�t�
                CsatolasokGridView.Columns[6].Visible = false;
                //Oks�g
                CsatolasokGridView.Columns[7].Visible = true;
                //kapcsolt t�pus
                CsatolasokGridView.Columns[9].Visible = false;
                break;
            case Constants.ParentForms.IratPeldany:
                //el�zm�ny id
                CsatolasokGridView.Columns[3].Visible = false;
                //el�zm�ny azonos�t�
                CsatolasokGridView.Columns[4].Visible = false;
                //el�zm�ny t�pus
                CsatolasokGridView.Columns[8].Visible = false;
                break;
            case Constants.ParentForms.Kuldemeny:
                //kapcsolt id
                CsatolasokGridView.Columns[5].Visible = false;
                //kapcsolt azonos�t�
                CsatolasokGridView.Columns[6].Visible = false;
                //Oks�g
                CsatolasokGridView.Columns[7].Visible = true;
                //kapcsolt t�pus
                CsatolasokGridView.Columns[9].Visible = false;
                break;
            default:
                break;
        }
    }

    #endregion

    #region List
    protected void CsatolasokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolasokGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolasokGridView", ViewState);
        CsatolasokGridViewBind(sortExpression, sortDirection);
    }

    protected void CsatolasokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(CsatolasokGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyiratObjKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
        EREC_UgyiratObjKapcsolatokSearch search = new EREC_UgyiratObjKapcsolatokSearch();

        //Regi szerelt adatokat nem itt hozzuk, hanme az �gyiratt�rk�pen
        search.KapcsolatTipus.Value = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles;
        search.KapcsolatTipus.Operator = Query.Operators.notequals;

        if (!String.IsNullOrEmpty(ParentId))
        {
            switch (ParentForm)
            {
                case Constants.ParentForms.Ugyirat:
                    search.Obj_Id_Elozmeny.Value = ParentId;
                    search.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
                    search.Obj_Id_Elozmeny.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Elozmeny.Group = "69";
                    //--------------------------------------------------------
                    search.Obj_Id_Kapcsolt.Value = ParentId;
                    search.Obj_Id_Kapcsolt.Operator = Query.Operators.equals;
                    search.Obj_Id_Kapcsolt.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Kapcsolt.Group = "69";
                    break;
                case Constants.ParentForms.IraIrat:
                    search.Obj_Id_Elozmeny.Value = ParentId;
                    search.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
                    search.Obj_Id_Elozmeny.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Elozmeny.Group = "69";
                    //--------------------------------------------------------
                    search.Obj_Id_Kapcsolt.Value = ParentId;
                    search.Obj_Id_Kapcsolt.Operator = Query.Operators.equals;
                    search.Obj_Id_Kapcsolt.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Kapcsolt.Group = "69";
                    break;
                case Constants.ParentForms.IratPeldany:
                    search.Obj_Id_Elozmeny.Value = ParentId;
                    search.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
                    break;
                case Constants.ParentForms.Kuldemeny:
                    search.Obj_Id_Elozmeny.Value = ParentId;
                    search.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
                    search.Obj_Id_Elozmeny.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Elozmeny.Group = "69";
                    //--------------------------------------------------------
                    search.Obj_Id_Kapcsolt.Value = ParentId;
                    search.Obj_Id_Kapcsolt.Operator = Query.Operators.equals;
                    search.Obj_Id_Kapcsolt.GroupOperator = Query.Operators.or;
                    search.Obj_Id_Kapcsolt.Group = "69";
                    break;
                default:
                    break;
            }
        }
        else
        {
            return;
        }

        search.OrderBy = Search.GetOrderBy("CsatolasokGridView", ViewState, SortExpression, SortDirection);
        Result res = service.GetAllWithExtension(ExecParam, search);
        this.ModifyResultSet(res);
        ui.GridViewFill(CsatolasokGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);
    }

    private void ModifyResultSet(Result res)
    {
        if (!res.IsError)
        {
            res.Ds.Tables[0].Columns.Add("Oksag", typeof(string));

            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                //res.Ds.Tables[0].Columns["Oksag"].Expression = String.Format("iif(Obj_Id_Kapcsolt<>'{0}', 'K', 'E')", ParentId);
                if (ParentForm == Constants.ParentForms.IraIrat
                    || ParentForm == Constants.ParentForms.Kuldemeny
                    || ParentForm == Constants.ParentForms.Ugyirat)
                {
                    List<int> deleteList = new List<int>();
                    foreach (DataRow row in res.Ds.Tables[0].Rows)
                    {
                        if (row["Obj_Id_Elozmeny"].ToString().ToLower() == ParentId.ToLower())
                        {
                            row["Obj_Id_Elozmeny"] = row["Obj_Id_Kapcsolt"];
                            row["Obj_Elozmeny_Azonosito"] = row["Obj_Kapcsolt_Azonosito"];
                            row["Obj_Type_Elozmeny"] = row["Obj_Type_Kapcsolt"];
                            //row["Obj_Type_Kapcsolt"] = "K";
                            row["Oksag"] = "K";
                        }
                        else
                        {
                            //row["Obj_Type_Kapcsolt"] = "E";
                            row["Oksag"] = "E";
                        }
                    }
                    DataTable _tempDT = RemoveDuplicateRows(res.Ds.Tables[0], "Obj_Id_Elozmeny");
                    res.Ds.Tables.Remove(res.Ds.Tables[0]);
                    res.Ds.Tables.Add(_tempDT);
                }
            }
        }
    }

    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();

        //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
        //And add duplicate item value in arraylist.
        foreach (DataRow drow in dTable.Rows)
        {
            if (hTable.Contains(drow[colName]))
                duplicateList.Add(drow);
            else
                hTable.Add(drow[colName], string.Empty);
        }

        //Removing a list of duplicate items from datatable.
        foreach (DataRow dRow in duplicateList)
            dTable.Rows.Remove(dRow);

        //Datatable which contains unique records will be return as output.
        return dTable;
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //a megfekek� form bek�t�se
            string url = String.Empty;
            string objId = String.Empty;
            GridViewRow row = CsatolasokGridView.SelectedRow;
            string labelTipusId = "labelTipus";
            string labelObjIdId = "labelObjId";
            if (row != null)
            {
                switch (ParentForm)
                {
                    case Constants.ParentForms.Ugyirat:
                        labelTipusId = "labelTipus";
                        labelObjIdId = "labelObjId";
                        break;
                    case Constants.ParentForms.IraIrat:
                        {
                            Label labelObjIdCheck = (Label)row.FindControl(labelObjIdId);
                            if (labelObjIdCheck != null && labelObjIdCheck.Text.Equals(ParentId, StringComparison.InvariantCultureIgnoreCase))
                            {
                                labelTipusId = "labelTipusKapcsolt";
                                labelObjIdId = "labelObjIdKapcsolt";
                            }
                            else
                            {
                                labelTipusId = "labelTipus";
                                labelObjIdId = "labelObjId";
                            }
                        }
                        break;
                    case Constants.ParentForms.IratPeldany:
                        labelTipusId = "labelTipusKapcsolt";
                        labelObjIdId = "labelObjIdKapcsolt";
                        break;
                    case Constants.ParentForms.Kuldemeny:
                        {
                            Label labelObjIdCheck = (Label)row.FindControl(labelObjIdId);
                            if (labelObjIdCheck != null && labelObjIdCheck.Text.Equals(ParentId, StringComparison.InvariantCultureIgnoreCase))
                            {
                                labelTipusId = "labelTipusKapcsolt";
                                labelObjIdId = "labelObjIdKapcsolt";
                            }
                            else
                            {
                                labelTipusId = "labelTipus";
                                labelObjIdId = "labelObjId";
                            }
                        }
                        break;
                    default:
                        break;
                }
                Label labelTipus = (Label)row.FindControl(labelTipusId);
                Label labelObjId = (Label)row.FindControl(labelObjIdId);
                if (labelTipus != null && labelTipus.Text.Trim() != String.Empty && labelObjId != null && labelObjId.Text.Trim() != String.Empty)
                {
                    switch (labelTipus.Text.Trim())
                    {
                        case Constants.ObjectNames.irat:
                            url = "IraIratokForm.aspx";
                            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
                            break;
                        case Constants.ObjectNames.iratpeldany:
                            url = "PldIratPeldanyokForm.aspx";
                            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View);
                            break;
                        case Constants.ObjectNames.ugyirat:
                            url = "UgyUgyiratokForm.aspx";
                            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
                            break;
                        case Constants.ObjectNames.kuldemeny:
                            url = "KuldKuldemenyekForm.aspx";
                            TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.View);
                            break;
                        default:
                            //jogosults�g ellen�rz�s
                            TabHeader1.ViewEnabled = false;
                            break;

                    }

                    objId = labelObjId.Text;
                }
                else
                {
                    //jogosults�g ellen�rz�s
                    TabHeader1.ViewEnabled = false;
                }
            }

            if (!String.IsNullOrEmpty(url))
            {
                TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick(url
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + objId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, CsatolasokUpdatePanel.ClientID);
            }
        }
    }

    protected void CsatolasokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolasokGridView, selectedRowNumber, "check");

        }
    }

    /// <summary>
    /// T�rli a CsatolasokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedCsatolasok()
    {
        string FunkcioKod = null;
        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            FunkcioKod = "KuldemenyCsatolasInvalidate";
        }
        else
        {
            FunkcioKod = "UgyiratCsatolasInvalidate";
        }


        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod))
        //if (FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsatolasokGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_UgyiratObjKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        }
    }


    #endregion
    protected void CsatolasokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null)
            {
                labelTipus.Text = UI.GetObjectNameByType(labelTipus.Text);
            }

            Label labelTipusKapcsolt = (Label)e.Row.FindControl("labelTipusKapcsolt");
            if (labelTipusKapcsolt != null)
            {
                //if (ParentForm == Constants.ParentForms.IraIrat || ParentForm == Constants.ParentForms.Kuldemeny)
                //{
                //    if (labelTipusKapcsolt.Text.ToString() == "E")
                //    {
                //        Label labelOksag = (Label)e.Row.FindControl("labelOksag");
                //        labelOksag.Text = "El�zm�ny";
                //    }

                //    if (labelTipusKapcsolt.Text.ToString() == "K")
                //    {
                //        Label labelOksag = (Label)e.Row.FindControl("labelOksag");
                //        labelOksag.Text = "K�vetkezm�ny";
                //    }
                //}

                labelTipusKapcsolt.Text = UI.GetObjectNameByType(labelTipusKapcsolt.Text);
            }

            
        }
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void CsatolasokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolasokGridViewBind(e.SortExpression, UI.GetSortToGridView("CsatolasokGridView", ViewState, e.SortExpression));

    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedCsatolasok();
            CsatolasokGridViewBind();
        }

    }
}
