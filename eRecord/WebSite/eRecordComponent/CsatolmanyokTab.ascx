﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CsatolmanyokTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratCsatolmanyokTab" %>
<%@ Register Src="FileUploadComponent.ascx" TagName="FileUploadComponent" TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc4" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="DokumentumHierarchia.ascx" TagName="DokumentumHierarchia" TagPrefix="uc6" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/MicroSigner.ascx" TagName="MicroSigner" TagPrefix="uc13" %>
<%@ Register Src="~/Component/DownloadDocumentAsZip.ascx" TagName="DownloadDocumentAsZip" TagPrefix="uc3" %>
<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="processPanel" runat="server" DisplayAfter="30">
    <ProgressTemplate>
        <div class="updateProgress" id="UpdateProgressPanelA" runat="server">
            <eUI:eFormPanel ID="EFormPanelProgress" runat="server">
                <table>
                    <tr>
                        <td id="imageTD1">
                            <div id="IMGDIV1">
                                <%--<img src="images/hu/egyeb/activity_indicator.gif" alt="" />--%>
                                <asp:Image runat="server" ImageUrl="~/images/hu/egyeb/activity_indicator.gif" />
                            </div>
                        </td>
                        <td class="updateProgressText">
                            <span id="progressLabel">Feldolgozás folyamatban...</span>
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
        </div>
        <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtenderA" runat="server" TargetControlID="processPanel"
            VerticalSide="Middle" HorizontalSide="Center">
        </ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:HiddenField ID="halkProcessId" runat="server" Value="" />
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        //$('#IMGDIV1').remove();
        $('#<%=processPanel.ClientID %>').hide();
    });
    var HALKTimer;
    var lastStatusGetTime;
    function checkStatus() {
        $('#<%=processPanel.ClientID %>').show();
        //$("#dataTable").find("input,button,textarea,select").attr("disabled", "disabled");
        HALKTimer = setInterval(checkStatusFromBG, 1000);
        //$('#imageTD1').append('<div id=\"IMGDIV1\"><img src=\"images/hu/egyeb/activity_indicator.gif\" /></div>');
    }

    function checkStatusFromBG() {
        var bgProcessId = $('#<%=halkProcessId.ClientID %>').val();
        if (bgProcessId) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: 'DokumentumAlairas.aspx/GetHALKStatusz',
                data: '{"processId":"' + bgProcessId + '", "lastStatusGetTime":"' + lastStatusGetTime + '"}',
                success:
                    function (response) {
                        processResult(response.d);
                    }
                , error: function (request, status, error) {
                    disableTimer();
                }
            });
        }
    }

    function processResult(statusResult) {
        if (statusResult) {
            $('#progressLabel').html(statusResult.BackgroundProcessStatus);
            lastStatusGetTime = statusResult.StatusGetTime;
            if (statusResult.BackgroundProcessEnded) {
                disableTimer();
            }
        }
    };

    function disableTimer() {
        $('#<%=processPanel.ClientID %>').hide();
        //$('#IMGDIV1').remove();
        //$("#dataTable").find("input,button,textarea,select").removeAttr("disabled");
        clearInterval(HALKTimer);

        window.returnValue = true;
        if (window.opener && window.returnValue && window.opener.__doPostBack) {
            window.opener.__doPostBack('', window.opener.postBackArgument);
        }
        window.close();
    }
</script>
<%--/Hiba megjelenites--%>
<uc7:CustomUpdateProgress ID="CustomUpdateProgress_FileUpload" runat="server" />
<asp:UpdatePanel ID="TabStatusUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hf_IsThisTabActive" runat="server" Value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="CsatolmanyokUpdatePanel" runat="server" OnLoad="CsatolmanyokUpdatePanel_Load">
    <ContentTemplate>
        <script type="text/javascript">            

            function downLoadDocument(gridId, documentId) {
                var tomorites = document.getElementById("vCSATOLMANY_TOMEGES_LETOLTES_TOMORITES").value;
                var count = getSelectedCheckBoxesCount(gridId, 'check');
                var docId = getDocumentIdsBySelectedCheckboxes(gridId, 'check');

                var isInSelected = false;
                if (arrContains(docId, documentId))
                    isInSelected = true;

                if (!documentId || documentId == "") {
                    if (count < 1) {
                        alert('Nincsen kijelölve tétel!');
                        return;
                    }
                    else if (count == 1) {
                        documentId = docId[0];
                    }
                }

                if (count < 2) {
                    window.open('GetDocumentContent.aspx?id=' + documentId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
                    return;
                }

                if (!isInSelected && documentId != "") {
                    docId[docId.length] = documentId;
                }

                if (tomorites == "2") {
                    var t = confirm("Szeretné tömöríteni a kiválasztott állományokat?");
                    if (t) {
                        tomorites = "1";
                    }
                    else {
                        tomorites = "0";
                    }
                }

                if (tomorites == "" || tomorites == "0") {
                    for (var i = 0; i < docId.length; i++) {
                        window.open('GetDocumentContent.aspx?id=' + docId[i], "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
                    }
                }
                else if (tomorites == "1") {
                    var strIDs = "";

                    for (var j = 0; j < docId.length; j++) {
                        strIDs = strIDs + docId[j];
                        if (j < docId.length - 1) {
                            strIDs = strIDs + "_";
                        }
                    }

                    var name = $("input[id*='iktatoszam_HiddenField']")[0].value;

                    downloadDocumentsAsZip(strIDs, name);
                    //window.open('GetDocumentContent.aspx?id=' + strIDs + '&mode=zip&Nev=' + name, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
                }
            }

            function arrContains(a, obj) {
                for (var i = 0; i < a.length; i++) {
                    if (a[i] === obj) {
                        return true;
                    }
                }
                return false;
            }

            function getDocumentIdsBySelectedCheckboxes(idPrefix, checkBoxID) {

                var strIds = [];
                var count = 0;

                var prefixLength = idPrefix.length;

                var x = document.getElementsByTagName("input");

                var i = 0;
                for (i = 0; i < x.length; i++) {
                    if (x[i].type == "checkbox"
                        && x[i].id.substr(0, prefixLength) == idPrefix && x[i].id.indexOf(checkBoxID) != -1
                    ) {
                        // Űkpapi elem:
                        if (x[i].checked == true && x[i].parentElement != null && x[i].parentElement.parentElement != null && x[i].parentElement.parentElement.parentElement != null) {
                            var elem = x[i].parentElement.parentElement.parentElement;
                            if (elem.attributes.getNamedItem('downloadDocumentId') != null) {
                                strIds[count] = elem.attributes.getNamedItem('downloadDocumentId').value;
                                count++;
                            }
                        }
                    }
                }

                return strIds;

            }

        </script>
        <asp:HiddenField ID="iktatoszam_HiddenField" runat="server" />
       <asp:HiddenField ID="hf_ConfirmLastDokumentDelete" runat="server" Value="" />
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <uc:DokumentumVizualizerComponent ID="DokumentumVizualizerComponent" runat="server" />
            <ajaxToolkit:CollapsiblePanelExtender ID="CsatolmanyokCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" AutoCollapse="false" AutoExpand="false"
                ExpandedSize="200" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="Record_Ver_HiddenField_KRT_Dokumentum" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <asp:Panel ID="KimenoCsatolmanyokPanel" runat="server" Style="padding-bottom: 10px">
                            <div style="padding-bottom: 5px;">
                                <asp:Label ID="KimenoCsatolmanyokHeader" runat="server" Text="Kimenõ küldemény csatolmányai:" Font-Underline="true" Font-Bold="true" />
                            </div>
                            <div>
                                   <asp:GridView ID="KimenoCsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                    AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                                    OnRowCommand="KimenoCsatolmanyokGridView_RowCommand" OnRowDataBound="KimenoCsatolmanyokGridView_RowDataBound"
                                    OnSorting="KimenoCsatolmanyokGridView_Sorting">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="IktatoSzam_Merge">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FajlNev" HeaderText="Állomány" SortExpression="FajlNev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="" SortExpression="KRT_Dokumentumok.Formatum">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="0px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                                    ImageUrl="images/hu/fileicons/default.gif" ToolTip="Megnyitás" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Leiras" HeaderText="Megjegyzés" SortExpression="Leiras">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Verzió" SortExpression="KRT_Dokumentumok.VerzioJel">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="linkVerzio" runat="server" NavigateUrl='<%# Contentum.eRecord.Utility.Dokumentumok.GetVerziokLink(Eval("krtDok_Id"),Eval("VerzioJel")) %>'
                                                    Text='<%# Eval("VerzioJel") %>' ToolTip="Korábbi verziók megtekintése" Visible='<%# Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                                <asp:Label ID="labelVerzio" runat="server" Text='<%# Eval("VerzioJel") %>' Visible='<%# !Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Formatum_Nev" HeaderText="Formátum" SortExpression="Formatum_Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                            <ItemTemplate>
                                                <asp:Label ID="Label_krtDok_Id" runat="server" Text='<%# Eval("krtDok_Id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%-- TabHeader --%>
                        <uc1:TabHeader ID="TabHeader1" runat="server" />
                        <%-- /TabHeader --%>
                        <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
                            <asp:GridView ID="CsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" DataKeyNames="Id,FajlNev" AutoGenerateColumns="False" Width="98%"
                                OnRowCommand="CsatolmanyokGridView_RowCommand" OnRowDataBound="CsatolmanyokGridView_RowDataBound"
                                OnSorting="CsatolmanyokGridView_Sorting">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <span style="white-space: nowrap">
                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                CssClass="HideCheckBoxText" />
                                            <%--'<%# E_val("Id") %>'--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                    </asp:CommandField>
                                    <%--                                    <asp:BoundField DataField="FajlNev" HeaderText="Állomány" SortExpression="FajlNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="Állomány" SortExpression="FajlNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelFajlNev" runat="server" Text='<%# Eval("FajlNev") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="imageButtonPDF" runat="server" OnClientClick='<%# String.Format("window.open(\"{0}\",\"_blank\",\"width=800,height=600\");return false;", GetPFDLink(Eval("Dokumentum_Id").ToString(), Eval("SablonAzonosito").ToString())) %>'
                                                            ImageUrl="~/images/hu/fileicons/pdf.gif" ToolTip="pdf" Style="padding-left: 5px;" Visible='<%# IsPDFSablon(Eval("SablonAzonosito").ToString()) %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" SortExpression="KRT_Dokumentumok.Formatum">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="0px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                                ImageUrl="images/hu/fileicons/default.gif" ToolTip="Megnyitás" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Azonosito" HeaderText="Azonosító" SortExpression="Azonosito" Visible="false">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Leiras" HeaderText="Megjegyzés" SortExpression="Leiras">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Verzió" SortExpression="KRT_Dokumentumok.VerzioJel">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="linkVerzio" runat="server" NavigateUrl='<%# Contentum.eRecord.Utility.Dokumentumok.GetVerziokLink(Eval("krtDok_Id"),Eval("VerzioJel")) %>'
                                                Text='<%# Eval("VerzioJel") %>' ToolTip="Korábbi verziók megtekintése" Visible='<%# Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                            <asp:Label ID="labelVerzio" runat="server" Text='<%# Eval("VerzioJel") %>' Visible='<%# !Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="Tipus" HeaderText="Típus" SortExpression="Tipus">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="Formatum_Nev" HeaderText="Formátum" SortExpression="Formatum_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DokumentumSzerep_Nev" HeaderText="Szerep" SortExpression="DokumentumSzerep_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DokumentumSzerep" HeaderText="">
                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    </asp:BoundField>
                                    <%--                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="labelMegnyithato" Text="Megnyitható" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbMegnyithato" runat="server" AutoPostBack="false" Text='<%# Eval("Megnyithato") %>'
                                                Enabled="false" CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <%--                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="labelOlvashato" Text="Olvasható" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbOlvashato" runat="server" AutoPostBack="false" Text='<%# Eval("Olvashato") %>'
                                                Enabled="false" CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:BoundField DataField="ElektronikusAlairasNev" HeaderText="Elektronikus aláírás"
                                        SortExpression="ElektronikusAlairasNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AlairasFelulvizsgalat" HeaderText="Ellenõrizve" SortExpression="AlairasFelulvizsgalat">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OCRAllapot_Nev" HeaderText="OCR állapot" SortExpression="OCRAllapot_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:Image ID="TitkositasImage" runat="server" ImageUrl="~/images/hu/egyeb/kulcs.gif"
                                                ToolTip="Titkosított" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:Image ID="MellekletImage" AlternateText="Melléklet" Height="20px" Width="25px"
                                                runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                        <ItemTemplate>
                                            <asp:Label ID="Label_krtDok_Id" runat="server" Text='<%# Eval("krtDok_Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>
                        <eUI:eFormPanel ID="Warning_Fodokumentum_Panel" runat="server" Visible="false">
                            <%-- Figyelmeztetés hiányzó vagy egynél több fődokumentumnál --%>
                            <asp:Label ID="Label_Warning_Fodokumentum" runat="server" Text="" CssClass="warningHeader" />
                        </eUI:eFormPanel>
                        <asp:Panel ID="SubListPanel" runat="server">
                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="0"
                                OnActiveTabChanged="TabContainer1_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged">
                                <ajaxToolkit:TabPanel ID="MellekletekTabPanel" runat="server" TabIndex="0" HeaderText="Csatolmány mellékletei">
                                    <ContentTemplate>
                                        <asp:Panel ID="MellekletekPanel" runat="server" Visible="False" Width="100%"
                                            OnLoad="MellekletekPanel_Load">
                                            <uc4:SubListHeader ID="MellekletekSubListHeader" runat="server" />
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="MellekletekCPE" runat="server" TargetControlID="Panel2"
                                                            CollapsedSize="20" ExpandedSize="0" Enabled="True">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <asp:Panel ID="Panel2" runat="server">
                                                            <asp:GridView ID="MellekletekGridView" runat="server" CellPadding="0"
                                                                BorderWidth="1px" AllowPaging="True"
                                                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                                                                OnRowCommand="MellekletekGridView_RowCommand" OnSorting="MellekletekGridView_Sorting"
                                                                OnPreRender="MellekletekGridView_PreRender" EnableModelValidation="True">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;&nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                CssClass="HideCheckBoxText" />
                                                                            <%--'<%# E_val("Id") %>'--%>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    </asp:TemplateField>
                                                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                                    </asp:CommandField>
                                                                    <asp:BoundField DataField="AdathordozoTipusNev" HeaderText="Mellékletek (adathordozó) típusa"
                                                                        SortExpression="AdathordozoTipusNev">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Mennyiseg" HeaderText="Mennyiség" SortExpression="Mennyiseg">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MennyisegiEgysegNev" HeaderText="Egység" SortExpression="MennyisegiEgysegNev">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyzés" SortExpression="Megjegyzes">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Melleklet_BarCode" HeaderText="Vonalkód" SortExpression="Melleklet_BarCode">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                <PagerSettings Visible="False" />
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="OsszetettDokuKapcsolatokTabPanel" runat="server" TabIndex="1">
                                    <HeaderTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_Label" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="Label1" runat="server" Text="Összetett dokumentum kapcsolatok"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <uc6:DokumentumHierarchia ID="DokumentumHierarchia1" runat="server" />
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                            </ajaxToolkit:TabContainer>
                        </asp:Panel>
                        <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <asp:Panel ID="AllomanyAdatokPanel" runat="server" style="text-align: left">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor" id="FileUploadRow" runat="server">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="LabelAllomany_Csillag" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="LabelAllomany" runat="server" Text="Állomány:"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <uc3:FileUploadComponent ID="FileUploadComponent1" runat="server" SingleMode="true"
                                                Visible="false" />
                                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="mrUrlapInput" Width="350" />
                                        </td>
                                    </tr>
                                    <%--LZS - BLG_4391--%>
                                    <tr class="urlapSor" id="CsatolmanyokListboxLabelRow" runat="server" visible="false">
                                        <td class="mrUrlapCaption" style="text-align: left; vertical-align: middle;" colspan="2">
                                            <asp:Label ID="Label4" runat="server" Text="Kérem a feltötött csatolmányok közül válasszon fődokumentumot!">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" id="CsatolmanyokListboxRow" runat="server" visible="false">

                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="Label3" runat="server" Text="Dokumentumok:"></asp:Label>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:ListBox ID ="CsatolmanyokListbox" runat="server" Rows="3"></asp:ListBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="lbDokumemtumSzerepStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="lbDokumemtumSzerep" runat="server" Text="Szerep:"></asp:Label>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_DokumentumSzerep" runat="server" CssClass="mrUrlapInput" />
                                            <%-- Megjegyezzük az eredeti értéket, hogy a fõdokumentumokat figyelni tudjuk --%>
                                            <asp:HiddenField ID="hfDokumentumSzerep" runat="server" />
                                            <asp:HiddenField ID="hfIsFodokument" runat="server" />
                                            <asp:CustomValidator ID="validatorDokumentumSzerep" runat="server" EnableClientScript="true" ErrorMessage="<%$Resources:Error,UIAlreadyExistsFodokumentumInCsatolmanyok %>" SetFocusOnError="true" Display="None"></asp:CustomValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="validatorCalloutDokumentumSzerep" runat="server"
                                                TargetControlID="validatorDokumentumSzerep">
                                                <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                    <HideAction Visible="true" />
                                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                                </Animations>
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="LabelCsatolmanyMegjegyzes" runat="server" Text="Megjegyzés:"></asp:Label>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:TextBox ID="CsatolmanyMegjegyzes" runat="server" CssClass="mrUrlapInput" />
                                        </td>
                                    </tr>
                                    <%--						    <tr class="urlapSor">
							        <td style="text-align: right; vertical-align: middle;">
								        <asp:Label ID="LabelCsatolmanyVerzio" runat="server" Text="Verzió:"></asp:Label>&nbsp;
                                        &nbsp;</td>
							        <td style="text-align: left; vertical-align: middle;">
								        <asp:TextBox ID="CsatlomanyVerzio" runat="server" Width="30" ReadOnly="True"/>
							        </td>
						        </tr>
                                    --%>
                                    <tr class="urlapSor" id="trMegnyithatoOlvashatoTitkositott" runat="server">
                                        <td class="mrUrlapCaption" style="height: 30px"></td>
                                        <td class="mrUrlapMezo" style="height: 30px">
                                            <asp:CheckBox ID="cbMegnyithato" Text="Megnyitható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbOlvashato" Text="Olvasható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbTitkositott" Text="Titkosított" TextAlign="Right" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" Text ="Jelöljön ki egy csatolmányt a listában!" Visible="false"/>
                                <asp:ImageButton ID="btnFodokumentumSelect" runat="server" CausesValidation="false" Text="Mentés" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"  style="text-align: right" Visible="false" OnClick="btnFodokumentumSelect_Click1" />
                            </asp:Panel>
                            <asp:Panel ID="ElektronikusAlairasPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAlairas" runat="server" Text="Aláírás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <%--<asp:DropDownList ID="ddl_AlairasManualis" runat="server" CssClass="mrUrlapInputComboBox_minWidth"
                                                        Enabled="false" Visible="false" />--%>
                                            <ktddl:KodtarakDropDownList ID="AlairasManualis_KodtarakDropDownList" runat="server" Enabled="false" Visible="false" />
                                            <asp:UpdatePanel ID="AlairasUpdatePanel" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:CheckBox ID="cb_Alairas" runat="server" Checked="false" />
                                                    <asp:DropDownList ID="ddl_AlairasSzabalyok" runat="server" CssClass="mrUrlapInputComboBox_minWidth"
                                                        Enabled="false" Visible="true" Style="display: none;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <object classid="clsid:0E9BF42E-38BC-4711-B1D4-D6A77A5AD96A" height="1" id="sdxsigner"
                                                width="1" codebase="http://pc-psenak2/SDX_SignAx.cab#Version=1,0,0,1">
                                                <param name="Easz" value="">
                                                <param name="Thumbprint" value="">
                                            </object>
                                        </td>
                                    </tr>
                                    <%-- Chip kártyás aláírás teszt: --%>
                                    <%--<tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="label1" runat="server" Text="Chip-kártyás aláírás:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">	
                                        <asp:FileUpload runat="server" ID="fileUpload1" />
                                        <asp:Button ID="ClientSignButton" runat="server" Text="Aláírás" />
                                        <asp:Button ID="UploadButton2" runat="server" Text="Feltöltés2" CausesValidation="false" 
                                            onclick="UploadButton2_Click" />
                                            <asp:Label ID="Label_fileName" runat="server" />
                                    </td>
                                </tr>--%>
                                    <%-- Titkosítás, egyelõre csak elõkészítve, nem látható --%>
                                    <%--    <tr class="urlapSor" visible="true">
                                    <td class="mrUrlapCaption" style="height: 30px">
                                    </td>
                                     <td class="mrUrlapMezo" style="height: 30px">						    
					                    <asp:CheckBox ID="cbTitkositas" Text="Titkosítás" TextAlign="Right" runat="server" Width="250"/>
				                    </td>
			                    </tr>--%>
                                </table>
                            </asp:Panel>

                            <asp:HiddenField ID="hf_confirmUploadByDocumentHash" runat="server" Value="" />
                            <%--BUG_7470--%>
                            <asp:HiddenField ID="hf_ConfirmUploadforStorno" runat="server" Value="" />
                            <%-- TabFooter --%>
                            <div style="margin-top: 5px;">
                                <uc2:TabFooter ID="TabFooter1" runat="server" />
                            </div>
                            <%-- /TabFooter --%>
                        </eUI:eFormPanel>
                        <eUI:eFormPanel ID="AlairasAdatok_EFormPanel" runat="server" Visible="false">
                            <table cellspacing="0" cellpadding="0" width="95%">
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="text-align: left;">
                                        <asp:Label ID="Label2" runat="server" Text="Aláírás információ:"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: left">
                                        <asp:Label ID="Label_FajlAdatok" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </asp:Panel>

        <uc3:DownloadDocumentAsZip ID="DownloadDocumentAsZip1" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
<%--Microsec ESign aláíráshoz--%>
<uc13:MicroSigner ID="MicroSigner1" runat="server" />
<asp:FileUpload ID="dummyFU" runat="server" Style="visibility: hidden;" />