﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

using Contentum.eDocument.Service;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using Contentum.eIntegrator.Service;
using Contentum.eUtility.NetLockSignAssist;
using Contentum.eUtility.Netlock;
using IO.Swagger.Models;

public partial class eRecordComponent_UgyiratCsatolmanyokTab : System.Web.UI.UserControl
{
    private const string kcsDokumentumAlairasPKI = "DOKUMENTUM_ALAIRAS_PKI";
    private const string kcsDokumentumAlairasManualis = "DOKUMENTUM_ALAIRAS_MANUALIS";
    private const string kcsDokumentumSzerep = "DOKUMENTUM_SZEREP";
    private const string Funkcio_CsatolmanyMellekletKapcsolatNew = "CsatolmanyMellekletKapcsolatNew";
    private const string Funkcio_CsatolmanyMellekletKapcsolatInvalidate = "CsatolmanyMellekletKapcsolatInvalidate";
    private const string CsatolmanyTomegesLetoltesTomoritesPKI = "CSATOLMANY_TOMEGES_LETOLTES_TOMORITES";
    private const string NeedSignCheck = "Dokumentum nem rendelkezik érvényes elektronikus aláírással, vagy aláírás ellenõrzés szükséges";
    private const string FodokUploadError = "A fõdokumentumnak feltöltendõ fájl csak aláírható fájl lehet.";
    private const string FodokUploadPaperError = "A fõdokumentumnak feltöltendõ fájl papír alapú irat esetén nem lehet kr fájl.";
    private const string KrUploadError = "KR kiterjesztésû állomány csak fõdokumentum lehet";
    private const string KrUploadErrorCsakEl = "KR kiterjesztésû állomány csak elektronikus irathoz csatolható.";


    // Jogellenõrzéshez a szülõ formon lekért objektum (küldemény, irat, iratpéldány)
    public object objFromParent = null;

    // a PKI_INTEGRACIO rendszerparamneter befolyasolja az elektronikus alairashoz
    // kapcsolodo funkciokat es a site megjelenest
    private string krt_Parameterek_PKI_INTEGRACIO = "";
    // az elektronikus alaira kodcsoportja a PKI_INTEGRACIO rendszerparameter erteketol fugg
    private string kcsDokumentumAlairas = "";

    private string CsatolmanyTomegesLetoltesTomorites = "";

    // érvényes path: ^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$
    // ellenõrzött esetek:
    // 1. A : elõtt, ha van, csak a-zA-Z karakter szerepelhet
    // 2. A \\ elõtt, ha van, nem lehet más karakter
    // 3. A : elõtt, ha van, max. 1 karakter szerepelhet
    // 4. : vagy \ után nem állhat közvetlenül .
    // 5. Az útvonalban/fájlnévben nem állhat ..
    // 6. Útvonal nem kezdõdhet . vagy : karakterrel
    // 7. Ha az útvonal meghajtójel: alakban kezdõdik, és nem követi közvetlenül \, akkor hátrébb sem állhat \ (azaz csak fájlnév követheti)
    // 8. A fájlnévben/útvonalban nem állhat a # % & * < > ? / { | } karakterek egyike sem
    private const string regexpCheckPathErrors = @"(^(.*[^a-zA-Z].*:|.+\\\\{2,}|.{2,}:))|((:|\\\\)\\.)|^(\\.|:)|(\\.{2,})|(^[a-zA-Z]:[^\\\\]+\\\\)|([#%&\\*\\<\\>\\?\\/\\{{\\|\\}}])";
    private string js_FileNameCheckFunction = String.Format(@"function checkFileName(fileName)
{{
    if (fileName) {{
        var re=new RegExp('{0}');
        var m = re.exec(fileName);
        if (m && m.length > 0) {{
            alert('{1}' + m[0]);
            return false;
        }}
    }}
    return true;
}}", regexpCheckPathErrors, Resources.Error.UIFileNameFormatError);

    private bool IsThisTabActive
    {
        get
        {
            if (hf_IsThisTabActive.Value == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private bool _ChangedToActive = false;
    public bool Active
    {
        get { return _ChangedToActive; }
        set
        {
            _ChangedToActive = value;
            //MainPanel.Visible = value;
            if (value == true)
            {
                hf_IsThisTabActive.Value = "1";
            }
            else
            {
                hf_IsThisTabActive.Value = "0";
            }
        }
    }

    public Label TabHeader = new Label();

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";


    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    private bool checkFileName = false;

    #region BUG 6380
    private bool _SignerCondition = true;
    public bool SignerCondition
    {
        get { return _SignerCondition; }
        set { _SignerCondition = value; }
    }

    #endregion


    bool ElektronikusAlairasEnabled
    {
        get
        {

            //BUG 6380 - azt is ellenõrizze, hogy ki a következõ aláíró
            return SignerCondition && (FunctionRights.GetFunkcioJog(Page, "KozpontiElektronikusAlairas")
                || FunctionRights.GetFunkcioJog(Page, "KartyasElektronikusAlairas")
                || Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), "PKI_INTEGRACIO") == "Nem");
        }
    }

    #region BLG 1646
    bool isAlairo
    {
        get
        {
            //csak irat, és iratPéldány felõl van megvalósítva a funkció
            if (_ParentForm == Constants.ParentForms.IraIrat || _ParentForm == Constants.ParentForms.IratPeldany)
            {
                //LZS - BUG_10796
                //if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.New))
                //{
                //    return true;
                //}

                //EREC_IratAlairok ellenõrzése
                if (FunctionRights.GetFunkcioJog(Page, "KozpontiElektronikusAlairas") || FunctionRights.GetFunkcioJog(Page, "KartyasElektronikusAlairas") || Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), "PKI_INTEGRACIO") == "Nem")
                {
                    EREC_IratAlairokService iratAlairokService = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                    EREC_IratAlairokSearch iratAlairokSearch = new EREC_IratAlairokSearch();

                    string iratId = string.Empty;

                    if (_ParentForm == Constants.ParentForms.IraIrat)
                    {
                        //Iratok felõl maga a ParentId az irat azonosítója
                        iratId = ParentId;
                    }
                    else
                    {
                        EREC_PldIratPeldanyok peldany = GetIratPeldany();

                        if (peldany == null)
                        {
                            return false;
                        }

                        iratId = peldany.IraIrat_Id;
                    }
                    if (!string.IsNullOrEmpty(iratId))
                    {
                        iratAlairokSearch.Obj_Id.Value = iratId;
                        iratAlairokSearch.Obj_Id.Operator = Query.Operators.equals;

                        try
                        {
                            Result resIRatAlairok = iratAlairokService.GetAll(new ExecParam(), iratAlairokSearch);
                            if (resIRatAlairok.IsError)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resIRatAlairok);
                                ErrorUpdatePanel.Update();
                                return false;
                            }
                            else
                            {
                                List<string> alairoIds = new List<string>();

                                foreach (DataRow row in resIRatAlairok.Ds.Tables[0].Rows)
                                {
                                    string Id = row["FelhasznaloCsoport_Id_Alairo"].ToString();
                                    if (!String.IsNullOrEmpty(Id) && !alairoIds.Contains(Id))
                                    {
                                        alairoIds.Add(Id);
                                    }
                                    string helyettesId = row["FelhaszCsoport_Id_Helyettesito"].ToString();
                                    if (!String.IsNullOrEmpty(helyettesId) && !alairoIds.Contains(helyettesId))
                                    {
                                        alairoIds.Add(helyettesId);
                                    }
                                }

                                if (alairoIds.Count > 0)
                                {
                                    if (alairoIds.Contains(FelhasznaloProfil.LoginUserId(Page)))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
                            ErrorUpdatePanel.Update();
                            return false;
                        }
                    }
                }

                //csatolmány jog ellenõrzése
                bool hasRight_Csatolmanyok = true;
                switch (_ParentForm)
                {
                    case Constants.ParentForms.IraIrat:
                        hasRight_Csatolmanyok = Iratok.CheckRights_Csatolmanyok(Page, ParentId, true);
                        break;
                    case Constants.ParentForms.IratPeldany:
                        hasRight_Csatolmanyok = IratPeldanyok.CheckRights_Csatolmanyok(Page, ParentId, true);
                        break;
                }
                return hasRight_Csatolmanyok;


            }
            return false;
        }
    }
    #endregion
    private bool ElektronikusAlairasVisszavonasEnabled
    {
        get;
        set;
    }

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        //        if (!MainPanel.Visible) return;

        ParentId = Request.QueryString.Get(QueryStringVars.Id);
        Command = Request.QueryString.Get(QueryStringVars.Command);

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IratAlairokService alairok_service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        if (String.IsNullOrEmpty(ParentId))
        {
            ElektronikusAlairasVisszavonasEnabled = false;
        }
        else
        {
            Result result = alairok_service.CheckAlairasVisszavonhato(execParam, ParentId);
            ElektronikusAlairasVisszavonasEnabled = !result.IsError;
        }

        CsatolmanyTomegesLetoltesTomorites = Rendszerparameterek.Get(execParam, CsatolmanyTomegesLetoltesTomoritesPKI);
        ViewState[CsatolmanyTomegesLetoltesTomoritesPKI] = CsatolmanyTomegesLetoltesTomorites;
        Page.ClientScript.RegisterHiddenField("v" + CsatolmanyTomegesLetoltesTomoritesPKI, CsatolmanyTomegesLetoltesTomorites);

        krt_Parameterek_PKI_INTEGRACIO = Rendszerparameterek.Get(execParam, "PKI_INTEGRACIO");
        switch (krt_Parameterek_PKI_INTEGRACIO)
        {
            case "Nem":
                kcsDokumentumAlairas = kcsDokumentumAlairasManualis;
                break;
            case "Igen":
                //nekrisz CR 3015 : PKI_integrációná a régi ellenörzés mellõzése
                string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    kcsDokumentumAlairas = kcsDokumentumAlairasManualis;
                }
                else kcsDokumentumAlairas = kcsDokumentumAlairasPKI;
                break;
            default:
                kcsDokumentumAlairas = kcsDokumentumAlairasManualis;
                break;
        }

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);
        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        MellekletekSubListHeader.AttachedGridView = MellekletekGridView;
        MellekletekSubListHeader.ButtonsClick += new CommandEventHandler(MellekletekSubListHeader_ButtonsClick);
        MellekletekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(MellekletekSubListHeader_ErvenyessegFilter_Changed);
        MellekletekSubListHeader.RowCount_Changed += new EventHandler(MellekletekSubListHeader_RowCount_Changed);

        #region BLG 2947
        TabHeader1.CheckVisible = krt_Parameterek_PKI_INTEGRACIO == "Igen";
        #endregion
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!MainPanel.Visible) return;
        if (!IsThisTabActive) return;

        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(TabContainer1);

        if (checkFileName)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "checkFileName", js_FileNameCheckFunction, true);
        }

        AlairasAdatok_EFormPanel.Visible = false;

        if (_ParentForm == Constants.ParentForms.Kuldemeny)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KuldemenyCsatolmanyList");
        }
        else if (_ParentForm == Constants.ParentForms.IraIrat
            || _ParentForm == Constants.ParentForms.IratPeldany)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IraIratCsatolmanyList");
        }

        #region DokumentumHierarchia

        string funkcioKod_DokumentumView = "";
        if (_ParentForm == Constants.ParentForms.Kuldemeny)
        {
            funkcioKod_DokumentumView = "KuldemenyCsatolmanyView";
        }
        else if (_ParentForm == Constants.ParentForms.IraIrat
            || _ParentForm == Constants.ParentForms.IratPeldany)
        {
            funkcioKod_DokumentumView = "IraIratCsatolmanyView";
        }

        DokumentumHierarchia1.InitComponent(EErrorPanel1, ErrorUpdatePanel, (Command == CommandName.Modify) ? true : false, funkcioKod_DokumentumView);

        #endregion

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }
        }

        if (Command == CommandName.Modify)
        {
            /// fileUpload:
            /// onchange: a fájlnév ellenõrzése (a nevükben ..-t tartalmazó fájlok sem tölthetõk fel)
            if (checkFileName)
            {
                FileUpload1.Attributes["onchange"] = "if (!checkFileName(this.value)) return false;";
            }

            if (Command == CommandName.Modify && krt_Parameterek_PKI_INTEGRACIO == "Igen")
            {
                //nekrisz CR 3015 : PKI_integrációná a régi ellenörzés mellõzése
                string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "microsec".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "halk".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    ElektronikusAlairasPanel.Visible = false;
                }
                else
                {
                    // Aláírás checkbox:
                    ElektronikusAlairasPanel.Visible = true;
                    cb_Alairas.Attributes["onclick"] = @" 
            var ddlAlairasSzabalyok = $get('" + ddl_AlairasSzabalyok.ClientID + @"');
            if (ddlAlairasSzabalyok)
            {
                if (this.checked)
                {
                    ddlAlairasSzabalyok.style.display = 'inline';
                    ddlAlairasSzabalyok.disabled = false;                    
                }
                else
                {
                    ddlAlairasSzabalyok.style.display = 'none';
                    ddlAlairasSzabalyok.disabled = true;       
                }
            }
            ";

                    // Aláírásszabály dropdown:
                    ddl_AlairasSzabalyok.Attributes["onchange"] = @"              
                if (typeof(isClientSideSign)=='function' && isClientSideSign(this.value) 
                    && typeof(SelectedFileIsSigned) == 'function' && !SelectedFileIsSigned()
                    && typeof(SignFile) == 'function')
                {                 
                    SignFile();                    
                }
            ";

                    /// fileUpload:
                    /// onchange: ha kliensoldali aláírásszabály van, és nem aláírt fájl lett kiválasztva, aláíró komponens meghívása:
                    FileUpload1.Attributes["onchange"] += @"var ddlAlairasSzabalyok = $get('" + ddl_AlairasSzabalyok.ClientID + @"');
                    if (typeof(isClientSideSign)=='function' && isClientSideSign(ddlAlairasSzabalyok.value) 
                        && typeof(SelectedFileIsSigned) == 'function' && !SelectedFileIsSigned()
                        && typeof(SignFile) == 'function')
                    {                       
                        SignFile();
                    }
                    ";
                }
            }
        }
        // Aláírásszabály

        //TabFooter1.ImageButton_Save.OnClientClick += " if $get('" + Alairas_RadioButtonList.ClientID  +"'){ SignFile2('C:\\\\teszt.txt'); return false;}";
        //        ClientSignButton.OnClientClick = @" var fileUpload = $get('" + fileUpload1.ClientID + @"');
        //                    if (fileUpload){ SignFile2(fileUpload.value);} return false;";
        //SetOCRGridColumns();

        //LZS 
        #region Aláírás visszavonása 
        TabHeader1.ElektronikusAlairasVisszavonasOnClientClick = Iratok.SetClientOnClickAlairasVisszavonas(ParentId, Page);
        TabHeader1.ElektronikusAlairasVisszavonasOnClientClick += "var count = getSelectedCheckBoxesCount('"
                   + CsatolmanyokGridView.ClientID + "','check'); if (count==0) {alert('"
                   + Resources.List.UI_NoSelectedItem + "'); return false;} else {" +
                   JavaScripts.SetOnClientClick("DokumentumAlairasVisszavonas.aspx",
                    QueryStringVars.CsatolmanyId + "=" + "'+getIdsBySelectedCheckboxes('" + CsatolmanyokGridView.ClientID + "','check')+'"
                    , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolmanyokList)
                   + "}";
        #endregion

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //if (!MainPanel.Visible) return;
        if (!IsThisTabActive) return;

        //if (_ChangedToActive)
        //{
        //    CsatolmanyokUpdatePanel.Update();
        //}


        #region fájlnév lista leküldése, hogy javascriptbõl könnyen elérjük:
        // TASK_4976
        string js = String.Empty;
        //if (Command == CommandName.Modify)
        //{
        StringBuilder sb_fileNamesList = new StringBuilder();

        // fájlnév lista a ViewState-bõl:
        List<string> fileNamesList = (List<string>)ViewState[viewState_FileNames];
        if (fileNamesList != null)
        {
            for (int i = 0; i < fileNamesList.Count; i++)
            {
                if (i == 0)
                {
                    sb_fileNamesList.Append("\"" + fileNamesList[i] + "\"");
                }
                else
                {
                    sb_fileNamesList.Append(",\"" + fileNamesList[i] + "\"");
                }
            }
        }

        js += "var fileNamesArray = new Array(" + sb_fileNamesList.ToString() + @"); 

                function fileAlreadyExist(filePath)
                {
                    var filePathParts = filePath.split('\\');
                    if (filePathParts && filePathParts.length > 0)
                    {
                        var fileName = filePathParts[filePathParts.length-1];
                    
                        if (fileNamesArray)
                        {
                            for (i=0;i<fileNamesArray.length;i++)
                            {
                                if (fileName==fileNamesArray[i]) { return true; }
                            }
                        }
                    }
                    return false;
                }";

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "FileNamesList", js, true);
        //}
        #endregion

        #region Kliens oldali aláírásszabályok és függvények leküldése
        // TASK_4976
        //if (Command == CommandName.Modify)
        //{
        if (ViewState[viewState_KliensoldaliAlairasSzabalyok] != null)
        {
            List<string> kliensoldaliAlairasSzabalyList = (List<string>)ViewState[viewState_KliensoldaliAlairasSzabalyok];

            if (kliensoldaliAlairasSzabalyList != null)
            {
                StringBuilder sb_alairasSzabalyok = new StringBuilder();

                for (int i = 0; i < kliensoldaliAlairasSzabalyList.Count; i++)
                {
                    if (i == 0)
                    {
                        sb_alairasSzabalyok.Append("\"" + kliensoldaliAlairasSzabalyList[i] + "\"");
                    }
                    else
                    {
                        sb_alairasSzabalyok.Append(",\"" + kliensoldaliAlairasSzabalyList[i] + "\"");
                    }
                }


                js += "var clientSideSignArray = new Array(" + sb_alairasSzabalyok.ToString() + @"); 

                        function isClientSideSign(easz_id)
                        {            
                            if (clientSideSignArray && easz_id)
                            {
                                for (i=0;i<clientSideSignArray.length;i++)
                                {
                                    if (easz_id==clientSideSignArray[i]) { return true; }
                                }
                            }
                            
                            return false;
                        }
                        
                        function SelectedFileIsSigned()
                        {
                            var fileUpload = $get('" + FileUpload1.ClientID + @"');
                            if (fileUpload)
                            {
                                // kiterjesztés megállapítása:
                                var indexOfDot = fileUpload.value.lastIndexOf('.');
                                if (indexOfDot > -1)
                                {
                                    var fileExtension = fileUpload.value.substring(indexOfDot+1);
                                    if (fileExtension.toLowerCase() == 'sdxm')
                                    {
                                        return true;
                                    }
                                }                                
                            }
                            return false;
                        }

                        function SignFile()
                        {   
                            // ha üres a fileUpload --> file kiválasztás, aláírás, aláírt file kiválasztás
                            // ha nem üres --> aláírás, aláírt file kiválasztás
                     
                            var fileUpload = $get('" + FileUpload1.ClientID + @"');
                            if (!fileUpload) { return false; }

                            if (fileUpload.value == null || fileUpload.value == '')
                            {
                                alert('Válassza ki az aláírandó fájlt!');
                                return false;
                            }
                            
                            var sdxsignerObj = $get('sdxsigner');
                          
                            // Aláírás
                            if (!sdxsignerObj) { alert('Nem található a kliensoldali aláíró komponens!'); return false; }

                            try
                            {   
                                sdxsignerObj.GetPolicyList();
                            }
                            catch(err)
                            {
                                alert('Nem található a kliensoldali aláíró komponens!'); 
                                return false;
                            }
                            
                            sdxsignerObj.Sign(fileUpload.value);                                                       

                            // Aláírt fájl kiválasztása:
                            alert('Válassza ki az aláírt fájlt! \n');

                            //fileUpload.click();                            
                        }
                    ";

                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "kliensoldaliAlairasJs", js, true);
            }
        }
        //}

        #endregion


        if (FileUpload1.Visible && FileUpload1.Enabled)
        {
            // FileUpload nem mûködik UpdatePanel-en belül aszinkron postback-kel, ezért kell ez:
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(TabFooter1.ImageButton_Save);

            // a scriptet az elejére rakjuk, mert a TabFooter már belepakolt egy return false; -t
            /// scriptek:
            ///    - ha kliens oldali aláírásszabályt választott, ellenõrzés, hogy a kiválasztott fájl aláírt fájl-e (.sdxm fájl)
            ///    - fájlnév meglétének ellenõrzése, illetve pörgõ bigyó elindítása:


            /// fileUpload:
            /// a fájlnév ellenõrzése (a nevükben ..-t tartalmazó fájlok sem tölthetõk fel)

            /// mivel nem tudjuk az IsPostBack vagy az ScriptManager.GetCurrent(Page).IsInAsyncPostBack alapján,
            /// hogy hozzá lett-e már adva, nem túl elegánsan, de így figyeljük, hogy kiegészítettük-e már
            /// az OnClientClicket... 
            if (!TabFooter1.ImageButton_Save.OnClientClick.Contains("fileUpload"))
            {
                string js_Check = "var fileUpload = $get('" + FileUpload1.ClientID + @"');";

                if (checkFileName)
                {
                    js_Check += @"if (fileUpload && !checkFileName(fileUpload.value)) return false;";
                }

                js_Check += @"var ddlAlairasSzabalyok = $get('" + ddl_AlairasSzabalyok.ClientID + @"');

if (typeof(clientSideSignArray) != 'undefined' && ddlAlairasSzabalyok && ddlAlairasSzabalyok.disabled == false)
{
       if (typeof(isClientSideSign)=='function' && typeof(SelectedFileIsSigned) == 'function'
            && isClientSideSign(ddlAlairasSzabalyok.value) && !SelectedFileIsSigned())
        {
            alert('A kiválasztott fájl nincs aláírva! \n(Nem .sdxm kiterjesztésû a fájl)');
            return false;
        }
}
if (fileUpload && fileAlreadyExist(fileUpload.value))
{ if (!confirm('" + Resources.Question.UIConfirm_FileAlreadyExist + @"')){ return false;} }";

                js_Check += "Page_ClientValidate('" + TabFooter1.ImageButton_Save.ValidationGroup + "'); if(!Page_IsValid) return false;";

                TabFooter1.ImageButton_Save.OnClientClick = js_Check + " $get('" + CustomUpdateProgress_FileUpload.UpdateProgress.ClientID + "').style.display = 'block'; "
                    + TabFooter1.ImageButton_Save.OnClientClick;
            }
        }

        #region Aláíró checkbox -ra hibaüzenet kötése, ha volt:

        if (Command == CommandName.Modify)
        {
            // ha volt elmentett hibaüzenet az aláírásszabályok feltöltésekor:
            if (ViewState[viewState_ERROR_AlairasSzabalyFeltoltes] != null)
            {
                string errorMessage = ViewState[viewState_ERROR_AlairasSzabalyFeltoltes].ToString();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    // Aláírás checkbox:
                    cb_Alairas.Attributes["onclick"] = "if (this.checked) { alert('" + errorMessage + "'); }";
                }
            }
        }

        #endregion

        #region Gombok engedélyezése funkciójogok alapján

        if (_ParentForm == Constants.ParentForms.Kuldemeny)
        {
            #region Kuldemeny

            // Funkciójogok alapján gombok tiltása:
            TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.New);
            TabHeader1.LinkViewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.View);
            TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.Modify);
            TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.Invalidate);
            // Küldeménynél nincs, csak iratnál:
            //bernat.laszlo added : NETLOCK El. Aláírás
            //TabHeader1.ElektronikusAlairasEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.ElektronikusAlairas);

            TabHeader1.ElektronikusAlairasEllenorzesEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.ElektronikusAlairas);
            //bernat.laszlo eddig
            TabHeader1.VersionEnabled = false;
            TabHeader1.ElosztoListaEnabled = false;

            #endregion
        }
        else if (_ParentForm == Constants.ParentForms.IraIrat
            || _ParentForm == Constants.ParentForms.IratPeldany)
        {
            #region Irat, Iratpéldány
            #region BLG 1646
            //LZS - BUG_10796
            bool functionNewCommand = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.New);
            TabHeader1.NewEnabled =
            TabHeader1.NewVisible = functionNewCommand;
            #endregion

            TabHeader1.LinkViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.View);
            TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.Modify);
            TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.Invalidate);
            //bernat.laszlo added : NETLOCK El. Aláírás
            // nekrisz: Aláírás csak ha PKI_Integracio be van kapcsolva
            if (krt_Parameterek_PKI_INTEGRACIO == "Igen")
            {
                TabHeader1.ElektronikusAlairasEnabled = this.ElektronikusAlairasEnabled;

            }

            TabHeader1.ElektronikusAlairasVisszavonasVisible = true;
            if (this.ElektronikusAlairasEnabled)
            {
                TabHeader1.ElektronikusAlairasEnabled = !this.ElektronikusAlairasVisszavonasEnabled;
            }
            TabHeader1.ElektronikusAlairasVisszavonasEnabled = this.ElektronikusAlairasVisszavonasEnabled;
            TabHeader1.ElektronikusAlairasEllenorzesEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.ElektronikusAlairas);
            TabHeader1.VersionEnabled = false;
            TabHeader1.ElosztoListaEnabled = false;


            //LZS - BUG_10796
            if (isAlairo)
            {
                TabHeader1.NewEnabled = true;
                TabHeader1.NewVisible = true;

                TabHeader1.InvalidateEnabled = true;

                TabHeader1.InvalidateVisible = true;

            }
            #endregion
        }
        else if (_ParentForm == Constants.ParentForms.Ugyirat)
        {
            TabHeader1.LinkViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.View);
            TabHeader1.NewVisible = false;
            TabHeader1.ModifyVisible = false;
        }
        else
        {
            TabHeader1.NewEnabled = false;
            TabHeader1.LinkViewEnabled = false;
            TabHeader1.ModifyEnabled = false;
            TabHeader1.InvalidateEnabled = false;
            TabHeader1.VersionEnabled = false;
            TabHeader1.ElosztoListaEnabled = false;
            TabHeader1.ElektronikusAlairasEnabled = false;
            //TODO - feltételek ellenõrzése
            TabHeader1.ElektronikusAlairasEllenorzesEnabled = true;
            TabHeader1.AlairasAdatokVisible = false;
            TabHeader1.ElektronikusAlairasEnabled = false;
            TabHeader1.AlairasAdatokEnabled = false;
        }

        TabHeader1.AlairasAdatokEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAlairasAdatok");

        // Zárolás engedélyezése:
        TabHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumokLock");
        TabHeader1.UnLockEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumokLock");

        MellekletekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatNew);
        MellekletekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatInvalidate);

        #endregion

        // láthatóság:
        if (Command == CommandName.Modify)
        {
            TabHeader1.LockVisible = true;
            TabHeader1.UnLockVisible = true;
        }

        TabHeader1.LinkViewVisible = true;
        TabHeader1.ViewVisible = false;

        if (IsPostBack)
        {
            string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
            RefreshOnClientClicksByGridViewSelectedRow(MasterListSelectedRowId);

            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                // subList kiürítése
                ui.GridViewClear(MellekletekGridView);
            }
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(CsatolmanyokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(MellekletekGridView);

        //selectedRecordId kezelése
        //ListHeader1.AttachedGridView = CsatolmanyokGridView;

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
        RegisterSharePointScripts();

        //// Aláíráshoz:
        //RegisterSignFunction();
        //ScriptManager.GetCurrent(Page).RegisterPostBackControl(UploadButton2);

        #region Dokumentum szerep validálás
        if (AllomanyAdatokPanel.Visible)
        {
            validatorDokumentumSzerep.ControlToValidate = String.Empty;
            ScriptManager.RegisterExpandoAttribute(validatorDokumentumSzerep, validatorDokumentumSzerep.ClientID, "controltovalidate", KodtarakDropDownList_DokumentumSzerep.DropDownList.ClientID, true);
            validatorDokumentumSzerep.ClientValidationFunction = "ValidateDokumentumSzerep";
            validatorDokumentumSzerep.ErrorMessage = "<div style=\"width:300px;border-width:0px;\">" + Resources.Error.UIAlreadyExistsFodokumentumInCsatolmanyok.Replace("!", "!<hr/>") + "</div>";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("function ValidateDokumentumSzerep(source, arguments) {");
            sb.AppendLine("var fu = $get('" + FileUpload1.ClientID + "');");
            sb.AppendLine("var fname; if(fu) {fname = fu.value;}");
            sb.AppendLine("else {var hf= $get('" + FileUploadComponent1.HiddenField.ClientID + "');");
            sb.AppendLine("if(hf) { var files = hf.value.split(';'); if(files.length > 1) fname = files[1]; }");
            sb.AppendLine("}");
            sb.AppendLine("if(fileAlreadyExist(fname)) return;");
            sb.AppendLine("var hf1 = $get('" + hfIsFodokument.ClientID + "');");
            sb.AppendLine("var isFodokument = (hf1 && hf1.value == 'true');");
            sb.AppendLine("var hf2 = $get('" + hfDokumentumSzerep.ClientID + "');");
            sb.AppendLine("var dokumentumSzerep = (hf2) ? hf2.value : '';");
            sb.AppendLine("var inputValue = arguments.Value;");
            sb.AppendLine("if(inputValue == '" + KodTarak.DOKUMENTUM_SZEREP.Fodokumentum + "' && dokumentumSzerep != '" + KodTarak.DOKUMENTUM_SZEREP.Fodokumentum + "') {");
            sb.AppendLine("arguments.IsValid = !isFodokument");
            sb.AppendLine("}");
            sb.AppendLine("}");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "jsDokumentumSzerepValidate", sb.ToString(), true);
        }
        #endregion


        #region OCR
        string OCR_ENABLED =
         Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()),
         Contentum.eUtility.Rendszerparameterek.SCAN_OCR_ENABLED);

        if (OCR_ENABLED != null && string.Equals(OCR_ENABLED, "1"))
        {
            TabHeader1.OCREnabled = true;
            TabHeader1.OCRVisible = true;
        }
        else
        {
            TabHeader1.OCREnabled = false;
            TabHeader1.OCRVisible = false;
        }
        SetOCRGridColumns();
        #endregion

        //BUG_4781
        trMegnyithatoOlvashatoTitkositott.Visible = false;
        ElektronikusAlairasPanel.Visible = false;

    }

    //    private void RegisterSignFunction()
    //    {
    //        if (ViewState["SignFile2"] == null)
    //        {
    //            ScriptManager sm = ScriptManager.GetCurrent(Page);
    //            if (sm != null)
    //            {
    //                string js = @"
    //			    function SignFile2(fld)
    //			    {
    //				    if ($get('sdxsigner'))
    //				    {
    //				        $get('sdxsigner').Sign2(fld, 'c:\Con\sdxm\mentes.sdxm');				    
    //				    }
    //				    return false;
    //			    }";

    //                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "SignFile2", js, true);
    //                ViewState["SignFile2"] = "Registered";
    //            }
    //        }
    //    }

    private void RegisterSharePointScripts()
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm != null)
        {
            ScriptManager.RegisterClientScriptInclude(Page, typeof(Page), "SharePointInit", "JavaScripts/SharePointInit.js");
            ScriptManager.RegisterClientScriptInclude(Page, typeof(Page), "SharePointCore", "JavaScripts/SharePointCore.js");
        }
    }

    protected void CsatolmanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        //if (!MainPanel.Visible) return;
        if (!IsThisTabActive) return;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshCsatolmanyokList:
                    CsatolmanyokGridViewBind();
                    break;
            }
        }
    }

    public void ReLoadTab()
    {
        //if (!MainPanel.Visible) return;
        if (!IsThisTabActive) return;

        if (Command != CommandName.New)
        {
            bool hasRight_Csatolmanyok = true;
            //if (ViewState["HasRight_Csatolmanyok"] == null)
            //{
            // jog a csatolmányok megtekintésére: "bizalmas" iratnál csak az õrzõ és vezetõje
            switch (_ParentForm)
            {
                case Constants.ParentForms.Kuldemeny:
                    hasRight_Csatolmanyok = Kuldemenyek.CheckRights_Csatolmanyok(Page, ParentId, false);
                    break;
                case Constants.ParentForms.IraIrat:
                    hasRight_Csatolmanyok = Iratok.CheckRights_Csatolmanyok(Page, ParentId, false);
                    break;
                case Constants.ParentForms.IratPeldany:
                    hasRight_Csatolmanyok = IratPeldanyok.CheckRights_Csatolmanyok(Page, ParentId, false);
                    break;
            }

            //    ViewState["HasRight_Csatolmanyok"] = hasRight_Csatolmanyok;
            //}
            //else
            //{
            //    hasRight_Csatolmanyok = (Boolean)ViewState["HasRight_Csatolmanyok"];
            //}

            if (!hasRight_Csatolmanyok)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.ErrorCode_52792);
                ErrorUpdatePanel.Update();
                MainPanel.Visible = false;
                return;
            }
        }
        MainPanel.Visible = true;

        if (_ParentForm == Constants.ParentForms.Kuldemeny || _ParentForm == Constants.ParentForms.IraIrat
            || _ParentForm == Constants.ParentForms.IratPeldany || _ParentForm == Constants.ParentForms.Ugyirat)
        {
            #region LZS BLG_1636
            if (_ParentForm == Constants.ParentForms.Ugyirat)
            {
                if (CsatolmanyokGridView.Columns[4].HeaderText == "Azonosító")
                {
                    CsatolmanyokGridView.Columns[4].Visible = true;
                }
            }
            #endregion



            // itt kell a tiltast megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;
            //TabHeader1.ElektronikusAlairasVisible = true;

            //TabHeader1.ElektronikusAlairasEllenorzesVisible = true;

            // Aláírás gomb küldeménynél nincs, csak iratnál
            if ((Command == CommandName.Modify || Command == CommandName.View)
                && (_ParentForm == Constants.ParentForms.IraIrat
                || _ParentForm == Constants.ParentForms.IratPeldany)
                )
            {
                TabHeader1.ElektronikusAlairasVisible = true;
            }

            //TabHeader1.LinkView.Attributes["onclick"] = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.LinkView.Attributes["onclick"] = "downLoadDocument('" + CsatolmanyokGridView.ClientID + "',''); return false;";

            TabHeader1.LinkView.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabFooter1.ImageButton_Save.OnClientClick = "";
            TabFooter1.ImageButton_Save.CausesValidation = false;

            #region CR3129 
            // Aláírás gomb küldeménynél nincs, csak iratnál
            if (_ParentForm == Constants.ParentForms.IraIrat || _ParentForm == Constants.ParentForms.IratPeldany)
            {
                string procId = string.Empty;
                if (CreateAlairasFolyamat(ParentId, out procId))
                {
                    String noSelectedError = alairasCsakFoDok() ? "if(true) " : "if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} else ";
                    TabHeader1.ElektronikusAlairasOnClientClick = Iratok.SetClientOnClickAlairas(ParentId, Page);
                    //Netlock kliens oldali aláírás nem megy dialog ablakban
                    string pkiVendor = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "PKI_VENDOR");
                    bool noDialog = "NETLOCK".Equals(pkiVendor, StringComparison.InvariantCultureIgnoreCase);
                    TabHeader1.ElektronikusAlairasOnClientClick += "var count = getSelectedCheckBoxesCount('"
                       + CsatolmanyokGridView.ClientID + "','check'); " + noSelectedError + " {" +
                       JavaScripts.SetOnClientClick("DokumentumAlairas.aspx",
                        QueryStringVars.CsatolmanyId + "=" + "'+getIdsBySelectedCheckboxes('" + CsatolmanyokGridView.ClientID + "','check')+'" + "&" + QueryStringVars.ProcId + "=" + procId
                        , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolmanyokList, false, noDialog)
                       + "}";
                }
            }

            //TabHeader1.ElektronikusAlairasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            #endregion

            #region Aláírás visszavonása
            TabHeader1.ElektronikusAlairasVisszavonasOnClientClick = Iratok.SetClientOnClickAlairasVisszavonas(ParentId, Page);
            TabHeader1.ElektronikusAlairasVisszavonasOnClientClick += "var count = getSelectedCheckBoxesCount('"
               + CsatolmanyokGridView.ClientID + "','check'); if (count==0) {alert('"
               + Resources.List.UI_NoSelectedItem + "'); return false;} else {" +
               JavaScripts.SetOnClientClick("DokumentumAlairasVisszavonas.aspx",
                QueryStringVars.CsatolmanyId + "=" + "'+getIdsBySelectedCheckboxes('" + CsatolmanyokGridView.ClientID + "','check')+'"
                , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolmanyokList)
               + "}";
            #endregion

            TabHeader1.AlairasAdatokVisible = false;
            TabHeader1.AlairasAdatokOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            TabHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(CsatolmanyokGridView.ClientID);
            TabHeader1.UnLockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(CsatolmanyokGridView.ClientID);
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsatolmanyokGridView.ClientID);

            MellekletekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            MellekletekSubListHeader.ViewVisible = false;
            MellekletekSubListHeader.ModifyVisible = false;
            MellekletekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(MellekletekGridView.ClientID);

            TabHeader1.OCROnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            // View módban bizonyos gombok eltüntetése:
            if (Request.QueryString.Get(QueryStringVars.Command) == CommandName.View)
            {
                MellekletekSubListHeader.NewVisible = false;
                MellekletekSubListHeader.InvalidateVisible = false;
            }

            ClearForm();
            EFormPanel1.Visible = false;
            SubListPanel.Visible = true;

            CsatolmanyokGridViewBind();

            KimenoCsatolmanyokGridViewBind();

            #region Fodokumentum Warning
            Label_Warning_Fodokumentum.Text = "";
            Warning_Fodokumentum_Panel.Visible = false;

            // adatsorok száma (ha nincs találat, akkor is van egy láthatatlan sor, de akkor a dataKey értéke üres)
            bool hasDataRow = (CsatolmanyokGridView.Rows.Count > 0);
            if (CsatolmanyokGridView.Rows.Count == 1 && String.IsNullOrEmpty(CsatolmanyokGridView.DataKeys[0].Value.ToString()))
            {
                hasDataRow = false;
            }

            int countFodokumentum = GetFodokumentumCountFromGridView(CsatolmanyokGridView);
            if (countFodokumentum == 0)
            {
                if (hasDataRow)
                {
                    Label_Warning_Fodokumentum.Text = String.Format("{0}<br />{1}", Resources.Form.UI_Warning_NincsFodokumentum, Resources.Form.UI_Warning_PontosanEgyFodokumentum);
                    Warning_Fodokumentum_Panel.Visible = true;
                }
            }
            else if (countFodokumentum > 1 && _ParentForm != Constants.ParentForms.Ugyirat)
            {
                if (hasDataRow)
                {
                    Label_Warning_Fodokumentum.Text = String.Format("{0}<br />{1}", Resources.Form.UI_Warning_EgynelTobbFodokumentum, Resources.Form.UI_Warning_PontosanEgyFodokumentum);
                    Warning_Fodokumentum_Panel.Visible = true;
                }
            }
            #endregion Fodokumentum Warning

            pageView.SetViewOnPage(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel.Update();
            MainPanel.Visible = false;
        }
    }

    #endregion
    #region BLG 2947
    public string GetFelulvizsgalatEredmenye(string krt_dok_id)
    {
        string retValue = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(krt_dok_id))
            {
                KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam docExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                docExecParam.Record_Id = krt_dok_id;
                Result dokResult = dokService.Get(docExecParam);
                if (!dokResult.IsError)
                {
                    KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                    if (docRecord != null)
                    {
                        //AlairasFelulvizsgalatEredmeny
                        retValue = docRecord.AlairasFelulvizsgalatEredmeny;
                        //try
                        //{
                        //    var data = (JObject)JsonConvert.DeserializeObject(docRecord.AlairasFelulvizsgalatEredmeny);
                        //    string AlairasFelulvizsgalatEredmeny = data["AlairasFelulvizsgalatEredmeny"].Value<string>();
                        //    if (!string.IsNullOrEmpty(AlairasFelulvizsgalatEredmeny))
                        //        retValue = AlairasFelulvizsgalatEredmeny;
                        //}
                        //catch (Exception)
                        //{
                        //    retValue = docRecord.AlairasFelulvizsgalatEredmeny;
                        //}
                    }
                }
            }
        }
        catch (Exception x)
        {
            Logger.Error("GetFelulvizsgalatEredmenye ERROR", x);
        }

        return string.IsNullOrEmpty(retValue) ? getDefaultResultJSon() : retValue;
    }

    private String getDefaultResultJSon()
    {
        return "{ \"AlairasFelulvizsgalatEredmeny\":" + NeedSignCheck + "}";
    }

    private String getNetlockJSon(SignatureData data)
    {
        String retValue = getDefaultResultJSon();
        if (data != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"?xml\":{\"@version\":\"1.0\",\"@encoding\":\"ISO-8859-2\"},\"pdf\":{\"signatures\":{\"signature\":");
            var subj = data.CertificateSubject;
            if (subj != null)
            {
                sb.Append("{\"@signerDN\":");
                sb.AppendFormat("\"CN= { 0}", subj.CommonName);
                if (!string.IsNullOrEmpty(subj.Email))
                    sb.AppendFormat(",\"emailAddress= { 0}", subj.Email);
                if (!string.IsNullOrEmpty(subj.Organization))
                    sb.AppendFormat(",\"O= { 0}", subj.Organization);
                sb.Append("\"");
            }

            var issuer = data.CertificateIssuer;
            if (issuer != null)
            {
                sb.Append("{\"@issuer\":");
                sb.AppendFormat("\"CN= { 0}", issuer.CommonName);
                if (!string.IsNullOrEmpty(issuer.Email))
                    sb.AppendFormat(",\"emailAddress= { 0}", issuer.Email);
                if (!string.IsNullOrEmpty(issuer.Organization))
                    sb.AppendFormat(",\"O= { 0}", issuer.Organization);
                sb.Append("\"");
            }
            if (data.CertificateValidityFrom != null)
            {
                sb.AppendFormat("{\"@issuerValidFrom\":\" { 0}\"", data.CertificateValidityFrom);
            }
            if (data.CertificateValidityTo != null)
            {
                sb.AppendFormat("{\"@issuerValidTo\":\" { 0}\"", data.CertificateValidityTo);
            }
            if (data.TimeStampDate != null)
            {
                sb.AppendFormat("{\"@timeStampDate\":\" { 0}\"", data.TimeStampDate);
            }

            retValue = sb.ToString();
        }
        return retValue;
    }

    private void CheckSign(List<String> selectedItems)
    {
        if (krt_Parameterek_PKI_INTEGRACIO == "Igen")
        {
            string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
            if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "halk".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "microsec".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
            {
                if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    checkNetLockSign(selectedItems);
                }
                else if ("halk".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    checkHALKSign(selectedItems);
                }
                else if ("microsec".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    checkMicroSecSign(selectedItems);
                }
                //ReLoadTab();
            }

        }
    }

    private void checkNetLockSign(List<String> selectedItems)
    {
        foreach (String recordId in selectedItems)
        {
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_CsatolmanyokService erec_CsatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
            erec_CsatolmanyokSearch.Id.Filter(recordId);

            Result result = erec_CsatolmanyokService.GetAllWithExtension(execparam, erec_CsatolmanyokSearch);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            if (result.GetCount > 0)
            {
                string dokumentumId = result.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString();

                KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                dokExecParam.Record_Id = dokumentumId;
                Result dokResult = dokService.Get(dokExecParam);
                if (!dokResult.IsError)
                {
                    KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;
                    //LoadComponentsFromBusinessObjects_DokumentumVizsgalat(krt_Dokumentumok);

                    bool AlairasChanged = false;
                    //TESZT
                    String alairasFelulvizsgalatEredmeny = String.Empty;
                    var netlockService = new NetLockSignAssistService();
                    try
                    {
                        // csatolmány letöltése
                        var content = NetLockHelper.DownloadDocument(krt_Dokumentumok.External_Link, UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                        AlairasAdatok_EFormPanel.Visible = true;

                        // aláírás validálása
                        var validationReportJson = netlockService.Validate(krt_Dokumentumok.FajlNev, content);
                        Logger.Debug(validationReportJson);

                        var validationReport = NetLockHelper.DeserializeValidation(validationReportJson);
                        validationReport.Dump();

                        // érvényes aláírások
                        var validSignatures = SignatureData.ParseNetLockSignAssistValidationReport(validationReport, SignatureReport.IndicationEnum.VALIDEnum);
                        if (validSignatures.Count > 0)
                        {
                            var signatureData = validSignatures[0]; // TODO: handle multiple valid signatures!?
                            alairasFelulvizsgalatEredmeny = getNetlockJSon(signatureData);
                            Label_FajlAdatok.Text = signatureData.ToHTML(null);
                            AlairasChanged = krt_Dokumentumok.ElektronikusAlairas != KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;
                        }
                        // érvénytelen aláírások
                        else
                        {
                            var invalidSignatures = SignatureData.ParseNetLockSignAssistValidationReport(validationReport, SignatureReport.IndicationEnum.INVALIDEnum);
                            if (invalidSignatures.Count > 0)
                            {
                                // TODO: handle invalid signatures
                                alairasFelulvizsgalatEredmeny = getDefaultResultJSon();
                                AlairasChanged = krt_Dokumentumok.ElektronikusAlairas != KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles;
                            }
                        }
                    }
                    catch (Exception x)
                    {
                        Label_FajlAdatok.Text = NeedSignCheck;
                        var errorMessage = x.Message;
                        Logger.Warn("NETLOCKSignatureService hiba: " + errorMessage);

                        alairasFelulvizsgalatEredmeny = getDefaultResultJSon();
                        AlairasChanged = krt_Dokumentumok.ElektronikusAlairas != KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles;
                    }

                    if (AlairasChanged)
                    {
                        krt_Dokumentumok.Updated.SetValueAll(false);
                        krt_Dokumentumok.Base.Updated.SetValueAll(false);

                        krt_Dokumentumok.Base.Updated.Ver = true;

                        krt_Dokumentumok.AlairasFelulvizsgalat = DateTime.Now.ToString();
                        krt_Dokumentumok.Updated.AlairasFelulvizsgalat = true;

                        krt_Dokumentumok.AlairasFelulvizsgalatEredmeny = alairasFelulvizsgalatEredmeny;
                        krt_Dokumentumok.Updated.AlairasFelulvizsgalatEredmeny = true;

                        Result result_alairas = dokService.Update(dokExecParam, krt_Dokumentumok);
                        if (!result_alairas.IsError)
                        {
                            CsatolmanyokGridViewBind_WithReselectRow();
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_alairas);
                            ErrorUpdatePanel.Update();
                        }
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, dokResult);
                    ErrorUpdatePanel.Update();
                }
            }
        }
    }

    private void checkHALKSign(List<String> selectedItems)
    {
        //HALK runapp meghívása verify metódussal
        Contentum.eRecord.Service.EREC_Alairas_FolyamatokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();
        EREC_Alairas_Folyamatok erec_AlairasFolyamatok = new EREC_Alairas_Folyamatok();

        erec_AlairasFolyamatok.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.folyamat_rogzitve;
        ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);
        Result result_Insert = service.Insert(execParam_Insert, erec_AlairasFolyamatok);
        if (result_Insert.IsError)
        {
            Logger.Error("Hiba: EREC_Alairas_FolyamatokService.Insert", execParam_Insert, result_Insert);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert);
            return;
        }
        string procId = result_Insert.Uid;
        int rowInd = 0;
        foreach (String recordId in selectedItems)
        {
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_CsatolmanyokService erec_CsatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
            erec_CsatolmanyokSearch.Id.Value = recordId;
            erec_CsatolmanyokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = erec_CsatolmanyokService.GetAllWithExtension(execparam, erec_CsatolmanyokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            string IraIrat_Id = string.Empty;

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                IraIrat_Id = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();
            }

            //if (string.IsNullOrEmpty(IraIrat_Id))
            //{
            //    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.IratNotFound);
            //    ErrorUpdatePanel.Update();
            //    return;
            //}

            #region createAlairasFolyamatTetelek
            Contentum.eRecord.Service.EREC_Alairas_Folyamat_TetelekService tetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
            EREC_Alairas_Folyamat_Tetelek erec_Alairas_Folyamat_Tetelek = new EREC_Alairas_Folyamat_Tetelek();
            erec_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id = procId;
            erec_Alairas_Folyamat_Tetelek.Csatolmany_Id = recordId;
            erec_Alairas_Folyamat_Tetelek.IraIrat_Id = IraIrat_Id;
            erec_Alairas_Folyamat_Tetelek.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.folyamat_rogzitve;
            ExecParam execParam_InsertTetel = Contentum.eUtility.UI.SetExecParamDefault(Page);
            Result result_Insert_Tetel = tetelekService.Insert(execParam_InsertTetel, erec_Alairas_Folyamat_Tetelek);
            if (result_Insert_Tetel.IsError)
            {
                Logger.Error("Hiba: EREC_Alairas_Folyamat_TetelekService.Insert", execParam_InsertTetel, result_Insert_Tetel);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert_Tetel);
                return;
            }
            #endregion

            rowInd++;

        }

        var queryString = "Proc_Id=" + procId + "&Felh_Id=" + FelhasznaloProfil.FelhasznaloId(Page) + "&FelhSz_Id=" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page) + "&Muvelet=Verify";
        string url = UI.GetAppSetting("RunHalkAppUrl") + "?" + queryString;
        //string javascript = String.Format("window.open('{0}');return false;", url);
        //Page.ClientScript.RegisterStartupScript(this.GetType(),"callCheck",javascript,true);

        halkProcessId.Value = procId;

        //var client = new WebClient();
        //var content = client.DownloadString(url);
        //string js = String.Format("checkStatus();window.open('{0}');return false;", url);
        string js = String.Format("checkStatus();window.open('{0}');", url);
        //string onClientClick = String.Format("alert('1');");
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallHALK", onClientClick, true);

        //string redirect = String.Format("<script>window.open('{0}');</script>", url);
        //Response.Write(redirect);

        //string js = JavaScripts.SetOnClientClickWithTimeout("DokumentumAlairas.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, TabStatusUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
        //string js = JavaScripts.SetOnClientClickWithTimeout(UI.GetAppSetting("RunHalkAppUrl"), queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, TabStatusUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HALKRunApp", js, true);
    }

    private void checkMicroSecSign(List<String> selectedItems)
    {
        MicroSigner1.iratIds = new List<string>();
        MicroSigner1.fileNames = new List<string>();
        MicroSigner1.externalLinks = new List<string>();
        MicroSigner1.recordIds = new List<string>();
        MicroSigner1.folyamatTetelekIds = new List<string>();
        MicroSigner1.csatolmanyokIds = new List<string>();

        #region set MicroSigner
        Contentum.eRecord.Service.EREC_Alairas_FolyamatokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();
        EREC_Alairas_Folyamatok erec_AlairasFolyamatok = new EREC_Alairas_Folyamatok();

        erec_AlairasFolyamatok.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.folyamat_rogzitve;
        ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);
        Result result_Insert = service.Insert(execParam_Insert, erec_AlairasFolyamatok);
        if (result_Insert.IsError)
        {
            Logger.Error("Hiba: EREC_Alairas_FolyamatokService.Insert", execParam_Insert, result_Insert);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert);
            return;
        }
        string procId = result_Insert.Uid;
        int rowInd = 0;
        foreach (String recordId in selectedItems)
        {
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_CsatolmanyokService erec_CsatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
            erec_CsatolmanyokSearch.Id.Value = recordId;
            erec_CsatolmanyokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = erec_CsatolmanyokService.GetAllWithExtension(execparam, erec_CsatolmanyokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            string IraIrat_Id = string.Empty;
            string docId = string.Empty;

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                IraIrat_Id = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();
                docId = result.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString();
            }

            //if (string.IsNullOrEmpty(IraIrat_Id))
            //{
            //    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.IratNotFound);
            //    ErrorUpdatePanel.Update();
            //    return;
            //}
            string filename = string.Empty;
            string externalLink = string.Empty;

            Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam ep = execparam.Clone();
            if (!string.IsNullOrEmpty(docId))
            {
                ep.Record_Id = docId;
                char jogszint = 'O';

                Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                if (dokGetResult.IsError)
                {
                    //throw new Contentum.eUtility.ResultException(dokGetResult);
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, dokGetResult.ErrorMessage);
                    ErrorUpdatePanel.Update();
                    return;
                }

                externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
            }


            #region createAlairasFolyamatTetelek
            Contentum.eRecord.Service.EREC_Alairas_Folyamat_TetelekService tetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
            EREC_Alairas_Folyamat_Tetelek erec_Alairas_Folyamat_Tetelek = new EREC_Alairas_Folyamat_Tetelek();
            erec_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id = procId;
            erec_Alairas_Folyamat_Tetelek.Csatolmany_Id = recordId;
            erec_Alairas_Folyamat_Tetelek.IraIrat_Id = IraIrat_Id;
            erec_Alairas_Folyamat_Tetelek.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.folyamat_rogzitve;
            ExecParam execParam_InsertTetel = Contentum.eUtility.UI.SetExecParamDefault(Page);
            Result result_Insert_Tetel = tetelekService.Insert(execParam_InsertTetel, erec_Alairas_Folyamat_Tetelek);
            if (result_Insert_Tetel.IsError)
            {
                Logger.Error("Hiba: EREC_Alairas_Folyamat_TetelekService.Insert", execParam_InsertTetel, result_Insert_Tetel);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert_Tetel);
                return;
            }

            MicroSigner1.iratIds.Add(IraIrat_Id);
            MicroSigner1.fileNames.Add(filename);
            MicroSigner1.externalLinks.Add(externalLink);
            MicroSigner1.recordIds.Add(docId);
            MicroSigner1.folyamatTetelekIds.Add(result_Insert_Tetel.Uid);
            MicroSigner1.csatolmanyokIds.Add(recordId);
            MicroSigner1.procId = procId;
            #endregion

            rowInd++;

        }
        #endregion

        MicroSigner1.StartNewVerifySession();
    }

    #region BLG 6473 szerver oldali aláírás
    private void runServerSideSign(List<String> selectedItems)
    {

    }
    #endregion

    #region HALK ellenõrzés folyamat állapotának visszajelzése
    [System.Web.Services.WebMethod]
    public static HALKStatusz GetHALKStatusz(string processId, string lastStatusGetTime)
    {
        try
        {
            const string dtFormat = "yyyy.MM.dd HH.mm.ss.fff";
            DateTime currentStatusGetTime = DateTime.Now;
            DateTime? lastStatusGetTimeDt = null;
            DateTime dtSeged;
            if (DateTime.TryParseExact(lastStatusGetTime, dtFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AllowWhiteSpaces
                    , out dtSeged))
            {
                lastStatusGetTimeDt = dtSeged;
            }

            string errorString = string.Empty;
            string statusString = string.Empty;

            bool finished = HALKProcessFinished(processId, out errorString, out statusString);

            var statusz = new HALKStatusz()
            {
                BackgroundProcessEnded = finished,
                BackgroundProcessError = errorString,
                BackgroundProcessStatus = statusString,
                StatusGetTime = currentStatusGetTime.ToString(dtFormat)
            };

            return statusz;
        }
        catch (Exception exc)
        {
            Logger.Error(exc.ToString());

            return new HALKStatusz()
            {
                BackgroundProcessError = exc.Message,
                BackgroundProcessEnded = true
            };
        }
    }

    public static bool HALKProcessFinished(string processId, out string errorString, out string statusString)
    {
        errorString = string.Empty;
        statusString = string.Empty;

        try
        {
            EREC_Alairas_FolyamatokService folyamatoService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();

            ExecParam ep = UI.SetExecParamDefault(HttpContext.Current.Session);
            ep.Record_Id = processId;

            Result resultAFGet = folyamatoService.Get(ep);
            if (!String.IsNullOrEmpty(resultAFGet.ErrorCode))
            {
                //hiba lépett fel, de a folyamat végére ért
                errorString = resultAFGet.ErrorMessage;
                return true;
            }
            EREC_Alairas_Folyamatok eREC_Alairas_Folyamatok = (EREC_Alairas_Folyamatok)resultAFGet.Record;
            switch (eREC_Alairas_Folyamatok.AlairasStatus)
            {
                case KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.ellenorzes_folyamatban: statusString = "Ellenõrzés folyamatban..."; break;
                case KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.folyamat_rogzitve: statusString = "Folyamat rögzítve..."; break;
                case KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes: statusString = "Sikeres ellenõrzés..."; break;
                case KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes: statusString = "Sikertelen ellenõrzés..."; break;
                default: statusString = "Ismeretlen állapot..."; break;
            }
            if (eREC_Alairas_Folyamatok.AlairasStatus == KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas || eREC_Alairas_Folyamatok.AlairasStatus == KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas)
            {
                return true;
            }
        }
        catch (Exception exc)
        {
            Logger.Error("CsatolmanyokTab.ascx HALKProcessFinished(" + processId + ") error:" + exc.Message);
            errorString = exc.Message;
            return true;
        }

        return false;
    }
    #endregion
    #endregion

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();

        //Command = e.CommandName;

        TabFooter1.CommandArgument = e.CommandName; ;

        // Elektronikus aláírás és állomány adatok panelek láthatóság kezelése
        switch (e.CommandName)
        {
            case CommandName.New:
                {
                    AllomanyAdatokPanel.Visible = true;
                    ElektronikusAlairasPanel.Visible = true;

                    // Aláírásdropdown feltöltése:
                    if (krt_Parameterek_PKI_INTEGRACIO == "Igen")
                    {
                        //nekrisz CR 3015 : PKI_integrációná a régi ellenörzés mellõzése
                        string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                        if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "microsec".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "halk".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                        {
                            ElektronikusAlairasPanel.Visible = false;
                        }
                        else
                        {
                            FillAlairasSzabalyDropDownList();
                        }
                    }

                    break;
                }
            case CommandName.View:
                AllomanyAdatokPanel.Visible = true;
                ElektronikusAlairasPanel.Visible = false;
                break;
            case CommandName.Modify:
                AllomanyAdatokPanel.Visible = true;

                if (krt_Parameterek_PKI_INTEGRACIO == "Nem")
                {
                    /// ha nincs PKI integráció, manuálisan lehet berögzíteni, hogy van-e aláírás
                    /// ilyenkor ezt lehet módosítani is:
                    ElektronikusAlairasPanel.Visible = true;
                }
                else
                {
                    ElektronikusAlairasPanel.Visible = false;
                }
                break;
            case CommandName.AlairasEllenorzes:
                AllomanyAdatokPanel.Visible = false;
                //BLG 2947
                List<String> selectedItems = ui.GetGridViewSelectedRows(CsatolmanyokGridView, EErrorPanel1, ErrorUpdatePanel);
                if (selectedItems != null && selectedItems.Count > 0)
                {
                    CheckSign(selectedItems);
                }
                else
                {
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel.Update();
                }

                //ElektronikusAlairasPanel.Visible = true;
                break;
            case CommandName.ElektronikusAlairas:
                {
                    AllomanyAdatokPanel.Visible = false;
                    break;
                }
            case CommandName.Lock:
                {
                    LockSelectedDokumentumRecords();
                    CsatolmanyokGridViewBind();
                    break;
                }
            case CommandName.Unlock:
                {
                    UnlockSelectedDokumentumRecords();
                    CsatolmanyokGridViewBind();
                    break;
                }
            case CommandName.OCR:
                {
                    OCRSelectedDokumentumRecords();
                    CsatolmanyokGridViewBind();
                    break;
                }
            default:
                AllomanyAdatokPanel.Visible = false;
                //LZS - BLG_4391 - Visszaállítjuk a File feltöltés sor láthatóságát.
                FileUploadRow.Visible = true;
                //ElektronikusAlairasPanel.Visible = false;
                break;
        }

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
            // Alsó listát eltüntetjük, mert kevés a hely
            SubListPanel.Visible = false;

            //FileUploadComponent1.Validate = true;
            //FileUploadComponent1.Enabled = true;
            FileUpload1.Enabled = true;
            LabelAllomany_Csillag.Visible = true;

            FileUpload1.Visible = true;
            FileUploadComponent1.Visible = false;

            //LZS - BLG_4391 - Beállítjuk új csatolmány felvételénél a láthatóságokat. Az újonnan belekerült elemeket elrejtjük,
            //az új felvitelhez szükséges controlokat láthatóvá teszzük.
            AllomanyAdatokPanel.Visible = true;
            CsatolmanyokListboxRow.Visible = false;
            FileUploadRow.Visible = true;
            KodtarakDropDownList_DokumentumSzerep.ReadOnly = false;
            CsatolmanyokListboxLabelRow.Visible = false;
            btnFodokumentumSelect.Visible = false;
            lblErrorMessage.Visible = false;

        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {

            EFormPanel1.Visible = true;
            SubListPanel.Visible = false;

            //FileUploadComponent1.Validate = false;
            //FileUploadComponent1.Enabled = false;
            FileUpload1.Enabled = false;
            LabelAllomany_Csillag.Visible = false;

            String RecordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel.Update();
            }
            else
            {
                // dokumentum vizsgálathoz minden típus esetén aktualizálni kell
                // a KRT_Dokumentumok tábla rekordját is
                string krtDokumentumId = String.Empty;

                // nyilvántartjuk, hogy sikerült-e beolvasni a fõ (küldemény, irat, iratpéldány) rekordot
                bool voltHiba = false;

                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_Csatolmanyok erec_Csatolmanyok = (EREC_Csatolmanyok)result.Record;
                    LoadComponentsFromBusinessObject(erec_Csatolmanyok);
                    // dokumentum vizsgálathoz
                    krtDokumentumId = erec_Csatolmanyok.Dokumentum_Id;
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();

                    voltHiba = true;
                }


                if (!voltHiba)
                {
                    // dokumentum vizsgálat adatai
                    KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                    ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    dokExecParam.Record_Id = krtDokumentumId;
                    Result dokResult = dokService.Get(dokExecParam);
                    if (String.IsNullOrEmpty(dokResult.ErrorCode))
                    {
                        KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;
                        LoadComponentsFromBusinessObjects_DokumentumVizsgalat(krt_Dokumentumok);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, dokResult);
                        ErrorUpdatePanel.Update();
                    }

                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            OnHeaderDeleteButtonClicked();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
        if (e.CommandName == CommandName.ElektronikusAlairas)
        {

        }

        //BLG 2947 Aláírás ellenõrzése tömegesen külön fügvénnyel
        //if (e.CommandName == CommandName.AlairasEllenorzes)
        //{
        //    // hasonló lépések, mint a módosításnál, de a fõ objektumot
        //    // (ügyirat, irat, példány...) nem töltük fel, csak a KRT_Dokumentumok
        //    // objektum azonosításához szükséges

        //    EFormPanel1.Visible = true;

        //    //FileUploadComponent1.Validate = false;
        //    //FileUploadComponent1.Enabled = false;
        //    FileUpload1.Enabled = false;

        //    LabelAllomany_Csillag.Visible = false;

        //    String RecordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);

        //    if (String.IsNullOrEmpty(RecordId))
        //    {
        //        // nincs Id megadva:
        //        ResultError.DisplayNoIdParamError(EErrorPanel1);
        //        ErrorUpdatePanel.Update();
        //    }
        //    else
        //    {
        //        // dokumentum vizsgálathoz minden típus esetén aktualizálni kell
        //        // a KRT_Dokumentumok tábla rekordját is
        //        String krtDokumentumId = String.Empty;

        //        // nyilvántartjuk, hogy sikerült-e beolvasni a fõ (küldemény, irat, iratpéldány) rekordot
        //        bool voltHiba = false;

        //        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        //        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParam.Record_Id = RecordId;

        //        Result result = service.Get(execParam);
        //        if (String.IsNullOrEmpty(result.ErrorCode))
        //        {
        //            EREC_Csatolmanyok erec_Csatolmanyok = (EREC_Csatolmanyok)result.Record;
        //            // dokumentum vizsgálathoz
        //            krtDokumentumId = erec_Csatolmanyok.Dokumentum_Id;
        //        }
        //        else
        //        {
        //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //            ErrorUpdatePanel.Update();

        //            voltHiba = true;
        //        }


        //        if (!voltHiba)
        //        {
        //            // dokumentum vizsgálat adatai
        //            KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        //            ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //            dokExecParam.Record_Id = krtDokumentumId;
        //            Result dokResult = dokService.Get(dokExecParam);
        //            if (String.IsNullOrEmpty(dokResult.ErrorCode))
        //            {
        //                KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;
        //                // TODO: automatikus ellenõrzés kezelése
        //                LoadComponentsFromBusinessObjects_DokumentumVizsgalat(krt_Dokumentumok);
        //            }
        //            else
        //            {
        //                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, dokResult);
        //                ErrorUpdatePanel.Update();
        //            }

        //        }
        //    }
        //}
    }

    private void OnHeaderDeleteButtonClicked()
    {
        if (_ParentForm == Constants.ParentForms.Kuldemeny && !FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmanyInvalidate")
          || ((_ParentForm == Constants.ParentForms.IraIrat
              || _ParentForm == Constants.ParentForms.IratPeldany) && !FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmanyInvalidate")))
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            return;
        }
        List<string> selectedItemsIDs = ui.GetGridViewSelectedRows(CsatolmanyokGridView, EErrorPanel1, ErrorUpdatePanel);
        bool areAllDocumentSelected = selectedItemsIDs.Count == this.CsatolmanyokGridView.Rows.Count;
        bool needsConfirm = CheckIfNeedsLastDocumentDeleteConfirm(selectedItemsIDs);
        if (needsConfirm)
        {
            return;
        }

        //LZS - BLG_4391 - Ha a "Fődokumentum" törlésre került, akkor kilistázzuk a megmaradtakat egy listbox-ba, és onnan lehet
        //majd kiválasztani, hogy melyik legyen ezt követően a fődokumentum.
        string foDokumentumID = GetFodokumentumID(selectedItemsIDs);
        if (foDokumentumID != null && !areAllDocumentSelected)//fődokumentum is kivan választva de van másik olyan dokumentum amit még kilehet választani mint fődokumentum
        {
            OldFodokumentumID = foDokumentumID; //ha kiválasztásra kerül az új fődokumentum akkor ezt a régit töröljük ez az ID alapján 
            ShowSelectAlternativeFodokumentum();
            return;
        }

        DeleteCsatolmanyok(selectedItemsIDs);
        //csak akkor töröljük az aláírókat ha nem marad egyetlen csatolmány se
        if (selectedItemsIDs.Count == this.CsatolmanyokGridView.Rows.Count)
        {
            InvalidateAlairok(GetAlairok());
        }

        CsatolmanyokGridViewBind();

        string csatolmanyId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);// BUG_4080
        ActiveTabRefresh(TabContainer1, csatolmanyId);
    }

    /// <summary>
    /// Törli a CsatolmanyokGridView -ban kijelölt elemeket és visszadja melyek voltak azok
    /// </summary>
    private void DeleteCsatolmanyok(List<string> deletableItemsList)
    {
        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        for (int i = 0; i < deletableItemsList.Count; i++)
        {
            execParams[i] = UI.SetExecParamDefault(Page, UI.SetExecParamDefault(Page));
            execParams[i].Record_Id = deletableItemsList[i];
        }
        Result result = service.MultiInvalidate(execParams);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
    }

    //Ha a törölt IDk között van fődokumentum akkor felkinálja hogy újat kell választani
    private string GetFodokumentumID(List<string> selectedIds)
    {
        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch eREC_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
        string ids = "";
        bool isFodokumentumDeleted = false;
        string fodokumentumID = string.Empty;
        eREC_CsatolmanyokSearch.Id.Value = Search.GetSqlInnerString(selectedIds.ToArray());
        eREC_CsatolmanyokSearch.Id.Operator = Query.Operators.inner;

        Result csatolmanyokRes = service.GetAll(UI.SetExecParamDefault(Page, UI.SetExecParamDefault(Page)), eREC_CsatolmanyokSearch);

        if (!csatolmanyokRes.IsError)
        {
            foreach (DataRow row in csatolmanyokRes.Ds.Tables[0].Rows)
            {
                if (row["DokumentumSzerep"].ToString() == KodTarak.DOKUMENTUM_SZEREP.Fodokumentum)
                {
                    isFodokumentumDeleted = true;
                    fodokumentumID = row["Id"].ToString();
                    break;
                }
            }
        }
        if (!isFodokumentumDeleted)
        {
            return null;
        }
        return fodokumentumID;
    }

    public void ShowSelectAlternativeFodokumentum()
    {

        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch eREC_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();

        //Irat Id lekérése:
        string IraIratId = Request.QueryString["Id"].ToString();

        //Csatolmányok lekérése:
        eREC_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
        eREC_CsatolmanyokSearch.IraIrat_Id.Value = IraIratId;
        eREC_CsatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

        Result csatolmanyokRes = service.GetAllWithExtension(UI.SetExecParamDefault(Page, UI.SetExecParamDefault(Page)), eREC_CsatolmanyokSearch);

        if (!csatolmanyokRes.IsError)
        {
            //Csatolmányok betöltése a ListBox-controll-ba:
            foreach (DataRow row in csatolmanyokRes.Ds.Tables[0].Rows)
            {
                ListItem item = new ListItem()
                {
                    Text = row["FajlNev"].ToString(),
                    Value = row["Id"].ToString()
                };

                CsatolmanyokListbox.Items.Add(item);
            }
        }

        //LZS - BLG_4391 - Control láthatóságok beállítása
        //Ha maradt kiválasztható dikumentum
        if (csatolmanyokRes.Ds.Tables[0].Rows.Count > 0)
        {
            AllomanyAdatokPanel.Visible = true;
            CsatolmanyokListboxRow.Visible = true;
            FileUploadRow.Visible = false;
            //Fixen beállítjuk a "Fődokumentum" szerepkört, ezen nem is lehet változtatni itt.
            KodtarakDropDownList_DokumentumSzerep.SetSelectedValue("01");
            KodtarakDropDownList_DokumentumSzerep.ReadOnly = true;
            CsatolmanyokListboxLabelRow.Visible = true;
            EFormPanel1.Visible = true;
            btnFodokumentumSelect.Visible = true;
        }
    }
    private string OldFokdumentumIDViewKey = "OldFodokumentumID";
    private string OldFodokumentumID
    {
        get
        {
            var obj = ViewState[OldFokdumentumIDViewKey];
            if (obj != null) { return obj as string; }
            return string.Empty;
        }
        set { ViewState.Add(OldFokdumentumIDViewKey, value); }
    }

    private void ResetAlairas()
    {
        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        var result = service.AlairasAlapallapotbaHelyezes(UI.SetExecParamDefault(Page), ParentId);
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        if (OldFodokumentumID != string.Empty)
        {
            DeleteCsatolmanyok(new List<string> { OldFodokumentumID });
        }
    }

    private bool CheckIfNeedsLastDocumentDeleteConfirm(List<string> selectedIds)
    {
        //Ha a törlés után nem marad egy dokumentum se akkor meg kell kérdezni, hogy biztos törli e mert akkor az aláírótól is törlésre kerül
        if (selectedIds.Count != this.CsatolmanyokGridView.Rows.Count) { return false; }
        //ha a nem 3 as értéke akkor se kell
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.ALAIROK_CSATOLMANY_NELKUL) != "3") { return false; }
        //ha nincs le van okézve akkro már nem kell dialog
        if (hf_ConfirmLastDokumentDelete.Value == "ok") { return false; }
        var alairok = GetAlairok();
        //ha nincs alairo akkor se kell
        if (alairok.Count == 0) { return false; }

        RegisterConfirmDialog("Biztosan törlöd az utolso csatolmányt is? Ha igen akkor az aláírótól is törlésre fog kerülni",
            "confirmLastCsatolmanyDelete",
            hf_ConfirmLastDokumentDelete,
            this.TabHeader1.ImageInvalidate);

        return true;
    }

    private void InvalidateAlairok(List<EREC_IratAlairok> alairok)
    {
        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        var execParams = new List<ExecParam>();
        foreach (var alairo in alairok)
        {
            var param = UI.SetExecParamDefault(Page);
            param.Record_Id = alairo.Id;
            execParams.Add(param);
        }

        Result invalidateResult = service.MultiInvalidate(execParams.ToArray());
        if (invalidateResult.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, invalidateResult);
            ErrorUpdatePanel.Update();
        }
    }
    private EREC_PldIratPeldanyok GetIratPeldany()
    {
        try
        {
            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
            execParam_PldGet.Record_Id = ParentId;
            Result result = service_Pld.Get(execParam_PldGet);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return null;
            }

            return (EREC_PldIratPeldanyok)result.Record;
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
            ErrorUpdatePanel.Update();
            return null;
        }
    }

    private EREC_UgyUgyiratok GetUgyirat()
    {
        try
        {
            EREC_UgyUgyiratokService service_Pld = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
            execParam_PldGet.Record_Id = ParentId;
            Result result = service_Pld.Get(execParam_PldGet);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return null;
            }

            return (EREC_UgyUgyiratok)result.Record;
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
            ErrorUpdatePanel.Update();
            return null;
        }
    }

    private EREC_IraIratok GetIrat()
    {
        try
        {
            EREC_IraIratokService service_Pld = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
            execParam_PldGet.Record_Id = ParentId;
            Result result = service_Pld.Get(execParam_PldGet);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return null;
            }

            return (EREC_IraIratok)result.Record;
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
            ErrorUpdatePanel.Update();
            return null;
        }
    }
    private List<EREC_IratAlairok> GetAlairok()
    {
        var alairok = new List<EREC_IratAlairok>();
        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();
        search.Obj_Id.Value = this.ParentId;
        search.Obj_Id.Operator = Query.Operators.equals;
        search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
        search.ErvVege.Operator = Query.Operators.greater;
        search.Allapot.Value = "1";//aláírandó
        search.Allapot.Operator = Query.Operators.equals;
        //search.AlairasMod.Value = "M_UTO";//Utolag regisztráltak nem kellenek
        //search.AlairasMod.Operator = Query.Operators.notequals;
        search.OrderBy = "AlairasSorrend  DESC";
        Result result = service.GetAll(UI.SetExecParamDefault(Page), search);
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
            return alairok;
        }
        if (result.Ds.Tables[0].Rows.Count == 0)
        {
            return alairok;
        }
        foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
        {
            EREC_IratAlairok alairo = new EREC_IratAlairok();
            Utility.LoadBusinessDocumentFromDataRow(alairo, row);
            alairok.Add(alairo);
        }
        return alairok;
    }

    private void RegisterConfirmDialog(string question, string functionName, HiddenField hiddenFieldForResult, ImageButton buttonFOrPostBackEventReference)
    {
        string js = @"
                                                function " + functionName + @"()  {
                                                if (confirm('" + question + "') == true) { $get('"
        + hiddenFieldForResult.ClientID + "').value = 'ok';  "
        + "window.setTimeout(\"" + Page.ClientScript.GetPostBackEventReference(buttonFOrPostBackEventReference, "") + "\",500);" +
        @"} else {}
                                                }"
        + " function pageLoad() { if(" + functionName + " != null){" + functionName + "();" + functionName + " = null;} } ";

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), functionName + "Script", js, true);
    }


    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;

        // Az AlairasEllenorzes es az ElektronikusAlairas funkciojogokat
        // jelenleg a csatolmanyok modositasi jogahoz kotjuk
        String CheckRightCommand = "";
        switch (SubCommand)
        {
            case CommandName.AlairasEllenorzes:
            case CommandName.ElektronikusAlairas:
                CheckRightCommand = CommandName.Modify;
                break;
            default:
                CheckRightCommand = SubCommand;
                break;
        }

        if (e.CommandName == CommandName.Save)
        {
            // BUG_7470
            List<string> iratpldIds = new List<string>();
            String firstIratpld = String.Empty;

            // Szükség van-e a fõdokumentum ellenõrzére
            bool bCheckFodokumentum = IsCheckFodokumentumNecessary();

            // TODO: Altalanos hiba kezeles mindket modulhoz:

            Result result = new Result();

            Csatolmany csatolmany = new Csatolmany();

            bool fileIsAlreadyOnTheServer = false;
            string randomTempDirectoryName = String.Empty;

          
            if (SubCommand == CommandName.New)
            {
                #region File ellenõrzése

                // A fájl már fel lett-e töltve, vagy ez egy új feltöltés:
                if (FileUploadComponent1.Visible
                    && !string.IsNullOrEmpty(FileUploadComponent1.HiddenField.Value)
                    && !FileUpload1.HasFile)
                {
                    fileIsAlreadyOnTheServer = true;
                }


                // ellenõrzés, van-e feltöltött fájl
                if (!fileIsAlreadyOnTheServer && FileUpload1.HasFile == false)
                {
                    ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UINoSelectedFile);
                    ErrorUpdatePanel.Update();
                    return;
                }

                #region BUG 7471 /4 - feltöltés ellenõrzés fõdokumentumnál

                if (alairasCsakFoDok())
                {
                    if (FileUpload1.HasFile)
                    {
                        String fileName = FileUpload1.FileName;
                        //a "." is visszajön a GetExtension -nal
                        String extension = Path.GetExtension(fileName).Replace(".", "").ToLower();

                        String IraIrat_Id = "";
                        if (ParentForm == Constants.ParentForms.Kuldemeny)
                        {
                            EREC_KuldKuldemenyekService service_Kuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam_KuldGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_KuldGet.Record_Id = ParentId;

                            Result result_KuldGet = service_Kuld.Get(execParam_KuldGet);
                            if (String.IsNullOrEmpty(result_KuldGet.ErrorCode))
                            {
                                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_KuldGet.Record;
                                IraIrat_Id = erec_KuldKuldemenyek.IraIratok_Id;
                            }
                        }
                        else if (ParentForm == Constants.ParentForms.IraIrat)
                        {
                            IraIrat_Id = ParentId;
                        }
                        else if (ParentForm == Constants.ParentForms.IratPeldany)
                        {
                            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_PldGet.Record_Id = ParentId;

                            Result result_PldGet = service_Pld.Get(execParam_PldGet);
                            if (String.IsNullOrEmpty(result_PldGet.ErrorCode))
                            {
                                EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;
                                IraIrat_Id = erec_PldIratPeldanyok.IraIrat_Id;
                            }

                        }
                        bool isElektronikus = IsElektronikus(IraIrat_Id);
                        //ebben az esetben mindegy a dokszerep
                        if (!isElektronikus && isKrFile(extension))

                        {
                            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, KrUploadErrorCsakEl);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        if (KodtarakDropDownList_DokumentumSzerep.SelectedValue == KodTarak.DOKUMENTUM_SZEREP.Fodokumentum)
                        {

                            /*if (!IsPkiFile(extension, isElektronikus))
                            {
                                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, FodokUploadError);
                                ErrorUpdatePanel.Update();
                                return;
                            }*/
                            //BUG_9118 - fõdokumantum feltöltése:
                            //-Elektronikus iratnál: pdf vagy KR állomány. KR állomány csak fõdokumentumként tölthetõ fel.
                            //-Papír alapú: pdf és egyéb típusok, kivéve KR
                            //-Vegyes: pdf"
                            if (!string.IsNullOrEmpty(IraIrat_Id))
                            {
                                string adathordozoTipusa = GetAdathordozoTipusa(IraIrat_Id);
                                if (adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir || adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                                {
                                    //return true;
                                    string pkiFileTypes = Rendszerparameterek.Get(Page, "PKI_FILETYPES");

                                    if (String.IsNullOrEmpty(pkiFileTypes))
                                    {
                                        pkiFileTypes = "pdf";
                                    }
                                    if (adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
                                    {
                                        pkiFileTypes += ",kr,krx";
                                    }
                                    if (!pkiFileTypes.ToLower().Contains(extension.ToLower()))
                                    {
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, FodokUploadError);
                                        ErrorUpdatePanel.Update();
                                        return;
                                    }
                                }
                                else if (adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir)
                                {
                                    if (",kr,krx".ToLower().Contains(extension.ToLower()))
                                    {
                                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, FodokUploadPaperError);
                                        ErrorUpdatePanel.Update();
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (isKrFile(extension))
                            {
                                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, KrUploadError);
                                ErrorUpdatePanel.Update();
                                return;
                            }
                        }
                    }
                }

                #endregion

                bool kliensoldaliAlairas = false;

                try
                {
                    #region Fájltartalom kiolvasása

                    if (fileIsAlreadyOnTheServer)
                    {
                        // FileUploadComponent1.HiddenField-ben van letéve a directory és file neve
                        string[] tomb = FileUploadComponent1.Value_HiddenField.Split('|');
                        if (tomb == null || tomb.Length < 2)
                        {
                            // hiba:
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, Resources.Error.UINoFile);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        randomTempDirectoryName = tomb[0];
                        string fileName = tomb[1];

                        csatolmany.Tartalom = FileFunctions.Upload.GetFileContentFromTempDirectory(randomTempDirectoryName, fileName);
                        csatolmany.Nev = fileName;
                    }
                    else
                    {
                        csatolmany.Tartalom = FileUpload1.FileBytes;
                        csatolmany.Nev = FileUpload1.FileName;
                    }
 
                    #region BUG 7470 iratpéldány kr file esetén ellenõrzése
                    string filaName = csatolmany.Nev;
                    string ext = System.IO.Path.GetExtension(filaName);

                    // .krx file-ra nem vonatkozik 
                    //if (".krx".Equals(ext, StringComparison.CurrentCultureIgnoreCase) || ".kr".Equals(ext, StringComparison.CurrentCultureIgnoreCase)) {
                    if (".kr".Equals(ext, StringComparison.CurrentCultureIgnoreCase))
                    {
                        //parentform vizsgálat
                        // BUG_7470
                        // Mindig szükséges a vizsgálat és Sztornózásra kerülnek a plussz iratpédányok
                        //#region BUG_8852 - rendszer paraméter ellenõrzés
                        //bool NeedKRCheck = true;
                        //if(Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.UGYIRATI_PELDANY_SZUKSEGES, false) || Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.EXPEDIALAS_UJ_IRATPELDANNYAL, false))
                        //{
                        //    NeedKRCheck = false;
                        //}
                        //#endregion
                        //if (NeedKRCheck) { 
                        String iraIratId = String.Empty;
                        if (_ParentForm == Constants.ParentForms.Kuldemeny)
                        {
                            EREC_KuldKuldemenyekService service_Kuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam_KuldGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_KuldGet.Record_Id = ParentId;

                            Result result_KuldGet = service_Kuld.Get(execParam_KuldGet);
                            if (!String.IsNullOrEmpty(result_KuldGet.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_KuldGet);
                                ErrorUpdatePanel.Update();
                                return;
                            }

                            EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_KuldGet.Record;
                            if (erec_KuldKuldemenyek != null)
                            {
                                iraIratId = erec_KuldKuldemenyek.IraIratok_Id;
                            }
                        }
                        else if (_ParentForm == Constants.ParentForms.IraIrat)
                        {
                            iraIratId = ParentId;
                        }
                        else if (_ParentForm == Constants.ParentForms.IratPeldany)
                        {
                            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_PldGet.Record_Id = ParentId;

                            Result result_PldGet = service_Pld.Get(execParam_PldGet);
                            if (!String.IsNullOrEmpty(result_PldGet.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldGet);
                                ErrorUpdatePanel.Update();
                                return;
                            }

                            EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;
                            if (erec_PldIratPeldanyok != null)
                            {
                                iraIratId = erec_PldIratPeldanyok.IraIrat_Id;
                            }
                        }
                        if (String.IsNullOrEmpty(iraIratId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.KrSearchIratError);
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        else
                        {
                            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch();

                            pldSearch.IraIrat_Id.Value = iraIratId;
                            pldSearch.IraIrat_Id.Operator = Query.Operators.equals;

                            pldSearch.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
                            pldSearch.Allapot.Operator = Query.Operators.notequals;

                            Result result_pldSearch = service_Pld.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), pldSearch);
                            if (!String.IsNullOrEmpty(result_pldSearch.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_pldSearch);
                                ErrorUpdatePanel.Update();
                                return;
                            }
                            else
                            {
                                if (result_pldSearch.Ds != null && result_pldSearch.Ds.Tables[0].Rows.Count > 0)
                                {

                                    int iratpldCount = 0;
                                    foreach (DataRow row in result_pldSearch.Ds.Tables[0].Rows)
                                    {
                                        if (row["Sorszam"].ToString() == "1")
                                        {
                                            firstIratpld = row["id"].ToString();
                                        }
                                        else iratpldIds.Add(row["id"].ToString());

                                        iratpldCount++;
                                    }
                                    if (iratpldCount < 1)
                                    {
                                        // BUG_7470
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.KrFileUploadError);
                                        ErrorUpdatePanel.Update();
                                        return;
                                    }
                                    else if (iratpldCount > 1)
                                    {
                                        if (hf_ConfirmUploadforStorno.Value != "ok")
                                        {
                                            // File lementése a szerverre, hogy ne kelljen a felhasználónak újra feltöltenie:
                                            string randomDirName;
                                            try
                                            {
                                                FileFunctions.Upload.SaveFileToTempDirectory(csatolmany.Tartalom, csatolmany.Nev, out randomDirName);
                                            }
                                            catch (Exception exc)
                                            {
                                                // hiba a file elmentése során:
                                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
                                                ErrorUpdatePanel.Update();
                                                return;
                                            }

                                            // FileUpload1 helyett FileUploadComponent1 megjelenítése, ami mutatja a feltöltött fájlt:
                                            FileUpload1.Visible = false;

                                            FileUploadComponent1.HiddenField.Value = randomDirName + "|" + csatolmany.Nev;
                                            FileUploadComponent1.Label.Text = csatolmany.Nev;
                                            FileUploadComponent1.Visible = true;
                                            FileUploadComponent1.Enabled = false;

                                            #region Figyelmeztetõ üzenet kell a felhasználónak: (javascriptes confirm)

                                            // 'Sztornózás figyelmeztetés'
                                            string confirmText = Resources.Form.UI_WarningKrFileFeltoltesSztornozas + " \\n";
                                            // 'Biztosan feltölti a dokumentumot?'
                                            confirmText += "\\n\\n" + Resources.Question.UIConfirmHeader_DokumentumUpload;
                                            string js = @"
                                                function ConfirmUploadforStorno()  {
                                                if (confirm('" + confirmText + "') == true) { $get('"
                                                + hf_ConfirmUploadforStorno.ClientID + "').value = 'ok';  "
                                                + "window.setTimeout(\"" + Page.ClientScript.GetPostBackEventReference(TabFooter1.ImageButton_Save, "") + "\",500);" +
                                                @"} else {}
                                                }"
                                                //+ " window.setTimeout(\"ConfirmUploadByDocumentHash()\",2000); ";

                                                + " function pageLoad() { if(ConfirmUploadforStorno != null){ConfirmUploadforStorno();ConfirmUploadforStorno = null;} } ";

                                            //+ " Sys.Application.add_load(ConfirmUploadByDocumentHash); ";

                                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "documentStornoConfirmScript", js, true);

                                            #endregion

                                            // Kilépés innen, még nem töltjük fel
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                    return;

                                }
                            }
                        }
                    }

                    #endregion

                    #endregion

                    #region Hash számítás és ellenõrzés, van-e már ilyen feltöltött fájl

                    // ha egy ellenõrzés utáni confirm-ra 'ok' jött, nem kell újból ellenõrzés
                    if (hf_confirmUploadByDocumentHash.Value == "ok")
                    {
                        hf_confirmUploadByDocumentHash.Value = "";

                        // MS fileoknál a tartalom hash-t meg kell képezni:
                        if (FileFunctions.NeedTartalomHashCheck(csatolmany.Nev))
                        {
                            csatolmany.TartalomHash = FileFunctions.GetMSFileContentSHA1(csatolmany.Tartalom, csatolmany.Nev);
                        }
                    }
                    else if (csatolmany.Tartalom != null && csatolmany.Tartalom.Length > 0)
                    {
                        bool tartalomHashEllenorzesIsKell = false;
                        string tartalomHash = String.Empty;
                        if (FileFunctions.NeedTartalomHashCheck(csatolmany.Nev))
                        {
                            tartalomHash = FileFunctions.GetMSFileContentSHA1(csatolmany.Tartalom, csatolmany.Nev);
                            if (!String.IsNullOrEmpty(tartalomHash))
                            {
                                tartalomHashEllenorzesIsKell = true;
                                // továbbadjuk a tartalomHash-et:
                                csatolmany.TartalomHash = tartalomHash;
                            }
                        }

                        string fileHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);

                        // Ellenõrzés, van-e már ilyen dokumentum feltöltve?
                        // CR3216 Csatolmány létezésének ellenörzése word filenal külön a KRT_DOKUMENTUMOKban és külön az EREC_CSATOLMANYOKban (Dokumentum_Id alapján)
                        KRT_DokumentumokService service_dokumentumok = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                        KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
                        dokumentumokSearch.KivonatHash.Value = fileHash;
                        dokumentumokSearch.KivonatHash.Operator = Query.Operators.equals;

                        if (tartalomHashEllenorzesIsKell)
                        {
                            // tartalomHash-re is ellenõrzünk (VAGY kapcsolat a kivonathash-sel)
                            dokumentumokSearch.TartalomHash.Value = tartalomHash;
                            dokumentumokSearch.TartalomHash.Operator = Query.Operators.equals;
                            dokumentumokSearch.TartalomHash.Group = "975";
                            dokumentumokSearch.TartalomHash.GroupOperator = Query.Operators.or;

                            dokumentumokSearch.KivonatHash.Group = "975";
                            dokumentumokSearch.KivonatHash.GroupOperator = Query.Operators.or;
                        }

                        Result result_dokSearch = service_dokumentumok.GetAll(
                            UI.SetExecParamDefault(Page, new ExecParam()), dokumentumokSearch);
                        if (result_dokSearch.IsError)
                        {
                            // hiba:
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_dokSearch);
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        else
                        {
                            // ha van már ilyen dokumentum:
                            if (result_dokSearch.Ds != null && result_dokSearch.Ds.Tables[0].Rows.Count > 0)
                            {

                                List<string> dokumentumIds = new List<string>();

                                foreach (DataRow row in result_dokSearch.Ds.Tables[0].Rows)
                                {
                                    string Id = row["Id"].ToString();
                                    if (!String.IsNullOrEmpty(Id) && !dokumentumIds.Contains(Id))
                                    {
                                        dokumentumIds.Add(Id);
                                    }
                                }

                                if (dokumentumIds.Count > 0)
                                {

                                    EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
                                    csatolmanyokSearch.Dokumentum_Id.Value = Search.GetSqlInnerString(dokumentumIds.ToArray());
                                    csatolmanyokSearch.Dokumentum_Id.Operator = Query.Operators.inner;
                                    //csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Value = fileHash;
                                    //csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Operator = Query.Operators.equals;

                                    //if (tartalomHashEllenorzesIsKell)
                                    //{
                                    //    // tartalomHash-re is ellenõrzünk (VAGY kapcsolat a kivonathash-sel)
                                    //    csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Value = tartalomHash;
                                    //    csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Operator = Query.Operators.equals;
                                    //    csatolmanyokSearch.Manual_Dokumentum_TartalomHash.Group = "975";
                                    //    csatolmanyokSearch.Manual_Dokumentum_TartalomHash.GroupOperator = Query.Operators.or;

                                    //    csatolmanyokSearch.Manual_Dokumentum_KivonatHash.Group = "975";
                                    //    csatolmanyokSearch.Manual_Dokumentum_KivonatHash.GroupOperator = Query.Operators.or;
                                    //}

                                    Result result_csatSearch = service_csatolmanyok.GetAllWithExtension(
                                     UI.SetExecParamDefault(Page, new ExecParam()), csatolmanyokSearch);
                                    if (result_csatSearch.IsError)
                                    {
                                        // hiba:
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_csatSearch);
                                        ErrorUpdatePanel.Update();
                                        return;
                                    }
                                    else
                                    {
                                        // ha van már ilyen dokumentum:
                                        if (result_csatSearch.Ds != null && result_csatSearch.Ds.Tables[0].Rows.Count > 0)
                                        {
                                            // File lementése a szerverre, hogy ne kelljen a felhasználónak újra feltöltenie:
                                            string randomDirName;
                                            try
                                            {
                                                FileFunctions.Upload.SaveFileToTempDirectory(csatolmany.Tartalom, csatolmany.Nev, out randomDirName);
                                            }
                                            catch (Exception exc)
                                            {
                                                // hiba a file elmentése során:
                                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFileSaveError, exc.Message);
                                                ErrorUpdatePanel.Update();
                                                return;
                                            }

                                            // FileUpload1 helyett FileUploadComponent1 megjelenítése, ami mutatja a feltöltött fájlt:
                                            FileUpload1.Visible = false;

                                            FileUploadComponent1.HiddenField.Value = randomDirName + "|" + csatolmany.Nev;
                                            FileUploadComponent1.Label.Text = csatolmany.Nev;
                                            FileUploadComponent1.Visible = true;
                                            FileUploadComponent1.Enabled = false;

                                            #region Figyelmeztetõ üzenet kell a felhasználónak: (javascriptes confirm)

                                            // 'Ez a dokumentum már feltöltésre került az alábbi elem(ek) csatolmányaként:'
                                            string confirmText = Resources.Form.DokumentumMarFeltoltve + " \\n";
                                            int felsoroltElemekMaxSzama = 3;
                                            for (int i = 0; i < result_csatSearch.Ds.Tables[0].Rows.Count && i < felsoroltElemekMaxSzama; i++)
                                            {
                                                DataRow row = result_csatSearch.Ds.Tables[0].Rows[i];
                                                if (!string.IsNullOrEmpty(row["IraIrat_Id"].ToString()))
                                                {
                                                    // irat:
                                                    string iktatoSzam = row["IktatoSzam_Merge"].ToString();
                                                    confirmText += "\\n" + iktatoSzam;
                                                }
                                                else
                                                {
                                                    // küldemény:
                                                    string erkeztetoSzam = row["ErkeztetoSzam_Merge"].ToString();
                                                    confirmText += "\\n" + erkeztetoSzam;
                                                }
                                            }

                                            if (result_csatSearch.Ds.Tables[0].Rows.Count > felsoroltElemekMaxSzama)
                                            {
                                                confirmText += "\\n...";
                                            }

                                            // 'Biztosan feltölti a dokumentumot?'
                                            confirmText += "\\n\\n" + Resources.Question.UIConfirmHeader_DokumentumUpload;

                                            string js = @"
                                             function ConfirmUploadByDocumentHash()  {
                                                if (confirm('" + confirmText + "') == true) { $get('"
                                                + hf_confirmUploadByDocumentHash.ClientID + "').value = 'ok';  "
                                                + "window.setTimeout(\"" + Page.ClientScript.GetPostBackEventReference(TabFooter1.ImageButton_Save, "") + "\",500);" +
                                               @"} else {}
                                             }"
                                                //+ " window.setTimeout(\"ConfirmUploadByDocumentHash()\",2000); ";

                                                + " function pageLoad() { if(ConfirmUploadByDocumentHash != null){ConfirmUploadByDocumentHash();ConfirmUploadByDocumentHash = null;} } ";

                                            //+ " Sys.Application.add_load(ConfirmUploadByDocumentHash); ";


                                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "documentHashConfirmScript", js, true);

                                            #endregion

                                            // Kilépés innen, még nem töltjük fel
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region Aláírás:

                    if (cb_Alairas.Checked && !string.IsNullOrEmpty(ddl_AlairasSzabalyok.SelectedValue))
                    {
                        // beállítjuk az aláírásra vonatkozó infókat a csatolmany objektumban:

                        string alairasSzabaly_Id = ddl_AlairasSzabalyok.SelectedValue;

                        // Aláírásszabályhoz tartozó adatok a lementett dictionary-bõl:
                        Dictionary<string, Csatolmany.Alairas> alairasAdatok_Dict =
                            (Dictionary<string, Csatolmany.Alairas>)ViewState[viewState_AlairasAdatok_Dict];

                        if (alairasAdatok_Dict != null && alairasAdatok_Dict.ContainsKey(alairasSzabaly_Id))
                        {
                            Csatolmany.Alairas elmentettAlairasAdatok = alairasAdatok_Dict[alairasSzabaly_Id];

                            csatolmany.AlairasAdatok = elmentettAlairasAdatok;
                        }

                        /// A csatolmány aláírandó:
                        /// Ha kliens oldali aláírásszabály volt választva, akkor ez nem kell,
                        /// különben a szerveren még egyszer alá lenne írva
                        List<string> kliensoldaliAlairasSzabalyok = (List<string>)ViewState[viewState_KliensoldaliAlairasSzabalyok];
                        if (kliensoldaliAlairasSzabalyok != null
                              && kliensoldaliAlairasSzabalyok.Contains(alairasSzabaly_Id))
                        {
                            // kliens oldali aláírás volt választva, nem kell már szerveren aláírni:
                            csatolmany.AlairasAdatok.CsatolmanyAlairando = false;
                            csatolmany.ElektronikusAlairas = csatolmany.AlairasAdatok.AlairasSzint.ToString();
                            kliensoldaliAlairas = true;
                        }
                        else
                        {
                            csatolmany.AlairasAdatok.CsatolmanyAlairando = true;
                        }

                        csatolmany.AlairasAdatok.AlairasSzabaly_Id = alairasSzabaly_Id;


                        //string signdata = System.Convert.ToBase64String(csatolmany.Tartalom);
                        //int ConfigId = 0;

                        ////string contentToSign = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        ////        + "<xml>"
                        ////        + "<config_item>" + ConfigId + "</config_item>"
                        ////        + "<filename>" + csatolmany.Nev + "</filename>"
                        ////        + "<signdata>" + signdata + "</signdata>"
                        ////        + "</xml>";

                        //eSign.SDXM.Url = UI.GetAppSetting("eASZBusinessServiceUrl");

                        ////eSign.common.SignResult sr = eSign.SDXM.Sign(contentToSign);
                        //eSign.common.SignResult sr = eSign.SDXM.SignDirectly(ConfigId, csatolmany.Nev, signdata);

                        //csatolmany.Tartalom = System.Convert.FromBase64String(sr.Result);
                        //csatolmany.Nev += ".sdxm";
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    result.ErrorCode = "222222";
                    result.ErrorMessage = "hiba a file beolvasasakor! \n" + ex.ToString();
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return;
                }

                if (csatolmany.Tartalom == null)
                {
                    result.ErrorCode = "222222";
                    result.ErrorMessage = "hiba a file beolvasásakor: null a tartalom!";
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return;
                }

                csatolmany.Megnyithato = cbMegnyithato.Checked ? "1" : "0";
                csatolmany.Olvashato = cbOlvashato.Checked ? "1" : "0";
                csatolmany.Titkositas = cbTitkositott.Checked ? "1" : "0";

                switch (krt_Parameterek_PKI_INTEGRACIO)
                {
                    case "Nem":
                        csatolmany.ElektronikusAlairas = AlairasManualis_KodtarakDropDownList.SelectedValue;
                        break;
                    case "Igen":
                        //nekrisz CR 3015 : PKI_integrációná a régi ellenörzés mellõzése
                        string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                        if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                        {
                            csatolmany.ElektronikusAlairas = AlairasManualis_KodtarakDropDownList.SelectedValue;
                        }
                        else
                        {
                            // csatolmany.AlairasAdatok -ban adjuk meg az aláírásos cuccokat
                            // kliensoldali aláírásnál már beállítottuk:
                            if (!kliensoldaliAlairas)
                            {
                                csatolmany.ElektronikusAlairas = "<null>";
                            }
                        }

                        break;
                    default:
                        csatolmany.ElektronikusAlairas = "<null>";
                        break;
                }
            }

            ExecParam execParam = UI.SetExecParamDefault(Page);

            // dokumentum vizsgálathoz minden típus esetén aktualizálni kell
            // a KRT_Dokumentumok tábla rekordját is, módosítás esetén
            String krtDokumentumId = String.Empty;

            // nyilvántartjuk, hogy sikerült-e beolvasni a fõ (küldemény, irat, iratpéldány) rekordot
            bool voltHiba = false;

            //// a dokumentum megnyithatósági vizsgálathoz szükséges
            //// a KRT_Dokumentumok típusú paraméter továbbítása is
            //// ez független attól, hogy mihez csatolunk
            //KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents_DokumentumVizsgalat();

            // Kuldemeny
            if (_ParentForm == Constants.ParentForms.Kuldemeny)
            {
                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CheckRightCommand))
                {
                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fõdokumentum ellenõrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, ParentId))
                                {
                                    return;
                                }
                                #endregion Fõdokumentum ellenõrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                execParam.Record_Id = ParentId;
                                result = erec_KuldKuldemenyekService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (result.IsError == false)
                                {
                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fõdokumentum ellenõrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, ParentId))
                                    {
                                        return;
                                    }
                                    #endregion Fõdokumentum ellenõrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
                }
            }
            // IraIrat
            else if (_ParentForm == Constants.ParentForms.IraIrat)
            {

                if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CheckRightCommand))
                {
                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fõdokumentum ellenõrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, ParentId))
                                {
                                    return;
                                }
                                #endregion Fõdokumentum ellenõrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();
                                
                                Result handleAzonosNevuWorldFodokumentumResult = Contentum.eRecord.BaseUtility.CsatolmanyHelper.HandleAzonosNevuWordFodokumentum(execParam, erec_Csatolmanyok, csatolmany.Nev, ParentId);
                                if (handleAzonosNevuWorldFodokumentumResult != null)
                                {
                                    Logger.Error("Nem sikerült az azonos nevű csatolmány szerepének megváltoztatása", execParam, handleAzonosNevuWorldFodokumentumResult);
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, handleAzonosNevuWorldFodokumentumResult);
                                    ErrorUpdatePanel.Update();
                                    return;
                                }

                                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                execParam.Record_Id = ParentId;
                                result = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (result.IsError == false)
                                {

                                    //BUG_7470
                                    if (hf_ConfirmUploadforStorno.Value == "ok")
                                    {
                                        hf_ConfirmUploadforStorno.Value = "";

                                        foreach (String pldId in iratpldIds)
                                        {
                                            IratPeldanyok.Sztornozas(pldId, ".KR file feltöltés", Page, EErrorPanel1, ErrorUpdatePanel);
                                        }
                                    }

                                    // BUG_7470
                                    // Elsõ iratpéldány címzett feltöltése 
                                    //
                                    try
                                    {
                                        // Iratpéldány lekérése:
                                        if (!String.IsNullOrEmpty(firstIratpld))
                                        {
                                            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                                            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
                                            execParam_PldGet.Record_Id = firstIratpld;


                                            Result result_PldGet = service_Pld.Get(execParam_PldGet);
                                            if (!String.IsNullOrEmpty(result_PldGet.ErrorCode))
                                            {
                                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldGet);
                                                ErrorUpdatePanel.Update();
                                                return;
                                            }

                                            EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;
                                            if (erec_PldIratPeldanyok != null)
                                            {
                                                MemoryStream ms = new MemoryStream(csatolmany.Tartalom);
                                                XmlReader reader = XmlReader.Create(ms);
                                                XDocument doc = XDocument.Load(reader);

                                                var fejlec = from element in doc.Root.Elements()
                                                             where element.Name.LocalName == "Fejlec"
                                                             select element;

                                                string PartnerRovidNev = (from element in fejlec.Elements()
                                                                          where element.Name.LocalName == "Cimzett"
                                                                          select element.Value).FirstOrDefault();

                                                if (!String.IsNullOrEmpty(PartnerRovidNev))
                                                {
                                                    KRT_PartnerekSearch src = new KRT_PartnerekSearch();
                                                    src.Forras.Value = Contentum.eAdmin.BaseUtility.KodTarak.Partner_Forras.HKP;
                                                    src.Forras.Operator = Query.Operators.equals;

                                                    src.KulsoAzonositok.Value = string.Format("%{0},%", PartnerRovidNev);
                                                    src.KulsoAzonositok.Operator = Query.Operators.like;

                                                    KRT_CimekSearch srcCim = new KRT_CimekSearch();
                                                    srcCim.Tipus.Value = KodTarak.Cim_Tipus.TitkosKapcsolatiKod;
                                                    srcCim.Tipus.Operator = Query.Operators.equals;

                                                    Contentum.eAdmin.Service.KRT_PartnerekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                                                    ExecParam execParamforCimzett = execParam.Clone();
                                                    Result partnerResult = service.GetAllWithCim(execParamforCimzett, src, srcCim);
                                                    if (!string.IsNullOrEmpty(partnerResult.ErrorCode))

                                                    {

                                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, partnerResult);
                                                        ErrorUpdatePanel.Update();
                                                        return;
                                                    }
                                                    if (partnerResult.Ds.Tables[0].Rows.Count != 1)
                                                    {
                                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "KR fájl feldoglozási hiba", "Címzett hiba! Talált: " + partnerResult.Ds.Tables[0].Rows.Count.ToString());
                                                        ErrorUpdatePanel.Update();
                                                        return;
                                                    }

                                                    erec_PldIratPeldanyok.Updated.SetValueAll(false);
                                                    erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

                                                    erec_PldIratPeldanyok.Base.Updated.Ver = true;

                                                    erec_PldIratPeldanyok.NevSTR_Cimzett = partnerResult.Ds.Tables[0].Rows[0]["Nev"].ToString();
                                                    erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
                                                    erec_PldIratPeldanyok.Partner_Id_Cimzett = partnerResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                                    erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

                                                    erec_PldIratPeldanyok.CimSTR_Cimzett = partnerResult.Ds.Tables[0].Rows[0]["CimTobbi"].ToString();
                                                    erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;
                                                    erec_PldIratPeldanyok.Cim_id_Cimzett = partnerResult.Ds.Tables[0].Rows[0]["CimId"].ToString();
                                                    erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                                                    ExecParam execParam_pldUpdate = execParam.Clone();
                                                    execParam_pldUpdate.Record_Id = firstIratpld;

                                                    Result result_pldUpdate = service_Pld.Update(execParam_pldUpdate, erec_PldIratPeldanyok);
                                                    if (!String.IsNullOrEmpty(result_pldUpdate.ErrorCode))
                                                    {
                                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_pldUpdate);
                                                        ErrorUpdatePanel.Update();
                                                        return;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "KR fájl feldoglozási hiba", ex.Message);
                                    }


                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fõdokumentum ellenõrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, ParentId))
                                    {
                                        return;
                                    }
                                    #endregion Fõdokumentum ellenõrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.RedirectToAccessDeniedErrorPage(Page);
                }
            }
            // IratPeldany
            else if (_ParentForm == Constants.ParentForms.IratPeldany)
            {

                if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CheckRightCommand))
                {

                    EREC_PldIratPeldanyok erec_PldIratPeldanyok = null;

                    if (SubCommand == CommandName.New
                        || SubCommand == CommandName.Modify)
                    {
                        // Iratpéldányból irat lekérése:
                        EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam_PldGet.Record_Id = ParentId;

                        Result result_PldGet = service_Pld.Get(execParam_PldGet);
                        if (!String.IsNullOrEmpty(result_PldGet.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldGet);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;
                    }

                    switch (SubCommand)
                    {
                        case CommandName.Cancel:
                            EFormPanel1.Visible = false;
                            SubListPanel.Visible = true;
                            break;
                        case CommandName.New:
                            {
                                #region Fõdokumentum ellenõrzés
                                if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(null, erec_PldIratPeldanyok.IraIrat_Id))
                                {
                                    return;
                                }
                                #endregion Fõdokumentum ellenõrzés

                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents_IratPeldany(erec_PldIratPeldanyok);

                                // erec_IraIratokDokumentumok.IraIrat_Id ParentId-ra volt állítva
                                erec_Csatolmanyok.IraIrat_Id = erec_PldIratPeldanyok.IraIrat_Id;


                                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                                execParam.Record_Id = erec_PldIratPeldanyok.IraIrat_Id;
                                result = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // ha már fel volt töltve a fájl, törlés a temp táblából:
                                    if (!String.IsNullOrEmpty(randomTempDirectoryName))
                                    {
                                        FileFunctions.Upload.TryToDeleteFromTempTable(randomTempDirectoryName, csatolmany.Nev);
                                    }

                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                                break;
                            }

                        case CommandName.Modify:
                            {
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    #region Fõdokumentum ellenõrzés
                                    if (bCheckFodokumentum && CheckFodokumentumAlreadyExists(service, erec_PldIratPeldanyok.IraIrat_Id))
                                    {
                                        return;
                                    }
                                    #endregion Fõdokumentum ellenõrzés

                                    EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents_IratPeldany(erec_PldIratPeldanyok);

                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Csatolmanyok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // a KRT_Dokumentumok rekord módosításához
                                        // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                        result = service.Get(execParam);
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;

                                        //ReLoadTab();
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                        case CommandName.ElektronikusAlairas:
                            break;
                        case CommandName.AlairasEllenorzes:
                            {
                                // hasonló lépések, mint a módosításnál,
                                // de az EREC_KuldDokumentumokat nem módosítjuk
                                String recordId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                                    execParam.Record_Id = recordId;

                                    // a KRT_Dokumentumok rekord módosításához
                                    // szükség van a dokumentum ID-ra, ehhez pedig a teljes hivatkozó rekordra
                                    result = service.Get(execParam);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        krtDokumentumId = ((EREC_Csatolmanyok)result.Record).Dokumentum_Id;
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel.Update();

                                        voltHiba = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.RedirectToAccessDeniedErrorPage(Page);
                }
            }

            if (!voltHiba &&
                    (SubCommand == CommandName.Modify
                     || SubCommand == CommandName.AlairasEllenorzes
                     )
                )
            {
                KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents_DokumentumVizsgalat(SubCommand);
                ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                dokExecParam.Record_Id = krtDokumentumId;
                krt_Dokumentumok.Base.Updated.Ver = true;
                result = dokService.Update(dokExecParam, krt_Dokumentumok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    // 
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }

                ReLoadTab();
            }

        }
        else if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
            SubListPanel.Visible = true;
        }
    }
  
    #region Fodokumnetum
    private Result GetFodokumentum(EREC_CsatolmanyokService service, string Id)
    {
        Result result = null;
        if (service == null)
        {
            service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        }

        ExecParam execparam = UI.SetExecParamDefault(Page);
        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        search.DokumentumSzerep.Value = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
        search.DokumentumSzerep.Operator = Query.Operators.equals;

        search.Manual_Dokumentum_Nev.Value = GetUploadingFileName();
        search.Manual_Dokumentum_Nev.Operator = Query.Operators.notequals;

        search.TopRow = 1; // elég a létezés vizsgálata

        switch (_ParentForm)
        {
            case Constants.ParentForms.Kuldemeny:
                search.KuldKuldemeny_Id.Value = Id;
                search.KuldKuldemeny_Id.Operator = Query.Operators.equals;
                break;
            case Constants.ParentForms.IraIrat:
            case Constants.ParentForms.IratPeldany:
                search.IraIrat_Id.Value = Id;
                search.IraIrat_Id.Operator = Query.Operators.equals;
                break;
            default:
                // hiba: ilyen elvileg nem lehet
                result = new Result();
                result.ErrorCode = "[PARENT]";
                result.ErrorMessage = "[PARENT]";
                return result;
        }

        return result = service.GetAllWithExtension(execparam, search);
    }

    public string GetUploadingFileName()
    {
        string fileName = String.Empty;
        if (FileUpload1.HasFile)
        {
            fileName = FileUpload1.FileName;
        }
        else
        {
            if ((!String.IsNullOrEmpty(FileUploadComponent1.Value_HiddenField)))
            {
                string[] file = FileUploadComponent1.Value_HiddenField.Split('|');
                if (file.Length > 1)
                {
                    fileName = file[1];
                }
            }
        }

        return fileName;
    }
    private bool IsCheckFodokumentumNecessary()
    {
        bool bCheckFodokumentum = false;
        // fõdokumentum létezés ellenõrzése szükséges-e
        if (KodtarakDropDownList_DokumentumSzerep.SelectedValue == KodTarak.DOKUMENTUM_SZEREP.Fodokumentum
            && hfDokumentumSzerep.Value != KodTarak.DOKUMENTUM_SZEREP.Fodokumentum
            )
        {
            bCheckFodokumentum = true;
        }
        return bCheckFodokumentum;
    }

    private bool CheckFodokumentumAlreadyExists(EREC_CsatolmanyokService service, string Id)
    {
        bool bAlreadyExists = true;

        Result result = GetFodokumentum(service, Id);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else if (result.Ds.Tables[0].Rows.Count > 0)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.UIAlreadyExistsFodokumentumInCsatolmanyok);
            ErrorUpdatePanel.Update();
        }
        else
        {
            bAlreadyExists = false;
        }

        return bAlreadyExists;
    }

    private int GetFodokumentumCountFromGridView(GridView gv)
    {
        int count = 0;

        foreach (GridViewRow gridViewRow in gv.Rows)
        {
            if (gridViewRow.RowType == DataControlRowType.DataRow)
            {
                if (UI.GetGridViewColumnText(gridViewRow, "DokumentumSzerep") == KodTarak.DOKUMENTUM_SZEREP.Fodokumentum)
                {
                    count++;
                }
            }
        }

        hfIsFodokument.Value = count > 0 ? "true" : "false";

        return count;
    }

    #endregion Fodokumentum


    private void ClearForm()
    {
        //FileUploadComponent1.Enabled = true;
        FileUpload1.Enabled = true;
        LabelAllomany_Csillag.Visible = true;

        //FileUploadComponent1.Clear();
        CsatolmanyMegjegyzes.Text = "";
        //        CsatlomanyVerzio.Text = "1";
        cbMegnyithato.Checked = false;
        cbOlvashato.Checked = false;
        cbTitkositott.Checked = false;

        // alapértelmezés: Dokumentum melléklet
        int countFodokumentum = GetFodokumentumCountFromGridView(CsatolmanyokGridView);
        string DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
        if (countFodokumentum == 0)
        {
            DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
        }
        KodtarakDropDownList_DokumentumSzerep.FillAndSetSelectedValue(kcsDokumentumSzerep, DokumentumSzerep, false, EErrorPanel1);
        hfDokumentumSzerep.Value = String.Empty;

        if (krt_Parameterek_PKI_INTEGRACIO == "Nem")
        {
            // Manuálisan lehet bejegyezni, hogy aláírt, vagy sem:

            AlairasManualis_KodtarakDropDownList.FillDropDownList(kcsDokumentumAlairasManualis, false, EErrorPanel1);

            AlairasManualis_KodtarakDropDownList.Visible = true;
            AlairasManualis_KodtarakDropDownList.Enabled = true;

            cb_Alairas.Visible = false;
            ddl_AlairasSzabalyok.Visible = false;
        }

        //if (KodtarakDropDownList_ElektronikusAlairas.Visible)
        //{
        //    KodtarakDropDownList_ElektronikusAlairas.FillDropDownList(kcsDokumentumAlairas, false, EErrorPanel1);
        //}


    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_Csatolmanyok erec_Csatolmanyok)
    {
        if (erec_Csatolmanyok == null)
        {
            return;
        }

        if (Command == CommandName.View)
        {
            CsatolmanyMegjegyzes.ReadOnly = true;
        }
        else
        {
            CsatolmanyMegjegyzes.ReadOnly = false;
        }

        //        CsatlomanyVerzio.ReadOnly = true;


        KodtarakDropDownList_DokumentumSzerep.FillAndSetSelectedValue(kcsDokumentumSzerep, erec_Csatolmanyok.DokumentumSzerep, false, EErrorPanel1);
        hfDokumentumSzerep.Value = erec_Csatolmanyok.DokumentumSzerep; // eredeti érték megjegyzése

        CsatolmanyMegjegyzes.Text = erec_Csatolmanyok.Leiras;

        Record_Ver_HiddenField.Value = erec_Csatolmanyok.Base.Ver;

        //FormPart_CreatedModified1.SetComponentValues(erec_KuldDokumentumok.Base);

    }

    private void LoadComponentsFromBusinessObjects_DokumentumVizsgalat(KRT_Dokumentumok krt_Dokumentumok)
    {
        cbMegnyithato.Checked = krt_Dokumentumok.Megnyithato == "1" ? true : false;
        cbOlvashato.Checked = krt_Dokumentumok.Olvashato == "1" ? true : false;
        cbTitkositott.Checked = krt_Dokumentumok.Titkositas == "1" ? true : false;

        if (krt_Parameterek_PKI_INTEGRACIO == "Nem")
        {
            AlairasManualis_KodtarakDropDownList.FillAndSetSelectedValue(kcsDokumentumAlairasManualis,
                krt_Dokumentumok.ElektronikusAlairas, false, EErrorPanel1);
        }

        if (Command == CommandName.View)
        {
            cbMegnyithato.Enabled = false;
            cbOlvashato.Enabled = false;
            cbTitkositott.Enabled = false;
            AlairasManualis_KodtarakDropDownList.Enabled = false;
        }
        else
        {
            cbMegnyithato.Enabled = true;
            cbOlvashato.Enabled = true;
            //KodtarakDropDownList_ElektronikusAlairas.Enabled = true;
            cbTitkositott.Enabled = true;
        }

        //FileUploadComponent1.Text = krt_Dokumentumok.FajlNev;

        FileUploadComponent1.Value_HiddenField = "|" + krt_Dokumentumok.FajlNev;

        Record_Ver_HiddenField_KRT_Dokumentum.Value = krt_Dokumentumok.Base.Ver;

    }


    // form --> business object
    private EREC_Csatolmanyok GetBusinessObjectFromComponents()
    {
        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
        erec_Csatolmanyok.Updated.SetValueAll(false);
        erec_Csatolmanyok.Base.Updated.SetValueAll(false);

        if (_ParentForm == Constants.ParentForms.Kuldemeny)
        {
            erec_Csatolmanyok.KuldKuldemeny_Id = ParentId;
            erec_Csatolmanyok.Updated.KuldKuldemeny_Id = true;
        }
        else if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            erec_Csatolmanyok.IraIrat_Id = ParentId;
            erec_Csatolmanyok.Updated.IraIrat_Id = true;
        }
        else return null;

        erec_Csatolmanyok.DokumentumSzerep = KodtarakDropDownList_DokumentumSzerep.SelectedValue;
        erec_Csatolmanyok.Updated.DokumentumSzerep = pageView.GetUpdatedByView(KodtarakDropDownList_DokumentumSzerep);

        erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes.Text;
        erec_Csatolmanyok.Updated.Leiras = pageView.GetUpdatedByView(CsatolmanyMegjegyzes);

        erec_Csatolmanyok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_Csatolmanyok.Base.Updated.Ver = true;

        return erec_Csatolmanyok;

    }

    private EREC_Csatolmanyok GetBusinessObjectFromComponents_IratPeldany(EREC_PldIratPeldanyok iratPeldany)
    {
        if (_ParentForm == Constants.ParentForms.IratPeldany)
        {
            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
            erec_Csatolmanyok.Updated.SetValueAll(false);
            erec_Csatolmanyok.Base.Updated.SetValueAll(false);

            erec_Csatolmanyok.IraIrat_Id = iratPeldany.IraIrat_Id;
            erec_Csatolmanyok.Updated.IraIrat_Id = true;

            erec_Csatolmanyok.DokumentumSzerep = KodtarakDropDownList_DokumentumSzerep.SelectedValue;
            erec_Csatolmanyok.Updated.DokumentumSzerep = pageView.GetUpdatedByView(KodtarakDropDownList_DokumentumSzerep);

            erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes.Text;
            erec_Csatolmanyok.Updated.Leiras = pageView.GetUpdatedByView(CsatolmanyMegjegyzes);

            erec_Csatolmanyok.Base.Ver = Record_Ver_HiddenField.Value;
            erec_Csatolmanyok.Base.Updated.Ver = true;

            return erec_Csatolmanyok;
        }
        else return null;
    }


    // mode: New, View, Modify, AlairasEllenorzes, ElektronikusAlairas stb.
    // TabHeader alapján a TabFooter Subcommandja
    private KRT_Dokumentumok GetBusinessObjectFromComponents_DokumentumVizsgalat(string mode)
    {
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
        krt_Dokumentumok.Updated.SetValueAll(false);
        krt_Dokumentumok.Base.Updated.SetValueAll(false);

        switch (mode)
        {
            case CommandName.New:
            // New esetén a Csatolmanyok objektumban adjuk át az értékeket
            // a TabFooterButtonsClick-ben
            //krt_Dokumentumok.Megnyithato = (cbMegnyithato.Checked ? "1" : "0");
            //krt_Dokumentumok.Updated.Megnyithato = pageView.GetUpdatedByView(cbMegnyithato);

            //krt_Dokumentumok.Olvashato = (cbOlvashato.Checked ? "1" : "0");
            //krt_Dokumentumok.Updated.Olvashato = pageView.GetUpdatedByView(cbOlvashato);

            //switch (Rendszerparameterek.PKI_INTEGRACIO)
            //{
            //    case "Igen":
            //        break;
            //    case "Nem":
            //        krt_Dokumentumok.ElektronikusAlairas = KodtarakDropDownList_ElektronikusAlairas.SelectedValue;
            //        krt_Dokumentumok.Updated.ElektronikusAlairas = pageView.GetUpdatedByView(KodtarakDropDownList_ElektronikusAlairas);

            //        // TODO: ez még csak elõ van készítve
            //        //krt_Dokumentumok.Titkositas = (cbTitkositas.Checked ? "1" : "0");
            //        //krt_Dokumentumok.Updated.Titkositas = pageView.GetUpdatedByView(cbTitkositas);

            //        krt_Dokumentumok.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
            //        krt_Dokumentumok.Updated.AlairasFelulvizsgalat = true;
            //        break;
            //    default:
            //        break;
            //}
            //break;
            case CommandName.Modify:
                krt_Dokumentumok.Megnyithato = (cbMegnyithato.Checked ? "1" : "0");
                krt_Dokumentumok.Updated.Megnyithato = pageView.GetUpdatedByView(cbMegnyithato);

                krt_Dokumentumok.Olvashato = (cbOlvashato.Checked ? "1" : "0");
                krt_Dokumentumok.Updated.Olvashato = pageView.GetUpdatedByView(cbOlvashato);

                krt_Dokumentumok.Titkositas = cbTitkositott.Checked ? "1" : "0";
                krt_Dokumentumok.Updated.Titkositas = pageView.GetUpdatedByView(cbTitkositott);

                if (krt_Parameterek_PKI_INTEGRACIO == "Nem")
                {
                    krt_Dokumentumok.ElektronikusAlairas = AlairasManualis_KodtarakDropDownList.SelectedValue;
                    krt_Dokumentumok.Updated.ElektronikusAlairas = pageView.GetUpdatedByView(AlairasManualis_KodtarakDropDownList);
                }
                break;
            case CommandName.AlairasEllenorzes:
                switch (krt_Parameterek_PKI_INTEGRACIO)
                {
                    case "Igen":
                        ////nekrisz CR 3015 : PKI_integrációná a régi ellenörzés mellõzése
                        //string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                        //if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                        //{
                        // TODO: automatizálás
                        //krt_Dokumentumok.ElektronikusAlairas = KodtarakDropDownList_ElektronikusAlairas.SelectedValue;
                        //krt_Dokumentumok.Updated.ElektronikusAlairas = pageView.GetUpdatedByView(KodtarakDropDownList_ElektronikusAlairas);

                        // TODO: ez még csak elõ van készítve
                        //krt_Dokumentumok.Titkositas = (cbTitkositas.Checked ? "1" : "0");
                        //krt_Dokumentumok.Updated.Titkositas = pageView.GetUpdatedByView(cbTitkositas);

                        krt_Dokumentumok.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                        krt_Dokumentumok.Updated.AlairasFelulvizsgalat = true;
                        //  }
                        break;
                    case "Nem":
                        //krt_Dokumentumok.ElektronikusAlairas = KodtarakDropDownList_ElektronikusAlairas.SelectedValue;
                        //krt_Dokumentumok.Updated.ElektronikusAlairas = pageView.GetUpdatedByView(KodtarakDropDownList_ElektronikusAlairas);

                        // TODO: ez még csak elõ van készítve
                        //krt_Dokumentumok.Titkositas = (cbTitkositas.Checked ? "1" : "0");
                        //krt_Dokumentumok.Updated.Titkositas = pageView.GetUpdatedByView(cbTitkositas);

                        krt_Dokumentumok.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                        krt_Dokumentumok.Updated.AlairasFelulvizsgalat = true;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;

        }
        krt_Dokumentumok.Base.Ver = Record_Ver_HiddenField_KRT_Dokumentum.Value;
        krt_Dokumentumok.Base.Updated.Ver = true;

        return krt_Dokumentumok;

    }

    #endregion

    #region List
    // mellékletek módosulása (felvétel, törlés) utáni újrakötéshez
    private void CsatolmanyokGridViewBind_WithReselectRow()
    {
        #region kiválaszott sor megjegyzése
        int selectedIndex = CsatolmanyokGridView.SelectedIndex;
        string dataKey = String.Empty;
        if (selectedIndex > -1 && CsatolmanyokGridView.DataKeys.Count > selectedIndex)
        {
            dataKey = CsatolmanyokGridView.DataKeys[selectedIndex].Value.ToString();
        }
        #endregion kiválaszott sor megjegyzése

        CsatolmanyokGridViewBind();

        #region sor újra kiválasztása
        if (CsatolmanyokGridView.Rows.Count > selectedIndex)
        {
            if (selectedIndex > -1 && CsatolmanyokGridView.DataKeys.Count > selectedIndex)
            {
                if (dataKey == CsatolmanyokGridView.DataKeys[selectedIndex].Value.ToString())
                {
                    CsatolmanyokGridView.SelectedIndex = selectedIndex;
                    UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedIndex, "check");
                }
            }
        }
        #endregion sor újra kiválasztása
    }

    protected void CsatolmanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolmanyokGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolmanyokGridView", ViewState);
        CsatolmanyokGridViewBind(sortExpression, sortDirection);
    }
    public string IratHelye
    {
        get
        {
            var temp = ViewState["IratHelye"];
            if (temp == null) { return string.Empty; }
            return temp.ToString();
        }
        set
        {
            ViewState["IratHelye"] = value;
        }
    }
    public List<string> IratAlairok
    {
        get
        {
            var temp = ViewState["IratAlairok"];
            if (temp == null) { return new List<string>(); }
            return new List<string>(temp as string[]) ;
        }
        set
        {
            ViewState["IratAlairok"] = value.ToArray();
        }
    }

    protected void CsatolmanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {


        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            var iratHelyeResult = GetIrat();
            IratHelye = iratHelyeResult.FelhasznaloCsoport_Id_Orzo;
        }

        if (_ParentForm == Constants.ParentForms.Ugyirat)
        {
            var iratHelyeResult = GetUgyirat();
            IratHelye = iratHelyeResult.FelhasznaloCsoport_Id_Orzo;
        }

        if (_ParentForm == Constants.ParentForms.IratPeldany)
        {
            var iratHelyeResult = GetIratPeldany();
            IratHelye = iratHelyeResult.FelhasznaloCsoport_Id_Orzo;
        }
        IratAlairok = GetAlairok().Select(alairo => alairo.FelhasznaloCsoport_Id_Alairo).ToList() ;

        DokumentumVizualizerComponent.ClearDokumentElements();

        UI.ClearGridViewRowSelection(CsatolmanyokGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;
        Result resIratok = null;
        List<EREC_IraIratok> iratId_azonosito = new List<EREC_IraIratok>();

        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch search = (EREC_CsatolmanyokSearch)Search.GetSearchObject(Page, new EREC_CsatolmanyokSearch());

        //LZS - BLG 1636
        if (_ParentForm == Constants.ParentForms.Ugyirat)
        {
            EREC_IraIratokService serviceIraIratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParamIraIratok = UI.SetExecParamDefault(Page, new ExecParam());

            if (!String.IsNullOrEmpty(ParentId))
            {
                EREC_IraIratokSearch searchIraIratok = new EREC_IraIratokSearch();

                searchIraIratok.Ugyirat_Id.Value = ParentId;
                searchIraIratok.Ugyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                searchIraIratok.OrderBy = "Azonosito";

                resIratok = serviceIraIratok.GetAll(ExecParam, searchIraIratok);

                if (resIratok != null && !resIratok.IsError)
                {
                    string iratokIdLista = String.Empty;

                    foreach (DataRow row in resIratok.Ds.Tables[0].Rows)
                    {
                        // Ellenõrzés, hogy van-e joga az irat csatolmányaihoz, mert ha nincs, akkor ennek a csatolmányait nem kérjük le:
                        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
                        Util.LoadBusinessDocumentFromDataRow(erec_IraIratok, row);
                        bool hasRights = Contentum.eRecord.Utility.Iratok.CheckRights_Csatolmanyok(this.Page, erec_IraIratok);
                        if (!hasRights)
                        {
                            continue;
                        }

                        EREC_IraIratok irat = new EREC_IraIratok();
                        string id = row["Id"].ToString();
                        string azonosito = row["Azonosito"].ToString();
                        iratokIdLista += "'" + id + "',";

                        irat.Id = id;
                        irat.Azonosito = azonosito;
                        iratId_azonosito.Add(irat);
                    }

                    if (!String.IsNullOrEmpty(iratokIdLista))
                    {
                        #region LZS - BLG 1636
                        search.IraIrat_Id.Value = iratokIdLista.Remove(iratokIdLista.Length - 1);
                        search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.inner;
                        #endregion
                    }
                    else
                    {
                        // Ha nincs egy irat Id sem, mert nincs egyikhez sem joga, akkor ne legyen találat:
                        search.WhereByManual = (Search.IsSearchObjectEmpty(search) ? "" : " and ")
                                                + " 1=2 ";
                    }
                }
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel.Update();
                return;
            }
        }
        else if (_ParentForm == Constants.ParentForms.Kuldemeny)
        {
            if (!String.IsNullOrEmpty(ParentId))
            {
                search.KuldKuldemeny_Id.Value = ParentId;
                search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel.Update();
                return;
            }
        }
        else if (_ParentForm == Constants.ParentForms.IraIrat) // IraIratokForm.aspx
        {
            if (!String.IsNullOrEmpty(ParentId))
            {
                search.IraIrat_Id.Value = ParentId;
                search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel.Update();
                return;
            }
        }
        else if (_ParentForm == Constants.ParentForms.IratPeldany) // PldIratPeldanyokForm.aspx
        {
            // Iratpéldány lekérése az IratId meghatározásához
            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam_PldGet = UI.SetExecParamDefault(Page, new ExecParam());
            execParam_PldGet.Record_Id = ParentId;

            Result result_PldGet = service_Pld.Get(execParam_PldGet);
            if (!String.IsNullOrEmpty(result_PldGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldGet);
                ErrorUpdatePanel.Update();
                return;
            }

            EREC_PldIratPeldanyok erec_PldIratPeldany = (EREC_PldIratPeldanyok)result_PldGet.Record;

            if (!String.IsNullOrEmpty(erec_PldIratPeldany.IraIrat_Id))
            {
                search.IraIrat_Id.Value = erec_PldIratPeldany.IraIrat_Id;
                search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel.Update();
                return;
            }
        }
        else
        {
            return;
        }

        search.OrderBy = Search.GetOrderBy("CsatolmanyokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        result = service.GetAllWithExtension(ExecParam, search);

        #region LZS - BLG 1636
        if (result != null && !result.IsError)
        {
            result.Ds.Tables[0].Columns.Add("Azonosito", typeof(string));

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                iktatoszam_HiddenField.Value = result.Ds.Tables[0].Rows[0]["IktatoSzam_Merge"].ToString();
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                for (int i = 0; i < iratId_azonosito.Count; i++)
                {
                    if (row["IraIrat_Id"].ToString() == iratId_azonosito[i].Id)
                    {
                        row.SetField("Azonosito", iratId_azonosito[i].Azonosito);
                    }
                }
            }
            #endregion


            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            if (result.Ds != null)
            {
                SaveFileNamesToViewState(result.Ds);
            }

            var a = result.Ds.Tables[0].Rows;
            ui.GridViewFill(CsatolmanyokGridView, result, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(result, TabHeader);
        }
    }

    private const string viewState_FileNames = "viewState_FileNames";

    private void SaveFileNamesToViewState(DataSet ds)
    {
        if (ds == null || ds.Tables.Count == 0) { return; }

        List<string> fileNamesList = new List<string>();

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            string fileName = row["FajlNev"].ToString();
            fileNamesList.Add(fileName);
        }

        // Lista elmentése ViewState-be
        ViewState[viewState_FileNames] = fileNamesList;
    }


    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_CsatolmanyokService erec_CsatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch erec_CsatolmanyokSearch = new EREC_CsatolmanyokSearch();
            erec_CsatolmanyokSearch.Id.Value = id;
            erec_CsatolmanyokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = erec_CsatolmanyokService.GetAllWithExtension(execparam, erec_CsatolmanyokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            string IraIrat_Id = String.Empty;

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                //FajlNev                
                string fajlnev = result.Ds.Tables[0].Rows[0]["FajlNev"].ToString();
                //string externalLink = result.Ds.Tables[0].Rows[0]["External_Link"].ToString();
                string externalLink = "../GetDocumentContent.aspx?id=" + result.Ds.Tables[0].Rows[0]["krtDok_Id"].ToString();

                string dokumentumId = result.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString();

                //string alairtDokumentumExternalLink = "../GetDocumentContent.aspx?id=" + result.Ds.Tables[0].Rows[0]["AlairtDokumentum_Id"].ToString();

                IraIrat_Id = result.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

                #region BLG 2947 eredményadatok megjelenítése panelen
                //GetFelulvizsgalatEredmenye
                if (krt_Parameterek_PKI_INTEGRACIO == "Igen")
                {
                    string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                    if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "microsec".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase) || "halk".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                    {
                        AlairasAdatok_EFormPanel.Visible = true;
                        String felulvizsgalatEredmenye = GetFelulvizsgalatEredmenye(dokumentumId);
                        if (!String.IsNullOrEmpty(felulvizsgalatEredmenye))
                        {
                            String LabelText = String.Empty;
                            try
                            {
                                //XmlDocument doc = JsonConvert.DeserializeXmlNode(felulvizsgalatERedmenye);
                                //var objects = JArray.Parse(felulvizsgalatERedmenye);
                                //var tmpObj = JsonValue.Parse(felulvizsgalatERedmenye);
                                if (felulvizsgalatEredmenye == @"{ ""AlairasFelulvizsgalatEredmeny"":Dokumentum nem rendelkezik érvényes elektronikus aláírással, vagy aláírás ellenõrzés szükséges}")
                                {
                                    felulvizsgalatEredmenye = @"{ ""AlairasFelulvizsgalatEredmeny"":""Dokumentum nem rendelkezik érvényes elektronikus aláírással, vagy aláírás ellenõrzés szükséges""}";
                                }
                                JObject obj = JObject.Parse(felulvizsgalatEredmenye);

                                //var stuff = JsonConvert.DeserializeObject(felulvizsgalatEredmenye);
                                if (obj["AlairasFelulvizsgalatEredmeny"] != null)
                                {
                                    Label_FajlAdatok.Text = obj["AlairasFelulvizsgalatEredmeny"].Value<string>();
                                }
                                else
                                {
                                    if (obj["pdf"] != null)
                                    {
                                        if (obj["pdf"]["signatures"] != null)
                                        {
                                            foreach (JProperty x in obj["pdf"]["signatures"])
                                            {
                                                string name = x.Name;
                                                JToken value = x.Value;
                                                if ("signature".Equals(name, StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    Dictionary<string, string> signatureDictionary = new Dictionary<string, string>();
                                                    Dictionary<string, string> additionalInformation = new Dictionary<string, string>();
                                                    bool hasError = false;
                                                    try
                                                    {
                                                        foreach (JProperty xSig in value)
                                                        {
                                                            string nameSig = xSig.Name.StartsWith("@") && xSig.Name.Length > 1 ? xSig.Name.Substring(1) : xSig.Name;
                                                            string valueSig = xSig.Value.ToString();
                                                            if ("signerDN".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "issuer".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "issuerValidTo".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "signerTime".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase))
                                                            {
                                                                signatureDictionary.Add(nameSig, valueSig);
                                                            }
                                                            else
                                                            {
                                                                additionalInformation.Add(nameSig, valueSig);
                                                            }
                                                        }
                                                        SignatureData data = SignatureData.ParseFromDict(signatureDictionary);
                                                        LabelText += data.ToHTML(additionalInformation);

                                                    }
                                                    catch (Exception)
                                                    {
                                                        hasError = true;
                                                    }
                                                    if (hasError)
                                                    {
                                                        try
                                                        {
                                                            for (int valueInd = 0; valueInd < value.Count(); valueInd++)
                                                            {
                                                                signatureDictionary = new Dictionary<string, string>();
                                                                additionalInformation = new Dictionary<string, string>();
                                                                LabelText += String.Format("<b>Signature[{0}] adatok</b><br/><br/>", valueInd);
                                                                foreach (JProperty xSig in value[valueInd])
                                                                {
                                                                    string nameSig = xSig.Name.StartsWith("@") && xSig.Name.Length > 1 ? xSig.Name.Substring(1) : xSig.Name;
                                                                    string valueSig = xSig.Value.ToString();
                                                                    if ("signerDN".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "issuer".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "issuerValidTo".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase) || "signerTime".Equals(nameSig, StringComparison.InvariantCultureIgnoreCase))
                                                                    {
                                                                        signatureDictionary.Add(nameSig, valueSig);
                                                                    }
                                                                    else
                                                                    {
                                                                        additionalInformation.Add(nameSig, valueSig);
                                                                    }
                                                                }
                                                                SignatureData data = SignatureData.ParseFromDict(signatureDictionary);
                                                                LabelText += data.ToHTML(additionalInformation);
                                                                LabelText += "<br/><br/>";
                                                            }
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    Label_FajlAdatok.Text = LabelText;
                                }

                            }
                            catch (Exception)
                            {
                                if (string.IsNullOrEmpty(LabelText))
                                {
                                    Label_FajlAdatok.Text = String.IsNullOrEmpty(felulvizsgalatEredmenye) || felulvizsgalatEredmenye.Contains(NeedSignCheck) ? NeedSignCheck : felulvizsgalatEredmenye;
                                }
                                else
                                {
                                    Label_FajlAdatok.Text = LabelText;
                                }
                            }
                            //Label_FajlAdatok.Text = LabelText;
                        }
                    }
                }
                #endregion
                //if (fajlnev.EndsWith(".sdxm") && isRowCommandSelect)
                //{
                //    TabHeader1.LinkViewOnClientClick = "alert('A fájl aláírt');";
                //    TabHeader1.LinkViewHRef = "";
                //    TabHeader1.LinkView.Attributes["onclick"] = "alert('A fájl aláírt');";

                //    AlairasAdatok_EFormPanel.Visible = true;

                //    #region SignerData

                //    eSign.SDXM.Url = UI.GetAppSetting("eASZBusinessServiceUrl");
                // nekrisz: Aláírásellenörzés csak ha PKI_Integracio be van kapcsolva

                //BLG 2947 - automatikus ellenõrzés helyett ellenõrzés gombnyomásra!
                //if (krt_Parameterek_PKI_INTEGRACIO == "Igen")
                //{
                //    string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_VENDOR");
                //    if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase)) /*&&
                //    Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()), "PKI_INTEGRACIO").ToLower().Equals("igen"))*/
                //    {
                //        KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                //        ExecParam docExecParam = execparam.Clone();
                //        docExecParam.Record_Id = result.Ds.Tables[0].Rows[0]["krtDok_Id"].ToString();
                //        Result dokResult = dokService.Get(docExecParam);
                //        if (!dokResult.IsError)
                //        {
                //            KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                //            bool AlairasChanged = false;
                //            Contentum.eUtility.NETLOCKSignatureService netlockService = new Contentum.eUtility.NETLOCKSignatureService();
                //            //TESZT
                //            AlairasAdatok_EFormPanel.Visible = true;
                //            netlockService.VerifyPDF(docRecord.FajlNev, docRecord.External_Link, UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                //            if (netlockService.RaiseError)
                //            {
                //                Label_FajlAdatok.Text = "A fájl nincs aláírva vagy az aláírás érvénytelen.";
                //                AlairasChanged = !(docRecord.ElektronikusAlairas == KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles);
                //            }
                //            else
                //            {
                //                SignatureData data = SignatureData.Parse(netlockService.SignatureData);
                //                Label_FajlAdatok.Text = data.ToHTML();
                //                AlairasChanged = !(docRecord.ElektronikusAlairas == KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas);
                //            }
                //            // CR 3033: Aláírásváltozás esetén KrT_Dokumentumok mósosítás  -- nekrisz 2016.09.19.
                //            if (AlairasChanged)
                //            {
                //                //ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                //                //dokExecParam.Record_Id = result.Ds.Tables[0].Rows[0]["krtDok_Id"].ToString(); ;

                //                docRecord.Updated.SetValueAll(false);
                //                docRecord.Base.Updated.SetValueAll(false);
                //                //LoadComponentsFromBusinessObjects_DokumentumVizsgalat(docRecord);
                //                //docRecord.Base.Ver = Record_Ver_HiddenField_KRT_Dokumentum.Value;
                //                docRecord.Base.Updated.Ver = true;

                //                docRecord.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                //                docRecord.Updated.AlairasFelulvizsgalat = true;

                //                if (netlockService.RaiseError)
                //                {
                //                    docRecord.ElektronikusAlairas = KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles;
                //                }
                //                else
                //                {
                //                    docRecord.ElektronikusAlairas = KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;
                //                }
                //                docRecord.Updated.ElektronikusAlairas = true;

                //                Result result_alairas = dokService.Update(docExecParam, docRecord);
                //                if (String.IsNullOrEmpty(result_alairas.ErrorCode))
                //                {
                //                    CsatolmanyokGridViewBind_WithReselectRow();
                //                }
                //                else
                //                {
                //                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_alairas);
                //                    ErrorUpdatePanel.Update();
                //                }

                //            }
                //        }
                //    }
                //}


                //    System.Net.WebRequest webRequest = System.Net.WebRequest.Create(externalLink);

                //    webRequest.Credentials = new NetworkCredential("contentumspuser","123456");
                //    System.IO.Stream ioStream = webRequest.GetResponse().GetResponseStream();

                //    //byte[] file_buffer = null;

                //    System.IO.StreamReader sr = new System.IO.StreamReader(ioStream);
                //    string content = sr.ReadToEnd();
                //    sr.Close();
                //    //ioStream.Read(file_buffer, 0, (Int32) ioStream.Length);
                //    //ioStream.Close();

                //    ////System.IO.StreamReader streamReader = new System.IO.StreamReader(externalLink);                   

                //    //string content = System.Convert.ToBase64String(file_buffer);                    

                //    eSign.SignerData signerData = eSign.SDXM.SignerData(content);

                //    Label_FajlAdatok.Text = signerData.FileName + "<br/>" + signerData.X509CertificateIssuer
                //            + "<br/>" + signerData.X509CertificateIssuerName
                //            + "<br/>" + signerData.X509CertificateName
                //            + "<br/>" + signerData.X509CertificateSubject
                //            + "<br/>" + signerData.X509ExprirationDate
                //            + "<br/>" + signerData.X509IssuerName;


                //    //Label_FajlAdatok

                //    #endregion

                //}

                if (Contentum.eUtility.CsatolmanyokUtility.CanDecrypt(result.Ds.Tables[0].Rows[0]["Formatum"].ToString().ToLower(), execparam))
                {
                    string link = "./GetDocumentContent.aspx?id=" + result.Ds.Tables[0].Rows[0]["krtDok_Id"].ToString();
                    TabHeader1.DecryptVisible = true;
                    TabHeader1.DecryptEnabled = true;
                    TabHeader1.DecryptOnClientClick = "window.open('" + link + "&mode=decrypt','_blank'); return false;";
                }
                else
                {
                    TabHeader1.DecryptVisible = false;
                    TabHeader1.DecryptEnabled = false;
                    TabHeader1.DecryptOnClientClick = "";
                }
                //TabHeader1.LinkView.Attributes["onclick"] = String.Empty;
                //TabHeader1.LinkViewHRef = "../GetDocumentContent.aspx?id=" + result.Ds.Tables[0].Rows[0]["krtDok_Id"].ToString();
                //TabHeader1.LinkViewOnClientClick = JavaScripts.SetOnClientClickSharePointOpenDocument();

                //if (!string.IsNullOrEmpty(alairtDokumentumExternalLink))
                //{
                TabHeader1.AlairasAdatokOnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("AlairasAdatok.aspx"
                    , QueryStringVars.DokumentumId + "=" + dokumentumId, Defaults.PopupWidth_Max, Defaults.PopupHeight);
                //}
                //else
                //{
                //    TabHeader1.AlairasAdatokOnClientClick = " alert('" + Resources.Error.UIDokumentumNincsAlairva + "'); return false;";
                //}

                TabHeader1.HitelessegiZaradekEnabled = true;
                TabHeader1.HitelessegiZaradekVisible = true;
                TabHeader1.HitelessegiZaradekOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("HitelessegiZaradekPrintForm.aspx?" + QueryStringVars.CsatolmanyId + "=" + id);
            }
            else
            {
                TabHeader1.LinkViewHRef = "";
                TabHeader1.LinkViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";

            // Mellékletek SubList:
            string queryString = QueryStringVars.CsatolmanyId + "=" + id;
            if (ParentForm == Constants.ParentForms.Kuldemeny)
            {
                queryString += "&" + QueryStringVars.KuldemenyId + "=" + ParentId;
            }
            else if (ParentForm == Constants.ParentForms.IraIrat)
            {
                queryString += "&" + QueryStringVars.IratId + "=" + ParentId;
            }
            else if (ParentForm == Constants.ParentForms.IratPeldany)
            {
                queryString += "&" + QueryStringVars.IratId + "=" + IraIrat_Id;
            }

            MellekletekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("CsatolmanyMellekletKapcsolatForm.aspx"
                , queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            TabHeader1.ElektronikusAlairasOnClientClick = Iratok.SetClientOnClickAlairas(ParentId, Page);
            // nekrisz: Aláírás csak ha PKI_Integracio be van kapcsolva
            if (krt_Parameterek_PKI_INTEGRACIO == "Igen" && (_ParentForm == Constants.ParentForms.IraIrat || _ParentForm == Constants.ParentForms.IratPeldany))
            {
                string procId = string.Empty;
                if (CreateAlairasFolyamat(ParentId, out procId))
                {
                    String noSelectedError = alairasCsakFoDok() ? "if(true) " : "if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} else ";
                    TabHeader1.ElektronikusAlairasOnClientClick = Iratok.SetClientOnClickAlairas(ParentId, Page);
                    //Netlock kliens oldali aláírás nem megy dialog ablakban
                    string pkiVendor = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "PKI_VENDOR");
                    bool noDialog = "NETLOCK".Equals(pkiVendor, StringComparison.InvariantCultureIgnoreCase);
                    TabHeader1.ElektronikusAlairasOnClientClick += "var count = getSelectedCheckBoxesCount('"
                       + CsatolmanyokGridView.ClientID + "','check'); " + noSelectedError + " {" +
                       JavaScripts.SetOnClientClick("DokumentumAlairas.aspx",
                       QueryStringVars.CsatolmanyId + "=" + "'+getIdsBySelectedCheckboxes('" + CsatolmanyokGridView.ClientID + "','check')+'" + "&" + QueryStringVars.ProcId + "=" + procId
                       , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolmanyokList, false, noDialog)
                       + "}";
                }
            }
            else
            {
                TabHeader1.ElektronikusAlairasOnClientClick += JavaScripts.SetOnClientClick("DokumentumAlairas.aspx",
                    QueryStringVars.CsatolmanyId + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshCsatolmanyokList, false, false);
            }

            TabHeader1.OCROnClientClick = "";


            if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, IratHelye,IratAlairok))
            {
                TabHeader1.ImageElektronikusAlairas.Enabled = false;
            }
            else
            {
                TabHeader1.ImageElektronikusAlairas.Enabled = true;

            }

        }
    }


    //bool isRowCommandSelect = false;

    #region BUG 7471

    private bool isKrFile(string extension)
    {
        // BUG_8852 
        // A krx kiterjesztésre való szûrést közmegegyezéses alapon kiszedtem.
        //return "kr".Equals(extension, StringComparison.CurrentCultureIgnoreCase) || "krx".Equals(extension, StringComparison.CurrentCultureIgnoreCase);
        return "kr".Equals(extension, StringComparison.CurrentCultureIgnoreCase);

    }
    #region BUG 9982
    private string GetAdathordozoTipusa(string IraIrat_Id)
    {
        string retValue = string.Empty;
        if (!string.IsNullOrEmpty(IraIrat_Id))
        {
            try
            {
                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParamIra = UI.SetExecParamDefault(Page, new ExecParam());
                execParamIra.Record_Id = IraIrat_Id;
                Result resIra = service_iratok.Get(execParamIra);
                if (String.IsNullOrEmpty(resIra.ErrorCode))
                {
                    EREC_IraIratok irat = (EREC_IraIratok)resIra.Record;
                    retValue = irat.AdathordozoTipusa;
                }
            }
            catch (Exception) { }
        }

        return retValue;
    }
    #endregion
    private bool IsElektronikus(string IraIrat_Id)
    {
        if (String.IsNullOrEmpty(IraIrat_Id)) return false;
        try
        {
            string adathordozoTipusa = GetAdathordozoTipusa(IraIrat_Id);
            if (adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
            {
                return true;
            }
        }
        catch (Exception) { }
        return false;
    }

    private bool IsPkiFile(string formatum, bool isElektronikus)
    {
        if (!String.IsNullOrEmpty(formatum))
        {
            try
            {
                string pkiFileTypes = Rendszerparameterek.Get(Page, "PKI_FILETYPES");

                if (String.IsNullOrEmpty(pkiFileTypes))
                {
                    pkiFileTypes = "pdf";
                }
                if (isElektronikus)
                {
                    pkiFileTypes += ",kr,krx";
                }
                if (pkiFileTypes.ToLower().Contains(formatum.ToLower()))
                    return true;
            }
            catch (Exception) { }
        }

        return false;
    }

    private bool alairasCsakFoDok()
    {
        ExecParam execParamRP = UI.SetExecParamDefault(Page, new ExecParam());
        string csakFodok = Rendszerparameterek.Get(execParamRP, "ALAIRAS_CSAK_FODOKUMENTUM");
        return "1".Equals(csakFodok);
    }
    #endregion

    private bool CreateAlairasFolyamat(string iratId, out string procId)
    {
        procId = string.Empty;
        #region getCsatolmanyok
        String csatolManyIds = "";

        EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        //EREC_CsatolmanyokService service_csat = new EREC_CsatolmanyokService(this.dataContext);

        EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

        Logger.Debug("AlairasFolyamat IratId: " + iratId);
        search_csatolmanyok.IraIrat_Id.Value = "'" + iratId + "'";
        search_csatolmanyok.IraIrat_Id.Operator = Query.Operators.inner;

        Result result_csatGetAll = service_csatolmanyok.GetAll(Contentum.eUtility.UI.SetExecParamDefault(Page), search_csatolmanyok);
        #region createAlairasFolyamat
        Contentum.eRecord.Service.EREC_Alairas_FolyamatokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();

        EREC_Alairas_Folyamatok erec_AlairasFolyamatok = new EREC_Alairas_Folyamatok();

        erec_AlairasFolyamatok.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.folyamat_rogzitve;
        ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);
        Result result_Insert = service.Insert(execParam_Insert, erec_AlairasFolyamatok);
        if (result_Insert.IsError)
        {
            Logger.Error("Hiba: EREC_Alairas_FolyamatokService.Insert", execParam_Insert, result_Insert);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert);
            return false;
        }
        procId = result_Insert.Uid;

        #endregion

        int rowInd = 0;
        if (!result_csatGetAll.IsError)
        {
            //BUG 7471
            foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
            {
                #region BUG 7471
                Boolean NeedFile = true;
                string DokumentumSzerep = row["DokumentumSzerep"].ToString();
                String IraIrat_Id = row["IraIrat_Id"].ToString();
                if (alairasCsakFoDok())
                {
                    if (KodTarak.DOKUMENTUM_SZEREP.Fodokumentum.Equals(DokumentumSzerep))
                    {
                        String DokId = row["Dokumentum_Id"].ToString();
                        KRT_DokumentumokService dokServ = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                        ExecParam execParamDok = UI.SetExecParamDefault(Page, new ExecParam());
                        execParamDok.Record_Id = DokId;
                        Result resDok = dokServ.Get(execParamDok);
                        if (String.IsNullOrEmpty(resDok.ErrorCode))
                        {
                            KRT_Dokumentumok dok = (KRT_Dokumentumok)resDok.Record;
                            bool isElektronikus = IsElektronikus(IraIrat_Id);
                            //BUG 9982
                            string adathordozoTipusa = GetAdathordozoTipusa(IraIrat_Id);
                            if (!string.IsNullOrEmpty(adathordozoTipusa) && adathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir)
                            {
                                string allowedfiletypes = "pdf,doc,docx";
                                string formatum = dok.Formatum.ToLower();
                                NeedFile = allowedfiletypes.ToLower().Contains(formatum);
                            }
                            else
                            {
                                NeedFile = IsPkiFile(dok.Formatum.ToLower(), isElektronikus);
                            }
                        }
                    }
                    else
                    {
                        NeedFile = false;
                    }
                }
                #endregion
                if (NeedFile)
                {
                    string row_Id = row["Id"].ToString();
                    Logger.Debug("Aláírandó csatolmány ID: " + row_Id);

                    csatolManyIds += rowInd == 0 ? row_Id : "," + row_Id;

                    #region createAlairasFolyamatTetelek
                    Contentum.eRecord.Service.EREC_Alairas_Folyamat_TetelekService tetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                    EREC_Alairas_Folyamat_Tetelek erec_Alairas_Folyamat_Tetelek = new EREC_Alairas_Folyamat_Tetelek();
                    erec_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id = procId;
                    erec_Alairas_Folyamat_Tetelek.Csatolmany_Id = row_Id;
                    erec_Alairas_Folyamat_Tetelek.IraIrat_Id = IraIrat_Id;
                    erec_Alairas_Folyamat_Tetelek.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.folyamat_rogzitve;
                    ExecParam execParam_InsertTetel = Contentum.eUtility.UI.SetExecParamDefault(Page);
                    Result result_Insert_Tetel = tetelekService.Insert(execParam_InsertTetel, erec_Alairas_Folyamat_Tetelek);
                    if (result_Insert_Tetel.IsError)
                    {
                        Logger.Error("Hiba: EREC_Alairas_Folyamat_TetelekService.Insert", execParam_InsertTetel, result_Insert_Tetel);
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert_Tetel);
                        return false;
                    }
                    #endregion

                    rowInd++;
                }
            }
        }
        else
        {
            Logger.Error("AlairasFolyamat IratId: " + iratId + " -hoz tartozó csatolmány lekérdezés hiba!" + result_csatGetAll.ErrorCode + result_csatGetAll.ErrorMessage);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_csatGetAll);
            return false;
        }
        #endregion

        return true;
    }

    protected void CsatolmanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;
            //isRowCommandSelect = true;

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            SubListPanel.Visible = true;
            ActiveTabRefresh(TabContainer1, id);
        }
    }

    protected void CsatolmanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbMegnyithato", "Megnyithato");
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbOlvashato", "Olvashato");
        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");

        GridView_RowDataBound_SetMellekletImage(e);

        UI.GridView_RowDataBound_SetImageVisibility(e, "TitkositasImage", "Titkositas");

        UI.GridView_RowDataBound_SetLockingInfo(e, Page, "krtDok_Zarolo_id", "krtDok_ZarolasIdo");

        #region CR3129 - Szükség lenne egy új ikonra az aláíráshoz
        String Mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (Mode == CommandName.Alairas)
        {
            if (e.Row != null && e.Row.FindControl("check") != null && e.Row.FindControl("check") is CheckBox)
            {
                var chk = e.Row.FindControl("check") as CheckBox;
                chk.Checked = true;
            }
        }
        #endregion
    }

    private void GridView_RowDataBound_SetMellekletImage(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string mellekletCount = String.Empty;

            if (drw["MellekletCount"] != null)
            {
                mellekletCount = drw["MellekletCount"].ToString();
            }

            if (!String.IsNullOrEmpty(mellekletCount))
            {
                Image MellekletImage = (Image)e.Row.FindControl("MellekletImage");
                if (MellekletImage != null)
                {
                    int count = 0;
                    Int32.TryParse(mellekletCount, out count);
                    if (count > 0)
                    {
                        MellekletImage.Visible = true;
                        MellekletImage.ToolTip = String.Format(Resources.Form.UI_MellekletImage_ToolTip_WithCount, mellekletCount);
                        MellekletImage.AlternateText = MellekletImage.ToolTip;
                    }
                    else
                    {
                        MellekletImage.Visible = false;
                    }
                }

            }
            else
            {
                Image MellekletImage = (Image)e.Row.FindControl("MellekletImage");
                if (MellekletImage != null)
                {
                    MellekletImage.Visible = false;
                }

            }
        }
    }


    /// <summary>
    /// Formatum mezõ alapján beállítja a fájlikonokat
    /// </summary>
    public void SetFileIcons_GridViewRowDataBound(GridViewRowEventArgs e, Page page, string imageButtonId)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType != DataControlRowType.DataRow) { return; }

        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;

        string formatum = String.Empty;
        string externalLink = String.Empty;
        string downloadDocumentId = String.Empty;

        if (dataRowView != null && dataRowView["Formatum"] != null)
        {
            formatum = dataRowView["Formatum"].ToString().ToLower();
        }

        if (dataRowView != null && dataRowView["External_Link"] != null)
        {
            //externalLink = drv["External_Link"].ToString();
            externalLink = "GetDocumentContent.aspx?id=" + dataRowView["krtDok_Id"];
            downloadDocumentId = dataRowView["krtDok_Id"].ToString();
        }

        if (string.IsNullOrEmpty(formatum))
        {
            return;
        }
        ImageButton fileIcon = (ImageButton)e.Row.FindControl(imageButtonId);

        if (fileIcon == null)
        {
            return;
        }
        fileIcon.ImageUrl = "~/images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum);


        if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, IratHelye,IratAlairok))
        {
            // TabHeader1.ImageElektronikusAlairas.Enabled = false;
            fileIcon.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight);
            fileIcon.Enabled = false;
            fileIcon.OnClientClick = "";
            TabHeader1.ImageElektronikusAlairas.Enabled = false;
            return;
        }
        else
        {
            TabHeader1.ImageElektronikusAlairas.Enabled = true;

        }

        if (!String.IsNullOrEmpty(externalLink))
        {
            //fileIcon.OnClientClick = "window.open('" + externalLink + "','_blank'); window.open('" + externalLink + "','_blank'); window.open('" + externalLink + "','_blank'); window.open('" + externalLink + "'); window.open('" + externalLink + "','_blank'); return false;";
            fileIcon.OnClientClick = "downLoadDocument('" + CsatolmanyokGridView.ClientID + "', '" + downloadDocumentId + "'); return false;";

            //fileIcon.Attributes.Add("downloadDocumentId", downloadDocumentId);
            e.Row.Attributes.Add("downloadDocumentId", downloadDocumentId);
            fileIcon.Enabled = true;
        }

        DokumentumVizualizerComponent.AddDokumentElement(
            fileIcon.ClientID, dataRowView["krtDok_Id"].ToString(), dataRowView["FajlNev"].ToString(), dataRowView["VerzioJel"].ToString(), dataRowView["External_Link"].ToString());
    }

    protected void CsatolmanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolmanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("CsatolmanyokGridView", ViewState, e.SortExpression));
    }


    private void LockSelectedDokumentumRecords()
    {
        // KRT_Dokumentumok rekordokat lockolunk, nem EREC_Csatolmanyokat
        List<string> dokumentumIdList = UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "Label_krtDok_Id");
        LockManager.LockSelectedRecords(dokumentumIdList, "KRT_Dokumentumok"
            , "DokumentumokLock", "DokumentumokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);

        ExecParam execParam = UI.SetExecParamDefault(Page);

        string documentStoreType =
            Contentum.eUtility.Rendszerparameterek.Get(execParam,
            Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
        {
            UCMDocumentService service = eDocumentService.ServiceFactory.GetUCMDocumentService();
            service.CheckOut(execParam, dokumentumIdList.ToArray());
        }
    }

    private void UnlockSelectedDokumentumRecords()
    {
        // KRT_Dokumentumok rekordokat lockolunk, nem EREC_Csatolmanyokat
        List<string> dokumentumIdList = UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "Label_krtDok_Id");
        LockManager.UnlockSelectedRecords(dokumentumIdList, "KRT_Dokumentumok"
            , "DokumentumokLock", "DokumentumokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);

        ExecParam execParam = UI.SetExecParamDefault(Page);

        string documentStoreType =
            Contentum.eUtility.Rendszerparameterek.Get(execParam,
            Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
        {
            UCMDocumentService service = eDocumentService.ServiceFactory.GetUCMDocumentService();
            service.CancelCheckOut(execParam, dokumentumIdList.ToArray());
        }
    }

    #endregion

    #region Alairas Cuccok

    private const string viewState_AlairasFolyamatKod = "AlairasFolyamatKod";
    private const string viewState_DokumentumMuveletKod = "DokumentumMuveletKod";
    private const string viewState_AlairasAdatok_Dict = "AlairasAdatok_Dict";
    private const string viewState_KliensoldaliAlairasSzabalyok = "KliensoldaliAlairasSzabalyList";
    private const string viewState_ERROR_AlairasSzabalyFeltoltes = "ERROR_AlairasSzabalyFeltoltes";

    private const string style_DefaultListItem = "background-color: #00CCFF;";

    //protected void cb_Alairas_CheckedChanged(object sender, EventArgs e)

    protected void FillAlairasSzabalyDropDownList()
    {

        #region Folyamat- és mûveletkód meghatározása

        string folyamatKod = "";
        string muveletKod = "";

        if (ViewState[viewState_AlairasFolyamatKod] != null && !string.IsNullOrEmpty(ViewState[viewState_AlairasFolyamatKod].ToString()))
        {
            folyamatKod = ViewState[viewState_AlairasFolyamatKod].ToString();

            if (ViewState[viewState_DokumentumMuveletKod] != null)
            {
                muveletKod = ViewState[viewState_DokumentumMuveletKod].ToString();
            }
        }
        else
        {
            if (ParentForm == Constants.ParentForms.Kuldemeny)
            {
                folyamatKod = Constants.FolyamatKodok.KuldemenyErkeztetes;
                muveletKod = Constants.DokumentumMuveletKodok.KuldemenyFeltoltesAdathordozorol;
            }
            else if (ParentForm == Constants.ParentForms.IraIrat)
            {
                // meg kell nézni, hogy belsõ irat, vagy bejövõ irat
                string iratId = ParentId;

                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iratGet.Record_Id = iratId;

                Result result_iratGet = service_iratok.Get(execParam_iratGet);
                if (result_iratGet.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iratGet);
                    ErrorUpdatePanel.Update();
                    return;
                }

                EREC_IraIratok iratObj = (EREC_IraIratok)result_iratGet.Record;

                if (iratObj.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                {
                    folyamatKod = Constants.FolyamatKodok.KuldemenyErkeztetes;
                    muveletKod = Constants.DokumentumMuveletKodok.KuldemenyFeltoltesAdathordozorol;
                }
                else if (iratObj.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                {
                    folyamatKod = Constants.FolyamatKodok.BelsoIratIktatas;
                    muveletKod = Constants.DokumentumMuveletKodok.IrattervezetFeltoltes;
                }
            }
            else if (ParentForm == Constants.ParentForms.IratPeldany)
            {
                // iratot le kell kérni:
                string iratPeldanyId = ParentId;

                EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam_pldGet = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_pldGet.Record_Id = iratPeldanyId;

                Result result_pldGet = service_pld.Get(execParam_pldGet);
                if (result_pldGet.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_pldGet);
                    ErrorUpdatePanel.Update();
                    return;
                }

                EREC_PldIratPeldanyok pldObj = (EREC_PldIratPeldanyok)result_pldGet.Record;

                // irat lekérése:
                string iratId = pldObj.IraIrat_Id;

                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iratGet.Record_Id = iratId;

                Result result_iratGet = service_iratok.Get(execParam_iratGet);
                if (result_iratGet.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iratGet);
                    ErrorUpdatePanel.Update();
                    return;
                }

                EREC_IraIratok iratObj = (EREC_IraIratok)result_iratGet.Record;

                if (iratObj.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                {
                    folyamatKod = Constants.FolyamatKodok.KuldemenyErkeztetes;
                    muveletKod = Constants.DokumentumMuveletKodok.KuldemenyFeltoltesAdathordozorol;
                }
                else if (iratObj.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                {
                    folyamatKod = Constants.FolyamatKodok.BelsoIratIktatas;
                    muveletKod = Constants.DokumentumMuveletKodok.IrattervezetFeltoltes;
                }
            }

            // folyamatKod és mûveletkód lementése ViewState-be, hogy ne kelljen újból lekérni az iratot
            ViewState[viewState_AlairasFolyamatKod] = folyamatKod;
            ViewState[viewState_DokumentumMuveletKod] = muveletKod;
        }

        #endregion

        #region Aláírásszabályok feltöltése

        ddl_AlairasSzabalyok.Items.Clear();

        AlairasokService service_alairasok = eRecordService.ServiceFactory.GetAlairasokService();

        Result result_alairasLista = service_alairasok.GetAll_PKI_MuveletAlairasLista(UI.SetExecParamDefault(Page, new ExecParam())
            , null, folyamatKod, muveletKod, null, false);

        if (result_alairasLista.IsError)
        {
            // hiba:
            //ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_alairasLista);
            //ErrorUpdatePanel.Update();

            // Nem rakunk ki hibapanelt, a hibaüzenetet elmentjük, és a checkbox megnyomására megjelenítjük
            string errorMessage = ResultError.GetErrorMessageFromResultObject(result_alairasLista);
            ViewState[viewState_ERROR_AlairasSzabalyFeltoltes] = "Hiba az aláírás szabályok lekérése során! \\n" + errorMessage;
            return;
        }
        else
        {
            ViewState[viewState_ERROR_AlairasSzabalyFeltoltes] = null;
        }

        if (result_alairasLista.Ds != null)
        {
            string default_alairasSzabaly_Id = "";

            Dictionary<string, Csatolmany.Alairas> dict_alairasAdatok = new Dictionary<string, Csatolmany.Alairas>();
            List<string> kliensoldaliAlairasSzabalyok = new List<string>();

            // dropdown feltöltése:
            foreach (DataRow row in result_alairasLista.Ds.Tables[0].Rows)
            {
                string AlairasNev = row["AlairasNev"].ToString();
                string AlairasSzabaly_Id = row["AlairasSzabaly_Id"].ToString();
                //string EASZKod = row["EASZKod"].ToString();
                string EASZKod = row["KonfigTetel"].ToString();
                string AlairasSzint = row["AlairasSzint"].ToString();
                string Tanusitvany_Id = row["Tanusitvany_Id"].ToString();
                string KivarasiIdo = row["KivarasiIdo"].ToString();
                string AlairasMod = row["AlairasMod"].ToString();
                string AlairasTulajdonos = row["AlairasTulajdonos"].ToString();
                string Kivitelezes = row["Kivitelezes"].ToString();

                if (string.IsNullOrEmpty(AlairasSzabaly_Id))
                {
                    continue;
                }

                bool defaultRow = false;
                if (row["DefSzabaly"].ToString() == "1")
                {
                    defaultRow = true;
                    default_alairasSzabaly_Id = AlairasSzabaly_Id;
                }

                // dictionary-ben eltároljuk az EASzKod-ot, aláírásszintet, és a tanusítvány id-t is, mert majd kelleni fognak
                if (!dict_alairasAdatok.ContainsKey(AlairasSzabaly_Id))
                {
                    Csatolmany.Alairas alairasAdatok = new Csatolmany.Alairas();
                    alairasAdatok.CsatolmanyAlairando = true;
                    alairasAdatok.AlairasSzabaly_Id = AlairasSzabaly_Id;
                    alairasAdatok.EASzKod = EASZKod;
                    alairasAdatok.Tanusitvany_Id = Tanusitvany_Id;
                    alairasAdatok.AlairasMod = AlairasMod;
                    alairasAdatok.AlairasTulajdonos = AlairasTulajdonos;

                    try
                    {
                        alairasAdatok.AlairasSzint = Int32.Parse(AlairasSzint);
                    }
                    catch
                    {
                        // hiba:
                        Logger.Error("Aláírásszint konvertálásakor hiba. Aláírásszint: " + AlairasSzint);
                    }

                    try
                    {
                        alairasAdatok.KivarasiIdo = Int32.Parse(KivarasiIdo);
                    }
                    catch
                    {
                        // hiba:
                        Logger.Error("Kivárási idõ konvertálásakor hiba. Kivárási idõ: " + KivarasiIdo);
                    }

                    dict_alairasAdatok.Add(AlairasSzabaly_Id, alairasAdatok);
                }

                // A kliensoldali aláírásszabályId-k kigyûjtése:
                if (Kivitelezes == "Kliens" && !kliensoldaliAlairasSzabalyok.Contains(AlairasSzabaly_Id))
                {
                    kliensoldaliAlairasSzabalyok.Add(AlairasSzabaly_Id);
                }

                ListItem listItem = new ListItem(AlairasNev, AlairasSzabaly_Id);
                if (defaultRow)
                {
                    // háttérSzín a default elemnek:
                    listItem.Attributes.Add("style", style_DefaultListItem);
                }
                ddl_AlairasSzabalyok.Items.Add(listItem);
            }

            // ha nem volt egy aláírásszabálya sem, akkor nem írhat alá --> checkbox letiltva
            if (ddl_AlairasSzabalyok.Items.Count == 0)
            {
                cb_Alairas.Checked = false;
                cb_Alairas.Enabled = false;
                ddl_AlairasSzabalyok.Enabled = false;

                Contentum.eUtility.ResultError.DisplayWarningOnErrorPanel(EErrorPanel1,
                    Resources.Error.WarningLabel, Resources.Error.UI_NincsAlairasSzabaly);
                ErrorUpdatePanel.Update();
                return;
            }

            if (!string.IsNullOrEmpty(default_alairasSzabaly_Id))
            {
                // ráállás a default elemre:
                if (ddl_AlairasSzabalyok.Items.FindByValue(default_alairasSzabaly_Id) != null)
                {
                    ddl_AlairasSzabalyok.SelectedValue = default_alairasSzabaly_Id;
                }
            }

            // dictionary-t letesszük ViewState-be, hogy ne kelljen újból lekérni az adatokat:
            ViewState[viewState_AlairasAdatok_Dict] = dict_alairasAdatok;

            // a kliensoldali aláírásszabályokat is letesszük ViewState-be (hogy mindig le tudjuk küldeni a javascriptnek)
            ViewState[viewState_KliensoldaliAlairasSzabalyok] = kliensoldaliAlairasSzabalyok;
        }


        ddl_AlairasSzabalyok.Enabled = true;
        ddl_AlairasSzabalyok.Visible = true;

        #endregion

        if (cb_Alairas.Checked == true)
        {
            ddl_AlairasSzabalyok.Enabled = true;
            ddl_AlairasSzabalyok.Style["display"] = "inline";
        }
        else
        {
            // cb_Alairas.Checked == false

            ddl_AlairasSzabalyok.Enabled = false;
            ddl_AlairasSzabalyok.Style["display"] = "none";
        }
    }

    #endregion


    #region Detail Tab

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (CsatolmanyokGridView.SelectedIndex == -1)
        {
            return;
        }
        string csatolmanyId = UI.GetGridViewSelectedRecordId(CsatolmanyokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, csatolmanyId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string csatolmanyId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                MellekletekGridViewBind(csatolmanyId);
                MellekletekPanel.Visible = true;
                break;
            case 1:
                DokumentumHierarchia1.LoadComponentByCsatolmanyId(csatolmanyId);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        MellekletekSubListHeader.RowCount = RowCount;
    }


    #endregion


    #region Mellékletek Sublist

    private void MellekletekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IratelemKapcsolatok();
            MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
            // csatolmányokat is frissíteni kell a mellékletre vonatkozó infók miatt
            CsatolmanyokGridViewBind_WithReselectRow();
        }
    }

    protected void MellekletekGridViewBind(string csatolmanyId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MellekletekGridView", ViewState, "Letrehozasido");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MellekletekGridView", ViewState, SortDirection.Descending);

        MellekletekGridViewBind(csatolmanyId, sortExpression, sortDirection);
    }

    protected void MellekletekGridViewBind(string csatolmanyId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(csatolmanyId))
        {
            EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratelemKapcsolatokSearch search = new EREC_IratelemKapcsolatokSearch();

            search.Csatolmany_Id.Value = csatolmanyId;
            search.Csatolmany_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("MellekletekGridView", ViewState, SortExpression, SortDirection);

            MellekletekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(execParam, search);

            UI.GridViewFill(MellekletekGridView, res, MellekletekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, MellekletekTabPanel);
        }
        else
        {
            ui.GridViewClear(MellekletekGridView);
        }
    }


    void MellekletekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView));
    }

    protected void MellekletekPanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainer1.ActiveTab.Equals(MellekletekTabPanel))
                    {
                        MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView));
                        // csatolmányokat is frissíteni kell a mellékletre vonatkozó infók miatt
                        CsatolmanyokGridViewBind_WithReselectRow();
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Törli a MellekletekGridView -ban kijelölt kapcsolatokat
    /// </summary>
    private void deleteSelected_IratelemKapcsolatok()
    {
        // Jogosultságkezelés:
        if (FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatInvalidate))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(MellekletekGridView, EErrorPanel1, ErrorUpdatePanel);

            EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }

    protected void MellekletekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MellekletekGridView, selectedRowNumber, "check");
        }
    }

    private void MellekletekGridView_RefreshOnClientClicks(string mellekletId)
    {
        //string id = mellekletId;
        //if (!String.IsNullOrEmpty(id))
        //{
        //    MellekletekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);
        //    MellekletekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        //}
    }

    protected void MellekletekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = MellekletekGridView.PageIndex;

        MellekletekGridView.PageIndex = MellekletekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != MellekletekGridView.PageIndex)
        {
            ////scroll állapotának törlése
            //JavaScripts.ResetScroll(Page, CsoportokCPE);
            MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView));
        }
        else
        {
            //UI.GridViewSetScrollable(MellekletekSubListHeader.Scrollable, MellekletekCPE);
        }
        MellekletekSubListHeader.PageCount = MellekletekGridView.PageCount;
        MellekletekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(MellekletekGridView);
    }


    void MellekletekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(MellekletekSubListHeader.RowCount);
        MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView));
    }

    protected void MellekletekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MellekletekGridViewBind(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView)
            , e.SortExpression, UI.GetSortToGridView("MellekletekGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region Kimenõ küldemény csatolmányai

    protected void KimenoCsatolmanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            //int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            //UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedRowNumber, "check");
            //EFormPanel1.Visible = false;

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //SubListPanel.Visible = true;
            //ActiveTabRefresh(TabContainer1, id);
        }
    }

    protected void KimenoCsatolmanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");
    }

    protected void KimenoCsatolmanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KimenoCsatolmanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("KimenoCsatolmanyokGridView", ViewState, e.SortExpression));
    }

    protected void KimenoCsatolmanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KimenoCsatolmanyokGridView", ViewState, "IktatoSzam_Merge, FajlNev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KimenoCsatolmanyokGridView", ViewState);
        KimenoCsatolmanyokGridViewBind(sortExpression, sortDirection);
    }

    protected void KimenoCsatolmanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (_ParentForm == Constants.ParentForms.IratPeldany) // PldIratPeldanyokForm.aspx
        {
            //DokumentumVizualizerComponent.ClearDokumentElements();

            UI.ClearGridViewRowSelection(KimenoCsatolmanyokGridView);
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = null;

            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

            search.KuldKuldemeny_Id.Value = String.Format("select KuldKuldemeny_Id EREC_Kuldemeny_IratPeldanyai from EREC_Kuldemeny_IratPeldanyai where Peldany_Id='{0}' and getdate() between ErvKezd and ErvVege", ParentId);
            search.KuldKuldemeny_Id.Operator = Query.Operators.inner;

            search.OrderBy = Search.GetOrderBy("KimenoCsatolmanyokGridView", ViewState, SortExpression, SortDirection);
            search.TopRow = UI.GetTopRow(Page);

            result = service.GetAllWithExtension(ExecParam, search);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            int count = result.Ds.Tables[0].Rows.Count;


            if (count > 0)
            {
                //iktatószámok lekérése
                FillDokumentumokIktatoszam(result.Ds.Tables[0]);
                ui.GridViewFill(KimenoCsatolmanyokGridView, result, EErrorPanel1, ErrorUpdatePanel);
            }
            else
            {
                KimenoCsatolmanyokPanel.Visible = false;
            }
        }
        else
        {
            KimenoCsatolmanyokPanel.Visible = false;
        }
    }

    void FillDokumentumokIktatoszam(DataTable dokumentumTable)
    {
        List<string> dokumentumIdList = new List<string>();
        foreach (DataRow row in dokumentumTable.Rows)
        {
            string dokumentumId = row["krtDok_Id"].ToString();
            dokumentumIdList.Add(dokumentumId);
        }

        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        search.Dokumentum_Id.Value = Search.GetSqlInnerString(dokumentumIdList.ToArray());
        search.Dokumentum_Id.Operator = Query.Operators.inner;
        search.IraIrat_Id.Operator = Query.Operators.notnull;

        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        Result result = service.GetAllWithExtension(ExecParam, search);

        if (!result.IsError)
        {
            Dictionary<string, string> iktatoszamok = new Dictionary<string, string>();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string dokumentumId = row["krtDok_Id"].ToString();
                string iktatoszam = row["IktatoSzam_Merge"].ToString();

                if (!iktatoszamok.ContainsKey(dokumentumId))
                {
                    iktatoszamok.Add(dokumentumId, iktatoszam);
                }
            }

            foreach (DataRow row in dokumentumTable.Rows)
            {
                string dokumentumId = row["krtDok_Id"].ToString();

                if (iktatoszamok.ContainsKey(dokumentumId))
                {
                    row["IktatoSzam_Merge"] = iktatoszamok[dokumentumId];
                }
            }
        }
    }

    #endregion

    protected string GetPFDLink(string dokumentumId, string sablonAzonosito)
    {
        if (IsPDFSablon(sablonAzonosito))
        {
            string baseUrl = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceUrl");
            string queryString = "DokumentumId=" + dokumentumId;
            if (sablonAzonosito == KodTarak.ELhiszUrlapSablon.Mediaszabalyozas)
            {
                queryString += "&Method=POST";
            }
            string url = baseUrl + "CreatePDF.aspx" + "?" + queryString;
            return url;
        }

        return String.Empty;
    }

    protected bool IsPDFSablon(string sablonAzonosito)
    {
        if (!String.IsNullOrEmpty(sablonAzonosito))
        {
            if (sablonAzonosito == KodTarak.ELhiszUrlapSablon.Adatkapu
                || sablonAzonosito == KodTarak.ELhiszUrlapSablon.Mediaszabalyozas
                || sablonAzonosito == KodTarak.ELhiszUrlapSablon.ElektronikusBeadvany)
                return true;
        }

        return false;
    }

    #region OCR
    private void OCRSelectedDokumentumRecords()
    {
        List<string> ocrFiles = new List<string>();
        List<string> nonOcrFiles = new List<string>();
        List<string> notSupportedOcrFiles = new List<string>();

        List<string> dokumentumIdList = UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "Label_krtDok_Id");

        KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_DokumentumokSearch src = new KRT_DokumentumokSearch();
        src.Id.Value = Search.GetSqlInnerString(dokumentumIdList.ToArray());
        src.Id.Operator = Query.Operators.inner;

        Result result_Search = dokService.GetAll(dokExecParam, src);
        if (result_Search.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Search);
            ErrorUpdatePanel.Update();
            return;
        }

        if (result_Search.Ds != null && result_Search.Ds.Tables[0].Rows.Count > 0)
        {
            List<string> OCREzhetoFileTipusok = GetKodtarFuggosegOCRFileTipusai();
            if (OCREzhetoFileTipusok == null || OCREzhetoFileTipusok.Count < 1)
                return;

            foreach (DataRow row in result_Search.Ds.Tables[0].Rows)
            {
                if (!string.IsNullOrEmpty(row["OCRAllapot"].ToString()))
                {
                    nonOcrFiles.Add(
                        string.Format("{0} - {1}",
                            row["FajlNev"].ToString(),
                            GetOcrAllapotNev(row["OCRAllapot"].ToString()))
                        );
                    continue;
                }

                if (OCREzhetoFileTipusok.Contains(row["Formatum"].ToString().ToLower()))
                {
                    SetDokumentumOCRStatus(row["Id"].ToString());
                    ocrFiles.Add(row["FajlNev"].ToString());
                }
                else
                {
                    notSupportedOcrFiles.Add(
                        string.Format("{0}{1}",
                        row["FajlNev"].ToString(),
                        Environment.NewLine
                        )
                        );
                    continue;
                }
            }
        }
        #region ERROR MSG
        StringBuilder sumMessage = new StringBuilder();
        if (notSupportedOcrFiles != null && notSupportedOcrFiles.Count > 0)
        {
            string msgNotSupp = string.Format(Resources.Error.ErrorDokumentumOCRFeldolgozasNotSupported, String.Join(", ", notSupportedOcrFiles.ToArray()));
            sumMessage.Append(msgNotSupp);
            sumMessage.Append(Environment.NewLine);
        }
        else if (nonOcrFiles != null && nonOcrFiles.Count > 0)
        {
            string msgNok = string.Format(Resources.Error.ErrorDokumentumOCRFeldolgozasNok, String.Join(", ", nonOcrFiles.ToArray()));
            sumMessage.Append(msgNok);
            sumMessage.Append(Environment.NewLine);
        }
        if (ocrFiles != null && ocrFiles.Count > 0)
        {
            string msg = string.Format(Resources.Error.ErrorDokumentumOCRFeldolgozasOk, String.Join(", ", ocrFiles.ToArray()));
            sumMessage.Append(msg);
            sumMessage.Append(Environment.NewLine);
        }

        if (!string.IsNullOrEmpty(sumMessage.ToString()))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageInfo, sumMessage.ToString());
            ErrorUpdatePanel.Update();
        }
        #endregion
    }
    /// <summary>
    /// GetKodtarFuggosegOCRFileTipusai
    /// </summary>
    /// <returns></returns>
    private List<string> GetKodtarFuggosegOCRFileTipusai()
    {
        Result result = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarById(UI.SetExecParamDefault(Page, new ExecParam()), Constants.FuggoKodtarOCRFileTipus);
        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
            return null;
        }
        KRT_KodtarFuggoseg kodtar = (KRT_KodtarFuggoseg)result.Record;

        Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(kodtar.Adat);
        if (fuggosegek == null)
            return null;

        if (fuggosegek.Items != null && fuggosegek.Items.Count > 0)
        {
            List<string> fileTipusKodtarIdk = new List<string>();
            KRT_KodTarak elem = null;
            foreach (var item in fuggosegek.Items)
            {
                elem = GetKodtarElem(UI.SetExecParamDefault(Page, new ExecParam()), fuggosegek.VezerloKodCsoportId, item.VezerloKodTarId);
                if (elem != null)
                    fileTipusKodtarIdk.Add(elem.Kod);
            }
            return fileTipusKodtarIdk;
        }
        return null;
    }
    /// <summary>
    /// GetKodtarElem
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kodcsoportKod"></param>
    /// <param name="kodtarKod"></param>
    /// <returns></returns>
    private KRT_KodTarak GetKodtarElem(ExecParam execParam, string kodcsoportKod, string kodtarId)
    {
        Contentum.eAdmin.Service.KRT_KodTarakService ktService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam ktExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        ktExecParam.Record_Id = kodtarId;
        Result resultFind = ktService.Get(ktExecParam);
        if (resultFind.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultFind);
            ErrorUpdatePanel.Update();
            return null;
        }
        return (KRT_KodTarak)resultFind.Record;

        //List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, execParam, HttpContext.Current.Cache, null);
        //foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
        //{
        //    if (elem.Kod == kodtarKod)
        //        return elem;
        //}

        //return null;
    }
    /// <summary>
    /// SetDokumentumOCRStatus
    /// </summary>
    /// <param name="stringGuid"></param>
    private void SetDokumentumOCRStatus(string stringGuid)
    {
        if (string.IsNullOrEmpty(stringGuid))
            return;

        KRT_DokumentumokService dokService = Contentum.eRecord.BaseUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        dokExecParam.Record_Id = stringGuid;
        Result resultFind = dokService.Get(dokExecParam);
        if (resultFind.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultFind);
            ErrorUpdatePanel.Update();
            return;
        }

        KRT_Dokumentumok dok = (KRT_Dokumentumok)resultFind.Record;
        ExecParam dokUpdateExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        dokUpdateExecParam.Record_Id = dok.Id;
        dok.OCRAllapot = KodTarak.OCR_Allapot.OCR_Ezendo_Dokumentum;
        dok.Updated.OCRAllapot = true;

        Result resultUpdate = dokService.Update(dokUpdateExecParam, dok);
        if (resultUpdate.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultUpdate);
            ErrorUpdatePanel.Update();
            return;
        }
    }

    /// <summary>
    /// SetOCRGridColumns
    /// </summary>
    private void SetOCRGridColumns()
    {
        string OCR_ENABLED =
        Contentum.eUtility.Rendszerparameterek.Get(UI.SetExecParamDefault(this.Page.Session, new ExecParam()),
        Contentum.eUtility.Rendszerparameterek.SCAN_OCR_ENABLED);

        bool visibleOcrAllapot = false;
        if (OCR_ENABLED != null && string.Equals(OCR_ENABLED, "1"))
        {
            visibleOcrAllapot = true;
        }
        for (int i = 0; i < CsatolmanyokGridView.Columns.Count; i++)
        {
            if (CsatolmanyokGridView.Columns[i].SortExpression == "OCRAllapot_Nev")
            {
                CsatolmanyokGridView.Columns[i].Visible = visibleOcrAllapot;
                return;
            }
        }
    }
    /// <summary>
    /// GetOcrAllapotNev
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private string GetOcrAllapotNev(string data)
    {
        if (string.IsNullOrEmpty(data))
            return string.Empty;
        switch (data)
        {
            case KodTarak.OCR_Allapot.OCR_Ezendo_Dokumentum:
                return GetKodtarNev(KodTarak.OCR_Allapot.KCS_OCR_Allapot, KodTarak.OCR_Allapot.OCR_Ezendo_Dokumentum);
            case KodTarak.OCR_Allapot.OCR_Ezes_Alatt_Levo_Dokumentum:
                return GetKodtarNev(KodTarak.OCR_Allapot.KCS_OCR_Allapot, KodTarak.OCR_Allapot.OCR_Ezes_Alatt_Levo_Dokumentum);
            case KodTarak.OCR_Allapot.OCR_Ezett_Dokumentum:
                return GetKodtarNev(KodTarak.OCR_Allapot.KCS_OCR_Allapot, KodTarak.OCR_Allapot.OCR_Ezett_Dokumentum);
            default:
                break;
        }
        return string.Empty;
    }
    /// <summary>
    /// GetKodtarNev
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="kod"></param>
    /// <returns></returns>
    private string GetKodtarNev(string kodcsoport, string kod)
    {
        if (!string.IsNullOrEmpty(kod))
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, null);
            if (kodtarakList != null)
            {
                foreach (Contentum.eUtility.KodTar_Cache.KodTarElem item in kodtarakList)
                {
                    if (item.Kod == kod)
                        return item.Nev;
                }
            }
        }
        return kod;
    }
    #endregion OCR

    //LZS - BLG_4391 - Külön mentés gomb a "Fődokumentum" törlése utáni kiválasztás mentéséhez.
    //A listából kiválasztott csatolmány "Fődokumentum" szerepkörre lesz állítva, így kerül frissítésre.
    //Az id-ja a listbox item (selected)value-ban van eltárolva.
    protected void btnFodokumentumSelect_Click1(object sender, ImageClickEventArgs e)
    {
        if (string.IsNullOrEmpty(CsatolmanyokListbox.SelectedValue))
        {
            lblErrorMessage.Visible = true;
            return;
        }

        EREC_CsatolmanyokService EREC_CsatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = CsatolmanyokListbox.SelectedValue;

        EREC_Csatolmanyok erec_Csatolmanyok = (EREC_Csatolmanyok)EREC_CsatolmanyokService.Get(execParam).Record;
        erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
        erec_Csatolmanyok.Updated.DokumentumSzerep = true;

        Result res = EREC_CsatolmanyokService.Update(execParam, erec_Csatolmanyok);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            ErrorUpdatePanel.Update();
            return;
        }
        // ha új fődokumentum került kiválasztásra akkro az aláírási sort újra kell kezdeni!
        ResetAlairas();
        ReLoadTab();

    }
}
