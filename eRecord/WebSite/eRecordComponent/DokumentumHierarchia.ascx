﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DokumentumHierarchia.ascx.cs"
    Inherits="eRecordComponent_DokumentumHierarchia" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="DokumentumTextBox.ascx" TagName="DokumentumTextBox" TagPrefix="uc3" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImageButtonNew" runat="server" ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                                        onmouseover="swapByName(this.id,'felvitel_trap2.jpg')" onmouseout="swapByName(this.id,'felvitel_trap.jpg')"
                                        CommandName="New" Visible="True" CausesValidation="false" 
                                        onclick="ImageButtonNew_Click" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonView" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                        onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')"
                                        CommandName="View" Visible="true" CausesValidation="false" 
                                        onclick="ImageButtonView_Click" />
                                </td>
                                <td style="width: 80%">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">   
                                     <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />                                 
                                    <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="false" BackColor="White"
                                        ShowLines="True" PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip=""
                                        SkipLinkText="" SelectedNodeStyle-CssClass="TreeViewSelectedNodeStyle">                                        
                                    </asp:TreeView>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <eUI:eFormPanel ID="EFormPanel1" runat="server" Visible="false">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <h5 id="header" class="emp_HeaderWrapper">
                                            <asp:Label ID="labelHeader" runat="server" Text="Dokumentum hozzákapcsolása" CssClass="subPanel_Header"></asp:Label>
                                        </h5>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;">
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>&nbsp;
                                        <asp:Label ID="Label1" runat="server" Text="Hozzákapcsolandó dokumentum:"></asp:Label>
                                        &nbsp;
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <uc3:DokumentumTextBox ID="DokumentumTextBox1" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;">
                                        <asp:Label ID="Label2" runat="server" Text="Kapcsolat jelleg:"></asp:Label>
                                        &nbsp;
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:TextBox ID="KapcsolatJelleg_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <uc2:TabFooter ID="TabFooter1" runat="server" />
                        </eUI:eFormPanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
