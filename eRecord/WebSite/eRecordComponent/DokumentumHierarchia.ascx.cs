﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eUIControls;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_DokumentumHierarchia : System.Web.UI.UserControl
{
    private eErrorPanel errorPanel = null;
    private UpdatePanel errorUpdatePanel = null;

    private string _Startup = String.Empty;
    public string Startup
    {
        get {
            return _Startup;
        }
        set {
            _Startup = value;
        }
    }

    private string funkcioKod_DokumentumView = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        ImageButtonNew.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ImageButtonView.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {       

        // ImageButtonNew-ból és View-ból kivenni a js-es tiltást, ha van kijelölve elem a TreeView-ban:
        if (!String.IsNullOrEmpty(TreeView1.SelectedValue))
        {
            ImageButtonNew.OnClientClick = String.Empty;
            ImageButtonView.OnClientClick = String.Empty;
        }

        // Gombok engedélyezése funkciójog alapján:
        ImageButtonNew.Enabled = FunctionRights.GetFunkcioJog(Page, "DokumentumKapcsolatFelvetel");
        if (String.IsNullOrEmpty(funkcioKod_DokumentumView))
        {
            ImageButtonView.Enabled = false;
        }
        else
        {
            ImageButtonView.Enabled = FunctionRights.GetFunkcioJog(Page, funkcioKod_DokumentumView);
        }

        #region DokumentumTextBox1-nek szűrés beállítása

        if (EFormPanel1.Visible)
        {

            if (this.Startup != Constants.Startup.Standalone)
            {
                if (ViewState[vsKey_DokumentumFilter_IratId] != null)
                {
                    DokumentumTextBox1.Filter_IratId = ViewState[vsKey_DokumentumFilter_IratId].ToString();
                }
                else if (ViewState[vsKey_DokumentumFilter_KuldemenyId] != null)
                {
                    DokumentumTextBox1.Filter_KuldemenyId = ViewState[vsKey_DokumentumFilter_KuldemenyId].ToString();
                }
                else
                {
                    // nincs beállítva iratId és kuldemenyId, letiltjuk a dokumentum kiválasztást:
                    DokumentumTextBox1.Enabled = false;
                }
            }
            else
            {
                // Különálló dokumentumok
                DokumentumTextBox1.Startup = this.Startup;
            }
        }

        #endregion
    }


    public void InitComponent(eErrorPanel errorPanel, UpdatePanel errorUpdatePanel, bool newEnabled, string funkcioKod_DokumentumView)
    {
        this.errorPanel = errorPanel;
        this.errorUpdatePanel = errorUpdatePanel;
        this.funkcioKod_DokumentumView = funkcioKod_DokumentumView;

        if (newEnabled == false)
        {
            ImageButtonNew.Enabled = false;
            ImageButtonNew.Visible = false;            
        }
    }

    public void LoadComponentByCsatolmanyId(string csatolmanyId)
    {
        // BUG_4080
        if (String.IsNullOrEmpty(csatolmanyId))
        {
            Clear();
            return;
        }
        // Dokumentum Id -hoz csatolmány lekérése:
        EREC_CsatolmanyokService service_csatolmanyok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = csatolmanyId;

        Result result = service_csatolmanyok.Get(execParam);
        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
        }

        EREC_Csatolmanyok csatolmany = (EREC_Csatolmanyok)result.Record;

        // BUG_10252
        LoadComponent(csatolmany.Dokumentum_Id, '0');
    }

    public void LoadComponent(string dokumentumId, char standalone)
    {
        if (String.IsNullOrEmpty(dokumentumId))
        {
            return;
        }
         
        DokumentumVizualizerComponent.ClearDokumentElements();
        
        KRT_DokumentumokService service = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

        // BUG_10252
        Result result = service.DokumentumHierarchiaGetAll(UI.SetExecParamDefault(Page), dokumentumId, standalone);
        if (result.IsError)
        {
            // hiba:
            if (errorPanel != null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel,result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
            TreeViewUpdatePanel.Visible = false;
            return;
        }

        // treeView kiürítése, form elrejtése:    
        Clear();
        
        // treeView felépítése:
        BuildDokumentumTreeView(null, result.Ds.Tables[0].Rows, new List<string>(), dokumentumId);
    
        if (this.Startup != Constants.Startup.Standalone)
        {
            #region Csatolmány lekérése Irat/Küldemény meghatározásához

            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            search_csatolmanyok.Dokumentum_Id.Value = dokumentumId;
            search_csatolmanyok.Dokumentum_Id.Operator = Query.Operators.equals;

            Result result_csatolmanyok = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);
            if (result_csatolmanyok.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_csatolmanyok);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }

            string kuldemenyId = String.Empty;
            string iratId = String.Empty;

            if (result_csatolmanyok.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result_csatolmanyok.Ds.Tables[0].Rows[0];
                kuldemenyId = row["KuldKuldemeny_Id"].ToString();
                iratId = row["IraIrat_Id"].ToString();
            }

            // ViewState-be lementjük az iratId-t vagy a kuldemenyId-t:
            if (!String.IsNullOrEmpty(iratId))
            {
                ViewState[vsKey_DokumentumFilter_IratId] = iratId;
            }
            else if (!String.IsNullOrEmpty(kuldemenyId))
            {
                ViewState[vsKey_DokumentumFilter_KuldemenyId] = kuldemenyId;
            }

            #endregion
        }
    }

    private const string vsKey_DokumentumFilter_IratId = "DokumentumFilter_IratId";
    private const string vsKey_DokumentumFilter_KuldemenyId = "DokumentumFilter_KuldemenyId";

    public void Clear()
    {
        TreeView1.Nodes.Clear();
        // BUG_10252
        if (!clickedNewButton)
        {
            EFormPanel1.Visible = false;
        }
        ClearForm();
    }

    public void ClearForm()
    {
        DokumentumTextBox1.Id_HiddenField = String.Empty;
        DokumentumTextBox1.Text = String.Empty;

        KapcsolatJelleg_TextBox.Text = String.Empty;
    }

        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="parentTreeNode"></param>
    /// <param name="dataRows"></param>
    /// <param name="processedIdList">Végtelen ciklus elkerülésére eltároljuk a már feldolgozott Id-kat</param>
    private void BuildDokumentumTreeView(TreeNode parentTreeNode, DataRowCollection dataRows, List<string> processedIdList, string sourceDokumentumId)
    {
        string parentId = String.Empty;
        if (parentTreeNode != null)
        {
            parentId = parentTreeNode.Value;
        }

        /// parentTreeNode -hoz hozzávesszük a gyerekeit:
        /// (ha parentTreeNode == null, akkor a gyökérelem(ek)et vesszük fel a treeView-ba)
        /// 
        foreach (DataRow row in dataRows)
        {
            string row_Id = row["Id"].ToString();
            string row_SzuloId = row["SzuloId"].ToString();
            string row_FajlNev = row["FajlNev"].ToString();
            string row_KapcsolatJelleg = row["KapcsolatJelleg"].ToString();
            string row_Formatum = row["Formatum"].ToString();
            string row_Version = row["VerzioJel"].ToString();
            string row_ExternalLink = row["External_Link"].ToString();

            if (parentTreeNode == null && String.IsNullOrEmpty(row_SzuloId)
                && processedIdList.Contains(row_Id) == false)
            {
                // Gyökérelem, TreeView-hoz felvesszük:
                TreeNode rootNode = new TreeNode(GetNodeText(row_FajlNev, row_KapcsolatJelleg, row_Formatum, row_Id, row_Version, row_ExternalLink), row_Id);
                TreeView1.Nodes.Add(rootNode);

                // Felvesszük a feldolgozott Id listába:
                processedIdList.Add(row_Id);

                // rekurzív hívás erre a node-ra, gyerek node-ok hozzávétele
                BuildDokumentumTreeView(rootNode, dataRows, processedIdList, sourceDokumentumId);
                // Csomópont kibontottra állítása:
                rootNode.Expanded = true;

                // ha ez az eredeti dokumentum, kijelöljük:
                if (row_Id == sourceDokumentumId)
                {
                    rootNode.Selected = true;
                }
            }
            else if (row_SzuloId == parentId && processedIdList.Contains(row_Id) == false)
            {
                // Node létrehozása, hozzáadás a szülőhöz:
                TreeNode newNode = new TreeNode(GetNodeText(row_FajlNev, row_KapcsolatJelleg, row_Formatum, row_Id, row_Version, row_ExternalLink), row_Id);
                parentTreeNode.ChildNodes.Add(newNode);

                // Felvesszük a feldolgozott Id listába:
                processedIdList.Add(row_Id);

                // rekurzív hívás erre a node-ra, gyerek node-ok hozzávétele
                BuildDokumentumTreeView(newNode, dataRows, processedIdList, sourceDokumentumId);
                // Csomópont kibontottra állítása:
                newNode.Expanded = true;

                // ha ez az eredeti dokumentum, kijelöljük:
                if (row_Id == sourceDokumentumId)
                {
                    newNode.Selected = true;
                }
            }
        }
    }

    private string GetNodeText(string fajlNev, string kapcsolatJelleg, string formatum, string id, string version, string externalLink)
    {
        string imageId = String.Format("fileIcon_{0}", id); ;

        string fileIcon_Html = "<img id=\"" + imageId + "\" src='images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum) + "' />";

        DokumentumVizualizerComponent.AddDokumentElement(imageId, id, fajlNev, version, externalLink);

        if (string.IsNullOrEmpty(kapcsolatJelleg))
        {
            return fileIcon_Html + fajlNev;
        }
        else
        {
            return fileIcon_Html + fajlNev + " <i>(" + kapcsolatJelleg + ")</i>";
        }
    }

    // BUG_10252
    protected bool clickedNewButton = false; 
    protected void ImageButtonNew_Click(object sender, ImageClickEventArgs e)
    {
        // Form megjelenítése:
        EFormPanel1.Visible = true;
        // BUG_10252
        clickedNewButton = true;
        // Form komponenseinek kiürítése:
        ClearForm();

        // TabFooter gombjainak megjelenítése:
        TabFooter1.CommandArgument = CommandName.New;
    }



    protected void ImageButtonView_Click(object sender, ImageClickEventArgs e)
    {
        // A kiválasztott dokumentum megjelenítése, ha van hozzá joga

        string selectedDokumentumId = TreeView1.SelectedValue;

        if (String.IsNullOrEmpty(selectedDokumentumId))
        {
            return;
        }

        // Jogosultság-ellenőrzés:
        KRT_DokumentumokService service = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = selectedDokumentumId;

        char jogszint = 'O';

        Result result = service.GetWithRightCheck(execParam, jogszint);

        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
            return;
        }

        KRT_Dokumentumok krt_dokumentum = (KRT_Dokumentumok) result.Record;
        
        // dokumentum megnyitó script leküldése:
        //string js = "window.open('" + krt_dokumentum.External_Link + "');";
		string js = "window.open('GetDocumentContent.aspx?id=" + krt_dokumentum.Id + "');";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openDocumentScript", js, true);
    }



    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "DokumentumKapcsolatFelvetel"))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            string foDokumentumId = TreeView1.SelectedValue;
                            string alDokumentumId = DokumentumTextBox1.Id_HiddenField;

                            if (String.IsNullOrEmpty(foDokumentumId)
                                || String.IsNullOrEmpty(alDokumentumId))
                            {
                                // hiba:
                                ResultError.DisplayErrorOnErrorPanel(errorPanel, Resources.Error.ErrorLabel, "Nincsenek kiválasztva az összekapcsolandó dokumentumok!");
                                if (errorUpdatePanel != null)
                                {
                                    errorUpdatePanel.Update();
                                }
                                return;
                            }

                            // Ellenőrzés, benne van-e az új DokumentumId a fában
                            if (ExistsIdInTreeView(alDokumentumId))
                            {
                                // hiba:
                                ResultError.DisplayWarningOnErrorPanel(errorPanel, Resources.Error.ErrorLabel, "A dokumentum már szerepel a hierarchiában!");
                                if (errorUpdatePanel != null)
                                {
                                    errorUpdatePanel.Update();
                                }
                                return;
                            }

                            KRT_DokumentumKapcsolatokService service_dokuKapcs = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumKapcsolatokService();

                            KRT_DokumentumKapcsolatok krt_DokumentumKapcsolatok = GetBusinessObjectFromComponents();

                            // Kapcsolat létrehozása:
                            Result result = service_dokuKapcs.Insert(UI.SetExecParamDefault(Page), krt_DokumentumKapcsolatok);
                           

                            if (!result.IsError)
                            {
                                // BUG_10252
                                char standalone = (this.Startup == Constants.Startup.Standalone)?'1':'0';
                                LoadComponent(krt_DokumentumKapcsolatok.Dokumentum_Id_Fo, standalone);                                
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                                if (errorUpdatePanel != null)
                                {
                                    errorUpdatePanel.Update();
                                }
                            }
                            // BUG_10252
                            EFormPanel1.Visible = false;
                            break;
                        }
                }
            }
            else
            {               
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private bool ExistsIdInTreeView(string dokId)
    {
        foreach (TreeNode treeNode in TreeView1.Nodes)
        {
            if (treeNode.Value == dokId)
            {
                return true;
            }
            else if (ExistIdInTreeNodeChildNodes(treeNode, dokId))
            {
                return true;
            }
        }

        return false;
    }

    private bool ExistIdInTreeNodeChildNodes(TreeNode treeNode, string dokId)
    {
        if (treeNode == null) { return false; }

        foreach (TreeNode childNode in treeNode.ChildNodes)
        {            
            if (childNode.Value == dokId)
            {
                return true;
            }
            else if (ExistIdInTreeNodeChildNodes(childNode, dokId))
            {
                return true;
            }
        }

        return false;
    }

    private KRT_DokumentumKapcsolatok GetBusinessObjectFromComponents()
    {
        KRT_DokumentumKapcsolatok dokuKapcs = new KRT_DokumentumKapcsolatok();

        // Fődokumentum a kiválasztott elem a TreeView-ban:
        dokuKapcs.Dokumentum_Id_Fo = TreeView1.SelectedValue;
        dokuKapcs.Updated.Dokumentum_Id_Fo = true;

        // Aldokumentum:
        dokuKapcs.Dokumentum_Id_Al = DokumentumTextBox1.Id_HiddenField;
        dokuKapcs.Updated.Dokumentum_Id_Al = true;

        // Típus:
        dokuKapcs.Tipus = KodTarak.DOKUKAPCSOLAT_TIPUS.Dokumentum_hierarchia;
        dokuKapcs.Updated.Tipus = true;

        dokuKapcs.DokumentumKapcsolatJelleg = KapcsolatJelleg_TextBox.Text;
        dokuKapcs.Updated.DokumentumKapcsolatJelleg = true;

        return dokuKapcs;
    }

}
