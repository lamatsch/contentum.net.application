﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DokumentumTextBox.ascx.cs" Inherits="eRecordComponent_DokumentumTextBox" %>

<div class="DisableWrap">
<asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
 CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />
  <asp:HiddenField ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="TextBox1" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>