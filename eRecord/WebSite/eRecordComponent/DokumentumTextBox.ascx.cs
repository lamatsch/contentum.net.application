﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class eRecordComponent_DokumentumTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent
{

    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
                //UI.SwapImageToDisabled(NewImageButton);
            }
        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set { TextBox1.ReadOnly = value; }
        get { return TextBox1.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    //public string OnClick_View
    //{
    //    set { ViewImageButton.OnClientClick = value; }
    //    get { return ViewImageButton.OnClientClick; }
    //}

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}

    public string Text
    {
        set { TextBox1.Text = value; }
        get { return TextBox1.Text; }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }

    private string filter_IratId = String.Empty;
    public string Filter_IratId
    {
        get { return filter_IratId; }
        set { filter_IratId = value; }
    }

    private string filter_KuldemenyId = String.Empty;
    public string Filter_KuldemenyId
    {
        get { return filter_KuldemenyId; }
        set { filter_KuldemenyId = value; }
    }

    private string _Startup = String.Empty;
    public string Startup
    {
        get { return _Startup; }
        set { _Startup = value; }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
        //        "KuldKuldemenyekForm.aspx", "", HiddenField1.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string queryString = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID;

        if (this.Startup != Constants.Startup.Standalone)
        {
            if (!String.IsNullOrEmpty(Filter_IratId))
            {
                queryString += "&" + QueryStringVars.IratId + "=" + Filter_IratId;
            }
            else if (!String.IsNullOrEmpty(Filter_KuldemenyId))
            {
                queryString += "&" + QueryStringVars.KuldemenyId + "=" + Filter_KuldemenyId;
            }
        }
        else
        {
            queryString = String.Format("{0}&{1}={2}", queryString, QueryStringVars.Startup, Constants.Startup.Standalone);
        }
        OnClick_Lov = JavaScripts.SetOnClientClick("DokumentumokLovList.aspx",
           queryString, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
        componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        //componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        //ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

}
