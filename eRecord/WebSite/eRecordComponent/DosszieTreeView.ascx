﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DosszieTreeView.ascx.cs" Inherits="eRecordComponent_DosszieTreeView" %>

<asp:UpdatePanel Id="UpdatePanel1" runat="server" OnLoad="TreeViewUpdatePanel_Load">
    <ContentTemplate>
        <asp:HiddenField runat="server" ID="DossziekTreeView_SelectedIdHiddenField" value=""/>
        <asp:HiddenField runat="server" ID="DossziekTreeView_FilterAttachedListHiddenField" value="0"/>
        <table cellpadding="0" cellspacing="0" style="border-collapse: separate; border-style:dashed; border-color:Blue; border-width:1px; border-spacing: 10px 5px; background-color:White; table-layout:auto; height:100%; width:210px;">
            <tr style="background-color:#D2D2D2;text-align:center;height:24px;">
                <td>
                    <asp:Label ID="Header" runat="server" Text="Dossziék" CssClass="ListHeaderText" />&nbsp;
                    <asp:Label ID="IsFilteredLabel" runat="server" CssClass="ListHeaderText" />
                </td>
            </tr>
            <tr>
                <td width="100%">

                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr style="background-image:url(images/hu/fejlec/dosszie_ikonok_hatter.jpg); background-repeat: repeat-x" align="left" >
                            <td class="ikonSorElem" id="ButtonTableSearch">
                                <asp:ImageButton ID="ImageSearch" runat="server" ImageUrl="../images/hu/ikon/dosszie_kereses_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_kereses.jpg')" onmouseout="swapByName(this.id,'dosszie_kereses_keret_nelkul.jpg')"
                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Search" AlternateText="<%$Resources:Buttons,Search %>"
                                    ToolTip="<%$Resources:Buttons,Search %>" />
                            </td>
                            <td class="ikonSorElem" id="ButtonTableButtonNew">
                                <asp:ImageButton ID="ImageNew" runat="server" ImageUrl="../images/hu/ikon/dosszie_uj_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_uj.jpg')" onmouseout="swapByName(this.id,'dosszie_uj_keret_nelkul.jpg')"
                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="New" AlternateText="<%$Resources:Buttons,New %>"
                                    ToolTip="<%$Resources:Buttons,New %>" />
                            </td>
                            <td class="ikonSorElem" id="ButtonTableButtonView">
                                <asp:ImageButton ID="ImageView" runat="server" ImageUrl="../images/hu/ikon/dosszie_megtekintes_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_megtekintes.jpg')" onmouseout="swapByName(this.id,'dosszie_megtekintes_keret_nelkul.jpg')"
                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="View" AlternateText="<%$Resources:Buttons,View %>"
                                    ToolTip="<%$Resources:Buttons,View %>" />
                            </td>
                            <td class="ikonSorElem" id="ButtonTableButtonModify">
                                <asp:ImageButton ID="ImageModify" runat="server" ImageUrl="../images/hu/ikon/dosszie_modositas_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_modositas.jpg')" onmouseout="swapByName(this.id,'dosszie_modositas_keret_nelkul.jpg')"
                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Modify" AlternateText="Módosítás"
                                    ToolTip="Módosítás" />
                            </td>
                            <td class="ikonSorElem" id="ButtonTableButtonInvalidate">
                                <asp:ImageButton ID="ImageInvalidate" runat="server" ImageUrl="../images/hu/ikon/dosszie_ervenytelenites_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_ervenytelenites.jpg')" onmouseout="swapByName(this.id,'dosszie_ervenytelenites_keret_nelkul.jpg')"
                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Invalidate" AlternateText="Érvénytelenítés"
                                    ToolTip="Érvénytelenítés" />
                            </td>
                            <td width="100%" style="background-image:url(images/hu/fejlec/dosszie_ikonok_hatter.jpg); background-repeat: repeat-x"></td>
                            <td class="ikonSorElem" id="ButtonTableButtonAtadas" align="right">
                                <asp:ImageButton ID="ImageAtadas" runat="server" ImageUrl="../images/hu/ikon/dosszie_atadas_keret_nelkul.jpg"
                                    onmouseover="swapByName(this.id,'dosszie_atadas.jpg')" onmouseout="swapByName(this.id,'dosszie_atadas_keret_nelkul.jpg')"
                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Atadas" AlternateText="Átadás"
                                    ToolTip="Átadás"/>
                            </td>  
                                 
                            <td class="ikonSorElem" id="ButtonTableButtonFilter" align="right">
                                <asp:ImageButton ID="ImageFilter" runat="server" ImageUrl="../images/hu/egyeb/filter_on.gif"
                                    onmouseover="swapByName(this.id,'filter_on2.gif')" onmouseout="swapByName(this.id,'filter_on.gif')"
                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Filter" AlternateText="Szűrés"
                                    ToolTip="Szűrés"/>
                            </td>
                            <td class="ikonSorElem" id="ButtonTableButtonDeFilter" align="right">
                                <asp:ImageButton ID="ImageDeFilter" runat="server" ImageUrl="../images/hu/egyeb/filter_on_active.gif"
                                    onmouseover="swapByName(this.id,'filter_on_active2.gif')" onmouseout="swapByName(this.id,'filter_on_active.gif')"
                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Filter" AlternateText="Szűrés megszüntetése"
                                    ToolTip="Szűrés megszüntetése"/>
                            </td>                                                    
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" width="210px">
                    <asp:TreeView ID="DossziekTreeView" runat="server" Visible="true" PopulateNodesFromClient="false"
                        Target="_self">
                        <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                    </asp:TreeView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>