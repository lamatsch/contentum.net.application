﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eUIControls;
using System.Collections.Generic;

public partial class eRecordComponent_DosszieTreeView : System.Web.UI.UserControl
{
    private const string imageUrlVirtualis = "../images/hu/egyeb/virtualis_dosszie.gif";
    private const string imageUrlFizikai = "../images/hu/egyeb/fizikai_dosszie.gif";

    public TreeView TreeView
    {
        get { return this.DossziekTreeView; }
    }

    public string SelectedId
    {
        get 
        { 
            return this.DossziekTreeView_SelectedIdHiddenField.Value; 
        }
        set 
        {
            if (this.SelectedId != value)
            {
                this.DossziekTreeView_SelectedIdHiddenField.Value = value;
                OnSelectedIdChanged();
            }
        }
    }

    private void OnSelectedIdChanged()
    {
        if (this.FilterAttachedList)
            RaiseFilterList();
    }

    public string SelectedId_HiddenField
    {
        get { return this.DossziekTreeView_SelectedIdHiddenField.ClientID; }
    }

    public bool FilterAttachedList
    {
        get { return this.DossziekTreeView_FilterAttachedListHiddenField.Value == "1"; }
        set { this.DossziekTreeView_FilterAttachedListHiddenField.Value = value?"1":"0"; }
    }

    public delegate void FilterListDelegate();

    public event FilterListDelegate FilterList;

    private void RaiseFilterList()
    {
        if (FilterList != null)
        {
            FilterList();
        }
    }

    eErrorPanel errorPanel = null;

    public eErrorPanel ErrorPanel
    {
        get
        {
            if (errorPanel == null)
            {
                int i = 0;
                foreach (eErrorPanel ep in UI.FindControls<eErrorPanel>(Page.Controls))
                {
                    i++;
                    errorPanel = ep;
                    if (i > 1)
                        break;
                }
            }
            return errorPanel; 
        }
        set { errorPanel = value; }
    }

    UpdatePanel errorUpdatePanel = null;

    public UpdatePanel ErrorUpdatePanel
    {
        get
        {
            if (errorUpdatePanel == null)
            {
                if (ErrorPanel != null)
                {
                    errorUpdatePanel = UI.FindParentControl<UpdatePanel>(ErrorPanel);
                }
            }
            return errorUpdatePanel; 
        }
        set { errorUpdatePanel = value; }
    }

    private bool IsFizikaiDosszie(TreeNode tn)
    {
        // workaround: a checkbox állapot alapján vezetjük vissza típust
        return tn != null && tn.Checked;
        //tn.ImageUrl == imageUrlFizikai;
    }

    private bool HasDosszieFizikaiSzulo(TreeNode tn)
    {
        bool hasPhysicalParent = false;
        if (tn != null)
        {
            tn = tn.Parent;
            while (tn != null && !hasPhysicalParent)
            {
                hasPhysicalParent = IsFizikaiDosszie(tn);
                tn = tn.Parent;
            }
        }
        return hasPhysicalParent;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ImageNew.Visible = Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "MappakNew");
        ImageView.Visible = Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "MappakView");
        ImageModify.Visible = Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "MappakModify");
        ImageInvalidate.Visible = Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "MappakInvalidate");

        ImageAtadas.Visible = Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "MappakAtadas");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Visible)
        {
            if (!IsPostBack)
            {
                this.LoadTreeView();

            }


            DossziekTreeView.SelectedNodeChanged += new EventHandler(DossziekTreeView_SelectedNodeChanged);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.Visible)
        {
            if (FilterAttachedList)
            {
                ImageFilter.Visible = false;
                ImageDeFilter.Visible = true;
            }
            else
            {
                ImageFilter.Visible = true;
                ImageDeFilter.Visible = false;
            }

            RefreshButtons();

            SetHeaderText();
        }
    }

    private void RefreshButtons()
    {
        ImageSearch.OnClientClick = JavaScripts.SetOnClientClick("DosszieSearch.aspx", ""
                , Defaults.PopupWidth, Defaults.PopupHeight, UpdatePanel1.ClientID, EventArgumentConst.refreshDossziekList);

        ImageNew.OnClientClick = JavaScripts.SetOnClientClick("DosszieForm.aspx", QueryStringVars.Command + "=" + CommandName.New
                    + "&SzuloMappa_Id=' + $get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + "').value + '"
                    , 1024, 768, UpdatePanel1.UniqueID, EventArgumentConst.refreshSelectedNodeChildren);

        ImageView.OnClientClick = @"
                if (!$get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + @"').value)
                {" + JavaScripts.SetOnClientClickNoSelectedRow() + @"}
                else
                {" + JavaScripts.SetOnClientClick("DosszieForm.aspx", QueryStringVars.Command + "=" + CommandName.View
            + "&Id=' + $get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + "').value+ '"
            , 1024, 768, UpdatePanel1.ClientID, EventArgumentConst.refreshMasterList) + @"}";

        ImageModify.OnClientClick = @"
                if (!$get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + @"').value)
                {" + JavaScripts.SetOnClientClickNoSelectedRow() + @"}
                else
                {" + JavaScripts.SetOnClientClick("DosszieForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify
            + "&Id=' + $get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + "').value + '"
            , 1024, 768, UpdatePanel1.ClientID, EventArgumentConst.refreshSelectedNodeChildren) + @"}";

        ImageInvalidate.OnClientClick = @"
                if (!$get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + @"').value)
                {" + JavaScripts.SetOnClientClickNoSelectedRow() + @"}
                else
                {if (confirm('"
                    + Resources.Question.UIisDeleteOne
                    + "')) { } else { return false; }}";

        ImageFilter.OnClientClick = @"
                if (!$get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + @"').value)
                {" + JavaScripts.SetOnClientClickNoSelectedRow() + @"}
                else
                {
                    $get('" + DossziekTreeView_FilterAttachedListHiddenField.ClientID + @"').value = '1';
                }";

        ImageDeFilter.OnClientClick = @"
                if (!$get('" + DossziekTreeView_SelectedIdHiddenField.ClientID + @"').value)
                {" + JavaScripts.SetOnClientClickNoSelectedRow() + @"}
                else
                {
                    $get('" + DossziekTreeView_FilterAttachedListHiddenField.ClientID + @"').value = '0';
                }";
    }

    private void SetHeaderText()
    {
        if (Search.IsSearchObjectInSession(Page, typeof(KRT_MappakSearch)))
        {
            IsFilteredLabel.Text = "(" + Resources.List.UI_IsFiltered + ")";
            if (!IsPostBack)
            {
                string filter = Request.QueryString.Get(QueryStringVars.Filter);
                if (filter == Constants.SajatMappa)
                {
                    IsFilteredLabel.Text += " - Saját";
                }
            }
        }
        else
        {
            IsFilteredLabel.Text = String.Empty;
        }
    }

    private void DossziekTreeView_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (DossziekTreeView.SelectedNode != null)
        {
            this.SelectedId = DossziekTreeView.SelectedValue;
            ImageAtadas.OnClientClick = String.Empty;

            if (!IsFizikaiDosszie(DossziekTreeView.SelectedNode))
            {
                // Csak fizikai dossziékat lehet átadni!
                ImageAtadas.OnClientClick = String.Format("alert('{0}');return false;", Resources.Error.ErrorCode_64046);
            }
            else
            {
                bool hasPhysicalParent = HasDosszieFizikaiSzulo(DossziekTreeView.SelectedNode);
                if (hasPhysicalParent)
                {
                    // Fizikai dossziéban lévő dosszié nem adható át, csak szülőjével együtt! Vegye ki a dossziét a tartalmazó fizikai dossziéból, vagy adja át azt a tartalmazó fizikai dossziéval együtt!
                    ImageAtadas.OnClientClick = String.Format("alert('{0}');return false;", Resources.Error.ErrorCode_64045);
                }
            }
        }
        else
        {
            this.SelectedId = String.Empty;
            ImageAtadas.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }
    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDossziekList:
                    this.LoadTreeView(DossziekTreeView.SelectedValue);
                    break;
                case EventArgumentConst.refreshSelectedNodeChildren:
                    this.LoadTreeView(DossziekTreeView.SelectedValue);
                    break;
            }
        }
    }

    private TreeNode GetDefaultTreeNode(string Text, string Id, string Tipus)
    {
        TreeNode tn = new TreeNode(Text, Id);
        switch (Tipus)
        {
            case KodTarak.MAPPA_TIPUS.Virtualis:
            default:
                tn.ImageUrl = imageUrlVirtualis; // "../images/hu/egyeb/virtualis_dosszie.gif";
                tn.Checked = false;
                break;
            case KodTarak.MAPPA_TIPUS.Fizikai:
                tn.ImageUrl = imageUrlFizikai; // "../images/hu/egyeb/fizikai_dosszie.gif";
                tn.Checked = true;
                break;
        }

        return tn;
    }

    public virtual void Left_FunkcionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case CommandName.New:
                break;
            case CommandName.View:
                break;
            case CommandName.Modify:
                break;
            case CommandName.Invalidate:
                this.Invalidate(DossziekTreeView_SelectedIdHiddenField.Value);
                this.LoadTreeView();
                break;
        }
    }

    public virtual void Right_FunkcionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case CommandName.Atadas:
                Session["SelectedDosszieIds"] = this.SelectedId;

                Session["SelectedBarcodeIds"] = null;
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedIratPeldanyIds"] = null;
                Session["SelectedUgyiratIds"] = null;

                string js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UpdatePanel1.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedDosszieAtadas", js, true);
                break;
            case CommandName.Filter:
                RaiseFilterList();
                break;
        }
    }

    private void Invalidate(string MappaId)
    {
        KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();
        
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = MappaId;

        Result result = service.InvalidateWithChildrenCheck(execParam);
    }

    private void LoadTreeView(string SelectedId)
    {
        KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();
        KRT_MappakSearch search = null;
        if (!IsPostBack)
        {
            string Startup = Page.Request.QueryString.Get(QueryStringVars.Startup);
            if (Startup == Constants.Startup.FromFeladataim) // ha feladataim panelról indítottuk
            {
                // új search objektumot hozunk létre, és sessionbe tesszük
                search = new KRT_MappakSearch();
                Search.SetSearchObject(Page, search);
            }
        }
        search = (KRT_MappakSearch)Search.GetSearchObject(Page, new KRT_MappakSearch());

        ExecParam execParam = new ExecParam();
        execParam = UI.SetExecParamDefault(Page, execParam);

        // Van-e szűrés kérés paraméterben? (Saját dossziékra)
        // (Csak első kéréskor figyeljük)
        if (!IsPostBack)
        {
            string filter = Request.QueryString.Get(QueryStringVars.Filter);
            if (filter == Constants.SajatMappa)
            {
                search.Csoport_Id_Tulaj.Value =
                    Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
                search.Csoport_Id_Tulaj.Operator = Contentum.eQuery.Query.Operators.equals;

                search.ReadableWhere = "Saját ";

                if (Search.IsSearchObjectInSession(Page, typeof(KRT_MappakSearch)) == false)
                {
                    Search.SetSearchObject(Page, search);
                }
            }
        }

        Result result = service.GetAllWithExtensionAndJogosultak(execParam, search);

        DossziekTreeView.Nodes.Clear();

        if (result.IsError)
        {
            if (ErrorPanel != null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, result);
            }
            return;
        }

        List<TreeNode> nodeList = new List<TreeNode>();

        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
        {
            if (string.IsNullOrEmpty(result.Ds.Tables[0].Rows[i]["SzuloMappa_Id"].ToString()))
            {
                TreeNode tn = this.GetDefaultTreeNode(result.Ds.Tables[0].Rows[i]["Nev"].ToString(), result.Ds.Tables[0].Rows[i]["Id"].ToString(), result.Ds.Tables[0].Rows[i]["Tipus"].ToString());
                DossziekTreeView.Nodes.Add(tn);

                if (tn.Value == SelectedId)
                    tn.Selected = true;

                nodeList.Add(tn);

                continue;
            }

            bool isRootNode = true;

            for (int j = 0; j < result.Ds.Tables[0].Rows.Count; j++)
            {
                if (result.Ds.Tables[0].Rows[i]["SzuloMappa_Id"].ToString() == result.Ds.Tables[0].Rows[j]["Id"].ToString())
                {
                    isRootNode = false;
                    break;
                }
            }

            if (isRootNode)
            {
                TreeNode tn = this.GetDefaultTreeNode(result.Ds.Tables[0].Rows[i]["Nev"].ToString(), result.Ds.Tables[0].Rows[i]["Id"].ToString(), result.Ds.Tables[0].Rows[i]["Tipus"].ToString());
                DossziekTreeView.Nodes.Add(tn);

                if (tn.Value == SelectedId)
                    tn.Selected = true;

                nodeList.Add(tn);
            }

        }

        while (nodeList.Count != 0)
        {
            TreeNode tn = (TreeNode)nodeList[0];

            nodeList.RemoveAt(0);

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                if (result.Ds.Tables[0].Rows[i]["SzuloMappa_Id"].ToString() == tn.Value)
                {
                    TreeNode cn = this.GetDefaultTreeNode(result.Ds.Tables[0].Rows[i]["Nev"].ToString(), result.Ds.Tables[0].Rows[i]["Id"].ToString(), result.Ds.Tables[0].Rows[i]["Tipus"].ToString());
                    tn.ChildNodes.Add(cn);

                    if (cn.Value == SelectedId)
                        cn.Selected = true;

                    nodeList.Add(cn);
                }
            }
        }

        DossziekTreeView.ExpandAll();

        DossziekTreeView_SelectedNodeChanged(DossziekTreeView, EventArgs.Empty);
    }

    private void LoadTreeView()
    {
        this.LoadTreeView(string.Empty);
    }
}
