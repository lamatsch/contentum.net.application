﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EFeladoJegyzekStatisztikaPanel.ascx.cs"
    Inherits="eRecordComponent_EFeladoJegyzekStatisztikaPanel" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc4" %>
<table id="MainTable" runat="server" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="GridViewHeaderBackGroundStyle">
            <asp:Panel ID="StatisztikaPanel" runat="server" Visible="false">
                <asp:GridView ID="StatisztikaGridView" runat="server" CellPadding="0" CellSpacing="0"
                    Caption="<div class='GridViewCaptionStyle'>Statisztika</div>" BorderWidth="1"
                    GridLines="Both" AllowPaging="False" PagerSettings-Visible="false" AllowSorting="False"
                    AutoGenerateColumns="False" DataKeyNames="TetelTipus" OnRowDataBound="gvMasterDetail_RowDataBound">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <asp:BoundField DataField="TetelTipus" HeaderText="Tétel típus">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Count" HeaderText="Tételek száma összesen">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Hiányos tételek">
                            <ItemTemplate>
                                <asp:GridView runat="server" ID="gvChild" CellPadding="0" CellSpacing="0" BorderWidth="1" Width="100%"
                                    GridLines="Both" AllowPaging="False" PagerSettings-Visible="false" AllowSorting="False"
                                    ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="DetailKey,FacetName"
                                    OnRowDataBound="gvChild_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:BoundField DataField="ErrorCategoryHeader" HeaderText="Hiba">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Count" HeaderText="Tételek száma összesen">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Mezők">
                                            <ItemTemplate>
                                                <asp:GridView runat="server" ID="gvGrandChild" CellPadding="0" CellSpacing="0" BorderWidth="1" Width="100%"
                                                    GridLines="Both" AllowPaging="False" PagerSettings-Visible="false" AllowSorting="False"
                                                    AutoGenerateColumns="False" DataKeyNames="FieldName" OnRowDataBound="gvGrandChild_RowDataBound">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Megnevezes" HeaderText="Megnevezés">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FieldName" HeaderText="Mezőnév">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Count" HeaderText="Darab">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Ids" HeaderText="Ids">
                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="30px" />
                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="" SortExpression="Ids">
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <ItemTemplate>
                                                                <div class="DisableWrap" style="text-align: left;">
                                                                    <%--<asp:HyperLink runat="server" ID="KimenoKuldemenyLink" Text="Javítás..." NavigateUrl='<%# string.Concat("~/KimenoKuldemenyekList.aspx?Id=", Eval("Ids")) %>' />--%>
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyListaLink" ImageUrl="~/images/hu/egyeb/lista.gif"
                                                                        onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                                                                        AlternateText="Vissza a listára (tételre szűréssel)..." CommandName="BackToFilteredList"
                                                                        CommandArgument='<%# Eval("Ids") %>' OnClick="ImageButton_Click" />
                                                                    <asp:ImageButton runat="server" ID="imgKimenoKuldemenyLink" ImageUrl="~/images/hu/egyeb/modositas.gif"
                                                                        onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')"
                                                                        AlternateText="Javítás..." Visible="false" />
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
            </asp:Panel>
        </td>
    </tr>
</table>
