﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// TODO: DataRelation mentén szűrni, lehet egy eredménytáblában az összes adat (most annyi tábla, ahány facet)
/// StatisticsResult fázis átugrása (DataTable visszaadása)
/// </summary>
public partial class eRecordComponent_EFeladoJegyzekStatisztikaPanel : System.Web.UI.UserControl
{
    private UI ui = new UI();

    private Dictionary<string, string> dictMezoMegnevezes = null;
    public Dictionary<string, string> DictMezoMegnevezes
    {
        get
        {
            if (dictMezoMegnevezes == null)
            {
                dictMezoMegnevezes = new Dictionary<string, string>();
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek, "Közönséges küldemény");
                // BLG_1938
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetelek, "Nemzetközi Közönséges küldemény");
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetelek, "Közönséges azonosított küldemény");
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetelek, "Nemzetközi Közönséges azonosított küldemény");
                
                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetelek, "Nyilvántartott küldemény");

                dictMezoMegnevezes.Add(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetelek, "Nemzetközi Nyilvántartott küldemény");

                dictMezoMegnevezes.Add("sorszam", "Sorszám");
                dictMezoMegnevezes.Add("azonosito", "Azonosító");
                dictMezoMegnevezes.Add("alapszolg", "Alapszolgáltatás");
                dictMezoMegnevezes.Add("orszagkod", "Országkód");
                dictMezoMegnevezes.Add("iranyitoszam", "IRSZ");
                dictMezoMegnevezes.Add("suly", "Súly");
                dictMezoMegnevezes.Add("kulonszolgok", "Különszolgáltatások");
                dictMezoMegnevezes.Add("vakok_irasa", "Vakok írása jelzés");
                dictMezoMegnevezes.Add("uv_osszeg", "Utánvétel összege");
                dictMezoMegnevezes.Add("uv_lapid", "Utánvétel biz. azon.");
                dictMezoMegnevezes.Add("eny_osszeg", "Értéknyilv. összege");
                dictMezoMegnevezes.Add("kezelesi_mod", "Kezelési mód");
                dictMezoMegnevezes.Add("meret", "Méret");
                dictMezoMegnevezes.Add("cimzett", "Címzett neve");
                dictMezoMegnevezes.Add("cimzett_hely", "Címzett helység");
                dictMezoMegnevezes.Add("cimzett_ertcsat", "Címzett e-értesítés fajta");
                dictMezoMegnevezes.Add("cimzett_ertcim", "Címzett e-értesítés cím");
                dictMezoMegnevezes.Add("felado_ertcim", "Feladó e-értesítés cím");
                dictMezoMegnevezes.Add("dij", "Díj");
                dictMezoMegnevezes.Add("potlapszam", "Pótlapszám");
                dictMezoMegnevezes.Add("kozelebbi_cim", "Közelebbi cím");
                dictMezoMegnevezes.Add("megjegyzes", "Megjegyzés");
                dictMezoMegnevezes.Add("feldolg", "Feldolgozottság mértéke");
                dictMezoMegnevezes.Add("sajat_azonosito", "Saját azonosító");
            }
            return this.dictMezoMegnevezes;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string GetMegnevezesByFieldname(string fieldname)
    {
        if (DictMezoMegnevezes.ContainsKey(fieldname))
        {
            return DictMezoMegnevezes[fieldname];
        }

        return fieldname;
    }

    private string GetIdsFromDataRows(DataRow[] rows)
    {
        if (rows == null || rows.Length == 0) return String.Empty;
        List<string> Ids = new List<string>();

        foreach (DataRow row in rows)
        {
            if (row.Table.Columns.Contains("ids"))
            {
                string[] arrayIds = row["ids"].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string id in arrayIds)
                {
                    if (Ids.Contains(id) == false)
                    {
                        Ids.Add(id);
                    }
                }
            }
        }
        return String.Join(";", Ids.ToArray());
    }

    // BLG_1938
    protected DataSet GetStatistics(Result result, EPostazasiParameterek ePostazasiParameterek)
    {
        if (result == null) return null;
        if (result.IsError)
        {
            ui.GridViewClear(StatisztikaGridView);
            return null;
        }

        DataSet ds = new DataSet();

        DataTable tbStatisztika = new DataTable("Statistics");
        tbStatisztika.Columns.Add("DetailKey");
        tbStatisztika.Columns.Add("TetelTipus");
        tbStatisztika.Columns.Add("Count");

        DataTable tbStatisztikaFacets = new DataTable("DetailKey_Facet");
        tbStatisztikaFacets.Columns.Add("DetailKey"); // Foreign Key
        tbStatisztikaFacets.Columns.Add("FacetName"); // Foreign Key
        tbStatisztikaFacets.Columns.Add("ErrorCategoryHeader");

        tbStatisztikaFacets.Columns.Add("Count");

        ds.Tables.Add(tbStatisztika);
        ds.Tables.Add(tbStatisztikaFacets);
        // Create DataRelation
        DataRelation relTetelTipus = new DataRelation("DetailKey",
             tbStatisztika.Columns["DetailKey"], tbStatisztikaFacets.Columns["DetailKey"]);
        // Add the relation to the DataSet
        ds.Relations.Add(relTetelTipus);

        if (!ePostazasiParameterek.IgnoreKozonsegesTetelek)
        {
            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel))
            {
                DataTable tableKozonseges = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableKozonseges);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableKozonseges.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }

            }

            // BLG_1938
            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel))
            {
                DataTable tableNemzKozonseges = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableNemzKozonseges);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableNemzKozonseges.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }

            }

            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel))
            {
                DataTable tableKozonsegesAzon = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableKozonsegesAzon);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableKozonsegesAzon.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }

            }

            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel))
            {
                DataTable tableNemzKozonsegesAzon = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableNemzKozonsegesAzon);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableNemzKozonsegesAzon.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }

            }
        }

        if (!ePostazasiParameterek.IgnoreNyilvantartottTetelek)
        {
            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel))
            {
                DataTable tableNyilvantartott = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableNyilvantartott);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableNyilvantartott.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }
            }

            if (result.Ds.Tables.Contains(Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel))
            {
                DataTable tableNemzNyilvantartott = result.Ds.Tables[Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel];

                // debug
                Dictionary<string, Dictionary<string, Contentum.eUtility.EFeladoJegyzek.Validation.StatisticsResult>> dict = Contentum.eUtility.EFeladoJegyzek.Validation.GetValidationStatistics(tableNemzNyilvantartott);
                string detailKey = Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetelek;
                DataSet dsDetail = Contentum.eUtility.EFeladoJegyzek.Validation.ConvertStatisticsToDataSet(detailKey, dict);

                string tetelTipus = GetMegnevezesByFieldname(detailKey);
                DataRow row = tbStatisztika.NewRow();
                row["DetailKey"] = detailKey;
                row["TetelTipus"] = tetelTipus;
                row["Count"] = tableNemzNyilvantartott.Rows.Count;
                tbStatisztika.Rows.Add(row);

                foreach (DataTable tbDetailFacet in dsDetail.Tables)
                {
                    if (tbDetailFacet.Rows.Count > 0)
                    {
                        string facetName = tbDetailFacet.Rows[0]["FacetName"].ToString();
                        DataTable tbFacet = tbDetailFacet.Copy();
                        ds.Tables.Add(tbFacet);

                        DataRow rowRel = tbStatisztikaFacets.NewRow();
                        rowRel["DetailKey"] = detailKey;
                        rowRel["FacetName"] = facetName;
                        rowRel["ErrorCategoryHeader"] = Contentum.eUtility.EFeladoJegyzek.Validation.GetErrorCategoryHeader(facetName);
                        int db = 0;
                        foreach (DataRow rowDetail in tbDetailFacet.Rows)
                        {
                            db += Int32.Parse(rowDetail["Count"].ToString());
                        }
                        rowRel["Count"] = db;
                        tbStatisztikaFacets.Rows.Add(rowRel);

                        tbFacet.Columns.Add("Megnevezes"); // Foreign Key: TetelTipus, FacetName
                        foreach (DataRow rowFacet in tbFacet.Rows)
                        {
                            rowFacet["Megnevezes"] = GetMegnevezesByFieldname(rowFacet["FieldName"].ToString());
                        }
                        // Create DataRelation
                        DataRelation relFacet = new DataRelation(String.Format("rel_{0}_{1}", detailKey, facetName),
                             new DataColumn[] { tbStatisztikaFacets.Columns["DetailKey"], tbStatisztikaFacets.Columns["FacetName"] }, new DataColumn[] { tbFacet.Columns["DetailKey"], tbFacet.Columns["FacetName"] });

                        // Add the relation to the DataSet
                        ds.Relations.Add(relFacet);
                    }
                }
            }
        }
        return ds;
    }

    protected void gvMasterDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataSet ds = ((GridView)sender).DataSource as DataSet;
            string detailKey = Convert.ToString(((DataRowView)e.Row.DataItem)["DetailKey"]);
            DataTable dtTetelTipusFacetRelationFields = GetTetelTipusFacetRelationFields(detailKey, ds);
            DataSet newDs = new DataSet();
            newDs.Tables.Add(dtTetelTipusFacetRelationFields);

            foreach (DataRow row in dtTetelTipusFacetRelationFields.Rows)
            {
                string facetName = row["FacetName"].ToString();
                if (ds.Tables.Contains(String.Format("{0}_{1}", detailKey, facetName)))
                {
                    DataTable dtFacet = ds.Tables[String.Format("{0}_{1}", detailKey, facetName)].Copy();
                    newDs.Tables.Add(dtFacet);
                }
            }
            ((GridView)e.Row.FindControl("gvChild")).DataSource = newDs; //dtTetelTipusFacetRelationFields;
            ((GridView)e.Row.FindControl("gvChild")).DataMember = "DetailKey_Facet";
            ((GridView)e.Row.FindControl("gvChild")).DataBind();
        }
    }

    protected void gvChild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataSet ds = ((GridView)sender).DataSource as DataSet;
            string  facetName = Convert.ToString(((DataRowView)e.Row.DataItem)["FacetName"]);
            DataTable dtTetelTipusFacetFields = GetTetelTipusFacetFields(Convert.ToString(((DataRowView)e.Row.DataItem)["DetailKey"]), facetName, ds);
            GridView gvGrandChild = (GridView)e.Row.FindControl("gvGrandChild");
            gvGrandChild.DataSource = dtTetelTipusFacetFields;
            gvGrandChild.DataBind();
        }
    }

    public void gvGrandChild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton link = e.Row.FindControl("imgKimenoKuldemenyLink") as ImageButton;
            if (link != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv != null)
                {
                    link.OnClientClick = JavaScripts.SetOnClientClick("PostazasEFeladoJegyzekJavitasForm.aspx",
                    QueryStringVars.KuldemenyId + "=" + drv["Ids"].ToString()
                    + "&" + QueryStringVars.Mode + "=" + drv["FieldName"].ToString()
                        //, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KimenoKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, null, EventArgumentConst.refreshMasterList);
                }
            }
        }
    }

    private DataTable GetTetelTipusFacetRelationFields(String tetelTipus, DataSet ds)
    {
        DataTable dtTetelTipusFacetRelationFields = ds.Tables["DetailKey_Facet"];
        DataTable dtClone = dtTetelTipusFacetRelationFields.Clone();
        foreach (DataRow oItem in dtTetelTipusFacetRelationFields.Rows)
        {
            if (Convert.ToString(oItem["DetailKey"]) == tetelTipus)
                dtClone.Rows.Add(oItem.ItemArray);
        }
        return dtClone;

        //foreach (DataRow orderRow in custRow.GetChildRows(customerOrdersRelation))
        //{
        //    Console.WriteLine(orderRow["OrderID"].ToString());
        //}

    }

    private DataTable GetTetelTipusFacetFields(String detailKey, string facetName, DataSet ds)
    {
        if (ds != null && ds.Tables.Contains(String.Format("{0}_{1}", detailKey, facetName)))
        {
            DataTable dtTetelTipusFacetFields = ds.Tables[String.Format("{0}_{1}", detailKey, facetName)];
            DataTable dtClone = dtTetelTipusFacetFields.Clone();
            foreach (DataRow oItem in dtTetelTipusFacetFields.Rows)
            {
                if (Convert.ToString(oItem["DetailKey"]) == detailKey && Convert.ToString(oItem["FacetName"]) == facetName)
                    dtClone.Rows.Add(oItem.ItemArray);
            }
            return dtClone;
        }

        return null;

        //foreach (DataRow orderRow in custRow.GetChildRows(customerOrdersRelation))
        //{
        //    Console.WriteLine(orderRow["OrderID"].ToString());
        //}

    }

    private DataTable GetMissingFields(String tetelTipus, DataSet ds)
    {
        DataTable dtMissingFields = ds.Tables[1];
        DataTable dtClone = dtMissingFields.Clone();
        foreach (DataRow oItem in dtMissingFields.Rows)
        {
            if (Convert.ToString(oItem["TetelTipus"]) == tetelTipus)
                dtClone.Rows.Add(oItem.ItemArray);
        }
        return dtClone;

        //foreach (DataRow orderRow in custRow.GetChildRows(customerOrdersRelation))
        //{
        //    Console.WriteLine(orderRow["OrderID"].ToString());
        //}

    }

    // BLG_1938
    public void FillFromResult(Result result, EPostazasiParameterek ePostazasiParameterek)
    {
        StatisztikaGridView_GridViewBind(result, ePostazasiParameterek);
    }

    // BLG_1938
    protected void StatisztikaGridView_GridViewBind(Result result, EPostazasiParameterek ePostazasiParameterek)
    {
        if (result == null) return;

        StatisztikaPanel.Visible = false;
        if (result == null || result.IsError)
        {
            ui.GridViewClear(StatisztikaGridView);
            StatisztikaPanel.Visible = false;
        }
        else
        {
            // BLG_1938
            DataSet dsStatisztika = GetStatistics(result, ePostazasiParameterek);
            //DataTable tbStatisztika = GetStatistics(result);

            //if (tbStatisztika == null)
            if (dsStatisztika == null)
            {
                ui.GridViewClear(StatisztikaGridView);
                StatisztikaPanel.Visible = false;
            }
            else
            {
                DataTable tbStatisztika = dsStatisztika.Tables[0];
                StatisztikaGridView.DataSource = dsStatisztika;//tbStatisztika;
                StatisztikaGridView.DataMember = dsStatisztika.Tables[0].TableName;
                StatisztikaGridView.DataBind();

                StatisztikaPanel.Visible = true;
            }
        }
    }

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgbtn = sender as ImageButton;
        if (imgbtn == null) return;

        if (imgbtn.CommandName == "BackToFilteredList")
        {
            if (!String.IsNullOrEmpty(imgbtn.CommandArgument))
            {
                EREC_KuldKuldemenyekSearch searchObject = new EREC_KuldKuldemenyekSearch(true);
                searchObject.Id.Value = Search.GetSqlInnerString(imgbtn.CommandArgument.Split(new char[] { ';' }));
                searchObject.Id.Operator = Query.Operators.inner;

                searchObject.ReadableWhere = "E-Feladójegyzék tétel";

                Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.KimenoKuldemenyekSearch);
            }
            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        }
        //else if (imgbtn.CommandName == ...)
        //{
        //// TODO
        //}
        else
        {
            // TODO
        }

    }


}

