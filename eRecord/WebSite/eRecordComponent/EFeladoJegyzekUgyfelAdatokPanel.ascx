﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EFeladoJegyzekUgyfelAdatokPanel.ascx.cs"
    Inherits="eRecordComponent_EFeladoJegyzekUgyfelAdatokPanel" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc4" %>
<asp:Panel ID="MainPanel" runat="server">
<asp:HiddenField ID="hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok" runat="server" />
<asp:HiddenField ID="hfFileNameXsdEFeladoJegyzekUgyfelAdatok" runat="server" />
<asp:HiddenField ID="hfFileNameXmlEFeladoJegyzekUgyfelAdatok" runat="server" />
<asp:HiddenField ID="hfPostakonyvId" runat="server" />
<table id="MainTable" runat="server" cellspacing="0" cellpadding="0" width="100%">
    <tr class="urlapSor_kicsi">
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelAzonositoStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelAzonosito" runat="server" Text="Feladó azonosító:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredNumberBox ID="RequiredNumberBox_Azonosito" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelAzonositoStar" />
        </td>
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelMegallapodasStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelMegallapodas" runat="server" Text="Megállapodás száma:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredNumberBox ID="RequiredNumberBox_Megallapodas" runat="server" Validate="true" MinLength="8" MaxLength="8" NumberMaxLength="8"
                LabelRequiredIndicatorID="labelMegallapodasStar" />
        </td>
    </tr>
    <tr class="urlapSor_kicsi">
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelNevStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelNev" runat="server" Text="Feladó neve:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredTextBox ID="RequiredTextBox_Nev" runat="server" Validate="false" LabelRequiredIndicatorID="labelNevStar" />
        </td>
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelKezdoallasStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelKezdoallas" runat="server" Text="Bérmentesítő gép kezdőállás:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredNumberBox ID="RequiredNumberBox_Kezdoallas" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelKezdoallasStar" />
        </td>
    </tr>
    <tr class="urlapSor_kicsi">
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelIRSZStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelIRSZ" runat="server" Text="Irányítószám:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredNumberBox ID="RequiredNumberBox_IRSZ" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelIRSZStar" MaxLength="4" />
        </td>
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelZaroallasStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelZaroallas" runat="server" Text="Bérmentesítő gép záróállás:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredNumberBox ID="RequiredNumberBox_Zaroallas" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelZaroallasStar" />
        </td>
    </tr>
    <tr class="urlapSor_kicsi">
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelHelysegStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelHelyseg" runat="server" Text="Helység:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredTextBox ID="RequiredTextBox_Helyseg" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelHelysegStar" />
        </td>
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelEngedelyszamStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelEngedelyszam" runat="server" Text="Bevizsg. engedély száma:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredTextBox ID="RequiredTextBox_Engedelyszam" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelEngedelyszamStar" />
        </td>
    </tr>
    <tr class="urlapSor_kicsi">
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelUtcaStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelUtca" runat="server" Text="Közelebbi cím:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredTextBox ID="RequiredTextBox_Utca" runat="server" Validate="false" LabelRequiredIndicatorID="labelUtcaStar" />
        </td>
        <td class="mrUrlapCaption">
            <div class="DisableWrap">
                <asp:Label ID="labelAdatfajlnevStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelAdatfajlnev" runat="server" Text="Adatfájlnév:" />
            </div>
        </td>
        <td class="mrUrlapMezo">
            <uc4:RequiredTextBox ID="RequiredTextBox_Adatfajlnev" runat="server" Validate="false"
                LabelRequiredIndicatorID="labelAdatfajlnevStar" MaxLength="20" />
        </td>
    </tr>
</table>
</asp:Panel>
