﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_EFeladoJegyzekUgyfelAdatokPanel : System.Web.UI.UserControl
{
    public string Megallapodas
    {
        get
        {
            return RequiredNumberBox_Megallapodas.Text;
        }
    }

    public string PostakonyvId
    {
        get { return hfPostakonyvId.Value; }
        set { hfPostakonyvId.Value = value; }
    }


    #region Paths to XSD/XML files

    public string DirectoryPathToXsd
    {
        get { return hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok.Value; }
        set { hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok.Value = value; }
    }
    
    public string FileNameXml
    {
        get { return hfFileNameXmlEFeladoJegyzekUgyfelAdatok.Value; }
        set { hfFileNameXmlEFeladoJegyzekUgyfelAdatok.Value = value; }
    }

    public string FileNameXsd
    {
        get { return hfFileNameXsdEFeladoJegyzekUgyfelAdatok.Value; }
        set { hfFileNameXsdEFeladoJegyzekUgyfelAdatok.Value = value; }
    }


    ////public string PathToUgyfelAdatokXml
    ////{
    ////    get
    ////    {
    ////        return Path.Combine(
    ////                 Path.Combine(
    ////                     Server.MapPath(".")
    ////                     , hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok.Value
    ////                 )
    ////                 , hfFileNameXmlEFeladoJegyzekUgyfelAdatok.Value
    ////              );
    ////    }
    ////}

    private string PathToUgyfelAdatokXsd
    {
        get
        {
            return Path.Combine(
                     Path.Combine(
                         Server.MapPath(".")
                         , hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok.Value
                     )
                     , hfFileNameXsdEFeladoJegyzekUgyfelAdatok.Value
                  );
        }
    }
    #endregion Paths to XSD/XML files

    // XSD/XML mezők leképezése vezérlőnevekre
    private Dictionary<string, string> dictMezoVezerloMegnevezes = null;
    public Dictionary<string, string> DictMezoVezerloMegnevezes
    {
        get
        {
            if (dictMezoVezerloMegnevezes == null)
            {
                dictMezoVezerloMegnevezes = new Dictionary<string, string>();
                dictMezoVezerloMegnevezes.Add("azonosito", RequiredNumberBox_Azonosito.UniqueID);
                dictMezoVezerloMegnevezes.Add("nev", RequiredTextBox_Nev.UniqueID);
                dictMezoVezerloMegnevezes.Add("iranyitoszam", RequiredNumberBox_IRSZ.UniqueID);
                dictMezoVezerloMegnevezes.Add("helyseg", RequiredTextBox_Helyseg.UniqueID);
                dictMezoVezerloMegnevezes.Add("utca", RequiredTextBox_Utca.UniqueID);
                dictMezoVezerloMegnevezes.Add("megallapodas", RequiredNumberBox_Megallapodas.UniqueID);
                dictMezoVezerloMegnevezes.Add("kezdoallas", RequiredNumberBox_Kezdoallas.UniqueID);
                dictMezoVezerloMegnevezes.Add("zaroallas", RequiredNumberBox_Zaroallas.UniqueID);
                dictMezoVezerloMegnevezes.Add("engedelyszam", RequiredTextBox_Engedelyszam.UniqueID);
                dictMezoVezerloMegnevezes.Add("adatfajlnev", RequiredTextBox_Adatfajlnev.UniqueID);
            }
            return this.dictMezoVezerloMegnevezes;
        }
    }

    // XSD/XML mezők leképezése mezőnevekre
    private Dictionary<string, string> dictMezoMegnevezes = null;
    public Dictionary<string, string> DictMezoMegnevezes
    {
        get
        {
            if (dictMezoMegnevezes == null)
            {
                dictMezoMegnevezes = new Dictionary<string, string>();
                dictMezoMegnevezes.Add("azonosito", "vevő azonosító");
                dictMezoMegnevezes.Add("nev", "név");
                dictMezoMegnevezes.Add("iranyitoszam", "irányítószám");
                dictMezoMegnevezes.Add("helyseg", "helység");
                dictMezoMegnevezes.Add("utca", "címhely");
                dictMezoMegnevezes.Add("megallapodas", "megállapodás szám");
                dictMezoMegnevezes.Add("kezdoallas", "bérmentesítő gép kezdőállás");
                dictMezoMegnevezes.Add("zaroallas", "bérmentesítő gép záróállás");
                dictMezoMegnevezes.Add("engedelyszam", "bevizsgálási engedélyszám");
                dictMezoMegnevezes.Add("adatfajlnev", "adatfájlnév");
            }
            return this.dictMezoMegnevezes;
        }
    }

    private Dictionary<string, string> dictMezoDefaultValue = null;
    public Dictionary<string, string> DictMezoDefaultValue
    {
        get
        {
            if (dictMezoDefaultValue == null)
            {
                dictMezoDefaultValue = new Dictionary<string, string>();
                dictMezoDefaultValue.Add("adatfajlnev", Contentum.eUtility.EFeladoJegyzek.Constants.FileNameXmlEFeladoJegyzek); // "ADAT.xml"
            }
            return this.dictMezoDefaultValue;
        }
    }

    public string GetXmlFieldValue(string fieldName)
    {
        if (!String.IsNullOrEmpty(fieldName))
        {
            Result result = GetUgyfelAdatokXml();
            if (!result.IsError)
            {
                try
                {
                    System.Xml.XmlDocument xmlDocument = result.Record as System.Xml.XmlDocument;
                    if (xmlDocument != null)
                    {
                        System.Xml.XmlNode nd = xmlDocument.SelectSingleNode(String.Format("ugyfel_adatok/{0}", fieldName));
                        if (nd != null)
                        {
                            return nd.InnerText;
                        }
                        else if (DictMezoDefaultValue.ContainsKey(fieldName))
                        {
                            string defaultValue = DictMezoDefaultValue[fieldName];
                            Logger.Info(String.Format("A '{0}' mező nem található az ügyfél adatok között! Alapértelmezett érték beállítása: {1}", fieldName, defaultValue));
                            return defaultValue;
                        }
                        else
                        {
                            Logger.Warn(String.Format("A '{0}' mező nem található az ügyfél adatok között!", fieldName));
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("Hiba az ügyfél adatok értelmezésénél! " + ex.Message);
                }
            }
        }

        return null;
    }

    private Dictionary<string, List<string>> mandatoryFields = null;
    public Dictionary<string, List<string>> MandatoryFields
    {
        get
        {
            if (this.mandatoryFields == null)
            {
                this.mandatoryFields = Contentum.eUtility.XmlHelper.GetMandatoryFieldsFromXmlSchema(XmlSchemaEFeladojegyzekUgyfelAdatok);
            }
            return this.mandatoryFields;
        }
    }

    private System.Xml.Schema.XmlSchema xmlSchemaEFeladojegyzekUgyfelAdatok = null;
    public System.Xml.Schema.XmlSchema XmlSchemaEFeladojegyzekUgyfelAdatok
    {
        get
        {
            if (this.xmlSchemaEFeladojegyzekUgyfelAdatok == null)
            {
                //System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
                //this.xmlSchemaEFeladojegyzekUgyfelAdatok = GetXsdSchemaFromFile(PathToUgyfelAdatokXsd, validationEventHandler);
                this.xmlSchemaEFeladojegyzekUgyfelAdatok = Contentum.eUtility.XmlHelper.GetXsdSchemaFromFile(PathToUgyfelAdatokXsd);
            }
            return this.xmlSchemaEFeladojegyzekUgyfelAdatok;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        // TODO: hívó állítja be
        // provizórikusan default értékek (teszt)
        SetDefaultPaths();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SetValidation();
    }

    public void SetDefaultPaths()
    {
        hfDirectoryPathToXsdEFeladoJegyzekUgyfelAdatok.Value = Contentum.eUtility.EFeladoJegyzek.Constants.DirectoryPathToXsdEFeladoJegyzekUgyfelAdatok;
        hfFileNameXmlEFeladoJegyzekUgyfelAdatok.Value = Contentum.eUtility.EFeladoJegyzek.Constants.FileNameXmlEFeladoJegyzekUgyfelAdatok;
        hfFileNameXsdEFeladoJegyzekUgyfelAdatok.Value = Contentum.eUtility.EFeladoJegyzek.Constants.FileNameXsdEFeladoJegyzekUgyfelAdatok;
    }

    public void SetValidation()
    {
        // kötelező mezőkre validálás
        foreach (string key in DictMezoVezerloMegnevezes.Keys)
        {
            Control ctrl = Page.FindControl(DictMezoVezerloMegnevezes[key]);

            // Reflection
            System.Reflection.PropertyInfo pi = ctrl.GetType().GetProperty("Validate", typeof(bool));
            if (pi != null)
            {
                if (MandatoryFields.ContainsKey(Contentum.eUtility.EFeladoJegyzek.Constants.ugyfel_adatok))
                {
                    pi.SetValue(ctrl, MandatoryFields[Contentum.eUtility.EFeladoJegyzek.Constants.ugyfel_adatok].Contains(key), null);
                }
            }

        }
    }
    
    protected TextBox GetTextBoxControlByUniqueID(string uniqueID)
    {
        TextBox tb = null;
        if (!String.IsNullOrEmpty(uniqueID))
        {
            Control ctrl = Page.FindControl(uniqueID);
            if (ctrl is TextBox)
            {
                tb = (TextBox)ctrl;
            }
            else if (ctrl is Component_RequiredTextBox)
            {
                tb = ((Component_RequiredTextBox)ctrl).TextBox;
            }
            else if (ctrl is Component_RequiredNumberBox)
            {
                tb = ((Component_RequiredNumberBox)ctrl).TextBox;
            }
            else
            {
                // Reflection
                System.Reflection.PropertyInfo pi = ctrl.GetType().GetProperty("TextBox", typeof(TextBox));
                if (pi != null)
                {
                    tb = pi.GetValue(ctrl, null) as TextBox;
                }
            }
        }
        return tb;
    }

    public Result GetUgyfelAdatokXml()
    {
        Result result = new Result();
        System.Xml.XmlDocument xmlDocument = null;

        if (!String.IsNullOrEmpty(PostakonyvId))
        {
            Contentum.eRecord.Service.EREC_IraIktatoKonyvekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page);
            execParam.Record_Id = PostakonyvId;

            Result result_postakonyvekGet = service.Get(execParam);

            if (result_postakonyvekGet.IsError)
            {
                return result_postakonyvekGet;
            }

            try
            {
                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = result_postakonyvekGet.Record as EREC_IraIktatoKonyvek;

                if (erec_IraIktatoKonyvek == null)
                {
                    throw new Exception("Hiba a postakönyv lekérésénél: a rekord értéke null!");
                }

                if (erec_IraIktatoKonyvek.IktatoErkezteto != Constants.IktatoErkezteto.Postakonyv)
                {
                    throw new Exception("Hiba a postakönyv lekérésénél: a talált tétel nem postakönyv!");
                }

                xmlDocument = new System.Xml.XmlDocument();
                if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok))
                {
                    xmlDocument.LoadXml(erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok);
                }
                else
                {
                    xmlDocument = GetUgyfelAdatokBaseXml();

                    if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.PostakonyvVevokod) || !String.IsNullOrEmpty(erec_IraIktatoKonyvek.PostakonyvMegallapodasAzon))
                    {
                        if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.PostakonyvVevokod))
                        {
                            System.Xml.XmlNode xmlNodeVevokod = xmlDocument.CreateElement("azonosito");
                            xmlNodeVevokod.InnerXml = erec_IraIktatoKonyvek.PostakonyvVevokod;
                            xmlDocument.DocumentElement.AppendChild(xmlNodeVevokod);
                        }
                        if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.PostakonyvMegallapodasAzon))
                        {
                            System.Xml.XmlNode xmlNodeMegallapodas = xmlDocument.CreateElement("megallapodas");
                            xmlNodeMegallapodas.InnerXml = erec_IraIktatoKonyvek.PostakonyvMegallapodasAzon;
                            xmlDocument.DocumentElement.AppendChild(xmlNodeMegallapodas);
                        }

                    }
                }

                result.Record = xmlDocument;
            }
            catch (Exception ex)
            {
                result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                result.ErrorType = ex.GetType().ToString();
            }

        }
        else
        {
            result.ErrorCode = "Nincs megadva a postakönyv azonosítója!";
            result.ErrorMessage = "Nincs megadva a postakönyv azonosítója!";
        }

        return result;
    }

    public Result LoadUgyfelAdatokComponentsFromXml()
    {
        Result result = GetUgyfelAdatokXml();
        if (result != null && result.Record != null)
        {
            System.Xml.XmlDocument xmlDocument = result.Record as System.Xml.XmlDocument;
            result = LoadUgyfelAdatokComponentsFromXml(xmlDocument);
        }

        return result;
    }

    public Result LoadUgyfelAdatokComponentsFromXml(System.Xml.XmlDocument xmlDocument)
    {
        Result result = new Result();
        if (xmlDocument != null)
        {
            try
            {
                System.Xml.XmlNodeList ndList = xmlDocument.SelectNodes("ugyfel_adatok/node()");

                foreach (System.Xml.XmlNode nd in ndList)
                {
                    if (DictMezoVezerloMegnevezes.ContainsKey(nd.Name))
                    {
                        TextBox tb = GetTextBoxControlByUniqueID(DictMezoVezerloMegnevezes[nd.Name]);

                        if (tb != null)
                        {
                            tb.Text = nd.InnerText;
                        }
                    }
                    else
                    {
                        Logger.Warn(String.Format("Ügyfél adatok XML: nem található a(z) '{0}' mező vezérlője", nd.Name));
                    }
                }

                result.Record = xmlDocument;
            }
            catch (Exception ex)
            {
                Logger.Error("Hiba az XML (ügyfél adatok) betöltésénél: " + ex.Message);
            }
        }
        else
        {
            System.ArgumentNullException ex = new ArgumentNullException("xmlDocument");
            result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            result.ErrorType = ex.GetType().ToString();
        }

        return result;
    }

    /// <summary>
    /// Xml declaration + document element
    /// </summary>
    /// <returns></returns>
    private System.Xml.XmlDocument GetUgyfelAdatokBaseXml()
    {
        System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
        
        // declaration
        string encodingname = System.Text.Encoding.GetEncoding(1250).HeaderName; // WebName?
        System.Xml.XmlNode xmlNodeXmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", encodingname, null);
        xmlDocument.AppendChild(xmlNodeXmlDeclaration);

        //1. szint DocumentElement (root)
        System.Xml.XmlElement xmlElement_ugyfel_adatok = xmlDocument.CreateElement(Contentum.eUtility.EFeladoJegyzek.Constants.ugyfel_adatok);
        xmlDocument.AppendChild(xmlElement_ugyfel_adatok);

        return xmlDocument;
    }

    public System.Xml.XmlDocument GetUgyfelAdatokXmlFromComponents()
    {
        System.Xml.XmlDocument xmlDocument = GetUgyfelAdatokBaseXml();
        ////xmlDocument.Schemas.Add(schema);


        foreach (string key in DictMezoVezerloMegnevezes.Keys)
        {
            TextBox tb = GetTextBoxControlByUniqueID(DictMezoVezerloMegnevezes[key]);

            if (tb != null && !String.IsNullOrEmpty(tb.Text))
            {
                System.Xml.XmlElement xmlElement = xmlDocument.CreateElement(key);
                xmlElement.InnerText = tb.Text;
                xmlDocument.DocumentElement.AppendChild(xmlElement);
            }
        }

        return xmlDocument;
    }

    public Result UgyfelAdatokSave()
    {
        Result result = new Result();
        System.Xml.XmlDocument xmlDocument = GetUgyfelAdatokXmlFromComponents();
        //System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);

        try
        {
            System.Xml.Schema.XmlSchema xmlSchema = XmlSchemaEFeladojegyzekUgyfelAdatok;
            //ValidateXmlAgainstXmlSchema(xmlDocument, xmlSchema, validationEventHandler);
            Contentum.eUtility.XmlHelper.ValidateXmlAgainstXmlSchema(xmlDocument, xmlSchema);

            if (!String.IsNullOrEmpty(PostakonyvId))
            {
                Contentum.eRecord.Service.EREC_IraIktatoKonyvekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = PostakonyvId;

                Result result_postakonyvekGet = service.Get(execParam);

                if (result_postakonyvekGet.IsError)
                {
                    return result_postakonyvekGet;
                }

                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_postakonyvekGet.Record;
                erec_IraIktatoKonyvek.Updated.SetValueAll(false);
                erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

                erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok = xmlDocument.OuterXml;
                erec_IraIktatoKonyvek.Updated.EFeladoJegyzekUgyfelAdatok = true;

                erec_IraIktatoKonyvek.Base.Updated.Ver = true;
                Result result_postakonyvekUpdate = service.Update(execParam, erec_IraIktatoKonyvek);

                if (result_postakonyvekUpdate.IsError)
                {
                    return result_postakonyvekUpdate;
                }

                result.Record = xmlDocument;

            }
        }
        catch (Exception ex)
        {
            result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            result.ErrorType = ex.GetType().ToString();
        }

        return result;
    }

}
