﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ElintezesAdatok.ascx.cs" Inherits="eRecordComponent_ElintezesAdatok" %>

<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox" TagPrefix="uc" %>

<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc" %>
<%@ Register Src="~/Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="OnkormanyzatUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <%-- HiddenFields --%>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <%-- /HiddenFields --%>
            <eUI:eFormPanel ID="OnkormanyzatAdatokForm" runat="server">
                   <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr id="trIdotartam" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIdotartam" runat="server" Text="<%$Forditas:labelIdotartam|Időtartam:%>" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <div class="DisableWrap">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <uc:CalendarControl ID="IdotartamKezd" runat="server" Validate="true" TimeVisible="true" ReadOnly="true"/>
                                                </td>
                                                <td style="width:20px; text-align:center">
                                                    <span>-</span>
                                                </td>
                                                <td>
                                                    <uc:CalendarControl ID="IdotartamVege" runat="server" Validate="true" TimeVisible="true" ReadOnly="true"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr  id="trIgazoloIrat" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIgazoloIrat" runat="server" Text="Igazoló irat nyilvántartási száma:"/>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:RequiredTextBox ID="IgazoloIrat" runat="server" Validate="true"  ReadOnly="true" />
                                </td>
                            </tr>
                            <tbody>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelEredetiHatarido" runat="server" Text="Eredeti határidő:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:Label ID="EredetiHatarido" runat="server" Text="-" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelHataridoMeghosszabbitas" runat="server" Text="Határidő meghosszabbítás:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:Label ID="HataridoMeghosszabbitas" runat="server" Text="-" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelHataridoTullepes" runat="server" Text="Határidő-túllépés:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:Label ID="HataridoTullepes" runat="server" Text="-" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelVisszafizetesJogcime" runat="server" Text="Visszafizetés jogcíme:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="VisszafizetesJogcime" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelVisszafizetesOsszege" runat="server" Text="Visszafizetés összege (Ft):" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="VisszafizetesOsszege" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelHatarozatSzama" runat="server" Text="Határozat száma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredTextBox ID="HatarozatSzama" runat="server" Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelVisszafizetesCimzettje" runat="server" Text="Visszafizetés címzettje:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:EditablePartnerTextBox ID="VisszafizetesCimzettje" runat="server" Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelHatarido" runat="server" Text="Határidő:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:CalendarControl ID="Hatarido" runat="server" Validate="false" />
                            </td>
                        </tr>
                            </tbody>
                        </tbody>
                    </table>
                   <div style="padding:5px;">
                   <%-- TabFooter --%>
                   <uc:TabFooter ID="TabFooter1" runat="server" />
                   <%-- /TabFooter --%>
                   </div>
            </eUI:eFormPanel>
        </asp:Panel>
        <br />
        <asp:Panel ID="PanelFeladatok" runat="server" Visible="true" Style="border-left: #e6e6e6 1px solid;">
            <asp:GridView ID="ElintezesFeljegyzesekGridView" runat="server" CellPadding="0" CellSpacing="0"
                BorderWidth="0" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" Width="100%"
                AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                <HeaderStyle CssClass="GridViewHeaderStyle" />
                <Columns>
                    <asp:BoundField DataField="Leiras" HeaderText="Megjegyzés" SortExpression="EREC_HataridosFeladatok.Leiras">
                        <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                    </asp:BoundField>
                </Columns>
                <PagerSettings Visible="False" />
            </asp:GridView>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>