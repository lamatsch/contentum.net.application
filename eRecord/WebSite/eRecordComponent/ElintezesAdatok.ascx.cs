﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_ElintezesAdatok : System.Web.UI.UserControl
{
    const string kcs_VISSZAFIZETES_JOGCIME = "VISSZAFIZETES_JOGCIME";

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;

    public string Command = "";

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    //private Boolean _Load = false;

    private void SetViewControls()
    {
        VisszafizetesJogcime.ReadOnly = true;
        VisszafizetesOsszege.ReadOnly = true;
        HatarozatSzama.ReadOnly = true;
        VisszafizetesCimzettje.ReadOnly = true;
        Hatarido.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            //FormHeader.TemplateObjectType = typeof(EREC_IraIratok);
            //FormHeader.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);

        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        LoadhataridosFeladatok();
    }

    public void ReLoadTab()
    {
        ReLoadTab(true);
    }

    private void ReLoadTab(bool loadRecord)
    {
        //nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord betöltése

                        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page);
                        execParam.Record_Id = ParentId;

                        Result result = service.Get(execParam);
                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            EREC_IraIratok irat = (EREC_IraIratok)result.Record;
                            LoadComponentsFromBusinessObject(irat);
                        }
                        else
                        {
                            // nem sikerült lekérdezni
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();
                        }

                    }
                }
            }


            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter1.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    #region BUSINESS OBJECT
    private void LoadComponentsFromBusinessObject(EREC_IraIratok erec_IraIratok)
    {
        IdotartamKezd.Text = erec_IraIratok.UzemzavarKezdete;
        IdotartamVege.Text = erec_IraIratok.UzemzavarVege;
        IgazoloIrat.Text = erec_IraIratok.UzemzavarIgazoloIratSzama;

        try
        {
            Iratok.Hatarido iratHatarido = new Iratok.Hatarido(Page, erec_IraIratok);
            EredetiHatarido.Text = iratHatarido.GetHataridoIdoTartam();
            HataridoMeghosszabbitas.Text = iratHatarido.GetHataridoMeghosszabitasText();
            HataridoTullepes.Text = iratHatarido.GetHataridoTullepessText();
        }
        catch (Exception ex)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }

        VisszafizetesJogcime.FillAndSetSelectedValue(kcs_VISSZAFIZETES_JOGCIME, erec_IraIratok.VisszafizetesJogcime, true, EErrorPanel1);
        VisszafizetesOsszege.Text = erec_IraIratok.VisszafizetesOsszege;
        HatarozatSzama.Text = erec_IraIratok.VisszafizetesHatarozatSzama;

        //LZS - BUG_6729
        if (string.IsNullOrEmpty(erec_IraIratok.Partner_Id_VisszafizetesCimzet))
        {
            VisszafizetesCimzettje.Text = erec_IraIratok.Partner_Nev_VisszafizetCimzett;
        }
        else
        {
            VisszafizetesCimzettje.Id_HiddenField = erec_IraIratok.Partner_Id_VisszafizetesCimzet;
            VisszafizetesCimzettje.SetPartnerTextBoxById(EErrorPanel1);
        }

       
        Hatarido.Text = erec_IraIratok.VisszafizetesHatarido;

        Record_Ver_HiddenField.Value = erec_IraIratok.Base.Ver;
    }

    private EREC_IraIratok GetBusinessObjectFromComponents()
    {
        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        erec_IraIratok.VisszafizetesJogcime = VisszafizetesJogcime.SelectedValue;
        erec_IraIratok.Updated.VisszafizetesJogcime = pageView.GetUpdatedByView(VisszafizetesJogcime);

        erec_IraIratok.VisszafizetesOsszege = VisszafizetesOsszege.Text;
        erec_IraIratok.Updated.VisszafizetesOsszege = pageView.GetUpdatedByView(VisszafizetesOsszege);

        erec_IraIratok.VisszafizetesHatarozatSzama = HatarozatSzama.Text;
        erec_IraIratok.Updated.VisszafizetesHatarozatSzama = pageView.GetUpdatedByView(HatarozatSzama);

        erec_IraIratok.Partner_Id_VisszafizetesCimzet = VisszafizetesCimzettje.Id_HiddenField;
        erec_IraIratok.Updated.Partner_Id_VisszafizetesCimzet = pageView.GetUpdatedByView(VisszafizetesCimzettje);

        erec_IraIratok.VisszafizetesHatarido = Hatarido.Text;
        erec_IraIratok.Updated.VisszafizetesHatarido = pageView.GetUpdatedByView(Hatarido);

        erec_IraIratok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IraIratok.Base.Updated.Ver = true;

        return erec_IraIratok;
    }

    #endregion

    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok állítása

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion



        #region Form komponensek (TextBoxok,...) állítása


        // A gombok láthatósága alapból a Modify esetre van beállítva,
        // View-nál pluszban még azokat kell letiltani, amik módosításnál látszanának
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        #endregion

    }

    private void Load_ComponentSelectModul()
    {
    }

    private void TabFooterButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
          e.CommandName == CommandName.SaveAndClose)
        {
            if (true)
            {
                bool voltHiba = false;

                switch (Command)
                {
                    case CommandName.New:
                        {
                            // New nem lehetséges
                            return;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                                execparam.Record_Id = ParentId;

                                EREC_IraIratok erec_IraIratok = GetBusinessObjectFromComponents();

                                Result result = service.Update(execparam, erec_IraIratok);
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    ReLoadTab(false);
                                    return;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }
                            }


                            break;

                        }
                }   // switch
            }
            else
            {
                // TODO: right check? UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        //if (IsPostBack)
        //{
        //    if (e.CommandName == CommandName.LoadTemplate)
        //    {
        //        _Load = true;

        //        LoadComponentsFromTemplate((EREC_IraOnkormAdatok)FormHeader.TemplateObject);

        //        _Load = false;
        //    }
        //    else if (e.CommandName == CommandName.NewTemplate)
        //    {
        //        FormHeader.NewTemplate(GetTemplateObjectFromComponents());
        //    }
        //    else if (e.CommandName == CommandName.SaveTemplate)
        //    {
        //        FormHeader.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        //    }
        //    else if (e.CommandName == CommandName.InvalidateTemplate)
        //    {
        //        // FormTemplateLoader lekezeli           
        //    }
        //}

    }

    #region ELINTEZETT FELJEGYZES
    /// <summary>
    /// Irat elintezesi feljegyzesei.
    /// </summary>
    private void LoadhataridosFeladatok()
    {
        if (string.IsNullOrEmpty(ParentId))
            return;

        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_HataridosFeladatokSearch src = new EREC_HataridosFeladatokSearch();

        src.Tipus.Value = KodTarak.FELADAT_TIPUS.ElintezesMegjegyzes;
        src.Tipus.Operator = Query.Operators.equals;

        src.Obj_Id.Value = ParentId;
        src.Obj_Id.Operator = Query.Operators.equals;

        Result result = svc.GetAllWithExtension(xpm, src);
        ElintezesFeljegyzesekGridView.DataSource = result.Ds.Tables[0];
        ElintezesFeljegyzesekGridView.DataBind();
    }
    #endregion
}