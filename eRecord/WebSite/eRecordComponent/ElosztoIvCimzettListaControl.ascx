﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="ElosztoIvCimzettListaControl.ascx.cs"
    Inherits="eRecordComponent_ElosztoIvCimzettListaControl" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<ajaxToolkit:TabContainer ID="TabContainerEIV" runat="server" Width="100%">
    <ajaxToolkit:TabPanel ID="tabPaneleBeadvanyCsatolmanyok" runat="server" TabIndex="0">
        <HeaderTemplate>
            <asp:Label ID="LabelCimlista" runat="server" Text="CímzettLista megadása vagy cseréje"></asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgressA" runat="server" DisplayAfter="30" AssociatedUpdatePanelID="UpdatePanelCimLista">
                <ProgressTemplate>
                    <div class="updateProgress" id="UpdateProgressPanelA" runat="server">
                        <eUI:eFormPanel ID="EFormPanelProgress" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="" />
                                    </td>
                                    <td class="updateProgressText">
                                        <asp:Label ID="Label_progress" runat="server" Text="Feldolgozás folyamatban..."></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                    </div>
                    <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtenderA" runat="server" TargetControlID="UpdateProgressPanelA"
                        VerticalSide="Middle" HorizontalSide="Center">
                    </ajaxToolkit:AlwaysVisibleControlExtender>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanelCimLista" runat="server">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="PanelContainer" HorizontalAlign="Left">
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUploadeElosztoIv" runat="server" HorizontalAlign="Left" ToolTip="Címzettlista fájl kiválasztása" />
                                    <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="FileUploadeElosztoIv"
                                        ErrorMessage="Csak XLSX fájl használható !" Display="Dynamic"
                                        ValidationExpression="(.+\.xlsx|.+\.XLSX|.+\.Xlsx)" />
                                    &nbsp;
                        <asp:ImageButton ID="ButtonUploadCimLista" runat="server"
                            ImageUrl="~/images/hu/trapezgomb/feltoltes_trap.jpg"
                            onmouseover="swapByName(this.id,'feltoltes_trap2.jpg')"
                            onmouseout="swapByName(this.id,'feltoltes_trap.jpg')"
                            OnClick="ButtonUpload_Click" AlternateText="Feltöltés"
                            ToolTip="Címzettlista beolvasása"
                            Visible="True" CausesValidation="false" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonTemplate" runat="server"
                                        ImageUrl="~/images/hu/ikon/ms_excel_template.png"
                                        onmouseover="swapByName(this.id,'ms_excel_template.png')"
                                        onmouseout="swapByName(this.id,'ms_excel_template.png')"
                                        AlternateText="Sablon letöltése"
                                        ToolTip="A címzett listás iktatáshoz használt excel sablon letöltése"
                                        Visible="True" CausesValidation="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="max-width: 800px; max-height: 500px; overflow-x: auto; white-space: nowrap;">
                                        <asp:GridView ID="GridViewPreview" runat="server" CellPadding="3"
                                            BorderWidth="1px" AllowPaging="False"
                                            AllowSorting="True" DataKeyNames="Id" EnableModelValidation="True">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" AutoPostBack="true" Checked="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" Visible="False"></asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ButtonSaveElosztoIv" runat="server"
                                        ImageUrl="~/images/hu/ovalgomb/mentes.gif"
                                        onmouseover="swapByName(this.id,'mentes2.gif')"
                                        onmouseout="swapByName(this.id,'mentes.gif')"
                                        OnClick="ButtonSaveElosztoIv_Click" CommandName="Save"
                                        ToolTip="Kiválasztott tételek mentése" OnClientClick="showProgress();" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ButtonUploadCimLista" />
                    <asp:PostBackTrigger ControlID="ButtonSaveElosztoIv" />
                    <asp:PostBackTrigger ControlID="ImageButtonTemplate" />
                </Triggers>
            </asp:UpdatePanel>
            <script type="text/javascript">
                ///Display progress on save button
                function showProgress() {
                    var state = document.getElementById('<%= UpdateProgressA.ClientID %>').style.display;
                    if (state == 'block') {
                        document.getElementById('<%= UpdateProgressA.ClientID %>').style.display = 'none';
                    }
                    else {
                        document.getElementById('<%= UpdateProgressA.ClientID %>').style.display = 'block';
                    }
                }
            </script>

        </ContentTemplate>
    </ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer>
