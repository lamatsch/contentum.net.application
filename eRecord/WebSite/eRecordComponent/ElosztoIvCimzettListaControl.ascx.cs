﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eUtility.ExcelImport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class eRecordComponent_ElosztoIvCimzettListaControl : System.Web.UI.UserControl
{
    public delegate void Saved(string id, string name);
    public event Saved OnSaved;

    #region PROPERTIES

    public string ElosztoIvNev
    {
        get
        {
            string prefix = ViewState == null || ViewState[ViewStateKeyFilename] == null
                ? "CIMZETT_LISTA"
                : ViewState[ViewStateKeyFilename].ToString();

            return string.Format(prefix + "_{0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss"));
        }
    }

    private const string ViewStateKeyElosztoIvCimzettLista = "ElosztoIvCimzettListaDataTable";
    private const string ViewStateKeyFilename = "ElosztoIvCimzettFilename";
    private const string ConstFileExtension = "XLSX";

    UI ui = new UI();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            SetVisibility(false);

        string externalLink = "DownloadSablonok.ashx";
        ImageButtonTemplate.OnClientClick = "window.open('" + externalLink + "'); return false;";
    }
    public void SetCotrolVisibility(bool visible)
    {
        TabContainerEIV.Visible = visible;
    }
    private void SetVisibility(bool visible)
    {
        GridViewPreview.Visible = visible;
        ButtonSaveElosztoIv.Visible = visible;
    }
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        PreviewProcedure();
    }

    protected void ButtonSaveElosztoIv_Click(object sender, EventArgs e)
    {
        SaveProcedure();
    }

    /// <summary>
    /// Main procedure
    /// </summary>
    private void PreviewProcedure()
    {
        ResetControls();

        HttpFileCollection uploadedFiles = Request.Files;
        if (uploadedFiles == null)
        {
            ShowErrorMessage(Resources.Error.MessageNoFile);
            return;
        }

        List<HttpPostedFile> files = ExtractPostedFiles(uploadedFiles);
        if (files == null || files.Count < 1)
        {
            ShowErrorMessage(Resources.Error.MessageNoUploadedFile);
            return;
        }
        else if (files.Count > 1)
        {
            ShowErrorMessage(Resources.Error.MessageTooMuchFile);
            return;
        }
        //        #region DEBUG
        //#if DEBUG
        //        foreach (HttpPostedFile item in files)
        //        {
        //            LabelMessage.Text += "File Name: " + item.FileName + "<br>";
        //            LabelMessage.Text += "File Size: " + item.ContentLength + "<br><br>";
        //        }
        //#endif
        //        #endregion

        //Dictionary<string, byte[]> rawFiles = ExtractFiles(files);
        Dictionary<string, byte[]> rawFiles = ExtractFiles();
        foreach (var item in rawFiles)
        {
            // BUG #5030
            ViewState[ViewStateKeyFilename] = item.Key;

            var stream = new MemoryStream(item.Value);
            DataTable dt = ExcelXlsxImport.GetDataTableFromExcel(EnumExcelTypes.CimzettListaIktatas, stream);
            if (dt == null)
                continue;

            List<ExcelClassCimzettListaIktatas> listAll = HelperCimzettLista.GetCimzettListaIktatasFromDataTable(dt);

            if (listAll != null && listAll.Count > 0)
            {
                List<ExcelClassCimzettListaIktatas> listAllValid = new List<ExcelClassCimzettListaIktatas>();
                bool hasInvalidRows = false;
                foreach (ExcelClassCimzettListaIktatas cimzett in listAll)
                {
                    if (cimzett.IsValid())
                        listAllValid.Add(cimzett);
                    else
                        hasInvalidRows = true;
                }
                if (hasInvalidRows)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "EIV.CTRL.HASINVALIDROWS", "javascript:alert('" + Resources.Error.MessageErrorInData + "');", true);

                SetVisibility(true);
                GridViewPreview.DataSource = listAllValid;
                GridViewPreview.DataBind();

                ViewState[ViewStateKeyElosztoIvCimzettLista] = listAllValid;
            }
            break; /*NOW ONLY 1 FILE*/
        }
    }
    /// <summary>
    /// SaveProcedure
    /// </summary>
    private void SaveProcedure()
    {
        List<ExcelClassCimzettListaIktatas> selected = GetSelectedItems();
        if (selected == null || selected.Count < 1)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "EIV.CTRL.NoSelectedItem", "javascript:alert('" + Resources.Error.MessageNoSelectedItem + "');", true);
            return;
        }
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        // elosztóív
        EREC_IraElosztoivek iv = GenerateElosztoIv(execParam);
        //Result resultPldHelye = GetPeldanyHelye(selected[0]);
        //if (resultPldHelye != null && resultPldHelye.Ds.Tables.Count > 0 && resultPldHelye.Ds.Tables[0].Rows.Count > 0)
        //    iv.Csoport_Id_Tulaj = resultPldHelye.Ds.Tables[0].Rows[0]["Id"].ToString();

        var elosztoivekService = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        Result resultIv = elosztoivekService.Insert(execParam, iv);
        if (resultIv.IsError)
        {
            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.SaveProcedure.elosztoivekService.Insert", execParam, resultIv);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultIv);
            return;
        }

        // tételek
        int tetelSorszam = 0;
        var tetelek = selected.Select(s => GenerateElosztoIvTetel(iv.Id, ++tetelSorszam, s)).ToArray();

        var elosztoivTetelekService = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
        var resultTetel = elosztoivTetelekService.InsertTomeges(execParam, tetelek);
        if (resultTetel.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultTetel);
            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.SaveProcedure.elosztoivTetelekService.Insert", execParam, resultIv);
        }

        if (OnSaved != null)
            OnSaved(iv.Id, iv.NEV);

        ResetControls();
        //Response.Redirect(Request.RawUrl);
    }
    /// <summary>
    /// Reset controls
    /// </summary>
    private void ResetControls()
    {
        EErrorPanel1.Visible = false;
        EErrorPanel1.Header = null;
        EErrorPanel1.Body = null;

        ViewState[ViewStateKeyElosztoIvCimzettLista] = null;
        ViewState[ViewStateKeyFilename] = null;

        GridViewPreview.DataSource = null;
        GridViewPreview.DataBind();

        SetVisibility(false);
    }
    //private Result GetPeldanyHelye(ExcelClassCimzettListaIktatas item)
    //{
    //    Result result = null;
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //    Contentum.eAdmin.Service.KRT_FelhasznalokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
    //    KRT_FelhasznalokSearch src = new KRT_FelhasznalokSearch();
    //    src.Nev.Value = item.Peldany_helye;
    //    src.Nev.Operator = Query.Operators.equals;
    //    src.TopRow = 1;
    //    result = service.GetAll(execParam, src);
    //    if (!string.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //        Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetPeldanyHelye ", execParam, result);
    //    }
    //    return result;

    //}
    /// <summary>
    /// GenerateElosztoIv
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    private EREC_IraElosztoivek GenerateElosztoIv(ExecParam execParam)
    {
        EREC_IraElosztoivek iv = new EREC_IraElosztoivek();
        iv.Updated.SetValueAll(false);
        iv.Base.Updated.SetValueAll(false);

        iv.Id = Guid.NewGuid().ToString();
        iv.Updated.Id = true;

        iv.NEV = ElosztoIvNev;
        iv.Updated.NEV = true;

        iv.Fajta = KodTarak.ELOSZTOIV_FAJTA.Cimzettlista;
        iv.Updated.Fajta = true;

        iv.Kod = "0";
        iv.Updated.Kod = true;

        iv.Ev = DateTime.Now.Year.ToString();
        iv.Updated.Ev = true;

        iv.Csoport_Id_Tulaj = execParam.Felhasznalo_Id;
        iv.Updated.Csoport_Id_Tulaj = true;

        iv.ErvKezd = DateTime.Now.ToString();
        iv.Updated.ErvKezd = true;
        return iv;
    }

    /// <summary>
    /// GenerateElosztoIvTetel
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="elosztoIv"></param>
    /// <param name="sorszam"></param>
    /// <param name="parameter"></param>
    /// <returns></returns>
    private EREC_IraElosztoivTetelek GenerateElosztoIvTetel(string elosztoIv, int sorszam, ExcelClassCimzettListaIktatas parameter)
    {
        EREC_IraElosztoivTetelek tetel = new EREC_IraElosztoivTetelek();
        tetel.Updated.SetValueAll(false);
        tetel.Base.Updated.SetValueAll(false);

        tetel.Id = Guid.NewGuid().ToString();
        tetel.Updated.Id = true;

        tetel.ElosztoIv_Id = elosztoIv;
        tetel.Updated.ElosztoIv_Id = true;

        tetel.Sorszam = sorszam.ToString();
        tetel.Updated.Sorszam = true;

        tetel.Kuldesmod = parameter.Expedialas_Modja;
        tetel.Updated.Kuldesmod = true;

        tetel.NevSTR = parameter.Cimzett_neve;
        tetel.Updated.NevSTR = true;

        // BUG_15979
        KRT_Cimek cimzettKrtCim = GetCimzettCim(parameter);
        if (cimzettKrtCim != null)
        {
            tetel.Cim_Id = cimzettKrtCim.Id;
            tetel.Updated.Cim_Id = true;

            tetel.CimSTR = Contentum.eUtility.CimUtility.MergeCim(cimzettKrtCim, true);
            tetel.Updated.CimSTR = true;
        }

        tetel.Base.Note = parameter.GetAsXml();
        tetel.Base.Updated.Note = true;

        tetel.ErvKezd = DateTime.Now.ToString();
        tetel.Updated.ErvKezd = true;
        return tetel;
    }

    // BUG_15979
    public KRT_Cimek GetCimzettCim(ExcelClassCimzettListaIktatas cim)
    {
        Contentum.eAdmin.Service.KRT_CimekService cimekService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CimekService();
        KRT_CimekSearch cimSearch = GetKRT_Cimek_Search(cim);

        if (cimSearch == null)
        {
            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim hibás keresendő cím: " + cim.ToStringCimzett());
            ShowErrorMessage("A címzett címe érvénytelen: " + cim.ToStringCimzett());
            return null;
        }

        ExecParam _execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result res = cimekService.GetAll(_execParam, cimSearch);
        if (res.IsError)
        {
            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim cím: " + cim.ToStringCimzett());
            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim ", _execParam, res);
            ShowErrorMessage("A címzett címe érvénytelen: " + cim.ToStringCimzett());
            return null;
        }

        string cimId;
        if (res.HasData())
        {
            cimId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
        }
        else // új cím felvétele feloldással
        {
            KRT_Cimek krt_cim = GetKRT_Cimek(cim);

            if (krt_cim == null)
            {
                Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim hibás új cím: " + cim.ToStringCimzett());
                ShowErrorMessage("A címzett címe érvénytelen: " + cim.ToStringCimzett());
                return null;
            }

            Result resultInsert = cimekService.InsertWithFKResolution(_execParam, krt_cim);
            if (resultInsert.IsError)
            {
                Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim cím felvétel hiba: " + cim.ToStringCimzett(), _execParam, resultInsert);
                ShowErrorMessage("A cím felvételekor hiba merült fel: " + cim.ToStringCimzett());
                return null;
            }

            cimId = resultInsert.Uid;
        }

        // cím lekérése
        if (!String.IsNullOrEmpty(cimId))
        {
            var execParam = _execParam.Clone();
            execParam.Record_Id = cimId;
            var resultGet = cimekService.Get(execParam);

            if (resultGet.IsError)
            {
                Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.GetCimzettCim cím felvétel hiba: " + cim.ToStringCimzett(), _execParam, resultGet);
                ShowErrorMessage("A cím lekérésekor hiba merült fel: " + cim.ToStringCimzett());
                return null;
            }

            return resultGet.Record as KRT_Cimek;
        }

        return null;
    }

    // BUG_15979
    public KRT_CimekSearch GetKRT_Cimek_Search(ExcelClassCimzettListaIktatas cim)
    {
        var krt_CimekSearch = new KRT_CimekSearch();

        krt_CimekSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
        krt_CimekSearch.ErvKezd.AndGroup("100");
        krt_CimekSearch.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
        krt_CimekSearch.ErvVege.AndGroup("100");

        if (cim.Cim_Tipus == KodTarak.Cim_Tipus.Postai)
        {
            krt_CimekSearch.OrszagNev.FilterIfNotEmpty(cim.Cimzett_Orszag);
            krt_CimekSearch.IRSZ.FilterIfNotEmpty(cim.Cimzett_IRSZ);
            krt_CimekSearch.TelepulesNev.FilterIfNotEmpty(cim.Cimzett_Telepules);
            krt_CimekSearch.KozteruletNev.FilterIfNotEmpty(cim.Cimzett_Kozterulet_Neve);
            krt_CimekSearch.KozteruletTipusNev.FilterIfNotEmpty(cim.Cimzett_Kozterulet_Tipusa);
            krt_CimekSearch.Hazszam.FilterIfNotEmpty(cim.Cimzett_Hazszam1);
            krt_CimekSearch.Hazszamig.FilterIfNotEmpty(cim.Cimzett_Hazszam2);
            krt_CimekSearch.HazszamBetujel.FilterIfNotEmpty(cim.Cimzett_Hazszam_Betujele);
            krt_CimekSearch.Lepcsohaz.FilterIfNotEmpty(cim.Cimzett_Lepcsohaz);
            krt_CimekSearch.Szint.FilterIfNotEmpty(cim.Cimzett_Emelet);
            krt_CimekSearch.Ajto.FilterIfNotEmpty(cim.Cimzett_Ajto);
            krt_CimekSearch.AjtoBetujel.FilterIfNotEmpty(cim.Cimzett_Ajto_Betujele);
            krt_CimekSearch.Tipus.Filter(cim.Cim_Tipus);
        }
        else if ((cim.Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu || cim.Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz) && !String.IsNullOrEmpty(cim.Hivatali_Kapus_Azonosito))
        {
            krt_CimekSearch.CimTobbi.Filter(cim.Hivatali_Kapus_Azonosito);
        }
        else
        {
            krt_CimekSearch = null;
        }

        return krt_CimekSearch;
    }

    // BUG_15979
    public KRT_Cimek GetKRT_Cimek(ExcelClassCimzettListaIktatas cim)
    {
        KRT_Cimek krt_Cimek = new KRT_Cimek();

        krt_Cimek.Tipus = cim.Cim_Tipus;
        krt_Cimek.Kategoria = "K";

        krt_Cimek.Updated.SetValueAll(true);
        krt_Cimek.Base.Updated.SetValueAll(true);

        if (cim.Cim_Tipus == KodTarak.Cim_Tipus.Postai)
        {
            krt_Cimek.OrszagNev = cim.Cimzett_Orszag;
            krt_Cimek.IRSZ = cim.Cimzett_IRSZ;
            krt_Cimek.TelepulesNev = cim.Cimzett_Telepules;
            krt_Cimek.KozteruletNev = cim.Cimzett_Kozterulet_Neve;
            krt_Cimek.KozteruletTipusNev = cim.Cimzett_Kozterulet_Tipusa;
            krt_Cimek.Hazszam = cim.Cimzett_Hazszam1;
            krt_Cimek.Hazszamig = cim.Cimzett_Hazszam2;
            krt_Cimek.HazszamBetujel = cim.Cimzett_Hazszam_Betujele;
            krt_Cimek.Lepcsohaz = cim.Cimzett_Lepcsohaz;
            krt_Cimek.Szint = cim.Cimzett_Emelet;
            krt_Cimek.Ajto = cim.Cimzett_Ajto;
            krt_Cimek.AjtoBetujel = cim.Cimzett_Ajto_Betujele;
        }
        else if ((cim.Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu || cim.Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz) && !String.IsNullOrEmpty(cim.Hivatali_Kapus_Azonosito))
        {
            krt_Cimek.CimTobbi = cim.Hivatali_Kapus_Azonosito;
        }
        else 
        {
            krt_Cimek = null;
        }


        return krt_Cimek;
    }

    //private string AddOrGetEIvPartner(ExcelClassCimzettListaIktatas item)
    //{
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //    string resultId = null;

    //    #region GET IF EXIST
    //    Contentum.eAdmin.Service.KRT_PartnerekService partnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
    //    KRT_PartnerekSearch src = new KRT_PartnerekSearch();
    //    src.Nev.Filter(item.Cimzett_neve);
    //    src.TopRow = 1;
    //    Result resultPartnerGet = partnerekService.GetAll(execParam, src);
    //    if (!string.IsNullOrEmpty(resultPartnerGet.ErrorCode))
    //    {
    //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultPartnerGet);
    //        Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.SaveProcedure.AddOrGetEIvPartner ", execParam, resultPartnerGet);
    //        return null;
    //    }
    //    #endregion

    //    if (resultPartnerGet.Ds.Tables.Count < 1 || resultPartnerGet.Ds.Tables[0].Rows.Count < 1)
    //    {
    //        #region ADD
    //        KRT_Partnerek newPartner = GeneratePartner(item);
    //        Result resultPartnerInsert = partnerekService.Insert(execParam, newPartner);
    //        if (!string.IsNullOrEmpty(resultPartnerGet.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultPartnerGet);
    //            Logger.Error("eRecordComponent_ElosztoIvCimzettListaControl.SaveProcedure.PartnerekInsert ", execParam, resultPartnerInsert);
    //            return null;
    //        }
    //        resultId = resultPartnerInsert.Uid;
    //        #endregion
    //    }
    //    else
    //    {
    //        #region EXIST
    //        resultId = resultPartnerGet.Ds.Tables[0].Rows[0]["Id"].ToString();
    //        #endregion
    //    }
    //    return resultId;
    //}
    ///// <summary>
    ///// AddPartner
    ///// </summary>
    ///// <param name="item"></param>
    ///// <returns></returns>
    //private static KRT_Partnerek GeneratePartner(ExcelClassCimzettListaIktatas item)
    //{
    //    KRT_Partnerek newPartner = new KRT_Partnerek();
    //    newPartner.Updated.SetValueAll(false);
    //    newPartner.Base.Updated.SetValueAll(false);

    //    newPartner.Nev = item.Cimzett_neve;
    //    newPartner.Updated.Nev = true;

    //    newPartner.Tipus = KodTarak.Partner_Tipus.Szervezet;
    //    newPartner.Updated.Tipus = true;

    //    return newPartner;
    //}

    private List<ExcelClassCimzettListaIktatas> GetSelectedItems()
    {
        if (ViewState[ViewStateKeyElosztoIvCimzettLista] == null)
            return null;
        List<string> selectedIds = ui.GetGridViewSelectedRows(GridViewPreview, EErrorPanel1, ErrorUpdatePanel);

        if (selectedIds == null || selectedIds.Count < 1)
            return null;

        List<ExcelClassCimzettListaIktatas> listAll = (List<ExcelClassCimzettListaIktatas>)ViewState[ViewStateKeyElosztoIvCimzettLista];
        List<ExcelClassCimzettListaIktatas> result = new List<ExcelClassCimzettListaIktatas>();

        int idx;
        foreach (string itemId in selectedIds)
        {
            idx = listAll.IndexOf(new ExcelClassCimzettListaIktatas() { Id = itemId });
            if (idx < 0)
                continue;
            result.Add(listAll[idx]);
        }

        return result;
    }
    /// <summary>
    /// Extract attached files from collection, and filter by type.
    /// </summary>
    /// <param name="collection"></param>
    /// <returns></returns>
    private List<HttpPostedFile> ExtractPostedFiles(HttpFileCollection collection)
    {
        if (collection.Count < 1)
            return null;

        List<HttpPostedFile> ret = new List<HttpPostedFile>();
        for (int i = 0; i < collection.Count; i++)
        {
            HttpPostedFile userPostedFile = collection[i];
            if (userPostedFile.FileName.ToUpper().EndsWith(ConstFileExtension))
            {
                ret.Add(userPostedFile);
            }
        }
        return ret;
    }
    private Dictionary<string, byte[]> ExtractFiles(List<HttpPostedFile> files)
    {
        Dictionary<string, byte[]> result = new Dictionary<string, byte[]>();

        foreach (HttpPostedFile file in files)
        {
            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            result.Add(file.FileName, fileData);
        }
        return result;
    }
    private Dictionary<string, byte[]> ExtractFiles()
    {
        Dictionary<string, byte[]> result = new Dictionary<string, byte[]>();
        if (FileUploadeElosztoIv.HasFile)
        {
            Stream fs = FileUploadeElosztoIv.PostedFile.InputStream;
            BinaryReader br = new BinaryReader(fs);
            byte[] bytes = br.ReadBytes((Int32)fs.Length);
            result.Add(FileUploadeElosztoIv.FileName, bytes);
        }
        return result;
    }

    private void ShowErrorMessage(string msg)
    {
        var result = new Result();
        result.ErrorMessage = msg;
        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    }
}