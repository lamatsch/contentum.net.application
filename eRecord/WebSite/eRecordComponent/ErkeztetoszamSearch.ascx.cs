using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

public partial class Component_ErkeztetoszamSearch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        if (Enabled)
        {
            masol_ImageButton.OnClientClick = "JavaScript: document.getElementById('" + ErkeztetoSzamIg_textBox.ClientID + "').value = " +
                " document.getElementById('" + ErkeztetoSzamTol_textBox.ClientID + "').value; " +
                " document.getElementById('" + ErkeztetoSzamEvIg_textBox.ClientID + "').value = " +
                " document.getElementById('" + ErkeztetoSzamEvTol_textBox.ClientID + "').value; " +
            "return false;";
            
        }

        JavaScripts.RegisterOnValidatorOverClientScript(Page);

    }

    /// <summary>
    /// SzamTol �s SzamIg be�ll�t�sa a megadott keres�si mez�nek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        if (!String.IsNullOrEmpty(ErkeztetoSzamTol) || !String.IsNullOrEmpty(ErkeztetoSzamIg))
        {
            if (String.IsNullOrEmpty(ErkeztetoSzamTol))
            {
                SearchField.Value = ErkeztetoSzamIg;
                SearchField.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(ErkeztetoSzamIg))
            {
                SearchField.Value = ErkeztetoSzamTol;
                SearchField.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                SearchField.Value = ErkeztetoSzamTol;
                SearchField.ValueTo = ErkeztetoSzamIg;
                SearchField.Operator = Query.Operators.between;
            }
        }
    }

    /// <summary>
    /// TextBoxok be�ll�t�sa a keres�si objektum mez�je alapj�n
    /// </summary>
    /// <param name="SearchField"></param>
    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        if (SearchField.Operator == Query.Operators.lessorequal)
        {
            ErkeztetoSzamIg = SearchField.Value;
        }
        else if (SearchField.Operator == Query.Operators.greaterorequal)
        {
            ErkeztetoSzamTol = SearchField.Value;
        }
        else
        {
            ErkeztetoSzamTol = SearchField.Value;
            ErkeztetoSzamIg = SearchField.ValueTo;
        }
    }

    /// <summary>
    /// true, ha mindk�t TextBox �res, egy�bk�nt false
    /// </summary>
    /// <returns></returns>
    public bool BothTextBoxesAreEmpty()
    {
        if (String.IsNullOrEmpty(ErkeztetoSzamTol) && String.IsNullOrEmpty(ErkeztetoSzamIg))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region public properties

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            //Enable_ErvKezd = _Enabled;
            //Enable_ErvVege = _Enabled;
        }
    }

    //public bool Validate
    //{
    //    set { Validator1.Enabled = value; }
    //    get { return Validator1.Enabled; }
    //}

    public String ErkeztetoSzamTol
    {
        get { return ErkeztetoSzamTol_textBox.Text; }
        set { ErkeztetoSzamTol_textBox.Text = value; }
    }

    public TextBox ErkeztetoSzamTol_TextBox
    {
        get { return ErkeztetoSzamTol_textBox; }
    }


    public String ErkeztetoSzamIg
    {
        get { return ErkeztetoSzamIg_textBox.Text; }
        set { ErkeztetoSzamIg_textBox.Text = value; }
    }

    public TextBox ErkeztetoSzamIg_TextBox
    {
        get { return ErkeztetoSzamIg_textBox; }
    }


    public String ErkeztetoSzamEvTol
    {
        get { return ErkeztetoSzamEvTol_textBox.Text; }
        set { ErkeztetoSzamEvTol_textBox.Text = value; }
    }

    public TextBox ErkeztetoSzamEvTol_TextBox
    {
        get { return ErkeztetoSzamEvTol_textBox; }
    }

    public String ErkeztetoSzamEvIg
    {
        get { return ErkeztetoSzamEvIg_textBox.Text; }
        set { ErkeztetoSzamEvIg_textBox.Text = value; }
    }

    public TextBox ErkeztetoSzamEvIg_TextBox
    {
        get { return ErkeztetoSzamEvIg_textBox; }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        // Lekell tiltani a ClientValidator
        //Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    protected void masol_ImageButton_Click(object sender, ImageClickEventArgs e)
    {

    }
}
