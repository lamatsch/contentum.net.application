﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeladatMasterAdatok.ascx.cs" Inherits="eRecordComponent_FeladatMasterAdatok" %>

<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc1" %>
<%@ Register Src="../Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc2" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc4" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc5" %>
<%@ Register Src="../Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc6" %>

<asp:UpdatePanel runat="server" ID="upFeladatMasterAdatok" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false" Width="100%">
            <asp:HiddenField ID="hfFeladatId" runat="server" />
            <asp:HiddenField ID="hfFeladatVer" runat="server" />
            <asp:HiddenField ID="hfFeladatTipus" runat="server" />
            <asp:HiddenField ID="hfPrioritas" runat="server" />
            <asp:HiddenField ID="hfLezarasPrioritas" runat="server" />
            <asp:HiddenField ID="hfForras" runat="server" />
            <asp:HiddenField ID="hfLetrehozasiIdo" runat="server" />
            <asp:HiddenField ID="hfObjetumTabLoaded" runat="server" />
            <asp:HiddenField ID="hfObjId" runat="server" />
            <asp:HiddenField ID="hfObjType" runat="server" />
            <asp:HiddenField ID="hfObjAzonosito" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:-1px">
                <tr>
                    <td>
                    <eUI:eFormPanel ID="panelForm" runat="server">
                        <asp:UpdatePanel runat="server" ID="upFeladat" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="FeldatForm" runat="server" Visible="false">
                               <table cellpadding="0" cellspacing="0" runat="server" id="MainTable" width="100%">
                                    <tr class="urlapSor">
                                        <%--<td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelTipus" runat="server" Text="Tipus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc4:KodtarakDropDownList ID="ktddTipus" runat="server" ReadOnly="true"/>
                                        </td>--%>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelFunkcio" runat="server" Text="Feladat:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:FunkcioTextBox ID="funkcioTextBox" runat="server" ReadOnly="true"/>
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelPrioritas" runat="server" Text="Prioritás"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="txtPrioritas" runat="server" CssClass="mrUrlapInput" ReadOnly="true" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelKiado" runat="server" Text="Kiadó"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc2:FelhasznaloTextBox runat="server" ID="ftbxKiado" Validate="false" ReadOnly="true" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelFelelos" runat="server" Text="Felelős:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:CsoportTextBox runat="server" ID="cstbxFelelos" Validate="false" ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label2" runat="server" Text="Leírás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:TextBox runat="server" ID="txbLeiras" TextMode="MultiLine" Rows="3" ReadOnly="true"
                                             Width="800px"/>        
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelAllapot" runat="server" Text="Állapot:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc4:KodtarakDropDownList ID="ktddAllapot" runat="server" ReadOnly="true" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIntezkedesiHatarido" runat="server" Text="Intézkedési&nbsp;határidő:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc1:CalendarControl runat="server" ID="ccIntezkedesiHatarido" ReadOnly="true" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelKezdesiIdo" runat="server" Text="Kezdési&nbsp;idő:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc1:CalendarControl runat="server" ID="ccKezdesiIdo" ReadOnly="true" Validate="false" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelLezarasDatuma" runat="server" Text="Lezárás&nbsp;dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc1:CalendarControl runat="server" ID="ccLezarasDatuma" ReadOnly="true" Validate="false" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </eUI:eFormPanel>
                    <asp:Panel ID="panelTableMainCell" runat="server">
                    
                    </asp:Panel>     
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
