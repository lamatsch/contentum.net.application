﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_FeladatMasterAdatok : System.Web.UI.UserControl
{
    UI ui = new UI();

    #region public properties

    public string FeladatId
    {
        get { return hfFeladatId.Value; }
        set { hfFeladatId.Value = value; }
    }

    public string FeleadatVersion
    {
        get
        {
            return hfFeladatVer.Value;
        }
        set
        {
            hfFeladatVer.Value = value;
        }
    }

    public string KiadoNev
    {
        get
        {
            return ftbxKiado.Text;
        }
    }

    public string AllapotNev
    {
        get
        {
            return ktddAllapot.Text;
        }
    }

    //public string TipusNev
    //{
    //    get
    //    {
    //        return ktddTipus.Text;
    //    }
    //}

    public string Tipus
    {
        get
        {
            return hfFeladatTipus.Value;
        }
    }

    public string FunkcioNev
    {
        get
        {
            return funkcioTextBox.Text;
        }
    }

    public string FunkcioBehaviorID
    {
        get
        {
            return funkcioTextBox.BehaviorID;
        }
    }

    public string FelelosNev
    {
        get
        {
            return cstbxFelelos.Text;
        }
    }

    public string LezarasPrioritas
    {
        get
        {
            return hfLezarasPrioritas.Value;
        }
    }

    public Unit LeirasWidth
    {
        get
        {
            return txbLeiras.Width;
        }
        set
        {
            txbLeiras.Width = value;
        }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel;

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return errorPanel; }
        set { errorPanel = value; }
    }

    private UpdatePanel errorUpdatePanel;

    public UpdatePanel ErrorUpdatePanel
    {
        get { return errorUpdatePanel; }
        set { errorUpdatePanel = value; }
    }

    private bool isOnForm = false;

    public bool IsOnForm
    {
        get { return isOnForm; }
        set { isOnForm = value; }
    }

    public EREC_HataridosFeladatok GetBusinessObject()
    {

        EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();

        if (!String.IsNullOrEmpty(FeladatId))
        {
            erec_HataridosFeladatok.Leiras = txbLeiras.Text;
            erec_HataridosFeladatok.Felhasznalo_Id_Kiado = ftbxKiado.Id_HiddenField;
            erec_HataridosFeladatok.Felhasznalo_Id_Felelos = cstbxFelelos.Id_HiddenField;
            erec_HataridosFeladatok.Tipus = hfFeladatTipus.Value;
            erec_HataridosFeladatok.Funkcio_Id_Inditando = funkcioTextBox.Id_HiddenField;
            erec_HataridosFeladatok.Prioritas = hfPrioritas.Value;
            erec_HataridosFeladatok.LezarasPrioritas = hfLezarasPrioritas.Value;
            erec_HataridosFeladatok.Allapot = ktddAllapot.SelectedValue;
            erec_HataridosFeladatok.IntezkHatarido = ccIntezkedesiHatarido.Text;
            erec_HataridosFeladatok.KezdesiIdo = ccKezdesiIdo.Text;
            erec_HataridosFeladatok.LezarasDatuma = ccLezarasDatuma.Text;
            erec_HataridosFeladatok.Forras = hfForras.Value;
            erec_HataridosFeladatok.Base.Ver = FeleadatVersion;
            erec_HataridosFeladatok.Base.LetrehozasIdo = hfLetrehozasiIdo.Value;
            erec_HataridosFeladatok.Obj_Id = hfObjId.Value;
            erec_HataridosFeladatok.Obj_type = hfObjType.Value;
            erec_HataridosFeladatok.Azonosito = hfObjAzonosito.Value;
        }

        return erec_HataridosFeladatok;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //SetComponentsVisibility();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (String.IsNullOrEmpty(FeladatId))
            {
                if (MainPanel.Visible)
                {
                    Reset();
                }
            }
        }
    }

    private void SetComponentsVisibility()
    {
        if (IsOnForm)
        {
            panelForm.Visible = true;
            panelForm.Controls.Add(FeldatForm);
        }
    }

    public EREC_HataridosFeladatok Megtekint()
    {
        return SetMasterAdatokById(true);
    }

    public EREC_HataridosFeladatok SetMasterAdatokById()
    {
        return SetMasterAdatokById(false);
    }

    private EREC_HataridosFeladatok SetMasterAdatokById(bool isMegtekintes)
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = FeladatId;

            Result result = new Result();
            if (isMegtekintes)
                result = service.Megtekint(execParam);
            else
                result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_HataridosFeladatok erec_hataridosFeladat = (EREC_HataridosFeladatok)result.Record;

                    this.SetMasterAdatokByBusinessObject(erec_hataridosFeladat);

                    return erec_hataridosFeladat;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }

    public void SetMasterAdatokByBusinessObject(EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        if (erec_HataridosFeladatok != null)
        {

            txbLeiras.Text = erec_HataridosFeladatok.Leiras;

            ftbxKiado.Id_HiddenField = erec_HataridosFeladatok.Felhasznalo_Id_Kiado;
            ftbxKiado.SetFelhasznaloTextBoxById(errorPanel);

            cstbxFelelos.Id_HiddenField = erec_HataridosFeladatok.Felhasznalo_Id_Felelos;
            cstbxFelelos.SetCsoportTextBoxById(errorPanel);

            //ktddTipus.FillWithOneValue(KodTarak.FELADAT_TIPUS.kcsNev, erec_HataridosFeladatok.Tipus, errorPanel);
            hfFeladatTipus.Value = erec_HataridosFeladatok.Tipus;
            if (isOnForm)
            {
                funkcioTextBox.FeladatMode = true;
            }
            funkcioTextBox.Id_HiddenField = erec_HataridosFeladatok.Funkcio_Id_Inditando;
            funkcioTextBox.SetFunkcioTextBoxById(errorPanel);

            //HataridosFeladatok.Prioritas.SetTextBoxByValue(txtPrioritas, erec_HataridosFeladatok.Prioritas);
            hfPrioritas.Value = erec_HataridosFeladatok.Prioritas;
            hfLezarasPrioritas.Value = erec_HataridosFeladatok.LezarasPrioritas;

            ktddAllapot.FillWithOneValue(KodTarak.FELADAT_ALLAPOT.kcsNev, erec_HataridosFeladatok.Allapot, errorPanel);

            ccIntezkedesiHatarido.Text = erec_HataridosFeladatok.IntezkHatarido;
            ccKezdesiIdo.Text = erec_HataridosFeladatok.KezdesiIdo;
            ccLezarasDatuma.Text = erec_HataridosFeladatok.LezarasDatuma;

            hfForras.Value = erec_HataridosFeladatok.Forras;

            FeleadatVersion = erec_HataridosFeladatok.Base.Ver;
            hfLetrehozasiIdo.Value = erec_HataridosFeladatok.Base.LetrehozasIdo;

            hfObjId.Value = erec_HataridosFeladatok.Obj_Id;
            hfObjType.Value = erec_HataridosFeladatok.Obj_type;
            hfObjAzonosito.Value = erec_HataridosFeladatok.Azonosito;

            this.Update();
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(errorPanel, "Hiba!", "Feladat üzleti objektum üres!");
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
        }
    }

    public void Update()
    {
        MainPanel.Visible = true;
        FeldatForm.Visible = true;
        upFeladat.Update();
        upFeladatMasterAdatok.Update();
    }

    public void Reset()
    {
        FeladatId = String.Empty;
        FeleadatVersion = String.Empty;
        MainPanel.Visible = false;
        FeldatForm.Visible = false;
        upFeladat.Update();
        upFeladatMasterAdatok.Update();
    }
}
