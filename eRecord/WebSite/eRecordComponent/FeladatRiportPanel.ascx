<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeladatRiportPanel.ascx.cs"
    Inherits="eRecordComponent_FeladatRiportPanel" %>
<%-- Megjegyz�s: a hyperlinkek NavigateUrl tulajdons�ga codebehindb�l �ll�tva! --%>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="fcstb" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox"
    TagPrefix="cstb" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="../Component/DdlIntervallum_SearchFormControl.ascx" TagName="DdlIntervallum_SearchFormControl" TagPrefix="ddlintervall" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%-- A k�dban haszn�ljuk! --%>
<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<%--<asp:UpdatePanel ID="FeladatRiportUpdatePanel" runat="server" OnLoad="FeladatRiportUpdatePanel_Load">
    <ContentTemplate>--%>
<asp:Panel ID="FeladatRiportPanel" runat="server">
    <asp:UpdatePanel ID="FeladatRiportUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="hfActive" runat="server" />
        <asp:HiddenField ID="hfLastRefreshUser" runat="server" />
        <asp:HiddenField ID="hfLastRefreshDateTime" runat="server" />
<%--    </ContentTemplate>
    </asp:UpdatePanel>--%>

        <eUI:eFormPanel ID="EFormPanel1" runat="server" Visible="true">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapSor">                 
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelFelhasznalo" runat="server" Text="Felhaszn�l�:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <fcstb:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox1" runat="server" CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelLejaratTol" runat="server" Text="Lej�rat:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <ddlintervall:DdlIntervallum_SearchFormControl ID="ddlintervallLejarat" runat="server" CssClass="mrUrlapInputComboBoxKozepes" />
                        </td>
                        <td class="mrUrlapCaptionBal" style="padding-left:10px;">
                            <asp:Label ID="labelFrissitesIdeje" runat="server" Text="Referencia&nbsp;d�tum:" />
                            <br />
                            <uc5:CalendarControl ID="CalendarControlFrissitesIdeje" TimeVisible="false" runat="server" />
                        </td>
                        <td style="width: 100%;">
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelSzervezet" runat="server" Text="Szervezet:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <cstb:CsoportTextBox ID="CsoportTextBox_Szervezet" runat="server" SzervezetCsoport="true" ViewMode="true" CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelPrioritasTol" runat="server" Text="Priorit�s:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <ddlintervall:DdlIntervallum_SearchFormControl ID="ddlintervallPrioritas" runat="server" CssClass="mrUrlapInputComboBoxKozepes" />
                        </td>
                        <td class="mrUrlapCaptionBal" style="padding-left:10px;">
<%--                            <asp:ImageButton ID="ImageButton_Refresh" runat="server" ImageUrl="../images/hu/trapezgomb/frissites_trap.jpg"
                                onmouseover="swapByName(this.id,'frissites_trap2.jpg')" onmouseout="swapByName(this.id,'frissites_trap.jpg')" />
--%>
                            <div class="DisableWrap">
                                <asp:ImageButton ID="ImageButton_Refresh" runat="server" ImageUrl="../images/hu/muvgomb/refresh.jpg" CssClass="highlightit"
                                    AlternateText="Riport friss�t�se" />
                                <asp:ImageButton ID="ImageButton_UpdateDataMart" runat="server" ImageUrl="../images/hu/muvgomb/refresh_piros.jpg" CssClass="highlightit"
                                    AlternateText="Feladatok aktualiz�l�sa" />
                            </div>
                            
                       </td>
                        <td style="width: 100%;">
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
    </ContentTemplate>
</asp:UpdatePanel>
        <eUI:eFormPanel ID="RiportForm" runat="server">
                    <table cellspacing="0" cellpadding="0">
                <tbody>
                    <tr height="300px">
                        <td class="mrUrlapMezo">
                            <rsweb:ReportViewer ID="ReportViewerFeladatOsszesito" runat="server"
                                Font-Names="Verdana" Font-Size="8pt" Height="100%" Width="100%" ZoomMode="Percent" ZoomPercent="100" EnableTelemetry="false">
                                <LocalReport ReportPath="" />
                                <ServerReport ReportPath="" ReportServerUrl="" />
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                    </tbody>
                    </table>
        </eUI:eFormPanel>
    </asp:Panel>
