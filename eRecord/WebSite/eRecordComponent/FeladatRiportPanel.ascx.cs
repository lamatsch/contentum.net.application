using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using System.Security.Principal;

[Serializable]
public sealed class ReportServerNetworkCredentials : IReportServerCredentials
{
    #region IReportServerCredentials Members

    public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
    {
        authCookie = null;
        userName = null;
        password = null;
        authority = null;

        // Not using form credentials
        return false;
    }

    public WindowsIdentity ImpersonationUser
    {
        get
        {
            return null;
        }
    }

    public System.Net.ICredentials NetworkCredentials
    {
        get
        {
            // Read the user information from the Web.config file.  
            // By reading the information on demand instead of 
            // storing it, the credentials will not be stored in 
            // session, reducing the vulnerable surface area to the
            // Web.config file, which can be secured with an ACL.

            // User name
            string userName = 
                ConfigurationManager.AppSettings
                    ["ReportViewerUser"];

            if (string.IsNullOrEmpty(userName))
                throw new Exception(
                    "Missing user name from web.config file");

            // Password
            string password = 
                ConfigurationManager.AppSettings
                    ["ReportViewerPassword"];

            if (string.IsNullOrEmpty(password))
                throw new Exception(
                    "Missing password from web.config file");

            // Domain
            string domainName = 
                ConfigurationManager.AppSettings
                    ["ReportViewerDomain"];

            if (string.IsNullOrEmpty(domainName))
                throw new Exception(
                    "Missing domain from web.config file");

            //string userName = "Axis\boda.eszter";
            //string domainName = "srvcontentumweb:100";
            //string password = "";

            return new System.Net.NetworkCredential(userName, password, domainName);
        }
    }

    #endregion
}

public partial class eRecordComponent_FeladatRiportPanel : System.Web.UI.UserControl
{
    private const bool isRemoteReport = false;
    private const string ServerReport_ReportUrl = "http://srvcontentumweb:100/reportserver";
    private const string ServerReport_ReportPath = "/Ertesitesek/Feladatok";
    private const string LocalReport_ReportPath = "Feladatok.rdlc";
    private const string LocalReport_DataSet = "DataSet_Feladatok";

    private const string UI_FeladatRiport_CannotRefresh = "Friss�t�s nem enged�lyezett, a legut�bbi friss�t�s �ta m�g nem telt el {0} perc!";

    private const int minUpdateTimeDiffInMin_Default = 10;  // alap�rtelmezett �rt�k a friss�t�sek k�z�tti minim�lis id�tartamra, egy�bk�nt rendszerparam�terb�l kapjuk

    public bool Active
    {
        get
        {
            if (hfActive.Value == "1")
                return true;
            return false;
        }
        set
        {
            if(value)
            {
                //FeladatRiportPanel.Visible = true;
                hfActive.Value = "1";
            }
            else
            {
                //FeladatRiportPanel.Visible = false;
                hfActive.Value = String.Empty;
            }
            
        }
    }

    private Component_ListHeader _listheader = null;

    public Component_ListHeader ListHeader
    {
        get { return _listheader; }
        set { _listheader = value; }
    }

    private int _minUpdateTimeDiffInMin = minUpdateTimeDiffInMin_Default;
    private int minUpdateTimeDiffInMin
    {
        get { return _minUpdateTimeDiffInMin; }
        set { _minUpdateTimeDiffInMin = value;  }
    }

    private int Get_MinUpdateTimeDiffInMin_FromRPM()
    {
        int min;
        string strMinUpdateTimeDiffInMin = Rendszerparameterek.Get(Page, Rendszerparameterek.FELADATOSSZESITO_FRISSITES_MIN_PERC);

        if (Int32.TryParse(strMinUpdateTimeDiffInMin, out min))
        {
            return min;
        }
        else
        {
            return minUpdateTimeDiffInMin_Default;
        }
    }

    private void RegisterStartupScript_RefreshReport()
    {
        // Tr�kk a teljes postback n�lk�li friss�t�sre:
        // megkeress�k a riport iframe elem�t, �s kicser�lj�k az src-t �nmag�ra
        string js = "var ifr = $get('ReportFrame' + '" + ReportViewerFeladatOsszesito.ClientID + "'); if (ifr) {var src_old = ifr.src; ifr.src = src_old;}";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "refreshReport", js, true);
    }

    private void SetComponentsVisibilityByVezeto(bool value)
    {
        FelhasznaloCsoportTextBox1.ReadOnly = !value;
    }

    private bool isExecutorUserVezeto()
    {
        string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);

        if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
        {
            return true;
        }

        return false;

    }

    private bool IsTabPanelEnabled
    {
        get
        {
            AjaxControlToolkit.TabPanel parentTabPanel = UI.FindParentControl<AjaxControlToolkit.TabPanel>(this);
            if (parentTabPanel != null)
            {
                return parentTabPanel.Enabled;
            }

            return true;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!FunctionRights.GetFunkcioJog(Page, "FeladatRiport"))
        {
            FeladatRiportPanel.Visible = false;
            return;
        }

        ImageButton_Refresh.Click += new ImageClickEventHandler(ImageButton_Refresh_Click);
        ImageButton_UpdateDataMart.Click += new ImageClickEventHandler(ImageButton_UpdateDataMart_Click);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsTabPanelEnabled)
        {
            FeladatRiportPanel.Visible = false;
            return;
        }
        //ScriptManager.GetCurrent(Page).RegisterPostBackControl(ImageButton_Refresh);

        FelhasznaloCsoportTextBox1.CsakSajatSzervezetDolgozoi = true;
        // friss�t�sek k�z�tti minim�lis id� be�ll�t�sa
        minUpdateTimeDiffInMin = Get_MinUpdateTimeDiffInMin_FromRPM();

        if (!IsPostBack)
        {
            CsoportTextBox_Szervezet.ReadOnly = true;

            bool isVezeto = isExecutorUserVezeto();

            SetComponentsVisibilityByVezeto(isVezeto);

            FillParameterControls();
            SetParameterControlsToDefault();

            if (isRemoteReport)
            {
                ReportViewerFeladatOsszesito.ProcessingMode = ProcessingMode.Remote;
                ReportViewerFeladatOsszesito.ServerReport.ReportServerUrl = new Uri(ServerReport_ReportUrl);
                ReportViewerFeladatOsszesito.ServerReport.ReportPath = ServerReport_ReportPath;

                ReportViewerFeladatOsszesito.ShowRefreshButton = false;
                ReportViewerFeladatOsszesito.ShowParameterPrompts = false;
            }
            else
            {
                ReportViewerFeladatOsszesito.ProcessingMode = ProcessingMode.Local;

                ReportViewerFeladatOsszesito.LocalReport.ReportPath = LocalReport_ReportPath;
                ReportViewerFeladatOsszesito.ShowRefreshButton = false;
                ReportViewerFeladatOsszesito.ShowParameterPrompts = false;
                ReportViewerFeladatOsszesito.LocalReport.EnableHyperlinks = true;
            }
            RefreshReport();

        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Active) return;
        RefreshButtonsVisiblity();
    }

    //protected void FeladatRiportUpdatePanel_Load(object sender, EventArgs e)
    //{
        
    //}

    protected void HideErrorPanel()
    {
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
    }

    protected void ImageButton_Refresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        HideErrorPanel();
        RefreshButtonsVisiblity();

        RefreshReport();
    }

    protected void ImageButton_UpdateDataMart_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        HideErrorPanel();
        RefreshButtonsVisiblity();

        string Felhasznalo_Id = FelhasznaloCsoportTextBox1.Id_HiddenField;

        if (CanRefresh(Felhasznalo_Id))
        {
            Contentum.eAdmin.Service.Fact_FeladatokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetFact_FeladatokService();

            ExecParam execParam = UI.SetExecParamDefault(Page);

            Result result = service.FeladatLetrehozo(execParam, Felhasznalo_Id);

            DateTime LastUpdateTime = DateTime.MinValue;
            if (result.Record != null)
            {
                LastUpdateTime = (DateTime)result.Record;
                if (LastUpdateTime != DateTime.MinValue)
                {
                    UpdateLastRefreshDate(Felhasznalo_Id, LastUpdateTime);
                }
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            CalendarControlFrissitesIdeje.Text = LastUpdateTime.ToString();

            FeladatRiportUpdatePanel.Update();

            //DateTime now = DateTime.Now;
            //UpdateLastRefreshDate(Felhasznalo_Id, now);
            //CalendarControlFrissitesIdeje.Text = now.ToString();

            //FeladatRiportUpdatePanel.Update();
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CannotRefresh", "alert('" + String.Format(UI_FeladatRiport_CannotRefresh, this.minUpdateTimeDiffInMin) + "');", true);
        }
    }

    public void ReLoadTab()
    {
        if (!Active) return;

        //RefreshReport();
    }

    protected void FillParameterControls()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);
        
        Contentum.eAdmin.Service.Dim_LejaratKategoriaService service_LejaratKategoria = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetDim_LejaratKategoriaService();
        Dim_LejaratKategoriaSearch search_LejaratKategoria = new Dim_LejaratKategoriaSearch();

        // Id=-1-es t�telek kisz�r�se
        search_LejaratKategoria.Id.Value = "-1";
        search_LejaratKategoria.Id.Operator = Query.Operators.notequals;

        search_LejaratKategoria.OrderBy = "Sorrend ASC";
        
        Result result_LejaratKategoria = service_LejaratKategoria.GetAll(execParam, search_LejaratKategoria);

        ddlintervallLejarat.Clear();
        if (String.IsNullOrEmpty(result_LejaratKategoria.ErrorCode))
        {
            // felt�lt�s
            ddlintervallLejarat.DataSource = result_LejaratKategoria.Ds.Tables[0];
            ddlintervallLejarat.DataTextField = "ElteresNev";
            ddlintervallLejarat.DataValueField = "Sorrend";
            ddlintervallLejarat.BindData(true);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_LejaratKategoria);
            ErrorUpdatePanel.Update();
            return;
        }

        Contentum.eAdmin.Service.Dim_PrioritasService service_Prioritas = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetDim_PrioritasService();
        Dim_PrioritasSearch search_Prioritas = new Dim_PrioritasSearch();
        
        // Id=-1-es t�telek kisz�r�se
        search_Prioritas.Id.Value = "-1";
        search_Prioritas.Id.Operator = Query.Operators.notequals;

        search_Prioritas.OrderBy = "Prioritas ASC";

        Result result_Prioritas = service_Prioritas.GetAll(execParam, search_Prioritas);

        ddlintervallPrioritas.Clear();
        if (String.IsNullOrEmpty(result_Prioritas.ErrorCode))
        {
            // felt�lt�s
            ddlintervallPrioritas.DataSource = result_Prioritas.Ds.Tables[0];
            ddlintervallPrioritas.DataTextField = "PrioritasNev";
            ddlintervallPrioritas.DataValueField = "Prioritas";
            ddlintervallPrioritas.BindData(true);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Prioritas);
            ErrorUpdatePanel.Update();
            return;
        }
    }

    protected DateTime GetUserLastRefreshTime(string Felhasznalo_Id)
    {
        DateTime LastRefreshTime = System.DateTime.MinValue;

        if (!String.IsNullOrEmpty(Felhasznalo_Id))
        {
            #region Get FrissitesIdo
            ExecParam execParam = UI.SetExecParamDefault(Page);

            Contentum.eAdmin.Service.Dim_FeladatFelelosService service_FeladatFelelos = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetDim_FeladatFelelosService();
            Dim_FeladatFelelosSearch search_FeladatFelelos = new Dim_FeladatFelelosSearch();
            search_FeladatFelelos.Felhasznalo_Id.Value = Felhasznalo_Id;
            search_FeladatFelelos.Felhasznalo_Id.Operator = Query.Operators.equals;

            search_FeladatFelelos.Szervezet_Id.Value = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
            search_FeladatFelelos.Szervezet_Id.Operator = Query.Operators.equals;

            search_FeladatFelelos.OrderBy = "FrissitesIdo DESC";
            search_FeladatFelelos.TopRow = 1;

            Result result_FeladatFelelos = service_FeladatFelelos.GetAll(execParam, search_FeladatFelelos);

            if (String.IsNullOrEmpty(result_FeladatFelelos.ErrorCode))
            {
                // felt�lt�s
                if (result_FeladatFelelos.Ds.Tables[0].Rows.Count > 0)
                {
                    string rowDatum = result_FeladatFelelos.Ds.Tables[0].Rows[0]["FrissitesIdo"].ToString();
                    if (!String.IsNullOrEmpty(rowDatum))
                    {
                        DateTime rowDateTime;
                        if (DateTime.TryParse(rowDatum, out rowDateTime))
                        {
                            LastRefreshTime = rowDateTime;

                            UpdateLastRefreshDate(Felhasznalo_Id, LastRefreshTime);
                        }
                    }
                }
            }
            #endregion Get FrissitesIdo        
        }

        return LastRefreshTime;
    }

    protected void SetParameterControlsToDefault()
    {
        //string DatumText = System.DateTime.Now.ToShortDateString();

        #region Get FrissitesIdo
        DateTime UserLastRefreshTime = GetUserLastRefreshTime(FelhasznaloProfil.FelhasznaloId(Page));
        if (UserLastRefreshTime == DateTime.MinValue)
        {
            UserLastRefreshTime = DateTime.Now;
        }
        string DatumText = UserLastRefreshTime.ToShortDateString();
        #endregion Get FrissitesIdo

        CalendarControlFrissitesIdeje.Text = DatumText;

        FelhasznaloCsoportTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        FelhasznaloCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        CsoportTextBox_Szervezet.Id_HiddenField = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        CsoportTextBox_Szervezet.SetCsoportTextBoxById(EErrorPanel1);
    }

    protected Dictionary<string, DateTime> GetLastRefreshDictionary()
    {
        if (ViewState["LastRefresh"] == null)
        {
            return new Dictionary<string, DateTime>();
        }
        else
        {
            return (Dictionary<string, DateTime>)ViewState["LastRefresh"];
        }
    }

    protected void SetLastRefreshDictionary(Dictionary<string, DateTime> LastRefreshDictionary)
    {
            ViewState["LastRefresh"] = LastRefreshDictionary;
    }

    protected void UpdateLastRefreshDate(string UserId, DateTime datetime)
    {
        Dictionary<string, DateTime> LastRefreshDictionary = GetLastRefreshDictionary();

        if (LastRefreshDictionary.ContainsKey(UserId))
        {
            LastRefreshDictionary[UserId] = datetime;
        }
        else
        {
            LastRefreshDictionary.Add(UserId, datetime);
        }

        SetLastRefreshDictionary(LastRefreshDictionary);
    }

    protected bool CanRefresh(string Felhasznalo_Id)
    {
        Dictionary<string, DateTime> LastRefreshDictionary = GetLastRefreshDictionary();
        
        if (LastRefreshDictionary == null)
        {
           // ilyen nem lehet
        }
        else
        {
            if (!LastRefreshDictionary.ContainsKey(Felhasznalo_Id))
            {
                DateTime UserLastRefreshTime = GetUserLastRefreshTime(Felhasznalo_Id);
                if (UserLastRefreshTime > DateTime.MinValue)
                {
                    UpdateLastRefreshDate(Felhasznalo_Id, UserLastRefreshTime);
                }
            }

            if (LastRefreshDictionary.ContainsKey(Felhasznalo_Id))
            {
                TimeSpan tsDiff = DateTime.Now.Subtract(LastRefreshDictionary[Felhasznalo_Id]);
                if (tsDiff.TotalMinutes < this.minUpdateTimeDiffInMin)
                {
                    return false;
                }
            }
        }
        return true;
    }

        // form --> business object
    private FeladatokRiportParameterek GetBusinessObjectFromComponents()
    {
        FeladatokRiportParameterek feladatokRiportParameterek = new FeladatokRiportParameterek();

        FeladatokRiportParameterek feladatRiportParameterek = new FeladatokRiportParameterek();
        feladatRiportParameterek.Datum = System.Data.SqlTypes.SqlDateTime.Parse(CalendarControlFrissitesIdeje.TextBox.Text);
        feladatRiportParameterek.Felhasznalo = FelhasznaloCsoportTextBox1.Id_HiddenField;
        feladatRiportParameterek.Szervezet = CsoportTextBox_Szervezet.Id_HiddenField;

        if (!String.IsNullOrEmpty(ddlintervallLejarat.DdlTol))
        {
            feladatRiportParameterek.LejaratTol = System.Data.SqlTypes.SqlInt32.Parse(ddlintervallLejarat.DdlTol);
        }
        if (!String.IsNullOrEmpty(ddlintervallLejarat.DdlIg))
        {
            feladatRiportParameterek.LejaratIg = System.Data.SqlTypes.SqlInt32.Parse(ddlintervallLejarat.DdlIg);
        }
        if (!String.IsNullOrEmpty(ddlintervallPrioritas.DdlTol))
        {
            feladatRiportParameterek.PrioritasTol = System.Data.SqlTypes.SqlInt32.Parse(ddlintervallPrioritas.DdlTol);
        }
        if (!String.IsNullOrEmpty(ddlintervallPrioritas.DdlIg))
        {
            feladatRiportParameterek.PrioritasIg = System.Data.SqlTypes.SqlInt32.Parse(ddlintervallPrioritas.DdlIg);
        }

        return feladatRiportParameterek;
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "datum":
                            ReportParameters[i].Values.Add(CalendarControlFrissitesIdeje.Text);
                            break;
                        case "Felhasznalo":
                            ReportParameters[i].Values.Add(FelhasznaloCsoportTextBox1.Id_HiddenField);
                            break;
                        case "Szervezet":
                            ReportParameters[i].Values.Add(CsoportTextBox_Szervezet.Id_HiddenField);
                            break;
                        case "ASPURL":
                            string requestUrl = Request.Url.GetLeftPart(UriPartial.Path);
                            int lengthOfPath = (requestUrl.LastIndexOf("/") > -1 ? requestUrl.LastIndexOf("/") + 1 : requestUrl.Length);

                            string eRecordWebSiteUrl = requestUrl.Substring(0, lengthOfPath);
                            ReportParameters[i].Values.Add(eRecordWebSiteUrl);
                            break;
                        case "Frissites":
                            string paramValue = CalendarControlFrissitesIdeje.Text;
                            DateTime dtDatum;
                            if (DateTime.TryParse(paramValue, out dtDatum))
                            {
                                if (DateTime.Today > dtDatum)
                                {
                                    DateTime dtDatumWithDefaultTime = new DateTime(dtDatum.Year, dtDatum.Month, dtDatum.Day, 23, 0, 0); // default automatikus friss�t�si id�
                                    paramValue = dtDatumWithDefaultTime.ToString();
                                }
                            }
                            ReportParameters[i].Values.Add(paramValue);
                            break;
                        case "FelhasznaloNev":
                            ReportParameters[i].Values.Add(FelhasznaloCsoportTextBox1.Text);
                            break;
                        case "SzervezetNev":
                            ReportParameters[i].Values.Add(CsoportTextBox_Szervezet.Text);
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }

    // a Header �s Footer mez�i csak param�terb�l t�lthet�k, adatforr�sb�l nem :(
    protected void SetReportParametersFromTableRow(ReportParameter[] ReportParameters, DataRow row)
    {
        if (row == null || ReportParameters == null) return;

        if (ReportParameters.Length > 0)
        {
            int index_rp = -1;

            #region Frissites
            // ha a lek�rdez�si d�tum mai, az utols� friss�t�si id�t k�rj�k el,
            // ha kor�bbi, a sz�r�si d�tumot kieg�sz�tj�k az alap�rtelmezett napi utols� (automatikus) friss�t�s �ra:perc adat�val
            index_rp = Array.FindIndex<ReportParameter>(ReportParameters, (delegate(ReportParameter rp) { return rp.Name == "Frissites"; }));
            if (index_rp > -1)
            {
                if (row.Table.Columns.Contains("datum"))
                {
                    string paramValue = row["datum"].ToString();
                    DateTime dtDatum;
                    if (DateTime.TryParse(paramValue, out dtDatum))
                    {
                        if (DateTime.Today > dtDatum)
                        {
                            DateTime dtDatumWithDefaultTime = new DateTime(dtDatum.Year, dtDatum.Month, dtDatum.Day, 23, 0, 0); // default automatikus friss�t�si id�
                            paramValue = dtDatumWithDefaultTime.ToString();
                        }
                        else if (row.Table.Columns.Contains("FrissitesIdo"))
                        {
                            paramValue = row["FrissitesIdo"].ToString();
                        }
                    }
                    ReportParameters[index_rp].Values.Clear();
                    ReportParameters[index_rp].Values.Add(paramValue);
                }
            }
            #endregion Frissites

            // ak�r el is hagyhat�, megegyezik a fel�letr�l be�ll�tottakkal
            #region FelhasznaloNev
            index_rp = Array.FindIndex<ReportParameter>(ReportParameters, (delegate(ReportParameter rp) { return rp.Name == "FelhasznaloNev"; }));
            if (index_rp > -1)
            {
                if (row.Table.Columns.Contains("FelhasznaloNev"))
                {
                    string paramValue = row["FelhasznaloNev"].ToString();
                    ReportParameters[index_rp].Values.Clear();
                    ReportParameters[index_rp].Values.Add(paramValue);
                }
            }
            #endregion FelhasznaloNev

            #region SzervezetNev
            index_rp = Array.FindIndex<ReportParameter>(ReportParameters, (delegate(ReportParameter rp) { return rp.Name == "SzervezetNev"; }));
            if (index_rp > -1)
            {
                if (row.Table.Columns.Contains("SzervezetNev"))
                {
                    string paramValue = row["SzervezetNev"].ToString();
                    ReportParameters[index_rp].Values.Clear();
                    ReportParameters[index_rp].Values.Add(paramValue);
                }
            }
            #endregion SzervezetNev
        }
    }

    protected void RefreshReport()
    {
        if (isRemoteReport)
        {
            #region ReportParameters
            ReportParameterInfoCollection rpis = ReportViewerFeladatOsszesito.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);
                ReportViewerFeladatOsszesito.ServerReport.SetParameters(ReportParameters);
            }
            #endregion ReportParameters

            ReportViewerFeladatOsszesito.ServerReport.Refresh();
        }
        else
        {
            Contentum.eAdmin.Service.Fact_FeladatokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetFact_FeladatokService();
            //Fact_FeladatokSearch search = new Fact_FeladatokSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page);

            FeladatokRiportParameterek feladatRiportParameterek = GetBusinessObjectFromComponents();
            Result result = service.Ertesitesek(execParam, feladatRiportParameterek);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }

            ReportDataSource datasource = new ReportDataSource(LocalReport_DataSet, result.Ds.Tables[0]);
            //ReportViewerFeladatOsszesito.Reset();
            ReportViewerFeladatOsszesito.ProcessingMode = ProcessingMode.Local;

            ReportViewerFeladatOsszesito.LocalReport.ReportPath = LocalReport_ReportPath;
            ReportViewerFeladatOsszesito.ShowRefreshButton = false;
            ReportViewerFeladatOsszesito.ShowParameterPrompts = false;
            ReportViewerFeladatOsszesito.LocalReport.EnableHyperlinks = true;

            ReportViewerFeladatOsszesito.LocalReport.DataSources.Clear();
            ReportViewerFeladatOsszesito.LocalReport.DataSources.Add(datasource);

            #region ReportParameters
            ReportParameterInfoCollection rpis = ReportViewerFeladatOsszesito.LocalReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                // header/footer adatok hozz�ad�sa az eredm�nyhalmaz alapj�n
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    SetReportParametersFromTableRow(ReportParameters, result.Ds.Tables[0].Rows[0]);
                }
                ReportViewerFeladatOsszesito.LocalReport.SetParameters(ReportParameters);
            }
            #endregion ReportParameters

            ReportViewerFeladatOsszesito.LocalReport.Refresh();
        }

        RegisterStartupScript_RefreshReport();
    }

    private void RefreshButtonsVisiblity()
    {
        if (ListHeader != null)
        {
            ListHeader.SetRightFunctionButtonsVisible(false);
            ListHeader.SendObjectsVisible = false;
            ListHeader.PagerVisible = false;
            #region Baloldali funkci�gombok kiszed�se
            ListHeader.SearchVisible = false;
            ListHeader.NewVisible = false;
            ListHeader.AddVisible = false;
            ListHeader.ViewVisible = false;
            ListHeader.ModifyVisible = false;
            ListHeader.ExportVisible = false;
            ListHeader.PrintVisible = false;
            ListHeader.InvalidateVisible = false;
            ListHeader.HistoryVisible = false;
            ListHeader.DefaultFilterVisible = false;

            ListHeader.RefreshVisible = false;
            #endregion
        }
    }
}
