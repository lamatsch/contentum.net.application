<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeladataimPanel.ascx.cs" Inherits="eRecordComponent_FeladataimPanel" %>
<%-- Megjegyz�s: a hyperlinkek NavigateUrl tulajdons�ga codebehindb�l �ll�tva! --%>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="FeladatokUpdatePanel" runat="server" OnLoad="FeladatokUpdatePanel_Load">
    <ContentTemplate>
        <asp:HiddenField ID="hfActive" runat="server" />
        <eUI:eFormPanel ID="EFormPanel1" runat="server" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Futtat�s&nbsp;ideje: " CssClass="FuttatasIdejeRowStyle"></asp:Label>
                    </td>
                    <td style="width: 100%; text-align: left; padding-left: 10px;">
                        <asp:Label ID="Label_Idopont" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td align="left" colspan="2">
                         <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td style="width: 0%;">
                                        </td>
                                    </tr>
                                     <tr class="urlapSor">
                                        <td>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" colspan="2" runat="server" id="td_Atveendok_Ossz">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Ossz" runat="server">
                                                <asp:Label CssClass="mrUrlapMezoCenterBoldUnderLine" ID="Label_Header_Atveendok" runat="server" Text="�tveend�k"></asp:Label>&nbsp;
                                                <asp:Label ID="Label_Atveendok_Ossz" runat="server" Text=""></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" colspan="5" runat="server" id="td_Jovahagyandok_Ossz">
                                                <asp:Label CssClass="mrUrlapMezoCenterBold" ID="Label_Header_Jovahagyandok" runat="server" Text="J�v�hagyand�k"></asp:Label>&nbsp;
                                                <asp:Label ID="Label_Jovahagyandok_Ossz" runat="server" Text=""></asp:Label>
                                        </td>                                     
                                    </tr>                                   
                                    <tr class="urlapSor">
                                        <td>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szemely_Ossz">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szemely_Ossz" runat="server">
                                                <asp:Label CssClass="mrUrlapMezoCenterBoldUnderLine" ID="Label_Header_Atveendok_Szemely" runat="server" Text="Szem�lyre"></asp:Label>&nbsp;<asp:Label ID="Label_Atveendok_Szemely_Ossz" runat="server" Text=""></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szervezet_Ossz">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szervezet_Ossz" runat="server">
                                                <asp:Label CssClass="mrUrlapMezoCenterBoldUnderLine" ID="Label_Header_Atveendok_Szervezet" runat="server" Text="Szervezetre"></asp:Label>&nbsp;<asp:Label ID="Label_Atveendok_Szervezet_Ossz" runat="server" Text=""></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atadandok_Ossz">
                                            <asp:HyperLink ID="HyperLink_Atadandok_Ossz" runat="server">
                                                <asp:Label CssClass="mrUrlapMezoCenterBoldUnderLine" ID="Label4" runat="server" Text="�tadand�k"></asp:Label>&nbsp;<asp:Label ID="Label_Atadandok_Ossz" runat="server" Text=""></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold">
                                            <asp:Label ID="Label6" runat="server" Text="Saj�t"></asp:Label>&nbsp;<asp:Label ID="Label_Sajat_Ossz" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyandok_Ossz_Irattarozasra">
                                            <asp:Label ID="Label1" runat="server" Text="Iratt�roz�sra"></asp:Label>&nbsp;<asp:Label ID="Label_Jovahagyandok_Ossz_Irattarozasra" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyandok_Ossz_Kolcsonzesre">
                                            <asp:Label ID="Label11" runat="server" Text="K�lcs�nz�sre"></asp:Label>&nbsp;<asp:Label ID="Label_Jovahagyandok_Ossz_Kolcsonzesre" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyandok_Ossz_Kikeresre">
                                            <asp:Label ID="Label3" runat="server" Text="Kik�r�sre"></asp:Label>&nbsp;<asp:Label ID="Label_Jovahagyandok_Ossz_Kikeresre" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyandok_Ossz_SkontrobaHelyezes">
                                            <asp:Label ID="Label_Jovahagyandok_Ossz_SkontrobaHelyezesTitle" runat="server" Text="Skontr�ba helyez�s"></asp:Label>&nbsp;
                                            <asp:Label ID="Label_Jovahagyandok_Ossz_SkontrobaHelyezesCount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyandok_Ossz_Elintezesre">
                                            <asp:Label ID="Label_Jovahagyandok_Ossz_ElintezesreTitle" runat="server" Text="Elint�zett� nyilv�n�t�sra"></asp:Label>&nbsp;
                                            <asp:Label ID="Label_Jovahagyandok_Ossz_ElintezesreCount" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label5" runat="server" Text="�gyiratok:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szemely_Ugyirat">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szemely_Ugyirat" runat="server">
                                                <asp:Label ID="Label_Atveendok_Ugyirat" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szervezet_Ugyirat">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szervezet_Ugyirat" runat="server">
                                                <asp:Label ID="Label_Atveendok_Szervezet_Ugyirat" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atadandok_Ugyirat">
                                            <asp:HyperLink ID="HyperLink_Atadandok_Ugyirat" runat="server">
                                                <asp:Label ID="Label_Atadandok_Ugyirat" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold">
                                            <asp:HyperLink ID="HyperLink_Sajat_Ugyiratok" runat="server">
                                                <asp:Label ID="Label_Sajat_Ugyiratok" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Ugyiratok_Irattarozasra">
                                            <asp:HyperLink ID="HyperLink_Jovahagyando_Ugyiratok_Irattarozasra" runat="server">
                                                <asp:Label ID="Label_Jovahagyando_Ugyiratok_Irattarozasra" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Ugyiratok_Kolcsonzesre">
                                            <asp:HyperLink ID="HyperLink_Jovahagyando_Ugyiratok_Kolcsonzesre" runat="server">
                                                <asp:Label ID="Label_Jovahagyando_Ugyiratok_Kolcsonzesre" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Ugyiratok_Kikeresre">
                                            <asp:HyperLink ID="HyperLink_Jovahagyando_Ugyiratok_Kikeresre" runat="server">
                                                <asp:Label ID="Label_Jovahagyando_Ugyiratok_Kikeresre" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Ugyiratok_SkontobaHelyezes">
                                            <asp:HyperLink ID="HyperLink_Jovahagyando_Ugyiratok_SkontrobaHelyezes" runat="server">
                                                <asp:Label ID="Label_Jovahagyando_Ugyiratok_SkontrobaHelyezes" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Ugyiratok_Elintezesre">
                                            <asp:HyperLink ID="HyperLink_Jovahagyando_Ugyiratok_Elintezesre" runat="server">
                                                <asp:Label ID="Label_Jovahagyando_Ugyiratok_Elintezesre" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label7" runat="server" Text="K�ldem�nyek:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szemely_Kuld">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szemely_Kuld" runat="server">
                                                <asp:Label ID="Label_Atveendok_Kuld" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szervezet_Kuld">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szervezet_Kuld" runat="server">
                                                <asp:Label ID="Label_Atveendok_Szervezet_Kuld" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atadandok_Kuld">
                                            <asp:HyperLink ID="HyperLink_Atadandok_Kuld" runat="server">
                                                <asp:Label ID="Label_Atadandok_Kuld" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold">
                                            <asp:HyperLink ID="HyperLink_Sajat_kuld" runat="server">
                                                <asp:Label ID="Label_Sajat_kuld" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_kuld_Irattarozasra">
                                                <asp:Label ID="Label_Jovahagyando_kuld_Irattarozasra" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_kuld_Kolcsonzesre">
                                                <asp:Label ID="Label_Jovahagyando_kuld_Kolcsonzesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_kuld_Kikeresre">
                                                <asp:Label ID="Label_Jovahagyando_kuld_Kikeresre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_kuld_SkontrobaHelyezes">
                                                <asp:Label ID="Label_Jovahagyando_kuld_SkontrobaHelyezes" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_kuld_Elintezesre">
                                                <asp:Label ID="Label_Jovahagyando_kuld_Elintezesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label8" runat="server" Text="Iratp�ld�nyok:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szemely_Pld">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szemely_Pld" runat="server">
                                                <asp:Label ID="Label_Atveendok_Pld" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szervezet_Pld">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szervezet_Pld" runat="server">
                                                <asp:Label ID="Label_Atveendok_Szervezet_Pld" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atadandok_Pld">
                                            <asp:HyperLink ID="HyperLink_Atadandok_Pld" runat="server">
                                                <asp:Label ID="Label_Atadandok_Pld" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold">
                                            <asp:HyperLink ID="HyperLink_Sajat_IratPeldanyok" runat="server">
                                                <asp:Label ID="Label_Sajat_IratPeldanyok" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_IratPeldanyok_Irattarozasra">
                                                <asp:Label ID="Label_Jovahagyando_IratPeldanyok_Irattarozasra" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_IratPeldanyok_Kolcsonzesre">
                                                <asp:Label ID="Label_Jovahagyando_IratPeldanyok_Kolcsonzesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_IratPeldanyok_Kikeresre">
                                                <asp:Label ID="Label_Jovahagyando_IratPeldanyok_Kikeresre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_IratPeldanyok_SkontrobaHelyezes">
                                                <asp:Label ID="Label_Jovahagyando_IratPeldanyok_SkontrobaHelyezes" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_IratPeldanyok_Elintezesre">
                                                <asp:Label ID="Label_Jovahagyando_IratPeldanyok_Elintezesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelMappak" runat="server" Text="Dosszi�k:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szemely_Mappak">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szemely_Mappak" runat="server">
                                                <asp:Label ID="Label_Atveendok_Mappak" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atveendok_Szervezet_Mappak">
                                            <asp:HyperLink ID="HyperLink_Atveendok_Szervezet_Mappak" runat="server">
                                                <asp:Label ID="Label_Atveendok_Szervezet_Mappak" CssClass="mrUrlapMezoCenterBoldUnderLine"
                                                    runat="server" Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Atadandok_Mappak">
                                            <asp:HyperLink ID="HyperLink_Atadandok_Mappak" runat="server">
                                                <asp:Label ID="Label_Atadandok_Mappak" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold">
                                            <asp:HyperLink ID="HyperLink_Sajat_Mappak" runat="server">
                                                <asp:Label ID="Label_Sajat_Mappak" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                    Text="0"></asp:Label>
                                            </asp:HyperLink>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Mappak_Irattarozasra">
                                                <asp:Label ID="Label_Jovahagyando_Mappak_Irattarozasra" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Mappak_Kolcsonzesre">
                                                <asp:Label ID="Label_Jovahagyando_Mappak_Kolcsonzesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Mappak_Kikeresre">
                                                <asp:Label ID="Label_Jovahagyando_Mappak_Kikeresre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Mappak_SkontrobaHelyezes">
                                                <asp:Label ID="Label_Jovahagyando_Mappak_SkontrobaHelyezes" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoCenterBold" runat="server" id="td_Jovahagyando_Mappak_Elintezesre">
                                                <asp:Label ID="Label_Jovahagyando_Mappak_Elintezesre" CssClass="mrUrlapMezoCenterBold"
                                                    runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table cellspacing="0" cellpadding="0" id="tableEngedelyezettKikeronLevo" runat="server">
                              <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                        </td>
                                </tr>
                                <tr>
                                    <td class="mrUrlapCaption_short" style="width:auto;padding-left:10px;">
                                        <asp:Label ID="captionEngedelyzettKikeronLevo" runat="server" Text="Enged�lyezett kik�r�n l�v� �gyiratok:"></asp:Label>
                                    </td>   
                                    <td class="mrUrlapMezoCenterBold" runat="server" id="td8">
                                        <asp:HyperLink ID="HyperLink_EngedelyzettKikeronLevo" runat="server">
                                            <asp:Label ID="labelEngedelyzettKikeronLevo" CssClass="mrUrlapMezoCenterBoldUnderLine" runat="server"
                                                Text="0"></asp:Label>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </table>
        </eUI:eFormPanel>
    </ContentTemplate>
</asp:UpdatePanel>
