using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_FeladataimPanel : System.Web.UI.UserControl
{
    private const string FunkcioKod_UgyiratAtvetel = "UgyiratAtvetel";
    private const string FunkcioKod_IratPeldanyAtvetel = "IratPeldanyAtvetel";
    private const string FunkcioKod_KuldemenyAtvetel = "KuldemenyAtvetel";
    private const string FunkcioKod_MappakAtvetel = "MappakAtvetel";
    private const string FunkcioKod_AtvetelSzervezettol = "AtvetelSzervezettol";
    private const string FunkcioKod_Atveendok = "Atveendok";

    private const string FunkcioKod_UgyiratAtadas = "UgyiratAtadas";
    private const string FunkcioKod_IratPeldanyAtadas = "IratPeldanyAtadas";
    private const string FunkcioKod_KuldemenyAtadas = "KuldemenyAtadas";
    private const string FunkcioKod_MappakAtadas = "MappakAtadas";
    private const string FunkcioKod_Atadandok = "Atadandok";

    private const string CssClass_NormalField = "mrUrlapMezoCenterBold";
    private const string CssClass_HyperLink = "mrUrlapMezoCenterBoldUnderLine";

    private readonly System.Drawing.Color colorNoFunctionRight = System.Drawing.Color.Red;

    public bool Active
    {
        get
        {
            if (hfActive.Value == "1")
                return true;
            return false;
        }
        set
        {
            if(value)
            {
                EFormPanel1.Visible = true;
                hfActive.Value = "1";
            }
            else
            {
                EFormPanel1.Visible = false;
                hfActive.Value = String.Empty;
            }
            
        }
    }

    private void SetComponentsVisibilityByVezeto(bool value)
    {

        bool isIrattarozasJovahagyasEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_JOVAHAGYAS_ENABLED);
        bool isKolcsonzesJovahagyasEnabled = true;
        bool isSkontroJovahagyasEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.SKONTRO_JOVAHAGYAS_ENABLED);
        bool isElintezesreJovahagyasEnabled = true;
        bool isKikeresJovahagyasEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES, false);

        bool isAnyJovahagyasEnabled = (isIrattarozasJovahagyasEnabled || isKolcsonzesJovahagyasEnabled 
            || isSkontroJovahagyasEnabled || isElintezesreJovahagyasEnabled || isKikeresJovahagyasEnabled);

        // EB 2009.03.27: Al��rand�k kiv�ve
        td_Jovahagyandok_Ossz.Visible = (value && isAnyJovahagyasEnabled);
        td_Jovahagyandok_Ossz_Irattarozasra.Visible = (value && isIrattarozasJovahagyasEnabled);
        td_Jovahagyandok_Ossz_Kolcsonzesre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyandok_Ossz_Kikeresre.Visible = (value && isKikeresJovahagyasEnabled);
        td_Jovahagyandok_Ossz_Elintezesre.Visible = (value && isElintezesreJovahagyasEnabled);
        td_Jovahagyandok_Ossz_SkontrobaHelyezes.Visible = (value && isSkontroJovahagyasEnabled);
        td_Jovahagyando_Ugyiratok_Irattarozasra.Visible = (value && isIrattarozasJovahagyasEnabled);
        td_Jovahagyando_Ugyiratok_Kolcsonzesre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyando_Ugyiratok_Kikeresre.Visible =  (value && isKikeresJovahagyasEnabled);
        td_Jovahagyando_Ugyiratok_Elintezesre.Visible = (value && isElintezesreJovahagyasEnabled);
        td_Jovahagyando_Ugyiratok_SkontobaHelyezes.Visible = (value && isSkontroJovahagyasEnabled);
        td_Jovahagyando_kuld_Irattarozasra.Visible = (value && isIrattarozasJovahagyasEnabled);
        td_Jovahagyando_kuld_Kolcsonzesre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyando_kuld_Kikeresre.Visible = (value && isKikeresJovahagyasEnabled);
        td_Jovahagyando_kuld_Elintezesre.Visible = (value && isElintezesreJovahagyasEnabled);
        td_Jovahagyando_kuld_SkontrobaHelyezes.Visible = (value && isSkontroJovahagyasEnabled);
        td_Jovahagyando_IratPeldanyok_Irattarozasra.Visible = (value && isIrattarozasJovahagyasEnabled);
        td_Jovahagyando_IratPeldanyok_Kolcsonzesre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyando_IratPeldanyok_Kikeresre.Visible = (value && isKikeresJovahagyasEnabled);
        td_Jovahagyando_IratPeldanyok_Elintezesre.Visible = (value && isElintezesreJovahagyasEnabled);
        td_Jovahagyando_IratPeldanyok_SkontrobaHelyezes.Visible = (value && isSkontroJovahagyasEnabled);
        td_Jovahagyando_Mappak_Irattarozasra.Visible = (value && isIrattarozasJovahagyasEnabled);
        td_Jovahagyando_Mappak_Kolcsonzesre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyando_Mappak_Kikeresre.Visible = (value && isKolcsonzesJovahagyasEnabled);
        td_Jovahagyando_Mappak_Elintezesre.Visible = (value && isElintezesreJovahagyasEnabled);
        td_Jovahagyando_Mappak_SkontrobaHelyezes.Visible = (value && isSkontroJovahagyasEnabled);
    }

    private void FormatLabelNoFunctionRight(Label label)
    {
        label.CssClass = CssClass_NormalField;

        int value;
        if (Int32.TryParse(label.Text, out value))
        {
            if (value == 0)
            {
                label.Text = "-";
            }
            else if (value > 0)
            {
                label.ForeColor = colorNoFunctionRight;
                label.Text = String.Format("{0} (!)", value);
                label.ToolTip = Resources.Error.UIDontHaveFunctionRights;
            }
        }
    }

    private void SetComponentsVisibilityByFunctionRights()
    {
        #region Atveendok
        bool isAtvetelSzervezettolEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol);
        bool isAtvetelUgyiratSzemelyEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel);
        bool isAtvetelKuldSzemelyEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel);
        bool isAtvetelPldSzemelyEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel);
        bool isAtvetelMappakSzemelyEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_MappakAtvetel);


        bool isAnyAtvetelSzemelyEnabled = (isAtvetelUgyiratSzemelyEnabled || isAtvetelKuldSzemelyEnabled || isAtvetelPldSzemelyEnabled || isAtvetelMappakSzemelyEnabled);
        bool isAnyAtvetelEnabled = (isAnyAtvetelSzemelyEnabled || isAtvetelSzervezettolEnabled);
        bool isBothAtvetelEnabled = (isAnyAtvetelSzemelyEnabled && isAtvetelSzervezettolEnabled);

        td_Atveendok_Ossz.Visible = isAnyAtvetelEnabled;

        // ha valamelyik �tveend�k oszlop nem l�that�, a c�mk�k l�that�s�g�t, sz�veg�t igaz�tani kell:
        if (isAnyAtvetelEnabled && !isBothAtvetelEnabled)
        {
            // mivel a t�bl�zatmez� elt�ntet�s�vel elcs�szn�nak a m�g�tte l�v� mez�k, csak a tartalm�t t�ntetj�k el
            foreach (Control control in td_Atveendok_Ossz.Controls)
            {
                control.Visible = false;
            }

            // az �tveend�k c�mfeliratot elt�ntetj�k, ez�rt a r�szoszlopok c�mk�j�nek felirat�t kieg�sz�tj�k
            if (!IsPostBack)
            {
                if (isAtvetelSzervezettolEnabled)
                {
                    Label_Header_Atveendok_Szervezet.Text = String.Format("{0} - {1}", Label_Header_Atveendok.Text, Label_Header_Atveendok_Szervezet.Text);
                }
                else
                {
                    Label_Header_Atveendok_Szemely.Text = String.Format("{0} - {1}", Label_Header_Atveendok.Text, Label_Header_Atveendok_Szemely.Text);
                }
            }
        }

        td_Atveendok_Szemely_Ossz.Visible = isAnyAtvetelSzemelyEnabled;
        td_Atveendok_Szemely_Ugyirat.Visible = isAnyAtvetelSzemelyEnabled;
        td_Atveendok_Szemely_Kuld.Visible = isAnyAtvetelSzemelyEnabled;
        td_Atveendok_Szemely_Pld.Visible = isAnyAtvetelSzemelyEnabled;
        td_Atveendok_Szemely_Mappak.Visible = isAnyAtvetelSzemelyEnabled;

        td_Atveendok_Szervezet_Ossz.Visible = isAtvetelSzervezettolEnabled;
        td_Atveendok_Szervezet_Ugyirat.Visible = isAtvetelSzervezettolEnabled;
        td_Atveendok_Szervezet_Kuld.Visible = isAtvetelSzervezettolEnabled;
        td_Atveendok_Szervezet_Pld.Visible = isAtvetelSzervezettolEnabled;
        td_Atveendok_Szervezet_Mappak.Visible = isAtvetelSzervezettolEnabled;

        // ha az �tveend�k szem�lyre oszlop l�that�, de nem minden elem�hez van joga,
        // a tiltott mez�k linkjeit levessz�k, �s tartalm�t kicser�lj�k "-"-re
        if (isAnyAtvetelSzemelyEnabled)
        {
            if (!isAtvetelUgyiratSzemelyEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atveendok_Ugyirat);
                HyperLink_Atveendok_Szemely_Ugyirat.NavigateUrl = "";
            }

            if (!isAtvetelKuldSzemelyEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atveendok_Kuld);
                HyperLink_Atveendok_Szemely_Kuld.NavigateUrl = "";
            }

            if (!isAtvetelPldSzemelyEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atveendok_Pld);
                HyperLink_Atveendok_Szemely_Pld.NavigateUrl = "";
            }

            if (!isAtvetelMappakSzemelyEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atveendok_Mappak);
                HyperLink_Atveendok_Szemely_Mappak.NavigateUrl = "";
            }
        }
        #endregion Atveendok

        #region Atadandok
        bool isAtadasUgyiratEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas);
        bool isAtadasKuldEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtadas);
        bool isAtadasPldEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtadas);
        bool isAtadasMappakEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_MappakAtadas);

        bool isAnyAtadasEnabled = (isAtadasUgyiratEnabled || isAtadasKuldEnabled || isAtadasPldEnabled || isAtadasMappakEnabled);

        td_Atadandok_Ossz.Visible = isAnyAtadasEnabled;
        td_Atadandok_Ugyirat.Visible = isAnyAtadasEnabled;
        td_Atadandok_Kuld.Visible = isAnyAtadasEnabled;
        td_Atadandok_Pld.Visible = isAnyAtadasEnabled;
        td_Atadandok_Mappak.Visible = isAnyAtadasEnabled;

        // ha az �tadand�k oszlop l�that�, de nem minden elem�hez van joga,
        // a tiltott mez�k linkjeit levessz�k, �s tartalm�t kicser�lj�k "-"-re
        if (isAnyAtadasEnabled)
        {
            if (!isAtadasUgyiratEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atadandok_Ugyirat);
                HyperLink_Atadandok_Ugyirat.NavigateUrl = "";
            }

            if (!isAtadasKuldEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atadandok_Kuld);
                HyperLink_Atadandok_Kuld.NavigateUrl = "";
            }

            if (!isAtadasPldEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atadandok_Pld);
                HyperLink_Atadandok_Pld.NavigateUrl = "";
            }

            if (!isAtadasMappakEnabled)
            {
                FormatLabelNoFunctionRight(Label_Atadandok_Mappak);
                HyperLink_Atadandok_Mappak.NavigateUrl = "";
            }
        }
        #endregion Atadandok
    }

    private bool isExecutorUserVezeto()
    {
        string Felhasznalo_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //Contentum.eUtility.FelhasznaloProfil f = (Contentum.eUtility.FelhasznaloProfil)Page.Session[Constants.FelhasznaloProfil];
        //string Felhasznalo_Id = f.Felhasznalo.Id;

        try
        {
            List<string> vezetettCsoportok = Contentum.eUtility.Csoportok.GetFelhasznaloVezetettCsoportjai(Page, Felhasznalo_Id);
            if (vezetettCsoportok != null && vezetettCsoportok.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    
    }

    public UpdatePanel MainUpdatePanel
    {
        get { return FeladatokUpdatePanel; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        string IraKezbesitesiTetelekListPath = "~/IraKezbesitesiTetelekList.aspx";
        string UgyUgyiratokListPath = "~/UgyUgyiratokList.aspx";
        string KuldKuldemenyekListPath = "~/KuldKuldemenyekList.aspx";
        string PldIratPeldanyokListPath = "~/PldIratPeldanyokList.aspx";
        string MappakListPath = "~/DosszieList.aspx";
        string IrattarListPath = "~/IrattarList.aspx";

        string StartupKozpontiIrattar = QueryStringVars.Startup + "=" + Constants.Startup.FromKozpontiIrattar;
        string StartupAtmenetiIrattar = QueryStringVars.Startup + "=" + Constants.Startup.FromAtmenetiIrattar;
        string StartupFromFeladataim = QueryStringVars.Startup + "=" + Constants.Startup.FromFeladataim;

        string FilterJovahagyandok = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.Jovahagyandok;
        string FilterJovahagyandokIrattarozasra = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.Irattarozasra;
        string FilterJovahagyandokKolcsonzesre = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.Kolcsonzesre;
        string FilterJovahagyandokKikeresre = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.Kikeresre;
        string FilterJovahagyandokSkontrobaHelyezes = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.SkontrobaHelyezes;
        string FilterJovahagyandokElintezesre = QueryStringVars.Filter + "=" + Constants.JovahagyandokFilter.Elintezesre;
        string FilterSajat = QueryStringVars.Filter + "=" + Constants.Sajat;
        string FilterSajatMappa = QueryStringVars.Filter + "=" + Constants.SajatMappa; // speci�lis kezel�s, m�sk�pp a kapcsol�d� list�kon is sz�rn�nk
        string FilterKezbesitesiTetelSajat = QueryStringVars.Filter + "=" + Constants.FilterType.IraKezbesitesiTetelekFilter.Sajat;
        string FilterKezbesitesiTetelSzervezet = QueryStringVars.Filter + "=" + Constants.FilterType.IraKezbesitesiTetelekFilter.Szervezet;
        string ModeAtveendok = QueryStringVars.Mode + "=" + CommandName.Atveendok;
        string ModeAtadandok = QueryStringVars.Mode + "=" + CommandName.Atadandok;
        string TableNameKuldemeny = QueryStringVars.TableName + "=" + Constants.TableNames.EREC_KuldKuldemenyek;
        string TableNameUgyirat = QueryStringVars.TableName + "=" + Constants.TableNames.EREC_UgyUgyiratok;
        string TableNameIratPeldany = QueryStringVars.TableName + "=" + Constants.TableNames.EREC_PldIratPeldanyok;
        string TableNameMappak = QueryStringVars.TableName + "=" + Constants.TableNames.KRT_Mappak;

        string FilterEngedelyezettKikeronLevo = QueryStringVars.Filter + "=" + Constants.EngedelyezettKikeronLevo;

        HyperLink_Atveendok_Ossz.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok;
        HyperLink_Atveendok_Szemely_Ossz.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + FilterKezbesitesiTetelSajat;
        HyperLink_Atveendok_Szervezet_Ossz.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + FilterKezbesitesiTetelSzervezet;
        HyperLink_Atadandok_Ossz.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtadandok;
        HyperLink_Atveendok_Szemely_Ugyirat.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameUgyirat + "&" + FilterKezbesitesiTetelSajat;
        HyperLink_Atveendok_Szervezet_Ugyirat.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameUgyirat + "&" + FilterKezbesitesiTetelSzervezet;
        HyperLink_Atadandok_Ugyirat.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtadandok + "&" + TableNameUgyirat;
        HyperLink_Sajat_Ugyiratok.NavigateUrl = UgyUgyiratokListPath + "?" + StartupFromFeladataim + "&" + FilterSajat;
        
        //HyperLink_Jovahagyando_Ugyiratok.NavigateUrl = UgyUgyiratokListPath + "?" + FilterJovahagyandok;
        HyperLink_Jovahagyando_Ugyiratok_Irattarozasra.NavigateUrl = UgyUgyiratokListPath + "?" + StartupFromFeladataim + "&" + FilterJovahagyandokIrattarozasra;
        HyperLink_Jovahagyando_Ugyiratok_Kolcsonzesre.NavigateUrl = IrattarListPath + "?" + StartupKozpontiIrattar + "&" + FilterJovahagyandokKolcsonzesre;
        HyperLink_Jovahagyando_Ugyiratok_Kikeresre.NavigateUrl = IrattarListPath + "?" + StartupAtmenetiIrattar + "&" + FilterJovahagyandokKikeresre;
        HyperLink_Jovahagyando_Ugyiratok_SkontrobaHelyezes.NavigateUrl = UgyUgyiratokListPath + "?" + StartupFromFeladataim + "&" + FilterJovahagyandokSkontrobaHelyezes;
        HyperLink_Jovahagyando_Ugyiratok_Elintezesre.NavigateUrl = UgyUgyiratokListPath + "?" + StartupFromFeladataim + "&" + FilterJovahagyandokElintezesre;
        
        HyperLink_Atveendok_Szemely_Kuld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameKuldemeny + "&" + FilterKezbesitesiTetelSajat;
        HyperLink_Atveendok_Szervezet_Kuld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameKuldemeny + "&" + FilterKezbesitesiTetelSzervezet;
        HyperLink_Atadandok_Kuld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtadandok + "&" + TableNameKuldemeny;
        HyperLink_Sajat_kuld.NavigateUrl = KuldKuldemenyekListPath + "?" + StartupFromFeladataim + "&" + FilterSajat;

        HyperLink_Atveendok_Szemely_Pld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameIratPeldany + "&" + FilterKezbesitesiTetelSajat;
        HyperLink_Atveendok_Szervezet_Pld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameIratPeldany + "&" + FilterKezbesitesiTetelSzervezet;
        HyperLink_Atadandok_Pld.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtadandok + "&" + TableNameIratPeldany;
        HyperLink_Sajat_IratPeldanyok.NavigateUrl = PldIratPeldanyokListPath + "?" + StartupFromFeladataim + "&" + FilterSajat;

        HyperLink_Atveendok_Szemely_Mappak.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameMappak + "&" + FilterKezbesitesiTetelSajat;
        HyperLink_Atveendok_Szervezet_Mappak.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtveendok + "&" + TableNameMappak + "&" + FilterKezbesitesiTetelSzervezet;
        HyperLink_Atadandok_Mappak.NavigateUrl = IraKezbesitesiTetelekListPath + "?" + StartupFromFeladataim + "&" + ModeAtadandok + "&" + TableNameMappak;
        HyperLink_Sajat_Mappak.NavigateUrl = MappakListPath + "?" + StartupFromFeladataim + "&" + FilterSajatMappa; // speci�lis kezel�s, m�sk�pp a kapcsol�d� list�kon is sz�rn�nk

        HyperLink_EngedelyzettKikeronLevo.NavigateUrl = IrattarListPath + "?" + StartupKozpontiIrattar + "&" + FilterEngedelyezettKikeronLevo; 
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //FeladatokTab1.ParentForm = Constants.ParentForms.Feladataim;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }
        }
    }

    protected void FeladatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshFeladatok:
                    ReloadDatas();
                    break;
            }
        }
    }



    public void ReloadDatas()
    {
        if (!Active) return;

        bool isVezeto = isExecutorUserVezeto();
        SetComponentsVisibilityByVezeto(isVezeto);

        EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.Feladatok_GetSummary(execParam);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else
        {
            Label_Idopont.Text = DateTime.Now.ToString();

            DataTable table_Atadandok = result.Ds.Tables[0];
            DataTable table_Atveendok_Szemelyre = result.Ds.Tables[1];
            DataTable table_Atveendok_Szervezetre = result.Ds.Tables[2];
            DataTable table_Sajat_obj = result.Ds.Tables[3];
            DataTable table_Jovahagyandok_obj_Irattarozasra = result.Ds.Tables[4];
            DataTable table_Jovahagyandok_obj_Kolcsonzesre = result.Ds.Tables[5];
            // EB 2009.03.27: Al��rand�k kiv�ve
            //DataTable table_Jovahagyandok_obj_Alairasra = result.Ds.Tables[6];
            DataTable table_Jovahagyandok_obj_SkontrobaHelyezes = result.Ds.Tables[6]; //result.Ds.Tables[7];
            DataTable table_Jovahagyandok_Elintezesre = result.Ds.Tables[7];
            DataTable table_Jovahagyandok_obj_Kikeresre = result.Ds.Tables[9];

            try
            {

                #region �tadand�k

                foreach (DataRow row in table_Atadandok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    // Ellen�rz�s:
                    if (allapot != Constants.KezbesitesiTetel_Allapot.Atadasra_kijelolt)
                    {
                        throw new Exception();
                    }
                    string obj_type = row["Obj_type"].ToString();
                    string darab = row["Darab"].ToString();

                    switch (obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            Label_Atadandok_Ugyirat.Text = darab;
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            Label_Atadandok_Kuld.Text = darab;
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            Label_Atadandok_Pld.Text = darab;
                            break;
                        case Constants.TableNames.KRT_Mappak:
                            Label_Atadandok_Mappak.Text = darab;
                            break;
                    }
                }

                try
                {
                    int atadandok_ossz = 0;
                    atadandok_ossz += Int32.Parse(Label_Atadandok_Ugyirat.Text);
                    atadandok_ossz += Int32.Parse(Label_Atadandok_Kuld.Text);
                    atadandok_ossz += Int32.Parse(Label_Atadandok_Pld.Text);
                    atadandok_ossz += Int32.Parse(Label_Atadandok_Mappak.Text);

                    Label_Atadandok_Ossz.Text = "(" + atadandok_ossz + ")";
                }
                catch { }

                #endregion

                #region Atveendok

                int atveendok_ossz = 0; // gy�jt� sz�ml�l�

                #region Atveendok szemelyre

                foreach (DataRow row in table_Atveendok_Szemelyre.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    // Ellen�rz�s:
                    if (allapot != Constants.KezbesitesiTetel_Allapot.Atadott)
                    {
                        throw new Exception();
                    }
                    string obj_type = row["Obj_type"].ToString();
                    string darab = row["Darab"].ToString();

                    switch (obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            Label_Atveendok_Ugyirat.Text = darab;
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            Label_Atveendok_Kuld.Text = darab;
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            Label_Atveendok_Pld.Text = darab;
                            break;
                        case Constants.TableNames.KRT_Mappak:
                            Label_Atveendok_Mappak.Text = darab;
                            break;
                    }
                }

                try
                {
                    int atveendok_szemely_ossz = 0;
                    atveendok_szemely_ossz += Int32.Parse(Label_Atveendok_Ugyirat.Text);
                    atveendok_szemely_ossz += Int32.Parse(Label_Atveendok_Kuld.Text);
                    atveendok_szemely_ossz += Int32.Parse(Label_Atveendok_Pld.Text);
                    atveendok_szemely_ossz += Int32.Parse(Label_Atveendok_Mappak.Text);

                    Label_Atveendok_Szemely_Ossz.Text = "(" + atveendok_szemely_ossz + ")";

                    atveendok_ossz += atveendok_szemely_ossz;
                }
                catch { }
                #endregion Atveendok szemelyre

                #region Atveendok szervezetre

                foreach (DataRow row in table_Atveendok_Szervezetre.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    // Ellen�rz�s:
                    if (allapot != Constants.KezbesitesiTetel_Allapot.Atadott)
                    {
                        throw new Exception();
                    }
                    string obj_type = row["Obj_type"].ToString();
                    string darab = row["Darab"].ToString();

                    switch (obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            Label_Atveendok_Szervezet_Ugyirat.Text = darab;
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            Label_Atveendok_Szervezet_Kuld.Text = darab;
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            Label_Atveendok_Szervezet_Pld.Text = darab;
                            break;
                        case Constants.TableNames.KRT_Mappak:
                            Label_Atveendok_Szervezet_Mappak.Text = darab;
                            break;
                    }
                }

                try
                {
                    int atveendok_szervezet_ossz = 0;
                    atveendok_szervezet_ossz += Int32.Parse(Label_Atveendok_Szervezet_Ugyirat.Text);
                    atveendok_szervezet_ossz += Int32.Parse(Label_Atveendok_Szervezet_Kuld.Text);
                    atveendok_szervezet_ossz += Int32.Parse(Label_Atveendok_Szervezet_Pld.Text);
                    atveendok_szervezet_ossz += Int32.Parse(Label_Atveendok_Szervezet_Mappak.Text);

                    Label_Atveendok_Szervezet_Ossz.Text = "(" + atveendok_szervezet_ossz + ")";

                    atveendok_ossz += atveendok_szervezet_ossz;
                }
                catch { }
                #endregion Atveendok szervezetre

                Label_Atveendok_Ossz.Text = "(" + atveendok_ossz + ")";

                #endregion Atveendok

                #region Saj�t �gyirat, k�ldem�ny, iratp�ld�ny, dosszi�

                DataRow row_sajat = table_Sajat_obj.Rows[0];
                if (row_sajat != null)
                {
                    string sajat_ugyirat = row_sajat["Sajat_Ugyiratok"].ToString();
                    string sajat_kuld = row_sajat["Sajat_Kuldemenyek"].ToString();
                    string sajat_pld = row_sajat["Sajat_IratPeldanyok"].ToString();
                    string sajat_mappak = row_sajat["Sajat_Mappak"].ToString();

                    Label_Sajat_Ugyiratok.Text = sajat_ugyirat;
                    Label_Sajat_kuld.Text = sajat_kuld;
                    Label_Sajat_IratPeldanyok.Text = sajat_pld;
                    Label_Sajat_Mappak.Text = sajat_mappak;
                }

                try
                {
                    int sajat_ossz = 0;
                    sajat_ossz += Int32.Parse(Label_Sajat_Ugyiratok.Text);
                    sajat_ossz += Int32.Parse(Label_Sajat_kuld.Text);
                    sajat_ossz += Int32.Parse(Label_Sajat_IratPeldanyok.Text);
                    sajat_ossz += Int32.Parse(Label_Sajat_Mappak.Text);

                    Label_Sajat_Ossz.Text = "(" + sajat_ossz + ")";
                }
                catch { }

                #endregion

                #region J�v�hagyand� �gyirat, iratp�ld�ny (k�ldem�nyre, mapp�ra nem �rtelmezett)

                // �gyirat j�v�hagyhat� iratt�roz�sra, k�lcs�nz�sre
                // Iratp�ld�ny j�v�hagy�sa az al��r�s sz�ks�gess�g�t jelenti
                // a t�bbi eset nincs �rtelmezve (�gyirat al��r�s, iratp�ld�ny k�lcs�nz�s stb.)

                #region Jovahagyandok
                if (isVezeto)
                {
                    int jovahagyandok_ossz = 0; // gy�jt� sz�ml�l�                

                    #region Jovahagyandok irattarozasra

                    DataRow row_jovahagyandok_Irattarozasra = table_Jovahagyandok_obj_Irattarozasra.Rows[0];
                    if (row_jovahagyandok_Irattarozasra != null)
                    {
                        string jovahagyando_ugyirat_Irattarozasra = row_jovahagyandok_Irattarozasra["Jovahagyando_Ugyiratok_Irattarozasra"].ToString();
                        //string jovahagyando_kuld_Irattarozasra = row_jovahagyandok_Irattarozasra["Jovahagyando_Kuldemenyek_Irattarozasra"].ToString();
                        //string jovahagyando_pld_Irattarozasra = row_jovahagyandok_Irattarozasra["Jovahagyando_IratPeldanyok_Irattarozasra"].ToString();

                        Label_Jovahagyando_Ugyiratok_Irattarozasra.Text = jovahagyando_ugyirat_Irattarozasra;
                        //Label_Jovahagyando_kuld_Irattarozasra.Text = jovahagyando_kuld_Irattarozasra;
                        //Label_Jovahagyando_IratPeldanyok_Irattarozasra.Text = jovahagyando_pld_Irattarozasra;
                    }

                    try
                    {
                        int jovahagyandok_ossz_Irattarozasra = 0;
                        jovahagyandok_ossz_Irattarozasra += Int32.Parse(Label_Jovahagyando_Ugyiratok_Irattarozasra.Text);
                        //jovahagyandok_ossz_Irattarozasra += Int32.Parse(Label_Jovahagyando_kuld_Irattarozasra.Text);
                        //jovahagyandok_ossz_Irattarozasra += Int32.Parse(Label_Jovahagyando_IratPeldanyok_Irattarozasra.Text);

                        Label_Jovahagyandok_Ossz_Irattarozasra.Text = "(" + jovahagyandok_ossz_Irattarozasra + ")";

                        jovahagyandok_ossz += jovahagyandok_ossz_Irattarozasra;
                    }
                    catch { }
                    #endregion Jovahagyandok irattarozasra

                    #region Jovahagyandok kolcsonzesre

                    DataRow row_jovahagyandok_Kolcsonzesre = table_Jovahagyandok_obj_Kolcsonzesre.Rows[0];
                    if (row_jovahagyandok_Kolcsonzesre != null)
                    {
                        string jovahagyando_ugyirat_Kolcsonzesre = row_jovahagyandok_Kolcsonzesre["Jovahagyando_Ugyiratok_Kolcsonzesre"].ToString();
                        //string jovahagyando_kuld_Kolcsonzesre = row_jovahagyandok_Kolcsonzesre["Jovahagyando_Kuldemenyek_Kolcsonzesre"].ToString();
                        //string jovahagyando_pld_Kolcsonzesre = row_jovahagyandok_Kolcsonzesre["Jovahagyando_IratPeldanyok_Kolcsonzesre"].ToString();

                        Label_Jovahagyando_Ugyiratok_Kolcsonzesre.Text = jovahagyando_ugyirat_Kolcsonzesre;
                        //Label_Jovahagyando_kuld_Kolcsonzesre.Text = jovahagyando_kuld_Kolcsonzesre;
                        //Label_Jovahagyando_IratPeldanyok_Kolcsonzesre.Text = jovahagyando_pld_Kolcsonzesre;
                    }

                    try
                    {
                        int jovahagyandok_ossz_Kolcsonzesre = 0;
                        jovahagyandok_ossz_Kolcsonzesre += Int32.Parse(Label_Jovahagyando_Ugyiratok_Kolcsonzesre.Text);
                        //jovahagyandok_ossz_Kolcsonzesre += Int32.Parse(Label_Jovahagyando_kuld_Kolcsonzesre.Text);
                        //jovahagyandok_ossz_Kolcsonzesre += Int32.Parse(Label_Jovahagyando_IratPeldanyok_Kolcsonzesre.Text);

                        Label_Jovahagyandok_Ossz_Kolcsonzesre.Text = "(" + jovahagyandok_ossz_Kolcsonzesre + ")";

                        jovahagyandok_ossz += jovahagyandok_ossz_Kolcsonzesre;
                    }
                    catch { }
                    #endregion Jovahagyandok kolcsonzesre

                    #region Jovahagyandok kikeresre

                    DataRow row_jovahagyandok_Kikeresre = table_Jovahagyandok_obj_Kikeresre.Rows[0];
                    if (row_jovahagyandok_Kikeresre != null)
                    {
                        string jovahagyando_ugyirat_Kikeresre = row_jovahagyandok_Kikeresre["Jovahagyando_Ugyiratok_Kikeresre"].ToString();
                        Label_Jovahagyando_Ugyiratok_Kikeresre.Text = jovahagyando_ugyirat_Kikeresre;
                    }

                    try
                    {
                        int jovahagyandok_ossz_Kikeresre = 0;
                        jovahagyandok_ossz_Kikeresre = Int32.Parse(Label_Jovahagyando_Ugyiratok_Kikeresre.Text);
                        Label_Jovahagyandok_Ossz_Kikeresre.Text = "(" + jovahagyandok_ossz_Kikeresre + ")";

                        jovahagyandok_ossz += jovahagyandok_ossz_Kikeresre;
                    }
                    catch { }
                    #endregion Jovahagyandok kikeresre

                    // EB 2009.03.27: Al��rand�k kiv�ve
                    #region Jovahagyandok alairasra

                    //DataRow row_jovahagyandok_Alairasra = table_Jovahagyandok_obj_Alairasra.Rows[0];
                    //if (row_jovahagyandok_Alairasra != null)
                    //{
                    //    //string jovahagyando_ugyirat_Alairasra = row_jovahagyandok_Alairasra["Jovahagyando_Ugyiratok_Alairasra"].ToString();
                    //    //string jovahagyando_kuld_Alairasra = row_jovahagyandok_Alairasra["Jovahagyando_Kuldemenyek_Alairasra"].ToString();
                    //    string jovahagyando_pld_Alairasra = row_jovahagyandok_Alairasra["Jovahagyando_IratPeldanyok_Alairasra"].ToString();

                    //    //Label_Jovahagyando_Ugyiratok_Alairasra.Text = jovahagyando_ugyirat_Alairasra;
                    //    //Label_Jovahagyando_kuld_Alairasra.Text = jovahagyando_kuld_Alairasra;
                    //    Label_Jovahagyando_IratPeldanyok_Alairasra.Text = jovahagyando_pld_Alairasra;
                    //}

                    //try
                    //{
                    //    int jovahagyandok_ossz_Alairasra = 0;
                    //    //jovahagyandok_ossz_Alairasra += Int32.Parse(Label_Jovahagyando_Ugyiratok_Alairasra.Text);
                    //    //jovahagyandok_ossz_Alairasra += Int32.Parse(Label_Jovahagyando_kuld_Alairasra.Text);
                    //    jovahagyandok_ossz_Alairasra += Int32.Parse(Label_Jovahagyando_IratPeldanyok_Alairasra.Text);

                    //    Label_Jovahagyandok_Ossz_Alairasra.Text = "(" + jovahagyandok_ossz_Alairasra + ")";

                    //    jovahagyandok_ossz += jovahagyandok_ossz_Alairasra;
                    //}
                    //catch { }
                    #endregion Jovahagyandok alairasra

                    #region Jovahagyandok Skontroba helyezesre

                    DataRow row_jovahagyandok_SkontrobaHelyezes = table_Jovahagyandok_obj_SkontrobaHelyezes.Rows[0];
                    if (row_jovahagyandok_SkontrobaHelyezes != null)
                    {
                        string jovahagyando_Ugyiratok_SkontrobaHelyezes = row_jovahagyandok_SkontrobaHelyezes["Jovahagyando_Ugyiratok_SkontrobaHelyezes"].ToString();

                        Label_Jovahagyando_Ugyiratok_SkontrobaHelyezes.Text = jovahagyando_Ugyiratok_SkontrobaHelyezes;
                    }

                    try
                    {
                        int jovahagyandok_ossz_SkontrobaHelyezes = 0;
                        jovahagyandok_ossz_SkontrobaHelyezes += Int32.Parse(Label_Jovahagyando_Ugyiratok_SkontrobaHelyezes.Text);

                        Label_Jovahagyandok_Ossz_SkontrobaHelyezesCount.Text = "(" + jovahagyandok_ossz_SkontrobaHelyezes + ")";

                        jovahagyandok_ossz += jovahagyandok_ossz_SkontrobaHelyezes;
                    }
                    catch { }

                    #endregion

                    #region Jov�hagyand�k elint�z�sre
                    jovahagyandok_ossz += SetJovahagyandokElintezesre(table_Jovahagyandok_Elintezesre);
                    #endregion

                    Label_Jovahagyandok_Ossz.Text = "(" + jovahagyandok_ossz + ")";
                }
                #endregion Jovahagyandok
                #endregion

                #region Engedelyezett kik�r�n l�v�

                if (ShowEngedelyzettKikeronLevo())
                {
                    tableEngedelyezettKikeronLevo.Visible = true;

                    if (result.Ds.Tables.Count > 8)
                    {
                        DataTable table_EngedelyettKikeronLevo = result.Ds.Tables[8];

                        if (table_EngedelyettKikeronLevo.Rows.Count > 0)
                        {
                            #region LZS - BUG_11401
                            //Overwrite table_EngedelyettKikeronLevo label value with Jogosultak:
                            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
                            ExecParam execParamFeladataim = execParam;
                            execParamFeladataim.FunkcioKod ="KozpontiIrattarList";

                            if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"))
                            {
                                Ugyiratok.SetSearchObjectTo_EngedelyezettKikeronLevo(search, Page);
                            }
                            else
                            {
                                // ne l�sson semmit
                                search.Id.IsNull();
                            }
                            Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);

                            var res = UtilityIrattar.GetIrattarList(Page, execParamFeladataim, "FromKozpontiIrattar", "EREC_UgyUgyiratok.LetrehozasIdo DESC", out search);

                            labelEngedelyzettKikeronLevo.Text = res.Ds.Tables[1].Rows[0].ItemArray[0].ToString();
                            
                            //Old version:
                            //labelEngedelyzettKikeronLevo.Text = table_EngedelyettKikeronLevo.Rows[0]["EngedelyettKikeronLevo_Ugyiratok"].ToString();
                            #endregion
                        }

                    }
                }
                else
                {
                    tableEngedelyezettKikeronLevo.Visible = false;
                }

                #endregion

                // funkci�jog hi�ny�t�l f�gg� mez�elt�ntet�sek
                SetComponentsVisibilityByFunctionRights();
            }
            catch
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    Resources.Error.ErrorText_Query);
                ErrorUpdatePanel.Update();
            }

        }
    }

    private int SetJovahagyandokElintezesre(DataTable table_Jovahagyandok_Elintezesre)
    {
        int countJovahagyandoUgyiratokElintezesre = GetIntValue(table_Jovahagyandok_Elintezesre, 0, "Jovahagyando_Ugyiratok_Elintezesre");
        
        Label_Jovahagyando_Ugyiratok_Elintezesre.Text = countJovahagyandoUgyiratokElintezesre.ToString();
        Label_Jovahagyandok_Ossz_ElintezesreCount.Text = String.Format("({0})", countJovahagyandoUgyiratokElintezesre);

        return countJovahagyandoUgyiratokElintezesre;
    }

    private static int GetIntValue(DataTable table, int rowNumber, string columnName)
    {
        if (table == null)
            return 0;

        if (table.Rows.Count > rowNumber && table.Columns.Contains(columnName))
        {
            string value = table.Rows[rowNumber][columnName].ToString();

            int iValue;
            if (Int32.TryParse(value, out iValue))
            {
                return iValue;
            }
        }

        return 0;
    }

    bool ShowEngedelyzettKikeronLevo()
    {
        bool hasFunction = FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa");

        if (!hasFunction)
        {
            return false;
        }
        else
        {
            string kozpontiIrattar = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id;
            //bool isKozpontiIrattaros = (FelhasznaloProfil.FelhasznaloSzerverzetId(Page) == kozpontiIrattar);

            bool hasKozpontiIrattar = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
            search.Csoport_Id_Jogalany.Value = FelhasznaloProfil.FelhasznaloId(Page);
            search.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            search.Csoport_Id.Value = kozpontiIrattar;
            search.Csoport_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(UI.SetExecParamDefault(Page), search);

            if (!result.IsError)
            {
                hasKozpontiIrattar = result.Ds.Tables[0].Rows.Count > 0;
            }

            return hasKozpontiIrattar;
        }
    }
}
