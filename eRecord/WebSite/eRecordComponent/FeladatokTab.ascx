<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeladatokTab.ascx.cs" Inherits="eRecordComponent_UgyiratFeladatokTab" %>

<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="uc3" %>
<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="fcstb" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc3" %>
<%@ Register Src="~/Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register src="~/Component/ListSortPopup.ascx" tagname="ListSortPopup" tagprefix="lsp" %>

<%@ Register src="~/eRecordComponent/ObjektumValasztoPanel.ascx" tagname="ObjektumValasztoPanel" tagprefix="uc4" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<%-- ListHeader --%>
<lh:ListHeader ID="ListHeaderInner" runat="server"/>
                        
<asp:Panel ID="panelWrapper" runat="server">  

<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
 
<%--Hiba megjelenites--%>    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>														                
<asp:UpdatePanel ID="FeladatokUpdatePanel" runat="server" OnLoad="FeladatokUpdatePanel_Load">
    <ContentTemplate>
        <asp:HiddenField ID="hfActive" runat="server"/>
        <asp:Panel ID="MainPanel" runat="server">
          <ajaxToolkit:CollapsiblePanelExtender ID="FeladatokCPE" runat="server" TargetControlID="Panel1"
           CollapsedSize="20" Collapsed="False" 
            AutoCollapse="false" AutoExpand="false"    
           ScrollContents="true" ExpandedSize="300">
           </ajaxToolkit:CollapsiblePanelExtender>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="hfPageIndex" runat="server" Value="0"/>
            <asp:HiddenField ID="hfSortExpression" runat="server"/>
            
            <asp:UpdatePanel ID="DolgozoUpdatePanel" runat="server" Visible="false">
                    <ContentTemplate>
                        <eUI:eFormPanel ID="DolgozoPanel" runat="server" Visible="false" style="padding-bottom:5px;">
                        <div style="width:100%;text-align:left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDolgozo" runat="server" Text="Dolgoz�:" />
                                    </td>
                                    <td style="text-align: left;">
                                        <fcstb:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox_Dolgozo" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        </eUI:eFormPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            
            <table cellpadding="0" cellspacing="0" style="text-align:left;width:100%">
                <tr>
                    <td>
                        <%--SubHeader --%>
                        <asp:Panel runat="server" ID="panelSubHeader" style="white-space:nowrap; height:20px;vertical-align:middle;text-align:left;width:100%;padding-bottom:10px;">
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div style="display:inline;padding-right:5px;font-weight:bold">
                                     <asp:Label runat="server" ID="labelViewMode" Text="N�zet:" Font-Bold="true"></asp:Label>
                                </div>
                            </td>
                            <td>
                            <div style="display:inline;padding-right:10px;">
                               <asp:DropDownList ID="ddListTreeView_ViewMode" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddListTreeView_ViewMode_SelectedIndexChanged">
                                    <asp:ListItem Text="Hierarchikus" Value="H" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Lista" Value="K"></asp:ListItem>
                                </asp:DropDownList>
                             </div>
                            </td>
                            <td>
                                <div style="display:inline;white-space:nowrap;">
                                    <div id="FilteredObjektumPanel" runat="server" style="display:inline;" Visible="false">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding-top:4px;">
                                                    A(z)&nbsp;<asp:HyperLink ID="linkFilteredObjektumAzonosito" runat="server" CssClass="linkStyle" ToolTip="ugr�s" Target="_blank" Font-Bold="true"/>&nbsp;-s&nbsp;<asp:Label ID="labelFilteredObjektumType" runat="server" Font-Bold="true"/>
                                                    <uc4:ObjektumValasztoPanel ID="FilteredObjektum" runat="server" ReadOnly="true"/>
                                                </td>
                                                <td>
                                                <asp:Label runat="server" id="panelUseUgyirethierarchia">
                                                    &nbsp;iratkezel�si elemeihez (pl. p�ld�nyokhoz) tartoz� kezel�si feljegyz�sek is&nbsp;<asp:CheckBox ID="cbUseUgyiratHierarchia" runat="server" Text="" Checked="true" AutoPostBack="true"/>
                                                </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td> 
                        </tr>
                        </table>
                        </asp:Panel>
                        <%-- NodeFilter --%>
                        <asp:HiddenField ID="Filter_HiddenField" runat="server" />
                        <%-- /NodeFilter --%>
                        <div style="border: inset 1px Silver;vertical-align:top;">
                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                        <asp:Panel ID="PanelScroll" runat="server" style="overflow:auto;overflow-y:hidden;padding-bottom:20px;">
                            <table runat="server" id="tableFeladatok" style="background-color:White;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 100%;text-align: left;padding-left:5px;padding-top:5px;vertical-align:top;">
                                        <asp:Label ID="labelTreeViewFeladatokHeader" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; text-align: left;padding-left:5px;padding-right:5px;padding-bottom:5px;vertical-align:top">
                                        <%-- az ujonnan letrehozott node-ok kivalasztasahoz szukseges adatok tarolasara --%>
                                        <asp:HiddenField ID="SelectedNodeId_HiddenField" runat="server" />
                                        <asp:HiddenField ID="SelectedNodeName_HiddenField" runat="server" />
                                        <asp:Panel ID="Panel2" runat="server" Visible="true" style="border-left: #e6e6e6 1px solid;">
                                            <eUI:TreeGridView ID="TreeViewFeladatok" runat="server" EnableClientScript="false" 
                                                OnSelectedNodeChanged="TreeViewFeladatok_SelectedNodeChanged"
                                                OnTreeNodePopulate="TreeViewFeladatok_TreeNodePopulate"
                                                ShowLines="True"
                                                PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip=""
                                                SkipLinkText="" AllowSorting="true" OnSorting="TreeViewFeladatok_Sorting"
                                                DefaultSortExpression = "EREC_HataridosFeladatok.LetrehozasIdo" DefaultSortDirection = "Descending">
                                               <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                                               <Columns>
                                               <asp:BoundField DataField="Tipus_Nev" HeaderText="T�pus" SortExpression="KRT_KodTarak_Tipus.Nev" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Memo" HeaderText="Memo" SortExpression="" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="50px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="50px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Forras" HeaderText="Forr�s" SortExpression="" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="80px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="80px"/>
											    </asp:BoundField>                                  
                                               <asp:BoundField DataField="Funkcio_Nev_Inditando" HeaderText="Iratkezel�si feladat" SortExpression="KRT_Funkciok_Inditando.Nev" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="180px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="180px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Leiras" HeaderText="Le�r�s" SortExpression="EREC_HataridosFeladatok.Leiras">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
											    </asp:BoundField>
                                                   <%--BLG_1054--%>
											    <%--<asp:BoundField DataField="Altipus_Nev" HeaderText="Kezel�si utas�t�sok" SortExpression="KRT_KodTarak_Altipus.Nev">--%>
                                                   <asp:BoundField DataField="Altipus_Nev" HeaderText="<%$Forditas:BoundField_Altipus_Nev|Kezel�si utas�t�sok%>" SortExpression="KRT_KodTarak_Altipus.Nev">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Obj_Type" HeaderText="Elem t�pus" SortExpression="EREC_HataridosFeladatok.Obj_Type" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="EREC_HataridosFeladatok.Azonosito">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="140px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="140px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="IntezkHatarido" HeaderText="Hat�rid�" SortExpression="EREC_HataridosFeladatok.IntezkHatarido">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="110px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="110px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Prioritas_Nev" HeaderText="Priorit�s" SortExpression="EREC_HataridosFeladatok.Prioritas">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="90px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="90px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="KRT_KodTarak_Allapot.Nev">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="90px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="90px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Felelos_Nev" HeaderText="Felel�s" SortExpression="KRT_Csoportok_Felelos.Nev">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="190px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="190px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Felelos_Csoport_Nev" HeaderText="Felel�s szervezete" SortExpression="KRT_Csoportok_Felelos_Csoport.Nev" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Kiado_Nev" HeaderText="Kiad�" SortExpression="KRT_Csoportok_Kiado.Nev">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="190px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="190px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Kiado_Csoport_Nev" HeaderText="Kiad� szervezet" SortExpression="KRT_Csoportok_Kiado_Csoport.Nev" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="LetrehozasIdo" HeaderText="Kiad�s d�tuma" SortExpression="EREC_HataridosFeladatok.LetrehozasIdo" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Letrehozo_Nev" HeaderText="L�trehoz�" SortExpression="KRT_Felhasznalok_Letrehozo.Nev">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Megoldas" HeaderText="Megold�s" SortExpression="EREC_HataridosFeladatok.Megoldas">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="EREC_HataridosFeladatok.Note" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="LezarasDatuma" HeaderText="Lez�r�s d�tuma" SortExpression="EREC_HataridosFeladatok.LezarasDatuma" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Esemeny_Kivalto" HeaderText="Kiv�lt� esem�ny" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Esemeny_Lezaro" HeaderText="Lez�r� esem�ny" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="150px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="Zarolo_Nev" HeaderText="Z�rol�" SortExpression="KRT_Felhasznalok_Zarolo.Nev" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
											    <asp:BoundField DataField="ZarolasIdo" HeaderText="Z�rol�s id�pontja" SortExpression="EREC_HataridosFeladatok.ZarolasIdo" Visible="false">
												   <HeaderStyle CssClass="GridViewHeaderStyle" Width="120px" />
												   <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="120px"/>
											    </asp:BoundField>
                                               </Columns>
                                           </eUI:TreeGridView>
                                        </asp:Panel>
                                        <asp:Label runat="server" ID="labelEmptyText" Visible="false" Text="A kezel�si feljegyz�s lista �res"
                                        style="font-weight:bold;font-style:italic;font-size:14px;color:Gray">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </asp:Panel>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<uc3:KezelesiFeljegyzesPanel runat="server" ID="FeladatView" LabelLeirasText="Le�r�s:" ObjektumPanelVisible="true" Validate="true" IsOnFeladatTab="true"/>
</asp:Panel> 
