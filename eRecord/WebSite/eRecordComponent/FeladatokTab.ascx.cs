﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using Contentum.eQuery;
using System.IO;
using System.Net;

public partial class eRecordComponent_UgyiratFeladatokTab : System.Web.UI.UserControl
{
    public const string Color_HighPriority = "#FFDDDD";
    public const string Color_LowPriority = "#DFDDFF";
    public const string Color_ExpiredDate = "";
    //public Boolean _Active = false;
    public const string EmptyObject = "Nincs";
    public const string MoreObject = " . . .";

    UI ui = new UI();

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public class TreeView_ViewMode
    {
        public const string Hierarchikus = "H";
        public const string Kiteritett = "K";
    }

    public Boolean Active
    {
        get
        {
            if (hfActive.Value == "1")
                return true;
            return false;
        }
        set
        {
            if (value)
            {
                hfActive.Value = "1";
                MainPanel.Visible = true;
                InitActiveTab();
            }
            else
            {
                hfActive.Value = "0";
                TreeViewFeladatok.Nodes.Clear();
                FeladatView.Reset();
            }
        }
    }

    private void InitActiveTab()
    {
        switch (ParentForm)
        {
            case Constants.ParentForms.Feladataim:
            case Constants.ParentForms.KiadottFeladataim:
            case Constants.ParentForms.DolgozokFeladatai:
                FeladatTipus = KodTarak.FELADAT_TIPUS.Feladat;
                break;
        }

        SetColumsVisiblity();
    }

    private bool IsInitalized
    {
        get
        {
            object o = ViewState["IsInitialized"];
            if (o != null)
                return (bool)o;
            return false;
        }
        set
        {
            ViewState["IsInitialized"] = true;
        }
    }

    private void InitTab()
    {
        if (!String.IsNullOrEmpty(ParentId))
        {
            ObjektumId = ParentId;
        }
        FeladatView.Command = CommandName.View;

        RemoveSearchObject(false);

        if (TabHeader != null)
        {
            string title = Page.Title;
            ListHeaderInner.HeaderLabel = TabHeader.Text;
            ListHeaderInner.PageName = title;
        }

        IsInitalized = true;
    }

    public string Command = "";

    private string StartObjektumId
    {
        get
        {
            object o = ViewState["StartObjektumId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["StartObjektumId"] = value;
        }
    }
    public string ObjektumId
    {
        get
        {
            object o = ViewState["ObjektumId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["ObjektumId"] = value;
            if (!IsPostBack)
            {
                StartObjektumId = value;
            }
        }
    }

    private string StartObjektumType
    {
        get
        {
            object o = ViewState["StartObjektumType"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["StartObjektumType"] = value;
        }
    }

    public string ObjektumType
    {
        get
        {
            object o = ViewState["ObjektumType"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["ObjektumType"] = value;
            if (!IsPostBack)
            {
                StartObjektumType = value;
            }
        }
    }

    private bool IsUgyiratObjektumType
    {
        get
        {
            if (GetObjType() == Constants.TableNames.EREC_UgyUgyiratok
                    || GetObjType() == Constants.TableNames.EREC_IraIratok
                    || GetObjType() == Constants.TableNames.EREC_PldIratPeldanyok
                || GetObjType() == Constants.TableNames.EREC_KuldKuldemenyek)
            {
                return true;
            }

            return false;
        }
    }
    public bool UseUgyiratHierarchy
    {
        get
        {
            if (IsUgyiratObjektumType && !String.IsNullOrEmpty(ObjektumId))
            {
                return cbUseUgyiratHierarchia.Checked;
            }
            return false;
        }
        set
        {
            cbUseUgyiratHierarchia.Checked = value;
        }
    }

    private bool _HasSpecialScroll = false;

    public bool HasSpecialScroll
    {
        get { return this._HasSpecialScroll; }
    }

    public string GetStartup()
    {
        string startup = String.Empty;

        switch (ParentForm)
        {
            case Constants.ParentForms.Ugyirat:
                startup = Constants.Startup.FromUgyirat;
                break;
            case Constants.ParentForms.IraIrat:
                startup = Constants.Startup.FromIrat;
                break;
            case Constants.ParentForms.Kuldemeny:
                startup = Constants.Startup.FromKuldemeny;
                break;
            case Constants.ParentForms.IratPeldany:
                startup = Constants.Startup.FromIratPeldany;
                break;
            case Constants.ParentForms.KiadottFeladataim:
                startup = Constants.Startup.FromKiadottFeladatok;
                break;
            case Constants.ParentForms.Feladataim:
                startup = Constants.Startup.FromFeladataim;
                break;
            case Constants.ParentForms.DolgozokFeladatai:
                startup = Constants.Startup.FromDolgozokFeladatai;
                break;
            default:
                break;
        }

        return startup;
    }

    public string GetObjType()
    {
        string objType = String.Empty;

        if (!String.IsNullOrEmpty(ObjektumType))
        {
            return ObjektumType;
        }

        switch (ParentForm)
        {
            case Constants.ParentForms.Ugyirat:
                objType = Constants.TableNames.EREC_UgyUgyiratok;
                break;
            case Constants.ParentForms.IraIrat:
                objType = Constants.TableNames.EREC_IraIratok;
                break;
            case Constants.ParentForms.Kuldemeny:
                objType = Constants.TableNames.EREC_KuldKuldemenyek;
                break;
            case Constants.ParentForms.IratPeldany:
                objType = Constants.TableNames.EREC_PldIratPeldanyok;
                break;
            default:
                break;
        }

        return objType;
    }

    public string GetOrderBy()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("TreeViewFeladatok", ViewState, TreeViewFeladatok.DefaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("TreeViewFeladatok", ViewState, TreeViewFeladatok.DefaultSortDirection);
        string orderby = Search.GetOrderBy("TreeViewFeladatok", ViewState, sortExpression, sortDirection);

        //if (IsListHeader)
        //{
        //    orderby = _listheader.ListSortPopup.GetSortExpression();
        //    hfSortExpression.Value = orderby;
        //    orderby = orderby.Remove(0, Search.FullSortExpression.Length);
        //}
        //else
        //{
        //    orderby = ddlistOrderby.SelectedValue + " " + ddlistOrderByDirection.SelectedValue;
        //}
        //orderby = ddlistOrderby.SelectedValue + " " + ddlistOrderByDirection.SelectedValue;
        return orderby;
    }

    public void SetOrderBy(string sortExpression, SortDirection sortDirection)
    {
        if (String.IsNullOrEmpty(sortExpression)) return;

        // BUG_8359
        if (sortExpression == Contentum.eUtility.Search.FullSortExpression)
        {
            sortExpression = "EREC_HataridosFeladatok.LetrehozasIdo";
            sortDirection = SortDirection.Descending;
        }

        string orderby = Search.GetOrderBy("TreeViewFeladatok", ViewState, sortExpression, sortDirection);

        // BUG_8359
        if (IsListHeader)
        {
            _listheader.ListSortPopup.SetSortExpression(orderby);

        }
    }

    public EREC_HataridosFeladatokSearch GetSearchObject(out bool isSearchObjectInSession)
    {
        EREC_HataridosFeladatokSearch searchObject;
        isSearchObjectInSession = false;
        string customSearchObjectsessionName = GetCustomSearchObjectSessionName();

        if (_listheader != null)
        {
            _listheader.CustomSearchObjectSessionName = customSearchObjectsessionName;
            _listheader.SearchObjectType = typeof(EREC_HataridosFeladatokSearch);
        }

        searchObject = (EREC_HataridosFeladatokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_HataridosFeladatokSearch(), customSearchObjectsessionName);
        isSearchObjectInSession = Search.IsSearchObjectInSession_CustomSessionName(Page, customSearchObjectsessionName);


        return searchObject;
    }

    private string GetCustomSearchObjectSessionName()
    {
        string customSearchObjectSessionName = String.Empty;

        switch (ParentForm)
        {
            case Constants.ParentForms.KiadottFeladataim:
                customSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch;
                break;
            case Constants.ParentForms.Feladataim:
                customSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.FeladataimSearch;
                break;
            case Constants.ParentForms.DolgozokFeladatai:
                customSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch;
                break;
            default:
                customSearchObjectSessionName = typeof(EREC_HataridosFeladatokSearch).Name;
                break;
        }

        return customSearchObjectSessionName;
    }

    public void SaveSearchObject(EREC_HataridosFeladatokSearch searchObject)
    {
        string customSearchObjectsessionName = GetCustomSearchObjectSessionName();

        if (searchObject != null)
        {
            if (!Search.IsIdentical(searchObject, new EREC_HataridosFeladatokSearch()))
            {
                Search.SetSearchObject_CustomSessionName(Page, searchObject, customSearchObjectsessionName);
            }
        }
    }

    public void RemoveSearchObject(bool always)
    {
        string customSessionObjectName = GetCustomSearchObjectSessionName();
        if (customSessionObjectName == typeof(EREC_HataridosFeladatokSearch).Name || always)
            Search.RemoveSearchObjectFromSession_CustomSessionName(Page, customSessionObjectName);
    }

    public int Height
    {
        get
        {
            return FeladatokCPE.ExpandedSize;
        }
        set
        {
            FeladatokCPE.ExpandedSize = value;
            Panel1.Height = Unit.Pixel(value);
        }
    }

    public int Width
    {
        set
        {
            MainPanel.Width = value;
            PanelScroll.Width = value;
        }
    }

    public string HeaderText
    {
        get
        {
            object o = ViewState["HeaderText"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["HeaderText"] = value;
        }
    }

    #region ButtonsProperty

    private void SetButtonEnabled(ImageButton imageButton, bool value)
    {
        imageButton.Enabled = value;
        imageButton.CssClass = (value) ? "" : "disableditem";
    }

    #endregion

    private Component_ListHeader _listheader = null;

    public Component_ListHeader ListHeader
    {
        get
        {
            if (_listheader == null)
            {
                _listheader = ListHeaderInner;
            }
            return _listheader;
        }
        set
        {
            ListHeaderInner.PreLoad -= _listheader_PreLoad;
            ListHeaderInner.Visible = false;
            _listheader = value;
            _listheader.PreLoad += new EventHandler(_listheader_PreLoad);
        }
    }

    private bool IsListHeader
    {
        get
        {
            if (_listheader == null)
                return false;
            return true;
        }
    }

    void _listheader_PreLoad(object sender, EventArgs e)
    {
        if (!Active) return;

        if (_listheader != null)
        {
            switch (ParentForm)
            {
                case Constants.ParentForms.KiadottFeladataim:
                    _listheader.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch;
                    break;
                case Constants.ParentForms.Feladataim:
                    _listheader.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.FeladataimSearch;
                    break;
                case Constants.ParentForms.DolgozokFeladatai:
                    _listheader.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch;
                    break;
                default:
                    _listheader.CustomSearchObjectSessionName = String.Empty;
                    break;
            }
            _listheader.SearchObjectType = typeof(EREC_HataridosFeladatokSearch);
        }
    }

    private Label _tabHeader = null;
    public Label TabHeader
    {
        get
        {
            return _tabHeader;
        }
        set
        {
            _tabHeader = value;
        }
    }


    #region Buttons

    private ImageButton NewButton
    {
        get
        {
            return _listheader.NewButton;
        }
    }
    private ImageButton ModifyButton
    {
        get
        {
            return _listheader.ModifyButton;
        }
    }
    private ImageButton LezarasButton
    {
        get
        {
            return _listheader.LezarasButton;
        }
    }
    private ImageButton InvalidateButton
    {
        get
        {
            return _listheader.InvalidateButton;
        }
    }
    private ImageButton SztornoButton
    {
        get
        {
            return _listheader.SztornoButton;
        }
    }
    private ImageButton SearchButton
    {
        get
        {
            return _listheader.SearchButton;
        }
    }
    private ImageButton DefaultFilterButton
    {
        get
        {
            return _listheader.DefaultFilterButton;
        }
    }
    private ImageButton LockButton
    {
        get
        {
            return _listheader.LockButton;
        }
    }
    private ImageButton UnLockButton
    {
        get
        {
            return _listheader.UnLockButton;
        }
    }
    private ImageButton AtvetelButton
    {
        get
        {
            return _listheader.AtvetelButton;
        }
    }
    private ImageButton ReszfeladatButton
    {
        get
        {
            return _listheader.AddButton;
        }
    }

    #endregion

    private bool _isErrorPanelCleared = false;
    private void ClearErrorPanel()
    {
        if (IsPostBack && !_isErrorPanelCleared)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        _isErrorPanelCleared = true;
    }

    private bool isReload = false;

    public string FeladatTipus
    {
        get
        {
            object o = ViewState["FeladatTipus"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set { ViewState["FeladatTipus"] = value; }
    }

    public string AthelyezendoFeladatId
    {
        get
        {
            object o = ViewState["AthelyezendoFeladatId"];
            if (o != null)
                return (string)o;

            return String.Empty;
        }

        set
        {
            ViewState["AthelyezendoFeladatId"] = value;

            if (String.IsNullOrEmpty(value))
            {
                _listheader.AthelyezesButton.Style.Add(HtmlTextWriterStyle.BorderWidth, "0px");
            }
            else
            {
                _listheader.AthelyezesButton.Style.Add(HtmlTextWriterStyle.BorderStyle, "groove");
                _listheader.AthelyezesButton.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                _listheader.AthelyezesButton.Style.Add(HtmlTextWriterStyle.BorderColor, "silver");
            }
        }
    }

    private bool _jogosultak = true;
    public bool Jogosultak
    {
        get
        {
            return _jogosultak;
        }
        set
        {
            _jogosultak = value;
        }
    }

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        FeladatView.ErrorPanel = EErrorPanel1;
        FeladatView.ErrorUpdatePanel = ErrorUpdatePanel1;

        #region ListHeader
        _listheader = ListHeaderInner;
        ListHeaderInner.PreLoad += new EventHandler(_listheader_PreLoad);
        //ListHeaderInner.HeaderLabel = TabHeader.Text;
        ListHeaderInner.SetRightFunctionButtonsVisible(false);
        ListHeaderInner.SendObjectsVisible = false;
        ListHeaderInner.PagerVisible = false;
        #region Baloldali funkciógombok kiszedése
        ListHeaderInner.SearchVisible = false;
        ListHeaderInner.NewVisible = false;
        ListHeaderInner.AddVisible = false;
        ListHeaderInner.ViewVisible = false;
        ListHeaderInner.ModifyVisible = false;
        ListHeaderInner.ExportVisible = false;
        ListHeaderInner.PrintVisible = false;
        ListHeaderInner.InvalidateVisible = false;
        ListHeaderInner.HistoryVisible = false;
        ListHeaderInner.DefaultFilterVisible = false;
        #endregion
        ListHeaderInner.CustomSearchObjectSessionName = "-----";
        ListHeaderInner.RefreshOnClientClick = "__doPostBack('" + FeladatokUpdatePanel.ClientID + "','" + EventArgumentConst.refreshFeladatok + "');";
        ListHeaderInner.DafaultPageLinkVisible = false;
        #endregion
        //TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        HataridosFeladatok.SetFeladatListColumnsVisibility(Page, TreeViewFeladatok);

        if (HataridosFeladatok.IsDisabled(Page))
        {
            ddListTreeView_ViewMode.Visible = false;
            labelViewMode.Visible = false;
        }
    }

    private bool pageLoaded = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitTab();
        }
        else
        {
            HandlePostBackEvent();
        }

        if (!Active) return;

        _HasSpecialScroll = (ParentForm != Constants.ParentForms.Feladataim && ParentForm != Constants.ParentForms.KiadottFeladataim
            && ParentForm != Constants.ParentForms.DolgozokFeladatai);

        if (!IsPostBack)
        {
            string qsObjektumType = Request.QueryString.Get(QueryStringVars.ObjektumType);
            if (!String.IsNullOrEmpty(qsObjektumType))
            {
                ObjektumType = qsObjektumType;
                RemoveSearchObject(true);
            }
        }

        ClearErrorPanel();

        if (ParentForm == Constants.ParentForms.DolgozokFeladatai)
        {
            FelhasznaloCsoportTextBox_Dolgozo.Validate = false;
            FelhasznaloCsoportTextBox_Dolgozo.TextBox.CssClass = "mrUrlapInputSzeles";
            FelhasznaloCsoportTextBox_Dolgozo.CsakSajatSzervezetDolgozoi = true;
            FelhasznaloCsoportTextBox_Dolgozo.SearchMode = true;
            FelhasznaloCsoportTextBox_Dolgozo.TextBox.TextChanged += new EventHandler(DolgozokTextBox_TextChanged);
            FelhasznaloCsoportTextBox_Dolgozo.TextBox.AutoPostBack = true;
            FelhasznaloCsoportTextBox_Dolgozo.RefreshCallingWindow = true;
        }

        RefreshButtonsVisiblity();

        ListHeader.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderButtonsClick);
        ListHeader.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderButtonsClick);

        LezarasButton.Command += new CommandEventHandler(Lezaras_Command);
        SztornoButton.Command += new CommandEventHandler(Sztorno_Command);


        ModifyButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        if (Command == CommandName.Modify || ParentForm == Constants.ParentForms.Feladataim)
        {
            LezarasButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }
        InvalidateButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SztornoButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ReszfeladatButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ReszfeladatButton.ToolTip = ReszfeladatButton.AlternateText = "Részfeladat felvétele";


        if (IsListHeader)
        {
            LockButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            UnLockButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            AtvetelButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            _listheader.SendObjectsOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            _listheader.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            if (String.IsNullOrEmpty(AthelyezendoFeladatId))
            {
                _listheader.AthelyezesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }
            _listheader.AthelyezesButton.ToolTip = _listheader.AthelyezesButton.AlternateText = "Feladat áthelyezése";
        }

        if (IsListHeader)
        {
            _listheader.PagerVisible = true;
            _listheader.PagingButtonsClick += new EventHandler(_listheader_PagingButtonsClick);
            _listheader.RowCount_Changed += new EventHandler(RowCountTextBox_TextChanged);
            if (!isReload)
            {
                _listheader.EnableListSortPopup = true;
                _listheader.AttachedTreeGridView = TreeViewFeladatok;
            }
            //_listheader.ListSortPopup.SortButtonClick += new EventHandler(btnRendez_Click);
        }

        if (!HasSpecialScroll)
        {
            //scroll állapotának mentése
            JavaScripts.RegisterScrollManagerScript(Page);
        }

        FeladatView.Updated += new EventHandler(FeladatView_Updated);
        FeladatView.Created += new EventHandler(FeladatView_Created);
        cbUseUgyiratHierarchia.CheckedChanged += new EventHandler(cbUseUgyiratHierarchia_CheckedChanged);
        TreeViewFeladatok.RowSelecting += new GridViewSelectEventHandler(TreeViewFeladatok_RowSelecting);

        pageLoaded = true;

        FilteredObjektum.ErrorPanel = EErrorPanel1;
        FilteredObjektum.ErrorUpdatePanel = ErrorUpdatePanel1;

        if (needReloadTab)
        {
            ReLoadTab();
        }
    }

    void TreeViewFeladatok_RowSelecting(object sender, GridViewSelectEventArgs e)
    {
        if (e.NewSelectedIndex == 0)
        {
            if (TreeViewFeladatok.SelectedNode != null)
            {
                TreeViewFeladatok.SelectedNode.Selected = false;
                RefreshSelectedNode();
                e.Cancel = true;
            }
        }
    }

    private void HandlePostBackEvent()
    {
        if (!Active)
        {
            if (Request.Params["__EVENTARGUMENT"] != null && Request.Params["__EVENTTARGET"] != null)
            {
                string eventTarget = Request.Params["__EVENTTARGET"];
                AjaxControlToolkit.TabContainer parentTabcontainer = UI.FindParentControl<AjaxControlToolkit.TabContainer>(this);
                if (parentTabcontainer != null)
                {
                    if (eventTarget == parentTabcontainer.UniqueID)
                    {
                        string eventArgument = Request.Params["__EVENTARGUMENT"];
                        string eventName = eventArgument.Split(':')[0];
                        if (String.Compare(eventName, "activeTabChanged", true) == 0)
                        {
                            AjaxControlToolkit.TabPanel parentTabPanel = UI.FindParentControl<AjaxControlToolkit.TabPanel>(this);
                            if (parentTabPanel != null)
                            {
                                if (parentTabcontainer.ActiveTab.Equals(parentTabPanel))
                                {
                                    Active = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Active)
            return;

        if (TreeViewFeladatok.Nodes.Count == 0)
        {
            labelEmptyText.Visible = true;
        }
        else
        {
            labelEmptyText.Visible = false;
        }

        //1 sor esetén a sor kijelölése
        SelectSingleRow();


        if (!IsOnEnabledParentForm())
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }


        RefreshButtonsClick();
        RefreshPagerLabel();
        UI.GridViewSetScrollable(Scrollable, FeladatokCPE);
        if (Scrollable)
        {
            tableFeladatok.Width = "98%";
        }
        else
        {
            tableFeladatok.Width = "100%";
        }

        if (!HataridosFeladatok.IsDisabled(Page))
        {
            NewButton.OnClientClick = JavaScripts.SetOnClientClick("FeladatokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.New + "&"
            + QueryStringVars.Startup + "=" + GetStartup() + "&" + QueryStringVars.ObjektumId + "=" + ObjektumId +
            "&" + QueryStringVars.ObjektumType + "=" + ObjektumType
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, FeladatokUpdatePanel.ClientID, EventArgumentConst.refreshFeladatok);
        }

        SearchButton.OnClientClick = JavaScripts.SetOnClientClick("FeladatokSearch.aspx",
            QueryStringVars.Startup + "=" + GetStartup() + "&" + QueryStringVars.ObjektumId + "="
            + ObjektumId + "&" + QueryStringVars.ObjektumType + "=" + GetObjType() + "&" + QueryStringVars.FeladatTipus
            + "=" + FeladatTipus
            , 800, 650, FeladatokUpdatePanel.ClientID, EventArgumentConst.refreshFeladatok);

        SetButtonEnabled(NewButton, FunctionRights.GetFunkcioJog(Page, "FeladatNew"));
        SetButtonEnabled(ReszfeladatButton, FunctionRights.GetFunkcioJog(Page, "FeladatNew"));
        SetButtonEnabled(InvalidateButton, FunctionRights.GetFunkcioJog(Page, "FeladatInvalidate"));
        SetButtonEnabled(LezarasButton, FunctionRights.GetFunkcioJog(Page, "FeladatLezaras"));
        SetButtonEnabled(SztornoButton, FunctionRights.GetFunkcioJog(Page, "FeladatSztorno"));
        if (IsListHeader)
        {
            SetButtonEnabled(LockButton, FunctionRights.GetFunkcioJog(Page, "FeladatLock"));
            SetButtonEnabled(UnLockButton, FunctionRights.GetFunkcioJog(Page, "FeladatLock"));
            SetButtonEnabled(AtvetelButton, FunctionRights.GetFunkcioJog(Page, "FeladatAtvetel"));
            _listheader.AthelyezesEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatAthelyezes");
        }

        labelTreeViewFeladatokHeader.Visible = TreeViewFeladatok.ShowHeader;

        if (isReload)
        {
            String sortExpression = Search.GetSortExpressionFromViewState("TreeViewFeladatok", ViewState, TreeViewFeladatok.DefaultSortExpression);
            SortDirection sortDirection = Search.GetSortDirectionFromViewState("TreeViewFeladatok", ViewState, TreeViewFeladatok.DefaultSortDirection);
            SetOrderBy(sortExpression, sortDirection);
        }

        if (IsUgyiratObjektumType && !String.IsNullOrEmpty(ObjektumId) && !String.IsNullOrEmpty(GetObjType()))
        {
            FilteredObjektum.SetObjektum(ObjektumId, GetObjType());
            linkFilteredObjektumAzonosito.Text = FilteredObjektum.GetSelected_Obj_Azonosito();
            linkFilteredObjektumAzonosito.NavigateUrl = "~/" + ObjektumTipusok.GetListUrl(FilteredObjektum.GetSelected_Obj_Id(), FilteredObjektum.GetSelected_Obj_Type());
            labelFilteredObjektumType.Text = FilteredObjektum.GetSelected_Obj_Type_Text();
            FilteredObjektum.Visible = false;
            FilteredObjektumPanel.Visible = true;
        }
        else
        {
            FilteredObjektumPanel.Visible = false;
        }

        if (!String.IsNullOrEmpty(HeaderText))
        {
            if (IsListHeader)
            {
                _listheader.HeaderLabel = HeaderText;
            }
        }

        //if (ParentForm != Constants.ParentForms.Feladataim && ParentForm != Constants.ParentForms.KiadottFeladataim
        //    && ParentForm != Constants.ParentForms.DolgozokFeladatai)
        if (HasSpecialScroll)
        {
            Width = 970;
        }
    }

    private void SelectSingleRow()
    {
        if (TreeViewFeladatok.Nodes.Count == 1)
        {
            if (TreeViewFeladatok.SelectedNode == null)
            {
                TreeViewFeladatok.Nodes[0].Select();
                RefreshSelectedNode();
            }
        }
    }

    bool refreshFeladatView = true;

    void FeladatView_Updated(object sender, EventArgs e)
    {
        refreshFeladatView = false;
        RefreshSelectedNode();
    }

    void FeladatView_Created(object sender, EventArgs e)
    {
        refreshFeladatView = true;
        SelectedRecordId = FeladatView.FeladatId;
        LoadTreeView(null);
    }

    private void SetColumsVisiblity()
    {
        //ddlistOrderby.Items.Add(new ListItem("Létrehozási idõ", "EREC_HataridosFeladatok.LetrehozasIdo"));
        foreach (DataControlField field in TreeViewFeladatok.Columns)
        {
            if (ParentForm == Constants.ParentForms.Feladataim)
            {
                if (field.SortExpression == "KRT_Csoportok_Felelos.Nev")
                {
                    field.Visible = false;
                }
            }
            else if (ParentForm == Constants.ParentForms.KiadottFeladataim)
            {
                if (field.SortExpression == "KRT_Felhasznalok_Kiado.Nev")
                {
                    field.Visible = false;
                }
            }
        }
    }

    private void RefreshButtonsVisiblity()
    {
        if (!HataridosFeladatok.IsDisabled(Page))
        {
            ReszfeladatButton.Visible = true;
            ModifyButton.Visible = true;
            InvalidateButton.Visible = true;

            if (_listheader != null)
            {
                _listheader.SendObjectsVisible = true;
                _listheader.HistoryVisible = true;
                _listheader.AthelyezesVisible = true;
                LockButton.Visible = true;
                UnLockButton.Visible = true;

            }

            AtvetelButton.Visible = true;
        }

        InvalidateButton.Visible = false;
        SztornoButton.Visible = false;
        SearchButton.Visible = true;
        DefaultFilterButton.Visible = true;
        LezarasButton.Visible = false;
        NewButton.Visible = true;



        if (ParentForm == Constants.ParentForms.Feladataim)
        {
        }

        if (ParentForm == Constants.ParentForms.DolgozokFeladatai)
        {
            // Csoportválasztó panel engedélyezése:
            DolgozoPanel.Visible = true;
            DolgozoUpdatePanel.Visible = true;
        }

    }

    private bool IsOnEnabledParentForm()
    {
        //if (ParentForm == Constants.ParentForms.Ugyirat ||
        //   ParentForm == Constants.ParentForms.IratPeldany ||
        //   ParentForm == Constants.ParentForms.Kuldemeny ||
        //   ParentForm == Constants.ParentForms.Feladataim ||
        //    ParentForm == Constants.ParentForms.KiadottFeladataim ||
        //    ParentForm == Constants.ParentForms.DolgozokFeladatai
        //    || ParentForm == Constants.ParentForms.IraIrat)
        //{
        //    return true;
        //}

        //return false;
        return true;
    }

    private void RefreshButtonsClick()
    {
        if (TreeViewFeladatok.SelectedNode == null) return;

        TreeNode selectedNode = TreeViewFeladatok.SelectedNode;
        string selectedId = selectedNode.Value;
        EREC_HataridosFeladatok erec_hataridosFeladat = FeladatView.GetBusinessObject();

        if (erec_hataridosFeladat == null) return;

        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        ErrorDetails errorDetails;

        if (HataridosFeladatok.ReszfeladatFelveheto(xpm, erec_hataridosFeladat, out errorDetails))
        {
            ReszfeladatButton.OnClientClick = JavaScripts.SetOnClientClick("FeladatokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Id + "=" + selectedId + "&"
            + QueryStringVars.Startup + "=" + GetStartup() + "&" + QueryStringVars.ObjektumId + "=" + ObjektumId
            + QueryStringVars.ObjektumType + "=" + ObjektumType
            , Defaults.PopupWidth_Max, Defaults.PopupHeight, FeladatokUpdatePanel.ClientID, "RefreshReszFeladatok");
        }
        else
        {
            ReszfeladatButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59004, errorDetails);
        }

        if (HataridosFeladatok.Modosithato(xpm, erec_hataridosFeladat, out errorDetails))
        {
            ModifyButton.OnClientClick = JavaScripts.SetOnClientClick("FeladatokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.Modify + "&"
            + QueryStringVars.Id + "=" + selectedId + "&"
            + QueryStringVars.Startup + "=" + GetStartup()
            , Defaults.PopupWidth_Max, Defaults.PopupHeight, FeladatokUpdatePanel.ClientID, EventArgumentConst.refreshFeladatok);
        }
        else
        {
            ModifyButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59005, errorDetails);
        }

        #region BLG_2956
        //LZS - Lekéri, hogy az adott feladat delegált állapotú-e.
        if (HataridosFeladatok.Delegalt(xpm, erec_hataridosFeladat))
        {
            ModifyButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59005, errorDetails);
        }
        #endregion
        if (HataridosFeladatok.Sztornozhato(xpm, erec_hataridosFeladat, out errorDetails))
        {
            SztornoButton.OnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_Sztorno + " \\n \\n"
                + Resources.Question.UI_ContinueQuestion + "')) {  } else return false;";
        }
        else
        {
            SztornoButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59001, errorDetails);
        }
        if (HataridosFeladatok.Lezarhato(xpm, erec_hataridosFeladat, out errorDetails))
        {
            LezarasButton.OnClientClick = String.Empty;
            LezarasButton.Enabled = true;
        }
        else
        {
            LezarasButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59002, errorDetails);
        }

        if (IsListHeader)
        {
            LockButton.OnClientClick = JavaScripts.SetOnClientClickLockConfirm();
            UnLockButton.OnClientClick = JavaScripts.SetOnClientClickUnlockConfirm();
            if (HataridosFeladatok.Atveheto(xpm, erec_hataridosFeladat, out errorDetails))
            {
                AtvetelButton.OnClientClick = "if (!confirm('" + Resources.Question.UIConfirmHeader_Atvetel
                    + "')) return false;";
            }
            else
            {
                AtvetelButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59003, errorDetails);
            }
        }

        if (HataridosFeladatok.Invalidalhato(xpm, erec_hataridosFeladat, out errorDetails))
        {
            InvalidateButton.OnClientClick = JavaScripts.SetOnClientClickConfirm(Resources.Question.UIConfirmHeader_Delete, Resources.Question.UIisDeleteOne);
        }
        else
        {
            InvalidateButton.OnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59006, errorDetails);
        }

        if (IsListHeader)
        {
            _listheader.SendObjectsOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
          QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                   , Defaults.PopupWidth, Defaults.PopupHeight, FeladatokUpdatePanel.ClientID, "", true);

            string tableName = "EREC_HataridosFeladatok";
            _listheader.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + selectedId + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, FeladatokUpdatePanel.ClientID);

            if (HataridosFeladatok.Athelyezheto(xpm, erec_hataridosFeladat, null, out errorDetails))
            {
                _listheader.AthelyezesOnClientClick = String.Empty;
            }
            else
            {
                _listheader.AthelyezesOnClientClick = JavaScripts.SetOnClientClickErrorDetailsAlert(Page, 59007, errorDetails);
            }

        }
    }

    protected void FeladatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (!Active) return;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshFeladatok:
                case EventArgumentConst.refreshMasterList:

                    if (eventArgument == EventArgumentConst.refreshMasterList) // BUG_7081
                    {
                        RemoveSearchObject(true);
                        FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField = "";
                        FelhasznaloCsoportTextBox_Dolgozo.SetCsoportTextBoxById(EErrorPanel1);
                    }

                    JavaScripts.ResetScroll(Page, FeladatokCPE);
                    //SelectedRecordId = _listheader.SelectedRecordId;
                    if (IsListHeader)
                    {
                        if (GetTopSelectedRecordId() != _listheader.SelectedRecordId)
                        {
                            SelectedRecordId = _listheader.SelectedRecordId;
                        }
                    }
                    //ForgetSelectedRecord();
                    //LoadTreeView(null);

                    // A keresés miatt nem mindig elég a kiválasztott node-ot frissíteni
                    //// Módosítás: a visszakapott Id-hoz tartozó elem megegyezik a kiválasztott elemmel
                    //if (TreeViewFeladatok.SelectedNode != null && SelectedRecordId == TreeViewFeladatok.SelectedNode.Value)
                    //{
                    //    RefreshSelectedNode();
                    //}
                    //else // Új: A teljes listát újratöltjük
                    {
                        LoadTreeView(null);
                    }

                    // ha nem volt aktuálisan kijelölve (nincs a találati listán)
                    // töröljük a ListHeaderbõl is
                    if (TreeViewFeladatok.SelectedNode == null)
                    {
                        ForgetSelectedRecord();
                    }
                    break;
                case "RefreshReszFeladatok":
                    //SelectedRecordId = SelectedRecordId + "/" + _listheader.SelectedRecordId;
                    RefreshSelectedNodeChilds(_listheader.SelectedRecordId);
                    break;
            }
        }
    }

    private void RefreshSelectedNodeChilds(string selectedChildNodeId)
    {
        TreeNode selectedTreeNode = TreeViewFeladatok.SelectedNode;

        if (selectedTreeNode == null)
        {
            LoadTreeView(null);
        }
        else
        {
            LoadTreeView(selectedTreeNode);
            selectedTreeNode.Expand();
            if (!String.IsNullOrEmpty(selectedChildNodeId))
            {
                TreeNode selectedChildNode = TreeViewFeladatok.FindNode(selectedTreeNode.ValuePath + "/" + selectedChildNodeId);
                if (selectedChildNode != null)
                {
                    selectedChildNode.Select();
                    RefreshSelectedNode();
                }
            }
        }

    }

    // TreeView frissítése a kijelölt sor szintjéig
    private void RefreshSelectedNode()
    {
        TreeNode selectedTreeNode = TreeViewFeladatok.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            ForgetSelectedRecord();
            return;
        }

        ////string selectedId = selectedTreeNode.Value;

        //TreeViewFeladatok.Nodes.Clear();

        //LoadTreeView(null);
        string selectedId = selectedTreeNode.Value;
        if (refreshFeladatView)
        {
            SelectedRecordId = selectedTreeNode.ValuePath;
            FeladatView.FeladatId = selectedId;
            FeladatView.Megtekint();
        }
        selectedTreeNode.Text = GetTreeNodeText(selectedTreeNode, selectedTreeNode.Parent);
    }

    private void ClearSelectedNode()
    {
        TreeNode selectedTreeNode = TreeViewFeladatok.SelectedNode;

        if (selectedTreeNode != null)
        {
            selectedTreeNode.Selected = false;
        }

        RefreshSelectedNode();
    }

    private bool needReloadTab = false;
    public void ReLoadTab()
    {
        if (pageLoaded)
        {
            if (!IsOnEnabledParentForm())
            {
                // ha egyéb oldalra lenne betéve a tabpanel
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                    Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
                ErrorUpdatePanel1.Update();
                Active = false;
            }
            else
            {
                if (!Active) return;

                isReload = true;

                ClearErrorPanel();

                if (!IsPostBack)
                {
                    //string feledatId = Request.QueryString.Get(QueryStringVars.FeladatId);
                    //if (!String.IsNullOrEmpty(feledatId))
                    //{
                    //    SelectedRecordId = feledatId;
                    //}
                    Search.SetIdsToSearchObject(Page, "Id", new EREC_HataridosFeladatokSearch(), GetCustomSearchObjectSessionName());
                }

                if (IsListHeader)
                {
                    _listheader.AttachedTreeGridView = TreeViewFeladatok;
                    _listheader.EnableListSortPopup = true;
                    int iPageIndex;
                    if (Int32.TryParse(hfPageIndex.Value, out iPageIndex))
                    {
                        _listheader.PageIndex = iPageIndex;
                    }
                    else
                    {
                        _listheader.PageIndex = 0;
                    }
                    _listheader.RecordNumber = 0;
                    _listheader.PageCount = 0;
                    _listheader.IsReload = true;
                }

                ViewState["Loaded"] = false;
                LoadTreeView(null);
                //FeladatView.Reset();
                ViewState["Loaded"] = true;
                pageView.SetViewOnPage(Command);
            }
        }
        else
        {
            needReloadTab = true;
        }

    }

    private void LoadTreeView(TreeNode parentNode)
    {
        if (!Active) return;

        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());

        bool isSearchObjectInSession;
        EREC_HataridosFeladatokSearch sch = GetSearchObject(out isSearchObjectInSession);
        sch.OrderBy = GetOrderBy();

        sch.Jogosultak = Jogosultak;
        //SetSearchFilterText(sch.ReadableWhere);

        sch.DisplayChildsIfParentNotVisible = false;

        if (ddListTreeView_ViewMode.SelectedValue == TreeView_ViewMode.Hierarchikus)
        {
            sch.DisplayChildsIfParentNotVisible = true;
        }

        if (parentNode == null)
        {
            switch (ddListTreeView_ViewMode.SelectedValue)
            {
                case TreeView_ViewMode.Hierarchikus:
                    sch.HataridosFeladat_Id.Operator = Query.Operators.isnull;
                    break;
                case TreeView_ViewMode.Kiteritett:
                    sch.HataridosFeladat_Id.Clear();
                    break;
                default:
                    sch.HataridosFeladat_Id.Operator = Query.Operators.isnull;
                    break;
            }
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, FeladatokCPE);
        }
        else
        {
            sch.HataridosFeladat_Id.Value = parentNode.Value;
            sch.HataridosFeladat_Id.Operator = Query.Operators.equals;
        }

        //Feladat
        if (HataridosFeladatok.IsDisabled(Page))
        {
            if (!String.IsNullOrEmpty(ObjektumId))
            {
                sch.Obj_Id.Value = ObjektumId;
                sch.Obj_Id.Operator = Query.Operators.equals;
            }

            sch.Obj_type.Value = GetObjType();
            sch.Obj_type.Operator = Query.Operators.isnullorequals;

            sch.Tipus.Value = KodTarak.FELADAT_TIPUS.Megjegyzes;
            sch.Tipus.Operator = Query.Operators.equals;


        }
        else
        {
            if (!isSearchObjectInSession)
            {
                ObjektumId = StartObjektumId;
                ObjektumType = StartObjektumType;
                if (!String.IsNullOrEmpty(FeladatTipus))
                {
                    sch.Tipus.Value = FeladatTipus;
                    sch.Tipus.Operator = Query.Operators.equals;
                    sch.ReadableWhere += "Típus: " + Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.FELADAT_TIPUS.kcsNev, FeladatTipus, Page);
                    sch.ReadableWhere += Search.whereDelimeter;
                }
                else
                {
                    sch.Tipus.Clear();
                }

                if (ParentForm == Constants.ParentForms.Feladataim)
                {
                    sch.Felhasznalo_Id_Felelos.Value = "'" + xpm.Felhasznalo_Id + "'" + ",'"
                        + xpm.FelhasznaloSzervezet_Id + "'";
                    sch.Felhasznalo_Id_Felelos.Operator = Query.Operators.inner;

                    sch.ReadableWhere += "Felelõs: " + Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(xpm.Felhasznalo_Id, Page);
                    sch.ReadableWhere += Search.whereDelimeter;

                }
                else if (ParentForm == Constants.ParentForms.KiadottFeladataim)
                {
                    sch.Felhasznalo_Id_Kiado.Value = xpm.Felhasznalo_Id;
                    sch.Felhasznalo_Id_Kiado.Operator = Query.Operators.equals;

                    sch.ReadableWhere += "Kiadó: " + Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(xpm.Felhasznalo_Id, Page);
                    sch.ReadableWhere += Search.whereDelimeter;
                }
                else if (ParentForm == Constants.ParentForms.DolgozokFeladatai)
                {
                    // ki kell választani a dolgozót:
                    if (string.IsNullOrEmpty(FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField))
                    {
                        //return;
                    }
                    else
                    {
                        // szûrés a megadott felhasználóra
                        sch.Felhasznalo_Id_Felelos.Value = FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField;
                        sch.Felhasznalo_Id_Felelos.Operator = Query.Operators.equals;
                    }
                }
                if (!String.IsNullOrEmpty(GetObjType()))
                {
                    if (!String.IsNullOrEmpty(ObjektumId))
                    {
                        sch.Obj_Id.Value = ObjektumId;
                        sch.Obj_Id.Operator = Query.Operators.equals;
                    }

                    if (String.Equals("null", GetObjType(), StringComparison.CurrentCultureIgnoreCase))
                    {
                        sch.Obj_type.Operator = Query.Operators.isnull;
                        FilteredObjektum.IsFuggetlen = true;
                    }
                    else
                    {
                        sch.Obj_type.Value = GetObjType();
                        sch.Obj_type.Operator = Query.Operators.isnullorequals;
                        FilteredObjektum.IsFuggetlen = false;
                        FilteredObjektum.SetObjektum(ObjektumId, GetObjType());
                    }


                    sch.ReadableWhere += "Iratkezelési elem: " + FilteredObjektum.GetSearchText();
                    sch.ReadableWhere += Search.whereDelimeter;
                }

                SaveSearchObject(sch);
            }

            if (ParentForm == Constants.ParentForms.DolgozokFeladatai)
            {
                FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField = sch.Felhasznalo_Id_Felelos.Value;
                FelhasznaloCsoportTextBox_Dolgozo.SetCsoportTextBoxById(EErrorPanel1);
                FelhasznaloCsoportTextBox_Dolgozo.TextBox.Focus();
            }

            ObjektumId = sch.Obj_Id.Value;
            ObjektumType = sch.Obj_type.Value;
        }

        sch.UseUgyiratHierarchy = UseUgyiratHierarchy;





        //lapozás
        if (parentNode == null)
            SetPaging(xpm);

        Result result = svc.GetAllWithExtension(xpm, sch);

        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            //MainPanel.Visible = false;
            return;
        }

        if (parentNode == null)
        {
            labelTreeViewFeladatokHeader.Text = GetTreeNodeText(null as DataRow, null, null);
        }
        AddNode(result.Ds.Tables[0], parentNode);

        //lapozás
        if (parentNode == null)
            RefreshPaging(result);

        if (parentNode == null && TabHeader != null)
            UI.SetTabHeaderRowCountText(result, TabHeader);

        // Node Select
        //selctedNode.Selected = true;
        //TreeViewFeladatok_SelectedNodeChanged(TreeViewFeladatok, new EventArgs());
    }

    private void AddNode(DataTable table, TreeNode parentNode)
    {
        if (parentNode == null)
            TreeViewFeladatok.Nodes.Clear();
        else
            parentNode.ChildNodes.Clear();

        if (TreeViewFeladatok.SelectedNode == null)
        {
            FeladatView.Reset();
        }

        foreach (DataRow row in table.Rows)
        {
            string id = row["Id"].ToString();
            string text = GetTreeNodeText(row, null, parentNode);
            TreeNode node = new TreeNode();
            node.Text = text;
            node.Value = id;
            node.SelectAction = TreeNodeSelectAction.None;
            string reszfeladatokSzama = row["ReszFeladatokCount"].ToString();
            if (reszfeladatokSzama == "0")
                node.PopulateOnDemand = false;
            else
            {
                node.Expanded = false;
                node.PopulateOnDemand = true;
            }
            if (parentNode == null)
                TreeViewFeladatok.Nodes.Add(node);
            else
                parentNode.ChildNodes.Add(node);
        }
    }

    private void ListHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        TreeNode selectedNode = TreeViewFeladatok.SelectedNode;
        string selectedId = String.Empty;
        if (selectedNode != null)
        {
            selectedId = selectedNode.Value;
        }
        else
        {
            if (e.CommandName == CommandName.New
                || (e.CommandName == CommandName.Athelyezes && !String.IsNullOrEmpty(AthelyezendoFeladatId)))
            {
            }
            else
            {
                return;
            }
        }

        try
        {
            switch (e.CommandName)
            {
                case CommandName.New:
                    ClearSelectedNode();
                    FeladatView.New(ObjektumId, GetObjType());
                    break;
                case CommandName.Lock:
                    {
                        LockManager.LockSelectedGridViewRecords(selectedId, "EREC_HataridosFeladatok"
                        , "FeladatLock", "FeladatForceLock", Page, EErrorPanel1, ErrorUpdatePanel1);
                        RefreshSelectedNode();
                    }
                    break;
                case CommandName.Unlock:
                    {
                        LockManager.UnlockSelectedGridViewRecords(selectedId, "EREC_HataridosFeladatok"
                        , "FeladatLock", "FeladatForceLock", Page, EErrorPanel1, ErrorUpdatePanel1);
                        RefreshSelectedNode();
                    }
                    break;
                case CommandName.Invalidate:
                    {
                        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                        xpm.Record_Id = selectedId;
                        Result res = svc.Invalidate(xpm);
                        if (res.IsError)
                        {
                            throw new Contentum.eUtility.ResultException(res);
                        }

                        if (selectedNode.Parent != null)
                        {
                            selectedNode.Parent.ChildNodes.Remove(selectedNode);
                        }
                        else
                        {
                            TreeViewFeladatok.Nodes.Remove(selectedNode);
                        }

                        FeladatView.Reset();
                    }
                    break;
                case CommandName.Atvetel:
                    {
                        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                        xpm.Record_Id = selectedId;
                        Result res = svc.Atvetel(xpm);
                        if (res.IsError)
                        {
                            throw new Contentum.eUtility.ResultException(res);
                        }

                        RefreshSelectedNode();
                    }
                    break;
                case CommandName.SendObjects:
                    SendMailSelectedFeladat();
                    break;
                case CommandName.Athelyezes:
                    AthelyezSelectedNode();
                    break;
            }
        }
        catch (Contentum.eUtility.ResultException re)
        {
            // hiba:
            Result result = Contentum.eUtility.ResultException.GetResultFromException(re);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            return;
        }

    }

    private void AthelyezSelectedNode()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FeladatAthelyezes"))
        {
            string selectedId = String.Empty;

            if (TreeViewFeladatok.SelectedNode != null)
            {
                selectedId = TreeViewFeladatok.SelectedNode.Value;
            }

            if (String.IsNullOrEmpty(AthelyezendoFeladatId))
            {
                AthelyezendoFeladatId = selectedId;
                refreshFeladatView = false;
                RefreshSelectedNode();
            }
            else
            {
                if (selectedId == AthelyezendoFeladatId)
                {
                    AthelyezendoFeladatId = String.Empty;
                    refreshFeladatView = false;
                    RefreshSelectedNode();
                    return;
                }

                ExecParam xpm = UI.SetExecParamDefault(Page);
                EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                Result res = svc.Athelyezes(xpm, AthelyezendoFeladatId, selectedId);

                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (!String.IsNullOrEmpty(SelectedRecordId))
                    {
                        SelectedRecordId = SelectedRecordId + "/" + AthelyezendoFeladatId;
                    }
                    else
                    {
                        SelectedRecordId = AthelyezendoFeladatId;
                    }
                    AthelyezendoFeladatId = String.Empty;
                    LoadTreeView(null);
                    //RefreshSelectedNodeChilds(String.Empty);
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        }
    }

    private void SendMailSelectedFeladat()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value) && TreeViewFeladatok.SelectedNode != null)
        {
            try
            {
                string id = TreeViewFeladatok.SelectedNode.Value;
                System.Security.Principal.WindowsImpersonationContext impersonationContext;
                impersonationContext = ((System.Security.Principal.WindowsIdentity)Context.User.Identity).Impersonate();
                string url = ObjektumTipusok.GetViewUrl(id, Constants.TableNames.EREC_HataridosFeladatok);
                url += "&Format=Email";
                string currentUrl = Request.Url.ToString();
                int index = currentUrl.LastIndexOf("/");
                url = currentUrl.Substring(0, index) + "/" + url;
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                Stream dataStream = webRequest.GetResponse().GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string response = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                impersonationContext.Undo();

                //response = response.Replace("href=\"", "href=\"http://lami.axis.hu:100/eRecord/");
                //response = response.Replace("href=\"http://lami.axis.hu:100/eRecord//eRecord", "href=\"http://lami.axis.hu:100/eRecord/");
                //response = response.Replace("src=\"", "src=\"http://lami.axis.hu:100/eRecord/");
                //response = response.Replace("src=\"http://lami.axis.hu:100/eRecord//eRecord", "src=\"http://lami.axis.hu:100/eRecord/");
                //response = response.Replace("url('", "url('http://lami.axis.hu:100/eRecord/");
                //EmailService email = eAdminService.ServiceFactory.GetEmailService();
                ExecParam xpm = UI.SetExecParamDefault(Page);
                //bool ret = email.SendEmail(xpm, "lamatsch.andras@axis.hu", new string[] { "lamatsch.andras@axis.hu" }, "teszt", null, null, true, response);
                List<string> selectedIdList = new List<string>(1);
                selectedIdList.Add(id);
                bool ret = Notify.SendSelectedItemsByEmail(Page, selectedIdList, UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value + "<br/>" + response, "EREC_HataridosFeladatok");
            }
            catch (Exception)
            {
            }
        }
    }

    protected void TreeViewFeladatok_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (TreeViewFeladatok.SelectedNode == null) return;
        TreeNode selectedNode = TreeViewFeladatok.SelectedNode;
        string selectedId = selectedNode.Value;
        RefreshSelectedNode();

    }

    protected void TreeViewFeladatok_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        TreeNode treeNode = e.Node;
        LoadTreeView(treeNode);
    }

    private string GetTreeNodeText(DataRow row, TreeNode selectedNode, TreeNode parentTreeNode)
    {
        bool isHeader = false;
        if (row == null)
            isHeader = true;

        int offset = 0;
        if (!isHeader)
        {
            if (parentTreeNode != null)
            {
                int depth = Math.Min(parentTreeNode.Depth, 2);
                offset = (depth + 1) * 20;
            }
        }

        string id = isHeader ? String.Empty : row["Id"].ToString();
        string Kiado = isHeader ? String.Empty : row["Kiado_Nev"].ToString();
        string Felelos = isHeader ? String.Empty : row["Felelos_Nev"].ToString();
        //string Tipus_Nev = row["Tipus_Nev"].ToString();
        string Leiras = isHeader ? String.Empty : row["Leiras"].ToString();
        string Prioritas = isHeader ? String.Empty : row["Prioritas"].ToString();
        string Allapot = isHeader ? String.Empty : row["Allapot"].ToString();
        string Allapot_Nev = isHeader ? String.Empty : row["Allapot_Nev"].ToString();
        string Hatarido = isHeader ? String.Empty : row["IntezkHatarido"].ToString();
        Hatarido = UI.GetShortDateTimeString(Hatarido);
        string Funkcio_Id_Inditando = isHeader ? String.Empty : row["Funkcio_Id_Inditando"].ToString();
        string Funkcio_Nev_Inditando = isHeader ? String.Empty : row["Funkcio_Nev_Inditando"].ToString();
        string Objektum_Azonosito = isHeader ? String.Empty : row["Azonosito"].ToString();
        string Obj_Id = isHeader ? String.Empty : row["Obj_Id"].ToString();
        string Obj_type = isHeader ? String.Empty : row["Obj_type"].ToString();
        string HataridosFeladat_Id = isHeader ? String.Empty : row["HataridosFeladat_Id"].ToString();
        bool isReszfeladat = !String.IsNullOrEmpty(HataridosFeladat_Id);
        //string Obj_Count = isHeader ? String.Empty : row["Obj_Count"].ToString();
        bool isLezart = HataridosFeladatok.Lezart(Allapot);
        //LZS - Delegált állapot lekérdezése
        bool isDelegalt = Allapot == KodTarak.FELADAT_ALLAPOT.Delegalt ? true : false;

        System.Text.StringBuilder sb = new StringBuilder();

        string style = String.Empty;
        string cellCssClass = String.Empty;
        string cssClass = String.Empty;
        string backgroundColor = String.Empty;

        //switch (Prioritas)
        //{
        //    case HataridosFeladatok.Prioritas.Alacsony:
        //        cssClass += " Feladat_LowPriority";
        //        break;
        //    case HataridosFeladatok.Prioritas.Normal:
        //        //backgroundColor = "#EAFFEA";
        //        break;
        //    case HataridosFeladatok.Prioritas.Magas:
        //        cssClass += " Feladat_HighPriority";
        //        break;
        //    default:
        //        break;
        //}
        if (selectedNode != null)
        {
            //UpdatePrevSelectedNodeColor();
            PrevSelectedRecord = selectedNode.ValuePath;
            //PrevBackGroundColor = cssClass;
            //cssClass = String.Empty;
        }

        if (Allapot == KodTarak.FELADAT_ALLAPOT.Uj)
        {
            cssClass += " Feladat_New";
        }

        //LZS - BLG_2956 Ha delegált a feladat, akkor a lezáráshoz használt CSS osztályt húzzuk rá.
        if (isLezart || isDelegalt)
        {
            cssClass += " Feladat_Lezart";
        }

        if (isHeader)
        {
            offset = 0;
            if (TreeViewFeladatok.Mode == Contentum.eUIControls.TreeGridView.TreeGridViewModes.GridView)
            {
                offset = 20;
            }
            cellCssClass = "GridViewHeaderStyle";
        }
        else
        {
            cellCssClass = "GridViewBoundFieldItemStyle";
        }
        if (!isHeader)
        {
            if (AthelyezendoFeladatId == id)
            {
                cssClass += " Feladat_Athelyezendo";
            }
        }
        if (isHeader && !HasSpecialScroll)
        {
            style += "position:relative;top:-2px;";
        }

        sb.Append("<table border='0' cellspacing='0' cellpadding='0' class='" + cssClass + "' style=\"" + style + "\"><tr>");
        if (!isHeader)
        {
            string srcImage = (isLezart ? (isReszfeladat ? "reszfeladat_lezart.jpg" : "treeview_feladat_lezart.jpg")
                : (isReszfeladat ? "reszfeladat.jpg" : "treeview_feladat_aktiv.jpg"));

            //LZS - BLG_2956 delegált feladat esetén a gridben megjelenítendő ikon.
            if (isDelegalt)
                srcImage = "delegalt1.jpg";

            sb.Append("<td class=\"" + cellCssClass + "\">");
            //sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/" + (isReszfeladat ? "reszfeladat.jpg" : "treeview_belsoirat.jpg") +"' alt='" + (isReszfeladat ? "Részfeladat" : "Feladat")  + "' />", id, parentTreeNode));
            sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/" + srcImage + "' alt='" + (isReszfeladat ? "Részfeladat" : "Feladat") + "' />", id, parentTreeNode));
            //sb.Append("&nbsp;</td> <td  class=\"" + cellCssClass + "\">");
        }
        //sb.Append(SetTreeNodeUrl(isHeader ? "<b>Feladat&nbsp</b>" : Funkcio_Nev_Inditando, id, parentTreeNode, 160 - offset, isHeader));
        //sb.Append("</td><td  class=\"" + cellCssClass + "\">");
        if (!isHeader)
        {
            string azonositoUrl = String.Empty;
            if (String.IsNullOrEmpty(Objektum_Azonosito))
            {
                azonositoUrl = EmptyObject;
            }
            else
            {
                string styleLinkStyle = ((isLezart || isDelegalt) ? "Feladat_LinkStyle_Lezart" : "linkStyle");

                string styleColor = ((isLezart || isDelegalt) ? "style=\"color:#666666;\"" : String.Empty);
                azonositoUrl = "<a href=\"" + ObjektumTipusok.GetListUrl(Obj_Id, Obj_type) + "\""
                    + "target=\"_blank\" class=\"" + styleLinkStyle + "\" title=\"ugrás\">";
                azonositoUrl += Objektum_Azonosito + "</a>";
            }
            row["Azonosito"] = azonositoUrl;

            string esemenyKivalto = row["Esemeny_Kivalto"].ToString();
            string esemenyIdKivalto = row["Esemeny_Id_Kivalto"].ToString();

            if (!String.IsNullOrEmpty(esemenyKivalto) && !String.IsNullOrEmpty(esemenyIdKivalto))
            {
                string url = ObjektumTipusok.GetViewUrl(esemenyIdKivalto, Constants.TableNames.KRT_Esemenyek);
                string onclick = JavaScripts.SetOnCLientClick_NoPostBack(url, "", Defaults.PopupWidth, Defaults.PopupHeight);
                string link = "<a href=\"javascript:void(0);\" onclick=\"" + onclick + "\""
                + " class=\"linkStyle\" title=\"Megtekintés\">";
                link += esemenyKivalto + "</a>";
                row["Esemeny_Kivalto"] = link;
            }

            string esemenyLezaro = row["Esemeny_Lezaro"].ToString();
            string esemenyIdLezaro = row["Esemeny_Id_Lezaro"].ToString();

            if (!String.IsNullOrEmpty(esemenyLezaro) && !String.IsNullOrEmpty(esemenyIdLezaro))
            {
                string url = ObjektumTipusok.GetViewUrl(esemenyIdLezaro, Constants.TableNames.KRT_Esemenyek);
                string onclick = JavaScripts.SetOnCLientClick_NoPostBack(url, "", Defaults.PopupWidth, Defaults.PopupHeight);
                string link = "<a href=\"javascript:void(0);\" onclick=\"" + onclick + "\""
                + " class=\"linkStyle\" title=\"Megtekintés\">";
                link += esemenyLezaro + "</a>";
                row["Esemeny_Lezaro"] = link;
            }
        }
        //row["Obj_Azonosito"] = azonositoUrl;
        //sb.Append(SetTreeNodeUrl(isHeader ? "<b>Azonosító&nbsp</b>" : azonositoUrl, id, parentTreeNode, 140,isHeader));
        //sb.Append("</td><td class=\"" + cellCssClass + GetHataridoCssClass(Hatarido) + "\" >");
        //sb.Append(SetTreeNodeUrl(isHeader ? "<b>Határidõ&nbsp</b>" : Hatarido, id, parentTreeNode, 90,isHeader));
        //sb.Append("</td><td  class=\"" + cellCssClass + "\">");
        //sb.Append(SetTreeNodeUrl(isHeader ? "<b>Állapot&nbsp</b>" : Allapot_Nev, id, parentTreeNode, 80,isHeader));
        //sb.Append("</td> <td  class=\"" + cellCssClass + "\">");
        //if (ParentForm == Constants.ParentForms.Feladataim)
        //{
        //    sb.Append(SetTreeNodeUrl(isHeader ? "<b>Kiadó&nbsp</b>" : Kiado, id, parentTreeNode, 130,isHeader));
        //}
        //else
        //{
        //    sb.Append(SetTreeNodeUrl(isHeader ? "<b>Felelõs&nbsp</b>" : Felelos, id, parentTreeNode, 190,isHeader));
        //}
        //sb.Append("</td> <td style=\"width:5px;\">");
        //sb.Append("</td> <td>");
        //sb.Append(SetTreeNodeUrl("<b>Prioritás:&nbsp</b>" + Prioritas, id, parentTreeNode, "120px"));

        bool IsFirstColumn = true;
        foreach (BoundField field in TreeViewFeladatok.Columns)
        {
            if (field.Visible)
            {
                if (isHeader)
                {
                    Unit width = field.HeaderStyle.Width;

                    if (IsFirstColumn)
                    {
                        if (!width.IsEmpty)
                            width = new Unit(width.Value + 80 - offset);
                        IsFirstColumn = false;
                    }
                    sb.Append("<td  class=\"" + field.HeaderStyle.CssClass + "\">");
                    sb.Append(SetTreeNodeUrl(field.HeaderText, id, parentTreeNode, width, true, field.SortExpression));
                    sb.Append("</td>");
                }
                else
                {
                    string hataridoCssClass = String.Empty;
                    if (!isLezart && field.DataField == "IntezkHatarido")
                    {
                        hataridoCssClass = GetHataridoCssClass(row[field.DataField].ToString());
                    }

                    Unit width = field.ItemStyle.Width;

                    if (IsFirstColumn)
                    {
                        if (!width.IsEmpty)
                            width = new Unit(width.Value + 40 - offset);
                        IsFirstColumn = false;
                    }

                    string text = row[field.DataField].ToString();

                    if (field.DataField == "IntezkHatarido")
                    {
                        text = UI.GetShortDateTimeString(text, false);
                    }

                    if (field.DataField == "Memo")
                    {
                        text = (text == Constants.Database.Yes) ? "Igen" : "Nem";
                    }

                    if (field.DataField == "Forras")
                    {
                        text = HataridosFeladatok.Forras.GetTextFromValue(text);
                    }

                    //if (field.DataField == "Prioritas")
                    //{
                    //    text = HataridosFeladatok.Prioritas.GetTextFromValue(text);
                    //}

                    if (field.DataField == "Obj_Type")
                    {
                        text = ObjektumTipusok.GetDisplayableNameFromObjectType(text);
                    }

                    sb.Append("<td  class=\"" + field.ItemStyle.CssClass + " " + hataridoCssClass + "\">");
                    sb.Append(SetTreeNodeUrl(text, id, parentTreeNode, width, isHeader, String.Empty));
                    sb.Append("</td>");
                }
            }
        }

        if (!isHeader)
        {
            Image locImage = new Image();
            //locImage.ID = TreeViewFeladatok.ID + "_" + id;
            UI.SetLockingInfo(row, Page, locImage);
            if (locImage.Visible)
            {
                sb.Append("</td> <td>");
                sb.Append(SetTreeNodeUrl(UI.RenderControl(locImage), id, parentTreeNode));
            }
        }
        sb.Append("</td></tr></table>");

        return sb.ToString();
    }

    private string GetHataridoCssClass(string Hatarido)
    {
        if (String.IsNullOrEmpty(Hatarido))
            return String.Empty;

        string cssClass = String.Empty;
        DateTime dtHatarido;
        if (DateTime.TryParse(Hatarido, out dtHatarido))
        {
            if (dtHatarido.Hour == 0 && dtHatarido.Minute == 0)
            {
                DateTime today = DateTime.Now.Date;
                if (dtHatarido < today)
                {
                    cssClass = " Feladat_ExpiredDate";
                }
            }
            else
            {
                DateTime now = DateTime.Now;
                if (dtHatarido < now)
                {
                    cssClass = " Feladat_ExpiredDate";
                }
            }
        }
        return cssClass;
    }

    private void UpdatePrevSelectedNodeColor()
    {
        if (!String.IsNullOrEmpty(PrevSelectedRecord) && !String.IsNullOrEmpty(PrevBackGroundColor))
        {
            TreeNode prevSelectedNode = TreeViewFeladatok.FindNode(PrevSelectedRecord);
            if (prevSelectedNode != null)
            {
                string text = prevSelectedNode.Text;
                text = "<span class=\"" + PrevBackGroundColor + "\">" + text + "</span>";
                prevSelectedNode.Text = text;

            }
        }
    }

    //private string GetTreeNodeText(EREC_HataridosFeladatok erec_hataridosFeladat,TreeNode selectedNode, TreeNode parentTreeNode)
    //{
    //    if (selectedNode == null)
    //        return String.Empty;

    //    DataTable table = new DataTable();
    //    table.Columns.Add("Id");
    //    table.Columns.Add("Kiado_Nev");
    //    table.Columns.Add("Felelos_Nev");
    //    table.Columns.Add("Leiras");
    //    table.Columns.Add("Allapot");
    //    table.Columns.Add("Allapot_Nev");
    //    table.Columns.Add("IntezkHatarido");
    //    table.Columns.Add("Prioritas");
    //    table.Columns.Add("Funkcio_Id_Inditando");
    //    table.Columns.Add("Funkcio_Nev_Inditando");
    //    table.Columns.Add("Obj_Id");
    //    table.Columns.Add("Azonosito");
    //    table.Columns.Add("Obj_type");
    //    table.Columns.Add("Obj_Count");
    //    table.Columns.Add("Zarolo_id");
    //    table.Columns.Add("ZarolasIdo");
    //    //table.Columns.Add("Tipus_Nev");
    //    DataRow row = table.NewRow();
    //    row["Id"] = erec_hataridosFeladat.Id;
    //    row["Kiado_Nev"] = FeladatView.KiadoNev;
    //    row["Felelos_Nev"] = FeladatView.FelelosNev;
    //    row["Leiras"] = erec_hataridosFeladat.Leiras;
    //    row["Allapot"] = erec_hataridosFeladat.Allapot;
    //    row["Allapot_Nev"] = FeladatView.AllapotNev;
    //    row["IntezkHatarido"] = erec_hataridosFeladat.IntezkHatarido;
    //    row["Prioritas"] = erec_hataridosFeladat.Prioritas;
    //    row["Funkcio_Id_Inditando"] = erec_hataridosFeladat.Funkcio_Id_Inditando;
    //    row["Funkcio_Nev_Inditando"] = FeladatView.FunkcioNev;
    //    row["Zarolo_id"] = erec_hataridosFeladat.Base.Zarolo_id;
    //    row["ZarolasIdo"] = erec_hataridosFeladat.Base.ZarolasIdo;
    //    row["Obj_Id"] = erec_hataridosFeladat.Obj_Id;
    //    row["Obj_type"] = erec_hataridosFeladat.Obj_type;
    //    row["Azonosito"] = erec_hataridosFeladat.Azonosito;

    //    return GetTreeNodeText(row,selectedNode, parentTreeNode);
    //}

    private string GetTreeNodeText(TreeNode selectedNode, TreeNode parentTreeNode)
    {
        if (selectedNode == null)
            return String.Empty;

        string FeladatId = selectedNode.Value;

        if (String.IsNullOrEmpty(FeladatId))
            return String.Empty;

        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        EREC_HataridosFeladatokSearch sch = new EREC_HataridosFeladatokSearch();
        sch.Id.Value = FeladatId;
        sch.Id.Operator = Query.Operators.equals;
        ExecParam xpm = UI.SetExecParamDefault(Page);
        Result res = svc.GetAllWithExtension(xpm, sch);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        }

        if (res.Ds == null || res.Ds.Tables.Count == 0 || res.Ds.Tables[0].Rows.Count == 0)
        {
            return String.Empty;
        }

        DataRow row = res.Ds.Tables[0].Rows[0];


        return GetTreeNodeText(row, selectedNode, parentTreeNode);
    }

    private string SetTreeNodeUrl(string szoveg, string Id, TreeNode parentNode, Unit width, bool isHeader, string sortExpression)
    {
        return SetTreeNodeUrl(szoveg, Id, parentNode, width, isHeader, sortExpression, false);
    }

    private string SetTreeNodeUrl(string szoveg, string Id, TreeNode parentNode, Unit width, bool isHeader, string sortExpression, bool postBack)
    {
        string parentNodeValuePath = "";
        string sWidth = String.Empty;
        if (parentNode != null)
        {
            parentNodeValuePath = parentNode.ValuePath.Replace("/", "\\\\") + "\\\\";
        }
        string onclick = String.Empty;
        string origSzoveg = szoveg;
        if (isHeader)
        {
            if (!String.IsNullOrEmpty(sortExpression) && TreeViewFeladatok.AllowSorting)
            {
                Image imgUp = new Image();
                imgUp.ImageUrl = "~/images/hu/egyeb/time_up.gif";
                imgUp.ToolTip = "Növekvõ";

                Image imgDown = new Image();
                imgDown.ImageUrl = "~/images/hu/egyeb/time_down.gif";
                imgDown.ToolTip = "Csökkenõ";

                string orderBy = GetOrderBy();
                Search.SortConditionList sortConditionList = new Search.SortConditionList(orderBy);

                if (sortConditionList.Contains(sortExpression))
                {
                    SortDirection sortDirection = sortConditionList.GetSortDirection(sortExpression);
                    string orderbyImage = String.Empty;
                    if (sortDirection == SortDirection.Descending)
                    {
                        orderbyImage = UI.RenderControl(imgDown);
                    }
                    else
                    {
                        orderbyImage = UI.RenderControl(imgUp);
                    }
                    szoveg += "&nbsp;" + orderbyImage;
                    if (sortConditionList.Count > 1)
                    {
                        szoveg += "(" + sortConditionList.GetSortNumber(sortExpression) + ")";
                    }
                }

                string href = "javascript:__doPostBack('" + TreeViewFeladatok.UniqueID + "','" + TreeViewFeladatok.GetSortEventArgument(sortExpression) + "');";
                szoveg = "<a href=\"" + href + "\">" + szoveg + "</a>";
            }
        }
        else if (postBack)
        {
            onclick = "javascript:__doPostBack('" + TreeViewFeladatok.UniqueID + "','s" + parentNodeValuePath + Id + "');";
        }

        if (!width.IsEmpty)
        {
            sWidth = "width:" + width.ToString() + ";";
        }

        return "<div style='" + (!String.IsNullOrEmpty(onclick) ? "cursor:pointer;" : "cursor:text;") + sWidth +
            "overflow:hidden;text-overflow:ellipsis;white-space:nowrap;' onclick=\"" + onclick + "\">"
            + szoveg + "</div>";
    }

    private string SetTreeNodeUrl(string szoveg, string Id, TreeNode parentNode)
    {
        string parentNodeValuePath = "";
        if (parentNode != null)
        {
            parentNodeValuePath = parentNode.ValuePath.Replace("/", "\\\\") + "\\\\";
        }
        return "<a href=\"javascript:__doPostBack('" + TreeViewFeladatok.UniqueID + "','s" + parentNodeValuePath + Id + "');\">" + szoveg + "</a>";
    }

    protected void DolgozokTextBox_TextChanged(object sender, EventArgs e)
    {
        if (ParentForm == Constants.ParentForms.DolgozokFeladatai)
        {
            bool isSearchObjectInSession;
            EREC_HataridosFeladatokSearch sch = GetSearchObject(out isSearchObjectInSession);
            // szûrés a megadott felhasználóra
            int index = sch.ReadableWhere.IndexOf("Felelõs");
            if (!String.IsNullOrEmpty(FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField))
            {
                sch.Felhasznalo_Id_Felelos.Value = FelhasznaloCsoportTextBox_Dolgozo.Id_HiddenField;
                sch.Felhasznalo_Id_Felelos.Operator = Query.Operators.equals;
                if (index > -1)
                {
                    int endIndex = sch.ReadableWhere.IndexOf(Search.whereDelimeter, index);
                    if (endIndex < 0)
                    {
                        endIndex = sch.ReadableWhere.Length;
                    }

                    sch.ReadableWhere = sch.ReadableWhere.Replace(sch.ReadableWhere.Substring(index, endIndex - index), "Felelõs: " + FelhasznaloCsoportTextBox_Dolgozo.Text);
                }
                else
                {
                    sch.ReadableWhere += "Felelõs: " + FelhasznaloCsoportTextBox_Dolgozo.Text;
                    sch.ReadableWhere += Search.whereDelimeter;
                }
            }
            else
            {
                sch.Felhasznalo_Id_Felelos.Clear();
                if (index > -1)
                {
                    int endIndex = sch.ReadableWhere.IndexOf(Search.whereDelimeter, index);
                    if (endIndex > -1)
                    {
                        sch.ReadableWhere = sch.ReadableWhere.Remove(index, endIndex + Search.whereDelimeter.Length - index);
                    }
                    else
                    {
                        sch.ReadableWhere = sch.ReadableWhere.Remove(index);
                    }
                }
            }
            if (!isSearchObjectInSession)
            {
                SaveSearchObject(sch);
            }
            LoadTreeView(null);
        }
    }

    #endregion

    protected void Lezaras_Command(object sender, CommandEventArgs e)
    {
        TreeNode selectedNode = TreeViewFeladatok.SelectedNode;

        if (selectedNode == null)
        {
            return;
        }

        string selectedId = selectedNode.Value;

        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        xpm.Record_Id = selectedId;

        Result res = svc.Lezaras(xpm);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            ErrorUpdatePanel1.Update();
        }

        RefreshSelectedNode();

    }

    protected void Sztorno_Command(object sender, CommandEventArgs e)
    {
        TreeNode selectedNode = TreeViewFeladatok.SelectedNode;

        if (selectedNode == null)
        {
            return;
        }

        string selectedId = selectedNode.Value;

        EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        xpm.Record_Id = selectedId;

        Result res = svc.Sztorno(xpm);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            ErrorUpdatePanel1.Update();
        }

        RefreshSelectedNode();

    }

    protected void TreeViewFeladatok_Sorting(object sender, GridViewSortEventArgs e)
    {
        SetOrderBy(e.SortExpression, e.SortDirection);
        ForgetSelectedRecord();
        LoadTreeView(null);
    }

    protected void Search_Command(object sender, CommandEventArgs e)
    {
    }

    #region Paging

    private void RefreshPaging(Result result)
    {
        DataSet ds = result.Ds;
        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count == 1)
        {
            // találatok száma:
            int recordNumberFromPaging = Int32.Parse(ds.Tables[1].Rows[0]["RecordNumber"].ToString());
            RecordNumber = recordNumberFromPaging;

            RefreshPagerLabel();

            // oldalszám:
            int pageIndex = Int32.Parse(ds.Tables[1].Rows[0]["PageNumber"].ToString());
            if (IsListHeader)
                PageIndex = pageIndex;
            else
                PageIndex = pageIndex + 1;

        }

        if (!String.IsNullOrEmpty(SelectedRecordId))
        {
            string[] ids = SelectedRecordId.Split('/');
            string id = String.Empty;
            for (int i = 0; i < ids.Length; i++)
            {
                if (!String.IsNullOrEmpty(id))
                    id += "/";
                id += ids[i];
                TreeNode selectedNode = TreeViewFeladatok.FindNode(id);
                if (selectedNode != null)
                {
                    if (i == ids.Length - 1)
                    {
                        selectedNode.Select();
                        RefreshSelectedNode();
                    }
                    else
                    {
                        selectedNode.Expand();
                    }
                }
            }
        }
    }
    private void SetPaging(ExecParam xpm)
    {
        if (IsListHeader)
        {
            _listheader.SelectedRecordId = GetTopSelectedRecordId();
            UI.SetPaging(xpm, _listheader);
        }
        else
        {
            xpm.Paging.PageSize = RowCount;

            xpm.Paging.PageNumber = PageIndex - 1;

            if (!String.IsNullOrEmpty(SelectedRecordId))
            {
                xpm.Paging.SelectedRowId = GetTopSelectedRecordId();
            }
        }
    }
    public int PageIndex
    {
        get
        {
            return _listheader.PageIndex;
        }
        set
        {
            _listheader.PageIndex = value;
            hfPageIndex.Value = value.ToString();
        }
    }
    public void RefreshPagerLabel()
    {
        // max. oldalszám:
        if (IsListHeader)
        {
            _listheader.RefreshPagerLabel();
        }
    }
    public int RecordNumber
    {
        get
        {
            return _listheader.RecordNumber;
        }
        set
        {
            _listheader.RecordNumber = value;
        }
    }
    public int RowCount
    {
        get
        {
            return Int32.Parse(_listheader.RowCount);
        }
    }
    public bool Scrollable
    {
        get
        {
            return _listheader.Scrollable;
        }
    }

    #region SelectedRecord properties

    public string PrevSelectedRecord
    {
        get
        {
            if (ViewState["PrevSelectedRecord"] == null)
                return String.Empty;
            return ViewState["PrevSelectedRecord"].ToString();
        }
        set
        {
            ViewState["PrevSelectedRecord"] = value;
        }
    }

    public string PrevBackGroundColor
    {
        get
        {
            if (ViewState["PrevBackGroundColor"] == null)
                return String.Empty;
            return ViewState["PrevBackGroundColor"].ToString();
        }
        set
        {
            ViewState["PrevBackGroundColor"] = value.Trim();
        }
    }

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(SelectedNodeId_HiddenField.Value))
            {
                return String.Empty;
            }
            else
            {
                return SelectedNodeId_HiddenField.Value;
            }
        }
        set
        {
            SelectedNodeId_HiddenField.Value = value;
            if (IsListHeader)
            {
                _listheader.SelectedRecordId = GetTopSelectedRecordId();
            }
        }
    }

    public string GetTopSelectedRecordId()
    {
        if (String.IsNullOrEmpty(SelectedRecordId))
            return String.Empty;
        return SelectedRecordId.Split('/')[0];
    }

    public string GetLastSelectedRecordId()
    {
        if (String.IsNullOrEmpty(SelectedRecordId))
            return String.Empty;

        int length = SelectedRecordId.Split('/').Length;

        return SelectedRecordId.Split('/')[length - 1];
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
        PrevSelectedRecord = String.Empty;
        PrevBackGroundColor = String.Empty;
        FeladatView.Reset();
    }

    #endregion

    protected void RowCountTextBox_TextChanged(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm != null)
        {
            if (IsListHeader)
                sm.SetFocus(_listheader.RefreshButton);
        }
        LoadTreeView(null);
    }

    void _listheader_PagingButtonsClick(object sender, EventArgs e)
    {
        ForgetSelectedRecord();
        JavaScripts.ResetScroll(Page, FeladatokCPE);
        LoadTreeView(null);
    }

    #endregion

    protected void ddListTreeView_ViewMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTreeView(null);
    }

    protected void cbUseUgyiratHierarchia_CheckedChanged(object sender, EventArgs e)
    {
        LoadTreeView(null);
    }
}
