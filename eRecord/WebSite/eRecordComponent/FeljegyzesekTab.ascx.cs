using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratFeljegyzesekTab : System.Web.UI.UserControl
{
    //private const string KodCsoportKezelesTipus = "KEZELESI_FELJEGYZES_KULDEMENY";
    private const string KodCsoport_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        //UI ui = new UI();
        //ui.SetClientScriptToGridViewSelectDeSelectButton(FeljegyzesekGridView);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        string FunctionRightPrefix = (ParentForm == Constants.ParentForms.IratPeldany ? Constants.ParentForms.IraIrat : ParentForm);

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, FunctionRightPrefix + "Feljegyzes" + CommandName.New);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, FunctionRightPrefix + "Feljegyzes" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, FunctionRightPrefix + "Feljegyzes" + CommandName.Invalidate);
        //TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.View);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.ElosztoLista);

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(FeljegyzesekGridView));
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(FeljegyzesekGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        //TabFooter1.SaveValidationGroup = "Feljegyzesek";
        //KezelesTipusKodtarakDropDownList.ValidationGroup = TabFooter1.SaveValidationGroup;
        //LeirasTextBox. = TabFooter1.SaveValidationGroup;        

    }

    protected void FeljegyzesekUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {

        if (ParentForm != Constants.ParentForms.IraIrat
            && ParentForm != Constants.ParentForms.Kuldemeny
            && ParentForm != Constants.ParentForms.Ugyirat
            && ParentForm != Constants.ParentForms.IratPeldany)
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
        else
        {

            //if (ParentForm == Constants.ParentForms.Ugyirat)
            //{
            //    MainPanel.Visible = false;
            //    return;
            //}


            if (!MainPanel.Visible) return;

            // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
            TabHeader1.ViewVisible = false;
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;

            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FeljegyzesekGridView.ClientID);
            //TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            ClearForm();
            EFormPanel1.Visible = false;
            FeljegyzesekGridViewBind();
            pageView.SetViewOnPage(Command);

            // p�ld�ny sorsz�mot csak az iratn�l mutatunk
            int indexSorszam = UI.GetGridViewColumnIndex(FeljegyzesekGridView, "Sorszam");
            if (indexSorszam > -1)
            {
                if (ParentForm == Constants.ParentForms.IraIrat)
                {
                    FeljegyzesekGridView.Columns[indexSorszam].Visible = true;
                }
                else
                {
                    FeljegyzesekGridView.Columns[indexSorszam].Visible = false;
                }
            }
        }
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //  nem ugyanaz a k�dcsoport a k�t esetben:
        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            //KezelesTipusKodtarakDropDownList.FillDropDownList(KodCsoportKezelesTipus, EErrorPanel1);
            KezelesTipusKodtarakDropDownList.FillDropDownList(KodCsoport_KEZELESI_FELJEGYZESEK_TIPUSA
                , KodTarak.KEZELESI_FELJEGYZESEK_TIPUSA.GetKuldemenyFilterList(), false, EErrorPanel1);
        }
        else if (ParentForm == Constants.ParentForms.IraIrat
                || ParentForm == Constants.ParentForms.IratPeldany
                || ParentForm == Constants.ParentForms.Ugyirat)
        {
            KezelesTipusKodtarakDropDownList.FillDropDownList(KodCsoport_KEZELESI_FELJEGYZESEK_TIPUSA, EErrorPanel1);
        }               

        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;

            String RecordId = UI.GetGridViewSelectedRecordId(FeljegyzesekGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;
                Result result = null;

                if (ParentForm == Constants.ParentForms.Kuldemeny)
                {
                    EREC_KuldKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_KuldKezFeljegyzesekService();
                    result = service.Get(execParam);
                }
                else if (ParentForm == Constants.ParentForms.IraIrat)
                {
                    EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                    result = service.Get(execParam);
                }
                else if (ParentForm == Constants.ParentForms.IratPeldany)
                {
                    EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                    result = service.Get(execParam);
                }
                else if (ParentForm == Constants.ParentForms.Ugyirat)
                {
                    EREC_UgyKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
                    result = service.Get(execParam);
                }

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    LoadComponentsFromBusinessObject(result.Record);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedFeljegyzesek();
            FeljegyzesekGridViewBind();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            string FunctionRightPrefix = (ParentForm == Constants.ParentForms.IratPeldany ? Constants.ParentForms.IraIrat : ParentForm);
            if (FunctionRights.GetFunkcioJog(Page, FunctionRightPrefix + "Feljegyzes" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            Result result = null;
                            if (ParentForm == Constants.ParentForms.Kuldemeny)
                            {
                                EREC_KuldKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_KuldKezFeljegyzesekService();
                                EREC_KuldKezFeljegyzesek EREC_KuldKezFeljegyzesek = (EREC_KuldKezFeljegyzesek)GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                result = service.Insert(execParam, EREC_KuldKezFeljegyzesek);
                            }

                            if (ParentForm == Constants.ParentForms.IraIrat)
                            {
                                EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                                EREC_IraKezFeljegyzesek EREC_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                result = service.Insert(execParam, EREC_IraKezFeljegyzesek);
                            }

                            if (ParentForm == Constants.ParentForms.IratPeldany)
                            {
                                EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                                EREC_IraKezFeljegyzesek EREC_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                result = service.Insert(execParam, EREC_IraKezFeljegyzesek);
                            }

                            if (ParentForm == Constants.ParentForms.Ugyirat)
                            {
                                EREC_UgyKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
                                EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = (EREC_UgyKezFeljegyzesek)GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                result = service.Insert(execParam, erec_UgyKezFeljegyzesek);
                            }

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ReLoadTab();
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = UI.GetGridViewSelectedRecordId(FeljegyzesekGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                Result result = null;
                                if (ParentForm == Constants.ParentForms.Kuldemeny)
                                {
                                    EREC_KuldKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_KuldKezFeljegyzesekService();
                                    EREC_KuldKezFeljegyzesek EREC_KuldKezFeljegyzesek = (EREC_KuldKezFeljegyzesek)GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, EREC_KuldKezFeljegyzesek);
                                }

                                if (ParentForm == Constants.ParentForms.IraIrat)
                                {
                                    EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                                    EREC_IraKezFeljegyzesek EREC_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, EREC_IraKezFeljegyzesek);
                                }

                                if (ParentForm == Constants.ParentForms.IratPeldany)
                                {
                                    EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                                    EREC_IraKezFeljegyzesek EREC_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, EREC_IraKezFeljegyzesek);
                                }

                                if (ParentForm == Constants.ParentForms.Ugyirat)
                                {
                                    EREC_UgyKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
                                    EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = (EREC_UgyKezFeljegyzesek)GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_UgyKezFeljegyzesek);
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                //UI.RedirectToAccessDeniedErrorPage(Page);
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private void ClearForm()
    {        
        if (KezelesTipusKodtarakDropDownList.DropDownList.Items.Count > 0)
            KezelesTipusKodtarakDropDownList.DropDownList.SelectedIndex = 0;
        LeirasTextBox.Text = "";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(Object KezFeljegyzesek)
    {        
        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = (EREC_KuldKezFeljegyzesek)KezFeljegyzesek;

            KezelesTipusKodtarakDropDownList.SetSelectedValue(erec_KuldKezFeljegyzesek.KezelesTipus);
            LeirasTextBox.Text = erec_KuldKezFeljegyzesek.Leiras;

            Record_Ver_HiddenField.Value = erec_KuldKezFeljegyzesek.Base.Ver;

            if (Command == CommandName.View)
            {
                //KezelesTipusKodtarakDropDownList.DropDownList.Readonly???
                LeirasTextBox.ReadOnly = true;
            }
            else
            {
                LeirasTextBox.ReadOnly = false;
            }
        }

        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)KezFeljegyzesek;

            KezelesTipusKodtarakDropDownList.SetSelectedValue(erec_IraKezFeljegyzesek.KezelesTipus);            
            LeirasTextBox.Text = erec_IraKezFeljegyzesek.Leiras;

            Record_Ver_HiddenField.Value = erec_IraKezFeljegyzesek.Base.Ver;

            if (Command == CommandName.View)
            {
                //KezelesTipusKodtarakDropDownList.DropDownList.Readonly???
                LeirasTextBox.ReadOnly = true;
            }
            else
            {
                LeirasTextBox.ReadOnly = false;
            }        
        }

        if (ParentForm == Constants.ParentForms.IratPeldany)
        {
            EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = (EREC_IraKezFeljegyzesek)KezFeljegyzesek;

            KezelesTipusKodtarakDropDownList.SetSelectedValue(erec_IraKezFeljegyzesek.KezelesTipus);
            LeirasTextBox.Text = erec_IraKezFeljegyzesek.Leiras;

            Record_Ver_HiddenField.Value = erec_IraKezFeljegyzesek.Base.Ver;

            if (Command == CommandName.View)
            {
                //KezelesTipusKodtarakDropDownList.DropDownList.Readonly???
                LeirasTextBox.ReadOnly = true;
            }
            else
            {
                LeirasTextBox.ReadOnly = false;
            }
        }

        if (ParentForm == Constants.ParentForms.Ugyirat)
        {
            EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = (EREC_UgyKezFeljegyzesek)KezFeljegyzesek;

            KezelesTipusKodtarakDropDownList.SetSelectedValue(erec_UgyKezFeljegyzesek.KezelesTipus);
            LeirasTextBox.Text = erec_UgyKezFeljegyzesek.Leiras;

            Record_Ver_HiddenField.Value = erec_UgyKezFeljegyzesek.Base.Ver;

            if (Command == CommandName.View)
            {
                //KezelesTipusKodtarakDropDownList.DropDownList.Readonly???
                LeirasTextBox.ReadOnly = true;
            }
            else
            {
                LeirasTextBox.ReadOnly = false;
            }
        }


        //FormPart_CreatedModified1.SetComponentValues(EREC_KuldKezFeljegyzesek.Base);
    }

    // form --> business object
    private Object GetBusinessObjectFromComponents()
    {
        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = new EREC_KuldKezFeljegyzesek();
            erec_KuldKezFeljegyzesek.Updated.SetValueAll(false);
            erec_KuldKezFeljegyzesek.Base.Updated.SetValueAll(false);

            erec_KuldKezFeljegyzesek.KuldKuldemeny_Id = ParentId;
            erec_KuldKezFeljegyzesek.Updated.KuldKuldemeny_Id = true;

            erec_KuldKezFeljegyzesek.KezelesTipus = KezelesTipusKodtarakDropDownList.SelectedValue;
            erec_KuldKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezelesTipusKodtarakDropDownList);

            erec_KuldKezFeljegyzesek.Leiras = LeirasTextBox.Text;
            erec_KuldKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(LeirasTextBox);

            erec_KuldKezFeljegyzesek.Base.Ver = Record_Ver_HiddenField.Value;
            erec_KuldKezFeljegyzesek.Base.Updated.Ver = true;

            return erec_KuldKezFeljegyzesek;
        }

        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = new EREC_IraKezFeljegyzesek();
            erec_IraKezFeljegyzesek.Updated.SetValueAll(false);
            erec_IraKezFeljegyzesek.Base.Updated.SetValueAll(false);

            // az els� iratp�ld�nyhoz k�t�nk
            string Pld_Id = "";
            #region 1. sorsz�m� p�ld�ny
            EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam_pld = UI.SetExecParamDefault(Page);
            Result result_pld = service_pld.GetElsoIratPeldanyByIraIrat(execParam_pld, ParentId);
            if (!String.IsNullOrEmpty(result_pld.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_pld);
                ErrorUpdatePanel1.Update();
                return null;
            }
            EREC_PldIratPeldanyok elsoIratPeldany = (EREC_PldIratPeldanyok)result_pld.Record;
            Pld_Id = elsoIratPeldany.Id;
            #endregion 1. sorsz�m� p�ld�ny

            erec_IraKezFeljegyzesek.IraIrat_Id = Pld_Id;
            erec_IraKezFeljegyzesek.Updated.IraIrat_Id = true;

            erec_IraKezFeljegyzesek.KezelesTipus = KezelesTipusKodtarakDropDownList.SelectedValue;
            erec_IraKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezelesTipusKodtarakDropDownList);
                        
            erec_IraKezFeljegyzesek.Leiras = LeirasTextBox.Text;
            erec_IraKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(LeirasTextBox);

            erec_IraKezFeljegyzesek.Base.Ver = Record_Ver_HiddenField.Value;
            erec_IraKezFeljegyzesek.Base.Updated.Ver = true;

            return erec_IraKezFeljegyzesek;        
        }

        if (ParentForm == Constants.ParentForms.IratPeldany)
        {
            EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = new EREC_IraKezFeljegyzesek();
            erec_IraKezFeljegyzesek.Updated.SetValueAll(false);
            erec_IraKezFeljegyzesek.Base.Updated.SetValueAll(false);

             erec_IraKezFeljegyzesek.IraIrat_Id = ParentId;
            erec_IraKezFeljegyzesek.Updated.IraIrat_Id = true;

            erec_IraKezFeljegyzesek.KezelesTipus = KezelesTipusKodtarakDropDownList.SelectedValue;
            erec_IraKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezelesTipusKodtarakDropDownList);

            erec_IraKezFeljegyzesek.Leiras = LeirasTextBox.Text;
            erec_IraKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(LeirasTextBox);

            erec_IraKezFeljegyzesek.Base.Ver = Record_Ver_HiddenField.Value;
            erec_IraKezFeljegyzesek.Base.Updated.Ver = true;

            return erec_IraKezFeljegyzesek;
        }

        if (ParentForm == Constants.ParentForms.Ugyirat)
        {
            EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = new EREC_UgyKezFeljegyzesek();
            erec_UgyKezFeljegyzesek.Updated.SetValueAll(false);
            erec_UgyKezFeljegyzesek.Base.Updated.SetValueAll(false);

            erec_UgyKezFeljegyzesek.UgyUgyirat_Id = ParentId;
            erec_UgyKezFeljegyzesek.Updated.UgyUgyirat_Id = true;

            erec_UgyKezFeljegyzesek.KezelesTipus = KezelesTipusKodtarakDropDownList.SelectedValue;
            erec_UgyKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezelesTipusKodtarakDropDownList);

            erec_UgyKezFeljegyzesek.Leiras = LeirasTextBox.Text;
            erec_UgyKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(LeirasTextBox);

            erec_UgyKezFeljegyzesek.Base.Ver = Record_Ver_HiddenField.Value;
            erec_UgyKezFeljegyzesek.Base.Updated.Ver = true;

            return erec_UgyKezFeljegyzesek;
        }        

        return null;
    }


    #endregion

    #region List
    protected void FeljegyzesekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FeljegyzesekGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FeljegyzesekGridView", ViewState);
        FeljegyzesekGridViewBind(sortExpression, sortDirection);
    }

    protected void FeljegyzesekGridViewBind(String SortExpression, SortDirection SortDirection)
    {        
        // ha nincs megadva a ParentId, nem tudunk sz�rni --> kil�p�s
        if (String.IsNullOrEmpty(ParentId))
        {
            return;
        }

        UI.ClearGridViewRowSelection(FeljegyzesekGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result res = null;

        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            EREC_KuldKezFeljegyzesekService service = null;
            EREC_KuldKezFeljegyzesekSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_KuldKezFeljegyzesekService();
            search = (EREC_KuldKezFeljegyzesekSearch)Search.GetSearchObject(Page, new EREC_KuldKezFeljegyzesekSearch());
            
            search.KuldKuldemeny_Id.Value = ParentId;
            search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            
            search.OrderBy = Search.GetOrderBy("FeljegyzesekGridView", ViewState, SortExpression, SortDirection);
            res = service.GetAllWithExtension(ExecParam, search);
        }

        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            EREC_IraKezFeljegyzesekService service = null;
            EREC_IraKezFeljegyzesekSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
            search = (EREC_IraKezFeljegyzesekSearch)Search.GetSearchObject(Page, new EREC_IraKezFeljegyzesekSearch());
          
            search.Manual_IraIratok_Id.Value = ParentId;
            search.Manual_IraIratok_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            
            search.OrderBy = Search.GetOrderBy("FeljegyzesekGridView", ViewState, SortExpression, SortDirection);
            res = service.GetAllWithExtension(ExecParam, search);
        }

        if (ParentForm == Constants.ParentForms.IratPeldany)
        {
            EREC_IraKezFeljegyzesekService service = null;
            EREC_IraKezFeljegyzesekSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
            search = (EREC_IraKezFeljegyzesekSearch)Search.GetSearchObject(Page, new EREC_IraKezFeljegyzesekSearch());

            search.IraIrat_Id.Value = ParentId;
            search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("FeljegyzesekGridView", ViewState, SortExpression, SortDirection);
            res = service.GetAllWithExtension(ExecParam, search);
        }

        if (ParentForm == Constants.ParentForms.Ugyirat)
        {
            EREC_UgyKezFeljegyzesekService service = null;
            EREC_UgyKezFeljegyzesekSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
            search = (EREC_UgyKezFeljegyzesekSearch)Search.GetSearchObject(Page, new EREC_UgyKezFeljegyzesekSearch());

            search.UgyUgyirat_Id.Value = ParentId;
            search.UgyUgyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("FeljegyzesekGridView", ViewState, SortExpression, SortDirection);
            res = service.GetAllWithExtension(ExecParam, search);
        }

        ui.GridViewFill(FeljegyzesekGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";
        }
    }

    protected void FeljegyzesekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FeljegyzesekGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    /// <summary>
    /// T�rli a FeljegyzesekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedFeljegyzesek()
    {
        string FunctionRightPrefix = (ParentForm == Constants.ParentForms.IratPeldany ? Constants.ParentForms.IraIrat : ParentForm);
        if (FunctionRights.GetFunkcioJog(Page, FunctionRightPrefix + "FeljegyzesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FeljegyzesekGridView, EErrorPanel1, ErrorUpdatePanel1);

            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            if (ParentForm == Constants.ParentForms.Kuldemeny)
            {
                EREC_KuldKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_KuldKezFeljegyzesekService();                
                Result result = service.MultiInvalidate(execParams);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
            else if (ParentForm == Constants.ParentForms.IraIrat)
            {
                EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                Result result = service.MultiInvalidate(execParams);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
            else if (ParentForm == Constants.ParentForms.IratPeldany)
            {
                EREC_IraKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_IraKezFeljegyzesekService();
                Result result = service.MultiInvalidate(execParams);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
            else if (ParentForm == Constants.ParentForms.Ugyirat)
            {
                EREC_UgyKezFeljegyzesekService service = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
                Result result = service.MultiInvalidate(execParams);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        else
        {
            //UI.RedirectToAccessDeniedErrorPage(Page);
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        }
    }


    #endregion
    protected void FeljegyzesekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void FeljegyzesekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FeljegyzesekGridViewBind(e.SortExpression, UI.GetSortToGridView("FeljegyzesekGridView", ViewState, e.SortExpression));
    }
}
