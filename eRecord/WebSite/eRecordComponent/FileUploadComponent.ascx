<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileUploadComponent.ascx.cs" Inherits="eRecordComponent_FileUploadComponent" %>

<div style="left:0px;text-align:left;">
    <asp:UpdatePanel ID="updatePanel" runat="server" OnPreRender="updatePanel_PreRender">
        <ContentTemplate>
            <div style="width:100%;height:100%;vertical-align:top;">
                <span class="fu_textIndicator">
                    <asp:ImageButton TabIndex = "-1" runat="server" ID="cpeButton" ImageUrl="~/images/hu/Grid/plus.gif" OnClientClick="return false;" Visible="false" />
                    <asp:Label ID="labelIndicator" runat="server" CssClass="mrUrlapInput"></asp:Label>
                </span>
                <span class="fu_imageButton">
                    <asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
                    AlternateText="Kiv�laszt"/>
                </span>
                <asp:TextBox ID="hiddenTextBox" Width="0" BorderStyle="None" BackColor="Transparent" runat="server" Text="a"></asp:TextBox>
                <asp:HiddenField ID="HiddenField1" runat="server" />
             </div>
             <div style="top:-5px;">
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeFiles" runat="server" TargetControlID="panelFileList"
                    CollapsedSize="0" Collapsed="True" ExpandControlID="cpeButton" CollapseControlID="cpeButton" ExpandDirection="Vertical"
                    AutoCollapse="False" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif" ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="cpeButton"             
                    ExpandedText="F�jlok elrejt�se" CollapsedText="F�jlok megtekint�se" Enabled="false">            
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel runat="server" ID="panelFileList" Wrap="true" HorizontalAlign="Left">
                        <asp:Label ID="labeFileList" runat="server" Width="100%" CssClass="fu_textFiles"></asp:Label>  
                </asp:Panel>
             </div>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" EnableClientScript="true" OnServerValidate="ServerValidateFileUpload"
            ControlToValidate="hiddenTextBox" ErrorMessage="<%$Resources:Form,RequiredFileValidationMessage%>"></asp:CustomValidator>
            <ajaxToolkit:ValidatorCalloutExtender  ID="ValidatorCalloutExtender1" runat="server" TargetControlID="CustomValidator1">
            <Animations>
                <OnShow>
                <Sequence>
                    <HideAction Visible="true" />
                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                </Sequence>    
                </OnShow>
            </Animations>
            </ajaxToolkit:ValidatorCalloutExtender>
         </ContentTemplate>
     </asp:UpdatePanel>
 </div>