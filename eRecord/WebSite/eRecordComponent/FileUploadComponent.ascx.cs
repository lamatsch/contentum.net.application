using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using System.IO;

public partial class eRecordComponent_FileUploadComponent : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponent
{
    private const string NoSelectedFile = "Dokumentum felt�lt�se";
    private const string IsSelectedFile = "Kiv�lasztott f�jlok sz�ma: ";

    public bool Validate
    {
        get 
        {
            return CustomValidator1.Enabled;
        }
        set
        {
            CustomValidator1.Enabled = value;
            ValidatorCalloutExtender1.Enabled = value;
        }

    }
    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string Text
    {
        set { labelIndicator.Text = value; }
        get { return labelIndicator.Text; }
    }

    public Label Label
    {
        get { return labelIndicator; }
    }

    public string Value_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public void Clear()
    {
        labeFileList.Text = String.Empty;
        HiddenField1.Value = String.Empty;
        disableFileList();
    }

    private void enableFileList()
    {
        if (Enabled)
        {
            panelFileList.Visible = true;
            cpeFiles.Enabled = true;
            cpeButton.Visible = true;
            cpeFiles.ClientState = true.ToString();
        }
    }

    private void disableFileList()
    {
        panelFileList.Visible = false;
        cpeFiles.Enabled = false;
        cpeButton.Visible = false;
        labelIndicator.Text = NoSelectedFile;
    }

    private bool enabled = true;
    public bool Enabled
    {
        set
        {
            enabled = value;
            LovImageButton.Enabled = value;
            panelFileList.Visible = value;
            cpeFiles.Enabled = value;
            cpeButton.Visible = value;
            if (value == false)
            {
                labelIndicator.CssClass = "mrUrlapInputWaterMarked";
                LovImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                labelIndicator.CssClass = "";
                LovImageButton.CssClass = "";
            }
        }
        get { return enabled; }
    }

    private bool readOnly = false;

    public bool ReadOnly
    {
        set
        {
            readOnly = value;
            LovImageButton.Enabled = !value;
            panelFileList.Visible = !value;
            cpeFiles.Enabled = !value;
            cpeButton.Visible = !value;
            if (value)
            {
                LovImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "";
            }
        }
        get { return readOnly; }
    }

    private bool singleMode = false;

    public bool SingleMode
    {
        get { return singleMode; }
        set { singleMode = value; }
    }

    private void setOnclickLov()
    {
        string queryMode = QueryStringVars.fileUploadMode + "=" + Constants.FileUploadMode.multiple.ToString();
        if (SingleMode)
        {
            queryMode = QueryStringVars.fileUploadMode + "=" + Constants.FileUploadMode.single.ToString();
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("FileUploadForm.aspx",
          QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + queryMode + "&t" + "=" + DateTime.Now.ToString(),
          Defaults.PopupWidth, Defaults.PopupHeight,
          updatePanel.ClientID, EventArgumentConst.refreshFileUploadComponent);
    }

    protected void Page_Init(object sender, EventArgs e)
    { 
        if(!IsPostBack)
        {
            setOnclickLov();
            disableFileList();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //validator script regisztr�l�sa
    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    { 
            JavaScripts.RegisterOnValidatorOverClientScript(Page);
            string vfName = "Validate_" + this.ID;
            string js = @"function " + vfName + @"(source, arguments)
                    {
                       var hidden = $get('" + HiddenField.ClientID + @"');
                       var parts = hidden.value.split(';');
                       if(parts.length>1)
                       {
                            arguments.IsValid = true;
                       }
                       else
                       {
                            arguments.IsValid = false;
                       }
                    }";

            ScriptManager.RegisterStartupScript(this, this.GetType(), vfName, js, true);
            CustomValidator1.ClientValidationFunction = vfName;
    }
    protected void updatePanel_PreRender(object sender, EventArgs e)
    {
        string eventArgument = Request.Params.Get("__EVENTARGUMENT");
        if (!String.IsNullOrEmpty(eventArgument))
        {
            if (eventArgument == EventArgumentConst.refreshFileUploadComponent)
            {
                labeFileList.Text = "";
                if (HiddenField.Value.Trim() != String.Empty)
                {
                    string[] files = HiddenField.Value.Split(';');
                    if (files.Length > 1)
                    {
                        int fileCount = files.Length - 1;
                        labelIndicator.Text = IsSelectedFile + fileCount.ToString();
                        for (int i = 1; i < files.Length; i++)
                        {
                            string fileName = Path.GetFileName(files[i]);
                            if (labeFileList.Text == String.Empty)
                                labeFileList.Text = fileName;
                            else
                                labeFileList.Text += " | " + fileName;
                          
                        }
                        enableFileList();
                    }
                    else
                    {
                        disableFileList();
                    }

                    setOnclickLov();
                }
                else
                {
                    disableFileList();
                }
            }
        }
    }

    //szerver oladali valid�l�s
    protected void ServerValidateFileUpload(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        if(HiddenField.Value.Trim() != String.Empty)
        {
            string[] files = HiddenField.Value.Split(';');
            if (files.Length > 1)
            {
                args.IsValid = true;
            }
        }

    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(labelIndicator);
        componentList.Add(LovImageButton);
        disableFileList();

        Validate = false;

        return componentList;
    }

    private bool _ViewEnabled = true;
    public bool ViewEnabled
    {
        get
        {
            return _ViewEnabled;
        }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !value;
            if (!value)
            {
                UI.AddCssClass(labelIndicator, "ViewReadOnlyWebControl");
                UI.AddCssClass(LovImageButton, "ViewReadOnlyWebControl");
            }
        }
    }

    private bool _ViewVisible = true;
    public bool ViewVisible
    {
        get
        {
            return _ViewVisible;
        }
        set
        {
            _ViewVisible = value;
            Enabled = value;
            if (!value)
            {
                UI.AddCssClass(labelIndicator, "ViewDisabledWebControl");
                UI.AddCssClass(LovImageButton, "ViewDisabledWebControl");
            }
        }
    }

    #endregion
}
