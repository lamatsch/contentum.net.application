<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FunkcioGombsor.ascx.cs"
    Inherits="Component_FunkcioGombsor" %>

<asp:UpdatePanel ID="_FunkcioGombsorUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="_FunkcioGombsorPanel" runat="server">
                <asp:ImageButton TabIndex = "-1" ID="Uj_ugyirat" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_letrehozasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Uj_ugyirat" AlternateText="<%$Resources:Buttons,Uj_ugyirat %>" />
                <asp:ImageButton TabIndex = "-1" ID="Modositas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/modositas.jpg"
                    OnClick="ImageButton_Click" CommandName="Modositas" AlternateText="<%$Resources:Buttons,Modositas %>" />
                <asp:ImageButton TabIndex = "-1" ID="Szignalas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szignalas.jpg"
                    OnClick="ImageButton_Click" CommandName="Szignalas" AlternateText="<%$Resources:Buttons,Szignalas %>" />
                <asp:ImageButton TabIndex = "-1" ID="Szignalas_vissza" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szignalas_vissza.jpg"
                    OnVonalkodNyomtatasaClick="ImageButton_Click" CommandName="Szignalas_vissza" AlternateText="<%$Resources:Buttons,Szignalas_vissza %>" />
                    
                <asp:ImageButton TabIndex = "-1" ID="AtvetelUgyintezesre" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/atvetel_ugyintezesre.jpg"
                    OnClick="ImageButton_Click" CommandName="AtvetelUgyintezesre" AlternateText="<%$Resources:Buttons,AtvetelUgyintezesre %>" />                    
                    
                <asp:ImageButton TabIndex = "-1" ID="Szkontro_inditas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szkontro_inditas.jpg"
                    OnClick="ImageButton_Click" CommandName="Szkontro_inditas" AlternateText="<%$Resources:Buttons,Szkontro_inditas %>" />
                <asp:ImageButton TabIndex = "-1" ID="Szkontrobol_vissza" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szkontrobol_visszavetel.jpg"
                    OnClick="ImageButton_Click" CommandName="Szkontrobol_vissza" AlternateText="<%$Resources:Buttons,Szkontrobol_vissza %>" />
                <asp:ImageButton TabIndex = "-1" ID="Felfuggesztes" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/felfuggesztes.jpg"
                    OnClick="ImageButton_Click" CommandName="Felfuggesztes" AlternateText="<%$Resources:Buttons,Felfuggesztes %>" />
                <asp:ImageButton TabIndex = "-1" ID="Felfuggesztesbol_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/felfuggesztesbol_visszavetel.jpg" OnClick="ImageButton_Click"
                    CommandName="Felfuggesztesbol_vissza" AlternateText="<%$Resources:Buttons,Felfuggesztesbol_vissza %>" />
                <asp:ImageButton TabIndex = "-1" ID="Tovabbitas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/tovabbitas.jpg"
                    OnClick="ImageButton_Click" CommandName="Tovabbitas" AlternateText="<%$Resources:Buttons,Tovabbitas %>" />
                    
                    <asp:ImageButton TabIndex = "-1" ID="Atadasra_kijeloles" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/tovabbitasrakijelol.jpg"
                    OnClick="ImageButton_Click" CommandName="AtadasraKijeloles" AlternateText="�tad�sra kijel�l" />
                    
                    <asp:ImageButton TabIndex = "-1" ID="Elintezette_nyilvanitas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/elintezettenyilvanit.jpg"
                    OnClick="ImageButton_Click" CommandName="ElintezetteNyilvanitas" AlternateText="<%$Resources:Buttons,ElintezetteNyilvanitas %>" />
                    
                    
                <asp:ImageButton TabIndex = "-1" ID="Sztornozas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/sztornozas.jpg"
                    OnClick="ImageButton_Click" CommandName="Sztornozas" AlternateText="<%$Resources:Buttons,Sztornozas %>" />
                <asp:ImageButton TabIndex = "-1" ID="Ugyintezes_lezarasa" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyintezes_lezarasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyintezes_lezarasa" AlternateText="<%$Resources:Buttons,Ugyintezes_lezarasa %>" />
                <asp:ImageButton TabIndex = "-1" ID="Ugyintezes_lezarasa_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/ugyintezes_lezarasa_vissza.jpg" OnClick="ImageButton_Click"
                    CommandName="Ugyintezes_lezarasa_vissza" AlternateText="<%$Resources:Buttons,Ugyintezes_lezarasa_vissza %>" />
                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_lezarasa" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_lezarasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_lezarasa" AlternateText="<%$Resources:Buttons,Ugyirat_lezarasa %>" />
                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_lezarasa_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/ugyirat_lezarasa_vissza.jpg" OnClick="ImageButton_Click"
                    CommandName="Ugyirat_lezarasa_vissza" AlternateText="<%$Resources:Buttons,Ugyirat_lezarasa_vissza %>" />
                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_irattarozasra_kijeloles" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/irattarozasrakijelol.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_irattarozasra_kijeloles" AlternateText="<%$Resources:Buttons,Irattarozasra_kijeloles %>" />

                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_AlszamraIktatasraElokeszit" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/AlszamraIktatasraElokeszit.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_AlszamraIktatasraElokeszit" AlternateText="Bels� iktat�sra el�k�sz�t�s" /><br />
                    
                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_AlszamraIktatasKimenoBelso" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_AlszamraIktatasKimenoBelso.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_AlszamraIktatasKimenoBelso" AlternateText="<%$Resources:Buttons,Ugyirat_AlszamraIktatasKimenoBelso %>" />
                    
                <asp:ImageButton TabIndex = "-1" ID="Ugyirat_AlszamraIktatasBejovo" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_AlszamraIktatasBejovo.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_AlszamraIktatasBejovo" AlternateText="<%$Resources:Buttons,Ugyirat_AlszamraIktatasBejovo %>" />
                    
                <asp:ImageButton TabIndex = "-1" ID="Csatolas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/csatolas.jpg"
                    OnClick="ImageButton_Click" CommandName="Csatolas" AlternateText="<%$Resources:Buttons,Csatolas %>" />                    
                    
                <asp:ImageButton TabIndex = "-1" ID="Szereles" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szereles.jpg"
                    OnClick="ImageButton_Click" CommandName="Szereles" AlternateText="<%$Resources:Buttons,Szereles %>" />                    
                    
                <asp:ImageButton TabIndex = "-1" ID="IratPeldany_Letrehozasa" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/iratpeldanyletrehozas.jpg" OnClick="ImageButton_Click"
                    CommandName="IratPeldanyLetrehozas" AlternateText="<%$Resources:Buttons,IratPeldanyLetrehozas %>" />                      
                <asp:ImageButton TabIndex = "-1" ID="Postazas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/postazas.jpg" OnClick="ImageButton_Click"
                    CommandName="Postazas" AlternateText="<%$Resources:Buttons,Postazas %>" />  
                <asp:ImageButton TabIndex = "-1" ID="Postazoba" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/postazoba.jpg" OnClick="ImageButton_Click"
                    CommandName="Postazoba" AlternateText="<%$Resources:Buttons,Postazoba %>" />  
                <asp:ImageButton TabIndex = "-1" ID="Expedialas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/expedialas.jpg" OnClick="ImageButton_Click"
                    CommandName="Expedialas" AlternateText="<%$Resources:Buttons,Expedialas %>" />  
                <asp:ImageButton TabIndex = "-1" ID="Valasz" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/valasz.jpg" OnClick="ImageButton_Click"
                    CommandName="Valasz" AlternateText="<%$Resources:Buttons,Valasz %>" />  
                <asp:ImageButton TabIndex = "-1" ID="IratPeldanyTovabbitas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/iratpeldanytovabbitas.jpg" OnClick="ImageButton_Click"
                    CommandName="IratPeldanyTovabbitas" AlternateText="Iratp�ld�ny tov�bb�t�s" />  
                <asp:ImageButton TabIndex = "-1" ID="Iktatas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/iktatas.jpg" OnClick="ImageButton_Click"
                    CommandName="Iktatas" AlternateText="<%$Resources:Buttons,Iktatas %>" />
                  <%--// CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n--%>
                <asp:ImageButton TabIndex = "-1" ID="Munkapeldany_Iktatas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/AlszamraIktatasraElokeszit.jpg" OnClick="ImageButton_Click"
                    CommandName="Iktatas" AlternateText="<%$Resources:Buttons,Munkapeldany_Iktatas %>" />
                <asp:ImageButton TabIndex = "-1" ID="MunkaanyagBeiktatas" CssClass="highlightit" runat="server" Visible="false"
                    ImageUrl="../images/hu/muvgomb/Iratta_minosites.jpg" OnClick="ImageButton_Click"
                    CommandName="MunkaanyagBeiktatas" AlternateText="<%$Resources:Buttons,MunkaanyagBeiktatas %>" />  
                                        
                <asp:ImageButton TabIndex = "-1" ID="IktatasMegtagadas" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/iktatas_megtagadas.jpg" OnClick="ImageButton_Click"
                    CommandName="IktatasMegtagadas" AlternateText="<%$Resources:Buttons,Iktatas_Megtagadas %>" />
                                        
                <asp:ImageButton TabIndex = "-1" ID="AtIktatas" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/atiktatas.jpg" OnClick="ImageButton_Click"
                    CommandName="AtIktatas" AlternateText="<%$Resources:Buttons,Atiktatas %>" />  

               <asp:ImageButton TabIndex = "-1" ID="ValaszIrat" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/valaszirat.jpg" OnClick="ImageButton_Click"
                    CommandName="ValaszIrat" AlternateText="<%$Resources:Buttons,ValaszIrat %>" />  
                    
                 <asp:ImageButton TabIndex = "-1" ID="Felszabaditas" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/felszabaditas.jpg" OnClick="ImageButton_Click"
                    CommandName="Felszabaditas" AlternateText="<%$Resources:Buttons,Felszabaditas %>" />  
                    
                <asp:ImageButton TabIndex = "-1" ID="KuldemenyMegtekintese" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/kuldemenymegtekintese.jpg" OnClick="ImageButton_Click"
                    CommandName="KuldemenyMegtekintese" AlternateText="<%$Resources:Buttons,KuldemenyMegtekintese %>" />
                <asp:ImageButton TabIndex = "-1" ID="Kuldemeny_lezarasa" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/kuldemeny_lezarasa.jpg" OnClick="ImageButton_Click"
                    CommandName="Kuldemeny_lezarasa" AlternateText="<%$Resources:Buttons,Kuldemeny_lezarasa %>" />
                    
                <asp:ImageButton TabIndex = "-1" ID="Borito_nyomtatasa_A4" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/borito_nyomtatasa_a4.jpg" OnClick="ImageButton_Click"
                    CommandName="Borito_nyomtatasa_A4" AlternateText="<%$Resources:Buttons,Borito_nyomtatasa_A4 %>" />
                <asp:ImageButton TabIndex = "-1" ID="Borito_nyomtatasa_A3" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/borito_nyomtatasa_a3.jpg" OnClick="ImageButton_Click"
                    CommandName="Borito_nyomtatasa_A3" AlternateText="<%$Resources:Buttons,Borito_nyomtatasa_A3 %>" />
                    <!--//CR 3058-->
                <asp:ImageButton TabIndex="-1" ID="VonalkodNyomtatasa" runat="server" CssClass="highlightit" 
                    ImageUrl="~/images/hu/muvgomb/nyomtatas_vonalkod.jpg" OnClick="ImageButton_Click"
                    CommandName="VonalkodNyomtatasa" AlternateText="<%$Resources:Buttons,Vonalkod_Nyomtatasa %>" />

                <asp:ImageButton TabIndex = "-1" ID="Kiserolap" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/kiserolap.jpg" OnClick="ImageButton_Click"
                    CommandName="Kiserolap" AlternateText="<%$Resources:Buttons,Kiserolap %>" />
                <asp:ImageButton TabIndex = "-1" ID="XMLExport" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/XMLExport.jpg" OnClick="ImageButton_Click"
                    CommandName="XMLExport" AlternateText="<%$Resources:Buttons,XMLExport %>" />
                <asp:ImageButton TabIndex = "-1" ID="HitelesitesiZaradek" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/HitelesitesiZaradek.jpg" OnClick="ImageButton_Click"
                    CommandName="HitelesitesiZaradek" AlternateText="<%$Resources:Buttons,HitelesitesiZaradek %>" />
                <asp:ImageButton TabIndex = "-1" ID="AtveteliElismerveny" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/atveteli_elismerveny.jpg" OnClick="ImageButton_Click"
                    CommandName="AtveteliElismerveny" AlternateText="<%$Resources:Buttons,AtveteliElismerveny %>" />
                
                <asp:ImageButton TabIndex = "-1" ID="Zarolas" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/zarolas.jpg" OnClick="ImageButton_Click"
                    CommandName="Zarolas" AlternateText="<%$Resources:Buttons,Zarolas %>" />
                <asp:ImageButton TabIndex = "-1" ID="Zarolas_feloldasa" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/zarolas_feloldasa.jpg" OnClick="ImageButton_Click"
                    CommandName="Zarolas_feloldasa" AlternateText="<%$Resources:Buttons,Zarolas_feloldasa %>" />
                <asp:ImageButton TabIndex = "-1" ID="EMailNyomtatas" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/email_nyomtatas.jpg" OnClick="ImageButton_Click"
                    CommandName="EMail_nyomtatas" AlternateText="<%$Resources:Buttons,EMail_nyomtatas %>" />
                <asp:ImageButton TabIndex = "-1" ID="DokumentumGeneralas" CssClass="highlightit" runat="server"
                    ImageUrl="~/images/hu/muvgomb/dokumentum_generalas.jpg" OnClick="ImageButton_Click"
                    CommandName="DokumentumGeneralas" AlternateText="Dokumentum gener�l�s" />
                 
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
