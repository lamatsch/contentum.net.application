using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eUIControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;

public partial class Component_FunkcioGombsor : System.Web.UI.UserControl
{

	#region IsDisabledButtonToSetHidden by RPM
	private bool IsDisabledButtonToSetHidden ( )
	{
		return Contentum.eUtility.Rendszerparameterek.GetBoolean ( Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS );
	}
	#endregion IsDisabledButtonToSetHidden by RPM

	#region public Properties

	private String ParentId = "";
	//private String Command = "";

	private String _ParentForm = "";

	public String ParentForm
	{
		get { return _ParentForm; }
		set { _ParentForm = value; }
	}

	#endregion

	public event CommandEventHandler FunkcionButtonsClick;

	public virtual void ImageButton_Click ( object sender, ImageClickEventArgs e )
	{
		if ( FunkcionButtonsClick != null )
		{
			CommandEventArgs args = new CommandEventArgs ( ( sender as ImageButton ).CommandName, "" );
			FunkcionButtonsClick ( sender, args );
		}
	}


	protected void Page_Init ( object sender, EventArgs e )
	{
		ParentId = Request.QueryString.Get ( "Id" );
		//Command = Request.QueryString.Get("Command");

		bool isDisabledIconToSetToHidden = IsDisabledButtonToSetHidden ( );
		Uj_ugyirat.HideIfDisabled = isDisabledIconToSetToHidden;
		Modositas.HideIfDisabled = isDisabledIconToSetToHidden;
		Szignalas.HideIfDisabled = isDisabledIconToSetToHidden;
		Szignalas_vissza.HideIfDisabled = isDisabledIconToSetToHidden;
		AtvetelUgyintezesre.HideIfDisabled = isDisabledIconToSetToHidden;
		Szkontro_inditas.HideIfDisabled = isDisabledIconToSetToHidden;
		Szkontrobol_vissza.HideIfDisabled = isDisabledIconToSetToHidden;
		Felfuggesztes.HideIfDisabled = isDisabledIconToSetToHidden;
		Felfuggesztesbol_vissza.HideIfDisabled = isDisabledIconToSetToHidden;
		Tovabbitas.HideIfDisabled = isDisabledIconToSetToHidden;
		Atadasra_kijeloles.HideIfDisabled = isDisabledIconToSetToHidden;
		Elintezette_nyilvanitas.HideIfDisabled = isDisabledIconToSetToHidden;
		Sztornozas.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyintezes_lezarasa.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyintezes_lezarasa_vissza.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_lezarasa.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_lezarasa_vissza.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_irattarozasra_kijeloles.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_AlszamraIktatasraElokeszit.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_AlszamraIktatasKimenoBelso.HideIfDisabled = isDisabledIconToSetToHidden;
		Ugyirat_AlszamraIktatasBejovo.HideIfDisabled = isDisabledIconToSetToHidden;
		Csatolas.HideIfDisabled = isDisabledIconToSetToHidden;
		Szereles.HideIfDisabled = isDisabledIconToSetToHidden;
		IratPeldany_Letrehozasa.HideIfDisabled = isDisabledIconToSetToHidden;
		Postazas.HideIfDisabled = isDisabledIconToSetToHidden;
		Postazoba.HideIfDisabled = isDisabledIconToSetToHidden;
		Expedialas.HideIfDisabled = isDisabledIconToSetToHidden;
		Valasz.HideIfDisabled = isDisabledIconToSetToHidden;
		IratPeldanyTovabbitas.HideIfDisabled = isDisabledIconToSetToHidden;
		Iktatas.HideIfDisabled = isDisabledIconToSetToHidden;
		// CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
		Munkapeldany_Iktatas.HideIfDisabled = isDisabledIconToSetToHidden;
		MunkaanyagBeiktatas.HideIfDisabled = isDisabledIconToSetToHidden;
		IktatasMegtagadas.HideIfDisabled = isDisabledIconToSetToHidden;
		AtIktatas.HideIfDisabled = isDisabledIconToSetToHidden;
		Felszabaditas.HideIfDisabled = isDisabledIconToSetToHidden;
		KuldemenyMegtekintese.HideIfDisabled = isDisabledIconToSetToHidden;
		Kuldemeny_lezarasa.HideIfDisabled = isDisabledIconToSetToHidden;
		Borito_nyomtatasa_A4.HideIfDisabled = isDisabledIconToSetToHidden;
		XMLExport.HideIfDisabled = isDisabledIconToSetToHidden;
		HitelesitesiZaradek.HideIfDisabled = isDisabledIconToSetToHidden;
		Borito_nyomtatasa_A3.HideIfDisabled = isDisabledIconToSetToHidden;
		Kiserolap.HideIfDisabled = isDisabledIconToSetToHidden;
		AtveteliElismerveny.HideIfDisabled = isDisabledIconToSetToHidden;
		Zarolas.HideIfDisabled = isDisabledIconToSetToHidden;
		Zarolas_feloldasa.HideIfDisabled = isDisabledIconToSetToHidden;
		EMailNyomtatas.HideIfDisabled = isDisabledIconToSetToHidden;
        DokumentumGeneralas.HideIfDisabled = isDisabledIconToSetToHidden;
	}

	protected void Page_Load ( object sender, EventArgs e )
	{

	}

	protected void Page_PreRender ( object sender, EventArgs e )
	{
		if ( String.IsNullOrEmpty ( ParentForm ) )
		{
			ButtonsVisible ( false );
			Label l = new Label ( );
			l.Text = "Hiba! Nem allitottad be a UserControl.ParentForm property-jet!";
			l.ForeColor = System.Drawing.Color.Red;
			_FunkcioGombsorPanel.Controls.Add ( l );
			return;
		}

		// ide jon a funkcio jogosultsag ellenorzes:

		// Xml Exportn�l k�z�s funkci�jog, ez�rt mehet ide:
		// (ha nincs joga, ne is l�tsz�djon)
		bool vanJoga_XmlExport = Contentum.eUtility.FunctionRights.GetFunkcioJog ( Page, "XmlExport" );
		if ( vanJoga_XmlExport == false )
		{
			XMLExportEnabled = false;
			XMLExportVisible = false;
		}
	}

	/*    /// <summary>
		 /// Gomb megnyom�sakor funkci� kezel�se.
		 /// </summary>
		 /// <param name="sender">The source of the event.</param>
		 /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
		 protected void ImageButton_Click(object sender, ImageClickEventArgs e)
		 {
			  switch ((sender as ImageButton).CommandName)
			  {
					case "Uj_ugyirat":
						 break;
					case "Modositas":
						 break;
					case "Szignalas":
						 break;
					case "Szignalas_vissza":
						 break;
					case "Szkontro_inditas":
						 break;
					case "Szkontrobol_vissza":
						 break;
					case "Felfuggesztes":
						 break;
					case "Felfuggesztesbol_vissza":
						 break;
					case "Tovabbitas":
						 break;
					case "Sztornozas":
						 break;
					case "Ugyintezes_lezarasa":
						 break;
					case "Ugyintezes_lezarasa_vissza":
						 break;
					case "ugyirat_lezarasa":
						 break;
					case "Ugyirat_lezarasa_vissza":
						 break;

					case "Ugyirat_irattarozasra_kijeloles":
						 break;

					case "Ugyirat_AlszamraIktatasKimenoBelso":
						 break;
					case "Ugyirat_AlszamraIktatasBejovo":
						 break;

					case "Postazas":
						 break;
					case "Postazoba":
						 break;
					case "Expedialas":
						 break;
					case "Valasz":
						 break;
					case "IratPeldanyTovabbitas":
						 break;           

					case "Iktatas":
						 break;

					case "KuldemenyMegtekintese":
						 break;

					case "Kuldemeny_lezarasa":
						 break;
					case "Borito_nyomtatasa_A4":
						 break;
					case "Borito_nyomtatasa_A3":
						 break;
					case "Kiserolap":
						 break;

					case "Zarolas":
						 break;
					case "Zarolas_feloldasa":
						 break;
			  }
		 }
	 * */
	/* UgyiratFunkcioGombsor
		 Uj_ugyirat Modositas Szignalas Szignalas_vissza Szkontro_inditas
		 Szkontrobol_vissza Felfuggesztes Felfuggesztesbol_vissza 
		 Tovabbitas Sztornozas Ugyintezes_lezarasa Ugyintezes_lezarasa_vissza 
		 Ugyirat_lezarasa Ugyirat_lezarasa_vissza 

	 * "Uj_ugyirat", "Modositas", "Szignalas", "Szignalas_vissza", "Szkontro_inditas",
		 "Szkontrobol_vissza", "Felfuggesztes", "Felfuggesztesbol_vissza", "Tovabbitas",
		 "Sztornozas", "Ugyintezes_lezarasa", "Ugyintezes_lezarasa_vissza", "Ugyirat_lezarasa",
		 "Ugyirat_lezarasa_vissza";
	*/

	/// <summary>
	/// Buttonses the visible.
	/// </summary>
	/// <param name="value"><c>true</c> l�tszanak a gombok.</param>
	public void ButtonsVisible ( Boolean value )
	{
		Uj_ugyirat.Visible = value;
		Modositas.Visible = value;
		Szignalas.Visible = value;
		Szignalas_vissza.Visible = value;
		AtvetelUgyintezesre.Visible = value;
		Szkontro_inditas.Visible = value;
		Szkontrobol_vissza.Visible = value;
		Felfuggesztes.Visible = value;
		Felfuggesztesbol_vissza.Visible = value;
		Tovabbitas.Visible = value;
		Atadasra_kijeloles.Visible = value;
		Elintezette_nyilvanitas.Visible = value;
		Sztornozas.Visible = value;
		Ugyintezes_lezarasa.Visible = value;
		Ugyintezes_lezarasa_vissza.Visible = value;
		Ugyirat_lezarasa.Visible = value;
		Ugyirat_lezarasa_vissza.Visible = value;
		Ugyirat_irattarozasra_kijeloles.Visible = value;

		Ugyirat_AlszamraIktatasraElokeszit.Visible = value;
		Ugyirat_AlszamraIktatasKimenoBelso.Visible = value;
		Ugyirat_AlszamraIktatasBejovo.Visible = value;

		Csatolas.Visible = value;
		Szereles.Visible = value;

		IratPeldany_Letrehozasa.Visible = value;
		Postazas.Visible = value;
		Postazoba.Visible = value;
		Expedialas.Visible = value;
		Valasz.Visible = value;
		IratPeldanyTovabbitas.Visible = value;

		Iktatas.Visible = value;
		// CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
		Munkapeldany_Iktatas.Visible = value;
		MunkaanyagBeiktatas.Visible = value;
		IktatasMegtagadas.Visible = value;
		AtIktatas.Visible = value;
        ValaszIrat.Visible = value;
		Felszabaditas.Visible = value;
		KuldemenyMegtekintese.Visible = value;
		Kuldemeny_lezarasa.Visible = value;
		Borito_nyomtatasa_A4.Visible = value;
		XMLExport.Visible = value;
		HitelesitesiZaradek.Visible = value;
		Borito_nyomtatasa_A3.Visible = value;
		Kiserolap.Visible = value;
		AtveteliElismerveny.Visible = value;

		Zarolas.Visible = value;
		Zarolas_feloldasa.Visible = value;

		EMailNyomtatas.Visible = value;
		//HA NEM T�K-�s
		if ( Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK ).ToUpper ( ).Trim ( ).Equals ( "0" ) )
		{
			//<!--CR 3058-->
			if ( Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES ).ToUpper ( ).Trim ( ).Equals ( "AZONOSITO" ) )
				VonalkodNyomtatasa.Visible = value;
			else
				VonalkodNyomtatasa.Visible = false;
		}
		else//HA T�K-�s
		{
			bool _visible = Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN ).ToUpper ( ).Trim ( ).Equals ( "1" );
			VonalkodNyomtatasa.Visible = _visible;
		}
        DokumentumGeneralas.Visible = value;
    }

	/// <summary>
	/// Gombok enged�lyez�se
	/// A gombok l�tszanak de "halv�nyak"
	/// </summary>
	/// <param name="value"><c>true</c> enged�lyezettek a gombok</param>
	public void ButtonsEnable ( Boolean value )
	{
		Uj_ugyirat.Enabled = value;
		Modositas.Enabled = value;
		Szignalas.Enabled = value;
		Szignalas_vissza.Enabled = value;
		AtvetelUgyintezesre.Enabled = value;
		Szkontro_inditas.Enabled = value;
		Szkontrobol_vissza.Enabled = value;
		Felfuggesztes.Enabled = value;
		Felfuggesztesbol_vissza.Enabled = value;
		Tovabbitas.Enabled = value;
		Atadasra_kijeloles.Enabled = value;
		Elintezette_nyilvanitas.Enabled = value;
		Sztornozas.Enabled = value;
		Ugyintezes_lezarasa.Enabled = value;
		Ugyintezes_lezarasa_vissza.Enabled = value;
		Ugyirat_lezarasa.Enabled = value;
		Ugyirat_lezarasa_vissza.Enabled = value;
		Ugyirat_irattarozasra_kijeloles.Enabled = value;
		Ugyirat_AlszamraIktatasraElokeszit.Enabled = value;
		Ugyirat_AlszamraIktatasKimenoBelso.Enabled = value;
		Ugyirat_AlszamraIktatasBejovo.Enabled = value;
		Csatolas.Enabled = value;
		Szereles.Enabled = value;
		IratPeldany_Letrehozasa.Enabled = value;
		Postazas.Enabled = value;
		Postazoba.Enabled = value;
		Expedialas.Enabled = value;
		Valasz.Enabled = value;
		IratPeldanyTovabbitas.Enabled = value;
		Iktatas.Enabled = value;
		// CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
		Munkapeldany_Iktatas.Enabled = value;
		MunkaanyagBeiktatas.Enabled = value;
		IktatasMegtagadas.Enabled = value;
		AtIktatas.Enabled = value;
        ValaszIrat.Enabled = value;
		Felszabaditas.Enabled = value;
		KuldemenyMegtekintese.Enabled = value;
		Kuldemeny_lezarasa.Enabled = value;
		Borito_nyomtatasa_A4.Enabled = value;
		XMLExport.Enabled = value;
		HitelesitesiZaradek.Enabled = value;
		Borito_nyomtatasa_A3.Enabled = value;
		Kiserolap.Enabled = value;
		AtveteliElismerveny.Enabled = value;
		Zarolas.Enabled = value;
		Zarolas_feloldasa.Enabled = value;
		EMailNyomtatas.Enabled = value;
        DokumentumGeneralas.Enabled = value;
	}

	public void HideButtons ( )
	{
		this.ButtonsVisible ( false );
	}

	public void ShowButtons ( )
	{
		this.ButtonsVisible ( true );
	}

	public void DisableButtons ( )
	{
		this.ButtonsEnable ( false );
	}

	public void EnableButtons ( )
	{
		this.ButtonsEnable ( true );
	}

	#region Uj_ugyirat
	public bool Uj_ugyiratEnabled
	{
		get { return Uj_ugyirat.Enabled; }
		set { Uj_ugyirat.Enabled = value; }
	}
	public bool Uj_ugyiratVisible
	{
		get { return Uj_ugyirat.Visible; }
		set
		{
			Uj_ugyirat.Visible = value;
		}
	}
	public String Uj_ugyiratOnClientClick
	{
		get { return Uj_ugyirat.OnClientClick; }
		set { Uj_ugyirat.OnClientClick = value; }
	}
	#endregion

	#region Modositas
	public bool ModositasEnabled
	{
		get { return Modositas.Enabled; }
		set { Modositas.Enabled = value; }
	}
	public bool ModositasVisible
	{
		get { return Modositas.Visible; }
		set
		{
			Modositas.Visible = value;
		}
	}
	public String ModositasOnClientClick
	{
		get { return Modositas.OnClientClick; }
		set { Modositas.OnClientClick = value; }
	}
	#endregion

	#region Szignalas
	public bool SzignalasEnabled
	{
		get { return Szignalas.Enabled; }
		set { Szignalas.Enabled = value; }
	}
	public bool SzignalasVisible
	{
		get { return Szignalas.Visible; }
		set
		{
			Szignalas.Visible = value;
		}
	}
	public String SzignalasOnClientClick
	{
		get { return Szignalas.OnClientClick; }
		set { Szignalas.OnClientClick = value; }
	}
	public CustomFunctionImageButton SzignalasImageButton
	{
		get { return Szignalas; }
	}
	#endregion

	#region Szignalas_vissza
	public bool Szignalas_visszaEnabled
	{
		get { return Szignalas_vissza.Enabled; }
		set { Szignalas_vissza.Enabled = value; }
	}
	public bool Szignalas_visszaVisible
	{
		get { return Szignalas_vissza.Visible; }
		set
		{
			Szignalas_vissza.Visible = value;
		}
	}
	public String Szignalas_visszaOnClientClick
	{
		get { return Szignalas_vissza.OnClientClick; }
		set { Szignalas_vissza.OnClientClick = value; }
	}
	#endregion

	#region AtvetelUgyintezesre
	public bool AtvetelUgyintezesreEnabled
	{
		get { return AtvetelUgyintezesre.Enabled; }
		set { AtvetelUgyintezesre.Enabled = value; }
	}
	public bool AtvetelUgyintezesreVisible
	{
		get { return AtvetelUgyintezesre.Visible; }
		set
		{
			AtvetelUgyintezesre.Visible = value;
		}
	}
	public String AtvetelUgyintezesreOnClientClick
	{
		get { return AtvetelUgyintezesre.OnClientClick; }
		set { AtvetelUgyintezesre.OnClientClick = value; }
	}
	public CustomFunctionImageButton AtvetelUgyintezesreImageButton
	{
		get { return AtvetelUgyintezesre; }
	}
	#endregion

	#region Szkontro_inditas

	public bool Szkontro_inditasEnabled
	{
		get { return Szkontro_inditas.Enabled; }
		set { Szkontro_inditas.Enabled = value; }
	}
	public bool Szkontro_inditasVisible
	{
		get { return Szkontro_inditas.Visible; }
		set
		{
			Szkontro_inditas.Visible = value;
		}
	}
	public String Szkontro_inditasOnClientClick
	{
		get { return Szkontro_inditas.OnClientClick; }
		set { Szkontro_inditas.OnClientClick = value; }
	}
	public CustomFunctionImageButton Szkontro_inditasImageButton
	{
		get { return Szkontro_inditas; }
	}

	#endregion

	#region Szkontrobol_vissza
	public bool Szkontrobol_visszaEnabled
	{
		get { return Szkontrobol_vissza.Enabled; }
		set { Szkontrobol_vissza.Enabled = value; }
	}
	public bool Szkontrobol_visszaVisible
	{
		get { return Szkontrobol_vissza.Visible; }
		set
		{
			Szkontrobol_vissza.Visible = value;
		}
	}
	public String Szkontrobol_visszaOnClientClick
	{
		get { return Szkontrobol_vissza.OnClientClick; }
		set { Szkontrobol_vissza.OnClientClick = value; }
	}
	public CustomFunctionImageButton Szkontrobol_visszaImageButton
	{
		get { return Szkontrobol_vissza; }
	}

	#endregion

	#region Felfuggesztes
	public bool FelfuggesztesEnabled
	{
		get { return Felfuggesztes.Enabled; }
		set { Felfuggesztes.Enabled = value; }
	}
	public bool FelfuggesztesVisible
	{
		get { return Felfuggesztes.Visible; }
		set
		{
			Felfuggesztes.Visible = value;
		}
	}
	public String FelfuggesztesOnClientClick
	{
		get { return Felfuggesztes.OnClientClick; }
		set { Felfuggesztes.OnClientClick = value; }
	}
	#endregion

	#region Felfuggesztesbol_vissza
	public bool Felfuggesztesbol_visszaEnabled
	{
		get { return Felfuggesztesbol_vissza.Enabled; }
		set { Felfuggesztesbol_vissza.Enabled = value; }
	}
	public bool Felfuggesztesbol_visszaVisible
	{
		get { return Felfuggesztesbol_vissza.Visible; }
		set
		{
			Felfuggesztesbol_vissza.Visible = value;
		}
	}
	public String Felfuggesztesbol_visszaOnClientClick
	{
		get { return Felfuggesztesbol_vissza.OnClientClick; }
		set { Felfuggesztesbol_vissza.OnClientClick = value; }
	}
	#endregion

	#region Tovabbitas
	public bool TovabbitasEnabled
	{
		get { return Tovabbitas.Enabled; }
		set { Tovabbitas.Enabled = value; }
	}
	public bool TovabbitasVisible
	{
		get { return Tovabbitas.Visible; }
		set
		{
			Tovabbitas.Visible = value;
		}
	}
	public String TovabbitasOnClientClick
	{
		get { return Tovabbitas.OnClientClick; }
		set { Tovabbitas.OnClientClick = value; }
	}
	#endregion

	#region Atadasra_kijeloles
	public bool Atadasra_kijelolesEnabled
	{
		get { return Atadasra_kijeloles.Enabled; }
		set { Atadasra_kijeloles.Enabled = value; }
	}
	public bool Atadasra_kijelolesVisible
	{
		get { return Atadasra_kijeloles.Visible; }
		set
		{
			Atadasra_kijeloles.Visible = value;
		}
	}
	public String Atadasra_kijelolesOnClientClick
	{
		get { return Atadasra_kijeloles.OnClientClick; }
		set { Atadasra_kijeloles.OnClientClick = value; }
	}
	public CustomFunctionImageButton Atadasra_kijeloles_ImageButton
	{
		get { return Atadasra_kijeloles; }
	}
	#endregion

	#region Elintezette_nyilvanitas
	public bool Elintezette_nyilvanitasEnabled
	{
		get { return Elintezette_nyilvanitas.Enabled; }
		set { Elintezette_nyilvanitas.Enabled = value; }
	}
	public bool Elintezette_nyilvanitasVisible
	{
		get { return Elintezette_nyilvanitas.Visible; }
		set
		{
			Elintezette_nyilvanitas.Visible = value;
		}
	}
	public String Elintezette_nyilvanitasOnClientClick
	{
		get { return Elintezette_nyilvanitas.OnClientClick; }
		set { Elintezette_nyilvanitas.OnClientClick = value; }
	}
	public CustomFunctionImageButton Elintezette_nyilvanitas_ImageButton
	{
		get { return Elintezette_nyilvanitas; }
	}
	#endregion

	#region Sztornozas
	public CustomFunctionImageButton SztornozasImage
	{
		get { return Sztornozas; }
		set { Sztornozas = value; }
	}
	public bool SztornozasEnabled
	{
		get { return Sztornozas.Enabled; }
		set { Sztornozas.Enabled = value; }
	}
	public bool SztornozasVisible
	{
		get { return Sztornozas.Visible; }
		set
		{
			Sztornozas.Visible = value;
		}
	}
	public String SztornozasOnClientClick
	{
		get { return Sztornozas.OnClientClick; }
		set { Sztornozas.OnClientClick = value; }
	}
	#endregion

	#region Ugyintezes_lezarasa
	public bool Ugyintezes_lezarasaEnabled
	{
		get { return Ugyintezes_lezarasa.Enabled; }
		set { Ugyintezes_lezarasa.Enabled = value; }
	}
	public bool Ugyintezes_lezarasaVisible
	{
		get { return Ugyintezes_lezarasa.Visible; }
		set
		{
			Ugyintezes_lezarasa.Visible = value;
		}
	}
	public String Ugyintezes_lezarasaOnClientClick
	{
		get { return Ugyintezes_lezarasa.OnClientClick; }
		set { Ugyintezes_lezarasa.OnClientClick = value; }
	}
	#endregion

	#region Ugyintezes_lezarasa_vissza
	public bool Ugyintezes_lezarasa_visszaEnabled
	{
		get { return Ugyintezes_lezarasa_vissza.Enabled; }
		set { Ugyintezes_lezarasa_vissza.Enabled = value; }
	}
	public bool Ugyintezes_lezarasa_visszaVisible
	{
		get { return Ugyintezes_lezarasa_vissza.Visible; }
		set
		{
			Ugyintezes_lezarasa_vissza.Visible = value;
		}
	}
	public String Ugyintezes_lezarasa_visszaOnClientClick
	{
		get { return Ugyintezes_lezarasa_vissza.OnClientClick; }
		set { Ugyintezes_lezarasa_vissza.OnClientClick = value; }
	}
	#endregion

	#region Ugyirat_lezarasa
	public bool Ugyirat_lezarasaEnabled
	{
		get { return Ugyirat_lezarasa.Enabled; }
		set { Ugyirat_lezarasa.Enabled = value; }
	}
	public bool Ugyirat_lezarasaVisible
	{
		get { return Ugyirat_lezarasa.Visible; }
		set
		{
			Ugyirat_lezarasa.Visible = value;
		}
	}
	public String Ugyirat_lezarasaOnClientClick
	{
		get { return Ugyirat_lezarasa.OnClientClick; }
		set { Ugyirat_lezarasa.OnClientClick = value; }
	}
	public CustomFunctionImageButton Ugyirat_lezarasa_ImageButton
	{
		get { return Ugyirat_lezarasa; }
	}
	#endregion

	#region Ugyirat_lezarasa_vissza
	public bool Ugyirat_lezarasa_visszaEnabled
	{
		get { return Ugyirat_lezarasa_vissza.Enabled; }
		set { Ugyirat_lezarasa_vissza.Enabled = value; }
	}
	public bool Ugyirat_lezarasa_visszaVisible
	{
		get { return Ugyirat_lezarasa_vissza.Visible; }
		set
		{
			Ugyirat_lezarasa_vissza.Visible = value;
		}
	}
	public String Ugyirat_lezarasa_visszaOnClientClick
	{
		get { return Ugyirat_lezarasa_vissza.OnClientClick; }
		set { Ugyirat_lezarasa_vissza.OnClientClick = value; }
	}
	#endregion


	#region Ugyirat_irattarozasra_kijeloles
	public bool Ugyirat_irattarozasra_kijelolesEnabled
	{
		get { return Ugyirat_irattarozasra_kijeloles.Enabled; }
		set { Ugyirat_irattarozasra_kijeloles.Enabled = value; }
	}
	public bool Ugyirat_irattarozasra_kijelolesVisible
	{
		get { return Ugyirat_irattarozasra_kijeloles.Visible; }
		set
		{
			Ugyirat_irattarozasra_kijeloles.Visible = value;
		}
	}
	public String Ugyirat_irattarozasra_kijelolesOnClientClick
	{
		get { return Ugyirat_irattarozasra_kijeloles.OnClientClick; }
		set { Ugyirat_irattarozasra_kijeloles.OnClientClick = value; }
	}
	public CustomFunctionImageButton Ugyirat_irattarozasra_kijeloles_ImageButton
	{
		get { return Ugyirat_irattarozasra_kijeloles; }
	}
	#endregion

	#region Ugyirat_AlszamraIktatasraElokeszit
	public bool Ugyirat_AlszamraIktatasraElokeszitEnabled
	{
		get { return Ugyirat_AlszamraIktatasraElokeszit.Enabled; }
		set { Ugyirat_AlszamraIktatasraElokeszit.Enabled = value; }
	}
	public bool Ugyirat_AlszamraIktatasraElokeszitVisible
	{
		get { return Ugyirat_AlszamraIktatasraElokeszit.Visible; }
		set
		{
			Ugyirat_AlszamraIktatasraElokeszit.Visible = value;
		}
	}
	public String Ugyirat_AlszamraIktatasraElokeszitOnClientClick
	{
		get { return Ugyirat_AlszamraIktatasraElokeszit.OnClientClick; }
		set { Ugyirat_AlszamraIktatasraElokeszit.OnClientClick = value; }
	}
	public CustomFunctionImageButton Ugyirat_AlszamraIktatasraElokeszit_ImageButton
	{
		get { return Ugyirat_AlszamraIktatasraElokeszit; }
	}
	#endregion

	#region Ugyirat_AlszamraIktatasKimenoBelso
	public bool Ugyirat_AlszamraIktatasKimenoBelsoEnabled
	{
		get { return Ugyirat_AlszamraIktatasKimenoBelso.Enabled; }
		set { Ugyirat_AlszamraIktatasKimenoBelso.Enabled = value; }
	}
	public bool Ugyirat_AlszamraIktatasKimenoBelsoVisible
	{
		get { return Ugyirat_AlszamraIktatasKimenoBelso.Visible; }
		set
		{
			Ugyirat_AlszamraIktatasKimenoBelso.Visible = value;
		}
	}
	public String Ugyirat_AlszamraIktatasKimenoBelsoOnClientClick
	{
		get { return Ugyirat_AlszamraIktatasKimenoBelso.OnClientClick; }
		set { Ugyirat_AlszamraIktatasKimenoBelso.OnClientClick = value; }
	}
	public CustomFunctionImageButton Ugyirat_AlszamraIktatasKimenoBelso_ImageButton
	{
		get { return Ugyirat_AlszamraIktatasKimenoBelso; }
	}
	#endregion

	#region Ugyirat_AlszamraIktatasBejovo
	public bool Ugyirat_AlszamraIktatasBejovoEnabled
	{
		get { return Ugyirat_AlszamraIktatasBejovo.Enabled; }
		set { Ugyirat_AlszamraIktatasBejovo.Enabled = value; }
	}
	public bool Ugyirat_AlszamraIktatasBejovoVisible
	{
		get { return Ugyirat_AlszamraIktatasBejovo.Visible; }
		set
		{
			Ugyirat_AlszamraIktatasBejovo.Visible = value;
		}
	}
	public String Ugyirat_AlszamraIktatasBejovoOnClientClick
	{
		get { return Ugyirat_AlszamraIktatasBejovo.OnClientClick; }
		set { Ugyirat_AlszamraIktatasBejovo.OnClientClick = value; }
	}
	public CustomFunctionImageButton Ugyirat_AlszamraIktatasBejovo_ImageButton
	{
		get { return Ugyirat_AlszamraIktatasBejovo; }
	}
	#endregion


	#region Csatolas
	public bool CsatolasEnabled
	{
		get { return Csatolas.Enabled; }
		set { Csatolas.Enabled = value; }
	}
	public bool CsatolasVisible
	{
		get { return Csatolas.Visible; }
		set
		{
			Csatolas.Visible = value;
		}
	}
	public String CsatolasOnClientClick
	{
		get { return Csatolas.OnClientClick; }
		set { Csatolas.OnClientClick = value; }
	}
	public CustomFunctionImageButton Csatolas_ImageButton
	{
		get { return Csatolas; }
	}
	#endregion

	#region Szereles
	public bool SzerelesEnabled
	{
		get { return Szereles.Enabled; }
		set { Szereles.Enabled = value; }
	}
	public bool SzerelesVisible
	{
		get { return Szereles.Visible; }
		set
		{
			Szereles.Visible = value;
		}
	}
	public String SzerelesOnClientClick
	{
		get { return Szereles.OnClientClick; }
		set { Szereles.OnClientClick = value; }
	}
	public CustomFunctionImageButton Szereles_ImageButton
	{
		get { return Szereles; }
	}
	#endregion


	#region IratPeldany_Letrehozasa
	public bool IratPeldany_LetrehozasaEnabled
	{
		get { return IratPeldany_Letrehozasa.Enabled; }
		set { IratPeldany_Letrehozasa.Enabled = value; }
	}
	public bool IratPeldany_LetrehozasaVisible
	{
		get { return IratPeldany_Letrehozasa.Visible; }
		set
		{
			IratPeldany_Letrehozasa.Visible = value;
		}
	}
	public String IratPeldany_LetrehozasaOnClientClick
	{
		get { return IratPeldany_Letrehozasa.OnClientClick; }
		set { IratPeldany_Letrehozasa.OnClientClick = value; }
	}

	public CustomFunctionImageButton IratPeldany_Letrehozasa_ImageButton
	{
		get { return IratPeldany_Letrehozasa; }
	}

	#endregion

	#region Postazas
	public bool PostazasEnabled
	{
		get { return Postazas.Enabled; }
		set { Postazas.Enabled = value; }
	}
	public bool PostazasVisible
	{
		get { return Postazas.Visible; }
		set
		{
			Postazas.Visible = value;
		}
	}
	public String PostazasOnClientClick
	{
		get { return Postazas.OnClientClick; }
		set { Postazas.OnClientClick = value; }
	}
	public CustomFunctionImageButton PostazasImageButton
	{
		get { return Postazas; }
	}
	#endregion

	#region Postazoba
	public bool PostazobaEnabled
	{
		get { return Postazoba.Enabled; }
		set { Postazoba.Enabled = value; }
	}
	public bool PostazobaVisible
	{
		get { return Postazoba.Visible; }
		set
		{
			Postazoba.Visible = value;
		}
	}
	public String PostazobaOnClientClick
	{
		get { return Postazoba.OnClientClick; }
		set { Postazoba.OnClientClick = value; }
	}
	public CustomFunctionImageButton PostazobaImageButton
	{
		get { return Postazoba; }
	}
	#endregion

	#region Expedialas
	public bool ExpedialasEnabled
	{
		get { return Expedialas.Enabled; }
		set { Expedialas.Enabled = value; }
	}
	public bool ExpedialasVisible
	{
		get { return Expedialas.Visible; }
		set
		{
			Expedialas.Visible = value;
		}
	}
	public String ExpedialasOnClientClick
	{
		get { return Expedialas.OnClientClick; }
		set { Expedialas.OnClientClick = value; }
	}
	public CustomFunctionImageButton ExpedialasImageButton
	{
		get { return Expedialas; }
	}
	#endregion

	#region Valasz
	public bool ValaszEnabled
	{
		get { return Valasz.Enabled; }
		set { Valasz.Enabled = value; }
	}
	public bool ValaszVisible
	{
		get { return Valasz.Visible; }
		set
		{
			Valasz.Visible = value;
		}
	}
	public String ValaszOnClientClick
	{
		get { return Valasz.OnClientClick; }
		set { Valasz.OnClientClick = value; }
	}
	public CustomFunctionImageButton ValaszImageButton
	{
		get { return Valasz; }
	}
	#endregion

	#region IratPeldanyTovabbitas
	public bool IratPeldanyTovabbitasEnabled
	{
		get { return IratPeldanyTovabbitas.Enabled; }
		set { IratPeldanyTovabbitas.Enabled = value; }
	}
	public bool IratPeldanyTovabbitasVisible
	{
		get { return IratPeldanyTovabbitas.Visible; }
		set
		{
			IratPeldanyTovabbitas.Visible = value;
		}
	}
	public String IratPeldanyTovabbitasOnClientClick
	{
		get { return IratPeldanyTovabbitas.OnClientClick; }
		set { IratPeldanyTovabbitas.OnClientClick = value; }
	}
	#endregion


	#region Iktatas
	public CustomFunctionImageButton IktatasImage
	{
		get { return Iktatas; }
		set { Iktatas = value; }
	}
	public bool IktatasEnabled
	{
		get { return Iktatas.Enabled; }
		set { Iktatas.Enabled = value; }
	}
	public bool IktatasVisible
	{
		get { return Iktatas.Visible; }
		set
		{
			Iktatas.Visible = value;
		}
	}
	public String IktatasOnClientClick
	{
		get { return Iktatas.OnClientClick; }
		set { Iktatas.OnClientClick = value; }
	}
	#endregion

	// CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
	#region Munkapeldany_Iktatas
	public CustomFunctionImageButton Munkapeldany_IktatasImage
	{
		get { return Munkapeldany_Iktatas; }
		set { Munkapeldany_Iktatas = value; }
	}
	public bool Munkapeldany_IktatasEnabled
	{
		get { return Munkapeldany_Iktatas.Enabled; }
		set { Munkapeldany_Iktatas.Enabled = value; }
	}
	public bool Munkapeldany_IktatasVisible
	{
		get { return Munkapeldany_Iktatas.Visible; }
		set
		{
			Munkapeldany_Iktatas.Visible = value;
		}
	}
	public String Munkapeldany_IktatasOnClientClick
	{
		get { return Munkapeldany_Iktatas.OnClientClick; }
		set { Munkapeldany_Iktatas.OnClientClick = value; }
	}
	#endregion

	#region Munkaanyag beiktat�s

	public CustomFunctionImageButton MunkaanyagBeiktatasImage
	{
		get { return MunkaanyagBeiktatas; }
		set { MunkaanyagBeiktatas = value; }
	}
	public bool MunkaanyagBeiktatasEnabled
	{
		get { return MunkaanyagBeiktatas.Enabled; }
		set { MunkaanyagBeiktatas.Enabled = value; }
	}
	public bool MunkaanyagBeiktatasVisible
	{
		get { return MunkaanyagBeiktatas.Visible; }
		set
		{
			MunkaanyagBeiktatas.Visible = value;
		}
	}
	public string MunkaanyagBeiktatasOnClientClick
	{
		get { return MunkaanyagBeiktatas.OnClientClick; }
		set { MunkaanyagBeiktatas.OnClientClick = value; }
	}

	#endregion

	#region Iktatas megtagad�s
	public CustomFunctionImageButton IktatasMegtagadasImage
	{
		get { return IktatasMegtagadas; }
		set { IktatasMegtagadas = value; }
	}
	public bool IktatasMegtagadasEnabled
	{
		get { return IktatasMegtagadas.Enabled; }
		set { IktatasMegtagadas.Enabled = value; }
	}
	public bool IktatasMegtagadasVisible
	{
		get { return IktatasMegtagadas.Visible; }
		set
		{
			IktatasMegtagadas.Visible = value;
		}
	}
	public String IktatasMegtagadasOnClientClick
	{
		get { return IktatasMegtagadas.OnClientClick; }
		set { IktatasMegtagadas.OnClientClick = value; }
	}
	#endregion

	#region AtIktatas
	public CustomFunctionImageButton AtIktatasImage
	{
		get { return AtIktatas; }
		set { AtIktatas = value; }
	}
	public bool AtIktatasEnabled
	{
		get { return AtIktatas.Enabled; }
		set { AtIktatas.Enabled = value; }
	}
	public bool AtIktatasVisible
	{
		get { return AtIktatas.Visible; }
		set
		{
			AtIktatas.Visible = value;
		}
	}
	public String AtIktatasOnClientClick
	{
		get { return AtIktatas.OnClientClick; }
		set { AtIktatas.OnClientClick = value; }
	}
	#endregion

    #region ValaszIrat
    public CustomFunctionImageButton ValaszIratImage
    {
        get { return ValaszIrat; }
        set { ValaszIrat = value; }
    }
    public bool ValaszIratEnabled
    {
        get { return ValaszIrat.Enabled; }
        set { ValaszIrat.Enabled = value; }
    }
    public bool ValaszIratVisible
    {
        get { return ValaszIrat.Visible; }
        set
        {
            ValaszIrat.Visible = value;
        }
    }
    public String ValaszIratOnClientClick
    {
        get { return ValaszIrat.OnClientClick; }
        set { ValaszIrat.OnClientClick = value; }
    }
    #endregion

	#region Felszabaditas
	public CustomFunctionImageButton FelszabaditasImage
	{
		get { return Felszabaditas; }
		set { Felszabaditas = value; }
	}
	public bool FelszabaditasEnabled
	{
		get { return Felszabaditas.Enabled; }
		set { Felszabaditas.Enabled = value; }
	}
	public bool FelszabaditasVisible
	{
		get { return Felszabaditas.Visible; }
		set
		{
			Felszabaditas.Visible = value;
		}
	}
	public string FelszabaditasOnClientClick
	{
		get { return Felszabaditas.OnClientClick; }
		set { Felszabaditas.OnClientClick = value; }
	}
	#endregion


	#region KuldemenyMegtekintese
	public bool KuldemenyMegtekinteseEnabled
	{
		get { return KuldemenyMegtekintese.Enabled; }
		set { KuldemenyMegtekintese.Enabled = value; }
	}
	public bool KuldemenyMegtekinteseVisible
	{
		get { return KuldemenyMegtekintese.Visible; }
		set
		{
			KuldemenyMegtekintese.Visible = value;
		}
	}
	public String KuldemenyMegtekinteseOnClientClick
	{
		get { return KuldemenyMegtekintese.OnClientClick; }
		set { KuldemenyMegtekintese.OnClientClick = value; }
	}
	#endregion


	#region Kuldemeny_lezarasa
	public CustomFunctionImageButton Kuldemeny_lezarasaImage
	{
		get { return Kuldemeny_lezarasa; }
		set { Kuldemeny_lezarasa = value; }
	}
	public bool Kuldemeny_lezarasaEnabled
	{
		get { return Kuldemeny_lezarasa.Enabled; }
		set { Kuldemeny_lezarasa.Enabled = value; }
	}
	public bool Kuldemeny_lezarasaVisible
	{
		get { return Kuldemeny_lezarasa.Visible; }
		set
		{
			Kuldemeny_lezarasa.Visible = value;
		}
	}
	public String Kuldemeny_lezarasaOnClientClick
	{
		get { return Kuldemeny_lezarasa.OnClientClick; }
		set { Kuldemeny_lezarasa.OnClientClick = value; }
	}
	#endregion

	#region Borito_nyomtatasa_A4
	public bool Borito_nyomtatasa_A4Enabled
	{
		get { return Borito_nyomtatasa_A4.Enabled; }
		set { Borito_nyomtatasa_A4.Enabled = value; }
	}
	public bool Borito_nyomtatasa_A4Visible
	{
		get { return Borito_nyomtatasa_A4.Visible; }
		set
		{
			Borito_nyomtatasa_A4.Visible = value;
		}
	}
	public String Borito_nyomtatasa_A4OnClientClick
	{
		get { return Borito_nyomtatasa_A4.OnClientClick; }
		set { Borito_nyomtatasa_A4.OnClientClick = value; }
	}
	#endregion

	#region XMLExport
	public bool XMLExportEnabled
	{
		get { return XMLExport.Enabled; }
		set { XMLExport.Enabled = value; }
	}
	public bool XMLExportVisible
	{
		get { return XMLExport.Visible; }
		set
		{
			XMLExport.Visible = value;
		}
	}
	public String XMLExportOnClientClick
	{
		get { return XMLExport.OnClientClick; }
		set { XMLExport.OnClientClick = value; }
	}
	#endregion

	#region Borito_nyomtatasa_A3
	public bool Borito_nyomtatasa_A3Enabled
	{
		get { return Borito_nyomtatasa_A3.Enabled; }
		set { Borito_nyomtatasa_A3.Enabled = value; }
	}
	public bool Borito_nyomtatasa_A3Visible
	{
		get { return Borito_nyomtatasa_A3.Visible; }
		set
		{
			Borito_nyomtatasa_A3.Visible = value;
		}
	}
	public String Borito_nyomtatasa_A3OnClientClick
	{
		get { return Borito_nyomtatasa_A3.OnClientClick; }
		set { Borito_nyomtatasa_A3.OnClientClick = value; }
	}
	#endregion

	#region HitelesitesiZaradek
	public bool HitelesitesiZaradekEnabled
	{
		get { return HitelesitesiZaradek.Enabled; }
		set { HitelesitesiZaradek.Enabled = value; }
	}
	public bool HitelesitesiZaradekVisible
	{
		get { return HitelesitesiZaradek.Visible; }
		set
		{
			HitelesitesiZaradek.Visible = value;
		}
	}
	public String HitelesitesiZaradekOnClientClick
	{
		get { return HitelesitesiZaradek.OnClientClick; }
		set { HitelesitesiZaradek.OnClientClick = value; }
	}
	#endregion

	#region Kiserolap
	public bool KiserolapEnabled
	{
		get { return Kiserolap.Enabled; }
		set { Kiserolap.Enabled = value; }
	}
	public bool KiserolapVisible
	{
		get { return Kiserolap.Visible; }
		set
		{
			Kiserolap.Visible = value;
		}
	}
	public String KiserolapOnClientClick
	{
		get { return Kiserolap.OnClientClick; }
		set { Kiserolap.OnClientClick = value; }
	}
	#endregion

	#region AtveteliElismerveny
	public bool AtveteliElismervenyEnabled
	{
		get { return AtveteliElismerveny.Enabled; }
		set { AtveteliElismerveny.Enabled = value; }
	}
	public bool AtveteliElismervenyVisible
	{
		get { return AtveteliElismerveny.Visible; }
		set
		{
			AtveteliElismerveny.Visible = value;
		}
	}
	public String AtveteliElismervenyOnClientClick
	{
		get { return AtveteliElismerveny.OnClientClick; }
		set { AtveteliElismerveny.OnClientClick = value; }
	}
	#endregion


	#region Zarolas
	public CustomFunctionImageButton ZarolasImage
	{
		get { return Zarolas; }
		set { Zarolas = value; }
	}
	public bool ZarolasEnabled
	{
		get { return Zarolas.Enabled; }
		set { Zarolas.Enabled = value; }
	}
	public bool ZarolasVisible
	{
		get { return Zarolas.Visible; }
		set
		{
			Zarolas.Visible = value;
		}
	}
	public String ZarolasOnClientClick
	{
		get { return Zarolas.OnClientClick; }
		set { Zarolas.OnClientClick = value; }
	}
	#endregion

	#region Zarolas_feloldasa
	public CustomFunctionImageButton Zarolas_feloldasaImage
	{
		get { return Zarolas_feloldasa; }
		set { Zarolas_feloldasa = value; }
	}
	public bool Zarolas_feloldasaEnabled
	{
		get { return Zarolas_feloldasa.Enabled; }
		set { Zarolas_feloldasa.Enabled = value; }
	}
	public bool Zarolas_feloldasaVisible
	{
		get { return Zarolas_feloldasa.Visible; }
		set
		{
			Zarolas_feloldasa.Visible = value;
		}
	}
	public String Zarolas_feloldasaOnClientClick
	{
		get { return Zarolas_feloldasa.OnClientClick; }
		set { Zarolas_feloldasa.OnClientClick = value; }
	}
	#endregion

	#region EMailNyomtatas
	public CustomFunctionImageButton EMailNyomtatasImage
	{
		get { return EMailNyomtatas; }
		set { EMailNyomtatas = value; }
	}
	public bool EMailNyomtatasEnabled
	{
		get { return EMailNyomtatas.Enabled; }
		set { EMailNyomtatas.Enabled = value; }
	}
	public bool EMailNyomtatasVisible
	{
		get { return EMailNyomtatas.Visible; }
		set
		{
			EMailNyomtatas.Visible = value;
		}
	}
	public String EMailNyomtatasOnClientClick
	{
		get { return EMailNyomtatas.OnClientClick; }
		set { EMailNyomtatas.OnClientClick = value; }
	}
	#endregion

	#region Vonalk�d nyomtat�sa <!--CR 3058-->
	public bool VonalkodNyomtatasa_Enabled
	{
		get { return VonalkodNyomtatasa.Enabled; }
		set
		{
			if ( Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES ).ToUpper ( ).Trim ( ).Equals ( "AZONOSITO" ) )
				VonalkodNyomtatasa.Enabled = value;
			else
				VonalkodNyomtatasa.Enabled = false;
		}
	}
	public bool VonalkodNyomtatasa_Visible
	{
		get { return VonalkodNyomtatasa.Visible; }
		set
		{
			if ( Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES ).ToUpper ( ).Trim ( ).Equals ( "AZONOSITO" ) )
				VonalkodNyomtatasa.Visible = value;
			else
				VonalkodNyomtatasa.Visible = false;
		}
	}
	public String VonalkodNyomtatasa_OnClientClick
	{
		get { return VonalkodNyomtatasa.OnClientClick; }
		set
		{
			if ( Rendszerparameterek.Get ( UI.SetExecParamDefault ( Page, new ExecParam ( ) ), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES ).ToUpper ( ).Trim ( ).Equals ( "AZONOSITO" ) )
				VonalkodNyomtatasa.OnClientClick = value;
			else
				VonalkodNyomtatasa.OnClientClick = null;
		}
	}
    #endregion

    #region DokumentumGeneralas
    public CustomFunctionImageButton DokumentumGeneralasImage
    {
        get { return DokumentumGeneralas; }
        set { DokumentumGeneralas = value; }
    }
    public bool DokumentumGeneralasEnabled
    {
        get { return DokumentumGeneralas.Enabled; }
        set { DokumentumGeneralas.Enabled = value; }
    }
    public bool DokumentumGeneralasVisible
    {
        get { return DokumentumGeneralas.Visible; }
        set
        {
            DokumentumGeneralas.Visible = value;
        }
    }
    public String DokumentumGeneralasOnClientClick
    {
        get { return DokumentumGeneralas.OnClientClick; }
        set { DokumentumGeneralas.OnClientClick = value; }
    }
    #endregion
}
