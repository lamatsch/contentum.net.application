﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="HatosagiStatisztikaTab.ascx.cs" Inherits="HatosagiStatisztikaTab"
    Description="Hatósági Statisztika" %>

<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="uc_OT" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc_TF" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc_FH" %>
<%@ Register Src="../Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc_FTL" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<asp:UpdatePanel ID="ErrorUpdatePanel_HS" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel_HS" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="UpdatePanel_HatosagiStatisztika" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">

            <uc_FTL:FormTemplateLoader ID="FormTemplateLoader1" runat="server" Visible="false"></uc_FTL:FormTemplateLoader>

            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server">
                <table style="width: 100%; border-spacing: 0px; border-collapse: separate;">
                    <tr class="urlapSor">
                        <td style="text-align: left; padding: 0px;">
                            <asp:Label ID="labelHatosagiStatisztika" runat="server"
                                Text="<%$Resources:Form,HatosagiStatisztikaTitle%>"
                                ToolTip="<%$Resources:Form,HatosagiStatisztikaTitle%>"
                                Style="font-weight: bold; padding-left: 10px;" />
                            &nbsp; &nbsp;
                             <asp:Label ID="LabelMessage" runat="server" Visible="false"
                                 Style="font-weight: bold; padding-left: 10px; color: orange" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td style="padding: 0px;">
                            <uc_OT:ObjektumTargyszavaiPanel ID="ObjektumTargyszavaiPanel_HS" RepeatColumns="1" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td style="padding: 0px;  text-align:left;">
                            <asp:ImageButton ID="imgAdatlapNyomtatas" runat="server" AlternateText="'A' adatlap nyomtatása" ImageUrl="../images/hu/ovalgomb/a_adatlap_nyomtatas.gif" onmouseout="swapByName(this.id,'a_adatlap_nyomtatas.gif')" onmouseover="swapByName(this.id,'a_adatlap_nyomtatas2.gif')" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
            <br />
            <br />
            <br />
            <div style="text-align: center;">
                <%-- TabFooter --%>
                <uc_TF:TabFooter ID="TabFooter_HS" runat="server" />
                <%-- /TabFooter --%>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>




