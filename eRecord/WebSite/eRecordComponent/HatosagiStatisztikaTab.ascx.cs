﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HatosagiStatisztikaTab : ContentumUserControlBaseClass
{
    #region PROP
    public bool _Active = false;
    public bool Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;

    public string Command = string.Empty;

    private List<string> _batchIratok;

    private string _ugyFajtaja;

    private PageView pageView = null;

    private string ParentId = string.Empty;

    private string _ParentForm = string.Empty;
    public string ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    EREC_IraIratok irat = null;

    public bool isValidTab;
    public bool IsValidTab
    {
        get { return isValidTab; }
        private set
        {
            isValidTab = value;
        }
    }
    #endregion

    #region PAGE EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel_HS.Visible)
            {
                EErrorPanel_HS.Visible = false;
                ErrorUpdatePanel_HS.Update();
            }
            LabelMessage.Text = string.Empty;
            LabelMessage.Visible = false;
        }

        //LZS - BUG_8882
        //"A" adatlap nyomtatásakor szükséges, hogy view módban maradjon a control.
        Command = Request.QueryString.Get(QueryStringVars.Command);
        if (Command == CommandName.View)
            SetViewControls();
    }
    /// <summary>
    /// Page_Init
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        FormTemplateLoader1.ErrorPanel = EErrorPanel_HS;
        FormTemplateLoader1.SearchObjectType = typeof(HatosagiStatisztikaTargyszavakTemplate);
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);

        if (String.IsNullOrEmpty(Command))
        {
            Command = Request.QueryString.Get(QueryStringVars.Command);
            if (string.IsNullOrEmpty(Command))
                return;
        }

        ParentId = Request.QueryString.Get(QueryStringVars.Id);
        if (string.IsNullOrEmpty(ParentId))
            return;

        TabFooter_HS.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        GetIrat();

        SetVisibility();

        //LZS - BUG_8882 
        //Az "A" adatlap nyomtatása gomb js onclick eseményére rátesszük az A_AdatLapAutoPrintSSRS.aspx nyomtatóoldalt, hogy az irat id-t ennek átadva nyomtatható legyen az adott irat hatósági statisztikája.
        imgAdatlapNyomtatas.OnClientClick = "javascript:window.open('A_AdatLapAutoPrintSSRS.aspx?" + QueryStringVars.IratId + "=" + ParentId + '&' + QueryStringVars.Ugy_fajtaja + "=" + irat.Ugy_Fajtaja + "')";
    }
    #endregion

    public void SetBatchModify(string[] iratIds, string ugyFajtaja)
    {
        _ugyFajtaja = ugyFajtaja;
        _batchIratok = new List<string>(iratIds);
        if (_batchIratok.Count > 0)
        {
            Command = CommandName.Modify;
            ParentId = String.Join(",", _batchIratok.ToArray());
            if (!ParentId.Contains(","))
            {
                // ha csak egy irat van
                GetIrat();
            }
            else
            {
                irat = new EREC_IraIratok { Ugy_Fajtaja = ugyFajtaja };
            }
            SetVisibility();
            Active = true;
            Visible = true;
            ReLoadTab();
        }
    }

    /// <summary>
    /// ReLoadTab
    /// </summary>
    public void ReLoadTab()
    {
        if (irat == null)
            return;

        if (string.IsNullOrEmpty(irat.Ugy_Fajtaja))
        {
            LabelMessage.Text = Resources.Error.UgyFajtajaNemKitoltott;
            LabelMessage.Visible = true;
            return;
        }

        ReLoadTab(true);
    }
    /// <summary>
    /// ReLoadTab
    /// </summary>
    /// <param name="loadRecord"></param>
    private void ReLoadTab(bool loadRecord)
    {
        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {
                if (string.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel_HS);
                    ErrorUpdatePanel_HS.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord betöltése
                        FillObjektumTargyszavaiPanel(ParentId);
                        FormTemplateLoader1.Visible = Command == CommandName.Modify;
                        //LZS - BUG_11755
                        TemplateLoadByDefaultTemplateAndIsFilled();
                    }

                    // BUG_9148
                    if (ObjektumTargyszavaiPanel_HS != null)
                    {
                        var ctrl_Ugyintezes_idotartama = ObjektumTargyszavaiPanel_HS.FindControlByTargyszoId("cfbdb7da-6922-e911-80c9-00155d020b39");
                        var ctrl_Hatarido_tullepes_napokban = ObjektumTargyszavaiPanel_HS.FindControlByTargyszoId("89fc37fe-6922-e911-80c9-00155d020b39");
                        if (ctrl_Ugyintezes_idotartama != null && ctrl_Ugyintezes_idotartama.Control != null && ctrl_Hatarido_tullepes_napokban != null && ctrl_Hatarido_tullepes_napokban.Control != null)
                        {
                            var id_Ugyintezes_idotartama = "#" + ctrl_Ugyintezes_idotartama.Control.ClientID + "_Kodtarak_DropDownList";
                            var id_Hatarido_tullepes_napokban = "#" + ctrl_Hatarido_tullepes_napokban.Control.ClientID + "_TextBox1";
                            var js =
                        @"try
                        {
                            $('" + id_Ugyintezes_idotartama + @"').on('change', function() {
                                    if ($('" + id_Ugyintezes_idotartama + @"').val() == '1') { // hataridon belul
                                        $('" + id_Hatarido_tullepes_napokban + @"').val('0');
                                    }
                                });
                        }
                        catch (e)
                        {
                        }
                        ";
                            ScriptManager.RegisterStartupScript(UpdatePanel_HatosagiStatisztika, UpdatePanel_HatosagiStatisztika.GetType(), "BUG_9148", js, true);
                        }
                    }
                }
            }

            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter_HS.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel_HS, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel_HS.Update();
            MainPanel.Visible = false;
        }
    }

    /// <summary>
    /// SetComponentsVisibility
    /// </summary>
    /// <param name="_command"></param>
    private void SetComponentsVisibility(string _command)
    {
        #region TabFooter gombok állítása

        if (_command == CommandName.Modify)
        {
            TabFooter_HS.Visible = _batchIratok == null || _batchIratok.Count == 0;
            TabFooter_HS.ImageButton_Save.Visible = true;
            TabFooter_HS.ImageButton_SaveAndClose.Visible = true;
            TabFooter_HS.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter_HS.ImageButton_Cancel.Visible = false;

        #endregion

        #region Form komponensek (TextBoxok,...) állítása


        // A gombok láthatósága alapból a Modify esetre van beállítva,
        // View-nál pluszban még azokat kell letiltani, amik módosításnál látszanának
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        #endregion
    }

    /// <summary>
    /// SetViewControls
    /// </summary>
    private void SetViewControls()
    {
        ObjektumTargyszavaiPanel_HS.ReadOnly = true;
        ObjektumTargyszavaiPanel_HS.CssClass = "mrUrlapInputShort ReadOnlyWebControl";

    }
    /// <summary>
    /// FillObjektumTargyszavaiPanel
    /// </summary>
    /// <param name="id"></param>
    protected void FillObjektumTargyszavaiPanel(string id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        if (id != null && id.Contains(",")) // batch modify
        {
            id = null; // ObjektumTargyszavaiPanel_HS.FillObjektumTargyszavaiByContentType(search, )
        }

        if (id == null || ObjektumTargyszavaiPanel_HS.HasObjektumTargyszavai(search, id, null
                    , Constants.TableNames.EREC_IraIratok, Constants.ColumnNames.EREC_IraIratok.Ugy_Fajtaja,
                    new[] { irat.Ugy_Fajtaja },
                    false, KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok, false, true,
                    EErrorPanel_HS))
        {
            ObjektumTargyszavaiPanel_HS.FillObjektumTargyszavai(search, id, null
                    , Constants.TableNames.EREC_IraIratok, Constants.ColumnNames.EREC_IraIratok.Ugy_Fajtaja,
                    new[] { irat.Ugy_Fajtaja },
                    false, KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok, false, true,
                    EErrorPanel_HS);
        }
        else
        {
            LabelMessage.Text = Resources.Error.UgyFajtajaNemMegfeleloHatosagiStatisztikahoz;
            LabelMessage.Visible = true;
            return;
        }
    }

    /// <summary>
    /// SetVisibility
    /// </summary>
    private void SetVisibility()
    {
        if (irat == null)
        {
            Visible = false;
            IsValidTab = false;
        }
        if (string.IsNullOrEmpty(irat.Ugy_Fajtaja))
        {
            Visible = false;
            IsValidTab = false;
        }
        else //LZS - BLG_8882 államigazgatási és önkormányzati ügyfajta esetén kell megjeleníteni az adatlap nyomtatása gombot.
        {
            if ((_batchIratok == null || _batchIratok.Count == 0) && // ha nem batch
                (irat.Ugy_Fajtaja == KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy ||
                irat.Ugy_Fajtaja == KodTarak.UGY_FAJTAJA.OnkormanyzatiUgy))
                imgAdatlapNyomtatas.Visible = true;
            else
                imgAdatlapNyomtatas.Visible = false;
        }

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        if ((ParentId != null && ParentId.Contains(",")) || ObjektumTargyszavaiPanel_HS.HasObjektumTargyszavai(search, ParentId, null
                , Constants.TableNames.EREC_IraIratok, Constants.ColumnNames.EREC_IraIratok.Ugy_Fajtaja,
                new[] { irat.Ugy_Fajtaja },
                false, KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok, false, true,
                 EErrorPanel_HS))
        {
            Visible = true;
            IsValidTab = true;
        }
        else
        {
            Visible = false;
            IsValidTab = false;
        }

    }

    public void ClearTextBoxValues()
    {
        ObjektumTargyszavaiPanel_HS.ClearTextBoxValues();
    }

    public Result BatchSave(IEnumerable<string> iratIds)
    {
        var batchResult = new Result();

        if (iratIds != null)
        {
            // újra be kell állítani
            Command = CommandName.Modify;
            _batchIratok = new List<string>(iratIds);

            foreach (var iratId in iratIds)
            {
                var iratRes = Save(new CommandEventArgs(CommandName.Save, iratId));
                if (iratRes.IsError)
                {
                    if (!batchResult.IsError)
                    { // display first error
                        batchResult = iratRes;
                        batchResult.Ds = new DataSet();
                        batchResult.Ds.Tables.Add();
                        batchResult.Ds.Tables[0].Columns.Add("Id");
                    }
                    batchResult.Ds.Tables[0].Rows.Add(iratId);
                }
            }
        }
        return batchResult;
    }

    /// <summary>
    /// Save
    /// </summary>
    /// <param name="e"></param>
    private Result Save(CommandEventArgs e)
    {
        var result = new Result();
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            switch (Command)
            {
                case CommandName.New:
                    {
                        // New nem lehetséges
                        return result;
                    }

                case CommandName.Modify:
                    {
                        // BUG_9148
                        var ctrl_Ugyintezes_idotartama = ObjektumTargyszavaiPanel_HS.FindControlByTargyszoId("cfbdb7da-6922-e911-80c9-00155d020b39");
                        var ctrl_Hatarido_tullepes_napokban = ObjektumTargyszavaiPanel_HS.FindControlByTargyszoId("89fc37fe-6922-e911-80c9-00155d020b39");
                        if (ctrl_Ugyintezes_idotartama != null && ctrl_Hatarido_tullepes_napokban != null)
                        {
                            if (ctrl_Ugyintezes_idotartama.Value == "1" // hataridon belul
                                && ctrl_Hatarido_tullepes_napokban.Value != "0")
                            {
                                ctrl_Hatarido_tullepes_napokban.Value = "0"; // 0 nap
                            }
                        }

                        // módosított tárgyszavak mentése

                        var isBatch = _batchIratok != null && _batchIratok.Count > 0;
                        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = ObjektumTargyszavaiPanel_HS.GetEREC_ObjektumTargyszavaiList(!isBatch);

                        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                        {
                            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

                            result = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                , EREC_ObjektumTargyszavaiList
                                , isBatch ? e.CommandArgument as string : ParentId
                                , null
                                , Constants.TableNames.EREC_IraIratok
                                , KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok
                                , false
                                );

                            if (result.IsError)
                            {
                                // hiba
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel_HS, result);
                                ErrorUpdatePanel_HS.Update();
                            }
                        }

                        if (!isBatch && !result.IsError)
                        {
                            if (e.CommandName == CommandName.Save)
                            {
                                ReLoadTab(true);
                            }
                            else if (e.CommandName == CommandName.SaveAndClose)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                            }
                        }
                        break;
                    }
            }
        }
        return result;
    }

    private void LoadComponentsFromTemplate(HatosagiStatisztikaTargyszavakTemplate templateObject)
    {
        if (templateObject != null)
        {
            ObjektumTargyszavaiPanel_HS.LoadFromTemplate(templateObject);
        }
    }

    private HatosagiStatisztikaTargyszavakTemplate GetTemplateObjectFromComponents()
    {
        var result = new HatosagiStatisztikaTargyszavakTemplate();
        ObjektumTargyszavaiPanel_HS.UpdateTemplate(result);
        return result;
    }

    #region EVENTS
    private void TabFooterButtonsClick(object sender, CommandEventArgs e)
    {
        Save(e);
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        //Amikor a módosít gombra nyom felül, akkor ide jön be.
        if (e.CommandName == CommandName.LoadTemplateBase)
        {
            TemplateLoadByDefaultTemplateAndIsFilled();

        }//Amikor direktben kiválaszt egy template-t betöltésre
        else if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromTemplate(FormTemplateLoader1.SearchObject as HatosagiStatisztikaTargyszavakTemplate);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormTemplateLoader1.NewTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormTemplateLoader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
        ErrorUpdatePanel_HS.Update();
    }

    private void TemplateLoadByDefaultTemplateAndIsFilled()
    {
        //LZS - BUG_11755
        string[] targyszoErtekek = ObjektumTargyszavaiPanel_HS.GetTargyszoErtekList();
        bool filled = false;

        //LZS - Megvizsgáljuk, hogy van-e ami nem default (üres/"") érték.
        foreach (string targyszo in targyszoErtekek)
        {
            string targyszoNev = targyszo.Split('|')[0];
            string targyszoErtek = targyszo.Split('|')[1];

            //LZS - Ha van amelyik nem default, akkor ez már egy kitöltött hatósági statisztika. filled = true;
            if (!string.IsNullOrEmpty(targyszoErtek) && targyszoErtek != "0")
            {
                filled = true;
                break;
            }

        }

        //Ha módosítás a parancs, van default template és nincs kitöltve még a hatósági statisztika, akkor betölthető a template.
        if (!String.IsNullOrEmpty(FormTemplateLoader1.DefaultTemplateId)
            && Command == CommandName.Modify
            && !filled)
        {
            FormTemplateLoader1.LoadTemplateObjectById(FormTemplateLoader1.DefaultTemplateId);
            LoadComponentsFromTemplate(FormTemplateLoader1.SearchObject as HatosagiStatisztikaTargyszavakTemplate);
        }
    }


    #endregion

    #region SVC
    private void GetIrat()
    {
        EREC_IraIratokService svc = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
        execparam.Record_Id = ParentId;
        Result resultIrat = svc.Get(execparam);
        if (!string.IsNullOrEmpty(resultIrat.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel_HS, resultIrat);
            ErrorUpdatePanel_HS.Update();
            return;
        }

        irat = (EREC_IraIratok)resultIrat.Record;
    }
    #endregion
}