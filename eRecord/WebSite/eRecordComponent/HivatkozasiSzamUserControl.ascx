﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HivatkozasiSzamUserControl.ascx.cs" Inherits="eRecordComponent_HivatkozasiSzamUserControl"
    Description="Hivatkozasi szam" %>

<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-ui.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/JavaScripts/json2.js") %>"></script>

<asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" />
<asp:HiddenField ID="HiddenField_FelelosSzervezetId" runat="server" />

<asp:HiddenField ID="HiddenField_WarningHSZMessage" runat="server" Value="<%$Resources:Error,WarningExistsHivatkozasiSzam%>" />
<asp:HiddenField ID="HiddenField_OkHSZMessage" runat="server" Value="<%$Resources:Form,HivatkozasiSzam%>" />

<asp:TextBox ID="TextBoxHivatkozasiSzam" runat="server" Text="" title="<%$Resources:Form,HivatkozasiSzam%>"></asp:TextBox>
<asp:RequiredFieldValidator ID="ValidatorHivatkozasiSzam" runat="server" ControlToValidate="TextBoxHivatkozasiSzam" SetFocusOnError="true"
    Display="None" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
&nbsp;
<span id="spanWarningHivatkozasiSzam" style="color: red; visibility: hidden; font-weight:bold;" runat="server" title="<%$Resources:Error,WarningExistsHivatkozasiSzam%>">
    !!!
</span>

<script type="text/javascript" title="Hivatkozasi szam">
    try {

        //#region controls
        var hivatkozasiSzamCtrlClientId = '<%=TextBoxHivatkozasiSzam.ClientID %>';
        var hivatkozasiSzamCtrl = $get('<%=TextBoxHivatkozasiSzam.ClientID %>');
        var spanWarningCtrlClientId = '<%=spanWarningHivatkozasiSzam.ClientID %>';
        //#endregion controls

        //#region Init events
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            /* propertychange change keyup paste input*/
            $("#" + hivatkozasiSzamCtrlClientId).on("propertychange change paste input", function () {
                checkHivatkozasiSzamExisting();
            });
        });

        $(function () {
            /* propertychange change keyup paste input*/
            $("#" + hivatkozasiSzamCtrlClientId).on("propertychange change paste input", function () {
                checkHivatkozasiSzamExisting();
            });
        });
        //#endregion Init events

    } catch (e) {
        //alert(e);
    }

    /**
     * Check existing of hivatkozasi szam
     * */
    function checkHivatkozasiSzamExisting() {
        var hivValue = hivatkozasiSzamCtrl.value;

        if (hivValue === null || hivValue === "" || hivValue === undefined) {
            resetWarning();
            return;
        }

        $.ajax({
            url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/IsExistsHivatkozasiSzam") %>',
            data: JSON.stringify({
                hivatkozasizzam: hivValue,
                contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ";"
                    + $("[id$=HiddenField_FelelosSzervezetId]").val()
            }),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                showHideMessage(data)
            },
            error: function (response) {
                //alert(response.responseText);
            },
            failure: function (response) {
                //alert(response.responseText);
            }
        });
    }

    /**
     * Show or hide message icon
     * @param data
     */
    function showHideMessage(data) {
        if (data === null || data.d === null || data.d.length < 1) {
            resetWarning();
            return;
        }

        if (data.d == "True") {
            setWarning();
        }
        else {
            resetWarning();
        }
    };

    /**
     * set warning
     * */
    function setWarning() {
        $("#" + spanWarningCtrlClientId).css("visibility", "visible");

        $("#" + hivatkozasiSzamCtrlClientId).css("border-color", "red");
        $("#" + hivatkozasiSzamCtrlClientId).attr("title", $("[id$=HiddenField_WarningHSZMessage]").val());
        //alert($("[id$=HiddenField_WarningHSZMessage]").val());
    }

    /**
     * reset warning
     * */
    function resetWarning() {
        $("#" + spanWarningCtrlClientId).css("visibility", "hidden");

        $("#" + hivatkozasiSzamCtrlClientId).css("border-color", "");
        $("#" + hivatkozasiSzamCtrlClientId).attr("title", $("[id$=HiddenField_OkHSZMessage]").val());
    }

</script>

