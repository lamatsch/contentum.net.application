﻿using Contentum.eAdmin.Utility;
using System;
using System.Web.UI.WebControls;

public partial class eRecordComponent_HivatkozasiSzamUserControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_FelelosSzervezetId.Value = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
    }

    public bool Enabled
    {
        set { TextBoxHivatkozasiSzam.Enabled = value; }
        get { return TextBoxHivatkozasiSzam.Enabled; }
    }

    public bool ReadOnly
    {
        set { TextBoxHivatkozasiSzam.ReadOnly = value; }
        get { return TextBoxHivatkozasiSzam.ReadOnly; }
    }

    public bool Visibility
    {
        set { TextBoxHivatkozasiSzam.Visible = value; }
        get { return TextBoxHivatkozasiSzam.Visible; }
    }

    public string Text
    {
        set { TextBoxHivatkozasiSzam.Text = value; }
        get { return TextBoxHivatkozasiSzam.Text; }
    }

    public TextBox TextBox
    {
        get { return TextBoxHivatkozasiSzam; }
    }


    #region VALIDATION
    public RequiredFieldValidator Validator
    {
        get
        {
            return ValidatorHivatkozasiSzam;
        }
    }

    public bool Validate
    {
        set { ValidatorHivatkozasiSzam.Enabled = value; }
        get { return ValidatorHivatkozasiSzam.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            ValidatorHivatkozasiSzam.ValidationGroup = value;
            TextBoxHivatkozasiSzam.ValidationGroup = value;

        }
        get { return ValidatorHivatkozasiSzam.ValidationGroup; }
    }
    #endregion

}