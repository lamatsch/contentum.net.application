<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IktatokonyvTextBox.ascx.cs" Inherits="eRecordComponent_IktatokonyvTextBox" %>
<div class="DisableWrap">
<asp:TextBox ID="IktatokonyvMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
    ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" /><asp:HiddenField ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="IktatokonyvMegnevezes" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>