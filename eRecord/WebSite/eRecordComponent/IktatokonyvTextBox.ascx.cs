using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_IktatokonyvTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public String Tipus_IktatoErkezteto
    {
        get 
        {
            object o = ViewState["Tipus_IktatoErkezteto"];
            if (o != null)
                return (string)o;
            return Constants.IktatoErkezteto.Iktato;
        }
        set 
        {
            ViewState["Tipus_IktatoErkezteto"] = value; 
        }
    }
	

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            IktatokonyvMegnevezes.Enabled = value;
            //LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            //if (value == false)
            //{
            //    UI.SwapImageToDisabled(LovImageButton);
            //    UI.SwapImageToDisabled(NewImageButton);
            //}
        }
        get { return IktatokonyvMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set { IktatokonyvMegnevezes.ReadOnly = value; }
        get { return IktatokonyvMegnevezes.ReadOnly; }
    }

    //public string OnClick_Lov
    //{
    //    set { LovImageButton.OnClientClick = value; }
    //    get { return LovImageButton.OnClientClick; }
    //}

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}

    public string Text
    {
        set { IktatokonyvMegnevezes.Text = value; }
        get { return IktatokonyvMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return IktatokonyvMegnevezes; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IktatokonyvMegnevezes.CssClass = "ViewReadOnlyWebControl";
                //LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IktatokonyvMegnevezes.CssClass = "ViewDisabledWebControl";
                //LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }

    private string labelID;

    public string LabelID
    {
        get { return labelID; }
        set { labelID = value; }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetIktatokonyvekTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result.Record;
                this.Tipus_IktatoErkezteto = erec_IraIktatoKonyvek.IktatoErkezteto;
                //Text = erec_IraIktatoKonyvek.MegkulJelzes + " (" + erec_IraIktatoKonyvek.Nev + ")";
                Text = IktatoKonyvek.GetIktatokonyvText(erec_IraIktatoKonyvek);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        SetViewLink();
        SetLabelText();
    }

    private void SetViewLink()
    {
        switch (Tipus_IktatoErkezteto)
        {
            case Constants.IktatoErkezteto.Erkezteto:
                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                   "IraErkeztetoKonyvekForm.aspx", "", HiddenField1.ClientID);
                break;
            case Constants.IktatoErkezteto.Postakonyv:
                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                    "IraPostaKonyvekForm.aspx", "", HiddenField1.ClientID);
                break;
            default:
                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                 "IraIktatoKonyvekForm.aspx", "", HiddenField1.ClientID);
                break;
        }
    }

    private void SetLabelText()
    {
        if (!String.IsNullOrEmpty(this.labelID))
        {
            Label label = (Label)this.NamingContainer.FindControl(this.labelID);
            if (label != null)
            {
                switch (Tipus_IktatoErkezteto)
                {
                    case Constants.IktatoErkezteto.Iktato:
                        label.Text = "Iktatókönyv:";
                        break;
                    case Constants.IktatoErkezteto.Erkezteto:
                        label.Text = "Érkeztetőkönyv:";
                        break;
                    case Constants.IktatoErkezteto.Postakonyv:
                        label.Text = "Postakönyv:";
                        break;
                }
            }
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IktatokonyvMegnevezes);
        //componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        //LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
