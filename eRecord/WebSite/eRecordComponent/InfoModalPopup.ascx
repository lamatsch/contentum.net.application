﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InfoModalPopup.ascx.cs" Inherits="eRecordComponent_InfoModalPopup" %>

<script type="text/javascript">
     function infoPanel_onContinue()  
     {  
       $find('<%=mdeInfo.ClientID %>').hide();
       return false;  
     }
     
    function imp_setText(element, text)  
    {  
      if (typeof element.innerText != 'undefined')  
      {  
          element.innerText = text;  
      }  
      else if (typeof element.textContent != 'undefined')  
      {  
          element.textContent = text;  
      }  
    }
    
    function imp_setLinkText(link, itemType)
    {
        var linkText = '';
        if (link && itemType)
        {
            switch (itemType)
            {                
                case 'EREC_UgyUgyiratok':
                    linkText = 'Ugrás az ügyiratra';
                    break;
                case 'EREC_UgyUgyiratdarabok':
                    linkText = 'Ugrás az ügyiratdarabra';
                    break;
                case 'EREC_IraIratok':
                    linkText = 'Ugrás az iratra';
                    break;
                case 'EREC_PldIratPeldanyok':
                    linkText = 'Ugrás az iratpéldányra';
                    break;
                case 'EREC_KuldKuldemenyek':
                    linkText = 'Ugrás a küldeményre';
                    break;
            }        
            // szöveg beállítása a linknek
            imp_setText(link, linkText);
       }        
    }
        
    var imp_itemId = '';
    var imp_itemType = ''; 
    
    var imp_itemId2 = '';
    var imp_itemType2 = ''; 
    
    function imp_showPopup(text, itemType, itemId, itemType2, itemId2)
    {        
        imp_itemType = itemType;
        imp_itemId = itemId;
        imp_itemType2 = itemType2;
        imp_itemId2 = itemId2;
        
        var element_errorText = $get('<%=labelError.ClientID %>');
        imp_setText(element_errorText, text);
        
        // link beállítása:
        var linkItem = $get('lnkItem');
        if (linkItem)
        {
            if (itemId && itemId != '')
            {
                linkItem.onclick = imp_openWindow_ItemLink1;

                // Szöveg beállítása:
                imp_setLinkText(linkItem, itemType);
            }
            else
            {
                imp_setText(linkItem, '');
                linkItem.style.display = 'none';
            }                     
        }
        
        // 2. link beállítása, ha különböző és meg is van adva
        var linkItem2 = $get('lnkItem2');
        if (linkItem2)
        {            
            linkItem2.onclick = imp_openWindow_ItemLink2;
            
            // ha ugyanaz a két link, a másodikat eltüntetjük:
            if (imp_itemId == imp_itemId2 && imp_itemType == imp_itemType2)
            {
                linkItem2.style.display = 'none';
            }
            else if (imp_itemId2 != '')
            {
                linkItem2.style.display = '';
            }
            
            // Szöveg beállítása:
            imp_setLinkText(linkItem2, itemType2);            
        }
        
        $find('<%=mdeInfo.ClientID %>').show();
        
        return false;
    }    
    
    function imp_openWindow_ItemLink1()
    {
        return imp_openWindow_ItemLink(1);
    }
    
    function imp_openWindow_ItemLink2()
    {
        return imp_openWindow_ItemLink(2);
    }
    
    function imp_openWindow_ItemLink(linkNumber)
    {
        if (   (linkNumber == 1 && imp_itemId && imp_itemId != '' && imp_itemType && imp_itemType != '')
            || (linkNumber == 2 && imp_itemId2 && imp_itemId2 != '' && imp_itemType2 && imp_itemType2 != '')
           )
        {
            var url = '<%=Request.Url.AbsoluteUri.Remove(Request.Url.AbsoluteUri.LastIndexOf("/")+1) %>'; 
            var itemType = '';
            var itemId = '';
            if (linkNumber == 1)
            {
                itemType = imp_itemType;
                itemId = imp_itemId;
            }
            else if (linkNumber == 2)
            {
                itemType = imp_itemType2;
                itemId = imp_itemId2;
            }
            
            switch (itemType)
            {
                case 'EREC_UgyUgyiratok':
                    url += 'UgyUgyiratokForm.aspx';          
                    break;
                case 'EREC_UgyUgyiratdarabok':
                    url += 'UgyUgyiratDarabokForm.aspx';
                    break;
                case 'EREC_IraIratok':
                    url += 'IraIratokForm.aspx';
                    break;
                case 'EREC_PldIratPeldanyok':
                    url += 'PldIratpeldanyokForm.aspx';
                    break;
                case 'EREC_KuldKuldemenyek':
                    url += 'KuldKuldemenyekForm.aspx';
                    break;
            }       
            url += '?Command=Modify&Id=' + itemId;
            
            window.open(url);                 
        }
    }
    
</script>


<asp:Panel ID="pnlInfo" runat="server" CssClass="emp_Panel" Style="display: none">
    <div style="padding: 8px">
        <h2 id="header" class="emp_HeaderWrapper">
            <asp:Label ID="labelHeader" runat="server" Text="A hiba oka:" CssClass="emp_Header"></asp:Label>
        </h2>
        <div id="errorText" class="emp_ErrorText">
            <asp:Label ID="labelError" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <a id="lnkItem" href="javascript:void(0)" style="text-decoration:underline" onclick="return false;">Ugrás a tételre</a>
        <br />
        <a id="lnkItem2" href="javascript:void(0)" style="text-decoration:underline; display: none" onclick="return false;"></a>
        <%--<br />
        <div id="divExceptionDetails" class="emp_Details" style="display: none">
            <asp:Label ID="labelExceptionDetails" runat="server" Text=""></asp:Label>
        </div>--%>
        <br />
        <asp:Button CssClass="emp_OkButton" ID="btnContinue" runat="server" CausesValidation="false" 
            Text="Bezár"/>          
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdeInfo" runat="server" TargetControlID="pnlInfo"
    PopupControlID="pnlInfo" OkControlID="btnContinue" BackgroundCssClass="emp_modalBackground"
    OnOkScript="infoPanel_onContinue()" OnCancelScript="infoPanel_onContinue()"/>
