<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IraIktatoKonyvekDropDownList.ascx.cs" Inherits="eRecordComponent_IraIktatoKonyvekDropDownList" %>

<%@ Register Src="../Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="upMain" runat="server">
    <ContentTemplate>
        <span runat="server" id="Wrapper">
            <asp:HiddenField ID="hfSelectedValue" Value="-1" runat="server" />
            <asp:HiddenField ID="hfSelectedText" runat="server" />
            <asp:HiddenField ID="hfDropDownListsCount" runat="server" />
            <div style="white-space: nowrap; display: inline">
                <asp:DropDownList ID="IraIktatoKonyvek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Validator1" runat="server" Enabled="false" ControlToValidate="IraIktatoKonyvek_DropDownList"
                    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                    TargetControlID="Validator1">
                    <Animations>
                                                                <OnShow>
                                                                <Sequence>
                                                                    <HideAction Visible="true" />
                                                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                </Sequence>    
                                                                </OnShow>
                    </Animations>
                </ajaxToolkit:ValidatorCalloutExtender>
                <asp:Button ID="btnAdd" runat="server" Text="+" OnClick="btnAdd_Click" ToolTip="Keres�si felt�tel hozz�ad�sa" />
                <asp:Button ID="btnRemove" runat="server" Text="-" OnClick="btnRemove_Click" ToolTip="Keres�si felt�tel elt�vol�t�sa" />
            </div>
            <asp:Panel ID="panelDropDownLists" runat="server" Style="display: inline" />
        </span>
    </ContentTemplate>
</asp:UpdatePanel>
