using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUIControls;
using System.Collections.Generic;

public partial class eRecordComponent_IraIktatoKonyvekDropDownList : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

    public string ToolTip
    {
        set { IraIktatoKonyvek_DropDownList.ToolTip = value; }

        get { return IraIktatoKonyvek_DropDownList.ToolTip; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }
    public eDropDownList DropDownList
    {
        get { return IraIktatoKonyvek_DropDownList; }
    }

    public String SelectedValue
    {
        get
        {
            string[] selectedValues = GetSelectedValues(false);
            if (selectedValues.Length > 0) return selectedValues[0];
            return IraIktatoKonyvek_DropDownList.SelectedValue;
        }
        set
        {
            SetSelectedValue(value);
        }
    }

    public bool Enabled
    {
        get { return IraIktatoKonyvek_DropDownList.Enabled; }
        set { IraIktatoKonyvek_DropDownList.Enabled = value; }
    }

    public string CssClass
    {
        get { return IraIktatoKonyvek_DropDownList.CssClass; }
        set { IraIktatoKonyvek_DropDownList.CssClass = value; }
    }

    public bool ReadOnly
    {
        get
        {
            return DropDownList.ReadOnly;
        }
        set
        {
            DropDownList.ReadOnly = value;
        }
    }

    #region MultiSearch

    private bool isMultiSearchMode = false;

    public bool IsMultiSearchMode
    {
        get { return isMultiSearchMode; }
        set { isMultiSearchMode = value; }
    }

    private int DropDownListsCount
    {
        get
        {
            string count = IsPageInitialized ? hfDropDownListsCount.Value : Request.Params[hfDropDownListsCount.UniqueID];
            if (!String.IsNullOrEmpty(count))
            {
                int iCount;
                if (Int32.TryParse(count, out iCount))
                    return iCount;
            }

            return 1;
        }
        set
        {
            hfDropDownListsCount.Value = value.ToString();
        }
    }

    private List<eDropDownList> _dropDownLists = new List<eDropDownList>();

    private string GetDropDownListID(int index)
    {
        return String.Format("{0}#{1}", DropDownList.ID, index);
    }

    #endregion

    protected override void OnLoad(EventArgs e)
    {
        if (!IsMultiSearchMode)
        {
            //Drop down lista selected value-j�nak be�ll�t�sa kliens oldalr�l
            if (IsPostBack && (!String.IsNullOrEmpty(EvIntervallum_SearchFormControlId) || (!String.IsNullOrEmpty(EvTextBoxId))))
            {
                if (hfSelectedValue.Value == "-1")
                {
                    DropDownList.SelectedIndex = -1;
                }
                else
                {
                    if (DropDownList.SelectedValue != hfSelectedValue.Value || DropDownList.Text != hfSelectedText.Value)
                    {
                        if (DropDownList.Items.FindByValue(hfSelectedValue.Value) == null || DropDownList.Items.FindByText(hfSelectedText.Value) == null)
                        {
                            DropDownList.Items.Clear();
                            ListItem item = new ListItem(hfSelectedText.Value, hfSelectedValue.Value);
                            DropDownList.Items.Add(item);
                        }

                        DropDownList.SelectedValue = hfSelectedValue.Value;
                    }
                }
            }
        }
        base.OnLoad(e);
    }

    private bool IsPageInitialized = false;
    protected void Page_Init(object sender, EventArgs e)
    {
        _dropDownLists.Add(this.DropDownList);
        for (int i = 2; i <= DropDownListsCount; i++)
        {
            AddDropDownList(i, true);
        }
        IsPageInitialized = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsMultiSearchMode)
        {
            if (EvIntervallum_SearchFormControl != null)
            {
                sm = ScriptManager.GetCurrent(Page);
                if (sm != null)
                    sm.RegisterAsyncPostBackControl(EvIntervallum_SearchFormControl.EvTol_TextBox);
                //EvIntervallum_SearchFormControl.EvTol_TextBox.AutoPostBack = true;
                EvIntervallum_SearchFormControl.EvTol_TextBox.Attributes["onchange"] =
                    String.Format("javascript:setTimeout('__doPostBack(\\'{0}\\',\\'\\')', 100)", EvIntervallum_SearchFormControl.EvTol_TextBox.UniqueID);
                EvIntervallum_SearchFormControl.EvTol_TextBox.TextChanged += new EventHandler(EvTextBox_TextChanged);
                if (sm != null)
                    sm.RegisterAsyncPostBackControl(EvIntervallum_SearchFormControl.EvIg_TextBox);
                //EvIntervallum_SearchFormControl.EvIg_TextBox.AutoPostBack = true;
                EvIntervallum_SearchFormControl.EvIg_TextBox.Attributes["onchange"] =
                   String.Format("javascript:setTimeout('__doPostBack(\\'{0}\\',\\'\\')', 100)", EvIntervallum_SearchFormControl.EvIg_TextBox.UniqueID);
                EvIntervallum_SearchFormControl.EvIg_TextBox.TextChanged += new EventHandler(EvTextBox_TextChanged);
            }
            else if (EvTextBox != null)
            {
                sm = ScriptManager.GetCurrent(Page);
                if (sm != null)
                    sm.RegisterAsyncPostBackControl(EvTextBox);
                EvTextBox.AutoPostBack = true;
                EvTextBox.TextChanged += new EventHandler(EvTextBox_TextChanged);
            }
        }
        else
        {
            RegisterJavascript();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        btnAdd.Visible = IsMultiSearchMode;
        btnRemove.Visible = isMultiSearchMode && DropDownListsCount > 1;
    }

    private void RegisterJavascript()
    {
        string js = @"function " + this.ClientID + @"_onchange(sender,e)
        {
            var dropDown = $get('" + IraIktatoKonyvek_DropDownList.ClientID + @"');";
        if (FelhasznaloProfil.OrgIsNMHH(Page))
        {
            js += @" if(dropDown && dropDown.length == 2 && !dropDown.options[0].value) { dropDown.selectedIndex = 1; } ";// BUG_6173
        }
        js += @"
        var hiddenValue = $get('" + hfSelectedValue.ClientID + @"');
            var hiddenText = $get('" + hfSelectedText.ClientID + @"');
            if(dropDown && hiddenValue && hiddenText)
            {
                if(dropDown.selectedIndex > -1)
                {
                    hiddenValue.value = dropDown.options[dropDown.selectedIndex].value;
                    hiddenText.value = dropDown.options[dropDown.selectedIndex].text;
                }
                else
                {
                    hiddenValue.value = '-1';
                    hiddenText.value = '';
                }
            }
        }";

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), this.ClientID + "_onchange", js, true);
    }

    public void FillDropDownList(bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto, bool addEmptyItem, eErrorPanel errorPanel)
    {
        FillDropDownList(filterForActualYear, filterForNotClosed, iktatoerkezteto, addEmptyItem, false, errorPanel);
    }

    /// <summary>
    /// Felt�lti a dropdownlist-et iktat�- vagy �rkeztet�k�nyvekkel
    /// </summary>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="filterForActualYear">ha true: csak az aktu�lis �vi iktat�k�nyveket hozza</param>
    /// <param name="filterForNotClosed">ha true: csak a lez�ratlan iktat�k�nyveket hozza</param>
    /// <param name="addEmptyItem"></param>
    /// <param name="addEmptyOnlyIfMoreIktKonyv">ha true: Csak akkor adja hozz� az �res elemet, ha legal�bb k�t iktat�k�nyv van</param>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto, bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, eErrorPanel errorPanel)
    {

        Result result;

        List<IktatoKonyvek.IktatokonyvItem> IktatokonyvekList = null;

        result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, filterForActualYear, filterForNotClosed, out IktatokonyvekList);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            IraIktatoKonyvek_DropDownList.Items.Clear();

            foreach (IktatoKonyvek.IktatokonyvItem item in IktatokonyvekList)
            {
                try
                {
                    IraIktatoKonyvek_DropDownList.Items.Add(new ListItem(item.Text, item.Id));
                }
                catch
                {
                    // hiba, pl. nem stimmel az oszlopn�v
                    IraIktatoKonyvek_DropDownList.Items.Clear();
                    IraIktatoKonyvek_DropDownList.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                    //IraIktatoKonyvek_DropDownList.Enabled = false;
                    break;
                }

            }

            if (addEmptyItem &&
                !(addEmptyOnlyIfMoreIktKonyv && IraIktatoKonyvek_DropDownList.Items.Count <= 1))
            {
                IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(emptyListItem.Text, emptyListItem.Value));
            }
        }
    }

    public void FillDropDownList(bool filterForActualYear, bool filterForNotClosed, string iktatoerkezteto, eErrorPanel errorPanel)
    {
        FillDropDownList(filterForActualYear, filterForNotClosed, iktatoerkezteto, false, errorPanel);
    }

    /// <summary>
    /// Felt�lti a dropdownlist-et az adott iratt�ri t�telsz�mhoz rendelt iktat�k�nyvekkel
    /// </summary>
    /// <param name="filterForActualYear">ha true: csak az aktu�lis �vi iktat�k�nyveket hozza</param>
    /// <param name="filterForNotClosed">ha true: csak a lez�ratlan iktat�k�nyveket hozza</param>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="errorPanel"></param>
    public void FillDropDownListByIrattariTetel(bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto, string IrattariTetel_Id, eErrorPanel errorPanel)
    {
        IktatoKonyvek.FillDropDownListByIrattariTetel(IraIktatoKonyvek_DropDownList, filterForActualYear, filterForNotClosed, iktatoerkezteto, IrattariTetel_Id, Page, errorPanel);
    }

    public EREC_IraIktatoKonyvek FillWithOneValue(string iktatoerkezteto, string iktatoId, eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(iktatoId))
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = iktatoId;

            //EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
            //Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
            //Search.IktatoErkezteto.Value = iktatoerkezteto;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                IraIktatoKonyvek_DropDownList.Items.Clear();

                if (result.Record != null)
                {
                    EREC_IraIktatoKonyvek erec_IraIktatokonyvek = (EREC_IraIktatoKonyvek)result.Record;

                    IraIktatoKonyvek_DropDownList.Items.Add(
                        new ListItem(IktatoKonyvek.GetIktatokonyvText(erec_IraIktatokonyvek), erec_IraIktatokonyvek.Id));

                    return erec_IraIktatokonyvek;

                }
                else
                {
                    IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(iktatoId + " " + deletedValue, iktatoId));
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(emptyListItem.Text, emptyListItem.Value));
        }

        return null;

    }

    /// <summary>
    /// Felt�lti a list�t, �s be�ll�tja az �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    public void FillAndSetSelectedValue(bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto, String selectedValue, bool addEmptyItem, eErrorPanel errorPanel)
    {
        FillDropDownList(filterForActualYear, filterForNotClosed, iktatoerkezteto, addEmptyItem, errorPanel);
        ListItem selectedListItem = IraIktatoKonyvek_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            IraIktatoKonyvek_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            // Megkeress�k az iktat�k�nyvek k�z�tt, �s megjelen�tj�k a nev�t (ha nincs meg, csak akkor �rjuk ki, hogy t�r�lt)
            if (!String.IsNullOrEmpty(selectedValue))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = selectedValue;

                Result result = service.Get(execParam);
                // ha nem volt hiba, �s megtal�ltuk a rekordot:
                if (String.IsNullOrEmpty(result.ErrorCode) && result.Record != null)
                {
                    EREC_IraIktatoKonyvek iktKonyv = (EREC_IraIktatoKonyvek)result.Record;

                    IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(IktatoKonyvek.GetIktatokonyvText(iktKonyv), selectedValue));
                    return;
                }
            }

            IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }

    public void FillAndSetSelectedValue(bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto, String selectedValue, eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(filterForActualYear, filterForNotClosed, iktatoerkezteto, selectedValue, false, errorPanel);
    }

    public void FillAndSetEmptyValue(bool filterForActualYear, bool filterForNotClosed, string iktatoerkezteto, eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(filterForActualYear, filterForNotClosed, iktatoerkezteto, emptyListItem.Value, true, errorPanel);
    }


    #region FillParameters
    [Serializable]
    private class FillParameters
    {
        private string iktatoErkezteto = Constants.IktatoErkezteto.Erkezteto;

        public string IktatoErkezteto
        {
            get { return iktatoErkezteto; }
            set { iktatoErkezteto = value; }
        }

        private bool filterIdeIktathat = true;

        public bool FilterIdeIktathat
        {
            get { return filterIdeIktathat; }
            set { filterIdeIktathat = value; }
        }

        private bool addEmptyItem = false;

        public bool AddEmptyItem
        {
            get { return addEmptyItem; }
            set { addEmptyItem = value; }
        }

        private bool addEmptyOnlyIfMoreIktKonyv = false;

        public bool AddEmptyOnlyIfMoreIktKonyv
        {
            get { return addEmptyOnlyIfMoreIktKonyv; }
            set { addEmptyOnlyIfMoreIktKonyv = value; }
        }

    }

    private FillParameters CurrentFillParameters
    {
        get
        {
            object o = ViewState["CurrentFillParameters"];
            if (o != null)
            {
                return o as FillParameters;
            }

            return null;
        }
        set
        {
            ViewState["CurrentFillParameters"] = value;
        }
    }

    #endregion FillParameteres

    #region FillDropDownList_SetMegKulJelzesToValues
    /// <summary>
    /// A listaelemek �rt�k�nek a Megk�l�nb�ztet� jelz�s lesz be�ll�tva, �s egy adott megk�l�nb�ztet� jelz�st csak egyszer ad a dropdown list�hoz
    /// Csak a szervezet �ltal l�that� iktat�k�nyveket hozza
    /// </summary>   
    public void FillDropDownList_SetMegKulJelzesToValues(string iktatoerkezteto, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, eErrorPanel errorPanel)
    {
        FillDropDownList_SetMegKulJelzesToValues(iktatoerkezteto, true
            , filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, errorPanel);
    }


    /// <summary>
    /// A listaelemek �rt�k�nek a Megk�l�nb�ztet� jelz�s lesz be�ll�tva, �s egy adott megk�l�nb�ztet� jelz�st csak egyszer ad a dropdown list�hoz
    /// </summary>   
    /// <param name="filter_IdeIktathat">ha false: Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre, j�n az �sszes</param>
    public void FillDropDownList_SetMegKulJelzesToValues(string iktatoerkezteto, bool filter_IdeIktathat, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, eErrorPanel errorPanel)
    {
        SetValues(false, iktatoerkezteto, filter_IdeIktathat, filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv);
    }

    /// <summary>
    /// A listaelemek �rt�k�nek a Megk�l�nb�ztet� jelz�s lesz be�ll�tva, �s egy adott megk�l�nb�ztet� jelz�st csak egyszer ad a dropdown list�hoz
    /// + a megadott elem kiv�laszt�sa a list�ban
    /// </summary>    
    public void FillAndSelectValue_SetMegKulJelzesToValues(string iktatoerkezteto, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, string selectedMegKulJelzes, eErrorPanel errorPanel)
    {
        /// filter_IdeIktathat = true, vagyis sz�r�s a szervezet iktat�k�nyveire
        FillAndSelectValue_SetMegKulJelzesToValues(iktatoerkezteto, true
            , filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, selectedMegKulJelzes, errorPanel);
    }

    public void FillAndSelectValue_SetMegKulJelzesToValues(string iktatoerkezteto, bool filter_IdeIktathat, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, string selectedMegKulJelzes, eErrorPanel errorPanel)
    {
        FillDropDownList_SetMegKulJelzesToValues(iktatoerkezteto, filter_IdeIktathat, filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, errorPanel);
        UpdateDropdownListsForSelectedValues(selectedMegKulJelzes);
    }
    #endregion FillDropDownList_SetMegKulJelzesToValues

    #region FillDropDownList_SetIdToValues

    /// <summary>
    /// A listaelemekbe az iktat�k�nyv Id-ja van-e belet�ltve (nem pedig az iktat�hely)
    /// </summary>
    public bool ValuesFilledWithId { get; set; }

    /// <summary>
    /// A listaelemek �rt�k�nek az iktat�k�nyv Id lesz be�ll�tva,
    /// olyan "iktat�k�nyv" t�pusokn�l (pl. postak�nyv) c�lszer� haszn�lni, amelyek nem ker�lnek �vente lez�r�sra
    /// Csak a szervezet �ltal l�that� iktat�k�nyveket hozza
    /// </summary>   
    public void FillDropDownList_SetIdToValues(string iktatoerkezteto, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, eErrorPanel errorPanel)
    {
        FillDropDownList_SetIdToValues(iktatoerkezteto, true
            , filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, errorPanel);
    }

    private bool IsFilterByEv(string filter_EvTol, string filter_EvIg)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        bool filterByEv = true;

        // �vre sz�r�s:
        if (!String.IsNullOrEmpty(filter_EvTol) || !String.IsNullOrEmpty(filter_EvIg))
        {
            if (mode == Constants.IktatoKonyvekDropDownListMode.IktatokonyvekWithRegiAdatok)
            {
                int iEvTol;
                if (Int32.TryParse(filter_EvTol, out iEvTol))
                {
                    int MigralasEve = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.MIGRALAS_EVE);
                    if (MigralasEve != 0 && MigralasEve > iEvTol)
                    {
                        filterByEv = false;
                    }
                }

            }
        }

        return filterByEv;
    }

    private void SetValues(bool useId, string iktatoerkezteto, bool filter_IdeIktathat, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv)
    {
        #region param�terek elment�se

        CurrentFillParameters = new FillParameters();
        CurrentFillParameters.IktatoErkezteto = iktatoerkezteto;
        CurrentFillParameters.FilterIdeIktathat = filter_IdeIktathat;
        CurrentFillParameters.AddEmptyItem = addEmptyItem;
        CurrentFillParameters.AddEmptyOnlyIfMoreIktKonyv = addEmptyOnlyIfMoreIktKonyv;

        #endregion

        Result result = new Result();

        List<IktatoKonyvek.IktatokonyvItem> IktatokonyvekList = null;

        bool filterByEv = IsFilterByEv(filter_EvTol, filter_EvIg);
        if (filterByEv)
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, filter_EvTol, filter_EvIg, out IktatokonyvekList);
            }
            else
            {
                // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Cache, UI.SetExecParamDefault(Page)
                , iktatoerkezteto, filter_EvTol, filter_EvIg, false, out IktatokonyvekList);
            }
        }
        else
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, out IktatokonyvekList);
            }
            else
            {
                // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Cache, UI.SetExecParamDefault(Page)
                , iktatoerkezteto, filter_EvTol, filter_EvIg, false, out IktatokonyvekList);
            }
        }

        if (!result.IsError)
        {
            foreach (eDropDownList dList in _dropDownLists)
            {
                dList.Items.Clear();

                foreach (IktatoKonyvek.IktatokonyvItem item in IktatokonyvekList)
                {
                    try
                    {
                        // ha m�g nincs ilyen elem a list�ban, hozz�adjuk:
                        var value = useId ? item.Id : item.Value;
                        if (dList.Items.FindByValue(value) == null)
                        {
                            dList.Items.Add(new ListItem(item.Text, value));
                        }
                    }
                    catch
                    {
                        // hiba, pl. nem stimmel az oszlopn�v
                        dList.Items.Clear();
                        dList.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                        //IraIktatoKonyvek_DropDownList.Enabled = false;
                        break;
                    }

                }

                if (addEmptyItem &&
                    !(addEmptyOnlyIfMoreIktKonyv && dList.Items.Count <= 1))
                {
                    dList.Items.Insert(0, new ListItem(emptyListItem.Text, emptyListItem.Value));

                    if (FelhasznaloProfil.OrgIsNMHH(Page))
                    {
                        if (filterByEv && dList.Items.Count == 2)
                        {
                            dList.SelectedIndex = 1;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// A listaelemek �rt�k�nek az iktat�k�nyv Id lesz be�ll�tva,
    /// olyan "iktat�k�nyv" t�pusokn�l (pl. postak�nyv) c�lszer� haszn�lni, amelyek nem ker�lnek �vente lez�r�sra
    /// </summary>   
    /// <param name="filter_IdeIktathat">ha false: Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre, j�n az �sszes</param>
    public void FillDropDownList_SetIdToValues(string iktatoerkezteto, bool filter_IdeIktathat, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, eErrorPanel errorPanel)
    {
        SetValues(true, iktatoerkezteto, filter_IdeIktathat, filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv);
    }

    /// <summary>
    /// A listaelemek �rt�k�nek az iktat�k�nyv Id lesz be�ll�tva,
    /// olyan "iktat�k�nyv" t�pusokn�l (pl. postak�nyv) c�lszer� haszn�lni, amelyek nem ker�lnek �vente lez�r�sra
    /// + a megadott elem kiv�laszt�sa a list�ban
    /// </summary>    
    public void FillAndSelectValue_SetIdToValues(string iktatoerkezteto, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, string selectedIktatokonyvId, eErrorPanel errorPanel)
    {
        /// filter_IdeIktathat = true, vagyis sz�r�s a szervezet iktat�k�nyveire
        FillAndSelectValue_SetIdToValues(iktatoerkezteto, true
            , filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, selectedIktatokonyvId, errorPanel);
    }

    /// <summary>
    /// A listaelemek �rt�k�nek az iktat�k�nyv Id lesz be�ll�tva,
    /// olyan "iktat�k�nyv" t�pusokn�l (pl. postak�nyv) c�lszer� haszn�lni, amelyek nem ker�lnek �vente lez�r�sra
    /// </summary>
    public void FillAndSelectValue_SetIdToValues(string iktatoerkezteto, bool filter_IdeIktathat, string filter_EvTol, string filter_EvIg
        , bool addEmptyItem, bool addEmptyOnlyIfMoreIktKonyv, string selectedIktatokonyvId, eErrorPanel errorPanel)
    {
        FillDropDownList_SetIdToValues(iktatoerkezteto, filter_IdeIktathat, filter_EvTol, filter_EvIg, addEmptyItem, addEmptyOnlyIfMoreIktKonyv, errorPanel);
        UpdateDropdownListsForSelectedValues(selectedIktatokonyvId);
    }

    #endregion FillDropDownList_SetIdToValues

    private void UpdateDropdownListsForSelectedValues(string selectedIktatokonyvId)
    {
        string[] selectedValues = GetSelectedValues(selectedIktatokonyvId);
        // ha van ilyen elem a list�ban:
        if (selectedValues[0] != null)
        {
            if (IraIktatoKonyvek_DropDownList.Items.FindByValue(selectedValues[0]) != null)
            {
                IraIktatoKonyvek_DropDownList.SelectedValue = selectedValues[0];
            }
        }

        for (int i = DropDownListsCount; i > selectedValues.Length; i--)
        {
            RemoveDropDownList();
        }

        for (int i = 1; i < selectedValues.Length; i++)
        {
            string selectedValue = selectedValues[i];
            DropDownListsCount = i + 1;
            eDropDownList dList = AddDropDownList(DropDownListsCount, false);
            if (selectedValue != null)
            {
                if (dList.Items.FindByValue(selectedValue) != null)
                {
                    dList.SelectedValue = selectedValue;
                }
            }
        }
    }

    /// <summary>
    /// Be�ll�tja a m�r felt�lt�tt lista �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ListItem selectedListItem = IraIktatoKonyvek_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            IraIktatoKonyvek_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            IraIktatoKonyvek_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
            IraIktatoKonyvek_DropDownList.SelectedValue = IraIktatoKonyvek_DropDownList.Items[0].Value;
        }
    }

    public void SetSearchObject(EREC_IraIktatoKonyvekSearch search)
    {
        // BUG#4290:
        SetSearchField(this.ValuesFilledWithId ? search.Id : search.Iktatohely);
    }

    private void SetSearchField(Field searchField)
    {
        if (searchField != null)
        {
            string[] selectedValues = this.GetSelectedValues(false);

            if (selectedValues.Length > 0)
            {
                if (selectedValues.Length == 1)
                {
                    searchField.Filter(selectedValues[0]);
                }
                else
                {
                    searchField.In(selectedValues);
                }
            }
        }
    }

    public void SetSearchObject(Field searchField_Iktatohely, Field searchField_MegkulJelzes)
    {
        SetSearchField(searchField_Iktatohely);
        // TODO: searchField_MegkulJelzes hi�nyzik!?
    }

    public string GetSelectedSav()
    {
        string sav = String.Empty;

        if (DropDownList.SelectedIndex > -1 && !String.IsNullOrEmpty(DropDownList.SelectedValue))
        {
            string iktatohely = String.Empty;
            string[] values = DropDownList.SelectedValue.Split(new string[] { IktatoKonyvek.valueDelimeter }, StringSplitOptions.None);

            iktatohely = values[0].Trim();

            sav = IktatoKonyvek.GetSavFromIktatohely(iktatohely);
        }

        return sav;
    }

    #region MultiSearch
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        RemoveDropDownList();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DropDownListsCount = DropDownListsCount + 1;
        AddDropDownList(DropDownListsCount, false);
    }

    private eDropDownList AddDropDownList(int index, bool isInit)
    {
        if (_dropDownLists.Count < index)
        {
            eDropDownList dlist = new eDropDownList();
            dlist.ID = GetDropDownListID(index);

            panelDropDownLists.Controls.Add(new LiteralControl("<br/>"));
            panelDropDownLists.Controls.Add(dlist);
            dlist.Style.Add("margin-top", "2px");

            if (!isInit)
            {
                dlist.CssClass = DropDownList.CssClass;
                foreach (ListItem item in DropDownList.Items)
                {
                    ListItem newItem = new ListItem(item.Text, item.Value);
                    dlist.Items.Add(newItem);
                }
            }

            _dropDownLists.Add(dlist);
        }

        return _dropDownLists[index - 1];
    }

    private void RemoveDropDownList()
    {
        DropDownListsCount = DropDownListsCount - 1;
        for (int i = 0; i < 2; i++)
        {
            if (panelDropDownLists.Controls.Count > 0)
            {
                panelDropDownLists.Controls.RemoveAt(panelDropDownLists.Controls.Count - 1);
            }
        }
    }

    private string[] GetSelectedValues(bool withEmptyValues)
    {
        List<string> selectedValues = new List<string>();

        foreach (eDropDownList dlist in _dropDownLists)
        {
            if (!String.IsNullOrEmpty(dlist.SelectedValue) || withEmptyValues)
                selectedValues.Add(dlist.SelectedValue);
        }

        return selectedValues.ToArray();
    }

    private string[] GetSelectedValues(string selectedValues)
    {
        string[] arraySelectedValues = selectedValues.Split(',');
        for (int i = 0; i < arraySelectedValues.Length; i++)
        {
            arraySelectedValues[i] = arraySelectedValues[i].Trim('\'');
        }

        return arraySelectedValues;
    }

    private string[] GetSelectedTexts()
    {
        List<string> selectedTexts = new List<string>();

        foreach (eDropDownList dlist in _dropDownLists)
        {
            if (!String.IsNullOrEmpty(dlist.SelectedValue))
                selectedTexts.Add(dlist.SelectedItem.Text);
        }

        return selectedTexts.ToArray();
    }

    protected void EvTextBox_TextChanged(object sender, EventArgs e)
    {
        if ((EvIntervallum_SearchFormControl != null || EvTextBox != null) &&
            CurrentFillParameters != null)
        {
            string evTol = String.Empty;
            string evIg = String.Empty;
            if (EvIntervallum_SearchFormControl != null)
            {
                evTol = EvIntervallum_SearchFormControl.EvTol;
                evIg = EvIntervallum_SearchFormControl.EvIg;
            }
            else if (EvTextBox != null)
            {
                evIg = evTol = EvTextBox.Text;
            }

            string selectedValues = String.Join(",", GetSelectedValues(true));

            // BUG#4290: Ha Id-val kell fel�lteni az elemeket:        
            if (this.ValuesFilledWithId)
            {
                FillAndSelectValue_SetIdToValues(CurrentFillParameters.IktatoErkezteto, CurrentFillParameters.FilterIdeIktathat
                    , evTol, evIg, CurrentFillParameters.AddEmptyItem, CurrentFillParameters.AddEmptyOnlyIfMoreIktKonyv, selectedValues, null);
            }
            else
            {
                FillAndSelectValue_SetMegKulJelzesToValues(CurrentFillParameters.IktatoErkezteto, CurrentFillParameters.FilterIdeIktathat,
                    evTol, evIg, CurrentFillParameters.AddEmptyItem, CurrentFillParameters.AddEmptyOnlyIfMoreIktKonyv, selectedValues, null);
            }
        }
    }

    #endregion

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(DropDownList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                Enabled = _ViewEnabled;
                DropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        text = String.Join(", ", GetSelectedTexts());

        return text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    private string evIntervallum_SearchFormControlId;

    public string EvIntervallum_SearchFormControlId
    {
        get { return (evIntervallum_SearchFormControlId == null) ? String.Empty : evIntervallum_SearchFormControlId; }
        set
        {
            evIntervallum_SearchFormControlId = value;
            if (String.IsNullOrEmpty(evIntervallum_SearchFormControlId))
            {
                evTextBoxId = String.Empty;
            }
        }
    }

    private ASP.component_evintervallum_searchformcontrol_ascx evIntervallum_SearchFormControl = null;

    private ASP.component_evintervallum_searchformcontrol_ascx EvIntervallum_SearchFormControl
    {
        get
        {
            if (evIntervallum_SearchFormControl == null)
            {
                if (!String.IsNullOrEmpty(EvIntervallum_SearchFormControlId))
                {
                    evIntervallum_SearchFormControl = (ASP.component_evintervallum_searchformcontrol_ascx)this.NamingContainer.FindControl(EvIntervallum_SearchFormControlId);
                    if (evIntervallum_SearchFormControl == null)
                    {
                        Logger.Warn(String.Format("Az EvIntervallum_SearchFormControlId-ban megadott vez�rl� nem tal�lhat�: {0}", EvIntervallum_SearchFormControlId));
                    }
                }
            }
            return evIntervallum_SearchFormControl;
        }
    }

    private string evTextBoxId;

    public string EvTextBoxId
    {
        get { return (evTextBoxId == null) ? String.Empty : evTextBoxId; }
        set
        {
            evTextBoxId = value;
            if (String.IsNullOrEmpty(evTextBoxId))
            {
                evIntervallum_SearchFormControlId = String.Empty;
            }
        }
    }

    private TextBox evTextBox = null;

    private TextBox EvTextBox
    {
        get
        {
            if (evTextBox == null)
            {
                if (!String.IsNullOrEmpty(EvTextBoxId))
                {
                    evTextBox = (TextBox)this.NamingContainer.FindControl(EvTextBoxId);
                    if (evTextBox == null)
                    {
                        Logger.Warn(String.Format("Az EvTextBoxId-ban megadott vez�rl� nem tal�lhat�: {0}", EvTextBoxId));
                    }
                }
            }

            return evTextBox;
        }
    }

    private Constants.IktatoKonyvekDropDownListMode mode = Constants.IktatoKonyvekDropDownListMode.ErkeztetoKonyvek;

    public Constants.IktatoKonyvekDropDownListMode Mode
    {
        get { return mode; }
        set { mode = value; }
    }


    private bool filter_IdeIktathat = true;

    public bool Filter_IdeIktathat
    {
        get { return filter_IdeIktathat; }
        set { filter_IdeIktathat = value; }
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (!IsMultiSearchMode)
        {
            if (!String.IsNullOrEmpty(EvIntervallum_SearchFormControlId))
            {
                evIntervallum_SearchFormControl = (ASP.component_evintervallum_searchformcontrol_ascx)this.NamingContainer.FindControl(EvIntervallum_SearchFormControlId);
                if (evIntervallum_SearchFormControl == null)
                {
                    Logger.Warn(String.Format("Az EvIntervallum_SearchFormControlId-ban megadott vez�rl� nem tal�lhat�: {0}", EvIntervallum_SearchFormControlId));
                }
            }
            else if (!String.IsNullOrEmpty(EvTextBoxId))
            {
                evTextBox = (TextBox)this.NamingContainer.FindControl(EvTextBoxId);
                if (evTextBox == null)
                {
                    Logger.Warn(String.Format("Az EvTextBoxId-ban megadott vez�rl� nem tal�lhat�: {0}", EvTextBoxId));
                }
            }

            if (evIntervallum_SearchFormControl != null || evTextBox != null)
            {
                if (!this.DesignMode)
                {
                    // Test for ScriptManager and register if it exists
                    sm = ScriptManager.GetCurrent(Page);

                    if (sm == null)
                        throw new HttpException("A ScriptManager control must exist on the current page.");


                    sm.RegisterScriptControl(this);

                    sm.Services.Add(new ServiceReference("~/WrappedWebService/Ajax_eRecord.asmx"));

                    string js_onchangeEvent = String.Empty;

                    if (evIntervallum_SearchFormControl != null)
                    {
                        if (filter_IdeIktathat)
                        {
                            js_onchangeEvent = "fillErkeztetokonyvDropDownByEv('" + DropDownList.ClientID
                            + "','" + evIntervallum_SearchFormControl.EvTol_TextBox.ClientID
                            + "','" + evIntervallum_SearchFormControl.EvIg_TextBox.ClientID
                            + "','" + FelhasznaloProfil.FelhasznaloId(Page)
                            + "','" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page)
                            + "','" + Mode + "','" + ValuesFilledWithId.ToString() + "');";
                        }
                        else
                        {
                            // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre, j�n az �sszes:
                            js_onchangeEvent = "fillAllErkeztetokonyvDropDownByEv('" + DropDownList.ClientID
                           + "','" + evIntervallum_SearchFormControl.EvTol_TextBox.ClientID
                           + "','" + evIntervallum_SearchFormControl.EvIg_TextBox.ClientID
                           + "','" + FelhasznaloProfil.FelhasznaloId(Page)
                           + "','" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page)
                           + "','" + Mode + "','" + ValuesFilledWithId.ToString() + "');";
                        }

                        // �v textboxok v�ltoz�s�nak figyel�s�re:
                        evIntervallum_SearchFormControl.EvTol_TextBox.Attributes["onchange"] = js_onchangeEvent;

                        evIntervallum_SearchFormControl.EvIg_TextBox.Attributes["onchange"] = js_onchangeEvent;

                        //string js = "var evTolTextBox = $get('" + evIntervallum_SearchFormControl.EvTol_TextBox.ClientID + "'); if(evTolTextBox) evTolTextBox.onchange=function(){" + js_onchangeEvent + "};";
                        //js += "var evIgTextBox = $get('" + evIntervallum_SearchFormControl.EvIg_TextBox.ClientID + "'); if(evIgTextBox) evIgTextBox.onchange=function(){" + js_onchangeEvent + "};";

                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "js_onchangeEvent", js, true);
                    }
                    else if (evTextBox != null)
                    {
                        if (filter_IdeIktathat)
                        {
                            js_onchangeEvent = "fillErkeztetokonyvDropDownByEv('" + DropDownList.ClientID
                            + "','" + evTextBox.ClientID
                            + "','" + evTextBox.ClientID
                            + "','" + FelhasznaloProfil.FelhasznaloId(Page)
                            + "','" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page)
                            + "','" + Mode + "','" + ValuesFilledWithId.ToString() + "');";
                        }
                        else
                        {
                            // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre, j�n az �sszes:
                            js_onchangeEvent = "fillAllErkeztetokonyvDropDownByEv('" + DropDownList.ClientID
                           + "','" + evTextBox.ClientID
                           + "','" + evTextBox.ClientID
                           + "','" + FelhasznaloProfil.FelhasznaloId(Page)
                           + "','" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page)
                           + "','" + Mode + "','" + ValuesFilledWithId.ToString() + "');";
                        }

                        // �v textboxok v�ltoz�s�nak figyel�s�re:
                        evTextBox.Attributes["onchange"] = js_onchangeEvent;

                        //string js = "var evTextBox = $get('" + evIntervallum_SearchFormControl.EvTol_TextBox.ClientID + "'); if(evTextBox) evTextBox.onchange=function(){" + js_onchangeEvent + "};";

                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "js_onchangeEvent", js, true);
                    }

                    //selected value kezel�se
                    string jsDropDownChange = this.ClientID + "_onchange()";

                    this.DropDownList.Attributes.Add("onchange", jsDropDownChange);
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!IsMultiSearchMode)
        {
            if ((evIntervallum_SearchFormControl != null) || (evTextBox != null))
            {
                if (!this.DesignMode)
                {
                    sm.RegisterScriptDescriptors(this);

                    if (DropDownList.SelectedIndex > -1)
                    {
                        hfSelectedValue.Value = DropDownList.SelectedValue;
                        hfSelectedText.Value = DropDownList.Text;
                    }
                    else
                    {
                        hfSelectedValue.Value = "-1";
                        hfSelectedText.Value = String.Empty;
                    }
                }
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!IsMultiSearchMode)
        {
            ScriptReference reference = new ScriptReference();
            reference.Path = "~/JavaScripts/WsCalls.js?t=20190918";
            ScriptReference referenceCommon = new ScriptReference();
            referenceCommon.Assembly = "AjaxControlToolkit";
            referenceCommon.Name = "AjaxControlToolkit.Common.Common.js";

            return new ScriptReference[] { reference, referenceCommon };
        }

        return null;
    }

    #endregion
}
