﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IraIratFormTab.ascx.cs"
    Inherits="eRecordComponent_IraIratFormTab" %>
<%@ Register Src="../Component/EditableElosztoivTextBox.ascx" TagName="ElosztoivTextBox"
    TagPrefix="uc16" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc15" %>
<%@ Register Src="IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox" TagPrefix="uc14" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc13" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc9" %>
<%@ Register Src="../Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="../Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList"
    TagPrefix="uc6" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc7" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="FunkcioGombsor.ascx" TagName="FunkcioGombsor" TagPrefix="fgs" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc10" %>
<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
<%@ Register Src="../Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc11" %>
<%@ Register Src="UgyiratSzerelesiLista.ascx" TagName="UgyiratSzerelesiLista" TagPrefix="uc17" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp" %>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm" %>
<%@ Register Src="~/eRecordComponent/IrattariTetelszamDropDownList.ascx" TagName="IrattariTetelszamDropDownList"
    TagPrefix="uc18" %>
<%@ Register Src="~/Component/FelelosSzervezetVezetoTextBox.ascx" TagPrefix="fszvez" TagName="FelelosSzervezetVezetoTextBox" %>
<%@ Register Src="~/eRecordComponent/PldIratIktatasPanelUserControl.ascx" TagPrefix="cc" TagName="PldIratIktatasPanelUserControl" %>


<%@ Register Src="~/eRecordComponent/TerjedelemPanel.ascx" TagName="TerjedelemPanel" TagPrefix="uc15" %>
<%@ Register Src="../Component/MinositoPartnerControl.ascx" TagName="MinositoPartnerControl" TagPrefix="ucMP" %>
<%@ Register Src="~/eRecordComponent/MellekletekPanel.ascx" TagName="MellekletekPanel" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/BejovoPeldanyPanel.ascx" TagName="BejovoPeldanyPanel" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/BejovoPeldanyListPanel.ascx" TagName="BejovoPeldanyListPanel" TagPrefix="uc" %>

<%@ Register Src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="EPARTTBWF" %>
<%@ Register Src="IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="IraIratUpdatePanel" runat="server" OnLoad="IraIratUpdatePanel_Load">
    <ContentTemplate>

        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="Storno_Empty_Ugyirat" runat="server" Value="0" />
            <table cellpadding="0" cellspacing="0" width="80%">
                <tr>
                    <td valign="top">
                        <asp:Panel ID="FoPanel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="80%">
                                <tr class="urlapSor_kicsi">
                                    <td>
                                        <asp:RadioButtonList ID="IktatasVagyMunkapeldanyLetrehozasRadioButtonList" runat="server"
                                            RepeatDirection="Horizontal" Visible="False">
                                            <asp:ListItem Selected="True" Value="Iktatas">Iktatás</asp:ListItem>
                                            <asp:ListItem Value="MunkapeldanyLetrehozas">Munkapéldány létrehozás</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <eUI:eFormPanel ID="KuldemenyPanel" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="0%">
                                                <tbody>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelSearch">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelVonalkodSearch" runat="server" Text="Küldemény vonalkód:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" style="color: red;">
                                                            <uc10:VonalKodTextBox ID="VonalkodTextBoxKuldemeny" runat="server" Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short" width="0%" colspan="2">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="ImageButton_KuldemenyKereses" runat="server" Visible="True"
                                                                                ToolTip="Küldemény keresése" ImageUrl="~/images/hu/trapezgomb/kuldemeny_keresese.gif"
                                                                                CommandName="KuldemenyKereses" CausesValidation="false" CssClass="highlightit"
                                                                                OnClick="ImageButton_KuldemenyKereses_Click"></asp:ImageButton>
                                                                        </td>
                                                                        <td style="padding-left: 2px; padding-right: 2px;">
                                                                            <asp:ImageButton ID="ImageButton_KuldemenyMegtekintes" runat="server" ImageUrl="../images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                                CommandName="" AlternateText="Megtekintés" CssClass="highlightit"></asp:ImageButton>
                                                                            <asp:HiddenField ID="Kuldemeny_Id_HiddenField" runat="server"></asp:HiddenField>
                                                                        </td>
                                                                        <td style="padding-left: 2px; padding-right: 2px;">
                                                                            <asp:ImageButton ID="ImageButton_KuldemenyModositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                                CommandName="" AlternateText="Módosítás" CssClass="highlightit"></asp:ImageButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="ImageButton_KuldemenyErkeztetes" runat="server" CausesValidation="false"
                                                                                CommandName="" CssClass="highlightit" ImageUrl="~/images/hu/trapezgomb/erkeztetes.gif"
                                                                                ToolTip="Küldemény érkeztetése" Visible="True" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelErkeztetoSzam">
                                                        <td class="mrUrlapCaption_short">
                                                            <%--<asp:Label ID="Label12" runat="server" Text="Érkezt.könyv:"></asp:Label>--%>
                                                            <asp:Label ID="Label12" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" style="color: red;">
                                                            <%--<uc14:IktatokonyvTextBox ID="ErkeztetoKonyv_IktatokonyvTextBox" runat="server" />--%>
                                                            <asp:TextBox ID="ErkeztetoSzam_TextBox" runat="server" CssClass="mrUrlapInputRed"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label22" runat="server" Text="Küldő/feladó neve:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc2:PartnerTextBox ID="Bekuldo_PartnerTextBox" runat="server" ViewMode="true" ReadOnly="true"
                                                                Validate="false"></uc2:PartnerTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBeerkezesModja">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label62" runat="server" Text="Beérkezés módja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="KuldesMod_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label3" runat="server" Text="Küldő/feladó címe:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" ReadOnly="true" ViewMode="true"
                                                                Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBonto">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label4" runat="server" Text="Bontó:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc15:FelhasznaloCsoportTextBox ID="Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox"
                                                                runat="server" ReadOnly="true" ViewMode="true" Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label6" runat="server" Text="Bontás időpontja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="Kuld_FelbontasDatuma_CalendarControl" runat="server" ReadOnly="true"
                                                                Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <%--BUG_7678--%>
                                                    <tr class="urlapSor_kicsi" runat="server" id="tr_feladasiIdo">
                                                        <td class="mrUrlapCaption_short">
                                                            <%--<asp:CheckBox ID="CheckBox1" runat="server" ToolTip="Beérkezés időpontjának megőrzése"
                                                                    TabIndex="-1" />--%>
                                                            <asp:Label ID="Label9" runat="server" Text="Feladási idő:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="3">
                                                            <cc:CalendarControl ID="FeladasiIdo_CalendarControl" runat="server" ReadOnly="true" TimeVisible="true"
                                                                Validate="false"></cc:CalendarControl>
                                                        </td>
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelKuldoIktatoszama">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label102" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:TextBox ID="HivatkozasiSzam_Kuldemeny_TextBox" runat="server" CssClass="mrUrlapInput"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label82" runat="server" Text="Beérkezés időpontja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="BeerkezesIdeje_CalendarControl" runat="server" ReadOnly="true"
                                                                Validate="false"></cc:CalendarControl>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBelsoCimzett">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label122" runat="server" Text="Címzett neve:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc5:CsoportTextBox ID="CsoportId_CimzettCsoportTextBox1" runat="server" ViewMode="true"
                                                                ReadOnly="true" Validate="false"></uc5:CsoportTextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label132" runat="server" Text="Kézbesítés prioritása:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Surgosseg_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="AdathordozoTipusa_KuldemenyTD" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label112" runat="server" Text="Küldemény típusa:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="AdathordozoTipusa_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label20" runat="server" Text="Elsődleges adathordozó típusa:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="UgyintezesModja_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="MunkaugyiratElozmenyPanel" runat="server" Visible="false">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelMunkaugyiratElozmeny" runat="server" CssClass="DisableWrap" Text="Szerelendő előzmény:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc9:UgyiratTextBox ID="Szerelendo_Ugyirat_UgyiratTextBox" runat="server" Filter="SzerelesKikeressel" MigralasVisible="True" Validate="false" />
                                                    </td>
                                                </tr>
                                            </table>

                                        </eUI:eFormPanel>
                                        <%-- Rejtett textbox tárolja a régi ügyirat irattári tételszámát és megkülönböztető jelzését --%>
                                        <asp:TextBox ID="IrattariTetelTextBox" Style="display: none;" Text="" runat="server" />
                                        <asp:TextBox ID="MegkulJelzesTextBox" Style="display: none;" Text="" runat="server" />
                                        <asp:TextBox ID="IRJ2000TextBox" Style="display: none;" Text="" runat="server" />
                                        <eUI:CustomCascadingDropDown ID="CascadingDropDown_AgazatiJel" runat="server" UseContextKey="true"
                                            ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetAgazatiJelek"
                                            Category="AgazatiJel" Enabled="False" TargetControlID="AgazatiJelek_DropDownList"
                                            EmptyText="---" EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]"
                                            PromptText="<Ágazati jel kiválasztása>" PromptValue="">
                                        </eUI:CustomCascadingDropDown>
                                        <%-- CR3119: Mindenhol irattári tételszám jelenjen meg--%>
                                        <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugykor" runat="server" TargetControlID="Ugykor_DropDownList"
                                            ParentControlID="AgazatiJelek_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                            ServiceMethod="GetUgykorokByAgazatiJel" Category="Ugykor" Enabled="False" EmptyText="---"
                                            EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Irattári tételszám kiválasztása>"
                                            PromptValue="">
                                        </eUI:CustomCascadingDropDown>
                                        <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="UgyUgyirat_Ugytipus_DropDownList"
                                            ParentControlID="Ugykor_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                            ServiceMethod="GetUgytipusokByUgykor" Category="Ugytipus" Enabled="False" EmptyText="---"
                                            EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Ügytípus kiválasztása>"
                                            PromptValue="">
                                        </eUI:CustomCascadingDropDown>
                                        <eUI:CustomCascadingDropDown ID="CascadingDropDown_IktatoKonyv" runat="server"
                                            TargetControlID="Iktatokonyvek_DropDownLis_Ajax" ParentControlID="Ugykor_DropDownList"
                                            UseContextKey="true" Category="Iktatokonyv" Enabled="false" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                            ServiceMethod="GetIktatokonyvekByIrattaritetel" EmptyText="---" EmptyValue=""
                                            LoadingText="[Feltöltés folyamatban...]" SelectedValue="" PromptText="<Iktatókönyv kiválasztása>"
                                            PromptValue="">
                                        </eUI:CustomCascadingDropDown>
                                        <uc17:UgyiratSzerelesiLista runat="server" ID="UgyiratSzerelesiLista1" />
                                        <eUI:eFormPanel ID="Ugyirat_Panel" runat="server">
                                            <%--
                                  EmptyText="---" EmptyValue="" LoadingText="[Feltöltés folyamatban...]"  SelectedValue="" PromptText="<Iktatókönyv kiválasztása>" PromptValue=""
                                            --%>
                                            <%-- <asp:Image ID="SpacerImage2" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif" Visible="false" />	
                                            --%>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr id="tr_lezartazUgyirat" runat="server" visible="false" class="urlapSor_kicsi">
                                                    <td colspan="2" style="text-align: center; color: Red; font-weight: bold;">
                                                        <asp:Label ID="Label2" runat="server" Text="Figyelem: Lezárt az előzmény ügyirat!"></asp:Label>
                                                    </td>
                                                    <td colspan="2" style="text-align: left;">
                                                        <asp:RadioButtonList ID="RadioButtonList_Lezartbaiktat_vagy_Ujranyit" runat="server"
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Lez&#225;rt &#252;gyiratba iktat&#225;s</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">&#220;gyirat &#250;jranyit&#225;sa</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <%--<td class="mrUrlapCaption_short">
                                        </td>
                                        <td class="mrUrlapMezo">
                                        </td>--%>
                                                </tr>
                                                <tr id="tr_lezartazIktatokonyv" runat="server" visible="false" class="urlapSor_kicsi">
                                                    <td colspan="4" style="text-align: center; color: Red; font-weight: bold;">
                                                        <asp:Label ID="labelLezart" runat="server" Text="Figyelem: Az előzmény lezárt iktatókönyvben van! Az előzmény az új ügyiratba lesz szerelve!" />
                                                        <asp:Label ID="labelLezartFolyamatos" runat="server" Text="Figyelem: Az előzmény lezárt, folyósorszámos iktatókönyvben van! (iktatás csak alszámra megengedett)" />
                                                        <asp:HiddenField ID="LezartIktatokonyv_HiddenField" runat="server" />
                                                        <asp:HiddenField ID="LezartFolyosorszamosIktatokonyv_HiddenField" runat="server" />
                                                    </td>
                                                    <%--                                                    <td colspan="2" style="text-align: left;">
                                                    </td>--%>
                                                </tr>
                                                <tr runat="server" id="trFoszam" visible="false" class="foszamRow">
                                                    <td class="foszamLabel" colspan="2">
                                                        <asp:Label ID="labelFoszam" runat="server" CssClass="DisableWrap" Text="Ügyirat iktatószáma:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="foszamText" colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td class="foszamText">
                                                                    <asp:Label ID="UgyiratFoszam_Label" runat="server" CssClass="DisableWrap" Text="[Főszám]"></asp:Label>
                                                                </td>
                                                                <td style="width: 100%"></td>
                                                                <td style="text-align: right;">
                                                                    <asp:ImageButton ID="FoszamraIktat_ImageButton" runat="server" Visible="false" CausesValidation="false"
                                                                        ImageUrl="~/images/hu/egyeb/reset_icon.png"
                                                                        onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')"
                                                                        AlternateText="Előzmény törlése" ToolTip="Előzmény törlése"
                                                                        OnClick="FoszamraIktat_ImageButton_Click"></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trRegiAdatAzonosito" visible="false" class="foszamRow">
                                                    <td class="foszamLabel" colspan="2">
                                                        <asp:Label ID="labelRegiAdatAzonosito" runat="server" CssClass="DisableWrap" Text="Régi azonosító:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="foszamText" colspan="2">
                                                        <asp:Label ID="RegiAdatAzonosito_Label" runat="server" CssClass="DisableWrap" Text="[Régi azonosító]">
                                                        </asp:Label>
                                                        <asp:Label ID="RegiAdatIRJ2000_Label" runat="server" CssClass="DisableWrap" Text="" Visible="false">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trElozmenySearch" style="background-color: #F2E3FB;">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                                    </td>
                                                    <td colspan="3" class="mrUrlapMezo" style="text-align: left;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left">
                                                            <tr>
                                                                <td class="mrUrlapMezo" style="width: 90px;">
                                                                    <uc11:EvIntervallum_SearchFormControl ID="evIktatokonyvSearch" IsIntervallumMode="false"
                                                                        runat="server" />
                                                                </td>
                                                                <td class="mrUrlapCaption_nowidth" style="width: 80px;">
                                                                    <asp:Label ID="labelIktatokonyvSearch" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" style="width: 110px">
                                                                    <uc11:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList_Search" runat="server"
                                                                        Mode="IktatokonyvekWithRegiAdatok" EvIntervallum_SearchFormControlId="evIktatokonyvSearch" />
                                                                </td>
                                                                <td class="mrUrlapCaption_nowidth" style="width: 80px">
                                                                    <asp:Label ID="labelFoszamSearch" runat="server" Text="Főszám:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" style="width: 120px">
                                                                    <uc11:RequiredNumberBox ID="numberFoszamSearch" Validate="false" runat="server" />
                                                                </td>
                                                                <td style="padding-top: 5px">
                                                                    <%--<asp:ImageButton ID="ImageButtonElozmenySearch" runat="server" CausesValidation="false"
                                                        CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif"                                            
                                                        ToolTip="Előzmény" Visible="True" CssClass="highlightit" OnClick="ImageButtonElozmenySearch_Click" />--%>
                                                                    <asp:ImageButton ID="ImageButton_Elozmeny" runat="server" CausesValidation="false"
                                                                        CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif" ToolTip="Előzmény"
                                                                        Visible="True" CssClass="highlightit" OnClick="ImageButton_Elozmeny_Click" />
                                                                </td>
                                                                <td style="padding-top: 5px">
                                                                    <asp:ImageButton ID="ImageButton_MigraltKereses" runat="server" CausesValidation="false"
                                                                        CommandName="" ImageUrl="~/images/hu/trapezgomb/migraltkereses_trap1.jpg" ToolTip="Régi adatok"
                                                                        Visible="True" CssClass="highlightit" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <%--Elõirat, utóirat iktatószámok --%>
                                                <tr id="trEloUtoIktatoszamok" runat="server" class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label14" runat="server" Text="Elõirat iktatószáma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:TextBox ID="eloirat_IktatoSzamTextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label21" runat="server" Text="Utóirat iktatószáma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:TextBox ID="utoirat_IktatoSzamTextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                    </td>
                                                </tr>

                                                <tr id="tr_agazatiJel" runat="server" class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label_agazatiJel" runat="server" Text="Ágazati jel:"></asp:Label>
                                                        <%--BLG_44--%>
                                                        <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"
                                                            Width="97%">
                                                        </asp:DropDownList>
                                                        <%--BLG_44--%>
                                                        <ktddl:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" CssClass="" Width="98%" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <%--                                           <asp:Lable ID="labelMerge_IrattariTetelszam" runat="server" Text="Összefûzött itsz.:" />--%>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:TextBox ID="Merge_IrattariTetelszamTextBox" runat="server" ReadOnly="true" CssClass="mrUrlapLabelLike"
                                                            TabIndex="-1" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelIktatokonyv">
                                                    <td class="mrUrlapCaption_short">
                                                        <%--CR3119 : Mindenhol irattári tételszám jelenjen meg--%>
                                                        <asp:Label ID="labelIrattariTetelszam" runat="server" Text="Irattári tételszám:"></asp:Label>
                                                        <%--<asp:Label ID="labelUgykorKod" runat="server" Text="Ügykör kód:"></asp:Label>--%>
                                                        <%--BLG_44--%>
                                                        <%--<asp:Label ID="label_IrattariTetelszam_DropDownList" runat="server" Text="Irattári tételszám:"></asp:Label>--%>  

                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"
                                                            Width="97%">
                                                        </asp:DropDownList>
                                                        <%--<asp:TextBox ID="Merge_IrattariTetelszamTextBox" runat="server" ReadOnly="true" Width="90px" TabIndex="-1" />--%>
                                                        <uc12:IraIrattariTetelTextBox ID="UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox"
                                                            runat="server" Validate="false" Visible="false" />
                                                        <%--BLG_44--%>
                                                        <uc18:IrattariTetelszamDropDownList ID="IrattariTetelszam_DropDownList" runat="server"></uc18:IrattariTetelszamDropDownList>

                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*">&nbsp;</asp:Label><asp:Label
                                                            ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc11:IraIktatoKonyvekDropDownList ID="ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1"
                                                            runat="server" />
                                                        <asp:DropDownList ID="Iktatokonyvek_DropDownLis_Ajax" runat="server" CssClass="mrUrlapInputComboBox"
                                                            Visible="False">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Iktatokonyvek_DropDownLis_Ajax"
                                                            Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                                            TargetControlID="RequiredFieldValidator1">
                                                            <Animations>
                                                                <OnShow>
                                                                <Sequence>
                                                                    <HideAction Visible="true" />
                                                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                </Sequence>    
                                                                </OnShow>
                                                            </Animations>
                                                        </ajaxToolkit:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="tr_ugytipus" runat="server" class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label27" runat="server" Text="Ügytípus:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" OnSelectedIndexChanged="UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged"
                                                            Width="97%">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="UgyUgyirat_Ugytipus_DropDownList"
                                                            Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                                                            TargetControlID="RequiredFieldValidator2">
                                                            <Animations>
                                                                    <OnShow>
                                                                    <Sequence>
                                                                        <HideAction Visible="true" />
                                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                    </Sequence>    
                                                                    </OnShow>
                                                            </Animations>
                                                        </ajaxToolkit:ValidatorCalloutExtender>
                                                        <%-- Módosításkor az irattípus váltásánál innen vesszük ki az értéket --%>
                                                        <asp:HiddenField ID="Ugytipus_HiddenField" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelTargy">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label55" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="Label30" runat="server" Text="Ügyirat tárgya:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc8:RequiredTextBox ID="UgyUgyirat_Targy_RequiredTextBox" Width="95%" runat="server"
                                                            Rows="2" TextBoxMode="MultiLine" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short" colspan="2">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left">
                                                            <tr>
                                                                <td></td>
                                                                <td style="padding-left: 2px;"></td>
                                                                <td></td>
                                                                <td style="width: 100%"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Megtekintes" runat="server" ImageUrl="../images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                        CommandName="" AlternateText="Megtekintés" CssClass="highlightit" />
                                                                    <asp:HiddenField ID="ElozmenyUgyiratID_HiddenField" runat="server" />
                                                                    <asp:HiddenField ID="MigraltUgyiratID_HiddenField" runat="server" />
                                                                </td>
                                                                <td style="padding-left: 2px;">
                                                                    <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Modositas" runat="server" ImageUrl="../images/hu/trapezgomb/modositas_trap.jpg"
                                                                        CommandName="" AlternateText="Módosítás" CssClass="highlightit" />
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="ImageButton_Ugyiratterkep" runat="server" CausesValidation="false"
                                                                        CssClass="highlightit" ImageUrl="~/images/hu/trapezgomb/ugyirat_fa_trap.jpg"
                                                                        ToolTip="Ügyirat-fa" Visible="True" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label runat="server" Text="Ügyirat ügyintézési ideje:" ID="trUgyiratIntezesiIdoLabel" Visible="false" />
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="IntezesiIdo_KodtarakDropDownList" runat="server" CssClass="" Visible="false" />
                                                        <ktddl:KodtarakDropDownList ID="IntezesiIdoegyseg_KodtarakDropDownList" runat="server" CssClass="" Visible="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label_CalendarControl_UgyintezesKezdete" runat="server" Text="Ügyirat ügyintézési kezdete:" />
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <cc:CalendarControl ID="CalendarControl_UgyintezesKezdete" runat="server" Validate="false" TimeVisible="true" Enabled="false" ReadOnly="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelHatarido">
                                                    <td class="mrUrlapCaption_short">&nbsp;<asp:Label ID="Label28" runat="server" Text="Ügyirat ügyintézési határideje:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <cc:CalendarControl ID="UgyUgyirat_Hatarido_CalendarControl" runat="server" Visible="true"
                                                            Validate="false" TimeVisible="true" />
                                                        <%-- az alszámra iktatásnál a határidő módosulásának figyeléséhez --%>
                                                        <asp:HiddenField ID="ElozmenyUgyiratHatarido_HiddenField" runat="server" />
                                                        <asp:HiddenField ID="UgyiratHataridoKitolas_Ugyirat_HiddenField" runat="server" />
                                                        <asp:HiddenField ID="IktatasDatuma_Ugyirat_HiddenField" runat="server" />
                                                        <asp:HiddenField ID="FelhasznaloCsoportIdOrzo_HiddenField" runat="server" />
                                                        <asp:HiddenField ID="Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelSkontroVege" runat="server" Text="Skontró vége dátum:" Visible="False"></asp:Label>
                                                        <asp:Label ID="labelIrattarba" runat="server" Text="Irattárba helyezés dátuma:" Visible="False"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <asp:TextBox ID="txtSkontroVege" runat="server" Style="display: none;" />
                                                        <asp:TextBox ID="txtIrattarba" runat="server" Style="display: none;" />
                                                        <cc:CalendarControl ID="UgyUgyirat_SkontroVege_CalendarControl" runat="server" Visible="false"
                                                            Validate="false" ReadOnly="True" />
                                                        <cc:CalendarControl ID="UgyUgyirat_IrattarbaHelyezes_CalendarControl" runat="server"
                                                            Visible="false" Validate="false" ReadOnly="True" />
                                                    </td>
                                                </tr>
                                                <%--<tr id="tr_felelos_kezelo" runat="server" class="urlapSor_kicsi">--%>
                                                <tr id="migraltUgyiratAzonositoMezo" class="urlapSor_kicsi" runat="server" style="display: none;">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="regiAzonositoLabel" runat="server" Text="Régi azonosító"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:TextBox ID="regiAzonositoTextBox" runat="server" Validate="false" CssClass="ReadOnlyWebControl" />
                                                    </td>
                                                </tr>
                                                <tr id="tr_JavasoltElozmeny" class="urlapSor_kicsi" runat="server" visible="false">
                                                    <td class="mrUrlapCaption_short"></td>
                                                    <td class="mrUrlapMezo" width="0%"></td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelJavasoltElozmeny" runat="server" Text="Javasolt előzmény:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%" colspan="1">
                                                        <asp:TextBox ID="JavasoltElozmenyTextBox" runat="server" CssClass="mrUrlapInput" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelFelelos">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="ReqStar_Ugyfelelos_CsoportTextBox" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="LabelSzervezetiEgyseg" runat="server" Text="<%$Forditas:LabelSzervezetiEgyseg|Szervezeti egység:%>"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc5:CsoportTextBox ID="Ugyfelelos_CsoportTextBox" SzervezetCsoport="true" Validate="false"
                                                            runat="server" LabelRequiredIndicatorID="ReqStar_Ugyfelelos_CsoportTextBox" />
                                                        <%--    <asp:Label ID="Label_KezeloSzignJegyzekAlapjan" runat="server" Text="(Kezelő meghatározása szignálási jegyzék alapján)"
                                            Visible="False"></asp:Label>--%>
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelUgyUgyirat_UgyintezoStar" runat="server" CssClass="ReqStar" Text="*" />
                                                        <%--BLG_1014--%>
                                                        <%--<asp:Label ID="labelUgyUgyirat_Ugyintezo" runat="server" Text="Ügyintéző:"></asp:Label>--%>
                                                        <asp:Label ID="labelUgyUgyirat_Ugyintezo" runat="server" Text="<%$Forditas:labelUgyUgyirat_Ugyintezo|Ügyintéző:%>"></asp:Label>

                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%" colspan="1">
                                                        <uc15:FelhasznaloCsoportTextBox ID="UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox"
                                                            runat="server" Validate="false" LabelRequiredIndicatorID="labelUgyUgyirat_UgyintezoStar" />
                                                    </td>
                                                </tr>
                                                <%--<tr id="tr_Ugyfelelos" runat="server" class="urlapSor_kicsi" visible="false">--%>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelKezelo">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label51" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="Label31" runat="server" Text="Kezelő:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc5:CsoportTextBox ID="UgyUgyiratok_CsoportId_Felelos" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="lblReqUgyindito" runat="server" Text="*" Visible="false" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="lblUgyindito" runat="server" Text="Ügyfél, ügyindító:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="1" width="0%">
                                                        <uc2:PartnerTextBox ID="Ugy_PartnerId_Ugyindito_PartnerTextBox" Validate="false" TryFireChangeEvent="true"
                                                            runat="server" WithAllCimCheckBoxVisible="false" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelIratHelye">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelIratHelye_Ugyirat" runat="server" Text="Irat helye:" Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc15:FelhasznaloCsoportTextBox ID="UgyiratOrzo_FelhasznaloCsoportTextBox"
                                                            runat="server" Validate="false" ViewMode="true" Visible="false" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="lblReqUgyinditoCime" runat="server" Text="*" Visible="false" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="labelUgyinditoCime" runat="server" Text="Ügyindító címe:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="1" width="0%">
                                                        <uc3:CimekTextBox ID="CimekTextBoxUgyindito" runat="server" Validate="false"></uc3:CimekTextBox>
                                                    </td>
                                                </tr>
                                                <tr id="tr_fizikaihelyUgyirat" runat="server" visible="false" class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="label7" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="LabelFizikaiUgyirat" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUKUgyirat" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                            </table>
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                                            <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak" RepeatColumns="2" RepeatDirection="Horizontal"
                                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                        </eUI:eFormPanel>
                                        <asp:HiddenField ID="UgyiratdarabEljarasiSzakasz_HiddenField" runat="server" />
                                        <eUI:eFormPanel ID="IratPanel" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="80%">
                                                <tbody>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelIratTargya">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label41" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label50" runat="server" Text="Irat tárgya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <uc8:RequiredTextBox ID="IraIrat_Targy_RequiredTextBox" runat="server" Width="95%"
                                                                TextBoxMode="MultiLine" Rows="2"></uc8:RequiredTextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short" width="0%">
                                                            <%--<asp:Label ID="Label38" runat="server" Text="Irat kategória:"></asp:Label>--%>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="Label_Pld_IratTipusCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <asp:Label ID="Label18" runat="server" Text="Irat típus:"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>

                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <%--<ktddl:KodtarakDropDownList ID="IraIrat_Kategoria_KodtarakDropDownList" runat="server">
                                                </ktddl:KodtarakDropDownList>--%>
                                                            <ktddl:KodtarakDropDownList ID="IraIrat_Irattipus_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_elosztoiv" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label15" runat="server" Text="Elosztóív:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc16:ElosztoivTextBox ID="ElosztoivTextBox" runat="server" Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_Kiadmanyozas" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:CheckBox ID="IraIrat_KiadmanyozniKell_CheckBox" runat="server" Text="Kiadmányozni kell"></asp:CheckBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Kiadmanyozo" runat="server" Text="Kiadmányozó:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <uc15:FelhasznaloCsoportTextBox ID="IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox"
                                                                runat="server" ReadOnly="true" Validate="false" ViewMode="true"></uc15:FelhasznaloCsoportTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratMinosites">
                                                        <td class="mrUrlapCaption_short">
                                                            <%--BLG_1053--%>
                                                            <%-- <asp:Label ID="labelIratMinosites" runat="server" Text="Kezelési utasítások"></asp:Label>--%>
                                                            <asp:Label ID="labelIratMinosites" runat="server" Text="<%$Forditas:labelIratMinosites|Irat minősítése:%>"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="ktDropDownListIratMinosites" runat="server" Width="97%" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:UpdatePanel ID="StarUpdate1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="Label_UgyintezoCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <%--BLG_1014--%>
                                                                    <%--<asp:Label ID="Label133" runat="server" Text="Ügyintéző:"></asp:Label>--%>
                                                                    <asp:Label ID="labelIratUgyintezo" runat="server" Text="<%$Forditas:labelIratUgyintezo|Ügyintéző:%>"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc15:FelhasznaloCsoportTextBox ID="Irat_Ugyintezo_FelhasznaloCsoportTextBox" runat="server"
                                                                Validate="false" />
                                                        </td>
                                                    </tr>

                                                    <%--BLG#1402 START--%>
                                                    <tr id="trTUKMinositoEsMinositesErvIdo" class="urlapSor" runat="server" visible="false">
                                                        <td class="mrUrlapCaption_middle">
                                                            <asp:Label ID="LabelMinositesErvenyessegIdeje" runat="server" Text="Min érv. ideje:" />
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo">
                                                            <cc:CalendarControl ID="CalendarControlMinositesErvenyessegIdeje" runat="server" Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="LabelMinositoSzervezet" runat="server" Text="Minősítő szervezet:" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="LabelMinosito" runat="server" Text="Minősítő:" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ucMP:MinositoPartnerControl ID="MinositoPartnerControlIraIratokFormTab" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <%--BLG#1402 STOP--%>

                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelAdathordozoTipusa">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_AdathordozoReqStar" runat="server" Text="*" CssClass="ReqStar" Visible="false"></asp:Label>
                                                            <asp:Label ID="Label_AdathordozoTipusa" runat="server" Text="Adathordozó típusa:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td id="Td1" class="mrUrlapMezo" runat="server" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="AdathordozoTipusa_KodtarakDropDownList" runat="server" Width="97%" ReadOnly="true" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label19" runat="server" Text="Elsődleges adathordozó típusa:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" id="valami" runat="server" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Pld_UgyintezesModja_KodtarakDropDownList" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="trIratPanelIktatoTD" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label103" runat="server" Text="Iktató:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" id="Iktato" runat="server" width="0%">
                                                            <uc15:FelhasznaloCsoportTextBox ID="Iktato_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label190" runat="server" Text="Irat elintézési időpontja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="IntezesIdopontja_CalendarControl" runat="server" Visible="true"
                                                                Validate="false" PopupPosition="TopLeft" TimeVisible="true" ReadOnly="true" />
                                                        </td>
                                                    </tr>
                                                    <%--BLG_1020--%>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratIntezesiIdo" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label8" runat="server" Text="Irat ügyintézési ideje:" />
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <input id="inputUgyintezesiNapok" runat="server"
                                                                list='datalistUgyintezesiNapok'
                                                                name="inputNameUgyintezesiNapok"
                                                                min="0" max="10000000"
                                                                style="width: 150px;" autopostback="true" />
                                                            <datalist id="datalistUgyintezesiNapok" runat="server">
                                                            </datalist>
                                                            <%--<ktddl:KodtarakDropDownList ID="IratIntezesiIdo_KodtarakDropDownList" runat="server" CssClass="" />--%>
                                                            <ktddl:KodtarakDropDownList ID="IratIntezesiIdoegyseg_KodtarakDropDownList" runat="server" CssClass="" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label11" runat="server" Text="Ugyintézés kezdő dátuma:" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <cc:CalendarControl ID="CalendarControl_UgyintezesKezdoDatuma" runat="server" Visible="true"
                                                                Validate="false" PopupPosition="TopLeft" TimeVisible="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_IratHatarido" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">&nbsp;<asp:Label ID="labelIratHatarido" runat="server" Text="Irat ügyintézési határideje:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="IraIrat_Hatarido_CalendarControl" runat="server" Visible="true"
                                                                Validate="false" PopupPosition="TopLeft" TimeVisible="true" />
                                                            <%-- az alszámra iktatásnál a határidő módosulásának figyeléséhez --%>
                                                            <asp:HiddenField ID="UgyiratHataridoKitolas_Irat_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="IktatasDatuma_Irat_HiddenField" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelKiadmanyozasDatuma" runat="server" Text="Kiadmányozás dátuma:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <cc:CalendarControl ID="ccKiadmanyozasDatuma" runat="server" Validate="false" ReadOnly="true" TimeVisible="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_EgyebMuvelet" class="urlapSor_kicsi" runat="server" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_EgyebMuvelet" runat="server" Text="Egyéb mûvelet:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:RadioButtonList ID="RadioButtonList_BejovoIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;b&#243;l kivesz</asp:ListItem>
                                                                <asp:ListItem Value="2">Lez&#225;r</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:RadioButtonList ID="RadioButtonList_BelsoIratIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;ba helyez</asp:ListItem>
                                                                <asp:ListItem Value="2">Elint&#233;z</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1"></td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelIratJellege">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelIratJelleg" runat="server" Text="Irat jellege:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="IratJellegKodtarakDropDown" runat="server" ReadOnly="true" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <%--BLG_619--%>
                                                            <div style="margin-top: 5px;" />
                                                            <asp:Label ID="labelFelelosSzervezet" runat="server" Text="<%$Forditas:labelFelelosSzervezet|Felelős szervezet:%>"></asp:Label><br />
                                                            <div style="margin-top: 10px;" />
                                                            <asp:Label runat="server" ID="LabelFelelosSzervezeteVezeto" Text="Felelős szervezet vezetője" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <%--BLG_619--%>
                                                            <uc5:CsoportTextBox ID="Iratfelelos_CsoportTextBox" SzervezetCsoport="true" Validate="false"
                                                                runat="server" />
                                                            <%--BLG_90--%>
                                                            <fszvez:FelelosSzervezetVezetoTextBox runat="server" ID="FelelosSzervezetVezetoTextBoxInstance" />
                                                        </td>
                                                    </tr>

                                                    <%-- bernat.laszlo added --%>
                                                    <tr class="urlapSor_kicsi" id="TR_Rendszeradatok" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label150" runat="server" Text="Iktató munkaállomás:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:TextBox ID="Ikt_Munkaallomas_TextBox" runat="server" CssClass="mrUrlapInput"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </td>

                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label151" runat="server" Text="Ügyintézés módja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Ugyint_modja_Dropdown" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="TR1" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label160" runat="server" Text="Irat iránya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Irat_Iranya_DropDownList" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                        <%--// CR 3110 : Munkapéldány kiválasztási lehetőség BOPMH-ban--%>
                                                        <%--Nem ezt használjuk--%>
                                                        <td class="mrUrlapCaption">
                                                            <%-- <asp:Label ID="Label_Munkapeldany" runat="server" Text="Iktatás előkészítés (munkapéldány):" Visible="false"></asp:Label>--%>
                                                            <asp:Label ID="LabelIratHatasaUgyintezesre" runat="server"
                                                                Text="<%$Forditas:LabelIratHatasaUgyintezesre|Irat hatása az ügyintézésre (sakkóra)%>" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <%--<asp:CheckBox ID="cb_Munkapeldany" runat="server" Text="" CssClass="mrUrlapInput" Visible="false" />--%>
                                                            <asp:TextBox ID="TextBox_IratHatasaUgyintezesre_Aktualis" runat="server" Enabled="false" ReadOnly="true" Width="95%" />
                                                            <ktddl:KodtarakDropDownList ID="IratHatasaUgyintezesre_KodtarakDropDownList" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" Width="95%" />
                                                        </td>
                                                    </tr>
                                                    <%-- bernat.laszlo eddig --%>

                                                    <tr class="urlapSor_kicsi" id="TR_UgyintezesiNapok_EljarasiSzakaszFok" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_EljarasiSzakaszFok" runat="server" Text="Eljárási szakasz:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" style="width: 0%"></td>
                                                        <td class="mrUrlapMezo" style="width: 0%">
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_EljarasiSzakaszFok" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" OnSelectedIndexChanged="KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_HivatkozasiSzam_Irat" runat="server" Text="Hivatkozási szám:" Visible="false"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:TextBox ID="HivatkozasiSzam_Irat_TextBox" runat="server" CssClass="mrUrlapInput" Visible="false"></asp:TextBox>
                                                        </td>
                                                        <%--LZS - BUG_10419--%>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" style="width: 0%" colspan="1"></td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="TR2" runat="server">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>

                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="LabelEljarasFefuggesztesenekOka" runat="server" Text="<%$Forditas:LabelEljarasFefuggesztesenekOka|Eljárás felfüggesztésének, szünetelésének oka%>"
                                                                Visible="false" />
                                                            <asp:Label ID="LabelEljarasLezarasOka" runat="server" Text="<%$Forditas:LabelEljarasLezarasOka|Eljárás lezárásának oka%>"
                                                                Visible="false" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownListUgyiratFelfuggesztesOka" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" Visible="false" />
                                                            <asp:TextBox runat="server" ID="TextBoxFelfuggesztesOka" CssClass="mrUrlapInput" AutoPostBack="true" Visible="false" />
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownListUgyiratLezarasOka" runat="server" CssClass="mrUrlapInput" Visible="false" />
                                                        </td>
                                                    </tr>

                                                    <tr class="urlapSor_kicsi" id="TR3" runat="server">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>

                                                        <td class="mrUrlapCaption"></td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1"></td>
                                                    </tr>

                                                    <tr id="trMellekletekPanel" runat="server" visible="false">
                                                        <td colspan="5">
                                                            <uc:MellekletekPanel ID="mellekletekPanel" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <%--BLG#1402 START--%>
                                                    <tr id="trTerjedelemMainPanel" runat="server" visible="false">
                                                        <td colspan="5" style="padding-top: 5px;">
                                                            <uc15:TerjedelemPanel ID="TerjedelemPanelA" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <%--BLG#1402 STOP--%>

                                                    <tr id="tr_kezelesifeljegyzes" runat="server">
                                                        <td colspan="5">
                                                            <div style="padding-top: 2px;">
                                                                <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" Collapsed="true" RenderMode="Panel" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="StandardTargyszavakPanel_Iratok" runat="server" Visible="true">
                                            <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak_Iratok" RepeatColumns="2" RepeatDirection="Horizontal"
                                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="TipusosTargyszavakPanel_Iratok" runat="server" Visible="true">
                                            <otp:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_Iratok" RepeatColumns="2" RepeatDirection="Horizontal"
                                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                            <otp:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_IratokPostazasIranya" RepeatColumns="2" RepeatDirection="Horizontal"
                                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="IratPeldanyPanel" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tbody>
                                                    <tr class="urlapSor_kicsi" id="IratPeldanyPanel_CimzettTR" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <div class="DisableWrap">
                                                                <asp:Label ID="Label53" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label17" runat="server" Text="Címzett neve:"></asp:Label>
                                                            </div>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" />
                                                            <%--<uc2:PartnerTextBox ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" WithAllCimCheckBoxVisible="false"></uc2:PartnerTextBox>--%>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Pld_KuldesMod_ReqLabel" runat="server" Text="*" CssClass="ReqStar" Visible="true"></asp:Label>
                                                            <asp:Label ID="Label5" runat="server" Text="Expediálás módja:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Pld_KuldesMod_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="label36" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="IratPeldanyPanel_CimzettCimeTR" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Pld_CimzettCime_ReqStar" runat="server" Text="*" CssClass="ReqStar" Visible="false"></asp:Label>
                                                            <asp:Label ID="Label_Pld_CimzettCime" runat="server" Text="Címzett címe:"></asp:Label>
                                                            <asp:Label ID="Label_Email_CimzettEmailCime" runat="server" Text="Címzett e-mail címe:"
                                                                Visible="false"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc3:CimekTextBox ID="Pld_CimId_Cimzett_CimekTextBox" runat="server" Validate="false" LabelRequiredIndicatorID="Label_Pld_CimzettCime_ReqStar" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <%--<asp:Label ID="Label18" runat="server" Text="Továbbító:"></asp:Label>--%>
                                                            <asp:Label ID="Label16" runat="server" Text="Elvárás:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <ktddl:KodtarakDropDownList ID="Pld_Visszavarolag_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            <%--<ktddl:KodtarakDropDownList ID="Pld_Tovabbito_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>--%>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPeldanyPanelVonalkod">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Pld_IratpeldanyFajtajaReq" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="Label_Pld_IratpeldanyFajtaja" runat="server" Text="Iratpéldány fajtája:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="Pld_IratpeldanyFajtaja_KodtarakDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Pld_IratpeldanyFajtaja_KodtarakDropDownList_SelectedIndexChanged" Validate="True" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:UpdatePanel ID="starUpdate" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="Label_Pld_VonalkodElottCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <asp:Label ID="Label_Pld_Vonalkod" runat="server" Text="Vonalkód:"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <asp:Label ID="Label_Email_CimzettSzervezete" runat="server" Text="Címzett szervezete:"
                                                                Visible="false"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc10:VonalKodTextBox ID="Pld_BarCode_VonalKodTextBox" runat="server" Validate="true" />
                                                            <uc2:PartnerTextBox ID="Email_CimzettSzervezete_PartnerTextBox" runat="server" ReadOnly="true"
                                                                ViewMode="true" Visible="false"></uc2:PartnerTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" id="IratPeldanyPanel_HataridoTR" runat="server">
                                                        <td class="mrUrlapCaption_short">&nbsp;<asp:Label ID="Label23" runat="server" Text="Elvárt határidő:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="Pld_VisszaerkezesiHatarido_CalendarControl" runat="server"
                                                                Validate="false" PopupPosition="TopLeft"></cc:CalendarControl>
                                                        </td>
                                                        <td colspan="2">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-right: 20px">
                                                                            <asp:CheckBox ID="UgyiratpeldanySzukseges_CheckBox" runat="server" Text="Ügyiratpéldány szükséges"></asp:CheckBox>
                                                                        </td>
                                                                        <td>

                                                                            <%--BLG_350--%>
                                                                            <%--<asp:CheckBox ID="KeszitoPeldanya_CheckBox" runat="server" Text="Készítő példánya">
                                                                            </asp:CheckBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <asp:Panel runat="server" ID="PanelTobbszorosIratPeldany" Visible="false">
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_2" Visible="false" SorSzam="2" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_3" Visible="false" SorSzam="3" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_4" Visible="false" SorSzam="4" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_5" Visible="false" SorSzam="5" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_6" Visible="false" SorSzam="6" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_7" Visible="false" SorSzam="7" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_8" Visible="false" SorSzam="8" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_9" Visible="false" SorSzam="9" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_10" Visible="false" SorSzam="10" EnableViewState="true" />
                                                <cc:PldIratIktatasPanelUserControl runat="server" ID="PldIratIktatasPanelUserControl_11" Visible="false" SorSzam="11" EnableViewState="true" />
                                                <asp:HiddenField runat="server" ID="HiddenFieldTobbszorosIratPeldanyUtolsoSorszam" Value="1" />
                                                <asp:ImageButton runat="server" ID="ImageButtonTobbszorosIratPeldany"
                                                    ImageUrl="~/images/hu/muvgomb/iratpeldanyletrehozas.jpg"
                                                    OnClick="ImageButtonTobbszorosIratPeldany_Click" CausesValidation="false" />
                                                <br />
                                            </asp:Panel>
                                        </eUI:eFormPanel>
                                        <uc:BejovoPeldanyListPanel ID="BejovoPeldanyListPanel1" runat="server" Visible="false" />
                                        <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" CssClass="mrResultPanel">
                                            <div class="mrResultPanelText">
                                                <asp:Label ID="Label_ResultPanelText" CssClass="mrResultPanelText" runat="server" Text="<%$Resources:Form,IktatasResultText %>" />
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
                                                <tr class="urlapSorBigSize">
                                                    <td class="mrUrlapCaptionBigSize">
                                                        <asp:Label ID="labelUgyirat" runat="server" Text="Ügyirat iktatószáma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoBigSize">
                                                        <asp:Label ID="labelUgyiratFoszam" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px">
                                                        <asp:ImageButton ID="imgUgyiratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                            onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                    </td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                                        <span style="padding-right: 100px">
                                                            <asp:ImageButton ID="imgUgyiratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                            <asp:ImageButton ID="imgEloadoiIv" runat="server" ImageUrl="~/images/hu/trapezgomb/eloadoi_iv_trap.jpg"
                                                                onmouseover="swapByName(this.id,'eloadoi_iv_trap2.jpg');" onmouseout="swapByName(this.id,'eloadoi_iv_trap.jpg');" />
                                                            <!--CR 3058-->
                                                            <asp:ImageButton ID="ImageButtonVonalkodNyomtatasUgyirat" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                                onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />
                                                            <!--CR3140-->
                                                            <asp:ImageButton ID="Atadasra_kijelolesUgyirat" runat="server" ImageUrl="~/images/hu/trapezgomb/atadas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'atadas_trap2.jpg');" onmouseout="swapByName(this.id,'atadas_trap.jpg');" />
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                                    <td class="mrUrlapCaptionBigSize">
                                                        <asp:Label ID="labelIrat" runat="server" Text="Irat iktatószáma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoBigSize">
                                                        <asp:Label ID="labelIratFoszam" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px">
                                                        <asp:ImageButton ID="imgIratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                            onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                    </td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px;">
                                                        <span style="padding-right: 80px">
                                                            <asp:ImageButton ID="imgIratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                            <asp:ImageButton ID="imgIratModositHatosagi" runat="server" ImageUrl="~/images/hu/trapezgomb/hatosagiadatok_trap.jpg"
                                                                onmouseover="swapByName(this.id,'hatosagiadatok_trap2.jpg');" onmouseout="swapByName(this.id,'hatosagiadatok_trap.jpg');" />
                                                            <asp:ImageButton ID="imgeMailNyomtatas" runat="server" ImageUrl="~/images/hu/trapezgomb/email_nyomtatas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'email_nyomtatas_trap2.jpg');" onmouseout="swapByName(this.id,'email_nyomtatas_trap.jpg');" />
                                                            <!--CR3058-->
                                                            <asp:ImageButton ID="ImageButtonVonalkodNyomtatasIrat" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                                onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />
                                                            <!--CR3153-->
                                                            <asp:ImageButton ID="Atadasra_kijelolesIrat" runat="server" ImageUrl="~/images/hu/trapezgomb/atadas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'atadas_trap2.jpg');" onmouseout="swapByName(this.id,'atadas_trap.jpg');" />
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr id="tr_resultPanel_UgyiratUjHatarido" runat="server" class="urlapSor" style="padding-bottom: 20px"
                                                    visible="false">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelUgyiratUjHatarido" runat="server" Text="Ügyirat új határidő:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:Label ID="labelResultUgyiratUjHatarido" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px"></td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                                </tr>
                                                <tr id="tr_resultPanel_UgyFelelos" runat="server" class="urlapSor" style="padding-bottom: 20px"
                                                    visible="false">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label13" runat="server" Text="Felelős:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc15:FelhasznaloCsoportTextBox ID="UgyFelelos_resultPanel_FelhasznaloCsoportTextBox"
                                                            runat="server" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px"></td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                                </tr>
                                                <tr id="tr_resultPanel_Ugyintezo" runat="server" class="urlapSor" style="padding-bottom: 20px"
                                                    visible="false">
                                                    <td class="mrUrlapCaption">
                                                        <%--BLG_1014--%>
                                                        <%--<asp:Label ID="Label11" runat="server" Text="Ügyintéző:"></asp:Label>--%>
                                                        <asp:Label ID="resultPanel_UgyiratUgyintezo" runat="server" Text="<%$Forditas:resultPanel_UgyiratUgyintezo|Ügyintéző:%>"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc15:FelhasznaloCsoportTextBox ID="Ugyintezo_resultPanel_FelhasznaloCsoportTextBox"
                                                            runat="server" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px"></td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                                </tr>
                                                <tr id="tr_resultPanel_kovFelelos" runat="server" visible="false" class="urlapSor"
                                                    style="padding-bottom: 20px">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="Label10" runat="server" Text="Az ügyirat következő kezelője:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc5:CsoportTextBox ID="KovFelelos_Ugyirat_result_CsoportTextBox" runat="server"
                                                            ReadOnly="true" Validate="false" ViewMode="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px"></td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                                </tr>
                                            </table>
                                            <div class="mrResultPanelText" runat="server" id="divSzerelesResult" visible="false">
                                                <asp:Label ID="labelSzerelesResult" CssClass="mrResultPanelText" runat="server" Text="A szerelés sikeresen véghajtódott!" />
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable" runat="server" id="tableSzereltUgyirat" visible="false">
                                                <tr class="urlapSorBigSize" runat="server" id="trSzereltUgyirat" visible="false">
                                                    <td class="mrUrlapCaptionBigSize">
                                                        <asp:Label ID="labelSzereltUgyirat" runat="server" CssClass="DisableWrap" Text="Előzmény iktatószáma:" />
                                                    </td>
                                                    <td class="mrUrlapMezoBigSize">
                                                        <asp:Label ID="labelSzereltUgyiratFoszam" runat="server" Text="" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor" runat="server" id="trSzerelesHiba" visible="false">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelSzerelesHiba" runat="server" Text="Hibaüzenet:" />
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:Label ID="labelSzerelesHibaUzenet" runat="server" CssClass="hibaBody" Text="" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="ResultPanel_KuldemenyElokeszites" runat="server" Visible="false"
                                            CssClass="mrResultPanel">
                                            <div class="mrResultPanelText">
                                                A küldemény iktatási adatainak rögzítése megtörtént.
                                                <br />
                                                A küldemény még nem került iktatásra.
                                                <br />
                                                <br />
                                                <%--A küldemény továbbításra került a szervezeti egység vezetőjének.--%>
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
                                                <tr class="urlapSor" style="padding-bottom: 20px;">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="label_result_kuldKovFelelos" runat="server" Text="A küldemény továbbításra került ide:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc5:CsoportTextBox ID="Kuld_KovFelelos_CsoportTextBox" runat="server" ViewMode="true"
                                                            ReadOnly="true" Validate="false"></uc5:CsoportTextBox>
                                                    </td>
                                                    <td></td>
                                                    <td style="width: 100%"></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr class="urlapSor" style="padding-bottom: 20px;">
                                                    <td class="mrUrlapCaption" style="padding-right: 20px;">
                                                        <asp:Label ID="labelKuldemeny" runat="server" Text="Küldemény megtekintése/módosítása:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                                        <asp:ImageButton ID="imgKuldemenyMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                            onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        <asp:ImageButton ID="imgKuldemenyModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                            onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                    </td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </eUI:eFormPanel>
                                    </td>
                                </tr>
                                <tr class="urlapSor_kicsi">
                                    <%--<td class="mrUrlapCaption_short" valign="top"></td>--%>
                                    <td class="mrUrlapMezo">
                                        <uc7:TabFooter ID="TabFooter1" runat="server" />
                                        <asp:Button ID="ForceSaveButton" runat="server" Style="display: none" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td valign="top" style="width: 100px">
                        <fgs:FunkcioGombsor ID="FunkcioGombsor" runat="server" />
                    </td>
                    <td style="width: 30px">&nbsp</td>
                </tr>
            </table>


        </asp:Panel>

        <script type="text/javascript">
            // a módosítás után ennek a gombnak a clickjét újra kell hívni
            var btnSaveToClick;

            function qmp_show(bUgyiratModosithato, bIratModosithato) {
                var rblChoices = document.getElementById('<%=rblChoices.ClientID %>');
                var radioArray = rblChoices.getElementsByTagName('input');

                if (radioArray && radioArray.length > 0) {
                    bCheckedUndisplayed = false;
                    for (var i = 0; i < radioArray.length; i++) {
                        if (!bUgyiratModosithato) {
                            if (radioArray[i].value == "UgyiratHataridoToIrat") {
                                radioArray[i].parentElement.style.display = 'none';
                                if (radioArray[i].checked) { bCheckedUndisplayed = true; }
                            }
                        }
                        else {
                            if (radioArray[i].value == "UgyiratHataridoToIrat") {
                                radioArray[i].parentElement.style.display = '';
                            }
                        }

                        if (!bIratModosithato) {
                            if (radioArray[i].value == "IratHataridoToUgyirat") {
                                radioArray[i].parentElement.style.display = 'none';
                                if (radioArray[i].checked) { bCheckedUndisplayed = true; }
                            }
                        }
                        else {
                            if (radioArray[i].value == "IratHataridoToUgyirat") {
                                radioArray[i].parentElement.style.display = '';
                            }
                        }
                    }
                    if (bCheckedUndisplayed) {
                        radioArray[radioArray.length - 1].checked = true;
                    }

                }


                $find('<%=mdeQuestion.ClientID %>').show();
                return false;
            }

            function qmp_onContinue() {
                var bReturnValue = qmp_setHatarido();
                $find('<%=mdeQuestion.ClientID %>').hide();
                if (bReturnValue && btnSaveToClick) {
                    btnSaveToClick.click();
                }
                return false;
            }

            function qmp_onCancel() {
                $find('<%=mdeQuestion.ClientID %>').hide();
                return false;
            }

            function qmp_setUgyiratHataridoToIrat() {
                var ugyiratHataridoTextBox = $get('<%=UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID %>');
                var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                if (ugyiratHataridoTextBox && iratHataridoTextBox) {
                    ugyiratHataridoTextBox.value = iratHataridoTextBox.value;
                }
                return true;
            }

            function qmp_setIratHataridoToUgyirat() {
                var ugyiratHataridoTextBox = $get('<%=UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID %>');
                var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                if (ugyiratHataridoTextBox && iratHataridoTextBox) {
                    iratHataridoTextBox.value = ugyiratHataridoTextBox.value;
                }
                if (iratIntezesiHataridoHourTextBox) {
                    iratIntezesiHataridoHourTextBox.value = "00";
                }
                if (iratIntezesiHataridoMinuteTextBox) {
                    iratIntezesiHataridoMinuteTextBox.value = "00";
                }
                return true;
            }

            function qmp_clearIratHatarido() {
                var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                if (iratHataridoTextBox) {
                    iratHataridoTextBox.value = "";
                }
                if (iratIntezesiHataridoHourTextBox) {
                    iratIntezesiHataridoHourTextBox.value = "00";
                }
                if (iratIntezesiHataridoMinuteTextBox) {
                    iratIntezesiHataridoMinuteTextBox.value = "00";
                }
                return true;
            }

            function qmp_setHatarido() {
                var rblChoices = document.getElementById('<%=rblChoices.ClientID %>');
                var radioArray = rblChoices.getElementsByTagName('input');

                if (radioArray && radioArray.length > 0) {
                    var selectedExpr = "";
                    for (var i = 0; i < radioArray.length; i++) {
                        if (radioArray[i].checked) {
                            selectedExpr = radioArray[i].value;
                        }
                    }
                    if (selectedExpr == 'IratHataridoToUgyirat') {
                        return qmp_setIratHataridoToUgyirat();
                    }
                    else if (selectedExpr == 'UgyiratHataridoToIrat') {
                        return qmp_setUgyiratHataridoToIrat();
                    }
                    else if (selectedExpr == 'ClearIratHatarido') {
                        return qmp_clearIratHatarido();
                    }
                    else if (selectedExpr == 'BackToForm') {
                        return false;
                    }
                }
                return false;
            }

        </script>

        <asp:Panel ID="pnlQuestion" runat="server" CssClass="emp_Panel" Style="display: none">
            <div style="padding: 8px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            <h2 id="header" class="emp_HeaderWrapper">
                                <asp:Label ID="labelHeader" runat="server" Text="Az ügyirat határideje korábbi, mint az iraté. Mi történjen?"
                                    CssClass="emp_Header"></asp:Label>
                            </h2>
                            <br />
                            <asp:RadioButtonList ID="rblChoices" runat="server">
                                <asp:ListItem Value="UgyiratHataridoToIrat" Selected="true" Text="Ügyirat határidő kitolása az irat határidőhöz és mentés" />
                                <asp:ListItem Value="IratHataridoToUgyirat" Selected="false" Text="Irat határidő igazítása az ügyirat határidőhöz és mentés" />
                                <asp:ListItem Value="ClearIratHatarido" Selected="false" Text="Irat határidő törlése és mentés" />
                                <asp:ListItem Value="BackToForm" Selected="false" Text="Visszatérés az ûrlaphoz, kézi módosítás" />
                            </asp:RadioButtonList>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <asp:ImageButton ID="btnContinue" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" />
                        </td>
                    </tr>
                </table>
                <%--        <asp:Button CssClass="emp_OkButton" ID="btnContinue" runat="server" CausesValidation="false" 
            Text="Mentés a fentiek szerint"/> --%>
                <%--        <asp:Button CssClass="emp_CancelButton" ID="btnCancel" runat="server" CausesValidation="false" 
            Text="Vissza az ûrlaphoz"/>--%>
                <%-- Erre csak azért van szükség, hogy meg tudjunk adni egy TargetControlID-t a ModalPopupExtender-nek --%>
                <asp:Button ID="btnHidden" runat="server" Style="display: none" CausesValidation="false" />
            </div>
        </asp:Panel>
        <asp:HiddenField ID="SzignalasTipusa" runat="server" />
        <asp:HiddenField ID="JavasoltUgyintezoId" runat="server" />
        <asp:HiddenField ID="HataridosFeladatId" runat="server" />
        <ajaxToolkit:ModalPopupExtender ID="mdeQuestion" runat="server" TargetControlID="btnHidden"
            PopupControlID="pnlQuestion" OkControlID="btnContinue" BackgroundCssClass="qmp_modalBackground"
            OnOkScript="qmp_onContinue()" />
        <%--        CancelControlID="btnCancel" OnCancelScript="qmp_onCancel()"--%>

        <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
        <script type="text/javascript">

            var onlyPeldanyReload = '0';

            Sys.Application.add_load(function () {
                  <%if (isTUKRendszer && Command == Contentum.eRecord.Utility.CommandName.New)
            { %>                
              <%  if (Mode == Contentum.eRecord.Utility.CommandName.BelsoIratIktatas)
            { %>

                try {

                    $('#<%=Ugyfelelos_CsoportTextBox.HiddenField.ClientID%>').on('change', function () {

                            loadFizikaihelyek();
                        });

                        $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>').on('change', function () {
                            var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');

                        var dVal = dropdown.val();
                        var dText = dropdown.find('option:selected').text();
                        if (dVal != undefined && dText != undefined) {
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                            }
                            //$('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_HiddenFieldFizikaiHely').val(event.target.options[event.target.selectedIndex].value);
                        });

                        if (!$('#<%= IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ClientID %>').is(':disabled')) {
                            $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ClientID %>').on('change', function () {
                            var dropdown = $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ClientID %>');

                            var dVal = dropdown.val();
                            var dText = dropdown.find('option:selected').text();
                            if (dVal != undefined && dText != undefined) {
                                $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                                $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                                }
                                //$('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_HiddenFieldFizikaiHely').val(event.target.options[event.target.selectedIndex].value);
                            });
                        }
                        loadFizikaihelyek();

                        $('#<%= Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID%>').on('change', function () {
                        //'#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Pld_KuldesMod_KodtarakDropDownList_Kodtarak_DropDownList'
                        onlyPeldanyReload = "1";
                        loadFizikaihelyek();
                    });

                } catch (e) {
                    //console.log(e);
                }
                 <%} %>
                 <%} %>
            });

            function loadFizikaihelyek() {
                var inPartnerId = $('#<%=Ugyfelelos_CsoportTextBox.HiddenField.ClientID%>').val();
                if (inPartnerId === null || inPartnerId === '' || inPartnerId === undefined || inPartnerId === 'undefined') {
                    inPartnerId = '1';
                }
                try {
                    var fel = $("[id$=HiddenField_Svc_FelhasznaloId]").val();
                    var szerv = $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val();
                    if (fel != undefined || szerv != undefined) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFizikaiHelyek") %>',
                            data: JSON.stringify({
                                partnerId: inPartnerId,
                                contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ';' + $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val()
                            }),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (onlyPeldanyReload == "0") {
                                    fillFizikaihelyekUgyirat(data);
                                }
                                fillFizikaihelyek(data);
                                onlyPeldanyReload = "0";

                            },
                            error: function (response) {
                                onlyPeldanyReload = "0";
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                onlyPeldanyReload = "0";
                                alert(response.responseText);
                            }
                        });
                    }
                } catch (e) {
                    //console.log(e);
                }
                //}
                //else {
                //    //console.log('partner id nem stimmel');
                //}
            }

            function fillFizikaihelyek(data) {
                //console.log('data');
                var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');
                var expDropdownVal = $('#<%= Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID%>').val();;

                if (expDropdownVal != '110') {
                    //var dropdown = $("#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_IrattariHelyLevelekDropDownTUK_IrattarLevelekDropdownList");
                    if (dropdown != null) {
                        dropdown.empty();
                        dropdown.html('<option value="' + '4f18bb74-aec8-e911-80df-00155d017b07' + '">' + 'Címzettnél' + '</option>');
                        $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dropdown.val());
                        $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dropdown.text());
                        return;
                        //dropdown.val("Címzettnél");

                    }
                }

                if (dropdown != null) {
                    var dVal = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val();
                    //var dText = dropdown.find('option:selected').text();
                    var dText = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val();
                    dropdown.empty();
                    dropdown.html('<option value="' + '' + '">' + '[Nincs megadva elem]' + '</option>');
                    //<option selected="selected" value="">[Nincs megadva elem]</option>

                    if (data === null || data.d === null) {
                        return;
                    }
                    if (dropdown !== null) {
                        var k = JSON.parse(data.d);

                        var s = '';
                        for (var i = 0; i < k.length; i++) {
                            s += '<option value="' + k[i].Id + '">' + k[i].Value + '</option>';
                        }
                        dropdown.html(s);

                        if (dVal != undefined && dText != undefined) {
                            dropdown.val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                        }
                        //if (k.length == 1) {
                        //    dropdown.
                        //}

                    }
                }
            }

            function fillFizikaihelyekUgyirat(data) {
                //console.log('data');
                if (!$('#<%= IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ClientID %>').is(':disabled')) {
                    var dropdown = $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ClientID %>');

                    if (dropdown != null) {
                        var dVal = $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyIdHiddenField.ClientID%>').val();
                        //var dText = dropdown.find('option:selected').text();
                        var dText = $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyTextHiddenField.ClientID%>').val();
                        dropdown.empty();
                        dropdown.html('<option value="' + '' + '">' + '[Nincs megadva elem]' + '</option>');
                        //<option selected="selected" value="">[Nincs megadva elem]</option>

                        if (data === null || data.d === null) {
                            return;
                        }
                        if (dropdown !== null) {
                            var k = JSON.parse(data.d);

                            var s = '';
                            for (var i = 0; i < k.length; i++) {
                                s += '<option value="' + k[i].Id + '">' + k[i].Value + '</option>';
                            }
                            dropdown.html(s);

                            if (dVal != undefined && dText != undefined) {
                                dropdown.val(dVal);
                                $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                                $('#<%= IrattariHelyLevelekDropDownTUKUgyirat.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                            }
                            //if (k.length == 1) {
                            //    dropdown.
                            //}

                        }
                    }
                }
            }

            function setIFFelelosSzervezetVezeto() {
                try {
                    var id = $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Iratfelelos_CsoportTextBox_HiddenField1').val();

                    if (id != '') {
                        if (typeof SetFelelosSzervezetVezeto !== 'undefined')
                            SetFelelosSzervezetVezeto(id, null);
                    }

                } catch (e) {
                    //console.log(e);
                }
            }

            $(document).ready(function () {
                try {
                    $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Iratfelelos_CsoportTextBox_HiddenField1').on('change', function () {
                        if (typeof setTimeout !== 'undefined' && typeof setIFFelelosSzervezetVezeto !== 'undefined')
                            setTimeout(setIFFelelosSzervezetVezeto, 1500);
                    });

                    $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Iratfelelos_CsoportTextBox_HiddenField1').on('change', function () {
                        if (typeof SetFelelosSzervezetVezeto !== 'undefined')
                            SetFelelosSzervezetVezeto($(this).val(), null);
                    });

                    //BUG_6042 Ügyintéző másolása autocomplete esetén is (Bejövő irat,Belső keletkezésű irat)
                    $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1').on('change', function () {
                        $("#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField1").val($("#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1").val());
                        $("#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Irat_Ugyintezo_FelhasznaloCsoportTextBox_CsoportMegnevezes").val($("#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_CsoportMegnevezes").val());
                    });

                    var Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_InitValue = $('#<%=Irat_Ugyintezo_FelhasznaloCsoportTextBox.HiddenField.ClientID%>').val();
                    var Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_InitValue = $('#<%=Irat_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.ClientID%>').val();

                    $('#<%=Iktatokonyvek_DropDownLis_Ajax.ClientID%>').on('change', function () {
                        setTimeout(function () {// Szükség van rá, mert különben nem frissül a háttérben a FelhasznloCsoportTextBox értéke és így Irat felelos neve se fogja követni a változtatást.

                            var isReadOnly = $('#<%=Irat_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.ClientID%>').prop('readonly');
                            var Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_NewValue = $("#<%=UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.HiddenField.ClientID%>").val();
                            var Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_NewValue = $("#<%=UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.TextBox.ClientID%>").val();

                            if (isReadOnly) {
                                if (Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_NewValue == '' ||
                                    Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_NewValue == '') {
                                    Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_NewValue = Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_InitValue;
                                    Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_NewValue = Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_InitValue;
                                }
                            }

                            $('#<%=Irat_Ugyintezo_FelhasznaloCsoportTextBox.HiddenField.ClientID%>').val(Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField_NewValue);
                            $('#<%=Irat_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.ClientID%>').val(Irat_Ugyintezo_FelhasznaloCsoportTextBox_TextBox_NewValue);
                        }, 200);
                    });
                    //BUG_4366
                    $(window).scrollTop(0);

                } catch (e) {
                    //console.log(e);
                }

            });
        </script>


    </ContentTemplate>

    <%-- <Triggers>
        <asp:PostBackTrigger ControlID="ImageButtonTobbszorosIratPeldany" />
    </Triggers>--%>
</asp:UpdatePanel>
