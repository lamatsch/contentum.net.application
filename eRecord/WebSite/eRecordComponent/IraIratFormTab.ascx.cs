﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using eAdminUtility = Contentum.eAdmin.Utility;
using Newtonsoft.Json;

public partial class eRecordComponent_IraIratFormTab : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    // BLG_350
    //#region CR#1284 iratpeldany nem szukseges
    //private const bool default_IratPeldanySzukseges = false;
    //#endregion

    #region Konstansok, Valtozok, Property-k

    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_TOVABBITO_SZERVEZET = "TOVABBITO_SZERVEZET";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_SURGOSSEG = "SURGOSSEG";
    //private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";
    private const string kcs_UGYTIPUS = "UGYTIPUS";
    //private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";
    private const string kcs_IRAT_JELLEG = "IRAT_JELLEG";
    //bernat.laszlo added
    private const string kcs_IRAT_UGYINT_MODJA = "IRAT_UGYINT_MODJA";
    private const string kcs_POSTAZAS_IRANYA = "POSTAZAS_IRANYA";
    //bernat.laszlo eddig
    private const string kcs_IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";
    private const string KCS_FELFUGGESZTESOKA = "UGYIRAT_FELFUGGESZTES_OKA";
    private const string kcs_UGYIRAT_INTEZESI_IDO = "UGYIRAT_INTEZESI_IDO";

    // BLG_44
    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
    // BLG_361
    private const string kcs_ELSODLEGES_ADATHORDOZO = "ELSODLEGES_ADATHORDOZO";

    private const string funkcio_BejovoIratIktatas = "BejovoIratIktatas";
    private const string funkcio_BelsoIratIktatas = "BelsoIratIktatas";
    private const string funkcio_BejovoIratIktatasCsakAlszamra = "BejovoIratIktatasCsakAlszamra";
    private const string funkcio_BelsoIratIktatasCsakAlszamra = "BelsoIratIktatasCsakAlszamra";
    private const string funkcio_MunkapeldanyBeiktatas_Bejovo = "MunkapeldanyBeiktatas_Bejovo";
    private const string funkcio_MunkapeldanyBeiktatas_Belso = "MunkapeldanyBeiktatas_Belso";
    private const string funkcio_IktatasElokeszites = "IktatasElokeszites";
    private const string funkcio_UgyiratSzereles = "UgyiratSzereles";

    private const string const_Iktatas = "Iktatas";
    private const string const_MunkapeldanyLetrehozas = "MunkapeldanyLetrehozas";


    public string Command = "";
    public string Mode = ""; // Pl. "BelsoIratIktatas"
    public string SubMode = String.Empty;

    //private const string migralasUrl = "/eMigration/FoszamList.aspx";

    private PageView pageView = null;

    private String ParentId = "";

    private string kuldemenyId = null;
    private string EmailBoritekokId = "";

    private String _ParentForm = "";

    private bool bUgyiratHataridoModositasiJog = false;
    public bool isTUKRendszer = false;
    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;
    public UpdatePanel FormHeaderUpdatePanel = null;

    // A szóban forgó irat objektum (megtekintés, módosítás esetén használatos)
    public EREC_IraIktatoKonyvek obj_Iktatokonyv = null;
    public EREC_IraIratok obj_Irat = null;
    public EREC_UgyUgyiratdarabok obj_UgyiratDarab = null;
    public EREC_UgyUgyiratok obj_Ugyirat = null;
    public EREC_PldIratPeldanyok obj_elsoIratPeldany = null;

    private Boolean _Load = false;


    // Az objektumban történõ változásokat jelzõ esemény, ami pl. kiválthat egy jogosultságellenõrzést
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    private IktatasiParameterek iktatasiParameterek = null;

    protected IktatasiParameterek IktatasiParameterek
    {
        get
        {
            if (iktatasiParameterek == null)
                iktatasiParameterek = new IktatasiParameterek();
            return iktatasiParameterek;
        }
        set { iktatasiParameterek = value; }
    }

    #endregion

    /// <summary>
    /// Lezárt iktatókönybe iktatás esetén az új ügyira ügyintézési határideje átmásolódjon-e, vaghy a default érték kerüljön beálíltásra
    /// </summary>
    private bool IntezesiIdoMasolas
    {
        get
        {
            return Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.DEAFULT_INTEZESI_IDO_MASOLAS, true);
        }
    }

    private bool Uj_Ugyirat_Hatarido_Kezeles
    {
        get
        {
            return Ugyiratok.Uj_Ugyirat_Hatarido_Kezeles(Page);
        }
    }

    private bool trUgyiratIntezesiIdoVisible
    {
        get
        {
            return trUgyiratIntezesiIdoLabel.Visible;
        }
        set
        {
            trUgyiratIntezesiIdoLabel.Visible = value;
            IntezesiIdo_KodtarakDropDownList.Visible = value;
            IntezesiIdoegyseg_KodtarakDropDownList.Visible = value;
        }
    }

    private bool IsIktatasBelsoUgyintezoIktato // BUG_8001
    {
        get
        {
            return Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_BELSO_UGYINTEZO_IKTATO, false);
        }
    }

    private bool IsTUK_SZIGNALAS_ENABLED // BUG_8765
    {
        get
        {
            return Rendszerparameterek.GetBoolean(Page, "TUK_SZIGNALAS_ENABLED", false);
        }
    }

    #region page

    private IratForm _iratForm;

    // funkciójogok ellenõrzése a fõ formon

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        _iratForm = new IratForm(this, pageView)
        {
            IsIrat = true,
            CalendarControl_UgyintezesKezdete = CalendarControl_UgyintezesKezdete,
            Label_CalendarControl_UgyintezesKezdete = Label_CalendarControl_UgyintezesKezdete,
        };

        Command = Request.QueryString.Get(QueryStringVars.Command);

        /// ha nincs megadva a Mode paraméter, akkor ha Command=New, BejovoIratIktatas-ként kezeljük
        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        InitSakkoraDateControls();

        if (Mode == CommandName.InfoszabIktatas)
        {
            SubMode = Mode;
            Mode = CommandName.BelsoIratIktatas;
        }

        if (String.IsNullOrEmpty(Mode) && Command == CommandName.New)
        {
            // default érték a BejovoIratIktatas
            Mode = CommandName.BejovoIratIktatas;
        }

        _iratForm.IsUgyintezesKezdeteVisible = (Command == CommandName.New && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.BelsoIratIktatas))
            || Command == CommandName.Modify || Command == CommandName.View; // BLG_8826

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        kuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
        EmailBoritekokId = Request.QueryString.Get(QueryStringVars.EmailBoritekokId);

        // alszámra iktatásnál jogosultság az ügyirat határidõ módosítására
        bUgyiratHataridoModositasiJog = FunctionRights.GetFunkcioJog(Page, "UgyiratModify");

        // iratpéldány címzett textbox-jához elosztóíveket is hozni kell
        Pld_PartnerId_Cimzett_PartnerTextBox.WithElosztoivek = true;

        //Ugy_PartnerId_Ugyindito_PartnerTextBox.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(80);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        // Szûrés megadása, hogy csak az iktatható küldeményeket hozza
        ImageButton_KuldemenyKereses.OnClientClick = GetKuldemenyKeresesOnClientClick();

        ImageButton_KuldemenyMegtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
             "KuldKuldemenyekForm.aspx", "", Kuldemeny_Id_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        #region Küldemény módosítás gomb

        string javascript = "";

        javascript = " var hidden = getElementById('" + Kuldemeny_Id_HiddenField.ClientID + "'); "
             + " if (hidden.value!=null && hidden.value!='') "
             + " { if (popup('KuldKuldemenyekForm.aspx?" + QueryStringVars.Id + "='+hidden.value"
             + "+'&" + CommandName.Command + "=" + CommandName.Modify + "', " + Defaults.PopupWidth_Max.ToString()
             + ", " + Defaults.PopupHeight_Max
             + " )){__doPostBack('" + ImageButton_KuldemenyModositas.ClientID + "','" + EventArgumentConst.refreshKuldemenyekPanel
             + "');} } else { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem") + "'); } "
             + " return false;";
        ImageButton_KuldemenyModositas.OnClientClick = javascript;

        #endregion

        ImageButton_KuldemenyErkeztetes.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx",
             QueryStringVars.Command + "=" + CommandName.New
             + "&" + QueryStringVars.HiddenFieldId + "=" + Kuldemeny_Id_HiddenField.ClientID
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, ImageButton_KuldemenyErkeztetes.ClientID, EventArgumentConst.refreshKuldemenyekPanel);


        ImageButton_Elozmeny.OnClientClick = GetElozmenyKeresesOnClientClick();

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
             "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
             "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        // másolva az IraIratUpdatePanel_Load-ba is (karbantartás !)
        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
             "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_MigraltKereses.OnClientClick = GetElozmenyKeresesOnClientClick();



        // Kimenõ email iktatásnál fel kell tenni az Iktatás/Munkapéldány létrehozás rádiógombot:

        if (Command == CommandName.New && Mode == CommandName.KimenoEmailIktatas)
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        // Jelenleg nem ezt a megoldást használjuk
        //if (Command == CommandName.New && ((Mode == CommandName.KimenoEmailIktatas) || (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)))
        {
            IktatasVagyMunkapeldanyLetrehozasRadioButtonList.Visible = true;

            // Munkapéldány létrehozás letiltva, ha nincs megfelelõ jogosultsága:
            IktatasVagyMunkapeldanyLetrehozasRadioButtonList.Items[1].Enabled =
                 FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);
        }


        if (Command == CommandName.New && Mode != CommandName.ElokeszitettIratIktatas)
        {
            #region CR956 fix, ügyirat tárgy ne másolódjon át irathoz
            //UgyUgyirat_Targy_RequiredTextBox.TextBox.Attributes["onblur"] +=
            //   @" var iratTargyObj = $get('" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID + @"');
            //    if (iratTargyObj.value == '') 
            //    {                  
            //        iratTargyObj.value = this.value;
            //    }  ";
            #endregion

            CascadingDropDown_AgazatiJel.Enabled = true;
            CascadingDropDown_Ugykor.Enabled = true;
            CascadingDropDown_Ugytipus.Enabled = true;

            CascadingDropDown_IktatoKonyv.Enabled = true;

            Iktatokonyvek_DropDownLis_Ajax.Visible = true;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;

            RequiredFieldValidator1.Enabled = true;
            // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
            {
                RequiredFieldValidator2.Enabled = true;
                // BUG_1499
                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;
                }
                else
                    CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";
            }
            else
            {
                // BUG_1499
                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    if (CascadingDropDown_Ugykor.SelectedValue == String.Empty)
                    {
                        CascadingDropDown_IktatoKonyv.ParentControlID = "";
                    }
                    else CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";

                }
                else
                {
                    if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
                    {
                        CascadingDropDown_IktatoKonyv.ParentControlID = "";
                    }
                    else CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;
                }
                RequiredFieldValidator2.Enabled = false;
            }

            SetIrattariTetelUI();
        }


        #region Cascading DropDown

        //CascadingDropDown_AgazatiJel.TargetControlID = AgazatiJelekDropDownList1.DropDownList.UniqueID

        //CascadingDropDown_Ugykor.ParentControlID = AgazatiJelekDropDownList1.DropDownList.UniqueID;
        //CascadingDropDown_Ugytipus.ParentControlID = Ugykor_DropDownList.UniqueID;

        // Contextkey-ben megadjuk a felhasználó id-t
        CascadingDropDown_AgazatiJel.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugykor.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugytipus.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);

        #endregion


        //Partner és cím összekötése 
        Bekuldo_PartnerTextBox.CimTextBox = Kuld_CimId_CimekTextBox.TextBox;
        Bekuldo_PartnerTextBox.CimHiddenField = Kuld_CimId_CimekTextBox.HiddenField;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimTextBox = CimekTextBoxUgyindito.TextBox;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimHiddenField = CimekTextBoxUgyindito.HiddenField;

        // A címzettnél a kötés kimenõ email iktatásnál nem kell, különben fejbevágná az emailcímet)
        if (Mode != CommandName.KimenoEmailIktatas)
        {
            Pld_PartnerId_Cimzett_PartnerTextBox.CimTextBox = Pld_CimId_Cimzett_CimekTextBox.TextBox;
            Pld_PartnerId_Cimzett_PartnerTextBox.CimHiddenField = Pld_CimId_Cimzett_CimekTextBox.HiddenField;
        }

        // Küldemény beérkezés és bontás idõpontja is látható
        BeerkezesIdeje_CalendarControl.TimeVisible = true;
        Kuld_FelbontasDatuma_CalendarControl.TimeVisible = true;

        IntezesIdopontja_CalendarControl.TimeVisible = true;

        CalendarControl_UgyintezesKezdoDatuma.TimeVisible = true;
        // Irat határidõ idõpontja is látható
        IraIrat_Hatarido_CalendarControl.TimeVisible = true;

        //DropDownList ListSearchExtender konfigurálása
        AgazatiJelek_DropDownList.LSEX_RaiseImmediateOnChange = true;
        Ugykor_DropDownList.LSEX_RaiseImmediateOnChange = true;

        RegisterJavascripts();

        //if (Command == CommandName.Modify)
        //{
        //    IraIrat_Irattipus_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        //    IraIrat_Irattipus_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(IrattipusDropDownList_SelectedIndexChanged);
        //}

        FeljegyzesPanel.ErrorPanel = EErrorPanel1;
        FeljegyzesPanel.ErrorUpdatePanel = ErrorUpdatePanel1;


        //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        //ExecParam execParam_KRT_Param =  UI.SetExecParamDefault(Page, new ExecParam());
        //execParam_KRT_Param.Record_Id = "F7D555E4-54BC-E611-80BB-00155D020D34"; //VonalkodKezeles, fajtája, hova tegyem a constansokat??
        //Result Result_KRTParam = service_parameterek.Get(execParam_KRT_Param);
        //if (((KRT_Parameterek)Result_KRTParam.Record).Ertek.ToUpper().Trim().Equals("AZONOSITO"))
        //{

        // CR 3058
        // Az alábbi mezők kötelező kitölthetősége nem a vonalkód_kezeléstől függ, Org-függő (nekrisz, Laura infoja alapján)
        // CR 3123 : Munkairat létrehozásánál nem kötelező kitölteni az irat típusát és az ügyintézőt
        // BLG_1392 
        // Kötelezőséget rendszerparaméter állítja
        //    if (!FelhasznaloProfil.OrgIsBOPMH(Page) || (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo))
        ////    if (!Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        //    {
        //        Label_Pld_IratTipusCsillag.Visible = false;
        //        IraIrat_Irattipus_KodtarakDropDownList.Validate = false;
        //        starUpdate.Update();
        //        Label_UgyintezoCsillag.Visible = false;
        //        Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = false;
        //        StarUpdate1.Update();
        //    }
        //    else
        //    {
        //        Label_Pld_IratTipusCsillag.Visible = true;
        //        IraIrat_Irattipus_KodtarakDropDownList.Validate = true;
        //        starUpdate.Update();
        //        Label_UgyintezoCsillag.Visible = true;
        //        Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = true;
        //        StarUpdate1.Update();
        //    }
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_KOTELEZO_IRATTIPUS, false) && (Mode != CommandName.MunkapeldanyBeiktatas_Bejovo))
        {
            Label_Pld_IratTipusCsillag.Visible = true;
            IraIrat_Irattipus_KodtarakDropDownList.Validate = true;
            starUpdate.Update();
        }
        else
        {
            Label_Pld_IratTipusCsillag.Visible = false;
            IraIrat_Irattipus_KodtarakDropDownList.Validate = false;
            starUpdate.Update();
        }
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_KOTELEZO_IRATUGYINTEZO, false) && (Mode != CommandName.MunkapeldanyBeiktatas_Bejovo))
        {
            Label_UgyintezoCsillag.Visible = true;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = true;
            StarUpdate1.Update();
        }
        else
        {
            Label_UgyintezoCsillag.Visible = false;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = false;
            StarUpdate1.Update();
        }
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        {
            Label_Pld_VonalkodElottCsillag.Visible = false;
            // BUG_8080
            Label_Pld_Vonalkod.Visible = false;
            Pld_BarCode_VonalKodTextBox.Visible = false;
            starUpdate.Update();
            IraIrat_KiadmanyozniKell_CheckBox.Checked = true;
        }
        else if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
               && AdathordozoTipusa_KodtarakDropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
        {
            Label_Pld_VonalkodElottCsillag.Visible = false;
            starUpdate.Update();
            Pld_BarCode_VonalKodTextBox.RequiredValidate = false;
        }
        else
        {
            Label_Pld_VonalkodElottCsillag.Visible = true;
            Pld_BarCode_VonalKodTextBox.Validate = true;
            Pld_BarCode_VonalKodTextBox.RequiredValidate = true;

            starUpdate.Update();
        }
        if (Mode == CommandName.KimenoEmailIktatas)
        {
            IraIrat_KiadmanyozniKell_CheckBox.Checked = false;
            IraIrat_KiadmanyozniKell_CheckBox.Enabled = false;

        }
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        // ORG függõ megjelenítés
        // Jelenleg nem ezt használjuk
        // Kikerült nyomógomra a KuldemenyList felületre, külön funkcióként meghívva
        //if ((Command == CommandName.New && Mode == CommandName.BejovoIratIktatas) && (FelhasznaloProfil.OrgIsBOPMH(Page)))
        //{
        //    cb_Munkapeldany.Visible = true;
        //    Label_Munkapeldany.Visible = true;
        //    cb_Munkapeldany.Enabled = FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);
        //}
        //else
        //{
        //    cb_Munkapeldany.Visible = false;
        //    Label_Munkapeldany.Visible = false;
        //}

        // BLG_1020
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            trUgyiratIntezesiIdoVisible = true;

            IntezesiIdo_KodtarakDropDownList.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, EErrorPanel1);

            List<string> filterList = new List<string>();
            filterList.Add(KodTarak.IDOEGYSEG.Nap);
            filterList.Add(KodTarak.IDOEGYSEG.Munkanap);
            IntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);

            // BLG_1020
            trIratIntezesiIdo.Visible = true;
            InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));

            IratIntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);
        }

        KodtarakDropDownList_EljarasiSzakaszFok.FillDropDownList(KodTarak.ELJARASI_SZAKASZ_FOK.KCS, true, EErrorPanel1);

        // BUG_3435
        //LZS - BUG_8854
        /*
        *if (!Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        *    KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = KodTarak.ELJARASI_SZAKASZ_FOK.I;
        */
        //LZS - BUG_6309
        if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.UGYFEL_ADATOK_KOTELEZOSEG) == 1)
        {
            lblReqUgyindito.Visible =
            lblReqUgyinditoCime.Visible =
            Ugy_PartnerId_Ugyindito_PartnerTextBox.Validate =
            CimekTextBoxUgyindito.Validate = true;
        }

        Pld_CimId_Cimzett_CimekTextBox.SetCimTipusFilter(Pld_KuldesMod_KodtarakDropDownList.DropDownList, Pld_PartnerId_Cimzett_PartnerTextBox.BehaviorID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, Pld_KuldesMod_KodtarakDropDownList.DropDownList.UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);

        // BUG_11079
        TobbesPldFuggoKodtarAssign();

        if (isTUKRendszer && !IsPostBack)
        {
            if (Mode == CommandName.BejovoIratIktatas)
            {
                ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null, true);
            }
            //else if (Command == CommandName.View || Command == CommandName.Modify)
            //{
            //    ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null, true);
            //}
        }

        IktatoKonyvekDropDownList_Search.ValuesFilledWithId = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
    }

    private void SetIrattariTetelUI()
    {
        // BLG_44
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            Label_agazatiJel.Visible = true;
            AgazatiJelek_DropDownList.Visible = true;
            labelIrattariTetelszam.Visible = true;
            Ugykor_DropDownList.Visible = true;
            tr_ugytipus.Visible = true;
            labelUgyFajtaja.Visible = false;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            IrattariTetelszam_DropDownList.Visible = false;
        }
        else
        {
            Label_agazatiJel.Visible = false;
            AgazatiJelek_DropDownList.Visible = false;
            labelIrattariTetelszam.Visible = true;
            Ugykor_DropDownList.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            tr_ugytipus.Visible = false;
            labelUgyFajtaja.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;
            IrattariTetelszam_DropDownList.Visible = true;
            IrattariTetelszam_DropDownList.ReadOnly = false;

            if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
            {
                CascadingDropDown_IktatoKonyv.ParentControlID = "";
            }
            //BUG_1499
            else CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;

            RequiredFieldValidator2.Enabled = false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        #region BLG 232
        //LZS
        //A Page_Load() metódusban dolgozzuk fel a speciális „ForceSave” postback - et.Ilyenkor állítjuk „1” re a TabFooter.ConfirmationResult.Value - t, 
        //amely tárolja majd, hogy a confirmation ablakban OK - t válaszoltunk.
        //Ezt követően meghívjuk a TabFooterButtonsClick() metódust újra „Save” paraméterrel.
        if (IsPostBack)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"];

            if (eventArgument == "ForceSave")
            {
                try
                {
                    TabFooter1.ConfirmationResult.Value = "1";
                    CommandEventArgs eventArgs = new CommandEventArgs(CommandName.Save, "New");
                    TabFooterButtonsClick(TabFooter1, eventArgs);
                }
                catch (Exception ex)
                {
                    Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                }

            }
        }
        #endregion


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            #region  LZS - BLG_2011
            if (!string.IsNullOrEmpty(Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField))
            {
                Contentum.eAdmin.Service.EREC_IraElosztoivekService service = eAdminUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

                EREC_IraElosztoivekSearch searchElosztoiv = new EREC_IraElosztoivekSearch();
                searchElosztoiv.Id.Value = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
                searchElosztoiv.Id.Operator = Query.Operators.equals;
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.GetAllWithExtension(execParam, searchElosztoiv);

                if (!result.IsError)
                {
                    bool isElosztoIvek = result.Ds.Tables[0].Rows.Count > 0 ? true : false;

                    if (isElosztoIvek)
                    {
                        Pld_PartnerId_Cimzett_PartnerTextBox.ViewButtonEnabled = false;
                    }
                    else
                    {
                        Pld_PartnerId_Cimzett_PartnerTextBox.ViewButtonEnabled = true;
                    }
                }
            }
            else
            {
                Pld_PartnerId_Cimzett_PartnerTextBox.ViewButtonEnabled = false;
            }

            #endregion

        }

        if (Command == CommandName.New) // Modify esetén a hatósági adatoknál állítjuk be, ez pedig felülírná
        {
            FormHeader.ButtonsClick +=
                 new CommandEventHandler(FormHeader1_ButtonsClick);
        }

        if (!_Active) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

        if (!IsPostBack)
        {
            UgyUgyirat_Ugytipus_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");

            Ugykor_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            AgazatiJelek_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            Iktatokonyvek_DropDownLis_Ajax.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");

            // BLG_44
            IrattariTetelszam_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");

        }



        #region Kimenõ küldemény iktatásnál néhány komponens inicializálása
        if (Command == CommandName.New && Mode == CommandName.KimenoEmailIktatas)
        {
            Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = true;
            AdathordozoTipusa_KodtarakDropDownList.ReadOnly = true;
            Pld_KuldesMod_KodtarakDropDownList.ReadOnly = true;
            Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly = true;
        }
        #endregion

        IraIrat_Irattipus_KodtarakDropDownList.AutoPostBack = true;
        IraIrat_Irattipus_KodtarakDropDownList.DropDownList.SelectedIndexChanged += IraIrat_Irattipus_KodtarakDropDownList_SelectedIndexChanged;

        // BLG_44
        IrattariTetelszam_DropDownList.DropDownList.AutoPostBack = true;
        IrattariTetelszam_DropDownList.DropDownList.SelectedIndexChanged += new EventHandler(IrattariTetelszam_DropDownList_SelectedIndexChanged);

        UGY_FAJTAJA_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        UGY_FAJTAJA_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged);

        // BLG#1402
        SetTUKFieldsVisibility();

        //blg 607
        InitTobbszorosIratPeldany();

        if (!IsPostBack)
        {
            BejovoPeldanyListPanel1.Visible = PeldanyokSorszamaMegadhato;
            if (PeldanyokSorszamaMegadhato)
                IratPeldanyPanel.Visible = false;
        }
        InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));

        //InitializeLezarasOkaDDL(null, null);
        ShowLezarasOka();

        SetElektronikusIratVonalkodControl();

        //LZS - BLG_232
        //A Page_Load() végén beállítjuk, hogy ha a ConfirmationResult értéke „1”, akkor látszódjon az ErrorPanel.
        if (TabFooter1.ConfirmationResult.Value == "1")
        {
            EErrorPanel1.Visible = true;
        }

        //LZS - BLG_335 - Ugyintező clone mezők beállítása
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZO_MASOLAS_UGYROL) == "1")
        {
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.CloneHiddenfieldId = Irat_Ugyintezo_FelhasznaloCsoportTextBox.HiddenField.ClientID;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.CloneTextboxId = Irat_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.ClientID;
        }

        // BUG_6549
        Irat_Iranya_DropDownList.AutoPostBack = true;
        Irat_Iranya_DropDownList.SelectedIndexChanged += Irat_Iranya_DropDownList_SelectedIndexChanged;
        Pld_CimId_Cimzett_CimekTextBox_SetValidate();

        // BUG_10524
        // BUG_9291
        //if (Command != CommandName.View)
        //    SetIratUgyintezesiIdoPlusHataridoVisibility();

        // BUG_8765
        if (IsTUK_SZIGNALAS_ENABLED)
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        //HiddenFieldCommand.Value = string.IsNullOrEmpty(Command) ? "" : Command.ToString();
        //HiddenFieldMode.Value = string.IsNullOrEmpty(Mode) ? "" : Mode.ToString();
        if (isTUKRendszer)
        {
            if (Mode == CommandName.BelsoIratIktatas || Mode == CommandName.BejovoIratIktatas)
            {
                IrattariHelyLevelekDropDownTUK.Validate = true;
                IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
                IrattariHelyLevelekDropDownTUKUgyirat.Validate = true;
                IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.AutoPostBack = false;
            }
        }

    }

    // BUG_6549
    private void Pld_CimId_Cimzett_CimekTextBox_SetValidate()
    {
        Pld_CimId_Cimzett_CimekTextBox.Validate = Command == CommandName.New && Mode == CommandName.BelsoIratIktatas
            && (Irat_Iranya_DropDownList.SelectedValue == KodTarak.POSTAZAS_IRANYA.Belso || Irat_Iranya_DropDownList.SelectedValue == KodTarak.POSTAZAS_IRANYA.Kimeno)
            && Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BELSO_KIMENO_CIMZETT_CIME_KOTELEZO);
    }

    private void Irat_Iranya_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Pld_CimId_Cimzett_CimekTextBox_SetValidate();
    }

    #endregion

    private void IrattariTetelszam_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        // BUG_1499
        SetMergeItszFromDDL();
        //if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
        //{
        //    CascadingDropDown_IktatoKonyv.ParentControlID = "";
        //}
        //else CascadingDropDown_IktatoKonyv.ParentControlID = "IrattariTetelszam_DropDownList";

    }

    // BUG_1499
    private void SetMergeItszFromDDL()
    {
        Merge_IrattariTetelszamTextBox.Visible = true;
        int elv = IrattariTetelszam_DropDownList.DropDownList.SelectedItem.Text.IndexOf('_');
        if (elv > -1)
        {
            Merge_IrattariTetelszamTextBox.Text = IrattariTetelszam_DropDownList.DropDownList.SelectedItem.Text.Substring(0, elv);

            CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.FelhasznaloId(Page) + ';' + IrattariTetelszam_DropDownList.DropDownList.SelectedValue;
        }
        if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
        {
            CascadingDropDown_IktatoKonyv.ParentControlID = "";
            Merge_IrattariTetelszamTextBox.Text = String.Empty;
        }
        else CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;
    }

    //private void IrattipusDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //    Result result = service.GetIratMetaDefinicioByIrattipus(execParam
    //        , UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField
    //        , Ugytipus_HiddenField.Value
    //        , UgyiratdarabEljarasiSzakasz_HiddenField.Value
    //        , IraIrat_Irattipus_KodtarakDropDownList.SelectedValue);

    //    EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
    //    string ToolTip = IratMetaDefinicio.GetHataridoToolTip(Page, erec_IratMetaDefinicio);
    //    ToolTip = ToolTip.Replace("\n", "\\n");
    //    //IratMetaDefinicio.SetHataridoToolTip(Page, erec_IratMetaDefinicio, IraIrat_Hatarido_CalendarControl.TextBox);
    //    RegisterStartupScript("IratToolTip", "var hatarido = $get('" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + "'); if (hatarido) {hatarido.title='" + ToolTip + "';}");
    //}

    // ezzel a külsõ hívással biztosítható, hogy csak egy metódusban végrehajtott mûveletek
    // lefutása után hívódjon meg 
    private void RegisterStartupScript(string key, string script)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), key, script, true);
    }

    // ha az egyik határidõ textbox tartalmát kézzel módosítják, a tooltip ennek megfelelõre vált,
    // a másik határidõ textbox tooltipje pedig törlõdik
    //LZS - BLG_335 - Új isClone paraméter hozzáadva, a határidő másolás funkció miatt.
    private string HataridoTextBoxManualSetJavaScript(TextBox changedHataridoTextBox, TextBox alternateHataridoTextBox, bool isCloneable)
    {
        string js = @"var txt_changedhatarido = $get('" + changedHataridoTextBox.ClientID + @"');
                    if (txt_changedhatarido)
                    {
                        txt_changedhatarido.title = '" + Resources.Form.UI_IntezesiIdo_ManualSet_ToolTip + @"';
                    }
                    var txt_alternatehatarido = $get('" + alternateHataridoTextBox.ClientID + @"');
                    if (txt_alternatehatarido)
                    {
                        txt_alternatehatarido.title = '';
                    }
                    ";

        //LZS - BLG_335
        //Határidő másolása a klón objektumba
        if (isCloneable)
        {
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
            {
                //Dátum
                js += @"var txt_ugyirathatarido = $get('" + changedHataridoTextBox.ClientID + @"');
                        var txt_irathatarido = $get('" + alternateHataridoTextBox.ClientID + @"');
                    
                        txt_irathatarido.value = txt_ugyirathatarido.value;
                      ";
            }
        }

        return js;
    }

    // BUG_10524
    private string HataridoTextBoxManualSetJavaScriptWithPldHatarido(TextBox changedHataridoTextBox, TextBox alternateHataridoTextBox, TextBox alternateHataridoTextBox2, bool isCloneable)
    {
        string js = HataridoTextBoxManualSetJavaScript(changedHataridoTextBox, alternateHataridoTextBox, isCloneable);
        string defaultpldToolTip = String.Empty;
        string defaultpldDate = GetDefaultVisszaerkezesiHataridoPld(out defaultpldToolTip);
        js += @"
                    var txt_changedhatarido2 = $get('" + changedHataridoTextBox.ClientID + @"');
                    var txt_alternatehatarido2 = $get('" + alternateHataridoTextBox2.ClientID + @"');
                    if (txt_alternatehatarido2)
                    {
                        if (txt_changedhatarido2.value =='')
                        {
                             txt_alternatehatarido2.title = '" + defaultpldToolTip + @"';
                        } else
                        {
                            txt_alternatehatarido2.title = '';
                        }
                    }
                    ";

        //LZS - BLG_335
        //Határidő másolása a klón objektumba
        if (isCloneable)
        {
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
            {
                //Dátum
                js += @"var txt_ugyirathatarido2 = $get('" + changedHataridoTextBox.ClientID + @"');
                        var txt_pldhatarido = $get('" + alternateHataridoTextBox2.ClientID + @"');
                    
                        if (txt_pldhatarido)
                        {
                            if (txt_ugyirathatarido2.value =='')
                            {
                                 txt_pldhatarido.value = '" + defaultpldDate + @"';
                            } else
                            {
                                txt_pldhatarido.value = txt_ugyirathatarido2.value;
                            }
                       }
                      ";
            }
        }
        return js;
    }

    private void RegisterJavascripts()
    {
        //// Csak bejövõ iktatásnál:
        //if (Command == CommandName.New
        //    && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.AtIktatas)
        //    )
        //{
        //    RegisterJavascriptsForWSCall();
        //}

        // Bejövõ és belsõ iktatásnál:
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        if (Command == CommandName.New
             && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.AtIktatas || Mode == CommandName.BelsoIratIktatas
                        || Mode == CommandName.KimenoEmailIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)

             )
        {
            RegisterJavascriptsForWSCall(Mode);
        }
        else if (Command == CommandName.Modify)
        {
            RegisterIrattipusChangeJavascriptsForWSCall();
        }

        if (Mode == CommandName.BejovoIratIktatas && Command == CommandName.New && String.IsNullOrEmpty(kuldemenyId))
        {
            string jsOnKeyDown = @"function OnVonalkodKeyDown(ev)
            {
                var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
                if (k === Sys.UI.Key.enter)
                {
                    " + ImageButton_KuldemenyKereses.OnClientClick + @"
                }
            return false;
            }";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnVonalkodKeyDown", jsOnKeyDown, true);

            VonalkodTextBoxKuldemeny.TextBox.Attributes.Add("onkeydown", "OnVonalkodKeyDown(event)");

            JavaScripts.SetFocusOnEnd(VonalkodTextBoxKuldemeny.TextBox);
        }

        if (Command == CommandName.New)
        {
            #region GombCsere

            string jsOnEvKeyUp = @"function OnEvKeyUp(ev)
            {
               var k= null;
               if(ev) var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
               if (k == null || k != Sys.UI.Key.enter)
               {
                   var textBoxEv = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + @"');
                   if(textBoxEv)
                    {
                       var evValue = textBoxEv.value;
                       var elozmenyVisible = true;
                       if(evValue != '')
                       {
                          evValue = parseInt(evValue);
                          if(!isNaN(evValue) && evValue < " + MigralasEve + @")
                          {
                             elozmenyVisible = false;
                          }
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               if(elozmenyVisible)
                               {
                                   imageElozmeny.style.display = '';
                                   imageMigralt.style.display = 'none';
                               }
                               else
                               {
                                   imageElozmeny.style.display = 'none';
                                   imageMigralt.style.display = '';
                               }
                           }  
                       }
                       else
                       {
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               imageElozmeny.style.display = '';
                               imageMigralt.style.display = '';
                           } 
                       }
                   }
               }     
             };OnEvKeyUp(null);";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnEvKeyUp", jsOnEvKeyUp, true);

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeyup", "OnEvKeyUp(event)");

            #endregion

            #region Enter figyelese

            string jsOnElozmenyKeyDown = @"function OnElozmenyKeyDown(ev)
            {
                var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
                if (k === Sys.UI.Key.enter)
                {
                    " + ImageButton_Elozmeny.OnClientClick + @"
                }
            return false;
            }";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnElozmenyKeyDown", jsOnElozmenyKeyDown, true);

            numberFoszamSearch.TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            IktatoKonyvekDropDownList_Search.DropDownList.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            #endregion
        }

        #region intézési határidõk
        //irat határidõ textbox változás figyelés
        IraIrat_Hatarido_CalendarControl.TextBox.Attributes.Add("onchange"
            , HataridoTextBoxManualSetJavaScript(IraIrat_Hatarido_CalendarControl.TextBox, UgyUgyirat_Hatarido_CalendarControl.TextBox, false));

        //LZS - BLG_335 - Másolandó (clone) textbox-ok átadása az ügyirat határidő calendar controlnak. Óra, perc külön.
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
        {
            //LZS - change - itt ki volt kommentelve az első változatban
            //ügyirat határidõ textbox változás figyelés
            // BUG_10524
            UgyUgyirat_Hatarido_CalendarControl.TextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScriptWithPldHatarido(UgyUgyirat_Hatarido_CalendarControl.TextBox, IraIrat_Hatarido_CalendarControl.TextBox, Pld_VisszaerkezesiHatarido_CalendarControl.TextBox, true));


            //LZS - BLG_335 - Óra textbox figyelés
            UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScript(UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox, IraIrat_Hatarido_CalendarControl.GetHourTextBox, true));

            //LZS - BLG_335 - Perc textbox figyelés
            UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScript(UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox, IraIrat_Hatarido_CalendarControl.GetMinuteTextBox, true));


            UgyUgyirat_Hatarido_CalendarControl.CloneHourTextId = IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID;
            UgyUgyirat_Hatarido_CalendarControl.CloneMinuteTextId = IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID;
        }
        #endregion intézési határidõk

        #region BLG#1402 (TÜK-ös mezõk)

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            if (Command == CommandName.New && Mode == CommandName.BelsoIratIktatas
                  || Command == CommandName.Modify)
            {
                // Minõsítõ mezõ az ügyindító partnertõl függ:
                //string jsMinositoStartupScript =
                //          @"
                //        try {                    
                //            $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Ugy_PartnerId_Ugyindito_PartnerTextBox_PartnerMegnevezes').on('change', function () {                        
                //                if (typeof setTimeout !== 'undefined' && typeof setMinosito !== 'undefined')
                //                    setTimeout(setMinosito, 1500);
                //            });

                //            $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Ugy_PartnerId_Ugyindito_PartnerTextBox_HiddenField1').on('change', function () {
                //                if (typeof SetMinositoSzuloPartner !== 'undefined')
                //                    SetMinositoSzuloPartner($(this).val(), null);
                //            });
                //        } catch (e) {
                //            //console.log(e);
                //        }
                //        ";
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetMinositoStartupScript", jsMinositoStartupScript, true);
            }
        }

        #endregion
    }

    // módosításnál csak az irattípus változását figyeljük
    private void RegisterIrattipusChangeJavascriptsForWSCall()
    {

        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null)
        {
            throw new Exception("ScriptManager not found.");
        }

        sm.Services.Add(new ServiceReference("~/WrappedWebService/Ajax_eRecord.asmx"));

        ScriptReference sr = new ScriptReference();
        sr.Path = "~/JavaScripts/WsCalls.js?t=20190918";
        sm.Scripts.Add(sr);

        string js_variables = @"
                    
                    var userId = '" + FelhasznaloProfil.FelhasznaloId(Page) + @"';
                    var UgyUgyiratHatarido_TextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_HourTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_MinuteTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_CalendarImage_Id = '" + UgyUgyirat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IrattipusDropDown_Id = '" + IraIrat_Irattipus_KodtarakDropDownList.DropDownList.ClientID + @"';
                    // BLG_1020
                    var Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + BeerkezesIdeje_CalendarControl.TextBox.ClientID + @"';
                    var IraIratHatarido_TextBox_Id = '" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var IraIratHatarido_HourTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var IraIratHatarido_MinuteTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var IraIratHatarido_CalendarImage_Id = '" + IraIrat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IraIratHatarido_ArrowUpImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowUpImage.ClientID + @"';
                    var IraIratHatarido_ArrowDownImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowDownImage.ClientID + @"';
                    var EljarasiSzakaszHiddenField_Id = '" + UgyiratdarabEljarasiSzakasz_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Ugyirat_HiddenField_Id = '" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Irat_HiddenField_Id = '" + UgyiratHataridoKitolas_Irat_HiddenField.ClientID + @"';
                    var IktatasDatumUgyirat_HiddenFieldId = '" + IktatasDatuma_Ugyirat_HiddenField.ClientID + @"';
                    var IktatasDatumIrat_HiddenFieldId = '" + IktatasDatuma_Irat_HiddenField.ClientID + @"';
                    var Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.ClientID + @"';
                    var UgyintezesKezdete_TextBox_Id = '" + CalendarControl_UgyintezesKezdete.TextBox.ClientID + @"';
                    var UgyintezesKezdete_HourTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetHourTextBox.ClientID + @"';
                    var UgyintezesKezdete_MinuteTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetMinuteTextBox.ClientID + @"';
                    // BLG_1020
                    var inputUgyintezesiNapok_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var UgyintezesKezdete_TextBox_Id = '" + CalendarControl_UgyintezesKezdete.TextBox.ClientID + @"';
                    var UgyintezesKezdete_HourTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetHourTextBox.ClientID + @"';
                    var UgyintezesKezdete_MinuteTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetMinuteTextBox.ClientID + @"';
                    var IratIntezesiIdoegyseg_DropDownList_Id = '" + IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"';";


        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "FelelosCsoportVariables", js_variables, true);

        string js_AddHandler = @"

        function IrattipusOnChange()
            {
                var elozmenyHiddenField = $get('" + ElozmenyUgyiratID_HiddenField.ClientID + @"');
                if (elozmenyHiddenField)
                {
                    SetTextBoxesIrattipusByUgyirat(elozmenyHiddenField.value, false);
                }
            }
        ";
        // BLG_1020
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            // BLG_1020
            inputUgyintezesiNapok.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");
            // TASK_5454
            //IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManual()");
            IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");

        }
        else
        //if (!Uj_Ugyirat_Hatarido_Kezeles)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "DropDown_handler", js_AddHandler, true);

            IraIrat_Irattipus_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "IrattipusOnChange()");
        }

    }

    // szétválasztjuk a bejövõ (kell szignálási jegyzék) és a belsõ (csak intézési határidõ kell) iktatást
    private void RegisterJavascriptsForWSCall(String Mode)
    {

        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null)
        {
            throw new Exception("ScriptManager not found.");
        }

        sm.Services.Add(new ServiceReference("~/WrappedWebService/Ajax_eRecord.asmx"));

        ScriptReference sr = new ScriptReference();
        sr.Path = "~/JavaScripts/WsCalls.js?t=20190918";
        sm.Scripts.Add(sr);

        // var eSetTextBoxesByUgykorUgytipusType = { All:0, OnlySzignalas:1, OnlyIntezesiIdo:2 };
        string eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo";
        switch (Mode)
        {
            case CommandName.BejovoIratIktatas:
            case CommandName.AtIktatas:
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            case CommandName.MunkapeldanyBeiktatas_Bejovo:
                eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.All";
                break;
        }

        // WorkAround: irattípus változásnál nem töltjük a szignálási jegyzék szerinti felelõst,
        // mert akkor a már szignálás alapján kitöltött és írásvédett értékek is felülíródnak a képernyõn
        string eSetTextBoxesByUgykorUgytipusIrattipusChangedType = "eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo";

        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.None";
        }

        string js_variables = @"
                    
                    var userId = '" + FelhasznaloProfil.FelhasznaloId(Page) + @"';
                    var userCsoportId = '" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page) + @"';
                    var UgykorDropDown_Id = '" + Ugykor_DropDownList.ClientID + @"'; 
                    var UgytipusDropDown_Id = '" + UgyUgyirat_Ugytipus_DropDownList.ClientID + @"';                    
                    var Ugyfelelos_TextBox_Id = '" + Ugyfelelos_CsoportTextBox.TextBox.ClientID + @"';
                    var Ugyfelelos_HiddenField_Id = '" + Ugyfelelos_CsoportTextBox.HiddenField.ClientID + @"';
                    var Ugyintezo_TextBox_Id = '" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.TextBox.ClientID + @"';
                    var Ugyintezo_HiddenField_Id = '" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.HiddenField.ClientID + @"';
                    var Kezelo_TextBox_Id = '" + UgyUgyiratok_CsoportId_Felelos.TextBox.ClientID + @"'
                    var Kezelo_HiddenField_Id = '" + UgyUgyiratok_CsoportId_Felelos.HiddenField.ClientID + @"';
                    var UgyTargy_TextBox_Id = '" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_TextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_CalendarImage_Id = '" + UgyUgyirat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IrattipusDropDown_Id = '" + IraIrat_Irattipus_KodtarakDropDownList.DropDownList.ClientID + @"';
                    var IraIratHatarido_TextBox_Id = '" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var IraIratHatarido_HourTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var IraIratHatarido_MinuteTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var IraIratHatarido_CalendarImage_Id = '" + IraIrat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IraIratHatarido_ArrowUpImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowUpImage.ClientID + @"';
                    var IraIratHatarido_ArrowDownImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowDownImage.ClientID + @"';
                    var EljarasiSzakaszHiddenField_Id = '" + UgyiratdarabEljarasiSzakasz_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Ugyirat_HiddenField_Id = '" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Irat_HiddenField_Id = '" + UgyiratHataridoKitolas_Irat_HiddenField.ClientID + @"';
                    var IktatasDatumUgyirat_HiddenFieldId = '" + IktatasDatuma_Ugyirat_HiddenField.ClientID + @"';
                    var IktatasDatumIrat_HiddenFieldId = '" + IktatasDatuma_Irat_HiddenField.ClientID + @"';
                    // BLG_1020
                    var Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + BeerkezesIdeje_CalendarControl.TextBox.ClientID + @"';
                    // BLG_619
                    var Iratfelelos_TextBox_Id = '" + Iratfelelos_CsoportTextBox.TextBox.ClientID + @"';
                    var Iratfelelos_HiddenField_Id = '" + Iratfelelos_CsoportTextBox.HiddenField.ClientID + @"';
                    // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                    var IktatokonyvDropDown_Id = '" + Iktatokonyvek_DropDownLis_Ajax.ClientID + @"'; 
                    var Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.ClientID + @"';
                    var IntezesiIdo_DropDownList_Id = '" + IntezesiIdo_KodtarakDropDownList.DropDownList.ClientID + @"';
                    var IntezesiIdoegyseg_DropDownList_Id = '" + IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"';
                    // BLG_1020
                    var inputUgyintezesiNapok_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var UgyUgyiratHatarido_TextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_HourTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_MinuteTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var UgyintezesKezdete_TextBox_Id = '" + CalendarControl_UgyintezesKezdete.TextBox.ClientID + @"';
                    var UgyintezesKezdete_HourTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetHourTextBox.ClientID + @"';
                    var UgyintezesKezdete_MinuteTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetMinuteTextBox.ClientID + @"';
                    var IratIntezesiIdoegyseg_DropDownList_Id = '" + IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"';";


        js_variables += @"var Merge_IrattariTetelszamTextBox_Id = '" + Merge_IrattariTetelszamTextBox.ClientID + @"';";

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "FelelosCsoportVariables", js_variables, true);

        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        string js_AddHandler = @"
        function UgyTipusChangeHandler()
            {
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");
            }    
        ";
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
        {
            js_AddHandler += @"
            function UgykorChangeHandler()
            {
                SetMerge_IrattariTetelszamTextBox();
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");
            }";
        }
        else
        {
            js_AddHandler += @"
            function UgykorChangeHandler()
            {
                SetMerge_IrattariTetelszamTextBox();
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");

                // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                var UgykorDropDown = $get(UgykorDropDown_Id);
                if (UgykorDropDown)
                {
                    if (UgykorDropDown.selectedIndex > 0)
                    {
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID(UgykorDropDown_Id);
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                    }
                    else
                    {
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID('');
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                    }
                }
            }
            ";
        }

        //BLG_335
        //LZS - Rendszerparaméter alapján hozzuk létre a paramétert, hogy kell-e clear-elni a határidőt vagy sem.
        string SetTextBoxesIrattipusByUgyiratParam = "";

        if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
        {
            SetTextBoxesIrattipusByUgyiratParam = ", false";
        }
        else
        {
            SetTextBoxesIrattipusByUgyiratParam = ", true";
        }

        js_AddHandler += @"
        function IrattipusOnChange()
            {
                var elozmenyHiddenField = $get('" + ElozmenyUgyiratID_HiddenField.ClientID + @"');
                if (!elozmenyHiddenField || elozmenyHiddenField.value == '')
                {
                    SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusIrattipusChangedType + @");
                }
                else
                {
                    SetTextBoxesIrattipusByUgyirat(elozmenyHiddenField.value" + SetTextBoxesIrattipusByUgyiratParam + @");
                }
            }

        function callCreateItemToolTips(sender, args)
            {
                var dropdown = sender.get_element();

                if (dropdown)
                {
                    createItemToolTips(dropdown);
                }
            }        

         function pageLoad(){//be called automatically.
            
            var cascadingDropDown_ugytipus = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
            if (cascadingDropDown_ugytipus)
            {
                cascadingDropDown_ugytipus.add_selectionChanged(UgyTipusChangeHandler);
                //cascadingDropDown_ugytipus.add_populated(createItemToolTips);
                cascadingDropDown_ugytipus.add_populated(callCreateItemToolTips);
            }
            var cascadingDropDown_ugykor = $find('" + CascadingDropDown_Ugykor.ClientID + @"');
            if (cascadingDropDown_ugykor)
            {
                cascadingDropDown_ugykor.add_selectionChanged(UgykorChangeHandler);
                cascadingDropDown_ugykor.add_populated(callCreateItemToolTips);   
            }

            var cascadingDropDown_agazatijel = $find('" + CascadingDropDown_AgazatiJel.ClientID + @"');
            if (cascadingDropDown_agazatijel)
            {
                cascadingDropDown_agazatijel.add_populated(callCreateItemToolTips);   
            }
          
            var cascadingDropDown_iktatokonyv = $find('" + CascadingDropDown_IktatoKonyv.ClientID + @"');
            if (cascadingDropDown_iktatokonyv)
            {
                cascadingDropDown_iktatokonyv.add_populated(callCreateItemToolTips);   
            }
         }
        
        var migraltUgyirat = document.getElementById('" + migraltUgyiratAzonositoMezo.ClientID + @"');
        if (migraltUgyirat)
        {
            migraltUgyirat.style.display = 'none';
        }

        ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "cascadingDropDown_handler", js_AddHandler, true);

        // BLG_1020
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            // BLG_1020
            inputUgyintezesiNapok.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");
            // TASK_5454
            //IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManual()");
            IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");

        }
        else
        //        if (!Uj_Ugyirat_Hatarido_Kezeles)
        {
            IraIrat_Irattipus_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "IrattipusOnChange()");
        }

        // ágazati jel váltásakor irattári tételszám törlése
        string jsClearIrattariTetelszam = @"var Merge_IrattariTetelszamTextBox = $get('" + Merge_IrattariTetelszamTextBox.ClientID + @"');
if (Merge_IrattariTetelszamTextBox)
{
    Merge_IrattariTetelszamTextBox.value = '';
}
";
        AgazatiJelek_DropDownList.Attributes.Add("onchange", jsClearIrattariTetelszam);

        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            IntezesiIdo_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIntezesiIdoManual()");
            IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIntezesiIdoManual()");
        }

    }

    private string SetCompareUgyiratIratHataridoJavaScript(ImageButton btnSaveToClick)
    {
        string js_IsIratModosithato = "";
        if (Command == CommandName.Modify)
        {
            // ha irat módosítás módban vagyunk, megengedjük, hogy a (számítottnál korábbi) ügyirat határidõt vegye át
            js_IsIratModosithato = "true";
        }
        else
        {
            // iktatásnál a kötött irat határidõ nem módosítható
            js_IsIratModosithato = "!bIratIntezesiIdoKotott";
        }
        // ha módosíthatja az ügyirat határidõt (fõszámra iktatás), akkor kitoljuk azt
        string js_ToDoIfDateMisMatch_AllowedToModify = @"
            var bUgyiratModosithato = bAllowedToModify && bUgyiratHataridoKitolas;
            var bIratModosithato = " + js_IsIratModosithato + @";
            btnSaveToClick = $get('" + btnSaveToClick.ClientID + @"');
            qmp_show(bUgyiratModosithato, bIratModosithato);
            return false;
";

        string js_AllowedToModify = "";
        if (Command == CommandName.Modify)
        {
            // irat módosításkor az ügyirat nem módosítható (TODO?)
            js_AllowedToModify = "false";
        }
        else if (Command == CommandName.New && bUgyiratHataridoModositasiJog)
        {
            // fõszámra iktatás vagy alszámra, és a határidõ kitolható
            js_AllowedToModify = "(!elozmenyHiddenField || elozmenyHiddenField.value == '' || (elozmenyHiddenField.value != '' && bUgyiratHataridoKitolas && ((bIsUgyintezo && bIsOrzo) || (bIsIktatokonyvLezart && !bIsIktatokonyvFSLezart))))";
        }
        else
        {
            // csak fõszámra iktatáskor
            js_AllowedToModify = "(!elozmenyHiddenField || elozmenyHiddenField.value == '')";
        }

        string js = @"var ugyiratHataridoTextBox = $get('" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"');
var iratHataridoTextBox = $get('" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + @"');
if (ugyiratHataridoTextBox && iratHataridoTextBox)
{
    var iratIntezesiHataridoHourTextBox = $get('" + IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"');
    var iratIntezesiHataridoMinuteTextBox = $get('" + IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"');
    var elozmenyHiddenField = $get('" + ElozmenyUgyiratID_HiddenField.ClientID + @"');
    var ugyiratHataridoKitolasUgyiratHiddenField = $get('" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"');
    var ugyiratHataridoKitolasIratHiddenField = $get('" + UgyiratHataridoKitolas_Irat_HiddenField.ClientID + @"');
    var ugyintezoHiddenField = $get('" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.HiddenField.ClientID + @"');
    var orzoHiddenField = $get('" + FelhasznaloCsoportIdOrzo_HiddenField.ClientID + @"');
    var lezartIktatokonyvHiddenField = $get('" + LezartIktatokonyv_HiddenField.ClientID + @"');
    var lezartIktatokonyvFSHiddenField = $get('" + LezartFolyosorszamosIktatokonyv_HiddenField.ClientID + @"');
    var bUgyiratHataridoKitolas = true;
    var bIsUgyintezo = false;
    var bIsOrzo = false;
    var bIsIktatokonyvLezart = false;
    var bIsIktatokonyvFSLezart = false;
    var bIratIntezesiIdoKotott = false;
    if (ugyiratHataridoKitolasUgyiratHiddenField && ugyiratHataridoKitolasIratHiddenField)
    {
        if (ugyiratHataridoKitolasUgyiratHiddenField.value == '0')
        {
            bUgyiratHataridoKitolas = false;
        }
        else if (ugyiratHataridoKitolasIratHiddenField.value == '0|0' || ugyiratHataridoKitolasIratHiddenField.value == '0|1')
       {
            bUgyiratHataridoKitolas = false;
       }

        if (ugyiratHataridoKitolasIratHiddenField.value == '0|1' || ugyiratHataridoKitolasIratHiddenField.value == '1|1')
        {
            bIratIntezesiIdoKotott = true;
        }
    }

    if (ugyintezoHiddenField && ugyintezoHiddenField.value == '" + FelhasznaloProfil.FelhasznaloId(Page) + @"')
    {
        bIsUgyintezo = true;
    }
    if (orzoHiddenField && orzoHiddenField.value == '" + FelhasznaloProfil.FelhasznaloId(Page) + @"')
    {
        bIsOrzo = true;
    }
    if (lezartIktatokonyvHiddenField && lezartIktatokonyvHiddenField.value != '')
    {
        bIsIktatokonyvLezart = true;
    }
    if (lezartIktatokonyvFSHiddenField && lezartIktatokonyvFSHiddenField.value != '')
    {
        bIsIktatokonyvLezart = true;
        bIsIktatokonyvFSLezart = true;
    }

    var ugyiratHatarido = Date.parseLocale(ugyiratHataridoTextBox.value);
    var iratHatarido = Date.parseLocale(iratHataridoTextBox.value);
    if (ugyiratHataridoTextBox.value != '' && iratHataridoTextBox.value != '' && ugyiratHatarido < iratHatarido)
    {
            var bAllowedToModify = " + js_AllowedToModify + @";
    " + js_ToDoIfDateMisMatch_AllowedToModify + @"
    }
}";

        return js;
    }


    protected void IraIratUpdatePanel_Load(object sender, EventArgs e)
    {

        // ha nem ez az aktív tab, nem fut le
        //if (!_Active) return;
        if (!MainPanel.Visible) return;

        // csak iktatáskor van értelme ezt figyelni (nehogy feleslegesen lefusson a tabfülek kattintgatásakor)

        if (Command == CommandName.New)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshKuldemenyekPanel:
                        LoadKuldemenyComponents();
                        break;

                    case EventArgumentConst.refreshUgyiratokPanel:
                        if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyUgyirat();
                        }
                        else if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyRegiUgyirat();
                        }
                        break;
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refresh:
                        RaiseEvent_OnChangedObjectProperties();
                        ReLoadTab();
                        break;
                }
            }
        }

        //BLG 1974
        //Amennyiben a VONALKOD_KEZELES = SAVOS és a VONALKOD_KIMENO_GENERALT értéke is 1-es, akkor belsõ keletkezésû/kimenõ irat iktatása esetén nem kötelezõ a vonalkód mezõ
        ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
        string vonalkodKiGeneralt = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KIMENO_GENERALT).Trim();

        //BLG 1974
        if (vonalkodkezeles.ToUpper().Trim().Equals("SAVOS") && vonalkodKiGeneralt.Trim().Equals("1") && Mode == CommandName.BelsoIratIktatas)
        {
            Pld_BarCode_VonalKodTextBox.Validate = false;
            //Pld_BarCode_VonalKodTextBox.RequiredValidate = false;
            Label_Pld_VonalkodElottCsillag.Visible = false;
        }
    }


    #region Elozmeny Ugyirat
    /// <summary>
    /// Ügyirat adatainak feltöltése adatbázisból
    /// </summary>
    private void SetElozmenyUgyirat()
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        if (!String.IsNullOrEmpty(UgyUgyirat_Id))
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = UgyUgyirat_Id;
            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                SetElozmenyUgyirat(erec_UgyUgyiratok);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }

        }
    }

    private void SetElozmenyUgyirat(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = UgyUgyirat_Id;
        if (erec_UgyUgyiratok != null)
        {
            /// Ellenõrzés, hogy lehet-e alszámra iktatni az ügyiratba
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
            ErrorDetails errorDetail = null;

            // BUG_12060
            string iratId = Request.QueryString.Get(QueryStringVars.IratId);
            EREC_UgyUgyiratok aktUgyirat = null;
            if (!string.IsNullOrEmpty(iratId))
            {
                ExecParam execParamFind = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_IraIratok irat = Contentum.eUtility.Sakkora.GetIrat(execParamFind, iratId);
                if (irat != null)
                {
                    aktUgyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParamFind, irat.Ugyirat_Id);
                }
            }
            if (aktUgyirat != null)
            {
                if (aktUgyirat.Id == UgyUgyirat_Id && this.Mode == "AtIktatas")
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52875); //Az önmagába átiktatás nem lehetséges, válasszon másik ügyiratot!

                    ErrorUpdatePanel1.Update();
                    // hiddenfield értékének törlése
                    ElozmenyUgyiratID_HiddenField.Value = "";
                    ClearElozmenyUgyirat();
                    return;
                }
            }

            /// CR#1305: (FPH_TITKARSAGNAL) Ellenõrzés: Ha BejovoIratIktatasCsakAlszamra vagy BelsoIratIktatasCsakAlszamra joga van, akkor 
            /// nem iktathat bizonyos állapotú ügyiratokba ()
            if (Ugyiratok.LehetAlszamraIktatni(execParam, ugyiratStatusz, out errorDetail) == false
                 || Ugyiratok.CheckAlszamraIktatasSkontroIrattar_Funkciojog(Page, erec_UgyUgyiratok, Mode) == false)
            {
                // nem lehet alszámra iktatni, hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(52109, errorDetail));
                ErrorUpdatePanel1.Update();

                // hiddenfield értékének törlése
                ElozmenyUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();
                return;
            }


            //// Egyéb mûveletek sor megjelenítése alszámra iktatásnál:
            //tr_EgyebMuvelet.Visible = true;

            // elõrébb hoztuk, mert az elõzmény ügyirat esetleg felszabadíthatja a LoadUgyiratComponentsben
            UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;
            // BUG_2074
            trUgyiratIntezesiIdoVisible = Uj_Ugyirat_Hatarido_Kezeles;
            IntezesiIdo_KodtarakDropDownList.ReadOnly = true;
            IntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;
            EREC_IraIktatoKonyvek erec_IraIktatokonyvek = LoadUgyiratComponents(erec_UgyUgyiratok, true);

            #region TÜK-ös mezõk

            // BLG#1402:
            // Minõsítõ mezõ választható értéke az ügyindító partner értékétõl függ:
            //MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId = erec_UgyUgyiratok.Partner_Id_Ugyindito;


            #endregion

            #region BLG_335
            //LZS – Előzményirat betöltésekor rendszerparaméterek alapján történik a tárgy, ügyintéző és az ügyintézési határidő másolása ügyiratról iratra.
            //LZS Mégis kell?
            // Mégsem kell (CR#1052)
            //// Ha üres az Iratok tárgy, segítségül beírjuk oda az Ügyiratok tárgyat
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
            {
                if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
                {
                    IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
                }
            }

            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
            {
                // BUG_10524
                //if (String.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.Text))
                //{
                IraIrat_Hatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                //}

                // BUG_10524
                if (!String.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.Text))
                {
                    Pld_VisszaerkezesiHatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = UgyUgyirat_Hatarido_CalendarControl.TextBox.ToolTip;
                }
                else
                {
                    //DateTime _30DaysAfterToday = DateTime.Now.AddDays(30);
                    //Pld_VisszaerkezesiHatarido_CalendarControl.Text = _30DaysAfterToday.ToShortDateString();
                    string errorMessage = String.Empty;
                    Pld_VisszaerkezesiHatarido_CalendarControl.Text = GetDefaultVisszaerkezesiHataridoPld(out errorMessage);
                    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = errorMessage;
                }
            }

            // BUG_8765
            //if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZO_MASOLAS_UGYROL) == "1")
            if ((Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZO_MASOLAS_UGYROL) == "1") || IsTUK_SZIGNALAS_ENABLED)

            {
                if (String.IsNullOrEmpty(Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
                {
                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Text = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;
                }
            }

            // BUG_8765
            if (IsTUK_SZIGNALAS_ENABLED)
                Irat_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;
            #endregion


            // Iktatóköny vizsgálata, lezárt-e esetleg:
            bool iktatoKonyvLezart = false;
            bool iktatokonyvFolyosorszamos = false;

            //CERTOP
            //Irattári tétel vizsgálat: Éven túli iktatás
            EREC_IraIrattariTetelekService svcIrattariTetelek = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam xpmIrattariTetel = UI.SetExecParamDefault(Page);
            xpmIrattariTetel.Record_Id = erec_UgyUgyiratok.IraIrattariTetel_Id;
            Result resIrattariTetel = svcIrattariTetelek.Get(xpmIrattariTetel);

            if (!resIrattariTetel.IsError)
            {
                EREC_IraIrattariTetelek irattariTetel = resIrattariTetel.Record as EREC_IraIrattariTetelek;

                if (irattariTetel != null && "1".Equals(irattariTetel.EvenTuliIktatas))
                {
                    iktatokonyvFolyosorszamos = true;
                    labelLezartFolyamatos.Text = "Figyelem: Éven túli ikatás esetén az iktatás csak alszámra megengedett!";
                }
            }

            if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatokonyvek) == true)
            {
                // Lezárt az iktatókönyv:
                iktatoKonyvLezart = true;

                if (erec_IraIktatokonyvek.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos)
                {
                    iktatokonyvFolyosorszamos = true;
                }

                if (iktatokonyvFolyosorszamos)
                {
                    LezartIktatokonyv_HiddenField.Value = "";
                    LezartFolyosorszamosIktatokonyv_HiddenField.Value = erec_IraIktatokonyvek.Id;
                }
                else
                {
                    LezartIktatokonyv_HiddenField.Value = erec_IraIktatokonyvek.Id;
                    LezartFolyosorszamosIktatokonyv_HiddenField.Value = "";
                }
            }
            else
            {
                LezartIktatokonyv_HiddenField.Value = "";
                LezartFolyosorszamosIktatokonyv_HiddenField.Value = "";
            }

            // Ügyirat lezárt-e?
            bool ugyiratLezart = false;
            if (Ugyiratok.Lezart(ugyiratStatusz) == true)
            {
                ugyiratLezart = true;
            }

            //Lezárt iktatókönyvû elõzménybe nem iktathat, ha csak alszámra iktathat joga van
            if (RaiseErrorLezartIktatokonyvbeNemIktathat(Mode))
            {
                // hiddenfield értékének törlése
                ElozmenyUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();
                return;
            }


            // ReadOnly tulajdonságok beállítása:                    
            //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.ReadOnly = true;
            //UgyUgyirat_UgyiratTextBox.ReadOnly = true;                    
            //UgyUgyirat_Ugytipus_KodtarakDropDownList.ReadOnly = true;
            // attól függ, hogy lezárt-e az iktatókönyv és van-e joga fõszámra iktatni
            //UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
            //UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;
            // elõrébb visszük, mert az elõzmény ügyirat esetleg felszabadíthatja a LoadUgyiratComponentsben
            //UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;
            // attól függ, hogy lezárt-e az iktatókönyv és van-e joga fõszámra iktatni
            //UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;

            // attól függ, hogy lezárt-e az iktatókönyv és van-e joga fõszámra iktatni
            //labelSkontroVege.Visible = false;
            //UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            //labelIrattarba.Visible = false;
            //UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            //if (erec_UgyUgyiratok.Allapot == "07")
            //{
            //    labelSkontroVege.Visible = true;
            //    UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
            //    labelIrattarba.Visible = false;
            //    UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            //}
            //if (erec_UgyUgyiratok.Allapot == "10")
            //{
            //    labelIrattarba.Visible = true;
            //    UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
            //    labelSkontroVege.Visible = false;
            //    UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            //}

            // Felelõs, Kezelõ sor látszódjon:
            UgyUgyiratok_CsoportId_Felelos.Visible = true;
            //Label_KezeloSzignJegyzekAlapjan.Visible = false;
            //tr_felelos_kezelo.Visible = true;

            //UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
            //Ugyfelelos_CsoportTextBox.ReadOnly = true;

            //// ügyindító, ügyfél
            //Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
            //CimekTextBoxUgyindito.ReadOnly = true;

            //// irat helye:
            //labelIratHelye_Ugyirat.Visible = true;
            //UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = true;

            // attól függ, hogy lezárt-e az iktatókönyv és van-e joga fõszámra iktatni
            bool bFoszamraIktathat = true;
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            // CR 3110: munkapéldánynál nem kell fõszámra iktatási jog/lehetõség (igaz?) (nem módosítottam)
            bFoszamraIktathat &= !(Mode == CommandName.BelsoIratIktatas && FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra));
            bFoszamraIktathat &= !((Mode == CommandName.BejovoIratIktatas || String.IsNullOrEmpty(Mode)) && FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra));
            if (iktatoKonyvLezart && bFoszamraIktathat && !iktatokonyvFolyosorszamos)
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = false;
                if (Command == CommandName.New)
                {
                    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                         Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);
                }
                Ugyfelelos_CsoportTextBox.ReadOnly = false;

                // ügyindító, ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = false;
                CimekTextBoxUgyindito.ReadOnly = false;

                UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = false;
                UgyUgyirat_Targy_RequiredTextBox.ReadOnly = false;

                UgyUgyiratok_CsoportId_Felelos.ReadOnly = false;

                // irat helye:
                labelIratHelye_Ugyirat.Visible = false;
                UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = false;

                // kezelõ a jelenlegi felhasználó lesz
                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

                // kezdõdátumot nem adunk meg
                IktatasDatuma_Ugyirat_HiddenField.Value = "";
                IktatasDatuma_Irat_HiddenField.Value = "";

                if (isTUKRendszer)
                {
                    IrattariHelyLevelekDropDownTUKUgyirat.Enabled = true;
                }

            }
            else
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
                Ugyfelelos_CsoportTextBox.ReadOnly = true;

                // ügyindító, ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
                CimekTextBoxUgyindito.ReadOnly = true;

                UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
                UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;

                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;

                // irat helye:
                labelIratHelye_Ugyirat.Visible = true;
                UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = true;

                if (isTUKRendszer)
                {
                    IrattariHelyLevelekDropDownTUKUgyirat.Enabled = false;
                }
            }

            labelSkontroVege.Visible = false;
            UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            labelIrattarba.Visible = false;
            UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            {
                labelSkontroVege.Visible = true;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
                labelIrattarba.Visible = false;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            }
            if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
            {
                labelIrattarba.Visible = true;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
                labelSkontroVege.Visible = false;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            }

            // BUG_1499         
            IrattariTetelszam_DropDownList.FillAndSetSelectedValue(erec_UgyUgyiratok.IraIrattariTetel_Id, true, EErrorPanel1);
            UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_UgyUgyiratok.Ugy_Fajtaja, true, EErrorPanel1);
            SetIrattariTetelUI();
            UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(UGY_FAJTAJA_KodtarakDropDownList.DropDownList, EventArgs.Empty);

            if (iktatoKonyvLezart == false || iktatokonyvFolyosorszamos)
            {
                #region Cascading cuccok letiltása:
                CascadingDropDown_AgazatiJel.Enabled = false;
                CascadingDropDown_Ugykor.Enabled = false;
                CascadingDropDown_Ugytipus.Enabled = false;
                CascadingDropDown_IktatoKonyv.Enabled = false;

                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    // BUG_1499
                    //// BLG_44
                    tr_agazatiJel.Visible = false;
                    //Label_agazatiJel.Visible = false;
                    AgazatiJelek_DropDownList.Visible = false;
                    Ugykor_DropDownList.Visible = false;
                    Merge_IrattariTetelszamTextBox.Visible = false;
                    UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = true;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    //labelIrattariTetelszam.Visible = true;
                    tr_ugytipus.Visible = false;
                    UgyUgyirat_Ugytipus_DropDownList.Visible = false;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelUgykorKod.Visible = false;
                }

                Iktatokonyvek_DropDownLis_Ajax.Visible = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = true;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = true;

                RequiredFieldValidator1.Enabled = false;
                RequiredFieldValidator2.Enabled = false;

                // BLG_44
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
                // BUG_1499
                //IrattariTetelszam_DropDownList.Visible = false;
                IrattariTetelszam_DropDownList.ReadOnly = true;
                #endregion
            }
            else
            {
                #region Cascading cuccok engedélyezése:
                CascadingDropDown_AgazatiJel.Enabled = true;
                CascadingDropDown_Ugykor.Enabled = true;
                CascadingDropDown_Ugytipus.Enabled = true;
                CascadingDropDown_IktatoKonyv.Enabled = true;

                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    // BUG_1499
                    tr_agazatiJel.Visible = true;
                    AgazatiJelek_DropDownList.Visible = true;
                    Ugykor_DropDownList.Visible = true;
                    Merge_IrattariTetelszamTextBox.Visible = true;
                    UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelIrattariTetelszam.Visible = false;
                    tr_ugytipus.Visible = true;
                    UgyUgyirat_Ugytipus_DropDownList.Visible = true;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelUgykorKod.Visible = true;
                }

                Iktatokonyvek_DropDownLis_Ajax.Visible = true;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

                RequiredFieldValidator1.Enabled = true;
                // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
                {
                    RequiredFieldValidator2.Enabled = true;
                }
                else RequiredFieldValidator2.Enabled = false;

                #endregion

                UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                if (Uj_Ugyirat_Hatarido_Kezeles)
                {
                    trUgyiratIntezesiIdoVisible = true;
                }
                SetIrattariTetelIktatokonyvElozmenyUgyirat(erec_UgyUgyiratok);

                // BUG_1499
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;
                IrattariTetelszam_DropDownList.ReadOnly = false;
            }

            #region Módosítás gomb állítása

            if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = "alert('" + Resources.Error.UIUgyiratNemModosithato
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }
            else
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                     , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + UgyUgyirat_Id
                     , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, ImageButton_ElozmenyUgyirat_Modositas.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }

            #endregion

            // iktatókönyv dropdown readonly, kivéve ha lezárt az elõzmény iktatókönyve
            if (iktatoKonyvLezart == true && !iktatokonyvFolyosorszamos)
            {
                // iktatokonyvek újratöltése:
                // TODO: itt majd a Cascading cuccot kéne újra engedélyezni
                //ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Iktato, EErrorPanel1);
                //ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownListByIrattariTetel(true, true, Constants.IktatoErkezteto.Iktato, erec_UgyUgyiratok.IraIrattariTetel_Id, EErrorPanel1);
            }
            else
            {
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = true;
            }

            // Figyelmeztetõ üzenetek megjelenítése:
            if (iktatoKonyvLezart == true)
            {
                tr_lezartazIktatokonyv.Visible = true;
                tr_lezartazUgyirat.Visible = false;
                if (iktatokonyvFolyosorszamos)
                {
                    labelLezart.Visible = false;
                    labelLezartFolyamatos.Visible = true;
                }
                else
                {
                    labelLezart.Visible = true;
                    labelLezartFolyamatos.Visible = false;
                }

            }
            else if (ugyiratLezart == true)
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = true;
            }
            else
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = false;
            }

            #region standard objektumfüggõ tárgyszavak ügyirathoz

            FillObjektumTargyszavaiPanel(UgyUgyirat_Id);
            if (iktatoKonyvLezart && !iktatokonyvFolyosorszamos && bFoszamraIktathat)
            {
                otpStandardTargyszavak.ReadOnly = false;
            }
            else
            {
                otpStandardTargyszavak.ReadOnly = true;
            }

            if (otpStandardTargyszavak.Count > 0)
            {
                StandardTargyszavakPanel.Visible = true;
            }
            else
            {
                StandardTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumfüggõ tárgyszavak ügyirathoz

            SetElozmenyUgyiratUI();

            #region irat határidõ
            RegisterStartupScript("setHatarido", "IrattipusOnChange();");
            #endregion irat határidõ

            #region SAKKORA
            SetObjUgyirat();
            InitializeIratHatasaDDL(true);
            #endregion

        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "Rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
        }
    }

    protected void FillObjektumTargyszavaiPanel(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak.FillObjektumTargyszavai(search, id, null
        , Constants.TableNames.EREC_UgyUgyiratok, null, null, false
        , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, true, EErrorPanel1);

        //otpStandardTargyszavak.SetDefaultValues();
        //TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
    }

    protected void FillObjektumTargyszavaiPanel_Iratok(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak_Iratok.FillObjektumTargyszavai(search, id, null
             , Constants.TableNames.EREC_IraIratok, null, null, false
             , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, true, EErrorPanel1);

        //otpStandardTargyszavak_Iratok.SetDefaultValues();
        //TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
    }

    private void SetElozmenyUgyiratUI()
    {
        // elõzményezés után már nem lehet régi adatot keresni
        //ImageButton_MigraltKereses.Enabled = false;
        //UI.SetImageButtonStyleToDisabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                  "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

    }

    private void ClearElozmenyUgyirat()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;
        MigraltUgyiratID_HiddenField.Value = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = false;
        UgyUgyirat_Targy_RequiredTextBox.ReadOnly = false;
        UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            trUgyiratIntezesiIdoVisible = true;
        }
        UgyUgyiratok_CsoportId_Felelos.ReadOnly = false;

        labelSkontroVege.Visible = false;
        UgyUgyirat_SkontroVege_CalendarControl.Visible = false;

        labelIrattarba.Visible = false;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;

        // Felelõs, Kezelõ sor látszódjon:
        UgyUgyiratok_CsoportId_Felelos.Visible = true;

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUKUgyirat.Enabled = true;
        }

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = false;
        if (Command == CommandName.New)
        {
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                 Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);
        }
        Ugyfelelos_CsoportTextBox.ReadOnly = false;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = false;
        CimekTextBoxUgyindito.ReadOnly = false;

        // Irat helye:
        labelIratHelye_Ugyirat.Visible = false;
        UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = false;

        #region Cascading cuccok engedélyezése:
        CascadingDropDown_AgazatiJel.Enabled = true;
        CascadingDropDown_Ugykor.Enabled = true;
        CascadingDropDown_Ugytipus.Enabled = true;
        CascadingDropDown_IktatoKonyv.Enabled = true;

        // Cascading cuccok kiürítése:
        CascadingDropDown_AgazatiJel.SelectedValue = String.Empty;
        CascadingDropDown_Ugykor.SelectedValue = String.Empty;
        CascadingDropDown_Ugytipus.SelectedValue = String.Empty;
        CascadingDropDown_IktatoKonyv.SelectedValue = String.Empty;

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            tr_agazatiJel.Visible = true;
            AgazatiJelek_DropDownList.Visible = true;
            Ugykor_DropDownList.Visible = true;
            Merge_IrattariTetelszamTextBox.Visible = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelIrattariTetelszam.Visible = false;
            tr_ugytipus.Visible = true;
            UgyUgyirat_Ugytipus_DropDownList.Visible = true;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelUgykorKod.Visible = true;
        }

        // BLG_44
        UGY_FAJTAJA_KodtarakDropDownList.FillAndSetEmptyValue(kcs_UGY_FAJTAJA, EErrorPanel1);
        IrattariTetelszam_DropDownList.FillAndSetEmptyValue(EErrorPanel1);
        SetIrattariTetelUI();
        UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(UGY_FAJTAJA_KodtarakDropDownList.DropDownList, EventArgs.Empty);

        Iktatokonyvek_DropDownLis_Ajax.Visible = true;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        RequiredFieldValidator1.Enabled = true;
        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
        {
            RequiredFieldValidator2.Enabled = true;
        }
        else RequiredFieldValidator2.Enabled = false;

        #endregion

        // Figyelmeztetõ üzenetek megjelenítése:
        tr_lezartazIktatokonyv.Visible = false;
        tr_lezartazUgyirat.Visible = false;

        // lezárt iktatókönyv Id törlése
        LezartIktatokonyv_HiddenField.Value = "";

        #region standard objektumfüggõ tárgyszavak ügyirathoz

        otpStandardTargyszavak.ClearTextBoxValues();
        otpStandardTargyszavak.ReadOnly = false;
        StandardTargyszavakPanel.Visible = true;

        #endregion standard objektumfüggõ tárgyszavak ügyirathoz

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
             "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
             "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
             "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = true;
        // elõzményezés után már nem lehet régi adatot keresni
        ImageButton_MigraltKereses.Enabled = true;
        UI.SetImageButtonStyleToEnabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;
        RegiAdatAzonosito_Label.Text = String.Empty;
        IrattariTetelTextBox.Text = String.Empty;
        MegkulJelzesTextBox.Text = String.Empty;
        IRJ2000TextBox.Text = String.Empty;
        UgyiratSzerelesiLista1.Reset();

        ClearUgyiratComponents();

        //#region irat határidõ
        //RegisterStartupScript("setHatarido", "IrattipusOnChange();");
        //#endregion irat határidõ

        // BLG_8826
        _iratForm.SetDefaultUgyintezesKezdete();
    }

    private void SetElozmenyRegiUgyirat()
    {
        string qsUgyiratterkep = QueryStringVars.Mode + "=" + Constants.UgyiratTerkep;
        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                                             eMigration.migralasFormUrl, qsUgyiratterkep, MigraltUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        string qsView = String.Empty;
        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(eMigration.migralasFormUrl, qsView, MigraltUgyiratID_HiddenField.ClientID,
                                Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = false;

        SetRegiadatAzonositoRow();

        SetIratHelyeCalendarControls();

        SetIrattariTetelIktatokonyvRegiUgyirat();

        #region irat határidõ
        RegisterStartupScript("setHataridoRegiUgyirat", "UgyTipusChangeHandler();");
        #endregion irat határidõ

        // BUG_13049
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
        {
            if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
            {
                IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
            }
        }
    }

    private void SetIratHelyeCalendarControls()
    {
        labelIrattarba.Visible = false;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
        labelSkontroVege.Visible = false;
        UgyUgyirat_SkontroVege_CalendarControl.Visible = false;

        // valamiért nem mûködik, ha közvetlenül a DateTextBoxba írunk
        // (Visible = "true" Style="display: none;" mellett)
        // ezért másik textboxba írással kerüljük meg a problémát
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = txtIrattarba.Text;
        UgyUgyirat_SkontroVege_CalendarControl.Text = txtSkontroVege.Text;

        if (!String.IsNullOrEmpty(UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text))
        {
            labelIrattarba.Visible = true;
            UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
        }
        else if (!String.IsNullOrEmpty(UgyUgyirat_SkontroVege_CalendarControl.Text))
        {
            labelSkontroVege.Visible = true;
            UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
        }

    }

    private void SetRegiadatAzonositoRow()
    {
        trRegiAdatAzonosito.Visible = true;
        RegiAdatAzonosito_Label.Text = regiAzonositoTextBox.Text;
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            if (!String.IsNullOrEmpty(IRJ2000TextBox.Text))
            {
                RegiAdatIRJ2000_Label.Text = " - " + IRJ2000TextBox.Text;
            }
            else
            {
                RegiAdatIRJ2000_Label.Text = String.Empty;
            }
            RegiAdatIRJ2000_Label.Visible = true;
        }
    }

    private void SetIrattariTetelIktatokonyvRegiUgyirat()
    {
        // elõkészítés: töröljük a mezõértékeket
        CascadingDropDown_AgazatiJel.SelectedValue = "";
        CascadingDropDown_IktatoKonyv.SelectedValue = "";

        // ezt nem kapjuk vissza, azért töröljük
        CascadingDropDown_Ugytipus.SelectedValue = "";

        if (!String.IsNullOrEmpty(IrattariTetelTextBox.Text))
        {
            #region irattári tétel
            string irattaritetelszam = IrattariTetelTextBox.Text;

            // Ágazati jel és azonosító meghatározása az irattári tételbõl
            EREC_IraIrattariTetelekService service_itsz = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search_itsz = new EREC_IraIrattariTetelekSearch();
            ExecParam execParam_itsz = UI.SetExecParamDefault(Page, new ExecParam());
            search_itsz.IrattariTetelszam.Value = irattaritetelszam;
            search_itsz.IrattariTetelszam.Operator = Query.Operators.equals;
            Result result_itsz = service_itsz.GetAll(execParam_itsz, search_itsz);

            if (String.IsNullOrEmpty(result_itsz.ErrorCode))
            {
                if (result_itsz.Ds.Tables[0].Rows.Count == 1)
                {
                    string Id_itsz = result_itsz.Ds.Tables[0].Rows[0]["Id"].ToString();
                    string AgazatiJel_Id = result_itsz.Ds.Tables[0].Rows[0]["AgazatiJel_Id"].ToString();

                    CascadingDropDown_AgazatiJel.SelectedValue = AgazatiJel_Id;
                    CascadingDropDown_Ugykor.SelectedValue = Id_itsz;

                    #region iktatókönyv
                    if (!String.IsNullOrEmpty(MegkulJelzesTextBox.Text))
                    {
                        string megkuljelzes = MegkulJelzesTextBox.Text;

                        // Iktatókönyv
                        EREC_IraIktatoKonyvekService service_iktato = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                        EREC_IraIktatoKonyvekSearch search_iktato = new EREC_IraIktatoKonyvekSearch();
                        ExecParam execParam_iktato = UI.SetExecParamDefault(Page, new ExecParam());
                        search_iktato.Iktatohely.Value = megkuljelzes;
                        search_iktato.Iktatohely.Operator = Query.Operators.likestart;
                        search_iktato.Ev.Value = System.DateTime.Now.Year.ToString();
                        search_iktato.Ev.Operator = Query.Operators.equals;
                        Result result_iktato = service_iktato.GetAll(execParam_iktato, search_iktato);

                        if (String.IsNullOrEmpty(result_iktato.ErrorCode))
                        {
                            if (result_iktato.Ds.Tables[0].Rows.Count == 1)
                            {
                                string Id_iktato = result_iktato.Ds.Tables[0].Rows[0]["Id"].ToString();

                                CascadingDropDown_IktatoKonyv.SelectedValue = Id_iktato;
                            }
                        }

                        // végül kiürítjük a textboxot
                        MegkulJelzesTextBox.Text = "";
                    }

                    #endregion iktatókönyv

                    Ajax_eRecord ajax_eRecord = new Ajax_eRecord();
                    string merge_itsz = ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(Id_itsz, FelhasznaloProfil.FelhasznaloId(Page));
                    Merge_IrattariTetelszamTextBox.Text = merge_itsz;
                }
            }

            // végül kiürítjük a textboxot
            IrattariTetelTextBox.Text = "";
            #endregion irattári tétel

        }

    }

    private void SetIrattariTetelIktatokonyvElozmenyUgyirat(EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        // elõkészítés: töröljük a mezõértékeket
        CascadingDropDown_AgazatiJel.SelectedValue = "";
        CascadingDropDown_IktatoKonyv.SelectedValue = "";
        CascadingDropDown_Ugytipus.SelectedValue = "";

        CascadingDropDown_IktatoKonyv.SelectedValue = "";

        string IraIrattariTetel_Id = erec_UgyUgyirat.IraIrattariTetel_Id;
        string IraIktatokonyv_Id = erec_UgyUgyirat.IraIktatokonyv_Id;
        string Ugytipus = erec_UgyUgyirat.UgyTipus;

        if (!String.IsNullOrEmpty(IraIrattariTetel_Id))
        {
            // Ágazati jel és azonosító meghatározása az irattári tételbõl
            EREC_IraIrattariTetelekService service_itsz = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search_itsz = new EREC_IraIrattariTetelekSearch();
            ExecParam execParam_itsz = UI.SetExecParamDefault(Page, new ExecParam());
            search_itsz.Id.Value = IraIrattariTetel_Id;
            search_itsz.Id.Operator = Query.Operators.equals;

            #region érvényesség ellenõrzés kikapcsolása
            search_itsz.ErvKezd.Clear();
            search_itsz.ErvVege.Clear();
            #endregion érvényesség ellenõrzés kikapcsolása

            Result result_itsz = service_itsz.GetAll(execParam_itsz, search_itsz);

            if (String.IsNullOrEmpty(result_itsz.ErrorCode))
            {
                if (result_itsz.Ds.Tables[0].Rows.Count == 1)
                {
                    string AgazatiJel_Id = result_itsz.Ds.Tables[0].Rows[0]["AgazatiJel_Id"].ToString();

                    CascadingDropDown_AgazatiJel.SelectedValue = AgazatiJel_Id;
                    CascadingDropDown_Ugykor.SelectedValue = IraIrattariTetel_Id;

                    CascadingDropDown_Ugytipus.SelectedValue = Ugytipus;

                    #region iktatókönyv

                    // Iktatókönyv
                    EREC_IraIktatoKonyvekService service_iktato = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                    ExecParam execParam_iktato_get = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_iktato_get.Record_Id = IraIktatokonyv_Id;

                    Result result_iktato_get = service_iktato.Get(execParam_iktato_get);
                    if (String.IsNullOrEmpty(result_iktato_get.ErrorCode))
                    {
                        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_iktato_get.Record;

                        // elvileg csak lezártakra hívjuk...
                        if (!Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek))
                        {
                            CascadingDropDown_IktatoKonyv.SelectedValue = IraIktatokonyv_Id;
                        }
                        else
                        {
                            EREC_IraIktatoKonyvekSearch search_iktato = new EREC_IraIktatoKonyvekSearch();
                            ExecParam execParam_iktato = UI.SetExecParamDefault(Page, new ExecParam());

                            if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes))
                            {
                                search_iktato.MegkulJelzes.Value = erec_IraIktatoKonyvek.MegkulJelzes;
                                search_iktato.MegkulJelzes.Operator = Query.Operators.equals;
                            }
                            else
                            {
                                search_iktato.MegkulJelzes.Value = "";
                                search_iktato.MegkulJelzes.Operator = Query.Operators.isnull;
                            }
                            search_iktato.Iktatohely.Value = erec_IraIktatoKonyvek.Iktatohely;
                            search_iktato.Iktatohely.Operator = Query.Operators.equals;
                            search_iktato.Ev.Value = System.DateTime.Now.Year.ToString();
                            search_iktato.Ev.Operator = Query.Operators.equals;
                            Result result_iktato = service_iktato.GetAll(execParam_iktato, search_iktato);

                            if (String.IsNullOrEmpty(result_iktato.ErrorCode))
                            {
                                if (result_iktato.Ds.Tables[0].Rows.Count == 1)
                                {
                                    string Id_iktato = result_iktato.Ds.Tables[0].Rows[0]["Id"].ToString();

                                    CascadingDropDown_IktatoKonyv.SelectedValue = Id_iktato;
                                }
                            }
                        }


                    }
                    #endregion iktatókönyv
                }
            }
        }

        Ajax_eRecord ajax_eRecord = new Ajax_eRecord();
        string merge_itsz = ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(IraIrattariTetel_Id, FelhasznaloProfil.FelhasznaloId(Page));
        Merge_IrattariTetelszamTextBox.Text = merge_itsz;

    }

    #endregion


    public void ReLoadTab()
    {

        FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Irat')";
        //blg 1868	FunkcioGombsor.HitelesitesiZaradekOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm ( "HitelesitesiZaradekFormTabPrintForm.aspx?" + QueryStringVars.IratId + "=" + ParentId );

        FunkcioGombsor.HitelesitesiZaradekOnClientClick = "javascript:window.open('HitelesitesizaradekFormSSRS.aspx?" + QueryStringVars.IratId + "=" + ParentId + "')";

        FunkcioGombsor.EMailNyomtatasOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("eMail_PrintForm.aspx?" + QueryStringVars.IratId + "=" + ParentId);
        //// CR 3058
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            FunkcioGombsor.VonalkodNyomtatasa_OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.IratId + "=" + ParentId + "&tipus=Irat&" + QueryStringVars.Command + "=Modify" + "')";
        //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny");
        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            FunkcioGombsor.ParentForm = _ParentForm;

            String irat_PostazasIranya = "";

            //// default ügyiratdarab eljárási szakasz
            //UgyiratdarabEljarasiSzakasz_HiddenField.Value = KodTarak.ELJARASI_SZAKASZ.Osztatlan;


            if (Command == CommandName.New)
            {
                #region (Iktatás)

                // Components Init
                InitElozmenySearchComponent();

                // EB 2010.01.22, CR#2284: ügyintézõ megadása rendszerparamétertõl függõen kötelezõ
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                     Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);
                //// BUG_10524
                //if ((Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
                //    && (! String.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.Text)))
                //{
                //    Pld_VisszaerkezesiHatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                //    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = UgyUgyirat_Hatarido_CalendarControl.TextBox.ToolTip;
                //}
                //else
                //{
                //    //DateTime _30DaysAfterToday = DateTime.Now.AddDays(30);
                //    //Pld_VisszaerkezesiHatarido_CalendarControl.Text = _30DaysAfterToday.ToShortDateString();
                //    string errorMessage = String.Empty;
                //    Pld_VisszaerkezesiHatarido_CalendarControl.Text = GetDefaultVisszaerkezesiHataridoPld(out errorMessage);
                //    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = errorMessage;
                //}
                // Kezelõ kitöltve alapból a felhasználóra:
                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);
                // eljárási szakasz irat határidõhöz (alapértelmezett)
                UgyiratdarabEljarasiSzakasz_HiddenField.Value = KodTarak.ELJARASI_SZAKASZ.Osztatlan;



                if (!IsPostBack)
                {
                    #region Template betöltés, ha kell

                    string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);
                    if (!String.IsNullOrEmpty(templateId))
                    {
                        FormHeader.LoadTemplateObjectById(templateId);
                        LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader.TemplateObject);

                    }

                    #endregion

                    // elõrehozva, mert a küldemény elõkészített iratából még felülírhatjuk
                    #region kódtár dropdownlistek feltöltése

                    IraIrat_Irattipus_KodtarakDropDownList.FillDropDownList(kcs_IRATTIPUS, true, EErrorPanel1);

                    string IratJelleg = KodTarak.IRAT_JELLEG.Webes; // default
                    if (Mode == CommandName.KimenoEmailIktatas)
                    {
                        IratJelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                    }
                    IratJellegKodtarakDropDown.FillWithOneValue(kcs_IRAT_JELLEG, IratJelleg, EErrorPanel1);

                    //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, EErrorPanel1);

                    ktDropDownListIratMinosites.FillDropDownList(kcs_IRATMINOSITES, true, EErrorPanel1);

                    if (Mode == CommandName.KimenoEmailIktatas)
                    {
                        // BLG_361
                        // Nincs beállított érték
                        //Pld_UgyintezesModja_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_ALAPJA
                        //    , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, EErrorPanel1);
                        // BUG_11613
                        //Pld_UgyintezesModja_KodtarakDropDownList.FillWithOneValue(kcs_ELSODLEGES_ADATHORDOZO
                        //    , "", EErrorPanel1);
                        Pld_UgyintezesModja_KodtarakDropDownList.FillWithOneValue(kcs_ELSODLEGES_ADATHORDOZO
                            , KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat, EErrorPanel1);
                        AdathordozoTipusa_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_ALAPJA
                            , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, EErrorPanel1);
                        Pld_IratpeldanyFajtaja_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_ALAPJA
                            , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, EErrorPanel1);
                    }
                    else if (Pld_UgyintezesModja_KodtarakDropDownList.DropDownList.Items.Count < 1)
                    {
                        // BLG_361
                        // Pld_UgyintezesModja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, false, EErrorPanel1);
                        Pld_UgyintezesModja_KodtarakDropDownList.FillDropDownList(kcs_ELSODLEGES_ADATHORDOZO, true, EErrorPanel1);

                        AdathordozoTipusa_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, false, EErrorPanel1);

                        // Az iratpéldány fajtájánál a listából le kell venni a 'Vegyes' értéket:
                        List<string> ktUgyintezesAlapjaFilterList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYINTEZES_ALAPJA, this.Page, null)
                                            .Where(kt => kt.Kod != KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                                            .Select(kt => kt.Kod)
                                            .ToList();
                        Pld_IratpeldanyFajtaja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, ktUgyintezesAlapjaFilterList, false, EErrorPanel1);
                    }

                    // BLG_44
                    UGY_FAJTAJA_KodtarakDropDownList.FillDropDownList(kcs_UGY_FAJTAJA, true, EErrorPanel1);
                    IrattariTetelszam_DropDownList.FillDropDownList(true, EErrorPanel1);

                    #endregion kódtár dropdownlistek feltöltése

                    // van-e megadva küldemény az URL paraméterei között?
                    if (!String.IsNullOrEmpty(kuldemenyId))
                    {
                        Kuldemeny_Id_HiddenField.Value = kuldemenyId;
                        ImageButton_KuldemenyKereses.Enabled = false;
                        UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyKereses);

                        ImageButton_KuldemenyErkeztetes.Enabled = false;
                        UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyErkeztetes);

                        LoadKuldemenyComponents();
                    }

                    if (Uj_Ugyirat_Hatarido_Kezeles)
                    {
                        trUgyiratIntezesiIdoVisible = true;

                        IntezesiIdo_KodtarakDropDownList.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, EErrorPanel1);

                        List<string> filterList = new List<string>();
                        filterList.Add(KodTarak.IDOEGYSEG.Nap);
                        filterList.Add(KodTarak.IDOEGYSEG.Munkanap);
                        IntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);

                        // BLG_1020
                        trIratIntezesiIdo.Visible = true;

                        IratIntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);

                        InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
                    }

                    SetDefaultHataridok();

                    #region Email iktatás (Kimenõ email)
                    if (Mode == CommandName.KimenoEmailIktatas)
                    {
                        // az EmailBoritekokId url paramétert meg kell adni
                        if (String.IsNullOrEmpty(EmailBoritekokId))
                        {
                            // hiba:
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.UINoEmailBoritekokIdParam);
                            ErrorUpdatePanel1.Update();
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            SetEmailBoritekAdatok();
                        }
                    }
                    #endregion

                    // -> elõretéve, mert a küldemény elõkészített iratából még felülírhatjuk
                    //IraIrat_Kategoria_KodtarakDropDownList.FillDropDownList(kcs_IRATKATEGORIA, true, EErrorPanel1);
                    //IraIrat_Irattipus_KodtarakDropDownList.FillDropDownList(kcs_IRATTIPUS, true, EErrorPanel1);

                    //string IratJelleg = KodTarak.IRAT_JELLEG.Webes; // default
                    //if (Mode == CommandName.KimenoEmailIktatas)
                    //{
                    //    IratJelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                    //}
                    //IratJellegKodtarakDropDown.FillWithOneValue(kcs_IRAT_JELLEG, IratJelleg, EErrorPanel1);

                    ////IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, EErrorPanel1);

                    //ktDropDownListIratMinosites.FillDropDownList(kcs_IRATMINOSITES, true, EErrorPanel1);

                    //if (Mode == CommandName.KimenoEmailIktatas)
                    //{
                    //    Pld_UgyintezesModja_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_ALAPJA
                    //        , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, EErrorPanel1);
                    //    AdathordozoTipusa_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_ALAPJA
                    //        , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, EErrorPanel1);
                    //}
                    //else if (Pld_UgyintezesModja_KodtarakDropDownList.DropDownList.Items.Count < 1)
                    //{
                    //    Pld_UgyintezesModja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, false, EErrorPanel1);
                    //    AdathordozoTipusa_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, false, EErrorPanel1);
                    //}

                    #region ÁtIktatás
                    string iratId = null;
                    if (Mode == CommandName.AtIktatas)
                    {
                        iratId = Request.QueryString.Get(QueryStringVars.IratId);
                        if (String.IsNullOrEmpty(iratId))
                        {
                            ResultError.DisplayNoIdParamError(EErrorPanel1);
                            ErrorUpdatePanel1.Update();
                            MainPanel.Visible = false;
                            return;
                        }

                        EREC_IraIratok irat_atIktatas = null;
                        EREC_UgyUgyiratdarabok ugyiratDarab_atIktatas = null;
                        EREC_UgyUgyiratok ugyirat_atIktatas = null;
                        EREC_PldIratPeldanyok elsoIratPeldany_atIktatas = null;

                        bool success = Iratok.GetObjectsHierarchyByIrat(UI.SetExecParamDefault(Page, new ExecParam()), iratId
                             , out irat_atIktatas, out ugyiratDarab_atIktatas, out ugyirat_atIktatas, out elsoIratPeldany_atIktatas, EErrorPanel1);
                        if (success == false)
                        {
                            return;
                        }

                        #region Ellenõrzés, átiktatható-e?

                        Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(irat_atIktatas, elsoIratPeldany_atIktatas);
                        Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyirat_atIktatas);
                        ErrorDetails errorDetail = null;

                        // Már nem csak bejövõ iratokra, hanem bármilyen iratra kell mennie
                        if (Iratok.AtIktathato(iratStatusz, ugyiratStatusz, UI.SetExecParamDefault(Page), out errorDetail) == false
                             //|| irat_atIktatas.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo
                             //|| String.IsNullOrEmpty(irat_atIktatas.KuldKuldemenyek_Id))
                             )
                        {
                            // Nem lehet átiktatni:
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(52621, errorDetail));
                            ErrorUpdatePanel1.Update();
                            MainPanel.Visible = false;
                            return;
                        }

                        #endregion Ellenõrzés, átiktatható-e?

                        if (irat_atIktatas.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                        {
                            #region Küldemény komponens feltöltése:

                            Kuldemeny_Id_HiddenField.Value = irat_atIktatas.KuldKuldemenyek_Id;
                            LoadKuldemenyComponents();

                            ImageButton_KuldemenyKereses.Enabled = false;
                            UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyKereses);

                            ImageButton_KuldemenyErkeztetes.Enabled = false;
                            UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyErkeztetes);

                            ImageButton_KuldemenyModositas.Enabled = false;
                            UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyModositas);

                            #endregion
                        }

                        #region régi irat adatainak felajánlása
                        // Felkínáljuk az eredeti ügyiratnál megadott jellemzõket alapértelmezett értéknek:

                        UgyUgyirat_Targy_RequiredTextBox.Text = ugyirat_atIktatas.Targy;
                        IraIrat_Targy_RequiredTextBox.Text = irat_atIktatas.Targy;

                        Ugyfelelos_CsoportTextBox.Id_HiddenField = ugyirat_atIktatas.Csoport_Id_Ugyfelelos;
                        Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = ugyirat_atIktatas.FelhasznaloCsoport_Id_Ugyintez;
                        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                        //LZS - BUG_7834
                        Pld_BarCode_VonalKodTextBox.Text = ugyirat_atIktatas.BARCODE;

                        //// Irat kategória
                        //ListItem item_kategoria = IraIrat_Kategoria_KodtarakDropDownList.DropDownList.Items.FindByValue(
                        //    irat_atIktatas.Kategoria);
                        //if (item_kategoria != null)
                        //{
                        //    IraIrat_Kategoria_KodtarakDropDownList.DropDownList.SelectedValue = irat_atIktatas.Kategoria;
                        //}

                        // Irattípus:
                        ListItem item_irattipus = IraIrat_Irattipus_KodtarakDropDownList.DropDownList.Items.FindByValue(
                                irat_atIktatas.Irattipus);
                        if (item_irattipus != null)
                        {
                            IraIrat_Irattipus_KodtarakDropDownList.DropDownList.SelectedValue = irat_atIktatas.Irattipus;
                        }

                        //Iratjelleg
                        IratJellegKodtarakDropDown.FillWithOneValue(kcs_IRAT_JELLEG, irat_atIktatas.Jelleg, EErrorPanel1);

                        if (irat_atIktatas.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                        {
                            // Elsõ iratpéldány lekérése, adatainak megjelenítése

                            EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParam_elsoPeldanyGet = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result_elsoPeldanyGet = erec_PldIratPeldanyokService.GetElsoIratPeldanyByIraIrat(
                                      execParam_elsoPeldanyGet, iratId);

                            if (String.IsNullOrEmpty(result_elsoPeldanyGet.ErrorCode))
                            {
                                if (result_elsoPeldanyGet.Record == null)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(51104));
                                    ErrorUpdatePanel1.Update();
                                }
                                else
                                {
                                    EREC_PldIratPeldanyok elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoPeldanyGet.Record;
                                    obj_elsoIratPeldany = elsoIratPeldany;

                                    Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(
                                         kcs_KULDEMENY_KULDES_MODJA, elsoIratPeldany.KuldesMod, false, EErrorPanel1);

                                    // adatok megjelenítése:
                                    Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = elsoIratPeldany.Partner_Id_Cimzett;
                                    Pld_PartnerId_Cimzett_PartnerTextBox.Text = elsoIratPeldany.NevSTR_Cimzett;

                                    Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = elsoIratPeldany.Cim_id_Cimzett;
                                    Pld_CimId_Cimzett_CimekTextBox.Text = elsoIratPeldany.CimSTR_Cimzett;

                                    Pld_BarCode_VonalKodTextBox.Text = elsoIratPeldany.BarCode;
                                    //BLG 1974
                                    //Amennyiben a VONALKOD_KEZELES = SAVOS és a VONALKOD_KIMENO_GENERALT értéke is 1-es, akkor belsõ keletkezésû/kimenõ irat iktatása esetén nem kötelezõ a vonalkód mezõ
                                    ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
                                    string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
                                    string vonalkodKiGeneralt = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KIMENO_GENERALT).Trim();

                                    // ha van megadva vonalkód, readonly, és nincs validálás:
                                    if (!string.IsNullOrEmpty(Pld_BarCode_VonalKodTextBox.Text))
                                    {
                                        Pld_BarCode_VonalKodTextBox.ReadOnly = true;
                                        Pld_BarCode_VonalKodTextBox.Validate = false;
                                    }
                                    else
                                    {
                                        //BLG 1974
                                        if (vonalkodkezeles.ToUpper().Trim().Equals("SAVOS") && vonalkodKiGeneralt.Trim().Equals("1") && Mode == CommandName.BelsoIratIktatas)
                                        {
                                            Pld_BarCode_VonalKodTextBox.Validate = false;
                                            Label_Pld_VonalkodElottCsillag.Visible = false;
                                        }
                                        Pld_BarCode_VonalKodTextBox.ReadOnly = false;
                                    }

                                    AdathordozoTipusa_KodtarakDropDownList.SelectedValue = irat_atIktatas.AdathordozoTipusa;
                                    Pld_UgyintezesModja_KodtarakDropDownList.SelectedValue = irat_atIktatas.UgyintezesAlapja;
                                    Pld_IratpeldanyFajtaja_KodtarakDropDownList.SelectedValue = elsoIratPeldany.UgyintezesModja;
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_elsoPeldanyGet);
                                ErrorUpdatePanel1.Update();
                            }
                        }

                        #endregion

                        irat_PostazasIranya = irat_atIktatas.PostazasIranya;

                        // Ha ügyirat utolsó iratát készülünk átiktatni, fel kell ajánlani az ügyirat sztornózását
                        if (ugyirat_atIktatas.IratSzam == "1")
                        {
                            string ugyiratSztornoJs = "if (confirm('" + Resources.Question.UISztornoUresUgyirat + @"'))
                                {document.getElementById('" + this.Storno_Empty_Ugyirat.ClientID + @"').value='1';}
                                else
                                {document.getElementById('" + this.Storno_Empty_Ugyirat.ClientID + @"').value='0';}";

                            this.TabFooter1.ImageButton_Save.OnClientClick += ugyiratSztornoJs;
                        }
                    }

                    #endregion Átiktatás



                    bool vanUgyirat = false;
                    //bool vanUgyiratDarab = false;

                    // Van-e megadva ügyirat alszámra iktatáshoz?
                    String ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
                    if (!String.IsNullOrEmpty(ugyiratId))
                    {
                        trElozmenySearch.Visible = false;
                        ElozmenyUgyiratID_HiddenField.Value = ugyiratId;
                        SearchElozmenyUgyirat(ugyiratId, false);
                        //SetElozmenyUgyirat();
                        vanUgyirat = true;
                    }



                    // komponensek inicializálása:

                    if (vanUgyirat == false)
                    {
                        //UgyUgyirat_Ugytipus_KodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, true, EErrorPanel1);
                        //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.FillDropDownList(kcs_IKTATOSZAM_KIEG, true, EErrorPanel1);
                    }
                    else
                    {
                        ImageButton_Elozmeny.Enabled = false;
                        UI.SetImageButtonStyleToDisabled(ImageButton_Elozmeny);
                    }

                    if (Mode != CommandName.ElokeszitettIratIktatas)
                    {
                        #region standard objektumfüggõ tárgyszavak ügyirathoz
                        if (vanUgyirat == true)
                        {
                            //standardObjektumTargyszavak.FillStandardTargyszavak(ugyiratId, null, Constants.TableNames.EREC_UgyUgyiratok, EErrorPanel1);
                            //standardObjektumTargyszavak.ReadOnly = true;

                            //if (standardObjektumTargyszavak.Count > 0)
                            //{
                            //    StandardTargyszavakPanel.Visible = true;
                            //}
                            //else
                            //{
                            //    StandardTargyszavakPanel.Visible = false;
                            //}
                        }
                        else
                        {
                            FillObjektumTargyszavaiPanel(null);
                            if (otpStandardTargyszavak.Count > 0)
                            {
                                StandardTargyszavakPanel.Visible = true;
                                otpStandardTargyszavak.SetDefaultValues();
                                TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
                            }
                            else
                            {
                                StandardTargyszavakPanel.Visible = false;
                            }
                        }
                        #endregion standard objektumfüggõ tárgyszavak ügyirathoz



                        #region standard objektumfüggõ tárgyszavak irathoz
                        FillObjektumTargyszavaiPanel_Iratok(iratId); // null vagy az átiktatott irat id
                        if (otpStandardTargyszavak_Iratok.Count > 0)
                        {
                            StandardTargyszavakPanel_Iratok.Visible = true;
                            otpStandardTargyszavak_Iratok.SetDefaultValues();
                            TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                            TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
                        }
                        else
                        {
                            StandardTargyszavakPanel_Iratok.Visible = false;
                        }

                        #endregion standard objektumfüggõ tárgyszavak irathoz
                    }

                    if (Mode == CommandName.BelsoIratIktatas || Mode == CommandName.KimenoEmailIktatas)
                    {
                        Pld_Visszavarolag_KodtarakDropDownList.FillDropDownList(kcs_VISSZAVAROLAG, true, EErrorPanel1);

                        // Küldés módja feltöltése
                        if (Mode == CommandName.KimenoEmailIktatas)
                        {
                            Pld_KuldesMod_KodtarakDropDownList.FillWithOneValue(kcs_KULDEMENY_KULDES_MODJA
                                 , KodTarak.KULDEMENY_KULDES_MODJA.E_mail, EErrorPanel1);

                        }
                        else
                        {
                            Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, false, EErrorPanel1);
                        }
                    }
                    // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                    // CR 3110 : Itt nem történik semmi, ezért nem raktam be a Munkapéldány-figyelést
                    else if (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.AtIktatas)
                    {

                        //#region Felelõst (Kezelõ mezõ) kitöltése az iktató szervezeti egység vezetõjére:

                        //if (String.IsNullOrEmpty(UgyUgyiratok_CsoportId_Felelos.Id_HiddenField))
                        //{

                        //    string szervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);

                        //    // Megadott szervezet vezetõjének lekérdezése:
                        //    Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
                        //        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                        //    ExecParam execParam_getLeader = UI.SetExecParamDefault(Page, new ExecParam());

                        //    Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, szervezetId);
                        //    if (String.IsNullOrEmpty(result_GetLeader.ErrorCode) && result_GetLeader.Record != null)
                        //    {
                        //        KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

                        //        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = csoportVezeto.Id;
                        //        UgyUgyiratok_CsoportId_Felelos.Text = csoportVezeto.Nev;
                        //    }
                        //}                        

                        //#endregion


                        //// Felirat kirakása a Kezelõ helyére (ha még nincs kitöltve a kezelõ)
                        //if (String.IsNullOrEmpty(UgyUgyiratok_CsoportId_Felelos.Id_HiddenField))
                        //{
                        //    Label_KezeloSzignJegyzekAlapjan.Visible = true;
                        //    UgyUgyiratok_CsoportId_Felelos.Visible = false;
                        //}


                        //if (Iratok.SZIGNALASIFOLYAMAT_CERTOP == false)
                        //{
                        //    // FPH-nak

                        //    tr_felelos_kezelo.Visible = false;
                        //}

                    }

                    InitializeIratHatasaDDL(true);
                    KodtarakDropDownListUgyiratFelfuggesztesOka.FillDropDownList(KCS_FELFUGGESZTESOKA, true, EErrorPanel1);
                    InitializeLezarasOkaDDL(null, null);

                    if (isTUKRendszer)
                    {
                        if (Mode == CommandName.BejovoIratIktatas)
                        {
                            if (obj_Ugyirat != null)
                            {
                                ReloadIrattariHelyLevelekDropDownTUK(Ugyfelelos_CsoportTextBox.HiddenField.Value, obj_Ugyirat.IrattarId, true);
                            }
                            else
                            {
                                ReloadIrattariHelyLevelekDropDownTUK(Ugyfelelos_CsoportTextBox.HiddenField.Value, null, true);
                            }
                        }
                        else if (Mode == CommandName.BelsoIratIktatas && !string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)) && obj_Ugyirat != null && IrattariHelyLevelekDropDownTUKUgyirat.Enabled == false)
                        {
                            ReloadIrattariHelyLevelekDropDownTUK(Ugyfelelos_CsoportTextBox.HiddenField.Value, obj_Ugyirat.IrattarId, true);
                        }
                    }

                    //Iratfordulat
                    if (Mode == CommandName.BelsoIratIktatas)
                    {
                        iratId = Request.QueryString.Get(QueryStringVars.IratId);
                        if (!String.IsNullOrEmpty(iratId))
                        {
                            iratId = Request.QueryString.Get(QueryStringVars.IratId);
                            LoadIratfordulatAdatok(iratId);
                        }
                    }

                    // BUG_8001
                    // BUG_8765
                    //if (Mode == CommandName.BelsoIratIktatas && IsIktatasBelsoUgyintezoIktato)
                    if (Mode == CommandName.BelsoIratIktatas && IsIktatasBelsoUgyintezoIktato && !IsTUK_SZIGNALAS_ENABLED)

                    {
                        // a bejelentkezett felhasználót kell beírni az Irat ügyintéző mezőbe, és le kell tiltani a módosíthatóságát
                        var felhasznalo = FelhasznaloProfil.FelhasznaloId(Page);
                        Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = felhasznalo;
                        Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                        Irat_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;

                        // Felelős szervezetnek tagja?
                        string felelosSzervezet = Iratfelelos_CsoportTextBox.Id_HiddenField;
                        if (!String.IsNullOrEmpty(felelosSzervezet))
                        {
                            if (!Contentum.eUtility.Csoportok.IsMember(felhasznalo, felelosSzervezet))
                            {
                                Iratfelelos_CsoportTextBox.Clear();
                            }
                        }
                        else
                        {
                            #region 11580
                            Iratfelelos_CsoportTextBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
                            Iratfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                            FelelosSzervezetVezetoTextBoxInstance.SetSzervezet(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                            #endregion 11580
                        }

                        Iratfelelos_CsoportTextBox.CsoportTagId = FelhasznaloProfil.FelhasznaloId(Page);
                    }
                }

                // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján 
                if ((Mode == CommandName.BejovoIratIktatas) || (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo))
                {
                    TabFooter1.ImageButton_Save.OnClientClick = JavaScripts.SetOnClientClick_Check_HiddenfieldIsNotEmpty(
                         Kuldemeny_Id_HiddenField.ClientID) + otpStandardTargyszavak.GetCheckErtekJavaScript();
                }

                //elõkészített irat iktatása
                if (Mode == CommandName.ElokeszitettIratIktatas)
                {
                    #region Elõkészített irat beiktatása

                    if (String.IsNullOrEmpty(ParentId))
                    {
                        // nincs Id megadva:
                        ResultError.DisplayNoIdParamError(EErrorPanel1);
                        ErrorUpdatePanel1.Update();
                    }
                    else
                    {
                        // rekord betöltése

                        if (obj_Irat == null)
                        {
                            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = ParentId;

                            Result result = service.Get(execParam);
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                                obj_Irat = erec_IraIratok;

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }

                        if (obj_Irat != null)
                        {
                            // Funkciójog ellenõrzés, attól függõen, hogy bejövõ, vagy belsõ a munkairat:                            
                            if ((obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo
                                      && !FunctionRights.GetFunkcioJog(Page, funkcio_MunkapeldanyBeiktatas_Bejovo))
                                || (obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
                                      && !FunctionRights.GetFunkcioJog(Page, funkcio_MunkapeldanyBeiktatas_Belso))
                                 )
                            {
                                // Nincs joga:
                                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                                MainPanel.Visible = false;
                                return;
                            }

                            bool nemIktathatFoszamraBelso = FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra);
                            bool nemIktathatFoszamraBejovo = FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra);
                            if (nemIktathatFoszamraBelso || nemIktathatFoszamraBejovo)
                            {
                                // ha munkaügyirat tartozik hozzá, nincs joga élesíteni (mert nem hozhat létre fõszámot)
                                if (obj_Ugyirat == null && !String.IsNullOrEmpty(obj_Irat.Ugyirat_Id))
                                {
                                    // ügyirat lekérése és betöltése
                                    EREC_UgyUgyiratokService erec_UgyUgyiratokService =
                                         eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                    ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam_ugyiratGet.Record_Id = obj_Irat.Ugyirat_Id;

                                    Result result_UgyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);

                                    if (result_UgyiratGet.IsError)
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_UgyiratGet);
                                        ErrorUpdatePanel1.Update();
                                        MainPanel.Visible = false;
                                        return;
                                    }

                                    obj_Ugyirat = (EREC_UgyUgyiratok)result_UgyiratGet.Record;
                                }

                                if (obj_Ugyirat != null)
                                {
                                    bool isMunkairat = Ugyiratok.Munkaanyag(obj_Ugyirat);

                                    if (isMunkairat)
                                    {
                                        if (nemIktathatFoszamraBelso && obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                                        {
                                            //"Munkairat élesítés (belsõ): Önnek nincs joga fõszámot létrehozni, ezért a mûvelet nem végezhetõ el."
                                            // Nincs joga:
                                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFunctionRightsError, Resources.Error.UIDontHaveFunctionRight_MunkairatElesitesBelso);
                                            MainPanel.Visible = false;
                                            return;
                                        }
                                        else if (nemIktathatFoszamraBejovo && obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                                        {
                                            //"Munkairat élesítés (bejövõ): Önnek nincs joga fõszámot létrehozni, ezért a mûvelet nem végezhetõ el."
                                            // Nincs joga:
                                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFunctionRightsError, Resources.Error.UIDontHaveFunctionRight_MunkairatElesitesBejovo);
                                            MainPanel.Visible = false;
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                                    ResultError.CreateNewResultWithErrorCode(50101));
                                    ErrorUpdatePanel1.Update();
                                    MainPanel.Visible = false;
                                    return;
                                }
                            }

                            LoadComponentsFromBusinessObject(obj_Irat);
                            irat_PostazasIranya = obj_Irat.PostazasIranya;


                            #region standard objektumfüggõ tárgyszavak ügyirathoz
                            //if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                            //{
                            //    StandardTargyszavakPanel.Visible = false;
                            //}
                            //else
                            //{
                            //    FillObjektumTargyszavaiPanel(ElozmenyUgyiratID_HiddenField.Value);
                            //    otpStandardTargyszavak.ReadOnly = true;

                            //    if (otpStandardTargyszavak.Count > 0)
                            //    {
                            //        StandardTargyszavakPanel.Visible = true;
                            //    }
                            //    else
                            //    {
                            //        StandardTargyszavakPanel.Visible = false;
                            //    }
                            //}

                            #endregion standard objektumfüggõ tárgyszavak ügyirathoz

                            #region standard objektumfüggõ tárgyszavak irathoz
                            FillObjektumTargyszavaiPanel_Iratok(ParentId);
                            otpStandardTargyszavak_Iratok.ReadOnly = (Command == CommandName.View);

                            if (otpStandardTargyszavak_Iratok.Count > 0)
                            {
                                StandardTargyszavakPanel_Iratok.Visible = true;
                            }
                            else
                            {
                                StandardTargyszavakPanel_Iratok.Visible = false;
                            }

                            #endregion standard objektumfüggõ tárgyszavak irathoz


                            if (obj_Ugyirat != null)
                            {
                                bool isMunkairat = Ugyiratok.Munkaanyag(obj_Ugyirat);

                                if (isMunkairat)
                                {
                                    JavasoltElozmenyTextBox.ReadOnly = true;
                                    if (JavasoltElozmenyTextBox.CssClass.Contains("ReadOnlyWebControl") == false)
                                    {
                                        JavasoltElozmenyTextBox.CssClass += " ReadOnlyWebControl";
                                    }
                                    tr_JavasoltElozmeny.Visible = true;
                                }
                            }

                            //Lezárt iktatókönyvbe nem lehet iktatni
                            if (obj_Iktatokonyv != null)
                            {
                                bool iktatoKonyvLezart = Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(obj_Iktatokonyv);

                                if (iktatoKonyvLezart)
                                {
                                    bool iktatokonyvFolyosorszamos = false;
                                    bool isMunkairat = true;

                                    if (obj_Ugyirat != null)
                                    {
                                        isMunkairat = Ugyiratok.Munkaanyag(obj_Ugyirat);

                                        if (!isMunkairat)
                                        {
                                            iktatokonyvFolyosorszamos = GetIktatokonyvFolyosorszamos(obj_Ugyirat, obj_Iktatokonyv);
                                        }
                                    }

                                    if (isMunkairat || !iktatokonyvFolyosorszamos)
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "Lezárt iktatókönybe nem lehet iktatni!");
                                        ErrorUpdatePanel1.Update();
                                        MainPanel.Visible = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                            ResultError.CreateNewResultWithErrorCode(50101));
                            ErrorUpdatePanel1.Update();
                        }
                    }
                    #endregion
                }
                #endregion
                // BUG_10524
                if ((Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
                    && (!String.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.Text)))
                {
                    Pld_VisszaerkezesiHatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = UgyUgyirat_Hatarido_CalendarControl.TextBox.ToolTip;
                }
                else
                {
                    //DateTime _30DaysAfterToday = DateTime.Now.AddDays(30);
                    //Pld_VisszaerkezesiHatarido_CalendarControl.Text = _30DaysAfterToday.ToShortDateString();
                    string errorMessage = String.Empty;
                    Pld_VisszaerkezesiHatarido_CalendarControl.Text = GetDefaultVisszaerkezesiHataridoPld(out errorMessage);
                    Pld_VisszaerkezesiHatarido_CalendarControl.TextBox.ToolTip = errorMessage;
                }
            }
            else if (Command == CommandName.View || Command == CommandName.Modify)
            {
                // BUG_8765
                //if (IsIktatasBelsoUgyintezoIktato) // BUG_8001
                if (IsIktatasBelsoUgyintezoIktato || IsTUK_SZIGNALAS_ENABLED)
                {
                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;
                }

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {

                    // rekord betöltése

                    if (obj_Irat == null)
                    {
                        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = ParentId;

                        Result result = service.Get(execParam);
                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                            obj_Irat = erec_IraIratok;

                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();
                        }
                    }

                    if (obj_Irat != null)
                    {
                        LoadComponentsFromBusinessObject(obj_Irat);

                        irat_PostazasIranya = obj_Irat.PostazasIranya;

                        #region BUG_4968
                        //LZS - Ha az irat postázási iránya bejövő, akkor tiltjuk a controlt, nem lehet módosítani.
                        if (irat_PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                        {
                            Irat_Iranya_DropDownList.ReadOnly = true;
                        }
                        else
                        {
                            //LZS BUG_4968
                            //LZS - Ha az irat postázási iránya kimenő vagy belső akkor tiltjuk engedélyezzük a controlt, lehet módosítani, de csak kimenő-re és belső-re.
                            Irat_Iranya_DropDownList.ReadOnly = false;
                            Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, KodTarak.POSTAZAS_IRANYA.Belso, EErrorPanel1);

                            //Eltávolítjuk a bejövő lehetőséget a lenyíló listából.
                            Irat_Iranya_DropDownList.DropDownList.Items.Remove(Irat_Iranya_DropDownList.DropDownList.Items.FindByValue("1"));
                        }
                        #endregion

                        #region standard objektumfüggõ tárgyszavak ügyirathoz
                        if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                        {
                            StandardTargyszavakPanel.Visible = false;
                        }
                        else
                        {
                            FillObjektumTargyszavaiPanel(ElozmenyUgyiratID_HiddenField.Value);
                            otpStandardTargyszavak.ReadOnly = true;

                            if (otpStandardTargyszavak.Count > 0)
                            {
                                StandardTargyszavakPanel.Visible = true;
                            }
                            else
                            {
                                StandardTargyszavakPanel.Visible = false;
                            }
                        }

                        #endregion standard objektumfüggõ tárgyszavak ügyirathoz

                        #region standard objektumfüggõ tárgyszavak irathoz
                        FillObjektumTargyszavaiPanel_Iratok(ParentId);
                        otpStandardTargyszavak_Iratok.ReadOnly = (Command == CommandName.View);

                        if (otpStandardTargyszavak_Iratok.Count > 0)
                        {
                            StandardTargyszavakPanel_Iratok.Visible = true;
                        }
                        else
                        {
                            StandardTargyszavakPanel_Iratok.Visible = false;
                        }

                        FillTipusosObjektumTargyszavaiPanel_Iratok(ParentId);

                        #endregion standard objektumfüggõ tárgyszavak irathoz
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                        ResultError.CreateNewResultWithErrorCode(50101));
                        ErrorUpdatePanel1.Update();
                    }

                    if (obj_elsoIratPeldany == null && obj_Irat != null)
                    {
                        #region Elsõ iratpéldány lekérése

                        EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                        Result result_elsoPldGet = service_Pld.GetElsoIratPeldanyByIraIrat(UI.SetExecParamDefault(Page)
                             , obj_Irat.Id);
                        if (result_elsoPldGet.IsError)
                        {
                            // hiba:
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_elsoPldGet);
                            ErrorUpdatePanel1.Update();
                        }
                        else
                        {
                            obj_elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoPldGet.Record;
                        }

                        #endregion
                    }

                    SetDokumentumGeneralasFunkciogomb(ParentId, obj_Irat, FunkcioGombsor.DokumentumGeneralasImage);

                    if (isTUKRendszer && !IsPostBack && obj_Ugyirat != null)
                    {
                        if (Command == CommandName.View)
                        {
                            ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, obj_Ugyirat.IrattarId, true);
                        }

                    }
                }
            }

            #region Funkciógombok beállítása


            if (Command == CommandName.Modify)
            {
                Iratok.Statusz iratStatusz = null;
                Ugyiratok.Statusz ugyiratStatusz = null;

                if (obj_Irat != null)
                {
                    iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, obj_elsoIratPeldany);
                }
                if (obj_Ugyirat != null)
                {
                    ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);
                }


                Iratok.SetIratPeldanyLetrehozasa_ImageButton(ParentId, FunkcioGombsor.IratPeldany_Letrehozasa_ImageButton);

                Iratok.SetAtIktatasFunkciogomb(ParentId, iratStatusz, ugyiratStatusz, FunkcioGombsor.AtIktatasImage, Page);

                Iratok.SetValaszIratFunkciogomb(ParentId, obj_Irat, FunkcioGombsor.ValaszIratImage, Page);

                Iratok.SetFelszabaditasFunkciogomb(ParentId, iratStatusz, ugyiratStatusz, FunkcioGombsor.FelszabaditasImage);

                Iratok.SetMunkaIratBeiktatasFunkciogomb(ParentId, obj_Irat, FunkcioGombsor.MunkaanyagBeiktatasImage);

                SetDokumentumGeneralasFunkciogomb(ParentId, obj_Irat, FunkcioGombsor.DokumentumGeneralasImage);
            }

            #endregion

            // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
            TabFooter1.CommandArgument = Command;


            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command, Mode, irat_PostazasIranya);

            //Átiktatás látszik, ha nem módosítható az irat, de átiktatható
            if (Command == CommandName.View)
            {
                Iratok.Statusz iratStatusz = null;
                Ugyiratok.Statusz ugyiratStatusz = null;
                UgyiratDarabok.Statusz ugyiratDarabStatusz = null;

                if (obj_Irat != null)
                {
                    iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, obj_elsoIratPeldany);
                }
                if (obj_Ugyirat != null)
                {
                    ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);
                }
                if (obj_UgyiratDarab != null)
                {
                    ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_UgyiratDarab);
                }

                if (iratStatusz != null && ugyiratDarabStatusz != null && ugyiratStatusz != null)
                {
                    ErrorDetails errorDetail;
                    if (!Iratok.Modosithato(iratStatusz, ugyiratStatusz, UI.SetExecParamDefault(Page), out errorDetail))
                    {
                        Iratok.SetAtIktatasFunkciogomb(ParentId, iratStatusz, ugyiratStatusz, FunkcioGombsor.AtIktatasImage, Page);
                        if (FunkcioGombsor.AtIktatasEnabled)
                        {
                            FunkcioGombsor.AtIktatasVisible = true;
                        }
                    }
                }

                Iratok.SetValaszIratFunkciogomb(ParentId, obj_Irat, FunkcioGombsor.ValaszIratImage, Page);

            }
            else if (!IsPostBack)
            {
                #region Hataridok
                TabFooter1.ImageButton_Save.OnClientClick += SetCompareUgyiratIratHataridoJavaScript(TabFooter1.ImageButton_Save);
                TabFooter1.ImageButton_SaveAndClose.OnClientClick += SetCompareUgyiratIratHataridoJavaScript(TabFooter1.ImageButton_SaveAndClose);
                #endregion Hataridok
            }

            FillTipusosObjektumTargyszavaiPanel_IratokPostazasIranya(ParentId, Irat_Iranya_DropDownList.SelectedValue);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                 Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

        // BUG_3435
        InitializeIratHatasaDDL(true);
        //InitializeIratHatasaDDL(obj_Irat.IratHatasaUgyintezesre);

        //InitializeIratHatasaDDL(null);
        //        // régi azonosító mezõ láthatóságának beállítása
        //        string js = @"
        //        var migraltUgyirat = $get('" + migraltUgyiratAzonositoMezo.ClientID + @"');
        //        var migraltUgyiratTextBox = $get('" + regiAzonositoTextBox.ClientID + @"');
        //
        //        if (migraltUgyirat && migraltUgyiratTextBox)
        //        {
        //            if (migraltUgyiratTextBox.value=='')
        //            {
        //                migraltUgyirat.style.display = 'none';
        //            }
        //            migraltUgyiratTextBox.readOnly = true;
        //        }
        //
        //        ";
        //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hideRow", js, true);
    }

    EREC_IrattariHelyek GetIrattariHelyById(string irattarId)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        ExecParam.Record_Id = irattarId;
        Result res = service.Get(ExecParam);

        if (!res.IsError && res.Record != null)
        {
            EREC_IrattariHelyek hely = (EREC_IrattariHelyek)res.Record;
            return hely;
        }
        return null;
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string id, string selectedValue, bool reloadUgyiratOnly)
    {
        if (Command == CommandName.View ||
            (Mode == CommandName.BelsoIratIktatas && !string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)) && obj_Ugyirat != null)
            )
        {
            if (string.IsNullOrEmpty(selectedValue))
                return;

            EREC_IrattariHelyek hely = GetIrattariHelyById(selectedValue);

            if (hely == null)
                Logger.Debug("Irattári hely nem található. IrattarId: " + hely.Id);

            if (!reloadUgyiratOnly)
            {
                IrattariHelyLevelekDropDownTUK.DropDownList.Items.Clear();
                IrattariHelyLevelekDropDownTUK.DropDownList.Items.Add(new ListItem(hely.Ertek, selectedValue));
            }
            else
            {
                IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.Items.Clear();
                IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.Items.Add(new ListItem(hely.Ertek, selectedValue));
            }

            return;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }
                Csoportok.Add(id);
                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    if (!reloadUgyiratOnly)
                    {
                        IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    }
                    IrattariHelyLevelekDropDownTUKUgyirat.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    if (!string.IsNullOrEmpty(selectedValue))
                    {
                        if (!reloadUgyiratOnly)
                        {
                            IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                        }
                        IrattariHelyLevelekDropDownTUKUgyirat.SetSelectedValue(selectedValue);
                    }
                }
            }
        }
    }

    private void SetEmailBoritekAdatok()
    {
        if (String.IsNullOrEmpty(EmailBoritekokId))
        {
            return;
        }

        // Emailboríték lekérdezése:
        EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
        ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
        execparam_emailGet.Record_Id = EmailBoritekokId;

        Result result_emailGet = erec_eMailBoritekokService.Get(execparam_emailGet);
        if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
            ErrorUpdatePanel1.Update();
            return;
        }

        EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

        //// verzió lementése:
        //eMailBoritekok_Ver_HiddenField.Value = erec_eMailBoritekok.Base.Ver;

        // cimzett és feladóból a partneradatok lekérése:
        Contentum.eAdmin.Service.KRT_PartnerekService service_partnerek =
             Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

        ExecParam execParam_partnerAdatok = UI.SetExecParamDefault(Page, new ExecParam());

        // Figyelem: ez a webmethod alapból a feladót a partnerek között keresi, a címzettet a csoportok között,
        // ezért ezt a kettõt megcseréljük a lekérdezéshez !!
        string cimzett = erec_eMailBoritekok.Felado;
        string felado = erec_eMailBoritekok.Cimzett;
        Result result_partnerAdatok = service_partnerek.GetEmailPartnerAdatok(execParam_partnerAdatok,
             felado, cimzett);
        if (!String.IsNullOrEmpty(result_partnerAdatok.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partnerAdatok);
            ErrorUpdatePanel1.Update();
            return;
        }

        // Figyelem: A címzett adatait a "Felado" és "FeladoSzervezet" táblából nyerhetjük ki, mivel megcseréltük
        // a lekérdezéshez a feladót és a címzettet (mivel itt a címzett a partner)

        DataTable table_felado = result_partnerAdatok.Ds.Tables["Felado"];
        if (table_felado != null && table_felado.Rows.Count > 0)
        {
            DataRow row = table_felado.Rows[0];

            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = row["Id"].ToString();
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = row["Nev"].ToString();

            //// Címe:
            //Email_KuldCim_CimekTextBox.ID = row["CimId"].ToString();
            //Email_KuldCim_CimekTextBox.Text = row["CimNev"].ToString();
        }

        // Szervezet megállapítása:
        DataTable table_feladoSzervezet = result_partnerAdatok.Ds.Tables["FeladoSzervezet"];
        if (table_feladoSzervezet != null && table_feladoSzervezet.Rows.Count > 0)
        {
            DataRow row = table_feladoSzervezet.Rows[0];

            Email_CimzettSzervezete_PartnerTextBox.Id_HiddenField = row["Partner_id_kapcsolt"].ToString();
            Email_CimzettSzervezete_PartnerTextBox.Text = row["Nev"].ToString();
        }

        //// Címzett megállapítása:
        //DataTable table_cimzett = result_partnerAdatok.Ds.Tables["Cimzett"];
        //if (table_cimzett != null && table_cimzett.Rows.Count > 0)
        //{
        //    DataRow row = table_cimzett.Rows[0];

        //    Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = row["Id"].ToString();
        //    Email_CsoportIdCimzett_CsoportTextBox.Text = row["Nev"].ToString();
        //}

        //// Címzett szervezetének megállapítása
        //DataTable table_cimzettSzervezet = result_partnerAdatok.Ds.Tables["CimzettSzervezet"];
        //if (table_cimzettSzervezet != null && table_cimzettSzervezet.Rows.Count > 0)
        //{
        //    DataRow row = table_cimzettSzervezet.Rows[0];

        //    Email_CsoportId_CimzettSzervezete.Id_HiddenField = row["Id"].ToString();
        //    Email_CsoportId_CimzettSzervezete.Text = row["Nev"].ToString();
        //}


        // Tárgy bemásolása az irat tárgyához is:
        IraIrat_Targy_RequiredTextBox.Text = erec_eMailBoritekok.Targy;

        Pld_CimId_Cimzett_CimekTextBox.Text = erec_eMailBoritekok.Cimzett;
        //Pld_CimId_Cimzett_CimekTextBox.ReadOnly = true;
    }

    private void InitElozmenySearchComponent()
    {
        //evIktatokonyvSearch.SetDefaultEvTol = true;
        //evIktatokonyvSearch.SetDefaultEvIg = true;
        //evIktatokonyvSearch.SetDefault();


        bool isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            //TÜK esetén az id lesz az érték mezőnek feltöltve, mivel itt Id szerinti keresés lesz
            IktatoKonyvekDropDownList_Search.FillDropDownList_SetIdToValues(Constants.IktatoErkezteto.Iktato
                  , evIktatokonyvSearch.EvTol, evIktatokonyvSearch.EvIg, true, false, EErrorPanel1);
        }
        else
        {
            IktatoKonyvekDropDownList_Search.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                  , evIktatokonyvSearch.EvTol, evIktatokonyvSearch.EvIg, true, false, EErrorPanel1);
        }
    }

    #region private methods

    #region Hataridok

    private string GetDefaultIntezesiHataridoUgyirat(out string ErrorMessage)
    {
        ErrorMessage = String.Empty;

        DateTime dtUgyintezesKezdete = GetUgyintezesKezdete();

        DateTime dtHatarido = Extra_Napok_Hatarido.GetDefaultIntezesiHatarido(Page, dtUgyintezesKezdete, out ErrorMessage);
        // hiba esetén kinullázzuk
        if (dtHatarido == DateTime.MinValue)
        {
            return System.Text.RegularExpressions.Regex.Replace(dtHatarido.ToShortDateString(), @"[0-9]", "0");
        }
        return dtHatarido.ToShortDateString();
    }

    private string GetDefaultVisszaerkezesiHataridoPld(out string ErrorMessage)
    {
        // jelenleg ugyanúgy számoljuk
        return GetDefaultIntezesiHataridoUgyirat(out ErrorMessage);
    }

    //Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(cache, ExecParam, "", "", DatumTol, out ErrorMessage);

    private string GetIntezesiHataridoUgyirat(string Idoegyseg, string IntezesiIdo)
    {
        string ErrorMessage = String.Empty;

        DateTime dtUgyintezesKezdete = GetUgyintezesKezdete();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        DateTime dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(Page.Cache, execParam, Idoegyseg, IntezesiIdo, dtUgyintezesKezdete, out ErrorMessage);

        return dtHatarido.ToShortDateString();
    }
    #endregion Hataridok

    private void SetDefaultHataridok()
    {
        if (String.IsNullOrEmpty(Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value))
        {
            Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value = Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(UI.SetExecParamDefault(Page));
        }
        //string _30DaysAfterToday = DateTime.Now.AddDays(30).ToShortDateString();
        //UgyUgyirat_Hatarido_CalendarControl.Text = _30DaysAfterToday;
        if (!Uj_Ugyirat_Hatarido_Kezeles)
        {
            string errorMessage = String.Empty;
            UgyUgyirat_Hatarido_CalendarControl.Text = GetDefaultIntezesiHataridoUgyirat(out errorMessage);
            UgyUgyirat_Hatarido_CalendarControl.TextBox.ToolTip = Resources.Form.UI_IntezesiIdoDefault_ToolTip + (String.IsNullOrEmpty(errorMessage) ? "" : "\n" + errorMessage);

            // BLG_1020
            IraIrat_Hatarido_CalendarControl.Text = GetDefaultIntezesiHataridoUgyirat(out errorMessage);
            IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = (String.IsNullOrEmpty(errorMessage) ? "" : errorMessage + "\n") + Resources.Form.UI_IntezesiIdoDefault_ToolTip;

        }

        //IraIrat_Hatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
        //IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = Resources.Form.UI_IntezesiIdoUgyiratbol_ToolTip;
        // BLG_1020
        //IraIrat_Hatarido_CalendarControl.Text = "";
        //IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = "";

        UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = "1";
        // kitolhat? | kötött?
        UgyiratHataridoKitolas_Irat_HiddenField.Value = "1|0";

        IktatasDatuma_Ugyirat_HiddenField.Value = "";
        IktatasDatuma_Irat_HiddenField.Value = "";
    }

    /// <summary>
    /// Komponensek láthatóságának beállítása
    /// </summary>
    /// <param name="_command">New, Modify, View</param>
    /// <param name="_mode">BejovoIratIktatas, BelsoIratIktatas, stb...</param>
    private void SetComponentsVisibility(String _command, String _mode, String _irat_PostazasIranya)
    {
        #region FunkcióGombsor állítása

        // Az osszes gomb eltuntetese
        FunkcioGombsor.ButtonsVisible(false);

        if (_command == CommandName.View)
        {
            // FunkcioGombSor gombjainak beallitasa

            //FunkcioGombsor.KuldemenyMegtekinteseVisible = true;

            //FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
            //FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            //FunkcioGombsor.KiserolapVisible = true;
            FunkcioGombsor.XMLExportVisible = true;
            FunkcioGombsor.HitelesitesiZaradekVisible = true;


            //HA NEM TÜK-ös
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
            {
                // CR 3058
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                    FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
                else
                    FunkcioGombsor.VonalkodNyomtatasa_Visible = false;
            }
            else//HA TÜK-ös
            {
                bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                FunkcioGombsor.VonalkodNyomtatasa_Visible = _visible;
            }
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = ParentId;

            Result res = service.Get(execParam);

            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)res.Record;

                if (erec_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
                {
                    FunkcioGombsor.EMailNyomtatasVisible = true;
                }
            }

            FunkcioGombsor.ValaszIratVisible = true;
            FunkcioGombsor.DokumentumGeneralasVisible = true;
        }

        if (_command == CommandName.Modify)
        {
            // FunkcioGombSor gombjainak beallitasa

            //FunkcioGombsor.KuldemenyMegtekinteseVisible = true;
            //FunkcioGombsor.SztornozasVisible = true;
            FunkcioGombsor.IratPeldany_LetrehozasaVisible = true;
            //FunkcioGombsor.PostazasVisible = true;
            //FunkcioGombsor.PostazobaVisible = true;
            //FunkcioGombsor.Tovabbitasra_kijelolesVisible = true;

            FunkcioGombsor.AtIktatasVisible = true;
            FunkcioGombsor.ValaszIratVisible = true;
            FunkcioGombsor.FelszabaditasVisible = true;
            // CR3058
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
            else
                FunkcioGombsor.VonalkodNyomtatasa_Visible = false;
            // TODO: Munkaanyag beiktatas ikon
            // Csak akkor látszódik, ha munkaanyagról van szó:
            if (obj_Irat != null && Iratok.Munkaanyag(obj_Irat))
            {
                FunkcioGombsor.MunkaanyagBeiktatasVisible = true;
            }

            //FunkcioGombsor.ZarolasVisible = true;
            //FunkcioGombsor.Zarolas_feloldasaVisible = true;
            FunkcioGombsor.XMLExportVisible = true;
            FunkcioGombsor.HitelesitesiZaradekVisible = true;

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = ParentId;

            Result res = service.Get(execParam);

            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)res.Record;

                if (erec_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
                {
                    FunkcioGombsor.EMailNyomtatasVisible = true;
                }
            }

            FunkcioGombsor.DokumentumGeneralasVisible = true;

        }
        #endregion

        #region TabFooter gombok állítása

        if (_command == CommandName.Modify || _command == CommandName.New)
        {
            // A tomeges felvitelt segito gombok megjelenitese:
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            // ez csak New-nál kell
            if (_command == CommandName.New)
            {
                TabFooter1.ImageButton_SaveAndNew.Visible = false;
                TabFooter1.ImageButton_SaveAndClose.Visible = false;
                TabFooter1.ImageButton_Cancel.Visible = true;
                TabFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            }
            else
            {
                // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
                TabFooter1.ImageButton_Cancel.Visible = false;
            }
        }
        else if (_command == CommandName.View)
        {

            // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
            TabFooter1.ImageButton_Cancel.Visible = false;
        }


        #endregion



        #region Form komponensek (TextBoxok,...) állítása
        //bernat.laszlo added
        // nekrisz modified
        if (obj_Irat == null)
        {
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            Ikt_Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
        }
        else
             if (string.IsNullOrEmpty(obj_Irat.Munkaallomas))
        {
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            Ikt_Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
        }
        else Ikt_Munkaallomas_TextBox.Text = obj_Irat.Munkaallomas;

        Ugyint_modja_Dropdown.FillAndSetSelectedValue(kcs_IRAT_UGYINT_MODJA, KodTarak.IRAT_UGYINT_MODJA.Szemelyes, EErrorPanel1);
        switch (Mode)
        {
            case CommandName.BejovoIratIktatas:
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            case CommandName.MunkapeldanyBeiktatas_Bejovo:
                Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, KodTarak.POSTAZAS_IRANYA.Bejovo, EErrorPanel1);
                Irat_Iranya_DropDownList.ReadOnly = true;
                if (isTUKRendszer)
                {
                    tr_fizikaihelyUgyirat.Visible = true;
                    IrattariHelyLevelekDropDownTUKUgyirat.Visible = true;
                }
                break;
            case CommandName.KimenoEmailIktatas:
            case CommandName.KimenoKuldemenyFelvitel:
                Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, KodTarak.POSTAZAS_IRANYA.Belso, EErrorPanel1);
                Irat_Iranya_DropDownList.ReadOnly = true;
                break;
            case CommandName.BelsoIratIktatas:
                /// BLG_635: Belsõ irat iktatásakor lehessen változtatni az irat irányát (belsõ vagy kimenõ).
                /// Alapértelmezett a "régi" belsõ, ami már át lett nevezve kimenõre, mivel az viselkedik kimenõ módjára...
                /// FPH-ban megmaradt belsõnek, mert ott már ezt szokták meg...
                /// 
                List<string> filterList = new List<string>()
                     {
                          KodTarak.POSTAZAS_IRANYA.Belso,
                          KodTarak.POSTAZAS_IRANYA.Kimeno
                     };
                Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, KodTarak.POSTAZAS_IRANYA.Belso, filterList, false, EErrorPanel1);

                Irat_Iranya_DropDownList.ReadOnly = false;

                if (isTUKRendszer)
                {
                    tr_fizikaihely.Visible = true;
                    IrattariHelyLevelekDropDownTUK.Visible = true;
                    tr_fizikaihelyUgyirat.Visible = true;
                    IrattariHelyLevelekDropDownTUKUgyirat.Visible = true;
                }

                break;
            default:
                if (obj_Irat == null)
                {
                    Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, KodTarak.POSTAZAS_IRANYA.Bejovo, EErrorPanel1);
                }
                else
                {
                    Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, obj_Irat.PostazasIranya, EErrorPanel1);
                    //LZS - BUG_4968 - Ha az irat postázás iránya nem bejövő, akkor kivesszük a bejövő értéket a lenyíló listából.
                    if (obj_Irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
                    {
                        Irat_Iranya_DropDownList.DropDownList.Items.Remove(Irat_Iranya_DropDownList.DropDownList.Items.FindByValue("1"));
                    }
                }
                if (isTUKRendszer)
                {
                    tr_fizikaihely.Visible = true;
                    IrattariHelyLevelekDropDownTUK.Visible = true;
                    tr_fizikaihelyUgyirat.Visible = true;
                    IrattariHelyLevelekDropDownTUKUgyirat.Visible = true;
                }
                break;
        }

        //bernat.laszlo eddig
        if (_irat_PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
            || _irat_PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno
             || (_command == CommandName.New && _mode == CommandName.BelsoIratIktatas)
             || (_command == CommandName.New && _mode == CommandName.KimenoEmailIktatas)
             || (_command == CommandName.New && _mode == CommandName.ElokeszitettIratIktatas))
        {
            // Belso keletkezesu irat

            KuldemenyPanel.Visible = false;
            tr_elosztoiv.Visible = false;
            //tr_Kiadmanyozas.Visible = true;

            HivatkozasiSzam_Irat_TextBox.Visible = true;
            Label_HivatkozasiSzam_Irat.Visible = true;

            // BelsoIratIktatásnál:
            if ((_command == CommandName.New && _mode == CommandName.BelsoIratIktatas)
                 || (_command == CommandName.New && _mode == CommandName.KimenoEmailIktatas))
            {
                //tr_EgyebMuvelet.Visible = true;

                Label_Kiadmanyozo.Visible = false;
                IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Visible = false;

                RadioButtonList_BejovoIktatasnal.Visible = false;
                RadioButtonList_BelsoIratIktatasnal.Visible = true;
                // BLG_350
                //UgyiratpeldanySzukseges_CheckBox.Checked=default_IratPeldanySzukseges;

                //LZS-BLG_310
                IraIrat_KiadmanyozniKell_CheckBox.Checked = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.KIADMANYOZNI_KELL_DEFAULT, true); ;

            }

            if (_command == CommandName.New && _mode == CommandName.ElokeszitettIratIktatas)
            {
                StandardTargyszavakPanel.Visible = false;
                Label_Kiadmanyozo.Visible = false;
                IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Visible = false;

                // Iratpéldányok panel nem kell a (bejövõ) elõkészített irat iktatásánál:
                if (_irat_PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                {
                    IratPeldanyPanel.Visible = false;
                }
            }

            if (_command == CommandName.New && _mode == CommandName.KimenoEmailIktatas)
            {
                Label_Pld_CimzettCime.Visible = false;
                Label_Email_CimzettEmailCime.Visible = true;

                Label_Pld_VonalkodElottCsillag.Visible = false;
                Label_Pld_Vonalkod.Visible = false;
                Label_Email_CimzettSzervezete.Visible = true;

                Pld_BarCode_VonalKodTextBox.Visible = false;
                Email_CimzettSzervezete_PartnerTextBox.Visible = true;
            }
            // BLG_350
            UgyiratpeldanySzukseges_CheckBox.Checked = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.UGYIRATI_PELDANY_SZUKSEGES, false);
        }
        else
        {
            // Bejovo irat

            IratPeldanyPanel.Visible = (Mode != CommandName.AtIktatas && Command == CommandName.New);
            IratPeldanyPanel_CimzettTR.Visible = false;
            IratPeldanyPanel_CimzettCimeTR.Visible = false;
            IratPeldanyPanel_HataridoTR.Visible = false;


            //tr_Kiadmanyozas.Visible = false;
            //SpacerImage1.Visible = true;
            //SpacerImage2.Visible = true;

            // Bejövõ irat iktatásnál:
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            if (_command == CommandName.New && ((_mode == CommandName.BejovoIratIktatas) || (_mode == CommandName.MunkapeldanyBeiktatas_Bejovo)))
            {
                //CR3221 Szignálás(elõkészítés) kezelés
                // IratPanel ne látszódjon
                if (_command == CommandName.New && (_mode == CommandName.MunkapeldanyBeiktatas_Bejovo))
                {
                    IratPanel.Visible = false;
                }
                //tr_EgyebMuvelet.Visible = true;
                tr_Kiadmanyozas.Visible = false;
                RadioButtonList_BejovoIktatasnal.Visible = true;
                RadioButtonList_BelsoIratIktatasnal.Visible = false;

                AdathordozoTipusa_KuldemenyTD.Visible = false;
            }
            else
            {
                tr_elosztoiv.Visible = false;

                AdathordozoTipusa_KuldemenyTD.Visible = false;
            }
        }
        // BUG_8765
        if (IsTUK_SZIGNALAS_ENABLED)
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;

        if (_command == CommandName.Modify || _command == CommandName.View)
        {
            #region KuldemenyPanel

            ImageButton_KuldemenyKereses.Enabled = false;
            ImageButton_KuldemenyKereses.Visible = false;

            ImageButton_KuldemenyErkeztetes.Enabled = false;
            ImageButton_KuldemenyErkeztetes.Visible = false;

            ImageButton_KuldemenyModositas.Enabled = false;
            ImageButton_KuldemenyModositas.Visible = false;

            // többi komponens alapból readonly...
            #endregion



            #region UgyiratPanel      

            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = true;

            ImageButton_Elozmeny.Enabled = false;
            ImageButton_Elozmeny.Visible = false;
            trElozmenySearch.Visible = false;
            UI.SetImageButtonStyleToDisabled(ImageButton_Elozmeny);

            ImageButton_Ugyiratterkep.Enabled = false;
            ImageButton_Ugyiratterkep.Visible = false;

            ImageButton_ElozmenyUgyirat_Modositas.Enabled = false;
            ImageButton_ElozmenyUgyirat_Modositas.Visible = false;

            ImageButton_MigraltKereses.Enabled = false;
            ImageButton_MigraltKereses.Visible = false;

            //UgyUgyirat_UgyiratTextBox.ReadOnly = true;
            //UgyUgyirat_Ugytipus_KodtarakDropDownList.ReadOnly = true;


            // BLG_44
            //tr_agazatiJel.Visible = false;
            Label_agazatiJel.Visible = false;
            AgazatiJelek_DropDownList.Visible = false;

            Ugykor_DropDownList.Visible = false;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelUgykorKod.Visible = false;
            Merge_IrattariTetelszamTextBox.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ViewMode = true;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            labelIrattariTetelszam.Visible = true;

            UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;

            UgyUgyirat_Hatarido_CalendarControl.Validate = false;
            UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;

            Ugyfelelos_CsoportTextBox.ReadOnly = true;
            Ugyfelelos_CsoportTextBox.ViewMode = true;
            Ugyfelelos_CsoportTextBox.Validate = false;

            UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
            UgyUgyiratok_CsoportId_Felelos.ViewMode = true;
            UgyUgyiratok_CsoportId_Felelos.Validate = false;

            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ViewMode = true;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate = false;

            Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
            Ugy_PartnerId_Ugyindito_PartnerTextBox.ViewMode = true;

            CimekTextBoxUgyindito.ReadOnly = true;
            CimekTextBoxUgyindito.ViewMode = true;

            labelIratHelye_Ugyirat.Visible = true;
            UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = true;
            UgyiratOrzo_FelhasznaloCsoportTextBox.ReadOnly = true;
            UgyiratOrzo_FelhasznaloCsoportTextBox.ViewMode = true;

            // BLG_44
            UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
            IrattariTetelszam_DropDownList.Visible = false;

            // BUG_2074
            IntezesiIdo_KodtarakDropDownList.ReadOnly = true;
            IntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;
            trUgyiratIntezesiIdoVisible = Uj_Ugyirat_Hatarido_Kezeles;
            #endregion

            #region UgyiratDarab Panel

            //UgyUgyIratDarab_Leiras_TextBox.ReadOnly = true;
            //UgyUgyiratDarab_EljarasiSzakasz_KodtarakDropDownList.ReadOnly = true;

            //UgyUgyiratDarab_Hatarido_CalendarControl.ReadOnly = true;
            //UgyUgyiratDarab_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;


            #endregion

            #region IratPanel

            if (_irat_PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                tr_Kiadmanyozas.Visible = false;
            }

            tr_kezelesifeljegyzes.Visible = false;

            tr_EgyebMuvelet.Visible = false;
            RadioButtonList_BejovoIktatasnal.Visible = false;
            RadioButtonList_BelsoIratIktatasnal.Visible = false;

            tr_ugytipus.Visible = false;

            //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.ReadOnly = true;
            if (_command == CommandName.View)
            {
                IraIrat_Targy_RequiredTextBox.ReadOnly = true;
                //IraIrat_Kategoria_KodtarakDropDownList.ReadOnly = true;
                IraIrat_Irattipus_KodtarakDropDownList.ReadOnly = true;
                Irat_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;
                //aIktato_FelhasznaloCsoportTextBox.Readonly = true;
                //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.ReadOnly = true;
                //IraKezFeljegyz_Leiras_TextBox.ReadOnly = true;
                ktDropDownListIratMinosites.ReadOnly = true;

                IraIrat_KiadmanyozniKell_CheckBox.Enabled = false;
                //IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.ReadOnly = true;

                // BLG_1020
                inputUgyintezesiNapok.Attributes.Add("readonly", "readonly");
                inputUgyintezesiNapok.Disabled = true;

                IratIntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;

                IraIrat_Hatarido_CalendarControl.Validate = false;
                // BUG_10524
                //IraIrat_Hatarido_CalendarControl.ReadOnly = true;
                IraIrat_Hatarido_CalendarControl.Enabled = false;
                IraIrat_Hatarido_CalendarControl.Attributes.Add("readonly", "readonly");

                Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = true;
                //bernat.laszlo added
                Ugyint_modja_Dropdown.ReadOnly = true;
                Irat_Iranya_DropDownList.ReadOnly = true;
                IntezesIdopontja_CalendarControl.ReadOnly = true;
                CalendarControl_UgyintezesKezdoDatuma.ReadOnly = true;
                //bernat.laszlo eddig
                IratHatasaUgyintezesre_KodtarakDropDownList.ReadOnly = true;
                datalistUgyintezesiNapok.Disabled = true;
                // BLG_619
                Iratfelelos_CsoportTextBox.ReadOnly = true;

                CalendarControlMinositesErvenyessegIdeje.ReadOnly = true;
                MinositoPartnerControlIraIratokFormTab.SetReadOnly(true);
                TerjedelemPanelA.SetReadOnly(true);

                KodtarakDropDownList_EljarasiSzakaszFok.ReadOnly = true;

                HivatkozasiSzam_Irat_TextBox.ReadOnly = true;

            }

            AdathordozoTipusa_KodtarakDropDownList.ReadOnly = true;
            #endregion

            #region IratPéldány Panel

            UgyiratpeldanySzukseges_CheckBox.Visible = false;
            // BLG_350
            //KeszitoPeldanya_CheckBox.Visible = false;

            Pld_PartnerId_Cimzett_PartnerTextBox.ReadOnly = true;
            Pld_PartnerId_Cimzett_PartnerTextBox.ViewMode = true;

            Pld_KuldesMod_KodtarakDropDownList.ReadOnly = true;

            Pld_CimId_Cimzett_CimekTextBox.ReadOnly = true;
            Pld_CimId_Cimzett_CimekTextBox.ViewMode = true;

            Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly = true;

            //Pld_Tovabbito_KodtarakDropDownList.ReadOnly = true;
            Pld_Visszavarolag_KodtarakDropDownList.ReadOnly = true;
            //Pld_Ragszam_TextBox.ReadOnly = true;
            Pld_VisszaerkezesiHatarido_CalendarControl.ReadOnly = true;

            Pld_BarCode_VonalKodTextBox.ReadOnly = true;
            Pld_BarCode_VonalKodTextBox.Validate = false;

            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Validate = false;
                IrattariHelyLevelekDropDownTUK.DropDownList.ReadOnly = true;
                IrattariHelyLevelekDropDownTUKUgyirat.Validate = false;
                IrattariHelyLevelekDropDownTUKUgyirat.DropDownList.ReadOnly = true;
            }

            #endregion
        }
        else if (_command == CommandName.New && Mode == CommandName.ElokeszitettIratIktatas)
        {

            #region Imagebuttons
            ImageButton_KuldemenyKereses.Enabled = false;
            ImageButton_KuldemenyKereses.Visible = false;

            ImageButton_KuldemenyErkeztetes.Enabled = false;
            ImageButton_KuldemenyErkeztetes.Visible = false;

            ImageButton_KuldemenyModositas.Enabled = false;
            ImageButton_KuldemenyModositas.Visible = false;

            #endregion
            #region Elõkészített irat UgyiratPanel

            // Elõzményt ne lehessen választani:
            trElozmenySearch.Visible = false;

            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = true;

            //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.Enabled = false;

            //UgyUgyirat_UgyiratTextBox.ReadOnly = true;
            //UgyUgyirat_Ugytipus_KodtarakDropDownList.ReadOnly = true;

            UgyUgyirat_Ugytipus_DropDownList.Visible = false;
            Label27.Visible = false;

            tr_agazatiJel.Visible = false;
            AgazatiJelek_DropDownList.Visible = false;

            Ugykor_DropDownList.Visible = false;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelUgykorKod.Visible = false;
            Merge_IrattariTetelszamTextBox.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ViewMode = true;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelIrattariTetelszam.Visible = true;

            UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;

            UgyUgyirat_Hatarido_CalendarControl.Validate = false;
            UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;

            Ugyfelelos_CsoportTextBox.ReadOnly = true;
            Ugyfelelos_CsoportTextBox.ViewMode = true;

            Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
            Ugy_PartnerId_Ugyindito_PartnerTextBox.ViewMode = true;

            CimekTextBoxUgyindito.ReadOnly = true;
            CimekTextBoxUgyindito.ViewMode = true;

            UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
            UgyUgyiratok_CsoportId_Felelos.ViewMode = true;
            UgyUgyiratok_CsoportId_Felelos.Validate = false;

            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ViewMode = true;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate = false;

            trUgyiratIntezesiIdoVisible = false;

            UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
            IrattariTetelszam_DropDownList.Visible = false;
            #endregion

            #region IratPanel
            // BLG_350
            //KeszitoPeldanya_CheckBox.Enabled = false;
            UgyiratpeldanySzukseges_CheckBox.Enabled = false;
            IraIrat_Targy_RequiredTextBox.ReadOnly = false;
            //IraIrat_Kategoria_KodtarakDropDownList.ReadOnly = false;
            IraIrat_Irattipus_KodtarakDropDownList.ReadOnly = false;
            //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.ReadOnly = false;
            //IraKezFeljegyz_Leiras_TextBox.ReadOnly = false;
            IraIrat_KiadmanyozniKell_CheckBox.Enabled = true;
            tr_kezelesifeljegyzes.Visible = false;

            // BUG_2051
            //Label_AdathordozoReqStar.Visible = true;
            //AdathordozoTipusa_KodtarakDropDownList.Validate = true;
            #endregion

        }

        // feladatok: felelõs kötelezõ
        if (Command == CommandName.New)
        {
            FeljegyzesPanel.ValidateIfFeladatIsNotEmpty = true;
        }
        else
        {
            FeljegyzesPanel.ValidateIfFeladatIsNotEmpty = false;
        }

        this.SetTUKFieldsVisibility();

        #endregion

        if (_command == CommandName.New && Mode == CommandName.BelsoIratIktatas)
        {
            CalendarControl_UgyintezesKezdoDatuma.Enabled = true;
            CalendarControl_UgyintezesKezdoDatuma.ReadOnly = false;
        }
        else
        {
            CalendarControl_UgyintezesKezdoDatuma.Enabled = false;
            CalendarControl_UgyintezesKezdoDatuma.ReadOnly = true;
        }

        // BUG_10524
        // BUG_9291
        //if (Command != CommandName.View)
        //    SetIratUgyintezesiIdoPlusHataridoVisibility();
    }

    private void SetIktatasMegtagadvaControls()
    {
        // megtagadott emailt nem lehet módosítani, iktatni:
        TabFooter1.ImageButton_Save.Visible = false;

        ImageButton_KuldemenyModositas.Enabled = false;
        UI.SetImageButtonStyleToDisabled(ImageButton_KuldemenyModositas);

        FunkcioGombsor.Visible = false;
        IratPanel.Visible = false;
        Ugyirat_Panel.Visible = false;
        IratPeldanyPanel.Visible = false;
        BejovoPeldanyListPanel1.Visible = false;

    }
    //business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraIratok erec_IraIratok)
    {
        if (erec_IraIratok != null)
        {
            bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

            // Irat rész:

            IraIrat_Targy_RequiredTextBox.Text = erec_IraIratok.Targy;
            //IraIrat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA, erec_IraIratok.Kategoria
            //    , true, EErrorPanel1);
            IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, erec_IraIratok.Irattipus, true, EErrorPanel1);

            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            //bernat.laszlo modified: Iktató ID- Ügyintézõ ID helyett
            Iktato_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Iktato;
            Iktato_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            IraIrat_KiadmanyozniKell_CheckBox.Checked = (erec_IraIratok.KiadmanyozniKell == "1") ? true : false;

            IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany;
            IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            //bernat.laszlo added
            Ikt_Munkaallomas_TextBox.Text = erec_IraIratok.Munkaallomas;

            Ugyint_modja_Dropdown.FillAndSetSelectedValue(kcs_IRAT_UGYINT_MODJA, erec_IraIratok.UgyintezesModja, false, EErrorPanel1);
            Irat_Iranya_DropDownList.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA, erec_IraIratok.PostazasIranya, false, EErrorPanel1);

            IntezesIdopontja_CalendarControl.Text = erec_IraIratok.IntezesIdopontja;
            CalendarControl_UgyintezesKezdoDatuma.Text = erec_IraIratok.UgyintezesKezdoDatuma;
            //bernat.laszlo eddig

            HivatkozasiSzam_Irat_TextBox.Text = erec_IraIratok.HivatkozasiSzam;

            if (String.IsNullOrEmpty(erec_IraIratok.AdathordozoTipusa))
            {
                AdathordozoTipusa_KodtarakDropDownList.FillAndSetEmptyValue(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);
                AdathordozoTipusa_DropDownList.FillAndSetEmptyValue(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);
            }
            else
            {
                AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_IraIratok.AdathordozoTipusa, false, EErrorPanel1);
                AdathordozoTipusa_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_IraIratok.AdathordozoTipusa, false, EErrorPanel1);
            }

            // BLG_361
            if (String.IsNullOrEmpty(erec_IraIratok.UgyintezesAlapja))
            {
                Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetEmptyValue(kcs_ELSODLEGES_ADATHORDOZO, EErrorPanel1);
            }
            else
            {
                //Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_IraIratok.UgyintezesAlapja, true, EErrorPanel1);
                Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_IraIratok.UgyintezesAlapja, true, EErrorPanel1);
            }
            //Kiadmányozás dátumának töltése, IratAlairok táblából
            SetKiadmanyozasDatuma(erec_IraIratok);

            ktDropDownListIratMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_IraIratok.Minosites, true, EErrorPanel1);

            InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
            if (!String.IsNullOrEmpty(erec_IraIratok.IntezesiIdoegyseg))
            {
                IratIntezesiIdoegyseg_KodtarakDropDownList.SelectedValue = erec_IraIratok.IntezesiIdoegyseg;
            }
            IraIrat_Hatarido_CalendarControl.Text = erec_IraIratok.Hatarido;

            IratJellegKodtarakDropDown.FillWithOneValue(kcs_IRAT_JELLEG, erec_IraIratok.Jelleg, EErrorPanel1);

            // BLG_619
            Iratfelelos_CsoportTextBox.Id_HiddenField = erec_IraIratok.Csoport_Id_Ugyfelelos;
            Iratfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            //BLG 90
            FelelosSzervezetVezetoTextBoxInstance.SetSzervezet(erec_IraIratok.Csoport_Id_Ugyfelelos);

            // BLG_1402:
            if (isTUK)
            {
                CalendarControlMinositesErvenyessegIdeje.Text = erec_IraIratok.MinositesErvenyessegiIdeje;
                TerjedelemPanelA.SetTerjedelem(erec_IraIratok.TerjedelemMennyiseg, erec_IraIratok.TerjedelemMennyisegiEgyseg, erec_IraIratok.TerjedelemMegjegyzes);
            }

            #region MINOSITO
            MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId = erec_IraIratok.Minosito;
            // Szülõ: Ügyindító partner:
            if (this.obj_Ugyirat == null)
            {
                this.SetObjUgyirat();
            }
            if (obj_Irat != null)
            {
                MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId = obj_Irat.MinositoSzervezet;
            }
            // Névfeloldás Id alapján:
            MinositoPartnerControlIraIratokFormTab.SetPartnerTextBoxById(EErrorPanel1);
            #endregion

            #region Intézési idõ
            IktatasDatuma_Irat_HiddenField.Value = erec_IraIratok.IktatasDatuma;
            // Intézési idõ kiolvasása az EREC_IratMetaDefinicio táblából
            //IratMetaDefinicio.SetHataridoToolTipByIratMetaDefinicioId(Page, erec_IraIratok.IratMetaDef_Id, IraIrat_Hatarido_CalendarControl.TextBox);
            EREC_IratMetaDefinicio erec_IratMetaDefinicio = IratMetaDefinicio.GetBusinessObjectById(Page, erec_IraIratok.IratMetaDef_Id);
            IratMetaDefinicio.SetHataridoToolTipByIratMetaDefinicio(Page, erec_IratMetaDefinicio, IraIrat_Hatarido_CalendarControl.TextBox);
            if (erec_IratMetaDefinicio != null)
            {
                // kitolhat? | kötött?
                UgyiratHataridoKitolas_Irat_HiddenField.Value =
                     (String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratHataridoKitolas) ? "1" : erec_IratMetaDefinicio.UgyiratHataridoKitolas)
                     + "|" + (String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott) ? "0" : erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott);

                if (Command == CommandName.Modify)
                {
                    if (erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott == "1")
                    {
                        IraIrat_Hatarido_CalendarControl.Enabled = false;
                    }
                    else
                    {
                        IraIrat_Hatarido_CalendarControl.Enabled = true;
                    }
                }
            }
            else
            {
                // kitolhat? | kötött?
                UgyiratHataridoKitolas_Irat_HiddenField.Value = "1|0";
                if (Command == CommandName.Modify)
                {
                    IraIrat_Hatarido_CalendarControl.Enabled = true;
                }
            }
            #endregion Intézési idõ

            //TODO
            InitializeIratHatasaDDL(true);
            KodtarakDropDownListUgyiratFelfuggesztesOka.FillDropDownList(KCS_FELFUGGESZTESOKA, true, EErrorPanel1);
            InitializeLezarasOkaDDL(erec_IraIratok.IratHatasaUgyintezesre, erec_IraIratok.LezarasOka);

            // Küldemény rész feltöltése:
            if (!String.IsNullOrEmpty(erec_IraIratok.KuldKuldemenyek_Id))
            {
                Kuldemeny_Id_HiddenField.Value = erec_IraIratok.KuldKuldemenyek_Id;
                LoadKuldemenyComponents();
            }

            // IratPéldány rész feltöltése (elvileg csak belsõ iratnál kell)
            if (erec_IraIratok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam_elsoPeldanyGet = UI.SetExecParamDefault(Page, new ExecParam());
                String iratId = erec_IraIratok.Id;

                if (!String.IsNullOrEmpty(iratId))
                {
                    Result result_elsoPeldanyGet = erec_PldIratPeldanyokService.GetElsoIratPeldanyByIraIrat(
                         execParam_elsoPeldanyGet, iratId);

                    if (String.IsNullOrEmpty(result_elsoPeldanyGet.ErrorCode))
                    {
                        if (result_elsoPeldanyGet.Record == null)
                        {
                            // átiktatott irat esetén nincs iratpéldány
                            if (erec_IraIratok.Allapot != KodTarak.IRAT_ALLAPOT.Atiktatott)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(51104));
                                ErrorUpdatePanel1.Update();
                            }
                        }
                        else
                        {
                            obj_elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoPeldanyGet.Record;
                            LoadIratPeldanyComponents(obj_elsoIratPeldany);
                        }
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_elsoPeldanyGet);
                        ErrorUpdatePanel1.Update();
                    }
                }
            }


            // Ügyiratdarab, ügyirat rész feltöltése:
            if (!String.IsNullOrEmpty(erec_IraIratok.UgyUgyIratDarab_Id))
            {
                // ügyiratdarab lekérése és betöltése

                if (obj_UgyiratDarab == null)
                {
                    EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService =
                         eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();

                    ExecParam execParam_ugyiratDarabGet = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_ugyiratDarabGet.Record_Id = erec_IraIratok.UgyUgyIratDarab_Id;

                    Result result_ugyiratDarabGet = erec_UgyUgyiratdarabokService.Get(execParam_ugyiratDarabGet);
                    if (String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
                    {

                        obj_UgyiratDarab = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratDarabGet);
                        ErrorUpdatePanel1.Update();
                    }
                }

                if (obj_UgyiratDarab != null)
                {
                    //LoadUgyiratDarabComponents(obj_UgyiratDarab);
                    // pl. irat határidõhöz kell, de jelenleg nem használjuk
                    UgyiratdarabEljarasiSzakasz_HiddenField.Value = obj_UgyiratDarab.EljarasiSzakasz;

                    if (obj_Ugyirat == null)
                    {
                        // ügyirat lekérése és betöltése
                        EREC_UgyUgyiratokService erec_UgyUgyiratokService =
                             eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam_ugyiratGet.Record_Id = obj_UgyiratDarab.UgyUgyirat_Id;

                        Result result_UgyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);
                        if (String.IsNullOrEmpty(result_UgyiratGet.ErrorCode))
                        {
                            if (result_UgyiratGet.Record == null)
                            {
                                return;
                            }
                            else
                            {
                                obj_Ugyirat = (EREC_UgyUgyiratok)result_UgyiratGet.Record;
                            }
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_UgyiratGet);
                            ErrorUpdatePanel1.Update();
                        }
                    }

                    EREC_IraIktatoKonyvek iktatoKonyv = null;

                    if (obj_Ugyirat != null)
                    {
                        iktatoKonyv = LoadUgyiratComponents(obj_Ugyirat);
                    }


                    #region Header címének beállítása:

                    string fullIktatoszam = erec_IraIratok.Azonosito;
                    // BLG_619
                    if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
                    {

                        EREC_IraIratokService tempService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        string iratfelelos_szervezetkod = "";
                        Result tempResult = tempService.GetIratFelelosSzervezetKod(temp_execParam, erec_IraIratok.Id);
                        if (!tempResult.IsError)
                        {
                            iratfelelos_szervezetkod = tempResult.Record.ToString();
                            if (!String.IsNullOrEmpty(iratfelelos_szervezetkod))
                            {
                                fullIktatoszam = iratfelelos_szervezetkod + " " + erec_IraIratok.Azonosito;
                            }
                        }
                    }

                    // BLG_619
                    if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
                    {

                        EREC_IraIratokService tempService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        string iratfelelos_szervezetkod = "";
                        Result tempResult = tempService.GetIratFelelosSzervezetKod(temp_execParam, erec_IraIratok.Id);
                        if (!tempResult.IsError)
                        {
                            iratfelelos_szervezetkod = tempResult.Record.ToString();
                            if (!String.IsNullOrEmpty(iratfelelos_szervezetkod))
                            {
                                fullIktatoszam = iratfelelos_szervezetkod + " " + erec_IraIratok.Azonosito;
                            }
                        }
                    }


                    if (Command == CommandName.View)
                    {
                        FormHeader.FullManualHeaderTitle = fullIktatoszam + " " + Resources.Form.Iratmegtekintese_ManualFormHeaderTitle;
                    }
                    else if (Command == CommandName.Modify)
                    {
                        FormHeader.FullManualHeaderTitle = fullIktatoszam + " " + Resources.Form.Iratmodositasa_ManualFormHeaderTitle;
                    }

                    // Updatepanel miatt frissíteni kell:
                    if (FormHeaderUpdatePanel != null)
                    {
                        FormHeaderUpdatePanel.Update();
                    }

                    #endregion

                }
            }

            Record_Ver_HiddenField.Value = erec_IraIratok.Base.Ver;

            #region BLG_1051
            LoadIratHatasaComponentsFromBo(erec_IraIratok);
            #endregion BLG_1051

            LoadUgyintezesiNapokFromBo(erec_IraIratok);
            LoadEljarasiSzakaszFokComponentsFromBo(erec_IraIratok);

            LoadLezarasOkaComponentsFromBo(erec_IraIratok);

            SetIratControlsDisableIfHatosagi(erec_IraIratok);

            //LZS - BUG_1074
            string ugyfelelos_szervezetkod = "";
            string eloirat_iktatoszam = "";
            string utoirat_iktatoszam = "";

            EREC_UgyUgyiratokService tempUgyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch ugyiratoSearch = new EREC_UgyUgyiratokSearch();
            ExecParam tempUgyirat_execParam = UI.SetExecParamDefault(Page, new ExecParam());
            ugyiratoSearch.Id.Value = erec_IraIratok.Ugyirat_Id;
            ugyiratoSearch.Id.Operator = Query.Operators.equals;
            tempUgyirat_execParam.Record_Id = erec_IraIratok.Ugyirat_Id;

            Result tempUgyiratResult = tempUgyiratService.GetAllWithExtension(tempUgyirat_execParam, ugyiratoSearch);

            if (!tempUgyiratResult.IsError)
            {
                if (tempUgyiratResult.Ds.Tables.Count != 0)
                {
                    if (tempUgyiratResult.Ds.Tables[0].Rows.Count != 0)
                    {
                        utoirat_iktatoszam = tempUgyiratResult.Ds.Tables[0].Rows[0]["Foszam_Merge_Szulo"].ToString();
                        eloirat_iktatoszam = string.IsNullOrEmpty(tempUgyiratResult.Ds.Tables[0].Rows[0]["Leszarmazott_Iktatoszam"].ToString()) ? tempUgyiratResult.Ds.Tables[0].Rows[0]["Regirendszeriktatoszam"].ToString() : tempUgyiratResult.Ds.Tables[0].Rows[0]["Leszarmazott_Iktatoszam"].ToString();
                        ugyfelelos_szervezetkod = tempUgyiratResult.Ds.Tables[0].Rows[0]["UgyFelelos_SzervezetKod"].ToString();
                    }
                }
            }

            eloirat_IktatoSzamTextBox.Text = eloirat_iktatoszam;
            utoirat_IktatoSzamTextBox.Text = utoirat_iktatoszam;
            //LZS
        }
    }

    /// <summary>
    /// Kiadmányozás dátumát beállítja az IratAlairok tábla áláírás dátuma mezõjébõl
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetKiadmanyozasDatuma(EREC_IraIratok erec_IraIratok)
    {
        ccKiadmanyozasDatuma.Text = String.Empty;
        //Csak, ha kiadmányozott az irat
        if (erec_IraIratok.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
        {
            if (!String.IsNullOrEmpty(erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany) && !String.IsNullOrEmpty(ParentId))
            {
                EREC_IratAlairokSearch schAlairok = new EREC_IratAlairokSearch();
                schAlairok.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                schAlairok.AlairoSzerep.Operator = Query.Operators.equals;
                schAlairok.FelhasznaloCsoport_Id_Alairo.Value = erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany;
                schAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
                schAlairok.Obj_Id.Value = ParentId;
                schAlairok.Obj_Id.Operator = Query.Operators.equals;
                ExecParam xpmAlairok = UI.SetExecParamDefault(Page);
                EREC_IratAlairokService svcAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                Result resAlairok = svcAlairok.GetAll(xpmAlairok, schAlairok);
                if (resAlairok.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resAlairok);
                    return;
                }

                if (resAlairok.Ds != null && resAlairok.Ds.Tables.Count > 0 && resAlairok.Ds.Tables[0].Rows.Count > 0)
                {
                    string alairasDatuma = resAlairok.Ds.Tables[0].Rows[0]["AlairasDatuma"].ToString();
                    ccKiadmanyozasDatuma.Text = alairasDatuma;
                }
            }
        }
    }

    // form --> business object
    private EREC_IraIratok GetBusinessObjectFromComponents()
    {
        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        // CR3221 Szignálás(elõkészítés) kezelés
        // Ha üres az Irat_Targy mezõ akkor bemásoljuk az ügyirat tárgyát
        if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
        {
            IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
        }
        erec_IraIratok.Targy = IraIrat_Targy_RequiredTextBox.Text;
        erec_IraIratok.Updated.Targy = pageView.GetUpdatedByView(IraIrat_Targy_RequiredTextBox);

        //erec_IraIratok.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;
        //erec_IraIratok.Updated.Kategoria = pageView.GetUpdatedByView(IraIrat_Kategoria_KodtarakDropDownList);

        erec_IraIratok.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.Irattipus = pageView.GetUpdatedByView(IraIrat_Irattipus_KodtarakDropDownList);

        erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(Irat_Ugyintezo_FelhasznaloCsoportTextBox);

        // BLG_1721
        // Backlog kapcsán derült ki, hogy  bejövõ irat iktatásnál kiadmányozandó fixen 0.
        if ((Mode == CommandName.BejovoIratIktatas) || (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo))
        {
            erec_IraIratok.KiadmanyozniKell = "0";
        }
        else
            erec_IraIratok.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";
        erec_IraIratok.Updated.KiadmanyozniKell = pageView.GetUpdatedByView(IraIrat_KiadmanyozniKell_CheckBox);

        erec_IraIratok.Minosites = ktDropDownListIratMinosites.SelectedValue;
        erec_IraIratok.Updated.Minosites = pageView.GetUpdatedByView(ktDropDownListIratMinosites);

        // BLG_1020
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            LoadUgyintezesiNapokBoFromComponents(erec_IraIratok);
            erec_IraIratok.IntezesiIdoegyseg = IratIntezesiIdoegyseg_KodtarakDropDownList.SelectedValue;
            erec_IraIratok.Updated.IntezesiIdoegyseg = pageView.GetUpdatedByView(IratIntezesiIdoegyseg_KodtarakDropDownList);
        }

        erec_IraIratok.Hatarido = IraIrat_Hatarido_CalendarControl.Text;
        erec_IraIratok.Updated.Hatarido = pageView.GetUpdatedByView(IraIrat_Hatarido_CalendarControl);

        //erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany = IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Id_HiddenField;
        //erec_IraIratok.Updated.FelhasznaloCsoport_Id_Kiadmany = pageView.GetUpdatedByView(IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox);        

        /// BLG#3451: Vegyes típusú irat:
        /// Az irat szinten az adathordozó típusa felületről nem módosítható, az iratpéldány(ok) alapján állítódik be (iratpéldány fajtája értékek alapján).
        /// 
        // Iktatásnál beállítjuk a mezőt, módosításnál nem (majd a webservice-ben újrakalkulálódik):
        if (Command == CommandName.New)
        {
            erec_IraIratok.AdathordozoTipusa = AdathordozoTipusa_KodtarakDropDownList.SelectedValue;
            erec_IraIratok.Updated.AdathordozoTipusa = pageView.GetUpdatedByView(AdathordozoTipusa_KodtarakDropDownList);
        }

        erec_IraIratok.UgyintezesAlapja = Pld_UgyintezesModja_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.UgyintezesAlapja = pageView.GetUpdatedByView(Pld_UgyintezesModja_KodtarakDropDownList);

        //erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.Webes;
        erec_IraIratok.Jelleg = IratJellegKodtarakDropDown.SelectedValue;
        erec_IraIratok.Updated.Jelleg = true;

        //bernat.laszlo added
        ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
        erec_IraIratok.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
        erec_IraIratok.Updated.Munkaallomas = true;
        erec_IraIratok.UgyintezesModja = Ugyint_modja_Dropdown.SelectedValue;
        erec_IraIratok.Updated.UgyintezesModja = pageView.GetUpdatedByView(Ugyint_modja_Dropdown);
        erec_IraIratok.PostazasIranya = Irat_Iranya_DropDownList.SelectedValue;
        erec_IraIratok.Updated.PostazasIranya = pageView.GetUpdatedByView(Irat_Iranya_DropDownList);
        erec_IraIratok.IntezesIdopontja = IntezesIdopontja_CalendarControl.Text;
        erec_IraIratok.Updated.IntezesIdopontja = pageView.GetUpdatedByView(IntezesIdopontja_CalendarControl);
        erec_IraIratok.FelhasznaloCsoport_Id_Iktato = Iktato_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IraIratok.Updated.FelhasznaloCsoport_Id_Iktato = pageView.GetUpdatedByView(Iktato_FelhasznaloCsoportTextBox);

        erec_IraIratok.HivatkozasiSzam = HivatkozasiSzam_Irat_TextBox.Text;
        erec_IraIratok.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(HivatkozasiSzam_Irat_TextBox);

        erec_IraIratok.UgyintezesKezdoDatuma = CalendarControl_UgyintezesKezdoDatuma.Text;
        erec_IraIratok.Updated.UgyintezesKezdoDatuma = pageView.GetUpdatedByView(CalendarControl_UgyintezesKezdoDatuma);
        //bernat.laszlo eddig

        erec_IraIratok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IraIratok.Base.Updated.Ver = true;

        if (!string.IsNullOrEmpty(ElosztoivTextBox.Id_HiddenField))
        {
            //erec_IraIratok.Base.Note = ElosztoivTextBox.Id_HiddenField;
            erec_IraIratok.Base.Note = Iratok.SetElosztoivIdForBusinessObjectsXML(erec_IraIratok.Base.Note, ElosztoivTextBox.Id_HiddenField);
            erec_IraIratok.Base.Updated.Note = true;
        }

        // BLG_619
        erec_IraIratok.Csoport_Id_Ugyfelelos = Iratfelelos_CsoportTextBox.Id_HiddenField;
        erec_IraIratok.Updated.Csoport_Id_Ugyfelelos = pageView.GetUpdatedByView(Iratfelelos_CsoportTextBox);

        SetFelelosSzervezetiEgysegHaBelsoIktatas(ref erec_IraIratok);

        #region BLG_1051
        LoadIratHatasaBoFromComponents(erec_IraIratok);
        #endregion BLG_1051

        LoadEljarasiSzakaszFokBoFromComponents(erec_IraIratok);

        LoadLezarasOkaBoFromComponents(erec_IraIratok);
        #region BLG_1402

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        if (isTUK)
        {
            erec_IraIratok.MinositesErvenyessegiIdeje = CalendarControlMinositesErvenyessegIdeje.SelectedDateText;
            erec_IraIratok.Updated.MinositesErvenyessegiIdeje = pageView.GetUpdatedByView(CalendarControlMinositesErvenyessegIdeje);

            erec_IraIratok.MinositoSzervezet = MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId;
            erec_IraIratok.Updated.MinositoSzervezet = pageView.GetUpdatedByView(MinositoPartnerControlIraIratokFormTab);

            erec_IraIratok.Minosito = MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId;
            erec_IraIratok.Updated.Minosito = pageView.GetUpdatedByView(MinositoPartnerControlIraIratokFormTab);

            string megj, menny, mennyEgys;
            TerjedelemPanelA.GetTerjedelem(out menny, out mennyEgys, out megj);

            erec_IraIratok.TerjedelemMennyiseg = menny;
            erec_IraIratok.Updated.TerjedelemMennyiseg = pageView.GetUpdatedByView(TerjedelemPanelA);

            erec_IraIratok.TerjedelemMennyisegiEgyseg = mennyEgys;
            erec_IraIratok.Updated.TerjedelemMennyisegiEgyseg = pageView.GetUpdatedByView(TerjedelemPanelA);

            erec_IraIratok.TerjedelemMegjegyzes = megj;
            erec_IraIratok.Updated.TerjedelemMegjegyzes = pageView.GetUpdatedByView(TerjedelemPanelA);
        }

        #endregion

        erec_IraIratok.Ugy_Fajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.Ugy_Fajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);

        return erec_IraIratok;
    }

    // kezelési feljegyzések businessobject összeálllítása:
    //private EREC_IraKezFeljegyzesek GetBusinessObjectFromComponents_EREC_IraKezFeljegyzesek()
    //{
    //    EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = new EREC_IraKezFeljegyzesek();
    //    // összes mezõ update-elhetõségét kezdetben letiltani:
    //    erec_IraKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_IraKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_IraKezFeljegyzesek.KezelesTipus = IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue;
    //    erec_IraKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(IraKezFeljegyz_KezelesTipus_KodtarakDropDownList);

    //    erec_IraKezFeljegyzesek.Leiras = IraKezFeljegyz_Leiras_TextBox.Text;
    //    erec_IraKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(IraKezFeljegyz_Leiras_TextBox);

    //    return erec_IraKezFeljegyzesek;
    //}

    // Az ügyiratokra vonatkozó néhány komponensbõl egy EREC_UgyUgyiratok objektum felépítése
    private EREC_UgyUgyiratok GetBusinessObjectFromComponents_EREC_UgyUgyiratok()
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_UgyUgyiratok.Updated.SetValueAll(false);
        erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

        // csak New -nál van értelme, és ha nincs elõzmény (nincs betöltve ügyirat) vagy az elõzmény lezárt iktatókönyvben van
        if (Command == CommandName.New
             && (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value) || !String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value)))
        {
            //erec_UgyUgyiratok.IktatoszamKieg = UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.IktatoszamKieg = pageView.GetUpdatedByView(UgyUgyirat_IktatoszamKieg_KodtarakDropDownList);

            //erec_UgyUgyiratok.IraIrattariTetel_Id = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField;
            //erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox);

            // BLG_44
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                erec_UgyUgyiratok.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(Ugykor_DropDownList);
            }
            else
            {
                erec_UgyUgyiratok.IraIrattariTetel_Id = IrattariTetelszam_DropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(IrattariTetelszam_DropDownList);

            }


            //erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_KodtarakDropDownList);

            erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_DropDownList);

            if (Uj_Ugyirat_Hatarido_Kezeles)
            {
                erec_UgyUgyiratok.IntezesiIdo = IntezesiIdo_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IntezesiIdo = pageView.GetUpdatedByView(IntezesiIdo_KodtarakDropDownList);
                erec_UgyUgyiratok.IntezesiIdoegyseg = IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = pageView.GetUpdatedByView(IntezesiIdoegyseg_KodtarakDropDownList);
            }

            erec_UgyUgyiratok.Hatarido = UgyUgyirat_Hatarido_CalendarControl.Text;
            erec_UgyUgyiratok.Updated.Hatarido = pageView.GetUpdatedByView(UgyUgyirat_Hatarido_CalendarControl);

            erec_UgyUgyiratok.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;
            erec_UgyUgyiratok.Updated.Targy = pageView.GetUpdatedByView(UgyUgyirat_Targy_RequiredTextBox);

            // kezelõ:
            erec_UgyUgyiratok.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(UgyUgyiratok_CsoportId_Felelos);

            //ügyintézõ
            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox);

            // ügyfelelõs:
            erec_UgyUgyiratok.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = pageView.GetUpdatedByView(Ugyfelelos_CsoportTextBox);

            // ügyindító/ügyfél
            erec_UgyUgyiratok.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            erec_UgyUgyiratok.Updated.NevSTR_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Cim_Id_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);
            erec_UgyUgyiratok.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;
            erec_UgyUgyiratok.Updated.CimSTR_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);

            // BLG_44
            erec_UgyUgyiratok.Ugy_Fajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.Ugy_Fajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);

            // régi rendszer iktatószáma:
            if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value) && !string.IsNullOrEmpty(regiAzonositoTextBox.Text))
            {
                erec_UgyUgyiratok.RegirendszerIktatoszam = regiAzonositoTextBox.Text;
                erec_UgyUgyiratok.Updated.RegirendszerIktatoszam = true;
                IktatasiParameterek.RegiAdatAzonosito = regiAzonositoTextBox.Text;
                IktatasiParameterek.RegiAdatId = MigraltUgyiratID_HiddenField.Value;

            }

            erec_UgyUgyiratok.UgyintezesKezdete = GetUgyintezesKezdete().ToString();
            erec_UgyUgyiratok.Updated.UgyintezesKezdete = true;

            if (IratHatasaUgyintezesre_KodtarakDropDownList.Visible && !string.IsNullOrEmpty(IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue))
            {
                Contentum.eUtility.Sakkora.SetUgySakkoraAllapotaFromIrat(IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue, ref erec_UgyUgyiratok);
            }

            if (isTUKRendszer)
            {
                erec_UgyUgyiratok.IrattarId = IrattariHelyLevelekDropDownTUKUgyirat.SelectedValue;
                erec_UgyUgyiratok.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUKUgyirat);
                erec_UgyUgyiratok.IrattariHely = IrattariHelyLevelekDropDownTUKUgyirat.SelectedText;
                erec_UgyUgyiratok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUKUgyirat);
            }

        }
        else if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            if (Command == CommandName.New)
            {
                // Alszámra iktatás és speciális jogok esetén, ha változott az ügyirat határideje, akkor azt is módosítani kell
                #region ügyirat határidõ módosítás, ha szükséges
                if (((!String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value) || (IsUgyintezo() && IsOrzo())) && bUgyiratHataridoModositasiJog) && IsUgyiratHataridoChanged())
                {
                    IktatasiParameterek.UgyiratUjHatarido = UgyUgyirat_Hatarido_CalendarControl.Text;
                }
                #endregion ügyirat határidõ módosítás, ha szükséges
            }
        }

        #region SAKKORA ALLAPOTA
        if (!string.IsNullOrEmpty(IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratok.SakkoraAllapot = IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.SakkoraAllapot = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);
        }
        else if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page, new ExecParam()))) //SAKKORAS UGYFEL
        {
            erec_UgyUgyiratok.SakkoraAllapot = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
            erec_UgyUgyiratok.Updated.SakkoraAllapot = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);
        }
        #endregion

        #region REFRESH UGYINTEZES KEZDETE
        Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(UI.SetExecParamDefault(Page), ref erec_UgyUgyiratok);
        #endregion

        return erec_UgyUgyiratok;
    }

    private bool IsUgyiratHataridoChanged()
    {
        DateTime elozmenyDate;
        DateTime ugyiratDate;
        bool isElozmenyDateParsed = DateTime.TryParse(ElozmenyUgyiratHatarido_HiddenField.Value, out elozmenyDate);
        bool isUgyiratDateParsed = DateTime.TryParse(UgyUgyirat_Hatarido_CalendarControl.Text, out ugyiratDate);
        if (isElozmenyDateParsed && isUgyiratDateParsed && elozmenyDate != ugyiratDate)
        {
            return true;
        }
        return false;
    }

    private bool IsUgyintezo()
    {
        return (UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField == FelhasznaloProfil.FelhasznaloId(Page));
    }

    private bool IsOrzo()
    {
        return (FelhasznaloCsoportIdOrzo_HiddenField.Value == FelhasznaloProfil.FelhasznaloId(Page));
    }

    // Az ügyiratdarabokra vonatkozó néhány komponensbõl egy EREC_UgyUgyiratdarabok objektum felépítése
    private EREC_UgyUgyiratdarabok GetBusinessObjectFromComponents_EREC_UgyUgyiratdarabok()
    {
        EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_UgyUgyiratdarabok.Updated.SetValueAll(false);
        erec_UgyUgyiratdarabok.Base.Updated.SetValueAll(false);

        // csak New -nál van értelme
        if (Command == CommandName.New)
        {
            //erec_UgyUgyiratdarabok.EljarasiSzakasz = UgyUgyiratDarab_EljarasiSzakasz_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratdarabok.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(UgyUgyiratDarab_EljarasiSzakasz_KodtarakDropDownList);

            //erec_UgyUgyiratdarabok.Leiras = UgyUgyIratDarab_Leiras_TextBox.Text;
            //erec_UgyUgyiratdarabok.Updated.Leiras = pageView.GetUpdatedByView(UgyUgyIratDarab_Leiras_TextBox);

            //erec_UgyUgyiratdarabok.Hatarido = UgyUgyiratDarab_Hatarido_CalendarControl.Text;
            //erec_UgyUgyiratdarabok.Updated.Hatarido = pageView.GetUpdatedByView(UgyUgyiratDarab_Hatarido_CalendarControl);

            //erec_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = UgyUgyiratDarab_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
            //erec_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(UgyUgyiratDarab_Ugyintezo_FelhasznaloCsoportTextBox);
        }

        return erec_UgyUgyiratdarabok;
    }

    // Az iratpéldányokra vonatkozó néhány komponensbõl egy EREC_PldIratPeldanyok objektum felépítése
    private EREC_PldIratPeldanyok GetBusinessObjectFromComponents_EREC_PldIratPeldanyok()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        // csak New -nál van értelme
        if (Command == CommandName.New)
        {
            erec_PldIratPeldanyok.Partner_Id_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
            erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

            erec_PldIratPeldanyok.NevSTR_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Text;
            erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

			// BUG:13213
            erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt = Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue;
            erec_PldIratPeldanyok.Updated.Partner_Id_CimzettKapcsolt = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

            erec_PldIratPeldanyok.KuldesMod = Pld_KuldesMod_KodtarakDropDownList.SelectedValue;
            erec_PldIratPeldanyok.Updated.KuldesMod = pageView.GetUpdatedByView(Pld_KuldesMod_KodtarakDropDownList);

            erec_PldIratPeldanyok.Cim_id_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField;
            erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);

            erec_PldIratPeldanyok.CimSTR_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Text;
            erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);

            //erec_PldIratPeldanyok.Tovabbito = Pld_Tovabbito_KodtarakDropDownList.SelectedValue;
            //erec_PldIratPeldanyok.Updated.Tovabbito = pageView.GetUpdatedByView(Pld_Tovabbito_KodtarakDropDownList);

            erec_PldIratPeldanyok.Visszavarolag = Pld_Visszavarolag_KodtarakDropDownList.SelectedValue;
            erec_PldIratPeldanyok.Updated.Visszavarolag = pageView.GetUpdatedByView(Pld_Visszavarolag_KodtarakDropDownList);

            //erec_PldIratPeldanyok.Ragszam = Pld_Ragszam_TextBox.Text;
            //erec_PldIratPeldanyok.Updated.Ragszam = pageView.GetUpdatedByView(Pld_Ragszam_TextBox);

            erec_PldIratPeldanyok.VisszaerkezesiHatarido = Pld_VisszaerkezesiHatarido_CalendarControl.Text;
            erec_PldIratPeldanyok.Updated.VisszaerkezesiHatarido = pageView.GetUpdatedByView(Pld_VisszaerkezesiHatarido_CalendarControl);

            erec_PldIratPeldanyok.BarCode = Pld_BarCode_VonalKodTextBox.Text;
            erec_PldIratPeldanyok.Updated.BarCode = pageView.GetUpdatedByView(Pld_BarCode_VonalKodTextBox);

            erec_PldIratPeldanyok.UgyintezesModja = Pld_IratpeldanyFajtaja_KodtarakDropDownList.SelectedValue; //Pld_UgyintezesModja_KodtarakDropDownList.SelectedValue;
            erec_PldIratPeldanyok.Updated.UgyintezesModja = pageView.GetUpdatedByView(Pld_IratpeldanyFajtaja_KodtarakDropDownList); //pageView.GetUpdatedByView(Pld_UgyintezesModja_KodtarakDropDownList);

            if (isTUKRendszer)
            {
                erec_PldIratPeldanyok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
                erec_PldIratPeldanyok.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
                erec_PldIratPeldanyok.IrattariHely = IrattariHelyLevelekDropDownTUK.SelectedText;
                erec_PldIratPeldanyok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            }
        }

        return erec_PldIratPeldanyok;
    }

    private void LoadKuldemenyComponents()
    {
        LoadKuldemenyComponents(String.Empty);
    }
    /// <summary>
    /// Küldemény adatainak feltöltése adatbázisból
    /// Ha iktatás van, ellenõrzés, hogy a küldemény iktatható-e
    /// </summary>
    private void LoadKuldemenyComponents(string vonalkod)
    {
        bool loadByVonalkod = !String.IsNullOrEmpty(vonalkod);
        if (!String.IsNullOrEmpty(Kuldemeny_Id_HiddenField.Value) || loadByVonalkod)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = new Result();

            if (loadByVonalkod)
            {
                result = service.GetByBarCode(execParam, vonalkod);
            }
            else
            {
                execParam.Record_Id = Kuldemeny_Id_HiddenField.Value;
                result = service.Get(execParam);
            }

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result.Record;


                if (erec_KuldKuldemenyek != null)
                {
                    if (loadByVonalkod)
                    {
                        Kuldemeny_Id_HiddenField.Value = erec_KuldKuldemenyek.Id;

                        if (String.IsNullOrEmpty(erec_KuldKuldemenyek.Id))
                        {
                            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "Sikertelen keresés!", String.Format("Nem található ilyen vonalkódú ({0}) küldemény a rendszerben.", vonalkod));
                            ErrorUpdatePanel1.Update();
                            ClearKuldemenyComponets();
                            return;
                        }
                    }

                    // vonalkód mezõ kiürítése elõre hozva, másképp a küldeményhez elõkészített iratból visszaírt kódot is töröljük
                    if (TabFooter1.ConfirmationResult.Value != "1")
                    {
                        Pld_BarCode_VonalKodTextBox.TextBox.Text = string.Empty;
                        Pld_BarCode_VonalKodTextBox.ReadOnly = false;
                    }

                    #region Ellenõrzés iktatás esetén:
                    if (Command == CommandName.New)
                    {
                        Kuldemenyek.Statusz kuldemenyStatusz =
                             Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);

                        IratPeldanyPanel.Visible = (Mode != CommandName.AtIktatas && Command == CommandName.New);

                        // ha ÁtIktatás vagy elõkészített irat iktatás van, kihagyjuk az ellenõrzést:
                        if (Mode != CommandName.AtIktatas && Mode != CommandName.ElokeszitettIratIktatas)
                        {
                            ErrorDetails errorDetail = null;

                            if (Kuldemenyek.Iktathato(execParam, kuldemenyStatusz, out errorDetail) == false)
                            {
                                string erkSzam = erec_KuldKuldemenyek.Azonosito;
                                string onclick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + erec_KuldKuldemenyek.Id
                                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                                erkSzam = " A küldemény érkeztetési azonosítója: " + "<span style=\"text-decoration:underline;cursor:pointer\" onclick=\"" + onclick + "\">" + erkSzam + "</span>";

                                if (execParam.Felhasznalo_Id != kuldemenyStatusz.FelhCsopId_Orzo)
                                {
                                    //Nincs jogom
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader,
                                         Resources.Error.UINincsJogIktathatoKuldemeny + erkSzam);
                                }
                                else
                                {
                                    // Nem iktatható, hiba
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader,
                                         Resources.Error.UINemIktathatoKuldemeny + erkSzam);

                                }
                                ErrorUpdatePanel1.Update();

                                ClearKuldemenyComponets();

                                if (kuldemenyStatusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva)
                                {
                                    SetIktatasMegtagadvaControls();
                                }
                                else
                                {
                                    // HiddenField értékét töröljük, hogy ne legyen kiválasztva küldemény
                                    Kuldemeny_Id_HiddenField.Value = "";
                                    //LoadKuldemenyComponents(erec_KuldKuldemenyek);
                                }
                                return;
                            }
                        }

                        // ha elõkészített irat beiktatása van, nem vágjuk fejbe a tárgyat:
                        if (Mode != CommandName.ElokeszitettIratIktatas && TabFooter1.ConfirmationResult.Value != "1")
                        {
                            IraIrat_Targy_RequiredTextBox.Text = erec_KuldKuldemenyek.Targy;
                        }

                        #region Van-e már elõkészített adat az iktatáshoz:

                        if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id) && Mode != CommandName.AtIktatas)
                        {
                            // Irat lekérése:
                            EREC_IraIratokService service_Iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_iratGet.Record_Id = erec_KuldKuldemenyek.IraIratok_Id;

                            Result result_IratGet = service_Iratok.Get(execParam_iratGet);
                            if (!String.IsNullOrEmpty(result_IratGet.ErrorCode))
                            {
                                // hiba:
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_IratGet);
                                ErrorUpdatePanel1.Update();
                                return;
                            }

                            EREC_IraIratok elokeszitettIrat = (EREC_IraIratok)result_IratGet.Record;

                            // Lehet-e már iktatni az elõkészített küldeményt?

                            // Ha szervezetre szignálás szükséges, és még nincs megadva az iratban az ügyfelelõs:
                            if (String.IsNullOrEmpty(elokeszitettIrat.Csoport_Id_Ugyfelelos)
                                 && (erec_KuldKuldemenyek.Iktathato == "S" || erec_KuldKuldemenyek.Iktathato == "U"))
                            {
                                // Nem iktatható még, nincs szervezetre szignálva:
                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.ErrorCode_52596);
                                ErrorUpdatePanel1.Update();
                                FoPanel.Visible = false;
                                return;
                            }
                            //else if (erec_KuldKuldemenyek.Iktathato == "U" && String.IsNullOrEmpty(elokeszitettIrat.FelhasznaloCsoport_Id_Ugyintez))
                            else if (erec_KuldKuldemenyek.Iktathato == "U" && !Iratok.IsElokeszitettIratSzignalt(elokeszitettIrat))
                            {
                                // Nem iktatható még, nincs ügyintézõre szignálva:
                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.ErrorCode_52597);
                                ErrorUpdatePanel1.Update();
                                FoPanel.Visible = false;
                                return;
                            }

                            // Ügyintézõ kitöltése, ha már megadták:
                            //if (!String.IsNullOrEmpty(elokeszitettIrat.FelhasznaloCsoport_Id_Ugyintez))
                            if (!String.IsNullOrEmpty(Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez")))
                            {
                                //UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = elokeszitettIrat.FelhasznaloCsoport_Id_Ugyintez;
                                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez");
                                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
                            }
                            // Ügyfelelõs, ha van:
                            if (!String.IsNullOrEmpty(elokeszitettIrat.Csoport_Id_Ugyfelelos))
                            {
                                Ugyfelelos_CsoportTextBox.Id_HiddenField = elokeszitettIrat.Csoport_Id_Ugyfelelos;
                                Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                                Ugyfelelos_CsoportTextBox.ReadOnly = true;

                                //tr_Ugyfelelos.Visible = true;
                            }
                            // Tárgy mezõ kitöltése, ha meg volt adva:
                            if (!String.IsNullOrEmpty(elokeszitettIrat.Targy))
                            {
                                IraIrat_Targy_RequiredTextBox.Text = elokeszitettIrat.Targy;
                                //UgyUgyirat_Targy_RequiredTextBox.Text = elokeszitettIrat.Targy;
                            }

                            // ha Szignálás típusa : 1 (Szervezetre, ügyintézõre, iktatás), akkor a kijelölt ügyintézõnek kell majd átküldeni az ügyiratot
                            if (erec_KuldKuldemenyek.Iktathato == "U")
                            {
                                // ügyintézõnek kell elküldeni
                                //UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = elokeszitettIrat.FelhasznaloCsoport_Id_Ugyintez;
                                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez");
                                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);
                                UgyUgyiratok_CsoportId_Felelos.Visible = true;
                                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
                                //Label_KezeloSzignJegyzekAlapjan.Visible = false;
                            }
                            else if (erec_KuldKuldemenyek.Iktathato == "S")
                            {
                                //// Ha szervezetre lett szignálva, az ügyfelelõsnek kell elküldeni
                                //UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = elokeszitettIrat.Csoport_Id_Ugyfelelos;
                                // CR#2241 2009.12.17: Az ügyfelelõs szervezet, de mi a vezetõnek küldjük
                                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = elokeszitettIrat.Csoport_Id_Felelos;
                                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);
                                UgyUgyiratok_CsoportId_Felelos.Visible = true;
                                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
                                //Label_KezeloSzignJegyzekAlapjan.Visible = false;
                            }

                            // IratMetaDef. van-e?
                            if (!String.IsNullOrEmpty(elokeszitettIrat.IratMetaDef_Id))
                            {
                                // IratMetaDef lekérése:
                                EREC_IratMetaDefinicioService service_iratMetaDef = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                                ExecParam execParam_iratMetaDefGet = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam_iratMetaDefGet.Record_Id = elokeszitettIrat.IratMetaDef_Id;

                                Result result_iratMetaDefGet = service_iratMetaDef.Get(execParam_iratMetaDefGet);
                                if (!String.IsNullOrEmpty(result_iratMetaDefGet.ErrorCode))
                                {
                                    // hiba:
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iratMetaDefGet);
                                    ErrorUpdatePanel1.Update();
                                    return;
                                }
                                else
                                {
                                    EREC_IratMetaDefinicio iratMetaDef = (EREC_IratMetaDefinicio)result_iratMetaDefGet.Record;

                                    if (iratMetaDef != null && !String.IsNullOrEmpty(iratMetaDef.Ugykor_Id))
                                    {
                                        Ugykor_DropDownList.Items.Clear();
                                        Ugykor_DropDownList.Items.Add(new ListItem(iratMetaDef.UgykorKod, iratMetaDef.Ugykor_Id));
                                        //Ugykor_DropDownList.SelectedIndex = 0;
                                        Ugykor_DropDownList.Enabled = false;
                                        //UI.CreateTextboxFromDropdownlistWithJs(Ugykor_DropDownList);

                                        UgyUgyirat_Ugytipus_DropDownList.Items.Clear();
                                        UgyUgyirat_Ugytipus_DropDownList.Items.Add(new ListItem(iratMetaDef.UgytipusNev, iratMetaDef.Ugytipus));
                                        //UgyUgyirat_Ugytipus_DropDownList.SelectedIndex = 0;
                                        UgyUgyirat_Ugytipus_DropDownList.Enabled = false;
                                        //UI.CreateTextboxFromDropdownlistWithJs(UgyUgyirat_Ugytipus_DropDownList);

                                        CascadingDropDown_AgazatiJel.Enabled = false;
                                        CascadingDropDown_Ugykor.Enabled = false;
                                        CascadingDropDown_Ugytipus.Enabled = false;
                                        //CascadingDropDown_IktatoKonyv.Enabled = false;

                                        CascadingDropDown_Ugykor.EnableViewState = false;
                                        CascadingDropDown_Ugykor.EnableClientState = false;
                                        CascadingDropDown_Ugytipus.EnableViewState = false;
                                        CascadingDropDown_Ugytipus.EnableClientState = false;

                                        Page.Controls.Remove(CascadingDropDown_AgazatiJel);
                                        Page.Controls.Remove(CascadingDropDown_Ugykor);
                                        Page.Controls.Remove(CascadingDropDown_Ugytipus);

                                        CascadingDropDown_IktatoKonyv.ParentControlID = "";


                                        CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.GetFelhasznaloCsoport(execParam) + ";" + iratMetaDef.Ugykor_Id;

                                        tr_agazatiJel.Visible = false;

                                        //IktatoKonyvek.FillDropDownListByIrattariTetel(Iktatokonyvek_DropDownLis_Ajax, true, Constants.IktatoErkezteto.Iktato
                                        //        , iratMetaDef.Ugykor_Id, Page, EErrorPanel1);
                                    }
                                }
                            }

                            #region Elõkészített iratból ügyirat és példány jellemzõk kitöltése
                            if (!String.IsNullOrEmpty(elokeszitettIrat.Base.Note))
                            {
                                // a sorosítás eredménye sajnos nem fér be a Note mezõbe
                                //EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)Contentum.eUtility.XmlFunction.XmlToObject(elokeszitettIrat.Base.Note, typeof(EREC_UgyUgyiratok));

                                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                                try
                                {
                                    UgyUgyirat_Targy_RequiredTextBox.Text = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, "EREC_UgyUgyiratok", "Targy");
                                    CascadingDropDown_IktatoKonyv.SelectedValue = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, "EREC_UgyUgyiratok", "IraIktatokonyv_Id");
                                    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, "EREC_UgyUgyiratok", "FelhasznaloCsoport_Id_Ugyintez");
                                    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                                    Pld_BarCode_VonalKodTextBox.Text = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, "EREC_PldIratPeldanyok", "BarCode");

                                    ElosztoivTextBox.Id_HiddenField = Iratok.GetElosztoivIdFromBusinessObjectsXML(elokeszitettIrat.Base.Note);
                                    ElosztoivTextBox.SetElosztoivTextBoxById(EErrorPanel1);

                                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = Iratok.GetBusinessObjectFieldFromXML(elokeszitettIrat.Base.Note, "EREC_IraIratok", "FelhasznaloCsoport_Id_Ugyintez");
                                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                                }
                                catch (System.Xml.XmlException e)
                                {
                                    // nem megfelelõ az XML formátuma...
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, e.Message);
                                }
                            }
                            #endregion Elõkészített iratból ügyirat és példány jellemzõk kitöltése

                            #region Elõkészített iratból irat jellemzõk kitöltése
                            // irat további jellemzõinek átvétele:
                            #region Irat panelen megadott beállítások
                            if (!String.IsNullOrEmpty(elokeszitettIrat.Irattipus))
                            {
                                ListItem irattipusItem = IraIrat_Irattipus_KodtarakDropDownList.DropDownList.Items.FindByValue(elokeszitettIrat.Irattipus);
                                if (irattipusItem != null)
                                {
                                    irattipusItem.Selected = true;
                                }
                                else
                                {
                                    IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, elokeszitettIrat.Irattipus, true, EErrorPanel1);
                                }

                                #region irat határidõ
                                RegisterStartupScript("setHatarido", "IrattipusOnChange();");
                                #endregion irat határidõ
                            }

                            if (!String.IsNullOrEmpty(elokeszitettIrat.Minosites))
                            {
                                ListItem iratminositesItem = ktDropDownListIratMinosites.DropDownList.Items.FindByValue(elokeszitettIrat.Minosites);
                                if (iratminositesItem != null)
                                {
                                    iratminositesItem.Selected = true;
                                }
                                else
                                {
                                    ktDropDownListIratMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, elokeszitettIrat.Minosites, true, EErrorPanel1);
                                }
                            }

                            if (!String.IsNullOrEmpty(elokeszitettIrat.Jelleg))
                            {
                                ListItem iratjellegItem = IratJellegKodtarakDropDown.DropDownList.Items.FindByValue(elokeszitettIrat.Jelleg);
                                if (iratjellegItem != null)
                                {
                                    iratjellegItem.Selected = true;
                                }
                                else
                                {
                                    IratJellegKodtarakDropDown.FillWithOneValue(kcs_IRAT_JELLEG, elokeszitettIrat.Jelleg, EErrorPanel1);
                                }
                            }

                            #region küldemény tartalmazza
                            // küldemény tartalmazza:
                            // AdathordozoTipusa_DropDownList,
                            // - Pld_UgyintezesModja_KodtarakDropDownList ("elsõdleges adathordozó típusa")
                            // beállítások (a LoadKuldemenyComponents(erec_KuldKuldemenyek) felülírná, ha itt állítjuk)


                            //if (!String.IsNullOrEmpty(elokeszitettIrat.AdathordozoTipusa))
                            //{
                            //    ListItem adathordozoTipusaItem = AdathordozoTipusa_DropDownList.DropDownList.Items.FindByValue(elokeszitettIrat.AdathordozoTipusa);
                            //    if (adathordozoTipusaItem != null)
                            //    {
                            //        adathordozoTipusaItem.Selected = true;
                            //    }
                            //    else
                            //    {
                            //        AdathordozoTipusa_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, elokeszitettIrat.AdathordozoTipusa, EErrorPanel1);
                            //    }
                            //}

                            //if (!String.IsNullOrEmpty(elokeszitettIrat.UgyintezesAlapja))
                            //{
                            //    ListItem ugyintezesAlapjaItem = Pld_UgyintezesModja_KodtarakDropDownList.DropDownList.Items.FindByValue(elokeszitettIrat.UgyintezesAlapja);
                            //    if (ugyintezesAlapjaItem != null)
                            //    {
                            //        ugyintezesAlapjaItem.Selected = true;
                            //    }
                            //    else
                            //    {
                            //        Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, elokeszitettIrat.UgyintezesAlapja, EErrorPanel1);
                            //    }
                            //}
                            #endregion küldemény tartalmazza

                            #endregion Irat panelen megadott beállítások
                            #endregion Elõkészített iratból irat jellemzõk kitöltése
                        }
                        #endregion Van-e már elõkészített adat az iktatáshoz
                    }

                    #endregion

                    LoadKuldemenyComponents(erec_KuldKuldemenyek);

                    if (Command == CommandName.View && String.IsNullOrEmpty(erec_KuldKuldemenyek.Cim_Id))
                    {
                        Kuld_CimId_CimekTextBox.ImageButton_View.Enabled = false;
                        Kuld_CimId_CimekTextBox.ImageButton_View.CssClass = "disabledLovListItem";
                    }

                    if (isTUKRendszer)
                    {
                        if (Command == "New" && Mode == CommandName.BejovoIratIktatas)
                        {
                            if (!string.IsNullOrEmpty(erec_KuldKuldemenyek.IrattarId))
                            {
                                BejovoPeldanyListPanel1.FizikaiHelyValue = erec_KuldKuldemenyek.IrattarId;
                            }
                        }
                        //else if (Request.Params["__EVENTARGUMENT"]!=null)
                        //{
                        //    string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                        //    if (&& eventArgument == EventArgumentConst.refreshKuldemenyekPanel
                        //}
                    }

                    if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                    {
                        if (String.IsNullOrEmpty(Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField))
                        {
                            // ügyindító, ügyfél
                            Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_KuldKuldemenyek.Partner_Id_Bekuldo, erec_KuldKuldemenyek.NevSTR_Bekuldo, EErrorPanel1);
                        }

                        if (String.IsNullOrEmpty(CimekTextBoxUgyindito.Id_HiddenField))
                        {
                            CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_KuldKuldemenyek.Cim_Id, erec_KuldKuldemenyek.CimSTR_Bekuldo, EErrorPanel1);
                        }
                    }

                    #region irat vonalkódjának kezelése
                    // küldeményhez tartozik-e papíralapú melléklet (irat) --> annak a vonalkódját beírjuk
                    EREC_MellekletekSearch search = new EREC_MellekletekSearch();

                    search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.PapirAlapu;
                    search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;

                    search.KuldKuldemeny_Id.Value = erec_KuldKuldemenyek.Id;
                    search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    EREC_MellekletekService mellekletService = eRecordService.ServiceFactory.GetEREC_MellekletekService();

                    Result mellekletGetAllResult = mellekletService.GetAll(execParam.Clone(), search);
                    if (string.IsNullOrEmpty(mellekletGetAllResult.ErrorCode) && mellekletGetAllResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        Pld_BarCode_VonalKodTextBox.TextBox.Text = mellekletGetAllResult.Ds.Tables[0].Rows[0]["BarCode"].ToString();
                        Pld_BarCode_VonalKodTextBox.ReadOnly = true;

                    }
                    //else
                    //{
                    //    Pld_BarCode_VonalKodTextBox.TextBox.Text = string.Empty;
                    //    Pld_BarCode_VonalKodTextBox.ReadOnly = false;

                    //    //Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = false;
                    //}

                    // Ha email-en jött küldeményt iktatunk, és a küldeménynek volt vonalkódja, akkor beírjuk az iratpéldányhoz
                    // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                    if (Command == CommandName.New && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
                         && erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail
                         && erec_KuldKuldemenyek.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
                         && !String.IsNullOrEmpty(erec_KuldKuldemenyek.BarCode))
                    {
                        Pld_BarCode_VonalKodTextBox.Text = erec_KuldKuldemenyek.BarCode;
                        Pld_BarCode_VonalKodTextBox.ReadOnly = true;
                    }
                    #endregion
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
    }

    private void ClearKuldemenyComponets()
    {
        IraIrat_Targy_RequiredTextBox.Text = String.Empty;

        ErkeztetoSzam_TextBox.Text = String.Empty;
        KuldesMod_DropDownList.Clear();
        AdathordozoTipusa_DropDownList.Clear();
        UgyintezesModja_DropDownList.Clear();
        Surgosseg_DropDownList.Clear();

        Bekuldo_PartnerTextBox.Id_HiddenField = String.Empty;
        Bekuldo_PartnerTextBox.Text = String.Empty;

        Kuld_FelbontasDatuma_CalendarControl.Text = String.Empty;

        Kuld_CimId_CimekTextBox.Id_HiddenField = String.Empty;
        Kuld_CimId_CimekTextBox.Text = String.Empty;
        // BUG_7678
        FeladasiIdo_CalendarControl.Text = String.Empty;

        BeerkezesIdeje_CalendarControl.Text = String.Empty;

        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = String.Empty;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Text = String.Empty;

        HivatkozasiSzam_Kuldemeny_TextBox.Text = String.Empty;

        CsoportId_CimzettCsoportTextBox1.Id_HiddenField = String.Empty;
        CsoportId_CimzettCsoportTextBox1.Text = String.Empty;

        Pld_BarCode_VonalKodTextBox.TextBox.Text = string.Empty;
        Pld_BarCode_VonalKodTextBox.ReadOnly = false;

        Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = false;

        // Ez mindig readonly:
        //AdathordozoTipusa_KodtarakDropDownList.ReadOnly = false;

        Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly = false;
    }

    private void LoadKuldemenyComponents(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        ErkeztetoSzam_TextBox.Text = erec_KuldKuldemenyek.Azonosito;

        KuldesMod_DropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
        erec_KuldKuldemenyek.KuldesMod, EErrorPanel1);

        // BLG_361
        //UgyintezesModja_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
        //erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
        // BUG_2051
        UgyintezesModja_DropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO,
        erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

        Surgosseg_DropDownList.FillAndSetSelectedValue(kcs_SURGOSSEG, erec_KuldKuldemenyek.Surgosseg, EErrorPanel1);


        Bekuldo_PartnerTextBox.Id_HiddenField = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
        Bekuldo_PartnerTextBox.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;

        Kuld_FelbontasDatuma_CalendarControl.Text = erec_KuldKuldemenyek.FelbontasDatuma;

        Kuld_CimId_CimekTextBox.Id_HiddenField = erec_KuldKuldemenyek.Cim_Id;
        Kuld_CimId_CimekTextBox.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;

        // BUG_7678
        FeladasiIdo_CalendarControl.Text = erec_KuldKuldemenyek.BelyegzoDatuma;

        BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        HivatkozasiSzam_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

        CsoportId_CimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
        CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

        if (Command == CommandName.New)
        {
            // BLG_361 
            Pld_UgyintezesModja_KodtarakDropDownList.Clear();
            // BUG_2051 ( visszarakva)
            //Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
            Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

            AdathordozoTipusa_KodtarakDropDownList.Clear();
            AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_KuldKuldemenyek.UgyintezesModja, false, EErrorPanel1);
            AdathordozoTipusa_KodtarakDropDownList.ReadOnly = true;

            Pld_IratpeldanyFajtaja_KodtarakDropDownList.Clear();
            Pld_IratpeldanyFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_KuldKuldemenyek.AdathordozoTipusa, false, EErrorPanel1);
            Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly = true;
        }

        if (Command == CommandName.View)
        {
            KodtarakDropDownListUgyiratFelfuggesztesOka.ReadOnly = true;
            TextBoxFelfuggesztesOka.ReadOnly = true;

            KodtarakDropDownListUgyiratLezarasOka.ReadOnly = true;
        }

        if (Command == CommandName.New)
        {
            // küldemény minõsítés átvétele az iratba
            ktDropDownListIratMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_KuldKuldemenyek.Minosites, true, EErrorPanel1);
            // BLG_361
            Pld_UgyintezesModja_KodtarakDropDownList.Clear();
            // BUG_2051
            //Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
            Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

        }
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        if (Command == CommandName.New && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo) && ImageButton_KuldemenyKereses.Enabled)
        {
        }
        else
        {
            VonalkodTextBoxKuldemeny.ReadOnly = true;
            VonalkodTextBoxKuldemeny.Text = erec_KuldKuldemenyek.BarCode;
        }

        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            // BLG_1348
            // Nem csak a papíralapúakra vonatkozik a beállítás
            //papír alapú ügyintézés
            //if (Kuldemenyek.IsPapirAlapu(erec_KuldKuldemenyek))
            //{
            Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value = erec_KuldKuldemenyek.BeerkezesIdeje;
            //}
        }
        if (Command == CommandName.New && (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.BelsoIratIktatas))
        {
            if (isTUKRendszer && !string.IsNullOrEmpty(erec_KuldKuldemenyek.IrattarId))
            {
                IrattariHelyLevelekDropDownTUKUgyirat.SelectedValue = erec_KuldKuldemenyek.IrattarId;
            }
        }

        // Szülõ: Ügyindító partner:        
        if (erec_KuldKuldemenyek != null && string.IsNullOrEmpty(MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId) && string.IsNullOrEmpty(MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId))
        {
            MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId = erec_KuldKuldemenyek.Minosito;

            // Névfeloldás Id alapján:
            MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId = erec_KuldKuldemenyek.MinositoSzervezet;
            MinositoPartnerControlIraIratokFormTab.SetPartnerTextBoxById(EErrorPanel1);

        }

    }

    private EREC_IraIktatoKonyvek LoadUgyiratComponents(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        return LoadUgyiratComponents(erec_UgyUgyiratok, false);
    }

    private EREC_IraIktatoKonyvek LoadUgyiratComponents(EREC_UgyUgyiratok erec_UgyUgyiratok, bool elozmeny)
    {
        ElozmenyUgyiratID_HiddenField.Value = erec_UgyUgyiratok.Id;

        EREC_IraIktatoKonyvek iktatoKonyv = null;

        if (Command == CommandName.Modify)
        {
            // irattípus váltáskor, az irat metadefiníció lekérdezéséhez használjuk (irat határidõ tooltip)
            Ugytipus_HiddenField.Value = erec_UgyUgyiratok.UgyTipus;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            iktatoKonyv = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillWithOneValue(
                Constants.IktatoErkezteto.Iktato, erec_UgyUgyiratok.IraIktatokonyv_Id, EErrorPanel1);
        }
        else
        {
            // iktatásnál
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillAndSetSelectedValue(true, true, Constants.IktatoErkezteto.Iktato
                 , erec_UgyUgyiratok.IraIktatokonyv_Id, true, EErrorPanel1);
        }

        //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATOSZAM_KIEG
        //    , erec_UgyUgyiratok.IktatoszamKieg, true, EErrorPanel1);        

        //UgyUgyirat_UgyiratTextBox.SetUgyiratTextBoxById(EErrorPanel1);

        trFoszam.Visible = true;
        //UgyiratFoszam_Label.Enabled = true;



        string fullFoszam = "";

        #region Iktatókönyv lekérése:
        if (iktatoKonyv == null)
        {
            EREC_IraIktatoKonyvekService IktatoKonyvekservice = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;

            Result IktatoKonyvekresult = IktatoKonyvekservice.Get(execParam);
            if (!string.IsNullOrEmpty(IktatoKonyvekresult.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, IktatoKonyvekresult);
                if (ErrorUpdatePanel1 != null)
                {
                    ErrorUpdatePanel1.Update();
                }
                return null;
            }
            else
            {
                iktatoKonyv = (EREC_IraIktatoKonyvek)IktatoKonyvekresult.Record;
            }
        }

        obj_Iktatokonyv = iktatoKonyv;

        #endregion

        // BLG_292
        //fullFoszam = Ugyiratok.GetFullFoszam(erec_UgyUgyiratok, iktatoKonyv);
        ExecParam execParamFoszam = UI.SetExecParamDefault(Page, new ExecParam());
        fullFoszam = Ugyiratok.GetFullFoszam(execParamFoszam, erec_UgyUgyiratok, iktatoKonyv);


        //fullFoszam = Ugyiratok.GetFullFoszam(erec_UgyUgyiratok, Page, EErrorPanel1, ErrorUpdatePanel1);


        UgyiratFoszam_Label.Text = fullFoszam;

        //BLG_619
        if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {

            EREC_UgyUgyiratokService tempService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());

            string ugyfelelos_szervezetkod = "";
            Result tempResult = tempService.GetUgyFelelosSzervezetKod(temp_execParam, erec_UgyUgyiratok.Id);
            if (!tempResult.IsError)
            {
                ugyfelelos_szervezetkod = tempResult.Record.ToString();

            }
            if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
            {
                UgyiratFoszam_Label.Text = ugyfelelos_szervezetkod + " " + fullFoszam;
            }
        }
        #region Elõzmény törlés gomb és munkaügyirat elõzmény választás megjelenítése

        if (Command == CommandName.New)
        {
            bool csakAlszamraIktathat_Bejovo = FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra);
            bool csakAlszamraIktathat_Belso = FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra);

            if ((Mode == CommandName.BejovoIratIktatas && !csakAlszamraIktathat_Bejovo)
                 || (Mode == CommandName.BelsoIratIktatas && !csakAlszamraIktathat_Belso)
                 || (Mode == CommandName.KimenoEmailIktatas && !csakAlszamraIktathat_Belso)
                 || (Mode == CommandName.AtIktatas && !csakAlszamraIktathat_Belso && !csakAlszamraIktathat_Bejovo)
                )
            {
                FoszamraIktat_ImageButton.Visible = true;
            }
            else
            {
                FoszamraIktat_ImageButton.Visible = false;
            }

            if (Mode == CommandName.ElokeszitettIratIktatas)
            {
                if (FunctionRights.GetFunkcioJog(Page, funkcio_UgyiratSzereles)
                     && Contentum.eRecord.Utility.Ugyiratok.Munkaanyag(erec_UgyUgyiratok))
                {
                    MunkaugyiratElozmenyPanel.Visible = true;
                }

                if (Ugyiratok.Munkaanyag(erec_UgyUgyiratok))
                {
                    if (!String.IsNullOrEmpty(erec_UgyUgyiratok.Base.Note))
                    {
                        #region LZS - obsolate
                        //System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                        //string javasoltElozmeny = null;
                        //try
                        //{
                        //    xmlDoc.LoadXml(erec_UgyUgyiratok.Base.Note);

                        //    if (xmlDoc != null)
                        //    {
                        //        System.Xml.XmlNodeList nodeList = xmlDoc.SelectNodes("JavasoltElozmeny");

                        //        if (nodeList.Count > 0)
                        //        {
                        //            javasoltElozmeny = nodeList[0].InnerText;
                        //        }
                        //        JavasoltElozmenyTextBox.Text = javasoltElozmeny;
                        //    }
                        //}
                        //catch (System.Xml.XmlException e)
                        //{
                        //    // nem megfelelõ az XML formátuma...
                        //}
                        #endregion

                        #region LZS - BUG_11081
                        //JSON formátumú erec_UgyUgyiratok.Base.Note objektumot deserializáljuk, és kiszedjük belőle a JavasoltElozmenyek értékét.
                        Note_JSON note;
                        try
                        {
                            note = JsonConvert.DeserializeObject<Note_JSON>(erec_UgyUgyiratok.Base.Note);
                            JavasoltElozmenyTextBox.Text = note.JavasoltElozmenyek;
                        }
                        catch (Exception)
                        {
                            // nem megfelelõ az JSON formátum...
                            note = null;
                        }
                        #endregion

                    }
                }
            }
        }
        else
        {
            FoszamraIktat_ImageButton.Visible = false;
        }

        #endregion

        if (!_Load)
        {
            // BUG_2074
            //bool setHatarido = (!elozmeny || IntezesiIdoMasolas);


            //határidõ átmásolása az elõzmény ügyiratból
            // BUG_2074
            //if (setHatarido)
            //{
            UgyUgyirat_Hatarido_CalendarControl.Text = erec_UgyUgyiratok.Hatarido;
            IntezesiIdo_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdo;
            // BLG_1020
            if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IntezesiIdoegyseg))
            {
                IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdoegyseg;
            }

            //}
            ElozmenyUgyiratHatarido_HiddenField.Value = erec_UgyUgyiratok.Hatarido;
            IktatasDatuma_Ugyirat_HiddenField.Value = erec_UgyUgyiratok.Base.LetrehozasIdo;
            FelhasznaloCsoportIdOrzo_HiddenField.Value = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = IratMetaDefinicio.GetBusinessObjectById(Page, erec_UgyUgyiratok.IratMetadefinicio_Id);
            IratMetaDefinicio.SetHataridoToolTipByIratMetaDefinicio(Page, erec_IratMetaDefinicio, UgyUgyirat_Hatarido_CalendarControl.TextBox);
            if (erec_IratMetaDefinicio != null)
            {
                UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = (String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratHataridoKitolas) ? "1" : erec_IratMetaDefinicio.UgyiratHataridoKitolas);
                if (erec_IratMetaDefinicio.UgyiratHataridoKitolas != "0"
                     && bUgyiratHataridoModositasiJog
                     && erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez == FelhasznaloProfil.FelhasznaloId(Page))
                {
                    UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                    if (Uj_Ugyirat_Hatarido_Kezeles)
                    {
                        trUgyiratIntezesiIdoVisible = true;
                    }
                }
            }
            else
            {
                UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = "1";
                if (bUgyiratHataridoModositasiJog
                     && erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez == FelhasznaloProfil.FelhasznaloId(Page))
                {
                    UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                    if (Uj_Ugyirat_Hatarido_Kezeles)
                    {
                        trUgyiratIntezesiIdoVisible = true;
                    }
                }
            }

            // BLG_8826
            _iratForm.SetUgyintezesKezdete(erec_UgyUgyiratok, GetUgyintezesKezdete().ToString());
        }

        //UgyUgyirat_Ugytipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYTIPUS
        //    , erec_UgyUgyiratok.UgyTipus, true, EErrorPanel1);

        UgyUgyirat_SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = erec_UgyUgyiratok.IrattarbaKuldDatuma;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField =
             erec_UgyUgyiratok.IraIrattariTetel_Id;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(EErrorPanel1);

        UgyUgyirat_Targy_RequiredTextBox.Text = erec_UgyUgyiratok.Targy;

        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Felelos;
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
             erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        Ugyfelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Ugyfelelos;
        Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        ////Ügyirat betöltése után alszámra iktatás lehetséges
        //if (Command == CommandName.New)
        //    TabFooter1.ImageButton_SaveAndAlszam.Visible = true;
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        // CR 3110  Itt kell ellenörzés?
        if (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
        {
            // 1. ha lezárt iktatókönyvben lévõ ügyirat, akkor a küldemény adatai élveznek elsõbbséget
            if (IktatoKonyvek.Lezart(iktatoKonyv))
            {
                // nem töltjük ki az ügyindító/ügyindító címe mezõket, a küldemény fogja meghatározni
            }
            else
            {
                // idei ügyirat esetén nem írjuk felül az ügyirat adatait, azaz a küldemény adatait felülbíráljuk
                // ügyindító, ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
                CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);
            }

            //// csak ha még nincs kitöltve
            //// ügyindító, ügyfél
            //if (String.IsNullOrEmpty(Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField))
            //{
            //    Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
            //}
            //if (String.IsNullOrEmpty(CimekTextBoxUgyindito.Id_HiddenField) && String.IsNullOrEmpty(CimekTextBoxUgyindito.Text))
            //{
            //    CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);
            //}

        }
        else
        {
            // ügyindító, ügyfél
            Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
            CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);
        }

        UgyiratOrzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;
        UgyiratOrzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        if (isTUKRendszer && !string.IsNullOrEmpty(erec_UgyUgyiratok.IrattarId))
        {
            IrattariHelyLevelekDropDownTUKUgyirat.SelectedValue = erec_UgyUgyiratok.IrattarId;
        }

        // BLG_44
        IrattariTetelszam_DropDownList.FillAndSetSelectedValue(erec_UgyUgyiratok.IraIrattariTetel_Id, EErrorPanel1);
        // BUG_1499
        SetMergeItszFromDDL();
        UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_UgyUgyiratok.Ugy_Fajtaja, EErrorPanel1);

        // BUG_1499
        //erec_UgyUgyiratok.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;
        //erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView ( Ugykor_DropDownList );

        regiAzonositoTextBox.Text = erec_UgyUgyiratok.RegirendszerIktatoszam;

        return iktatoKonyv;
    }

    private void ClearUgyiratComponents()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;

        //// iktatásnál
        //ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, Constants.IktatoErkezteto.Iktato
        //    , false, EErrorPanel1);

        trFoszam.Visible = false;

        UgyiratFoszam_Label.Text = String.Empty;

        SetDefaultHataridok();
        ElozmenyUgyiratHatarido_HiddenField.Value = String.Empty;

        UgyUgyirat_SkontroVege_CalendarControl.Text = String.Empty;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = String.Empty;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text = String.Empty;

        UgyUgyirat_Targy_RequiredTextBox.Text = String.Empty;

        //Kezelõ kitöltve alapból a felhasználóra:
        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text = String.Empty;

        Ugyfelelos_CsoportTextBox.Id_HiddenField = String.Empty;
        Ugyfelelos_CsoportTextBox.Text = String.Empty;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField = String.Empty;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = String.Empty;
        CimekTextBoxUgyindito.Id_HiddenField = String.Empty;
        CimekTextBoxUgyindito.Text = String.Empty;

        regiAzonositoTextBox.Text = String.Empty;

        Merge_IrattariTetelszamTextBox.Text = String.Empty;

        if (isTUKRendszer)
        {
            if (Mode == CommandName.BejovoIratIktatas)
            {
                ReloadIrattariHelyLevelekDropDownTUK(Ugyfelelos_CsoportTextBox.HiddenField.Value, null, true);
            }
            else if (Mode == CommandName.BelsoIratIktatas)
            {
                IrattariHelyLevelekDropDownTUKUgyirat.SetSelectedValue("-1");
            }
        }

        _iratForm.SetUgyintezesKezdete(null, "");
    }

    private void LoadIratPeldanyComponents(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = erec_PldIratPeldanyok.Partner_Id_Cimzett;
        Pld_PartnerId_Cimzett_PartnerTextBox.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
        
		 // BUG_13213
        Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt;
		
		//if (Command == CommandName.View && String.IsNullOrEmpty(erec_PldIratPeldanyok.Partner_Id_Cimzett))
        //{
        //    Pld_PartnerId_Cimzett_PartnerTextBox.ImageButton_View.Enabled = false;
        //    Pld_PartnerId_Cimzett_PartnerTextBox.ImageButton_View.CssClass = "disabledLovListItem";
        //}

        Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(
             kcs_KULDEMENY_KULDES_MODJA, erec_PldIratPeldanyok.KuldesMod, false, EErrorPanel1);

        Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = erec_PldIratPeldanyok.Cim_id_Cimzett;
        Pld_CimId_Cimzett_CimekTextBox.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;
        if (!String.IsNullOrEmpty(erec_PldIratPeldanyok.Cim_id_Cimzett))
        {
            var execParam = UI.SetExecParamDefault(Page, new ExecParam());
            var cim = Contentum.eUtility.CimUtility.GetCimSTRById(execParam, erec_PldIratPeldanyok.Cim_id_Cimzett);
            if (!String.IsNullOrEmpty(cim))
            {
                Pld_CimId_Cimzett_CimekTextBox.Text = cim;
            }
        }

        if (Command == CommandName.View && String.IsNullOrEmpty(erec_PldIratPeldanyok.Cim_id_Cimzett))
        {
            Pld_CimId_Cimzett_CimekTextBox.ImageButton_View.Enabled = false;
            Pld_CimId_Cimzett_CimekTextBox.ImageButton_View.CssClass = "disabledLovListItem";
        }

        //Pld_Tovabbito_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TOVABBITO_SZERVEZET,
        //    erec_PldIratPeldanyok.Tovabbito, true, EErrorPanel1);

        Pld_Visszavarolag_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VISSZAVAROLAG,
             erec_PldIratPeldanyok.Visszavarolag, true, EErrorPanel1);

        //Pld_Ragszam_TextBox.Text = erec_PldIratPeldanyok.Ragszam;

        Pld_BarCode_VonalKodTextBox.Text = erec_PldIratPeldanyok.BarCode;

        // Az iratpéldány fajtájánál a listából le kell venni a 'Vegyes' értéket:
        List<string> ktUgyintezesAlapjaFilterList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYINTEZES_ALAPJA, this.Page, null)
                            .Where(kt => kt.Kod != KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                            .Select(kt => kt.Kod)
                            .ToList();

        Pld_IratpeldanyFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(
            kcs_UGYINTEZES_ALAPJA, erec_PldIratPeldanyok.UgyintezesModja, ktUgyintezesAlapjaFilterList, true, EErrorPanel1);

        //Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(
        //    kcs_UGYINTEZES_ALAPJA, erec_PldIratPeldanyok.UgyintezesModja, true, EErrorPanel1);

        if (isTUKRendszer && !string.IsNullOrEmpty(erec_PldIratPeldanyok.IrattarId))
        {
            if (IrattariHelyLevelekDropDownTUK.DropDownList.Items.Count < 1)
            {
                ReloadIrattariHelyLevelekDropDownTUK(Ugyfelelos_CsoportTextBox.HiddenField.Value, erec_PldIratPeldanyok.IrattarId, false);
            }
            else
            {
                IrattariHelyLevelekDropDownTUK.SetSelectedValue(erec_PldIratPeldanyok.IrattarId);
            }
        }

        if (!_Load)
        {
            Pld_VisszaerkezesiHatarido_CalendarControl.Text = erec_PldIratPeldanyok.VisszaerkezesiHatarido;
        }
    }

    private void Load_ComponentSelectModul()
    {
        /*if (compSelector == null) { return; }
        else
        {
             compSelector.Enabled = true;

             // TODO: felkell sorolni...
             /*compSelector.Add_ComponentOnClick(Nev);
             compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

             FormFooter1.SaveEnabled = false;
        }*/
    }

    #region Utility
    private bool IsIktatokonyvLezart
    {
        get
        {
            return !String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value) || !String.IsNullOrEmpty(LezartFolyosorszamosIktatokonyv_HiddenField.Value);
        }
    }

    private bool IsIktatokonyvLezartFolyosorszamos
    {
        get
        {
            return !String.IsNullOrEmpty(LezartFolyosorszamosIktatokonyv_HiddenField.Value);
        }
    }

    private bool IsElozmenyUgyirat
    {
        get
        {
            return !String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value);
        }
    }

    private bool IsAlszamraIktatas
    {
        get
        {
            // Ha van megadva ügyirat id, akkor alszámra iktatás van
            // de ha az iktatókönyv lezárt és van joga fõszámra iktatni, akkor mégis fõszámra iktatás + szerelés/szerelés elõkészítés
            return (IsElozmenyUgyirat && (!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos));
        }
    }

    private bool IsMunkaPeldanyLetrehozas
    {
        get
        {
            return (IktatasVagyMunkapeldanyLetrehozasRadioButtonList.SelectedValue == const_MunkapeldanyLetrehozas) ? true : false;
        }
    }

    private bool IsBelsoIktatas
    {
        get
        {
            return String.IsNullOrEmpty(Kuldemeny_Id_HiddenField.Value);
        }
    }

    private bool IsBejovoIktatas
    {
        get
        {
            return !String.IsNullOrEmpty(Kuldemeny_Id_HiddenField.Value);
        }
    }

    private bool IktathatFoszamra(string Mode)
    {
        switch (Mode)
        {
            case CommandName.BejovoIratIktatas:
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            // CR 3110 Itt se biztos hogy van értelme
            case CommandName.MunkapeldanyBeiktatas_Bejovo:
                return !FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra);
            case CommandName.KimenoEmailIktatas:
                return (!FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra) || IsMunkaPeldanyLetrehozas);
            case CommandName.BelsoIratIktatas:
                return !FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra);
            case CommandName.AtIktatas:
                return (IsBejovoIktatas && !FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra))
                     || (IsBelsoIktatas && !FunctionRights.GetFunkcioJog(Page, funkcio_BelsoIratIktatasCsakAlszamra));
            default:
                return false;
        }

    }

    private bool RaiseErrorNemIktathatFoszamra(string Mode)
    {
        if (!IktathatFoszamra(Mode))
        {
            // Hiba: Csak alszámra iktathat!
            // "A jogosultsága alapján Ön csak alszámra iktathat! Válasszon elõzmény ügyiratot!"
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UICsakAlszamraIktathat);
            ErrorUpdatePanel1.Update();
        }

        return !IktathatFoszamra(Mode);
    }

    //Lezárt iktatókönyvû elõzménybe nem iktathat, ha csak alszámra iktathat joga van
    private bool RaiseErrorLezartIktatokonyvbeNemIktathat(string Mode)
    {
        bool ret = IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos && !IktathatFoszamra(Mode);

        if (ret)
        {
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UILezartIktatokonyCsakAlszamraIktathat);
            ErrorUpdatePanel1.Update();
        }

        return ret;
    }

    #endregion

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        bool RequireReloadTab = false;
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (_ParentForm == Constants.ParentForms.IraIrat)
            {
                if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
                {
                    bool isHatosagi = CheckUgyFajtaIsHatosagi(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);

                    #region UGY FAJTA: HATOSAGI -ELJARASI SZAKASZ
                    if (!CheckUgyFajtaEsEljarasiSzakasz(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
                        return;
                    #endregion UGY FAJTA: HATOSAGI -ELJARASI SZAKASZ

                    #region SAKKORA KOTELEZO
                    if (!CheckUgyFajtaSakkoraKotelezoseg(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue))
                        return;
                    #endregion
                }
            }

            if (Command == CommandName.Modify && FunctionRights.GetFunkcioJog(Page, "IraIratModify")
                 || (Command == CommandName.New && Mode == CommandName.BejovoIratIktatas &&
                      FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatas"))
                 // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                 || (Command == CommandName.New && Mode == CommandName.MunkapeldanyBeiktatas_Bejovo &&
                      FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Bejovo"))
                 || (Command == CommandName.New && Mode == CommandName.BelsoIratIktatas &&
                      FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas"))
                 || (Command == CommandName.New && Mode == CommandName.KimenoEmailIktatas &&
                      FunctionRights.GetFunkcioJog(Page, "EmailIktatas"))
                 || (Command == CommandName.New && Mode == CommandName.AtIktatas &&
                      FunctionRights.GetFunkcioJog(Page, "AtIktatas"))
                 )
            {
                bool voltHiba = false;
                bool ujUgyirat = false; // annak nyilvántartásához, hogy kell-e beszúrni standard tárgyszó rekordokat (csak újhoz kell)

                switch (Command)
                {
                    case CommandName.New:
                        {
                            // IKTATÁS
                            #region Iktatás
                            // (Alszámra iktatásnál) ha lezárt az ügyirat, felhasználótól megkérdezni valahogy, hogy most akkor 
                            // újra kéne nyitni az ügyiratot, vagy beleiktassunk a lezárt ügyiratba
                            // defaultból most az utóbbi eset lesz
                            IktatasiParameterek.UgyiratUjranyitasaHaLezart = false;

                            if (RadioButtonList_Lezartbaiktat_vagy_Ujranyit.SelectedValue == "1")
                            {
                                IktatasiParameterek.UgyiratUjranyitasaHaLezart = true;
                            }
                            else
                            {
                                IktatasiParameterek.UgyiratUjranyitasaHaLezart = false;
                            }


                            bool csakKuldemenyElokeszitesVolt = false;

                            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result_iktatas = null;
                            String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
                            String Erec_IraIktatokonyvek_Id = GetSelectedIktatokonyvId();
                            EREC_UgyUgyiratok erec_UgyUgyirat = GetBusinessObjectFromComponents_EREC_UgyUgyiratok();
                            EREC_UgyUgyiratdarabok erec_UgyUgyiratdarab = GetBusinessObjectFromComponents_EREC_UgyUgyiratdarabok();
                            EREC_IraIratok erec_IraIrat = GetBusinessObjectFromComponents();
                            EREC_HataridosFeladatok erec_HataridosFeladat = FeljegyzesPanel.GetBusinessObject();
                            if (IsAlszamraIktatas)
                            {
                                if (!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos)
                                {
                                    // !!! Alszámra iktatásnál nem az ajax-os dropdownból vesszük az iktatókönyvet!!!
                                    Erec_IraIktatokonyvek_Id = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.SelectedValue;
                                }
                                else
                                {
                                    // Ha lezárt iktatókönyvben lévõ ügyiratba iktatunk, akkor a Cascadingdrop downból vesszük az adatokat
                                    Erec_IraIktatokonyvek_Id = GetSelectedIktatokonyvId();
                                    IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                    IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
                                }
                            }
                            else
                            {
                                // ha elõzményt választott, de az elõzmény lezárt iktatókönyvben volt,
                                // és van joga fõszámra iktatni, akkor itt adjuk át a szerelendõ eredeti ügyirat azonosítóját
                                if (IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos)
                                {
                                    IktatasiParameterek.SzerelendoUgyiratId = UgyUgyirat_Id;
                                }
                            }
                            #endregion

                            /// Bejövõ Irat Iktatás
                            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                            if ((Mode == CommandName.BejovoIratIktatas) || (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo))
                            {
                                #region Bejövõ Irat Iktatás
                                if (!String.IsNullOrEmpty(Kuldemeny_Id_HiddenField.Value))
                                {
                                    #region UgyintezesKezdoDatuma
                                    EREC_KuldKuldemenyek kuldemeny = GetKuldemenyById(Kuldemeny_Id_HiddenField.Value);
                                    if (kuldemeny != null)
                                    {
                                        SetBejovoIktatasUgyintezesKezdoDatuma(erec_IraIrat, kuldemeny);
                                    }
                                    #endregion UgyintezesKezdoDatuma

                                    IktatasiParameterek.KuldemenyId = Kuldemeny_Id_HiddenField.Value;
                                    IktatasiParameterek.Iratpeldany_Vonalkod = Pld_BarCode_VonalKodTextBox.Text;
                                    // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                                    if (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
                                    {
                                        IktatasiParameterek.MunkaPeldany = true;
                                    }

                                    if (PeldanyokSorszamaMegadhato)
                                    {
                                        string peldanyokError;

                                        if (!BejovoPeldanyListPanel1.IsValid(out peldanyokError))
                                        {
                                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, peldanyokError);
                                            ErrorUpdatePanel1.Update();
                                            return;
                                        }

                                        IktatasiParameterek.Peldanyok = BejovoPeldanyListPanel1.GetBusinessObjectsFromComponents();

                                        if (isTUKRendszer && IktatasiParameterek.Peldanyok != null && IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null)
                                        {
                                            //if (Mode ==CommandName.BelsoIratIktatas)
                                            //{
                                            //    foreach (EREC_PldIratPeldanyok pld in IktatasiParameterek.Peldanyok)
                                            //    {
                                            //        pld.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
                                            //        pld.Updated.IrattarId = true;
                                            //        pld.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
                                            //        pld.Updated.IrattariHely = true;
                                            //    }
                                            //}
                                        }
                                    }
                                    //Alszámra iktatás
                                    if (IsAlszamraIktatas)
                                    {

                                        #region BLG_232
                                        if (KettosIktatasControlling(kuldemeny.HivatkozasiSzam, service))
                                        {
                                            return;
                                        }
                                        #endregion

                                        result_iktatas = service.BejovoIratIktatasa_Alszamra(execparam, Erec_IraIktatokonyvek_Id,
                                             UgyUgyirat_Id, erec_UgyUgyiratdarab, erec_IraIrat, erec_HataridosFeladat, IktatasiParameterek);

                                    }
                                    else
                                    {
                                        // Normál iktatás

                                        // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                        if (RaiseErrorNemIktathatFoszamra(Mode))
                                        {
                                            voltHiba = true;
                                        }
                                        else
                                        {
                                            #region BLG_232
                                            if (KettosIktatasControlling(kuldemeny.HivatkozasiSzam, service))
                                            {
                                                return;
                                            }
                                            #endregion

                                            IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                            IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

                                            result_iktatas = service.BejovoIratIktatasa(execparam,
                                                 Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, erec_UgyUgyiratdarab,
                                                 erec_IraIrat, erec_HataridosFeladat, iktatasiParameterek);

                                            #region BLG_2950 - Automaitikus szignálás
                                            if (!result_iktatas.IsError)
                                            {
                                                //LZS - Automatikus szignálás javasolt ügyintézőre iktatás után
                                                AutomatikusSzignalas(service, result_iktatas.Uid);
                                            }
                                            #endregion

                                        }
                                    }

                                }
                                else
                                {
                                    // nincs küldemény kiválasztva

                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                                         Resources.Error.UINincsMegadvaKuldemeny);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                                #endregion

                            }  /// Belsõ Irat Iktatás
							else if (Mode == CommandName.BelsoIratIktatas)
                            {
                                bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

                                #region Belsõ Irat Iktatás
                                EREC_PldIratPeldanyok erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();
                                IktatasiParameterek.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;
                                // BLG_350
                                //IktatasiParameterek.KeszitoPeldanyaSzukseges = KeszitoPeldanya_CheckBox.Checked;
                                IktatasiParameterek.KeszitoPeldanyaSzukseges = false;

                                if (SubMode == CommandName.InfoszabIktatas)
                                {
                                    try
                                    {
                                        IktatasiParameterek.Dokumentum = GetInfoszabDokumentum();
                                    }
                                    catch (Exception ex)
                                    {
                                        result_iktatas = Contentum.eUtility.ResultException.GetResultFromException(ex);
                                        voltHiba = true;
                                    }
                                }

                                if (!voltHiba)
                                {
                                    #region Belsõ Irat Iktatás
                                    erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();
                                    IktatasiParameterek.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;
                                    List<EREC_PldIratPeldanyok> erec_PldIratPeldanyTobbSzoros = GetTobbSzorosIratPeldanyok();

                                    // BLG_350
                                    //IktatasiParameterek.KeszitoPeldanyaSzukseges = KeszitoPeldanya_CheckBox.Checked;
                                    IktatasiParameterek.KeszitoPeldanyaSzukseges = false;

                                    // BLG#1402: TÜK-ös iktatásnál mellékletek panelt is kezelni kell:
                                    if (isTUK)
                                    {
                                        IktatasiParameterek.Mellekletek = mellekletekPanel.GetMellekletek();
                                    }

                                    #region UgyintezesKezdoDatuma
                                    SetBelsoIktatasUgyintezesKezdoDatuma(erec_IraIrat);
                                    #endregion UgyintezesKezdoDatuma

                                    if (IsAlszamraIktatas)
                                    {
                                        // Alszámra iktatás
                                        if (SubMode == CommandName.InfoszabIktatas)
                                        {
                                            // Infoszab
                                            result_iktatas = service.InfoszabIratIktatasa_Alszamra(execparam,
                                                 Erec_IraIktatokonyvek_Id, UgyUgyirat_Id, erec_UgyUgyiratdarab, erec_IraIrat, erec_HataridosFeladat,
                                                 erec_PldIratPeldany, iktatasiParameterek);

                                        }
                                        else
                                        {
                                            if (erec_PldIratPeldanyTobbSzoros != null && erec_PldIratPeldanyTobbSzoros.Count > 0)
                                            {
                                                SetIratPeldanyokTobbesLista(ref erec_PldIratPeldanyTobbSzoros, erec_PldIratPeldany);
                                                IktatasiParameterek.Peldanyok = erec_PldIratPeldanyTobbSzoros;
                                            }

                                            result_iktatas = service.BelsoIratIktatasa_Alszamra(execparam,
                                                    Erec_IraIktatokonyvek_Id, UgyUgyirat_Id, erec_UgyUgyiratdarab, erec_IraIrat, erec_HataridosFeladat,
                                                    erec_PldIratPeldany, iktatasiParameterek);
                                        }

                                    }
                                    else
                                    {
                                        // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                        if (RaiseErrorNemIktathatFoszamra(Mode))
                                        {
                                            voltHiba = true;
                                        }
                                        else
                                        {

                                            if (SubMode == CommandName.InfoszabIktatas)
                                            {
                                                // Infoszab
                                                result_iktatas = service.InfoszabIratIktatasa(execparam, Erec_IraIktatokonyvek_Id,
                                                     erec_UgyUgyirat, erec_UgyUgyiratdarab, erec_IraIrat, erec_HataridosFeladat, erec_PldIratPeldany,
                                                     IktatasiParameterek);
                                            }
                                            else
                                            {
                                                if (erec_PldIratPeldanyTobbSzoros != null && erec_PldIratPeldanyTobbSzoros.Count > 0)
                                                {
                                                    SetIratPeldanyokTobbesLista(ref erec_PldIratPeldanyTobbSzoros, erec_PldIratPeldany);
                                                    IktatasiParameterek.Peldanyok = erec_PldIratPeldanyTobbSzoros;
                                                }
                                                // Normál iktatás
                                                result_iktatas = service.BelsoIratIktatasa(execparam, Erec_IraIktatokonyvek_Id,
                                                erec_UgyUgyirat, erec_UgyUgyiratdarab, erec_IraIrat, erec_HataridosFeladat, erec_PldIratPeldany,
                                                IktatasiParameterek);

                                                #region BLG_2950 - Automaitikus szignálás
                                                if (!result_iktatas.IsError)
                                                {
                                                    //LZS - Automatikus szignálás javasolt ügyintézőre iktatás után
                                                    AutomatikusSzignalas(service, result_iktatas.Uid);
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            else if (Mode == CommandName.KimenoEmailIktatas)
                            {
                                #region Kimenõ Email Iktatás

                                EREC_PldIratPeldanyok erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();
                                IktatasiParameterek.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;
                                // BLG_350
                                //IktatasiParameterek.KeszitoPeldanyaSzukseges = KeszitoPeldanya_CheckBox.Checked;
                                IktatasiParameterek.KeszitoPeldanyaSzukseges = false;

                                // Ha munkaanyag létrehozás kell, nem iktatás:
                                if (IsMunkaPeldanyLetrehozas)
                                {
                                    IktatasiParameterek.MunkaPeldany = true;
                                }

                                if (IsAlszamraIktatas)
                                {

                                    // Kimenõ email alszámra iktatása:
                                    result_iktatas = service.KimenoEmailIktatas_Alszamra(execparam, EmailBoritekokId,
                                         Erec_IraIktatokonyvek_Id, UgyUgyirat_Id, erec_IraIrat, erec_PldIratPeldany
                                         , erec_HataridosFeladat, iktatasiParameterek);

                                }
                                else
                                {
                                    // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                    // (munkaanyag létrehozásnál nem nézzük)
                                    if (RaiseErrorNemIktathatFoszamra(Mode))
                                    {
                                        voltHiba = true;
                                    }
                                    else
                                    {
                                        result_iktatas = service.KimenoEmailIktatas(execparam, EmailBoritekokId, Erec_IraIktatokonyvek_Id,
                                             erec_UgyUgyirat, erec_IraIrat, erec_PldIratPeldany, erec_HataridosFeladat, IktatasiParameterek);

                                    }
                                }

                                #endregion
                            }
                            else if (Mode == CommandName.AtIktatas)
                            {
                                #region Átiktatás

                                string IratId = Request.QueryString.Get(QueryStringVars.IratId);

                                if (!String.IsNullOrEmpty(IratId))
                                {
                                    EREC_PldIratPeldanyok erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();

                                    IktatasiParameterek.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;
                                    // BLG_350
                                    //IktatasiParameterek.KeszitoPeldanyaSzukseges = KeszitoPeldanya_CheckBox.Checked;
                                    IktatasiParameterek.KeszitoPeldanyaSzukseges = false;

                                    // Ügyirat sztornó ha kell
                                    if (Storno_Empty_Ugyirat.Value == "1")

                                    {
                                        IktatasiParameterek.EmptyUgyiratSztorno = true;
                                    }

                                    if (IsAlszamraIktatas)
                                    {

                                        result_iktatas = service.IratAtIktatasa_Alszamra(execparam, IratId, Erec_IraIktatokonyvek_Id,
                                             UgyUgyirat_Id, erec_UgyUgyiratdarab, erec_IraIrat, erec_PldIratPeldany, erec_HataridosFeladat, IktatasiParameterek);
                                    }
                                    else
                                    {
                                        // Normál iktatás

                                        // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                        if (RaiseErrorNemIktathatFoszamra(Mode))
                                        {
                                            voltHiba = true;
                                        }
                                        else
                                        {

                                            IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                            IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

                                            result_iktatas = service.IratAtIktatasa(execparam, IratId,
                                                 Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, erec_UgyUgyiratdarab, erec_IraIrat, erec_PldIratPeldany
                                                 , erec_HataridosFeladat, IktatasiParameterek);
                                        }
                                    }

                                }
                                else
                                {


                                    IratId = Request.QueryString.Get(QueryStringVars.IratId);

                                    if (!String.IsNullOrEmpty(IratId))
                                    {
                                        EREC_PldIratPeldanyok erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();

                                        IktatasiParameterek.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;
                                        // BLG_350
                                        //IktatasiParameterek.KeszitoPeldanyaSzukseges = KeszitoPeldanya_CheckBox.Checked;
                                        IktatasiParameterek.KeszitoPeldanyaSzukseges = false;

                                        // Ügyirat sztornó ha kell
                                        if (Storno_Empty_Ugyirat.Value == "1")
                                        {
                                            IktatasiParameterek.EmptyUgyiratSztorno = true;
                                        }

                                        if (IsAlszamraIktatas)
                                        {

                                            result_iktatas = service.IratAtIktatasa_Alszamra(execparam, IratId, Erec_IraIktatokonyvek_Id,
                                                 UgyUgyirat_Id, erec_UgyUgyiratdarab, erec_IraIrat, erec_PldIratPeldany, erec_HataridosFeladat, IktatasiParameterek);
                                        }
                                        else
                                        {
                                            // Normál iktatás

                                            // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                            if (RaiseErrorNemIktathatFoszamra(Mode))
                                            {
                                                voltHiba = true;
                                            }
                                            else
                                            {

                                                IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                                IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

                                                result_iktatas = service.IratAtIktatasa(execparam, IratId,
                                                     Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, erec_UgyUgyiratdarab, erec_IraIrat, erec_PldIratPeldany
                                                     , erec_HataridosFeladat, IktatasiParameterek);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        ResultError.DisplayNoIdParamError(EErrorPanel1);
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel1.Update();
                                    return;
                                }


                                #endregion  Átiktatás

                            }

                            #region hiba kezeles
                            if (result_iktatas != null)
                            {
                                if (result_iktatas.IsError)
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                                else if (result_iktatas.ErrorType == "NincsIktatva")
                                {
                                    // Küldemény nem lett iktatva, csak rögzítve lettek bizonyos iktatási adatok
                                    csakKuldemenyElokeszitesVolt = true;
                                }
                                else if (!IsAlszamraIktatas)
                                {
                                    ujUgyirat = true;
                                }
                            }
                            #endregion

                            if (!voltHiba && result_iktatas != null)
                            {
                                if (csakKuldemenyElokeszitesVolt == true)
                                {
                                    // Nem volt hiba, de a küldemény nem lett iktatva:

                                    #region ResultPanel_KuldemenyElokeszites beállítása

                                    //Panel-ek beállítása
                                    KuldemenyPanel.Visible = false;
                                    Ugyirat_Panel.Visible = false;
                                    IratPanel.Visible = false;
                                    IratPeldanyPanel.Visible = false;
                                    BejovoPeldanyListPanel1.Visible = false;

                                    ResultPanel.Visible = false;
                                    StandardTargyszavakPanel.Visible = false;
                                    StandardTargyszavakPanel_Iratok.Visible = false;
                                    TipusosTargyszavakPanel_Iratok.Visible = false;
                                    ResultPanel_KuldemenyElokeszites.Visible = true;

                                    TabFooter1.HideButton();
                                    TabFooter1.ImageButton_Close.Visible = true;

                                    string kovFelelos_Id = "";
                                    // Küldemény új felelõsének kiírása:
                                    if (result_iktatas.Record != null)
                                    {
                                        kovFelelos_Id = result_iktatas.Record.ToString();
                                    }

                                    if (!String.IsNullOrEmpty(kovFelelos_Id))
                                    {
                                        Kuld_KovFelelos_CsoportTextBox.Id_HiddenField = kovFelelos_Id;
                                        Kuld_KovFelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                                    }
                                    else
                                    {
                                        label_result_kuldKovFelelos.Visible = false;
                                        Kuld_KovFelelos_CsoportTextBox.Visible = false;
                                    }

                                    if (!String.IsNullOrEmpty(Kuldemeny_Id_HiddenField.Value))
                                    {
                                        string kuldemenyID = Kuldemeny_Id_HiddenField.Value;

                                        //Módosítás,megtekintés gombok beállítása
                                        imgKuldemenyMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                                                                  "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                                        imgKuldemenyModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                                                                  "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                                    }

                                    #endregion
                                }
                                else
                                {
                                    // Iktatás sikeres volt:
                                    ParentId = result_iktatas.Uid;

                                    String UjUgyirat_Id = "";
                                    //String UjUgyiratDarab_Id = "";
                                    String UjIrat_Id = "";
                                    //String UjIratPeldany_Id = "";
                                    ErkeztetesIktatasResult iktatasResult = null;
                                    try
                                    {
                                        // egyelõre csak az ügyirat id-ját küldjük vissza
                                        //UjUgyirat_Id = (String)result_iktatas.Record;

                                        UjIrat_Id = result_iktatas.Uid;

                                        if (result_iktatas.Record != null && result_iktatas.Record is ErkeztetesIktatasResult)
                                        {
                                            iktatasResult = (ErkeztetesIktatasResult)result_iktatas.Record;
                                            UjUgyirat_Id = iktatasResult.UgyiratId;
                                        }
                                        else
                                        {
                                            UjUgyirat_Id = (String)result_iktatas.Record;
                                        }

                                        #region standard objektumfüggõ tárgyszavak mentése ügyirathoz
                                        // csak új ügyiratra
                                        if (ujUgyirat == true)
                                        {
                                            // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                                            // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                                            // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                                            EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak.GetEREC_ObjektumTargyszavaiList(!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos, IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos);

                                            if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                                            {
                                                ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                                EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                                Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                                                , EREC_ObjektumTargyszavaiList
                                                                , UjUgyirat_Id
                                                                , null
                                                                , Constants.TableNames.EREC_UgyUgyiratok
                                                                , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                                                , false
                                                                );

                                                if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                                {
                                                    // hiba
                                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                                    ErrorUpdatePanel1.Update();
                                                    //voltHiba = true; // TODO: ??? true or false ???
                                                }
                                            }
                                        }
                                        #endregion standard objektumfüggõ tárgyszavak mentése ügyirathoz

                                        #region standard objektumfüggõ tárgyszavak mentése irathoz
                                        // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                                        // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                                        // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                                        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList_Iratok = otpStandardTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);

                                        if (EREC_ObjektumTargyszavaiList_Iratok != null && EREC_ObjektumTargyszavaiList_Iratok.Length > 0)
                                        {
                                            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                            Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                                            , EREC_ObjektumTargyszavaiList_Iratok
                                                            , UjIrat_Id
                                                            , null
                                                            , Constants.TableNames.EREC_IraIratok
                                                            , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                                            , false
                                                            );

                                            if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                            {
                                                // hiba
                                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                                ErrorUpdatePanel1.Update();
                                                //voltHiba = true; // TODO: ??? true or false ???
                                            }
                                        }
                                        #endregion standard objektumfüggõ tárgyszavak mentése irathoz

                                        #region típusos objektumfüggõ tárgyszavak mentése irathoz

                                        Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Iratok(UjIrat_Id);

                                        if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                                        {
                                            // hiba
                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_tipusos);
                                            ErrorUpdatePanel1.Update();
                                            //voltHiba = true; // TODO: ??? true or false ???
                                        }

                                        #endregion típusos objektumfüggõ tárgyszavak mentése irathoz
                                    }
                                    catch
                                    {
                                        JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                        return;
                                    }

                                    /// TODO majd a hívó listától függõen beállítani az új rekord id-t,
                                    /// egyelõre az új ügyirat id-ja lesz megadva
                                    String returnId = UjUgyirat_Id;

                                    if (e.CommandName == CommandName.Save)
                                    {
                                        if (Command == CommandName.New)
                                        {

                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);

                                            this.SetResultPanel(iktatasResult);
                                        }
                                    }
                                    else if (e.CommandName == CommandName.SaveAndClose)
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);
                                        JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                    }
                                }

                            }
                            else
                            {
                                // ha be volt töltve küldemény iktatási adat,
                                // egy hibajelenség leküzdésére:
                                LoadKuldemenyComponents();

                            }



                            break;
                        }

                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                                voltHiba = true;
                            }
                            else
                            {
                                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                                execparam.Record_Id = ParentId;

                                EREC_IraIratok erec_IraIratok = GetBusinessObjectFromComponents();

                                Result result_update = service.Update(execparam, erec_IraIratok);
                                if (!String.IsNullOrEmpty(result_update.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_update);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                                else
                                {
                                    #region standard objektumfüggõ tárgyszavak mentése irathoz
                                    // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                                    // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                                    // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                                    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList_Iratok = otpStandardTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);

                                    if (EREC_ObjektumTargyszavaiList_Iratok != null && EREC_ObjektumTargyszavaiList_Iratok.Length > 0)
                                    {
                                        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                        Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                                        , EREC_ObjektumTargyszavaiList_Iratok
                                                        , ParentId
                                                        , null
                                                        , Constants.TableNames.EREC_IraIratok
                                                        , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                                        , false
                                                        );

                                        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                        {
                                            // hiba
                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                            ErrorUpdatePanel1.Update();
                                            voltHiba = true; // TODO: ??? true or false ???
                                        }
                                    }
                                    #endregion standard objektumfüggõ tárgyszavak mentése irathoz

                                    #region típusos objektumfüggõ tárgyszavak mentése irathoz

                                    Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Iratok(ParentId);

                                    if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                                    {
                                        // hiba
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_tipusos);
                                        ErrorUpdatePanel1.Update();
                                        voltHiba = true; // TODO: ??? true or false ???
                                    }

                                    #endregion típusos objektumfüggõ tárgyszavak mentése irathoz
                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    //// újratöltjük a módosítás oldalt
                                    //UI.popupRedirect(Page, "IraIratokForm.aspx?"
                                    //       + QueryStringVars.Command + "=" + CommandName.Modify
                                    //       + "&" + QueryStringVars.Id + "=" + ParentId);            

                                    Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    RaiseEvent_OnChangedObjectProperties();
                                    RequireReloadTab = true;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    //JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }

                            }

                            break;
                        }
                }
            }
            else if (Command == CommandName.New && Mode == CommandName.ElokeszitettIratIktatas &&
                         (FunctionRights.GetFunkcioJog(Page, funkcio_MunkapeldanyBeiktatas_Bejovo)
                          || FunctionRights.GetFunkcioJog(Page, funkcio_MunkapeldanyBeiktatas_Belso))
                 && (String.IsNullOrEmpty(Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField) || FunctionRights.GetFunkcioJog(Page, funkcio_UgyiratSzereles))
                      )
            {
                #region Munkapéldány beiktatása

                EREC_IraIratok erec_IraIrat = GetBusinessObjectFromComponents();
                //EREC_HataridosFeladatok erec_HataridosFeladat = FeljegyzesPanel.GetBusinessObject();

                EREC_PldIratPeldanyok erec_PldIratPeldany = null;

                if (IratPeldanyPanel.Visible == false)
                {
                    // ha nem tettük fel az iratpéldány panelt, az iratpéldány adatokat ne vágjuk fejbe:
                    erec_PldIratPeldany = new EREC_PldIratPeldanyok();
                    erec_PldIratPeldany.Updated.SetValueAll(false);
                    erec_PldIratPeldany.Base.Updated.SetValueAll(false);
                }
                else
                {
                    erec_PldIratPeldany = GetBusinessObjectFromComponents_EREC_PldIratPeldanyok();
                }

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                string iratId = ParentId;
                ErkeztetesIktatasResult iktatasResult = null;

                Result result_munkapeldanyBeiktatas = service_iratok.MunkapeldanyBeiktatasa(execParam, iratId, erec_IraIrat, erec_PldIratPeldany);

                if (!String.IsNullOrEmpty(result_munkapeldanyBeiktatas.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_munkapeldanyBeiktatas);
                    ErrorUpdatePanel1.Update();
                    return;
                }
                else
                {

                    JavaScripts.RegisterSelectedRecordIdToParent(Page, iratId);

                    string ugyiratId = result_munkapeldanyBeiktatas.Uid;


                    #region standard objektumfüggõ tárgyszavak mentése ügyirathoz
                    // csak új ügyiratra
                    //if (!String.IsNullOrEmpty(ugyiratId))
                    //{
                    //    // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                    //    // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                    //    // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                    //    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak.GetEREC_ObjektumTargyszavaiList(!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos, IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos);

                    //    if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                    //    {
                    //        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                    //        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                    //        Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                    //                    , EREC_ObjektumTargyszavaiList
                    //                    , ugyiratId
                    //                    , null
                    //                    , Constants.TableNames.EREC_UgyUgyiratok
                    //                    , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                    //                    , false
                    //                    );

                    //        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                    //        {
                    //            // hiba
                    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                    //            ErrorUpdatePanel1.Update();
                    //            //voltHiba = true; // TODO: ??? true or false ???
                    //        }
                    //    }
                    //}
                    #endregion standard objektumfüggõ tárgyszavak mentése ügyirathoz

                    #region standard objektumfüggõ tárgyszavak mentése irathoz
                    // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                    // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                    // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList_Iratok = otpStandardTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);

                    if (EREC_ObjektumTargyszavaiList_Iratok != null && EREC_ObjektumTargyszavaiList_Iratok.Length > 0)
                    {
                        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                        Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                        , EREC_ObjektumTargyszavaiList_Iratok
                                        , iratId
                                        , null
                                        , Constants.TableNames.EREC_IraIratok
                                        , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                        , false
                                        );

                        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                        {
                            // hiba
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                            ErrorUpdatePanel1.Update();
                            //voltHiba = true; // TODO: ??? true or false ???
                        }
                    }
                    #endregion standard objektumfüggõ tárgyszavak mentése irathoz

                    #region típusos objektumfüggõ tárgyszavak mentése irathoz

                    Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Iratok(iratId);

                    if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                    {
                        // hiba
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_tipusos);
                        ErrorUpdatePanel1.Update();
                        //voltHiba = true; // TODO: ??? true or false ???
                    }

                    #endregion típusos objektumfüggõ tárgyszavak mentése irathoz


                    if (result_munkapeldanyBeiktatas.Record != null && result_munkapeldanyBeiktatas.Record is ErkeztetesIktatasResult)
                    {
                        iktatasResult = (ErkeztetesIktatasResult)result_munkapeldanyBeiktatas.Record;
                    }
                    this.SetResultPanel(iktatasResult);

                    #region Munkaügyirat elõzmény szerelése
                    string szerelendo_ugyiratId = Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField;
                    if (!String.IsNullOrEmpty(szerelendo_ugyiratId))
                    {
                        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam_szereles = UI.SetExecParamDefault(Page);

                        Result result_szereles = null;

                        if (Szerelendo_Ugyirat_UgyiratTextBox.IsMigraltSelected)
                        {
                            string cel_ugyiratAzon = (iktatasResult == null ? "" : (iktatasResult.UgyiratAzonosito ?? "")); ;
                            string szerelendo_ugyiratAzon = Szerelendo_Ugyirat_UgyiratTextBox.Text;
                            result_szereles = service_ugyiratok.RegiAdatSzereles(execParam, ugyiratId, cel_ugyiratAzon
                                 , szerelendo_ugyiratId, szerelendo_ugyiratAzon, null);
                        }
                        else
                        {
                            result_szereles = service_ugyiratok.SzerelesVagyElokeszitesKikeressel(execParam_szereles
                                 , szerelendo_ugyiratId, ugyiratId, null);
                        }

                        SetResultPanel_Szereles(result_szereles);

                        //if (result_szereles.IsError)
                        //{
                        //    // hiba:
                        //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_szereles);
                        //    ErrorUpdatePanel1.Update();
                        //    //return;
                        //}
                    }
                    #endregion Munkaügyirat elõzmény szerelése
                }

                #endregion

            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                //UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        #region IRAT HATASA
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        {
            if ((e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
                 && (Command == CommandName.Modify))
            {
                if (!StartIrataHatasaProcedure())
                    RequireReloadTab = false;
            }
        }
        #endregion
        if (RequireReloadTab)
        {
            ReLoadTab();
        }
    }

    #region BLG_2950
    private void AutomatikusSzignalas(EREC_IraIratokService service, string erec_IratId)
    {
        //LZS - Amennyiben a szignálás típusa automatikus (5) és 
        //a felelős ügyintéző nem lett megváltoztattva az ügytípus betöltése utáni javasolt felelős ügyintézőről
        //akkor lekérjük az ügytípushoz kapcsolódó határidős feladatot, és ID nélkül átadjuk az EREC_IraIratokService Szignalas() metódusának.
        //Szükséges paraméterek még az irat id-ja és a javasoltügyintéző id-ja is.
        if (SzignalasTipusa.Value == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore &&
            JavasoltUgyintezoId.Value.Equals(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField))
        {
            EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = HataridosFeladatId.Value;

            EREC_HataridosFeladatok hataridosFeladat = null;

            if (!string.IsNullOrEmpty(HataridosFeladatId.Value))
            {
                Result resGet = hataridosFeladatokService.Get(execParam);

                if (resGet.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, resGet);
                }
                else
                {
                    hataridosFeladat = (EREC_HataridosFeladatok)resGet.Record;
                    //kiszedjük, mert csak business object feltöltésre használjuk, ellenkező esetben access violation
                    hataridosFeladat.Id = "";
                }
            }

            #region Automatikis szignálás
            ExecParam execParamSzignalas = UI.SetExecParamDefault(Page, new ExecParam());

            Result resultSzignalas = service.Szignalas(execParamSzignalas, erec_IratId, JavasoltUgyintezoId.Value, hataridosFeladat);

            if (resultSzignalas.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, resultSzignalas);
            }

            #endregion
        }
    }
    #endregion

    #region BLG_232
    //LZS
    //Lekérjük a HIVSZAM_ELLENORZES rendszerparaméter értékét, és ha az „1” és a TabFooter.ConfirmationResult.Value nem „1”,
    //akkor lekérjük, hogy van - e már irat a felületen kiválasztott küldemény „Hivatkozási számával” a rendszerben.
    //Amennyiben nincs, akkor megyünk tovább az ágban, de ha igen, akkor feldobunk a felhasználónak egy figyelmeztető ablakot,
    //amelyen kiíratjuk a meglévő rekord „Azonosítóját” és „Tárgyát”. 
    //A felhasználó az ablakon „Ok”-t választva a végrehajtás egy speciális „ForceSave” postbackre fut, amit a Page_Load()-ban kezelünk le.
    private bool KettosIktatasControlling(string hivatkozasiSzam, EREC_IraIratokService service)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        bool hivszam_ellenorzes = Rendszerparameterek.Get(execParam, Rendszerparameterek.HIVSZAM_ELLENORZES) == "1" ? true : false;

        if (hivszam_ellenorzes && TabFooter1.ConfirmationResult.Value != "1")
        {
            bool isExist = false;
            EREC_IraIratokSearch hivatkozasiSzamSearch = new EREC_IraIratokSearch();
            hivatkozasiSzamSearch.HivatkozasiSzam.Value = hivatkozasiSzam;
            hivatkozasiSzamSearch.HivatkozasiSzam.Operator = Query.Operators.equals;

            Result resIratokHivatkozasiSzam = service.GetAll(new ExecParam(), hivatkozasiSzamSearch);

            if (!resIratokHivatkozasiSzam.IsError)
            {
                isExist = resIratokHivatkozasiSzam.Ds.Tables[0].Rows.Count > 0 ? true : false;
            }

            if (isExist)
            {
                string azonosito = "";
                string targy = "";
                string summary = "";

                foreach (DataRow irat in resIratokHivatkozasiSzam.Ds.Tables[0].Rows)
                {
                    azonosito = irat["Azonosito"].ToString();
                    targy = irat["Targy"].ToString();
                    summary += String.Format("\\n[{0}], [{1}]", azonosito, targy);

                }

                string msg = String.Format("A hivatkozásszám mező már szerepel az iratok táblában, a következő azonosítóval és tárggyal. {0} \\n\\nEnnek ellenére végre kívánja hajtani az iktatási műveletet?", summary);

                string js = String.Format("if (confirm('{0}')) {{ __doPostBack('','ForceSave'); }}", msg);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
                return true;
            }

            return false;
        }

        return false;
    }
    #endregion

    private Csatolmany GetInfoszabDokumentum()
    {
        string infoszabDokumentumUrl = Request.QueryString.Get(QueryStringVars.ItemUrl);
        if (String.IsNullOrEmpty(infoszabDokumentumUrl))
        {
            throw new Contentum.eUtility.ResultException(String.Format("Az {0} url paraméter hiányzik!", QueryStringVars.ItemUrl));
        }

        #region infoszab dokumentum letöltése
        byte[] fileData = null;
        try
        {
            //Dokumentum letöltése SharePoint-ból
            Logger.Debug(String.Format("Dokumentum ({0}) letöltése", infoszabDokumentumUrl));

            using (System.Net.WebClient wc = new System.Net.WebClient())
            {
                wc.Credentials = new System.Net.NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                fileData = wc.DownloadData(infoszabDokumentumUrl);
            }
        }
        catch (Exception ex)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(ex);
            resError.ErrorMessage = String.Format("Dokumentum ({0}) letöltése sikertelen!", infoszabDokumentumUrl)
                 + "<br/>" + resError.ErrorMessage;

            throw new Contentum.eUtility.ResultException(resError);
        }

        #endregion

        Csatolmany dokumentum = new Csatolmany();
        dokumentum.Tartalom = fileData;
        dokumentum.Nev = System.IO.Path.GetFileName(infoszabDokumentumUrl);

        Logger.Debug(String.Format("Dokumentum név: {0}", dokumentum.Nev));

        string tartalomHash = String.Empty;
        Logger.Debug("TartalomHash");
        if (FileFunctions.NeedTartalomHashCheck(dokumentum.Nev))
        {
            Logger.Debug("TartalomHash: GetMSFileContentSHA1");
            tartalomHash = FileFunctions.GetMSFileContentSHA1(dokumentum.Tartalom, dokumentum.Nev);
        }
        else
        {
            Logger.Debug("TartalomHash: QueryString");
            tartalomHash = Request.QueryString.Get(QueryStringVars.Hash);
        }



        if (!String.IsNullOrEmpty(tartalomHash))
        {
            // továbbadjuk a tartalomHash-et:
            dokumentum.TartalomHash = tartalomHash;
        }

        return dokumentum;
    }

    private void SetResultPanel(ErkeztetesIktatasResult iktatasResult)
    {
        //Panel-ek beállítása
        KuldemenyPanel.Visible = false;
        Ugyirat_Panel.Visible = false;
        //UgyiratDarab_Panel.Visible = false;
        IratPanel.Visible = false;
        IratPeldanyPanel.Visible = false;
        BejovoPeldanyListPanel1.Visible = false;

        MunkaugyiratElozmenyPanel.Visible = false;
        ResultPanel.Visible = true;
        StandardTargyszavakPanel.Visible = false;
        StandardTargyszavakPanel_Iratok.Visible = false;
        TipusosTargyszavakPanel_Iratok.Visible = false;
        IktatasVagyMunkapeldanyLetrehozasRadioButtonList.Visible = false;
        UgyiratSzerelesiLista1.Reset();

        //TabFooter beállítása
        TabFooter1.ImageButton_Close.Visible = true;
        TabFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";

        #region CR3140 - Iktatószám visszajelzõ panelre is kerüljön ki az Átadás gomb, van amikor kinn láttam már, de általánosságban legyen kinn, mert hasznos. CR3183 - org független
        //if (FelhasznaloProfil.OrgIsBOPMH(Page))
        //{
        Atadasra_kijelolesUgyirat.Visible = true;
        Atadasra_kijelolesUgyirat.Enabled = true;

        // Átadásra kijelölés
        if (Atadasra_kijelolesUgyirat.Enabled)
        {
            if (!string.IsNullOrEmpty(iktatasResult.UgyiratId))
            {
                var statusz = Ugyiratok.GetAllapotById(iktatasResult.UgyiratId, Page, EErrorPanel1);
                Ugyiratok.SetAtadasraKijelolesMulti_Funkciogomb(iktatasResult.UgyiratId, statusz, Atadasra_kijelolesUgyirat, Page);
            }
        }
        //}
        #endregion
        #region CR3153 - BOPMH-ban az iktatás visszajelzõ képernyõre kellene irat átadas gomb is. CR3183 - org független
        //if (FelhasznaloProfil.OrgIsBOPMH(Page))
        //{
        Atadasra_kijelolesIrat.Visible = true;
        Atadasra_kijelolesIrat.Enabled = true;
        // Átadásra kijelölés
        if (Atadasra_kijelolesIrat.Enabled)
        {
            if (!string.IsNullOrEmpty(iktatasResult.IratId))
            {
                //GetElsoIratPeldanyByIraIrat
                var elsoIratpeldany = new EREC_PldIratPeldanyok();
                using (var service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
                {
                    var res = service.GetElsoIratPeldanyByIraIrat(UI.SetExecParamDefault(Page), iktatasResult.IratId);
                    if (res.IsError)
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
                        ErrorUpdatePanel1.Update();
                        ResultPanel.Visible = false;
                        return;
                    }
                    elsoIratpeldany = (EREC_PldIratPeldanyok)res.Record;

                }

                if (elsoIratpeldany != null)
                {
                    var ugyiratStatusz = Ugyiratok.GetAllapotById(iktatasResult.UgyiratId, UI.SetExecParamDefault(Page), EErrorPanel1);
                    var iratStatusz = Iratok.GetAllapotById(iktatasResult.IratId, UI.SetExecParamDefault(Page), EErrorPanel1);
                    var iratPeldanyStatusz = IratPeldanyok.GetAllapotById(elsoIratpeldany.Id, UI.SetExecParamDefault(Page), EErrorPanel1);
                    IratPeldanyok.SetAtadasraKijelolesMulti_Funkciogomb(elsoIratpeldany.Id, iratPeldanyStatusz, iratStatusz, ugyiratStatusz, Atadasra_kijelolesIrat, Page);
                }
                else
                {
                    Atadasra_kijelolesIrat.Enabled = false;
                }
            }
        }
        //}
        #endregion

        // Kimenõ email iktatásakor nem kell a Rendben és új gomb
        // (Nem kell a rendben és köv. alszám sem):
        if (Mode == CommandName.KimenoEmailIktatas)
        {
            TabFooter1.Link_SaveAndNew.Visible = false;
            TabFooter1.Link_SaveAndAlszamBejovo.Visible = false;
            TabFooter1.Link_SaveAndAlszamBelso.Visible = false;
        }
        else
        {
            TabFooter1.Link_SaveAndNew.Visible = true;
            TabFooter1.Link_SaveAndAlszamBejovo.Visible = true;
            TabFooter1.Link_SaveAndAlszamBelso.Visible = true;
        }

        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        // Ha az egyszerûsített iktatás képernyõrõl jött az átirányítás, azt meg kell jegyezni az URL-ekben
        string startup = Request.QueryString.Get(QueryStringVars.Startup);
        string startupQueryString = "";
        if (startup == Constants.Startup.FromEgyszerusitettIktatas)
        {
            startupQueryString = "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromEgyszerusitettIktatas;
        }
        else
        {
            startupQueryString = "";
        }

        string saveAndNewUrl = String.Empty;
        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        if (Mode == CommandName.BejovoIratIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
        {
            saveAndNewUrl = "~/IraIratokForm.aspx?"
                 + QueryStringVars.Command + "=" + CommandName.New;

            // ha volt betöltve template:
            if (!String.IsNullOrEmpty(FormHeader.CurrentTemplateId))
            {
                saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader.CurrentTemplateId;
            }
        }
        else if (Mode == CommandName.BelsoIratIktatas)
        {
            saveAndNewUrl = "~/IraIratokForm.aspx?"
                 + QueryStringVars.Command + "=" + CommandName.New
                 + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                 + startupQueryString;

            // ha volt betöltve template:
            if (!String.IsNullOrEmpty(FormHeader.CurrentTemplateId))
            {
                saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader.CurrentTemplateId;
            }
        }
        else if (Mode == CommandName.ElokeszitettIratIktatas)
        {
            saveAndNewUrl = "~/IraIratokForm.aspx?"
                 + QueryStringVars.Command + "=" + CommandName.New
                 + "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas;
        }

        TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

        if (iktatasResult == null)
        {
            Logger.Error("iktatasResult == null");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }
        if (String.IsNullOrEmpty(iktatasResult.UgyiratId) || String.IsNullOrEmpty(iktatasResult.IratId))
        {
            Logger.Error("String.IsNullOrEmpty(iktatasResult.UgyiratId) || String.IsNullOrEmpty(iktatasResult.IratId)");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }

        string ugyiratId = iktatasResult.UgyiratId;
        string iratId = iktatasResult.IratId;

        // Felirat az IktatasResult panelen:
        if (iktatasResult.MunkaPeldany == true)
        {
            // A munkapéldány sikeresen létrejött.
            Label_ResultPanelText.Text = Resources.Form.MunkaanyagLetrehozasResultText;
        }
        else
        {
            // Az iktatás sikeresen végrehajtódott.
            Label_ResultPanelText.Text = Resources.Form.IktatasResultText;
        }


        try
        {
            //Fõszámok beállítása
            //ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
            ////Ugyirat
            //EREC_UgyUgyiratokService srvUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            //EREC_UgyUgyiratokSearch searchUgyiratok = new EREC_UgyUgyiratokSearch(); ;
            //searchUgyiratok.Id.Value = ugyiratId;
            //searchUgyiratok.Id.Operator = Query.Operators.equals;
            //Result res = srvUgyiratok.GetAllWithExtension(xcParam, searchUgyiratok);
            //if (!String.IsNullOrEmpty(res.ErrorCode))
            //{
            //    throw new Contentum.eUtility.ResultException(res);
            //}
            //if (res.Ds.Tables[0].Rows.Count == 0)
            //{
            //    throw new Contentum.eUtility.ResultException(52106);
            //}
            //labelUgyiratFoszam.Text = res.Ds.Tables[0].Rows[0]["Foszam_Merge"].ToString();
            labelUgyiratFoszam.Text = iktatasResult.UgyiratAzonosito ?? Resources.Error.UIAzonositoUndefined;
            // BLG_619
            // Egyelõre nem kell
            //if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
            //{

            //    EREC_UgyUgyiratokService tempService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            //    ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //    string ugyfelelos_szervezetkod = "";
            //    Result tempResult = tempService.GetUgyFelelosSzervezetKod(temp_execParam, iktatasResult.UgyiratId);
            //    if (!tempResult.IsError)
            //    {
            //        ugyfelelos_szervezetkod = tempResult.Record.ToString();

            //    }
            //    if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
            //    {
            //        labelUgyiratFoszam.Text = ugyfelelos_szervezetkod + " " + iktatasResult.UgyiratAzonosito ?? Resources.Error.UIAzonositoUndefined;
            //    }
            //}



            // Nem Alszámos, Bejövõ iktatásnál megjelenítjük a következõ felelõst (kezelõt)
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            if ((Mode == CommandName.BejovoIratIktatas || Mode == CommandName.MunkapeldanyBeiktatas_Bejovo) && String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                tr_resultPanel_kovFelelos.Visible = true;
                KovFelelos_Ugyirat_result_CsoportTextBox.Id_HiddenField = iktatasResult.UgyiratCsoportIdFelelos ?? String.Empty;
                KovFelelos_Ugyirat_result_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            }


            //Irat
            //EREC_IraIratokService srvIratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            //EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
            //searchIratok.Id.Value = iratId;
            //searchIratok.Id.Operator = Query.Operators.equals;
            //res = srvIratok.GetAllWithExtension(xcParam, searchIratok);
            //if (!String.IsNullOrEmpty(res.ErrorCode))
            //{
            //    throw new Contentum.eUtility.ResultException(res);
            //}
            //if (res.Ds.Tables[0].Rows.Count == 0)
            //{
            //    throw new Contentum.eUtility.ResultException(52118);
            //}
            //labelIratFoszam.Text = res.Ds.Tables[0].Rows[0]["IktatoSzam_Merge"].ToString();
            labelIratFoszam.Text = iktatasResult.IratAzonosito ?? Resources.Error.UIAzonositoUndefined;
            // BLG_619
            // Egyelõre nem kell
            //if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
            //{

            //    EREC_IraIratokService tempService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            //    ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //    string iratfelelos_szervezetkod = "";
            //    Result tempResult = tempService.GetIratFelelosSzervezetKod(temp_execParam, iktatasResult.IratId);
            //    if (!tempResult.IsError)
            //    {
            //        iratfelelos_szervezetkod = tempResult.Record.ToString();
            //        if (!String.IsNullOrEmpty(iratfelelos_szervezetkod))
            //        {
            //            labelIratFoszam.Text = iratfelelos_szervezetkod + " " + iktatasResult.IratAzonosito ?? Resources.Error.UIAzonositoUndefined;
            //        }
            //    }
            //}
            //TabFooter beállítása

            string saveAndAlszamBejovoUrl = String.Empty;
            if (startup == Constants.Startup.FromEgyszerusitettIktatas)
            {
                // Vissza az egyszerûsített iktatás képernyõre
                saveAndAlszamBejovoUrl = "~/EgyszerusitettIktatasForm.aspx?"
                     + QueryStringVars.Command + "=" + CommandName.New
                     + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;
            }
            else
            {
                saveAndAlszamBejovoUrl = "~/IraIratokForm.aspx?"
                          + QueryStringVars.Command + "=" + CommandName.New
                          + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;
            }

            TabFooter1.Link_SaveAndAlszamBejovo.NavigateUrl = saveAndAlszamBejovoUrl;

            string saveAndAlszamBelsoUrl = String.Empty;
            if (Mode == CommandName.ElokeszitettIratIktatas)
            {
                saveAndAlszamBelsoUrl = "~/IraIratokForm.aspx?"
                     + QueryStringVars.Command + "=" + CommandName.New
                     + "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas
                     + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;
            }
            else
            {
                saveAndAlszamBelsoUrl = "~/IraIratokForm.aspx?"
                     + QueryStringVars.Command + "=" + CommandName.New
                     + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                     + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                     + startupQueryString;
            }
            TabFooter1.Link_SaveAndAlszamBelso.NavigateUrl = saveAndAlszamBelsoUrl;


            // ha módosult az ügyirat határideje alszámos iktatásnál, megjelenítjük:
            if (!String.IsNullOrEmpty(iktatasResult.UgyiratUjHatarido))
            {
                tr_resultPanel_UgyiratUjHatarido.Visible = true;
                labelResultUgyiratUjHatarido.Text = iktatasResult.UgyiratUjHatarido;
            }

            //Módosítás,megtekintés gombok beállítása
            //ügyirat
            imgUgyiratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                                      "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgUgyiratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                                      "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgEloadoiIv.OnClientClick = "javascript:window.open('UgyUgyiratokPrintFormSSRSEloadoiIv.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "')";

            //imgEloadoiIv.OnClientClick = "javascript:if(document.getElementById(\"downloadDiv\") == null) { var downloadDiv = document.createElement(\"div\"); document.getElementsByTagName(\"body\")[0].appendChild(downloadDiv); downloadDiv.style.width = \"0px\"; downloadDiv.style.height = \"0px\"; downloadDiv.id = \"downloadDiv\"; } else var downloadDiv = document.getElementById(\"downloadDiv\"); if(document.getElementById(\"downloadFrame\") == null) { var downloadFrame = document.createElement(\"iframe\"); downloadFrame.id = \"downloadFrame\"; downloadFrame.src = 'EloadoiIv_PrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "'; downloadFrame.scrolling = \"no\"; downloadFrame.style.width = \"0px\"; downloadFrame.style.height = \"0px\"; downloadDiv.innerHTML = downloadFrame.outerHTML; } else { var downloadFrame = document.getElementById(\"downloadFrame\"); downloadFrame.src = 'EloadoiIv_PrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "'; }"; 
            //irat
            imgIratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                                      "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                                      "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModositHatosagi.OnClientClick = UI.OpenHatosagiAdatokWithNoPostback(iratId, true);
            imgeMailNyomtatas.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("eMail_PrintForm.aspx?" + QueryStringVars.IratId + "=" + iratId + "&" + QueryStringVars.EmailBoritekokId + "=" + EmailBoritekokId);

            //HA NEM TÜK-ös
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
            {
                //CR 3058
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                {
                    ImageButtonVonalkodNyomtatasUgyirat.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "&tipus=Ugyirat&" + QueryStringVars.Command + "=Modify" + "')";
                    //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny"); 
                    ImageButtonVonalkodNyomtatasIrat.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.IratId + "=" + iratId + "&tipus=Irat&" + QueryStringVars.Command + "=Modify" + "')";
                    //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny"); 
                }
                else
                {
                    ImageButtonVonalkodNyomtatasUgyirat.Visible = false;
                    ImageButtonVonalkodNyomtatasUgyirat.Enabled = false;
                    //JavaScripts.SetOnClientClickIFrame
                    ImageButtonVonalkodNyomtatasIrat.Visible = false;
                    ImageButtonVonalkodNyomtatasIrat.Enabled = false;
                }
            }
            else//HA TÜK-ös
            {
                bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                ImageButtonVonalkodNyomtatasUgyirat.Visible = _visible;
                ImageButtonVonalkodNyomtatasUgyirat.Enabled = _visible;
                ImageButtonVonalkodNyomtatasIrat.Visible = _visible;
                ImageButtonVonalkodNyomtatasIrat.Enabled = _visible;
            }

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = iratId;

            Result result = service.Get(execParam);

            if (string.IsNullOrEmpty(result.ErrorMessage))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                if (erec_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
                {
                    imgeMailNyomtatas.Visible = true;
                }
                else
                {
                    imgeMailNyomtatas.Visible = false;
                }
                // Bejövõ irat esetén a ahtósági adatok gomb eltüntetése
                // BLG_354
                // BLG_2020 (execParam)
                if (!Iratok.IsHatosagiUgy(execParam, iratId))
                {
                    imgIratModositHatosagi.Visible = false;
                }
                else
                    if ((erec_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo) && (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.HATOSAGI_ADATOK_BEJOVOHOZ).ToUpper().Trim().Equals("NEM")))
                {
                    imgIratModositHatosagi.Visible = false;
                }
                else
                    imgIratModositHatosagi.Visible = true;

                //if (erec_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                //    imgIratModositHatosagi.Visible = false;
                //else
                //    imgIratModositHatosagi.Visible = true;
            }
            else
            {
                imgeMailNyomtatas.Visible = false;
            }


        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }

        FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);

        if (fm != null)
        {
            fm.ObjektumId = iratId;
        }


    }

    private void SetResultPanel_Szereles(Result result_szereles)
    {
        tableSzereltUgyirat.Visible = true;
        divSzerelesResult.Visible = true;
        if (result_szereles.IsError)
        {
            labelSzerelesResult.Text = Resources.Form.MunkaugyiratElozmenySzerelesResultText_Error; //"Hiba történt az elõzmény szerelése során! A szerelés sikertelen!";
            trSzerelesHiba.Visible = true;
            labelSzerelesHibaUzenet.Text = ResultError.GetErrorMessageFromResultObject(result_szereles);
        }
        else
        {
            trSzereltUgyirat.Visible = true;
            if (result_szereles.Record != null && (bool)result_szereles.Record == false)
            {
                labelSzerelesResult.Text = Resources.Form.MunkaugyiratElozmenySzerelesElokeszitesResultText_Success; //Az elõzmény szerelése elõ lett készítve!
            }
            else
            {
                labelSzerelesResult.Text = Resources.Form.MunkaugyiratElozmenySzerelesResultText_Success; //Az elõzmény szerelése sikeresen véghajtódott!
            }
            labelSzereltUgyiratFoszam.Text = Szerelendo_Ugyirat_UgyiratTextBox.Text;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        //LZS - BUG_7043 - Eltávolítjuk a [Törölt kódérték] szöveget úgy, hogy felülírjuk az Items[0].Value-val, mert abban nincs benne.
        if (IntezesiIdo_KodtarakDropDownList.KodtarakList.Items.Count > 0)
        {
            IntezesiIdo_KodtarakDropDownList.KodtarakList.Items[0].Text = IntezesiIdo_KodtarakDropDownList.KodtarakList.Items[0].Value;
        }

        // ügyintézõ csak az ügyfelelõs szervezete alá tartozó lehet, csak azok jöjjenek a listában is
        //string orig = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.OnClick_Lov;
        //string insert =
        //        QueryStringVars.CsoportFilter.HierarchiabanLefele + "=' + ($get('" + Ugyfelelos_CsoportTextBox.HiddenField.ClientID + "').value.length>0).toString() + '&" +
        //        QueryStringVars.CsoportFilter.KozvetlenulHozzarendelt + "=false&" +
        //        QueryStringVars.CsoportFilter.SzervezetId +
        //        "=' + $get('" + Ugyfelelos_CsoportTextBox.HiddenField.ClientID + "').value + '&";

        //UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.OnClick_Lov = orig.Insert(orig.IndexOf("CsoportokLovList.aspx?") + "CsoportokLovList.aspx?".Length, insert);


        if (ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible)
        {
            //Szûrés beállítása a csoportTextBox-okra
            Ugyfelelos_CsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            UgyUgyiratok_CsoportId_Felelos.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            // BLG_619
            Iratfelelos_CsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);

        }
        else
        {
            //Szûrés beállítása a csoportTextBox-okra
            Ugyfelelos_CsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            UgyUgyiratok_CsoportId_Felelos.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            // BLG_619
            Iratfelelos_CsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);

        }

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterBySzervezet(Ugyfelelos_CsoportTextBox);
        Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterBySzervezet(Ugyfelelos_CsoportTextBox);
        // BLG_619
        //Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterBySzervezet(Ugyfelelos_CsoportTextBox);
        Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterBySzervezet(Iratfelelos_CsoportTextBox);



        labelKiadmanyozasDatuma.Visible = ccKiadmanyozasDatuma.Visible = IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Visible;
        #region CR3083 iktatáskor van egy olyan mezõ, hogy "ügyfél, ügyindító", innen kerüljön le az ügyindító, csak "ügyfél" maradjon
        // és CR3082 iktatáskor van egy olyan mezõ, hogy irat jellege, ez a mezõ nem szükséges, eltüntethetõ a felületrõl 
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            lblUgyindito.Text = "Ügyfél:";
            labelIratJelleg.Visible = false;
            IratJellegKodtarakDropDown.Validate = false;
            IratJellegKodtarakDropDown.Enabled = false;
            IratJellegKodtarakDropDown.Visible = false;
        }
        #endregion
        //#region CR3081 : BOPMH paraméterezetten kellene megoldani, hogy ne lehessen kiválasztani a migráltból irattárban levõt elõzményként
        //if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        //{
        //    IratJellegKodtarakDropDown.Visible = false;

        //}
        //#endregion

        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
            CopyTargy();

        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE)
        //if (!FelhasznaloProfil.OrgIsNMHH(Page))
        {
            SetVisibilityIratHatasa(false);
            HideFelfuggesztesOka();
            HideLezarasOka();
        }

        //NMHH
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        //if (FelhasznaloProfil.OrgIsNMHH(Page))
        {
            // BLG_1876 
            // BLG kapcsán javítva (olyan esetben validált, amikor a mezõ nem is látszott)
            if (Pld_KuldesMod_KodtarakDropDownList.Enabled)
                Pld_KuldesMod_KodtarakDropDownList.Validate = true;
            SetVisibilityIratHatasa(true);
            ShowFelfuggesztesOka();
            ShowLezarasOka();

            // BUG_9291
            if (Command == CommandName.View)
            {
                KodtarakDropDownListUgyiratFelfuggesztesOka.ReadOnly = true;
                TextBoxFelfuggesztesOka.ReadOnly = true;

                KodtarakDropDownListUgyiratLezarasOka.ReadOnly = true;
            }
        }

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_JOVAHAGYO_UGYFELELOS, false))
        {
            Ugyfelelos_CsoportTextBox.Validate = !Ugyfelelos_CsoportTextBox.ReadOnly && Ugyfelelos_CsoportTextBox.Visible;
        }

        Pld_IratpeldanyFajtaja_KodtarakDropDownList.Validate = !Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly;
        Label_Pld_IratpeldanyFajtajaReq.Visible = Pld_IratpeldanyFajtaja_KodtarakDropDownList.Validate;

        // BLG_8826
        if (!Page.IsPostBack)
        {
            _iratForm.SetDefaultUgyintezesKezdete();
        }

        Pld_CimId_Cimzett_CimekTextBox.SetAutomatikusKitoltes(Pld_KuldesMod_KodtarakDropDownList.DropDownList, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList, ElozmenyUgyiratID_HiddenField, null);
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            _Load = true;

            LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader.TemplateObject);

            _Load = false;
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormHeader.NewTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    private void LoadComponentsFromTemplate(IktatasFormTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            // csak ha nincs megadva elõzmény:
            if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                #region Ügyirat komponensek

                CascadingDropDown_AgazatiJel.SelectedValue = templateObject.AgazatiJel_Id;
                CascadingDropDown_Ugykor.SelectedValue = templateObject.UgyiratComponents.IraIrattariTetel_Id;
                CascadingDropDown_IktatoKonyv.SelectedValue = templateObject.UgyiratComponents.IraIktatokonyv_Id;
                CascadingDropDown_Ugytipus.SelectedValue = templateObject.UgyiratComponents.UgyTipus;

                UgyUgyirat_Targy_RequiredTextBox.Text = templateObject.UgyiratComponents.Targy;

                Ugyfelelos_CsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos;
                Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Felelos;
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
                     templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez;
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                // ügyindító/ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(templateObject.UgyiratComponents.Partner_Id_Ugyindito, templateObject.UgyiratComponents.NevSTR_Ugyindito, EErrorPanel1);
                CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(templateObject.UgyiratComponents.Cim_Id_Ugyindito, templateObject.UgyiratComponents.CimSTR_Ugyindito, EErrorPanel1);

                #endregion
            }

            #region Irat komponensek

            //IraIrat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA, templateObject.IratComponents.Kategoria
            //    , true, EErrorPanel1);

            IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, templateObject.IratComponents.Irattipus, true, EErrorPanel1);

            IraIrat_KiadmanyozniKell_CheckBox.Checked = templateObject.IratComponents.KiadmanyozniKell == "1";

            IraIrat_Targy_RequiredTextBox.Text = templateObject.IratComponents.Targy;

            // CR3401
            var isNewBelsoIratIktatasBelsoUgyintezo = Command == CommandName.New && Mode == CommandName.BelsoIratIktatas && IsIktatasBelsoUgyintezoIktato; // BUG_8001
            if (!isNewBelsoIratIktatasBelsoUgyintezo)
            {
                Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField =
                     templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez;
                Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            }
            else
            {
                #region 11580
                Iratfelelos_CsoportTextBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
                Iratfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                FelelosSzervezetVezetoTextBoxInstance.SetSzervezet(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                #endregion 11580
            }

            // BLG_619
            if (!isNewBelsoIratIktatasBelsoUgyintezo ||
                (Contentum.eUtility.Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(Page), templateObject.IratComponents.Csoport_Id_Ugyfelelos))) // BUG_8001
            {
                Iratfelelos_CsoportTextBox.Id_HiddenField = templateObject.IratComponents.Csoport_Id_Ugyfelelos;
                Iratfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                FelelosSzervezetVezetoTextBoxInstance.SetSzervezet(templateObject.IratComponents.Csoport_Id_Ugyfelelos);
            }

            // BUG_7320
            AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
                    templateObject.IratComponents.UgyintezesModja, false, EErrorPanel1);
            #endregion

            #region IratPéldány komponensek

            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = templateObject.IratPeldanyComponents.Partner_Id_Cimzett;
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = templateObject.IratPeldanyComponents.NevSTR_Cimzett;

            Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = templateObject.IratPeldanyComponents.Cim_id_Cimzett;
            Pld_CimId_Cimzett_CimekTextBox.Text = templateObject.IratPeldanyComponents.CimSTR_Cimzett;

            Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(
                 kcs_KULDEMENY_KULDES_MODJA, templateObject.IratPeldanyComponents.KuldesMod, false, EErrorPanel1);

            Pld_Visszavarolag_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VISSZAVAROLAG,
                 templateObject.IratPeldanyComponents.Visszavarolag, true, EErrorPanel1);

            if (!Pld_IratpeldanyFajtaja_KodtarakDropDownList.ReadOnly)
            {
                // BUG_7717
                // Az iratpéldány fajtájánál a listából le kell venni a 'Vegyes' értéket:
                List<string> ktUgyintezesAlapjaFilterList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYINTEZES_ALAPJA, this.Page, null)
                                    .Where(kt => kt.Kod != KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                                    .Select(kt => kt.Kod)
                                    .ToList();
                Pld_IratpeldanyFajtaja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, ktUgyintezesAlapjaFilterList, false, EErrorPanel1);

                if (!String.IsNullOrEmpty(templateObject.IratPeldanyComponents.UgyintezesModja))
                {
                    Pld_IratpeldanyFajtaja_KodtarakDropDownList.SetSelectedValue(templateObject.IratPeldanyComponents.UgyintezesModja);
                }
            }

            #endregion

            #region elozmeny search

            if (templateObject.ElozmenySearch != null)
            {
                evIktatokonyvSearch.EvTol = templateObject.ElozmenySearch.Ev;
                bool isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);
                if (isTUK)
                {
                    //TÜK esetén az id lesz az érték mezőnek feltöltve, mivel itt Id szerinti keresés lesz
                    IktatoKonyvekDropDownList_Search.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato, templateObject.ElozmenySearch.Ev, templateObject.ElozmenySearch.Ev, true,
                     false, templateObject.ElozmenySearch.Iktatokonyv, EErrorPanel1);
                }
                else
                {
                    IktatoKonyvekDropDownList_Search.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, templateObject.ElozmenySearch.Ev, templateObject.ElozmenySearch.Ev, true,
                     false, templateObject.ElozmenySearch.Iktatokonyv, EErrorPanel1);
                }
                numberFoszamSearch.Text = templateObject.ElozmenySearch.Foszam;
            }

            #endregion

            #region hataridos feladat

            if (templateObject.HataridosFeladat != null)
            {
                FeljegyzesPanel.SetFeladatByBusinessObject(templateObject.HataridosFeladat);
            }
            #endregion

            #region MINOSITO
            MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId = templateObject.IratComponents.Minosito;
            // Szülõ: Ügyindító partner:
            MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId = templateObject.IratComponents.MinositoSzervezet;
            // Névfeloldás Id alapján:
            MinositoPartnerControlIraIratokFormTab.SetPartnerTextBoxById(EErrorPanel1);

            #endregion
        }
    }

    private IktatasFormTemplateObject GetTemplateObjectFromComponents()
    {
        //EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();      
        IktatasFormTemplateObject templateObject = new IktatasFormTemplateObject();

        // csak ha nincs megadva elõzmény:
        if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            #region Ügyirat komponensek

            templateObject.AgazatiJel_Id = AgazatiJelek_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIktatokonyv_Id = Iktatokonyvek_DropDownLis_Ajax.SelectedValue; ;

            templateObject.UgyiratComponents.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;

            // intézési határidõ nem kell

            //templateObject.UgyiratComponents.IktatoszamKieg = UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.SelectedValue;

            templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;

            // ügyindító/ügyfél
            templateObject.UgyiratComponents.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            templateObject.UgyiratComponents.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            templateObject.UgyiratComponents.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            templateObject.UgyiratComponents.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;

            #endregion
        }

        #region Irat komponensek

        templateObject.IratComponents.Targy = IraIrat_Targy_RequiredTextBox.Text;

        //templateObject.IratComponents.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;

        templateObject.IratComponents.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;


        templateObject.IratComponents.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";

        // CR3401
        templateObject.IratComponents.FelhasznaloCsoport_Id_Ugyintez = Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;

        // BLG_619
        templateObject.IratComponents.Csoport_Id_Ugyfelelos = Iratfelelos_CsoportTextBox.Id_HiddenField;

        // BUG_7320
        templateObject.IratComponents.UgyintezesModja = AdathordozoTipusa_KodtarakDropDownList.SelectedValue;
        #endregion

        #region IratPéldány komponensek

        templateObject.IratPeldanyComponents.Partner_Id_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;

        templateObject.IratPeldanyComponents.NevSTR_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Text;

        templateObject.IratPeldanyComponents.Cim_id_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField;

        templateObject.IratPeldanyComponents.CimSTR_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Text;

        templateObject.IratPeldanyComponents.KuldesMod = Pld_KuldesMod_KodtarakDropDownList.SelectedValue;

        templateObject.IratPeldanyComponents.Visszavarolag = Pld_Visszavarolag_KodtarakDropDownList.SelectedValue;

        templateObject.UgyiratPeldanySzukseges = UgyiratpeldanySzukseges_CheckBox.Checked;

        // BLG_350
        //templateObject.KeszitoPeldanya = KeszitoPeldanya_CheckBox.Checked;

        // BUG_7320
        templateObject.IratPeldanyComponents.UgyintezesModja = Pld_IratpeldanyFajtaja_KodtarakDropDownList.SelectedValue;

        #endregion

        #region elozmeny search

        templateObject.ElozmenySearch = new IktatasFormTemplateObject.ElozmenySearchcomponent();
        templateObject.ElozmenySearch.Ev = evIktatokonyvSearch.EvTol;
        templateObject.ElozmenySearch.Iktatokonyv = IktatoKonyvekDropDownList_Search.SelectedValue;
        templateObject.ElozmenySearch.Foszam = numberFoszamSearch.Text;

        #endregion

        #region hataridos feladat

        templateObject.HataridosFeladat = FeljegyzesPanel.GetBusinessObject();

        #endregion

        #region MINOSITO
        templateObject.IratComponents.Minosito = MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoPartnerId;
        templateObject.IratComponents.MinositoSzervezet = MinositoPartnerControlIraIratokFormTab.HiddenFieldMinositoSzuloPartnerId;
        #endregion

        return templateObject;
    }

    #endregion

    #region Kuldemeny, Elozmeny gyors kereses

    protected void ImageButton_KuldemenyKereses_Click(object sender, ImageClickEventArgs e)
    {
        string vonalkod = VonalkodTextBoxKuldemeny.Text;
        if (!String.IsNullOrEmpty(vonalkod))
        {
            LoadKuldemenyComponents(vonalkod);
        }
    }

    protected void ImageButton_Elozmeny_Click(object sender, ImageClickEventArgs e)
    {
        UgyiratSzerelesiLista1.Reset();

        #region Keresesi mezok ellenorzese
        string errorHeader = "Elõzmény keresés hiba!";
        if (String.IsNullOrEmpty(evIktatokonyvSearch.EvTol))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva év!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (IktatoKonyvekDropDownList_Search.DropDownList.SelectedIndex == -1 || String.IsNullOrEmpty(IktatoKonyvekDropDownList_Search.SelectedValue))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs kiválasztva iktatókönyv!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (String.IsNullOrEmpty(numberFoszamSearch.Text))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva fõszám!");
            ErrorUpdatePanel1.Update();
            return;
        }
        #endregion

        try
        {
            if (IsRegiAdatSearch())
            {
                SearchRegiElozmenyUgyirat();
            }
            else
            {
                SearchElozmenyUgyirat();
            }
        }
        catch (FormatException fe)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, fe.Message);
            ErrorUpdatePanel1.Update();
        }
    }

    private void SearchRegiElozmenyUgyirat()
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch search = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
        evIktatokonyvSearch.SetSearchObjectFields(search.UI_YEAR);
        search.UI_SAV.Value = IktatoKonyvekDropDownList_Search.GetSelectedSav();
        search.UI_SAV.Operator = Query.Operators.equals;
        search.UI_NUM.Value = numberFoszamSearch.Text;
        search.UI_NUM.Operator = Query.Operators.equals;
        Contentum.eMigration.Service.MIG_FoszamService service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetFoszamWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            DisplayElozmenySearchError(res, false);
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

        Contentum.eMigration.BusinessDocuments.MIG_Foszam foszam = (Contentum.eMigration.BusinessDocuments.MIG_Foszam)res.Record;

        if (foszam != null && !String.IsNullOrEmpty(foszam.Id))
        {
            //Edok-os ügyiratba van szerelve
            string utoiratId = foszam.Edok_Utoirat_Id;
            if (!String.IsNullOrEmpty(utoiratId))
            {
                SetSzerelesiLista(res, true, true);
                SearchElozmenyUgyirat(utoiratId);
                return;
            }

            if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }

            DataRow foszamRow = res.Ds.Tables[0].Rows[res.Ds.Tables[0].Rows.Count - 1];
            String UgyHol = foszamRow["UGYHOL"].ToString();

            // selejtezett migrált ügyirat: nem lehet bele iktatni!
            if (UgyHol == Constants.MIG_IratHelye.Selejtezett)
            {
                MigraltUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();

                // Az ügyiratba nem lehet iktatni! Az ügyirat selejtezett.
                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.ErrorCode_52139);
                ErrorUpdatePanel1.Update();
                return;
            }

            MigraltUgyiratID_HiddenField.Value = foszam.Id;
            // BUG_13049
            //UgyUgyirat_Targy_RequiredTextBox.Text = foszam.Conc;
            UgyUgyirat_Targy_RequiredTextBox.Text = foszam.MEMO;

            regiAzonositoTextBox.Text = foszamRow["Foszam_Merge"].ToString();
            IrattariTetelTextBox.Text = foszamRow["IrattariTetelszam"].ToString();
            MegkulJelzesTextBox.Text = foszamRow["MegkulJelzes"].ToString();
            IRJ2000TextBox.Text = foszamRow["IRJ2000"].ToString();

            // a JavaScript valamiért nem mûködik, ha közvetlenül a CalendarControl
            // DateTextBoxba írunk (Visible = "true" Style="display: none;" mellett)
            // ezért másik textboxba írással kerüljük meg a problémát -> itt csak azért,
            // mert úgyis ezekbõl vesszük át az értéket a CalendarControlokba a
            // SetElozmenyRegiUgyirat()-ban
            txtIrattarba.Text = "";
            txtSkontroVege.Text = "";
            if (UgyHol == Constants.MIG_IratHelye.Irattarban)
            {
                txtIrattarba.Text = foszamRow["IRATTARBA"].ToString();
                // ne legyen üres érték, mert az irat helyét nem adjuk át
                if (String.IsNullOrEmpty(txtIrattarba.Text))
                {
                    txtIrattarba.Text = " ";
                }
            }
            else if (UgyHol == Constants.MIG_IratHelye.Skontro)
            {
                txtSkontroVege.Text = foszamRow["SCONTRO"].ToString();
                // ne legyen üres érték, mert az irat helyét nem adjuk át
                if (String.IsNullOrEmpty(txtSkontroVege.Text))
                {
                    txtSkontroVege.Text = " ";
                }
            }

            #region standard objektumfüggõ tárgyszavak
            string HelyrajziSzam = foszamRow["UI_HRSZ"].ToString();
            if (!String.IsNullOrEmpty(HelyrajziSzam))
            {
                otpStandardTargyszavak.TryFillFieldWithValue("Helyrajzi szám", HelyrajziSzam);
            }
            #endregion standard objektumfüggõ tárgyszavak

            SetElozmenyRegiUgyirat();
            SetSzerelesiLista(res, true);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }
    }

    private void SearchElozmenyUgyirat(string ugyiratId)
    {
        SearchElozmenyUgyirat(ugyiratId, true);
    }

    private void SearchElozmenyUgyirat(string ugyiratId, bool alwaysSetSzerelesiLista)
    {
        bool searchUtoirat = !String.IsNullOrEmpty(ugyiratId);
        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

        if (!searchUtoirat)
        {
            evIktatokonyvSearch.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);
            IktatoKonyvekDropDownList_Search.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
            search.Foszam.Value = numberFoszamSearch.Text;
            search.Foszam.Operator = Query.Operators.equals;
        }
        else
        {
            search.Id.Value = ugyiratId;
            search.Id.Operator = Query.Operators.equals;
        }

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetUgyiratWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            if (searchUtoirat)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A keresett régi ügyirat szerelve van. A szülõ ügyirat lekérése sikertelen: " + ugyiratId);
            }
            else
            {
                DisplayElozmenySearchError(res, true);
                ClearElozmenyUgyirat();
            }
            ErrorUpdatePanel1.Update();
            return;
        }



        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;

        if (ugyirat != null && !String.IsNullOrEmpty(ugyirat.Id))
        {
            if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }
            // BUG_12060
            string iratId = Request.QueryString.Get(QueryStringVars.IratId);
            EREC_UgyUgyiratok aktUgyirat = null;
            if (!string.IsNullOrEmpty(iratId))
            {
                ExecParam execParamFind = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_IraIratok irat = Contentum.eUtility.Sakkora.GetIrat(execParamFind, iratId);
                if (irat != null)
                {
                    aktUgyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParamFind, irat.Ugyirat_Id);
                }
            }
            if (aktUgyirat != null)
            {
                if (aktUgyirat.Id == ugyirat.Id && this.Mode == "AtIktatas")
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52875); //Az önmagába átiktatás nem lehetséges, válasszon másik ügyiratot!

                    ErrorUpdatePanel1.Update();
                    return;
                }
            }
            ElozmenyUgyiratID_HiddenField.Value = ugyirat.Id;
            SetElozmenyUgyirat(ugyirat);
            if (alwaysSetSzerelesiLista || ugyirat.Id != ugyiratId)
            {
                SetSzerelesiLista(res, false, searchUtoirat);
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

    }

    private void DisplayElozmenySearchError(Result res, bool IsElozmenyHiba)
    {
        if (res.ErrorCode == "57000")
        {
            string onclick;
            if (IsElozmenyHiba)
                onclick = GetElozmenyOnClientClick();
            else
                onclick = GetMigraltOnClientClick();
            string hiba = "A keresett ügyirat nem található. <span onclick=\"" + onclick + "\" class=\"linkStyle\">Keresõ képernyõ megnyitása</span>";
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", hiba);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        }
    }

    private void SearchElozmenyUgyirat()
    {
        SearchElozmenyUgyirat(String.Empty);
    }

    private void SetSzerelesiLista(Result result)
    {
        SetSzerelesiLista(result, false, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat)
    {
        SetSzerelesiLista(result, RegiAdat, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat, bool IsUtoirat)
    {
        string SelectedId = String.Empty;

        if (!RegiAdat)
            SelectedId = ElozmenyUgyiratID_HiddenField.Value;
        else
            SelectedId = MigraltUgyiratID_HiddenField.Value;

        DataTable ugyiratokTable = result.Ds.Tables[0];

        if (ugyiratokTable != null)
        {
            if (ugyiratokTable.Rows.Count > 1 || IsUtoirat)
            {
                UgyiratSzerelesiLista1.SelectedId = SelectedId;
                if (RegiAdat)
                {
                    UgyiratSzerelesiLista1.DataSourceRegiAdat = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBindRegiAdat();
                }
                else
                {
                    UgyiratSzerelesiLista1.DataSource = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBind();
                }
            }
        }
    }

    private int MigralasEve
    {
        get
        {
            int MigralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE);
            if (MigralasEve == 0) MigralasEve = 2008;
            return MigralasEve;
        }
    }

    private bool IsRegiAdatSearch()
    {
        int Ev = evIktatokonyvSearch.EvTol_Integer;
        Logger.Debug("Migrálás Éve: " + MigralasEve);
        if (Ev < MigralasEve)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public string GetKuldemenyKeresesOnClientClick()
    {
        string jsVK = "var element = $get('" + VonalkodTextBoxKuldemeny.TextBox.ClientID + "');if(element && element.value.trim() != '')";
        jsVK += "{" + Page.ClientScript.GetPostBackEventReference(ImageButton_KuldemenyKereses, "") + "; element.disabled = true; element.value='Keresés folyamatban...'; return false;}";
        string OnClientClick = jsVK + JavaScripts.SetOnClientClick("KuldKuldemenyekLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + Kuldemeny_Id_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=1"
            + "&" + QueryStringVars.Filter + "=" + Constants.IktathatoKuldemenyek
          //+ "&" + QueryStringVars.TextBoxId + "=" + FunkcioNev.ClientID
          , Defaults.PopupWidth, Defaults.PopupHeight, ImageButton_KuldemenyKereses.ClientID, EventArgumentConst.refreshKuldemenyekPanel);

        return OnClientClick;
    }

    public string GetElozmenyKeresesOnClientClick()
    {
        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            return "return false;";
        }

        string jsQuery = "var query = ''; if(ev) query = '&" + QueryStringVars.ElozmenySearchParameters.Ev + "=' + ev.value;";
        jsQuery += "if(selectedValue.trim() != '') query += '&" + QueryStringVars.ElozmenySearchParameters.IktatokonyvValue + "=' + selectedValue;";
        jsQuery += "if(foszam && foszam.value != '') query += '&" + QueryStringVars.ElozmenySearchParameters.Foszam + "=' + foszam.value;";

        string js = "var ev = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + "');var iktatokonyv = $get('" + IktatoKonyvekDropDownList_Search.DropDownList.ClientID + "');var index = -1; if(iktatokonyv) index = iktatokonyv.selectedIndex;";
        js += "var selectedValue = ''; if(index > -1) selectedValue = iktatokonyv.options[index].value;var foszam = $get('" + numberFoszamSearch.TextBox.ClientID + "');if(ev && foszam && ev.value.trim() != '' && foszam.value.trim() != '' && selectedValue.trim() != '')";
        js += "{" + Page.ClientScript.GetPostBackEventReference(ImageButton_Elozmeny, "") + ";this.disabled = true;ev.disabled = true;foszam.disabled = true;iktatokonyv.disabled = true;iktatokonyv.options[iktatokonyv.selectedIndex].text = 'Keresés folyamatban...';return false;}";

        string OnClientClick = js + jsQuery + "if(ev && (ev.value== '' && this.id == '" + ImageButton_MigraltKereses.ClientID + "' || !isNaN(parseInt(ev.value)) && parseInt(ev.value) < " + MigralasEve + ")){";
        OnClientClick += GetMigraltOnClientClick("' + query + '");
        OnClientClick += "} else {";
        OnClientClick += GetElozmenyOnClientClick("' + query + '");
        OnClientClick += "}";

        return OnClientClick;
    }

    public string GetMigraltOnClientClick()
    {
        return GetMigraltOnClientClick(String.Empty);
    }

    public string GetMigraltOnClientClick(string dynamicQuery)
    {
        // BUG_13049
        string targymasolas = "&UgyiratTargy=" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID;
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
        {
            targymasolas += "&IratTargy=" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID;
        }
        string OnClientClick = JavaScripts.SetOnClientClick(eMigration.migralasLovListUrl,
             QueryStringVars.HiddenFieldId + "=" + MigraltUgyiratID_HiddenField.ClientID
             + "&" + QueryStringVars.AzonositoTextBox + "=" + regiAzonositoTextBox.ClientID
             + "&" + QueryStringVars.IrattariTetelTextBox + "=" + IrattariTetelTextBox.ClientID
             + "&" + QueryStringVars.MegkulJelzesTextBox + "=" + MegkulJelzesTextBox.ClientID
             + "&Irj=" + IRJ2000TextBox.ClientID
            // BUG_13049
            + targymasolas
             + "&" + QueryStringVars.TextBoxId + "=" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID
             // valamiért nem mûködik, ha közvetlenül a DateTextBoxba írunk
             // (Visible = "true" Style="display: none;" mellett)
             // ezért másik textboxba írással kerüljük meg a problémát
             //+ "&" + QueryStringVars.IrattarbaTextBox + "=" + UgyUgyirat_IrattarbaHelyezes_CalendarControl.TextBox.ClientID
             //+ "&" + QueryStringVars.SkontroVegeTextBox + "=" + UgyUgyirat_SkontroVege_CalendarControl.TextBox.ClientID
             + "&" + QueryStringVars.IrattarbaTextBox + "=" + txtIrattarba.ClientID
             + "&" + QueryStringVars.SkontroVegeTextBox + "=" + txtSkontroVege.ClientID
             + "&" + QueryStringVars.RefreshCallingWindow + "=1"
             + "&" + QueryStringVars.Filter + "=" + Constants.FilterType.Ugyiratok.Szereles
             + dynamicQuery,
             Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, Ugyirat_Panel.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    public string GetElozmenyOnClientClick()
    {
        return GetElozmenyOnClientClick(String.Empty);
    }

    public string GetElozmenyOnClientClick(string dynamicQuery)
    {
        string OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokLovList.aspx"
             , QueryStringVars.HiddenFieldId + "=" + ElozmenyUgyiratID_HiddenField.ClientID
             + "&" + QueryStringVars.RefreshCallingWindow + "=1"
             + "&" + QueryStringVars.Filter + "=" + Constants.AlszamraIktathatoak + dynamicQuery
             , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, ImageButton_Elozmeny.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    #endregion

    protected void FoszamraIktat_ImageButton_Click(object sender, EventArgs e)
    {
        if (Command == CommandName.New)
        {
            // Ügyirat panel beállítása fõszámra iktatáshoz:
            ClearElozmenyUgyirat();

            DisplayElozmenySearchRow();

            ClearElozmenySearch();
        }
    }

    private void DisplayElozmenySearchRow()
    {
        trElozmenySearch.Visible = true;

        ImageButton_Elozmeny.Enabled = true;
        UI.SetImageButtonStyleToEnabled(ImageButton_Elozmeny);
    }

    /// <summary>
    /// Elõzmény keresõ sor elemeit kiüríti
    /// </summary>
    private void ClearElozmenySearch()
    {
        evIktatokonyvSearch.EvTol = String.Empty;
        evIktatokonyvSearch.EvIg = String.Empty;

        IktatoKonyvekDropDownList_Search.SelectedValue = String.Empty;

        numberFoszamSearch.Text = String.Empty;
    }

    #region ISelectableUserComponentContainer Members

    public List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        componentList.Add(IktatasVagyMunkapeldanyLetrehozasRadioButtonList);
        componentList.Add(KuldemenyPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(KuldemenyPanel.Controls));
        componentList.Add(Ugyirat_Panel);
        componentList.AddRange(PageView.GetSelectableChildComponents(Ugyirat_Panel.Controls));
        componentList.Add(IratPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IratPanel.Controls));
        componentList.Add(IratPeldanyPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IratPeldanyPanel.Controls));
        return componentList;
    }

    #endregion

    private string GetDefaultIktatokonyvId()
    {
        List<IktatoKonyvek.IktatokonyvItem> iktatokonyvek;
        IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(this.Session, Constants.IktatoErkezteto.Iktato, true, true, out iktatokonyvek);

        //csak egy iktatókönyv van, akkor nem kell kiválasztani felületen
        if (iktatokonyvek.Count == 1)
        {
            return iktatokonyvek[0].Id;
        }

        return String.Empty;
    }

    private string GetSelectedIktatokonyvId()
    {
        if (!Iktatokonyvek_DropDownLis_Ajax.Visible)
            return GetDefaultIktatokonyvId();

        return Iktatokonyvek_DropDownLis_Ajax.SelectedValue;
    }

    private bool GetIktatokonyvFolyosorszamos(EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatokonyvek)
    {
        bool iktatokonyvFolyosorszamos = false;

        //CERTOP
        //Irattári tétel vizsgálat: Éven túli iktatás
        EREC_IraIrattariTetelekService svcIrattariTetelek = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        ExecParam xpmIrattariTetel = UI.SetExecParamDefault(Page);
        xpmIrattariTetel.Record_Id = erec_UgyUgyiratok.IraIrattariTetel_Id;
        Result resIrattariTetel = svcIrattariTetelek.Get(xpmIrattariTetel);

        if (!resIrattariTetel.IsError)
        {
            EREC_IraIrattariTetelek irattariTetel = resIrattariTetel.Record as EREC_IraIrattariTetelek;

            if (irattariTetel != null && "1".Equals(irattariTetel.EvenTuliIktatas))
            {
                iktatokonyvFolyosorszamos = true;
            }
        }

        if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatokonyvek) == true)
        {
            if (erec_IraIktatokonyvek.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos)
            {
                iktatokonyvFolyosorszamos = true;
            }
        }

        return iktatokonyvFolyosorszamos;
    }

    private void CopyTargy()
    {
        //LZS - BLG_335 
        //Mode == CommandName.BelsoIratIktatas is hozzáadva
        if (Command == CommandName.New &&
              (Mode == CommandName.BelsoIratIktatas || Mode == CommandName.BejovoIratIktatas || Mode == CommandName.KimenoEmailIktatas))
        {
            if (!UgyUgyirat_Targy_RequiredTextBox.ReadOnly)
            {
                UgyUgyirat_Targy_RequiredTextBox.TextBox.Attributes["onfocusout"] =
                    @" var iratTargyObj = $get('" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID + @"');
            if (iratTargyObj.value == '' || iratTargyObj.value == this.lastCopy) 
            {   
                this.lastCopy = this.value;               
                iratTargyObj.value = this.value;
            }  ";
            }
            else
            {
                UgyUgyirat_Targy_RequiredTextBox.TextBox.Attributes["onfocusout"] = "";
            }
        }
    }

    #region tipusos tárgyszavak

    private void IraIrat_Irattipus_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTipusosObjektumTargyszavaiPanel_Iratok(ParentId);
    }
    protected void FillTipusosObjektumTargyszavaiPanel_Iratok(String id)
    {
        TipusosTargyszavakPanel_Iratok.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_Iratok.FillObjektumTargyszavai(search, id, null
             , Constants.TableNames.EREC_IraIratok, "Irattipus", new string[] { IraIrat_Irattipus_KodtarakDropDownList.SelectedValue }, false
             , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_Iratok.Visible = (otpTipusosTargyszavak_Iratok.Count > 0 || otpTipusosTargyszavak_IratokPostazasIranya.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_Iratok.Count > 0)
            {
                otpTipusosTargyszavak_Iratok.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpTipusosTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_Iratok.ReadOnly = true;
            otpTipusosTargyszavak_Iratok.ViewMode = true;
        }
    }

    Result Save_TipusosObjektumTargyszavai_Iratok(string iratid)
    {
        List<EREC_ObjektumTargyszavai> EREC_ObjektumTargyszavaiList = new List<EREC_ObjektumTargyszavai>();

        EREC_ObjektumTargyszavai[] tipusosTargyszavak = otpTipusosTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);
        EREC_ObjektumTargyszavai[] tipusosTargyszavaPostazasIranya = otpTipusosTargyszavak_IratokPostazasIranya.GetEREC_ObjektumTargyszavaiList(true);

        if (tipusosTargyszavak != null)
        {
            EREC_ObjektumTargyszavaiList.AddRange(tipusosTargyszavak);
        }
        if (tipusosTargyszavaPostazasIranya != null)
        {
            EREC_ObjektumTargyszavaiList.AddRange(tipusosTargyszavaPostazasIranya);
        }
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList.Count > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                        , EREC_ObjektumTargyszavaiList.ToArray()
                        , iratid
                        , null
                        , Constants.TableNames.EREC_IraIratok
                        , "*"
                        , null
                        , false
                        , null
                        , false
                        , false
                        );
        }

        return result_ot;
    }

    protected void FillTipusosObjektumTargyszavaiPanel_IratokPostazasIranya(String id, string postazasIranya)
    {
        TipusosTargyszavakPanel_Iratok.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_IratokPostazasIranya.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_IraIratok, "PostazasIranya", new string[] { postazasIranya }, false
            , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_Iratok.Visible = (otpTipusosTargyszavak_Iratok.Count > 0 || otpTipusosTargyszavak_IratokPostazasIranya.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_IratokPostazasIranya.Count > 0)
            {
                otpTipusosTargyszavak_IratokPostazasIranya.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_IratokPostazasIranya.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpTipusosTargyszavak_IratokPostazasIranya.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_IratokPostazasIranya.ReadOnly = true;
            otpTipusosTargyszavak_IratokPostazasIranya.ViewMode = true;
        }

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            otpTipusosTargyszavak_IratokPostazasIranya.Visible = false;
            otpTipusosTargyszavak_IratokPostazasIranya.Enabled = false;
        }
    }

    #endregion

    #region TÜK-ös mûködés

    /// <summary>
    /// Beallitja a TÜK-ös esethez tartozó mezõk láthatóságát BLG#1402
    /// </summary>
    private void SetTUKFieldsVisibility()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        bool isBelsoIratIktatas = this.Mode == CommandName.BelsoIratIktatas;
        bool IsBejovoIktatas = this.Mode == CommandName.BejovoIratIktatas;
        bool isNewCommand = this.Command == CommandName.New;
        bool isModifyCommand = this.Command == CommandName.Modify;
        bool isViewCommand = this.Command == CommandName.View;

        // Sor elrejtése:
        // Ha iktatás van (New), akkor csak a belsõ irat iktatásnál legyen kint:
        trTUKMinositoEsMinositesErvIdo.Visible = isTUK && (!isNewCommand || isBelsoIratIktatas || IsBejovoIktatas);
        CalendarControlMinositesErvenyessegIdeje.Visible = isTUK && (!isNewCommand || isBelsoIratIktatas || IsBejovoIktatas);
        MinositoPartnerControlIraIratokFormTab.Visible = isTUK && (!isNewCommand || isBelsoIratIktatas || IsBejovoIktatas);

        trTerjedelemMainPanel.Visible = isTUK && (!isNewCommand || isBelsoIratIktatas);
        TerjedelemPanelA.Visible = isTUK && (!isNewCommand || isBelsoIratIktatas);

        // Mellékletek csak iktatáskor kell, megtekintés/módosításkor van neki külön tablap
        trMellekletekPanel.Visible = isTUK && isBelsoIratIktatas;
        mellekletekPanel.Visible = isTUK && isBelsoIratIktatas;

        MinositoPartnerControlIraIratokFormTab.Validate = false;
        MinositoPartnerControlIraIratokFormTab.ValidationGroup = null;

        //LZS - BUG_7248
        lblUgyindito.Text = Resources.Form.UI_Ugyfel_Ugyindito_Szervezet;
    }

    #endregion

    #region TOBB IRAT PELDANY
    private void InitTobbszorosIratPeldany()
    {
        if (Mode != CommandName.BelsoIratIktatas)
            return;
        if (!Page.IsPostBack)
            if (KellTobbszorosIratPeldany())
            {
                UgyiratpeldanySzukseges_CheckBox.Visible = false;
                PanelTobbszorosIratPeldany.Visible = true;

                InitTobbSzorosPldIrat();
            }
            else
            {
                UgyiratpeldanySzukseges_CheckBox.Visible = true;
                PanelTobbszorosIratPeldany.Visible = false;
            }
        else
        {
            if (KellTobbszorosIratPeldany())
            {
                InitTobbSzorosPldIrat();
            }
        }
    }
    private bool KellTobbszorosIratPeldany()
    {
        int isTobbes = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()),
            Rendszerparameterek.TOBBES_IRATPELDANY_IKTATASKOR, 1);
        if (isTobbes == 1)
            return true;
        return false;
    }
    private void InitTobbSzorosPldIrat()
    {
        PldIratIktatasPanelUserControl_2.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_3.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_4.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_5.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_6.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_7.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_8.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_9.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_10.OnClose += PldIratIktatasPanelUserControl_OnClose;
        PldIratIktatasPanelUserControl_11.OnClose += PldIratIktatasPanelUserControl_OnClose;


    }

    // BUG_11079
    private void TobbesPldFuggoKodtarAssign()
    {
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_2.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_3.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_4.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_5.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_6.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_7.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_8.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_10.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, PldIratIktatasPanelUserControl_11.Pld_KuldesModDropDown_UniqueID, Pld_IratpeldanyFajtaja_KodtarakDropDownList.DropDownList.ClientID);
    }

    private void PldIratIktatasPanelUserControl_OnClose(string sorszam, eRecordComponent_PldIratIktatasPanelUserControl self)
    {
        switch (sorszam)
        {
            case "2":
                PldIratIktatasPanelUserControl_2.Visible = false;
                PldIratIktatasPanelUserControl_2.Reset();
                break;
            case "3":
                PldIratIktatasPanelUserControl_3.Visible = false;
                PldIratIktatasPanelUserControl_3.Reset();
                break;
            case "4":
                PldIratIktatasPanelUserControl_4.Visible = false;
                PldIratIktatasPanelUserControl_4.Reset();
                break;
            case "5":
                PldIratIktatasPanelUserControl_5.Visible = false;
                PldIratIktatasPanelUserControl_5.Reset();
                break;
            case "6":
                PldIratIktatasPanelUserControl_6.Visible = false;
                PldIratIktatasPanelUserControl_6.Reset();
                break;
            case "7":
                PldIratIktatasPanelUserControl_7.Visible = false;
                PldIratIktatasPanelUserControl_7.Reset();
                break;
            case "8":
                PldIratIktatasPanelUserControl_8.Visible = false;
                PldIratIktatasPanelUserControl_8.Reset();
                break;
            case "9":
                PldIratIktatasPanelUserControl_9.Visible = false;
                PldIratIktatasPanelUserControl_9.Reset();
                break;
            case "10":
                PldIratIktatasPanelUserControl_10.Visible = false;
                PldIratIktatasPanelUserControl_10.Reset();
                break;
            case "11":
                PldIratIktatasPanelUserControl_11.Visible = false;
                PldIratIktatasPanelUserControl_11.Reset();
                break;
            default:
                break;
        }
    }

    protected void ImageButtonTobbszorosIratPeldany_Click(object sender, ImageClickEventArgs e)
    {
        int sorszam = 2;
        if (int.TryParse(HiddenFieldTobbszorosIratPeldanyUtolsoSorszam.Value, out sorszam))
        {
            ++sorszam;
        }
        else
        {
            sorszam = 2;
        }

        if (!PldIratIktatasPanelUserControl_2.Visible)
            PldIratIktatasPanelUserControl_2.Visible = true;
        else if (!PldIratIktatasPanelUserControl_3.Visible)
            PldIratIktatasPanelUserControl_3.Visible = true;
        else if (!PldIratIktatasPanelUserControl_4.Visible)
            PldIratIktatasPanelUserControl_4.Visible = true;
        else if (!PldIratIktatasPanelUserControl_5.Visible)
            PldIratIktatasPanelUserControl_5.Visible = true;
        else if (!PldIratIktatasPanelUserControl_6.Visible)
            PldIratIktatasPanelUserControl_6.Visible = true;
        else if (!PldIratIktatasPanelUserControl_7.Visible)
            PldIratIktatasPanelUserControl_7.Visible = true;
        else if (!PldIratIktatasPanelUserControl_8.Visible)
            PldIratIktatasPanelUserControl_8.Visible = true;
        else if (!PldIratIktatasPanelUserControl_9.Visible)
            PldIratIktatasPanelUserControl_9.Visible = true;
        else if (!PldIratIktatasPanelUserControl_10.Visible)
            PldIratIktatasPanelUserControl_10.Visible = true;
        else if (!PldIratIktatasPanelUserControl_11.Visible)
            PldIratIktatasPanelUserControl_11.Visible = true;

        HiddenFieldTobbszorosIratPeldanyUtolsoSorszam.Value = sorszam.ToString();
    }

    private string GetLastPldIratUCtrlName()
    {
        return "PldIratIktatasPanelUserControl" + HiddenFieldTobbszorosIratPeldanyUtolsoSorszam.Value;
    }
    private List<EREC_PldIratPeldanyok> GetTobbSzorosIratPeldanyok()
    {
        if (!KellTobbszorosIratPeldany())
            return null;

        List<EREC_PldIratPeldanyok> result = new List<EREC_PldIratPeldanyok>();
        EREC_PldIratPeldanyok tmp = null;
        if ((PldIratIktatasPanelUserControl_2.Visible && !PldIratIktatasPanelUserControl_2.IsValid())
            ||
            (PldIratIktatasPanelUserControl_3.Visible && !PldIratIktatasPanelUserControl_3.IsValid())
            ||
            (PldIratIktatasPanelUserControl_4.Visible && !PldIratIktatasPanelUserControl_4.IsValid())
            ||
            (PldIratIktatasPanelUserControl_5.Visible && !PldIratIktatasPanelUserControl_5.IsValid())
            ||
            (PldIratIktatasPanelUserControl_6.Visible && !PldIratIktatasPanelUserControl_6.IsValid())
            ||
            (PldIratIktatasPanelUserControl_7.Visible && !PldIratIktatasPanelUserControl_7.IsValid())
            ||
            (PldIratIktatasPanelUserControl_8.Visible && !PldIratIktatasPanelUserControl_8.IsValid())
            ||
            (PldIratIktatasPanelUserControl_9.Visible && !PldIratIktatasPanelUserControl_9.IsValid())
            ||
            (PldIratIktatasPanelUserControl_10.Visible && !PldIratIktatasPanelUserControl_10.IsValid())
            ||
            (PldIratIktatasPanelUserControl_11.Visible && !PldIratIktatasPanelUserControl_11.IsValid())
            )
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageIratPeldanyParametersError);
            ErrorUpdatePanel1.Update();
            throw new Exception(Resources.Error.MessageIratPeldanyParametersError);
        }
        int ujraSorSzamoz = 1;
        if (PldIratIktatasPanelUserControl_2.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_2.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_3.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_3.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_4.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_4.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_5.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_5.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_6.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_6.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_7.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_7.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_8.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_8.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_9.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_9.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_10.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_10.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }
        if (PldIratIktatasPanelUserControl_11.Visible)
        {
            tmp = PldIratIktatasPanelUserControl_11.GetBO();
            tmp.Sorszam = ujraSorSzamoz.ToString();
            result.Add(tmp);
            ++ujraSorSzamoz;
        }

        return result;
    }
    /// <summary>
    /// Iat példányok lista összeállítása és újrasorszámozása.
    /// </summary>
    /// <param name="pldIratPeldanyTobbSzoros"></param>
    /// <param name="pldIratPeldanyAlap"></param>
    /// <returns></returns>
    private void SetIratPeldanyokTobbesLista(ref List<EREC_PldIratPeldanyok> pldIratPeldanyTobbSzoros, EREC_PldIratPeldanyok pldIratPeldanyAlap)
    {
        pldIratPeldanyTobbSzoros.Insert(0, pldIratPeldanyAlap);
        for (int i = 0; i < pldIratPeldanyTobbSzoros.Count; i++)
        {
            pldIratPeldanyTobbSzoros[i].Sorszam = (i + 1).ToString();
        }
    }
    #endregion

    bool PeldanyokSorszamaMegadhato
    {
        get
        {
            return Rendszerparameterek.IsTUK(Page) && Mode == CommandName.BejovoIratIktatas;
        }
    }


    #region SAKKORA
    private const string FelfuggesztesOkaSpecItemContains = "EGYEB_OK";
    private const string FelfuggesztesOkaCustomtextValue = "Egyéb ok";

    private bool StartIrataHatasaProcedure()
    {
        if (obj_Irat == null)
        {
            SetObjIrat();
        }
        if (obj_Irat == null)
            return true;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        bool kellElteltIdoUjraSzamolas = false;
        Contentum.eUtility.Sakkora.SakkoraKezeles(execParam, obj_Irat, obj_Ugyirat, out kellElteltIdoUjraSzamolas);
        //if (kellElteltIdoUjraSzamolas && !string.IsNullOrEmpty(obj_Irat.Ugyirat_Id))
        //{
        //    SakkoraService sakkSvc = eRecordService.ServiceFactory.Get_SakkoraService();
        //    sakkSvc.SakkoraUgyintezesiIdoUjraSzamolas(execParam.Clone(), obj_Irat.Ugyirat_Id);
        //}
        return true;
    }

    #region BO
    /// <summary>
    /// LoadIratHatasaComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadIratHatasaComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        try
        {
            #region BLG_1051
            if (!string.IsNullOrEmpty(erec_IraIratok.IratHatasaUgyintezesre))
                IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue = erec_IraIratok.IratHatasaUgyintezesre;
            if (!string.IsNullOrEmpty(erec_IraIratok.FelfuggesztesOka))
            {
                string[] splitted = erec_IraIratok.FelfuggesztesOka.Split(new string[] { "|" }, StringSplitOptions.None);
                if (splitted != null && splitted.Length == 3)
                {
                    if (!string.IsNullOrEmpty(splitted[0]))
                        KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue = splitted[0];
                    if (!string.IsNullOrEmpty(splitted[2]))
                        TextBoxFelfuggesztesOka.Text = splitted[2];
                }
            }
            #endregion BLG_1051
            TextBox_IratHatasaUgyintezesre_Aktualis.Text = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.IRAT_HATASA_UGYINTEZESRE.KCS, erec_IraIratok.IratHatasaUgyintezesre, UI.SetExecParamDefault(Page, new ExecParam()), HttpContext.Current.Cache);
        }
        catch (Exception exc)
        {
            Logger.Error("LoadIratHatasaComponentsFromBo", exc);
        }
    }

    /// <summary>
    /// LoadIrathatasaBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadIratHatasaBoFromComponents(EREC_IraIratok erec_IraIratok)
    {
        #region BLG_1051
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        {
            erec_IraIratok.IratHatasaUgyintezesre = IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue;
            erec_IraIratok.Updated.IratHatasaUgyintezesre = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);

            erec_IraIratok.FelfuggesztesOka = string.Format("{0}|{1}|{2}", KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue, KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedItem.Text, TextBoxFelfuggesztesOka.Text);
            erec_IraIratok.Updated.FelfuggesztesOka = pageView.GetUpdatedByView(KodtarakDropDownListUgyiratFelfuggesztesOka);
        }
        #endregion BLG_1051
    }

    /// <summary>
    /// LoadLezarasOkaComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadLezarasOkaComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        if (!string.IsNullOrEmpty(erec_IraIratok.LezarasOka))
            KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue = erec_IraIratok.LezarasOka;
    }
    /// <summary>
    /// LoadLezarasOkaBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadLezarasOkaBoFromComponents(EREC_IraIratok erec_IraIratok)
    {
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        {
            erec_IraIratok.LezarasOka = KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue;
            erec_IraIratok.Updated.LezarasOka = pageView.GetUpdatedByView(KodtarakDropDownListUgyiratLezarasOka);
        }
    }
    #endregion

    #region CTRL
    /// <summary>
    /// Beállítja az irat Hatása lenyíló listát
    /// </summary>
    private void InitializeIratHatasaDDL(bool addIratHatasaElemMindenkepp)
    {
        GetAndSetUgyiratObject();
        GetAndSetIratObject();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (Contentum.eUtility.Sakkora.SakkoraModosithato(execParam, null, obj_Ugyirat))
        {
            if (Command == CommandName.View)
            {
                IratHatasaUgyintezesre_KodtarakDropDownList.Enabled = false;
                IratHatasaUgyintezesre_KodtarakDropDownList.ReadOnly = true;
            }
            else
            {
                IratHatasaUgyintezesre_KodtarakDropDownList.Enabled = true;
                IratHatasaUgyintezesre_KodtarakDropDownList.ReadOnly = false;
            }
            Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre actualType = Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre.NOTHING;
            List<string> toStates = new List<string>();

            toStates = Command == CommandName.View
                ? Contentum.eUtility.Sakkora.SakkoraAllapotok(execParam, obj_Ugyirat, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue, true, out actualType)
                : Contentum.eUtility.Sakkora.SakkoraAllapotok(execParam, obj_Ugyirat, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue, false, out actualType);

            // BUG_3435
            if (toStates != null && toStates.Count >= 0)
                IratHatasaUgyintezesre_KodtarakDropDownList.FillDropDownList(kcs_IRAT_HATASA_UGYINTEZESRE, toStates, false, EErrorPanel1);

            // BUG_3435
            if (obj_Irat != null && !string.IsNullOrEmpty(obj_Irat.IratHatasaUgyintezesre))
            {
                if (!toStates.Contains(obj_Irat.IratHatasaUgyintezesre))
                {
                    if (addIratHatasaElemMindenkepp)
                    {
                        KRT_KodTarak kodtar = Contentum.eUtility.Sakkora.GetIratHatasaItem(execParam.Clone(), obj_Irat.IratHatasaUgyintezesre);
                        if (kodtar != null)
                        {
                            IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.Items.Add(new ListItem(kodtar.Nev, kodtar.Kod));
                            IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue = obj_Irat.IratHatasaUgyintezesre;
                        }
                    }
                }
                else
                {
                    IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue = obj_Irat.IratHatasaUgyintezesre;
                }
            }

        }
        else
        {
            IratHatasaUgyintezesre_KodtarakDropDownList.Enabled = false;
            IratHatasaUgyintezesre_KodtarakDropDownList.ReadOnly = true;
            if (obj_Ugyirat != null && !string.IsNullOrEmpty(obj_Ugyirat.SakkoraAllapot))
            {
                KRT_KodTarak kodtar = Contentum.eUtility.Sakkora.GetIratHatasaItem(execParam.Clone(), obj_Ugyirat.SakkoraAllapot);
                IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.Items.Add(new ListItem(kodtar.Nev));
            }
            else
            {
                IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.Items.Add(new ListItem("Nincs beállított sakkóra állapot "));
            }
        }
    }
    /// <summary>
    /// Megkeresi az ugyirat objektumot
    /// </summary>
    private void GetAndSetUgyiratObject()
    {
        #region GET UGYIRAT OBJ
        if (obj_Ugyirat == null)
        {
            SetObjUgyirat();
            if (obj_Ugyirat == null)
            {
                string ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
                if (string.IsNullOrEmpty(ugyiratId))
                {
                    string iratId = Request.QueryString.Get(QueryStringVars.IratId);
                    if (!string.IsNullOrEmpty(iratId))
                    {
                        ExecParam execParamFind = UI.SetExecParamDefault(Page, new ExecParam());
                        EREC_IraIratok irat = Contentum.eUtility.Sakkora.GetIrat(execParamFind, iratId);
                        if (irat != null)
                        {
                            obj_Ugyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParamFind, irat.Ugyirat_Id);
                        }
                    }
                }
                else
                {
                    ExecParam execParamFind = UI.SetExecParamDefault(Page, new ExecParam());
                    obj_Ugyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParamFind, ugyiratId);
                }
            }
        }
        #endregion GET UGYIRAT OBJ
    }
    private void GetAndSetIratObject()
    {
        #region GET IRAT OBJ
        if (obj_Irat == null)
        {
            string iratId;
            if (Mode == CommandName.AtIktatas)
            {
                iratId = Request.QueryString.Get(QueryStringVars.IratId);
            }
            else
            {
                iratId = Request.QueryString.Get(QueryStringVars.Id);
            }
            if (!string.IsNullOrEmpty(iratId))
            {
                ExecParam execParamFind = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_IraIratok irat = Contentum.eUtility.Sakkora.GetIrat(execParamFind, iratId);
                if (irat != null)
                    obj_Irat = irat;
            }
        }
        #endregion GET UGYIRAT OBJ
    }
    #endregion

    #region HELPER
    private void ShowErrorOnPanel(string message)
    {
        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, message);
        ErrorUpdatePanel1.Update();
    }

    private void ShowErrorOnPanel(Result result)
    {
        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        ErrorUpdatePanel1.Update();
    }

    #region FELFUGGESZTES OKA
    public void ShowFelfuggesztesOka()
    {
        if (IsIratHatasaPauseStrict())
            SetVisibilityFelfuggesztesOka(true);
        else
            SetVisibilityFelfuggesztesOka(false);
    }
    public void HideFelfuggesztesOka()
    {
        SetVisibilityFelfuggesztesOka(false);
    }
    /// <summary>
    /// Visszaadja a felfüggesztés okát.
    /// </summary>
    /// <returns></returns>
    private string GetFelfuggesztesOka()
    {
        if (KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue != null &&
                   KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            return string.IsNullOrEmpty(TextBoxFelfuggesztesOka.Text) ? FelfuggesztesOkaCustomtextValue : TextBoxFelfuggesztesOka.Text;
        else
            return KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedItem.Text;
    }
    /// <summary>
    /// Felfüggesztés oka kontrolok láthatósága
    /// </summary>
    /// <param name="visible"></param>
    private void SetVisibilityFelfuggesztesOka(bool visible)
    {
        LabelEljarasFefuggesztesenekOka.Visible = visible;
        KodtarakDropDownListUgyiratFelfuggesztesOka.Visible = visible;
        KodtarakDropDownListUgyiratFelfuggesztesOka.Enabled = visible;

        if (visible)
        {
            if (KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue != null &&
                    KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            {
                TextBoxFelfuggesztesOka.Visible = true;
            }
            else
            {
                TextBoxFelfuggesztesOka.Visible = false;
            }
        }
        else
        {
            TextBoxFelfuggesztesOka.Visible = false;
        }
    }
    #endregion

    #region LEZARAS OKA
    public void ShowLezarasOka()
    {
        if (IsIratHatasaStop())
            SetVisibilityLezarasOka(true);
        else
            SetVisibilityLezarasOka(false);
    }
    public void HideLezarasOka()
    {
        SetVisibilityLezarasOka(false);
    }

    /// <summary>
    /// GetLezarasOka
    /// </summary>
    /// <returns></returns>
    private string GetLezarasOka()
    {
        if (KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue != null &&
                   KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            return FelfuggesztesOkaCustomtextValue;
        else
            return KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedItem.Text;
    }
    /// <summary>
    /// Lezárás oka kontrolok láthatósága
    /// </summary>
    /// <param name="visible"></param>
    private void SetVisibilityLezarasOka(bool visible)
    {
        LabelEljarasLezarasOka.Visible = visible;
        KodtarakDropDownListUgyiratLezarasOka.Visible = visible;
    }
    /// <summary>
    /// FillLezarasOkaDDL
    /// </summary>
    private void InitializeLezarasOkaDDL(string inIratHatasa, string lezarasOka)
    {
        string iratHatasValue;
        if (string.IsNullOrEmpty(inIratHatasa))
            iratHatasValue = IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue;
        else
            iratHatasValue = inIratHatasa;

        if (string.IsNullOrEmpty(iratHatasValue) || Command == CommandName.View)
            KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, true, EErrorPanel1);
        else
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            string[] lezarasOkai = Contentum.eUtility.Sakkora.SakkoraLezarasOkai(execParam, iratHatasValue);
            if (lezarasOkai == null || lezarasOkai.Length < 1)
                KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, true, EErrorPanel1);
            else if (string.IsNullOrEmpty(KodtarakDropDownListUgyiratLezarasOka.SelectedValue))
            {
                List<string> filterList = new List<string>();
                filterList.AddRange(lezarasOkai);
                KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, filterList, false, EErrorPanel1);
            }
        }
        if (!string.IsNullOrEmpty(lezarasOka))
            KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue = lezarasOka;
    }
    #endregion

    private void SetVisibilityIratHatasa(bool visible)
    {
        LabelIratHatasaUgyintezesre.Visible = visible;
        IratHatasaUgyintezesre_KodtarakDropDownList.Visible = visible;
    }
    #endregion HELPER

    #region CHECKS

    #region IRAT HATAS
    /// <summary>
    /// Irat hatása felfüggesztés?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaPause()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaPause(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    /// <summary>
    /// Irat hatása felfüggesztés?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaPauseStrict()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaPauseStrict(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    /// <summary>
    /// Irat hatása indítás?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaStart()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaStart(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    /// <summary>
    /// Irat hatása lezárt?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaStop()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaStop(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    #endregion IRAT HATAS

    #endregion CHECKS

    /// <summary>
    /// Irat obj beállítása
    /// </summary>
    private void SetObjIrat()
    {
        if (ParentId == null)
            return;
        if (obj_Irat == null)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = ParentId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                obj_Irat = erec_IraIratok;

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
    }
    /// <summary>
    /// Ugyirat obj beállítása
    /// </summary>
    private void SetObjUgyirat()
    {
        if (!string.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            LoadUgyirat(ElozmenyUgyiratID_HiddenField.Value);
            return;
        }

        if (obj_Irat == null)
        {
            SetObjIrat();
            if (obj_Irat == null)
                return;
        }

        if (obj_Ugyirat == null && !String.IsNullOrEmpty(obj_Irat.Ugyirat_Id))
        {
            LoadUgyirat(obj_Irat.Ugyirat_Id);
        }
    }
    /// <summary>
    /// LoadUgyirat
    /// </summary>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private bool LoadUgyirat(string ugyiratId)
    {
        if (string.IsNullOrEmpty(ugyiratId))
            return false;

        // ügyirat lekérése és betöltése
        EREC_UgyUgyiratokService erec_UgyUgyiratokService =
              eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page, new ExecParam());
        execParam_ugyiratGet.Record_Id = ugyiratId;

        Result result_UgyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);

        if (result_UgyiratGet.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_UgyiratGet);
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
            return false;
        }

        obj_Ugyirat = (EREC_UgyUgyiratok)result_UgyiratGet.Record;
        return true;
    }

    /// <summary>
    /// Ugyintezes kezdete
    /// </summary>
    /// <returns></returns>
    private DateTime GetUgyintezesKezdete()
    {
        DateTime dtUgyintezesKezdete = DateTime.Now;
        if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page)))
        {
            dtUgyintezesKezdete = DateTime.Parse(Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(Contentum.eUtility.UI.SetExecParamDefault(Page)));
        }
        else
        {
            if (!String.IsNullOrEmpty(Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value))
            {
                DateTime.TryParse(Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value, out dtUgyintezesKezdete);
            }
        }
        return dtUgyintezesKezdete;
    }

    private void LoadUgyintezesiNapokFromBo(EREC_IraIratok erec_IraIratok)
    {
        inputUgyintezesiNapok.Value = erec_IraIratok.IntezesiIdo;
    }
    private void LoadUgyintezesiNapokBoFromComponents(EREC_IraIratok erec_IraIratok)
    {
        CheckUgyintezesiNapok();
        erec_IraIratok.IntezesiIdo = inputUgyintezesiNapok.Value;
        erec_IraIratok.Updated.IntezesiIdo = true;
    }
    private bool CheckUgyintezesiNapok()
    {
        if (string.IsNullOrEmpty(inputUgyintezesiNapok.Value))
            return true;

        int value;
        if (int.TryParse(inputUgyintezesiNapok.Value, out value))
        {
            if (value < 0)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageUgyintzesiIdoNegativ);
                ErrorUpdatePanel1.Update();
                throw new Exception(Resources.Error.MessageUgyintzesiIdoNegativ);
            }
        }
        return true;
    }
    /// <summary>
    /// InitUgyintezesiNapok
    /// </summary>
    /// <param name="execParam"></param>
    public void InitUgyintezesiNapok(ExecParam execParam)
    {

        //LZS - BUG_7043
        string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
              Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYIRAT_INTEZESI_IDO, Page, null);

        var builder = new System.Text.StringBuilder();
        string kodtarKod;
        string kodtarNev;
        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
        {
            kodtarKod = kte.Kod; // Key a kódtár kód
            kodtarNev = kte.Nev; // Value a kódtárérték neve
            builder.Append(String.Format("<option value='{0}'>", kodtarKod));
        }
        datalistUgyintezesiNapok.InnerHtml = builder.ToString();
        inputUgyintezesiNapok.Attributes["list"] = datalistUgyintezesiNapok.ClientID;

    }

    /// <summary>
    /// LoadEljarasiSzakaszFokComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadEljarasiSzakaszFokComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        if (!string.IsNullOrEmpty(erec_IraIratok.EljarasiSzakasz))
            KodtarakDropDownList_EljarasiSzakaszFok.DropDownList.SelectedValue = erec_IraIratok.EljarasiSzakasz;
    }

    /// <summary>
    /// LoadEljarasiSzakaszFokBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadEljarasiSzakaszFokBoFromComponents(EREC_IraIratok erec_IraIratok)
    {
        erec_IraIratok.EljarasiSzakasz = KodtarakDropDownList_EljarasiSzakaszFok.DropDownList.SelectedValue;
        erec_IraIratok.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(KodtarakDropDownList_EljarasiSzakaszFok);

        //if (KodtarakDropDownList_EljarasiSzakaszFok.Enabled && string.IsNullOrEmpty(erec_IraIratok.EljarasiSzakasz))
        //{
        //    Contentum.eUtility.Sakkora.SetIratEljarasiSzakaszDefault(Contentum.eUtility.UI.SetExecParamDefault(Page), ref erec_IraIratok, UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);
        //}
    }

    /// <summary>
    /// Check UgyFajta: hatósági -> Eljarasi szakasz
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <param name="eljarasiSzakaszFokKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaEsEljarasiSzakasz(ExecParam execParam, string ugyFajtaKodja, string eljarasiSzakaszFokKodja)
    {
        bool isHatosagi = CheckUgyFajtaIsHatosagi(execParam, ugyFajtaKodja);

        if (isHatosagi && string.IsNullOrEmpty(eljarasiSzakaszFokKodja))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageEljarasiSzakaszFokRequired);
            ErrorUpdatePanel1.Update();
            return false;
            // throw new Exception(Resources.Error.MessageEljarasiSzakaszFokRequired);
        }
        return true;
    }
    /// <summary>
    /// CheckUgyFajtaSakkoraKotelezo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaSakkoraKotelezoseg(ExecParam execParam, string ugyFajtaKodja)
    {
        if (!Rendszerparameterek.UseSakkora(execParam))
        { return true; }

        bool isHatosagi = CheckUgyFajtaIsHatosagi(execParam, ugyFajtaKodja);
        if (isHatosagi)
        {
            if (string.IsNullOrEmpty(IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue))
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageHatosagiUgySakkoraKotelezo);
                ErrorUpdatePanel1.Update();
                return false;
            }
        }
        return true;
    }
    /// <summary>
    /// CheckUgyFajtaIsHatosagi
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaIsHatosagi(ExecParam execParam, string ugyFajtaKodja)
    {
        return Contentum.eUtility.Sakkora.IsUgyFajtaHatosagi(execParam.Clone(), ugyFajtaKodja);
    }
    /// <summary>
    /// GetKuldemenyById
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private EREC_KuldKuldemenyek GetKuldemenyById(string id)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = new Result();
        execParam.Record_Id = id;
        result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
            return null;
        return (EREC_KuldKuldemenyek)result.Record;
    }
    /// <summary>
    /// SetBejovoIktatasUgyintezesKezdoDatuma
    /// </summary>
    /// <param name="erec_IraIrat"></param>
    /// <param name="kuldemeny"></param>
    private void SetBejovoIktatasUgyintezesKezdoDatuma(EREC_IraIratok erec_IraIrat, EREC_KuldKuldemenyek kuldemeny)
    {
        DateTime newUgyintezesKezdoDatuma;
        DateTime.TryParse(kuldemeny.BeerkezesIdeje, out newUgyintezesKezdoDatuma);
        if (newUgyintezesKezdoDatuma == null)
            newUgyintezesKezdoDatuma = DateTime.Now;
        newUgyintezesKezdoDatuma = newUgyintezesKezdoDatuma.AddHours(1);
        newUgyintezesKezdoDatuma = new DateTime(newUgyintezesKezdoDatuma.Year, newUgyintezesKezdoDatuma.Month, newUgyintezesKezdoDatuma.Day, newUgyintezesKezdoDatuma.Hour, 0, 0);

        erec_IraIrat.UgyintezesKezdoDatuma = newUgyintezesKezdoDatuma.ToString();
        erec_IraIrat.Updated.UgyintezesKezdoDatuma = true;
    }
    /// <summary>
    /// SetBelsoIktatasUgyintezesKezdoDatuma
    /// </summary>
    /// <param name="erec_IraIrat"></param>
    private void SetBelsoIktatasUgyintezesKezdoDatuma(EREC_IraIratok erec_IraIrat)
    {
        if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
        {
            erec_IraIrat.UgyintezesKezdoDatuma = DateTime.Now.ToString();
            erec_IraIrat.Updated.UgyintezesKezdoDatuma = true;
        }
        else if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            ///POSTAZAS LESZ MAJD
            erec_IraIrat.UgyintezesKezdoDatuma = DateTime.Now.ToString();
            erec_IraIrat.Updated.UgyintezesKezdoDatuma = true;
        }
    }
    #endregion

    protected void KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitializeIratHatasaDDL(true);
    }

    /// <summary>
    /// SetElektronikusIratVonalkodControl
    /// </summary>
    private void SetElektronikusIratVonalkodControl()
    {
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
                && Pld_IratpeldanyFajtaja_KodtarakDropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
        {
            Label_Pld_VonalkodElottCsillag.Visible = false;
            Pld_BarCode_VonalKodTextBox.RequiredValidate = false;
        }
    }

    //protected void AdathordozoTipusa_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    SetElektronikusIratVonalkodControl();
    //}

    protected void Pld_IratpeldanyFajtaja_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetElektronikusIratVonalkodControl();

        // Az iratpéldány fajtája lesz az irat adathordozó típusa:
        AdathordozoTipusa_KodtarakDropDownList.SelectedValue = Pld_IratpeldanyFajtaja_KodtarakDropDownList.SelectedValue;
    }

    void LoadIratfordulatAdatok(string iratId)
    {
        ExecParam xpm = UI.SetExecParamDefault(Page);
        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        xpm.Record_Id = iratId;
        Result iratokResult = iratokService.Get(xpm);

        if (iratokResult.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, iratokResult);
            ErrorUpdatePanel1.Update();
            return;
        }

        EREC_IraIratok erec_IraIratok = iratokResult.Record as EREC_IraIratok;

        IraIrat_Targy_RequiredTextBox.Text = String.Format("Válasz: {0}", erec_IraIratok.Targy);

        // BUG_8765
        if (!IsTUK_SZIGNALAS_ENABLED)
        {
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
        }
        AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_IraIratok.AdathordozoTipusa, false, EErrorPanel1);
        Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_IraIratok.UgyintezesAlapja, true, EErrorPanel1);
        HivatkozasiSzam_Irat_TextBox.Text = erec_IraIratok.HivatkozasiSzam;

        if (!String.IsNullOrEmpty(erec_IraIratok.KuldKuldemenyek_Id))
        {
            EREC_KuldKuldemenyekService kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            xpm.Record_Id = erec_IraIratok.KuldKuldemenyek_Id;
            Result kuldemenyekResult = kuldemenyekService.Get(xpm);

            if (kuldemenyekResult.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kuldemenyekResult);
                ErrorUpdatePanel1.Update();
                return;
            }

            EREC_KuldKuldemenyek kuldemeny = kuldemenyekResult.Record as EREC_KuldKuldemenyek;

            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = kuldemeny.Partner_Id_Bekuldo;
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = kuldemeny.NevSTR_Bekuldo;
            Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = kuldemeny.Partner_Id_BekuldoKapcsolt;
            Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA, kuldemeny.KuldesMod, false, EErrorPanel1);
            Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = kuldemeny.Cim_Id;
            Pld_CimId_Cimzett_CimekTextBox.Text = kuldemeny.CimSTR_Bekuldo;
        }
    }

    #region  BLG_2950
    //LZS
    //Az ügytípus kiválasztása során betöltjük az javasolt ügyintézőt a felületre és a határidős feladat id-ját.
    protected void UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Hidden field-ekben tároljuk a felületen, hogy postback után is elérhetőek legyenek. Itt clear-eljük őket.
        #region Hidden fields
        JavasoltUgyintezoId.Value = "";
        HataridosFeladatId.Value = "";
        SzignalasTipusa.Value = "";
        #endregion

        //Lekérjük az EREC_IratMetaDefinicio táblából a kiválasztott ügytípushoz tartozó metaDef_Id-t.
        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch metaDefSearch = new EREC_IratMetaDefinicioSearch();
        metaDefSearch.Ugytipus.Value = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
        metaDefSearch.Ugytipus.Operator = Query.Operators.equals;
        metaDefSearch.UgytipusNev.Value = UgyUgyirat_Ugytipus_DropDownList.SelectedItem.Text;
        metaDefSearch.UgytipusNev.Operator = Query.Operators.equals;
        metaDefSearch.Ugykor_Id.Value = Ugykor_DropDownList.SelectedValue;
        metaDefSearch.Ugykor_Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string metaDef_Id = "";

        Result result = service.GetAll(execParam, metaDefSearch);

        if (!result.IsError)
        {
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                metaDef_Id = row["Id"].ToString();
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, result);
        }

        //A metaDef_Id segítségével lekérjük a megfelelő szignálási jegyzéket EREC_SzignalasiJegyzekek táblából, amely tartalmazza a 
        //szignálás típusát, a határidős feladat id-ját és a javasolt felelős ügyintéző id-ját.
        if (!string.IsNullOrEmpty(metaDef_Id))
        {
            System.Data.DataRow dataRow_EREC_SzignalasiJegyzekek = null;


            EREC_SzignalasiJegyzekekService SzJservice = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
            EREC_SzignalasiJegyzekekSearch SzJSearch = new EREC_SzignalasiJegyzekekSearch();

            ExecParam SzignalasiJegyzek = UI.SetExecParamDefault(Page, new ExecParam());

            SzJSearch.UgykorTargykor_Id.Value = metaDef_Id;
            SzJSearch.UgykorTargykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result resSzJ = SzJservice.GetAll(SzignalasiJegyzek, SzJSearch);

            if (!resSzJ.IsError)
            {
                if (resSzJ.Ds.Tables[0].Rows.Count == 1)
                {
                    dataRow_EREC_SzignalasiJegyzekek = resSzJ.Ds.Tables[0].Rows[0];

                    if (dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"] != null)
                    {
                        JavasoltUgyintezoId.Value = dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"].ToString();
                    }
                    else
                    {
                        JavasoltUgyintezoId.Value = "";
                    }

                    if (dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"] != null)
                    {
                        HataridosFeladatId.Value = dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"].ToString();
                    }
                    else
                    {
                        HataridosFeladatId.Value = "";
                    }

                    if (dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"] != null)
                    {
                        SzignalasTipusa.Value = dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"].ToString();
                    }
                    else
                    {
                        SzignalasTipusa.Value = "";
                    }

                }
                else if (resSzJ.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Warn("A szignálási jegyzék nem egyértelmû! Szignálási jegyzékek -bõl visszakapott sorok száma = " + resSzJ.Ds.Tables[0].Rows.Count);
                    ResultError.SetResultByErrorCode(resSzJ, 57001); // a keresés több találatot eredményezett! 
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, resSzJ);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, resSzJ);
            }

            //Itt történik a javasolt ügyintéző felületen történő beállítása.
            if (!string.IsNullOrEmpty(JavasoltUgyintezoId.Value) && SzignalasTipusa.Value == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = JavasoltUgyintezoId.Value;
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(FormHeader.ErrorPanel);
            }
            //else
            //{
            //    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = "";
            //    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(FormHeader.ErrorPanel);
            //}
        }

        RegisterStartupScript("UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged", "ugytipus_lastValue = '';UgyTipusChangeHandler();");

    }
    #endregion

    #region HA HATOSAGI UGY
    /// <summary>
    /// SetIratUgyintezesiHataridoDisable
    /// </summary>
    /// <param name="irat"></param>
    private void SetIratControlsDisableIfHatosagi(EREC_IraIratok irat)
    {
        if (irat == null || string.IsNullOrEmpty(irat.Ugy_Fajtaja))
            return;
        ExecParam xpm = UI.SetExecParamDefault(Page);

        if (Contentum.eUtility.Sakkora.IsUgyFajtaHatosagi(xpm, irat.Ugy_Fajtaja))
        {
            IraIrat_Hatarido_CalendarControl.Enabled = false;
            IraIrat_Hatarido_CalendarControl.ReadOnly = true;

            inputUgyintezesiNapok.Disabled = true;

            IratIntezesiIdoegyseg_KodtarakDropDownList.Enabled = false;
            IratIntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;
        }
    }
    #endregion
    /// <summary>
    /// SetIratUgyintezesiIdoPlusHataridoVisibility
    /// </summary>
    private void SetIratUgyintezesiIdoPlusHataridoVisibility()
    {
        bool enabled = true;
        switch (Mode)
        {
            case CommandName.BejovoIratIktatas:
            case CommandName.BelsoIratIktatas:
                enabled = false;
                break;
            default:
                break;
        }

        if (enabled)
        {
            inputUgyintezesiNapok.Attributes.Remove("readonly");
            // BUG_10524
            inputUgyintezesiNapok.Disabled = false;

            IratIntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = false;
            IratIntezesiIdoegyseg_KodtarakDropDownList.Enabled = true;

            // BUG_10524
            //IraIrat_Hatarido_CalendarControl.ReadOnly = false;
            IraIrat_Hatarido_CalendarControl.Enabled = true;
            IraIrat_Hatarido_CalendarControl.Attributes.Remove("readonly");
        }
        else
        {
            inputUgyintezesiNapok.Attributes.Add("readonly", "readonly");
            // BUG_10524
            inputUgyintezesiNapok.Disabled = true;

            IratIntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;
            IratIntezesiIdoegyseg_KodtarakDropDownList.Enabled = false;

            // BUG_10524
            //IraIrat_Hatarido_CalendarControl.ReadOnly = true;
            IraIrat_Hatarido_CalendarControl.Enabled = false;
            IraIrat_Hatarido_CalendarControl.Attributes.Add("readonly", "readonly");
        }
    }
    /// <summary>
    /// InitSakkoraDateControls
    /// </summary>
    private void InitSakkoraDateControls()
    {
        if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page)))
        {
            CalendarControl_UgyintezesKezdete.Text = Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(UI.SetExecParamDefault(Page));
            //if (string.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Text))
            //    UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Text = "23";
            //if (string.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Text))
            //    UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Text = "59";

            //if (string.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.GetHourTextBox.Text))
            //    IraIrat_Hatarido_CalendarControl.GetHourTextBox.Text = "23";
            //if (string.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.Text))
            //    IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.Text = "59";
        }
    }
    protected void UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isHatosagi = CheckUgyFajtaIsHatosagi(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);
        if (isHatosagi && String.IsNullOrEmpty(KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
        {
            KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = KodTarak.ELJARASI_SZAKASZ_FOK.I;
            KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(KodtarakDropDownList_EljarasiSzakaszFok, EventArgs.Empty);
        }
        else if (!isHatosagi && !String.IsNullOrEmpty(KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
        {
            KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = String.Empty;
            KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(KodtarakDropDownList_EljarasiSzakaszFok, EventArgs.Empty);
        }
    }

    public static void SetDokumentumGeneralasFunkciogomb(string iratId, EREC_IraIratok obj_Irat, ImageButton imageButton)
    {
        imageButton.Enabled = false;

        Page page = imageButton.Page;
        ExecParam execParam = UI.SetExecParamDefault(page);

        // Funkciójog:
        //TODO
        //if (!FunctionRights.GetFunkcioJog(page, "Dokumentum_Generalas"))
        //{
        //    return;
        //}

        if (String.IsNullOrEmpty(iratId) || obj_Irat == null)
        {
            imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }

        string irattipus = obj_Irat.Irattipus;

        if (String.IsNullOrEmpty(irattipus))
        {
            return;
        }

        string irattipusKcsKod = "IRATTIPUS";
        string sablonokKcsKod = "SABLONOK";
        bool nincsItem;
        List<string> sablonKodok = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, irattipusKcsKod, sablonokKcsKod, irattipus, out nincsItem);

        //ha van sablon adott irattipushoz
        if (sablonKodok != null && sablonKodok.Count > 0)
        {
            string sablonKod = sablonKodok.First();

            //dokumentum id lekérése
            Contentum.eAdmin.Service.KRT_KodTarakService ktService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
            Result ktResult = ktService.GetByKodcsoportKod(execParam, sablonokKcsKod, sablonKod);

            if (!ktResult.IsError)
            {
                string dokumentumId = (ktResult.Record as KRT_KodTarak).Obj_Id;

                if (!String.IsNullOrEmpty(dokumentumId))
                {
                    imageButton.Enabled = true;
                    imageButton.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("DokumentumGeneralas.aspx?" + QueryStringVars.IratId + "=" + iratId + "&" + QueryStringVars.DokumentumId + "=" + dokumentumId);
                }
            }
        }
    }

    /// <summary>
    /// SetFelelosSzervezetiEgysegHaBelsoIktatas
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetFelelosSzervezetiEgysegHaBelsoIktatas(ref EREC_IraIratok erec_IraIratok)
    {
        #region 11580
        if (Command == CommandName.New && string.IsNullOrEmpty(erec_IraIratok.Csoport_Id_Ugyfelelos) && IsIktatasBelsoUgyintezoIktato)
        {
            erec_IraIratok.Csoport_Id_Ugyfelelos = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
            erec_IraIratok.Updated.Csoport_Id_Ugyfelelos = true;
        }
        #endregion 11580
    }
}