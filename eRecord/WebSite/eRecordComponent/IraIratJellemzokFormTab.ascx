﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IraIratJellemzokFormTab.ascx.cs" Inherits="eRecordComponent_IraIratJellemzokFormTab" %>

<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="otp" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="UgyiratJellemzokUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr class="urlapSor">
                        <td style="text-align:left;">
                            <asp:Label ID="labelStatisztika" runat="server" Text="<%$Forditas:labelStatisztika|Irattípus statisztika%>" style="font-weight:bold;padding-left: 10px;" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td>
                            <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak" RepeatColumns="1" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
            </br>
            <div style="text-align:center;">
                <%-- TabFooter --%>
                <uc:TabFooter ID="TabFooter1" runat="server" />
                <%-- /TabFooter --%>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>