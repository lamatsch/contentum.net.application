using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_IraIrattariTetelTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    #region public properties

    private bool _TryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _TryFireChangeEvent; }
        set { _TryFireChangeEvent = value; }
    }

    bool _FromIratmetaDefiniciok = false;
    
    public bool FromIratmetaDefiniciok
    {
        set { _FromIratmetaDefiniciok = value; }
        get { return _FromIratmetaDefiniciok; }
    }
    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            IraIrattariTetelMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "";
                NewImageButton.CssClass = "";
                ResetImageButton.CssClass = "";
            }
        }
        get { return IraIrattariTetelMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            IraIrattariTetelMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            } else
            {
                LovImageButton.CssClass = "";
                NewImageButton.CssClass = "";
                ResetImageButton.CssClass = "";
            }
        }
        get { return IraIrattariTetelMegnevezes.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { IraIrattariTetelMegnevezes.Text = value; }
        get { return IraIrattariTetelMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return IraIrattariTetelMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IraIrattariTetelMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IraIrattariTetelMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public DataRow SetIraIrattariTetelTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //EREC_IraIrattariTetelek erec_IraIrattariTetelek = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            // GetAllWithExtension-nel kérjük le:

            EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

            search.Id.Value = Id_HiddenField;
            search.Id.Operator = Query.Operators.equals;

            #region érvényesség ellenőrzés kikapcsolása
            search.ErvKezd.Clear();
            search.ErvVege.Clear();
            #endregion érvényesség ellenőrzés kikapcsolása

            Result result = service.GetAllWithExtension(execParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds == null || result.Ds.Tables[0].Rows.Count != 1)
                {
                    // hiba:
                    return null;
                }
                else
                {
                    DataRow row = result.Ds.Tables[0].Rows[0];

                    #region érvényesség ellenőrzés
                    DateTime ErvVege;
                    bool ervenyes = true;

                    if (DateTime.TryParse(row["ErvVege"].ToString(), out ErvVege) && ErvVege < DateTime.Now)
                    {
                        ervenyes = false;
                    }

                    #endregion érvényesség ellenőrzés

                    string mergeIrattariTetelszam = row["Merge_IrattariTetelszam"].ToString();
                    //string nev = row["Nev"].ToString();
                    string irattariTetelSzam = row["IrattariTetelszam"].ToString();

                    if (FromIratmetaDefiniciok)
                    {
                        Text = irattariTetelSzam;
                    }
                    else
                    {
                        Text = mergeIrattariTetelszam;// +" (" + nev + ")";
                    }

                    if (!ervenyes)
                    {
                        string deletedValue = "[" + Resources.Form.UI_ErvenytelenitettTetel + "]";
                        Text += " " + deletedValue;
                    }

                    return row;

                    //erec_IraIrattariTetelek = (EREC_IraIrattariTetelek)result.Record;
                    //Text = erec_IraIrattariTetelek.IrattariTetelszam + " (" + erec_IraIrattariTetelek.Nev + ")";
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }

        return null;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {        

        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //String asdf = IraIrattariTetelMegnevezes.Text;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string query = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + 
            "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelMegnevezes.ClientID;

        if (TryFireChangeEvent)
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        if (FromIratmetaDefiniciok)
        {
            query += "&FromIratMetaDefiniciok=1";
        }
        OnClick_Lov = JavaScripts.SetOnClientClick("IraIrattariTetelekLovList.aspx",query, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelMegnevezes.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "IraIrattariTetelekForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "var tbox = $get('" + TextBox.ClientID + "');tbox.value = '';$get('" +
            HiddenField1.ClientID + "').value = '';" +
            (TryFireChangeEvent ? "$common.tryFireEvent(tbox, 'change');" : "") +
            "return false;";

    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IraIrattariTetelMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
