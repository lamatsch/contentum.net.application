<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IraIrattariTetelTextBoxWithExtensions.ascx.cs" Inherits="eRecordComponent_IraIrattariTetelTextBoxWithExtensions" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<div class="DisableWrap">
    <asp:HiddenField ID="hfErvKezd" runat="server" />
    <asp:HiddenField ID="hfErvVege" runat="server" />
    <asp:HiddenField ID="hfIsErvenyessegTartomany" runat="server" />
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <tr class="urlapSor_kicsi">
                <td class="<% = LongCaption ? "mrUrlapCaption" : "mrUrlapCaption_short" %>" style="width: 103px" >
                    <asp:Label ID="Irattari_tetelszam_felirat" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                <td class="mrUrlapMezo" style="width: 450px">
                    <asp:TextBox ID="IraIrattariTetelMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true" Width="60%"></asp:TextBox>
                    
                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" Enabled="false"
                        MinimumPrefixLength="2" TargetControlID="IraIrattariTetelMegnevezes" CompletionSetCount="20" CompletionInterval="1000"
                        ContextKey="" UseContextKey="true" FirstRowSelected="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                        ServiceMethod="GetIrattariTetelszamList" CompletionListItemCssClass="GridViewRowStyle"
                        CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle">
                    </ajaxToolkit:AutoCompleteExtender>

                    <eUI:WorldJumpExtender ID="WorldJumpExtender1" Enabled="false" runat="server" TargetControlID="IraIrattariTetelMegnevezes" AutoCompleteExtenderId="AutoCompleteExtender1" />

                    <asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
                        CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" />
                    <asp:ImageButton TabIndex = "-1" ID="NewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
                        ImageUrl="~/images/hu/lov/hozzaad.gif" onmouseover="swapByName(this.id,'hozzaad_keret.gif')" onmouseout="swapByName(this.id,'hozzaad.gif')" AlternateText="�j" />
                    <asp:ImageButton TabIndex = "-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
                        ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" /><asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alap�llapot"/>       
                    <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="IraIrattariTetelMegnevezes" SetFocusOnError="true"
                        Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
                        <Animations>
                                <OnShow>
                                <Sequence>
                                    <HideAction Visible="true" />
                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                </Sequence>    
                                </OnShow>
                         </Animations>
                    </ajaxToolkit:ValidatorCalloutExtender>
                </td>
                <td class="<% = LongCaption ? "mrUrlapCaption" : "mrUrlapCaption_short" %>">
                    <asp:Label ID="Irattari_jel_felirat" runat="server" Text="<%$Resources:Form,UI_Megorzesi_mod%>"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <asp:HiddenField ID="hfIrattariJel" runat="server" />
                    <asp:TextBox ID="IrattariJelTextBox1" runat="server" ReadOnly="true" Width="200px"></asp:TextBox>&nbsp;
                    </td>
            </tr>
            <tr class="urlapSor_kicsi">
                <td class="<% = LongCaption ? "mrUrlapCaption" : "mrUrlapCaption_short" %>" style="width: 103px">
                    <asp:Label ID="Megnevezes_felirat" runat="server" Text="Megnevez�s:"></asp:Label></td>
                <td class="mrUrlapMezo" colspan="3">
                    <asp:TextBox ID="IraIttl_Nev_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="True"
                        Width="99%"></asp:TextBox></td>
                <%--<td class="<% = LongCaption ? "mrUrlapCaption" : "mrUrlapCaption_short" %>">
                                <asp:Label ID="Megorzesi_ido_felirat" runat="server" Text="Meg�rz�si id�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:RequiredNumberBox ID="IraIttl_MegorzesiIdo_RequiredNumberBox" runat="server"
                                    ReadOnly="true" Validate="false" />
                            </td>--%>
            </tr>
        </tbody>
    </table>
</div>