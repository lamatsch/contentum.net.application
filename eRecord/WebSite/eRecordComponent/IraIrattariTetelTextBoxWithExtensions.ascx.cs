﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_IraIrattariTetelTextBoxWithExtensions : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent
{

    #region public properties

    // BUG_7088
    private const string autoCompleteTextChanged = "autoCompleteTextChanged";
    private const string autoCompleteValue = "autoCompleteValue";
    private bool _autoComplete;
    public bool AutoComplete { get { return _autoComplete; }
        set {
            string onblur = "if (" + autoCompleteTextChanged + ") { alert('Válassza ki az elemet a listából.'); $get('" + IraIrattariTetelMegnevezes.ClientID + "').focus(); }";
            string onchange = "if (event.lovlist && event.lovlist == 1) {" + autoCompleteValue + "=$get('" + IraIrattariTetelMegnevezes.ClientID + "').value;};" + autoCompleteTextChanged + "=(" + autoCompleteValue + "!=$get('" + IraIrattariTetelMegnevezes.ClientID + "').value);";

            if (value && !ReadOnly) {
                _autoComplete = true;
                WorldJumpExtender1.Enabled = true;
                AutoCompleteExtender1.Enabled = true;
                AutoCompleteExtender1.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
                AppendAttribute(IraIrattariTetelMegnevezes, "onblur", onblur);
                AppendAttribute(IraIrattariTetelMegnevezes, "onchange", onchange);
            }
            else
            {
                _autoComplete = false;
                WorldJumpExtender1.Enabled = false;
                AutoCompleteExtender1.Enabled = false;
                IraIrattariTetelMegnevezes.Attributes.Remove("onblur");
                IraIrattariTetelMegnevezes.Attributes.Remove("onchange");
            }
        }
    }
    //
    void AppendAttribute(WebControl control, string key, string value)
    {
        string currentValue = control.Attributes[key];
        if (String.IsNullOrEmpty(currentValue))
            currentValue = String.Empty;
        else
            currentValue += ";";

        if (!currentValue.Contains(value))
        {
            control.Attributes[key] = currentValue + value;
        }
    }

    private const string kcs_IRATTARI_JEL = "IRATTARI_JEL";
    Contentum.eUIControls.eErrorPanel EErrorPanel1 = new Contentum.eUIControls.eErrorPanel();

    private bool _TryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _TryFireChangeEvent; }
        set { _TryFireChangeEvent = value; }
    }

    public string IrattariJel
    {
        get
        {
            return hfIrattariJel.Value.Trim();
        }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            IraIrattariTetelMegnevezes.Enabled = value;
            IrattariJelTextBox1.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return IraIrattariTetelMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            IraIrattariTetelMegnevezes.ReadOnly = value;
            IrattariJelTextBox1.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
                AutoComplete = false;
            }
        }
        get { return IraIrattariTetelMegnevezes.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { IraIrattariTetelMegnevezes.Text = value; }
        get { return IraIrattariTetelMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return IraIrattariTetelMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IraIrattariTetelMegnevezes.CssClass = "ViewReadOnlyWebControl";
                IrattariJelTextBox1.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IraIrattariTetelMegnevezes.CssClass = "ViewDisabledWebControl";
                IrattariJelTextBox1.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    #region Ervenyesseg

    public string ErvenyessegDatum
    {
        get { return (IsTartomany ? "" : hfErvKezd.Value); }
        set { hfErvKezd.Value = value; IsTartomany = false; }
    }

    public string ErvKezd
    {
        get { return (IsTartomany ? hfErvKezd.Value : "");   }
        set { hfErvKezd.Value = value; IsTartomany = true; }
    }

    public string ErvVege
    {
        get { return (IsTartomany ? hfErvVege.Value : ""); }
        set { hfErvVege.Value = value; IsTartomany = true; }
    }

    public bool IsTartomany
    {
        get { return hfIsErvenyessegTartomany.Value == "1" ? true : false; }
        set { hfIsErvenyessegTartomany.Value = (value == true ? "1" : "0"); }
    }

    #endregion Ervenyesseg

    public string IraIktatokonyv_Id
    {
        get
        {
            if (ViewState["IraIktatokonyv_Id"] != null)
            {
                return ViewState["IraIktatokonyv_Id"].ToString();
            }
            else
            {
                return "";
            }
        }

        set { ViewState["IraIktatokonyv_Id"] = value;  }
    
    }

    public bool LongCaption
    {
        get { return ViewState["LongCaption"] == null ? false : (bool)ViewState["LongCaption"]; }
        set { ViewState["LongCaption"] = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public DataRow SetIraIrattariTetelTextBoxWithExtensionsById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //EREC_IraIrattariTetelek erec_IraIrattariTetelek = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            // GetAllWithExtension-nel kérjük le:

            EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

            search.Id.Filter(Id_HiddenField);

            #region érvényesség ellenõrzés kikapcsolása
            search.ErvKezd.Clear();
            search.ErvVege.Clear();
            #endregion érvényesség ellenõrzés kikapcsolása

            Result result = service.GetAllWithExtension(execParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.GetCount == 0)
                {
                    // hiba:
                    return null;
                }
                else
                {
                    DataRow row = result.Ds.Tables[0].Rows[0];
                    Text = row["Merge_IrattariTetelszam"].ToString();

                    if (!String.IsNullOrEmpty(row["IrattariJel"].ToString()))
                    {
                       hfIrattariJel.Value = row["IrattariJel"].ToString();
                       Dictionary<String, String> kodtarak_Dictionary =
                       Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_IRATTARI_JEL, Page);
                        if (kodtarak_Dictionary != null)
                        {
                            if (kodtarak_Dictionary.ContainsKey(row["IrattariJel"].ToString()))
                            {
                                IrattariJelTextBox1.Text = kodtarak_Dictionary[row["IrattariJel"].ToString()];
                            }
                        }
                    }

                    IraIttl_Nev_TextBox.Text = row["Nev"].ToString();

                    return row;
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
            // BLG_439
            IraIttl_Nev_TextBox.Text = "";
            IrattariJelTextBox1.Text = "";
        }

        return null;
    }

    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {
        IraIttl_Nev_TextBox.Attributes.Add("onmouseover", "this.title = this.value;");

        // BUG_11855 // egyelőre csak BOPMH
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            Irattari_jel_felirat.Text = "Megőrzési idő:";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;

        //ASP.NET 2.0 bug work around
        if (!AutoComplete)
        {
            TextBox.Attributes.Add("readonly", "readonly");
        }
        else
        {
            // BUG_7088
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsStartupScriptRegistered(this.GetType(), "setAutoCompleteGlobals"))
            {
                cs.RegisterStartupScript(this.GetType(), "setAutoCompleteGlobals", 
                    "var " + autoCompleteTextChanged + "=false; var " + autoCompleteValue + "='';", true);
            }
            AutoCompleteExtender1.OnClientItemSelected = "updateSelectedItsz";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "updateSelectedItsz", "function updateSelectedItsz(sender,e){" + autoCompleteTextChanged + "=false;" +
                //"console.log('updateSelectedItsz:'+e._item.innerText);" +
                autoCompleteValue + "=e._item.innerText;" +
                "$get('" + HiddenField1.ClientID + "').value=e.get_value().substr(0,36); $get('" + IraIttl_Nev_TextBox.ClientID + "').value=e.get_value().substr(36);" +
                (TryFireChangeEvent ? "$common.tryFireEvent($get('" + IraIrattariTetelMegnevezes.ClientID + "'), 'change');}" : "}"), true);
            //AutoCompleteExtender1.OnClientPopulated = "itszPopulated";
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "itszPopulated", 
            //    "function itszPopulated(sender,e){completionList=$find('" + AutoCompleteExtender1.ClientID + 
            //    "').get_completionList();alert('cL='+completionList.length);}", true);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string query = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
   + "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelMegnevezes.ClientID
   + "&" + QueryStringVars.NevTextBoxId + "=" + IraIttl_Nev_TextBox.ClientID
   + "&" + QueryStringVars.IrattariJelTextBoxId + "=" + IrattariJelTextBox1.ClientID;

        if (!String.IsNullOrEmpty(ErvenyessegDatum))
        {
            query += "&" + QueryStringVars.ErvenyessegSearchParameters.GetErvenyessegDatumFilterAsQsString(ErvenyessegDatum);
        }
        else if (IsTartomany && (!String.IsNullOrEmpty(ErvKezd) || !String.IsNullOrEmpty(ErvVege)))
        {
            query += "&" + QueryStringVars.ErvenyessegSearchParameters.GetErvenyessegTartomanyFilterAsQsString(ErvKezd, ErvVege);
        }

        if (!String.IsNullOrEmpty(IraIktatokonyv_Id))
        {
            query += "&" + QueryStringVars.IktatokonyvId + "=" + IraIktatokonyv_Id;
        }

        if (TryFireChangeEvent)
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }


        OnClick_Lov = JavaScripts.SetOnClientClick("IraIrattariTetelekLovList.aspx", query
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelMegnevezes.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "IraIrattariTetelekForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "var tbox = $get('" + TextBox.ClientID + "');tbox.value = '';$get('" +
            HiddenField1.ClientID + "').value = '';$get('" +
            IraIttl_Nev_TextBox.ClientID + "').value = '';$get('" +
            IrattariJelTextBox1.ClientID + "').value = '';" +
            (TryFireChangeEvent ? "$common.tryFireEvent(tbox, 'change');" : "") +
            "return false;";
    }

    #region ISelectableUserComponent method implementations

    public List<WebControl> GetComponentList()
    {
        List<WebControl> componentList = new List<WebControl>();

        componentList.Add(IraIrattariTetelMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

}
