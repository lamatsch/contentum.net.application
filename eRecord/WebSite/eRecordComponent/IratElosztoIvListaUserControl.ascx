﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="IratElosztoIvListaUserControl.ascx.cs"
    Inherits="eRecordComponent_IratElosztoIvListaUserControl" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="~/eRecordComponent/ElosztoIvCimzettListaControl.ascx" TagPrefix="uc1" TagName="ElosztoIvCimzettListaControl" %>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<eUI:eFormPanel ID="EFormPanel" runat="server" HorizontalAlign="Left">
    <asp:HiddenField runat="server" ID="HiddenFieldIratId" />
    <%--<asp:HiddenField runat="server" ID="HiddenFieldPldIratId" />--%>
    <asp:HiddenField runat="server" ID="HiddenFieldElosztoIvId" />
    <asp:Panel runat="server" ID="PanelContainerCimzettLista" HorizontalAlign="Left">
        <uc1:ElosztoIvCimzettListaControl runat="server" ID="ElosztoIvCimzettListaControl" />

        <ajaxToolkit:TabContainer ID="TabContainerCimzettListaTetelek" runat="server" Width="100%">
            <ajaxToolkit:TabPanel ID="TabPanelCLT" runat="server" TabIndex="0">
                <HeaderTemplate>
                    <asp:Label ID="LabelCimlista" runat="server" Text="CímzettLista és tételek"></asp:Label>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:GridView ID="ElosztoivekGridView" runat="server"
                        CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                        AutoGenerateColumns="false" DataKeyNames="Id">
                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                        <Columns>
                            <asp:BoundField DataField="Nev" HeaderText="Név" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="Nev" HeaderStyle-Width="200px" HeaderStyle-CssClass="GridViewBorderHeader" />
                            <asp:BoundField DataField="Fajta_Nev" HeaderText="Fajta" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="Fajta_Nev" HeaderStyle-Width="150px" />
                        </Columns>
                        <PagerSettings Visible="False" />
                    </asp:GridView>
                    <br />
                    <asp:GridView ID="ElosztoivTagokGridView" runat="server"
                        CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                        AutoGenerateColumns="false" DataKeyNames="Id">
                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                        <Columns>
                            <asp:BoundField DataField="NevSTR" HeaderText="Partner neve"
                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="NevSTR" HeaderStyle-Width="200px"></asp:BoundField>
                            <asp:BoundField DataField="CimSTR" HeaderText="Partner címe"
                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="CimSTR" HeaderStyle-Width="200px"></asp:BoundField>
                            <asp:BoundField DataField="Kuldesmod_Nev" HeaderText="Küldésmód"
                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="Kuldesmod_Nev" HeaderStyle-Width="200px"></asp:BoundField>
                            <asp:BoundField DataField="AlairoSzerep_Nev" HeaderText="Aláírószerep"
                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                SortExpression="AlairoSzerep_Nev" HeaderStyle-Width="200px"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
</eUI:eFormPanel>
