﻿using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Data;

public partial class eRecordComponent_IratElosztoIvListaUserControl : System.Web.UI.UserControl
{
    #region PROPERTIES
    UI ui = new UI();

    public string ElosztoIvId
    {
        get { return HiddenFieldElosztoIvId.Value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
       // TabContainerCimzettListaTetelek.Visible = false;
        ElosztoIvCimzettListaControl.OnSaved += ElosztoIvCimzettListaControl_OnSaved;
    }

    private void ElosztoIvCimzettListaControl_OnSaved(string elosztoIvId, string name)
    {
        HiddenFieldElosztoIvId.Value = elosztoIvId;
        if (!string.IsNullOrEmpty(HiddenFieldIratId.Value))
        {
            SaveIratIdToElosztoIv(HiddenFieldIratId.Value, elosztoIvId);
            LoadData();
        }
    }

    /// <summary>
    /// Inicializál iratból
    /// </summary>
    /// <param name="pldIratId"></param>
    public void InitializeWithIrat(string iratId)
    {
        try
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = GetElosztoIvByIrat(execParam, iratId);
            if (string.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables[0].Rows.Count > 0)
            {
                HiddenFieldElosztoIvId.Value = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            HiddenFieldIratId.Value = iratId;
        }
        catch (Exception exc)
        {
            Logger.Error("InitializeWithIrat ", exc);
        }

    }
    /// <summary>
    /// Inicializál elosztóív id-val
    /// </summary>
    /// <param name="elosztoIvId"></param>
    public void Initialize(string elosztoIvId)
    {
        try
        {
            HiddenFieldElosztoIvId.Value = elosztoIvId;
            EREC_IraElosztoivek iv = GetElosztoIv(elosztoIvId);
            if (iv != null && !string.IsNullOrEmpty(iv.Base.Note))
                HiddenFieldIratId.Value = iv.Base.Note;
        }
        catch (Exception exc)
        {
            Logger.Error("InitializeWithIrat ", exc);
            throw;
        }
    }
    /// <summary>
    /// Adatok letöltése és gridek feltötlése
    /// </summary>
    public void LoadData()
    {
        try
        {
            TabContainerCimzettListaTetelek.Visible = true;
            FillElosztoIvGridView();
            FillElosztoIvTetelekGridView();
        }
        catch (Exception exc)
        {
            Logger.Error("InitializeWithIrat.LoadData ", exc);
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ElosztoivekList");
    }

    protected void FillElosztoIvGridView()
    {
        Result result = GetAllElosztoIv();
        if (result != null)
            ui.GridViewFill(ElosztoivekGridView, result, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void FillElosztoIvTetelekGridView()
    {
        Result result = GetAllElosztoIvTetel();
        if (result != null)
            ui.GridViewFill(ElosztoivTagokGridView, result, EErrorPanel1, ErrorUpdatePanel);
    }
    #region SERVICE
    private Result GetElosztoIvByIrat(ExecParam execParam, string iratId)
    {
        if (string.IsNullOrEmpty(iratId))
            return null;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        EREC_IraElosztoivekSearch src = new EREC_IraElosztoivekSearch();
        src.WhereByManual = " and EREC_IraElosztoivek.Note = '" + iratId + "'";
        Result result = service.GetAll(execParam, src);

        return result;
    }

    /// <summary>
    /// Elosztoiv lekérése
    /// </summary>
    /// <returns></returns>
    private Result GetAllElosztoIv()
    {
        if (string.IsNullOrEmpty(HiddenFieldElosztoIvId.Value))
            return null;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        EREC_IraElosztoivekSearch search = null;

        search = new EREC_IraElosztoivekSearch();
        search.Id.Value = HiddenFieldElosztoIvId.Value;
        search.Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAllWithExtension(execParam, search);
        return result;
    }

    private EREC_IraElosztoivek GetElosztoIv(string id)
    {
        if (string.IsNullOrEmpty(id))
            return null;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        if (string.IsNullOrEmpty(result.ErrorCode))
            return (EREC_IraElosztoivek)result.Record;
        return null;
    }
    private Result UpdateElosztoIvNoteField(string elosztoIvId, string iratId, string pldIratId)
    {
        if (string.IsNullOrEmpty(elosztoIvId) || string.IsNullOrEmpty(iratId))
            return null;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraElosztoivek eiv = GetElosztoIv(elosztoIvId);
        eiv.Base.Note = iratId;
        eiv.Base.Updated.Note = true;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        execParam.Record_Id = elosztoIvId;
        Result result = service.Update(execParam, eiv);
        if (string.IsNullOrEmpty(result.ErrorCode))
            return result;
        return null;
    }

    /// <summary>
    /// ElosztoIv tételek lekérése
    /// </summary>
    /// <returns></returns>
    private Result GetAllElosztoIvTetel()
    {
        if (string.IsNullOrEmpty(HiddenFieldElosztoIvId.Value))
            return null;

        Contentum.eAdmin.Service.EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
        EREC_IraElosztoivTetelekSearch search = null;

        search = new EREC_IraElosztoivTetelekSearch();
        search.ElosztoIv_Id.Value = HiddenFieldElosztoIvId.Value;
        search.ElosztoIv_Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAllWithExtension(execParam, search);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("eRecordComponent_IratElosztoIvListaUserControl.SaveElosztoIvIdToPldIrat ", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "IratElosztoIvLista", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
        }
        return result;
    }

    /// <summary>
    /// Iratok lekérése id-ból
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private EREC_IraIratok GetIrat(string id)
    {
        if (string.IsNullOrEmpty(id))
            return null;

        Contentum.eRecord.Service.EREC_IraIratokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("eRecordComponent_IratElosztoIvListaUserControl.GetIrat ", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "GetIrat", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
        }
        EREC_IraIratok item = (EREC_IraIratok)result.Record;
        return item;
    }
    private void SaveIratIdToElosztoIv(string iratId, string elosztoIvId)
    {
        if (string.IsNullOrEmpty(iratId) || string.IsNullOrEmpty(elosztoIvId))
            return;

        EREC_IraElosztoivek iv = GetElosztoIv(elosztoIvId);
        if (iv == null)
            return;

        ResetPreviousElosztoIvbyIrat(iratId);

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = elosztoIvId;
        iv.Base.Note = iratId;
        iv.Base.Updated.Note = true;

        Result result = service.Update(execParam, iv);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("eRecordComponent_IratElosztoIvListaUserControl.SaveIratIdToElosztoIv ", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SaveIratIdToElosztoIv", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
        }
    }
    private void ResetPreviousElosztoIvbyIrat(string iratId)
    {
        if (string.IsNullOrEmpty(iratId))
            return;
        ExecParam execParamNoteInvalidate = UI.SetExecParamDefault(Page, new ExecParam());
        Result resultNoteInvalidate = GetElosztoIvByIrat(execParamNoteInvalidate, iratId);
        if (string.IsNullOrEmpty(resultNoteInvalidate.ErrorCode) && resultNoteInvalidate.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in resultNoteInvalidate.Ds.Tables[0].Rows)
            {
                ResetIratIdInElosztoIv(row["id"].ToString());
            }
        }
    }
    private void ResetIratIdInElosztoIv(string elosztoIvId)
    {
        if (string.IsNullOrEmpty(elosztoIvId))
            return;

        EREC_IraElosztoivek iv = GetElosztoIv(elosztoIvId);
        if (iv == null)
            return;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = elosztoIvId;
        iv.Base.Note = string.Empty;
        iv.Base.Updated.Note = true;

        Result result = service.Update(execParam, iv);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("eRecordComponent_IratElosztoIvListaUserControl.ResetIratIdInElosztoIv ", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ResetIratIdInElosztoIv", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
        }
    }
    #endregion
}