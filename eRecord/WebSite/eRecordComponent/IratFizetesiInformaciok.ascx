﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratFizetesiInformaciok.ascx.cs" Inherits="eRecordComponent_IratFizetesiInformaciok" %>

<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="otp" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc" %>
<%@ Register Src="~/Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="UgyiratJellemzokUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr class="urlapSor">
                        <td>
                            <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak" RepeatColumns="1" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
            </br>
            <div style="text-align:center;">
                <%-- TabFooter --%>
                <uc:TabFooter ID="TabFooter1" runat="server" />
                <%-- /TabFooter --%>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

