﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_IratFizetesiInformaciok : System.Web.UI.UserControl
{
    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;

    public string Command = "";

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    private Boolean _Load = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        ReLoadTab(true);

    }

    private void ReLoadTab(bool loadRecord)
    {
        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord betöltése
                        FillObjektumTargyszavaiPanel(ParentId);

                    }
                }
            }


            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter1.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    protected void FillObjektumTargyszavaiPanel(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak.FillObjektumTargyszavai(search, id, null
        , Constants.TableNames.EREC_IraIratok, null, null, false
        , KodTarak.OBJMETADEFINICIO_TIPUS.B4, false, true, EErrorPanel1);
    }

    private void SetComponentsVisibility(String _command)
    {

        if (_command == CommandName.View)
        {


        }

        if (_command == CommandName.Modify)
        {
        }

        #region TabFooter gombok állítása

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion



        #region Form komponensek (TextBoxok,...) állítása


        // A gombok láthatósága alapból a Modify esetre van beállítva,
        // View-nál pluszban még azokat kell letiltani, amik módosításnál látszanának
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        #endregion

    }

    private void SetViewControls()
    {
        otpStandardTargyszavak.ReadOnly = true;
    }

    private EREC_IraOnkormAdatok GetBusinessObjectFromComponents()
    {


        return null;
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
          e.CommandName == CommandName.SaveAndClose)
        {
            switch (Command)
            {
                case CommandName.New:
                    {
                        // New nem lehetséges
                        return;
                    }
                case CommandName.Modify:
                    {
                        bool voltHiba = false;
                        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak.GetEREC_ObjektumTargyszavaiList(true);

                        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                        {
                            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

                            Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                , EREC_ObjektumTargyszavaiList
                                , ParentId
                                , null
                                , Constants.TableNames.EREC_IraIratok
                                , KodTarak.OBJMETADEFINICIO_TIPUS.B4
                                , false
                                );

                            if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                            {
                                // hiba
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                ErrorUpdatePanel1.Update();
                                voltHiba = true;
                            }

                        }

                        if (!voltHiba)
                        {
                            if (e.CommandName == CommandName.Save)
                            {
                                ReLoadTab(true);
                                return;
                            }
                            else if (e.CommandName == CommandName.SaveAndClose)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                            }
                        }


                        break;

                    }
            }
        }
    }

}