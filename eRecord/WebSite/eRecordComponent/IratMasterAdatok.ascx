﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratMasterAdatok.ascx.cs" Inherits="eRecordComponent_IratMasterAdatok" %>

<%@ Register Src="IratTextBox.ascx" TagName="IratTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc2" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc3" %>


<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>   
   <eUI:eFormPanel ID="IratForm" runat="server">
       <table cellpadding="0" cellspacing="0" runat="server" ID="MainTable" width="100%">
                <tr class="urlapSor" id="trIrat" runat="server">
                    <td class="mrUrlapCaption_short">
                        <asp:Label ID="Label2" runat="server" Text="Irat:"></asp:Label></td>
                    <td class="mrUrlapMezo">
                        <uc1:IratTextBox ID="IratTextBox1" runat="server" ViewMode="true" ReadOnly="true" />
                    </td>
                    <td class="mrUrlapCaption_short">
                       <asp:Label ID="Irat_targya_felirat" runat="server" Text="Irat tárgya:"></asp:Label>
                    </td>   
                    <td class="mrUrlapMezo" >
                      <asp:TextBox ID="Targy_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                    </td>    
                </tr>
                <tr class="urlapSor" id="trAllapot" runat="server">
                    <td class="mrUrlapCaption_short">
                       <asp:Label ID="Allapot_felirat" runat="server" Text="Állapot:"></asp:Label> 
                    </td>
                    <td class="mrUrlapMezo">
                       <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true"/>
                    </td>
                    <td class="mrUrlapCaption_short">
                       <asp:Label ID="Label18" runat="server" Text="Irat típus:"></asp:Label>                    
                    </td>   
                    <td class="mrUrlapMezo" >
                       <uc2:KodtarakDropDownList ID="Tipus_KodtarakDropDownList" runat="server" ReadOnly="true"/>
                    </td>    
                </tr>
                <tr class="urlapSor" id="trIktato" runat="server">
                    <td class="mrUrlapCaption_short">
                       <asp:Label ID="Label3" runat="server" Text="Iktató:"></asp:Label> 
                    </td>
                    <td class="mrUrlapMezo">
                        <uc3:FelhasznaloCsoportTextBox ID="Iktato_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false" />
                    </td>
                    <td class="mrUrlapCaption_short">
                         <asp:Label ID="Label1" runat="server" Text="Ügyintéző:"></asp:Label>                     
                    </td>   
                    <td class="mrUrlapMezo" >
                      <uc3:FelhasznaloCsoportTextBox ID="Ugyintezo_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false" />
                    </td>    
                </tr>      
       </table>
   </eUI:eFormPanel>
  </td>    
</tr>
</table>
</asp:Panel>