﻿using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_IratMasterAdatok : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    #region public properties

    private const string kcs_IRAT_ALLAPOT = "IRAT_ALLAPOT";
    private const string kcs_IRATTIPUS = "IRATTIPUS";

    public String IratId
    {
        get { return IratTextBox1.Id_HiddenField; }
        set { IratTextBox1.Id_HiddenField = value; }
    }



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public EREC_IraIratok SetIratMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        return SetIratMasterAdatokById(errorPanel, null);
    }

    public EREC_IraIratok SetIratMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(IratId))
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = IratId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                    IratTextBox1.Id_HiddenField = erec_IraIratok.Id;
                    IratTextBox1.SetIratTextBoxById(errorPanel, errorUpdatePanel);

                    Targy_TextBox.Text = erec_IraIratok.Targy;

                    Allapot_KodtarakDropDownList.FillWithOneValue(kcs_IRAT_ALLAPOT,
                        erec_IraIratok.Allapot, errorPanel);

                    Tipus_KodtarakDropDownList.FillWithOneValue(kcs_IRATTIPUS,
                        erec_IraIratok.Irattipus, errorPanel);

                    Iktato_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Iktato;
                    Iktato_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);

                    Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez;
                    Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);

                    return erec_IraIratok;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }

    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(IratForm);
        componentList.AddRange(Contentum.eUtility.PageView.GetSelectableChildComponents(IratForm.Controls));
        return componentList;
    }

    #endregion
}
