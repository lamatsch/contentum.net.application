<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratMetaDefinicioTerkep.ascx.cs" Inherits="eRecordComponent_IratMetaDefinicioTerkep
" %>
    <%-- 
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    --%>
    <asp:HiddenField ID="SelectedId_HiddenField" runat="server" />
    <asp:HiddenField ID="SelectedNode_HiddenField" runat="server" />
    <%-- NodeFilter --%>
    <asp:HiddenField ID="IsFilteredWithoutHit_HiddenField" runat="server" />
    <asp:HiddenField ID="AgazatiJelFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="IrattariTetelFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="IratMetaDefinicioFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="UgytipusFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="EljarasiSzakaszFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="IrattipusFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="hfIsFiltered" runat="server" />
   <%-- /NodeFilter --%>
    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always"
        OnLoad="TreeViewUpdatePanel_Load">
        <ContentTemplate>    
            <asp:Panel ID="Panel1" runat="server" Visible="true">                                        
                <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="false"
                    OnTreeNodeExpanded="TreeView1_TreeNodeExpanded" OnTreeNodeCollapsed="TreeView1_TreeNodeCollapsed"
                    OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"
                    OnTreeNodePopulate="TreeView1_TreeNodePopulate" BackColor="White"
                    ShowLines="True"
                    PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip=""
                    SkipLinkText="">
                    <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                </asp:TreeView>
            </asp:Panel>     
        </ContentTemplate>
    </asp:UpdatePanel>
