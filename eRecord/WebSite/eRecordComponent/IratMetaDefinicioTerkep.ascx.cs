//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eUIControls;

public partial class eRecordComponent_IratMetaDefinicioTerkep : System.Web.UI.UserControl
{
    private const string Root_Symbolic_Id = "root"; // A root node �rt�ke, csak a megtal�lhat�s�g kedv��rt, nincs m�g�tte adat
    private const string strSzignalasTipusa = "<b>Szign�l�s: </b>";
    private const string strJavasoltFelelos = "<b>Jav.felel�s: </b>";
    public enum NodeType { Unknown, Root, AgazatiJelek, IraIrattariTetelek, IratMetaDefinicio, EljarasiSzakasz, Irattipus, Custom };
    public const char NodeIdSeparatorChar = '_';
    public char[] NodeIdSeparator = new char[1];

    public enum CustomNodePositionType { BeforeOthers, AfterOthers };

    //private string ItemFormatString = "<div style='width: 100%; word-wrap: break-word;'><span style='width: 30px'>{0}&nbsp;</span><span style='width: 20%;word-wrap: break-word;'>{1}</span><span style='width: 0%; word-wrap: break-word;'>{2}</span></div>";

    private string ItemFormatString = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>{0}&nbsp;</td><td style='width: 500px;'>{1}</td><td style='width: 100%;'>{2}</td></tr></table>";
    private string ClassFormatString = " class='{0}'";

    private String[] _NodeTypeText = { "Ismeretlen", "Iratt�ri terv", "�gazati jel", "Iratt�ri t�tel", "�gyt�pus", "Elj�r�si szakasz", "Iratt�pus", "Felhaszn�l�i t�pus" };

    private String[] _NodeTypeClass = { "", "", "", "", "", "", "", "" };

    #region Properties

    private eErrorPanel _ErrorPanel;

    public eErrorPanel ErrorPanel
    {
        get { return _ErrorPanel; }
        set { _ErrorPanel = value; }
    }

    private UpdatePanel _ErrorUpdatePanel;

    public UpdatePanel ErrorUpdatePanel
    {
        get { return _ErrorUpdatePanel; }
        set { _ErrorUpdatePanel = value; }
    }

    public String SelectedId
    {
        get { return SelectedId_HiddenField.Value; }
        set { SelectedId_HiddenField.Value = value; }
    }

    public String SelectedNode
    {
        get { return SelectedNode_HiddenField.Value; }
        set { SelectedNode_HiddenField.Value = value; }
    }

    private string _Command = CommandName.Modify;

    public String Command
    {
        get { return _Command; }
        set { _Command = value; }
    }

    public UpdatePanel UpdatePanel
    {
        get { return TreeViewUpdatePanel; }
        set { TreeViewUpdatePanel = value; }
    }

    public char ValuePathSeparator
    {
        get { return TreeView1.PathSeparator; }
        set { TreeView1.PathSeparator = value; }
    }

    public Unit Width
    {
        get { return Panel1.Width; }
        set { Panel1.Width = value; }
    }

    public Unit Height
    {
        get { return Panel1.Height; }
        set { Panel1.Height = value; }
    }

    public TreeView TreeView
    {
        get { return TreeView1; }
        set { TreeView1 = value; }
    }

    public bool IsFilteredWithoutHit
    {
        get { return (IsFilteredWithoutHit_HiddenField.Value == "1" ? true : false); }
        set { IsFilteredWithoutHit_HiddenField.Value = (value == true ? "1" : "0"); }
    }

    public string AgazatiJelFilter
    {
        get { return AgazatiJelFilter_HiddenField.Value; }
        set { AgazatiJelFilter_HiddenField.Value = value; }
    }

    public string IrattariTetelFilter
    {
        get { return IrattariTetelFilter_HiddenField.Value; }
        set { IrattariTetelFilter_HiddenField.Value = value; }
    }

    // az Ugytipus, EljarasiSzakasz, Irattipus �sszef�z�se
    public string IratMetaDefinicioFilter
    {
        get
        {
            string[] filters = new string[3];
            filters[0] = UgytipusFilter_HiddenField.Value;
            filters[1] = EljarasiSzakaszFilter_HiddenField.Value;
            filters[2] = IrattipusFilter_HiddenField.Value;

            String Separator = ",";

            // �res elemek elt�vol�t�sa
            String strResult = String.Join(Separator, String.Join(Separator, filters).Split(Separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            return strResult;
        }
    }

    public string UgytipusFilter
    {
        get { return UgytipusFilter_HiddenField.Value; }
        set { UgytipusFilter_HiddenField.Value = value; }
    }

    public string EljarasiSzakaszFilter
    {
        get { return EljarasiSzakaszFilter_HiddenField.Value; }
        set { EljarasiSzakaszFilter_HiddenField.Value = value; }
    }

    public string IrattipusFilter
    {
        get { return IrattipusFilter_HiddenField.Value; }
        set { IrattipusFilter_HiddenField.Value = value; }
    }

    public bool IsFiltered
    {
        get
        {
            return hfIsFiltered.Value == "1";
        }
        set
        {
            hfIsFiltered.Value = value ? "1" : "0";
        }
    }

    public Type SearchObjectType;

    private IrattariTervHierarchiaSearch SearchObject
    {
        get
        {
            if (SearchObjectType == typeof(IrattariTervHierarchiaSearch))
            {
                if (Search.IsSearchObjectInSession(Page, typeof(IrattariTervHierarchiaSearch)))
                {
                    IrattariTervHierarchiaSearch irattariTervHierarchiaSearch = (IrattariTervHierarchiaSearch)Search.GetSearchObject(Page, new IrattariTervHierarchiaSearch());
                    return irattariTervHierarchiaSearch;
                }
            }
            return null;
        }
    }

    private void CopyFieldExpression(Field fieldSource, Field fieldDest)
    {
        fieldDest.Operator = fieldSource.Operator;
        fieldDest.Group = fieldSource.Group;
        fieldDest.GroupOperator = fieldSource.GroupOperator;
        fieldDest.Value = fieldSource.Value;
        fieldDest.ValueTo = fieldSource.ValueTo;
    }


    private string selectedAgazatiJelKod;
    private void SetSelectedAgazatiJelKod(DataRow selectedIratMetaDefinicio)
    {
        selectedAgazatiJelKod = selectedIratMetaDefinicio["AgazatiJel_Kod"].ToString();
    }
    private string selectedUgyKorKod;
    private void SetSelectedUgyKorKod(DataRow selectedIratMetaDefinicio)
    {
        selectedUgyKorKod = selectedIratMetaDefinicio["UgykorKod"].ToString();
    }
    private string selectedUgyTipusKod;
    private void SetSelectedUgyTipusKod(DataRow selectedIratMetaDefinicio)
    {
        selectedUgyTipusKod = selectedIratMetaDefinicio["Ugytipus"].ToString();
    }
    private string selectedEljarasiSzakaszKod;
    private void SetSelectedEljarasiSzakaszKod(DataRow selectedIratMetaDefinicio)
    {
        selectedEljarasiSzakaszKod = selectedIratMetaDefinicio["EljarasiSzakaszKod"].ToString();
    }
    private string selectedIratTipusKod;
    private void SetSelectedIratTipusKod(DataRow selectedIratMetaDefinicio)
    {
        selectedIratTipusKod = selectedIratMetaDefinicio["Irattipus"].ToString();
    }

    private NodeType selectedNodeType
    {
        get
        {
            NodeType _selectedNodeType = NodeType.Unknown;
            if (!String.IsNullOrEmpty(selectedAgazatiJelKod))
            {
                if (!String.IsNullOrEmpty(selectedUgyKorKod))
                {
                    if (!String.IsNullOrEmpty(selectedUgyTipusKod))
                    {
                        if (!String.IsNullOrEmpty(selectedEljarasiSzakaszKod))
                        {
                            if (!String.IsNullOrEmpty(selectedIratTipusKod))
                            {
                                _selectedNodeType = NodeType.Irattipus;
                            }
                            else
                            {
                                _selectedNodeType = NodeType.EljarasiSzakasz;
                            }
                        }
                        else
                        {
                            _selectedNodeType = NodeType.IratMetaDefinicio;
                        }
                    }
                    else
                    {
                        _selectedNodeType = NodeType.IraIrattariTetelek;
                    }
                }
                else
                {
                    _selectedNodeType = NodeType.AgazatiJelek;
                }
            }
            return _selectedNodeType;
        }
    }

    private void SetSelectedNode(TreeNode selectedNode)
    {
        selectedNode.Select();
        TreeView1_SelectedNodeChanged(TreeView1, EventArgs.Empty);
    }

    private int selectedNodePosition = 0;

    public int SelectedNodePosition
    {
        get { return selectedNodePosition; }
        set { selectedNodePosition = value; }
    }
    private const int nodeHeight = 20;

    #endregion Properties

    #region StoredInDictionary

    public Boolean StoredInDictionaryAgazatiJelek(String Id)
    {
        AgazatiJelekItem agazatiJelekItem = null;
        AgazatiJelekDictionary.TryGetValue(Id, out agazatiJelekItem);
        return (agazatiJelekItem == null ? false : true);
    }

    public String GetParentIdFromDictionaryIraIrattariTetelek(String Id)
    {
        IraIrattariTetelekItem iraIrattariTetelekItem = null;
        IraIrattariTetelekDictionary.TryGetValue(Id, out iraIrattariTetelekItem);
        return (iraIrattariTetelekItem == null ? null : iraIrattariTetelekItem.AgazatiJel_Id);
    }

    public String GetIratMetaDefinicioIdFromDictionaryIraIrattariTetelek(String Id)
    {
        IraIrattariTetelekItem iraIrattariTetelekItem = null;
        IraIrattariTetelekDictionary.TryGetValue(Id, out iraIrattariTetelekItem);
        return (iraIrattariTetelekItem == null ? null : iraIrattariTetelekItem.IratMetaDefinicio_Id);
    }

    public String GetParentIdFromDictionaryIratMetaDefinicio(String Id)
    {
        IratMetaDefinicioItem iratMetaDefinicioItem = null;
        IratMetaDefinicioDictionary.TryGetValue(Id, out iratMetaDefinicioItem);
        return (iratMetaDefinicioItem == null ? null : iratMetaDefinicioItem.Ugykor_Id);
    }

    public String GetParentIdFromDictionaryEljarasiSzakasz(String Id)
    {
        IratMetaDefinicioItem iratMetaDefinicioItem = null;
        EljarasiSzakaszDictionary.TryGetValue(Id, out iratMetaDefinicioItem);
        return (iratMetaDefinicioItem == null ? null : iratMetaDefinicioItem.IratMetadefinicio_Id_Szulo);
    }

    public String GetParentIdFromDictionaryIrattipus(String Id)
    {
        IratMetaDefinicioItem iratMetaDefinicioItem = null;
        IrattipusDictionary.TryGetValue(Id, out iratMetaDefinicioItem);
        return (iratMetaDefinicioItem == null ? null : iratMetaDefinicioItem.IratMetadefinicio_Id_Szulo);
    }

    public String GetParentIdFromDictionaryCustom(String Id)
    {
        CustomTreeNodeItem customTreeNodeItem = null;
        CustomTreeNodesDictionary.TryGetValue(Id, out customTreeNodeItem);
        return (customTreeNodeItem == null ? null : customTreeNodeItem.ParentId);
    }

    public String GetParentNodeTypeNameFromDictionaryCustom(String Id)
    {
        CustomTreeNodeItem customTreeNodeItem = null;
        CustomTreeNodesDictionary.TryGetValue(Id, out customTreeNodeItem);
        return (customTreeNodeItem == null ? null : customTreeNodeItem.ParentNodeTypeName);
    }

    public String GetNodeTypeNameFromDictionaryCustom(String Id)
    {
        CustomTreeNodeItem customTreeNodeItem = null;
        CustomTreeNodesDictionary.TryGetValue(Id, out customTreeNodeItem);
        return (customTreeNodeItem == null ? null : customTreeNodeItem.NodeTypeName);
    }

    public bool IsExpanded(String NodeValue)
    {
        bool isExpaded = false;
        if (ViewState["ExpandedStateDictionary"] != null)
        {
            ExpandedStateDictionary = (System.Collections.Generic.Dictionary<String, bool>)ViewState["ExpandedStateDictionary"];
        }
        if (ExpandedStateDictionary.ContainsKey(NodeValue))
        {
            ExpandedStateDictionary.TryGetValue(NodeValue, out isExpaded);
        }
        return isExpaded;
    }
    #endregion StoredInDictionary

    #region dictionary item oszt�lyok

    [Serializable]
    private class AgazatiJelekItem
    {
        private string _Id;
        private string _Kod;
        private string _Nev;

        public AgazatiJelekItem()
        {
            _Id = "";
            _Kod = "";
            _Nev = "";
        }

        public AgazatiJelekItem(string Id, string Kod, string Nev)
        {
            _Id = Id;
            _Kod = Kod;
            _Nev = Nev;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }

        public string Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
        #endregion properties

    }

    [Serializable]
    private class IraIrattariTetelekItem
    {
        private string _Id;
        private string _AgazatiJel_Id;
        private string _IrattariTetelszam;
        private string _Nev;
        // elvben minden IraIrattariTelek rekordhoz tartozik egy IratMetaDefinicio rekord, ahol az �gyt�pus NULL
        private string _IratMetaDefinicio_Id;

        public IraIrattariTetelekItem()
        {
            _Id = "";
            _AgazatiJel_Id = "";
            _IrattariTetelszam = "";
            _Nev = "";
            _IratMetaDefinicio_Id = "";
        }

        public IraIrattariTetelekItem(string Id, string AgazatiJel_Id, string IrattariTetelszam, string Nev)
        {
            _Id = Id;
            _AgazatiJel_Id = AgazatiJel_Id;
            _IrattariTetelszam = IrattariTetelszam;
            _Nev = Nev;
            _IratMetaDefinicio_Id = "";
        }

        public IraIrattariTetelekItem(string Id, string AgazatiJel_Id, string IrattariTetelszam, string Nev, string IratMetaDefinicio_Id)
        {
            _Id = Id;
            _AgazatiJel_Id = AgazatiJel_Id;
            _IrattariTetelszam = IrattariTetelszam;
            _Nev = Nev;
            _IratMetaDefinicio_Id = IratMetaDefinicio_Id;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string AgazatiJel_Id
        {
            get { return _AgazatiJel_Id; }
            set { _AgazatiJel_Id = value; }
        }

        public string IrattariTetelszam
        {
            get { return _IrattariTetelszam; }
            set { _IrattariTetelszam = value; }
        }

        public string Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }

        public string IratMetaDefinicio_Id
        {
            get { return _IratMetaDefinicio_Id; }
            set { _IratMetaDefinicio_Id = value; }
        }

        #endregion properties

    }

    [Serializable]
    private class IratMetaDefinicioItem
    {
        private string _Id;
        private string _Ugykor_Id;
        private string _UgykorKod;
        private string _Ugytipus;
        private string _UgytipusNev;
        private string _EljarasiSzakasz;
        private string _Irattipus;
        private string _IratMetadefinicio_Id_Szulo; // a t�bl�zat maga nem hierarchikus, az�rt itt t�roljuk a "kinyomozott" sz�l�t

        public IratMetaDefinicioItem()
        {
            _Id = "";
            _Ugykor_Id = "";
            _Ugytipus = "";
            _UgytipusNev = "";
            _IratMetadefinicio_Id_Szulo = "";
            _EljarasiSzakasz = "";
            _Irattipus = "";
        }

        public IratMetaDefinicioItem(string Id, string Ugykor_Id, string UgykorKod, string Ugytipus,
            string UgytipusNev, string IratMetadefinicio_Id_Szulo, string EljarasiSzakasz, string Irattipus)
        {
            _Id = Id;
            _Ugykor_Id = Ugykor_Id;
            _UgykorKod = UgykorKod;
            _Ugytipus = Ugytipus;
            _UgytipusNev = UgytipusNev;
            _IratMetadefinicio_Id_Szulo = IratMetadefinicio_Id_Szulo;
            _EljarasiSzakasz = EljarasiSzakasz;
            _Irattipus = Irattipus;
        }

        public IratMetaDefinicioItem(string Id, string Ugykor_Id, string UgykorKod, string Ugytipus,
            string UgytipusNev, string IratMetadefinicio_Id_Szulo, string EljarasiSzakasz)
        {
            _Id = Id;
            _Ugykor_Id = Ugykor_Id;
            _UgykorKod = UgykorKod;
            _Ugytipus = Ugytipus;
            _UgytipusNev = UgytipusNev;
            _IratMetadefinicio_Id_Szulo = IratMetadefinicio_Id_Szulo;
            _EljarasiSzakasz = EljarasiSzakasz;
            _Irattipus = "";
        }

        public IratMetaDefinicioItem(string Id, string Ugykor_Id, string UgykorKod, string Ugytipus, string UgytipusNev, string IratMetadefinicio_Id_Szulo)
        {
            _Id = Id;
            _Ugykor_Id = Ugykor_Id;
            _UgykorKod = UgykorKod;
            _Ugytipus = Ugytipus;
            _UgytipusNev = UgytipusNev;
            _IratMetadefinicio_Id_Szulo = IratMetadefinicio_Id_Szulo;
            _EljarasiSzakasz = "";
            _Irattipus = "";
        }

        // _IratMetadefinicio_Id_Szulo n�lk�li konstruktor, kompatibilis a kor�bbi, nem hierarchikus megold�ssal
        public IratMetaDefinicioItem(string Id, string Ugykor_Id, string UgykorKod, string Ugytipus, string UgytipusNev)
        {
            _Id = Id;
            _Ugykor_Id = Ugykor_Id;
            _UgykorKod = UgykorKod;
            _Ugytipus = Ugytipus;
            _UgytipusNev = UgytipusNev;
            _IratMetadefinicio_Id_Szulo = "";
            _EljarasiSzakasz = "";
            _Irattipus = "";
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Ugykor_Id
        {
            get { return _Ugykor_Id; }
            set { _Ugykor_Id = value; }
        }

        public string UgykorKod
        {
            get { return _UgykorKod; }
            set { _UgykorKod = value; }
        }

        public string Ugytipus
        {
            get { return _Ugytipus; }
            set { _Ugytipus = value; }
        }

        public string UgytipusNev
        {
            get { return _UgytipusNev; }
            set { _UgytipusNev = value; }
        }

        public string IratMetadefinicio_Id_Szulo
        {
            get { return _IratMetadefinicio_Id_Szulo; }
            set { _IratMetadefinicio_Id_Szulo = value; }
        }

        public string EljarasiSzakasz
        {
            get { return _EljarasiSzakasz; }
            set { _EljarasiSzakasz = value; }
        }

        public string Irattipus
        {
            get { return _Irattipus; }
            set { _Irattipus = value; }
        }

        #endregion properties

    }

    [Serializable]
    private class CustomTreeNodeItem
    {
        private string _Id;
        private string _Text;
        private string _NodeTypeName;// megadhat� a customt�l elt�r� �rt�k a megk�l�nb�ztet�shez
        private string _ParentId;
        private string _ParentNodeTypeName;

        public CustomTreeNodeItem()
        {
            _Id = "";
            _Text = "";
            _NodeTypeName = "Custom";
            _ParentId = "";
            _ParentNodeTypeName = "";
        }

        public CustomTreeNodeItem(string Id, string Text, string ParentId, string ParentNodeTypeName)
        {
            _Id = Id;
            _Text = Text;
            _ParentId = ParentId;
            _ParentNodeTypeName = ParentNodeTypeName;
        }

        public CustomTreeNodeItem(string Id, string Text, string NodeTypeName, string ParentId, string ParentNodeTypeName)
            : this(Id, Text, ParentId, ParentNodeTypeName)
        {
            _NodeTypeName = NodeTypeName;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public string NodeTypeName
        {
            get { return _NodeTypeName; }
            set { _NodeTypeName = value; }
        }

        public string ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        public string ParentNodeTypeName
        {
            get { return _ParentNodeTypeName; }
            set { _ParentNodeTypeName = value; }
        }

        #endregion properties

    }
    #endregion dictionary item oszt�lyok

    #region NodeTypeUtils
    public String ConcatNodeType(NodeType nodeType, String Id)
    {
        NodeIdSeparatorChar.ToString().ToCharArray();
        return String.Concat(GetNameByNodeType(nodeType), NodeIdSeparator[0], Id);
    }


    public String ConcatNodeType(String nodeTypeName, String Id)
    {
        NodeIdSeparatorChar.ToString().ToCharArray();
        return String.Concat(nodeTypeName, NodeIdSeparator[0], Id);
    }

    public NodeType GetNodeTypeByName(string name)
    {
        NodeType nodeType;
        try
        {
            nodeType = (NodeType)Enum.Parse(typeof(NodeType), name);
            return nodeType;
        }
        catch
        {
            return NodeType.Unknown;
        }

    }

    public String GetNameByNodeType(NodeType nodeType)
    {
        return Enum.GetName(typeof(NodeType), nodeType);
    }

    public NodeType ExtractNodeTypeFromNodeTypeName(String nodeTypeName)
    {
        String namePart = String.Empty;
        String[] parts = nodeTypeName.Split(NodeIdSeparator, StringSplitOptions.RemoveEmptyEntries);
        if (parts == null) return NodeType.Unknown;

        if (parts.Length > 0)
        {
            namePart = parts[0];
        }

        return GetNodeTypeByName(namePart);
    }

    public String ExtractNodeIdFromNodeTypeName(String nodeTypeName)
    {
        String idPart = String.Empty;
        String[] parts = nodeTypeName.Split(NodeIdSeparator, StringSplitOptions.RemoveEmptyEntries);

        if (parts.Length > 1)
        {
            idPart = parts[1];
        }

        return idPart;
    }

    public String ExtractNodeNameFromNodeTypeName(String nodeTypeName)
    {
        String namePart = String.Empty;
        String[] parts = nodeTypeName.Split(NodeIdSeparator, StringSplitOptions.RemoveEmptyEntries);
        if (parts == null) return String.Empty;

        if (parts.Length > 0)
        {
            namePart = parts[0];
        }

        return namePart;
    }

    public String GetNodeTypeText(NodeType nodeType)
    {
        return _NodeTypeText[(int)nodeType];
    }

    public void SetNodeTypeText(NodeType nodeType, String name)
    {
        _NodeTypeText[(int)nodeType] = name;
    }

    public String GetNodeTypeClass(NodeType nodeType)
    {
        return _NodeTypeClass[(int)nodeType];
    }

    public void SetNodeTypeClass(NodeType nodeType, String classname)
    {
        _NodeTypeClass[(int)nodeType] = classname;
    }

    #endregion NodeTypeUtils


    #region FilterUtils

    public void SetNodeFilter(bool isFilteredWithoutHit, string AgazatiJel_Ids, string IrattariTetel_Ids//, string IratMetaDefinicio_Ids
                                , string Ugytipus_Ids, string EljarasiSzakasz_Ids, string Irattipus_Ids)
    {
        IsFilteredWithoutHit = isFilteredWithoutHit;
        AgazatiJelFilter = AgazatiJel_Ids;
        IrattariTetelFilter = IrattariTetel_Ids;
        //IratMetaDefinicioFilter = IratMetaDefinicio_Ids;
        UgytipusFilter = Ugytipus_Ids;
        EljarasiSzakaszFilter = EljarasiSzakasz_Ids;
        IrattipusFilter = Irattipus_Ids;
        IsFiltered = true;
    }

    public void ClearNodeFilter()
    {
        IsFilteredWithoutHit = false;
        AgazatiJelFilter = "";
        IrattariTetelFilter = "";
        //IratMetaDefinicioFilter = "";
        UgytipusFilter = "";
        EljarasiSzakaszFilter = "";
        IrattipusFilter = "";
        IsFiltered = false;
    }

    /// <summary>
    /// Egy (szimpla id�z�jelek k�z�tt l�v�, vagy csak felsorolt) elemekb�l (Id-k) �ll�,
    /// vessz�kkel elv�lasztott list�ban megkeresi az �tadott Id-t, �s visszaadja a sorsz�m�t
    /// </summary>
    /// <param name="strElement">A keresett elem (Id)</param>
    /// <param name="strFilter">Az elemek vessz�vel tagolt sorozata, az egyes Id-k lehetnek szimpla id�z�jelben is.</param>
    /// <returns>-1, ha az elem nem tal�lhat�, egy�bk�nt az els� el�fordul�s indexe</returns>
    public static int findInFilter(string strElement, string strFilter)
    {
        char[] Separators = new char[1];
        Separators[0] = ',';

        int iPosition = -1;

        string[] arrayFilter = strFilter.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
        int i = -1;
        foreach (String item in arrayFilter)
        {
            i++;
            if (item.Replace("'", "") == strElement)
            {
                iPosition = i;
                break;
            }
        }

        return iPosition;

    }

    /// <summary>
    /// Egy (szimpla id�z�jelek k�z�tt l�v�, vagy csak felsorolt) elemekb�l (Id-k) �ll�,
    /// vessz�kkel elv�lasztott list�ban megvizsg�lja, hogy az �tadott Id szerepel-e benne
    /// </summary>
    /// <param name="strElement">A keresett elem (Id)</param>
    /// <param name="strFilter">Az elemek vessz�vel tagolt sorozata, az egyes Id-k lehetnek szimpla id�z�jelben is.</param>
    /// <returns>False, ha az elem nem tal�lhat�, egy�bk�nt True</returns>
    public static bool isInFilter(string strElement, string strFilter)
    {
        return (findInFilter(strElement, strFilter) > -1 ? true : false);
    }

    #endregion FilterUtils

    #region Base Tree Dictionary

    System.Collections.Generic.Dictionary<String, AgazatiJelekItem> AgazatiJelekDictionary = new System.Collections.Generic.Dictionary<string, AgazatiJelekItem>();
    System.Collections.Generic.Dictionary<String, IraIrattariTetelekItem> IraIrattariTetelekDictionary = new System.Collections.Generic.Dictionary<string, IraIrattariTetelekItem>();
    System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem> IratMetaDefinicioDictionary = new System.Collections.Generic.Dictionary<string, IratMetaDefinicioItem>();
    System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem> EljarasiSzakaszDictionary = new System.Collections.Generic.Dictionary<string, IratMetaDefinicioItem>();
    System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem> IrattipusDictionary = new System.Collections.Generic.Dictionary<string, IratMetaDefinicioItem>();

    System.Collections.Generic.Dictionary<String, CustomTreeNodeItem> CustomTreeNodesDictionary = new System.Collections.Generic.Dictionary<string, CustomTreeNodeItem>();

    System.Collections.Generic.Dictionary<String, bool> ExpandedStateDictionary = new System.Collections.Generic.Dictionary<String, bool>();
    #endregion

    #region public Properties

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }


    #endregion

    #region IconsProperty
    private string _IconPathRoot = "images/hu/egyeb/treeview_root.gif";

    public string IconPathRoot
    {
        get { return _IconPathRoot; }
        set { _IconPathRoot = value; }
    }

    private string _IconPathAgazatiJelek = "images/hu/egyeb/treeview_agazatijel.gif";

    public string IconPathAgazatiJelek
    {
        get { return _IconPathAgazatiJelek; }
        set { _IconPathAgazatiJelek = value; }
    }

    private string _IconPathIraIrattariTetelek = "images/hu/egyeb/treeview_irattaritetel.gif";

    public string IconPathIraIrattariTetelek
    {
        get { return _IconPathIraIrattariTetelek; }
        set { _IconPathIraIrattariTetelek = value; }
    }

    private string _IconPathIratMetaDefinicio = "images/hu/egyeb/treeview_iratmetadefinicio.gif";

    public string IconPathIratMetaDefinicio
    {
        get { return _IconPathIratMetaDefinicio; }
        set { _IconPathIratMetaDefinicio = value; }
    }

    private string _IconPathEljarasiSzakasz = "images/hu/egyeb/treeview_eljarasiszakasz.gif";

    public string IconPathEljarasiSzakasz
    {
        get { return _IconPathEljarasiSzakasz; }
        set { _IconPathEljarasiSzakasz = value; }
    }

    private string _IconPathIrattipus = "images/hu/egyeb/treeview_irattipus.gif";

    public string IconPathIrattipus
    {
        get { return _IconPathIrattipus; }
        set { _IconPathIrattipus = value; }
    }

    private string _IconPathCustomTreeNode = "images/hu/egyeb/treeview_customtreenode.gif";

    public string IconPathCustomTreeNode
    {
        get { return _IconPathCustomTreeNode; }
        set { _IconPathCustomTreeNode = value; }
    }

    private string _IconAltRoot = "Iratt�ri terv (gy�k�r)";

    public string IconAltRoot
    {
        get { return _IconAltRoot; }
        set { _IconAltRoot = value; }
    }

    private string _IconAltAgazatiJelek = "�gazati jel";

    public string IconAltAgazatiJelek
    {
        get { return _IconAltAgazatiJelek; }
        set { _IconAltAgazatiJelek = value; }
    }

    private string _IconAltIraIrattariTetelek = "Iratt�ri t�tel";

    public string IconAltIraIrattariTetelek
    {
        get { return _IconAltIraIrattariTetelek; }
        set { _IconAltIraIrattariTetelek = value; }
    }

    private string _IconAltIratMetaDefinicio = "�gyt�pus";

    public string IconAltIratMetaDefinicio
    {
        get { return _IconAltIratMetaDefinicio; }
        set { _IconAltIratMetaDefinicio = value; }
    }

    private string _IconAltEljarasiSzakasz = "Elj�r�si szakasz";

    public string IconAltEljarasiSzakasz
    {
        get { return _IconAltEljarasiSzakasz; }
        set { _IconAltEljarasiSzakasz = value; }
    }

    private string _IconAltIrattipus = "Iratt�pus";

    public string IconAltIrattipus
    {
        get { return _IconAltIrattipus; }
        set { _IconAltIrattipus = value; }
    }

    private string _IconAltCustomTreeNode = "";

    public string IconAltCustomTreeNode
    {
        get { return _IconAltCustomTreeNode; }
        set { _IconAltCustomTreeNode = value; }
    }
    #endregion IconsProperty

    #region Custom Node Position Properties
    private CustomNodePositionType _CustomNodePosition = CustomNodePositionType.AfterOthers;

    public CustomNodePositionType CustomNodePosition
    {
        get { return _CustomNodePosition; }
        set { _CustomNodePosition = value; }
    }

    #endregion Custom Node Position Properties
    // *******************************************************
    #region Events
    public event EventHandler SelectedNodeChanged;
    public event TreeNodeEventHandler TreeNodePopulate;
    public event IMDTerkepEventHandler TreeViewLoading;
    public event IMDTerkepEventHandler RefreshingSelectedNode;
    public event IMDTerkepEventHandler RefreshingSelectedNodeChildren;

    protected virtual void OnTreeViewLoading(IMDTerkepEventArgs e)
    {
        if (TreeViewLoading != null)
        {
            TreeViewLoading(this.TreeView1, e);
        }
    }

    protected virtual void OnRefreshingSelectedNode(IMDTerkepEventArgs e)
    {
        if (RefreshingSelectedNode != null)
        {
            RefreshingSelectedNode(this.TreeView1, e);
        }
    }

    protected virtual void OnRefreshingSelectedNodeChildren(IMDTerkepEventArgs e)
    {
        if (RefreshingSelectedNodeChildren != null)
        {
            RefreshingSelectedNodeChildren(this.TreeView1, e);
        }
    }
    #endregion Events
    // *******************************************************
    #region Event Args
    public delegate void IMDTerkepEventHandler(object sender, IMDTerkepEventArgs e);
    public class IMDTerkepEventArgs : EventArgs
    {
        public String nodeId;
        public NodeType nodeType;
        //public String parentNodeId;
        //public NodeType parentNodeType;
        public String nodeTypeName; // Custom noden�l ezzel tehet� megk�l�nb�ztet�s

        public IMDTerkepEventArgs() { }

        public IMDTerkepEventArgs(String nodeId, NodeType nodeType)
        {
            this.nodeId = nodeId;
            this.nodeType = nodeType;
        }

        public IMDTerkepEventArgs(String nodeId, NodeType nodeType, String nodeTypeName)
            : this(nodeId, nodeType)
        {
            this.nodeTypeName = nodeTypeName;
        }

    }
    #endregion
    // *******************************************************



    protected void Page_Init(object sender, EventArgs e)
    {
        NodeIdSeparator[0] = NodeIdSeparatorChar;
        if (!IsPostBack)
        {
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (!String.IsNullOrEmpty(id))
            {
                ExecParam xpm = UI.SetExecParamDefault(Page);
                EREC_IratMetaDefinicioSearch sch = new EREC_IratMetaDefinicioSearch();
                sch.Id.Value = id;
                sch.Id.Operator = Query.Operators.equals;
                EREC_IratMetaDefinicioService svc = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                Result res = svc.GetAllWithExtension(xpm, sch);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
                }
                if (res.Ds != null && res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count > 0)
                {
                    DataRow selectedIratMetaDefinicio = res.Ds.Tables[0].Rows[0];
                    SetSelectedAgazatiJelKod(selectedIratMetaDefinicio);
                    SetSelectedUgyKorKod(selectedIratMetaDefinicio);
                    SetSelectedUgyTipusKod(selectedIratMetaDefinicio);
                    SetSelectedEljarasiSzakaszKod(selectedIratMetaDefinicio);
                    SetSelectedIratTipusKod(selectedIratMetaDefinicio);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterScrollManagerScript(Page);

        if (IsPostBack)
        {
            //ViewState -bol visszatolti az elmentett statuszokat
            try
            {
                if (ViewState["AgazatiJelekDictionary"] != null)
                {
                    AgazatiJelekDictionary = (System.Collections.Generic.Dictionary<String, AgazatiJelekItem>)ViewState["AgazatiJelekDictionary"];
                }
                if (ViewState["IraIrattariTetelekDictionary"] != null)
                {
                    IraIrattariTetelekDictionary = (System.Collections.Generic.Dictionary<String, IraIrattariTetelekItem>)ViewState["IraIrattariTetelekDictionary"];
                }
                if (ViewState["IratMetaDefinicioDictionary"] != null)
                {
                    IratMetaDefinicioDictionary = (System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem>)ViewState["IratMetaDefinicioDictionary"];
                }
                if (ViewState["EljarasiSzakaszDictionary"] != null)
                {
                    EljarasiSzakaszDictionary = (System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem>)ViewState["EljarasiSzakaszDictionary"];
                }
                if (ViewState["IrattipusDictionary"] != null)
                {
                    IrattipusDictionary = (System.Collections.Generic.Dictionary<String, IratMetaDefinicioItem>)ViewState["IrattipusDictionary"];
                }

                if (ViewState["CustomTreeNodesDictionary"] != null)
                {
                    CustomTreeNodesDictionary = (System.Collections.Generic.Dictionary<String, CustomTreeNodeItem>)ViewState["CustomTreeNodesDictionary"];
                }

                //// �llapot
                //if (ViewState["ExpandedStateDictionary"] != null)
                //{
                //    ExpandedStateDictionary = (System.Collections.Generic.Dictionary<String, bool>)ViewState["ExpandedStateDictionary"];
                //}
            }
            catch
            { }
        }
        else
        {
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
    }


    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshSelectedNode:// m�dos�t�s
                        RefreshSelectedNode();
                        break;
                    case EventArgumentConst.refreshSelectedNodeChildren: // gyerek felv�tele
                        RefreshSelectedNodeChildren();
                        ExpandSelectedNode();
                        break;
                }
            }
        }
    }

    public void ClearTreeView()
    {
        // Dictionary-k t�rl�se:
        AgazatiJelekDictionary.Clear();
        IraIrattariTetelekDictionary.Clear();
        IratMetaDefinicioDictionary.Clear();
        EljarasiSzakaszDictionary.Clear();
        IrattipusDictionary.Clear();
        CustomTreeNodesDictionary.Clear();

        TreeView1.Nodes.Clear();

        // add root node if missing
        TreeNode RootNode = UpdateRootNode();
    }


    public void LoadTreeView()
    {

        ViewState["Loaded"] = false;
        if (IsFilteredWithoutHit)
        {
            ClearTreeView();
        }
        else
        {
            LoadTreeView(NodeType.Root, Root_Symbolic_Id);
        }
        ViewState["Loaded"] = true;
    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        TreeView tv = (TreeView)sender;

        if (tv.SelectedNode == null) return;

        SelectedNode = ExtractNodeNameFromNodeTypeName(tv.SelectedValue);
        SelectedId = ExtractNodeIdFromNodeTypeName(tv.SelectedValue);

        SelectedNodeChanged(sender, e);
    }


    public void ExpandSelectedNodeAllLevels()
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.ExpandAll();
        }
    }

    public TreeNode LoadTreeView(NodeType nodeType, String nodeId)
    {
        // Dictionary-k t�rl�se:
        AgazatiJelekDictionary.Clear();
        IraIrattariTetelekDictionary.Clear();
        IratMetaDefinicioDictionary.Clear();
        EljarasiSzakaszDictionary.Clear();
        IrattipusDictionary.Clear();
        CustomTreeNodesDictionary.Clear();

        TreeView1.Nodes.Clear();

        // add root node if missing
        TreeNode RootNode = UpdateRootNode();

        switch (nodeType)
        {
            case NodeType.Custom:
                {
                    // gener�lunk egy esem�nyt, az als� szint felt�lt�shez
                    IMDTerkepEventArgs e = new IMDTerkepEventArgs(nodeId, nodeType);
                    OnTreeViewLoading(e);
                    // Custom node bet�lt�s�t az �gyf�l v�gzi
                    TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                    return new TreeNode();
                }

            case NodeType.Irattipus:
                {
                    EREC_IratMetaDefinicioService imds = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    ExecParam imdEP = new ExecParam();
                    imdEP.Record_Id = nodeId;
                    Result res = imds.Get(imdEP);

                    EREC_IratMetaDefinicioSearch searchEljarasiSzakasz = new EREC_IratMetaDefinicioSearch();
                    string EljarasiSzakasz = (res.Record as EREC_IratMetaDefinicio).EljarasiSzakasz;
                    string Ugytipus = (res.Record as EREC_IratMetaDefinicio).Ugytipus;
                    string Ugykor_Id = (res.Record as EREC_IratMetaDefinicio).Ugykor_Id;
                    searchEljarasiSzakasz.Irattipus.IsNull();
                    searchEljarasiSzakasz.EljarasiSzakasz.Filter(EljarasiSzakasz);
                    searchEljarasiSzakasz.Ugytipus.Filter(Ugytipus);
                    searchEljarasiSzakasz.Ugykor_Id.Filter(Ugykor_Id);

                    Result res_EljarasiSzakasz = imds.GetAll(imdEP, searchEljarasiSzakasz);

                    if (String.IsNullOrEmpty(res_EljarasiSzakasz.ErrorCode) && res_EljarasiSzakasz.Ds.Tables[0].Rows.Count > 0)
                    {
                        string EljarasiSzakasz_Id = res_EljarasiSzakasz.Ds.Tables[0].Rows[0]["Id"].ToString();

                        EREC_IratMetaDefinicioSearch searchUgytipus = new EREC_IratMetaDefinicioSearch();
                        searchUgytipus.Irattipus.IsNull();
                        searchUgytipus.EljarasiSzakasz.IsNull();
                        searchUgytipus.Ugytipus.Filter(Ugytipus);
                        searchUgytipus.Ugykor_Id.Filter(Ugykor_Id);

                        Result res_Ugytipus = imds.GetAll(imdEP, searchUgytipus);

                        if (String.IsNullOrEmpty(res_Ugytipus.ErrorCode) && res_Ugytipus.Ds.Tables[0].Rows.Count > 0)
                        {
                            string Ugytipus_Id = res_Ugytipus.Ds.Tables[0].Rows[0]["Id"].ToString();

                            EREC_IraIrattariTetelekService iits = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                            ExecParam iitEP = new ExecParam();
                            iitEP.Record_Id = Ugykor_Id;
                            res = iits.Get(iitEP);

                            TreeNode AgazatiJelekNode = AddAgazatiJelekNode(RootNode, (res.Record as EREC_IraIrattariTetelek).AgazatiJel_Id);
                            AgazatiJelekNode.Expand();

                            foreach (TreeNode tn in AgazatiJelekNode.ChildNodes)
                                if (ExtractNodeIdFromNodeTypeName(tn.Value) == Ugykor_Id)
                                {
                                    tn.Expanded = true;
                                    foreach (TreeNode iitTN in tn.ChildNodes)
                                        if (ExtractNodeIdFromNodeTypeName(iitTN.Value) == Ugytipus_Id)
                                        {
                                            iitTN.Expanded = true;
                                            foreach (TreeNode imdTN_Ugytipus in iitTN.ChildNodes)
                                                if (ExtractNodeIdFromNodeTypeName(imdTN_Ugytipus.Value) == EljarasiSzakasz_Id)
                                                {
                                                    imdTN_Ugytipus.Expanded = true;
                                                    foreach (TreeNode imdTN_EljarasiSzakasz in imdTN_Ugytipus.ChildNodes)
                                                        if (ExtractNodeIdFromNodeTypeName(imdTN_EljarasiSzakasz.Value) == nodeId)
                                                        {
                                                            imdTN_EljarasiSzakasz.Selected = true;
                                                            TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                                                        }
                                                }
                                        }
                                }

                        }

                    }
                    return TreeView1.SelectedNode;
                }

            case NodeType.EljarasiSzakasz:
                {
                    EREC_IratMetaDefinicioService imds = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    ExecParam imdEP = new ExecParam();
                    imdEP.Record_Id = nodeId;
                    Result res = imds.Get(imdEP);

                    string Ugytipus = (res.Record as EREC_IratMetaDefinicio).Ugytipus;
                    string Ugykor_Id = (res.Record as EREC_IratMetaDefinicio).Ugykor_Id;

                    EREC_IratMetaDefinicioSearch searchUgytipus = new EREC_IratMetaDefinicioSearch();
                    searchUgytipus.Irattipus.Operator = Query.Operators.isnull;
                    searchUgytipus.EljarasiSzakasz.Operator = Query.Operators.isnull;
                    searchUgytipus.Ugytipus.Value = Ugytipus;
                    searchUgytipus.Ugytipus.Operator = Query.Operators.equals;
                    searchUgytipus.Ugykor_Id.Value = Ugykor_Id;
                    searchUgytipus.Ugykor_Id.Operator = Query.Operators.equals;

                    Result res_Ugytipus = imds.GetAll(imdEP, searchUgytipus);

                    if (String.IsNullOrEmpty(res_Ugytipus.ErrorCode) && res_Ugytipus.Ds.Tables[0].Rows.Count > 0)
                    {
                        string Ugytipus_Id = res_Ugytipus.Ds.Tables[0].Rows[0]["Id"].ToString();

                        EREC_IraIrattariTetelekService iits = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                        ExecParam iitEP = new ExecParam();
                        iitEP.Record_Id = Ugykor_Id;
                        res = iits.Get(iitEP);

                        TreeNode AgazatiJelekNode = AddAgazatiJelekNode(RootNode, (res.Record as EREC_IraIrattariTetelek).AgazatiJel_Id);
                        AgazatiJelekNode.Expand();

                        foreach (TreeNode tn in AgazatiJelekNode.ChildNodes)
                            if (ExtractNodeIdFromNodeTypeName(tn.Value) == Ugykor_Id)
                            {
                                tn.Expanded = true;
                                foreach (TreeNode iitTN in tn.ChildNodes)
                                    if (ExtractNodeIdFromNodeTypeName(iitTN.Value) == Ugytipus_Id)
                                    {
                                        iitTN.Expanded = true;
                                        foreach (TreeNode imdTN_Ugytipus in iitTN.ChildNodes)
                                            if (ExtractNodeIdFromNodeTypeName(imdTN_Ugytipus.Value) == nodeId)
                                            {
                                                imdTN_Ugytipus.Selected = true;
                                                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                                            }
                                    }
                            }

                    }
                    return TreeView1.SelectedNode;
                }

            case NodeType.IratMetaDefinicio:
                {
                    EREC_IratMetaDefinicioService imds = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    ExecParam imdEP = new ExecParam();
                    imdEP.Record_Id = nodeId;
                    Result res = imds.Get(imdEP);
                    string Ugykor_Id = (res.Record as EREC_IratMetaDefinicio).Ugykor_Id;

                    EREC_IraIrattariTetelekService iits = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    ExecParam iitEP = new ExecParam();
                    iitEP.Record_Id = Ugykor_Id;
                    res = iits.Get(iitEP);

                    TreeNode AgazatiJelekNode = AddAgazatiJelekNode(RootNode, (res.Record as EREC_IraIrattariTetelek).AgazatiJel_Id);
                    AgazatiJelekNode.Expand();

                    foreach (TreeNode tn in AgazatiJelekNode.ChildNodes)
                        if (ExtractNodeIdFromNodeTypeName(tn.Value) == Ugykor_Id)
                        {
                            tn.Expanded = true;
                            foreach (TreeNode iitTN in tn.ChildNodes)
                                if (ExtractNodeIdFromNodeTypeName(iitTN.Value) == nodeId)
                                {
                                    iitTN.Selected = true;
                                    TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                                }
                        }
                    return TreeView1.SelectedNode;
                }

            case NodeType.IraIrattariTetelek:
                {
                    EREC_IraIrattariTetelekService iits = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    ExecParam iitEP = new ExecParam();
                    iitEP.Record_Id = nodeId;
                    Result res = iits.Get(iitEP);

                    TreeNode AgazatiJelekNode = AddAgazatiJelekNode(RootNode, (res.Record as EREC_IraIrattariTetelek).AgazatiJel_Id);
                    AgazatiJelekNode.Expand();

                    foreach (TreeNode tn in AgazatiJelekNode.ChildNodes)
                        if (ExtractNodeIdFromNodeTypeName(tn.Value) == nodeId)
                        {
                            tn.Selected = true;
                            TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

                        }

                    return TreeView1.SelectedNode;
                }

            case NodeType.AgazatiJelek:
                {
                    TreeNode AgazatiJelekNode = AddAgazatiJelekNode(RootNode, nodeId);
                    // �gazati jelek szintet kibontjuk
                    //AgazatiJelekNode.Expand();

                    AgazatiJelekNode.Selected = true;
                    TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

                    return TreeView1.SelectedNode;
                }

            case NodeType.Root:
                {
                    // �gazati jelek szintet kibontjuk
                    RootNode.Expand();

                    if (TreeView1.SelectedNode == null)
                    {
                        RootNode.Selected = true;
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                    }

                    return TreeView1.SelectedNode;
                }
        }

        return null;
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        switch (ExtractNodeTypeFromNodeTypeName(e.Node.Value))
        {
            case NodeType.Root:
                AddAgazatiJelekNode(e.Node, "");
                break;
            case NodeType.AgazatiJelek:
                AddIraIrattariTetelekNode(e.Node);
                break;
            case NodeType.IraIrattariTetelek:
                AddIratMetaDefinicioNode(e.Node);
                break;
            case NodeType.IratMetaDefinicio:
                AddEljarasiSzakaszNode(e.Node);
                //TreeNodePopulate(sender, e);
                break;
            case NodeType.EljarasiSzakasz:
                AddIrattipusNode(e.Node);
                //TreeNodePopulate(sender, e);
                break;
            case NodeType.Irattipus:
                //TreeNodePopulate(sender, e);
                break;
            case NodeType.Custom:
                break;
            case NodeType.Unknown:
                break;
            default:
                break;
        }

        TreeNodePopulate(sender, e);
    }

    protected void TreeView1_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        if (ExpandedStateDictionary.ContainsKey(e.Node.Value))
        {
            ExpandedStateDictionary.Remove(e.Node.Value);
        }
        ExpandedStateDictionary.Add(e.Node.Value, true);

        ViewState["ExpandedStateDictionary"] = ExpandedStateDictionary;

        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

    }

    protected void TreeView1_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
        if (ExpandedStateDictionary.ContainsKey(e.Node.Value))
        {
            ExpandedStateDictionary.Remove(e.Node.Value);
        }
        ExpandedStateDictionary.Add(e.Node.Value, false);

        ViewState["ExpandedStateDictionary"] = ExpandedStateDictionary;

        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
    }

    public void DeleteChildrenNodes(TreeNode node)
    {
        if (node != null)
        {
            foreach (TreeNode tn in node.ChildNodes)
            {
                // csak a Dictionaryt �r�ti
                RemoveNodesFromDictionaries(tn);
            }
            // node-ok t�nyleges t�rl�se
            node.ChildNodes.Clear();
        }
    }

    public void DeleteNode(TreeNode node)
    {
        if (node != null)
        {
            if (TreeView1.SelectedNode == node)
            {
                DeleteSelectedNode();
            }
            else
            {
                RemoveNodesFromDictionaries(node);
                node.Parent.ChildNodes.Remove(node);
            }
        }
    }

    public void DeleteSelectedNode()
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeNode ParentNode = TreeView1.SelectedNode.Parent;
            if (ParentNode != null)
            {
                RemoveNodesFromDictionaries(TreeView1.SelectedNode);
                ParentNode.ChildNodes.Remove(TreeView1.SelectedNode);
                ParentNode.Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
        }

    }

    // hierarchikusan t�rli a Dictionary-kb�l a node-oknak megfelel� Id-et
    private void RemoveNodesFromDictionaries(TreeNode treeNode)
    {
        if (treeNode == null) return;

        foreach (TreeNode tn in treeNode.ChildNodes)
        {
            RemoveNodesFromDictionaries(tn);
        }

        if (treeNode.ChildNodes.Count == 0)
        {
            string Id = ExtractNodeIdFromNodeTypeName(treeNode.Value);
            NodeType nodeType = ExtractNodeTypeFromNodeTypeName(treeNode.Value);

            switch (nodeType)
            {
                case NodeType.Root:
                    break;
                case NodeType.AgazatiJelek:
                    if (AgazatiJelekDictionary.ContainsKey(Id))
                        AgazatiJelekDictionary.Remove(Id);
                    break;
                case NodeType.IraIrattariTetelek:
                    if (IraIrattariTetelekDictionary.ContainsKey(Id))
                        IraIrattariTetelekDictionary.Remove(Id);
                    break;
                case NodeType.IratMetaDefinicio:
                    if (IratMetaDefinicioDictionary.ContainsKey(Id))
                        IratMetaDefinicioDictionary.Remove(Id);
                    break;
                case NodeType.EljarasiSzakasz:
                    if (EljarasiSzakaszDictionary.ContainsKey(Id))
                        EljarasiSzakaszDictionary.Remove(Id);
                    break;
                case NodeType.Irattipus:
                    if (IrattipusDictionary.ContainsKey(Id))
                        IrattipusDictionary.Remove(Id);
                    break;
                case NodeType.Custom:
                    if (CustomTreeNodesDictionary.ContainsKey(Id))
                        CustomTreeNodesDictionary.Remove(Id);
                    break;
                default:
                    break;

            }
        }

    }

    private TreeNode AddAgazatiJelekNode(TreeNode RootNode, string AgazatiJel_Id)
    {
        if (IsFilteredWithoutHit) return new TreeNode();

        #region Init
        EREC_AgazatiJelekService erec_AgazatiJelekService = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
        EREC_AgazatiJelekSearch erec_AgazatiJelekSearch = new EREC_AgazatiJelekSearch();
        if (SearchObject != null)
        {
            CopyFieldExpression(SearchObject.ErvKezd, erec_AgazatiJelekSearch.ErvKezd);
            CopyFieldExpression(SearchObject.ErvVege, erec_AgazatiJelekSearch.ErvVege);
        }
        ExecParam erec_AgazatiJelekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_AgazatiJelekResult = new Result();
        #endregion

        #region Search be�ll�t�s

        erec_AgazatiJelekSearch.OrderBy = "Kod";

        erec_AgazatiJelekResult = erec_AgazatiJelekService.GetAll(erec_AgazatiJelekExecParam, erec_AgazatiJelekSearch);
        if (!String.IsNullOrEmpty(erec_AgazatiJelekResult.ErrorCode))
        {
            return new TreeNode();
        }
        #endregion

        TreeNode AgazatiJelekTreeNode = null;

        if (String.IsNullOrEmpty(AgazatiJel_Id))
        {
            // ha van tal�lat
            // a 0. sor lesz a visszat�r�si �rt�k, vagy egy �j TreeNode
            int position = 0;
            foreach (DataRow row in erec_AgazatiJelekResult.Ds.Tables[0].Rows)
            {
                string row_agazatiJelId = row["Id"].ToString();

                bool canBeAdded = true;

                if (!String.IsNullOrEmpty(AgazatiJelFilter) && !isInFilter(row_agazatiJelId, AgazatiJelFilter))
                {
                    canBeAdded = false;
                }

                if (canBeAdded)
                {
                    AgazatiJelekTreeNode = UpdateAgazatiJelekNode(row, RootNode);
                    position += nodeHeight;
                    if (!String.IsNullOrEmpty(selectedAgazatiJelKod))
                    {
                        string kod = row["Kod"].ToString();
                        if (kod == selectedAgazatiJelKod)
                        {
                            selectedNodePosition += position;
                            if (selectedNodeType == NodeType.AgazatiJelek)
                            {
                                SetSelectedNode(AgazatiJelekTreeNode);
                            }
                            else
                            {
                                AgazatiJelekTreeNode.Expand();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            TreeNode CurrentTreeNode = null;
            foreach (DataRow row in erec_AgazatiJelekResult.Ds.Tables[0].Rows)
            {
                string row_agazatiJelId = row["Id"].ToString();

                bool canBeAdded = true;

                if (!String.IsNullOrEmpty(AgazatiJelFilter) && !isInFilter(row_agazatiJelId, AgazatiJelFilter))
                {
                    canBeAdded = false;
                }

                if (canBeAdded)
                {
                    CurrentTreeNode = UpdateAgazatiJelekNode(row, RootNode);
                    if (row_agazatiJelId == AgazatiJel_Id)
                    {
                        AgazatiJelekTreeNode = CurrentTreeNode;
                    }
                }

            }    
        }
        
        return AgazatiJelekTreeNode;

    }

    private void AddIraIrattariTetelekNode(TreeNode AgazatiJelekTreeNode)
    {
        if (IsFilteredWithoutHit) return;

        #region Init
        EREC_IraIrattariTetelekService erec_IraIrattariTetelekService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        EREC_IraIrattariTetelekSearch erec_IraIrattariTetelekSearch = new EREC_IraIrattariTetelekSearch();
        if (SearchObject != null)
        {
            CopyFieldExpression(SearchObject.ErvKezd, erec_IraIrattariTetelekSearch.ErvKezd);
            CopyFieldExpression(SearchObject.ErvVege, erec_IraIrattariTetelekSearch.ErvVege);
        }
        ExecParam erec_IraIrattariTetelekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIrattariTetelekResult = new Result();
        #endregion

        #region Search be�ll�t�s
        erec_IraIrattariTetelekSearch.AgazatiJel_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_IraIrattariTetelekSearch.AgazatiJel_Id.Value = ExtractNodeIdFromNodeTypeName(AgazatiJelekTreeNode.Value);

        erec_IraIrattariTetelekSearch.OrderBy = "IrattariTetelszam";

        erec_IraIrattariTetelekResult = erec_IraIrattariTetelekService.GetAllWithExtension(erec_IraIrattariTetelekExecParam, erec_IraIrattariTetelekSearch);
        if (!String.IsNullOrEmpty(erec_IraIrattariTetelekResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_IraIrattariTetelekDataSet = erec_IraIrattariTetelekResult.Ds;
        int position = 0;
        foreach (DataRow row in erec_IraIrattariTetelekDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(IrattariTetelFilter) && !isInFilter(row_Id, IrattariTetelFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                //AddIraIrattariTetelekNode(row, AgazatiJelekTreeNode);
                TreeNode IraIrattariTetelekTreeNode = UpdateIraIrattariTetelekNode(row, AgazatiJelekTreeNode);
                position += nodeHeight;
                if (!String.IsNullOrEmpty(selectedUgyKorKod))
                {
                    string kod = row["IrattariTetelszam"].ToString();
                    if (kod == selectedUgyKorKod)
                    {
                        selectedNodePosition += position;
                        if (selectedNodeType == NodeType.IraIrattariTetelek)
                        {
                            SetSelectedNode(IraIrattariTetelekTreeNode);
                        }
                        else
                        {
                            IraIrattariTetelekTreeNode.Expand();
                        }
                    }
                }
            }

        }


        #endregion
    }

    private void AddIratMetaDefinicioNode(TreeNode IraIrattariTetelekTreeNode)
    {
        if (IsFilteredWithoutHit) return;

        #region Init
        EREC_IratMetaDefinicioService erec_IratMetaDefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();
        if (SearchObject != null)
        {
            CopyFieldExpression(SearchObject.ErvKezd, erec_IratMetaDefinicioSearch.ErvKezd);
            CopyFieldExpression(SearchObject.ErvVege, erec_IratMetaDefinicioSearch.ErvVege);
        }
        ExecParam erec_IratMetaDefinicioExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IratMetaDefinicioResult = new Result();
        #endregion

        #region Search be�ll�t�s
        erec_IratMetaDefinicioSearch.Ugykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_IratMetaDefinicioSearch.Ugykor_Id.Value = ExtractNodeIdFromNodeTypeName(IraIrattariTetelekTreeNode.Value);
        erec_IratMetaDefinicioSearch.Ugytipus.Operator = Query.Operators.notnull;
        erec_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.isnull;
        erec_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.isnull;

        erec_IratMetaDefinicioSearch.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

        erec_IratMetaDefinicioResult = erec_IratMetaDefinicioService.GetAllWithExtension(erec_IratMetaDefinicioExecParam, erec_IratMetaDefinicioSearch);
        if (!String.IsNullOrEmpty(erec_IratMetaDefinicioResult.ErrorCode))
        {
            return;
        }

        #endregion

        #region sz�l� irat metadefin�ci� (�gyt�pus null)

        //String IratMetaDefinicioParent_Id = "";
        //EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioParentSearch = new EREC_IratMetaDefinicioSearch();
        //erec_IratMetaDefinicioParentSearch.Irattipus.Operator = Query.Operators.isnull;
        //erec_IratMetaDefinicioParentSearch.EljarasiSzakasz.Operator = Query.Operators.isnull;
        //erec_IratMetaDefinicioParentSearch.Ugytipus.Operator = Query.Operators.isnull;

        //Result res_IratMetaDefinicioParent = erec_IratMetaDefinicioService.GetAll(erec_IratMetaDefinicioExecParam, erec_IratMetaDefinicioParentSearch);

        //if (!String.IsNullOrEmpty(res_IratMetaDefinicioParent.ErrorCode))
        //{
        //    return;
        //}

        //if (!(res_IratMetaDefinicioParent.Ds.Tables[0].Rows.Count > 0))
        //{
        //    return;
        //}
        //// elvileg csak egy lehet
        //IratMetaDefinicioParent_Id = res_IratMetaDefinicioParent.Ds.Tables[0].Rows[0]["Id"].ToString();

        #endregion sz�l� irat metadefin�ci� (�gyt�pus null)

        #region DS lek�r�s

        DataSet erec_IratMetaDefinicioDataSet = erec_IratMetaDefinicioResult.Ds;

        int position = 0;
        foreach (DataRow row in erec_IratMetaDefinicioDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(IratMetaDefinicioFilter) && !isInFilter(row_Id, UgytipusFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                //AddIratMetaDefinicioNode(row, IraIrattariTetelekTreeNode);
                TreeNode IratMetaDefinicioTreeNode = UpdateIratMetaDefinicioNode(row, IraIrattariTetelekTreeNode);
                position += nodeHeight;
                if (!String.IsNullOrEmpty(selectedUgyTipusKod))
                {
                    string kod = row["Ugytipus"].ToString();
                    if (kod == selectedUgyTipusKod)
                    {
                        selectedNodePosition += position;
                        if (selectedNodeType == NodeType.IratMetaDefinicio)
                        {
                            SetSelectedNode(IratMetaDefinicioTreeNode);
                        }
                        else
                        {
                            IratMetaDefinicioTreeNode.Expand();
                        }
                    }
                }
            }
        }

        #endregion
    }

    private void AddEljarasiSzakaszNode(TreeNode IratMetaDefinicioTreeNode)
    {
        if (IsFilteredWithoutHit) return;

        #region Init
        EREC_IratMetaDefinicioService erec_IratMetaDefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();
        if (SearchObject != null)
        {
            CopyFieldExpression(SearchObject.ErvKezd, erec_IratMetaDefinicioSearch.ErvKezd);
            CopyFieldExpression(SearchObject.ErvVege, erec_IratMetaDefinicioSearch.ErvVege);
        }
        ExecParam erec_IratMetaDefinicioExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IratMetaDefinicioResult = new Result();
        #endregion

        ExecParam execParam_IratMetaDefinicio = UI.SetExecParamDefault(Page, new ExecParam());
        String IratMetadefinicio_Id = ExtractNodeIdFromNodeTypeName(IratMetaDefinicioTreeNode.Value);
        execParam_IratMetaDefinicio.Record_Id = IratMetadefinicio_Id;
        Result res_EljarasiSzakasz = erec_IratMetaDefinicioService.Get(execParam_IratMetaDefinicio);

        string Ugykor_Id = (res_EljarasiSzakasz.Record as EREC_IratMetaDefinicio).Ugykor_Id;
        string Ugytipus = (res_EljarasiSzakasz.Record as EREC_IratMetaDefinicio).Ugytipus;

        #region Search be�ll�t�s

        erec_IratMetaDefinicioSearch.Ugykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_IratMetaDefinicioSearch.Ugykor_Id.Value = Ugykor_Id;
        erec_IratMetaDefinicioSearch.Ugytipus.Value = Ugytipus;
        erec_IratMetaDefinicioSearch.Ugytipus.Operator = Query.Operators.equals;
        erec_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.notnull;
        erec_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.isnull;

        erec_IratMetaDefinicioSearch.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

        erec_IratMetaDefinicioResult = erec_IratMetaDefinicioService.GetAll(erec_IratMetaDefinicioExecParam, erec_IratMetaDefinicioSearch);
        if (!String.IsNullOrEmpty(erec_IratMetaDefinicioResult.ErrorCode))
        {
            return;
        }

        #endregion

        #region DS lek�r�s

        DataSet erec_IratMetaDefinicioDataSet = erec_IratMetaDefinicioResult.Ds;
        int position = 0;
        foreach (DataRow row in erec_IratMetaDefinicioDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(IratMetaDefinicioFilter) && !isInFilter(row_Id, EljarasiSzakaszFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                //AddEljarasiSzakaszNode(row, IratMetaDefinicioTreeNode);
                TreeNode EljarasiSzakaszTreeNode = UpdateEljarasiSzakaszNode(row, IratMetaDefinicioTreeNode);
                position += nodeHeight;
                if (!String.IsNullOrEmpty(selectedEljarasiSzakaszKod))
                {
                    string kod = row["EljarasiSzakasz"].ToString();
                    if (kod == selectedEljarasiSzakaszKod)
                    {
                        selectedNodePosition += position;
                        if (selectedNodeType == NodeType.EljarasiSzakasz)
                        {
                            SetSelectedNode(EljarasiSzakaszTreeNode);
                        }
                        else
                        {
                            EljarasiSzakaszTreeNode.Expand();
                        }
                    }
                }
            }
        }

        #endregion
    }


    private void AddIrattipusNode(TreeNode EljarasiSzakaszTreeNode)
    {
        if (IsFilteredWithoutHit) return;

        #region Init
        EREC_IratMetaDefinicioService erec_IratMetaDefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();
        if (SearchObject != null)
        {
            CopyFieldExpression(SearchObject.ErvKezd, erec_IratMetaDefinicioSearch.ErvKezd);
            CopyFieldExpression(SearchObject.ErvVege, erec_IratMetaDefinicioSearch.ErvVege);
        }
        ExecParam erec_IratMetaDefinicioExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IratMetaDefinicioResult = new Result();
        #endregion

        ExecParam execParam_IratMetaDefinicio = UI.SetExecParamDefault(Page, new ExecParam());
        String EljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(EljarasiSzakaszTreeNode.Value);
        execParam_IratMetaDefinicio.Record_Id = EljarasiSzakasz_Id;
        Result res_Irattipus = erec_IratMetaDefinicioService.Get(execParam_IratMetaDefinicio);

        string Ugykor_Id = (res_Irattipus.Record as EREC_IratMetaDefinicio).Ugykor_Id;
        string Ugytipus = (res_Irattipus.Record as EREC_IratMetaDefinicio).Ugytipus;
        string EljarasiSzakasz = (res_Irattipus.Record as EREC_IratMetaDefinicio).EljarasiSzakasz;

        #region Search be�ll�t�s

        erec_IratMetaDefinicioSearch.Ugykor_Id.Value = Ugykor_Id;
        erec_IratMetaDefinicioSearch.Ugykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_IratMetaDefinicioSearch.Ugytipus.Value = Ugytipus;
        erec_IratMetaDefinicioSearch.Ugytipus.Operator = Query.Operators.equals;
        erec_IratMetaDefinicioSearch.EljarasiSzakasz.Value = EljarasiSzakasz;
        erec_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.equals;
        erec_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.notnull;

        erec_IratMetaDefinicioSearch.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

        erec_IratMetaDefinicioResult = erec_IratMetaDefinicioService.GetAll(erec_IratMetaDefinicioExecParam, erec_IratMetaDefinicioSearch);
        if (!String.IsNullOrEmpty(erec_IratMetaDefinicioResult.ErrorCode))
        {
            return;
        }

        #endregion

        #region DS lek�r�s

        DataSet erec_IratMetaDefinicioDataSet = erec_IratMetaDefinicioResult.Ds;

        int position = 0;
        foreach (DataRow row in erec_IratMetaDefinicioDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(IratMetaDefinicioFilter) && !isInFilter(row_Id, IrattipusFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                //AddIrattipusNode(row, EljarasiSzakaszTreeNode);
                TreeNode IrattipusTreeNode = UpdateIrattipusNode(row, EljarasiSzakaszTreeNode);
                position += nodeHeight;
                if (!String.IsNullOrEmpty(selectedIratTipusKod))
                {
                    string kod = row["Irattipus"].ToString();
                    if (kod == selectedIratTipusKod)
                    {
                        selectedNodePosition += position;
                        if (selectedNodeType == NodeType.Irattipus)
                        {
                            SetSelectedNode(IrattipusTreeNode);
                        }
                        else
                        {
                            IrattipusTreeNode.Expand();
                        }
                    }
                }
            }
        }

        #endregion
    }


    #region AddCustomNode

    private int GetNewCustomNodePosition(TreeNode ParentTreeNode)
    {

        int i = 0;
        switch (CustomNodePosition)
        {
            case CustomNodePositionType.BeforeOthers:
                // az utols� custom node (el�lr�l keresve) ut�ni poz�ci�
                while (i < ParentTreeNode.ChildNodes.Count
                        && ExtractNodeTypeFromNodeTypeName(ParentTreeNode.ChildNodes[i].Value) == NodeType.Custom)
                {
                    i++;
                }
                break;
            case CustomNodePositionType.AfterOthers:
                i = ParentTreeNode.ChildNodes.Count;
                break;
        }

        return i;
    }

    // custom node a t�bbi node m�g�
    public void AddCustomNode(TreeNode ParentTreeNode, String Kod, String Nev, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(GetNewCustomNodePosition(ParentTreeNode), ParentTreeNode, Kod, Nev, GetNameByNodeType(NodeType.Custom), IconPathCustomTreeNode, IconAltCustomTreeNode, Custom_Id);
    }

    public void AddCustomNode(TreeNode ParentTreeNode, String Kod, String Nev, String IconPath, String IconAlt, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(GetNewCustomNodePosition(ParentTreeNode), ParentTreeNode, Kod, Nev, GetNameByNodeType(NodeType.Custom), IconPath, IconAlt, Custom_Id);
    }

    public void AddCustomNode(TreeNode ParentTreeNode, String Kod, String Nev, String NodeTypeName, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(GetNewCustomNodePosition(ParentTreeNode), ParentTreeNode, Kod, Nev, NodeTypeName, IconPathCustomTreeNode, IconAltCustomTreeNode, Custom_Id);
    }

    public void AddCustomNode(TreeNode ParentTreeNode, String Kod, String Nev, String NodeTypeName, String IconPath, String IconAlt, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(GetNewCustomNodePosition(ParentTreeNode), ParentTreeNode, Kod, Nev, NodeTypeName, IconPath, IconAlt, Custom_Id);
    }

    #endregion AddCustomNode

    #region AddCustomNodeAt

    public void AddCustomNodeAt(int index, TreeNode ParentTreeNode, String Kod, String Nev, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(index, ParentTreeNode, Kod, Nev, GetNameByNodeType(NodeType.Custom), IconPathCustomTreeNode, IconAltCustomTreeNode, Custom_Id);
    }

    public void AddCustomNodeAt(int index, TreeNode ParentTreeNode, String Kod, String Nev, String IconPath, String IconAlt, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(index, ParentTreeNode, Kod, Nev, GetNameByNodeType(NodeType.Custom), IconPath, IconAlt, Custom_Id);
    }

    public void AddCustomNodeAt(int index, TreeNode ParentTreeNode, String Kod, String Nev, String NodeTypeName, String Custom_Id)
    {
        if (ParentTreeNode == null) return;
        AddCustomNodeAt(index, ParentTreeNode, Kod, Nev, NodeTypeName, IconPathCustomTreeNode, IconAltCustomTreeNode, Custom_Id);
    }

    public void AddCustomNodeAt(int index, TreeNode ParentTreeNode, String Kod, String Nev, String NodeTypeName, String IconPath, String IconAlt, String Custom_Id)
    {
        if (ParentTreeNode == null) return;

        NodeType parentNodeType = GetNodeTypeByName(ExtractNodeNameFromNodeTypeName(ParentTreeNode.Value));
        String parentNodeId = ExtractNodeIdFromNodeTypeName(ParentTreeNode.Value);

        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (CustomTreeNodesDictionary.ContainsKey(Custom_Id) == false)
        {
            CustomTreeNodeItem customTreeNodeItem = new CustomTreeNodeItem(
                Custom_Id,
                GetTreeNodeText(Kod, Nev, NodeType.Custom, ParentTreeNode.ValuePath, IconPath, IconAlt, Custom_Id),
                NodeTypeName,
                parentNodeId,
                GetNameByNodeType(parentNodeType)
                );

            // ha custom node a sz�l� is, NodeTypeName lek�r�se
            if (parentNodeType == NodeType.Custom)
            {
                if (CustomTreeNodesDictionary.ContainsKey(parentNodeId))
                {
                    CustomTreeNodeItem parentCustomTreeNodeItem = null;
                    CustomTreeNodesDictionary.TryGetValue(parentNodeId, out parentCustomTreeNodeItem);
                    if (parentCustomTreeNodeItem != null)
                    {
                        customTreeNodeItem.ParentNodeTypeName = parentCustomTreeNodeItem.NodeTypeName;
                    }
                }
            }

            CustomTreeNodesDictionary.Add(Custom_Id, customTreeNodeItem);
        }

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.Custom, ParentTreeNode.ValuePath, IconPath, IconAlt, Custom_Id);
        String NodeValue = ConcatNodeType(NodeType.Custom, Custom_Id);

        TreeNode CustomTreeNode = new TreeNode(NodeText, NodeValue);
        CustomTreeNode.PopulateOnDemand = true;
        CustomTreeNode.Expanded = IsExpanded(NodeValue);

        ParentTreeNode.ChildNodes.AddAt(index, CustomTreeNode);

        ViewState["CustomTreeNodesDictionary"] = CustomTreeNodesDictionary;
    }

    #endregion AddCustomNodeAt


    #region UpdateTreeNodes

    private TreeNode UpdateRootNode()
    {
        #region TreeNode hozz�ad�sa
        String Kod = "";
        String Nev = GetNodeTypeText(NodeType.Root);

        String NodeText = "<b>" + GetTreeNodeText(Kod, Nev, NodeType.Root, "", IconPathRoot, IconAltRoot, Root_Symbolic_Id) + "</b>";
        String NodeValue = ConcatNodeType(NodeType.Root, Root_Symbolic_Id);
        String NodePath = NodeValue;// fels� szint

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            currentNode.Text = NodeText;
        }

        if (currentNode == null)
        {
            TreeNode RootTreeNode = new TreeNode(NodeText, NodeValue);
            RootTreeNode.PopulateOnDemand = true;
            //RootTreeNode.Expanded = true; //IsExpanded(NodeValue);

            TreeView1.Nodes.AddAt(0, RootTreeNode);
            return TreeView1.Nodes[0];
        }
        else
        {
            return currentNode;
        }
        #endregion
    }


    private TreeNode UpdateAgazatiJelekNode(DataRow dataRow_AgazatiJelek, TreeNode RootNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;
        string AgazatiJel_Id = "";

        AgazatiJel_Id = dataRow_AgazatiJelek["Id"].ToString();

        String Kod = dataRow_AgazatiJelek["Kod"].ToString();
        String Nev = dataRow_AgazatiJelek["Nev"].ToString();

        #region TreeNode hozz�ad�sa
        // update in Dictionary
        if (AgazatiJelekDictionary.ContainsKey(AgazatiJel_Id) == true)
        {
            AgazatiJelekItem agazatiJelekItem = null;
            AgazatiJelekDictionary.TryGetValue(AgazatiJel_Id, out agazatiJelekItem);
            OrderChanged = (agazatiJelekItem.Kod == Kod ? false : true);

            agazatiJelekItem.Kod = Kod;
            agazatiJelekItem.Nev = Nev;
            AgazatiJelekDictionary.Remove(AgazatiJel_Id);
            AgazatiJelekDictionary.Add(AgazatiJel_Id, agazatiJelekItem);
        }
        else
        {
            AgazatiJelekItem agazatiJelekItem = new AgazatiJelekItem(AgazatiJel_Id,
                dataRow_AgazatiJelek["Kod"].ToString(),
                dataRow_AgazatiJelek["Nev"].ToString()
                );
            AgazatiJelekDictionary.Add(AgazatiJel_Id, agazatiJelekItem);
        }

        ViewState["AgazatiJelekDictionary"] = AgazatiJelekDictionary;

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.AgazatiJelek, RootNode.ValuePath, IconPathAgazatiJelek, IconAltAgazatiJelek, AgazatiJel_Id);
        String NodeValue = ConcatNodeType(NodeType.AgazatiJelek, AgazatiJel_Id);
        String NodePath = RootNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                RootNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            TreeNode AgazatiJelekTreeNode = new TreeNode(NodeText, NodeValue);
            AgazatiJelekTreeNode.PopulateOnDemand = true;
            //AgazatiJelekTreeNode.Expanded = false;
            AgazatiJelekTreeNode.Expanded = IsExpanded(NodeValue);

            // megkeress�k a hely�t
            bool bFound = false;
            int i = 0;

            switch (CustomNodePosition)
            {
                case CustomNodePositionType.AfterOthers:
                    {
                        // fel�l vannak az �gazati jel node-ok, alatta a custom node-ok, el�lr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        while (i < RootNode.ChildNodes.Count && !bFound)
                        {
                            TreeNode tn = RootNode.ChildNodes[i];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.AgazatiJelek)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnAgazatiJel_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                AgazatiJelekItem agazatiJelekItem = null;
                                AgazatiJelekDictionary.TryGetValue(tnAgazatiJel_Id, out agazatiJelekItem);
                                if (agazatiJelekItem != null && String.Compare(agazatiJelekItem.Kod, dataRow_AgazatiJelek["Kod"].ToString()) > 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i++;
                                }
                            }

                        }

                    }
                    break;
                case CustomNodePositionType.BeforeOthers:
                    {
                        // alul vannak az �gazati jel node-ok, felette a custom node-ok, h�tulr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        i = RootNode.ChildNodes.Count;
                        while (i > 0 && !bFound)
                        {
                            TreeNode tn = RootNode.ChildNodes[i - 1];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.AgazatiJelek)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnAgazatiJel_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                AgazatiJelekItem agazatiJelekItem = null;
                                AgazatiJelekDictionary.TryGetValue(tnAgazatiJel_Id, out agazatiJelekItem);
                                if (agazatiJelekItem != null && String.Compare(agazatiJelekItem.Kod, dataRow_AgazatiJelek["Kod"].ToString()) <= 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i--;
                                }
                            }

                        }

                    }
                    break;
            }

            RootNode.ChildNodes.AddAt(i, AgazatiJelekTreeNode);
            if (isSelected)
            {
                RootNode.ChildNodes[i].Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
            return RootNode.ChildNodes[i];
        }
        else
        {
            return currentNode;
        }
        #endregion
    }

    private TreeNode UpdateIraIrattariTetelekNode(DataRow dataRow_IrattariTetelekExtended, TreeNode AgazatiJelekTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;
        string AgazatiJelek_Id = ExtractNodeIdFromNodeTypeName(AgazatiJelekTreeNode.Value);

        String Ugykor_Id = dataRow_IrattariTetelekExtended["Id"].ToString();

        String Kod = dataRow_IrattariTetelekExtended["IrattariTetelszam"].ToString();
        String Nev = dataRow_IrattariTetelekExtended["Nev"].ToString();

        // update in Dictionary 
        if (IraIrattariTetelekDictionary.ContainsKey(Ugykor_Id) == true)
        {
            IraIrattariTetelekItem iraIrattariTetelekItem = null;
            IraIrattariTetelekDictionary.TryGetValue(Ugykor_Id, out iraIrattariTetelekItem);
            OrderChanged = (iraIrattariTetelekItem.IrattariTetelszam == Kod ? false : true);

            iraIrattariTetelekItem.AgazatiJel_Id = dataRow_IrattariTetelekExtended["AgazatiJel_Id"].ToString();
            iraIrattariTetelekItem.IrattariTetelszam = Kod;
            iraIrattariTetelekItem.Nev = Nev;
            IraIrattariTetelekDictionary.Remove(Ugykor_Id);
            IraIrattariTetelekDictionary.Add(Ugykor_Id, iraIrattariTetelekItem);

        }
        else
        {
            // sz�l� irat metadefin�ci� (�gyt�pus null)

            String IratMetaDefinicioParent_Id = dataRow_IrattariTetelekExtended["IratMetaDefinicio_Id"].ToString();

            IraIrattariTetelekItem iraIrattariTetelekItem = new IraIrattariTetelekItem(
                dataRow_IrattariTetelekExtended["Id"].ToString(),
                dataRow_IrattariTetelekExtended["AgazatiJel_Id"].ToString(),
                dataRow_IrattariTetelekExtended["IrattariTetelszam"].ToString(),
                dataRow_IrattariTetelekExtended["Nev"].ToString(),
                IratMetaDefinicioParent_Id
                );
            IraIrattariTetelekDictionary.Add(Ugykor_Id, iraIrattariTetelekItem);
            OrderChanged = true;
        }


        ViewState["IraIrattariTetelekDictionary"] = IraIrattariTetelekDictionary;

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.IraIrattariTetelek, AgazatiJelekTreeNode.ValuePath, IconPathIraIrattariTetelek, IconAltIraIrattariTetelek, Ugykor_Id);
        String NodeValue = ConcatNodeType(NodeType.IraIrattariTetelek, Ugykor_Id);

        String NodePath = AgazatiJelekTreeNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);   // fels� szint

        if (currentNode != null)
        {
            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                AgazatiJelekTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            TreeNode IraIrattariTetelekTreeNode = new TreeNode(NodeText, NodeValue);
            IraIrattariTetelekTreeNode.PopulateOnDemand = true;
            IraIrattariTetelekTreeNode.Expanded = IsExpanded(NodeValue);

            // megkeress�k a hely�t
            bool bFound = false;
            int i = 0;
            switch (CustomNodePosition)
            {
                case CustomNodePositionType.AfterOthers:
                    {
                        // fel�l vannak az iratt�ri t�telsz�m node-ok, alatta a custom node-ok, el�lr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        while (i < AgazatiJelekTreeNode.ChildNodes.Count && !bFound)
                        {
                            TreeNode tn = AgazatiJelekTreeNode.ChildNodes[i];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.IraIrattariTetelek)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIraIrattariTetelek_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IraIrattariTetelekItem irattariTetelekItem = null;
                                IraIrattariTetelekDictionary.TryGetValue(tnIraIrattariTetelek_Id, out irattariTetelekItem);
                                if (irattariTetelekItem != null && String.Compare(irattariTetelekItem.IrattariTetelszam, dataRow_IrattariTetelekExtended["IrattariTetelszam"].ToString()) > 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i++;
                                }
                            }

                        }

                    }
                    break;
                case CustomNodePositionType.BeforeOthers:
                    {
                        // alul vannak az iratt�ri t�telsz�m node-ok, felette a custom node-ok, h�tulr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        i = AgazatiJelekTreeNode.ChildNodes.Count;
                        while (i > 0 && !bFound)
                        {
                            TreeNode tn = AgazatiJelekTreeNode.ChildNodes[i - 1];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.IraIrattariTetelek)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIraIrattariTetelek_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IraIrattariTetelekItem irattariTetelekItem = null;
                                IraIrattariTetelekDictionary.TryGetValue(tnIraIrattariTetelek_Id, out irattariTetelekItem);
                                if (irattariTetelekItem != null && String.Compare(irattariTetelekItem.IrattariTetelszam, dataRow_IrattariTetelekExtended["IrattariTetelszam"].ToString()) <= 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i--;
                                }
                            }

                        }

                    }
                    break;
            }

            AgazatiJelekTreeNode.ChildNodes.AddAt(i, IraIrattariTetelekTreeNode);
            if (isSelected)
            {
                AgazatiJelekTreeNode.ChildNodes[i].Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
            return AgazatiJelekTreeNode.ChildNodes[i];
        }
        else
        {
            return currentNode;
        }
    }

    private TreeNode UpdateIratMetaDefinicioNode(DataRow dataRow_IratMetaDefinicio, TreeNode IraIrattariTetelekTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        string IraIrattariTetelek_Id = ExtractNodeIdFromNodeTypeName(IraIrattariTetelekTreeNode.Value);

        String IratMetaDefinicio_Id = dataRow_IratMetaDefinicio["Id"].ToString();
        String Kod = GetNodeTypeText(NodeType.IratMetaDefinicio); //"�gyt�pus";
        String Nev = dataRow_IratMetaDefinicio["UgytipusNev"].ToString();
        String Ugytipus = dataRow_IratMetaDefinicio["Ugytipus"].ToString();
        String SzignalasTipusa = dataRow_IratMetaDefinicio["SzignalasTipusa"].ToString();
        String JavasoltFelelos = dataRow_IratMetaDefinicio["JavasoltFelelos"].ToString();
        if (String.IsNullOrEmpty(Nev))
        {
            Nev = "-";
        }

        if (!String.IsNullOrEmpty(SzignalasTipusa))
        {
            Nev += " (" + strSzignalasTipusa + SzignalasTipusa;
            if (!String.IsNullOrEmpty(JavasoltFelelos))
            {
                Nev += " " + strJavasoltFelelos + JavasoltFelelos;
            }
            Nev += ")";
        }

        // update in Dictionary 
        if (IratMetaDefinicioDictionary.ContainsKey(IratMetaDefinicio_Id) == true)
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = null;
            IratMetaDefinicioDictionary.TryGetValue(IratMetaDefinicio_Id, out iratMetaDefinicioItem);

            OrderChanged = (iratMetaDefinicioItem.Ugytipus == Ugytipus ? false : true);

            iratMetaDefinicioItem.Ugykor_Id = dataRow_IratMetaDefinicio["Ugykor_Id"].ToString();
            iratMetaDefinicioItem.UgykorKod = dataRow_IratMetaDefinicio["UgykorKod"].ToString();
            iratMetaDefinicioItem.Ugytipus = dataRow_IratMetaDefinicio["Ugytipus"].ToString();
            iratMetaDefinicioItem.UgytipusNev = dataRow_IratMetaDefinicio["UgytipusNev"].ToString();
            iratMetaDefinicioItem.IratMetadefinicio_Id_Szulo = IraIrattariTetelek_Id;

            IratMetaDefinicioDictionary.Remove(IratMetaDefinicio_Id);
            IratMetaDefinicioDictionary.Add(IratMetaDefinicio_Id, iratMetaDefinicioItem);

        }
        else
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = new IratMetaDefinicioItem(
                IratMetaDefinicio_Id,
                dataRow_IratMetaDefinicio["Ugykor_Id"].ToString(),
                dataRow_IratMetaDefinicio["UgykorKod"].ToString(),
                dataRow_IratMetaDefinicio["Ugytipus"].ToString(),
                dataRow_IratMetaDefinicio["UgytipusNev"].ToString(),
                IraIrattariTetelek_Id
                );
            IratMetaDefinicioDictionary.Add(IratMetaDefinicio_Id, iratMetaDefinicioItem);
            OrderChanged = true;
        }

        ViewState["IratMetaDefinicioDictionary"] = IratMetaDefinicioDictionary;

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.IratMetaDefinicio, IraIrattariTetelekTreeNode.ValuePath, IconPathIratMetaDefinicio, IconAltIratMetaDefinicio, IratMetaDefinicio_Id);
        String NodeValue = ConcatNodeType(NodeType.IratMetaDefinicio, IratMetaDefinicio_Id);
        String NodePath = IraIrattariTetelekTreeNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                IraIrattariTetelekTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            TreeNode IratMetaDefinicioTreeNode = new TreeNode(NodeText, NodeValue);
            IratMetaDefinicioTreeNode.PopulateOnDemand = true;
            IratMetaDefinicioTreeNode.Expanded = IsExpanded(NodeValue);

            // megkeress�k a hely�t
            bool bFound = false;
            int i = 0;
            switch (CustomNodePosition)
            {
                case CustomNodePositionType.AfterOthers:
                    {
                        // fel�l vannak az irat metadefin�ci� (�gyt�pus) node-ok, alatta a custom node-ok, el�lr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        while (i < IraIrattariTetelekTreeNode.ChildNodes.Count && !bFound)
                        {
                            TreeNode tn = IraIrattariTetelekTreeNode.ChildNodes[i];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.IratMetaDefinicio)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIratMetaDefinicio_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                IratMetaDefinicioDictionary.TryGetValue(tnIratMetaDefinicio_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.Ugytipus, dataRow_IratMetaDefinicio["Ugytipus"].ToString()) > 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i++;
                                }
                            }

                        }

                    }
                    break;
                case CustomNodePositionType.BeforeOthers:
                    {
                        // alul vannak az irat metadefin�ci� (�gyt�pus) node-ok, felette a custom node-ok, h�tulr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        i = IraIrattariTetelekTreeNode.ChildNodes.Count;
                        while (i > 0 && !bFound)
                        {
                            TreeNode tn = IraIrattariTetelekTreeNode.ChildNodes[i - 1];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.IratMetaDefinicio)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIratMetaDefinicio_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                IratMetaDefinicioDictionary.TryGetValue(tnIratMetaDefinicio_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.Ugytipus, dataRow_IratMetaDefinicio["Ugytipus"].ToString()) <= 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i--;
                                }
                            }

                        }

                    }
                    break;
            }

            IraIrattariTetelekTreeNode.ChildNodes.AddAt(i, IratMetaDefinicioTreeNode);
            if (isSelected)
            {
                IraIrattariTetelekTreeNode.ChildNodes[i].Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
            return IraIrattariTetelekTreeNode.ChildNodes[i];
        }
        else
        {
            return currentNode;
        }

    }

    private TreeNode UpdateEljarasiSzakaszNode(DataRow dataRow_IMD_EljarasiSzakasz, TreeNode IratMetaDefinicioTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        String IratMetadefinicio_Id = ExtractNodeIdFromNodeTypeName(IratMetaDefinicioTreeNode.Value);

        String EljarasiSzakasz_Id = dataRow_IMD_EljarasiSzakasz["Id"].ToString();
        String Kod = GetNodeTypeText(NodeType.EljarasiSzakasz).Replace(" ", "&nbsp;");
        string EljarasiSzakasz = "";
        // elj�r�si szakasz kiolvas�sa
        Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("ELJARASI_SZAKASZ", Page).TryGetValue(dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString(), out EljarasiSzakasz);

        String Nev = EljarasiSzakasz;

        if (String.IsNullOrEmpty(Nev))
        {
            Nev = "-";
        }

        // update in Dictionary 
        if (EljarasiSzakaszDictionary.ContainsKey(EljarasiSzakasz_Id) == true)
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = null;
            EljarasiSzakaszDictionary.TryGetValue(EljarasiSzakasz_Id, out iratMetaDefinicioItem);

            OrderChanged = (iratMetaDefinicioItem.EljarasiSzakasz == dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString() ? false : true);

            iratMetaDefinicioItem.Ugykor_Id = dataRow_IMD_EljarasiSzakasz["Ugykor_Id"].ToString();
            iratMetaDefinicioItem.UgykorKod = dataRow_IMD_EljarasiSzakasz["UgykorKod"].ToString();
            iratMetaDefinicioItem.Ugytipus = dataRow_IMD_EljarasiSzakasz["Ugytipus"].ToString();
            iratMetaDefinicioItem.UgytipusNev = dataRow_IMD_EljarasiSzakasz["UgytipusNev"].ToString();
            iratMetaDefinicioItem.IratMetadefinicio_Id_Szulo = IratMetadefinicio_Id;
            iratMetaDefinicioItem.EljarasiSzakasz = dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString();

            EljarasiSzakaszDictionary.Remove(EljarasiSzakasz_Id);
            EljarasiSzakaszDictionary.Add(EljarasiSzakasz_Id, iratMetaDefinicioItem);
        }
        else
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = new IratMetaDefinicioItem(
                EljarasiSzakasz_Id,
                dataRow_IMD_EljarasiSzakasz["Ugykor_Id"].ToString(),
                dataRow_IMD_EljarasiSzakasz["UgykorKod"].ToString(),
                dataRow_IMD_EljarasiSzakasz["Ugytipus"].ToString(),
                dataRow_IMD_EljarasiSzakasz["UgytipusNev"].ToString(),
                IratMetadefinicio_Id,
                dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString()
                );
            EljarasiSzakaszDictionary.Add(EljarasiSzakasz_Id, iratMetaDefinicioItem);
            OrderChanged = true;
        }

        ViewState["EljarasiSzakaszDictionary"] = EljarasiSzakaszDictionary;

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.EljarasiSzakasz, IratMetaDefinicioTreeNode.ValuePath, IconPathEljarasiSzakasz, IconAltEljarasiSzakasz, EljarasiSzakasz_Id);
        String NodeValue = ConcatNodeType(NodeType.EljarasiSzakasz, EljarasiSzakasz_Id);
        String NodePath = IratMetaDefinicioTreeNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                IratMetaDefinicioTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            TreeNode EljarasiSzakaszTreeNode = new TreeNode(NodeText, NodeValue);
            EljarasiSzakaszTreeNode.PopulateOnDemand = true;
            EljarasiSzakaszTreeNode.Expanded = IsExpanded(NodeValue);

            // megkeress�k a hely�t
            bool bFound = false;
            int i = 0;
            switch (CustomNodePosition)
            {
                case CustomNodePositionType.AfterOthers:
                    {
                        // fel�l vannak az irat metadefin�ci� (elj�r�si szakasz) node-ok, alatta a custom node-ok, el�lr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        while (i < IratMetaDefinicioTreeNode.ChildNodes.Count && !bFound)
                        {
                            TreeNode tn = IratMetaDefinicioTreeNode.ChildNodes[i];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.EljarasiSzakasz)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnEljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                EljarasiSzakaszDictionary.TryGetValue(tnEljarasiSzakasz_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.EljarasiSzakasz, dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString()) > 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i++;
                                }
                            }

                        }

                    }
                    break;
                case CustomNodePositionType.BeforeOthers:
                    {
                        // alul vannak az irat metadefin�ci� (elj�r�si szakasz) node-ok, felette a custom node-ok, h�tulr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        i = IratMetaDefinicioTreeNode.ChildNodes.Count;
                        while (i > 0 && !bFound)
                        {
                            TreeNode tn = IratMetaDefinicioTreeNode.ChildNodes[i - 1];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.EljarasiSzakasz)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnEljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                EljarasiSzakaszDictionary.TryGetValue(tnEljarasiSzakasz_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.EljarasiSzakasz, dataRow_IMD_EljarasiSzakasz["EljarasiSzakasz"].ToString()) <= 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i--;
                                }
                            }

                        }

                    }
                    break;
            }

            IratMetaDefinicioTreeNode.ChildNodes.AddAt(i, EljarasiSzakaszTreeNode);
            if (isSelected)
            {
                IratMetaDefinicioTreeNode.ChildNodes[i].Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
            return IratMetaDefinicioTreeNode.ChildNodes[i];
        }
        else
        {
            return currentNode;
        }

    }

    private TreeNode UpdateIrattipusNode(DataRow dataRow_IMD_Irattipus, TreeNode EljarasiSzakaszTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        string EljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(EljarasiSzakaszTreeNode.Value);

        String Irattipus_Id = dataRow_IMD_Irattipus["Id"].ToString();
        String Kod = GetNodeTypeText(NodeType.Irattipus).Replace(" ", "&nbsp;");
        string Irattipus = "";
        // iratt�pus kiolvas�sa
        Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("IRATTIPUS", Page).TryGetValue(dataRow_IMD_Irattipus["Irattipus"].ToString(), out Irattipus);

        String Nev = Irattipus;
        if (String.IsNullOrEmpty(Nev))
        {
            Nev = "-";
        }

        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (IrattipusDictionary.ContainsKey(Irattipus_Id) == true)
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = null;
            IrattipusDictionary.TryGetValue(Irattipus_Id, out iratMetaDefinicioItem);
            OrderChanged = (iratMetaDefinicioItem.Irattipus == dataRow_IMD_Irattipus["Irattipus"].ToString() ? false : true);
            iratMetaDefinicioItem.Ugykor_Id = dataRow_IMD_Irattipus["Ugykor_Id"].ToString();
            iratMetaDefinicioItem.UgykorKod = dataRow_IMD_Irattipus["UgykorKod"].ToString();
            iratMetaDefinicioItem.Ugytipus = dataRow_IMD_Irattipus["Ugytipus"].ToString();
            iratMetaDefinicioItem.UgytipusNev = dataRow_IMD_Irattipus["UgytipusNev"].ToString();
            iratMetaDefinicioItem.IratMetadefinicio_Id_Szulo = EljarasiSzakasz_Id;
            iratMetaDefinicioItem.EljarasiSzakasz = dataRow_IMD_Irattipus["EljarasiSzakasz"].ToString();
            iratMetaDefinicioItem.Irattipus = dataRow_IMD_Irattipus["Irattipus"].ToString();

            IrattipusDictionary.Remove(Irattipus_Id);
            IrattipusDictionary.Add(Irattipus_Id, iratMetaDefinicioItem);

        }
        else
        {
            IratMetaDefinicioItem iratMetaDefinicioItem = new IratMetaDefinicioItem(
                Irattipus_Id,
                dataRow_IMD_Irattipus["Ugykor_Id"].ToString(),
                dataRow_IMD_Irattipus["UgykorKod"].ToString(),
                dataRow_IMD_Irattipus["Ugytipus"].ToString(),
                dataRow_IMD_Irattipus["UgytipusNev"].ToString(),
                EljarasiSzakasz_Id,
                dataRow_IMD_Irattipus["EljarasiSzakasz"].ToString(),
                dataRow_IMD_Irattipus["Irattipus"].ToString()
                );
            IrattipusDictionary.Add(Irattipus_Id, iratMetaDefinicioItem);
            OrderChanged = true;
        }

        ViewState["IrattipusDictionary"] = IrattipusDictionary;

        String NodeText = GetTreeNodeText(Kod, Nev, NodeType.Irattipus, EljarasiSzakaszTreeNode.ValuePath, IconPathIrattipus, IconAltIrattipus, Irattipus_Id);
        String NodeValue = ConcatNodeType(NodeType.Irattipus, Irattipus_Id);

        String NodePath = EljarasiSzakaszTreeNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                EljarasiSzakaszTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            TreeNode IrattipusTreeNode = new TreeNode(NodeText, NodeValue);
            IrattipusTreeNode.PopulateOnDemand = true;
            IrattipusTreeNode.Expanded = IsExpanded(NodeValue);

            // megkeress�k a hely�t
            bool bFound = false;
            int i = 0;
            switch (CustomNodePosition)
            {
                case CustomNodePositionType.AfterOthers:
                    {
                        // fel�l vannak az irat metadefin�ci� (iratt�pus) node-ok, alatta a custom node-ok, el�lr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        while (i < EljarasiSzakaszTreeNode.ChildNodes.Count && !bFound)
                        {
                            TreeNode tn = EljarasiSzakaszTreeNode.ChildNodes[i];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.Irattipus)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIrattipus_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                IrattipusDictionary.TryGetValue(tnIrattipus_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.Irattipus, dataRow_IMD_Irattipus["Irattipus"].ToString()) > 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i++;
                                }
                            }

                        }

                    }
                    break;
                case CustomNodePositionType.BeforeOthers:
                    {
                        // alul vannak az irat metadefin�ci� (iratt�pus) node-ok, felette a custom node-ok, h�tulr�l keres�nk
                        // vigy�zat: rel�ci�s jel �ll�sa!
                        i = EljarasiSzakaszTreeNode.ChildNodes.Count;
                        while (i > 0 && !bFound)
                        {
                            TreeNode tn = EljarasiSzakaszTreeNode.ChildNodes[i - 1];

                            if (ExtractNodeTypeFromNodeTypeName(tn.Value) != NodeType.Irattipus)
                            {
                                bFound = true;
                            }
                            else
                            {
                                string tnIrattipus_Id = ExtractNodeIdFromNodeTypeName(tn.Value);

                                IratMetaDefinicioItem iratMetaDefinicioItem = null;
                                IrattipusDictionary.TryGetValue(tnIrattipus_Id, out iratMetaDefinicioItem);
                                if (iratMetaDefinicioItem != null && String.Compare(iratMetaDefinicioItem.Irattipus, dataRow_IMD_Irattipus["Irattipus"].ToString()) <= 0)
                                {
                                    bFound = true;
                                }
                                else
                                {
                                    i--;
                                }
                            }

                        }

                    }
                    break;
            }

            EljarasiSzakaszTreeNode.ChildNodes.AddAt(i, IrattipusTreeNode);
            if (isSelected)
            {
                EljarasiSzakaszTreeNode.ChildNodes[i].Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
            return EljarasiSzakaszTreeNode.ChildNodes[i];
        }
        else
        {
            return currentNode;
        }

    }

    #endregion UpdateTreeNodes

    // TreeView gyerekeinek ellen�rz�se, hi�nyz� gyerek felv�tele
    public void RefreshSelectedNodeChildren()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        switch (ExtractNodeTypeFromNodeTypeName(selectedTreeNode.Value))
        {
            case NodeType.Root:
                {
                    #region Root:
                    EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                    EREC_AgazatiJelekSearch search = new EREC_AgazatiJelekSearch();

                    search.OrderBy = "Kod";

                    result = service.GetAll(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(AgazatiJelFilter) && !isInFilter(Id, AgazatiJelFilter))
                            {
                                canBeAdded = false;
                            }

                            if (!AgazatiJelekDictionary.ContainsKey(Id))  // �j node, mindenk�pp megjelen�tj�k
                            {
                                UpdateAgazatiJelekNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && AgazatiJelekDictionary.ContainsKey(Id))
                            {
                                AgazatiJelekDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + ConcatNodeType(NodeType.AgazatiJelek, Id));
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }
                        }
                    }

                    #endregion
                }
                break;
            case NodeType.AgazatiJelek:
                {
                    #region �gazati jelek:
                    string AgazatiJel_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
                    search.AgazatiJel_Id.Value = AgazatiJel_Id;
                    search.AgazatiJel_Id.Operator = Query.Operators.equals;

                    search.OrderBy = "IrattariTetelszam";

                    result = service.GetAllWithExtension(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(IrattariTetelFilter) && !isInFilter(Id, IrattariTetelFilter))
                            {
                                canBeAdded = false;
                            }

                            if (!IraIrattariTetelekDictionary.ContainsKey(Id))  // �j node, mindenk�pp megjelen�tj�k
                            {
                                UpdateIraIrattariTetelekNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && IraIrattariTetelekDictionary.ContainsKey(Id))
                            {
                                IraIrattariTetelekDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + ConcatNodeType(NodeType.IraIrattariTetelek, Id));
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }
                        }
                    }

                    #endregion
                }
                break;
            case NodeType.IraIrattariTetelek:
                {
                    #region Iratt�ri t�telek:

                    string Ugykor_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();
                    search.Ugykor_Id.Value = Ugykor_Id;
                    search.Ugykor_Id.Operator = Query.Operators.equals;
                    search.Ugytipus.Operator = Query.Operators.notnull;
                    search.EljarasiSzakasz.Operator = Query.Operators.isnull;
                    search.Irattipus.Operator = Query.Operators.isnull;
                    search.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

                    result = service.GetAllWithExtension(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(UgytipusFilter) && !isInFilter(Id, UgytipusFilter))
                            {
                                canBeAdded = false;
                            }

                            if (!IratMetaDefinicioDictionary.ContainsKey(Id))   // �j node, mindenk�ppen hozz�adjuk
                            {
                                UpdateIratMetaDefinicioNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && IratMetaDefinicioDictionary.ContainsKey(Id))
                            {
                                IratMetaDefinicioDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + ConcatNodeType(NodeType.IratMetaDefinicio, Id));
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }
                        }
                    }

                    #endregion
                }
                break;
            case NodeType.IratMetaDefinicio:
                {
                    #region Irat metadefin�ci�:

                    string IratMetaDefinicio_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    IratMetaDefinicioItem iratMetaDefinicioItem = null;
                    IratMetaDefinicioDictionary.TryGetValue(IratMetaDefinicio_Id, out iratMetaDefinicioItem);

                    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();
                    search.Ugykor_Id.Value = iratMetaDefinicioItem.Ugykor_Id;
                    search.Ugykor_Id.Operator = Query.Operators.equals;
                    search.Ugytipus.Value = iratMetaDefinicioItem.Ugytipus;
                    search.Ugytipus.Operator = Query.Operators.equals;
                    search.EljarasiSzakasz.Operator = Query.Operators.notnull;
                    search.Irattipus.Operator = Query.Operators.isnull;
                    search.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

                    result = service.GetAll(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(IratMetaDefinicioFilter) && !isInFilter(Id, EljarasiSzakaszFilter))
                            {
                                canBeAdded = false;
                            }


                            if (!EljarasiSzakaszDictionary.ContainsKey(Id)) // �j node, mindenk�ppen hozz�adjuk
                            {
                                UpdateEljarasiSzakaszNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && EljarasiSzakaszDictionary.ContainsKey(Id))
                            {
                                EljarasiSzakaszDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + ConcatNodeType(NodeType.IratMetaDefinicio, Id));
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }

                        }
                    }

                    #endregion
                }
                break;
            case NodeType.EljarasiSzakasz:
                {
                    #region Elj�r�si szakasz:

                    string EljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    IratMetaDefinicioItem iratMetaDefinicioItem = null;
                    EljarasiSzakaszDictionary.TryGetValue(EljarasiSzakasz_Id, out iratMetaDefinicioItem);

                    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();
                    search.Ugykor_Id.Value = iratMetaDefinicioItem.Ugykor_Id;
                    search.Ugykor_Id.Operator = Query.Operators.equals;
                    search.Ugytipus.Value = iratMetaDefinicioItem.Ugytipus;
                    search.Ugytipus.Operator = Query.Operators.equals;
                    search.EljarasiSzakasz.Value = iratMetaDefinicioItem.EljarasiSzakasz;
                    search.EljarasiSzakasz.Operator = Query.Operators.equals;
                    search.Irattipus.Operator = Query.Operators.notnull;
                    search.OrderBy = "Ugytipus, EljarasiSzakasz, Irattipus";

                    result = service.GetAll(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(IratMetaDefinicioFilter) && !isInFilter(Id, IrattipusFilter))
                            {
                                canBeAdded = false;
                            }

                            if (!IrattipusDictionary.ContainsKey(Id)) // �j node, mindenk�ppen hozz�adjuk
                            {
                                UpdateIrattipusNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && IrattipusDictionary.ContainsKey(Id))
                            {
                                IrattipusDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + ConcatNodeType(NodeType.IratMetaDefinicio, Id));
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }
                        }
                    }

                    #endregion
                }
                break;
            case NodeType.Irattipus:
                {
                    #region Iratt�pus:

                    //string Irattipus_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);


                    #endregion
                }
                break;
            case NodeType.Custom:
                {
                    #region Custom:

                    //string CustomTreeNode_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    #endregion
                }
                break;
            default:
                break;
        }

        // gener�lunk egy esem�nyt, az als� szint felt�lt�shez
        string nodeId = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);
        NodeType nodeType = ExtractNodeTypeFromNodeTypeName(selectedTreeNode.Value);
        IMDTerkepEventArgs e = new IMDTerkepEventArgs(nodeId, nodeType);
        if (nodeType == NodeType.Custom)
        {
            if (CustomTreeNodesDictionary.ContainsKey(nodeId))
            {
                CustomTreeNodeItem customTreeNodeItem = null;
                CustomTreeNodesDictionary.TryGetValue(nodeId, out customTreeNodeItem);
                if (customTreeNodeItem != null)
                {
                    e.nodeTypeName = customTreeNodeItem.NodeTypeName;
                }
            }
        }

        OnRefreshingSelectedNodeChildren(e);

    }

    // TreeView friss�t�se a kijel�lt sor szintj�ig
    public void RefreshSelectedNode()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        TreeNode ParentNode = selectedTreeNode.Parent;
        string ParentNode_Id = String.Empty;
        if (ParentNode != null)
        {
            ParentNode_Id = ExtractNodeIdFromNodeTypeName(ParentNode.Value);
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        switch (ExtractNodeTypeFromNodeTypeName(selectedTreeNode.Value))
        {
            case NodeType.Root:
                break;
            case NodeType.AgazatiJelek:
                {
                    #region �gazati jelek:
                    string AgazatiJel_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {
                        EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                        //execParam.Record_Id = AgazatiJel_Id;

                        EREC_AgazatiJelekSearch search = new EREC_AgazatiJelekSearch();

                        search.Id.Value = AgazatiJel_Id;
                        search.Id.Operator = Query.Operators.equals;

                        //result = service.Get(execParam);
                        result = service.GetAll(execParam, search);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            //EREC_AgazatiJelek erec_AgazatiJelek = (EREC_AgazatiJelek)result.Record;
                            //UpdateAgazatiJelekNode(erec_AgazatiJelek);

                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow_AgazatiJelek = result.Ds.Tables[0].Rows[0];
                                UpdateAgazatiJelekNode(dataRow_AgazatiJelek, ParentNode);
                            }
                        }
                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.AgazatiJelek, AgazatiJel_Id);
                    }
                    #endregion
                }
                break;
            case NodeType.IraIrattariTetelek:
                {
                    #region Iratt�ri t�telek:

                    string Ugykor_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {
                        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

                        search.Id.Value = Ugykor_Id;
                        search.Id.Operator = Query.Operators.equals;

                        result = service.GetAllWithExtension(execParam, search);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow_IrattariTetelekExtended = result.Ds.Tables[0].Rows[0];
                                UpdateIraIrattariTetelekNode(dataRow_IrattariTetelekExtended, ParentNode);
                            }
                        }

                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.IraIrattariTetelek, Ugykor_Id);
                    }

                    #endregion
                }
                break;
            case NodeType.IratMetaDefinicio:
                {
                    #region Irat metadefin�ci�:

                    string IratMetaDefinicio_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {
                        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

                        search.Id.Value = IratMetaDefinicio_Id;
                        search.Id.Operator = Query.Operators.equals;

                        result = service.GetAllWithExtension(execParam, search);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow_IratMetaDefinicio = result.Ds.Tables[0].Rows[0];
                                UpdateIratMetaDefinicioNode(dataRow_IratMetaDefinicio, ParentNode);
                            }
                        }

                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.IratMetaDefinicio, IratMetaDefinicio_Id);
                    }

                    #endregion
                }
                break;
            case NodeType.EljarasiSzakasz:
                {
                    #region Elj�r�si szakasz:

                    string EljarasiSzakasz_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {
                        //EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                        //execParam.Record_Id = EljarasiSzakasz_Id;

                        //result = service.Get(execParam);

                        //if (String.IsNullOrEmpty(result.ErrorCode))
                        //{
                        //    EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
                        //    UpdateEljarasiSzakaszNode(erec_IratMetaDefinicio, ParentNode);
                        //}
                        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

                        search.Id.Value = EljarasiSzakasz_Id;
                        search.Id.Operator = Query.Operators.equals;

                        result = service.GetAll(execParam, search);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow_IMD_EljarasiSzakasz = result.Ds.Tables[0].Rows[0];
                                UpdateEljarasiSzakaszNode(dataRow_IMD_EljarasiSzakasz, ParentNode);
                            }
                        }

                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.EljarasiSzakasz, EljarasiSzakasz_Id);
                    }

                    #endregion
                }
                break;
            case NodeType.Irattipus:
                {
                    #region Iratt�pus:

                    string Irattipus_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {
                        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                        //execParam.Record_Id = Irattipus_Id;

                        //result = service.Get(execParam);

                        //if (String.IsNullOrEmpty(result.ErrorCode))
                        //{
                        //    EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
                        //    UpdateIrattipusNode(erec_IratMetaDefinicio, ParentNode);
                        //}
                        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

                        search.Id.Value = Irattipus_Id;
                        search.Id.Operator = Query.Operators.equals;

                        result = service.GetAll(execParam, search);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (result.Ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow_IMD_Irattipus = result.Ds.Tables[0].Rows[0];
                                UpdateIrattipusNode(dataRow_IMD_Irattipus, ParentNode);
                            }
                        }

                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.Irattipus, Irattipus_Id);
                    }

                    #endregion
                }
                break;
            case NodeType.Custom:
                {
                    #region Custom:

                    string CustomTreeNode_Id = ExtractNodeIdFromNodeTypeName(selectedTreeNode.Value);

                    if (ParentNode != null)
                    {

                        // Custom node bet�lt�s�t az �gyf�l v�gzi
                        IMDTerkepEventArgs e = new IMDTerkepEventArgs(CustomTreeNode_Id, NodeType.Custom);
                        if (CustomTreeNodesDictionary.ContainsKey(CustomTreeNode_Id))
                        {
                            CustomTreeNodeItem customTreeNodeItem = null;
                            CustomTreeNodesDictionary.TryGetValue(CustomTreeNode_Id, out customTreeNodeItem);
                            if (customTreeNodeItem != null)
                            {
                                e.nodeTypeName = customTreeNodeItem.NodeTypeName;
                            }
                        }
                        OnRefreshingSelectedNode(e);

                    }
                    else
                    {
                        TreeView1.Nodes.Clear();
                        LoadTreeView(NodeType.Custom, CustomTreeNode_Id);
                    }

                    #endregion
                }
                break;
            default:
                break;
        }

        //// gener�lunk egy esem�nyt, az als� szint felt�lt�shez
        //if (ParentNode != null)
        //{
        //    // Custom node bet�lt�s�t az �gyf�l v�gzi
        //    string nodeId = ExtractNodeIdFromNodeTypeName(ParentNode.Value);
        //    NodeType nodeType = ExtractNodeTypeFromNodeTypeName(ParentNode.Value);
        //    IMDTerkepEventArgs e = new IMDTerkepEventArgs(nodeId, nodeType);
        //    if (nodeType == NodeType.Custom)
        //    {
        //        if (CustomTreeNodesDictionary.ContainsKey(nodeId))
        //        {
        //            CustomTreeNodeItem customTreeNodeItem = null;
        //            CustomTreeNodesDictionary.TryGetValue(nodeId, out customTreeNodeItem);
        //            if (customTreeNodeItem != null)
        //            {
        //                e.nodeTypeName = customTreeNodeItem.NodeTypeName;
        //            }
        //        }
        //    }
        //    OnRefreshingSelectedNode(e);
        //}

        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs()); // ennek hi�ny�ban a befogad� komponens elveszti a be�ll�t�sait (pl. gombok)

    }

    //// TreeView node kiv�laszt�sa Id �s NodeType alapj�n
    //public void SelectChildNode(String nodeTypeName, String Id)
    //{
    //    if (TreeView1.SelectedNode != null)
    //    {
    //        String ValuePath = "";
    //        ValuePath = TreeView1.SelectedNode.ValuePath;
    //        ValuePath += ValuePathSeparator + ConcatNodeType(nodeTypeName, Id);

    //        TreeView1.SelectedNode.Expanded = true;
    //        TreeNodeEventArgs tnea = new TreeNodeEventArgs(TreeView1.SelectedNode);
    //        TreeView1_TreeNodePopulate(TreeView1, tnea);

    //        if (!String.IsNullOrEmpty(ValuePath))
    //        {
    //            TreeNode selectedTreeNode = TreeView1.FindNode(ValuePath);
    //            if (selectedTreeNode != null)
    //            {
    //                selectedTreeNode.Selected = true;
    //                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
    //            }
    //        }
    //    }

    //}

    // TreeView node selected node gyermek kiv�laszt�sa Id alapj�n
    // felt�telezi az Id-k egyedis�g�t, t�pust�l f�ggetlen�l
    public void SelectChildNode(String Id)
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.Expanded = true;
            //TreeNodeEventArgs tnea = new TreeNodeEventArgs(TreeView1.SelectedNode);
            //TreeView1_TreeNodePopulate(TreeView1, tnea);

            if (!String.IsNullOrEmpty(Id))
            {
                foreach (TreeNode tn in TreeView1.SelectedNode.ChildNodes)
                {
                    if (ExtractNodeIdFromNodeTypeName(tn.Value) == Id)
                    {
                        tn.Selected = true;
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                        return;
                    }
                }
            }
        }

    }

    // TreeView friss�t�se a kijel�lt sor szintj�ig
    public void ExpandSelectedNode()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        selectedTreeNode.Expanded = true;
        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

    }



    #region TreeViewRowTexts

    public String GetTreeNodeText(String Kod, String Nev, NodeType nodeType, String ValuePath, String IconPath, String IconAlt, String Id)
    {
        String TreeNodeText = String.Format(ItemFormatString
                                                , SetNodeUrl("<img src='" + IconPath
                                                    + "' alt='" + IconAlt
                                                    + "' />"
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                                , SetNodeUrl(" <b>" + Kod + "&nbsp;</b>"
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                                , SetNodeUrl(Nev
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                            );
        return TreeNodeText;
    }

    #endregion TreeViewRowTexts

    private string SetNodeUrl(String szoveg, String ValuePath, NodeType nodeType, String Id)
    {
        String strClass = GetNodeTypeClass(nodeType);

        string NodeTypeName = GetNameByNodeType(nodeType);

        return "<a" + (String.IsNullOrEmpty(strClass) ? "" : String.Format(ClassFormatString, strClass))
            + " href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s"
            + (String.IsNullOrEmpty(ValuePath) ? "" : ValuePath.Replace(ValuePathSeparator.ToString(), "\\\\") + "\\\\")
            + ConcatNodeType(NodeTypeName, Id) + "');\">" + szoveg + "</a>";
    }


}
