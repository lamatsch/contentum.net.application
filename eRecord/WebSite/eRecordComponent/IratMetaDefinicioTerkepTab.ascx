<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratMetaDefinicioTerkepTab.ascx.cs" Inherits="eRecordComponent_IratMetaDefinicioTerkepTab" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
    
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %> 
<%@ Register Src="../Component/TreeViewHeader.ascx" TagName="TreeViewHeader" TagPrefix="tvh" %> 
 <%@ Register Src="IratMetaDefinicioTerkep.ascx" TagName="IratMetaDefinicioTerkep"
    TagPrefix="imdt" %>         
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <tvh:TreeViewHeader ID="TreeViewHeader1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
<%--    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />--%>
    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always" OnLoad="TreeViewUpdatePanel_Load">
        <ContentTemplate>
        
    <%-- NodeFilter --%>
    <asp:HiddenField ID="ObjMetaDefinicioFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="ObjMetaAdataiFilter_HiddenField" runat="server" />
   <%-- /NodeFilter --%>   

     <asp:Panel ID="MainPanel" runat="server" Visible="true">
     
    <ajaxToolkit:CollapsiblePanelExtender ID="MetaAdatokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="400"
        ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>   
         
         <table style="width: 100%;">
<%--
             <tr>
                <td style="width: 100%;">
                            <table style="width: 100%;">
                               <tr>
                                    <td style="width: 100%;">                               
                                        <asp:Panel ID="PanelGombok" runat="server" Visible="false">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                               <tr>
                                                    <td style="width: 0%; text-align: center;">
                                                        <asp:Label ID="labelSelectedNode" runat="server" /> 
                                                    </td>
                                                    <td style="width: 0%; text-align: center;">
                                                        <asp:Label ID="labelSelectedNodeChild" runat="server" /> 
                                                    </td>
                                                    <td style="width: 0%; text-align: left;">
                                                        &nbsp; 
                                                    </td>                                                     
                                               </tr>       
                                                <tr>                                             
                                                    <td style="width: 0%; text-align: center;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%">
                                                            <tr>
                                                                <td style="width: 0%">
                                                                    <asp:ImageButton ID="Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')" />
                                                                </td>
                                                                <td style="width: 0%">
                                                                    <asp:ImageButton ID="Modositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg" 
                                                                        onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')"/>
                                                                </td>
                                                                <td style="width: 0%">
                                                                    <asp:ImageButton ID="Torles" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg" 
                                                                        onmouseover="swapByName(this.id,'torles_trap2.jpg')" onmouseout="swapByName(this.id,'torles_trap.jpg')"/> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:ImageButton ID="ExpandNode" runat="server" ImageUrl="~/images/hu/trapezgomb/csomopont_kibontasa_trap.jpg"
                                                                                    onmouseover="swapByName(this.id,'csomopont_kibontasa_trap2.jpg')" onmouseout="swapByName(this.id,'csomopont_kibontasa_trap.jpg')"/>
                                                                </td>
                                                            </tr>                                                                                                                                                                                 
                                                        </table>
                                                    </td>
                                                                                             
                                                    <td style="width: 0%; text-align: center;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%">
                                                            <tr>
                                                                <td style="width: 0%;text-align:top;">
                                                                    <asp:ImageButton ID="Letrehozas" runat="server" ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'felvitel_trap2.jpg')" onmouseout="swapByName(this.id,'felvitel_trap.jpg')"/>
                                                                </td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
                                                        </table>
                                                   </td>
                                                    <td style="width: 0%; text-align: center;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%">                                                   
                                                            <tr>
                                                                <td style="width: 0%" colspan="3">
                                                                    <asp:CheckBox ID="cbKivalasztUjElem" Text="Felvitt elem automatikus kiv�laszt�sa" TextAlign="Right" runat="server" />
                                                                </td>                                                                
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
                                                        </table>
                                                   </td>
                                                    <td style="width: 0%; text-align: center;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%">
                                                            <tr>
                                                                <td style="width: 0%;text-align:top;">
                                                                    <asp:ImageButton ID="LetrehozasMetadefinicio" runat="server" ImageUrl="~/images/hu/trapezgomb/metadefinicio_hozzarendeles_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'metadefinicio_hozzarendeles_trap2.jpg')" onmouseout="swapByName(this.id,'metadefinicio_hozzarendeles_trap.jpg')"/>
                                                                </td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
                                                        </table>
                                                   </td>
                                                </tr>

                                            </table> 
                                        </asp:Panel>
                                    </td>
                               </tr>
--%>
                            <tr>
                                <td style="width: 100%;">
                                    <table style="width: 100%;">
                                
                                    <td style="width: 100%; text-align: left; border: solid 1px Silver; ">                   
                                <tr>
                                    <td style="width: 100%; text-align: left; border: solid 1px Silver; ">
                                        <%-- az ujonnan letrehozott node-ok kivalasztasahoz szukseges adatok tarolasara --%>
                                        <asp:HiddenField ID="SelectedNodeId_HiddenField" runat="server" />
                                        <asp:HiddenField ID="SelectedNodeName_HiddenField" runat="server" />

                                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                                            <imdt:IratMetaDefinicioTerkep ID="IratMetaDefinicioTerkep1" runat="server"
                                                OnSelectedNodeChanged = "IratMetaDefinicioTerkep_SelectedNodeChanged"
                                                OnTreeNodePopulate = "IratMetaDefinicioTerkep_TreeNodePopulate"
                                                OnTreeViewLoading = "IratMetaDefinicioTerkep_TreeViewLoading"
                                                OnRefreshingSelectedNode = "IratMetaDefinicioTerkep_RefreshSelectedNode"
                                                OnRefreshingSelectedNodeChildren = "IratMetaDefinicioTerkep_RefreshSelectedNodeChildren"
                                            />                                        
                                        </asp:Panel>     
                                    </td>                                   
                               </tr>                               
                            </table>
                </td>
                <%-- <td>                 
                    <table>
                        <tr>
                        <td>
                     <eUI:eFormPanel ID="EFormPanel_IratMozgatas" runat="server">
                         <table cellspacing="0" cellpadding="0" border="0" width="100%">
                             <tr class="urlapSor">
                                 <td class="mrUrlapCaptionCenter_nowidth">
                                     <asp:Label ID="Label1" runat="server" Text="Metaadatok&nbsp;mozgat�sa"></asp:Label>
                                 </td>
                             </tr>
                             <tr>
                                 <td  style="vertical-align: top;">
                                     <hr style="width: 90%;" />
                                 </td>
                             </tr>
                             <tr class="urlapSor">
                                 <td>
                                     <asp:Label ID="Label_Mozgatas" runat="server" Text="Mozgat�s ide:"></asp:Label>
                                 </td>
                                 </tr>
                                 <tr class="urlapSor">
                                 <td>
                                     <asp:DropDownList ID="IratMetaDefinicio_DropDownList" runat="server" CssClass="mrUrlapInputKozepes">
                                     </asp:DropDownList>
                                 </td>
                                 </tr>
                               <tr class="urlapSor">
                                 <td>
                                     <asp:Button ID="Button_Mozgat" runat="server" Text="Mehet" OnClick="Button_Mozgat_Click" />
                                 </td>
                             </tr>
                             <tr>
                                 <td  style="vertical-align: top;">
                                     <hr style="width: 90%;" />
                                 </td>
                             </tr>
                         </table>
                     </eUI:eFormPanel>
                     </td>
                     </tr>
                     <tr>
                     <td>
                     <eUI:eFormPanel ID="EFormPanel_UgyiratDarabKezeles" runat="server">
                         <table cellspacing="0" cellpadding="0" border="0" width="100%">
                             <tr class="urlapSor">
                                 <td class="mrUrlapCaptionCenter_nowidth">
                                     <asp:Label ID="Label2" runat="server" Text="Metaadatok&nbsp;kezel�se"></asp:Label>
                                 </td>
                                 
                             </tr>
                             <tr>
                                 <td style="vertical-align: top;">
                                     <hr style="width: 90%;" />
                                 </td>
                             </tr>
                             <tr class="urlapSor">
                                 <td>
                                     <asp:Label ID="Label_UjUgyiratDarab" runat="server" Text="�j metaadat:"></asp:Label>
                                 </td>
                             </tr>
                             <tr class="urlapSor">
                                 <td>
                                     <asp:TextBox ID="UjMetaadatNeve_TextBox" runat="server" CssClass="mrUrlapInputKozepes"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr class="urlapSor">
                                 <td>
                                     <asp:ImageButton ID="ImageButton_UjMetaadat" runat="server" ImageUrl="~/images/hu/trapezgomb/uj_trap.jpg" OnClick="ImageButton_UjMetaadat_Click" /></td>
                     </tr>
                        <tr>
                            <td>
                                <hr style="width: 90%;" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                                <asp:Label ID="Label_DeleteMetaadat" runat="server" Text="Metaadat t�rl�se:"></asp:Label></td>
                        </tr>
                        <tr class="urlapSor">
                            <td>
                                <asp:ImageButton ID="ImageButton_DeleteMetaadat" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg" OnClick="ImageButton_DeleteMetaadat_Click" /></td>
                        </tr>
                    </table>
                     </eUI:eFormPanel>
                     </td>
                     </tr>
                     </table>
                 </td>--%>
                          </tr>
                       </table>
                </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
