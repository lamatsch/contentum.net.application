//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
// csak a r�videbb megnevez�s�rt
using IMDterkep = eRecordComponent_IratMetaDefinicioTerkep;

public partial class eRecordComponent_IratMetaDefinicioTerkepTab : System.Web.UI.UserControl
{
    private const string strSorszam = "sorsz�m";
    private const string valueOpcionalis0 = "k�telez�";
    private const string valueOpcionalis1 = "opcion�lis";
    private const string valueIsmetlodo0 = "egyszeri";
    private const string valueIsmetlodo1 = "ism�tl�d�";
    private const string strFunkcio = "<b> Funkci�: </b>";
    private const string strFelettes = "<b> K�zvetlen&nbsp;felettes: </b>";
    private const string strSzinkronizalt = "szinkroniz�lt";
    private const string strOrokolt = "eredet";

    private const string strMetaDefinicioNodeNev = "Definicio";
    private const string strMetaDefinicioNodeText = "Metadefin�ci�";

    private const string strIconPathMetaDefinicio = "images/hu/egyeb/treeview_metadefinicio.gif";
    private const string strIconAltMetaDefinicio = "Metadefin�ci�";
    private const string strIconPathMetaAdat = "images/hu/egyeb/treeview_metaadat.gif";
    private const string strIconAltMetaAdat = "Metaadat";

    private const string strMetaAdatNodeNev = "Metaadat";
    private const string strMetaAdatNodeText = "Metaadat";
    private const string strCustomNodeClass = "TreeViewCustomNode";
    private const string strCustomNodeClassInherited = "TreeViewCustomNodeInherited";

    // CustomNode kieg�sz�t�se navig�ci�s elemmel
    private const string LinkImageFormatString = "&nbsp;<a href='{0}' target='_blank'><img src='{1}' alt='{2}' /></a>";
    private const string IconPathLinkImage = "images/hu/egyeb/right-arrow.jpg";
    private const string IconAltLinkImage = "Ugr�s";
    private const string IconHrefFormatString = "MetaAdatokForm.aspx?" + QueryStringVars.Command + "={0}&" + QueryStringVars.SelectedTab + "={1}&" + QueryStringVars.Id + "={2}";

    //private bool IsFilteredWithoutHit;
    //private string ObjMetaDefinicioFilter = "";
    //private string ObjMetaAdataiFilter = "";

    // �rt�ke megmutatja, hogy a custom node-ok az irat metadefin�ci� t�rk�p
    // egy node-j�nak al�rendelt node t�pusai el� vagy m�g� ker�ljenek
    private IMDterkep.CustomNodePositionType customNodePosition = IMDterkep.CustomNodePositionType.BeforeOthers;

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            //MainPanel.Visible = value;
            TreeViewHeader1.Visible = value;
            if (!value)
            {
                IratMetaDefinicioTerkep1.ClearTreeView();
            }
        }
    }

    public string Command = "";
    private PageView pageView = null;

    private String ParentId = "";

    private string RegisterClientScriptDeleteConfirm()
    {
        string js = "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n\\n"
        + "Biztosan t�rli a kijel�lt csom�pontot �s lesz�rmazottait?"
        + "')) {  } else return false;";
        return js;
    }

    private string RegisterClientScriptSetElementValue(string componentId, string value)
    {
        return "var element = window.document.getElementById('" + componentId + "'); if (element == null) return; else element.value = '" + value + "';";
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;

    public string ObjMetaDefinicioFilter
    {
        get { return ObjMetaDefinicioFilter_HiddenField.Value; }
        set { ObjMetaDefinicioFilter_HiddenField.Value = value; }
    }

    public string ObjMetaAdataiFilter
    {
        get { return ObjMetaAdataiFilter_HiddenField.Value; }
        set { ObjMetaAdataiFilter_HiddenField.Value = value; }
    }

    private void SetNodeFilter(string objMetaDefinicioFilter, string objMetaAdataiFilter)
    {
        ObjMetaDefinicioFilter = objMetaDefinicioFilter;
        ObjMetaAdataiFilter = objMetaAdataiFilter;
    }

    private void ClearNodeFilter()
    {
        ObjMetaDefinicioFilter = "";
        ObjMetaAdataiFilter = "";
    }
    

    #region dictionary item oszt�lyok

    [Serializable]
    private class Obj_MetaAdataiItem
    {
        private string _Id;
        private string _Obj_MetaDefinicio_Id;
        private string _Targyszavak_Id;
        private string _TargySzavak;
        private int _Sorszam;
        private string _Funkcio;
        private string _Ismetlodo;
        private string _Opcionalis;
        private string _SPSSzinkronizalt;
        private int _Szint;
        private bool _IsExpanded;

        public Obj_MetaAdataiItem()
        {
            _Id = "";
            _Obj_MetaDefinicio_Id = "";
            _Targyszavak_Id = "";
            _TargySzavak = "";
            _Sorszam = 0;
            _Funkcio = "";
            _Ismetlodo = "";
            _Opcionalis = "";
            _SPSSzinkronizalt = "0";
            _Szint = 0;
            _IsExpanded = false;
        }

        public Obj_MetaAdataiItem(string Id, string Obj_MetaDefinicio_Id, string Targyszavak_Id, string TargySzavak,
            int Sorszam, string Funkcio, string Ismetlodo, string Opcionalis, string SPSSzinkronizalt)
        {
            _Id = Id;
            _Obj_MetaDefinicio_Id = Obj_MetaDefinicio_Id;
            _Targyszavak_Id = Targyszavak_Id;
            _TargySzavak = TargySzavak;
            _Sorszam = Sorszam;
            _Funkcio = Funkcio;
            _Ismetlodo = Ismetlodo;
            _Opcionalis = Opcionalis;
            _SPSSzinkronizalt = SPSSzinkronizalt;
        }

        public Obj_MetaAdataiItem(string Id, string Obj_MetaDefinicio_Id, string Targyszavak_Id, string TargySzavak,
            int Sorszam, string Funkcio, string Ismetlodo, string Opcionalis, string SPSSzinkronizalt, int Szint)
        {
            _Id = Id;
            _Obj_MetaDefinicio_Id = Obj_MetaDefinicio_Id;
            _Targyszavak_Id = Targyszavak_Id;
            _TargySzavak = TargySzavak;
            _Sorszam = Sorszam;
            _Funkcio = Funkcio;
            _Ismetlodo = Ismetlodo;
            _Opcionalis = Opcionalis;
            _SPSSzinkronizalt = SPSSzinkronizalt;
            _Szint = Szint;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Obj_MetaDefinicio_Id
        {
            get { return _Obj_MetaDefinicio_Id; }
            set { _Obj_MetaDefinicio_Id = value; }
        }

        public string Targyszavak_Id
        {
            get { return _Targyszavak_Id; }
            set { _Targyszavak_Id = value; }
        }

        public string TargySzavak
        {
            get { return _TargySzavak; }
            set { _TargySzavak = value; }
        }

        public int Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }

        public string Funkcio
        {
            get { return _Funkcio; }
            set { _Funkcio = value; }
        }

        public string Ismetlodo
        {
            get { return _Ismetlodo; }
            set { _Ismetlodo = value; }
        }

        public string Opcionalis
        {
            get { return _Opcionalis; }
            set { _Opcionalis = value; }
        }

        public int Szint
        {
            get { return _Szint; }
            set { _Szint = value; }
        }

        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set { _IsExpanded = value; }
        }

        #endregion properties

    }

    [Serializable]
    private class Obj_MetaDefinicioItem
    {
        private string _Id;
        private string _Objtip_Id;
        private string _Objtip_Id_Column;
        private string _IratMetadefinicio_Id; // ColumnValue
        //private string _DefinicioTipus; // itt mindig C2
        private string _Felettes_Obj_Meta;
        private string _Felettes_Obj_Meta_CTT;
        private string _ContentType;
        private string _SPSSzinkronizalt;
        private bool _IsExpanded;

        public Obj_MetaDefinicioItem()
        {
            _Id = "";
            _Objtip_Id = "";
            _Objtip_Id_Column = "";
            _IratMetadefinicio_Id = "";
            _Felettes_Obj_Meta = "";
            _Felettes_Obj_Meta_CTT = "";
            _ContentType = "";
            _SPSSzinkronizalt = "0";
            _IsExpanded = false;
        }

        public Obj_MetaDefinicioItem(string Id, string Objtip_Id, string Objtip_Id_Column,
            string IratMetadefinicio_Id, string Felettes_Obj_Meta, string Felettes_Obj_Meta_CTT, string ContentType, string SPSSzinkronizalt)
        {
            _Id = Id;
            _Objtip_Id = Objtip_Id;
            _Objtip_Id_Column = Objtip_Id_Column;
            _IratMetadefinicio_Id = IratMetadefinicio_Id;
            _Felettes_Obj_Meta = Felettes_Obj_Meta;
            _Felettes_Obj_Meta_CTT = Felettes_Obj_Meta_CTT;
            _ContentType = ContentType;
            _SPSSzinkronizalt = SPSSzinkronizalt;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        public string Objtip_Id
        {
            get { return _Objtip_Id; }
            set { _Objtip_Id = value; }
        }

        public string Objtip_Id_Column
        {
            get { return _Objtip_Id_Column; }
            set { _Objtip_Id_Column = value; }
        }

        public string IratMetadefinicio_Id
        {
            get { return _IratMetadefinicio_Id; }
            set { _IratMetadefinicio_Id = value; }
        }

        public string Felettes_Obj_Meta
        {
            get { return _Felettes_Obj_Meta; }
            set { _Felettes_Obj_Meta = value; }
        }

        public string Felettes_Obj_Meta_CTT
        {
            get { return _Felettes_Obj_Meta_CTT; }
            set { _Felettes_Obj_Meta_CTT = value; }
        }

        public string ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }

        public string SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }

        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set { _IsExpanded = value; }
        }

        #endregion properties

    }

    #endregion dictionary item oszt�lyok

    #region Base Tree Dictionary
    Dictionary<String, Obj_MetaDefinicioItem> Obj_MetaDefinicioDictionary = new Dictionary<string, Obj_MetaDefinicioItem>();
    Dictionary<String, Obj_MetaAdataiItem> Obj_MetaAdataiDictionary = new Dictionary<string, Obj_MetaAdataiItem>();
    #endregion

    #region public Properties
    
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion

    #region Utils

    private String GetIdListFromTable(DataTable table, string Separator, bool bQuoteId)
    {
        List<String> Ids = new List<string>();
        if (table == null) return "";

        foreach (DataRow row in table.Rows)
        {
            string row_Id = row["Id"].ToString();
            if (bQuoteId)
            {
                row_Id = "'" + row_Id + "'";
            }

            Ids.Add(row_Id);
        }

        return String.Join(Separator, Ids.ToArray());
    }

    #endregion Utils

    
    protected void Page_Init(object sender, EventArgs e)
    {        
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        TreeViewHeader1.HeaderLabel = "Irat metadefin�ci�/metaadat t�rk�p";
        TreeViewHeader1.SearchObjectType = typeof(MetaAdatHierarchiaSearch);
        TreeViewHeader1.SetFilterFunctionButtonsVisible(true);
        TreeViewHeader1.SetNodeFunctionButtonsVisible(true);
        TreeViewHeader1.SetChildFunctionButtonsVisible(true);
        TreeViewHeader1.SetCustomNodeFunctionButtonsVisible(true);

        TreeViewHeader1.HeaderCustomNodeButtonsLabel = strMetaDefinicioNodeText;
        TreeViewHeader1.AttachedTreeView = IratMetaDefinicioTerkep1.TreeView;

        TreeViewHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("MetaAdatokHierarchiaSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        TreeViewHeader1.NodeFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderNodeFunctionButtonsClick);
        TreeViewHeader1.ChildFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderChildFunctionButtonsClick);

        TreeViewHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TreeViewUpdatePanel.ClientID);

        //IratMetaDefinicioTerkep1.IconPathCustomTreeNode = strIconPathMetaDefinicio;
        //IratMetaDefinicioTerkep1.IconAltCustomTreeNode = strIconAltMetaDefinicio;
        //IratMetaDefinicioTerkep1.SetNodeTypeText(IMDterkep.NodeType.Custom, strMetaDefinicioNodeText);
        IratMetaDefinicioTerkep1.SetNodeTypeClass(IMDterkep.NodeType.Custom, strCustomNodeClass);
        IratMetaDefinicioTerkep1.CustomNodePosition = customNodePosition;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

             //ViewState -bol visszatolti az elmentett statuszokat
            try
            {
                if (ViewState["Obj_MetaAdataiDictionary"] != null)
                {
                    Obj_MetaAdataiDictionary = (Dictionary<String, Obj_MetaAdataiItem>)ViewState["Obj_MetaAdataiDictionary"];
                }

                if (ViewState["Obj_MetaDefinicioDictionary"] != null)
                {
                    Obj_MetaDefinicioDictionary = (Dictionary<String, Obj_MetaDefinicioItem>)ViewState["Obj_MetaDefinicioDictionary"];
                }

            }
            catch
            { }
        }
        else
        {
            //ReLoadTab();        
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //IratMetaDefinicioTerkep1.Width = new Unit("100%");
        //IratMetaDefinicioTerkep1.Height = new Unit("80%");
    }

    private void TreeViewHeaderNodeFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Invalidate:
                InvalidateSelectedNode();
                break;
            case CommandName.ExpandNode:
                IratMetaDefinicioTerkep1.ExpandSelectedNodeAllLevels();
                break;
        }
        
    }

    private void TreeViewHeaderChildFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        //switch (e.CommandName)
        //{
        //}
    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.View)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshSelectedNode:
                        break;
                    case EventArgumentConst.refreshSelectedNodeChildren:
                        break;
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
    }


    public void ReLoadTab()
    {
        if (ParentForm != "MetaAdatKarbantartas")    // TODO: a szulo form meghatarozasa utan ezt kivenni
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            Active = false;
        }
        else
        {
            if (!Active) return;
            LoadTreeView();

            pageView.SetViewOnPage(Command);
        }    
    }

    protected void LoadTreeView()
    {
        ViewState["Loaded"] = false;

        // Dictionaryk t�rl�se
        Obj_MetaAdataiDictionary.Clear();
        Obj_MetaDefinicioDictionary.Clear();

        bool isFilterActive = SetKeresesFilter();

        if (isFilterActive && IratMetaDefinicioTerkep1.IsFilteredWithoutHit)
        {
            //IratMetaDefinicioTerkep1.ClearTreeView();
            SetAllButtonsEnabled(false);
            TreeViewHeader1.HeaderNodeButtonsLabel = "";
            TreeViewHeader1.HeaderChildButtonsLabel = "";
        }

        IratMetaDefinicioTerkep1.LoadTreeView();

        ViewState["Loaded"] = true;
    }

    protected void IratMetaDefinicioTerkep_RefreshSelectedNode(object sender, IMDterkep.IMDTerkepEventArgs e)
    {
        if (e == null || e.nodeId == null || e.nodeId == "") return;


        if (e.nodeType == IMDterkep.NodeType.Custom)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = e.nodeId;
            Result result;

            String NodeTypeName = e.nodeTypeName;
            if (NodeTypeName == strMetaDefinicioNodeNev)
            {
                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();

                search.Id.Value = e.nodeId;
                search.Id.Operator = Query.Operators.equals;

                result = service.GetAllWithExtension(execParam, search);

                if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables[0].Rows.Count == 1)
                {
                    DataRow dataRow_objMetaDefinicioExtended = result.Ds.Tables[0].Rows[0];
                    UpdateObj_MetaDefinicioNode(dataRow_objMetaDefinicioExtended, IratMetaDefinicioTerkep1.TreeView.SelectedNode.Parent);
                }
            }
            else if (NodeTypeName == strMetaAdatNodeNev)
            {
                EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                search.Id.Value = e.nodeId;
                search.Id.Operator = Query.Operators.equals;
                result = service.GetAllWithExtension(execParam, search);

                if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables[0].Rows.Count == 1)
                {
                    DataRow dataRow_objMetaAdataiExtended = result.Ds.Tables[0].Rows[0];
                    UpdateObj_MetaAdataiNode(dataRow_objMetaAdataiExtended, IratMetaDefinicioTerkep1.TreeView.SelectedNode.Parent);
                }

            }

        }
    }

    protected void IratMetaDefinicioTerkep_RefreshSelectedNodeChildren(object sender, IMDterkep.IMDTerkepEventArgs e)
    {
        if (e == null || e.nodeId == null || e.nodeId == "") return;
        if (IratMetaDefinicioTerkep1.IsFilteredWithoutHit) return;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result;

        string NodeId = e.nodeId;   // ha �j node-ot vesz�nk fel, v�ltozik

        bool isFilterAlreadySet = false; // flag, hogy sz�ks�g eset�n is csak egyszer t�lts�k be a filtert

        if (e.nodeType == IMDterkep.NodeType.IraIrattariTetelek
            || e.nodeType == IMDterkep.NodeType.IratMetaDefinicio
            || e.nodeType == IMDterkep.NodeType.Irattipus)
        {
            
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();

            if (e.nodeType == IMDterkep.NodeType.IraIrattariTetelek)
            {
                search.ColumnValue.Value = IratMetaDefinicioTerkep1.GetIratMetaDefinicioIdFromDictionaryIraIrattariTetelek(e.nodeId);
            }
            else
            {
                search.ColumnValue.Value = e.nodeId;
            }
            search.ColumnValue.Operator = Query.Operators.equals;
            result = service.GetAllWithExtension(execParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string Id = row["Id"].ToString();

                    bool canBeAdded = true;

                    if (!String.IsNullOrEmpty(ObjMetaDefinicioFilter) && !IMDterkep.isInFilter(Id, ObjMetaDefinicioFilter))
                    {
                        if (isFilterAlreadySet == false)
                        {
                            isFilterAlreadySet = SetKeresesFilter();
                        }
                        if (isFilterAlreadySet == true)
                        {
                            // az �j filterrel ism�t ellen�rizz�k
                            if (!String.IsNullOrEmpty(ObjMetaDefinicioFilter) && !IMDterkep.isInFilter(Id, ObjMetaDefinicioFilter))
                            {
                                canBeAdded = false;
                            }
                        }
                    }

                    if (canBeAdded)
                    {
                        UpdateObj_MetaDefinicioNode(row, IratMetaDefinicioTerkep1.TreeView.SelectedNode);
                    }
                    else if (Obj_MetaDefinicioDictionary.ContainsKey(Id))
                    {
                        Obj_MetaDefinicioDictionary.Remove(Id);
                        TreeNode node = IratMetaDefinicioTerkep1.TreeView.FindNode(IratMetaDefinicioTerkep1.TreeView.SelectedNode.ValuePath + IratMetaDefinicioTerkep1.ValuePathSeparator.ToString() + IratMetaDefinicioTerkep1.ConcatNodeType(IMDterkep.NodeType.Custom, Id));
                        if (node != null)
                        {
                            IratMetaDefinicioTerkep1.DeleteNode(node);
                        }
                    }

                }
            }
        }
        else if (e.nodeType == IMDterkep.NodeType.Custom)    // ez most metadefin�ci� vagy metaadat is lehet
        {
            String NodeTypeName = e.nodeTypeName;
            if (NodeTypeName == strMetaDefinicioNodeNev)
            {
                EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                //search.Obj_MetaDefinicio_Id.Value = NodeId;
                //search.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

                search.OrderBy = "Sorszam";

                //result = service.GetAllWithExtension(execParam, search);
                result = service.GetAllWithExtensionByObjMetaDefinicio(execParam, search, e.nodeId, false);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string Id = row["Id"].ToString();

                        bool canBeAdded = true;

                        if (!String.IsNullOrEmpty(ObjMetaAdataiFilter) && !IMDterkep.isInFilter(Id, ObjMetaAdataiFilter))
                        {
                            if (isFilterAlreadySet == false)
                            {
                                isFilterAlreadySet = SetKeresesFilter();
                            }
                            if (isFilterAlreadySet == true)
                            {
                                // az �j filterrel ism�t ellen�rizz�k 
                                if (!String.IsNullOrEmpty(ObjMetaAdataiFilter) && !IMDterkep.isInFilter(Id, ObjMetaAdataiFilter))
                                {
                                    canBeAdded = false;
                                }
                            }
                        }

                        if (canBeAdded)
                        {
                           UpdateObj_MetaAdataiNode(row, IratMetaDefinicioTerkep1.TreeView.SelectedNode);
                        }
                        else if (Obj_MetaAdataiDictionary.ContainsKey(Id))
                        {
                            Obj_MetaAdataiDictionary.Remove(Id);
                            TreeNode node = IratMetaDefinicioTerkep1.TreeView.FindNode(IratMetaDefinicioTerkep1.TreeView.SelectedNode.ValuePath + IratMetaDefinicioTerkep1.ValuePathSeparator.ToString() + IratMetaDefinicioTerkep1.ConcatNodeType(IMDterkep.NodeType.Custom, Id));
                            if (node != null)
                            {
                                IratMetaDefinicioTerkep1.DeleteNode(node);
                            }
                        }
                    }
                }
            }
            else if (NodeTypeName == strMetaAdatNodeNev)
            {
                // ennek nem lehetnek gyerekei

            }

        }

        // l�trehoz�skor, ig�ny szerint kiv�lasztjuk az �j node-ot
        if (TreeViewHeader1.SelectNewChild == true)
        {
            String SelectedId = SelectedNodeId_HiddenField.Value;
            IratMetaDefinicioTerkep1.SelectChildNode(SelectedId);
        }

    }

    protected void IratMetaDefinicioTerkep_TreeViewLoading(object sender, IMDterkep.IMDTerkepEventArgs e)
    {
        if (e.nodeType == IMDterkep.NodeType.Custom)
        {
            String NodeTypeName = IratMetaDefinicioTerkep1.GetNodeTypeNameFromDictionaryCustom(e.nodeId);

            if (NodeTypeName == strMetaDefinicioNodeNev)
            {
                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = e.nodeId;
                Result result = service.Get(execParam);

                String IratMetadefinicio_Id = ((EREC_Obj_MetaDefinicio)result.Record).ColumnValue;

                ExecParam execParam_imd = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_imd.Record_Id = IratMetadefinicio_Id;
                EREC_IratMetaDefinicioService service_imd = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                Result res_imd = service_imd.Get(execParam_imd);

                TreeNode treeNode = null;

                EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)res_imd.Record;
                if (String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugytipus))
                {
                    treeNode = IratMetaDefinicioTerkep1.LoadTreeView(IMDterkep.NodeType.IraIrattariTetelek, IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIratMetaDefinicio(IratMetadefinicio_Id));
                }
                else if (String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                {
                    treeNode = IratMetaDefinicioTerkep1.LoadTreeView(IMDterkep.NodeType.IratMetaDefinicio, IratMetadefinicio_Id);
                }
                else if (String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus))
                {
                    treeNode = IratMetaDefinicioTerkep1.LoadTreeView(IMDterkep.NodeType.EljarasiSzakasz, IratMetadefinicio_Id);
                }
                else
                {
                    treeNode = IratMetaDefinicioTerkep1.LoadTreeView(IMDterkep.NodeType.Irattipus, IratMetadefinicio_Id);
                }

                if (treeNode != null)
                {
                    treeNode.Expanded = true;
                    foreach (TreeNode custTN in treeNode.ChildNodes)
                        if (IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(custTN.Value) == e.nodeId)
                        {
                            custTN.Selected = true;
                        }
                }
            }
        }

    }

    protected void IratMetaDefinicioTerkep_SelectedNodeChanged(object sender, EventArgs e)
    {
        IMDterkep.NodeType nodeType = IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode);

        //Torles.OnClientClick = RegisterClientScriptDeleteConfirm();
        TreeViewHeader1.InvalidateOnClientClick = RegisterClientScriptDeleteConfirm();

        switch (nodeType)
        {
            case IMDterkep.NodeType.AgazatiJelek:
                SetButtonsByAgazatiJelek();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.AgazatiJelek);  //"�gazati jel";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IraIrattariTetelek);  //"Iratt�ri t�tel"; 
                break;
            case IMDterkep.NodeType.IraIrattariTetelek:
                SetButtonsByIraIrattariTetelek();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IraIrattariTetelek);  //"Iratt�ri t�tel";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IratMetaDefinicio);  //"Irat metadefin�ci�";
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IratMetaDefinicio);  //"�gyt�pus";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.EljarasiSzakasz); //"Elj�r�si szakasz";
                break;
            case IMDterkep.NodeType.EljarasiSzakasz:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.EljarasiSzakasz);  //"Elj�r�si szakasz";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.Irattipus);  //"Iratt�pus";
                break;
            case IMDterkep.NodeType.Irattipus:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.Irattipus);  //"Iratt�pus";
                TreeViewHeader1.HeaderChildButtonsLabel = "";
                break;
            case IMDterkep.NodeType.Custom:
                {
                    String NodeTypeName = IratMetaDefinicioTerkep1.GetNodeTypeNameFromDictionaryCustom(IratMetaDefinicioTerkep1.SelectedId);
                    if (NodeTypeName == strMetaDefinicioNodeNev)
                    {
                        SetButtonsByObj_MetaDefinicio();
                        TreeViewHeader1.HeaderNodeButtonsLabel = strMetaDefinicioNodeText; //"Objektum metadefin�ci�";
                        TreeViewHeader1.HeaderChildButtonsLabel = strMetaAdatNodeText;
                    }
                    else if (NodeTypeName == strMetaAdatNodeNev)
                    {
                        SetButtonsByObj_MetaAdatai();
                        TreeViewHeader1.HeaderNodeButtonsLabel = strMetaAdatNodeText; //"T�rgysz� hozz�rendel�s";
                        TreeViewHeader1.HeaderChildButtonsLabel = strMetaAdatNodeText;
                    }
                }
                break;
            default:
                TreeViewHeader1.HeaderNodeButtonsLabel = "";  //�res
                TreeViewHeader1.HeaderChildButtonsLabel = "";  //�res
                SetAllButtonsEnabled(false);
                break;
        }

    }

    protected void IratMetaDefinicioTerkep_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        switch (IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(e.Node.Value))
        {
            case IMDterkep.NodeType.IraIrattariTetelek:
                AddObj_MetaDefinicioNode(e.Node);
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
                AddObj_MetaDefinicioNode(e.Node);
                break;
            case IMDterkep.NodeType.EljarasiSzakasz:
                // jelenleg ehhez nem rendelhet�
                //AddObj_MetaDefinicioNode(e.Node);
                break;
            case IMDterkep.NodeType.Irattipus:
                AddObj_MetaDefinicioNode(e.Node);
                break;
            case IMDterkep.NodeType.Custom:
                {
                    String Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(e.Node.Value);
                    String NodeTypeName = IratMetaDefinicioTerkep1.GetNodeTypeNameFromDictionaryCustom(Id);
                    if (NodeTypeName == strMetaDefinicioNodeNev)
                    {
                        AddObj_MetaAdataiNode(e.Node);
                    }
                }
                break;
            default:
                break;
        }
    }

    // IratMetaDefinicioTreeNode lehet val�di irat metadefin�ci� vagy IraIrattariTetel is, ami egyben k�pviseli
    // azt az IratMetaDefiniciot, melynek �gyk�re ez az iratt�ri t�tel �s �gyt�pusa null
    private void AddObj_MetaDefinicioNode(TreeNode IratMetaDefinicioTreeNode)
    {
        if (IratMetaDefinicioTerkep1.IsFilteredWithoutHit) return;

        #region Init
        EREC_Obj_MetaDefinicioService erec_Obj_MetaDefinicioService = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
        EREC_Obj_MetaDefinicioSearch erec_Obj_MetaDefinicioSearch = new EREC_Obj_MetaDefinicioSearch();
        ExecParam erec_Obj_MetaDefinicioExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_Obj_MetaDefinicioResult = new Result();
        #endregion

        #region tipus szerinti elk�l�n�t�s
        string NodeTypeName = IratMetaDefinicioTerkep1.ExtractNodeNameFromNodeTypeName(IratMetaDefinicioTreeNode.Value);
        string IratMetaDefinicio_Id = "";
        switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(NodeTypeName))
        {
            case IMDterkep.NodeType.IraIrattariTetelek:
                {
                    string IraIrattariTetelek_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(IratMetaDefinicioTreeNode.Value);
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetIratMetaDefinicioIdFromDictionaryIraIrattariTetelek(IraIrattariTetelek_Id);
                }
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
                IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(IratMetaDefinicioTreeNode.Value);
                break;
            case IMDterkep.NodeType.Irattipus:
                {
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(IratMetaDefinicioTreeNode.Value);
                }
                break;
        }

        if (String.IsNullOrEmpty(IratMetaDefinicio_Id)) return;

        #endregion tipus szerinti elk�l�n�t�s

        #region Search be�ll�t�s
        erec_Obj_MetaDefinicioSearch.ColumnValue.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_Obj_MetaDefinicioSearch.ColumnValue.Value = IratMetaDefinicio_Id;

        erec_Obj_MetaDefinicioSearch.DefinicioTipus.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_Obj_MetaDefinicioSearch.DefinicioTipus.Value = KodTarak.OBJMETADEFINICIO_TIPUS.C2;

        erec_Obj_MetaDefinicioSearch.OrderBy = "ContentType";

        erec_Obj_MetaDefinicioResult = erec_Obj_MetaDefinicioService.GetAllWithExtension(erec_Obj_MetaDefinicioExecParam, erec_Obj_MetaDefinicioSearch);
        if (!String.IsNullOrEmpty(erec_Obj_MetaDefinicioResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_Obj_MetaDefinicioDataSet = erec_Obj_MetaDefinicioResult.Ds;

        foreach (DataRow row in erec_Obj_MetaDefinicioDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(ObjMetaDefinicioFilter) && !IMDterkep.isInFilter(row_Id, ObjMetaDefinicioFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                UpdateObj_MetaDefinicioNode(row, IratMetaDefinicioTreeNode);
            }
        }
        #endregion

        // TODO: (teszt) ->Obj_MetaAdatai node-ok
        foreach (TreeNode tn in IratMetaDefinicioTreeNode.ChildNodes)
        {
            if (IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(tn.Value) == IMDterkep.NodeType.Custom)
            {
                AddObj_MetaAdataiNode(tn);
            }
        }
    }

    // Objektum metadefin�ci�hoz kapcsol�dik
    private void AddObj_MetaAdataiNode(TreeNode ObjMetaDefinicioTreeNode)
    {
        if (IratMetaDefinicioTerkep1.IsFilteredWithoutHit) return;

        #region Init
        EREC_Obj_MetaAdataiService erec_Obj_MetaAdataiService = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
        EREC_Obj_MetaAdataiSearch erec_Obj_MetaAdataiSearch = new EREC_Obj_MetaAdataiSearch();
        ExecParam erec_Obj_MetaAdataiExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_Obj_MetaAdataiResult = new Result();
        #endregion

        string Obj_MetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(ObjMetaDefinicioTreeNode.Value);
        if (String.IsNullOrEmpty(Obj_MetaDefinicio_Id)) return;

        #region Search be�ll�t�s
        //erec_Obj_MetaAdataiSearch.Obj_MetaDefinicio_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        //erec_Obj_MetaAdataiSearch.Obj_MetaDefinicio_Id.Value = Obj_MetaDefinicio_Id;

        erec_Obj_MetaAdataiSearch.OrderBy = "Sorszam";

        //erec_Obj_MetaAdataiResult = erec_Obj_MetaAdataiService.GetAllWithExtension(erec_Obj_MetaAdataiExecParam, erec_Obj_MetaAdataiSearch);
        erec_Obj_MetaAdataiResult = erec_Obj_MetaAdataiService.GetAllWithExtensionByObjMetaDefinicio(erec_Obj_MetaAdataiExecParam, erec_Obj_MetaAdataiSearch, Obj_MetaDefinicio_Id, false);
        if (!String.IsNullOrEmpty(erec_Obj_MetaAdataiResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_Obj_MetaAdataiDataSet = erec_Obj_MetaAdataiResult.Ds;

        foreach (DataRow row in erec_Obj_MetaAdataiDataSet.Tables[0].Rows)
        {
            string row_Id = row["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(ObjMetaAdataiFilter) && !IMDterkep.isInFilter(row_Id, ObjMetaAdataiFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                UpdateObj_MetaAdataiNode(row, ObjMetaDefinicioTreeNode);
            }
        }
        #endregion
    }

    #region UpdateTreeNodes

    private void UpdateObj_MetaDefinicioNode(DataRow dataRow_objMetaDefinicioExtended, TreeNode IratMetaDefinicioTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        bool isFelettesChanged = false;

        String Obj_MetaDefinicio_Id = dataRow_objMetaDefinicioExtended["Id"].ToString();
        String strContentType = dataRow_objMetaDefinicioExtended["ContentType"].ToString();
        String Felettes_Obj_Meta_CTT = dataRow_objMetaDefinicioExtended["Felettes_Obj_Meta_CTT"].ToString();
        String Felettes_Obj_Meta = dataRow_objMetaDefinicioExtended["Felettes_Obj_Meta"].ToString();

        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (Obj_MetaDefinicioDictionary.ContainsKey(Obj_MetaDefinicio_Id) == true)
        {
            Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
            Obj_MetaDefinicioDictionary.TryGetValue(Obj_MetaDefinicio_Id, out obj_MetaDefinicioItem);

            OrderChanged = (obj_MetaDefinicioItem.ContentType.ToString() == dataRow_objMetaDefinicioExtended["ContentType"].ToString() ? false : true);
            isFelettesChanged = (obj_MetaDefinicioItem.Felettes_Obj_Meta.ToString() == Felettes_Obj_Meta ? false : true);

            obj_MetaDefinicioItem.Felettes_Obj_Meta= Felettes_Obj_Meta;
            obj_MetaDefinicioItem.Felettes_Obj_Meta_CTT = Felettes_Obj_Meta_CTT;
            obj_MetaDefinicioItem.ContentType = dataRow_objMetaDefinicioExtended["ContentType"].ToString();

            Obj_MetaDefinicioDictionary.Remove(Obj_MetaDefinicio_Id);
            Obj_MetaDefinicioDictionary.Add(Obj_MetaDefinicio_Id, obj_MetaDefinicioItem);
        }
        else
        {
            Obj_MetaDefinicioItem Obj_MetaDefinicioItem = new Obj_MetaDefinicioItem(
                Obj_MetaDefinicio_Id,
                dataRow_objMetaDefinicioExtended["Objtip_Id"].ToString(),
                dataRow_objMetaDefinicioExtended["Objtip_Id_Column"].ToString(),
                dataRow_objMetaDefinicioExtended["ColumnValue"].ToString(),
                Felettes_Obj_Meta,
                Felettes_Obj_Meta_CTT,
                strContentType,
                dataRow_objMetaDefinicioExtended["SPSSzinkronizalt"].ToString()
                );
            Obj_MetaDefinicioDictionary.Add(Obj_MetaDefinicio_Id, Obj_MetaDefinicioItem);
            OrderChanged = true;
            isFelettesChanged = true;
        }

        ViewState["Obj_MetaDefinicioDictionary"] = Obj_MetaDefinicioDictionary;

        String SPSSzinkronizalt = "";
        if (dataRow_objMetaDefinicioExtended["SPSSzinkronizalt"].ToString() == "1")
        {
            SPSSzinkronizalt = " (" + strSzinkronizalt + ")";
        }

        if (!String.IsNullOrEmpty(Felettes_Obj_Meta_CTT))
        {
            Felettes_Obj_Meta_CTT = strFelettes + Felettes_Obj_Meta_CTT;
            String LinkHrefText_Felettes = String.Format(IconHrefFormatString, Command, "TabObjMetaDefinicioTerkepPanel", Felettes_Obj_Meta);
            String LinkText_Felettes = String.Format(LinkImageFormatString, LinkHrefText_Felettes, IconPathLinkImage, IconAltLinkImage);
            Felettes_Obj_Meta_CTT = Felettes_Obj_Meta_CTT + LinkText_Felettes;
        }

        String LinkHrefText = String.Format(IconHrefFormatString, Command, "TabObjMetaDefinicioTerkepPanel", Obj_MetaDefinicio_Id);
        String LinkText = String.Format(LinkImageFormatString, LinkHrefText, IconPathLinkImage, IconAltLinkImage);

        String Kod = strMetaDefinicioNodeText;
        String Nev = strContentType + SPSSzinkronizalt + LinkText + Felettes_Obj_Meta_CTT;
        
        String NodeText = "";

        String NodeValue = IratMetaDefinicioTerkep1.ConcatNodeType(IMDterkep.NodeType.Custom, Obj_MetaDefinicio_Id);
        String NodePath = IratMetaDefinicioTreeNode.ValuePath + IratMetaDefinicioTerkep1.ValuePathSeparator + NodeValue;

        TreeNode currentNode = IratMetaDefinicioTerkep1.TreeView.FindNode(NodePath);

        if (currentNode != null)
        {
            NodeText = IratMetaDefinicioTerkep1.GetTreeNodeText(Kod, Nev, IMDterkep.NodeType.Custom, IratMetaDefinicioTreeNode.ValuePath, strIconPathMetaDefinicio, strIconAltMetaDefinicio, Obj_MetaDefinicio_Id);

            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                IratMetaDefinicioTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            // megkeress�k a hely�t
            bool bFound = false;
            int i = -1;

            switch (customNodePosition)
            {
                case IMDterkep.CustomNodePositionType.AfterOthers:
                    // fel�l vannak az irat metadefin�ci� node-ok, alatta a custom node-ok, h�tulr�l keres�nk
                    // vigy�zat: String.Compare rel�ci�s jel �ll�sa!
                    i = IratMetaDefinicioTreeNode.ChildNodes.Count;
                    while (i > 0 && !bFound
                        && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(IratMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
                    {
                        TreeNode tn = IratMetaDefinicioTreeNode.ChildNodes[i];
                        string tnObj_MetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

                        Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
                        Obj_MetaDefinicioDictionary.TryGetValue(tnObj_MetaDefinicio_Id, out obj_MetaDefinicioItem);
                        if (obj_MetaDefinicioItem != null && String.Compare(obj_MetaDefinicioItem.ContentType, strContentType, true) <= 0)
                        {
                            bFound = true;
                        }
                        else
                        {
                            i--;
                        }
                    }
                    break;
                case IMDterkep.CustomNodePositionType.BeforeOthers:
                    // alul vannak az irat metadefin�ci� node-ok, felette a custom node-ok, el�lr�l keres�nk
                    // vigy�zat: String.Compare rel�ci�s jel �ll�sa!
                    i = 0;
                    while (i < IratMetaDefinicioTreeNode.ChildNodes.Count && !bFound
                        && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(IratMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
                    {
                        TreeNode tn = IratMetaDefinicioTreeNode.ChildNodes[i];
                        string tnObj_MetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

                        Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
                        Obj_MetaDefinicioDictionary.TryGetValue(tnObj_MetaDefinicio_Id, out obj_MetaDefinicioItem);
                        if (obj_MetaDefinicioItem != null && String.Compare(obj_MetaDefinicioItem.ContentType, strContentType, true) > 0)
                        {
                            bFound = true;
                        }
                        else
                        {
                            i++;
                        }
                    }
                    break;
            }

            IratMetaDefinicioTerkep1.AddCustomNodeAt(i
                                      , IratMetaDefinicioTreeNode
                                      , Kod
                                      , Nev
                                      , strMetaDefinicioNodeNev
                                      , strIconPathMetaDefinicio
                                      , strIconAltMetaDefinicio
                                      , Obj_MetaDefinicio_Id);

            currentNode = IratMetaDefinicioTreeNode.ChildNodes[i];

            if (isSelected)
            {
                IratMetaDefinicioTreeNode.ChildNodes[i].Select();
            }

        }

        //if (isFelettesChanged)
        //{

        // megjegyezz�k a kiv�lasztottat:
        TreeNode selectedNode = IratMetaDefinicioTerkep1.TreeView.SelectedNode;

        currentNode.Select();
        IratMetaDefinicioTerkep1.DeleteChildrenNodes(currentNode);
        IratMetaDefinicioTerkep_RefreshSelectedNodeChildren(this, new IMDterkep.IMDTerkepEventArgs(Obj_MetaDefinicio_Id, IMDterkep.NodeType.Custom, strMetaDefinicioNodeNev));

        // kiv�laszt�s helyre�ll�t�sa:
        if (selectedNode != null)
        {
            selectedNode.Select();
        }
        //}
    }


    private void UpdateObj_MetaAdataiNode(DataRow dataRow_objMetaAdataiExtended, TreeNode ObjMetaDefinicioTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        String Obj_MetaAdatai_Id = dataRow_objMetaAdataiExtended["Id"].ToString();

        string targySzavak = dataRow_objMetaAdataiExtended["TargySzavak"].ToString();
        string strTargySzavak = "";
        string sorszam = "";
        string opcionalis = "";
        string ismetlodo = "";
        string funkcio = "";
        string SPSSzinkronizalt = "";
        string szint = "";

        if (!String.IsNullOrEmpty(targySzavak))
        {
            strTargySzavak = targySzavak;

            sorszam = dataRow_objMetaAdataiExtended["Sorszam"].ToString();
            szint = dataRow_objMetaAdataiExtended["Szint"].ToString();
            switch (dataRow_objMetaAdataiExtended["Opcionalis"].ToString())
            {
                case "0":
                    opcionalis = valueOpcionalis0;
                    break;
                case "1":
                    opcionalis = valueOpcionalis1;
                    break;
                default:
                    break;
            }

            switch (dataRow_objMetaAdataiExtended["Ismetlodo"].ToString())
            {
                case "0":
                    ismetlodo = valueIsmetlodo0;
                    break;
                case "1":
                    ismetlodo = valueIsmetlodo1;
                    break;
                default:
                    break;
            }
            if (dataRow_objMetaAdataiExtended["SPSSzinkronizalt"].ToString() == "1")
            {
                SPSSzinkronizalt = strSzinkronizalt;
            }

            strTargySzavak += " (" + strSorszam + ": " + sorszam;
            if (!String.IsNullOrEmpty(opcionalis))
            {
                strTargySzavak += ", " + opcionalis;
            }
            if (!String.IsNullOrEmpty(ismetlodo))
            {
                strTargySzavak += ", " + ismetlodo;
            }
            if (!String.IsNullOrEmpty(SPSSzinkronizalt))
            {
                strTargySzavak += ", " + SPSSzinkronizalt;
            }
            strTargySzavak += ")";

            funkcio = dataRow_objMetaAdataiExtended["Funkcio"].ToString();
            if (!String.IsNullOrEmpty(funkcio))
            {
                strTargySzavak += strFunkcio + funkcio;
            }
        }

        int nSorszam;
        Int32.TryParse(sorszam, out nSorszam);

        int nSzint;
        Int32.TryParse(szint, out nSzint);
        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (Obj_MetaAdataiDictionary.ContainsKey(Obj_MetaAdatai_Id) == true)
        {
            Obj_MetaAdataiItem obj_MetaAdataiItem = null;
            Obj_MetaAdataiDictionary.TryGetValue(Obj_MetaAdatai_Id, out obj_MetaAdataiItem);

            OrderChanged = (obj_MetaAdataiItem.Sorszam.ToString() == sorszam ? false : true);

            obj_MetaAdataiItem.Targyszavak_Id = dataRow_objMetaAdataiExtended["Targyszavak_Id"].ToString();    // elvileg nem v�ltozhat
            obj_MetaAdataiItem.TargySzavak = targySzavak;
            obj_MetaAdataiItem.Sorszam = nSorszam;
            obj_MetaAdataiItem.Funkcio = dataRow_objMetaAdataiExtended["Funkcio"].ToString();
            obj_MetaAdataiItem.Ismetlodo = dataRow_objMetaAdataiExtended["Ismetlodo"].ToString();
            obj_MetaAdataiItem.Opcionalis = dataRow_objMetaAdataiExtended["Opcionalis"].ToString();
            obj_MetaAdataiItem.Szint = nSzint;

            Obj_MetaAdataiDictionary.Remove(Obj_MetaAdatai_Id);
            Obj_MetaAdataiDictionary.Add(Obj_MetaAdatai_Id, obj_MetaAdataiItem);
        }
        else
        {
            Obj_MetaAdataiItem Obj_MetaAdataiItem = new Obj_MetaAdataiItem(
                Obj_MetaAdatai_Id,
                null, //erec_Obj_MetaAdatai.IratMetadefinicio_Id,
                dataRow_objMetaAdataiExtended["Targyszavak_Id"].ToString(),
                targySzavak,
                nSorszam,
                dataRow_objMetaAdataiExtended["Funkcio"].ToString(),
                dataRow_objMetaAdataiExtended["Ismetlodo"].ToString(),
                dataRow_objMetaAdataiExtended["Opcionalis"].ToString(),
                dataRow_objMetaAdataiExtended["SPSSzinkronizalt"].ToString(),
                nSzint
                );
            Obj_MetaAdataiDictionary.Add(Obj_MetaAdatai_Id, Obj_MetaAdataiItem);
            OrderChanged = true;

        }

        ViewState["Obj_MetaAdataiDictionary"] = Obj_MetaAdataiDictionary;

        String orokolt = "";
        if (nSzint > 0)
        {
            String Obj_MetaDefinicio_Id_Felettes = dataRow_objMetaAdataiExtended["Obj_MetaDefinicio_Id"].ToString();
            String LinkHrefText = String.Format(IconHrefFormatString, Command, "TabObjMetaDefinicioTerkepPanel", Obj_MetaDefinicio_Id_Felettes);
            String LinkText = String.Format(LinkImageFormatString, LinkHrefText, IconPathLinkImage, IconAltLinkImage);

            orokolt = " <b>(" + strOrokolt + ": " + dataRow_objMetaAdataiExtended["ContentType"].ToString() + LinkText + ")</b>";

            // st�lus �t�ll�t�s
            IratMetaDefinicioTerkep1.SetNodeTypeClass(IMDterkep.NodeType.Custom, strCustomNodeClassInherited);
        }

        String Kod = strMetaAdatNodeNev;
        String Nev = strTargySzavak + orokolt;
        String NodeText = "";

        String NodeValue = IratMetaDefinicioTerkep1.ConcatNodeType(IMDterkep.NodeType.Custom, Obj_MetaAdatai_Id);
        String NodePath = ObjMetaDefinicioTreeNode.ValuePath + IratMetaDefinicioTerkep1.ValuePathSeparator + NodeValue;

        // ez valami�rt nem m�k�dik j�l, ez�rt m�s m�dszerrel ellen�rizz�k
        //TreeNode currentNode = IratMetaDefinicioTerkep1.TreeView.FindNode(NodePath);

        TreeNode currentNode = null;
        foreach (TreeNode node in ObjMetaDefinicioTreeNode.ChildNodes)
        {
            if (NodePath == node.ValuePath)
            {
                currentNode = node;
                break;
            };
        }

        if (currentNode != null)
        {
            NodeText = IratMetaDefinicioTerkep1.GetTreeNodeText(Kod, Nev, IMDterkep.NodeType.Custom, ObjMetaDefinicioTreeNode.ValuePath, strIconPathMetaAdat, strIconAltMetaAdat, Obj_MetaAdatai_Id);

            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                ObjMetaDefinicioTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            // megkeress�k a hely�t
            bool bFound = false;
            int i = -1;

            switch (customNodePosition)
            {
                case IMDterkep.CustomNodePositionType.AfterOthers:
                    // fel�l vannak az irat metadefin�ci� node-ok, alatta a custom node-ok, h�tulr�l keres�nk
                    // vigy�zat: rel�ci�s jel �ll�sa!
                    i = ObjMetaDefinicioTreeNode.ChildNodes.Count;
                    while (i > 0 && !bFound
                        && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
                    {
                        TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
                        string tnObj_MetaAdatai_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

                        Obj_MetaAdataiItem obj_MetaAdataiItem = null;
                        Obj_MetaAdataiDictionary.TryGetValue(tnObj_MetaAdatai_Id, out obj_MetaAdataiItem);
                        if (obj_MetaAdataiItem != null && obj_MetaAdataiItem.Sorszam <= nSorszam)
                        {
                            bFound = true;
                        }
                        else
                        {
                            i--;
                        }
                    }
                    break;
                case IMDterkep.CustomNodePositionType.BeforeOthers:
                    // alul vannak az irat metadefin�ci� node-ok, felette a custom node-ok, el�lr�l keres�nk
                    // vigy�zat: rel�ci�s jel �ll�sa!
                    i = 0;
                    while (i < ObjMetaDefinicioTreeNode.ChildNodes.Count && !bFound
                        && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
                    {
                        TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
                        string tnObj_MetaAdatai_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

                        Obj_MetaAdataiItem obj_MetaAdataiItem = null;
                        Obj_MetaAdataiDictionary.TryGetValue(tnObj_MetaAdatai_Id, out obj_MetaAdataiItem);
                        if (obj_MetaAdataiItem != null && obj_MetaAdataiItem.Sorszam > nSorszam)
                        {
                            bFound = true;
                        }
                        else
                        {
                            i++;
                        }
                    }
                    break;
            }

            IratMetaDefinicioTerkep1.AddCustomNodeAt(i
                                                  , ObjMetaDefinicioTreeNode
                                                  , Kod
                                                  , Nev
                                                  , strMetaAdatNodeNev
                                                  , strIconPathMetaAdat
                                                  , strIconAltMetaAdat
                                                  , Obj_MetaAdatai_Id);

            // st�lus vissza�ll�t�s
            IratMetaDefinicioTerkep1.SetNodeTypeClass(IMDterkep.NodeType.Custom, strCustomNodeClass);

            if (isSelected)
            {
                ObjMetaDefinicioTreeNode.ChildNodes[i].Select();
            }
        }

    }


    #endregion UpdateTreeNodes


    #region Buttons

    private void SetButtonsByAgazatiJelek()
    {
        // -------------- Engedelyek Beallitasa -------------------
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(IratMetaDefinicioTerkep1.SelectedId)) return;

        TreeViewHeader1.ViewEnabled = false;
        TreeViewHeader1.ModifyEnabled = false;
        TreeViewHeader1.InvalidateEnabled = false;
 
        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelNew");

            if (TreeViewHeader1.NewEnabled)
            {
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.New
                  + "&" + QueryStringVars.AgazatiJelId + "=" + IratMetaDefinicioTerkep1.SelectedId
                  + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                  + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                  , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

                //SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IraIrattariTetelek); 
            }

        }
        //cbKivalasztUjElem.Enabled = TreeViewHeader1.NewEnabled;
        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion
    }

    private void SetButtonsByIraIrattariTetelek()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        String AgazatiJel_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIraIrattariTetelek(IratMetaDefinicioTerkep1.SelectedId);
        if (String.IsNullOrEmpty(AgazatiJel_Id)) return;
        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(AgazatiJel_Id)) return;

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelView");

            if (TreeViewHeader1.ViewEnabled)
            {
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                   , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelModify");

            if (TreeViewHeader1.ModifyEnabled)
            {
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate");

            // hozz�rendelt IratMetaDefinicio rekord Id kiolvas�sa
            string IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetIratMetaDefinicioIdFromDictionaryIraIrattariTetelek(IratMetaDefinicioTerkep1.SelectedId);

            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioNew");

            if (TreeViewHeader1.NewEnabled)
            {
                // ha van f�l�rendelt irat metadefin�ci�, akkor azt adjuk �t, egy�bk�nt az iratt�ri t�tel Id-t
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + (!String.IsNullOrEmpty(IratMetaDefinicio_Id)?  "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicio_Id : "&"+ QueryStringVars.IrattariTetelId + "=" + IratMetaDefinicioTerkep1.SelectedId)
                    + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                    + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

                //TreeViewHeader1.NewOnClientClick += RegisterClientScriptSetElementValue(SelectedNodeName_HiddenField.ClientID, IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IratMetaDefinicio));
                ////SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IratMetaDefinicio); 
            }

            TreeViewHeader1.NewCustomNodeEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioNew");

            if (TreeViewHeader1.NewCustomNodeEnabled)
            {
                TreeViewHeader1.NewCustomNodeOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + (!String.IsNullOrEmpty(IratMetaDefinicio_Id) ? "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicio_Id : "&" + QueryStringVars.IrattariTetelId + "=" + IratMetaDefinicioTerkep1.SelectedId)
                    + "&" + QueryStringVars.DefinicioTipus + "=" + KodTarak.OBJMETADEFINICIO_TIPUS.C2 
                    + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                    + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

               //TreeViewHeader1.NewCustomNodeOnClientClick += RegisterClientScriptSetElementValue(SelectedNodeName_HiddenField.ClientID, IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.Custom));

                // TODO: ide mi ker�lj�n?
                //SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.Custom);
                
            }
        }
        //cbKivalasztUjElem.Enabled = TreeViewHeader1.NewEnabled;
        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetButtonsByIratMetaDefinicio()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        String IratMetaDefinicio_Id = "";
        String tipus = String.Empty;// m�dos�t�skor �tadva a formnak szab�lyozhat� a mez�k �rhat�s�ga
        switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode))
        {
            case IMDterkep.NodeType.Irattipus:
                {
                    String EljarasiSzakasz_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIrattipus(IratMetaDefinicioTerkep1.SelectedId);
                    if (String.IsNullOrEmpty(EljarasiSzakasz_Id)) return;
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryEljarasiSzakasz(EljarasiSzakasz_Id);
                    if (String.IsNullOrEmpty(IratMetaDefinicio_Id)) return;
                    tipus = Constants.IratMetaDefinicioTipus.Irattipus;
                }
                break;
            case IMDterkep.NodeType.EljarasiSzakasz:
                {
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryEljarasiSzakasz(IratMetaDefinicioTerkep1.SelectedId);
                    if (String.IsNullOrEmpty(IratMetaDefinicio_Id)) return;
                    tipus = Constants.IratMetaDefinicioTipus.EljarasiSzakasz;
                }
                break;
            default:
                IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.SelectedId;
                tipus = Constants.IratMetaDefinicioTipus.IratMetaDefinicio;
                break;
        }

        String Ugykor_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIratMetaDefinicio(IratMetaDefinicio_Id);
        if (String.IsNullOrEmpty(Ugykor_Id)) return;
        String AgazatiJel_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIraIrattariTetelek(Ugykor_Id);
        if (String.IsNullOrEmpty(AgazatiJel_Id)) return;
        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(AgazatiJel_Id)) return;

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioView");

            if (TreeViewHeader1.ViewEnabled)
            {
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                       + "&" + QueryStringVars.IrattariTetelId + "=" + Ugykor_Id
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioModify");


            if (TreeViewHeader1.ModifyEnabled)
            {
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                    + "&" + QueryStringVars.Tipus + "=" + tipus
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioInvalidate");

            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioNew");

            if (TreeViewHeader1.NewEnabled)
            {
                switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode))
                {
                    case IMDterkep.NodeType.Irattipus:
                        TreeViewHeader1.NewEnabled = false;
                        break;
                    default:
                        TreeViewHeader1.NewEnabled = true;
                        break;
                }
                if (TreeViewHeader1.NewEnabled)
                {
                    TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicioTerkep1.SelectedId
                        + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                        + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
                }
            }

            TreeViewHeader1.NewCustomNodeEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioNew");

            if (TreeViewHeader1.NewCustomNodeEnabled)
            {
                switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode))
                {
                    case IMDterkep.NodeType.IratMetaDefinicio:
                    case IMDterkep.NodeType.Irattipus:
                        TreeViewHeader1.NewCustomNodeEnabled = true;
                        break;
                    default:
                        TreeViewHeader1.NewCustomNodeEnabled = false;
                        break;
                }

                if (TreeViewHeader1.NewCustomNodeEnabled)
                {
                    TreeViewHeader1.NewCustomNodeOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicioTerkep1.SelectedId
                        + "&" + QueryStringVars.DefinicioTipus + "=" + KodTarak.OBJMETADEFINICIO_TIPUS.C2
                        + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                        + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
                }

                // TODO: ide mi ker�lj�n?
                //SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.Custom);
            }

        }

        //cbKivalasztUjElem.Enabled = TreeViewHeader1.NewEnabled || TreeViewHeader1.NewCustomNodeEnabled;
        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled || TreeViewHeader1.NewCustomNodeEnabled; ;
        #endregion

    }

    private void SetButtonsByObj_MetaDefinicio()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
        Obj_MetaDefinicioDictionary.TryGetValue(IratMetaDefinicioTerkep1.SelectedId, out obj_MetaDefinicioItem);
        if (obj_MetaDefinicioItem == null)
            return;


        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioView");

            if (TreeViewHeader1.ViewEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaDefinicio_Id
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + obj_MetaDefinicioItem.Id
                       + (!String.IsNullOrEmpty(obj_MetaDefinicioItem.IratMetadefinicio_Id) ? "&" + QueryStringVars.IratMetadefinicioId + "=" + obj_MetaDefinicioItem.IratMetadefinicio_Id : "")
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioModify");

            if (TreeViewHeader1.ModifyEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaDefinicio_Id
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + obj_MetaDefinicioItem.Id
                    + (!String.IsNullOrEmpty(obj_MetaDefinicioItem.IratMetadefinicio_Id) ? "&" + QueryStringVars.IratMetadefinicioId + "=" + obj_MetaDefinicioItem.IratMetadefinicio_Id : "")
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }


            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiNew");

            if (TreeViewHeader1.NewEnabled)
            {
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + "&" + QueryStringVars.ObjMetaDefinicioId + "=" + IratMetaDefinicioTerkep1.SelectedId
                    + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                    + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate");

            TreeViewHeader1.NewCustomNodeEnabled = false;
        }

        //cbKivalasztUjElem.Enabled = TreeViewHeader1.NewEnabled;
        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetButtonsByObj_MetaAdatai()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        Obj_MetaAdataiItem obj_MetaAdataiItem = null;
        Obj_MetaAdataiDictionary.TryGetValue(IratMetaDefinicioTerkep1.SelectedId, out obj_MetaAdataiItem);
        if (obj_MetaAdataiItem == null)
            return;


        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiView");

            if (TreeViewHeader1.ViewEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaAdatai_Id
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + obj_MetaAdataiItem.Id
                       + "&" + QueryStringVars.TargyszavakId + "=" + obj_MetaAdataiItem.Targyszavak_Id
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiModify");

            TreeViewHeader1.ModifyEnabled = TreeViewHeader1.ModifyEnabled && (obj_MetaAdataiItem.Szint == 0);

            if (TreeViewHeader1.ModifyEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaAdatai_Id
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + obj_MetaAdataiItem.Id
                    + "&" + QueryStringVars.TargyszavakId + "=" + obj_MetaAdataiItem.Targyszavak_Id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefinicioTerkep1.UpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.NewEnabled = false;

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiInvalidate");
            TreeViewHeader1.InvalidateEnabled = TreeViewHeader1.InvalidateEnabled && (obj_MetaAdataiItem.Szint == 0);

            TreeViewHeader1.NewCustomNodeEnabled = false;

        }

        //cbKivalasztUjElem.Enabled = TreeViewHeader1.NewEnabled;
        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetAllButtonsEnabled(Boolean value)
    {
        TreeViewHeader1.ViewEnabled = value;
        TreeViewHeader1.ModifyEnabled = value;
        TreeViewHeader1.NewEnabled = value;
        TreeViewHeader1.InvalidateEnabled = value;
        TreeViewHeader1.NewCustomNodeEnabled = value;

        //cbKivalasztUjElem.Enabled = value;
        TreeViewHeader1.SelectNewChildEnabled = value;
    }

    private void ResetAllButtonsOnClientClick()
    {
        TreeViewHeader1.ViewOnClientClick = "";
        TreeViewHeader1.ModifyOnClientClick = "";
        TreeViewHeader1.NewOnClientClick = "";
        TreeViewHeader1.NewCustomNodeOnClientClick = "";
        //Torles.OnClientClick = "";
    }

    #endregion Buttons

    // ha a keres�s akt�v, true-val t�r vissza, egy�bk�nt false
    private bool SetKeresesFilter()
    {
        bool isSearchActive = false;

        if (Search.IsSearchObjectInSession(Page, typeof(MetaAdatHierarchiaSearch)))
        {
            MetaAdatHierarchiaSearch metaAdatHierarchiaSearch = (MetaAdatHierarchiaSearch)Search.GetSearchObject(Page, new MetaAdatHierarchiaSearch());

            if (metaAdatHierarchiaSearch != null)
            {
                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.GetAllMetaAdatokHierarchiaFilter(execParam, metaAdatHierarchiaSearch, false);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, result);
                }
                else
                {
                    //String IsFilteredWithNoHit = result.Ds.Tables[0].Rows[0]["IsFilteredWithoutHit"].ToString();
                    bool IsFilteredWithNoHit = (bool)result.Record;
                    String AgazatiJelek_Ids = GetIdListFromTable(result.Ds.Tables[1], ",", true);
                    String IraIrattariTetelek_Ids = GetIdListFromTable(result.Ds.Tables[2], ",", true);
                    //String IratMetaDefinicio_Ids = GetIdListFromTable(result.Ds.Tables[3], ",", true);
                    String Ugytipus_Ids = GetIdListFromTable(result.Ds.Tables[4], ",", true);
                    String EljarasiSzakasz_Ids = GetIdListFromTable(result.Ds.Tables[5], ",", true);
                    String Irattipus_Ids = GetIdListFromTable(result.Ds.Tables[6], ",", true);
                    String Obj_MetaDefinicio_Ids = GetIdListFromTable(result.Ds.Tables[7], ",", true);
                    String Obj_MetaAdatai_Ids = GetIdListFromTable(result.Ds.Tables[8], ",", true);
                    String TargySzavak_Ids = GetIdListFromTable(result.Ds.Tables[9], ",", true);

                    IratMetaDefinicioTerkep1.SetNodeFilter(
                        IsFilteredWithNoHit
                        , AgazatiJelek_Ids
                        , IraIrattariTetelek_Ids
                        , Ugytipus_Ids
                        , EljarasiSzakasz_Ids
                        , Irattipus_Ids
                        );

                    this.SetNodeFilter(Obj_MetaDefinicio_Ids, Obj_MetaAdatai_Ids);

                    isSearchActive = true;
                }

            }
            else
            {
                IratMetaDefinicioTerkep1.ClearNodeFilter();
                this.ClearNodeFilter();
            }

        }
        else
        {
            IratMetaDefinicioTerkep1.ClearNodeFilter();
            this.ClearNodeFilter();
        }

        return isSearchActive;
    }

    /// <summary>
    /// T�rli a kijel�lt elemet
    /// </summary>
    private void InvalidateSelectedNode()
    {
        if (String.IsNullOrEmpty(IratMetaDefinicioTerkep1.SelectedId)) return;

        String nodeId = IratMetaDefinicioTerkep1.SelectedId;

        IMDterkep.NodeType nodeType = IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode);
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = nodeId;
        Result result = null;

        switch (nodeType)
        {
            case IMDterkep.NodeType.AgazatiJelek:
                if (FunctionRights.GetFunkcioJog(Page, "AgazatiJelekInvalidate"))
                {
                    EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                    result = service.Invalidate(execParam);
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            case IMDterkep.NodeType.IraIrattariTetelek:
                if (FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate"))
                {
                    EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    result = service.Invalidate(execParam); // hozz� kapcsol�d� irat metadefin�ci�k �rv�nytelen�t�se is
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
            case IMDterkep.NodeType.EljarasiSzakasz:
            case IMDterkep.NodeType.Irattipus:
                if (FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioInvalidate"))
                {
                    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    result = service.Invalidate(execParam);   // al�rendelt irat metadefin�ci�k �rv�nytelen�t�se is
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            case IMDterkep.NodeType.Custom:
                {
                    String NodeTypeName = IratMetaDefinicioTerkep1.GetNodeTypeNameFromDictionaryCustom(nodeId);
                    if (NodeTypeName == strMetaDefinicioNodeNev)
                    {
                        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate"))
                        {
                            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                            result = service.Invalidate(execParam);
                            // t�rl�s a sz�t�rb�l
                            if (Obj_MetaDefinicioDictionary.ContainsKey(nodeId))
                                Obj_MetaDefinicioDictionary.Remove(nodeId);
                            // lesz�rmazott metaadat hozz�rendel�sek t�rl�se a lok�lis sz�t�rb�l
                            TreeNode SelectedNode = IratMetaDefinicioTerkep1.TreeView.SelectedNode;
                            if (SelectedNode != null)
                            {
                                foreach (TreeNode tn in SelectedNode.ChildNodes)
                                {
                                    String Id_Child = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);
                                    String NodeTypeName_Child = IratMetaDefinicioTerkep1.GetNodeTypeNameFromDictionaryCustom(Id_Child);

                                    if (NodeTypeName_Child == strMetaAdatNodeNev && Obj_MetaAdataiDictionary.ContainsKey(Id_Child))
                                    {
                                        Obj_MetaAdataiDictionary.Remove(Id_Child);
                                    }
                                }
                            }
                        }
                        else
                        {
                            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                        }
                    }
                    else if (NodeTypeName == strMetaAdatNodeNev)
                    {
                        if (FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiInvalidate"))
                        {
                            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                            result = service.Invalidate(execParam);
                            // t�rl�s a sz�t�rb�l
                            if (Obj_MetaAdataiDictionary.ContainsKey(nodeId))
                                Obj_MetaAdataiDictionary.Remove(nodeId);
                        }
                        else
                        {
                            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                        }
                    }
                }
                break;
            default:
                break;
        }


        if (result != null)
        {
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else
            {
                IratMetaDefinicioTerkep1.DeleteSelectedNode();  // t�rli a kijel�lt elemet, �s gyermekeit a f�b�l
            }
        }
    }

   

}
