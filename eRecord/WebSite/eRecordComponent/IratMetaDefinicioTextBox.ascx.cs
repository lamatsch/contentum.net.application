using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class eRecordComponent_IratMetaDefinicioTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    #region public properties
    
    private string _Filter;    
    
    public string Filter
    {
        get { return _Filter; }
        set { _Filter = value; }
    }    

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            IratMetaDefinicioMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return IratMetaDefinicioMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            IratMetaDefinicioMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return IratMetaDefinicioMegnevezes.ReadOnly; }
    }

    public String CssClass
    {
        set { IratMetaDefinicioMegnevezes.CssClass = value; }
        get { return IratMetaDefinicioMegnevezes.CssClass; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { IratMetaDefinicioMegnevezes.Text = value; }
        get { return IratMetaDefinicioMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return IratMetaDefinicioMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IratMetaDefinicioMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IratMetaDefinicioMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_Reset
    {
        get { return ResetImageButton; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public EREC_IratMetaDefinicio SetIratMetaDefinicioTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        EREC_IratMetaDefinicio EREC_IratMetaDefinicio = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_IratMetaDefinicioService service =  eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;

                Text = EREC_IratMetaDefinicio.UgykorKod;
                if (!String.IsNullOrEmpty(EREC_IratMetaDefinicio.Ugytipus))
                {
                    Text += "/" + EREC_IratMetaDefinicio.UgytipusNev;

                    if (!String.IsNullOrEmpty(EREC_IratMetaDefinicio.EljarasiSzakasz))
                    {
                        string eljarasiSzakaszNev;
                        if (Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("ELJARASI_SZAKASZ", Page).TryGetValue(EREC_IratMetaDefinicio.EljarasiSzakasz, out eljarasiSzakaszNev))
                        {
                            Text += "/" + eljarasiSzakaszNev;
                        }
                        else
                        {
                            Text += "/" + EREC_IratMetaDefinicio.EljarasiSzakasz;
                        }

                        if (!String.IsNullOrEmpty(EREC_IratMetaDefinicio.Irattipus))
                        {
                            string irattipusNev;
                            if (Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("IRATTIPUS", Page).TryGetValue(EREC_IratMetaDefinicio.Irattipus, out irattipusNev))
                            {
                                Text += "/" + irattipusNev;
                            }
                            else
                            {
                                Text += "/" + EREC_IratMetaDefinicio.Irattipus;
                            }
                        }
                    }
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }

        return EREC_IratMetaDefinicio; // hiba eset�n �rt�ke null
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {        

        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //String asdf = IratMetaDefinicioMegnevezes.Text;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string query = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + 
            "&" + QueryStringVars.TextBoxId + "=" + IratMetaDefinicioMegnevezes.ClientID;
            
        if (!String.IsNullOrEmpty(Filter))
        {
            query += "&" + QueryStringVars.Filter + "=" + Filter;
            NewImageButton.Visible = false;
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("IratMetaDefinicioLovList.aspx",query, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + IratMetaDefinicioMegnevezes.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "IratMetaDefinicioForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IratMetaDefinicioMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
