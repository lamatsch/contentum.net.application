﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratPeldanyMasterAdatok.ascx.cs" Inherits="eRecordComponent_IratPeldanyMasterAdatok" %>

<%@ Register Src="PldIratPeldanyokTextBox.ascx" TagName="IratPeldanyTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc2" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc4" %>

<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>   
   <eUI:eFormPanel ID="IratPeldanyForm" runat="server">
       <table cellpadding="0" cellspacing="0" runat="server" ID="MainTable" width="100%">
           <tr class="urlapSor" id="trIratpeldany" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label2" runat="server" Text="Iratpéldány:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc1:IratPeldanyTextBox ID="IratPeldanyTextBox1" runat="server" ViewMode="true" ReadOnly="true" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Allapot_felirat" runat="server" Text="Állapot:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" />
               </td>
           </tr>
           <tr class="urlapSor" id="trCimzett" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label17" runat="server" Text="Iratpéldány címzettjének neve:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc3:PartnerTextBox ID="Cimzett_PartnerTextBox" runat="server" ViewMode="true" ReadOnly="true"
                       Validate="false" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label20" runat="server" Text="Iratpéldány címzettjének címe:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc4:CimekTextBox ID="Cimzett_CimekTextBox" runat="server" ViewMode="true" ReadOnly="true"
                       Validate="false" />
               </td>
           </tr>
           <tr class="urlapSor" id="trKezelo" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label8" runat="server" Text="Példány kezelője:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc5:CsoportTextBox ID="Felelos_CsoportTextBox" runat="server" ViewMode="true" ReadOnly="true"
                       Validate="false" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label9" runat="server" Text="Példány helye:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc6:FelhasznaloCsoportTextBox ID="Orzo_FelhasznaloCsoportTextBox" runat="server"
                       ViewMode="true" ReadOnly="true" Validate="false" />
               </td>
           </tr>
       </table>
   </eUI:eFormPanel>
  </td>    
</tr>
</table>
</asp:Panel>