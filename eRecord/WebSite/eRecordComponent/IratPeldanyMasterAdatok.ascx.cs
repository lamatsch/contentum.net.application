﻿using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_IratPeldanyMasterAdatok : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    #region public properties

    private const string kcs_IRATPELDANY_ALLAPOT = "IRATPELDANY_ALLAPOT";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";

    public String IratPeldanyId
    {
        get { return IratPeldanyTextBox1.Id_HiddenField; }
        set { IratPeldanyTextBox1.Id_HiddenField = value; }
    }



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        SetComponentsVisibility();
    }

    private void SetComponentsVisibility()
    {
        Allapot_KodtarakDropDownList.ReadOnly = true;
    }

    public EREC_PldIratPeldanyok SetIratPeldanyMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        return SetIratPeldanyMasterAdatokById(errorPanel, null);
    }

    public EREC_PldIratPeldanyok SetIratPeldanyMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(IratPeldanyId))
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = IratPeldanyId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result.Record;

                    IratPeldanyTextBox1.Id_HiddenField = erec_PldIratPeldanyok.Id;
                    IratPeldanyTextBox1.SetIratPeldanyTextBoxById(errorPanel, errorUpdatePanel);

                    Cimzett_PartnerTextBox.Id_HiddenField = erec_PldIratPeldanyok.Partner_Id_Cimzett;
                    Cimzett_PartnerTextBox.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;

                    Cimzett_CimekTextBox.Id_HiddenField = erec_PldIratPeldanyok.Cim_id_Cimzett;
                    Cimzett_CimekTextBox.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;

                    Allapot_KodtarakDropDownList.FillWithOneValue(kcs_IRATPELDANY_ALLAPOT,
                        erec_PldIratPeldanyok.Allapot, errorPanel);

                    Felelos_CsoportTextBox.Id_HiddenField = erec_PldIratPeldanyok.Csoport_Id_Felelos;
                    Felelos_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

                    Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo;
                    Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);


                    return erec_PldIratPeldanyok;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }

    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(IratPeldanyForm);
        componentList.AddRange(Contentum.eUtility.PageView.GetSelectableChildComponents(IratPeldanyForm.Controls));
        return componentList;
    }

    #endregion
}
