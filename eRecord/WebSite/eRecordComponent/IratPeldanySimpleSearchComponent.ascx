<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratPeldanySimpleSearchComponent.ascx.cs" Inherits="eRecordComponent_IratPeldanySimpleSearchComponent" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<div class="DisableWrap" id="ContainerDiv" runat="server">
	<uc7:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList" Mode="Iktatokonyvek" EvTextBoxId="TextBoxEv"
	    runat="server" ToolTip="Iktat�k�nyv" />
	/
	<asp:TextBox ID="TextBoxFoszam" CssClass="mrUrlapInputNumber" runat="server" ToolTip="F�sz�m" />
	-
	<asp:TextBox ID="TextBoxAlszam" CssClass="mrUrlapInputNumber" runat="server" ToolTip="Alsz�m" />
	/
	<asp:TextBox ID="TextBoxEv" CssClass="mrUrlapInputNumber" runat="server" ToolTip="�v" MaxLength="4" />
	(
	<asp:TextBox ID="TextBoxSorszam" CssClass="mrUrlapInputNumber" runat="server" ToolTip="Sorsz�m" />
	)
</div>
<ajaxToolkit:FilteredTextBoxExtender ID="ftbFoszam" FilterType="Numbers" TargetControlID="TextBoxFoszam" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="ftbAlszam" FilterType="Numbers" TargetControlID="TextBoxAlszam" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="ftbSorszam" FilterType="Numbers" TargetControlID="TextBoxSorszam" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="ftbEv" FilterType="Numbers" TargetControlID="TextBoxEv" runat="server" />