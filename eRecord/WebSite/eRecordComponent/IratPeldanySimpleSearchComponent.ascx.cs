using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_IratPeldanySimpleSearchComponent : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties


    public bool Enabled
    {
        set
        {
            IktatoKonyvekDropDownList.Enabled = value;
            TextBoxEv.Enabled = value;
            TextBoxFoszam.Enabled = value;
            TextBoxAlszam.Enabled = value;
            TextBoxSorszam.Enabled = value; 
        }
        get
        {
            return IktatoKonyvekDropDownList.Enabled;
        }
    }

    public bool ReadOnly
    {
        set {
            IktatoKonyvekDropDownList.ReadOnly = value;
            TextBoxEv.ReadOnly = value;
            TextBoxFoszam.ReadOnly = value;
            TextBoxAlszam.ReadOnly = value;
            TextBoxSorszam.ReadOnly = value;
        }
        get { return IktatoKonyvekDropDownList.ReadOnly; }
    }

    public string Text
    {
        get {
            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue)
                || !String.IsNullOrEmpty(this.Foszam)
                || !String.IsNullOrEmpty(this.Alszam)
                || !String.IsNullOrEmpty(this.Ev)
                || !String.IsNullOrEmpty(this.Sorszam))
            {
                string[] IktatokonyvJelzes = IktatoKonyvekDropDownList.SelectedValue.Split(new string[] { IktatoKonyvek.valueDelimeter }, StringSplitOptions.None);
                string txtIktatohely = (IktatokonyvJelzes.Length > 0) ? IktatokonyvJelzes[0] : String.Empty;
                if (String.IsNullOrEmpty(txtIktatohely))
                {
                    txtIktatohely = "?";
                }
                string txtAzonosito = (IktatokonyvJelzes.Length > 1) ? IktatokonyvJelzes[1] : String.Empty;
                string txtFoszam = (String.IsNullOrEmpty(this.Foszam) ? "?" : this.Foszam);
                string txtAlszam = (String.IsNullOrEmpty(this.Alszam) ? "?" : this.Alszam);
                string txtSorszam = (String.IsNullOrEmpty(this.Sorszam) ? "?" : this.Sorszam);
                string txtEv = (String.IsNullOrEmpty(this.Ev) ? "?" : this.Ev);
                return txtIktatohely + " /" + txtFoszam + " - " + txtAlszam + " /" + txtEv + (!String.IsNullOrEmpty(txtAzonosito) ? " /" + txtAzonosito : "") + " (" + txtSorszam + ")";
            }
            else
            {
                return "";
            }
        }
    
    }

    /// <summary>
    /// Ha a keres�si objektumban nincs megadva iktat�k�nyv
    /// �s �v sem, akkor ez lesz be�ll�tva az �v mez�ben,
    /// amennyiben meg van adva (> 0)
    /// </summary>
    private int _AlapertelmezettEv = 0;
    public int AlapertelmezettEv
    {
        get { return _AlapertelmezettEv; }
        set { _AlapertelmezettEv = value; }
    }
    
    public string IktatoKonyv
    {
        set { IktatoKonyvekDropDownList.SetSelectedValue(value); }
        get { return IktatoKonyvekDropDownList.SelectedValue; }
    }

    public string Foszam
    {
        set { TextBoxFoszam.Text = value; }
        get { return TextBoxFoszam.Text; }
    }

    public string Alszam
    {
        set { TextBoxAlszam.Text = value; }
        get { return TextBoxAlszam.Text; }
    }

    public string Sorszam
    {
        set { TextBoxSorszam.Text = value; }
        get { return TextBoxSorszam.Text; }
    }

    public string Ev
    {
        set { TextBoxEv.Text = value; }
        get { return TextBoxEv.Text; }
    }

    public DropDownList IktatoKonyvDropDownList
    {
        get { return IktatoKonyvekDropDownList.DropDownList; }
    }

    public TextBox FoszamTextBox
    {
        get { return TextBoxFoszam; }
    }

    public TextBox AlszamTextBox
    {
        get { return TextBoxAlszam; }
    }

    public TextBox SorszamTextBox
    {
        get { return TextBoxSorszam; }
    }

    public TextBox EvTextBox
    {
        get { return TextBoxEv; }
    }

    public HtmlControl Container
    {
        get { return this.ContainerDiv; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //JavaScripts.RegisterPopupWindowClientScript(Page);

        //ASP.NET 2.0 bug work around
        //TextBoxFoszam.Attributes.Add("readonly", "readonly");
        //TextBoxAlszam.Attributes.Add("readonly", "readonly");
        //TextBoxSorszam.Attributes.Add("readonly", "readonly");
        //TextBoxEv.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    // ha a kit�ltend� mez�knek megfelel� �rt�kek k�z�l mindegyik �res, akkor defaultnak tekintj�k
    protected bool IsDefaultSearchObject(EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch)
    {
        bool isDefaultSearchObject = true;
        if (erec_PldIratPeldanyokSearch != null)
        {
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Sorszam.Value);
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Sorszam.ValueTo);
            if (erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                isDefaultSearchObject &= String.IsNullOrEmpty(IktatoKonyvek.GetIktatokonyvValue(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch));
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value);
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo);
            }
            if (erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
            {
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value);
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.ValueTo);
            }
            if (erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
            {
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Value);
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.ValueTo);
            }

        }
        return isDefaultSearchObject;
    }

    public void FillIktatoKonyvek(string MegkulJelzes, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        IktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                       , this.Ev, this.Ev
                       , true, false, MegkulJelzes, ErrorPanel);
    }

    public void FillSearchObjectFromComponents(ref EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch)
    {
        if (erec_PldIratPeldanyokSearch != null)
        {
            if (erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }

            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue))
            {
                IktatoKonyvekDropDownList.SetSearchObject(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch);
            }

            if (!String.IsNullOrEmpty(this.Foszam))
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value = this.Foszam;
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.ValueTo = this.Foszam;
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator = Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(this.Ev))
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = this.Ev;
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo = this.Ev;
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(this.Alszam))
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Value = this.Alszam;
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.ValueTo = this.Alszam;
                erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Operator = Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(this.Sorszam))
            {
                erec_PldIratPeldanyokSearch.Sorszam.Value = this.Sorszam;
                erec_PldIratPeldanyokSearch.Sorszam.ValueTo = this.Sorszam;
                erec_PldIratPeldanyokSearch.Sorszam.Operator = Query.Operators.equals;
            }
        }
    }

    /// <summary>
    /// A keres�si objektum alapj�n kit�lti a beviteli mez�ket.
    /// </summary>
    /// <param name="erec_PldIratPeldanyokSearch">A kit�lt�s alapj�ul szolg�l� keres�si objektum.</param>
    /// <returns>True, ha a keres�si objektum form�tuma egyszer� (azaz intervallumok n�lk�li), false, ha intervallum t�pus� megad�sok is voltak. </returns>
    public bool FillComponentsFromSearchObject(EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        bool isSimpleSearch = true;
        this.ClearInputFields();
        bool isDefaultSearchObject = IsDefaultSearchObject(erec_PldIratPeldanyokSearch);

        if (erec_PldIratPeldanyokSearch != null)
        {
            if (erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                this.Ev = erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value;
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek(IktatoKonyvek.GetIktatokonyvValue(erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch), ErrorPanel);

                isSimpleSearch &= (
                    (erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == Query.Operators.equals)
                    ||
                    (erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == "")
                        );
            }
            else
            {
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek("", ErrorPanel);
            }

            if (erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
            {
                this.Foszam = erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value;

                isSimpleSearch &= (
                    (erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value == erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.ValueTo
                        && erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator == Query.Operators.equals)
                    ||
                    (erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.ValueTo == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator == "")
                        );
            }

            if (erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
            {
                this.Alszam = erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Value;

                isSimpleSearch &= (
                    (erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Value == erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.ValueTo
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Operator == Query.Operators.equals)
                    ||
                    (erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Value == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.ValueTo == ""
                        && erec_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam.Operator == "")
                        );
            }

            this.Sorszam = erec_PldIratPeldanyokSearch.Sorszam.Value;

            isSimpleSearch &= (
                (erec_PldIratPeldanyokSearch.Sorszam.Value == erec_PldIratPeldanyokSearch.Sorszam.ValueTo
                    && erec_PldIratPeldanyokSearch.Sorszam.Operator == Query.Operators.equals)
                ||
                (erec_PldIratPeldanyokSearch.Sorszam.Value == ""
                    && erec_PldIratPeldanyokSearch.Sorszam.ValueTo == ""
                    && erec_PldIratPeldanyokSearch.Sorszam.Operator == "")
                    );

        }
        return isSimpleSearch;
    }


    public void ClearInputFields()
    {
        IktatoKonyvekDropDownList.SetSelectedValue("");
        this.Foszam = "";
        this.Alszam = "";
        this.Sorszam = "";
        this.Ev = "";
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IktatoKonyvekDropDownList.DropDownList);
        componentList.Add(TextBoxFoszam);
        componentList.Add(TextBoxAlszam);
        componentList.Add(TextBoxEv);
        componentList.Add(TextBoxSorszam);

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
