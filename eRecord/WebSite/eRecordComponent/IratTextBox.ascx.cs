using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_IratTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, UI.ILovListTextBox
{
    #region public properties

    private string filter;

    public string Filter
    {
        get { return filter; }
        set { filter = value; }
    }

    private string filterByObjektumKapcsolat = String.Empty;

    public string FilterByObjektumKapcsolat
    {
        get { return filterByObjektumKapcsolat; }
        set { filterByObjektumKapcsolat = value; }
    }

    private string objektumId = String.Empty;

    public string ObjektumId
    {
        get { return objektumId; }
        set { objektumId = value; }
    }


    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            IratTargy.Enabled = value;
            LovImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
            }
        }
        get { return IratTargy.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            IratTargy.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return IratTargy.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { IratTargy.Text = value; }
        get { return IratTargy.Text; }
    }

    public TextBox TextBox
    {
        get { return IratTargy; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IratTargy.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IratTargy.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }


    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetIratTextBoxById(errorPanel, errorUpdatePanel);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetIratTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            search.Id.Value = Id_HiddenField;
            search.Id.Operator = Query.Operators.equals;

            Result result = service.GetAllWithExtension(execParam,search);

            ResultError.CreateNoRowResultError(result);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                DataRow iratRow = result.Ds.Tables[0].Rows[0];

                SetIratTextBoxByDataRow(iratRow, errorPanel, errorUpdatePanel);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        else
        {
            Text = "";
        }
    }

    private void SetIratTextBoxByDataRow(DataRow iratRow, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Id_HiddenField = iratRow["Id"].ToString();

        Text = iratRow["IktatoSzam_Merge"].ToString();
    }

    /// <summary>
    /// A megadott küldeményhez beiktatott irat betöltése a komponensbe
    /// </summary>    
    public void SetIratTextBoxByKuldemenyId(string kuldemenyId, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(kuldemenyId))
        {
            EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            // Keressük azt az iratot, ahol a küldeményId a megadott, és nem felszabadított az irat
            EREC_IraIratokSearch search_irat = new EREC_IraIratokSearch();

            search_irat.KuldKuldemenyek_Id.Value = kuldemenyId;
            search_irat.KuldKuldemenyek_Id.Operator = Query.Operators.equals;

            // Nem felszabadított az állapota:
            search_irat.Allapot.Value = KodTarak.IRAT_ALLAPOT.Felszabaditva;
            search_irat.Allapot.Operator = Query.Operators.notequals;

            Result result = service_iratok.GetAll(UI.SetExecParamDefault(Page), search_irat);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }

                Id_HiddenField = String.Empty;
                Text = String.Empty;
            }
            else if (result.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result.Ds.Tables[0].Rows[0];

                Id_HiddenField = row["Id"].ToString();
                Text = row["Azonosito"].ToString();
            }
            else
            {
                Id_HiddenField = String.Empty;
                Text = String.Empty;
            }
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string filterQuery = String.Empty;
        if (!String.IsNullOrEmpty(filter))
        {
            filterQuery = "&" + QueryStringVars.Filter + "=" + filter;
        }

        if (!String.IsNullOrEmpty(FilterByObjektumKapcsolat) && !String.IsNullOrEmpty(objektumId))
        {
            filterQuery += "&" + QueryStringVars.ObjektumKapcsolatTipus + "=" + filterByObjektumKapcsolat;
            filterQuery += "&" + QueryStringVars.ObjektumId + "=" + ObjektumId;
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("IraIratokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + IratTargy.ClientID + filterQuery
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "IraIratokForm.aspx", "", HiddenField1.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IratTargy);
        componentList.Add(LovImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion



    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
