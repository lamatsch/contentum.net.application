﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IratpeldanyokList.ascx.cs"
    Inherits="eRecordComponent_IratpeldanyokList" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="DosszieTreeView.ascx" TagName="DosszieTreeView" TagPrefix="dtv" %>
<%@ Register Src="~/eRecordComponent/SztornoPopup.ascx" TagName="SztornoPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent"
    TagPrefix="uc" %>
<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <%--Hiba megjelenites--%>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="MessageHiddenField" runat="server" />
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td rowspan="2" valign="top" align="left" id="PanelDossziekTD" runat="server">
            <div id="PanelDossziekDiv" style="position: relative;">
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="PanelDossziekTVPanel" runat="server">
                                <dtv:DosszieTreeView ID="DosszieTreeView" runat="server" />
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="DossziekCE" runat="server" TargetControlID="PanelDossziekTVPanel"
                                CollapsedSize="0" Collapsed="true" AutoCollapse="false" AutoExpand="false" ExpandDirection="Horizontal"
                                CollapsedImage="../images/hu/egyeb/dosszie_panel_ki.gif" ExpandedImage="../images/hu/egyeb/dosszie_panel_be.gif"
                                ImageControlID="DossziekCEButton" ScrollContents="false">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                        <td valign="middle" style="background-image: url(images/hu/egyeb/dosszie_panel_hatter.jpg); background-repeat: repeat-y;"
                            onclick="$find('<%=DossziekCE.ClientID %>').togglePanel();"
                            style="cursor: hand;">
                            <asp:ImageButton runat="server" ID="DossziekCEButton" OnClientClick="return false;"
                                ImageUrl="../images/hu/egyeb/dosszie_panel_ki.gif" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton runat="server" ID="PldIratPeldanyokCPEButton" ImageUrl="../images/hu/Grid/minus.gif"
                OnClientClick="return false;" />
        </td>
        <td style="text-align: left; vertical-align: top; width: 100%;">
            <asp:UpdatePanel ID="PldIratPeldanyokUpdatePanel" runat="server" OnLoad="PldIratPeldanyokUpdatePanel_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="PldIratPeldanyokCPE" runat="server" TargetControlID="Panel1"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="PldIratPeldanyokCPEButton"
                        CollapseControlID="PldIratPeldanyokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                        AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif" ExpandedImage="../images/hu/Grid/minus.gif"
                        ImageControlID="PldIratPeldanyokCPEButton" ExpandedSize="0" ExpandedText="Iratpéldányok listája"
                        CollapsedText="Iratpéldányok listája">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="Panel1" runat="server">
                        <table style="width: 98%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <uc:SztornoPopup runat="server" ID="SztornoPopup" />
                                    <uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
                                    <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                    <asp:GridView ID="PldIratPeldanyokGridView" runat="server" OnRowCommand="PldIratPeldanyokGridView_RowCommand"
                                        CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                        PagerSettings-Visible="false" AllowSorting="True" OnPreRender="PldIratPeldanyokGridView_PreRender"
                                        AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="PldIratPeldanyokGridView_Sorting"
                                        OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                    &nbsp;&nbsp;
                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                <HeaderStyle Width="25px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <%--
                                            TODO: megtudni ez a két flag honnan jön
                                            
                                    <asp:BoundField DataField="EREC_IraIratok_Szerelt" HeaderText="Sz" SortExpression="EREC_IraIratok_Szerelt">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREC_IraIratok_Csatolt" HeaderText="Cs" SortExpression="EREC_IraIratok_Csatolt">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                    </asp:BoundField>
                                            --%>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" AccessibleHeaderText="Csny" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Csny.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" AccessibleHeaderText="F" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="F.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="FeladatImage" AlternateText="Kezelési feljegyzés" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" AccessibleHeaderText="Tv" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Tv.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="TertivevenyImage" AlternateText="Tértivevény" runat="server" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="~/images/hu/ikon/tertiveveny.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IktatoHely" AccessibleHeaderText="IktatoHely" HeaderText="Iktatókönyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely"
                                                Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Foszam" AccessibleHeaderText="Foszam" HeaderText="Főszám" SortExpression="EREC_UgyUgyiratok.Foszam"
                                                Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Alszam" AccessibleHeaderText="Alszam" HeaderText="Alszám" SortExpression="EREC_IraIratok.Alszam"
                                                Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Ev" AccessibleHeaderText="Ev" HeaderText="Év" SortExpression="EREC_IraIktatokonyvek.Ev"
                                                Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Sorszam" AccessibleHeaderText="Sorszam" HeaderText="Sorszám" SortExpression="EREC_PldIratpeldanyok.Sorszam"
                                                Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--BLG_619--%>
                                            <asp:BoundField DataField="IratFelelos_SzervezetKod" AccessibleHeaderText="IratFelelos_SzervezetKod" HeaderText="<%$Forditas:BoundFieldIratFelelos_SzervezetKod|Szk%>" SortExpression="Csoportok_IratFelelosNev.Kod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="20px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MinositesNev" AccessibleHeaderText="MinositesNev" HeaderText="Minősítés" SortExpression="EREC_IraIratok.Minosites" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IktatoSzam_Merge" AccessibleHeaderText="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatoKonyvek.Iktatohely ASC, EREC_UgyUgyiratok.Foszam {0}, EREC_IraIratok.Alszam {0}, EREC_IraIratok.Sorszam {0}, EREC_PldIratPeldanyok.Sorszam">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BarCode" AccessibleHeaderText="BarCode" HeaderText="Vonalkód" SortExpression="EREC_PldIratPeldanyok.BarCode">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EREC_IraIratok_Targy" AccessibleHeaderText="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok.Targy">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="VisszaerkezesiHatarido" AccessibleHeaderText="VisszaerkezesiHatarido" HeaderText="Határidő" SortExpression="EREC_PldIratPeldanyok.VisszaerkezesiHatarido">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Allapot_Nev" AccessibleHeaderText="Allapot_Nev" HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NevSTR_Cimzett" AccessibleHeaderText="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="EREC_PldIratPeldanyok.NevSTR_Cimzett">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CimSTR_Cimzett" AccessibleHeaderText="CimSTR_Cimzett" HeaderText="Címzett&nbsp;címe" Visible="false"
                                                SortExpression="EREC_PldIratPeldanyok.CimSTR_Cimzett">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KuldesMod_Nev" AccessibleHeaderText="KuldesMod_Nev" HeaderText="Küldés módja" SortExpression="KuldesModKodTarak.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" AccessibleHeaderText="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Példány helye"
                                                SortExpression="Csoportok_OrzoNev.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Csoport_Id_Felelos_Nev" AccessibleHeaderText="Csoport_Id_Felelos_Nev" HeaderText="Példány kezelője"
                                                Visible="false" SortExpression="Csoportok_FelelosNev.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LetrehozasIdo_Rovid" AccessibleHeaderText="LetrehozasIdo_Rovid" HeaderText="Létrehozás dátuma" Visible="false"
                                                SortExpression="EREC_PldIratPeldanyok.LetrehozasIdo">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Letrehozo_Nev" AccessibleHeaderText="Letrehozo_Nev" HeaderText="Létrehozó" Visible="false"
                                                SortExpression="Csoportok_Letrehozo.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="PostazasDatuma" AccessibleHeaderText="PostazasDatuma" HeaderText="Postára adás dátuma" Visible="false"
                                                SortExpression="EREC_PldIratPeldanyok.PostazasDatuma">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <%--BUG 2110 - használjuk a küldemény ragszámát--%>
                                            <%--BLG_237--%>
                                            <asp:TemplateField HeaderText="Postai azonosító" AccessibleHeaderText="Ragszam" SortExpression="EREC_KuldKuldemenyek_Kimeno.RagSzam" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <%-- iktatott állapot esetén link az iratra --%>
                                                    <asp:Label ID="labelRagszam" runat="server" Text='<%#Eval("Ragszam").ToString().Length>20?String.Concat(Eval("Ragszam").ToString().Substring(0,20),"..."):Eval("Ragszam").ToString() %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField DataField="Ragszam" HeaderText="Postai azonosító" Visible="false"
                                                SortExpression="EREC_KuldKuldemenyek.Ragszam">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>--%>
                                             <asp:BoundField DataField="TertiKezbesitesEredmenye" HeaderText="Kézb. eredménye" Visible="false"
                                                     SortExpression="EREC_KuldTertivevenyek.TertivisszaKod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAtvevoSzemely" HeaderText="Átvevő személy" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.AtvevoSzemely">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField> 
                                                <asp:BoundField DataField="TertiAtvetelIdopontja" HeaderText="Átvétel időpontja" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.AtvetelDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAtvetelJogcime" HeaderText="Átvétel jogcíme" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.Note">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiAllapot" HeaderText="Tertiv. állapot" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.Allapot">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertiVisszaerkezesIdeje" HeaderText="Visszaérk. ideje" Visible="false"
                                                      SortExpression="EREC_KuldTertivevenyek.TertiVisszaDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TertivevenyVonalkod" HeaderText="Tértivevény vonalkód" Visible="false" SortExpression="EREC_KuldTertivevenyek.BarCode">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>                                            
                                            <asp:BoundField DataField="Eredet_Nev" AccessibleHeaderText="Eredet_Nev" HeaderText="Iratpld. jellege" Visible="false"
                                                SortExpression="Irat_FajtaKodTarak.nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KezelesTipusNev" AccessibleHeaderText="KezelesTipusNev" HeaderText="Kezelési útasítás">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--  <asp:BoundField DataField="Id" HtmlEncode="False" HeaderText="Boríték ⎙"
                                                Visible='<%# Eval("Allapot").ToString() ==  Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Expedialt.ToString() %>'
                                                DataFormatString="<a target='_blank' href='PldIratPeldanyokAutoPrint.aspx?docId=6&ids={0}'>Boríték ⎙</a>">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Id" HtmlEncode="False" HeaderText="Feladóvevény ⎙"
                                                Visible='<%# Eval("Allapot").ToString() ==  Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Expedialt.ToString() %>'
                                                DataFormatString="<a target='_blank' href='PldIratPeldanyokAutoPrint.aspx?docId=3&ids={0}'>Feladóvevény ⎙</a>">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>

                                            <%-- Visible='<%# Eval("Allapot").ToString() ==  Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Expedialt.ToString() %>'--%>

                                            <%--CR3343--%>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="150px" HeaderText="Nyomtatás ⎙">
                                                <ItemTemplate>
                                                    <asp:Panel runat="server"
                                                        ToolTip="Expediált iratpéldány nyomtatása"
                                                        Visible='<%# Eval("Allapot").ToString() ==  Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Expedialt.ToString() %>'>
                                                        <asp:LinkButton ID="LinkButton2" Text="Nyomtat" runat="server"
                                                            OnClientClick='<%# String.Format("return popupAxelPrintSelector(\"{0}\")", Eval("Id")) %>' />
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" AccessibleHeaderText="Zarolas" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<asp:HiddenField ID="clickOrderInGrid" runat="server" />

<script type="text/javascript">
    function popupAxelPrintSelector(id) {
        newname = "WindowPldIratPeldanyokAutoPrintSelector";
        settings = "scrollbars=yes,resizable=yes, width=480, height=400,toolbar=no, menubar=no";
        window.open("PldIratPeldanyokAutoPrintSelector.aspx?ids=" + id, newname, settings);
    }

    //4788-Regisztrálni a grid elemek kijelölésének sorrendjét
    var elemsClick = [];

    function getSelectedInClickOrder() {
        var strIds = new String();
        for (i = 0; i < elemsClick.length; i++) {
            if (i == 0) {
                strIds = elemsClick[i];
            }
            else {
                strIds = strIds + "," + elemsClick[i];
            }
        }
        return strIds;
    }


    //$('#ctl00_ContentPlaceHolder1_IratpeldanyokList_PldIratPeldanyokGridView_ctl01_SelectingRowsImageButton').on('change', function () {

    //}

    function CheckCheckBoxes() {
        try {

            var kuldemenyGridView = $('#ctl00_ContentPlaceHolder1_IratpeldanyokList_PldIratPeldanyokGridView');
            var checkBoxID = 'check';
            var idPrefix = 'ctl00_ContentPlaceHolder1_IratpeldanyokList_PldIratPeldanyokGridView';

            var count = 0;
            var prefixLength = idPrefix.length;

            var x = document.getElementsByTagName("input");

            var i = 0;
            for (i = 0; i < x.length; i++) {
                if (x[i].type == "checkbox"
                    && x[i].id.substr(0, prefixLength) == idPrefix && x[i].id.indexOf(checkBoxID) != -1
                ) {
                    var gui = x[i].nextSibling.firstChild.nodeValue;
                    if (elemsClick.indexOf(gui) != -1) {
                        if (x[i].checked == false) {
                            for (var i = 0; i < elemsClick.length; i++) {
                                if (elemsClick[i] === gui) {
                                    elemsClick.splice(i, 1);
                                }
                            }
                        }
                    }
                    else {
                        if (x[i].checked == true) {
                            elemsClick.push(gui);
                        }
                    }

                    $('#' + x[i].id).on('click', function () {

                        var checkId = $(event.target).attr('id');
                        var hField = $('#ctl00_ContentPlaceHolder1_IratpeldanyokList_clickOrderInGrid');
                        var currentCheckBox = document.getElementById(checkId);
                        var guid = currentCheckBox.nextSibling.firstChild.nodeValue;

                        if (elemsClick.indexOf(guid) == -1) {
                            if (currentCheckBox.checked == true) {
                                elemsClick.push(guid);
                            }
                        }
                        else {
                            if (currentCheckBox.checked == false) {
                                for (var i = 0; i < elemsClick.length; i++) {
                                    if (elemsClick[i] === guid) {
                                        elemsClick.splice(i, 1);
                                    }
                                }
                            }
                        }

                        hField.val(JSON.stringify(elemsClick));
                    });
                }
            }
            var hField = $('#ctl00_ContentPlaceHolder1_IratpeldanyokList_clickOrderInGrid');
            hField.val(JSON.stringify(elemsClick));
        } catch (e) {
            //console.log(e);
        }
    }

    Sys.Application.add_load(function () {
        CheckCheckBoxes();
    });

</script>
