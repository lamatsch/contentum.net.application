using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Reflection;
using Contentum.eQuery;
using System.Text;
using Newtonsoft.Json;
using System.Linq;

public partial class eRecordComponent_IratpeldanyokList : System.Web.UI.UserControl
{

    private ASP.component_listheader_ascx ListHeader1;

    private string POSTAI_RAGSZAM_KIOSZTAS_SORREND = "A";

    public ASP.component_listheader_ascx ListHeader
    {
        get { return this.ListHeader1; }
        set { this.ListHeader1 = value; }
    }

    private ScriptManager ScriptManager1;

    public ScriptManager ScriptManager
    {
        get { return this.ScriptManager1; }
        set { this.ScriptManager1 = value; }
    }

    public ASP.erecordcomponent_dosszietreeview_ascx AttachedDosszieTreeView
    {
        get { return this.DosszieTreeView; }
        set { this.DosszieTreeView = value; }
    }

    public bool DossziePanelVisible
    {
        get { return PanelDossziekTD.Visible; }
        set { PanelDossziekTD.Visible = value; }
    }

    private string _dosszieId = string.Empty;

    public string DosszieId
    {
        get { return this._dosszieId; }
        set { this._dosszieId = value; }
    }


    private string _dosszieId_HiddenField_ClientId;

    public string DosszieId_HiddenField_ClientId
    {
        get { return this._dosszieId_HiddenField_ClientId; }
        set { this._dosszieId_HiddenField_ClientId = value; }
    }


    public GridView IratPeldanyokGridView
    {
        get { return this.PldIratPeldanyokGridView; }
    }

    public UpdatePanel IratPeldanyokUpdatePanel
    {
        get { return this.PldIratPeldanyokUpdatePanel; }
    }

    UI ui = new UI();

    private String _Startup = "";

    public string Startup
    {
        get
        {
            return _Startup;
        }
        set
        {
            _Startup = value;
        }
    }

    private bool isActive = true;

    public bool IsActive
    {
        get { return isActive; }
        set { isActive = value; }
    }

    #region Base Page

    public void InitPage()
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratPeldanyokList");

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
        if ((Startup == Constants.Startup.SearchForm || Startup == Constants.Startup.FastSearchForm)
            && (Session["IratPeldanyokListStartup"] == null))
        {
            Session["IratPeldanyokListStartup"] = Page.Request.QueryString.Get(Constants.Startup.StartupName);
            Response.Clear();
            Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
        }

        POSTAI_RAGSZAM_KIOSZTAS_SORREND = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "POSTAI_RAGSZAM_KIOSZTAS_SORREND");

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        // Jobb oldali dosszi�lista
        if (FunctionRights.GetFunkcioJog(Page, "MappakList"))
        {
            AttachedDosszieTreeView.FilterList += PldIratPeldanyokGridViewBind;
            JavaScripts.RegisterSetDossziePositionScript(Page, false);
        }
        else
        {
            this.DossziePanelVisible = false;
        }

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, PldIratPeldanyokGridView);

        // T�K-os oszlopok:
        this.SetGridTukColumns();

        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            UI.SetGridViewColumnVisiblity(PldIratPeldanyokGridView, "KezelesTipusNev", false);
        }

        // BLG_619
        if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {
            UI.SetGridViewColumnVisiblity(PldIratPeldanyokGridView, "IratFelelos_SzervezetKod", true);
        }
        else UI.SetGridViewColumnVisiblity(PldIratPeldanyokGridView, "IratFelelos_SzervezetKod", false);

        //BLG_2948 - Calendar localization miatt
        ScriptManager.EnableScriptLocalization = true;
        ScriptManager.EnableScriptGlobalization = true;
    }

    public void LoadPage()
    {
        ListHeader1.HeaderLabel = Resources.List.PldIratPeldanyokHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_PldIratPeldanyokSearch);

        ListHeader1.NumericSearchVisible = true;

        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;

        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager.RegisterPostBackControl(ListHeader1.ExportButton);
        //bernat.laszlo eddig

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
            ListHeader1.SSRSPrintFutarVisible = true;
        else
            ListHeader1.SSRSPrintFutarVisible = false;

        ListHeader1.UgyiratTerkepVisible = true;
        //BLG 1880 AUTO_RAGSZAMOSZTAS_TOMEGESEN = 1 eset�n
        ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
        ListHeader1.TomegesRagszamGeneralasVisible = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.AUTO_RAGSZAMOSZTAS_TOMEGESEN).Trim().Equals("1");

        ListHeader1.SetRightFunctionButtonsVisible(false);

        // Dosszi�s ikonok l�that�s�g�nak �ll�t�sa
        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            ListHeader1.DossziebaHelyezVisible = false;
            ListHeader1.DossziebolKiveszVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakNew");
        }
        else
        {
            ListHeader1.DossziebaHelyezVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakInvalidate");
            ListHeader1.DossziebolKiveszVisible = false;
        }

        ListHeader1.AtadasraKijelolVisible = true;
        ListHeader1.AtvetelVisible = true;
        ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;
        ListHeader1.AthelyezesVisible = true; // iratp�ld�ny �gyiratba juttat�sa
        ListHeader1.ExpedialasVisible = true;
        ListHeader1.PostazasVisible = true;
        ListHeader1.PostazasTomegesVisible = true;
        ListHeader1.SztornoVisible = true;
        ListHeader1.CsatolasVisible = true;

        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromIratPeldany,
            "", Defaults.PopupWidth, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //BLG 1880 t�gemes ragsz�m gener�l�s
        ListHeader1.TomegesRagszamGeneralasOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + PldIratPeldanyokGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";


        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidthMaxExtended, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(PldIratPeldanyokGridView.ClientID);

        if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
        {
            ListHeader1.PrintOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                    + PldIratPeldanyokGridView.ClientID + "','check'); "
                                    + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                                    + "javascript:window.open('PldIratpeldanyokAutoPrintSelector.aspx?Tomeges=true&ids=" + "'+getSelectedInClickOrder()" + ")";
        }
        else
        {
            ListHeader1.PrintOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                    + PldIratPeldanyokGridView.ClientID + "','check'); "
                                    + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                                    + "javascript:window.open('PldIratpeldanyokAutoPrintSelector.aspx?Tomeges=true&ids=" + "'+getIdsBySelectedCheckboxes('" + PldIratPeldanyokGridView.ClientID + "','check')" + ")";
        }

        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IratPeldanyokGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IraJegyzekekColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.PldIratPeldanyokSSSRS] = visibilityState;


        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('PldIratPeldanyokSSSRS.aspx')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(PldIratPeldanyokGridView.ClientID);

        //ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + PldIratPeldanyokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //ListHeader1.AtadasraKijelolOnClientClick += JavaScripts.SetOnClientClick("AtadasForm.aspx",
        //    QueryStringVars.IratPeldanyId + "=" + "'+getIdsBySelectedCheckboxes('" + PldIratPeldanyokGridView.ClientID + "','check')+'"
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                               + PldIratPeldanyokGridView.ClientID + "','check'); "
                               + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AthelyezesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                               + PldIratPeldanyokGridView.ClientID + "','check'); "
                               + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        #region CR3152 - Expedi�l�s folyamat t�meges�t�se
        //ListHeader1.ExpedialasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ExpedialasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + PldIratPeldanyokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        #endregion CR3152 - Expedi�l�s folyamat t�meges�t�se

        ListHeader1.PostazasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(PldIratPeldanyokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(PldIratPeldanyokGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(PldIratPeldanyokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, "", true);

        ListHeader1.DossziebaHelyezOnClientClick = "var count = getSelectedCheckBoxesCount('"
                        + PldIratPeldanyokGridView.ClientID + "','check'); "
                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                        + " if (!$get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value) {alert('Nincs dosszi� kijel�lve' + $get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value); return false;}";

        ListHeader1.DossziebolKiveszOnClientClick = " var count = getSelectedCheckBoxesCount('"
                                + PldIratPeldanyokGridView.ClientID + "','check'); "
                                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //Fel�lvizs�lat
        if (Rendszerparameterek.IsTUK(Page))
        {
            ListHeader1.FelulvizsgalatVisible = true;

            ListHeader1.FelulvizsgalatOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + PldIratPeldanyokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.FelulvizsgalatOnClientClick += JavaScripts.SetOnClientClick("FelulvizsgalatForm.aspx",
                QueryStringVars.IratPeldanyId + "=" + "'+getIdsBySelectedCheckboxes('" + PldIratPeldanyokGridView.ClientID + "','check')+'" +
                "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromIratPeldany
                , Defaults.PopupWidth, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        }

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = PldIratPeldanyokGridView;

        #region BLG 1131 visszav�tel
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        {
            ListHeader1.VisszavetelVisible = true;
            ListHeader1.VisszavetelEnabled = true;
            ListHeader1.VisszavetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + PldIratPeldanyokGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
        #endregion

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_PldIratPeldanyokSearch(true));

        //if (!IsPostBack) PldIratPeldanyokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);


        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["IratPeldanyokListStartup"] != null &&
            (Session["IratPeldanyokListStartup"].ToString() == Constants.Startup.SearchForm
            || Session["IratPeldanyokListStartup"].ToString() == Constants.Startup.FastSearchForm))
        {
            string script = "OpenNewWindow(); function OpenNewWindow() { ";
            if (Session["IratPeldanyokListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("PldIratPeldanyokSearch.aspx", ""
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                , CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            else if (Session["IratPeldanyokListStartup"].ToString() == Constants.Startup.FastSearchForm)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                    , Constants.Startup.StartupName + "=" + Constants.Startup.FromIratPeldany, Defaults.PopupWidth, Defaults.PopupHeight
                    , PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            script += "}";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "IratPeldanyokListStartup", script, true);
            Session.Remove("IratPeldanyokListStartup");

            popupNyitas = true;
        }

        //ListHeader1.PostazasTomegesOnClientClick = JavaScripts.SetOnClientClick("PostazasTomegesForm.aspx", ""
        //  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


        // Ha keres� k�perny�t is kell nyitni, nem t�ltj�k fel a list�t:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            PldIratPeldanyokGridViewBind();
        }
    }

    public void PreRenderPage()
    {
        //ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.New);
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Modify);
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.View);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
            ListHeader1.SSRSPrintFutarEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");
        else
            ListHeader1.SSRSPrintFutarEnabled = false;

        //bernat.laszlo added
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.ExcelExport);
        //bernat.laszlo eddig

        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Invalidate);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.ViewHistory);

        ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtadas");
        ListHeader1.ExpedialasEnabled = FunctionRights.GetFunkcioJog(Page, "Expedialas");
        ListHeader1.PostazasEnabled = FunctionRights.GetFunkcioJog(Page, "Postazas");
        ListHeader1.SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanySztorno");

        ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtvetel");
        ListHeader.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtvetel"); // egyel�re az �tv�tel jog�hoz k�tj�k
        ListHeader1.AthelyezesEnabled = FunctionRights.GetFunkcioJog(Page, "IratpeldanyUgyiratbaHelyezes");

        ListHeader1.CsatolasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasNew");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldany" + CommandName.Lock);

        ListHeader1.FelulvizsgalatEnabled = FunctionRights.GetFunkcioJog(Page, "Felulvizsgalat");

        //if (IsPostBack)
        {
            string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        SetClientScriptToGridViewSelectDeSelectButton(PldIratPeldanyokGridView);

        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IratPeldanyokGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IraJegyzekekColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.PldIratPeldanyokSSSRS] = visibilityState;

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('PldIratPeldanyokSSSRS.aspx')";
    }


    public void SetClientScriptToGridViewSelectDeSelectButton(GridView ParentGridView)
    {
        if (ParentGridView == null) return;
        if (ParentGridView.Rows.Count > 0)
        {
            //(ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

            //(ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

            ImageButton sib = (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton);
            if (sib != null)
                sib.Attributes["onClick"] = "SelectAllCheckBox('" + ParentGridView.ClientID + "','check');CheckCheckBoxes();return false";
            else
            {
                Logger.Error("Sorkiv�laszt� gomb nem talalhato !!!");
                //throw new Exception("Sorkiv�laszt� gomb nem talalhato !!!");
            }

            ImageButton desib = (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton);
            if (desib != null)
                desib.Attributes["onClick"] = "DeSelectAllCheckBox('" + ParentGridView.ClientID + "','check');CheckCheckBoxes();return false;";
            else
            {
                Logger.Error("Sorkiv�laszt�st t�rl� gomb nem talalhato !!!");
                // throw new Exception("Sorkiv�laszt�st t�rl� gomb nem talalhato !!!");
            }
        }
    }

    #endregion

    #region Master List

    public string GetDefaultSortExpression()
    {
        // BUG_6459
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.LISTA_RENDEZETTSEG_IKTATOSZAM, false))
        {
            return Search.FullSortExpression + "EREC_IraIktatokonyvek.Ev DESC, EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC, EREC_PldIratPeldanyok.Sorszam";
        }
        else
        {
            return "EREC_IraIktatoKonyvek.Iktatohely ASC, EREC_UgyUgyiratok.Foszam {0}, EREC_IraIratok.Alszam {0}, EREC_IraIratok.Sorszam {0}, EREC_PldIratPeldanyok.Sorszam"; // EREC_PldIratPeldanyok.LetrehozasIdo
        }
    }

    public void PldIratPeldanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("PldIratPeldanyokGridView", ViewState, this.GetDefaultSortExpression());
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PldIratPeldanyokGridView", ViewState, SortDirection.Descending);

        PldIratPeldanyokGridViewBind(sortExpression, sortDirection);
    }

    /// <summary>
    /// Adatk�t�s
    /// </summary>
    protected void PldIratPeldanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (!this.IsActive)
            return;

        DokumentumVizualizerComponent.ClearDokumentElements();

        // BUG_8359
        if (SortExpression == Contentum.eUtility.Search.FullSortExpression)
        {
            SortExpression = this.GetDefaultSortExpression();
            SortDirection = SortDirection.Descending;
        }

        EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_PldIratPeldanyokSearch search = null;

        if (!IsPostBack)
        {
            string qsStartup = Page.Request.QueryString.Get(QueryStringVars.Startup);
            if (qsStartup == Constants.Startup.FromFeladataim) // ha feladataim panelr�l ind�tottuk
            {
                // �j search objektumot hozunk l�tre, �s sessionbe tessz�k
                search = new EREC_PldIratPeldanyokSearch(true);
                Search.SetSearchObject(Page, search);
            }
        }

        if (Startup == Constants.Startup.FromKimenoIratPeldany)
        {
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch))
            {
                search = (EREC_PldIratPeldanyokSearch)Search.CreateSearchObjWithDefFilter_KimenoIratPeldany(Page);
                // sessionbe ment�s
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch);
            }
            else
            {
                search = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_PldIratPeldanyokSearch(true), Constants.CustomSearchObjectSessionNames.KimenoPeldanyokSearch);
            }

            /*POSTAZAS_IRANYA a BOPMH-ban a bels� = kimen�.*/
            search.Extended_EREC_IraIratokSearch.PostazasIranya.Filter(KodTarak.POSTAZAS_IRANYA.Belso);
            // k�ld�s m�d: helyben marad�-k ne jelenjenek meg
            string helybenMarado = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false) ?
                KodTarak.KULDEMENY_KULDES_MODJA.TUK_Helyben_marado : KodTarak.KULDEMENY_KULDES_MODJA.Helyben_marad;
            string sqlKuldesMod = Search.whereNotEquals(search.KuldesMod.Name, helybenMarado);
            if (!search.WhereByManual.Contains(sqlKuldesMod))
            {
                search.WhereByManual += sqlKuldesMod;
            }

            // c�mzett: �tmeneti iratt�r-os t�telek ne jelenjenek meg (r�gebbi rekordok, nem helyben marad� k�ld�s m�ddal)
            string peldanyCimzettje = Rendszerparameterek.Get(Page, Rendszerparameterek.UGYIRATBAN_MARADO_PELDANY_CIMZETTJE); // "�tmeneti iratt�r"
            if (!String.IsNullOrEmpty(peldanyCimzettje))
            {
                string sql = Search.whereNotEquals(search.NevSTR_Cimzett.Name, peldanyCimzettje);
                if (!search.WhereByManual.Contains(sql))
                {
                    search.WhereByManual += sql;
                }
            }
        }
        else
        // Keres�si objektum kiv�tele a sessionb�l; ha nincs benne, a default sz�r�ssel beletessz�k
        if (!Search.IsSearchObjectInSession(Page, typeof(EREC_PldIratPeldanyokSearch)))
        {
            search = (EREC_PldIratPeldanyokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_PldIratPeldanyokSearch), Page);
            Search.SetSearchObject(Page, search);
        }
        // CR3413
        if (search == null) search = (EREC_PldIratPeldanyokSearch)Search.GetSearchObject(Page, new EREC_PldIratPeldanyokSearch(true));

        search.OrderBy = Search.GetOrderBy("PldIratPeldanyokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Van-e sz�r�s k�r�s param�terben? (Saj�t iratp�ld�nyokra)
        // (Csak els� k�r�skor figyelj�k)
        if (!IsPostBack)
        {
            string filter = Request.QueryString.Get(QueryStringVars.Filter);
            if (filter == Constants.Sajat)
            {
                search.Manual_Sajat.Filter(Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page)));

                // Nem kell:
                // ((Lez�rt jegyz�ken, Jegyz�kre helyezett, C�mzett �tvette, Post�zott, Sztorn�zott)
                var notInAllapotok = new string[] {
                    KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken,
                    KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett,
                    //KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette,
                    //KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at,
                    //KodTarak.IRATPELDANY_ALLAPOT.Postazott,
                    KodTarak.IRATPELDANY_ALLAPOT.Sztornozott
                };

                search.Manual_Sajat_Allapot.NotIn(notInAllapotok);

                search.Manual_Sajat_TovabbitasAlattAllapot.IsNullOrNotIn(notInAllapotok);

                search.ReadableWhere = "Saj�t ";

                if (!Search.IsSearchObjectInSession(Page, typeof(EREC_PldIratPeldanyokSearch)))
                {
                    Search.SetSearchObject(Page, search);
                }
            }
            else if (filter == Constants.JovahagyandokFilter.Jovahagyandok)
            {
                IratPeldanyok.SetSearchObjectTo_Jovahagyandok(search, Page);

                if (!Search.IsSearchObjectInSession(Page, typeof(EREC_PldIratPeldanyokSearch)))
                {
                    Search.SetSearchObject(Page, search);
                }
            }
        }

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = new KRT_MappakSearch();

            search.Extended_KRT_MappakSearch.Id.Filter(this.AttachedDosszieTreeView.SelectedId);
        }

        Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
        //Result res = service.GetAllWithExtension(ExecParam, search);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = null;
        }

        UI.GridViewFill(PldIratPeldanyokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetVegyesInfo(e);
        GridView_RowDataBound_SetTertivevenyInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_PldIratPeldanyok);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;
        string csatolmanyCount = String.Empty;

        if (dataRowView.Row.Table.Columns.Contains("CsatolmanyCount") && dataRowView["CsatolmanyCount"] != null)
        {
            csatolmanyCount = dataRowView["CsatolmanyCount"].ToString();
        }

        ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
        if (CsatolmanyImage == null)
        {
            return;
        }
        if (string.IsNullOrEmpty(csatolmanyCount))
        {
            CsatolmanyImage.OnClientClick = "";
            CsatolmanyImage.Visible = false;
            return;
        }

        CsatolmanyImage.OnClientClick = "";
        int count = 0;
        Int32.TryParse(csatolmanyCount, out count);

        if (count == 0)
        {
            CsatolmanyImage.Visible = false;
            return;
        }
        //t�rolt elj�r�s vizsg�lja
        //#region BLG_577
        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
        //{   /*NINCS JOGA*/
        //    CsatolmanyImage.Visible = false;
        //    return;
        //}
        //#endregion

        CsatolmanyImage.Visible = true;

        string orzoId = dataRowView.Row["FelhasznaloCsoport_Id_Orzo"] == null ? string.Empty : dataRowView.Row["FelhasznaloCsoport_Id_Orzo"].ToString();
        if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, orzoId))
        {
            CsatolmanyImage.ToolTip = Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight;
            CsatolmanyImage.Enabled = false;
            CsatolmanyImage.OnClientClick = "";
            return;
        }

        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

        if (count == 1 && dataRowView.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(dataRowView["Dokumentum_Id"].ToString()))
        {
            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", dataRowView["Dokumentum_Id"]);
            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, dataRowView["Dokumentum_Id"].ToString());
        }
        else
        {
            // set "link"
            string id = "";

            if (dataRowView["Id"] != null)
            {
                id = dataRowView["Id"].ToString();
            }

            if (!string.IsNullOrEmpty(id))
            {
                // Modify->View �tir�ny�t�s jog szerint a formon, ez�rt itt nem vizsg�ljuk feleslegesen
                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
        }

    }

    private void GridView_RowDataBound_SetTertivevenyInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;
        string tertivevenyDokumentumId = null;

        ImageButton TertivevenyImage = (ImageButton)e.Row.FindControl("TertivevenyImage");

        if (TertivevenyImage == null)
        {
            return;
        }

        TertivevenyImage.OnClientClick = "";
        TertivevenyImage.Visible = false;
        if (!dataRowView.Row.Table.Columns.Contains("Tertiveveny_Dokumentum_Id"))
        {
            return;
        }

        tertivevenyDokumentumId = dataRowView["Tertiveveny_Dokumentum_Id"].ToString();
        if (String.IsNullOrEmpty(tertivevenyDokumentumId))
        {
            return;
        }

        string imageId = String.Format("tertivevenyImage_{0}", dataRowView["Id"].ToString());
        DokumentumVizualizerComponent.RemoveDokumentumElement(imageId);

        string tooltipText = "T�rtivev�ny";

        TertivevenyImage.Visible = true;
        TertivevenyImage.AlternateText = tooltipText;
        TertivevenyImage.ToolTip = tooltipText;

        TertivevenyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", tertivevenyDokumentumId);
        DokumentumVizualizerComponent.AddDokumentElement(imageId, tertivevenyDokumentumId);
    }

    protected void PldIratPeldanyokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, PldIratPeldanyokCPE);

        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        PldIratPeldanyokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, PldIratPeldanyokCPE);
        PldIratPeldanyokGridViewBind();
    }

    protected void PldIratPeldanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PldIratPeldanyokGridView, selectedRowNumber, "check");
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID);

            string tableName = "EREC_PldIratPeldanyok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, PldIratPeldanyokUpdatePanel.ClientID);
            ListHeader1.PrintOnClientClick = "javascript:window.open('PldIratPeldanyokPrintForm.aspx?" + QueryStringVars.IratPeldanyId + "=" + id + "')";

            EREC_PldIratPeldanyok obj_iratPeldany = null;
            EREC_IraIratok obj_irat = null;
            EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
            EREC_UgyUgyiratok obj_ugyirat = null;

            bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                , Page, EErrorPanel1);

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);

            // irathoz els� iratp�ld�ny �rz�j�t csak akkor �ll�tjuk be, ha ez az iratp�ld�ny volt az els�
            // TODO: minden esetben be k�ne �ll�tani az els� iratp�ld�ny �rz�j�t
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);

            ErrorDetails errorDetail;

            // �tv�tel:
            if (IratPeldanyok.Atveheto(iratPeldanyStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.AtvetelOnClientClick = "if (confirm('"
                    + Resources.Question.UIConfirmHeader_Atvetel
                    + "')) { } else { return false; }";
            }
            else
            {
                // Nem vehet� �t:
                ListHeader1.AtvetelOnClientClick = "alert('" + Resources.Error.ErrorCode_52292
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }

            // az esetleg felesleges h�v�sok elker�l�se miatt nem v�gezz�k el a teljes ellen�rz�st
            //if (IratPelanyok.CheckVisszakuldhetoWithFunctionRight(Page, statusz, out errorDetail))
            if (IratPeldanyok.Visszakuldheto(execParam, iratPeldanyStatusz, out errorDetail))
            {
                VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
            }
            else
            {
                // Nem k�ldhet� vissza:
                ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            bool isModosithato = IratPeldanyok.Modosithato(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            bool isModosithatoOverride = IratPeldanyok.IsModosithatoOveride(iratPeldanyStatusz, execParam);
            // M�dos�t�s
            if (!isModosithato && !isModosithatoOverride)
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_IratPeldanyMegtekintes
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" +
                    JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                    + "} else return false;";

                // �gyiratt�rk�p View m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID);
            }
            else if (!isModosithato && isModosithatoOverride)
            {
                //Az�rt mehet modify m�dban mert a form �tfogja dobni  view ra..
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                             
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                // �gyiratt�rk�p Modify m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                   + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID);
            }

            //// �tad�sra kijel�lhet�:            
            //if (IratPeldanyok.AtadasraKijelolheto(iratPeldanyStatusz, iratStatusz, ugyiratDarabStatusz, execParam) == true)
            //{
            //    ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
            //        , QueryStringVars.IratPeldanyId + "=" + id
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.AtadasraKijelolOnClientClick = "alert('" + Resources.Error.ErrorCode_52281 + "'); return false;";
            //}

            #region CR3152 - Expedi�l�s folyamat t�meges�t�se
            // Expedi�l�s
            bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;

            if (IratPeldanyok.Expedialhato(iratPeldanyStatusz, kiadmanyozando
                        , iratStatusz, ugyiratStatusz, execParam, out errorDetail) == true)
            {
                ListHeader1.ExpedialasOnClientClick = JavaScripts.SetOnClientClick("ExpedialasForm.aspx"
                    , QueryStringVars.IratPeldanyId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // Nem expedi�lhat�
                ListHeader1.ExpedialasOnClientClick = "alert('" + Resources.Error.ErrorCode_52501
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }
            #endregion  CR3152 - Expedi�l�s folyamat t�meges�t�se

            // Postazas
            if (IratPeldanyok.Postazhato(iratPeldanyStatusz, kiadmanyozando
                    , iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.PostazasOnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx",
                   QueryStringVars.IratPeldanyId + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                //ListHeader1.PostazasOnClientClick =
                //    "if (confirm('"
                //        + Resources.Question.UIConfirmHeader_IratPeldanyPostazas
                //        + "')) { } else { return false; }";
            }
            else
            {
                // Nem post�zhat�
                ListHeader1.PostazasOnClientClick = "alert('" + Resources.Error.ErrorCode_52533
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }

            // Sztorn�z�s:
            if (IratPeldanyok.Sztornozhato(iratPeldanyStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                //BLG_2948 - LZS - Megvizsg�ljuk, hogy az iratp�ld�ny pap�ralap�-e, �s ezzel a param�terrel ind�tjuk a sztorn�z�st,
                //ha az iratp�ld�ny sztorn�zhat� az IratPeldanyok.Sztornozhato met�dus alapj�n.
                bool isMegsemmisitesOption = obj_iratPeldany.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir ? true : false;
                SztornoPopup.SetOnclientClickShowFunction(ListHeader1.SztornoButton, isMegsemmisitesOption);
            }
            else
            {
                // Nem sztorn�zhat�:
                ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.ErrorCode_52301
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // Csatol�s:
            if (IratPeldanyok.Csatolhato(iratPeldanyStatusz, out errorDetail))
            {
                ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx",
                    QueryStringVars.Id + "=" + id + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromIratPeldany
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.CsatolasOnClientClick = "alert('" + Resources.Error.ErrorCode_55030
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // Ugyiratba helyez�s:
            if (!IratPeldanyok.UgyiratbaHelyezheto(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.AthelyezesOnClientClick = "alert('" + Resources.Error.ErrorCode_52743
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

        }
    }

    protected void PldIratPeldanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    PldIratPeldanyokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    //deleteSelectedPldIratPeldany();
        //    //PldIratPeldanyokGridViewBind();
        //}
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, PldIratPeldanyokGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
        //bernat.laszlo eddig

        else if (e.CommandName == CommandName.PrintSSRSFutar)
        {
            #region Show SSRS print selector
            string idsString = GetSelectedIdsAsSeparatedString();
            string js = null;
            if (string.IsNullOrEmpty(idsString))
            {
                js = String.Format("alert('{0}{1}');", Resources.Error.MessageNoSelectedItem, null);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SSRSFutarMessageNoSelectedItem", js, true);
            }
            else
            {
                string url = "PldIratPeldanyokSSSRSFutarJegyzek.aspx?ids=" + idsString;
                js = JavaScripts.SetOnClientClickWithTimeout(url, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanySSRSFutar", js, true);
            }
            #endregion
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        string js = string.Empty;

        String RecordId = UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView);
        switch (e.CommandName)
        {
            case CommandName.Sztorno:
                /// BLG_2948 - LZS - Sztornozas kieg�sz�t�se a 'SztornoMegsemmisitve' �s a 'SztornoMegsemmisitesDatuma' param�terrel.
                IratPeldanyok.Sztornozas(UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView), SztornoPopup.SztornoIndoka, SztornoPopup.SztornoMegsemmisitve, SztornoPopup.SztornoMegsemmisitesDatuma, Page, EErrorPanel1, ErrorUpdatePanel);
                PldIratPeldanyokGridViewBind();
                break;
            case CommandName.DossziebaHelyez:
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = null;
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebaHelyez + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyDossziebaMozgatas", js, true);
                break;
            case CommandName.DossziebolKivesz:
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = null;
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebolKivesz + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyDossziebaMozgatas", js, true);
                break;
            case CommandName.Atvetel:
                {
                    bool mindigTomegesAtvetel = Rendszerparameterek.GetBoolean(Page, "TOMEGES_ATVETEL_ENABLED", false);

                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check") == 1
                        && !String.IsNullOrEmpty(RecordId)
                        && lstGridViewSelectedRows.Contains(RecordId) && !mindigTomegesAtvetel)
                    {
                        if (lstGridViewSelectedRows.Count > 0)
                        {
                            ErrorDetails errorDetail;
                            bool atveheto = IratPeldanyok.CheckAtvehetoWithFunctionRight(Page, RecordId, out errorDetail);

                            if (!atveheto)
                            {
                                js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52292, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAt", js, true);
                            }
                            else
                            {
                                IratPeldanyok.Atvetel(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
                                PldIratPeldanyokGridViewBind();
                            }
                        }
                    }
                    else
                    {
                        Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                        Session["SelectedKuldemenyIds"] = null;
                        Session["SelectedBarcodeIds"] = null;
                        Session["SelectedUgyiratIds"] = null;
                        Session["SelectedDosszieIds"] = null;
                        //ne okozzon gondot, ha kor�bban h�vtuk a t�k visszav�tel funkci�t
                        Session["TUKVisszavetel"] = null;

                        js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                    }
                }
                //IratPeldanyok.Atvetel(UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
                //PldIratPeldanyokGridViewBind();
                break;
            case CommandName.Visszakuldes:
                {
                    ErrorDetails errorDetail;
                    bool visszakuldheto = IratPeldanyok.CheckVisszakuldhetoWithFunctionRight(Page, RecordId, out errorDetail);

                    if (!visszakuldheto)
                    {
                        js = String.Format("alert('{0}{1}');", Resources.Error.UINemVisszakuldhetoTetel, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemKuldhetoVissza", js, true);
                    }
                    else
                    {
                        IratPeldanyok.Visszakuldes(RecordId, VisszakuldesPopup.VisszakuldesIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                        PldIratPeldanyokGridViewBind();
                    }
                }
                break;
            //case CommandName.Postazas:
            //    IratPeldanyok.Postazas(UI.GetGridViewSelectedRecordId(PldIratPeldanyokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
            //    PldIratPeldanyokGridViewBind();
            //    break;
            case CommandName.Lock:
                LockSelectedPldIratPeldanyRecords();
                PldIratPeldanyokGridViewBind();
                break;
            case CommandName.Unlock:
                UnlockSelectedPldIratPeldanyRecords();
                PldIratPeldanyokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedPldIratPeldanyok();
                break;
            case CommandName.AtadasraKijeloles:
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyAtadas", js, true);

                break;
            case CommandName.Athelyezes:
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("IratPldUgyiratbaHelyezesForm.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight
                    , PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyUgyiratbaHelyezes", js, true);

                break;
            case CommandName.Expedialas:
                #region CR3152 - Expedi�l�s folyamat t�meges�t�se
                // Expedi�l�s

                List<string> kijeloltek = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, EErrorPanel1, ErrorUpdatePanel);
                // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
                if (UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check") == 1
                    && !String.IsNullOrEmpty(RecordId)
                    && kijeloltek.Contains(RecordId))
                {
                    if (kijeloltek.Count > 0)
                    {
                        ErrorDetails errorDetail = new ErrorDetails();

                        EREC_PldIratPeldanyok obj_iratPeldany = null;
                        EREC_IraIratok obj_irat = null;
                        EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
                        EREC_UgyUgyiratok obj_ugyirat = null;

                        bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(RecordId, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                            , Page, EErrorPanel1);

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);

                        Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_irat);
                        UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
                        Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
                        bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;
                        bool expedialhato = IratPeldanyok.Expedialhato(iratPeldanyStatusz, kiadmanyozando
                                    , iratStatusz, ugyiratStatusz, execParam, out errorDetail);

                        if (!expedialhato)
                        {
                            js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52501, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemExpedialhato", js, true);
                        }
                        else
                        {
                            ListHeader1.ExpedialasOnClientClick = JavaScripts.SetOnClientClick("ExpedialasForm.aspx"
                                , QueryStringVars.IratPeldanyId + "=" + RecordId
                                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
                        }
                    }
                }
                else
                {
                    Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                    Session["Tomeges"] = "1";

                    js = JavaScripts.SetOnClientClickWithTimeout("ExpedialasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
                }

                #endregion  CR3152 - Expedi�l�s folyamat t�meges�t�se
                break;
            case CommandName.TomegesRagszamGeneralas:
                #region BLG1880 - T�meges ragsz�m gener�l�s
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["Tomeges"] = "1";
                Session["RagszamSource"] = "IratPeldanyokList";

                if (POSTAI_RAGSZAM_KIOSZTAS_SORREND == "K")
                {
                    string[] clickOrder = JsonConvert.DeserializeObject<string[]>(clickOrderInGrid.Value);
                    if (clickOrder != null)
                    {
                        List<string> keys = new List<string>();
                        foreach (DataKey key in PldIratPeldanyokGridView.DataKeys)
                        {
                            keys.Add(key.Value.ToString());
                        }
                        List<string> toRemove = new List<string>();
                        for (int i = 0; i < clickOrder.Length; i++)
                        {
                            if (!keys.Contains(clickOrder[i]))
                            {
                                toRemove.Add(clickOrder[i]);
                            }
                        }

                        var clone = clickOrder.ToList();

                        foreach (string key in toRemove)
                        {
                            clone.Remove(key);
                        }

                        clickOrder = clone.ToArray();
                        clickOrderInGrid.Value = JsonConvert.SerializeObject(clickOrder);

                        Session["SelectedIratPeldanyIdsClickOrder"] = clickOrder;
                    }
                    else
                    {
                        Session["SelectedIratPeldanyIdsClickOrder"] = null;
                    }

                }

                //js = JavaScripts.SetOnClientClickWithTimeout("ExpedialasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                js = JavaScripts.SetOnClientClickWithTimeout("RagszamOsztasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyRagszamOsztas", js, true);
                #endregion
                break;
            case CommandName.Postazas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IratPeldanyokGridView, EErrorPanel1, ErrorUpdatePanel);

                    if (lstGridViewSelectedRows == null || lstGridViewSelectedRows.Count < 1)
                    {
                        string errorJs = String.Format("alert('{0}{1}');", Resources.Error.DefaultErrorHeader, Resources.Error.MessageNoSelectedItem);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "\\t\\tPostazasTomegesHiba", errorJs, true);

                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.MessageNoSelectedItem);
                        return;
                    }
                    string error = CheckPostazhato(lstGridViewSelectedRows);
                    if (!string.IsNullOrEmpty(error))
                    {
                        //string errorJs = String.Format("alert('Hiba t�meges post�z�skor!\n{0}');" ,error);
                        string errorJs = String.Format("alert('{0}\\n{1}');", "Hiba t�meges post�z�s sor�n!", error);

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PostazasTomegesStatuszHiba", errorJs, true);

                        //  ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, error);
                        return;
                    }
                    var sb = new StringBuilder();
                    bool first = true;
                    foreach (string id in lstGridViewSelectedRows)
                    {
                        if (first)
                        {
                            first = false;
                            sb.Append(string.Format("'{0}'", id));
                        }
                        else
                        {
                            sb.Append(string.Format(",'{0}'", id));
                        }
                    }
                    Session["SelectedIratPldIds"] = sb.ToString();

                    js = JavaScripts.SetOnClientClickWithTimeout("PostazasTomegesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectePostazasTomegesForm", js, true);
                }
                break;
            case CommandName.Visszavetel:
                #region BLG1131Visszav�tel
                Session["SelectedIratPeldanyIds"] = GetSelectedIdsAsSeparatedString();
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = null;
                Session["SelectedDosszieIds"] = null;
                Session["TUKVisszavetel"] = "1";

                js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, PldIratPeldanyokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                #endregion
                break;
        }
    }

    private string CheckPostazhato(List<string> ids)
    {
        string result = "";
        foreach (string id in ids)
        {
            EREC_PldIratPeldanyok obj_iratPeldany = null;
            EREC_IraIratok obj_irat = null;
            EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
            EREC_UgyUgyiratok obj_ugyirat = null;

            bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                , Page, EErrorPanel1);


            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);

            // irathoz els� iratp�ld�ny �rz�j�t csak akkor �ll�tjuk be, ha ez az iratp�ld�ny volt az els�
            // TODO: minden esetben be k�ne �ll�tani az els� iratp�ld�ny �rz�j�t
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
            bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;
            ErrorDetails errorDetail;
            bool postazhato = IratPeldanyok.Postazhato(iratPeldanyStatusz, kiadmanyozando
                    , iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            if (!postazhato)
            {

                result = result + obj_iratPeldany.Azonosito + " : " + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "\\n";
            }
        }
        return result;
    }
    //BLG 1880
    private static bool CheckRagszamSavInService(string postakonyv, string kuldFajta, out string errorString, out string outRagSzamSavId, out DataRow outRagSzamSav)
    {
        outRagSzamSavId = null;
        errorString = null;
        outRagSzamSav = null;
        Result result = GetAllRagszamSavFilteredByPkKf(postakonyv, kuldFajta);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.GetCount < 1)
        {
            errorString = Resources.Error.MessageRagszamSavNotFound;
            return false;
        }

        if (result.GetCount > 1)
        {
            errorString = Resources.Error.MessageRagszamSavMoreThanOne;
            return false;
        }

        outRagSzamSavId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        outRagSzamSav = result.Ds.Tables[0].Rows[0];
        return true;
    }
    //BLG 1880
    private static Result GetAllRagszamSavFilteredByPkKf(string postakonyv, string kuldFajta)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Postakonyv_Id.Filter(postakonyv);

        search.SavType.Filter(kuldFajta);

        search.SavAllapot.Filter("A");

        return service.GetAll(execParam, search);
    }
    //BLG 1880
    private bool CheckRendszerParameterKellPostaiRagszam()
    {
        string val = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_EXPEDIALASKOR);
        if (val == null)
            return false;
        else if (val == "1")
            return true;
        else
            return false;
    }

    private Result PostaiRagSzamKiosztasProcedure(EREC_KuldKuldemenyek kuldemeny, string mode, string postakonyvId, string kuldesModja, string ragszamSavId, string ragszam)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.PostaiRagszamKiosztas(execParam, kuldemeny, mode, postakonyvId, kuldesModja, ragszamSavId, ragszam);
        return result;
    }

    private void LockSelectedPldIratPeldanyRecords()
    {
        LockManager.LockSelectedGridViewRecords(PldIratPeldanyokGridView, "EREC_PldIratPeldanyok"
            , "IratPeldanyLock", "IratPeldanyForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedPldIratPeldanyRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(PldIratPeldanyokGridView, "EREC_PldIratPeldanyok"
            , "IratPeldanyLock", "IratPeldanyForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a PldIratPeldanyokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedPldIratPeldanyok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_PldIratPeldanyok");
        }
    }

    protected void PldIratPeldanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PldIratPeldanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("PldIratPeldanyokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }


    #region BLG#1402
    /// <summary>
    /// T�k specifikus oszlopok hozz�ad�sa a gridhez
    /// </summary>
    private void SetGridTukColumns()
    {
        bool isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);
        if (!isTuk)
            return;

        //BoundField bfMinosites = new BoundField();
        //bfMinosites.DataField = "MinositesNev";
        //bfMinosites.HeaderText = "Min�s�t�s";
        //bfMinosites.SortExpression = "EREC_IraIratok.Minosites";
        //bfMinosites.HeaderStyle.CssClass = "GridViewBorderHeader";
        //bfMinosites.HeaderStyle.Width = new Unit("80");
        //bfMinosites.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        //bfMinosites.Visible = true;
        ////PldIratPeldanyokGridView.Columns.Add(bfMinosites);

        ////LZS - BUG_7245
        //PldIratPeldanyokGridView.Columns.Insert(11, bfMinosites);
        UI.SetGridViewColumnVisiblity(PldIratPeldanyokGridView, "MinositesNev", true);

        BoundField bfMinosito = new BoundField();
        bfMinosito.DataField = "MinositoNev";
        bfMinosito.HeaderText = "Min�s�t�";
        bfMinosito.SortExpression = "EREC_IraIratok.Minosito";
        bfMinosito.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinosito.HeaderStyle.Width = new Unit("80");
        bfMinosito.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinosito.Visible = false;
        PldIratPeldanyokGridView.Columns.Add(bfMinosito);

        BoundField bfMinositoSzervezet = new BoundField();
        bfMinositoSzervezet.DataField = "MinositoSzervezetNev";
        bfMinositoSzervezet.HeaderText = "Min�s�t� szervezet";
        bfMinositoSzervezet.SortExpression = "EREC_IraIratok.MinositoSzervezet";
        bfMinositoSzervezet.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinositoSzervezet.HeaderStyle.Width = new Unit("80");
        bfMinositoSzervezet.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinositoSzervezet.Visible = false;
        PldIratPeldanyokGridView.Columns.Add(bfMinositoSzervezet);

        BoundField bfMinErvIdo = new BoundField();
        bfMinErvIdo.DataField = "MinositesErvenyessegiIdeje";
        bfMinErvIdo.HeaderText = "Min.�rv.id�";
        bfMinErvIdo.SortExpression = "EREC_IraIratok.MinositesErvenyessegiIdeje";
        bfMinErvIdo.DataFormatString = "{0:yyyy.MM.dd}";
        bfMinErvIdo.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinErvIdo.HeaderStyle.Width = new Unit("80");
        bfMinErvIdo.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinErvIdo.Visible = false;
        PldIratPeldanyokGridView.Columns.Add(bfMinErvIdo);

        BoundField bfTerjedelem = new BoundField();
        bfTerjedelem.DataField = "TerjedelemMennyiseg";
        bfTerjedelem.HeaderText = "Terjedelem";
        bfTerjedelem.SortExpression = "EREC_IraIratok.TerjedelemMennyiseg";
        bfTerjedelem.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfTerjedelem.HeaderStyle.Width = new Unit("80");
        bfTerjedelem.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfTerjedelem.Visible = false;
        PldIratPeldanyokGridView.Columns.Add(bfTerjedelem);

        BoundField bfMellekletekSzama = new BoundField();
        bfMellekletekSzama.DataField = "MellekletekSzama";
        bfMellekletekSzama.HeaderText = "Mell�kletek sz�ma";
        bfMellekletekSzama.SortExpression = null;
        bfMellekletekSzama.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMellekletekSzama.HeaderStyle.Width = new Unit("80");
        bfMellekletekSzama.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMellekletekSzama.Visible = false;
        PldIratPeldanyokGridView.Columns.Add(bfMellekletekSzama);

        BoundField bfIrattariHely = new BoundField();
        bfIrattariHely.DataField = "IrattariHely";
        bfIrattariHely.HeaderText = "Fizikai hely";
        bfIrattariHely.SortExpression = "EREC_PldIratpeldanyok.IrattariHely";
        bfIrattariHely.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfIrattariHely.HeaderStyle.Width = new Unit("80");
        bfIrattariHely.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfIrattariHely.Visible = true;
        PldIratPeldanyokGridView.Columns.Add(bfIrattariHely);
    }
    #endregion

    #endregion

    /// <summary>
    /// GetSelectedIdsAsSeparatedString
    /// </summary>
    /// <returns></returns>
    private string GetSelectedIdsAsSeparatedString()
    {
        StringBuilder sb = new System.Text.StringBuilder();
        foreach (string s in ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, EErrorPanel1, ErrorUpdatePanel))
        {
            sb.Append(s + ",");
        }
        if (sb == null || sb.Length < 1)
            return string.Empty;

        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }
}
