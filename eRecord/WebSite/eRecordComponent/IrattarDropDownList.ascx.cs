using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;

public partial class eRecordComponent_IrattarDropDownList : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";


    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList.Items.Clear();
            DropDownList.Items.Add(new ListItem("�tmeneti iratt�r", Constants.IrattarJellege.AtmenetiIrattar));
            DropDownList.Items.Add(new ListItem("K�zponti iratt�r", Constants.IrattarJellege.KozpontiIrattar));
        }
    }

    public void SetAtmenetiIrattar()
    {
        DropDownList.SelectedIndex = 0;
    }

    public void SetKozpontiIrattar()
    {
        DropDownList.SelectedIndex = 1;
    }


    public eDropDownList DropDownList
    {
        get { return Irattar_DropDownList; }
    }

    public String SelectedValue
    {
        get { return Irattar_DropDownList.SelectedValue; }
    }

    public bool Enabled
    {
        get { return Irattar_DropDownList.Enabled; }
        set { Irattar_DropDownList.Enabled = value; }
    }

    public string CssClass
    {
        get { return Irattar_DropDownList.CssClass; }
        set { Irattar_DropDownList.CssClass = value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                Irattar_DropDownList.CssClass = "ViewReadOnlyWebControl";
                //LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                Irattar_DropDownList.CssClass = "ViewDisabledWebControl";
                //LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    public bool ReadOnly
    {
        get
        {
            return DropDownList.ReadOnly;
        }
        set
        {
            DropDownList.ReadOnly = value;
        }
    }

    /// <summary>
    /// Felt�lti a DropDownList-et a megadott k�dcsoporthoz tartoz� (�rv�nyes) k�dt�r�rt�kekkel
    /// </summary>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="errorPanel"></param>
    //public void FillDropDownList(String iktatoerkezteto, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //    EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
    //    Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
    //    Search.IktatoErkezteto.Value = iktatoerkezteto;
    //    Result result = service.GetAll(execParam, Search);
    //    if (String.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        Irattar_DropDownList.Items.Clear();

    //        foreach (DataRow row in result.Ds.Tables[0].Rows)
    //        {
    //            String iktatokonyvNev = row["Nev"].ToString();
    //            String iktatokonyvId = row["Id"].ToString();
    //            Irattar_DropDownList.Items.Add(new ListItem(iktatokonyvNev, iktatokonyvId));
    //        }
    //        if (addEmptyItem)
    //        {
    //            Irattar_DropDownList.Items.Insert(0, emptyListItem);
    //        }
    //    }
    //}

    //public void FillDropDownList(string iktatoerkezteto, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillDropDownList(iktatoerkezteto, false, errorPanel);
    //}

    //public void FillWithOneValue(string iktatoerkezteto, string iktatoId, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    if (!String.IsNullOrEmpty(iktatoId))
    //    {
    //        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
    //        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
    //        Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
    //        Search.IktatoErkezteto.Value = iktatoerkezteto;
    //        Result result = service.GetAll(execParam, Search);

    //        if (String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            Irattar_DropDownList.Items.Clear();
    //            if (result.Ds.Tables[0].Rows.Count > 0)
    //            {
    //                bool isEqualIktatokonyv = false;
    //                foreach (DataRow row in result.Ds.Tables[0].Rows)
    //                {
    //                    String iktatokonyvId = row["Id"].ToString();
    //                    if (iktatokonyvId == iktatoId)
    //                    {
    //                        String iktatokonyvNev = row["Nev"].ToString();
    //                        Irattar_DropDownList.Items.Add(new ListItem(iktatokonyvNev, iktatokonyvId));
    //                        isEqualIktatokonyv = true;
    //                        break;
    //                    }
    //                }
    //                if (!isEqualIktatokonyv)
    //                {
    //                    Irattar_DropDownList.Items.Insert(0, new ListItem(iktatoId + " " + deletedValue, iktatoId));
    //                }
    //            }
    //            else
    //            {
    //                Irattar_DropDownList.Items.Insert(0, new ListItem(iktatoId + " " + deletedValue, iktatoId));
    //            }
    //        }
    //        else
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
    //        }
    //    }
    //    else
    //    {
    //        Irattar_DropDownList.Items.Insert(0, emptyListItem);
    //    }

    //}

    /// <summary>
    /// Felt�lti a list�t, �s be�ll�tja az �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    //public void FillAndSetSelectedValue(String iktatoerkezteto, String selectedValue, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillDropDownList(iktatoerkezteto, addEmptyItem, errorPanel);
    //    ListItem selectedListItem = Irattar_DropDownList.Items.FindByValue(selectedValue);
    //    if (selectedListItem != null)
    //    {
    //        Irattar_DropDownList.SelectedValue = selectedValue;
    //    }
    //    else
    //    {
    //        Irattar_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
    //    }
    //}

    //public void FillAndSetSelectedValue(String iktatoerkezteto, String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillAndSetSelectedValue(iktatoerkezteto, selectedValue, false, errorPanel);
    //}

    //public void FillAndSetEmptyValue(string iktatoerkezteto, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillAndSetSelectedValue(iktatoerkezteto, emptyListItem.Value, true, errorPanel);
    //}


    /// <summary>
    /// Be�ll�tja a m�r felt�lt�tt lista �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ListItem selectedListItem = Irattar_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            Irattar_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            Irattar_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(Irattar_DropDownList);
        // Lekell tiltani a ClientValidator
        //Validator1.Enabled = false;

        return componentList;
    }

    #endregion
    protected void Irattar_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
