﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IrattariHelyLevelekDropDown.ascx.cs" Inherits="eRecordComponent_IrattariHelyLevelekDropDown" %>

<asp:HiddenField ID="HiddenFieldFizikaiHelyId" runat="server" />
<asp:HiddenField ID="HiddenFieldFizikaiHelyText" runat="server" />
<asp:DropDownList ID="IrattarLevelekDropdownList" EnableViewState="true" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" OnSelectedIndexChanged="dropDownList1_SelectedIndexChanged">
</asp:DropDownList>
 <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"  InitialValue="" ErrorMessage="<%$Resources:Form,IrattariHelyNotSelected%>" Display="None" ControlToValidate="IrattarLevelekDropdownList" Enabled="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
 <ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator1" >
       <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
 </ajaxToolkit:ValidatorCalloutExtender>