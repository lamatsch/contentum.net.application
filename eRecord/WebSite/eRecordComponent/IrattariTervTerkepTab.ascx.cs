//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

// csak a r�videbb megnevez�s�rt
using IMDterkep = eRecordComponent_IratMetaDefinicioTerkep;

public partial class eRecordComponent_IrattariTervTerkepTab : System.Web.UI.UserControl
{
    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            //MainPanel.Visible = value;
            TreeViewHeader1.Visible = value;
            if (!value)
            {
                IratMetaDefinicioTerkep1.ClearTreeView();
            }
        }
    }

    public string Command = "";
    private PageView pageView = null;

    private String ParentId = "";

    private string RegisterClientScriptDeleteConfirm()
    {
        string js = "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n\\n"
        + "Biztosan t�rli a kijel�lt csom�pontot �s lesz�rmazottait?"
        + "')) {  } else return false;";
        return js;
    }

    private string RegisterClientScriptSetElementValue(string componentId, string value)
    {
        return "var element = window.document.getElementById('" + componentId + "'); if (element == null) return; else element.value = '" + value + "';";
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;

    #region public Properties
    
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion

    #region Utils

    private String GetIdListFromTable(DataTable table, string Separator, bool bQuoteId)
    {
        List<String> Ids = new List<string>();
        if (table == null) return "";

        foreach (DataRow row in table.Rows)
        {
            string row_Id = row["Id"].ToString();
            if (bQuoteId)
            {
                row_Id = "'" + row_Id + "'";
            }

            Ids.Add(row_Id);
        }

        return String.Join(Separator, Ids.ToArray());
    }

    #endregion Utils

    
    protected void Page_Init(object sender, EventArgs e)
    {        
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        IratMetaDefinicioTerkep1.SearchObjectType = typeof(IrattariTervHierarchiaSearch);
        IratMetaDefinicioTerkep1.ErrorPanel = EErrorPanel1;
        IratMetaDefinicioTerkep1.ErrorUpdatePanel = ErrorUpdatePanel1;
    }

    private bool _reloadTab = false;
    private bool _pageLoaded = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        TreeViewHeader1.HeaderLabel = "Iratt�ri terv t�rk�p";
        TreeViewHeader1.SearchObjectType = typeof(IrattariTervHierarchiaSearch);
        TreeViewHeader1.SetFilterFunctionButtonsVisible(true);
        TreeViewHeader1.SetNodeFunctionButtonsVisible(true);
        TreeViewHeader1.SetChildFunctionButtonsVisible(true);
        TreeViewHeader1.SetCustomNodeFunctionButtonsVisible(false);

        TreeViewHeader1.AttachedTreeView = IratMetaDefinicioTerkep1.TreeView;

        TreeViewHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IrattariTervHierarchiaFilterSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        TreeViewHeader1.NodeFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderNodeFunctionButtonsClick);
        TreeViewHeader1.ChildFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderChildFunctionButtonsClick);

        TreeViewHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TreeViewUpdatePanel.ClientID);     

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

        }
        else
        {
            //ReLoadTab();        
        }

        _pageLoaded = true;
        if (_reloadTab)
        {
            ReLoadTab();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IratMetaDefinicioTerkep1.SelectedNodePosition > 0)
        {
            string js = "Sys.Application.add_load(ScrollToSelectedNode);";
            js += "function ScrollToSelectedNode(){";
            js += "Sys.Application.remove_load(ScrollToSelectedNode);";
            js += "var panel = $get('" + Panel1.ClientID + "');";
            js += "if(panel){";
            js += "var pos = " + IratMetaDefinicioTerkep1.SelectedNodePosition + ";";
            js += "var scrollTop = pos - panel.offsetHeight/2;";
            js += "if(scrollTop > 0){";
            js += "panel.scrollTop = scrollTop;";
            js += "}";
            js += "}";
            js += "}";
 
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ScrollToSelectedNode", js, true);
        }
    }

    private void TreeViewHeaderNodeFunctionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Invalidate:
                InvalidateSelectedNode();
                break;
            case CommandName.ExpandNode:
                IratMetaDefinicioTerkep1.ExpandSelectedNodeAllLevels();
                break;
        }
        
    }

    private void TreeViewHeaderChildFunctionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.View)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshSelectedNode:
                        break;
                    case EventArgumentConst.refreshSelectedNodeChildren:
                        break;
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
    }


    public void ReLoadTab()
    {
        if (ParentForm != "IrattariTervForm")    // TODO: a szulo form meghatarozasa utan ezt kivenni
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            Active = false;
        }
        else
        {
            if (!Active) return;

            if (!_pageLoaded)
            {
                _reloadTab = true;
                return;
            }
            _reloadTab = false;

            LoadTreeView();

            pageView.SetViewOnPage(Command);
        }    
    }

    protected void LoadTreeView()
    {
        ViewState["Loaded"] = false;
        bool isFilterActive = SetKeresesFilter();

        if (isFilterActive && IratMetaDefinicioTerkep1.IsFilteredWithoutHit)
        {
            //IratMetaDefinicioTerkep1.ClearTreeView();
            SetAllButtonsEnabled(false);
            TreeViewHeader1.HeaderNodeButtonsLabel = "";
            TreeViewHeader1.HeaderChildButtonsLabel = "";
        }

        IratMetaDefinicioTerkep1.LoadTreeView();

        ViewState["Loaded"] = true;
    }

    protected void IratMetaDefinicioTerkep_RefreshSelectedNodeChildren(object sender, IMDterkep.IMDTerkepEventArgs e)
    {
        if (e == null || e.nodeId == null || e.nodeId == "") return;
        if (IratMetaDefinicioTerkep1.IsFilteredWithoutHit) return;

        // l�trehoz�skor, ig�ny szerint kiv�lasztjuk az �j node-ot
        if (TreeViewHeader1.SelectNewChild == true)
        {
            String SelectedId = SelectedNodeId_HiddenField.Value;
            IratMetaDefinicioTerkep1.SelectChildNode(SelectedId);
        }

    }

    protected void IratMetaDefinicioTerkep_SelectedNodeChanged(object sender, EventArgs e)
    {
        IMDterkep.NodeType nodeType = IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode);

        //Torles.OnClientClick = RegisterClientScriptDeleteConfirm();
        TreeViewHeader1.InvalidateOnClientClick = RegisterClientScriptDeleteConfirm();

        switch (nodeType)
        {
            case IMDterkep.NodeType.Root:
                SetButtonsByRoot();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.Root);  //"Iratt�ri t�rk�p"
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.AgazatiJelek);  //"�gazati jel";
                break;
            case IMDterkep.NodeType.AgazatiJelek:
                SetButtonsByAgazatiJelek();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.AgazatiJelek);  //"�gazati jel";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IraIrattariTetelek);  //"Iratt�ri t�tel"; 
                break;
            case IMDterkep.NodeType.IraIrattariTetelek:
                SetButtonsByIraIrattariTetelek();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IraIrattariTetelek);  //"Iratt�ri t�tel";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IratMetaDefinicio);  //"Irat metadefin�ci�";
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.IratMetaDefinicio);  //"�gyt�pus";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.EljarasiSzakasz); //"Elj�r�si szakasz";
                break;
            case IMDterkep.NodeType.EljarasiSzakasz:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.EljarasiSzakasz);  //"Elj�r�si szakasz";
                TreeViewHeader1.HeaderChildButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.Irattipus);  //"Iratt�pus";
                break;
            case IMDterkep.NodeType.Irattipus:
                SetButtonsByIratMetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = IratMetaDefinicioTerkep1.GetNodeTypeText(IMDterkep.NodeType.Irattipus);  //"Iratt�pus";
                TreeViewHeader1.HeaderChildButtonsLabel = "";
                break;
            default:
                TreeViewHeader1.HeaderNodeButtonsLabel = "";  //�res
                TreeViewHeader1.HeaderChildButtonsLabel = "";  //�res
                SetAllButtonsEnabled(false);
                break;
        }

    }

    protected void IratMetaDefinicioTerkep_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        // do nothing
    }

    #region Buttons

    private void SetButtonsByRoot()
    {
        // -------------- Engedelyek Beallitasa -------------------
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        TreeViewHeader1.ViewEnabled = false;
        TreeViewHeader1.ModifyEnabled = false;
        TreeViewHeader1.InvalidateEnabled = false;
 
        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJelNew");

            if (TreeViewHeader1.NewEnabled)
            {
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.New
                  + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                  , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

                //SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IraIrattariTetelek); 
            }

        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion
    }

    private void SetButtonsByAgazatiJelek()
    {
        // -------------- Engedelyek Beallitasa -------------------
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(IratMetaDefinicioTerkep1.SelectedId)) return;

        TreeViewHeader1.ViewEnabled = false;
        TreeViewHeader1.ModifyEnabled = false;
        TreeViewHeader1.InvalidateEnabled = false;

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJelView");

            if (TreeViewHeader1.ViewEnabled)
            {
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                   , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJelModify");

            if (TreeViewHeader1.ModifyEnabled)
            {
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJelInvalidate");

            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelNew");

            if (TreeViewHeader1.NewEnabled)
            {
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.New
                  + "&" + QueryStringVars.AgazatiJelId + "=" + IratMetaDefinicioTerkep1.SelectedId
                  + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                  + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                  , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

                //SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IraIrattariTetelek); 
            }

        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion
    }

    private void SetButtonsByIraIrattariTetelek()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        String AgazatiJel_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIraIrattariTetelek(IratMetaDefinicioTerkep1.SelectedId);
        if (String.IsNullOrEmpty(AgazatiJel_Id)) return;
        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(AgazatiJel_Id)) return;

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelView");

            if (TreeViewHeader1.ViewEnabled)
            {
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                   , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelModify");

            if (TreeViewHeader1.ModifyEnabled)
            {
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate");

            // hozz�rendelt IratMetaDefinicio rekord Id kiolvas�sa
            string IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetIratMetaDefinicioIdFromDictionaryIraIrattariTetelek(IratMetaDefinicioTerkep1.SelectedId);

            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioNew");

            if (TreeViewHeader1.NewEnabled)
            {
                // ha van f�l�rendelt irat metadefin�ci�, akkor azt adjuk �t, egy�bk�nt az iratt�ri t�tel Id-t
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + (!String.IsNullOrEmpty(IratMetaDefinicio_Id)?  "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicio_Id : "&"+ QueryStringVars.IrattariTetelId + "=" + IratMetaDefinicioTerkep1.SelectedId)
                    + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                    + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);

                //TreeViewHeader1.NewOnClientClick += RegisterClientScriptSetElementValue(SelectedNodeName_HiddenField.ClientID, IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IratMetaDefinicio));
                ////SelectedNodeName_HiddenField.Value = IratMetaDefinicioTerkep1.GetNameByNodeType(IMDterkep.NodeType.IratMetaDefinicio); 
            }


        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetButtonsByIratMetaDefinicio()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        String IratMetaDefinicio_Id = "";
        String tipus = String.Empty;// m�dos�t�skor �tadva a formnak szab�lyozhat� a mez�k �rhat�s�ga
        switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode))
        {
            case IMDterkep.NodeType.Irattipus:
                {
                    String EljarasiSzakasz_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIrattipus(IratMetaDefinicioTerkep1.SelectedId);
                    if (String.IsNullOrEmpty(EljarasiSzakasz_Id)) return;
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryEljarasiSzakasz(EljarasiSzakasz_Id);
                    if (String.IsNullOrEmpty(IratMetaDefinicio_Id)) return;
                    tipus = Constants.IratMetaDefinicioTipus.Irattipus;
                }
                break;
            case IMDterkep.NodeType.EljarasiSzakasz:
                {
                    IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryEljarasiSzakasz(IratMetaDefinicioTerkep1.SelectedId);
                    if (String.IsNullOrEmpty(IratMetaDefinicio_Id)) return;
                    tipus = Constants.IratMetaDefinicioTipus.EljarasiSzakasz;
                }
                break;
            default:
                IratMetaDefinicio_Id = IratMetaDefinicioTerkep1.SelectedId;
                tipus = Constants.IratMetaDefinicioTipus.IratMetaDefinicio;
                break;
        }

        String Ugykor_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIratMetaDefinicio(IratMetaDefinicio_Id);
        if (String.IsNullOrEmpty(Ugykor_Id)) return;
        String AgazatiJel_Id = IratMetaDefinicioTerkep1.GetParentIdFromDictionaryIraIrattariTetelek(Ugykor_Id);
        if (String.IsNullOrEmpty(AgazatiJel_Id)) return;
        if (!IratMetaDefinicioTerkep1.StoredInDictionaryAgazatiJelek(AgazatiJel_Id)) return;

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioView");

            if (TreeViewHeader1.ViewEnabled)
            {
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                       + "&" + QueryStringVars.IrattariTetelId + "=" + Ugykor_Id
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioModify");


            if (TreeViewHeader1.ModifyEnabled)
            {
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + IratMetaDefinicioTerkep1.SelectedId
                    + "&" + QueryStringVars.Tipus + "=" + tipus
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioInvalidate");

            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioNew");

            if (TreeViewHeader1.NewEnabled)
            {
                switch (IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode))
                {
                    case IMDterkep.NodeType.Irattipus:
                        TreeViewHeader1.NewEnabled = false;
                        break;
                    default:
                        TreeViewHeader1.NewEnabled = true;
                        break;
                }
                if (TreeViewHeader1.NewEnabled)
                {
                    TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.IratMetadefinicioId + "=" + IratMetaDefinicioTerkep1.SelectedId
                        + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                        + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
                }
            }

        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetAllButtonsEnabled(Boolean value)
    {
        TreeViewHeader1.ViewEnabled = value;
        TreeViewHeader1.ModifyEnabled = value;
        TreeViewHeader1.NewEnabled = value;
        TreeViewHeader1.InvalidateEnabled = value;
        TreeViewHeader1.NewCustomNodeEnabled = value;

        TreeViewHeader1.SelectNewChildEnabled = value;
    }

    private void ResetAllButtonsOnClientClick()
    {
        TreeViewHeader1.ViewOnClientClick = "";
        TreeViewHeader1.ModifyOnClientClick = "";
        TreeViewHeader1.NewOnClientClick = "";
        TreeViewHeader1.NewCustomNodeOnClientClick = "";
        //Torles.OnClientClick = "";
    }

    #endregion Buttons

    // ha a keres�s akt�v, true-val t�r vissza, egy�bk�nt false
    private bool SetKeresesFilter()
    {
        bool isSearchActive = false;

        if (Search.IsSearchObjectInSession(Page, typeof(IrattariTervHierarchiaSearch)))
        {
            IrattariTervHierarchiaSearch irattariTervHierarchiaSearch = (IrattariTervHierarchiaSearch)Search.GetSearchObject(Page, new IrattariTervHierarchiaSearch());

            if (irattariTervHierarchiaSearch != null)
            {
                EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.GetAllIrattariTervHierarchiaFilter(execParam, irattariTervHierarchiaSearch);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                }
                else
                {
                    ////String IsFilteredWithNoHit = result.Ds.Tables[0].Rows[0]["IsFilteredWithoutHit"].ToString();
                    bool IsFilteredWithNoHit = (bool)result.Record;
                    String AgazatiJelek_Ids = GetIdListFromTable(result.Ds.Tables[1], ",", true);
                    String IraIrattariTetelek_Ids = GetIdListFromTable(result.Ds.Tables[2], ",", true);
                    ////String IratMetaDefinicio_Ids = GetIdListFromTable(result.Ds.Tables[3], ",", true);
                    String Ugytipus_Ids = GetIdListFromTable(result.Ds.Tables[4], ",", true);
                    String EljarasiSzakasz_Ids = GetIdListFromTable(result.Ds.Tables[5], ",", true);
                    String Irattipus_Ids = GetIdListFromTable(result.Ds.Tables[6], ",", true);

                    IratMetaDefinicioTerkep1.SetNodeFilter(
                        IsFilteredWithNoHit
                        , AgazatiJelek_Ids
                        , IraIrattariTetelek_Ids
                        , Ugytipus_Ids
                        , EljarasiSzakasz_Ids
                        , Irattipus_Ids
                        );

                    isSearchActive = true;
                }

            }
            else
            {
                IratMetaDefinicioTerkep1.ClearNodeFilter();
            }

        }
        else
        {
            IratMetaDefinicioTerkep1.ClearNodeFilter();
        }

        return isSearchActive;
    }

    /// <summary>
    /// T�rli a kijel�lt elemet
    /// </summary>
    private void InvalidateSelectedNode()
    {
        if (String.IsNullOrEmpty(IratMetaDefinicioTerkep1.SelectedId)) return;

        String nodeId = IratMetaDefinicioTerkep1.SelectedId;

        IMDterkep.NodeType nodeType = IratMetaDefinicioTerkep1.GetNodeTypeByName(IratMetaDefinicioTerkep1.SelectedNode);
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = nodeId;
        Result result = null;

        switch (nodeType)
        {
            case IMDterkep.NodeType.AgazatiJelek:
                if (FunctionRights.GetFunkcioJog(Page, "AgazatiJelInvalidate"))
                {
                    EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                    result = service.Invalidate(execParam);
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            case IMDterkep.NodeType.IraIrattariTetelek:
                if (FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate"))
                {
                    EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    result = service.Invalidate(execParam); // hozz� kapcsol�d� irat metadefin�ci�k �rv�nytelen�t�se is
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            case IMDterkep.NodeType.IratMetaDefinicio:
            case IMDterkep.NodeType.EljarasiSzakasz:
            case IMDterkep.NodeType.Irattipus:
                if (FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicioInvalidate"))
                {
                    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    result = service.Invalidate(execParam);   // al�rendelt irat metadefin�ci�k �rv�nytelen�t�se is
                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                }
                break;
            default:
                break;
        }


        if (result != null)
        {
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else
            {
                IratMetaDefinicioTerkep1.DeleteSelectedNode();  // t�rli a kijel�lt elemet, �s gyermekeit a f�b�l
            }
        }
    }

   

}
