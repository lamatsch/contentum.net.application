﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eUIControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class eRecordComponent_IrattariTetelszamDropDownList : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";
    protected void Page_Load(object sender, EventArgs e)
    {
        IrattariTetelszam_DropDownList.SelectedIndexChanged += new EventHandler(IrattariTetelszam_DropDownList_SelectedIndexChanged);
    }

    public eDropDownList DropDownList
    {
        get { return IrattariTetelszam_DropDownList; }
    }

    public String SelectedValue
    {
        get { return IrattariTetelszam_DropDownList.SelectedValue; }
    }

    public bool Enabled
    {
        get { return IrattariTetelszam_DropDownList.Enabled; }
        set { IrattariTetelszam_DropDownList.Enabled = value; }
    }

    public string CssClass
    {
        get { return IrattariTetelszam_DropDownList.CssClass; }
        set { IrattariTetelszam_DropDownList.CssClass = value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IrattariTetelszam_DropDownList.CssClass = "ViewReadOnlyWebControl";
                //LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IrattariTetelszam_DropDownList.CssClass = "ViewDisabledWebControl";
                //LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    public bool ReadOnly
    {
        get
        {
            return DropDownList.ReadOnly;
        }
        set
        {
            DropDownList.ReadOnly = value;
        }
    }

    /// <summary>
    /// Feltölti a DropDownList-et a megadott kódcsoporthoz tartozó (érvényes) kódtárértékekkel
    /// </summary>
    /// <param name="iktatoerkezteto"></param>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraIrattariTetelekSearch Search = new EREC_IraIrattariTetelekSearch();
        Search.OrderBy = "Merge_IrattariTetelszam";
        Result result = service.GetAllWithExtension(execParam, Search);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            IrattariTetelszam_DropDownList.Items.Clear();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                String irattariTetelNev = row["Merge_IrattariTetelszam"].ToString().PadRight(12,'_')+ " " + row["Nev"].ToString();
                //String irattariTetelNev = String.Format("{0}\t{1}", row["Merge_IrattariTetelszam"].ToString(), row["Nev"].ToString());  
                String irattariTetelId = row["Id"].ToString();
                IrattariTetelszam_DropDownList.Items.Add(new ListItem(irattariTetelNev, irattariTetelId));
            }
            if (addEmptyItem)
            {
                IrattariTetelszam_DropDownList.Items.Insert(0, emptyListItem);
            }
        }
    }

    public void FillDropDownList(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList( false, errorPanel);
    }

    //public void FillWithOneValue(string iktatoerkezteto, string iktatoId, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    if (!String.IsNullOrEmpty(iktatoId))
    //    {
    //        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
    //        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
    //        Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
    //        Search.IktatoErkezteto.Value = iktatoerkezteto;
    //        Result result = service.GetAll(execParam, Search);

    //        if (String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            Irattar_DropDownList.Items.Clear();
    //            if (result.Ds.Tables[0].Rows.Count > 0)
    //            {
    //                bool isEqualIktatokonyv = false;
    //                foreach (DataRow row in result.Ds.Tables[0].Rows)
    //                {
    //                    String iktatokonyvId = row["Id"].ToString();
    //                    if (iktatokonyvId == iktatoId)
    //                    {
    //                        String iktatokonyvNev = row["Nev"].ToString();
    //                        Irattar_DropDownList.Items.Add(new ListItem(iktatokonyvNev, iktatokonyvId));
    //                        isEqualIktatokonyv = true;
    //                        break;
    //                    }
    //                }
    //                if (!isEqualIktatokonyv)
    //                {
    //                    Irattar_DropDownList.Items.Insert(0, new ListItem(iktatoId + " " + deletedValue, iktatoId));
    //                }
    //            }
    //            else
    //            {
    //                Irattar_DropDownList.Items.Insert(0, new ListItem(iktatoId + " " + deletedValue, iktatoId));
    //            }
    //        }
    //        else
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
    //        }
    //    }
    //    else
    //    {
    //        Irattar_DropDownList.Items.Insert(0, emptyListItem);
    //    }

    //}

    /// <summary>
    /// Feltölti a listát, és beállítja az értékét a megadottra, ha az létezik.
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létező kódtárérték
    /// </summary>
    /// <param name = "iktatoerkezteto" ></ param >
    /// < param name= "selectedValue" ></ param >
    /// < param name= "addEmptyItem" ></ param >
    /// < param name= "errorPanel" ></ param >
    public void FillAndSetSelectedValue( String selectedValue, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList( addEmptyItem, errorPanel);
        ListItem selectedListItem = IrattariTetelszam_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            IrattariTetelszam_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            IrattariTetelszam_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }

    public void FillAndSetSelectedValue( String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(selectedValue, false, errorPanel);
    }

    public void FillAndSetEmptyValue(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(emptyListItem.Value, true, errorPanel);
    }


    /// <summary>
    /// Beállítja a már feltöltött lista értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létező kódtárérték
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ListItem selectedListItem = IrattariTetelszam_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            IrattariTetelszam_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            IrattariTetelszam_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }
    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IrattariTetelszam_DropDownList);
        // Lekell tiltani a ClientValidator
        //Validator1.Enabled = false;

        return componentList;
    }

    #endregion
    protected void IrattariTetelszam_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}