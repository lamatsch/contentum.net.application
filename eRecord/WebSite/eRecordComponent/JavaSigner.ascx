﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JavaSigner.ascx.cs" Inherits="eRecordComponent_JavaSigner" %>

<!-- Dialógus ablakok -->

<div id="divWarning" title="Hiányzó Java">
		Az oldal használatához a JAVA 1.6.0.10 ill. újabb verzió telepítése, engedélyezése szükséges.
		<br/><br/>
		Az oldal át lesz irányítva a Java letöltési oldalára. Miután telepítette a Java legfrissebb verzióját, kérjük zárja be az összes böngésző ablakot, majd indítsa újra a böngészőjét és töltse be újra ezt az oldalt.
		<br/><br/>
</div>

<div id="divError" title="Hiba"></div>

<div id="divResult" title="Aláírás base64 formátumban"></div>

<div id="divResultPDF" title="Aláírt PDF dokumentum URL-je"></div>

 <div id="UpdateProgressPanel" class="updateProgress" style="display:none;">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <table>
            <tr>
            <td>
                <img src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="" />
            </td>
            <td class="updateProgressText">
                <span id="updateProgressText">Feldolgozás folyamatban...</span>
            </td>
            </tr>
        </table>
   </eUI:eFormPanel> 
</div>

<div id="ProgressDialog" title="Aláírás folyamatban">
    	<div id="progressblock">Az aláírás folyamatban, kérjük, várjon!
			<div id="progressbar"></div>
        </div>		
        <div id="errorblock">
	  	<img class="imagealignleft" src="netlock/images/error.png" width="64" height="64" />            
            <p class="errorhint">Hiba történt az aláírás során. Hibaüzenet:</p>
            <div class="alignbottom">                
                <p id="errortext"></p>
            </div>		        
        </div>		        
</div>
	
<div id="SuccesDialog" title="Aláírás sikeresen elkészült">
    <div id="succesblock">
    	<img class="imagealignleft" src="netlock/images/success.png" width="64" height="64" />          
        <div>
        Az aláírás sikeresen elkészült!
        <br />
        Eredeti PDF dokumentum azonosító: <input type="text" id="SourceData" style="width: 200px;" value="" />
		<br />
		Aláírt fájl azonosító: <input type="text" id="signeddata" style="width: 200px;" value="" />
		</div>            
    </div>		        
</div>

<div id="JAVAErrorDialog" title="Az oldal használatához a JAVA 6 szükséges" style="display:none">    	
    <div id="javaerrorblock">
    	<img class="imagealignleft" src="netlock/images/error.png" width="64" height="64" />            
        <p class="errorhint">Az oldal használatához a <a href="http://www.java.com/en/download/index.jsp">JAVA 6 ill. újabb verzió telepítése</a>, engedélyezése szükséges.<br>Kívánja telepíteni a JAVA-t?</p>            
    </div>		        
    </div>
 
<div id="FlashErrorDialog" title="Az oldal használatához a Flash plugin szükséges" style="display:none">    	
    <div id="javaerrorblock">
    	<img class="imagealignleft" src="netlock/images/error.png" width="64" height="64" />            
        <p class="errorhint">A JAVA Form Signing használatához legalább 9.0.124-es verziójú <a href="http://get.adobe.com/flashplayer/">Flash plugin</a> telepítése és engedélyezése szükséges.</p>            
    </div>		        
    </div>
     
<div id="flash_not_installed" style="display:none">
	<h1>A JAVA Form Signing használatához legalább 9.0.124-es verziójú <a href="http://get.adobe.com/flashplayer/">Flash plugin</a> telepítése és engedélyezése szükséges.</h1>            
</div>
<div id="java_not_installed" style="display:none">
	<h1>Az oldal használatához a <a href="http://www.java.com/en/download/index.jsp">JAVA 6 ill. újabb verzió telepítése</a> és engedélyezése szükséges.</h1>            
</div>
<div id="java_not_enabled" style="display:none">
	<h1>A JAVA telepítve van, de nincs engedélyezve a böngészőjében.</h1>            
	<h2>Az oldal használatához a <a href="http://www.java.com/en/download/index.jsp">JAVA 6 ill. újabb verzió telepítése</a> és engedélyezése szükséges.</h2>            
</div>