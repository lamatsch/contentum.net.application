﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

public partial class eRecordComponent_JavaSigner : System.Web.UI.UserControl, IScriptControl
{
    #region properties

    public string SignButtonID
    {
        get;
        set;
    }

    public string ContentToSign
    {
        get;
        set;
    }

    private List<FileProperties> _FilesToSign = new List<FileProperties>();

    public List<FileProperties> FilesToSign
    {
        get { return _FilesToSign; }
        set { _FilesToSign = value; }
    }

    public string IratId
    {
        get;
        set;
    }

    public string AsyncPostBackUrl
    {
        get;
        set;
    }

    public string ProcId
    {
        get;
        set;
    }

    #endregion

    #region #events

    public event PDFSignedEventHandler PDFSigned
    {
        add
        {
            _PDFSigned += value;
        }
        remove
        {
            _PDFSigned -= value;
        }
    }


    #endregion

    #region PDFSignedEvent

    public class PDFSignedEventArgs : EventArgs
    {
        public FileProperties SignedFile { get; set; }

        public PDFSignedEventArgs(FileProperties signedFile)
        {
            this.SignedFile = signedFile;
        }
    }

    public delegate void PDFSignedEventHandler(object sender, PDFSignedEventArgs e);

    public event PDFSignedEventHandler _PDFSigned;

    #endregion

    [Serializable]
    public class FileProperties
    {
        public string FileName {get; set;}
        public string FileUrl {get; set;}
        public string SignedFileUrl { get; set; }
        public string IratId { get; set; }
        public string ProcId { get; set; }
        public string DokumentumId { get; set; }
        public string CsatolmanyId { get; set; }
        public bool IsLastFile { get; set; }
        public string Error { get; set; }
    }

    string SignButtonClientID
    {
        get
        {
            return this.Parent.FindControl(this.SignButtonID).ClientID;
        }
    }

    private string NlServerPostURL
    {
        get
        {
            return WebConfigurationManager.AppSettings["NlServerPostURL"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            string eventTarget = Request.Params["__EVENTTARGET"];
            string eventArgument = Request.Params["__EVENTARGUMENT"];

            if (this.UniqueID.Equals(eventTarget))
            {
                OnPDFSigned(eventArgument);
            }
        }
    }

    void OnPDFSigned(string signedFile)
    {
        PDFSignedEventHandler handler = _PDFSigned;

        if (handler != null)
        {
            FileProperties file = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<FileProperties>(signedFile);
            _PDFSigned(this, new PDFSignedEventArgs(file));
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        AddCss("~/netlock/css/smoothness/jquery-ui-1.10.1.custom.min.css");
        AddCss("~/netlock/css/yui/logger.css");
        AddCss("~/netlock/css/NlJavaSigner.css");
    }

    public void AddCss(string path)
    {
        Literal cssFile = new Literal() { Text = @"<link href=""" + this.ResolveClientUrl(path) + @""" type=""text/css"" rel=""stylesheet"" />" };
        this.Page.Header.Controls.Add(cssFile);
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Netlock.JavaSignerBehavior", SignButtonClientID);

        descriptor.AddProperty("NlServerPostURL", this.NlServerPostURL);
        descriptor.AddProperty("ContentToSign", this.ContentToSign);
        descriptor.AddProperty("AsyncPostBackUrl", this.AsyncPostBackUrl);
        descriptor.AddProperty("IratId", this.IratId);

        if (this.FilesToSign != null && this.FilesToSign.Count > 0)
        {
            descriptor.AddProperty("FilesToSign", this.FilesToSign);
        }

        descriptor.AddProperty("ProcId", this.ProcId);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        List<ScriptReference> references = new List<ScriptReference>()
        {
            new ScriptReference("~/netlock/js/deployJava.js"),
            new ScriptReference("~/netlock/js/swfobject.js"),
            new ScriptReference("~/netlock/js/jquery-1.9.1.min.js"),
            new ScriptReference("~/netlock/js/jquery-ui-1.10.1.custom.min.js"),
            new ScriptReference("~/netlock/js/yui/yui_combined-2.9-min.js"),
            new ScriptReference("~/netlock/js/date.format.js"),
            new ScriptReference("~/netlock/js/JavaSignerBehavior.js")

        };

        return references;
    }

    #endregion

}