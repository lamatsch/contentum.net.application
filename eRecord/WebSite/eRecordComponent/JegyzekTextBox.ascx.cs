using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Text;

public partial class eRecordComponent_JegyzekTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, UI.ILovListTextBox
{
    #region public properties

    public string JegyzekTipus
    {
        get { return ViewState["JegyzekTipus"] as string; }
        set { ViewState["JegyzekTipus"] = value;  }
    }

    private string startup;

    public string StartUp
    {
        get { return startup; }
        set { startup = value; }
    }
	

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            txtJegyzekNev.ValidationGroup = value;
        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { txtJegyzekNev.Text = value; }
        get { return txtJegyzekNev.Text; }
    }

    public TextBox TextBox
    {
        get { return txtJegyzekNev; }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }

    public bool Enabled
    {
        set
        {
            txtJegyzekNev.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return txtJegyzekNev.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            txtJegyzekNev.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return txtJegyzekNev.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                txtJegyzekNev.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                txtJegyzekNev.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

       /// <param name="errorPanel"></param>
    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        SetTextBoxById(errorPanel, null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraJegyzekek jegyzek = (EREC_IraJegyzekek)result.Record;
                if (jegyzek == null)
                {
                    Text = ""; // ha nem talal recordot
                    JegyzekTipus = null;
                }
                else
                {
                    Text = jegyzek.Nev;
                    JegyzekTipus = jegyzek.Tipus;
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
            JegyzekTipus = null;
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, CsoportMegnevezes.ClientID);   
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string query = String.Empty;
        if (!String.IsNullOrEmpty(startup))
        {
            query = "&" + Constants.Startup.StartupName + "=" + startup;
            //if (startup == Constants.Startup.FromSelejtezes)
            //{
            //    query = "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromSelejtezes;
            //}
            //else if (startup == Constants.Startup.FromLeveltarbaAdas)
            //{
            //    query = "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromLeveltarbaAdas;
            //}
            //else if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
            //{
            //    query = "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromEgyebSzervezetnekAtadas;
            //}
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("IraJegyzekekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + txtJegyzekNev.ClientID + query
                    , 900, 650, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("IraJegyzekekForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + txtJegyzekNev.ClientID + query
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "IraJegyzekekForm.aspx", "", HiddenField1.ClientID);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(txtJegyzekNev);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}