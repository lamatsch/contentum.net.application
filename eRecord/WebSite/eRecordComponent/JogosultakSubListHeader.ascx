<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JogosultakSubListHeader.ascx.cs"
    Inherits="Component_JogosultakSubListHeader" %>
<table class="kUrlap" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="text-align: left; height:0px; vertical-align: top;">
                        <!-- A kiv�lasztand� record Id-j�nak t�rol�sa, updatepanel-en bel�l legyen -->
                        <asp:HiddenField ID="selectedRecordId" runat="server" />
                        <table style="width:292px" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td>
                                        <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"  ID="ImageNew" OnClick="Buttons_OnClick" CommandName="New"
                                             AlternateText="M�dos�t�s" onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')" />
                                    </td>
                                    <td>
                                        <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/trapezgomb/megtekintes_trap.jpg"  ID="ImageView" OnClick="Buttons_OnClick" CommandName="View"
                                             AlternateText="Megtekint�s" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')"/>
                                    </td>
                                    <td>
                                        <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/trapezgomb/torles_trap.jpg" ID="ImageDelete" OnClick="Buttons_OnClick" CommandName="Invalidate"
                                             AlternateText="T�rl�s" onmouseover="swapByName(this.id,'torles_trap2.jpg')" onmouseout="swapByName(this.id,'torles_trap.jpg')"/>
                                    </td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align: right; vertical-align: top; height:0px; width:100%;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td style="width:100%">
                                </td>
                                <td style="text-align: right;white-space:nowrap;padding-right:20px;" class="sublistHeaderElement">
                                    <asp:TextBox ID="RowCountTextBox" runat="server" AutoPostBack="True" MaxLength="4" OnTextChanged="RowCountTextBox_TextChanged" Text="" ToolTip="Oldalank�nti rekordok sz�ma" Width="30" CssClass="sublistTextBox"></asp:TextBox>
                                    <asp:CheckBox ID="ScrollableCheckBox" runat="server" AutoPostBack="true" Checked="false" OnCheckedChanged="ScrollableCheckBox_CheckedChanged" ToolTip="Scrollozhat� lista" />
                                    <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server"
                                    TargetControlID="ScrollableCheckBox" UncheckedImageUrl="../images/hu/ikon/scrolling_off.jpg" UncheckedImageAlternateText="Scrollozhat�s�g bekapcsol�sa"
                                    CheckedImageUrl="../images/hu/ikon/scrolling_on.jpg" CheckedImageAlternateText="Scrollozhat�s�g kikapcsol�sa" ImageHeight="24" ImageWidth="26">
                                    </ajaxToolkit:ToggleButtonExtender>
                                </td>
                                <td valign="top" style="padding-right:10px;">
                                    <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/egyeb/rewall.jpg" id="rewall" onmouseover="swapByName(this.id,'rewall2.jpg')" onmouseout="swapByName(this.id,'rewall.jpg')" OnClick="ImageButton1_Click" CommandName="First" AlternateText="Els� oldal"/>
                                </td>
                                <td valign="top">
                                    <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/egyeb/rew.jpg" id="rew" onmouseover="swapByName(this.id,'rew2.jpg')" onmouseout="swapByName(this.id,'rew.jpg')" OnClick="ImageButton1_Click" CommandName="Prev" AlternateText="El�z� oldal"/>                                        
                                </td>
                                <td valign="top" align="center" class="tlSzamlalo">
                                    <div style="height:2px"></div>
                                    <span style="white-space:nowrap">
                                        <asp:Label ID="Pager" runat="server" Text="Pager"></asp:Label>
                                        <asp:HiddenField ID="PageIndex_HiddenField" runat="server" />
                                        <asp:HiddenField ID="PageCount_HiddenField" runat="server" />
                                        <asp:Label ID="labelRecordNumber" runat="server" Text="(0)"></asp:Label>
                                    </span>     
                                </td>
                                <td valign="top" style="padding-right:10px;">
                                     <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/egyeb/ffw.jpg" id="ffw" onmouseover="swapByName(this.id,'ffw2.jpg')" onmouseout="swapByName(this.id,'ffw.jpg')" OnClick="ImageButton1_Click" CommandName="Next" AlternateText="K�vetkez� oldal"/>
                                        </td>
                                <td valign="top">
                                    <asp:ImageButton TabIndex = "-1" runat="server" ImageUrl="../images/hu/egyeb/ffwd.jpg" id="ffwd" onmouseover="swapByName(this.id,'ffwd2.jpg')" onmouseout="swapByName(this.id,'ffwd.jpg')" OnClick="ImageButton1_Click" CommandName="Last" AlternateText="Utols� oldal"/>                                        
                                        </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
