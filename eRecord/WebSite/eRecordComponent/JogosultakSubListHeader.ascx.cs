using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_JogosultakSubListHeader : System.Web.UI.UserControl
{
    //private static int _PageIndex = 0;
    //private static int _PageCount = 0;

    #region Pager Public Property
    public int PageIndex
    {
        get
        {
            try
            {
                return Int32.Parse(PageIndex_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }
        set
        {
            PageIndex_HiddenField.Value = value.ToString();
        }
    }
    public int PageCount
    {
        get
        {
            try
            {
                return Int32.Parse(PageCount_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 1;
            }
        }
        set
        {
            PageCount_HiddenField.Value = value.ToString();
        }
    }

    #region SelectedRecord properties

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(selectedRecordId.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedRecordId.Value;
            }
        }
        set
        {
            selectedRecordId.Value = value;
        }
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
    }

    private GridView attachedGridView = null;

    public GridView AttachedGridView
    {
        get { return attachedGridView; }
        set
        {
            if (attachedGridView != null)
            {
                attachedGridView.SelectedIndexChanged -= attachedGridView_SelectedIndexChanged;
            }
            attachedGridView = value;
            attachedGridView.SelectedIndexChanged += new EventHandler(attachedGridView_SelectedIndexChanged);
        }
    }

    protected void attachedGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (attachedGridView.SelectedValue != null)
            SelectedRecordId = attachedGridView.SelectedValue.ToString();
    }

    public string AttachedGridViewId
    {
        get
        {
            if (attachedGridView != null)
                return attachedGridView.ID;
            else
                return String.Empty;
        }
    }

    #endregion

    public String PagerLabel
    {
        get { return Pager.Text; }
        set { Pager.Text = value; }
    }

    public bool Scrollable
    {
        get { return ScrollableCheckBox.Checked; }
        //set { _PageIndex = value; }
    }

    public string RowCount
    {
        get { return RowCountTextBox.Text; }
        set { RowCountTextBox.Text = value; }
    }

    public int RecordNumber
    {
        get
        {
            string text = labelRecordNumber.Text;
            int number = 0;
            if (text.StartsWith("(") && text.EndsWith(")"))
            {
                Int32.TryParse(text.Substring(1, text.Length - 2), out number);
            }
            return number;
        }
        set { labelRecordNumber.Text = String.Format("({0})", value); }
    }

    #endregion

    #region New Button Public Properties
    public String NewOnClientClick
    {
        get { return ImageNew.OnClientClick; }
        set { ImageNew.OnClientClick = value; }
    }
    public bool NewEnabled
    {
        get { return ImageNew.Enabled; }
        set
        {
            ImageNew.Enabled = value;
            if (value == false)
            {
                ImageNew.CssClass = "disableditem";
            }
        }
    }
    public bool NewVisible
    {
        get { return ImageNew.Visible; }
        set { ImageNew.Visible = value; }
    }
    #endregion

    #region View Button Public Properties
    public String ViewOnClientClick
    {
        get { return ImageView.OnClientClick; }
        set { ImageView.OnClientClick = value; }
    }
    public bool ViewEnabled
    {
        get { return ImageView.Enabled; }
        set
        {
            ImageView.Enabled = value;
            if (value == false)
            {
                ImageView.CssClass = "disableditem";
            }
        }
    }
    public bool ViewVisible
    {
        get { return ImageView.Visible; }
        set { ImageView.Visible = value; }
    }
    #endregion

    #region Delete Button Public Properties
    public String DeleteOnClientClick
    {
        get { return ImageDelete.OnClientClick; }
        set { ImageDelete.OnClientClick = value; }
    }
    public bool DeleteEnabled
    {
        get { return ImageDelete.Enabled; }
        set
        {
            ImageDelete.Enabled = value;
            if (value == false)
            {
                ImageDelete.CssClass = "disableditem";
            }
        }
    }
    public bool DeleteVisible
    {
        get { return ImageDelete.Visible; }
        set { ImageDelete.Visible = value; }
    }
    #endregion


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //lapoz�s eset�n kiv�laszt�s t�rl�se
        switch ((sender as ImageButton).CommandName.ToString())
        {
            case "First":
                if (PageIndex != 0)
                {
                    PageIndex = 0;
                    ForgetSelectedRecord();
                }
                break;
            case "Prev":
                if (PageIndex - 1 >= 0)
                {
                    PageIndex = PageIndex - 1;
                    ForgetSelectedRecord();
                }
                break;
            case "Next":
                if (PageIndex + 1 < PageCount)
                {
                    PageIndex = PageIndex + 1;
                    ForgetSelectedRecord();
                }
                break;
            case "Last":
                if (PageIndex != PageCount - 1)
                {
                    PageIndex = PageCount - 1;
                    ForgetSelectedRecord();
                }
                break;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UI.SwapImageToDisabled(ImageNew);
        UI.SwapImageToDisabled(ImageView);
        UI.SwapImageToDisabled(ImageDelete);

        if (Session[Constants.DetailRowCount] == null)
        {
            Session[Constants.DetailRowCount] = RowCountTextBox.Text;
        }

        if (!IsPostBack)
        {
            if (Page.Session[Constants.DetailGridViewScrollable] != null)
                ScrollableCheckBox.Checked = (bool)Page.Session[Constants.DetailGridViewScrollable];

            if (Session[Constants.DetailRowCount] != null)
            {
                RowCountTextBox.Text = Session[Constants.DetailRowCount].ToString();
            }
        }

        //rendez�s eset�n selectedRecordId t�rl�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (eventArgument.Split('$')[0].ToLower() == "sort")
            {
                if (Request.Params["__EVENTTARGET"] != null)
                {
                    string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                    string[] list = eventTarget.Split('$');
                    if (AttachedGridView != null && list[list.Length - 1] == AttachedGridViewId)
                    {
                        ForgetSelectedRecord();
                    }
                }
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Ha t�r�lve lett a gridView a SelectedRecordId mez�t is t�r�lni kell
        if (attachedGridView.Rows.Count == 0)
            ForgetSelectedRecord();
    }

    public event CommandEventHandler ButtonsClick;

    public virtual void Buttons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    protected void ScrollableCheckBox_CheckedChanged(object sender, EventArgs e)
    {

    }

    public event EventHandler RowCount_Changed;

    protected void RowCountTextBox_TextChanged(object sender, EventArgs e)
    {
        Session[Constants.DetailRowCount] = RowCount;
        if (RowCount_Changed != null)
        {
            RowCount_Changed(sender, e);
        }
    }

}
