<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JogosultakTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratJogosultakTab" %>
    <%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<!--CR3189 - Jogosultakon is legyen kisherceg-->
<%@ Register Src="../Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc6" %>
<!--CR3189 - Jogosultakon is legyen kisherceg-->

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="JogosultakUpdatePanel" runat="server" OnLoad="JogosultakUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <ajaxToolkit:CollapsiblePanelExtender ID="JogosultakCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" AutoCollapse="false" AutoExpand="false"
                ExpandedSize="200" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <%-- TabHeader --%>
                        <uc1:TabHeader ID="TabHeader1" runat="server" />
                        <%-- /TabHeader --%>

                        <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                        <uc6:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="true"></uc6:FormTemplateLoader> 
                        <%-- CR3189 - Jogosultakon is legyen kisherceg--%>

                        <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
                            <asp:GridView ID="JogosultakGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                                OnRowCommand="JogosultakGridView_RowCommand" OnRowDataBound="JogosultakGridView_RowDataBound"
                                OnSorting="JogosultakGridView_Sorting">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                CssClass="HideCheckBoxText" />
                                            <%--'<%# Eval("Id") %>'--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                    </asp:CommandField>
                                    <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                        <ItemTemplate>                                    
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                                    <asp:BoundField DataField="Nev" HeaderText="Jogosult csoport" SortExpression="Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                     <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                                    <asp:BoundField DataField="Csoport_Id_Jogalany" HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle" >
                                    </asp:BoundField>
                                     <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                                    <asp:BoundField DataField="Jogszint" HeaderText="Jogszint" SortExpression="Jogszint">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Kezi" HeaderText="K�zi" SortExpression="Kezi" Visible="false">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ErvKezd" HeaderText="Kezd" SortExpression="ErvKezd">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ErvVege" HeaderText="V�ge" SortExpression="ErvVege">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                             <%-- CR3189 - Jogosultakon is legyen kisherceg--%>
                             <asp:ImageButton ID="TemplatesSave" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/rendben.jpg" Visible="false" OnClick="TemplatesSave_OnClick"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" />
                              <%-- CR3189 - Jogosultakon is legyen kisherceg--%>

                              <%-- CR3204 - M�gse gomb az al��r�k �s jogosultak kisherceg�hez.--%>
                             <asp:ImageButton ID="TemplatesDelete" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/megsem.jpg" Visible="false" OnClick="TemplatesDelete_OnClick"
                                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')" />
                             <%-- CR3204 - M�gse gomb az al��r�k �s jogosultak kisherceg�hez.--%>
                        </asp:Panel>
                        <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label_Csoport_Jogosult" runat="server" Text="Jogosult:"></asp:Label>&nbsp;
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CsoportTextBox ID="Csoport_Id_JogosultTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr id="tr_Jogszint" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label_Jogszint" runat="server" Text="Jogszint:"></asp:Label>&nbsp;
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="DropDownListJogszint" runat="server" />
                            </td>
                            
                        </tr>

                            </table>
                            <%-- TabFooter --%>
                            <uc2:TabFooter ID="TabFooter1" runat="server" />
                            <%-- /TabFooter --%>
                        </eUI:eFormPanel>
                        <asp:Panel ID="CsatolmanyJogosultakPanel" runat="server" style="padding-top: 5px;overflow-y:auto; max-height:200px;">
                            <div style="padding-bottom: 5px;">
                                <span style="font-weight:bold; text-decoration: underline">Sz�rmaztatott jogosults�gok:</span>
                            </div>
                            <asp:GridView ID="CsatolmanyJogosultakGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" Width="200px"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserNev" HeaderText="Felhaszn�l�n�v" SortExpression="UserNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBorder" Width="200px" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
