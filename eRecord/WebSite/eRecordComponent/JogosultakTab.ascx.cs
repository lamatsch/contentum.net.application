using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_UgyiratJogosultakTab : System.Web.UI.UserControl
{
    // use defines if needed
    //private const bool showJogszintField = false; // ha true, a jogszint be�ll�that�, ha false, akkor alap�rtelmezetten 'I' (�r�s)
    //private const bool isModifyVisible = showJogszintField; // mivel csak a jogszint m�dos�that�, ha az nem l�tszik, a m�dos�t�s �rtelmetlen
    //private const bool showOrokoltRows = false; // ha true, az Orokolt="I" sorok sz�rk�tve l�tszanak, ha false, az ilyen sorokat le sem k�rj�k

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        DropDownListJogszint.Items.Add(new ListItem("�r�s","I"));
        #if showJogszintField
        DropDownListJogszint.Items.Add(new ListItem("Olvas�s", "O"));
        #endif

        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

//        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        EFormPanel1.Visible = false;
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;
        TabHeader1.ViewVisible = false;

#region CR3189 - Jogosultakon is legyen kisherceg..
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
        FormTemplateLoader1.ErrorPanel = EErrorPanel1;
        CustomTemplateTipusNev = "JogosultakTemplate";
        FormTemplateLoader1.SearchObjectType = _type;
#endregion CR3189 - Jogosultakon is legyen kisherceg..

        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        //TabFooter1.SaveValidationGroup = "Jogosultak";
        // TODO:
        //UploadJogosult.ValidationGroup = TabFooter1.SaveValidationGroup;
        //JogosultMegjegyzes.ValidationGroup = TabFooter1.SaveValidationGroup;
        //CsatlomanyVerzio.ValidationGroup = TabFooter1.SaveValidationGroup;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        //TabHeader1.NewEnabled = Command == CommandName.Modify; 

        //TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.New);
        //TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.View);
        //TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Modify);
        //TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Invalidate);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.ElosztoLista);

        // Jogszint elt�ntet�se
        #if !showJogszintField
        int index = UI.GetGridViewColumnIndex(JogosultakGridView, "Jogszint");
        if (index >= 0 && index < JogosultakGridView.Columns.Count)
        {
            JogosultakGridView.Columns[index].Visible = false;
        }

        tr_Jogszint.Visible = false;
        #endif

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(JogosultakGridView));
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            JogosultakGridViewBind();
#region CR3189 - Jogosultakon is legyen kisherceg
            TemplatesSave.Visible = false;
            TemplatesDelete.Visible = false;
#endregion CR3189 - Jogosultakon is legyen kisherceg
        }
    }

    protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (ParentForm != Constants.ParentForms.IraIrat
            && ParentForm != Constants.ParentForms.Kuldemeny
            && ParentForm != Constants.ParentForms.Ugyirat
            && ParentForm != Constants.ParentForms.IratPeldany)
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
            return;
        }

        if (!MainPanel.Visible) return;

        // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;
        TabHeader1.ViewVisible = false;

#if showJogszintField
        TabHeader1.ModifyVisible = true;
#else
        TabHeader1.ModifyVisible = false;
#endif

        TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JogosultakGridView.ClientID);
        //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ClearForm();
        EFormPanel1.Visible = false;

        RefreshJogosultakFromUcm();

        JogosultakGridViewBind();
        pageView.SetViewOnPage(Command);
#region CR3189 - Jogosultakon is legyen kisherceg
        TemplatesSave.Visible = false;
        TemplatesDelete.Visible = false;
#endregion CR3189 - Jogosultakon is legyen kisherceg

        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            CsatolmanyJogosultakPanel.Visible = true;
            CsatolmanyJogosultakGridViewBind();
        }
        else
        {
            CsatolmanyJogosultakPanel.Visible = false;
        }
    }

#region CR3189 - Jogosultakon is legyen kisherceg

    private Type _type = typeof(DataTable);
    public object TemplateObject
    {
        get { return FormTemplateLoader1.SearchObject; }
        set { FormTemplateLoader1.SearchObject = value; }
    }

    public Type TemplateObjectType
    {
        get { return FormTemplateLoader1.SearchObjectType; }
        set { FormTemplateLoader1.SearchObjectType = value; }
    }

    public string CustomTemplateTipusNev
    {
        get { return FormTemplateLoader1.CustomTemplateTipusNev; }
        set { FormTemplateLoader1.CustomTemplateTipusNev = value; }
    }

    public string CurrentTemplateName
    {
        get { return FormTemplateLoader1.CurrentTemplateName; }
        set { FormTemplateLoader1.CurrentTemplateName = value; }
    }

    public string CurrentTemplateId
    {
        get { return FormTemplateLoader1.CurrentTemplateId; }
        set { FormTemplateLoader1.CurrentTemplateId = value; }
    }

    /// <summary>
    /// �j template l�trehoz�sa
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void NewTemplate(object newSearchObject)
    {
        FormTemplateLoader1.NewTemplate(newSearchObject);
    }

    /// <summary>
    /// Aktu�lis template elment�se
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void SaveCurrentTemplate(object newSearchObject)
    {
        FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    }

    public void TemplateReset()
    {
        FormTemplateLoader1.TemplateReset();
    }

    //public event CommandEventHandler ButtonsClick;

    protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            SetGridViewRowsFromTemplate(TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            NewTemplate(GetGridViewRowsToTemplate());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SaveCurrentTemplate(GetGridViewRowsToTemplate());
        }
        //else if (e.CommandName == CommandName.InvalidateTemplate)
        //{
        //    // FormTemplateLoader lekezeli           
        //}
        ErrorUpdatePanel1.Update();
    }
#region Template bet�lt�s a gridview-ba
    private void SetGridViewRowsFromTemplate(object TemplateObject)
    {

        // ha nincs megadva a ParentId, nem tudunk sz�rni --> kil�p�s
        if (String.IsNullOrEmpty(ParentId))
        {
            return;
        }

        UI.ClearGridViewRowSelection(JogosultakGridView);

        //CR3227 - �gyirat oldalr�l a Jogosultak f�l�n nem m�k�dik a kisherceg.
        if (ParentForm == Constants.ParentForms.IraIrat || ParentForm == Constants.ParentForms.Ugyirat)
        {
            String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Id");
            SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
            search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, sortExpression, sortDirection);

            string Id = Request.QueryString.Get("Id");
            if (string.IsNullOrEmpty(Id)) return;

#if !showOrokoltRows
            // Orokolt = 'I' sorok kisz�r�se
            search.WhereByManual = String.Format(" KRT_Jogosultak.Obj_Id='{0}'", Id);
#endif

            Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, Id, search);

            DataTable dt = GetDataTableFromTemplate(TemplateObject);

            foreach (DataRow row in dt.Rows)
            {
                DataRow dr = res.Ds.Tables[0].NewRow();

                foreach (DataColumn col in res.Ds.Tables[0].Columns)
                {
                    if (dt.Columns.Contains(col.ColumnName))
                        dr[col.ColumnName] = row[col.ColumnName];
                }
                res.Ds.Tables[0].Rows.Add(dr);
            }

            ui.GridViewFill(JogosultakGridView, res, EErrorPanel1, ErrorUpdatePanel1);
            UI.SetTabHeaderRowCountText(res, TabHeader);
        }
        TemplatesSave.Visible = true;
        TemplatesDelete.Visible = true;
    }

    private DataTable GetDataTableFromTemplate(object TemplateObject)
    {
        DataTable dt = new DataTable();
        if (TemplateObject is DataTable)
            dt = (TemplateObject as DataTable);

        return dt;
    }
#endregion Template bet�lt�s a gridview-ba
#region a gridview-ba bet�lt�tt template rekordok esem�nyei
    //create checked template rows - delete template rows = postback?
    protected void TemplatesSave_OnClick(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = GetGridViewRowsToDatabase();
            SaveBusinessObjectsFromDataTable(dt);
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "hiba", ex.Message);
            ErrorUpdatePanel1.Update();
        }
    }
    protected void TemplatesDelete_OnClick(object sender, EventArgs e)
    {
        try
        {
            JogosultakGridViewBind();
            TemplatesDelete.Visible = false;
            TemplatesSave.Visible = false;
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "hiba", ex.Message);
            ErrorUpdatePanel1.Update();
        }
    }
#endregion a gridview-ba bet�lt�tt template rekordok esem�nyei

#region A gridview template sorok lek�r�se db-be ment�shez
    private DataTable GetGridViewRowsToDatabase()
    {
        DataTable _resultTable = new DataTable("JogosultakGridView");

        foreach (DataControlFieldCell cell in JogosultakGridView.HeaderRow.Cells)
        {
            int cellIndex = 0;
            if (cell.ContainingField.HeaderStyle.CssClass == "GridViewCheckBoxTemplateHeaderStyle")
            {
                if (cellIndex == 0)
                    _resultTable.Columns.Add("Id");
                ++cellIndex;
                continue;
            }
            if (cell.ContainingField is BoundField && ((cell.ContainingField as BoundField).DataField.Equals("ErvKezd") || (cell.ContainingField as BoundField).DataField.Equals("ErvVege"))) continue;
            else if (cell.ContainingField is BoundField)
            {
                _resultTable.Columns.Add((cell.ContainingField as BoundField).DataField);
            }
            else
                if (cell.ContainingField is TemplateField)
                {
                    if (!String.IsNullOrEmpty(cell.ContainingField.SortExpression))
                    {
                        _resultTable.Columns.Add(cell.ContainingField.SortExpression);
                    }
                }
            cellIndex++;
        }

        foreach (GridViewRow rowItem in JogosultakGridView.Rows)
        {
            DataRow _resultRowItem = _resultTable.NewRow();
            if (rowItem.Cells[0].Controls[1].GetType() == typeof(CheckBox) && (!string.IsNullOrEmpty((rowItem.Cells[0].Controls[1] as CheckBox).Text) || !(rowItem.Cells[0].Controls[1] as CheckBox).Checked))
            {
                //if (!(rowItem.Cells[0].Controls[1] as CheckBox).Checked)
                    //{
                    continue;
                //}
            }
            else
            {
                _resultRowItem["Id"] = "";
            }
            foreach (DataControlFieldCell cell in rowItem.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    if (_resultTable.Columns.Contains((cell.ContainingField as BoundField).DataField))
                    {
                        _resultRowItem[(cell.ContainingField as BoundField).DataField] = HttpUtility.HtmlDecode(cell.Text);
                    }
                }
                else
                    if (cell.ContainingField is TemplateField && cell.Controls.Count != 0)
                    {
                        string controlString = "";
                        for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                        {
                            if (cell.Controls[controlIndex].GetType() == typeof(Label))
                            {
                                controlString += Contentum.eUtility.TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                            }
                           
                            if (cell.Controls[controlIndex].GetType() == typeof(CheckBox))
                            {
                                controlString += (rowItem.Cells[0].Controls[1] as CheckBox).Text;
                            }
                            
                        }
                        if (!String.IsNullOrEmpty(controlString))
                        {
                            if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                            {
                                _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                            }
                            else if (_resultTable.Columns.Contains(cell.ContainingField.SortExpression))
                            {
                                _resultRowItem[cell.ContainingField.SortExpression] = controlString;
                            }
                        }
                    }
            }
            _resultTable.Rows.Add(_resultRowItem);
        }
        return _resultTable;
    }
#endregion A gridview template sorok lek�r�se db-be ment�shez

#region A kijel�lt template sorok l�trehoz�sa
    // datatable --> business object
    private void SaveBusinessObjectsFromDataTable(DataTable dt)
    {
        using (RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService())
        {
            foreach (DataRow row in dt.Rows)
            {
                KRT_Jogosultak krt_Jogosultak = new KRT_Jogosultak();
                krt_Jogosultak.Updated.SetValueAll(false);
                krt_Jogosultak.Base.Updated.SetValueAll(false);

                krt_Jogosultak.Obj_Id = ParentId;
                krt_Jogosultak.Updated.Obj_Id = true;


                krt_Jogosultak.Csoport_Id_Jogalany = row["Csoport_Id_Jogalany"].ToString();
                krt_Jogosultak.Updated.Csoport_Id_Jogalany = true;

                krt_Jogosultak.Jogszint = row["Jogszint"].ToString();
                krt_Jogosultak.Updated.Jogszint = true;

                krt_Jogosultak.Kezi = "I";
                krt_Jogosultak.Updated.Kezi = true;

                krt_Jogosultak.Base.Ver = "1";
                krt_Jogosultak.Base.Updated.Ver = true;

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                // Request.QueryString.Get("Id"), krt_jogosultak.Csoport_Id_Jogalany,'I',DropDownListJogszint.SelectedValue.ToCharArray()[0],'0');
                Result result = service.AddCsoportToJogtargy(execParam, ParentId, krt_Jogosultak.Csoport_Id_Jogalany, krt_Jogosultak.Kezi[0]);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    ReLoadTab();
                    //RaiseEvent_OnChangedObjectProperties();
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }

            }
        }

        return;
    }
#endregion A kijel�lt template sorok l�trehoz�sa

#region A gridview sorok lek�r�se db-be ment�shez (sablon ment�se)
    private DataTable GetGridViewRowsToTemplate()
    {
        DataTable _resultTable = new DataTable("JogosultakGridView");

        foreach (DataControlFieldCell cell in JogosultakGridView.HeaderRow.Cells)
        {
            int cellIndex = 0;
            if (cell.ContainingField is BoundField && ((cell.ContainingField as BoundField).DataField.Equals("ErvKezd") || (cell.ContainingField as BoundField).DataField.Equals("ErvVege"))) continue;
            else if (cell.ContainingField is BoundField)
            {
                _resultTable.Columns.Add((cell.ContainingField as BoundField).DataField);
            }
            else
                if (cell.ContainingField is TemplateField)
                {
                    if (!String.IsNullOrEmpty(cell.ContainingField.SortExpression))
                    {
                        _resultTable.Columns.Add(cell.ContainingField.SortExpression);
                    }
                }
            cellIndex++;
        }

        foreach (GridViewRow rowItem in JogosultakGridView.Rows)
        {
            DataRow _resultRowItem = _resultTable.NewRow();
            if (rowItem.Cells[0].Controls[1].GetType() == typeof(CheckBox) && string.IsNullOrEmpty((rowItem.Cells[0].Controls[1] as CheckBox).Text))
            {
                continue;
            }
            foreach (DataControlFieldCell cell in rowItem.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    if (_resultTable.Columns.Contains((cell.ContainingField as BoundField).DataField) && (cell.ContainingField as BoundField).DataField.Equals("Kezi"))
                    {
                        _resultRowItem[(cell.ContainingField as BoundField).DataField] = 'I';
                    }
                    else if (_resultTable.Columns.Contains((cell.ContainingField as BoundField).DataField))
                    {
                        _resultRowItem[(cell.ContainingField as BoundField).DataField] = HttpUtility.HtmlDecode(cell.Text);
                    }
                }
                else
                    if (cell.ContainingField is TemplateField && cell.Controls.Count != 0)
                    {
                        string controlString = "";
                        for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                        {
                            if (cell.Controls[controlIndex].GetType() == typeof(Label))
                            {
                                controlString += Contentum.eUtility.TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                            }
                            if (cell.Controls[controlIndex].GetType() == typeof(System.Web.UI.WebControls.Image) || cell.Controls[controlIndex].GetType().Name == "CustomFunctionImageButton")
                            {
                                if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                                {
                                    controlString = cell.ContainingField.HeaderText;
                                }
                            }
                            
                        }
                        if (!String.IsNullOrEmpty(controlString))
                        {
                            if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                            {
                                _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                            }
                            else if (_resultTable.Columns.Contains(cell.ContainingField.SortExpression))
                            {
                                _resultRowItem[cell.ContainingField.SortExpression] = controlString;
                            }
                        }
                    }
            }
            _resultTable.Rows.Add(_resultRowItem);
        }
        return _resultTable;
    }
#endregion A gridview sorok lek�r�se db-be ment�shez (sablon ment�se)

#endregion CR3189 - Jogosultakon is legyen kisherceg
    
    
    private void TabHeaderButtonsClick(object sender, CommandEventArgs e)
    {
        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            Csoport_Id_JogosultTextBox.Text = "";
            DropDownListJogszint.SelectedIndex = 0;
            Csoport_Id_JogosultTextBox.Enabled = true;
            EFormPanel1.Visible = true;
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedJogosultak();
            JogosultakGridViewBind();
        }

        if (e.CommandName == CommandName.Modify)
        {
            KRT_JogosultakService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_JogosultakService();
            ExecParam ep = new ExecParam();
            ep.Record_Id = UI.GetGridViewSelectedRecordId(JogosultakGridView);
            KRT_Jogosultak krt_jogosultak = (KRT_Jogosultak)service.Get(ep).Record;

            Csoport_Id_JogosultTextBox.Id_HiddenField = krt_jogosultak.Csoport_Id_Jogalany;
            Record_Ver_HiddenField.Value = krt_jogosultak.Base.Ver;

            Csoport_Id_JogosultTextBox.SetCsoportTextBoxById(EErrorPanel1);

            Csoport_Id_JogosultTextBox.Enabled = false;
            EFormPanel1.Visible = true;
        }
    }

    private void TabFooterButtonsClick(object sender, CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            switch (SubCommand)
            {
                case CommandName.Cancel:
                    EFormPanel1.Visible = false;
                    break;
                case CommandName.New:
                    {
                        KRT_Jogosultak krt_jogosultak = GetBusinessObjectFromComponents();
                        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result result = service.AddCsoportToJogtargyWithJogszint(execParam, Request.QueryString.Get("Id"), krt_jogosultak.Csoport_Id_Jogalany,'I',DropDownListJogszint.SelectedValue.ToCharArray()[0],'0');

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            JogosultakGridViewBind();
                            EFormPanel1.Visible = false;
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();
                        }
                        break;
                    }
                case CommandName.Modify:
                    {
                        KRT_JogosultakService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_JogosultakService();

                        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
                        ep.Record_Id = UI.GetGridViewSelectedRecordId(JogosultakGridView);

                        var krt_jogosultak = (KRT_Jogosultak)service.Get(ep).Record;
                    
                        krt_jogosultak.Jogszint = DropDownListJogszint.SelectedValue;
                        krt_jogosultak.Updated.Jogszint = true;
                        krt_jogosultak.Base.Ver = Record_Ver_HiddenField.Value;
                        krt_jogosultak.Base.Updated.Ver = true;

                        Result res = service.Update(ep,krt_jogosultak);

                        if (string.IsNullOrEmpty(res.ErrorCode))
                        {
                            JogosultakGridViewBind();
                            EFormPanel1.Visible = false;
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                            ErrorUpdatePanel1.Update();
                        }
                    }
                    break;
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private void ClearForm()
    {
        // TODO:
        //UploadJogosult.Text = "";
        //JogosultMegjegyzes.Text = "";
        //CsatlomanyVerzio.Text = "1";
        //UploadJogosult.Id_HiddenField = "";
    }

    // form --> business object
    private KRT_Jogosultak GetBusinessObjectFromComponents()
    {
        KRT_Jogosultak krt_jogosultak = new KRT_Jogosultak();
        krt_jogosultak.Updated.SetValueAll(false);
        krt_jogosultak.Base.Updated.SetValueAll(false);

        krt_jogosultak.Csoport_Id_Jogalany = Csoport_Id_JogosultTextBox.Id_HiddenField; ;
        krt_jogosultak.Updated.Csoport_Id_Jogalany = pageView.GetUpdatedByView(Csoport_Id_JogosultTextBox);

        krt_jogosultak.Base.Updated.Ver = true;

        return krt_jogosultak;
    }


#endregion

#region List
    protected void JogosultakGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);
        JogosultakGridViewBind(sortExpression, sortDirection);
    }

    protected void JogosultakGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(JogosultakGridView);

        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
        search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);
        
        string Id = Request.QueryString.Get("Id");
        if (string.IsNullOrEmpty(Id)) return;

#if !showOrokoltRows
        // Orokolt = 'I' sorok kisz�r�se
        search.WhereByManual = String.Format(" KRT_Jogosultak.Obj_Id='{0}'", Id);
#endif

        Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, Id, search);

        ui.GridViewFill(JogosultakGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.InvalidateOnClientClick = "";
        }
    }

    protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    /// <summary>
    /// T�rli a JogosultakGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedJogosultak()
    {
        List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, EErrorPanel1, ErrorUpdatePanel1);
        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        for (int i = 0; i < deletableItemsList.Count; i++)
        {
            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
            execParams[i].Record_Id = deletableItemsList[i];
        }
        Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }
    }


#endregion
    
    protected void JogosultakGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        #if showOrokoltRows
            var drv = (DataRowView)e.Row.DataItem;
            try
            {
                if (drv.Row.ItemArray[7].ToString() == "I")
                {
                    e.Row.Enabled = false;
                    e.Row.FindControl("check").Visible = false;
                }
            }
            catch { };
#endif
#region CR3189 - Jogosultakon is legyen kisherceg..
        Iratok.JogosultakGridView_RowDataBound_CheckTemplatesRowKijeloles(e, Page, ParentId);
#endregion
    }
    
    protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JogosultakGridViewBind(e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    }

    private void RefreshJogosultakFromUcm()
    {
        if (ParentForm == Constants.ParentForms.IraIrat)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page);
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                Contentum.eDocument.Service.UCMDocumentService ucmDocumentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetUCMDocumentService();
                Result res = ucmDocumentService.RefreshJogosultakFromUcm(execParam, ParentId);

                if (res.IsError)
                {
                    Logger.Error("RefreshJogosultakFromUcm hiba", execParam, res);
                }
            }
        }
    }

#region CsatolmanyJogosultakGridView
    protected void CsatolmanyJogosultakGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolmanyJogosultakGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolmanyJogosultakGridView", ViewState);
        CsatolmanyJogosultakGridViewBind(sortExpression, sortDirection);
    }

    protected void CsatolmanyJogosultakGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(CsatolmanyJogosultakGridView);

        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        Result res = service.GetCsatolmanyJogosultakOrokolt(execParam, ParentId);

        ui.GridViewFill(CsatolmanyJogosultakGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);
    }

    protected void CsatolmanyJogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolmanyJogosultakGridViewBind(e.SortExpression, UI.GetSortToGridView("CsatolmanyJogosultakGridView", ViewState, e.SortExpression));
    }

#endregion
}
