<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JogosultakTabPage.ascx.cs"
    Inherits="Component_JogosultakTabPage" %>
<%@ Register Src="JogosultakSubListHeader.ascx" TagName="JogosultakSubListHeader"
    TagPrefix="uc2" %>

<asp:UpdatePanel ID="JogosultakUpdatePanel" runat="server" OnLoad="JogosultakUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="JogosultakPanel" runat="server" Visible="false" Width="100%">
            <uc2:JogosultakSubListHeader ID="JogosultakSubListHeader" runat="server" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <ajaxToolkit:CollapsiblePanelExtender ID="JogosultakCPE" runat="server" TargetControlID="Panel4"
                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" ExpandedSize="0">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel4" runat="server">
                            <asp:GridView ID="JogosultakGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" AutoGenerateColumns="false" OnSorting="JogosultakGridView_Sorting"
                                OnPreRender="JogosultakGridView_PreRender" OnRowCommand="JogosultakGridView_RowCommand"
                                DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                        SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" />
                                        <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Nev" HeaderStyle-Width="250px" />
                                    <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s&nbsp;ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
