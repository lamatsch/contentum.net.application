using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;
using Contentum.eUIControls;

public partial class Component_JogosultakTabPage : System.Web.UI.UserControl
{
    #region Private fields
    private UI ui = new UI();
    private UpdatePanel _ErrorUpdatePanel;
    private eErrorPanel _ErrorPanel;
    private string _JogtargyId;

    #endregion

    public UpdatePanel ErrorUpdatePanel
    {
        get { return _ErrorUpdatePanel; }
        set { _ErrorUpdatePanel = value; }
    }

    public eErrorPanel ErrorPanel
    {
        get { return _ErrorPanel; }
        set { _ErrorPanel = value; }
    }

    public string JogtargyId
    {
        get { return _JogtargyId; }
        set 
        {
            _JogtargyId = value;
            JogosultakGridViewBind(value);
        }
    }

    #region Init
    protected void Page_Load(object sender, EventArgs e)
    {
        JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.DeleteOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JogosultakGridView.ClientID);
        JogosultakSubListHeader.ButtonsClick += new CommandEventHandler(JogosultakSubListHeader_ButtonsClick);

        JogosultakSubListHeader.AttachedGridView = JogosultakGridView;
        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JogosultakSubListHeader.RowCount_Changed += new EventHandler(JogosultakSubListHeader_RowCount_Changed);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        JogosultakPanel.Visible = true;

        JogosultakSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        JogosultakSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        JogosultakSubListHeader.DeleteEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);
    }

    #endregion

    #region Jogosultak Detail

    private void JogosultakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Jogosultak();
            JogosultakGridViewBind(this._JogtargyId);
        }
    }

    public void JogosultakGridViewBind(string IraIktatoKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);

        JogosultakGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
        JogosultakPanel.Visible = true;
    }

    private void JogosultakGridViewBind(string IraIktatoKonyvId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(IraIktatoKonyvId))
        {
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
            search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);

            Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, IraIktatoKonyvId, search);

            UI.GridViewFill(JogosultakGridView, res, JogosultakSubListHeader, _ErrorPanel, _ErrorUpdatePanel);

            ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
        }
        else
        {
            ui.GridViewClear(JogosultakGridView);
        }
    }

    private void deleteSelected_Jogosultak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, _ErrorPanel, _ErrorUpdatePanel);
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(_ErrorPanel, result);
                _ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    JogosultakGridViewBind(_JogtargyId);
                break;
            }
        }
    }

    protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
        }
    }

    public void JogosultakGridView_RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                  + "&" + QueryStringVars.IktatokonyvId + "=" + _JogtargyId
                , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID);
        }
    }

    protected void JogosultakGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JogosultakGridView.PageIndex;

        JogosultakGridView.PageIndex = JogosultakSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != JogosultakGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, JogosultakCPE);
            JogosultakGridViewBind(_JogtargyId);
        }
        else
        {
            UI.GridViewSetScrollable(JogosultakSubListHeader.Scrollable, JogosultakCPE);
        }
        JogosultakSubListHeader.PageCount = JogosultakGridView.PageCount;
        JogosultakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JogosultakGridView);
    }


    void JogosultakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        JogosultakGridViewBind(_JogtargyId);
    }

    protected void JogosultakGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;
        try
        {
            if (int.Parse(drv.Row.ItemArray[7].ToString()) != 1 || (char)drv.Row.ItemArray[2] == 'N')
            {
                e.Row.Enabled = false;
                e.Row.FindControl("check").Visible = false;
            }
        }
        catch { };
    }


    protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JogosultakGridViewBind(JogtargyId
            , e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region OuterVoids
    public void RefreshOnClientClicks()
    {
        JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick(
            "IraIktatokonyvJogosultakForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + _JogtargyId
            , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

    }

    public void GridViewClear()
    {
        ui.GridViewClear(JogosultakGridView);
    }

    #endregion

}
