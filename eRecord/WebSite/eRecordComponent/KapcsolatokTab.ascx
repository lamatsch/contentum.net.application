<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KapcsolatokTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratKapcsolatokTab" %>
<%@ Register Src="KuldemenyTextBox.ascx" TagName="KuldemenyTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="KapcsolatokUpdatePanel" runat="server" OnLoad="KapcsolatokUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <ajaxToolkit:CollapsiblePanelExtender ID="KapcsolatokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>                                                  
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		    <td>
				<%-- TabHeader --%>
                
                <uc1:TabHeader ID="TabHeader1" runat="server" />
							    
			    <%-- /TabHeader --%>
                        
                <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
				        <asp:GridView ID="KapcsolatokGridView"
				                      runat="server"
                                      CellPadding="0"
				                      CellSpacing="0"
				                      BorderWidth="1"
				                      GridLines="Both"
				                      AllowPaging="True"
				                      PagerSettings-Visible="false"
				                      AllowSorting="True"
				                      DataKeyNames="Id"
				                      AutoGenerateColumns="False" Width="98%"
				                      OnRowCommand="KapcsolatokGridView_RowCommand" OnRowDataBound="KapcsolatokGridView_RowDataBound" OnSorting="KapcsolatokGridView_Sorting"
				                      >
						    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						    <Columns>
							    <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
									    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
									    &nbsp;&nbsp;
									    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
								    </HeaderTemplate>
								    <ItemTemplate>
									    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" /> <%--'<%# Eval("Id") %>'--%>
								    </ItemTemplate>
							    </asp:TemplateField>
							    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							    </asp:CommandField>
									<asp:BoundField DataField="KapcsolatTipusa" HeaderText="Kapcsolat t�pusa" SortExpression="KapcsolatTipusa">
										<HeaderStyle CssClass="GridViewBorderHeader"/>
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="Azonosito">
										<HeaderStyle CssClass="GridViewBorderHeader" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="Kezi" HeaderText="K�zi" SortExpression="Kezi">
										<HeaderStyle CssClass="GridViewBorderHeader" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="ErvKezdete" HeaderText="�rv.kezdete" SortExpression="ErvKezdete">
										<HeaderStyle CssClass="GridViewBorderHeader" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="ErvVege" HeaderText="�rv.v�ge" SortExpression="ErvVege">
										<HeaderStyle CssClass="GridViewBorderHeader" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="Leiras" HeaderText="Le�r�s" SortExpression="Leiras">
										<HeaderStyle CssClass="GridViewBorderHeader" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
						    </Columns>
				                      
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>                            
        
			        <eUI:eFormPanel ID="EFormPanel1" runat="server">
					    <table cellspacing="0" cellpadding="0" width="100%">
						    <tr class="urlapSor">
							    <td style="text-align: right; vertical-align: middle;">
								    <asp:Label ID="Label1" runat="server" Text="Kapcsoland�:"></asp:Label>
								</td>
							    <td style="text-align: left; vertical-align: middle;">
                                    &nbsp;<uc3:KuldemenyTextBox ID="KuldemenyTextBox1" runat="server" />
							    </td>
						    </tr>
						    <tr class="urlapSor">
							    <td style="text-align: right; vertical-align: middle;">
								    <asp:Label ID="LabelKapcsolatTipus" runat="server" Text="Kapcsol�s t�pusa:"></asp:Label>
							    </td>
							    <td style="text-align: left; vertical-align: middle;">
                                    &nbsp;<uc4:KodtarakDropDownList ID="KapcsolatTipusKodtarakDropDownList" runat="server" />
							    </td>
						    </tr>
						    <tr class="urlapSor">
							    <td style="text-align: right; vertical-align: middle;">
								    <asp:Label ID="LabelLeiras" runat="server" Text="Megjegyz�s:"></asp:Label>								    
							    </td>
							    <td style="text-align: left; vertical-align: middle;">
                                    &nbsp;<asp:TextBox ID="LeirasTextBox" runat="server" Width="250"/>
							    </td>
						    </tr>
					    </table>
					    <%-- TabFooter --%>
    
                        <uc2:TabFooter ID="TabFooter1" runat="server" />
						
					    <%-- /TabFooter --%>
			    </eUI:eFormPanel>    			
		    </td>
	    </tr>
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
