using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratKapcsolatokTab : System.Web.UI.UserControl
{

    private const string KodCsoportKapcsolatTipus = "KAPCSOLATTIPUS";

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }


    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        TabFooter1.SaveValidationGroup = "Kapcsolatok";
        // TODO:
        //UploadKapcsolat.ValidationGroup = TabFooter1.SaveValidationGroup;
        //KapcsolatMegjegyzes.ValidationGroup = TabFooter1.SaveValidationGroup;
        //CsatlomanyVerzio.ValidationGroup = TabFooter1.SaveValidationGroup;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Kapcsolat" + CommandName.New);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Kapcsolat" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Kapcsolat" + CommandName.Invalidate);
        //TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kapcsolat" + CommandName.View);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, "Kapcsolat" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, "Kapcsolat" + CommandName.ElosztoLista);

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(KapcsolatokGridView));
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(KapcsolatokGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void KapcsolatokUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;

        TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KapcsolatokGridView.ClientID);
        //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ClearForm();
        EFormPanel1.Visible = false;
        KapcsolatokGridViewBind();
        pageView.SetViewOnPage(Command);
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        KapcsolatTipusKodtarakDropDownList.FillDropDownList(KodCsoportKapcsolatTipus, EErrorPanel1);

        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;

            String RecordId = UI.GetGridViewSelectedRecordId(KapcsolatokGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {                
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;
                
                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();                
                Result result = service.Get(execParam);
                
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    LoadComponentsFromBusinessObject(result.Record);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKapcsolatok();
            KapcsolatokGridViewBind();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, ParentForm + "Kapcsolat" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                            EREC_Csatolmanyok erec_Csatolmanyok = (EREC_Csatolmanyok)GetBusinessObjectFromComponents();

                            Result result = service.Insert(execParam, erec_Csatolmanyok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ReLoadTab();
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = UI.GetGridViewSelectedRecordId(KapcsolatokGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                                EREC_Csatolmanyok erec_Csatolmanyok = GetBusinessObjectFromComponents();

                                Result result = service.Update(execParam, erec_Csatolmanyok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }

                            }
                            break;
                        }
                }
                
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }        
    }

    private void ClearForm()
    {
        // TODO:
        KuldemenyTextBox1.Id_HiddenField = "";
        KuldemenyTextBox1.SetKuldemenyTextBoxById(EErrorPanel1, ErrorUpdatePanel1);

        KapcsolatTipusKodtarakDropDownList.DropDownList.SelectedIndex = -1;
        LeirasTextBox.Text = "";
        //UploadKapcsolat.Text = "";
        //KapcsolatMegjegyzes.Text = "";
        //CsatlomanyVerzio.Text = "1";
        //UploadKapcsolat.Id_HiddenField = "";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(Object Kapcsolatok)
    {
        // TODO:
        //if (ParentForm == Constants.ParentForms.Kuldemeny)
        //{
        //    EREC_KuldDokumentumok erec_KuldDokumentumok = (EREC_KuldDokumentumok)Kapcsolatok;
        //    KuldemenyTextBox1.Id_HiddenField = erec_KuldDokumentumok.Kapcsolat;
        //    KuldemenyTextBox1.SetKuldemenyTextBoxById(EErrorPanel1);
        //    KapcsolatTipusKodtarakDropDownList.SetSelectedValue(erec_KuldDokumentumok.KapcsolatTipus);
        //    LeirasTextBox.Text = erec_KuldDokumentumok.Megjegyzes;
            
        //    Record_Ver_HiddenField.Value = erec_KuldDokumentumok.Base.Ver;

        //    if (Command == CommandName.View)
        //    {
        //        KapcsolatTipusKodtarakDropDownList.Enabled = false;
        //        LeirasTextBox.ReadOnly = true;
        //    }
        //    else
        //    {
        //        KapcsolatTipusKodtarakDropDownList.Enabled = true;
        //        LeirasTextBox.ReadOnly = false;
        //    }
        //}

        //if (ParentForm == Constants.ParentForms.IraIrat)
        //{
        //    EREC_KuldDokumentumok erec_KuldDokumentumok = (EREC_KuldDokumentumok)Mellekletek;
        //    KuldemenyTextBox1.Id_HiddenField = erec_KuldDokumentumok.Kapcsolat;
        //    KuldemenyTextBox1.SetKuldemenyTextBoxById(EErrorPanel1);
        //    KapcsolatTipusKodtarakDropDownList.SetSelectedValue(erec_KuldDokumentumok.KapcsolatTipus);
        //    LeirasTextBox.Text = erec_KuldDokumentumok.Megjegyzes;

        //    Record_Ver_HiddenField.Value = erec_KuldDokumentumok.Base.Ver;

        //    if (Command == CommandName.View)
        //    {
        //        KapcsolatTipusKodtarakDropDownList.Enabled = false;
        //        LeirasTextBox.ReadOnly = true;
        //    }
        //    else
        //    {
        //        KapcsolatTipusKodtarakDropDownList.Enabled = true;
        //        LeirasTextBox.ReadOnly = false;
        //    }
        //}
    }

    // form --> business object
    private EREC_Csatolmanyok GetBusinessObjectFromComponents()
    {
        // TODO:
        //if (ParentForm == Constants.ParentForms.Kuldemeny)
        //{

        //    EREC_KuldDokumentumok erec_KuldDokumentumok = new EREC_KuldDokumentumok();
        //    erec_KuldDokumentumok.Updated.SetValueAll(false);
        //    erec_KuldDokumentumok.Base.Updated.SetValueAll(false);

        //    erec_KuldDokumentumok.kapcsolaat = KuldemenyTextBox1.Id_HiddenField;
        //    erec_KuldDokumentumok.Updated.kapcsolaat = true;

        //    erec_KuldDokumentumok.KapcsolatTipus = KapcsolatTipusKodtarakDropDownList.SelectedValue;
        //    erec_KuldDokumentumok.Updated.KapcsolatTipus = true;

        //    erec_KuldDokumentumok.Leiras = LeirasTextBox.Text;
        //    erec_KuldDokumentumok.Updated.Leiras = true;

        //    erec_KuldDokumentumok.Base.Ver = Record_Ver_HiddenField.Value;
        //    erec_KuldDokumentumok.Base.Updated.Ver = true;

        //    return erec_KuldDokumentumok;
        //}
        
        //if (ParentForm == Constants.ParentForms.IraIrat)
        //{

        //    EREC_KuldDokumentumok erec_KuldDokumentumok = new EREC_KuldDokumentumok();
        //    erec_KuldDokumentumok.Updated.SetValueAll(false);
        //    erec_KuldDokumentumok.Base.Updated.SetValueAll(false);

        //    erec_KuldDokumentumok.kapcsolaat = KuldemenyTextBox1.Id_HiddenField;
        //    erec_KuldDokumentumok.Updated.kapcsolaat = true;

        //    erec_KuldDokumentumok.KapcsolatTipus = KapcsolatTipusKodtarakDropDownList.SelectedValue;
        //    erec_KuldDokumentumok.Updated.KapcsolatTipus = true;

        //    erec_KuldDokumentumok.Leiras = LeirasTextBox.Text;
        //    erec_KuldDokumentumok.Updated.Leiras = true;

        //    erec_KuldDokumentumok.Base.Ver = Record_Ver_HiddenField.Value;
        //    erec_KuldDokumentumok.Base.Updated.Ver = true;

        //    return erec_KuldDokumentumok;
        //}
        
        return null;
    }


    #endregion

    #region List
    protected void KapcsolatokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KapcsolatokGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KapcsolatokGridView", ViewState);
        KapcsolatokGridViewBind(sortExpression, sortDirection);
    }

    protected void KapcsolatokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        //UI.ClearGridViewRowSelection(KapcsolatokGridView);
        //ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //EREC_KuldDokumentumokService service = null;
        //EREC_KuldDokumentumokSearch search = null;
        
        //Result res = null;

        //if (ParentForm == Constants.ParentForms.Kuldemeny)
        //{
        //    service = eRecordService.ServiceFactory.GetEREC_KuldDokumentumokService();
        //    search = (EREC_KuldDokumentumokSearch)Search.GetSearchObject(Page, new EREC_KuldDokumentumokSearch());
        //    if (!String.IsNullOrEmpty(ParentId))
        //    {
        //        search.KuldKuldemeny_Id.Value = ParentId;
        //        search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        //    }
        //    search.OrderBy = Search.GetOrderBy("KapcsolatokGridView", ViewState, SortExpression, SortDirection);
        //    res = service.GetAll(ExecParam, search);
        //}

        //if (ParentForm == Constants.ParentForms.IraIrat)
        //{
        //    service = eRecordService.ServiceFactory.GetEREC_KuldDokumentumokService();
        //    search = (EREC_KuldDokumentumokSearch)Search.GetSearchObject(Page, new EREC_KuldDokumentumokSearch());
        //    if (!String.IsNullOrEmpty(ParentId))
        //    {
        //        search.KuldKuldemeny_Id.Value = ParentId;
        //        search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        //    }
        //    search.OrderBy = Search.GetOrderBy("KapcsolatokGridView", ViewState, SortExpression, SortDirection);
        //    res = service.GetAll(ExecParam, search);
        //}
        ////TODO: 
        ////ui.GridViewFill(KapcsolatokGridView, res, EErrorPanel1, ErrorUpdatePanel1);

    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";
        }
    }

    protected void KapcsolatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KapcsolatokGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    /// <summary>
    /// T�rli a KapcsolatokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedKapcsolatok()
    {
        //if (FunctionRights.GetFunkcioJog(Page, ParentForm + "KapcsolatInvalidate"))
        //{
        //    List<string> deletableItemsList = ui.GetGridViewSelectedRows(KapcsolatokGridView, EErrorPanel1, ErrorUpdatePanel1);
        //    ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        //    for (int i = 0; i < deletableItemsList.Count; i++)
        //    {
        //        execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParams[i].Record_Id = deletableItemsList[i];
        //    }
            
        //    Result result = null;
            
        //    if (ParentForm == Constants.ParentForms.Kuldemeny)
        //    {
        //        EREC_KuldDokumentumokService service = eRecordService.ServiceFactory.GetEREC_KuldDokumentumokService();
        //        result = service.MultiInvalidate(execParams);
        //    }

        //    if (ParentForm == Constants.ParentForms.IraIrat)
        //    {
        //        EREC_KuldDokumentumokService service = eRecordService.ServiceFactory.GetEREC_KuldDokumentumokService();
        //        result = service.MultiInvalidate(execParams);
        //    }

        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        ErrorUpdatePanel1.Update();
        //    }
        //}
        //else
        //{
        //    UI.RedirectToAccessDeniedErrorPage(Page);
        //}
    }


    #endregion
    protected void KapcsolatokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void KapcsolatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KapcsolatokGridViewBind(e.SortExpression, UI.GetSortToGridView("KapcsolatokGridView", ViewState, e.SortExpression));
    }
}
