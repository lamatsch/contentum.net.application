using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratKapjakMegTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.New);
        TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.View);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.Invalidate);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.ElosztoLista);

        // Z�rol�s enged�lyez�se:
        TabHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.Lock);
        TabHeader1.UnLockEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + CommandName.Lock);

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(KapjakMegGridView));
        }
        ui.SetClientScriptToGridViewSelectDeSelectButton(KapjakMegGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(QueryStringVars.Command);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void KapjakMegUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        if (_ParentForm == Constants.ParentForms.IratPeldany)
        {

            // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;

            if (Command == CommandName.Modify)
            {
                TabHeader1.LockVisible = true;
                TabHeader1.UnLockVisible = true;
                TabHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(KapjakMegGridView.ClientID);
                TabHeader1.UnLockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(KapjakMegGridView.ClientID); 
            }
            else
            {
                TabHeader1.LockVisible = false;
                TabHeader1.UnLockVisible = false;
                TabHeader1.LockOnClientClick = "";
                TabHeader1.UnLockOnClientClick = "";       
            }

            TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KapjakMegGridView.ClientID);
            //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            ClearForm();
            EFormPanel1.Visible = false;
            KapjakMegGridViewBind();

            pageView.SetViewOnPage(Command);
        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;

            String RecordId = UI.GetGridViewSelectedRecordId(KapjakMegGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                EREC_PldKapjakMegService service = eRecordService.ServiceFactory.GetEREC_PldKapjakMegService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_PldKapjakMeg erec_PldKapjakMeg = (EREC_PldKapjakMeg)result.Record;
                    LoadComponentsFromBusinessObject(erec_PldKapjakMeg);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKapjakMeg();
            KapjakMegGridViewBind();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
        if (e.CommandName == CommandName.Lock)
        {
            LockSelectedRecords();
            KapjakMegGridViewBind();
        }
        if (e.CommandName == CommandName.Unlock)
        {
            UnlockSelectedRecords();
            KapjakMegGridViewBind();
        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMeg" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            EREC_PldKapjakMegService service = eRecordService.ServiceFactory.GetEREC_PldKapjakMegService();
                            EREC_PldKapjakMeg erec_PldKapjakMeg = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            
                            Result result = service.Insert(execParam, erec_PldKapjakMeg);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ReLoadTab();
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = UI.GetGridViewSelectedRecordId(KapjakMegGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_PldKapjakMegService service = eRecordService.ServiceFactory.GetEREC_PldKapjakMegService();
                                EREC_PldKapjakMeg erec_PldKapjakMeg = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_PldKapjakMeg);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private void ClearForm()
    {
        CsoportTextBoxKapjakMeg.Id_HiddenField = "";
        CsoportTextBoxKapjakMeg.SetCsoportTextBoxById(EErrorPanel1);

        TextBoxMegjegyzes.Text = "";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_PldKapjakMeg erec_PldKapjakMeg)
    {
        CsoportTextBoxKapjakMeg.Id_HiddenField = erec_PldKapjakMeg.Csoport_Id;
        CsoportTextBoxKapjakMeg.SetCsoportTextBoxById(EErrorPanel1);

        TextBoxMegjegyzes.Text = erec_PldKapjakMeg.Base.Note;

        Record_Ver_HiddenField.Value = erec_PldKapjakMeg.Base.Ver;

        if (Command == CommandName.View)
        {
            CsoportTextBoxKapjakMeg.ReadOnly = true;
            TextBoxMegjegyzes.ReadOnly = true;
        }
        else
        {
            CsoportTextBoxKapjakMeg.ReadOnly = false;
            TextBoxMegjegyzes.ReadOnly = false;
        }

        //FormPart_CreatedModified1.SetComponentValues(erec_PldKapjakMeg.Base);
    }

    // form --> business object
    private EREC_PldKapjakMeg GetBusinessObjectFromComponents()
    {
        EREC_PldKapjakMeg erec_PldKapjakMeg = new EREC_PldKapjakMeg();
        erec_PldKapjakMeg.Updated.SetValueAll(false);
        erec_PldKapjakMeg.Base.Updated.SetValueAll(false);

        erec_PldKapjakMeg.PldIratPeldany_Id = ParentId;
        erec_PldKapjakMeg.Updated.PldIratPeldany_Id = true;

        erec_PldKapjakMeg.Csoport_Id = CsoportTextBoxKapjakMeg.Id_HiddenField;
        erec_PldKapjakMeg.Updated.Csoport_Id = pageView.GetUpdatedByView(CsoportTextBoxKapjakMeg);

        erec_PldKapjakMeg.Base.Note = TextBoxMegjegyzes.Text;
        erec_PldKapjakMeg.Base.Updated.Note = pageView.GetUpdatedByView(TextBoxMegjegyzes);

        erec_PldKapjakMeg.Base.Ver = Record_Ver_HiddenField.Value;
        erec_PldKapjakMeg.Base.Updated.Ver = true;

        return erec_PldKapjakMeg;
    }


    #endregion

    #region List
    protected void KapjakMegGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KapjakMegGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KapjakMegGridView", ViewState);
        KapjakMegGridViewBind(sortExpression, sortDirection);
    }

    protected void KapjakMegGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(KapjakMegGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_PldKapjakMegService service = null;
        EREC_PldKapjakMegSearch search = null;

        service = eRecordService.ServiceFactory.GetEREC_PldKapjakMegService();
        search = (EREC_PldKapjakMegSearch)Search.GetSearchObject(Page, new EREC_PldKapjakMegSearch());
        if (!String.IsNullOrEmpty(ParentId))
        {
            search.PldIratPeldany_Id.Value = ParentId;
            search.PldIratPeldany_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        search.OrderBy = Search.GetOrderBy("KapjakMegGridView", ViewState, SortExpression, SortDirection);
        Result res = service.GetAllWithExtension(ExecParam, search);
        ui.GridViewFill(KapjakMegGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);

        ui.SetClientScriptToGridViewSelectDeSelectButton(KapjakMegGridView);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";
        }
    }

    protected void KapjakMegGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KapjakMegGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    /// <summary>
    /// T�rli a KapjakMegGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedKapjakMeg()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyKapjakMegInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(KapjakMegGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_PldKapjakMegService service = eRecordService.ServiceFactory.GetEREC_PldKapjakMegService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void LockSelectedRecords()
    {
        // EREC_PldKapjakMeg rekordokat lockolunk
        List<string> ItemsList = ui.GetGridViewSelectedRows(KapjakMegGridView, EErrorPanel1, ErrorUpdatePanel1);
        LockManager.LockSelectedRecords(ItemsList, "EREC_PldKapjakMeg"
            , "IratPeldanyKapjakMeg" + CommandName.Lock, "IratPeldanyKapjakMegForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel1);
    }

    private void UnlockSelectedRecords()
    {
        // EREC_PldKapjakMeg rekordokat lockolunk
        List<string> ItemsList = ui.GetGridViewSelectedRows(KapjakMegGridView, EErrorPanel1, ErrorUpdatePanel1);
        LockManager.UnlockSelectedRecords(ItemsList, "EREC_PldKapjakMeg"
            , "IratPeldanyKapjakMeg" + CommandName.Lock, "IratPeldanyKapjakMegForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel1);
    }

    #endregion
    protected void KapjakMegGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void KapjakMegGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KapjakMegGridViewBind(e.SortExpression, UI.GetSortToGridView("KapjakMegGridView", ViewState, e.SortExpression));
    }
}
