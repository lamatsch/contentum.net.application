﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KezelesiFeljegyzesPanel.ascx.cs"
    Inherits="eRecordComponent_KezelesiFeljegyzesPanel" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc1" %>
<%@ Register Src="~/Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc2" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc3" %>
<%@ Register Src="~/Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="~/eRecordComponent/ObjektumValasztoPanel.ascx" TagName="ObjektumValasztoPanel"
    TagPrefix="uc7" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager"
    TagPrefix="fm" %>
<%@ Register Src="~/Component/FelhasznalokMultiSelectPanel.ascx" TagName="FelhasznalokMultiSelectPanel"
    TagPrefix="fmsp" %>
<%@ Register Src="~/eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc9" %>

<asp:UpdatePanel runat="server" ID="MainUpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server">
            <asp:HiddenField ID="hfFeladatId" runat="server" />
            <ajaxToolkit:CollapsiblePanelExtender ID="FeljegyzesCPE" runat="server" TargetControlID="KezelesiFeljegyzesForm"
                CollapsedSize="0" Collapsed="true" ExpandControlID="panelCpeHeader" CollapseControlID="panelCpeHeader"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
                ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="FeljegyzesCPEButton"
                ExpandedSize="0" ExpandedText="Feladat / Kezelési feljegyzés elrejt" CollapsedText="Feladat / Kezelési feljegyzés mutat"
                TextLabelID="labelFeljegyzesCpeText" Enabled="false">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel ID="trFeljegyzesCpe" runat="server" Visible="false" class="tabExtendableListFejlec"
                Style="text-align: left;">
                <asp:Panel ID="panelCpeHeader" runat="server" Style="display: block; padding-left: 5px; cursor: pointer; font-weight: bold;">
                    <asp:ImageButton runat="server" ID="FeljegyzesCPEButton" ImageUrl="../images/hu/Grid/minus.gif"
                        OnClientClick="return false;" />
                    <asp:Label ID="labelFeljegyzesCpeText" runat="server" Text="Kezelési feljegyzés"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <eUI:eFormPanel ID="KezelesiFeljegyzesForm" runat="server" Style="margin-top: 5px;">
                <div style="text-align: left; width: 100%">
                    <table cellspacing="0" cellpadding="0" style="text-align: left;">
                        <tr class="urlapSor" id="tr1" runat="server">
                            <td class="mrUrlapCaption_shorter" id="tdImageSave" runat="server">
                                <div style="text-align: left">
                                    <asp:ImageButton ID="imgSave" Height="20" Width="20" ImageUrl="~/images/hu/Grid/saverow.gif"
                                        AlternateText="Mentés" CommandName="Save" runat="server" />
                                    <asp:ImageButton ID="imgModify" Height="25px" Width="25px" ImageUrl="../images/hu/ikon/modositas1_keret_nelkul.jpg"
                                        CausesValidation="false" onmouseover="swapByName(this.id,'modositas1.jpg')" onmouseout="swapByName(this.id,'modositas1_keret_nelkul.jpg')"
                                        AlternateText="Módosítás" CommandName="Modify" runat="server" />
                                    <asp:ImageButton ID="imgDelegate" Height="25px" Width="25px" ImageUrl="../images/hu/ikon/delegate.jpg"
                                        CausesValidation="false" onmouseover="swapByName(this.id,'delegate.jpg')" onmouseout="swapByName(this.id,'delegate.jpg')"
                                        AlternateText="Delegálás" CommandName="Delegate" runat="server" />
                                    <asp:HiddenField ID="isDelegate" runat="server" Value="default" />
                                    <asp:Label ID="lblDelegate" runat="server" Text="Delegálás" Visible="false" />
                                </div>
                            </td>
                            <td id="tdFeladt" runat="server" style="text-align: left; width: 280px;">
                                <uc1:KodtarakDropDownList ID="ktListFeladat" runat="server" ListMode="RadioButtonList"
                                    AutoPostBack="true" OnSelectedIndexChanged="ktListFeladat_SelectedIndexChanged" />
                            </td>
                            <td id="tdMemo" runat="server">
                                <asp:CheckBox ID="cbMemo" runat="server" Text="Memo" Style="font-weight: bold; padding-right: 30px"></asp:CheckBox>
                            </td>
                            <td class="mrUrlapCaption_nowidth" style="width: 85px;" id="tdLabelKezeles" runat="server">
                                <asp:Label ID="labelKezelesiFeljegyzesRequired" runat="server" CssClass="ReqStar"
                                    Text="*" Visible="false"></asp:Label>
                                <%--BLG_1054--%>
                                <%--<asp:Label ID="labelKezelesiFeljegyzes" runat="server" Text="Kezelési utasítások:">--%>
                                <asp:Label ID="labelKezelesiFeljegyzes" runat="server" Text="<%$Forditas:labelKezelesiFeljegyzes|Kezelési utasítások:%>">
                                </asp:Label>
                            </td>
                            <td id="tdKezelesiFeljegyzesTipus" runat="server" class="mrUrlapMezo" style="width: 280px;">
                                <uc1:KodtarakDropDownList ID="ktddKezelesiFeljegyzesTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_shorter">
                                <asp:Label ID="labelKezelesiFeljegyzesLeirasRequired" runat="server" CssClass="ReqStar"
                                    Text="*"></asp:Label>
                                <asp:Label ID="labelKezelesiFeljegyzesLeiras" runat="server" Text="Szöveges kifejtés:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="4">
                                <uc8:RequiredTextBox ID="txtKezelesiFeljegyzesLeiras" Rows="4" Width="705px" TextMode="MultiLine"
                                    runat="server" Validate="false" LabelRequiredIndicatorID="labelKezelesiFeljegyzesLeirasRequired" CssClass="break">
                                </uc8:RequiredTextBox>
                            </td>
                        </tr>
                        <tr id="rowObjektum" runat="server" class="urlapSor" visible="false">
                            <td class="mrUrlapCaption_shorter">
                                <asp:Label ID="labelObjektum" runat="server" Text="Iratkezelési elem:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="4">
                                <uc7:ObjektumValasztoPanel ID="ObjektumValaszto" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:UpdatePanel ID="FeladatUpdatePanel" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="FeladatPanel" Visible="false" runat="server">

                                            <table cellspacing="0" cellpadding="0">
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <%--<asp:Label ID="labelFeladatRequired" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%>
                                                        <asp:Label ID="labelFeladat" runat="server" Text="Iratkezelési feladat:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" style="width: 280px;" id="tdFeladat" runat="server">
                                                        <asp:UpdatePanel runat="server" ID="UpdatePanelFunkcio" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <uc2:FunkcioTextBox ID="ftbFeladat" runat="server" AjaxEnabled="true" FeladatMode="true"
                                                                    Validate="false" ParentUpdatePanelID="UpdatePanelFunkcio" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td class="mrUrlapCaption_shorter" runat="server" id="tdAllapotLabel">
                                                        <asp:Label ID="labelAllapot" runat="server" Text="Állapot:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" runat="server" id="tdAllapot">
                                                        <uc1:KodtarakDropDownList ID="ktddAllapot" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="rowHataridoPrioritas" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelIntezkedesiHatarido" runat="server" Text="Intézkedési határidő:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc3:CalendarControl runat="server" TimeVisible="true" Validate="false" ID="ccIntezkedes"
                                                            bOpenDirectionTop="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shorter" id="tdPrioritasLabel" runat="server">
                                                        <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="labelPrioritas" runat="server" Text="Prioritás:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" id="tdPrioritas" runat="server">
                                                        <uc1:KodtarakDropDownList runat="server" ID="ktListPrioritas" />
                                                    </td>
                                                </tr>
                                                <tr id="rowFelelos" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelFelelosRequired" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="labelFelelos" runat="server" Text="Felelős:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <uc6:FelhasznaloCsoportTextBox runat="server" ID="fcstbxFelelos" SajatSzervezetDolgozoiVagyOsszes="true" />
                                                    </td>
                                                </tr>
                                                <tr id="rowAddTovabbiFelelosok" runat="server" class="urlapSor" visible="false">
                                                    <td class="mrUrlapCaption_shorter"></td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <asp:CheckBox ID="cbAddTovabbiFelelosok" runat="server" Text="Azonos feladat több felelős számára" />
                                                    </td>
                                                </tr>
                                                <tr id="rowTovabbiFelelosok" runat="server" class="urlapSor" visible="false">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelTovabbiFelelosok" runat="server" Text="További felelősök:" />
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <fmsp:FelhasznalokMultiSelectPanel ID="fmspTovabbiFelelosok" runat="server" CsoporSelectionEnabled="true"
                                                            CustomTextEnabled="false" Rows="5" Width="100%" />
                                                    </td>
                                                </tr>
                                                <tr id="rowKiado" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelKiado" runat="server" Text="Kiadó:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc6:FelhasznaloCsoportTextBox runat="server" ID="ftbxKiado" Validate="false" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelKiadoSzervezete" runat="server" Text="Kiadó&nbsp;szervezete:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc4:CsoportTextBox runat="server" ID="cstbxKiadoSzervezete" Validate="false" ReadOnly="true" />
                                                    </td>
                                                </tr>
                                                <tr id="rowMegoldas" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelMegoldas" runat="server" Text="Megoldás:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <asp:TextBox ID="txtMegoldas" Rows="2" Width="705px" TextMode="MultiLine" runat="server">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="rowNote" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelNote" runat="server" Text="Megjegyzés:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <asp:TextBox ID="txtNote" Rows="2" Width="705px" TextMode="MultiLine" runat="server">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="rowKezdesLezaras" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelKezdesiIdo" runat="server" Text="Kezdési&nbsp;idő:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc3:CalendarControl runat="server" ID="ccKezdesiIdo" Validate="false" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelLezarasDatuma" runat="server" Text="Lezárás&nbsp;dátuma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc3:CalendarControl runat="server" ID="ccLezarasDatuma" Validate="false" ReadOnly="true" />
                                                    </td>
                                                </tr>
                                                <tr id="rowForrasKiadasDatuma" runat="server" class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelKiadasDatuma" runat="server" Text="Kiadás&nbsp;dátuma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc3:CalendarControl runat="server" ID="ccKiadasDatuma" Validate="false" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelForras" runat="server" Text="Forrás:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:RadioButtonList ID="rblistForras" runat="server" Enabled="false" RepeatDirection="Horizontal" />
                                                    </td>

                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:CustomValidator ID="CustomValidator_ValidateIfFeladatIsNotEmpty" runat="server"
                                            ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" SetFocusOnError="true"
                                            ClientValidationFunction="ValidateIfFeladatIsNotEmpty" EnableClientScript="true"
                                            Display="None" ValidateEmptyText="true" Enabled="false" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_ValidateIfFeladatIsNotEmpty"
                                            runat="server" TargetControlID="CustomValidator_ValidateIfFeladatIsNotEmpty">
                                            <Animations>
                                            <OnShow>
                                            <Sequence>
                                                <HideAction Visible="true" />
                                                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                            </Sequence>    
                                            </OnShow>
                                            </Animations>
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: center">
                                <div style="padding-top: 10px">
                                    <uc9:TabFooter ID="TabFooter1" runat="server" Visible="true" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </eUI:eFormPanel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
