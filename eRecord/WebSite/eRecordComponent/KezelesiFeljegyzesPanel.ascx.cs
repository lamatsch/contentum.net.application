﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Collections.Generic;
using Contentum.eUIControls;
using System.Linq;

public partial class eRecordComponent_KezelesiFeljegyzesPanel : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    private int _renderMode = -1;
    public FormPanelRenderMode RenderMode
    {
        get
        {
            return KezelesiFeljegyzesForm.RenderMode;
        }
        set
        {
            if (KezelesiFeljegyzesForm != null)
            {
                KezelesiFeljegyzesForm.RenderMode = value;
                _renderMode = -1;
            }
            else
            {
                _renderMode = (int)value;
            }
        }
    }

    public string FeladatId
    {
        get
        {
            object o = ViewState["FeladatId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["FeladatId"] = value;
        }
    }

    public string FeleadatVersion
    {
        get
        {
            object o = ViewState["FeladatVersion"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["FeladatVersion"] = value;
        }
    }

    public string FeladatLetrehozoId
    {
        get
        {
            object o = ViewState["FeladatLetrehozoId"];
            if (o != null)
            {
                return (string)o;
            }
            return String.Empty;
        }
        set
        {
            ViewState["FeladatLetrehozoId"] = value;
        }
    }

    public string FeladatLetrehozoasiIdo
    {
        get
        {
            object o = ViewState["FeladatLetrehozoasiIdo"];
            if (o != null)
            {
                return (string)o;
            }
            return String.Empty;
        }
        set
        {
            ViewState["FeladatLetrehozoasiIdo"] = value;
        }
    }

    private string ElozoFeladatAllapot
    {
        get
        {
            object o = ViewState["ElozoFeladatAllapot"];
            if (o != null)
            {
                return (string)o;
            }
            return String.Empty;
        }
        set
        {
            ViewState["ElozoFeladatAllapot"] = value;
        }
    }

    public string ParentFeladatId
    {
        get
        {
            object o = ViewState["ParentFeladatId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["ParentFeladatId"] = value;
        }
    }

    public void IncrementFeladatVersion()
    {
        int current;
        if (Int32.TryParse(FeleadatVersion, out current))
        {
            FeleadatVersion = (++current).ToString(); ;
        }
    }

    public string FelelosControlID
    {
        get
        {
            object o = ViewState["FelelosControlID"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["FelelosControlID"] = value;
        }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel = null;

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return errorPanel; }
        set { errorPanel = value; }
    }

    private UpdatePanel errorUpdatePanel = null;

    public UpdatePanel ErrorUpdatePanel
    {
        get { return errorUpdatePanel; }
        set { errorUpdatePanel = value; }
    }

    private string _Command = CommandName.New;

    public string Command
    {
        get
        {
            Object o = ViewState["Command"];
            if (o != null)
                return (string)o;
            return _Command;
        }
        set
        {
            if (Command != value)
            {
                ViewState["Command"] = value;
                if (ktddKezelesiFeljegyzesTipus != null)
                    SetControls();
            }
        }
    }

    private int _objektumPanelVisible = -1;
    public bool ObjektumPanelVisible
    {
        get { return rowObjektum.Visible; }
        set
        {
            if (rowObjektum != null)
            {
                rowObjektum.Visible = value;
            }
            else
            {
                _objektumPanelVisible = value ? 1 : 0;
            }
        }
    }

    public void SetObjektumType(string Obj_Type)
    {
        if (ObjektumPanelVisible)
        {
            SetObjektum(String.Empty, Obj_Type, String.Empty);
        }
        else
        {
            FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
            if (fm != null)
            {
                fm.ObjektumType = Obj_Type;
                fm.IsFinal = true;
            }
        }
    }

    public void SetObjektum(string Obj_Id, string Obj_Type)
    {
        SetObjektum(Obj_Id, Obj_Type, String.Empty);
    }

    public void SetObjektum(string Obj_Id, string Obj_Type, string Obj_Azonosito)
    {
        ObjektumValaszto.SetObjektum(Obj_Id, Obj_Type, Obj_Azonosito);
    }

    public string GetObjektum_Id()
    {
        return ObjektumValaszto.GetSelected_Obj_Id();
    }

    public string GetObjektum_Type()
    {
        return ObjektumValaszto.GetSelected_Obj_Type();
    }

    public string GetObjektum_Azonosito()
    {
        return ObjektumValaszto.GetSelected_Obj_Azonosito();
    }

    private string _labelLeirasText;
    public string LabelLeirasText
    {
        get
        {
            return labelKezelesiFeljegyzesLeiras.Text;
        }
        set
        {
            if (labelKezelesiFeljegyzesLeiras != null)
            {
                labelKezelesiFeljegyzesLeiras.Text = value;
            }
            else
            {
                _labelLeirasText = value;
            }
        }
    }

    public bool Validate
    {
        get
        {
            if (Command == CommandName.View)
                return false;

            object o = ViewState["Validate"];
            if (o != null)
                return (bool)o;

            return false;
        }
        set
        {
            ViewState["Validate"] = value;
        }
    }

    public bool ValidateIfFeladatIsNotEmpty
    {
        get
        {
            if (Command == CommandName.View)
                return false;

            object o = ViewState["ValidateIfFeladatIsNotEmpty"];
            if (o != null)
                return (bool)o;

            return false;
        }
        set
        {
            ViewState["ValidateIfFeladatIsNotEmpty"] = value;
        }
    }

    public bool KezelesiFeljegyzesTipusValidate
    {
        get
        {
            return ktddKezelesiFeljegyzesTipus.Validate;
        }
        set
        {
            ktddKezelesiFeljegyzesTipus.Validate = value;
            labelKezelesiFeljegyzesRequired.Visible = value;
        }
    }

    public bool SmallSize
    {
        get
        {
            object o = ViewState["SmallSize"];
            if (o != null)
                return (bool)o;
            return false;
        }
        set
        {
            ViewState["SmallSize"] = value;
        }
    }

    private int _collapsed = -1;
    public bool Collapsed
    {
        get
        {
            return FeljegyzesCPE.Collapsed;
        }
        set
        {
            if (FeljegyzesCPE != null && trFeljegyzesCpe != null)
            {
                FeljegyzesCPE.Enabled = true;
                trFeljegyzesCpe.Visible = true;
                FeljegyzesCPE.Collapsed = value;
            }
            else
            {
                _collapsed = value ? 1 : 0;
            }
        }
    }

    private bool isOnFeladatTab = false;

    public bool IsOnFeladatTab
    {
        get { return isOnFeladatTab; }
        set { isOnFeladatTab = value; }
    }

    private bool IsVisible
    {
        get
        {
            return HataridosFeladatok.IsKezelesiFeljegyzesPanelVisible(Page) || IsOnFeladatTab;
        }
    }

    private bool _panelEnabled = true;
    public bool PanelEnabled
    {
        get { return _panelEnabled; }
        set
        {
            _panelEnabled = value;

            if (!value)
            {
                SetReadOnly();
            }
            else
            {
                SetModifyControls();
                imgSave.Visible = false;
                imgModify.Visible = false;
                imgDelegate.Visible = false;
                lblDelegate.Visible = false;
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        MainPanel.Visible = IsVisible;
        if (!IsVisible) return;

        if (_labelLeirasText != null)
        {
            LabelLeirasText = _labelLeirasText;
            _labelLeirasText = null;
        }

        if (_objektumPanelVisible > -1)
        {
            ObjektumPanelVisible = (_objektumPanelVisible == 1) ? true : false;
            _objektumPanelVisible = -1;
        }

        if (_isfeladat > -1)
        {
            IsFeladat = (_isfeladat == 1) ? true : false;
            _isfeladat = -1;
        }

        if (_renderMode > -1)
        {
            try
            {
                RenderMode = (FormPanelRenderMode)_renderMode;
            }
            catch
            {
            }
            _renderMode = -1;
        }

        if (_collapsed > -1)
        {
            Collapsed = (_collapsed == 1) ? true : false;
            _collapsed = -1;
        }

        if (HataridosFeladatok.IsDisabled(Page))
        {
            tdFeladt.Visible = false;
            tdMemo.Visible = false;
            tdImageSave.Visible = false;
            tdKezelesiFeljegyzesTipus.ColSpan = 4;
            IsFeladat = false;
            tdLabelKezeles.Style.Add(HtmlTextWriterStyle.Width, "150px");
            TabFooter1.CommandArgument = CommandName.New;
            TabFooter1.ButtonsClick += new CommandEventHandler(imgSave_Command);
        }

        #region BLG_2956
        //LZS
        TabFooter1.CommandArgument = CommandName.Delegate;
        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooter1_ButtonsClick);
        cbAddTovabbiFelelosok.AutoPostBack = true;
        cbAddTovabbiFelelosok.CheckedChanged += CbAddTovabbiFelelosok_CheckedChanged;
        #endregion
    }



    private void CbAddTovabbiFelelosok_CheckedChanged(object sender, EventArgs e)
    {
        if (cbAddTovabbiFelelosok.Checked)
            rowTovabbiFelelosok.Visible = true;
        else
            rowTovabbiFelelosok.Visible = false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsVisible) return;

        if (!IsPostBack)
        {
            InitForm();
            SetControls();
        }

        imgSave.Command += new CommandEventHandler(imgSave_Command);
        imgModify.Command += new CommandEventHandler(imgModify_Command);
        //LZS - BLG_2956 - A delegálás ikonjára illesztett eseménykezelő:
        imgDelegate.Command += new CommandEventHandler(imgDelegate_Command);

        ObjektumValaszto.SelectedTipusChanged += new EventHandler(ObjektumValaszto_SelectedTipusChanged);

        SetValidationGroup();

        if (IsFeladat && Command == CommandName.New)
        {
            SetTovabbiFelelosok();
        }
    }

    protected void ObjektumValaszto_SelectedTipusChanged(object sender, EventArgs e)
    {
        UpdatePanelFunkcio.Update();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsVisible) return;

        SetValidationGroup();
        if (!String.IsNullOrEmpty(FelelosControlID))
        {
            rowFelelos.Visible = false;
        }

        txtKezelesiFeljegyzesLeiras.Validate = Validate;

        if (Validate == true)
        {
            if (!String.IsNullOrEmpty(FelelosControlID))
            {
                Control felelosControl = FindControl(FelelosControlID);
                if (felelosControl != null)
                {
                    if (felelosControl is Component_CsoportTextBox)
                    {
                        ((Component_CsoportTextBox)felelosControl).Validate = true;
                    }
                    else if (felelosControl is Component_FelhasznaloCsoportTextBox)
                    {
                        ((Component_FelhasznaloCsoportTextBox)felelosControl).Validate = true;
                    }
                }
            }
            else
            {
                fcstbxFelelos.Validate = true;
            }
        }
        else
        {
            fcstbxFelelos.Validate = false;
        }


        if (!fcstbxFelelos.Validate && IsFeladat && Command != CommandName.View)
        {
            CustomValidator_ValidateIfFeladatIsNotEmpty.Enabled = (ValidateIfFeladatIsNotEmpty && this.Visible);

            if (CustomValidator_ValidateIfFeladatIsNotEmpty.Enabled)
            {
                string felelosHiddenClientID = fcstbxFelelos.HiddenField.ClientID;
                string felelosControlClientID = fcstbxFelelos.TextBox.ClientID;

                if (!String.IsNullOrEmpty(FelelosControlID))
                {
                    Control felelosControl = FindControl(FelelosControlID);
                    if (felelosControl != null)
                    {
                        if (felelosControl is Component_CsoportTextBox)
                        {
                            felelosControlClientID = ((Component_CsoportTextBox)felelosControl).TextBox.ClientID;
                            felelosHiddenClientID = ((Component_CsoportTextBox)felelosControl).HiddenField.ClientID;
                        }
                        else if (felelosControl is Component_FelhasznaloCsoportTextBox)
                        {
                            felelosControlClientID = ((Component_FelhasznaloCsoportTextBox)felelosControl).TextBox.ClientID;
                            felelosHiddenClientID = ((Component_FelhasznaloCsoportTextBox)felelosControl).HiddenField.ClientID;
                        }
                    }
                }

                RegisterValidateIfFeladatIsNotEmptyJavaScript(felelosHiddenClientID);

                // ha nincs megadva controltovalidate, akkor javascript hiba, mert nem tudja megjeleníteni a "buborékot"
                ScriptManager.RegisterExpandoAttribute(CustomValidator_ValidateIfFeladatIsNotEmpty, CustomValidator_ValidateIfFeladatIsNotEmpty.ClientID
                    , "controltovalidate", felelosControlClientID, true);
            }

        }
        else
        {
            CustomValidator_ValidateIfFeladatIsNotEmpty.Enabled = false;
        }

        if (!String.IsNullOrEmpty(FeladatId))
        {
            ExecParam xpm = UI.SetExecParamDefault(Page);
            EREC_HataridosFeladatok erec_feladat = GetBusinessObject();
            if (erec_feladat != null)
            {
                if (HataridosFeladatok.Modosithato(xpm, erec_feladat))
                {
                    UI.SetImageButtonStyleToEnabled(imgModify);
                    imgModify.Enabled = true;
                }
                else
                {
                    UI.SetImageButtonStyleToDisabled(imgModify);
                    imgModify.Enabled = false;
                    //LZS - BLG_2956 - Ha nem módosítható, akkor nem is delegálható.
                    imgDelegate.Enabled = false;

                }

                #region BLG_2956
                //LZS - Delegált feladat esetén a módosítás és a delegálás ikonokat letiltom, valamint a további felelősök checkboxot és panelt is.
                if (IsDelegate || !(HataridosFeladatok.Modosithato(xpm, erec_feladat)))
                {
                    imgModify.Enabled = imgDelegate.Enabled = false;
                    cbAddTovabbiFelelosok.Enabled = false;
                    fmspTovabbiFelelosok.PanelEnabled = false;
                }
                else
                {
                    imgDelegate.Enabled = true;
                    cbAddTovabbiFelelosok.Enabled = true;
                    fmspTovabbiFelelosok.PanelEnabled = true;
                }

                //LZS - Ha nincs meg a jogosultsága valakinek a delegáláshoz, akkor a gomb számára nem is látható
                if (!FunctionRights.GetFunkcioJog(Page, "Feladat" + CommandName.Delegate))
                {
                    imgDelegate.Visible = false;
                }
                #endregion
            }
        }


        if (ObjektumValaszto.Visible)
        {
            ftbFeladat.ObjektumTipusFilter = ObjektumValaszto.GetSelected_Obj_Type();
            if (Command != CommandName.View)
            {
                ftbFeladat.ReadOnly = String.IsNullOrEmpty(ftbFeladat.ObjektumTipusFilter);
            }
        }
        else
        {
            ftbFeladat.ObjektumTipusFilter = String.Empty;
            FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
            if (fm != null)
            {
                if (fm.IsValidObjektumType())
                {
                    ftbFeladat.ObjektumTipusFilter = fm.ObjektumType;
                }
            }
        }

        if (SmallSize)
        {
            SetSmallSize();
        }

        ftbFeladat.ImageButton_Reset.Visible = false;

        if (HataridosFeladatok.IsDisabled(Page))
        {
            ObjektumPanelVisible = false;
        }

        if (!HataridosFeladatok.IsDisabled(Page) || !IsOnFeladatTab || Command != CommandName.New)
        {
            TabFooter1.Visible = false;

            //BLG_2956 - LZS - Delegálás parancs esetén beállítom a hiddenfieldet „1”-re.
            if (Command == CommandName.Delegate)
            {
                isDelegate.Value = "1";
            }
        }

        //BLG_2956 - LZS - Ha delegálás-ban vagyunk, akkor megjelenítem a footert, és annak a megfelelő gombjait.
        if (isDelegate.Value == "1")
        {
            ShowTabFooterOnDelegation();
        }
    }

    private void SetSmallSize()
    {
        txtKezelesiFeljegyzesLeiras.Width = txtNote.Width = Unit.Pixel(560);
        tdFeladt.Style[HtmlTextWriterStyle.Width] = "180px";
        tdKezelesiFeljegyzesTipus.Style[HtmlTextWriterStyle.Width] = "180px";
        ktddKezelesiFeljegyzesTipus.Width = Unit.Pixel(180);
        ftbFeladat.TextBox.Style[HtmlTextWriterStyle.Width] = "195px";
        tdFeladat.Style[HtmlTextWriterStyle.Width] = "240px";
        //ktddAllapot.DropDownList.Style[HtmlTextWriterStyle.Width] = "202px";
        tdAllapotLabel.Style[HtmlTextWriterStyle.Width] = "70px";
        tdPrioritasLabel.Style[HtmlTextWriterStyle.Width] = "70px";
        tdLabelKezeles.Style[HtmlTextWriterStyle.Width] = "60px";

    }

    private void ProcessQueryString()
    {
        string commandName = Request.QueryString.Get(QueryStringVars.Command);

        if (!String.IsNullOrEmpty(commandName) && ViewState["Command"] == null)
        {
            Command = commandName;
        }
    }

    private bool IsInitalized
    {
        get
        {
            object o = ViewState["IsInitalized"];
            if (o != null)
                return (bool)o;
            return false;
        }
        set
        {
            ViewState["IsInitalized"] = true;
        }
    }

    //LZS - LINQ-s Max() miért nem megy???
    //'List<int>' does not contain a definition for 'Max' and no extension method 'Max' accepting a first argument of type 'List<int>' could be found(are you missing a using directive or an assembly reference?)
    public string FindMax(List<string> list)
    {
        if (list.Count == 0)
        {
            return "";
        }

        int num = 0;
        int max = int.MinValue;

        foreach (string item in list)
        {
            num = int.Parse(item);
            if (num > max)
            {
                max = num;
            }
        }
        return max.ToString();
    }

    private void InitForm()
    {
        if (!IsInitalized)
        {
            Authentication.CheckLogin(Page);  //nekrisz CR 2985

            ProcessQueryString();
            ktListPrioritas.FillAndSetSelectedValue(KodTarak.FELADAT_Prioritas.kcsNev, KodTarak.FELADAT_Prioritas.DefaultValue, errorPanel);


            #region LZS - BLG_1632
            List<string> prioList = new List<string>();
            string fprior = Rendszerparameterek.Get(Page, Rendszerparameterek.FELADAT_MANUALIS_PRIORITASOK, false);
            if (fprior != null)
                prioList = fprior.Split(',').ToList();

            foreach (ListItem item in ktListPrioritas.DropDownList.Items)
            {
                item.Enabled = prioList.Contains(item.Value);
            }

            if (!prioList.Contains(KodTarak.FELADAT_Prioritas.DefaultValue))
            {
                ktListPrioritas.SetSelectedValue(FindMax(prioList));
            }

            #endregion

            ktListFeladat.FillAndSetSelectedValue(KodTarak.FELADAT_TIPUS.kcsNev, KodTarak.FELADAT_TIPUS.Megjegyzes, errorPanel);
            ktddKezelesiFeljegyzesTipus.FillAndSetEmptyValue(KodTarak.FELADAT_ALTIPUS.kcsNev, ErrorPanel);
            if (Command != CommandName.New)
            {
                ktddKezelesiFeljegyzesTipus.DropDownList.Items.Add(new ListItem(Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.FELADAT_ALTIPUS.kcsNev, KodTarak.FELADAT_ALTIPUS.EloadoiMunkanaploMegjegyzes, Page), KodTarak.FELADAT_ALTIPUS.EloadoiMunkanaploMegjegyzes));
            }
            ktddAllapot.FillDropDownList(KodTarak.FELADAT_ALLAPOT.kcsNev, ErrorPanel);
            ktddAllapot.SelectedValue = KodTarak.FELADAT_ALLAPOT.Uj;
            HataridosFeladatok.Forras.FillRadioButtonList(rblistForras);
            IsInitalized = true;
        }
    }

    public void SetControls()
    {
        //BLG_2950
        if (this.PanelEnabled)
            ClearControls();

        switch (Command)
        {
            case CommandName.New:
                SetNewControls();
                break;
            case CommandName.Modify:
                SetModifyControls();
                break;
            case CommandName.View:
                SetViewControls();
                break;
            case CommandName.Delegate:
                SetDelegateControls();
                break;

        }

        UpdateIfVisible();
    }

    private void SetViewControls()
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            ktListFeladat.ReadOnly = true;
            ktddKezelesiFeljegyzesTipus.ReadOnly = true;
            txtKezelesiFeljegyzesLeiras.ReadOnly = true;
            cbMemo.Enabled = false;
            fcstbxFelelos.ReadOnly = true;
            ktddAllapot.ReadOnly = true;
            ftbFeladat.ReadOnly = true;
            ccIntezkedes.ReadOnly = true;
            ktListPrioritas.ReadOnly = true;
            txtNote.ReadOnly = true;
            txtMegoldas.ReadOnly = true;
            rblistForras.Enabled = false;
            ftbxKiado.ReadOnly = true;
            cstbxKiadoSzervezete.ReadOnly = true;
            ccKezdesiIdo.ReadOnly = true;
            ccLezarasDatuma.ReadOnly = true;
            ObjektumValaszto.ReadOnly = true;
            imgSave.Visible = false;
            ObjektumPanelVisible = true;
            imgSave.Visible = false;
            imgModify.Visible = true;
            imgDelegate.Visible = true;
            TabFooter1.Visible = true;

            //LZS - BLG_2956
            lblDelegate.Visible = false;
        }
    }

    #region BLG_2950
    public void SetReadOnly()
    {
        ktListFeladat.ReadOnly = true;
        ktddKezelesiFeljegyzesTipus.ReadOnly = true;
        txtKezelesiFeljegyzesLeiras.ReadOnly = true;
        cbMemo.Enabled = false;
        fcstbxFelelos.ReadOnly = true;
        ktddAllapot.ReadOnly = true;
        ftbFeladat.ReadOnly = true;
        ccIntezkedes.ReadOnly = true;
        ktListPrioritas.ReadOnly = true;
        txtNote.ReadOnly = true;
        txtMegoldas.ReadOnly = true;
        rblistForras.Enabled = false;
        ftbxKiado.ReadOnly = true;
        cstbxKiadoSzervezete.ReadOnly = true;
        ccKezdesiIdo.ReadOnly = true;
        ccLezarasDatuma.ReadOnly = true;
        ObjektumValaszto.ReadOnly = true;
        imgSave.Visible = false;
        ObjektumPanelVisible = true;
        imgSave.Visible = false;
        imgModify.Visible = false;
        imgDelegate.Visible = false;
        TabFooter1.Visible = false;

        //LZS - BLG_2956
        lblDelegate.Visible = false;

    }
    #endregion

    private void SetModifyControls()
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            if (!IsMemo)
            {
                cbMemo.Enabled = false;
            }

            if (IsFeladat)
            {
                ktListFeladat.ReadOnly = true;
                //ftbFeladat.ReadOnly = String.IsNullOrEmpty(ftbFeladat.ObjektumTipusFilter);
                if (IsModosithato)
                {
                    if (IsMyKiadott)
                    {
                    }
                    else if (IsMeFelelos)
                    {
                        cbMemo.Enabled = false;
                        fcstbxFelelos.ReadOnly = true;
                        //ktddAllapot.ReadOnly = true;
                        ftbFeladat.ReadOnly = true;
                        ccIntezkedes.ReadOnly = true;
                        ktListPrioritas.ReadOnly = true;
                        rblistForras.Enabled = false;
                        ccKezdesiIdo.ReadOnly = true;
                        ObjektumValaszto.ReadOnly = true;
                    }
                }
                else
                {
                    SetViewControls();
                }
            }
            else
            {
                ktddKezelesiFeljegyzesTipus.ReadOnly = true;
                txtKezelesiFeljegyzesLeiras.ReadOnly = true;
                //cbMemo.Enabled = false;
                ktListFeladat.ReadOnly = false;
                ObjektumValaszto.ReadOnly = true;
            }

            //LZS - BLG_2956
            imgSave.Visible = true;
            imgModify.Visible = false;
            imgDelegate.Visible = false;
            lblDelegate.Visible = false;


            TabFooter1.Visible = true;
        }
    }

    private void SetNewControls()
    {
        rowMegoldas.Visible = false;
        rowKezdesLezaras.Visible = false;
        rowKiado.Visible = false;
        rowForrasKiadasDatuma.Visible = false;
        imgSave.Visible = false;
        imgModify.Visible = false;
        imgDelegate.Visible = false;
        ccIntezkedes.EnableCompareValidation();
        TabFooter1.Visible = true;
        //ftbFeladat.ReadOnly = true;

        rowTovabbiFelelosok.Visible = true;
        rowAddTovabbiFelelosok.Visible = true;

        //LZS - BLG_2956
        lblDelegate.Visible = false;
    }

    #region BLG_2956
    //LZS - Beállítja a delegálás során használatos mezőket írhatóvá, a többit ReadOnly-ra teszi.
    private void SetDelegateControls()
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            if (!IsMemo)
            {
                cbMemo.Enabled = false;
            }

            if (IsFeladat)
            {
                ktListFeladat.ReadOnly = true;

                if (IsModosithato)
                {
                    txtKezelesiFeljegyzesLeiras.ReadOnly =
                    ObjektumValaszto.ReadOnly =
                    ftbFeladat.ReadOnly =
                    ktddAllapot.ReadOnly =
                    ftbxKiado.ReadOnly =
                    cstbxKiadoSzervezete.ReadOnly =
                    txtMegoldas.ReadOnly =
                    ccKezdesiIdo.ReadOnly =
                    ccLezarasDatuma.ReadOnly =
                    ccKiadasDatuma.ReadOnly =
                    ktddKezelesiFeljegyzesTipus.ReadOnly =
                    rowAddTovabbiFelelosok.Visible =
                    fcstbxFelelos.Visible =
                    cbAddTovabbiFelelosok.Visible =
                    fcstbxFelelos.SajatSzervezetDolgozoiVagyOsszes = true;

                    rblistForras.Enabled =
                    fcstbxFelelos.ReadOnly = false;
                }
            }

            imgSave.Visible = false;
            imgModify.Visible = false;
            imgDelegate.Visible = false;
            ShowTabFooterOnDelegation();

        }
    }

    //BLG_2956 - LZS - A tabfooter gombjainak láthatóságát állítjuk, valamint a delegálás feliratot.
    private void ShowTabFooterOnDelegation()
    {
        lblDelegate.Visible =
        TabFooter1.Visible =
        TabFooter1.ImageButton_Save.Visible =
            TabFooter1.ImageButton_Cancel.Visible = true;
    }

    //BLG_2956 - LZS - A tabfooter gombjainak lekezelése.
    //A művelethez szükséges a megfelelő funkciógomb.
    //Save - Mentés esetén az eredeti feladat rekordja delegált állapotba kerül, és létrejön egy új feladat a delegált nevére, a form-on átírt mezők alapján.
    //Mégse gombnál csak elrejtjük a tabfootert és az ikonokat.
    private void TabFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, "Feladat" + CommandName.Delegate))
        {
            if (e.CommandName == CommandName.Save)
            {
                #region Régi feladat delegáltra állítása, és mentése
                EREC_HataridosFeladatok eredetiFeladat = GetFeladatById();

                bool isError = this.SaveAs(eredetiFeladat);

                if (!isError)
                {
                    //RaiseUpdated(this, EventArgs.Empty);
                    //Command = CommandName.View;
                }
                #endregion

                #region Új delegált feladat létrehozása
                ktddAllapot.SelectedValue = KodTarak.FELADAT_ALLAPOT.Uj;
                ftbxKiado.Id_HiddenField = eredetiFeladat.Felhasznalo_Id_Felelos;

                string resultId;

                isError = this.Create(ParentFeladatId, true, out resultId);
                if (!isError)
                {
                    FeladatId = resultId;
                    FeleadatVersion = "1";
                    RaiseCreated(this, EventArgs.Empty);
                    Command = CommandName.View;
                }
                #endregion

                HideTabFooterOnDelegation();
            }

            if (e.CommandName == CommandName.Cancel)
            {
                HideTabFooterOnDelegation();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    //BLG_2956 - LZS - A tabfooter gombjainak láthatóságát állítjuk, valamint a delegálás feliratot.
    private void HideTabFooterOnDelegation()
    {
        lblDelegate.Visible =
        TabFooter1.Visible = TabFooter1.ImageButton_Save.Visible =
                    TabFooter1.ImageButton_Cancel.Visible = false;

        Command = CommandName.View;
        isDelegate.Value = "0";
    }
    #endregion

    private void SetTovabbiFelelosok()
    {
        RegisterSetTovabbiFelelosokVisibilityJavaScript();
        cbAddTovabbiFelelosok.Attributes.Add("onclick", "setTovabbiFelelosokVisibility()");

        if (cbAddTovabbiFelelosok.Checked)
        {
            rowTovabbiFelelosok.Style.Add("display", "");
        }
        else
        {
            rowTovabbiFelelosok.Style.Add("display", "none");
        }
    }

    protected void cbFeladat_CheckedChanged(object sender, EventArgs e)
    {
        HandleTipusChanged();
    }

    protected void ktListFeladat_SelectedIndexChanged(object sender, EventArgs e)
    {
        HandleTipusChanged();
    }

    protected void HandleTipusChanged()
    {
        SetFeladatPanelVisiblity(IsFeladat);
    }

    protected void SetFeladatPanelVisiblity(bool visible)
    {
        FeladatPanel.Visible = visible;
        UpdateIfVisible();
    }

    private bool IsEmpty //CR3075 - MoPe
    {
        get
        {
            if (IsFeladat)
            {
                if (String.IsNullOrEmpty(txtKezelesiFeljegyzesLeiras.Text)
                    && String.IsNullOrEmpty(ftbFeladat.Id_HiddenField) && String.IsNullOrEmpty(ktddKezelesiFeljegyzesTipus.SelectedValue))
                    return true;
            }
            else
            {
                if (String.IsNullOrEmpty(txtKezelesiFeljegyzesLeiras.Text) && String.IsNullOrEmpty(ktddKezelesiFeljegyzesTipus.SelectedValue))
                    return true;
            }

            return false;
        }
    }

    private int _isfeladat = -1;
    public bool IsFeladat
    {
        get
        {
            return (ktListFeladat.SelectedValue == KodTarak.FELADAT_TIPUS.Feladat);
        }
        set
        {
            if (ktListFeladat != null)
            {
                if (ktListFeladat.SelectedValue != (value ? KodTarak.FELADAT_TIPUS.Feladat : KodTarak.FELADAT_TIPUS.Megjegyzes))
                {
                    InitForm();
                    ktListFeladat.SelectedValue = (value ? KodTarak.FELADAT_TIPUS.Feladat : KodTarak.FELADAT_TIPUS.Megjegyzes);
                    HandleTipusChanged();
                }
            }
            else
            {
                _isfeladat = value ? 1 : 0;
            }
        }
    }

    private bool IsMyKiadott
    {
        get
        {
            if (String.IsNullOrEmpty(ftbxKiado.Id_HiddenField))
                return false;
            ExecParam xpm = UI.SetExecParamDefault(Page);
            if (xpm.Felhasznalo_Id == ftbxKiado.Id_HiddenField)
                return true;
            return false;
        }
    }

    private bool IsDelegate
    {
        get
        {
            if (ktddAllapot.SelectedValue == KodTarak.FELADAT_ALLAPOT.Delegalt)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private bool IsMeFelelos
    {
        get
        {
            if (String.IsNullOrEmpty(fcstbxFelelos.Id_HiddenField))
                return false;
            ExecParam xpm = UI.SetExecParamDefault(Page);
            if (xpm.Felhasznalo_Id == fcstbxFelelos.Id_HiddenField)
                return true;
            return false;
        }
    }

    private bool IsModosithato
    {
        get
        {
            return true;
        }
    }

    public bool IsMemo
    {
        get
        {
            return cbMemo.Checked;
        }
        set
        {
            cbMemo.Checked = value;
        }
    }

    public EREC_HataridosFeladatok GetBusinessObject()
    {
        if (IsEmpty || !IsVisible)
        {
            return null;
        }
        EREC_HataridosFeladatok erec_HataridosFeladat = new EREC_HataridosFeladatok();
        erec_HataridosFeladat.Updated.SetValueAll(false);
        erec_HataridosFeladat.Base.Updated.SetValueAll(false);
        erec_HataridosFeladat.Tipus = ktListFeladat.SelectedValue;
        erec_HataridosFeladat.Updated.Tipus = true;
        erec_HataridosFeladat.Altipus = ktddKezelesiFeljegyzesTipus.SelectedValue;
        erec_HataridosFeladat.Updated.Altipus = true;
        erec_HataridosFeladat.Leiras = txtKezelesiFeljegyzesLeiras.Text;
        erec_HataridosFeladat.Updated.Leiras = true;
        erec_HataridosFeladat.Memo = cbMemo.Checked ? Constants.Database.Yes : Constants.Database.No;
        erec_HataridosFeladat.Updated.Memo = true;
        erec_HataridosFeladat.Obj_Id = ObjektumValaszto.GetSelected_Obj_Id();
        erec_HataridosFeladat.Updated.Obj_Id = true;
        erec_HataridosFeladat.Obj_type = ObjektumValaszto.GetSelected_Obj_Type();
        erec_HataridosFeladat.Updated.Obj_type = true;
        erec_HataridosFeladat.Azonosito = ObjektumValaszto.GetSelected_Obj_Azonosito();
        erec_HataridosFeladat.Updated.Azonosito = true;
        if (IsFeladat)
        {
            SetFeladatFelelos(erec_HataridosFeladat);
            erec_HataridosFeladat.Allapot = ktddAllapot.SelectedValue;
            erec_HataridosFeladat.Updated.Allapot = (ktddAllapot.SelectedValue != ElozoFeladatAllapot);
            erec_HataridosFeladat.Funkcio_Id_Inditando = ftbFeladat.Id_HiddenField;
            erec_HataridosFeladat.Updated.Funkcio_Id_Inditando = true;
            erec_HataridosFeladat.IntezkHatarido = ccIntezkedes.Text;
            erec_HataridosFeladat.Updated.IntezkHatarido = true;
            erec_HataridosFeladat.Prioritas = ktListPrioritas.SelectedValue;
            erec_HataridosFeladat.Updated.Prioritas = true;
            erec_HataridosFeladat.LezarasPrioritas = ktListPrioritas.SelectedValue;
            erec_HataridosFeladat.Updated.LezarasPrioritas = true;
            erec_HataridosFeladat.Base.Note = txtNote.Text;
            erec_HataridosFeladat.Base.Updated.Note = true;
            erec_HataridosFeladat.Megoldas = txtMegoldas.Text;
            erec_HataridosFeladat.Updated.Megoldas = true;
        }
        else
        {
            //if (Command == CommandName.New)
            //{
            //    erec_HataridosFeladat.Allapot = KodTarak.FELADAT_ALLAPOT.Uj;
            //    erec_HataridosFeladat.Updated.Allapot = true;
            //}
            //else
            //{
            //    erec_HataridosFeladat.Allapot = ktddAllapot.SelectedValue;
            //}
        }

        #region Default


        if (Command == CommandName.New)
        {
            if (IsFeladat)
            {
                erec_HataridosFeladat.Forras = HataridosFeladatok.Forras.Kezi;
                erec_HataridosFeladat.Updated.Forras = true;

                ExecParam xpm = UI.SetExecParamDefault(Page);

                erec_HataridosFeladat.Felhasznalo_Id_Kiado = xpm.Felhasznalo_Id;
                erec_HataridosFeladat.Updated.Felhasznalo_Id_Kiado = true;

                erec_HataridosFeladat.Csoport_Id_Kiado = xpm.FelhasznaloSzervezet_Id;
                erec_HataridosFeladat.Updated.Csoport_Id_Kiado = true;
            }
        }
        else
        {
            erec_HataridosFeladat.Forras = rblistForras.SelectedValue;
            erec_HataridosFeladat.Felhasznalo_Id_Kiado = ftbxKiado.Id_HiddenField;
            erec_HataridosFeladat.Csoport_Id_Kiado = cstbxKiadoSzervezete.Id_HiddenField;
        }

        erec_HataridosFeladat.Base.Ver = FeleadatVersion;
        erec_HataridosFeladat.Base.Updated.Ver = true;

        erec_HataridosFeladat.Base.Letrehozo_id = FeladatLetrehozoId;
        erec_HataridosFeladat.Base.LetrehozasIdo = FeladatLetrehozoasiIdo;

        #endregion

        return erec_HataridosFeladat;
    }

    private void SetFeladatFelelos(EREC_HataridosFeladatok erec_HataridosFeladat)
    {
        if (!String.IsNullOrEmpty(FelelosControlID))
        {
            Control felelosControl = FindControl(FelelosControlID);
            if (felelosControl != null)
            {
                if (felelosControl is Component_CsoportTextBox)
                {
                    erec_HataridosFeladat.Felhasznalo_Id_Felelos = ((Component_CsoportTextBox)felelosControl).Id_HiddenField;
                    erec_HataridosFeladat.Updated.Felhasznalo_Id_Felelos = true;
                }
                if (felelosControl is Component_FelhasznaloCsoportTextBox)
                {
                    erec_HataridosFeladat.Felhasznalo_Id_Felelos = ((Component_FelhasznaloCsoportTextBox)felelosControl).Id_HiddenField;
                    erec_HataridosFeladat.Updated.Felhasznalo_Id_Felelos = true;
                }
            }
        }
        else
        {
            erec_HataridosFeladat.Felhasznalo_Id_Felelos = fcstbxFelelos.Id_HiddenField;
            erec_HataridosFeladat.Updated.Felhasznalo_Id_Felelos = true;
        }
    }

    public override Control FindControl(string id)
    {
        Control c = base.FindControl(id);
        if (c == null)
        {
            c = Parent.FindControl(id);
        }
        return c;
    }

    public EREC_HataridosFeladatok Megtekint()
    {
        return SetFeladatById(true);
    }

    public EREC_HataridosFeladatok SetFeladatById()
    {
        return SetFeladatById(false);
    }

    #region BLG_2956
    //BLG_2956 - LZS - Betölti a kiválassztott feladatot, de nem írja be a formra.
    private EREC_HataridosFeladatok GetFeladatById()
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = FeladatId;

            Result result = new Result();
            result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_HataridosFeladatok erec_hataridosFeladat = (EREC_HataridosFeladatok)result.Record;

                    return erec_hataridosFeladat;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }
    #endregion

    private EREC_HataridosFeladatok SetFeladatById(bool isMegtekintes)
    {
        if (!String.IsNullOrEmpty(FeladatId))
        {
            EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = FeladatId;

            Result result = new Result();
            if (isMegtekintes)
            {
                result = service.Megtekint(execParam);
                Command = CommandName.View;
            }
            else
                result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_HataridosFeladatok erec_hataridosFeladat = (EREC_HataridosFeladatok)result.Record;

                    this.SetFeladatByBusinessObject(erec_hataridosFeladat);

                    return erec_hataridosFeladat;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }

    public void SetFeladatByBusinessObject(EREC_HataridosFeladatok erec_HataridosFeladat)
    {
        this.InitForm();
        this.Clear();
        if (erec_HataridosFeladat != null)
        {
            IsFeladat = (erec_HataridosFeladat.Tipus == KodTarak.FELADAT_TIPUS.Feladat) ? true : false;
            ktddKezelesiFeljegyzesTipus.SelectedValue = erec_HataridosFeladat.Altipus;
            txtKezelesiFeljegyzesLeiras.Text = erec_HataridosFeladat.Leiras;
            cbMemo.Checked = (erec_HataridosFeladat.Memo == Constants.Database.Yes) ? true : false;
            if (!String.IsNullOrEmpty(erec_HataridosFeladat.Obj_Id) && !String.IsNullOrEmpty(erec_HataridosFeladat.Obj_type))
            {
                ObjektumValaszto.SetObjektum(erec_HataridosFeladat.Obj_Id, erec_HataridosFeladat.Obj_type, erec_HataridosFeladat.Azonosito);
            }
            ftbFeladat.ObjektumTipusFilter = erec_HataridosFeladat.Obj_type;
            if (IsFeladat)
            {
                try
                {
                    ktddAllapot.SelectedValue = erec_HataridosFeladat.Allapot;
                    ElozoFeladatAllapot = erec_HataridosFeladat.Allapot;
                }
                catch
                {
                }
                ftbFeladat.Id_HiddenField = erec_HataridosFeladat.Funkcio_Id_Inditando;
                ftbFeladat.SetFunkcioTextBoxById(ErrorPanel);
                ccIntezkedes.Text = erec_HataridosFeladat.IntezkHatarido;
                try
                {
                    ktListPrioritas.SelectedValue = erec_HataridosFeladat.Prioritas;
                }
                catch
                {
                }
                txtNote.Text = erec_HataridosFeladat.Base.Note;
                txtMegoldas.Text = erec_HataridosFeladat.Megoldas;
                try
                {
                    rblistForras.SelectedValue = erec_HataridosFeladat.Forras;
                }
                catch
                {
                }
                fcstbxFelelos.Id_HiddenField = erec_HataridosFeladat.Felhasznalo_Id_Felelos;
                fcstbxFelelos.SetCsoportTextBoxById(ErrorPanel);
                //erec_HataridosFeladat.Csoport_Id_Felelos
                ftbxKiado.Id_HiddenField = erec_HataridosFeladat.Felhasznalo_Id_Kiado;
                ftbxKiado.SetCsoportTextBoxById(ErrorPanel);
                cstbxKiadoSzervezete.Id_HiddenField = erec_HataridosFeladat.Csoport_Id_Kiado;
                cstbxKiadoSzervezete.SetCsoportTextBoxById(ErrorPanel);
                ccKezdesiIdo.Text = erec_HataridosFeladat.KezdesiIdo;
                ccLezarasDatuma.Text = erec_HataridosFeladat.LezarasDatuma;
                ccKiadasDatuma.Text = erec_HataridosFeladat.Base.LetrehozasIdo;
            }


            //FeladatId = erec_HataridosFeladat.Id;
            FeleadatVersion = erec_HataridosFeladat.Base.Ver;
            FeladatLetrehozoId = erec_HataridosFeladat.Base.Letrehozo_id;
            FeladatLetrehozoasiIdo = erec_HataridosFeladat.Base.LetrehozasIdo;

            SetControls();

            this.Update();
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(errorPanel, "Hiba!", "Feladat üzleti objektum üres!");
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
        }
    }

    private void Clear()
    {
        ktddKezelesiFeljegyzesTipus.DropDownList.SelectedIndex = -1;
        cbMemo.Checked = false;
        ktListFeladat.SelectedValue = IsFeladat ? KodTarak.FELADAT_TIPUS.Feladat : KodTarak.FELADAT_TIPUS.Megjegyzes;
        txtKezelesiFeljegyzesLeiras.Text = String.Empty;
        ftbFeladat.Text = ftbFeladat.Id_HiddenField = String.Empty;
        ktddAllapot.SelectedValue = KodTarak.FELADAT_ALLAPOT.Uj;
        fcstbxFelelos.Text = fcstbxFelelos.Id_HiddenField = String.Empty;
        ftbxKiado.Text = ftbxKiado.Id_HiddenField = String.Empty;
        cstbxKiadoSzervezete.Text = ftbxKiado.Id_HiddenField = String.Empty;
        ccIntezkedes.Text = String.Empty;
        ktListPrioritas.SelectedValue = KodTarak.FELADAT_Prioritas.DefaultValue;
        txtMegoldas.Text = String.Empty;
        txtNote.Text = String.Empty;
        ccKezdesiIdo.Text = String.Empty;
        ccLezarasDatuma.Text = String.Empty;
        ccKiadasDatuma.Text = String.Empty;
        rblistForras.SelectedValue = HataridosFeladatok.Forras.Kezi;
        ObjektumValaszto.Clear();

        fmspTovabbiFelelosok.Clear();

        ClearControls();
    }

    private void ClearControls()
    {
        ktddKezelesiFeljegyzesTipus.ReadOnly = false;
        cbMemo.Enabled = true;
        ktListFeladat.ReadOnly = false;
        txtKezelesiFeljegyzesLeiras.ReadOnly = false;
        ftbFeladat.ReadOnly = false;
        ktddAllapot.ReadOnly = false;
        ccIntezkedes.ReadOnly = false;
        fcstbxFelelos.ReadOnly = false;
        ktListPrioritas.ReadOnly = false;
        txtMegoldas.ReadOnly = false;
        txtNote.ReadOnly = false;
        ObjektumValaszto.ReadOnly = false;
    }

    public void Update()
    {
        if (!IsVisible) return;

        MainPanel.Visible = true;
        MainUpdatePanel.Update();
    }

    public void UpdateIfVisible()
    {
        if (MainPanel.Visible)
        {
            MainUpdatePanel.Update();
        }
    }

    public void Reset()
    {
        FeladatId = String.Empty;
        FeleadatVersion = String.Empty;
        MainPanel.Visible = false;
        MainUpdatePanel.Update();
    }

    public void New(string ObjektumId, string ObjektumType)
    {
        Clear();
        Update();
        this.SetObjektum(ObjektumId, ObjektumType);
        Command = CommandName.New;
    }

    private EREC_HataridosFeladatokService _service = null;

    private EREC_HataridosFeladatokService Service
    {
        get
        {
            if (_service == null)
                _service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            return _service;
        }
    }

    public bool Save()
    {
        if (String.IsNullOrEmpty(FeladatId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(ErrorPanel);
            return false;
        }
        else
        {
            EREC_HataridosFeladatok erec_feladat = GetBusinessObject();
            if (erec_feladat != null)
            {
                ExecParam xpm = UI.SetExecParamDefault(Page);
                xpm.Record_Id = FeladatId;
                Result res = Service.Update(xpm, erec_feladat);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
                }
                else
                {
                    IncrementFeladatVersion();
                    ElozoFeladatAllapot = erec_feladat.Allapot;
                }
                return res.IsError;
            }
            return false;
        }

    }

    #region BLG_2956
    //LZS - BLG_2956 A delegálás során átállítja az eredeti feladat rekordjának állapot mezőjét Delegált [7] státuszúra.
    public bool SaveAs(EREC_HataridosFeladatok erec_feladat)
    {
        if (String.IsNullOrEmpty(FeladatId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(ErrorPanel);
            return false;
        }
        else
        {
            if (erec_feladat != null)
            {
                ExecParam xpm = UI.SetExecParamDefault(Page);
                xpm.Record_Id = FeladatId;
                erec_feladat.Allapot = KodTarak.FELADAT_ALLAPOT.Delegalt;

                Result res = Service.Update(xpm, erec_feladat);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
                }
                else
                {
                    IncrementFeladatVersion();
                    ElozoFeladatAllapot = erec_feladat.Allapot;
                }
                return res.IsError;
            }
            return false;
        }

    }
    #endregion

    public bool Create(string ParentFeladatId, bool isDelegate, out string resultId)
    {
        EREC_HataridosFeladatok erec_feladat = GetBusinessObject();

        #region BLG_2956
        //LZS - Kapott egy plusz „isDelegate” paramétert, mert delegálás során létrejövő új feladatnál át kell adni a kiadó-t is.
        if (isDelegate)
        {
            erec_feladat.Felhasznalo_Id_Kiado = ftbxKiado.Id_HiddenField;
            erec_feladat.Updated.Felhasznalo_Id_Kiado = true;
        }
        #endregion

        resultId = String.Empty;
        if (erec_feladat != null)
        {
            if (!String.IsNullOrEmpty(ParentFeladatId))
            {
                erec_feladat.HataridosFeladat_Id = ParentFeladatId;
                erec_feladat.Updated.HataridosFeladat_Id = true;
            }
            ExecParam xpm = UI.SetExecParamDefault(Page);
            Result res = null;

            if (cbAddTovabbiFelelosok.Checked && fmspTovabbiFelelosok.Count > 0)
            {
                res = InsertTaskForMoreUsers(xpm, erec_feladat);
            }
            else
            {
                res = Service.Insert(xpm, erec_feladat);
            }
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, res);
                resultId = String.Empty;
            }
            else
            {
                resultId = res.Uid;
            }

            return res.IsError;
        }

        return false;
    }

    public Result InsertTaskForMoreUsers(ExecParam xpm, EREC_HataridosFeladatok erec_feladat)
    {
        if (erec_feladat != null && cbAddTovabbiFelelosok.Checked && fmspTovabbiFelelosok.Count > 0)
        {
            return Service.Insert_OneTaskForMoreUsers(xpm ?? UI.SetExecParamDefault(Page), erec_feladat, fmspTovabbiFelelosok.FelhasznaloIds, fmspTovabbiFelelosok.CsoportIds);
        }
        return new Result();
    }

    private event EventHandler _Updated;

    public event EventHandler Updated
    {
        add
        {
            _Updated += value;
        }
        remove
        {
            _Updated -= value;
        }
    }

    public void RaiseUpdated(object sender, EventArgs e)
    {
        if (_Updated != null)
        {
            _Updated(sender, e);
        }
    }

    private event EventHandler _Created;

    public event EventHandler Created
    {
        add
        {
            _Created += value;
        }
        remove
        {
            _Created -= value;
        }
    }

    public void RaiseCreated(object sender, EventArgs e)
    {
        if (_Created != null)
        {
            _Created(sender, e);
        }
    }

    private void ControlList()
    {
        List<Control> controls = new List<Control>();
        controls.Add(ktddKezelesiFeljegyzesTipus);
        controls.Add(cbMemo);
        controls.Add(ktListFeladat);
        controls.Add(txtKezelesiFeljegyzesLeiras);
        controls.Add(ftbFeladat);
        controls.Add(ktddAllapot);
        controls.Add(fcstbxFelelos);
        controls.Add(ftbxKiado);
        controls.Add(cstbxKiadoSzervezete);
        controls.Add(ccIntezkedes);
        controls.Add(ktListPrioritas);
        controls.Add(txtMegoldas);
        controls.Add(txtNote);
        controls.Add(ccKezdesiIdo);
        controls.Add(ccLezarasDatuma);
        controls.Add(ccKiadasDatuma);
        controls.Add(rblistForras);
        controls.Add(fmspTovabbiFelelosok);

        //ktddKezelesiFeljegyzesTipus;
        //cbMemo;
        //cbFeladat;
        //txtKezelesiFeljegyzesLeiras;
        //ftbFeladat;
        //ktddAllapot;
        //fcstbxFelelos;
        //ftbxKiado;
        //cstbxKiadoSzervezete;
        //ccIntezkedes;
        //rblistPrioritas;
        //txtMegoldas;
        //txtNote;
        //ccKezdesiIdo;
        //ccLezarasDatuma;
        //ccKiadasDatuma;
        //rblistForras;
        //ObjektumValaszto
    }

    private string _ValidationGroup = null;

    public string ValidationGroup
    {
        get
        {
            if (_ValidationGroup == null)
                return this.ClientID;

            return _ValidationGroup;
        }
        set { _ValidationGroup = value; }
    }

    public void SetValidationGroup()
    {
        if (!String.IsNullOrEmpty(ValidationGroup))
        {
            ftbFeladat.ValidationGroup = ValidationGroup;
            fcstbxFelelos.ValidationGroup = ValidationGroup;
            ftbxKiado.ValidationGroup = ValidationGroup;
            cstbxKiadoSzervezete.ValidationGroup = ValidationGroup;
            ccIntezkedes.ValidationGroup = ValidationGroup;
            ccKezdesiIdo.ValidationGroup = ValidationGroup;
            ccLezarasDatuma.ValidationGroup = ValidationGroup;
            ccKiadasDatuma.ValidationGroup = ValidationGroup;
            ObjektumValaszto.ValidationGroup = ValidationGroup;
            imgSave.ValidationGroup = ValidationGroup;
            txtKezelesiFeljegyzesLeiras.ValidationGroup = ValidationGroup;
            TabFooter1.ImageButton_Save.ValidationGroup = ValidationGroup;
        }
    }

    void imgSave_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Feladat" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            string resultId;
                            bool isError = this.Create(ParentFeladatId, false, out resultId);
                            if (!isError)
                            {
                                FeladatId = resultId;
                                FeleadatVersion = "1";
                                RaiseCreated(this, EventArgs.Empty);
                                Command = CommandName.View;
                            }
                        }
                        break;
                    case CommandName.Modify:
                        {
                            bool isError = this.Save();
                            //this.Update();
                            if (!isError)
                            {
                                RaiseUpdated(this, EventArgs.Empty);
                                Command = CommandName.View;
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            this.Reset();
        }
    }

    void imgModify_Command(object sender, CommandEventArgs e)
    {
        Command = CommandName.Modify;
    }

    #region BLG_2956
    //LZS - Delegálás gombra kattintva átírjuk a Command parancs értékét Delegate-re.
    void imgDelegate_Command(object sender, CommandEventArgs e)
    {
        Command = CommandName.Delegate;
    }
    #endregion

    void RegisterValidateIfFeladatIsNotEmptyJavaScript(string FelelosHiddenFieldClientID)
    {
        // ha feladat és ki van töltve a szöveg, akkor a felelős kötelező
        string js = String.Format(@"function ValidateIfFeladatIsNotEmpty(source, args)
{{
	var txtLeiras = $get('{0}');
	var hfFeladatId = $get('{1}');
	var hfFelelos = $get('{2}');

    args.IsValid = true;
	if (hfFelelos)
	{{
		if (txtLeiras && txtLeiras.value != '')
		{{
			args.IsValid = (hfFelelos.value != '');
		}}
		else if (hfFeladatId && hfFeladatId.value != '')
		{{
			args.IsValid = (hfFelelos.value != '');
		}}
	}}
}}", txtKezelesiFeljegyzesLeiras.TextBox.ClientID, ftbFeladat.HiddenField.ClientID, FelelosHiddenFieldClientID);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);

    }

    private void RegisterSetTovabbiFelelosokVisibilityJavaScript()
    {
        string Felhasznalo_Id = FelhasznaloProfil.FelhasznaloId(Page);
        string FelhasznaloCsoport_Nev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Felhasznalo_Id, Page);
        string tbFelelos_ClientID = fcstbxFelelos.TextBox.ClientID;
        string hfFelelos_ClientID = fcstbxFelelos.HiddenField.ClientID;
        if (!String.IsNullOrEmpty(FelelosControlID))
        {
            Control felelosControl = FindControl(FelelosControlID);
            if (felelosControl != null)
            {
                if (felelosControl is Component_CsoportTextBox)
                {
                    Component_CsoportTextBox cstb = (Component_CsoportTextBox)felelosControl;
                    tbFelelos_ClientID = cstb.TextBox.ClientID;
                    hfFelelos_ClientID = cstb.HiddenField.ClientID;
                }
                else if (felelosControl is Component_FelhasznaloCsoportTextBox)
                {
                    Component_FelhasznaloCsoportTextBox cstb = (Component_FelhasznaloCsoportTextBox)felelosControl;
                    tbFelelos_ClientID = cstb.TextBox.ClientID;
                    hfFelelos_ClientID = cstb.HiddenField.ClientID;
                }
            }
        }

        string js = @"function setTovabbiFelelosokVisibility() {
    var tovabbiFelelosok = $get('" + rowTovabbiFelelosok.ClientID + @"');
    var tb_felelos = $get('" + tbFelelos_ClientID + @"');
    var hf_felelos = $get('" + hfFelelos_ClientID + @"');
    if (tovabbiFelelosok)
    {
        var bShow = (tovabbiFelelosok.style.display == 'none');
        tovabbiFelelosok.style.display = (bShow ?  '' : 'none');
        if (hf_felelos)
        {
            if (bShow) {if (hf_felelos.value == '') {hf_felelos.value = '" + Felhasznalo_Id + @"';tb_felelos.value ='" + FelhasznaloCsoport_Nev + @"';}}
            else {if (hf_felelos.value == '" + Felhasznalo_Id + @"') {hf_felelos.value = '';tb_felelos.value = '';}}
        }
    }
    return false;
}
";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
    }

    #region ISelectableUserComponentContainer Members

    public List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        FeljegyzesCPE.Enabled = false;
        FeljegyzesCPE.Collapsed = false;
        componentList.Add(MainPanel);
        return componentList;
    }

    #endregion

    public void ShowOnlyFeljegyzes()
    {
        ShowOnlyFeljegyzes(10);
    }

    public void ShowOnlyFeljegyzes(int rows)
    {
        txtKezelesiFeljegyzesLeiras.Visible = true;
        txtKezelesiFeljegyzesLeiras.Rows = rows;

        ktListFeladat.Visible = false;
        ktddKezelesiFeljegyzesTipus.Visible = false;
        cbMemo.Visible = false;
        fcstbxFelelos.Visible = false;
        ktddAllapot.Visible = false;
        ftbFeladat.Visible = false;
        ccIntezkedes.Visible = false;
        ktListPrioritas.Visible = false;
        txtNote.Visible = false;
        txtMegoldas.Visible = false;
        rblistForras.Visible = false;
        ftbxKiado.Visible = false;
        cstbxKiadoSzervezete.Visible = false;
        ccKezdesiIdo.Visible = false;
        ccLezarasDatuma.Visible = false;
        ObjektumValaszto.Visible = false;
        ObjektumPanelVisible = false;
        imgSave.Visible = false;
        imgModify.Visible = false;
        imgDelegate.Visible = false;
        TabFooter1.Visible = false;
        labelKezelesiFeljegyzes.Visible = false;
    }

    public string GetFeljegyzes()
    {
        return txtKezelesiFeljegyzesLeiras.Text;
    }
    public bool IsEmptyFeljegyzes()
    {
        return string.IsNullOrEmpty(txtKezelesiFeljegyzesLeiras.Text);
    }
}
