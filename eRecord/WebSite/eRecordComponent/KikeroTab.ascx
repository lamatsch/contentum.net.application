<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KikeroTab.ascx.cs"
    Inherits="eRecordComponent_KikeroTab" %>

<%@ Register Src="../Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList" TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>

<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="../Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>

<%--Hiba megjelenites--%>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<%--/Hiba megjelenites--%>

<asp:UpdatePanel ID="KikeroUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">

            <%--<ajaxToolkit:CollapsiblePanelExtender ID="KikeroCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>  --%>

            <%--HiddenFields--%>

            <asp:HiddenField
                ID="HiddenField1" runat="server" />
            <asp:HiddenField
                ID="MessageHiddenField" runat="server" />
            <%--/HiddenFields--%>

            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />

            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="updatePanelKikero" runat="server" OnLoad="updatePanelKikero_Load">
                            <ContentTemplate>
                                <uc1:SubListHeader ID="SubListHeaderKikero" runat="server" />
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeKikero" runat="server" TargetControlID="panelKikero"
                                    CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeKikero" CollapseControlID="btnCpeKikero"
                                    ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
                                    ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="btnCpeKikero"
                                    ExpandedSize="0" ExpandedText="K�lcs�nz�sek" CollapsedText="K�lcs�nz�sek">
                                </ajaxToolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="panelKikero" runat="server" Visible="true" Width="100%">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top; width: 0px;">
                                                <asp:ImageButton ImageUrl="../images/hu/Grid/minus.gif" runat="server" ID="btnCpeKikero" OnClientClick="return false;" />
                                            </td>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <asp:GridView ID="gridViewKikero" runat="server" GridLines="Both" BorderWidth="1px"
                                                    BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewKikero_RowCommand"
                                                    OnPreRender="gridViewKikero_PreRender" AllowSorting="True" PagerSettings-Visible="false"
                                                    CellPadding="0" DataKeyNames="Id" AutoGenerateColumns="False"
                                                    OnRowDataBound="gridViewKikero_RowDataBound" OnSorting="gridViewKikero_Sorting" AllowPaging="true">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage"
                                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="KiadasDatuma" HeaderText="Kiad�s d�tuma" SortExpression="KiadasDatuma">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"  />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Kikero_IdNev" HeaderText="K�lcs�nz�" SortExpression="Kikero_IdNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Jovahagyo_IdNev" HeaderText="K�lcs�nz�st enged�lyez�" SortExpression="Jovahagyo_IdNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"  />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="KikerVege" HeaderText="K�lcs�nz�si hat�rid�" HeaderStyle-HorizontalAlign="Center" SortExpression="KikerVege">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"  />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="VisszavetelDatuma" HeaderText="Visszav�tel id�pontja" SortExpression="VisszavetelDatuma">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"  />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FelhasznaloCsoport_Id_VisszaveNev" HeaderText="Visszavev�" SortExpression="FelhasznaloCsoport_Id_VisszaveNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="180px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center"  />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="AllapotNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <tr class="urlapElvalasztoSor">
                <td></td>
            </tr>
            <%-- TabFooter --%>
            <uc2:TabFooter ID="TabFooter1" runat="server" />
            <%-- /TabFooter --%>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>


