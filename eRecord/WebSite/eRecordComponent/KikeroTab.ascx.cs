using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Linq;

// TODO: kitakar�tani, tiszt�tani a k�dot
// TODO: funkci�jogok
public partial class eRecordComponent_KikeroTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    //private PageView pageView = null;
    UI ui = new UI();

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //pageView = new PageView(Page, ViewState);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");
        Command = Request.QueryString.Get(CommandName.Command);
        SubListHeaderKikero.RowCount_Changed += new EventHandler(SubListHeaderKikero_RowCount_Changed);
        SubListHeaderKikero.ErvenyessegFilter_Changed += new EventHandler(SubListHeaderKikero_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (pageView == null)
        //    pageView = new PageView(Page, ViewState);

        //F� lista gombjainak l�that�s�g�nak be�ll�t�sa
        SubListHeaderKikero.NewVisible = false;
        SubListHeaderKikero.ModifyVisible = true;
        SubListHeaderKikero.ViewVisible = true;
        SubListHeaderKikero.InvalidateVisible = true;
        SubListHeaderKikero.ValidFilterVisible = true;
        SubListHeaderKikero.ValidFilterVisible = false;
        SubListHeaderKikero.ModifyVisible = false;
        SubListHeaderKikero.InvalidateVisible = false;

        TabFooter1.ImageButton_Save.Visible = true;
        TabFooter1.ImageButton_Cancel.Visible = false;
        TabFooter1.ImageButton_SaveAndClose.Visible = true;
        TabFooter1.ImageButton_SaveAndClose.Visible = false;
        TabFooter1.ImageButton_Save.Visible = false;

        //selectedRecordId kezel�se
        SubListHeaderKikero.AttachedGridView = gridViewKikero;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKikero);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        ////A f� lista adatk�t�s, ha nem postback a k�r�s
        //if (!IsPostBack)
        //{
        //    KikeroGridViewBind();
        //}

        ////scroll �llapot�nak ment�se
        //JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKikero);
        //if (IsPostBack)
        {
            // biztos�tja, hogy a tabok k�z�tti v�lt�skor is meg�rz�dj�k a kiv�lasztott sor Id-ja
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewKikero);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }

    public void ReLoadTab()
    {
        SubListHeaderKikero.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SubListHeaderKikero.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ClearForm();

        KikeroGridViewBind();
   }

    #endregion

    #region Master List

    protected void KikeroGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewKikero", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewKikero", ViewState);

        KikeroGridViewBind(sortExpression, sortDirection);
    }

    public static Result GetKikeroResult(EREC_IrattariKikeroSearch search, Page page, string parentID)
    {
        EREC_IrattariKikeroService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
        ExecParam ExecParam = UI.SetExecParamDefault(page, new ExecParam());

        search.UgyUgyirat_Id.Value = parentID;
        search.UgyUgyirat_Id.Operator = Query.Operators.equals;

        if (Rendszerparameterek.GetBoolean(page, Rendszerparameterek.KOLCSONZESFUL_ATMENETI_IRATTAR_ENABLED, false))
        {
            search.Allapot.Value = KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott + "," + KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott + "," + KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt + "," + KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Visszautasitott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett + "," + KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            search.Allapot.Operator = Query.Operators.inner;

            List<string> irattarak = new List<string> { KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id };
            irattarak.AddRange(GetAtmenetiIrattarakId(page));
            search.Irattar_Id.Value = Search.GetSqlInnerString(irattarak.ToArray());
            search.Irattar_Id.Operator = Query.Operators.inner;
        }
        else
        {
            search.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Visszautasitott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett + "," + KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            search.Allapot.Operator = Query.Operators.inner;

            search.Irattar_Id.Value = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id;
            search.Irattar_Id.Operator = Query.Operators.equals;
        }
        return service.GetAllWithExtension(ExecParam, search);
    }
    private static List<string> GetAtmenetiIrattarakId(Page page)
    {
        var irattarak = new List<string>();
        var service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        var execParam = UI.SetExecParamDefault(page, new ExecParam());
        var result = service.GetAllAtmenetiIrattar(execParam);
        if (!result.IsError)
        {
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                // BUG_12208
                var irattarId = row["Id"].ToString(); // =Partner_id_kapcsolt, set in GetAllAtmenetiIrattar
                if (!String.IsNullOrEmpty(irattarId) && !irattarak.Contains(irattarId))
                {
                    //irattarak.Add(row["Id"].ToString());
                    irattarak.Add(irattarId);
                }
            }
        }
        return irattarak;
    }

    protected void KikeroGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();

        search.OrderBy = Search.GetOrderBy("gridViewKikero", ViewState, SortExpression, SortDirection);
        SubListHeaderKikero.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

        var result = eRecordComponent_KikeroTab.GetKikeroResult(search, Page, ParentId);

        UI.GridViewFill(gridViewKikero, result, SubListHeaderKikero, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void gridViewKikero_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            //DataRowView drw = (DataRowView)e.Row.DataItem;

            //BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }

        //Lockol�s jelz�se
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void gridViewKikero_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewKikero.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewKikero.PageIndex = SubListHeaderKikero.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewKikero.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeKikero);
            KikeroGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(SubListHeaderKikero.Scrollable, cpeKikero);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKikero);

        //oldalsz�mok be�ll�t�sa
        SubListHeaderKikero.PageCount = gridViewKikero.PageCount;
        SubListHeaderKikero.PagerLabel = UI.GetGridViewPagerLabel(gridViewKikero);
    }

    void SubListHeaderKikero_RowCount_Changed(object sender, EventArgs e)
    {
        KikeroGridViewBind();
    }

    void SubListHeaderKikero_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
    }

    protected void gridViewKikero_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewKikero, selectedRowNumber, "check");

            string id = gridViewKikero.DataKeys[selectedRowNumber].Value.ToString();

            //gridViewKikero_SelectRowCommand(id);
            RefreshOnClientClicksByMasterListSelectedRow(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
        if (string.IsNullOrEmpty(id))
            return;

        SubListHeaderKikero.ViewOnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
             , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
             , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKikero.ClientID);
    }

    protected void updatePanelKikero_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KikeroGridViewBind();
                    break;
            }
        }
    }


    /// <summary>
    /// Elkuldi emailben a gridViewKikero -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEsemenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewKikero, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Esemenyek");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewKikero_Sorting(object sender, GridViewSortEventArgs e)
    {
        KikeroGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewKikero", ViewState, e.SortExpression));
    }

    #endregion

    private void ClearForm()
    {
        //TextBoxEgyedi.Text = "";
        //textNote.Text = "";
    }

}
