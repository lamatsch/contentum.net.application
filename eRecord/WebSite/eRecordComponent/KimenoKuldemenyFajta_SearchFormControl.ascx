﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KimenoKuldemenyFajta_SearchFormControl.ascx.cs"
    Inherits="eRecordComponent_KimenoKuldemenyFajta_SearchFormControl" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
<asp:UpdatePanel ID="ListaUpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rbBelfoldi" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="rbEuropai" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="rbEgyebKulfoldi" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="rbMind" EventName="CheckedChanged" />
    </Triggers>
    <ContentTemplate>
        <div class="DisableWrap">
            <asp:RadioButton runat="server" ID="rbBelfoldi" Text="Belföldi" AutoPostBack="true"
                GroupName="OrszagViszonylatKod" />
            <asp:RadioButton runat="server" ID="rbEuropai" Text="Európai" AutoPostBack="true"
                GroupName="OrszagViszonylatKod" />
            <asp:RadioButton runat="server" ID="rbEgyebKulfoldi" Text="Egyéb külföldi" AutoPostBack="true"
                GroupName="OrszagViszonylatKod" />
            <asp:RadioButton runat="server" ID="rbMind" Text="Összes viszonylat" AutoPostBack="true"
                GroupName="OrszagViszonylatKod" Checked="true" />
        </div>
        <uc4:KodtarakDropDownList ID="KimenoKuldFajta_KodtarakDropDownList" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
