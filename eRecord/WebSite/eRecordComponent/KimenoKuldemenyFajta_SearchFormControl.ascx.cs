﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;

public partial class eRecordComponent_KimenoKuldemenyFajta_SearchFormControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    private const string kcs_KIMENO_KULDEMENY_FAJTA = "KIMENO_KULDEMENY_FAJTA";

    private const string OrszagViszonylatMind = "-";

    private List<RadioButton> rbList
    {
        get { return new List<RadioButton>(new RadioButton[] { rbBelfoldi, rbEuropai, rbEgyebKulfoldi, rbMind }); }
    }

    #region Properties
    eErrorPanel errorPanel = null;

    public eErrorPanel ErrorPanel
    {
        get
        {
            if (errorPanel == null)
            {
                int i = 0;
                foreach (eErrorPanel ep in UI.FindControls<eErrorPanel>(Page.Controls))
                {
                    i++;
                    errorPanel = ep;
                    if (i > 1)
                        break;
                }
            }
            return errorPanel;
        }
        set { errorPanel = value; }
    }

    UpdatePanel errorUpdatePanel = null;

    public UpdatePanel ErrorUpdatePanel
    {
        get
        {
            if (errorUpdatePanel == null)
            {
                if (ErrorPanel != null)
                {
                    errorUpdatePanel = UI.FindParentControl<UpdatePanel>(ErrorPanel);
                }
            }
            return errorUpdatePanel;
        }
        set { errorUpdatePanel = value; }
    }

    #endregion Properties

    #region Page
    protected void Page_Init(object sender, EventArgs e)
    {
        foreach (RadioButton rb in rbList)
        {
            rb.CheckedChanged += new EventHandler(rbOrszagViszonylat_CheckedChanged);
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion Page

    public void FillDropDownList()
    {
        FillDropDownList(null);
    }
    public void FillDropDownList(string selectedValue)
    {
        Fill_KimenoKuldFajta_KodtarakDropDownList(selectedValue);
    }

    private void FillDropDownListDefault()
    {
        IsMind = true;
        this.Viszonylatkod = OrszagViszonylatMind;
        KimenoKuldFajta_KodtarakDropDownList.SetSelectedValue(String.Empty);
        Fill_KimenoKuldFajta_KodtarakDropDownList(null);
    }

    #region LoadComponentFromSearchObject
    public void LoadComponentFromSearchObject(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        if (erec_KuldKuldemenyekSearch != null)
        {
            LoadComponentFromSearchObjectField(erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta);
        }
    }

    public void LoadComponentFromSearchObjectField(Field KimenoKuldemenyFajta)
    {
        if (KimenoKuldemenyFajta != null)
        {
            // ha inner volt, akkor viszonylatra szűrtünk és nem tudjuk ismét visszaállítani
            // ha volt kiválasztva érték, a viszonylatot is megpróbáljuk megadni
            if (!String.IsNullOrEmpty(KimenoKuldemenyFajta.Value))
            {
                if (KimenoKuldemenyFajta.Operator == Query.Operators.equals)
                {
                    Fill_KimenoKuldFajta_KodtarakDropDownList(KimenoKuldemenyFajta.Value);

                    // beállítjuk a viszonylatot
                    if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(KimenoKuldemenyFajta.Value))
                    {
                        rbEuropai.Checked = true;
                    }
                    else if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(KimenoKuldemenyFajta.Value))
                    {
                        rbEgyebKulfoldi.Checked = true;
                    }
                    else if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(KimenoKuldemenyFajta.Value))
                    {
                        rbBelfoldi.Checked = true;
                    }
                    else
                    {
                        // elvileg ide nem jöhet be...
                        rbMind.Checked = true;
                    }
                }
                else if (KimenoKuldemenyFajta.Operator == Query.Operators.inner)
                {
                    // ha inner volt, akkor nem tudjuk visszatölteni!
                    Fill_KimenoKuldFajta_KodtarakDropDownList(null);

                    string[] arrayKimenoKuldemenyFajta = KimenoKuldemenyFajta.Value.Replace("'", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    // egyelőre csak az elsőre vizsgáljuk, mivel felületről nem lehet inner operátorral megadni, csak kódból,
                    // és most csak a viszonylat checkboxok alapján végezzük a szűrést
                    if (arrayKimenoKuldemenyFajta.Length > 0)
                    {
                        // beállítjuk a viszonylatot
                        if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(arrayKimenoKuldemenyFajta[0]))
                        {
                            rbEuropai.Checked = true;
                        }
                        else if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(arrayKimenoKuldemenyFajta[0]))
                        {
                            rbEgyebKulfoldi.Checked = true;
                        }
                        else if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(arrayKimenoKuldemenyFajta[0]))
                        {
                            rbBelfoldi.Checked = true;
                        }
                        else
                        {
                            // elvileg ide nem jöhet be...
                            rbMind.Checked = true;
                        }
                    }

                }
            }
            else
            {
                // ha üres volt, akkor nem tudjuk visszatölteni!
                FillDropDownListDefault();
            }
        }
    }

    #endregion LoadComponentFromSearchObject

    #region SetSearchObjectFromComponent
    public void SetSearchObjectFromComponent(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        if (erec_KuldKuldemenyekSearch != null)
        {
            SetSearchObjectFieldFromComponent(erec_KuldKuldemenyekSearch.KimenoKuldemenyFajta);
        }
    }

    public void SetSearchObjectFieldFromComponent(Field KimenoKuldemenyFajta)
    {
        if (KimenoKuldemenyFajta != null)
        {
            if (!String.IsNullOrEmpty(KimenoKuldFajta_KodtarakDropDownList.SelectedValue))
            {
                KimenoKuldemenyFajta.Value = KimenoKuldFajta_KodtarakDropDownList.SelectedValue;
                KimenoKuldemenyFajta.Operator = Query.Operators.equals;
            }
            else
            {
                if (rbBelfoldi.Checked || rbEuropai.Checked || rbEgyebKulfoldi.Checked)
                {
                    List<string> filter = GetKimenoKuldemenyFajta(this.Viszonylatkod);
                    KimenoKuldemenyFajta.Value = Search.GetSqlInnerString(filter.ToArray());
                    KimenoKuldemenyFajta.Operator = Query.Operators.inner;
                }
                else
                {
                    KimenoKuldemenyFajta.Clear();
                }
            }
        }
    }
    #endregion SetSearchObjectFromComponent


    #region Viszonylat
    
    void rbOrszagViszonylat_CheckedChanged(object sender, EventArgs e)
    {
        Fill_KimenoKuldFajta_KodtarakDropDownList(null);
    }

    private bool IsBelfoldi
    {
        get
        {
            return rbBelfoldi.Checked;
        }
        set
        {
            rbBelfoldi.Checked = value;
        }
    }

    private bool IsEuropai
    {
        get
        {
            return rbEuropai.Checked;
        }
        set
        {
            rbEuropai.Checked = value;
        }
    }

    private bool IsEgyebKulfoldi
    {
        get
        {
            return rbEgyebKulfoldi.Checked;
        }
        set
        {
            rbEgyebKulfoldi.Checked = value;
        }
    }

    private bool IsMind
    {
        get
        {
            return rbMind.Checked;
        }
        set
        {
            rbMind.Checked = value;
        }
    }

    private string Viszonylatkod
    {
        get
        {
            if (IsEuropai)
                return Constants.OrszagViszonylatKod.Europai;

            if (IsEgyebKulfoldi)
                return Constants.OrszagViszonylatKod.EgyebKulfoldi;

            if (IsMind)
                return OrszagViszonylatMind;

            return String.Empty;
        }
        set
        {
            switch (value)
            {
                case Constants.OrszagViszonylatKod.Europai:
                    IsBelfoldi = false;
                    IsEuropai = true;
                    IsEgyebKulfoldi = false;
                    IsMind = false;
                    break;
                case Constants.OrszagViszonylatKod.EgyebKulfoldi:
                    IsBelfoldi = false;
                    IsEuropai = false;
                    IsEgyebKulfoldi = true;
                    IsMind = false;
                    break;
                case OrszagViszonylatMind:
                    IsBelfoldi = false;
                    IsEuropai = false;
                    IsEgyebKulfoldi = false;
                    IsMind = true;
                    break;
                default:
                    IsBelfoldi = true;
                    IsEuropai = false;
                    IsEgyebKulfoldi = false;
                    IsMind = false;
                    break;
            }
        }
    }

    private void Fill_KimenoKuldFajta_KodtarakDropDownList(string SelectedValue)
    {
        string prevSelectedText = String.Empty;
        if (!String.IsNullOrEmpty(SelectedValue))
        {
            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(SelectedValue))
                this.Viszonylatkod = Constants.OrszagViszonylatKod.Europai;

            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(SelectedValue))
                this.Viszonylatkod = Constants.OrszagViszonylatKod.EgyebKulfoldi;

            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(SelectedValue))
                this.Viszonylatkod = String.Empty;
        }
        else
        {
            if (KimenoKuldFajta_KodtarakDropDownList.DropDownList.SelectedItem != null)
            {
                prevSelectedText = KimenoKuldFajta_KodtarakDropDownList.DropDownList.SelectedItem.Text;
            }
        }

        List<string> filterList = GetKimenoKuldemenyFajta(this.Viszonylatkod);

        if (!String.IsNullOrEmpty(SelectedValue))
        {
            KimenoKuldFajta_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KIMENO_KULDEMENY_FAJTA,
                    SelectedValue, filterList, true, this.ErrorPanel);
        }
        else
        {
            KimenoKuldFajta_KodtarakDropDownList.FillDropDownList(kcs_KIMENO_KULDEMENY_FAJTA, filterList, true, this.ErrorPanel);
        }

        if (!String.IsNullOrEmpty(prevSelectedText))
        {
            ListItem prevSelectedItem = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.FindByText(prevSelectedText);
            if (prevSelectedItem != null)
            {
                prevSelectedItem.Selected = true;
            }
        }
    }

    private List<string> GetKimenoKuldemenyFajta(string Viszonylatkod)
    {
        List<string> filterList = new List<string>();

        Dictionary<String, String> kodtarak_Dictionary =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KIMENO_KULDEMENY_FAJTA, Page);

        foreach (KeyValuePair<String, String> kvp in kodtarak_Dictionary)
        {
            string kodtarKod = kvp.Key; // Key a kódtár kód
            string kodtarNev = kvp.Value; // Value a kódtárérték neve

            switch (Viszonylatkod)
            {
                case Constants.OrszagViszonylatKod.Europai:
                    if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(kodtarKod))
                    {
                        filterList.Add(kodtarKod);
                    }
                    break;
                case Constants.OrszagViszonylatKod.EgyebKulfoldi:
                    if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(kodtarKod))
                    {
                        filterList.Add(kodtarKod);
                    }
                    break;
                case OrszagViszonylatMind:
                    {
                        filterList.Add(kodtarKod);
                    }
                    break;
                default:
                    if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(kodtarKod))
                    {
                        filterList.Add(kodtarKod);
                    }
                    break;
            }
        }

        return filterList;
    }
    #endregion Viszonylat

    #region ISelectableUserComponent Members


    public List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        foreach (RadioButton rb in rbList)
        {
            componentList.Add(rb);
        }
        componentList.AddRange(KimenoKuldFajta_KodtarakDropDownList.GetComponentList());

        return componentList;
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;

            if (!_ViewEnabled)
            {
                KimenoKuldFajta_KodtarakDropDownList.ViewEnabled = value;
                foreach (RadioButton rb in rbList)
                {
                    rb.CssClass = "ViewReadOnlyWebControl";
                }
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;

            if (!_ViewVisible)
            {
                KimenoKuldFajta_KodtarakDropDownList.ViewVisible = value;
                foreach (RadioButton rb in rbList)
                {
                    rb.CssClass = "ViewDisabledWebControl";
                }
            }
        }
    }
    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        // viszonylat
        foreach (RadioButton rb in rbList)
        {
            if (rb.Checked)
            {
                text = Search.GetQueryText(String.Empty, rb.Text);
                break;
            }
        }

        // kimenő küldemény fajta
        if (String.IsNullOrEmpty(KimenoKuldFajta_KodtarakDropDownList.SelectedValue) == false)
        {
            text += KimenoKuldFajta_KodtarakDropDownList.GetSearchText();
        }

        return text;
    }

    #endregion
}
