<%@ Control Language="C#"
    AutoEventWireup="true"
    CodeFile="KuldKuldemenyFormTab.ascx.cs"
    Inherits="Component_KuldemenyKuldemenyTab" %>
<%@ Register Src="IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc9" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="../Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList"
    TagPrefix="uc6" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc7" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc2" %>
<%@ Register Src="../Component/MinositoPartnerControl.ascx" TagName="MinositoPartnerControl" TagPrefix="ucMP" %>

<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="FunkcioGombsor.ascx" TagName="FunkcioGombsor" TagPrefix="fgs" %>

<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>

<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc10" %>

<%@ Register Src="../Component/FullCimField.ascx" TagName="FullCimField" TagPrefix="uc11" %>

<%@ Register Src="PostazasiAdatokPanel.ascx" TagName="PostazasiAdatokPanel" TagPrefix="uc12" %>

<%@ Register Src="IratTextBox.ascx" TagName="IratTextBox" TagPrefix="uc13" %>

<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp" %>
<%@ Register Src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="EPARTTBWF" %>
<%--<%@ Register Src="~/eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>--%>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc14" %>
<%@ Register Src="~/eRecordComponent/SztornoPopup.ascx" TagName="SztornoPopup" TagPrefix="uc" %>
<%--<%@ Register Src="../Component/AdoszamTextBox.ascx" TagName="AdoszamTextBox" TagPrefix="uc1" %>--%>
<%@ Register Src="SzamlaAdatokPanel.ascx" TagName="SzamlaAdatokPanel" TagPrefix="szap" %>
<%@ Register Src="~/eRecordComponent/MellekletekPanel.ascx" TagName="MellekletekPanel" TagPrefix="uc" %>
<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/TerjedelemPanel.ascx" TagName="TerjedelemPanel" TagPrefix="uc15" %>
<%--BUG_10649--%>
<%@ Register Src="~/eRecordComponent/RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>


<script language="JavaScript" type="text/javascript">
    function setIktatasValue() {
        var checkBoxControl_Yes = document.getElementById('<%=Ikt_n_igeny_Igen_RadioButton.ClientID %>');
        var IktatasDropdown = document.getElementById('<%=KodtarakDropDownListIktatasiKotelezettseg.ClientID +"_Kodtarak_DropDownList" %>');

        if (IktatasDropdown) {
            if (checkBoxControl_Yes.checked) {
                IktatasDropdown.selectedIndex = 1;
            }
            else {
                IktatasDropdown.selectedIndex = 0;
            }
        }
    }


</script>

<%--<style>
    body, html {
        overflow : auto;
    }
</style>--%>
<%--Hiba megjelenites--%>

<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="ErrorUpdatePanel1_Load" RenderMode="Inline">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<%--/Hiba megjelenites--%>

<asp:UpdatePanel ID="KuldemenyKuldemenyUpdatePanel" runat="server" UpdateMode="Always" OnLoad="KuldemenyKuldemenyUpdatePanel_Load">
    <ContentTemplate>

        <asp:Panel ID="MainPanel" runat="server" Visible="false">

            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="eMailBoritekok_Ver_HiddenField" runat="server" />
            <uc:SztornoPopup runat="server" ID="SztornoPopup" />

            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr class="urlapSor">
                                    <td colspan="4">
                                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="Label11" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="Label12" runat="server" Text="�rkeztet�k�nyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc9:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" CssClass="mrUrlapInputComboBox" />
                                                    </td>
                                                    <td class="mrUrla"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel2" runat="server" Width="100%" Visible="false">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor" id="trPanel2ErkeztetesiAzonosito" runat="server">
                                                    <td class="mrUrlapCaption_shorter" style="height: 38px">
                                                        <asp:Label ID="Label3" runat="server" Text="�rkeztet�si azonos�t�:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%" style="height: 38px">
                                                        <asp:TextBox ID="ErkeztetoszamTolReadOnlyTextBox1" CssClass="mrUrlapInput" runat="server" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shortest" style="height: 38px">
                                                        <asp:Label ID="labelEredetiErkSzam" runat="server" Text="Eredeti �rk.sz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%" style="height: 38px">
                                                        <asp:TextBox ID="ErkeztetoszamIgReadOnlyTextBox2" CssClass="mrUrlapInput" runat="server" ReadOnly="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor" id="trPanel2Allapot" runat="server">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="Label1" runat="server" Text="�llapot:"></asp:Label></td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="AllapotKodtarakDropDownList1" CssClass="mrUrlapInputComboBox" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shortest">
                                                        <%--<asp:Label ID="Label6" runat="server" Text="Iktat�si k�telezetts�g:"></asp:Label>--%>
                                                        <asp:Label ID="Label_IktatottIrat" runat="server" Text="Iktatott irat:" Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <%--<ktddl:KodtarakDropDownList ID="AllapotKodtarakDropDownList2" width="150" runat="server" />--%>
                                                        <uc13:IratTextBox ID="IratTextBox1" runat="server" Visible="false" ViewMode="true" ReadOnly="true" Validate="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="IktatasMegtagadasPanel" runat="server" Visible="false">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor" id="trIktatasMegtagadasPanelMegtagado" runat="server">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelMegtagado" runat="server" Text="Megtagad�:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc4:FelhasznaloTextBox ID="MegtagadoId_FelhasznaloTextBox" CssClass="mrUrlapInputKozepes" runat="server" ViewEnabled="true"
                                                            ReadOnly="true" Validate="false"></uc4:FelhasznaloTextBox>
                                                    </td>

                                                    <td class="mrUrlapCaption_shortest">
                                                        <asp:Label ID="labelMegtagadasDat" runat="server" Text="Megtagad�s id�pontja:"></asp:Label></td>
                                                    <td class="mrUrlapMezo">
                                                        <cc:CalendarControl ID="MegtagadasDat_CalendarControl" runat="server"
                                                            TimeVisible="true" ReadOnly="true" Validate="false"></cc:CalendarControl>
                                                    </td>

                                                </tr>

                                                <tr class="urlapSor" id="trIktatasMegtagadasPanelIndok" runat="server">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="labelMegtagadasIndokaCaption" runat="server" Text="Megtagad�s indoka:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%" colspan="3">
                                                        <%--
    					                <asp:TextBox ID="TextBoxMegtagadasIndoka" CssClass="mrUrlapInput" Rows="3" ReadOnly="true" TextMode="MultiLine" runat="server"/>
                                                        --%>
                                                        <div runat="server" id="divUzenet" style="overflow: auto; height: 60px; color: Black; border: solid 1px #9d9da1; width: 95%">
                                                            <asp:Label ID="labelMegtagadasIndoka" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_IktatasiKotelezettseg" runat="server" style="display: none; visibility: hidden;">
                                    <td class="mrUrlapCaption_middle" nowrap="true">
                                        <asp:CheckBox ID="cbIktatasiKotelezettsegMegorzes" runat="server" TabIndex="-1"
                                            ToolTip="Iktat�si k�telezetts�g meg�rz�se" />
                                        <asp:Label ID="Label7" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label8" runat="server" Text="Iktat�si k�telezetts�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="KodtarakDropDownListIktatasiKotelezettseg" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest" nowrap="true">
                                        <asp:Label ID="Label_IktatasStatusza" runat="server" Text="Iktat�s st�tusza:" Visible="false"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_IktatasStatusza" CssClass="mrUrlapInputComboBox" ReadOnly="true" Visible="false" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelVonalkod" runat="server">
                                    <td class="mrUrlapCaption_middle" style="height: 24px">
                                        <asp:UpdatePanel ID="starUpdate" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="labelVonalkodStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                <asp:Label ID="labelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="mrUrlapMezo" style="height: 24px; white-space: nowrap">
                                        <uc10:VonalKodTextBox ID="VonalkodTextBoxVonalkod" runat="server" RequiredValidate="false" CssClass="mrUrlapInput" LabelRequiredIndicatorID="labelVonalkodStar" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:CheckBox ID="cbBoritoTipusMegorzes" runat="server" Checked="false" ToolTip="<%$Forditas:labelBoritoTipusMegorzese|Bor�t� t�pus�nak meg�rz�se%>" TabIndex="-1" />
                                        <asp:Label ID="labelBoritoTipus" runat="server" Text="<%$Forditas:labelBoritoTipus|Bor�t� t�pusa:%>"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="KodtarakDropDownListBoritoTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelKuldo" runat="server">
                                    <td class="mrUrlapCaption_middle" nowrap="true">
                                        <asp:CheckBox ID="cbKuldoNeveMegorzes" runat="server" ToolTip="K�ld�/felad� nev�nek meg�rz�se" TabIndex="-1" />
                                        <asp:Label ID="labelKuldoNeveStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelKuldoNeve" runat="server" Text="K�ld�/felad� neve:"></asp:Label><br />
                                        <br style="height: auto" />
                                        <asp:Label ID="Label34" runat="server" Text="Kapcsolt partner:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="PartnerTextBox1" runat="server" TryFireChangeEvent="true" LabelRequiredIndicatorID="labelKuldoNeveStar" />
                                        <%--<uc2:PartnerTextBox ID="PartnerTextBox1" runat="server" TryFireChangeEvent="true" LabelRequiredIndicatorID="labelKuldoNeveStar" />--%>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelKuldoCim" runat="server">
                                    <td class="mrUrlapCaption_middle" rowspan="1">
                                        <asp:CheckBox ID="CheckBoxKuldoCime" runat="server" TabIndex="-1"
                                            ToolTip="K�ld�/felad� c�m�nek meg�rz�se" />
                                        <asp:Label ID="labelKuldoCimeStar" runat="server" Text="*" CssClass="ReqStar" />
                                        <asp:Label ID="labelKuldoCime" runat="server" Text="K�ld�/felad� c�me:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr class="urlapSor" runat="server" id="tr_FullCimField">
                                                <td class="mrUrlapMezo">
                                                    <uc11:FullCimField ID="FullCimField1" runat="server" Validate="false" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapMezo">
                                                    <uc3:CimekTextBox ID="CimekTextBox1" runat="server" Validate="false" LabelRequiredIndicatorID="labelKuldoCimeStar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelTargy" runat="server">
                                    <td class="mrUrlapCaption_middle" valign="top">
                                        <asp:CheckBox ID="CheckBox_Targy" ToolTip="T�rgy meg�rz�se" runat="server" TabIndex="-1" />
                                        <asp:Label ID="Label142" runat="server" Text="T�rgy:"></asp:Label></td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <asp:TextBox ID="TargyTextBox" runat="server" CssClass="mrUrlapInput" Width="99%"></asp:TextBox></td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelErkezteto" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="Label9" runat="server" Text="�rkeztet�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc5:CsoportTextBox ID="CsoportTextBox1" runat="server" ReadOnly="true" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="LabelBelyegzoDatuma" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label5" runat="server" Text="Felad�si id�:"></asp:Label></td>
                                    <td class="mrUrlapMezo" nowrap="nowrap">
                                        <cc:CalendarControl ID="BelyegzoDatumaCalendarControl1" runat="server" TimeVisible="true" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelBeerkezesModja" runat="server">
                                    <td class="mrUrlapCaption_shortest">
                                        <div class="DisableWrap">
                                            <asp:CheckBox ID="cbBeerkezesModjaMegorzes" runat="server" ToolTip="Be�rkez�s m�dj�nak meg�rz�se" TabIndex="-1" />
                                            <%-- lara becsillagozta --%>
                                            <asp:Label ID="Label622" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="lblBeerkezesModja" runat="server" Text="<%$Forditas:labelBeerkezesModja|Be�rkez�s m�dja:%>"></asp:Label>&nbsp;
                                        </div>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="KuldesModja_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label159" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="lblKezbesitesModja" runat="server" Text="<%$Forditas:lblKezbesitesModja|K�zbes�t�s m�dja:%>"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <ktddl:KodtarakDropDownList ID="Kezbesites_modja_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelBeerkezes" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:CheckBox ID="cbBeerkezesIdopontjaMegorzes" runat="server" ToolTip="Be�rkez�s id�pontj�nak meg�rz�se" TabIndex="-1" />
                                        <asp:Label ID="Label41" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label42" runat="server" Text="Be�rkez�s id�pontja:"></asp:Label></td>
                                    <td class="mrUrlapMezo" nowrap="nowrap">
                                        <cc:CalendarControl ID="BeerkezesiIdoCalendarControl1" runat="server" TimeVisible="true" />
                                    </td>
                                    <%-- bernat.laszlo added & modified: Be�rkez�si id� �s �rkeztet�si id� sz�tbont�sa--%>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label165" runat="server" Text="�rkeztet�s id�pontja:" TimeVisible="true"></asp:Label></td>
                                    <td class="mrUrlapMezo" nowrap="nowrap">
                                        <cc:CalendarControl ID="ErkeztetesiIdoCalendarControl1" runat="server" Validate="false" ReadOnly="true" />
                                    </td>
                                </tr>
                                <%-- bernat.laszlo eddig --%>
                                <tr class="urlapSor" id="trKuldemenyPanelKuldoItsz" runat="server">
                                    <td class="mrUrlapCaption_middle" id="td_labelKuldoIktatoSzama" runat="server">
                                        <asp:CheckBox ID="cbKuldoIktatoszamaMegorzes" runat="server" ToolTip="Hivatkoz�si sz�m meg�rz�se" TabIndex="-1" />
                                        <asp:Label ID="labelKuldoIktatoszama" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label></td>
                                    <td class="mrUrlapMezo" id="td_KuldoIktatoSzama" runat="server">
                                        <asp:TextBox ID="HivatkozasiSzam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:CheckBox ID="cbRagszamMegorzes" runat="server" ToolTip="Postai azonos�t� meg�rz�se" TabIndex="-1" />
                                        <%--BUG_10649--%>
                                        <asp:Label ID="LabelReqRagszam" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelRagszam" runat="server" Text="Postai azonos�t�:" />
                                        <%--BLG_452 START--%>
                                        <asp:Label ID="LabelFutarjegyzekListaSzama" runat="server" Text="Fut�rjegyz�k lista sz�ma:" Visible="false" />
                                        <%--BLG_452 STOP--%>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <%--BUG_10649--%>
<%--                                    <uc10:VonalKodTextBox ID="RagszamRequiredTextBox" CssClass="mrUrlapInput" runat="server" Validate="false" />--%>
                                         <%--BUG_11273--%>
                                        <%--<uc5:RagszamTextBox ID="RagszamRequiredTextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" EnableViewState="True" />--%>
                                        <uc5:RagszamTextBox ID="RagszamRequiredTextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" EnableViewState="True" Validate="false" />

                                        <%--BLG_452 START--%>
                                        <asp:TextBox ID="TextBoxFutarjegyzekListaSzama" CssClass="mrUrlapInput" runat="server" Visible="false" />
                                        <%--BLG_452 STOP--%>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_AdatHordozo_Tartalom" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <%-- lara becsillagozta --%>
                                        <asp:Label ID="Label271" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label27" runat="server" Text="K�ldem�ny t�pusa:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="AdathordozoTipusa_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest" rowspan="3">
                                        <asp:CheckBox ID="cbTartalomMegorzes" runat="server" ToolTip="Tartalom meg�rz�se" TabIndex="-1" />
                                        <asp:Label ID="Label13" runat="server" Text="Tartalom:"></asp:Label></td>
                                    <td class="mrUrlapMezo" rowspan="3">
                                        <asp:TextBox ID="Tartalom_TextBox" runat="server" CssClass="mrUrlapInput" Height="75px"></asp:TextBox></td>
                                </tr>
                                <tr class="urlapSor" id="tr_ElsodlegesAdathordozo" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <%-- lara becsillagozta --%>
                                        <asp:Label ID="Label91" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label112" runat="server" Text="Els�dleges adathordoz� t�pusa:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="UgyintezesAlapja_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelCimzett" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:CheckBox ID="cbCimzettMegorzes" runat="server" ToolTip="C�mzett meg�rz�se" TabIndex="-1" />
                                        <asp:Label ID="Label26" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label122" runat="server" Text="C�mzett neve:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc5:CsoportTextBox ID="EredetiCimzettCsoportTextBox1" runat="server" Validate="true" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelKezelo" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:CheckBox ID="CheckBox_Kezelo" ToolTip="Kezel� meg�rz�se" runat="server" TabIndex="-1" />
                                        <asp:Label ID="Label25" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text="Kezel�:"></asp:Label><%--<asp:Label ID="Label121" runat="server" Text="*" CssClass="ReqStar"></asp:Label>--%></td>
                                    <td class="mrUrlapMezo">
                                        <uc5:CsoportTextBox ID="FelelosCsoportTextBox1" runat="server" Validate="true" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="labelSurgosseg" runat="server" Text="K�zbes�t�s priorit�sa:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="Surgosseg_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                    </td>
                                    <td class="mrUrlapCaption_short"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor" id="trKuldemenyPanelMinosites" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="labelKuldemenyMinosites" runat="server" Text="<%$Forditas:labelKuldemenyMinosites|K�ldem�ny min�s�t�se:%>"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodtarakDropDownList ID="ktDropDownListKuldemenyMinosites" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label181" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label182" runat="server" Text="C�mz�s t�pusa:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <ktddl:KodtarakDropDownList ID="CimzesTipusa_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                    </td>

                                </tr>
                                <%--BLG_452 START--%>
                                <tr id="trFutarJegyzekParametersB" class="urlapSor" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="LabelMinositesErvenyessegIdeje" runat="server" Text="Min �rv. ideje:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <cc:CalendarControl ID="CalendarControlMinositesErvenyessegIdeje" runat="server" Validate="false" />
                                    </td>
                                    <td class="mrUrlapCaption_shortest">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelMinositoSzervezet" runat="server" Text="Min�s�t� szervezet:" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelMinosito" runat="server" Text="Min�s�t�:" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <ucMP:MinositoPartnerControl ID="MinositoPartnerControlKuldKuldemenyekFormTab" runat="server" />
                                    </td>
                                </tr>
                                <%--BLG_452 STOP--%>
                                <tr class="urlapSor" id="tr_MellekletekCsatolmanyokSzama" runat="server" visible="false">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="Label19" runat="server" Text="Csatolm�nyok sz�ma:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc14:RequiredNumberBox ID="CsatolmanyokSzama_RequiredNumberBox1" Validate="false" ReadOnly="true" runat="server" />
                                    </td>
                                    <td colspan="2" style="text-align: right;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Label CssClass="mrUrlapCaption_shortest" ID="LabelMellekletekSzama" runat="server" Text="Mell�kletek sz�ma:" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <uc14:RequiredNumberBox ID="MellekletekSzama_RequiredNumberBox1" Validate="false" ReadOnly="true" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="urlapSor" id="tr_KulsoAzonosito" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label runat="server" Text="<%$Resources:Form,KuldemenyKulsoAzonosito %>"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBox_KulsoAzonosito" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="mrUrlapCaption_shortest"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>

                                <tr class="urlapSor" id="trKuldemenyPanelBontas" runat="server">
                                    <td colspan="4">
                                        <asp:Panel ID="BontoPanel" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor" id="trBontoPanelBontasNelkul" runat="server">
                                                    <td class="mrUrlapCaption_shorter"></td>
                                                    <td class="mrUrlapMezo" style="font-weight: bold">
                                                        <asp:CheckBox ID="cbBontoasNelkul" ToolTip="Bont�s n�lk�li �rkeztet�s" runat="server"
                                                            Text="Bont�s n�lk�li �rkeztet�s" OnCheckedChanged="cbBontoasNelkul_CheckedChanged"
                                                            AutoPostBack="true" TabIndex="-1" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shortest"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                                <tr class="urlapSor" id="trBontas" runat="server">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:Label ID="Label20" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="Label52" runat="server" Text="K�ldem�ny bont�ja:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc4:FelhasznaloTextBox ID="FelbontoFelhasznaloTextBox1" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_shortest">
                                                        <asp:Label ID="Label23" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                        <asp:Label ID="Label72" runat="server" Text="Bont�s id�pontja:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <cc:CalendarControl ID="FelbontasiIdoCalendarControl1" runat="server" TimeVisible="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor" id="trBontasMegjegyzes" runat="server">
                                                    <td class="mrUrlapCaption_shorter">
                                                        <asp:CheckBox ID="cbBontasiMegjegyzesMegorzes" runat="server" ToolTip="Bont�ssal kapcsolatos megjegyz�s meg�rz�se"
                                                            TabIndex="-1" />
                                                        <asp:Label ID="Label24" runat="server" Text="Bont�ssal kapcsolatos megjegyz�s:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" colspan="3">
                                                        <asp:TextBox ID="bontasMegjegyzesTextBox" Rows="3" TextMode="MultiLine" runat="server"
                                                            Width="99%" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>

                                <%--MAIN bernat.laszlo added--%>
                                <tr class="urlapSor_kicsi" runat="server" id="trMunkaallomas">
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label154" runat="server" Text="�rkeztet� munka�llom�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <asp:TextBox ID="Munkaallomas_TextBox" runat="server" CssClass="mrUrlapInput" Enabled="false"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr class="urlapSor_kicsi" runat="server" id="trIktatasExtra">
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label170" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="Label151" runat="server" Text="Iktat�st ig�nyel?"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <asp:RadioButton ID="Ikt_n_igeny_Igen_RadioButton" runat="server" GroupName="Ikt_n_igeny_Selector" Text="Igen" />
                                        <asp:RadioButton ID="Ikt_n_igeny_Nem_RadioButton" runat="server" GroupName="Ikt_n_igeny_Selector" Text="Nem" />
                                    </td>

                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label156" runat="server" Text="S�r�lt k�ldem�ny?"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <asp:RadioButton ID="serult_kuld_Igen_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Igen" />
                                        <asp:RadioButton ID="serult_kuld_Nem_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Nem" />
                                    </td>
                                </tr>

                                <tr class="urlapSor_kicsi" runat="server" id="trTevedesSerules">
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label157" runat="server" Text="T�ves c�mz�s?"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <asp:RadioButton ID="teves_cim_Igen_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Igen" />
                                        <asp:RadioButton ID="teves_cim_Nem_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Nem" />
                                    </td>

                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label158" runat="server" Text="T�ves �rkeztet�s?"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" width="0%">
                                        <asp:RadioButton ID="teves_erk_Igen_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Igen" />
                                        <asp:RadioButton ID="teves_erk_Nem_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Nem" />
                                    </td>
                                </tr>
                                <%--MAIN bernat.laszlo eddig--%>

                                <tr id="trMellekletekPanel" runat="server" visible="false">
                                    <td colspan="4">
                                        <uc:MellekletekPanel ID="mellekletekPanel" runat="server" />
                                    </td>
                                </tr>

                                <%--BLG_452 START--%>
                                <tr id="trTerjedelemMainPanel" runat="server" visible="false">
                                    <td colspan="4" style="padding-top: 5px;">
                                        <uc15:TerjedelemPanel ID="TerjedelemPanelA" runat="server" />
                                    </td>
                                </tr>
                                <%--BLG_452 STOP--%>

                                <tr id="trKezelesiFeljegyzes" runat="server" visible="false">
                                    <td colspan="4">
                                        <div style="padding-top: 5px;">
                                            <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" Collapsed="true" RenderMode="Panel" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>

                        <eUI:eFormPanel ID="SzamlaAdatok_EFormPanel" Width="100%" runat="server" Visible="false">
                            <szap:SzamlaAdatokPanel ID="SzamlaAdatokPanel" runat="server" SzuloTipus="Kuldemeny" />
                        </eUI:eFormPanel>

                        <%--			<eUI:eFormPanel ID="ObjektumTargyszavai_EFormPanel" runat="server" Visible="False">
				<table cellspacing="0" cellpadding="0" width="100%">
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelAdoszamStar" runat="server" Text="*" CssClass="ReqStar" />
                            <asp:Label ID="labelAdoszam" runat="server" Text="Sz�ll�t� ad�sz�ma:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc1:AdoszamTextBox ID="AdoszamTextBox1" runat="server" LabelRequiredIndicatorID="labelAdoszamStar" Validate="false" ValidateFormat="true" />
                        </td>
                    </tr>
                <otp:ObjektumTargyszavaiPanel ID="ObjektumTargyszavaiPanel1" RepeatColumns="1" RepeatDirection="Vertical"
                    runat="server" Validate="false" RepeatLayout="Flow" />
                    </table>
			</eUI:eFormPanel>--%>

                        <eUI:eFormPanel ID="KimenoKuldemeny_EFormPanel" Width="100%" runat="server" Visible="False">
                            <uc12:PostazasiAdatokPanel ID="KimenoKuld_PostazasiAdatokPanel" runat="server" />
                        </eUI:eFormPanel>

                        <eUI:eFormPanel ID="IktatasAdatokFormPanel" Width="100%" runat="server" Visible="False">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor" id="trIktatasAdatokFormPanelAgazatiJel" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="Label15" runat="server" Text="�gazati jel:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="IktAdatok_AgJel_TextBox" runat="server" CssClass="mrUrlapInput"
                                            ReadOnly="True"></asp:TextBox></td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label16" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc11:IraIrattariTetelTextBox ID="IktAdatok_IraIrattariTetelTextBox" runat="server"
                                            ReadOnly="true" Validate="false" ViewMode="true" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trIktatasAdatokFormPanelUgytipus" runat="server">
                                    <td class="mrUrlapCaption_middle">
                                        <asp:Label ID="Label14" runat="server" Text="�gyt�pus:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="IktAdatok_Ugytipus_TextBox" runat="server" CssClass="mrUrlapInput"
                                            ReadOnly="True"></asp:TextBox></td>
                                    <td class="mrUrlapCaption_shortest">
                                        <asp:Label ID="Label17" runat="server" Text="Szign�l�si forma:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="IktAdatok_SzignTipus_TextBox" runat="server" CssClass="mrUrlapInput"
                                            ReadOnly="True"></asp:TextBox></td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>

                        <eUI:eFormPanel ID="StandardTargyszavakPanel_Kuldemenyek" runat="server" Width="100%" Visible="false">
                            <uc:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak_Kuldemenyek" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputKozepes" />
                        </eUI:eFormPanel>

                        <eUI:eFormPanel ID="TipusosTargyszavakPanel_Kuldemenyek" runat="server" Width="100%" Visible="false">
                            <uc:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_Kuldemenyek" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputKozepes" />
                        </eUI:eFormPanel>

                        <eUI:eFormPanel ID="TipusosTargyszavakPanel_KimenoKuldemenyek" runat="server" Width="100%" Visible="false">
                            <uc:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_KimenoKuldemenyek" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" RepeatLayout="Table" CssClass="mrUrlapInputKozepes" />
                        </eUI:eFormPanel>


                        <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" Width="100%" CssClass="mrResultPanelErkeztetes">
                            <div class="mrResultPanelText">Az �rkeztet�s sikeresen v�grehajt�dott.</div>
                            <table cellspacing="0" cellpadding="0" width="700px" class="mrResultTableErkeztetes">
                                <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                    <td class="mrUrlapCaptionBigSize">
                                        <asp:Label ID="labelKuldemeny" runat="server" Text="�rkeztet�sz�m:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezoBigSize">
                                        <asp:Label ID="labelKuldemenyErkSzam" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td class="mrUrlapCaption" style="padding-top: 5px">
                                        <asp:ImageButton ID="imgKuldemenyMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                            onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                    </td>
                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                        <span style="padding-right: 80px">
                                            <asp:ImageButton ID="imgKuldemenyModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                            <asp:ImageButton ID="imgCsatolmanyFeltoltes" runat="server" ImageUrl="~/images/hu/trapezgomb/feltoltes_trap.jpg"
                                                onmouseover="swapByName(this.id,'feltoltes_trap2.jpg');" onmouseout="swapByName(this.id,'feltoltes_trap.jpg');" />
                                            <asp:ImageButton ID="imgCsatolas" runat="server" ImageUrl="~/images/hu/trapezgomb/kapcsolas_trap.jpg"
                                                onmouseover="swapByName(this.id,'kapcsolas_trap2.jpg');" onmouseout="swapByName(this.id,'kapcsolas_trap.jpg');"
                                                Visible="false" />
                                            <!--// CR 3058-->
                                            <asp:ImageButton ID="ImageButtonVonalkodNyomtatas" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>

                    </td>
                    <td valign="top" style="width: 0px">
                        <div id="divFunkcioGombsor" runat="server" visible="false" style="margin-top: 8px;"></div>
                        <fgs:FunkcioGombsor ID="FunkcioGombsor" runat="server" Visible="true" />
                    </td>
                </tr>
            </table>
        </asp:Panel>


        <table>
            <tr>
                <td valign="top" align="center" style="width: 100%">
                    <uc7:TabFooter ID="TabFooter1" runat="server" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" />
        <asp:HiddenField ID="HiddenField_Svc_FelhasznaloSzervezetId" runat="server" />
        <asp:HiddenField ID="HiddenFieldCommand" runat="server" />
        <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
        <script type="text/javascript">      

            Sys.Application.add_load(function () {

                var command = $('#<%= HiddenFieldCommand.ClientID %>').val();
                if (command == 'New') {

                    try {

                        $('#<%= FelelosCsoportTextBox1.HiddenField.ClientID%>').on('change', function () {
                            loadFizikaihelyek();
                        });

                        $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>').on('change', function () {
                            var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');

                            var dVal = dropdown.val();
                            var dText = dropdown.find('option:selected').text();
                            if (dVal != undefined && dText != undefined) {
                                $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                                $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                            }
                        });

                        loadFizikaihelyek();

                       <%-- setTimeout(function () {
                            var val = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val();

                            if (val != '' && val != undefined) {
                                var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');
                                dropdown.val(val);
                            }
                        }, 1000);--%>

                    } catch (e) {
                        //console.log(e);
                    }
                }
            });

            function loadFizikaihelyek() {
                var inPartnerId = $('#<%= FelelosCsoportTextBox1.HiddenField.ClientID%>').val();
                if (inPartnerId !== null && inPartnerId !== '' && inPartnerId !== undefined && inPartnerId !== 'undefined') {

                    try {
                        var fel = $('#<%= HiddenField_Svc_FelhasznaloId.ClientID%>').val();
                        var szerv = $('#<%= HiddenField_Svc_FelhasznaloSzervezetId.ClientID%>').val();
                        if (fel != undefined || szerv != undefined) {
                            $.ajax({
                                url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFizikaiHelyek") %>',
                                data: JSON.stringify({
                                    partnerId: inPartnerId,
                                    contextKey: $('#<%= HiddenField_Svc_FelhasznaloId.ClientID%>').val() + ';' + $('#<%= HiddenField_Svc_FelhasznaloSzervezetId.ClientID%>').val()
                                }),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    fillFizikaihelyek(data);
                                },
                                error: function (response) {
                                    alert(response.responseText);
                                },
                                failure: function (response) {
                                    alert(response.responseText);
                                }
                            });
                        }
                    } catch (e) {
                        //console.log(e);
                    }
                }
                else {
                    //console.log('partner id nem stimmel');
                }
            }

            function fillFizikaihelyek(data) {
                //console.log('data');
                var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');
                if (dropdown != null) {
                    var dVal = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val();
                    //var dText = dropdown.find('option:selected').text();
                    var dText = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val();
                    dropdown.empty();
                    dropdown.html('<option value="' + '' + '">' + '[Nincs megadva elem]' + '</option>');
                    //<option selected="selected" value="">[Nincs megadva elem]</option>

                    if (data === null || data.d === null) {
                        return;
                    }
                    if (dropdown !== null) {
                        var k = JSON.parse(data.d);

                        var s = '';
                        for (var i = 0; i < k.length; i++) {
                            s += '<option value="' + k[i].Id + '">' + k[i].Value + '</option>';
                        }

                        dropdown.html(s);

                        if (dVal != undefined && dText != undefined) {
                            dropdown.val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                        }
                        //if (k.length == 1) {
                        //    dropdown.
                        //}

                    }
                }
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
