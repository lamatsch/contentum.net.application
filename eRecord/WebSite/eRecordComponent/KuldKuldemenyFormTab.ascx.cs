﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;


public partial class Component_KuldemenyKuldemenyTab : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    private const string KodCsoportErkeztetoKonyv = "ERKEZTETOKONYV";
    private const string KodCsoportKuldesModja = "KULDEMENY_KULDES_MODJA";
    private const string KodCsoportTovabbito = "TOVABBITO_SZERVEZET";
    private const string KodCsoportUgyintezesAlapja = "UGYINTEZES_ALAPJA";
    private const string KodCsoportSurgosseg = "SURGOSSEG";
    private const string KodCsoportAllapot = "KULDEMENY_ALLAPOT";
    private const string KodCsoportIktatasikotelezettseg = "IKTATASI_KOTELEZETTSEG";
    private const string KodCsoportKuldemenyIktatasStatusza = "KULDEMENY_IKTATAS_STATUSZA";
    private const string KodCsoportKuldemenyKuldesModja = "KULDEMENY_KULDES_MODJA";
    private const string KodCsoportTovabbitoSzervezet = "TOVABBITO_SZERVEZET";
    private const string KodCsoportBoritoTipus = "KULDEMENY_BORITO_TIPUS";
    private const string KodCsoportELSODLEGES_ADATHORDOZO = "ELSODLEGES_ADATHORDOZO";
    //private const string KodCsoportKEZELESI_FELJEGYZES_KULDEMENY = "KEZELESI_FELJEGYZES_KULDEMENY";
    private const string KodCsoportKEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";

    //bernat.laszlo added
    private const string KodCsoport_KULD_KEZB_MODJA = "KULD_KEZB_MODJA";
    private const string KodCsoportKULD_CIMZES_TIPUS = "KULD_CIMZES_TIPUS";
    //bernat.laszlo eddig

    // A küldemény minõsítése azonos értékkészletû és szerepû mint az iraté,
    // ezért nem veszünk fel rá új kódcsoportot, hanem az irat minõsítést használjuk
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";

    //private const string SzallitoPartnerTextBox_Targyszo = "Szállító megnevezése"; // TODO: Rendszerparaméter

    public string Command = "";
    private PageView pageView = null;
    private String Startup = String.Empty;
    private string EmailBoritekokId = "";
    private EREC_eMailBoritekok eMailBoritekok = null;
    //private string eMailBoritekok_ver = ""; // Megjegyzi a foma megnyitasakor es menteskor osszehasonlitja!
    private bool isTUKRendszer = false;
    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    private Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public Component_FormHeader FormHeader = null;

    // A szóban forgó küldemény objektum (megtekintés, módosítás esetén használatos)
    public EREC_KuldKuldemenyek obj_kuldemeny = null;

    private Boolean _TemplateMode = false;

    protected bool IsRequireOverwriteTemplateDatas()
    {
        if (!IsPostBack && Command == CommandName.New && _TemplateMode)
        {
            return true;
        }

        return false;
    }

    #region page

    protected void Page_Init(object sender, EventArgs e)
    {
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        /*FormHeader.ButtonsClick +=
            new CommandEventHandler(FormHeader1_ButtonsClick);*/

        #region ORG függõ megjelenítés
        // CR 3054
        //if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        //{
        //    labelBoritoTipus.Text = "Vonalkód helye:";
        //} 

        #endregion

        Command = Request.QueryString.Get(CommandName.Command);
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        HiddenFieldCommand.Value = string.IsNullOrEmpty(Command) ? "" : Command.ToString();        

        #region EmailBoritekok

        EmailBoritekokId = Request.QueryString.Get("EmailBoritekokId");
        if (!IsPostBack && !String.IsNullOrEmpty(EmailBoritekokId))
        {
            // TODO:
            LoadEmailBoritekKuldKuldemenyId(EmailBoritekokId);
            if (!String.IsNullOrEmpty(eMailBoritekok.KuldKuldemeny_Id))
            {
                // TODO:
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "TODO", "Az email már érkeztetve van! Kérem, válasszon másik email-t!");
            }

            //eMailBoritekok_ver = eMailBoritekok.Base.Ver;
            eMailBoritekok_Ver_HiddenField.Value = eMailBoritekok.Base.Ver;
        }
        #endregion

        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, ParentForm + Command);

        FunkcioGombsor.FunkcionButtonsClick += new CommandEventHandler(FunkcioGombsorFunkcionButtonsClick);

        PartnerTextBox1.TextBox.Width = Unit.Percentage(86);
        CimekTextBox1.TextBox.Width = Unit.Percentage(86);

        //Tovabbito_DropDownList.DropDownList.Width = 140;
        //UgyintezesAlapja_DropDownList.DropDownList.Width = 140;
        //Surgosseg_DropDownList.DropDownList.Width = 140;

        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);


        //Partner és cim összekötése
        PartnerTextBox1.CimTextBox = CimekTextBox1.TextBox;
        PartnerTextBox1.CimHiddenField = CimekTextBox1.HiddenField;

        //Cim összekötése
        FullCimField1.CimTextBox = CimekTextBox1.TextBox;
        FullCimField1.CimHiddenField = CimekTextBox1.HiddenField;

        // BUG_4731
        FullCimField1.CimTextBoxClone = SzamlaAdatokPanel.Szallito_CimekTextBox.TextBox;
        FullCimField1.CimHiddenFieldClone = SzamlaAdatokPanel.Szallito_CimekTextBox.HiddenField;

        FeljegyzesPanel.ErrorPanel = EErrorPanel1;
        FeljegyzesPanel.ErrorUpdatePanel = ErrorUpdatePanel1;

        VonalkodTextBoxVonalkod.Validate = false;

        // CR 3058 && CR3182 - FPH-s - Sávos vonalkódkezelés környezetben a giga patch kirakása után érkeztetéskor nem volt kötelezõ a vonalód megadása, ami hiba.
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        {
            //VonalkodTextBoxVonalkod.Validate = true;
            labelVonalkodStar.Visible = false;
            starUpdate.Update();
        }
        else
        {
            VonalkodTextBoxVonalkod.RequiredValidate = true;
            VonalkodTextBoxVonalkod.Validate = true;
            labelVonalkodStar.Visible = true;
            starUpdate.Update();
        }

        if (Startup == Constants.Startup.Szamla)
        {
            tr_IktatasiKotelezettseg.Visible = false;

            if (Command == CommandName.New || Command == CommandName.Modify)
            {
                VonalkodTextBoxVonalkod.Validate = true;
                CimekTextBox1.Validate = true;

                //AdoszamTextBox1.Validate = true;

                // BUG_6221
                //ConvertSzamlaAdatokToPartnerTextbox();

                PartnerTextBox1.CloneCimTextBox = SzamlaAdatokPanel.Szallito_CimekTextBox.TextBox;
                PartnerTextBox1.CloneCimHiddenField = SzamlaAdatokPanel.Szallito_CimekTextBox.HiddenField;

                // BUG_6221
                PartnerTextBox1.ClonePartnerTextBoxClientId = SzamlaAdatokPanel.Szallito_PartnerTextBox.TextBox.ClientID; //  Ugy_PartnerId_Ugyindito_PartnerTextBox.TextBox.ClientID;
                PartnerTextBox1.ClonePartnerHiddenFieldClientId = SzamlaAdatokPanel.Szallito_PartnerTextBox.Control_HiddenField.ClientID; // Ugy_PartnerId_Ugyindito_PartnerTextBox.Control_HiddenField.ClientID;

                CimekTextBox1.TryFireChangeEvent = true;
            }
            // BUG_7678
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PAGE_FELADASIIDO_KOTELEZO, false))
            {
                BelyegzoDatumaCalendarControl1.Validate = true;
                LabelBelyegzoDatuma.Visible = true;
            }
            else
            {
                BelyegzoDatumaCalendarControl1.Validate = false;
                LabelBelyegzoDatuma.Visible = false;
            }
            // eredeti érkeztetõszám (csak bontásnál keletkezhet, számla típust nem tudunk megadni bontáskor)
            labelEredetiErkSzam.Visible = false;
            ErkeztetoszamIgReadOnlyTextBox2.Visible = false;

            EredetiCimzettCsoportTextBox1.SzervezetCsoport = true;

            // borító (csak irat lehet)
            cbBoritoTipusMegorzes.Visible = false;
            labelBoritoTipus.Visible = false;
            KodtarakDropDownListBoritoTipus.Visible = false;

            // küldõ iktatószáma
            //cbKuldoIktatoszamaMegorzes.Visible = false;
            //labelKuldoIktatoszama.Visible = false;
            //HivatkozasiSzam_TextBox.Visible = false;
            td_labelKuldoIktatoSzama.Visible = false;
            td_KuldoIktatoSzama.Visible = false;

            // Adathordozó, elsõdleges adathordozó (csak papír lehet)
            tr_AdatHordozo_Tartalom.Visible = false;
            tr_ElsodlegesAdathordozo.Visible = false;

            // Kézbesítés prioritása
            labelSurgosseg.Visible = false;
            Surgosseg_DropDownList.Visible = false;

            // mindig fel van bontva
            BontoPanel.Visible = false;

            SzamlaAdatok_EFormPanel.Visible = true;

            //ObjektumTargyszavai_EFormPanel.Visible = true;
        }

        if (isTUKRendszer)
        {
            tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;
            IrattariHelyLevelekDropDownTUK.Validate = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
            //FelelosCsoportTextBox1.TextBox.TextChanged += TextBox_TextChanged;
        }
    }

    EREC_IrattariHelyek GetIrattariHelyById(string irattarId)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        ExecParam.Record_Id = irattarId;
        Result res = service.Get(ExecParam);

        if (!res.IsError && res.Record != null)
        {
            EREC_IrattariHelyek hely = (EREC_IrattariHelyek)res.Record;
            return hely;
        }
        return null;
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string id, string selectedValue)
    {
        if (Command == CommandName.View)
        {
            if (string.IsNullOrEmpty(selectedValue))
                return;

            EREC_IrattariHelyek hely = GetIrattariHelyById(selectedValue);

            if (hely == null)
                Logger.Debug("Irattári hely nem található. IrattarId: " + hely.Id);

            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Clear();
            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Add(new ListItem(hely.Ertek, selectedValue));
            return;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Filter(id);

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (result_csoportok.IsError)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }
                Csoportok.Add(id);
                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.GetCount > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    if (res.GetCount == 1)
                        IrattariHelyLevelekDropDownTUK.DropDownList.SelectedIndex = 1;
                    else if (!string.IsNullOrEmpty(selectedValue))
                    {
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                    }
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader.ButtonsClick +=
            new CommandEventHandler(FormHeader1_ButtonsClick);

        FunkcioGombsor.ParentForm = ParentForm;
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        //bernat.laszlo added
        Ikt_n_igeny_Igen_RadioButton.Attributes.Add("onclick", "setIktatasValue()");
        Ikt_n_igeny_Nem_RadioButton.Attributes.Add("onclick", "setIktatasValue()");

        KuldesModja_DropDownList.AutoPostBack = true;
        KuldesModja_DropDownList.DropDownList.SelectedIndexChanged += KuldesModja_DropDownList_SelectedIndexChanged;

        // BLG_1721
        //SetFutarJegyzekVisibility();
        SetKezbesitesVisibility();

        // BUG_10649
        if (Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_ERKEZTETESKOR, false))
        {
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = RagszamRequiredTextBox.CheckRagszamRequired(KuldesModja_DropDownList.SelectedValue);
        }
        else
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = false;

        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_Svc_FelhasznaloSzervezetId.Value = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Validate = true;
        }
    }

    

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }

        // fókusz a vonalkód mezõre érkeztetésnél:
        if (Command == CommandName.New)
        {
            JavaScripts.SetFocus(VonalkodTextBoxVonalkod.TextBox, 400);
        }

        if (Command != CommandName.New)
        {
            #region Megorzesi checkbox-ok
            cbBoritoTipusMegorzes.Visible = false;

            cbKuldoNeveMegorzes.Visible = false;
            cbCimzettMegorzes.Visible = false;
            CheckBoxKuldoCime.Visible = false;
            CheckBox_Kezelo.Visible = false;
            CheckBox_Targy.Visible = false;
            cbIktatasiKotelezettsegMegorzes.Visible = false;
            cbBeerkezesModjaMegorzes.Visible = false;
            cbBeerkezesIdopontjaMegorzes.Visible = false;
            cbRagszamMegorzes.Visible = false;
            cbBontasiMegjegyzesMegorzes.Visible = false;
            cbKuldoIktatoszamaMegorzes.Visible = false;
            cbTartalomMegorzes.Visible = false;
            #endregion
        }

        if (Startup != Constants.Startup.Szamla)
        {
            SetBontasPanelVisiblity();
        }
        KodtarakDropDownListIktatasiKotelezettseg.Attributes.Add("onchange", "HideTextBox()");

        Page.Title = Page.Title + " - " + Rendszerparameterek.Get(UI.SetExecParamDefault(Page), Rendszerparameterek.APPLICATION_VERSION);
    }

    private string GetOrgKod()
    {
        String orgKod = String.Empty;
        Result org = new Result();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        using (KRT_OrgokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_OrgokService())
        {
            ExecParam _exec = execParam.Clone();
            _exec.Record_Id = execParam.Org_Id;
            org = service.Get(_exec);
        }

        if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
        return orgKod;
    }

    private void SetBontasPanelVisiblity()
    {
        SetBontasPanelVisiblity(Command);
    }

    // Command helyett az átadott paramétert használja
    private void SetBontasPanelVisiblity(String mode)
    {
        if (FunctionRights.GetFunkcioJog(Page, "KuldemenyFizikaiBontas"))
        {
            BontoPanel.Visible = true;
        }
        else
        {
            if (!String.IsNullOrEmpty(FelbontoFelhasznaloTextBox1.Id_HiddenField.Trim()) && !String.IsNullOrEmpty(FelbontasiIdoCalendarControl1.Text.Trim()))
            {
                BontoPanel.Visible = true;
                FelbontoFelhasznaloTextBox1.ReadOnly = true;
                FelbontasiIdoCalendarControl1.ReadOnly = true;
                cbBontoasNelkul.Visible = false;
            }
            else
            {
                BontoPanel.Visible = false;
                FelbontoFelhasznaloTextBox1.ReadOnly = true;
                FelbontasiIdoCalendarControl1.ReadOnly = true;
                cbBontoasNelkul.Visible = false;
            }
        }

        if (mode == CommandName.View)
        {
            FelbontoFelhasznaloTextBox1.ReadOnly = true;
            FelbontasiIdoCalendarControl1.ReadOnly = true;
            cbBontoasNelkul.Visible = false;
            if (String.IsNullOrEmpty(FelbontoFelhasznaloTextBox1.Id_HiddenField.Trim()) && String.IsNullOrEmpty(FelbontasiIdoCalendarControl1.Text.Trim()))
            {
                BontoPanel.Visible = false;
            }
        }

        // Érkeztetéskor defaultból a bontó adatok ne látszódjanak:
        //CR 1232 igény: Default-ból látszódjanak a bontó adatok
        // CR#2242: ha a template-ben nincs bontó és bontás ideje, akkor mégse látsszon
        if (mode == CommandName.New && !IsPostBack)
        {
            bool _TemplateMode = !String.IsNullOrEmpty(FormHeader.CurrentTemplateId);

            if (!_TemplateMode)
            {
                cbBontoasNelkul.Checked = false;
                trBontas.Visible = true;
                trBontasMegjegyzes.Visible = true;
            }
        }
    }

    private void SetIktatasMegtagadvaControls()
    {
        // megtagadott emailt nem lehet módosítani:
        //TabFooter1.DisableSaveButtons();
        TabFooter1.ImageButton_Save.Visible = false;
        SetViewControls(true);
        SetBontasPanelVisiblity(CommandName.View);

        FunkcioGombsor.Visible = false;
    }

    protected void KuldemenyKuldemenyUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();

                switch (eventArgument)
                {
                    case EventArgumentConst.refreshKuldemenyekPanel:
                        ReLoadTab();
                        break;
                    case EventArgumentConst.refreshMasterList:
                        ReLoadTab();
                        break;
                }
            }
        }
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        // szabályozza, hogy felvihetõ-e nem törzsbõl vett partner és cím a beküldõhöz
        bool isBekuldoPartnerCimNemTorzsbolDisabled = (Rendszerparameterek.GetBoolean(Page
            , Rendszerparameterek.BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED) == false);
        // szabályozza, hogy bevihetõ-e szabad szöveg a konkatenált címmezõbe
        bool isCimSzabadSzovegEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.CIM_SZABAD_SZOVEG_ENABLED);

        // Az osszes gomb eltuntetese
        FunkcioGombsor.ButtonsVisible(false);

        //SetVisibleAllButtonsOnFunkcioGombSor(false);

        // Ezt a commandargument ertekadasa elott megkell tenni:
        //blg 1868
        FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = "javascript:window.open('KuldKuldemenyekSSRSBoritoA3.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=BoritoA3')";
        FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = "javascript:window.open('KuldKuldemenyekSSRSBorito.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=BoritoA4')";

        // FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=BoritoA4");
        // blg 1868  FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kisero");
        FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("KuldKuldemenyekPrintFormSSRSKisero.aspx", QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kisero");
        // blg 1868 FunkcioGombsor.AtveteliElismervenyOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Atveteli");
        FunkcioGombsor.AtveteliElismervenyOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("KuldKuldemenyekPrintFormSSRS.aspx", QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Atveteli");

        FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Kuldemeny')";
        // CR3058
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            FunkcioGombsor.VonalkodNyomtatasa_OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny&" + QueryStringVars.Command + "=Modify" + "')";
        //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny");

        // BUG_7678
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PAGE_FELADASIIDO_KOTELEZO, false))
        {
            BelyegzoDatumaCalendarControl1.Validate = true;
            LabelBelyegzoDatuma.Visible = true;
        }
        else
        {
            BelyegzoDatumaCalendarControl1.Validate = false;
            LabelBelyegzoDatuma.Visible = false;
        }

        if (Command == CommandName.New)
        {
            ClearForm();
            Panel1.Visible = true;
            Panel2.Visible = false;
            SetViewControls(false);
            // BUG_4731
            //if (Startup == Constants.Startup.Szamla || isBekuldoPartnerCimNemTorzsbolDisabled)
            if (isBekuldoPartnerCimNemTorzsbolDisabled)
            {
                CimekTextBox1.FreeTextEnabled = false;
                // strukturált cím (csak törzsbõl lehet)
                tr_FullCimField.Visible = false;

                //BUG_4731
                PartnerTextBox1.FreeTextEnabled = false;

                PartnerTextBox1.WithKuldemeny = false;
                // BUG_4731
                //    if (Startup == Constants.Startup.Szamla)
                //    {
                //        SzamlaAdatokPanel.SetComponentsForFelvitel(EErrorPanel1, ErrorUpdatePanel1);
                //        TargyTextBox.Text = Resources.Form.UI_KuldemenyTargy_Szamla;
                //    }
                //}
                //else
                //{
                //    CimekTextBox1.FreeTextEnabled = isCimSzabadSzovegEnabled;   // rendszerparaméter szabályozza
                //}

            }
            // BUG_4731
            else
            {
                CimekTextBox1.FreeTextEnabled = isCimSzabadSzovegEnabled;   // rendszerparaméter szabályozza
                PartnerTextBox1.FreeTextEnabled = true;
            }

            if (Startup == Constants.Startup.Szamla)
            {
                SzamlaAdatokPanel.SetComponentsForFelvitel(EErrorPanel1, ErrorUpdatePanel1);
                TargyTextBox.Text = Resources.Form.UI_KuldemenyTargy_Szamla;
            }

            #region Template betöltés, ha megadták:

            string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);

            if (!String.IsNullOrEmpty(templateId))
            {
                FormHeader.LoadTemplateObjectById(templateId);
                _TemplateMode = true;
                if (Startup == Constants.Startup.Szamla)
                {
                    LoadComponentsFromTemplate((SzamlaErkeztetesFormTemplateObject)FormHeader.TemplateObject);
                }
                else
                {
                    LoadComponentsFromBusinessObject((EREC_KuldKuldemenyek)FormHeader.TemplateObject);
                }
                _TemplateMode = false;
            }

            #endregion

            FillStandardObjektumTargyszavaiPanel_Kuldemenyek(ParentId);
            FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(ParentId);
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            if (Startup == Constants.Startup.Szamla)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
                FunkcioGombsor.KiserolapVisible = false;
                FunkcioGombsor.AtveteliElismervenyVisible = true;
                FunkcioGombsor.XMLExportVisible = true;

                //HA NEM TÜK-ös
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
                {
                    //CR 3058
                    if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                        FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
                    else
                        FunkcioGombsor.VonalkodNyomtatasa_Visible = false;
                }
                else//HA TÜK-ös
                {
                    bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                    FunkcioGombsor.VonalkodNyomtatasa_Visible = _visible;
                }
            }
            else
            {
                if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
                {
                    FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                    FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
                }
                else
                {
                    FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                    FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
                }

                FunkcioGombsor.KiserolapVisible = true;
                FunkcioGombsor.AtveteliElismervenyVisible = true;
                FunkcioGombsor.XMLExportVisible = true;
                //HA NEM TÜK-ös
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
                {
                    //CR 3058
                    if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                        FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
                    else
                        FunkcioGombsor.VonalkodNyomtatasa_Visible = false;
                }
                else//HA TÜK-ös
                {
                    bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                    FunkcioGombsor.VonalkodNyomtatasa_Visible = _visible;
                }



            }

            Panel1.Visible = false;
            Panel2.Visible = true;

            if (obj_kuldemeny == null)
            {
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = ParentId;

                Result result = service.Get(execParam);

                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                    return;
                }
                else
                {
                    obj_kuldemeny = (EREC_KuldKuldemenyek)result.Record;
                }
            }

            // Kimenõ küldeménynél más panelt kell megjeleníteni:
            if (obj_kuldemeny.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                KimenoKuldemeny_EFormPanel.Visible = true;
                EFormPanel1.Visible = false;
                IktatasAdatokFormPanel.Visible = false;

                KimenoKuld_PostazasiAdatokPanel.SetComponents_ViewMode(obj_kuldemeny, EErrorPanel1, ErrorUpdatePanel1);
                FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(ParentId, KimenoKuld_PostazasiAdatokPanel.KuldesModja);
                FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(ParentId);
            }
            else
            {
                LoadComponentsFromBusinessObject(obj_kuldemeny);

                if (Startup == Constants.Startup.Szamla)
                {
                    SzamlaAdatokPanel.SetComponentsForModositas(obj_kuldemeny.Id, EErrorPanel1, ErrorUpdatePanel1);
                }

                FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(ParentId);
                FillStandardObjektumTargyszavaiPanel_Kuldemenyek(ParentId);
            }


            if (!IsPostBack)
            {
                if (isTUKRendszer)
                {
                    if (Command == CommandName.New)
                    {
                        ReloadIrattariHelyLevelekDropDownTUK(FelelosCsoportTextBox1.HiddenField.Value, obj_kuldemeny.IrattarId);
                    }
                    else if (Command == CommandName.Modify || Command==CommandName.View)
                    {
                        ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, obj_kuldemeny.IrattarId);
                    }
                }
            }
        }

        if (Command == CommandName.Modify)
        {
            Kuldemenyek.Statusz statusz = null;

            if (obj_kuldemeny != null)
            {
                statusz = Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            }
            else
            {
                statusz = Kuldemenyek.GetAllapotById(ParentId, Page, EErrorPanel1);
            }

            //if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva || statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva)
            //{
            //    UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?"
            //        + QueryStringVars.Command + "=" + CommandName.View
            //        + "&" + QueryStringVars.Id + "=" + ParentId);
            //}

            ExecParam execParam = UI.SetExecParamDefault(Page);
            //Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(ParentId, execParam, EErrorPanel1);
            ErrorDetails errorDetail;

            if (!Kuldemenyek.Modosithato(statusz, execParam, out errorDetail))
            {
                // eltüntetjük, hogy ne küldje vissza a szervernek, mert a javascriptes redirect elég lassú
                KuldemenyKuldemenyUpdatePanel.Visible = false;

                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);
                UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?" + qs);
            }
        }

        // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor megkell kulonboztetni gridview felvetel/modositast!!!
        TabFooter1.CommandArgument = Command;

        if (Command == CommandName.View)
        {
            // Allapot szerint beállítjuk a gombok viselkedeset:
            Kuldemenyek.Statusz statusz = null;

            if (obj_kuldemeny == null)
            {
                statusz = Kuldemenyek.GetAllapotById(ParentId, Page, EErrorPanel1);
            }
            else
            {
                statusz = Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            }

            if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva)
            {
                SetIktatasMegtagadvaControls();
            }
            else
            {
                // FunkcioGombSor gombjainak beallitasa
                if (Startup == Constants.Startup.Szamla)
                {
                    FunkcioGombsor.Borito_nyomtatasa_A3Enabled = false;
                    FunkcioGombsor.Borito_nyomtatasa_A4Enabled = false;
                    FunkcioGombsor.KiserolapEnabled = false;
                    FunkcioGombsor.AtveteliElismervenyEnabled = true;
                    FunkcioGombsor.XMLExportEnabled = true;
                }
                else
                {
                    if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
                    {
                        FunkcioGombsor.Borito_nyomtatasa_A3Enabled = false;
                        FunkcioGombsor.Borito_nyomtatasa_A4Enabled = false;
                    }
                    else
                    {
                        FunkcioGombsor.Borito_nyomtatasa_A3Enabled = true;
                        FunkcioGombsor.Borito_nyomtatasa_A4Enabled = true;

                    }

                    FunkcioGombsor.KiserolapEnabled = true;
                    FunkcioGombsor.AtveteliElismervenyEnabled = true;
                    FunkcioGombsor.XMLExportEnabled = true;
                }

                SetViewControls(true);
            }

            // Validálás letiltása néhány komponensnél (korábban nem kötelezõ mezõknél gond lehet, ha nincsenek kitöltve)
            EredetiCimzettCsoportTextBox1.Validate = false;
            FelelosCsoportTextBox1.Validate = false;
        }

        if (Command == CommandName.Modify)
        {
            // Allapot szerint beállítjuk a gombok viselkedeset:

            Kuldemenyek.Statusz statusz = null;

            if (obj_kuldemeny != null)
            {
                statusz = Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            }
            else
            {
                statusz = Kuldemenyek.GetAllapotById(ParentId, Page, EErrorPanel1);
            }

            // FunkcioGombSor gombjainak beallitasa
            if (Startup == Constants.Startup.Szamla)
            {
                FunkcioGombsor.Atadasra_kijelolesVisible = true;
                FunkcioGombsor.SzignalasVisible = true;
                FunkcioGombsor.SztornozasVisible = true;
                FunkcioGombsor.IktatasVisible = false;
                // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                FunkcioGombsor.Munkapeldany_IktatasVisible = false;
                FunkcioGombsor.Kuldemeny_lezarasaVisible = true;
                FunkcioGombsor.ZarolasVisible = true;
                FunkcioGombsor.Zarolas_feloldasaVisible = true;

                FunkcioGombsor.IktatasMegtagadasVisible = false;
            }
            else
            {
                FunkcioGombsor.Atadasra_kijelolesVisible = true;
                FunkcioGombsor.SzignalasVisible = true;
                FunkcioGombsor.SztornozasVisible = true;
                FunkcioGombsor.IktatasVisible = true;
                // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
                FunkcioGombsor.Munkapeldany_IktatasVisible = (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEJOVO_IRAT_MUNKAIRATBA_IKTATAS));
                FunkcioGombsor.Kuldemeny_lezarasaVisible = true;
                FunkcioGombsor.ZarolasVisible = true;
                FunkcioGombsor.Zarolas_feloldasaVisible = true;

                FunkcioGombsor.IktatasMegtagadasVisible = true;
            }

            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
            ErrorDetails errorDetail = null;

            FunkcioGombsor.SztornozasEnabled = Kuldemenyek.Sztornozhato(execparam, statusz, out errorDetail);
            if (FunkcioGombsor.SztornozasEnabled)
            {
                SztornoPopup.SetOnclientClickShowFunction(FunkcioGombsor.SztornozasImage);
            }
            FunkcioGombsor.IktatasEnabled = Kuldemenyek.Iktathato(execparam, statusz, out errorDetail);
            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEJOVO_IRAT_MUNKAIRATBA_IKTATAS))
            {
                FunkcioGombsor.Munkapeldany_IktatasEnabled = Kuldemenyek.Iktathato(execparam, statusz, out errorDetail);
            }
            else
                FunkcioGombsor.Munkapeldany_IktatasEnabled = false;
            FunkcioGombsor.IktatasMegtagadasEnabled = Kuldemenyek.IktatasMegtagadhato(execparam, statusz);

            FunkcioGombsor.Kuldemeny_lezarasaEnabled = Kuldemenyek.Lezarhato(execparam, statusz, out errorDetail);
            FunkcioGombsor.Atadasra_kijelolesEnabled = Kuldemenyek.AtadasraKijelolheto(execparam, statusz, out errorDetail);

            FunkcioGombsor.ZarolasEnabled = true;
            FunkcioGombsor.Zarolas_feloldasaEnabled = true;

            FunkcioGombsor.SzignalasEnabled = Kuldemenyek.Szignalhato(statusz, execparam, out errorDetail);

            // Iktatas:
            if (FunkcioGombsor.IktatasEnabled) { Kuldemenyek.SetIktatas(ParentId, statusz, FunkcioGombsor.IktatasImage, KuldemenyKuldemenyUpdatePanel); }

            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            // Munkapeldany_Iktatas:
            if (FunkcioGombsor.Munkapeldany_IktatasEnabled) { Kuldemenyek.SetIktatasMunkapeldany(ParentId, statusz, FunkcioGombsor.Munkapeldany_IktatasImage, KuldemenyKuldemenyUpdatePanel); }

            // Iktatas megtagadás:
            if (FunkcioGombsor.IktatasMegtagadasEnabled) { Kuldemenyek.SetIktatasMegtagadas(ParentId, statusz, FunkcioGombsor.IktatasMegtagadasImage, KuldemenyKuldemenyUpdatePanel); }

            // Lezaras:
            if (FunkcioGombsor.Kuldemeny_lezarasaEnabled) { Kuldemenyek.SetLezaras(ParentId, statusz, FunkcioGombsor.Kuldemeny_lezarasaImage, KuldemenyKuldemenyUpdatePanel); }

            // Sztornozas:
            if (FunkcioGombsor.SztornozasEnabled) { Kuldemenyek.SetSztorno(ParentId, statusz, FunkcioGombsor.SztornozasImage, KuldemenyKuldemenyUpdatePanel); }

            // Zarolas:
            if (FunkcioGombsor.ZarolasEnabled) { Kuldemenyek.SetZarolas(FunkcioGombsor.ZarolasImage, KuldemenyKuldemenyUpdatePanel); }

            // Zarolas_feloldasa:
            if (FunkcioGombsor.Zarolas_feloldasaEnabled) { Kuldemenyek.SetZarolas_feloldasa(FunkcioGombsor.Zarolas_feloldasaImage, KuldemenyKuldemenyUpdatePanel); }

            // Átadásra kijelölés
            if (FunkcioGombsor.Atadasra_kijelolesEnabled) { Kuldemenyek.SetAtadasraKijeloles(ParentId, statusz, FunkcioGombsor.Atadasra_kijeloles_ImageButton, KuldemenyKuldemenyUpdatePanel); }

            // Szignálás:
            if (FunkcioGombsor.SzignalasEnabled) { Kuldemenyek.SetSzignalas(ParentId, statusz, FunkcioGombsor.SzignalasImageButton, KuldemenyKuldemenyUpdatePanel); }

            if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva)
            {
                SetIktatasMegtagadvaControls();
            }
            else
            {
                TabFooter1.ImageButton_SaveAndClose.Visible = true;

                SetViewControls(false);
                // BUG_4731
                //if (Startup == Constants.Startup.Szamla || isBekuldoPartnerCimNemTorzsbolDisabled)
                if (isBekuldoPartnerCimNemTorzsbolDisabled)
                {
                    CimekTextBox1.FreeTextEnabled = false;
                    // strukturált cím (csak törzsbõl lehet)
                    tr_FullCimField.Visible = false;

                    PartnerTextBox1.WithKuldemeny = false;
                }
                else
                {
                    CimekTextBox1.FreeTextEnabled = isCimSzabadSzovegEnabled;   // rendszerparaméter szabályozza
                }

                PartnerTextBox1.ReadOnly = false;
            }
            AllapotKodtarakDropDownList1.ReadOnly = true;
            //FelbontoFelhasznaloTextBox1.ReadOnly = true;
            BeerkezesiIdoCalendarControl1.ReadOnly = true;
            FelelosCsoportTextBox1.ReadOnly = true;
        }

        if (Command == CommandName.New)
        {
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
            TabFooter1.ImageButton_SaveAndClose.Visible = false;
            TabFooter1.ImageButton_Cancel.Visible = true;
            TabFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        }
        else
        {
            // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
            TabFooter1.ImageButton_Cancel.Visible = false;
            // CR3359 Számla átadásnál (PIR interface) új csoportosító mezõ (ettõl függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            // Számlaküldéssel változhat a számla módosíthatósága, ezért a sima SAVE kikapcsolva
            if (Startup == Constants.Startup.Szamla)
            {
                TabFooter1.ImageButton_Save.Visible = false;
            }
        }

        //if (Startup == Constants.Startup.Szamla)
        //{
        //    FillObjektumTargyszavaiPanel(ParentId, KodTarak.KULDEMENY_TIPUS.Szamla);
        //    if (ObjektumTargyszavaiPanel1.Count == 0)
        //    {
        //        ObjektumTargyszavai_EFormPanel.Visible = false;
        //    }
        //    else
        //    {
        //        ObjektumTargyszavai_EFormPanel.Visible = true;

        //        if (Startup == Constants.Startup.Szamla)
        //        {
        //            Contentum.eUIControls.DynamicValueControl partnerTextBoxDVC = ObjektumTargyszavaiPanel1.FindControlByTargyszo(SzallitoPartnerTextBox_Targyszo);

        //            if (partnerTextBoxDVC != null && partnerTextBoxDVC.Control != null)
        //            {
        //                Component_EditablePartnerTextBox partnerTextBox = partnerTextBoxDVC.Control as Component_EditablePartnerTextBox;
        //                if (partnerTextBox != null)
        //                {
        //                    partnerTextBox.WithKuldemeny = false;

        //                    //partnerTextBox.TryFireChangeEvent = true;

        //                    AdoszamTextBox1.PartnerTextBox = partnerTextBox;
        //                    AdoszamTextBox1.SetTextBoxByPartnerId(EErrorPanel1);
        //                }
        //            }
        //        }
        //    }
        //}

        if (isTUKRendszer && !IsPostBack)
        {
            if (Command == CommandName.New)
            {
                ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null);                
            }
        }

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    #endregion

    #region private methods

    //protected void FillObjektumTargyszavaiPanel(String id, String tipus)
    //{
    //    EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
    //    ObjektumTargyszavaiPanel1.FillObjektumTargyszavai(search, id, null
    //    , Constants.TableNames.EREC_KuldKuldemenyek, Constants.ColumnNames.EREC_KuldKuldemenyek.Tipus, new string[] { tipus }, false
    //    , null, false, false, EErrorPanel1);

    //    ObjektumTargyszavaiPanel1.SetDefaultValues();
    //    TabFooter1.ImageButton_Save.OnClientClick = ObjektumTargyszavaiPanel1.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
    //}

    //business object --> form       
    private void LoadComponentsFromBusinessObject(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        //IraIktatoKonyvekDropDownList1.FillDropDownList("E", EErrorPanel1);

        //ErkeztetoKonyv_DataSetDropDownList.DropDownList.Items.Clear();

        // template betöltésnél:

        if (_TemplateMode)
        {
            ListItem item =
                IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);

            if (item != null)
            {
                IraIktatoKonyvekDropDownList1.DropDownList.SelectedValue = item.Value;
            }
        }

        // Kimenõ küldeménynél nincs érkeztetõszám, nem kell lekérni:
        if (erec_KuldKuldemenyek.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            // Template betöltésnél (érkeztetésnél) nem kell:
            if (!_TemplateMode)
            {
                ErkeztetoszamTolReadOnlyTextBox1.Text = erec_KuldKuldemenyek.Azonosito;
            }
        }

        if (String.IsNullOrEmpty(erec_KuldKuldemenyek.KuldKuldemeny_Id_Szulo))
        {
            ErkeztetoszamIgReadOnlyTextBox2.Text = "";
        }
        else
        {
            //LZs - BUG_13264
            #region BUG_13264
            ExecParam execParam_Szulo = UI.SetExecParamDefault(Page, new ExecParam());
            execParam_Szulo.Record_Id = erec_KuldKuldemenyek.KuldKuldemeny_Id_Szulo;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ErkeztetoszamIgReadOnlyTextBox2.Text = (service.Get(execParam_Szulo).Record as EREC_KuldKuldemenyek).Azonosito;
            #endregion
        }

        // Header címének beállítása:
        if (Startup == Constants.Startup.Szamla)
        {
            if (Command == CommandName.View)
            {
                FormHeader.HeaderTitle = ErkeztetoszamTolReadOnlyTextBox1.Text + " " + Resources.Form.KuldemenyekFormHeaderTitle_Szamla;
            }
            else if (Command == CommandName.Modify)
            {
                FormHeader.HeaderTitle = ErkeztetoszamTolReadOnlyTextBox1.Text + " " + Resources.Form.KuldemenyekFormHeaderTitle_Szamla;
            }
        }
        else if (Command == CommandName.View)
        {
            FormHeader.FullManualHeaderTitle = ErkeztetoszamTolReadOnlyTextBox1.Text + " " + Resources.Form.Kuldemenymegtekintese_ManualFormHeaderTitle;
        }
        else if (Command == CommandName.Modify)
        {
            FormHeader.FullManualHeaderTitle = ErkeztetoszamTolReadOnlyTextBox1.Text + " " + Resources.Form.Kuldemenymodositasa_ManualFormHeaderTitle;
        }

        AllapotKodtarakDropDownList1.FillAndSetSelectedValue(KodCsoportAllapot, erec_KuldKuldemenyek.Allapot, EErrorPanel1);

        // Ha iktatott a küldemény, az iktatott irat megjelenítése:
        if (erec_KuldKuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva)
        {
            Label_IktatottIrat.Visible = true;
            IratTextBox1.Visible = true;
            IratTextBox1.SetIratTextBoxByKuldemenyId(erec_KuldKuldemenyek.Id, EErrorPanel1, ErrorUpdatePanel1);
        }

        if (Command == CommandName.View)
        {
            KodtarakDropDownListIktatasiKotelezettseg.FillAndSetSelectedValue(KodCsoportIktatasikotelezettseg, erec_KuldKuldemenyek.IktatniKell, EErrorPanel1);
        }
        else
        {
            // Alapesetben csak az Iktatandó/Nem iktatandó értéket engedjük meg:
            var filterList_iktatasiKotelezettseg = new List<string>
            {
                KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando,
                KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando
            };

            KodtarakDropDownListIktatasiKotelezettseg.FillAndSetSelectedValue(KodCsoportIktatasikotelezettseg, erec_KuldKuldemenyek.IktatniKell
                , filterList_iktatasiKotelezettseg, false, EErrorPanel1);
        }

        #region Iktatás státuszának feltöltése:

        string iktatasStatusz = String.Empty;

        if (erec_KuldKuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva)
        {
            iktatasStatusz = KodTarak.KULDEMENY_IKTATAS_STATUSZA.Iktatva;
        }
        else if (erec_KuldKuldemenyek.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando)
        {
            iktatasStatusz = KodTarak.KULDEMENY_IKTATAS_STATUSZA.Nem_kell_iktatni;
        }
        else if (erec_KuldKuldemenyek.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Teves_iktatas)
        {
            iktatasStatusz = KodTarak.KULDEMENY_IKTATAS_STATUSZA.Teves_iktatas;
        }
        else
        {
            // CR#2229: Certop követelmény: iktatás státusza a csak érkeztetett küldeménynél legyen üres („nincs iktatva” helyett)
            //iktatasStatusz = KodTarak.KULDEMENY_IKTATAS_STATUSZA.Nincs_iktatva;
        }
        if (!String.IsNullOrEmpty(iktatasStatusz))
        {
            KodtarakDropDownList_IktatasStatusza.FillWithOneValue(KodCsoportKuldemenyIktatasStatusza, iktatasStatusz, EErrorPanel1);
        }
        else
        {
            // CR#2229: Certop követelmény: iktatás státusza a csak érkeztetett küldeménynél legyen üres („nincs iktatva” helyett)
            KodtarakDropDownList_IktatasStatusza.Clear();
        }

        #endregion

        if (String.IsNullOrEmpty(erec_KuldKuldemenyek.BoritoTipus))
        {
            KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(KodCsoportBoritoTipus, erec_KuldKuldemenyek.BoritoTipus, true, EErrorPanel1);
        }
        else
        {
            KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(KodCsoportBoritoTipus, erec_KuldKuldemenyek.BoritoTipus, false, EErrorPanel1);
        }

        PartnerTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
        PartnerTextBox1.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;
        //BUG 5197
        PartnerTextBox1.KapcsoltPartnerDropDownValue = erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt;
        //if (Command == CommandName.View && String.IsNullOrEmpty(erec_KuldKuldemenyek.Partner_Id_Bekuldo))
        //{
        //    PartnerTextBox1.ImageButton_View.Enabled = false;
        //    PartnerTextBox1.ImageButton_View.CssClass = "disabledLovListItem";
        //}

        if (EErrorPanel1.Parent.Parent.GetType() == typeof(System.Web.UI.UpdatePanel))
        {
            (EErrorPanel1.Parent.Parent as System.Web.UI.UpdatePanel).Update();
        }

        CimekTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Cim_Id;
        CimekTextBox1.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;
        if (Command == CommandName.View && String.IsNullOrEmpty(erec_KuldKuldemenyek.Cim_Id))
        {
            CimekTextBox1.ImageButton_View.Enabled = false;
            CimekTextBox1.ImageButton_View.CssClass = "disabledLovListItem";
        }

        KuldesModja_DropDownList.FillAndSetSelectedValue(KodCsoportKuldemenyKuldesModja, erec_KuldKuldemenyek.KuldesMod, EErrorPanel1);

        if (!_TemplateMode)
        {
            BeerkezesiIdoCalendarControl1.Text = erec_KuldKuldemenyek.BeerkezesIdeje;
            CsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Base.Letrehozo_id;
            CsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
        }
        //Tovabbito_DropDownList.FillAndSetSelectedValue(KodCsoportTovabbitoSzervezet, erec_KuldKuldemenyek.Tovabbito, EErrorPanel1);                
        //FelbontoFelhasznaloTextBox1.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
        //FelbontoFelhasznaloTextBox1.SetFelhasznaloTextBoxById(EErrorPanel1);

        //FelbontasiIdoCalendarControl1.Text = erec_KuldKuldemenyek.FelbontasDatuma;

        BelyegzoDatumaCalendarControl1.Text = erec_KuldKuldemenyek.BelyegzoDatuma;

        HivatkozasiSzam_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

        //adathordozó típusa ne legyen üres
        if (_TemplateMode)
        {
            if (String.IsNullOrEmpty(erec_KuldKuldemenyek.AdathordozoTipusa))
            {
                erec_KuldKuldemenyek.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
            }
        }

        AdathordozoTipusa_DropDownList.FillAndSetSelectedValue(KodCsoportUgyintezesAlapja, erec_KuldKuldemenyek.AdathordozoTipusa, EErrorPanel1);
        // BLG_361
        //UgyintezesAlapja_DropDownList.FillAndSetSelectedValue(KodCsoportUgyintezesAlapja, erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
        // BUG_2051
        //UgyintezesAlapja_DropDownList.FillAndSetSelectedValue(KodCsoportELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
        UgyintezesAlapja_DropDownList.FillAndSetSelectedValue(KodCsoportELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);        //ElsodlegesAdathordozo_KodtarakDropDownList.FillAndSetSelectedValue(KodCsoportELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

        ktDropDownListKuldemenyMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_KuldKuldemenyek.Minosites, true, EErrorPanel1);

        RagszamRequiredTextBox.Text = erec_KuldKuldemenyek.RagSzam;

        VonalkodTextBoxVonalkod.Text = erec_KuldKuldemenyek.BarCode;

        EredetiCimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
        EredetiCimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        Surgosseg_DropDownList.FillAndSetSelectedValue(KodCsoportSurgosseg, erec_KuldKuldemenyek.Surgosseg, EErrorPanel1);
        FelelosCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Felelos;
        FelelosCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
        TargyTextBox.Text = erec_KuldKuldemenyek.Targy;
        Tartalom_TextBox.Text = erec_KuldKuldemenyek.Tartalom;
        bontasMegjegyzesTextBox.Text = erec_KuldKuldemenyek.BontasiMegjegyzes;

        //bernat.laszlo added

        Kezbesites_modja_DropDownList.FillAndSetSelectedValue(KodCsoport_KULD_KEZB_MODJA,
        erec_KuldKuldemenyek.KezbesitesModja, true, EErrorPanel1);
        CimzesTipusa_DropDownList.FillAndSetSelectedValue(KodCsoportKULD_CIMZES_TIPUS,
        erec_KuldKuldemenyek.CimzesTipusa, true, EErrorPanel1);

        // nekrisz modified (CR 3000)
        if (erec_KuldKuldemenyek == null)
        {
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
        }
        else
            if (string.IsNullOrEmpty(erec_KuldKuldemenyek.Munkaallomas))
        {
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
        }
        else Munkaallomas_TextBox.Text = erec_KuldKuldemenyek.Munkaallomas;

        #region Iktatást nem igényel setup
        if (string.IsNullOrEmpty(erec_KuldKuldemenyek.IktatastNemIgenyel.ToString()))
        {
            Ikt_n_igeny_Igen_RadioButton.Checked = true;
        }
        else
        {
            if (erec_KuldKuldemenyek.IktatastNemIgenyel == "1")
            {
                Ikt_n_igeny_Igen_RadioButton.Checked = true;
            }
            else
            {
                Ikt_n_igeny_Nem_RadioButton.Checked = true;
            }
        }
        #endregion

        #region Sérült küldemény setup
        if (string.IsNullOrEmpty(erec_KuldKuldemenyek.SerultKuldemeny.ToString()))
        {
            serult_kuld_Nem_RadioButton.Checked = true;
        }
        else
        {
            if (erec_KuldKuldemenyek.SerultKuldemeny == "1")
            {
                serult_kuld_Igen_RadioButton.Checked = true;
            }
            else
            {
                serult_kuld_Nem_RadioButton.Checked = true;
            }
        }
        #endregion

        #region Téves címzés setup
        if (string.IsNullOrEmpty(erec_KuldKuldemenyek.TevesCimzes.ToString()))
        {
            teves_cim_Nem_RadioButton.Checked = true;
        }
        else
        {
            if (erec_KuldKuldemenyek.TevesCimzes == "1")
            {
                teves_cim_Igen_RadioButton.Checked = true;
            }
            else
            {
                teves_cim_Nem_RadioButton.Checked = true;
            }
        }
        #endregion

        #region Téves érkeztetés setup
        if (string.IsNullOrEmpty(erec_KuldKuldemenyek.TevesErkeztetes.ToString()))
        {
            teves_erk_Nem_RadioButton.Checked = true;
        }
        else
        {
            if (erec_KuldKuldemenyek.TevesErkeztetes == "1")
            {
                teves_erk_Igen_RadioButton.Checked = true;
            }
            else
            {
                teves_erk_Nem_RadioButton.Checked = true;
            }
        }
        #endregion
        ErkeztetesiIdoCalendarControl1.Text = erec_KuldKuldemenyek.Base.LetrehozasIdo;

        //bernat.laszlo eddig

        LoadBontoPanel(erec_KuldKuldemenyek);

        #region EmailMegtagadas
        if (erec_KuldKuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva)
        {
            IktatasMegtagadasPanel.Visible = true;
            MegtagadoId_FelhasznaloTextBox.Id_HiddenField = erec_KuldKuldemenyek.Megtagado_Id;
            MegtagadoId_FelhasznaloTextBox.SetFelhasznaloTextBoxById(EErrorPanel1);
            MegtagadoId_FelhasznaloTextBox.LovImageButtonVisible = false;
            MegtagadoId_FelhasznaloTextBox.ResetImageButtonVisible = false;
            MegtagadoId_FelhasznaloTextBox.NewImageButtonVisible = false;
            MegtagadasDat_CalendarControl.Text = erec_KuldKuldemenyek.MegtagadasDat;
            //TextBoxMegtagadasIndoka.Text = erec_KuldKuldemenyek.MegtagadasIndoka;
            labelMegtagadasIndoka.Text = erec_KuldKuldemenyek.MegtagadasIndoka;
        }
        else
        {
            IktatasMegtagadasPanel.Visible = false;
        }

        #endregion EmailMegtagadas

        #region Iktatási adatok

        if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id))
        {
            IktatasAdatokFormPanel.Visible = true;

            LoadIktatasiAdatok(erec_KuldKuldemenyek);
        }

        #endregion

        #region Mellékletek, Csatolmányok száma

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MellekletekSzama_RequiredNumberBox1.Text = GetMellekletekSzama(erec_KuldKuldemenyek.Id);
            CsatolmanyokSzama_RequiredNumberBox1.Text = GetCsatolmanyokSzama(erec_KuldKuldemenyek.Id);
        }

        //bernat.laszlo added
        if (Command == CommandName.View)
        {
            teves_cim_Igen_RadioButton.Enabled = false;
            teves_cim_Nem_RadioButton.Enabled = false;
            teves_erk_Igen_RadioButton.Enabled = false;
            teves_erk_Nem_RadioButton.Enabled = false;
            Kezbesites_modja_DropDownList.Enabled = false;
            Ikt_n_igeny_Igen_RadioButton.Enabled = false;
            Ikt_n_igeny_Nem_RadioButton.Enabled = false;
            serult_kuld_Igen_RadioButton.Enabled = false;
            serult_kuld_Nem_RadioButton.Enabled = false;
            CimzesTipusa_DropDownList.Enabled = false;
        }
        //bernat.laszlo eddig

        #endregion

        Record_Ver_HiddenField.Value = erec_KuldKuldemenyek.Base.Ver;

        if (IsRequireOverwriteTemplateDatas())
        {
            LoadSessionDatas();
        }


        #region BLG_452
        // BLG_1721
        //LoadFutarJegyzekParameters(erec_KuldKuldemenyek);
        LoadKezbesitesParameters(erec_KuldKuldemenyek);
        #endregion BLG_452

        #region MINOSITO
        if (!string.IsNullOrEmpty(erec_KuldKuldemenyek.Minosito))
        {
            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId = erec_KuldKuldemenyek.Minosito;
            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoSzuloPartnerId = erec_KuldKuldemenyek.MinositoSzervezet;
            // Névfeloldás Id alapján:
            MinositoPartnerControlKuldKuldemenyekFormTab.SetPartnerTextBoxById(EErrorPanel1);
        }
        #endregion

        //if (isTUKRendszer)
        //{
        //    if (Command == CommandName.Modify || Command == CommandName.View)
        //    {
        //        ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id);
        //    }
        //    else
        //    {
        //        ReloadIrattariHelyLevelekDropDownTUK(FelelosCsoportTextBox1.HiddenField.Value);
        //    }
        //}


        TextBox_KulsoAzonosito.Text = erec_KuldKuldemenyek.KulsoAzonosito;
    }

    private void LoadIktatasiAdatok(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        string iraIratok_Id = erec_KuldKuldemenyek.IraIratok_Id;
        if (String.IsNullOrEmpty(iraIratok_Id)) { return; }

        // Irat Get
        EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
        execParam_iratGet.Record_Id = iraIratok_Id;

        Result result_iratGet = service_iratok.Get(execParam_iratGet);

        if (result_iratGet.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iratGet);
            ErrorUpdatePanel1.Update();
            return;
        }

        EREC_IraIratok iraIrat = (EREC_IraIratok)result_iratGet.Record;

        // EREC_IratMetaDefiniciok Get (GetAllWithExtension)
        if (!String.IsNullOrEmpty(iraIrat.IratMetaDef_Id))
        {
            EREC_IratMetaDefinicioService service_iratMetaDef = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam execParam_getAll = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratMetaDefinicioSearch search_iratMetaDef = new EREC_IratMetaDefinicioSearch();

            search_iratMetaDef.Id.Filter(iraIrat.IratMetaDef_Id);

            Result result_getAll = service_iratMetaDef.GetAllWithExtension(execParam_getAll, search_iratMetaDef);

            if (result_getAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_getAll);
                ErrorUpdatePanel1.Update();
                return;
            }
            else
            {
                // egy sort kellett kapnunk:
                if (result_getAll.GetCount != 1)
                {
                    // TODO: hiba jelzése:
                }
                else
                {
                    DataRow row = result_getAll.Ds.Tables[0].Rows[0];

                    string agazatiJel_Kod = row["AgazatiJel_Kod"].ToString();
                    string agazatiJel_Nev = row["AgazatiJel_Nev"].ToString();

                    if (!String.IsNullOrEmpty(agazatiJel_Kod)
                        || !String.IsNullOrEmpty(agazatiJel_Nev))
                    {
                        IktAdatok_AgJel_TextBox.Text = "[" + agazatiJel_Kod + "] " + agazatiJel_Nev;
                    }

                    string Merge_IrattariTetelszam = row["Merge_IrattariTetelszam"].ToString();
                    string ugykor_Id = row["Ugykor_Id"].ToString();

                    IktAdatok_IraIrattariTetelTextBox.Id_HiddenField = ugykor_Id;
                    IktAdatok_IraIrattariTetelTextBox.Text = Merge_IrattariTetelszam;

                    string UgytipusNev = row["UgytipusNev"].ToString();

                    IktAdatok_Ugytipus_TextBox.Text = UgytipusNev;

                    string SzignalasTipusa = row["SzignalasTipusa"].ToString();

                    IktAdatok_SzignTipus_TextBox.Text = SzignalasTipusa;

                    // ha nem tudtuk meghatározni, megpróbáljuk a küldemény Iktathato mezõjébõl:
                    if (String.IsNullOrEmpty(SzignalasTipusa) && !String.IsNullOrEmpty(erec_KuldKuldemenyek.Iktathato))
                    {
                        Dictionary<string, string> szignalasTipusok =
                            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("SZIGNALAS_TIPUSA", Page);

                        switch (erec_KuldKuldemenyek.Iktathato)
                        {
                            case "U":
                                // Szervezetre, ügyintézõre                            
                                if (szignalasTipusok.ContainsKey(KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas))
                                {
                                    IktAdatok_SzignTipus_TextBox.Text =
                                        szignalasTipusok[KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas];
                                }
                                break;
                            case "S":
                                // Szervezetre
                                if (szignalasTipusok.ContainsKey(KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore))
                                {
                                    IktAdatok_SzignTipus_TextBox.Text =
                                        szignalasTipusok[KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore];
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    private void LoadBontoPanel(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (String.IsNullOrEmpty(erec_KuldKuldemenyek.FelbontasDatuma) && String.IsNullOrEmpty(erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto))
        {
            if (FunctionRights.GetFunkcioJog(Page, "KuldemenyFizikaiBontas"))
            {
                if (Command == CommandName.Modify)
                {
                    if (!_TemplateMode)
                    {
                        FelbontasiIdoCalendarControl1.Text = DateTime.Now.ToString();
                        FelbontoFelhasznaloTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                        FelbontoFelhasznaloTextBox1.SetFelhasznaloTextBoxById(EErrorPanel1);
                    }
                }
                else if (Command == CommandName.New)
                {
                    if (_TemplateMode)
                    {
                        // ha volt bontási megjegyzés a template-ben, akkor nem tekintjük bontás nélkülinek az érkeztetést
                        cbBontoasNelkul.Checked = String.IsNullOrEmpty(erec_KuldKuldemenyek.BontasiMegjegyzes);
                        cbBontoasNelkul_CheckedChanged(cbBontoasNelkul, new EventArgs());
                    }
                }
            }
        }
        else
        {
            if (!_TemplateMode)
            {
                FelbontasiIdoCalendarControl1.Text = erec_KuldKuldemenyek.FelbontasDatuma;
                FelbontasiIdoCalendarControl1.ReadOnly = true;
            }
            FelbontoFelhasznaloTextBox1.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
            FelbontoFelhasznaloTextBox1.SetFelhasznaloTextBoxById(EErrorPanel1);

            // CR#2242: ha érkeztetünk és template-bõl töltünk be, nem tüntetjük el a checkboxot és
            // nem tiltjuk le a bontó mezõt!
            // (feltehetõen a fentiek azért kerültek be, hogy a már bontott küldeményt módosításkor ne lehessen
            // ismét fel nem bontottra állítani)
            if (Command == CommandName.New && _TemplateMode)
            {
                FelbontoFelhasznaloTextBox1.ReadOnly = false;
                cbBontoasNelkul.Visible = true;
                cbBontoasNelkul.Checked = false;
                cbBontoasNelkul_CheckedChanged(cbBontoasNelkul, new EventArgs());
            }
            else
            {
                FelbontoFelhasznaloTextBox1.ReadOnly = true;
                cbBontoasNelkul.Visible = false;
            }
        }
    }

    private string GetMellekletekSzama(string kuldemenyId)
    {
        if (String.IsNullOrEmpty(kuldemenyId)) { return String.Empty; }

        EREC_MellekletekService service_mellekletek = eRecordService.ServiceFactory.GetEREC_MellekletekService();
        EREC_MellekletekSearch search_mellekletek = new EREC_MellekletekSearch();

        search_mellekletek.KuldKuldemeny_Id.Filter(kuldemenyId);

        var notMellekletTipus = new List<string>
        {
            KodTarak.AdathordozoTipus.Boritek,
            KodTarak.AdathordozoTipus.PapirAlapu
        };
        search_mellekletek.AdathordozoTipus.NotIn(notMellekletTipus);

        Result result = service_mellekletek.GetAll(UI.SetExecParamDefault(Page), search_mellekletek);

        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            return String.Empty;
        }

        int mellekletCount = result.GetCount;

        return mellekletCount.ToString();
    }

    private string GetCsatolmanyokSzama(string kuldemenyId)
    {
        if (String.IsNullOrEmpty(kuldemenyId)) { return String.Empty; }

        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        search.KuldKuldemeny_Id.Filter(kuldemenyId);

        Result result = service.GetAll(UI.SetExecParamDefault(Page), search);

        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            return String.Empty;
        }

        int csatolmanyokCount = result.GetCount;

        return csatolmanyokCount.ToString();
    }

    // form --> business object
    private EREC_KuldKuldemenyek GetBusinessObjectFromComponents()
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyek.Tipus = KodTarak.KULDEMENY_TIPUS.Szamla;
            erec_KuldKuldemenyek.Updated.Tipus = true;
        }

        if (Command == CommandName.New)
        {
            erec_KuldKuldemenyek.IraIktatokonyv_Id = IraIktatoKonyvekDropDownList1.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(IraIktatoKonyvekDropDownList1);

            // az Erkeztetes web service-ben fogjuk beállítani,nem a felületrõl
            //erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
            //    FelhasznaloProfil.FelhasznaloId(Page));
            //erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;
        }

        if (Command == CommandName.Modify)
        {
            // elvileg nem kell updatelni az Erkezteto_Szam-ot, mert nem valtozhat!!!
            //erec_KuldKuldemenyek.Erkezteto_Szam = ErkeztetoszamTolReadOnlyTextBox1.Text;
            //erec_KuldKuldemenyek.Updated.Erkezteto_Szam = true;
        }

        // az Erkeztetes web service-ben fogjuk beállítani, nem a felületrõl
        //erec_KuldKuldemenyek.Allapot = AllapotKodtarakDropDownList1.SelectedValue;
        //erec_KuldKuldemenyek.Updated.Allapot = pageView.GetUpdatedByView(AllapotKodtarakDropDownList1);

        //if (Command == CommandName.New)
        //{
        //    erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
        //    erec_KuldKuldemenyek.Updated.IktatniKell = true;
        //}
        //else
        //{
        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando;
            erec_KuldKuldemenyek.Updated.IktatniKell = true;
        }
        else
        {
            erec_KuldKuldemenyek.IktatniKell = KodtarakDropDownListIktatasiKotelezettseg.SelectedValue;
            erec_KuldKuldemenyek.Updated.IktatniKell = pageView.GetUpdatedByView(KodtarakDropDownListIktatasiKotelezettseg);
        }
        //}
        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
            erec_KuldKuldemenyek.Updated.MegorzesJelzo = true;

            erec_KuldKuldemenyek.BoritoTipus = KodTarak.BoritoTipus.Irat;
            erec_KuldKuldemenyek.Updated.BoritoTipus = true;
        }
        else
        {
            switch (KodtarakDropDownListBoritoTipus.SelectedValue)
            {
                case KodTarak.BoritoTipus.Boritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.Irat:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.EldobhatoBoritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.No;
                    break;
            }
            erec_KuldKuldemenyek.Updated.MegorzesJelzo = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            erec_KuldKuldemenyek.BoritoTipus = KodtarakDropDownListBoritoTipus.SelectedValue;
            erec_KuldKuldemenyek.Updated.BoritoTipus = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);
        }

        erec_KuldKuldemenyek.Partner_Id_Bekuldo = PartnerTextBox1.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(PartnerTextBox1);

        erec_KuldKuldemenyek.NevSTR_Bekuldo = PartnerTextBox1.Text;
        erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(PartnerTextBox1);

        #region BUG 5197
        erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt = PartnerTextBox1.KapcsoltPartnerDropDownValue;
        erec_KuldKuldemenyek.Updated.Partner_Id_BekuldoKapcsolt = pageView.GetUpdatedByView(PartnerTextBox1);

        PartnerTextBox1.CheckPartnerCimek(PartnerTextBox1.KapcsoltPartnerDropDownValue, CimekTextBox1.Id_HiddenField, Record_Ver_HiddenField.Value, EErrorPanel1);
        #endregion

        erec_KuldKuldemenyek.Cim_Id = CimekTextBox1.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(CimekTextBox1);

        erec_KuldKuldemenyek.CimSTR_Bekuldo = CimekTextBox1.Text;
        erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(CimekTextBox1);

        erec_KuldKuldemenyek.KuldesMod = KuldesModja_DropDownList.SelectedValue;
        erec_KuldKuldemenyek.Updated.KuldesMod = pageView.GetUpdatedByView(KuldesModja_DropDownList);

        erec_KuldKuldemenyek.Minosites = ktDropDownListKuldemenyMinosites.SelectedValue;
        erec_KuldKuldemenyek.Updated.Minosites = pageView.GetUpdatedByView(ktDropDownListKuldemenyMinosites);

        if (!_TemplateMode)
        {
            DateTime beerkezes;

            if (!DateTime.TryParse(BeerkezesiIdoCalendarControl1.Text, out beerkezes))
            {
                throw new FormatException("A beérkezés idõpontjának megadott érték nem megfelelõ formátumú!");
            }
            if (beerkezes > DateTime.Now)
            {
                throw new FormatException("A beérkezés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
            }
            erec_KuldKuldemenyek.BeerkezesIdeje = BeerkezesiIdoCalendarControl1.Text;
            erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(BeerkezesiIdoCalendarControl1);

            // BUG_7678
            if (!string.IsNullOrEmpty(BelyegzoDatumaCalendarControl1.Text))
            {
                DateTime feladasiIdo;
                if (!DateTime.TryParse(BelyegzoDatumaCalendarControl1.Text, out feladasiIdo))
                {
                    throw new FormatException("A feladási idõnek megadott érték nem megfelelõ formátumú!");
                }
                if (feladasiIdo > beerkezes)
                {
                    throw new FormatException("A feladási idõ nem lehet későbbi mint a beérkezés ideje!");
                }
                erec_KuldKuldemenyek.BelyegzoDatuma = BelyegzoDatumaCalendarControl1.Text;
                erec_KuldKuldemenyek.Updated.BelyegzoDatuma = pageView.GetUpdatedByView(BelyegzoDatumaCalendarControl1);
            }
        }
        //bernat.laszlo added : Érkeztetési idõ
        if (Command == CommandName.New)
        {
            erec_KuldKuldemenyek.Base.LetrehozasIdo = ErkeztetesiIdoCalendarControl1.Text;
            erec_KuldKuldemenyek.Base.Updated.LetrehozasIdo = true;

            // nekrisz (CR3000)
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            erec_KuldKuldemenyek.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
            erec_KuldKuldemenyek.Updated.Munkaallomas = true;
        }
        //bernat.laszlo eddig
        //erec_KuldKuldemenyek.Tovabbito = Tovabbito_DropDownList.SelectedValue;
        //erec_KuldKuldemenyek.Updated.Tovabbito = pageView.GetUpdatedByView(Tovabbito_DropDownList);

        //erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto = FelbontoFelhasznaloTextBox1.Id_HiddenField;
        //erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Bonto = pageView.GetUpdatedByView(FelbontoFelhasznaloTextBox1);

        //erec_KuldKuldemenyek.FelbontasDatuma = FelbontasiIdoCalendarControl1.Text;

        // BUG_7678
        //erec_KuldKuldemenyek.BelyegzoDatuma = BelyegzoDatumaCalendarControl1.Text;
        //erec_KuldKuldemenyek.Updated.BelyegzoDatuma = pageView.GetUpdatedByView(BelyegzoDatumaCalendarControl1);


        erec_KuldKuldemenyek.HivatkozasiSzam = HivatkozasiSzam_TextBox.Text;
        erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(HivatkozasiSzam_TextBox);

        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyek.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
            erec_KuldKuldemenyek.Updated.AdathordozoTipusa = true;

            erec_KuldKuldemenyek.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = true;
        }
        else
        {
            erec_KuldKuldemenyek.AdathordozoTipusa = AdathordozoTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.AdathordozoTipusa = pageView.GetUpdatedByView(AdathordozoTipusa_DropDownList);

            // BUG_2051
            //erec_KuldKuldemenyek.UgyintezesModja = UgyintezesAlapja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.UgyintezesModja = AdathordozoTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesAlapja_DropDownList);

            // BUG_2051
            erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa = UgyintezesAlapja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.ElsodlegesAdathordozoTipusa = pageView.GetUpdatedByView(UgyintezesAlapja_DropDownList);
        }
        //erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa = ElsodlegesAdathordozo_KodtarakDropDownList.SelectedValue;
        //erec_KuldKuldemenyek.Updated.Elektronikus_Kezbesitesi_Allap = pageView.GetUpdatedByView(ElsodlegesAdathordozo_KodtarakDropDownList);

        erec_KuldKuldemenyek.RagSzam = RagszamRequiredTextBox.Text;
        erec_KuldKuldemenyek.Updated.RagSzam = pageView.GetUpdatedByView(RagszamRequiredTextBox);

        erec_KuldKuldemenyek.BarCode = VonalkodTextBoxVonalkod.Text;
        erec_KuldKuldemenyek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBoxVonalkod);

        erec_KuldKuldemenyek.Csoport_Id_Cimzett = EredetiCimzettCsoportTextBox1.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(EredetiCimzettCsoportTextBox1);

        if (Startup == Constants.Startup.Szamla)
        {
            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;
        }
        else
        {
            erec_KuldKuldemenyek.Surgosseg = Surgosseg_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.Surgosseg = pageView.GetUpdatedByView(Surgosseg_DropDownList);
        }

        erec_KuldKuldemenyek.Csoport_Id_Felelos = FelelosCsoportTextBox1.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(FelelosCsoportTextBox1);

        erec_KuldKuldemenyek.Targy = TargyTextBox.Text;
        erec_KuldKuldemenyek.Updated.Targy = pageView.GetUpdatedByView(TargyTextBox);

        erec_KuldKuldemenyek.Tartalom = Tartalom_TextBox.Text;
        erec_KuldKuldemenyek.Updated.Tartalom = pageView.GetUpdatedByView(Tartalom_TextBox);

        erec_KuldKuldemenyek.Base.Ver = Record_Ver_HiddenField.Value;
        erec_KuldKuldemenyek.Base.Updated.Ver = true;

        erec_KuldKuldemenyek.PeldanySzam = "1";
        erec_KuldKuldemenyek.Updated.PeldanySzam = true;

        // Áttéve webservice-be
        //erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        //erec_KuldKuldemenyek.Updated.PostazasIranya = true;

        erec_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
        erec_KuldKuldemenyek.Updated.Erkeztetes_Ev = true;

        // CR#2242: áttéve a GetFelbontasPanelDatas-ba
        //erec_KuldKuldemenyek.BontasiMegjegyzes = bontasMegjegyzesTextBox.Text;
        //erec_KuldKuldemenyek.Updated.BontasiMegjegyzes = true;

        //bernat.laszlo added
        #region Iktatást nem igényel setup
        if (Ikt_n_igeny_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyek.IktatastNemIgenyel = "1";
            erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = pageView.GetUpdatedByView(Ikt_n_igeny_Igen_RadioButton);
        }
        else
        {
            erec_KuldKuldemenyek.IktatastNemIgenyel = "0";
            erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = pageView.GetUpdatedByView(Ikt_n_igeny_Igen_RadioButton);
        }
        #endregion

        erec_KuldKuldemenyek.KezbesitesModja = Kezbesites_modja_DropDownList.SelectedValue;
        erec_KuldKuldemenyek.Updated.KezbesitesModja = pageView.GetUpdatedByView(Kezbesites_modja_DropDownList);

        erec_KuldKuldemenyek.CimzesTipusa = CimzesTipusa_DropDownList.SelectedValue;
        erec_KuldKuldemenyek.Updated.CimzesTipusa = pageView.GetUpdatedByView(CimzesTipusa_DropDownList);

        // nekrisz (CR 3000) - csak New esetén kell tölteni
        //ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
        //erec_KuldKuldemenyek.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
        //erec_KuldKuldemenyek.Updated.Munkaallomas = pageView.GetUpdatedByView(Munkaallomas_TextBox);

        #region Sérült küldemény setup
        if (serult_kuld_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyek.SerultKuldemeny = "1";
            erec_KuldKuldemenyek.Updated.SerultKuldemeny = pageView.GetUpdatedByView(serult_kuld_Igen_RadioButton);
        }
        else
        {
            erec_KuldKuldemenyek.SerultKuldemeny = "0";
            erec_KuldKuldemenyek.Updated.SerultKuldemeny = pageView.GetUpdatedByView(serult_kuld_Igen_RadioButton);
        }
        #endregion

        #region Téves címzés setup
        if (teves_cim_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyek.TevesCimzes = "1";
            erec_KuldKuldemenyek.Updated.TevesCimzes = pageView.GetUpdatedByView(teves_cim_Igen_RadioButton);
        }
        else
        {
            erec_KuldKuldemenyek.TevesCimzes = "0";
            erec_KuldKuldemenyek.Updated.TevesCimzes = pageView.GetUpdatedByView(teves_cim_Igen_RadioButton);
        }
        #endregion

        #region Téves érkeztetés setup
        if (teves_erk_Igen_RadioButton.Checked)
        {
            erec_KuldKuldemenyek.TevesErkeztetes = "1";
            erec_KuldKuldemenyek.Updated.TevesErkeztetes = pageView.GetUpdatedByView(teves_erk_Igen_RadioButton);
        }
        else
        {
            erec_KuldKuldemenyek.TevesErkeztetes = "0";
            erec_KuldKuldemenyek.Updated.TevesErkeztetes = pageView.GetUpdatedByView(teves_erk_Igen_RadioButton);
        }
        #endregion
        //bernat.laszlo eddig

        GetFelbontoPanelDatas(erec_KuldKuldemenyek);

        #region BLG_452
        // BLG_1721
        //FillKuldemenyBOFromFutarJegyzekParameters(erec_KuldKuldemenyek);
        FillKuldemenyBOFromKezbesitesParameters(erec_KuldKuldemenyek);
        #endregion BLG_452

        #region MINOSITO
        if (!string.IsNullOrEmpty(MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId))
        {
            erec_KuldKuldemenyek.Minosito = MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId;
            erec_KuldKuldemenyek.Updated.Minosito = true;
        }
        #endregion

        if (isTUKRendszer)
        {

            erec_KuldKuldemenyek.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_KuldKuldemenyek.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            erec_KuldKuldemenyek.IrattariHely = IrattariHelyLevelekDropDownTUK.SelectedText;
            erec_KuldKuldemenyek.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
        }


        erec_KuldKuldemenyek.KulsoAzonosito = TextBox_KulsoAzonosito.Text;
        erec_KuldKuldemenyek.Updated.KulsoAzonosito = pageView.GetUpdatedByView(TextBox_KulsoAzonosito);
        return erec_KuldKuldemenyek;
    }

    private void GetFelbontoPanelDatas(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (BontoPanel.Visible && !cbBontoasNelkul.Checked)
        {
            if (!_TemplateMode)
            {
                DateTime beerkezes;

                if (!DateTime.TryParse(BeerkezesiIdoCalendarControl1.Text, out beerkezes))
                {
                    throw new FormatException("A beérkezés idõpontjának megadott érték nem megfelelõ formátumú!");
                }

                DateTime felbontas;

                if (!DateTime.TryParse(FelbontasiIdoCalendarControl1.Text, out felbontas))
                {
                    throw new FormatException("A bontás idõpontjának megadott érték nem megfelelõ formátumú!!");
                }
                if (beerkezes > DateTime.Now)
                {
                    throw new FormatException("A beérkezés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
                }
                if (felbontas > DateTime.Now)
                {
                    throw new FormatException("A bontás idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
                }
                if (felbontas < beerkezes)
                {
                    throw new FormatException("A bontás idõpontja nem lehet a beérkezés idõpontjánál korábbi!");
                }

                erec_KuldKuldemenyek.FelbontasDatuma = FelbontasiIdoCalendarControl1.Text;
                erec_KuldKuldemenyek.Updated.FelbontasDatuma = pageView.GetUpdatedByView(FelbontasiIdoCalendarControl1);
            }
            erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto = FelbontoFelhasznaloTextBox1.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Bonto = pageView.GetUpdatedByView(FelbontoFelhasznaloTextBox1);

            erec_KuldKuldemenyek.BontasiMegjegyzes = bontasMegjegyzesTextBox.Text;
            erec_KuldKuldemenyek.Updated.BontasiMegjegyzes = pageView.GetUpdatedByView(bontasMegjegyzesTextBox); ;
        }
    }

    // form --> business object
    private SzamlaErkeztetesParameterek GetBusinessObjectFromComponents_Szamla()
    {
        SzamlaErkeztetesParameterek szamlaErkeztetesParameterek = new SzamlaErkeztetesParameterek();

        if (Startup == Constants.Startup.Szamla)
        {
            //szamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray = ObjektumTargyszavaiPanel1.GetEREC_ObjektumTargyszavaiList(false, true);
            szamlaErkeztetesParameterek.InsertSzemelyOrVallakozasToPartnerIfMissing = true;
            szamlaErkeztetesParameterek.BindPartnerCim = true;
            szamlaErkeztetesParameterek.SzervezetiEgyseg = EredetiCimzettCsoportTextBox1.Text;
            szamlaErkeztetesParameterek.Csoport_Id_SzervezetiEgyseg = EredetiCimzettCsoportTextBox1.Id_HiddenField;

            //// a dinamikus elemek késõbb kerülnek összekapcsolásra,
            //// ezért itt az AdoszamTextBox1 még nincs összekötve a DinamicValueControllal
            ////szamlaErkeztetesParameterek.Partner_Id_Szallito = AdoszamTextBox1.Partner_Id;

            //Component_EditablePartnerTextBox partnerTextBox = FindPartnerTextBox_Szallito();
            //if (partnerTextBox != null)
            //{
            //    szamlaErkeztetesParameterek.Partner_Id_Szallito = partnerTextBox.Id_HiddenField;
            //    if (AdoszamTextBox1.isAdoszamChanged)
            //    {
            //        szamlaErkeztetesParameterek.Adoszam = AdoszamTextBox1.Text;
            //    }
            //}

            if (SzamlaAdatokPanel.IsAdoszamChanged)
            {
                szamlaErkeztetesParameterek.Adoszam = SzamlaAdatokPanel.Adoszam;
                szamlaErkeztetesParameterek.IsKulfoldiAdoszam = SzamlaAdatokPanel.IsKulfoldiAdoszam;
            }
        }

        return szamlaErkeztetesParameterek;
    }

    private void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    // TODO: felkell sorolni...
        //    compSelector.Add_ComponentOnClick(Nev);
        //    compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

        //    FormFooter1.SaveEnabled = false;
        //}
    }

    private void ClearForm()
    {
        if (Command == CommandName.New)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, EErrorPanel1);

            //EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
            //search.IktatoErkezteto.Value = "E";
            //search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

            //Result result = service.GetAll(execParam, search);
            //if (String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    ErkeztetoKonyv_DataSetDropDownList.FillDropDownList(result, "Id", "Nev", false, EErrorPanel1);
            //}
            //else
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            //    ErrorUpdatePanel1.Update();
            //    return;
            //}

            Ikt_n_igeny_Igen_RadioButton.Checked = true;
            serult_kuld_Nem_RadioButton.Checked = true;
            teves_cim_Nem_RadioButton.Checked = true;
            teves_erk_Nem_RadioButton.Checked = true;
        }

        cbKuldoNeveMegorzes.Checked = false;
        cbCimzettMegorzes.Checked = false;
        cbBoritoTipusMegorzes.Checked = false;

        ErkeztetoszamTolReadOnlyTextBox1.Text = "";
        ErkeztetoszamIgReadOnlyTextBox2.Text = "";
        AllapotKodtarakDropDownList1.FillDropDownList(KodCsoportAllapot, EErrorPanel1);
        //        KodtarakDropDownListIktatasiKotelezettseg.FillDropDownList(KodCsoportIktatasikotelezettseg, EErrorPanel1);
        //KodtarakDropDownListIktatasiKotelezettseg.FillAndSetSelectedValue(KodCsoportIktatasikotelezettseg, KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando, EErrorPanel1);

        // Alapesetben csak az Iktatandó/Nem iktatandó értéket engedjük meg:
        List<string> filterList_iktatasiKotelezettseg = new List<string>();
        filterList_iktatasiKotelezettseg.Add(KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando);
        filterList_iktatasiKotelezettseg.Add(KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando);

        KodtarakDropDownListIktatasiKotelezettseg.FillAndSetSelectedValue(KodCsoportIktatasikotelezettseg, KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando
            , filterList_iktatasiKotelezettseg, false, EErrorPanel1);

        //bernat.laszlo added
        Kezbesites_modja_DropDownList.FillAndSetSelectedValue(KodCsoport_KULD_KEZB_MODJA, KodTarak.KULD_KEZB_MODJA.posta, EErrorPanel1);
        CimzesTipusa_DropDownList.FillAndSetSelectedValue(KodCsoportKULD_CIMZES_TIPUS, KodTarak.KULD_CIMZES_TIPUS.nevre_szolo, EErrorPanel1);

        ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
        Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;

        ErkeztetesiIdoCalendarControl1.SetToday();
        //bernat.laszlo eddig
        #region ORG függõ megjelenítés
        // CR 3054
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(KodCsoportBoritoTipus, KodTarak.BoritoTipus.Irat, EErrorPanel1);
        }
        else
        {
            KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(KodCsoportBoritoTipus, KodTarak.BoritoTipus.Boritek, EErrorPanel1);
        }
        #endregion

        FelbontoFelhasznaloTextBox1.SetFelhasznaloTextBoxById(EErrorPanel1);

        if (FunctionRights.GetFunkcioJog(Page, "KuldemenyFizikaiBontas"))
        {
            FelbontasiIdoCalendarControl1.SetToday();
            FelbontoFelhasznaloTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
            FelbontoFelhasznaloTextBox1.SetFelhasznaloTextBoxById(EErrorPanel1);
        }

        ktDropDownListKuldemenyMinosites.FillDropDownList(kcs_IRATMINOSITES, true, EErrorPanel1);
        //Kezbesites_modja_DropDownList.FillAndSetSelectedValue(KodCsoport_KULD_KEZB_MODJA, 
        //KodTarak.KULD_KEZB_MODJA.posta, EErrorPanel1);
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            // CR3201 Beérkezés módja alapértelmezetten 01 legyen
            KuldesModja_DropDownList.FillAndSetSelectedValue(KodCsoportKuldemenyKuldesModja, KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima, EErrorPanel1);
        }
        else
        {
            KuldesModja_DropDownList.FillDropDownList(KodCsoportKuldemenyKuldesModja, EErrorPanel1);
        }

        BeerkezesiIdoCalendarControl1.SetToday();
        //CsoportTextBox1.Text = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        CsoportTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        CsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        //Tovabbito_DropDownList.FillDropDownList(KodCsoportTovabbitoSzervezet, EErrorPanel1);

        // TODO: nincsen felbontasi ido!

        BelyegzoDatumaCalendarControl1.Text = "";
        HivatkozasiSzam_TextBox.Text = "";
        AdathordozoTipusa_DropDownList.FillDropDownList(KodCsoportUgyintezesAlapja, EErrorPanel1);
        // BLG_361
        //UgyintezesAlapja_DropDownList.FillDropDownList(KodCsoportUgyintezesAlapja, EErrorPanel1);
        UgyintezesAlapja_DropDownList.FillDropDownList(KodCsoportELSODLEGES_ADATHORDOZO, EErrorPanel1);
        //ElsodlegesAdathordozo_KodtarakDropDownList.FillDropDownList(KodCsoportELSODLEGES_ADATHORDOZO, EErrorPanel1);

        EredetiCimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
        Surgosseg_DropDownList.FillDropDownList(KodCsoportSurgosseg, EErrorPanel1);
        Surgosseg_DropDownList.SelectedValue = KodTarak.SURGOSSEG.Normal;

        VonalkodTextBoxVonalkod.Text = "";

        if (!String.IsNullOrEmpty(EmailBoritekokId))
        {
            // Ha email a kuldemeny elozmenye!

            Contentum.eAdmin.Service.KRT_FelhasznalokService fhservice = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //execParam.Record_Id = ParentId;

            KRT_Felhasznalok krt_Felhasznalok = new KRT_Felhasznalok();

            Result fhresult = fhservice.GetByEmail(execParam, eMailBoritekok.Felado);

            if (!fhresult.IsError)
            {
                krt_Felhasznalok = (KRT_Felhasznalok)fhresult.Record;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, fhresult);
                ErrorUpdatePanel1.Update();
            }

            PartnerTextBox1.TextBox.ToolTip = krt_Felhasznalok.Nev;//eMailBoritekok.Felado;
            PartnerTextBox1.TextBox.Text = krt_Felhasznalok.Nev;//eMailBoritekok.Felado;
            PartnerTextBox1.Id_HiddenField = krt_Felhasznalok.Id;
            //PartnerTextBox1.SetPartnerTextBoxById(FormHeader.ErrorPanel);

            CimekTextBox1.TextBox.ToolTip = eMailBoritekok.Felado;
            CimekTextBox1.TextBox.Text = eMailBoritekok.Felado;
            // cimzett
            EredetiCimzettCsoportTextBox1.TextBox.ToolTip = eMailBoritekok.Cimzett;

            TargyTextBox.Text = eMailBoritekok.Targy;
            BelyegzoDatumaCalendarControl1.Text = eMailBoritekok.FeladasDatuma;
            FelbontasiIdoCalendarControl1.Text = eMailBoritekok.ErkezesDatuma;
            AdathordozoTipusa_DropDownList.SelectedValue = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            // BLG_361
            //  Nincs default
            //UgyintezesAlapja_DropDownList.SelectedValue = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            UgyintezesAlapja_DropDownList.FillDropDownList(KodCsoportELSODLEGES_ADATHORDOZO, EErrorPanel1); ;
            KuldesModja_DropDownList.SelectedValue = KodTarak.KULDEMENY_KULDES_MODJA.E_mail;
        }

        if (Command == CommandName.New)
        {
            //KezelesiFeljegyzes_DropDownList.FillDropDownList(KodCsoportKEZELESI_FELJEGYZES_KULDEMENY, EErrorPanel1);
            trKezelesiFeljegyzes.Visible = true;
            FeljegyzesPanel.ValidateIfFeladatIsNotEmpty = true;
            trMellekletekPanel.Visible = true;
        }
        else
        {
            FeljegyzesPanel.ValidateIfFeladatIsNotEmpty = false;
        }

        if (Command == CommandName.New)
        {
            LoadSessionDatas();
        }
        //KezelesiFeljegyzes_DropDownList.DropDownList.SelectedValue = KodTarak.;
    }

    protected void LoadSessionDatas()
    {
        //Beküldõ
        if (Command == CommandName.New && UI.GetSession(Page, "PartnerChecked") == "true")
        {
            PartnerTextBox1.Id_HiddenField = UI.GetSession(Page, "PartnerId");
            PartnerTextBox1.Text = UI.GetSession(Page, "PartnerText");
            PartnerTextBox1.KapcsoltPartnerDropDownValue = UI.GetSession(Page, "PartnerIdBekuldoKapcsolt");
            cbKuldoNeveMegorzes.Checked = true;
        }
        else if (!_TemplateMode)
        {
            PartnerTextBox1.Id_HiddenField = "";
            PartnerTextBox1.Text = "";
            PartnerTextBox1.KapcsoltPartnerDropDownValue = "";
        }
        //Beküldõ címe
        if (Command == CommandName.New && UI.GetSession(Page, "CimChecked") == "true")
        {
            CimekTextBox1.Id_HiddenField = UI.GetSession(Page, "CimId");
            CimekTextBox1.Text = UI.GetSession(Page, "CimText");
            CheckBoxKuldoCime.Checked = true;
        }
        else if (!_TemplateMode)
        {
            CimekTextBox1.Id_HiddenField = "";
            CimekTextBox1.Text = "";
        }
        //Borító típus
        if (Command == CommandName.New)
        {
            if (UI.GetSession(Page, "BoritoTipusChecked") == "true")
            {
                KodtarakDropDownListBoritoTipus.SetSelectedValue(UI.GetSession(Page, "BoritoTipusValue"));
                cbBoritoTipusMegorzes.Checked = true;
            }
        }
        //Beérkezés módja
        if (Command == CommandName.New)
        {
            if (UI.GetSession(Page, "BeerkezesModjaChecked") == "true")
            {
                cbBeerkezesModjaMegorzes.Checked = true;
                KuldesModja_DropDownList.SetSelectedValue(UI.GetSession(Page, "BeerkezesModja"));
            }
        }
        //Ragszám
        if (UI.IsStoredValueInSession(Page, RagszamRequiredTextBox.ID))
        {
            RagszamRequiredTextBox.Text = UI.GetSessionStoredValue(Page, RagszamRequiredTextBox.ID, cbRagszamMegorzes);
        }
        else if (!_TemplateMode)
        {
            RagszamRequiredTextBox.Text = String.Empty;
        }
        //Címzett
        if (Command == CommandName.New && UI.GetSession(Page, "CsoportChecked") == "true")
        {
            EredetiCimzettCsoportTextBox1.Id_HiddenField = UI.GetSession(Page, "CsoportId");
            EredetiCimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
            cbCimzettMegorzes.Checked = true;
        }
        else if (!_TemplateMode)
        {
            EredetiCimzettCsoportTextBox1.Id_HiddenField = "";
        }
        // Kezelõ:
        if (Command == CommandName.New)
        {
            if (UI.GetSession(Page, "FelelosChecked") == "true")
            {
                FelelosCsoportTextBox1.Id_HiddenField = UI.GetSession(Page, "FelelosId");
                FelelosCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

                CheckBox_Kezelo.Checked = true;
            }
            else if (!_TemplateMode)
            {
                // felelõs defaultból beállítva a saját csoportra:
                FelelosCsoportTextBox1.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
                    FelhasznaloProfil.FelhasznaloId(Page));
                FelelosCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
            }
        }
        // Tárgy:
        if (Command == CommandName.New)
        {
            if (UI.GetSession(Page, "TargyChecked") == "true")
            {
                TargyTextBox.Text = UI.GetSession(Page, "TargyText");

                CheckBox_Targy.Checked = true;
            }
            else if (!_TemplateMode)
            {
                TargyTextBox.Text = "";
            }
        }

        if (Command == CommandName.New)
        {
            //Beerkezes Ido
            if (UI.IsStoredValueInSession(Page, BeerkezesiIdoCalendarControl1.ID))
            {
                BeerkezesiIdoCalendarControl1.Text = UI.GetSessionStoredValue(Page, BeerkezesiIdoCalendarControl1.ID, cbBeerkezesIdopontjaMegorzes);
            }
            //Bontasi Megjegyzes
            if (UI.IsStoredValueInSession(Page, bontasMegjegyzesTextBox.ID))
            {
                bontasMegjegyzesTextBox.Text = UI.GetSessionStoredValue(Page, bontasMegjegyzesTextBox.ID, cbBontasiMegjegyzesMegorzes);
            }
            else if (!_TemplateMode)
            {
                bontasMegjegyzesTextBox.Text = String.Empty;
            }

            //Iktatási kötelezettség
            if (UI.IsStoredValueInSession(Page, KodtarakDropDownListIktatasiKotelezettseg.ID))
            {
                KodtarakDropDownListIktatasiKotelezettseg.SetSelectedValue(UI.GetSessionStoredValue(Page, KodtarakDropDownListIktatasiKotelezettseg.ID, cbIktatasiKotelezettsegMegorzes));
            }

            //Küldõ iktatószáma
            if (UI.IsStoredValueInSession(Page, HivatkozasiSzam_TextBox.ID))
            {
                HivatkozasiSzam_TextBox.Text = UI.GetSessionStoredValue(Page, HivatkozasiSzam_TextBox.ID, cbKuldoIktatoszamaMegorzes);
            }
            else if (!_TemplateMode)
            {
                HivatkozasiSzam_TextBox.Text = String.Empty;
            }

            //Tartalom
            if (UI.IsStoredValueInSession(Page, Tartalom_TextBox.ID))
            {
                Tartalom_TextBox.Text = UI.GetSessionStoredValue(Page, Tartalom_TextBox.ID, cbTartalomMegorzes);
            }
            else if (!_TemplateMode)
            {
                Tartalom_TextBox.Text = String.Empty;
            }
        }
    }

    // ha true, akkor readonly, egyebkent normal
    private void SetViewControls(Boolean value)
    {
        //ErkeztetoszamTolReadOnlyTextBox1.ReadOnly = value;
        //ErkeztetoszamIgReadOnlyTextBox2.ReadOnly = value;
        AllapotKodtarakDropDownList1.ReadOnly = value;
        KodtarakDropDownListIktatasiKotelezettseg.ReadOnly = value;
        KodtarakDropDownList_IktatasStatusza.Visible = value;
        Label_IktatasStatusza.Visible = value;
        KodtarakDropDownListBoritoTipus.ReadOnly = value;
        bontasMegjegyzesTextBox.ReadOnly = value;

        PartnerTextBox1.ReadOnly = value;

        CimekTextBox1.ReadOnly = value;

        KuldesModja_DropDownList.ReadOnly = value;

        //FullCimField1.Visible = !value;
        tr_FullCimField.Visible = !value;

        tr_MellekletekCsatolmanyokSzama.Visible = value;

        if (value)
        {
            //mellékletek száma csak papír esetén látszik
            LabelMellekletekSzama.Visible = MellekletekSzama_RequiredNumberBox1.Visible =
                (AdathordozoTipusa_DropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir);
        }

        BeerkezesiIdoCalendarControl1.ReadOnly = value;
        //Tovabbito_DropDownList.ReadOnly = value;
        //FelbontoFelhasznaloTextBox1.ReadOnly = value;        

        // TODO: nincsen felbontasi ido!
        //FelbontasiIdoCalendarControl1.ReadOnly = value;

        BelyegzoDatumaCalendarControl1.ReadOnly = value;
        HivatkozasiSzam_TextBox.ReadOnly = value;
        AdathordozoTipusa_DropDownList.ReadOnly = value;
        UgyintezesAlapja_DropDownList.ReadOnly = value;
        //ElsodlegesAdathordozo_KodtarakDropDownList.ReadOnly = value;
        RagszamRequiredTextBox.ReadOnly = value;
        Tartalom_TextBox.ReadOnly = value;
        EredetiCimzettCsoportTextBox1.ReadOnly = value;
        Surgosseg_DropDownList.ReadOnly = value;
        FelelosCsoportTextBox1.ReadOnly = value;
        TargyTextBox.ReadOnly = value;

        VonalkodTextBoxVonalkod.ReadOnly = value;

        ktDropDownListKuldemenyMinosites.ReadOnly = value;

        //ObjektumTargyszavaiPanel1.ReadOnly = value;

        TabFooter1.ImageButton_Cancel.Visible = !value;

        //AdoszamTextBox1.ReadOnly = value;

        // validátorok letiltása
        if (value)
        {
            PartnerTextBox1.Validate = false;
            CimekTextBox1.Validate = false;
        }

        SzamlaAdatokPanel.ReadOnly = value;

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Enabled = !value;
        }
        TextBox_KulsoAzonosito.ReadOnly = value;
    }

    private void SetModifyControls()
    {
        VonalkodTextBoxVonalkod.ReadOnly = string.IsNullOrEmpty(VonalkodTextBoxVonalkod.Text) ? false : true;
        //KodtarakDropDownListBoritoTipus.ReadOnly = true;
        KodtarakDropDownList_IktatasStatusza.Visible = true;
        Label_IktatasStatusza.Visible = true;
        tr_MellekletekCsatolmanyokSzama.Visible = true;
        //mellékletek száma csak papír esetén látszik
        LabelMellekletekSzama.Visible = MellekletekSzama_RequiredNumberBox1.Visible =
                (AdathordozoTipusa_DropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir);
    }

    private void FunkcioGombsorFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case "Szignalas":
                break;
            case "Szignalas_vissza":
                break;
            case "Sztornozas":
                Kuldemenyek.Sztorno(ParentId, SztornoPopup.SztornoIndoka, Page, EErrorPanel1);
                UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?"
                    + QueryStringVars.Command + "=" + CommandName.View
                    + "&" + QueryStringVars.Id + "=" + ParentId);
                break;

            case "Iktatas":
                // Javascriptbol hivha onclientclick
                break;

            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            case "Munkapeldany_Iktatas":
                // Javascriptbol hivha onclientclick
                break;
            case "IktatasMegtagadas":
                // Javascript onclientclick
                break;

            case "Kuldemeny_lezarasa":
                Kuldemenyek.Lezaras(ParentId, Page, EErrorPanel1);
                UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?"
                    + QueryStringVars.Command + "=" + CommandName.View
                    + "&" + QueryStringVars.Id + "=" + ParentId);
                break;

            case "Borito_nyomtatasa_A4":
                break;
            case "Borito_nyomtatasa_A3":
                break;
            case "Kiserolap":
                break;

            case "Zarolas":
                Kuldemenyek.Zarolas(ParentId, Page, EErrorPanel1, KuldemenyKuldemenyUpdatePanel);
                break;
            case "Zarolas_feloldasa":
                Kuldemenyek.Zarolas_feloldasa(ParentId, Page, EErrorPanel1, KuldemenyKuldemenyUpdatePanel);
                break;
        }
    }

    //private Component_EditablePartnerTextBox FindPartnerTextBox_Szallito()
    //{
    //    // Szállító megnevezése partner textbox, számla üzemmódban
    //    Contentum.eUIControls.DynamicValueControl partnerTextBoxDVC = ObjektumTargyszavaiPanel1.FindControlByTargyszo(SzallitoPartnerTextBox_Targyszo);
    //    if (partnerTextBoxDVC != null && partnerTextBoxDVC.Control != null)
    //    {
    //        return partnerTextBoxDVC.Control as Component_EditablePartnerTextBox;
    //    }

    //    return null;
    //}

    private bool CheckSzamlaAdatok()
    {
        //bool isPartnerIdEmpty = String.IsNullOrEmpty(PartnerTextBox1.Id_HiddenField);

        //bool isPartnerIdEmpty = true;
        //// Szállító megnevezése partner textbox
        //Component_EditablePartnerTextBox partnerTextBox = FindPartnerTextBox_Szallito();
        //if (partnerTextBox != null)
        //{
        //    isPartnerIdEmpty = String.IsNullOrEmpty(partnerTextBox.Id_HiddenField);
        //}

        bool isPartnerIdEmpty = String.IsNullOrEmpty(SzamlaAdatokPanel.Partner_Id_Szallito);

        //bool isCimIdEmpty = String.IsNullOrEmpty(CimekTextBox1.Id_HiddenField);
        bool isCimIdEmpty = String.IsNullOrEmpty(SzamlaAdatokPanel.Cim_Id_Szallito);
        //bool isAdoszamEmpty = String.IsNullOrEmpty(AdoszamTextBox1.Text);
        bool isAdoszamEmpty = SzamlaAdatokPanel.IsAdoszamVisible ? String.IsNullOrEmpty(SzamlaAdatokPanel.Adoszam) : false;
        // BUG_4731
        bool isPartnerTextEmpty = String.IsNullOrEmpty(SzamlaAdatokPanel.Szallito_PartnerTextBox.Text);
        bool isCimTextEmpty = String.IsNullOrEmpty(SzamlaAdatokPanel.Szallito_CimekTextBox.Text);

        bool isBekuldoPartnerCimNemTorzsbolDisabled = (Rendszerparameterek.GetBoolean(Page
          , Rendszerparameterek.BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED) == false);
        bool isDataComplete;
        if (isBekuldoPartnerCimNemTorzsbolDisabled)
        {
            isDataComplete = !isPartnerIdEmpty && !isCimIdEmpty && !isAdoszamEmpty;
        }
        else
        {
            isDataComplete = !isPartnerTextEmpty && !isCimTextEmpty;  // TODO: Adószám rögzítés
        }
        if (!isDataComplete)
        {
            //string message = String.Format(@"A következõ kötelezõ adatok hiányoznak:{0}{1}{2}!"
            //    , isPartnerIdEmpty ? "\npartner (partnertörzsbõl)" : ""
            //    , isCimIdEmpty ? "\ncím (címtörzsbõl)" : ""
            //    , isAdoszamEmpty ? "\nadószám" : "");

            string[] missingParams = new string[3];
            int i = 0;

            // BUG_4731
            //if (isPartnerIdEmpty)
            if ((isPartnerIdEmpty && isBekuldoPartnerCimNemTorzsbolDisabled) || isPartnerTextEmpty)

            {
                missingParams[i] = Resources.Error.UIMissingParameters_Partner_Id;
                i++;
            }

            //if (isCimIdEmpty)
            if ((isCimIdEmpty && isBekuldoPartnerCimNemTorzsbolDisabled) || isCimTextEmpty)

            {
                missingParams[i] = Resources.Error.UIMissingParameters_Cim_Id;
                i++;
            }

            if (isAdoszamEmpty)
            {
                missingParams[i] = Resources.Error.UIMissingParameters_Adoszam;
                i++;
            }

            string message = String.Concat(Resources.Error.UIMissingParameters, String.Join(", ", missingParams, 0, i));

            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, message);
            ErrorUpdatePanel1.Update();
            return false;
        }

        return isDataComplete;
    }

    /// <summary>
    /// A megõrzõs checkboxok alapján adat Session-be mentése vagy kidobása onnan
    /// </summary>
    public void SetSessionData()
    {
        if (cbKuldoNeveMegorzes.Checked)
        {
            Session["PartnerId"] = PartnerTextBox1.Id_HiddenField;
            Session["PartnerText"] = PartnerTextBox1.Text;
            Session["PartnerIdBekuldoKapcsolt"] = PartnerTextBox1.KapcsoltPartnerDropDownValue;
            Session["PartnerChecked"] = "true";
        }
        else
        {
            Session.Remove("PartnerId");
            Session.Remove("PartnerText");
            Session.Remove("PartnerIdBekuldoKapcsolt");
            Session.Remove("PartnerChecked");
        }

        if (cbCimzettMegorzes.Checked)
        {
            Session["CsoportId"] = EredetiCimzettCsoportTextBox1.Id_HiddenField;
            Session["CsoportChecked"] = "true";
        }
        else
        {
            Session.Remove("CsoportId");
            Session.Remove("CsoportChecked");
        }

        if (CheckBoxKuldoCime.Checked)
        {
            Session["CimId"] = CimekTextBox1.Id_HiddenField;
            Session["CimText"] = CimekTextBox1.Text;
            Session["CimChecked"] = "true";
        }
        else
        {
            Session.Remove("CimId");
            Session.Remove("CimText");
            Session.Remove("CimChecked");
        }

        if (CheckBox_Kezelo.Checked)
        {
            Session["FelelosId"] = FelelosCsoportTextBox1.Id_HiddenField;
            Session["FelelosChecked"] = "true";
        }
        else
        {
            Session.Remove("FelelosId");
            Session.Remove("FelelosChecked");
        }

        if (CheckBox_Targy.Checked)
        {
            Session["TargyText"] = TargyTextBox.Text;
            Session["TargyChecked"] = "true";
        }
        else
        {
            Session.Remove("TargyText");
            Session.Remove("TargyChecked");
        }

        if (cbBoritoTipusMegorzes.Checked)
        {
            Session["BoritoTipusValue"] = KodtarakDropDownListBoritoTipus.SelectedValue;
            Session["BoritoTipusChecked"] = "true";
        }
        else
        {
            Session.Remove("BoritoTipusValue");
            Session.Remove("BoritoTipusChecked");
        }

        if (cbBeerkezesModjaMegorzes.Checked)
        {
            Session["BeerkezesModja"] = KuldesModja_DropDownList.SelectedValue;
            Session["BeerkezesModjaChecked"] = "true";
        }
        else
        {
            Session.Remove("BeerkezesModja");
            Session.Remove("BeerkezesModjaChecked");
        }

        UI.StoreValueInSession(Page, RagszamRequiredTextBox.ID, RagszamRequiredTextBox.Text, cbRagszamMegorzes);
        UI.StoreValueInSession(Page, BeerkezesiIdoCalendarControl1.ID, BeerkezesiIdoCalendarControl1.Text, cbBeerkezesIdopontjaMegorzes);
        UI.StoreValueInSession(Page, bontasMegjegyzesTextBox.ID, bontasMegjegyzesTextBox.Text, cbBontasiMegjegyzesMegorzes);
        UI.StoreValueInSession(Page, KodtarakDropDownListIktatasiKotelezettseg.ID, KodtarakDropDownListIktatasiKotelezettseg.SelectedValue, cbIktatasiKotelezettsegMegorzes);
        UI.StoreValueInSession(Page, HivatkozasiSzam_TextBox.ID, HivatkozasiSzam_TextBox.Text, cbKuldoIktatoszamaMegorzes);
        UI.StoreValueInSession(Page, Tartalom_TextBox.ID, Tartalom_TextBox.Text, cbTartalomMegorzes);

        if (Startup == Constants.Startup.Szamla)
        {
            SzamlaAdatokPanel.SetSessionData();
        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //        String SubCommand = (String)e.CommandArgument;
        bool haserror = false;
        
        if (e.CommandName == CommandName.Cancel)
        {
            ReLoadTab();
            return;
        }

        #region Számla kötelezõ adatok meglétének ellenõrzése (partner_id, cím_id, adószám)
        if (Startup == Constants.Startup.Szamla)
        {
            // ellenõrzés és partner/cím/adószám mentés
            if (!CheckSzamlaAdatok())
            {
                return;
            }
        }

        #endregion Számla kötelezõ adatok meglétének ellenõrzése (partner_id, cím_id, adószám)

        #region Form adatok sessionba mentese
        if (Command == CommandName.New)
        {
            SetSessionData();
        }

        #endregion

        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            Startup = Request.QueryString.Get(QueryStringVars.Startup);

            string funkcioJog = ParentForm + Command;

            if (Startup == Constants.Startup.Szamla)
            {
                funkcioJog = "Szamla" + Command;
            }

            //if (FunctionRights.GetFunkcioJog(Page, ParentForm + Command))
            if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
            {
                String NewId = "";
                ErkeztetesIktatasResult erkeztetesResult = null;

                try
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                EREC_KuldKuldemenyek erec_KuldKuldemenyek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = null;

                                ErkeztetesParameterek ep = new ErkeztetesParameterek();
                                ep.Mellekletek = mellekletekPanel.GetMellekletek();

                                if (!String.IsNullOrEmpty(EmailBoritekokId))
                                {
                                    // Email erkeztetes
                                    LoadEmailBoritekKuldKuldemenyId(EmailBoritekokId);
                                    if (!String.IsNullOrEmpty(eMailBoritekok.KuldKuldemeny_Id))
                                    {
                                        // TODO:
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "TODO", "Az email már érkeztetve van! Kérem, válaszzon másik email-t!");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    if (eMailBoritekok_Ver_HiddenField.Value != eMailBoritekok.Base.Ver)
                                    {
                                        // TODO:
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "TODO", "A kiválasztott email-t módosították! Kérem, válassza ki újra ez email-t!");
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    result = service.ErkeztetesEmailbol(execParam, erec_KuldKuldemenyek, eMailBoritekok, null, ep);
                                }
                                else
                                {
                                    //Kezelési feljegyzés
                                    EREC_HataridosFeladatok erec_HataridosFeladat = FeljegyzesPanel.GetBusinessObject();

                                    if (Startup == Constants.Startup.Szamla)
                                    {
                                        EREC_Szamlak erec_Szamlak = SzamlaAdatokPanel.GetBusinessObjectFromComponents();

                                        SzamlaErkeztetesParameterek szamlaErkeztetesParameterek = GetBusinessObjectFromComponents_Szamla();
                                        szamlaErkeztetesParameterek.Mellekletek = mellekletekPanel.GetMellekletek();
                                        // Normal erkeztetes + partner adoszam es cim kotes + metaadatok
                                        result = service.SzamlaErkeztetes(execParam, erec_KuldKuldemenyek, erec_Szamlak, erec_HataridosFeladat, szamlaErkeztetesParameterek);
                                    }
                                    else
                                    {
                                        // Normal erkeztetes
                                        result = service.Erkeztetes(execParam, erec_KuldKuldemenyek, erec_HataridosFeladat, ep);
                                    }
                                }

                                NewId = result.Uid;

                                if (result.Record != null && result.Record is ErkeztetesIktatasResult)
                                {
                                    erkeztetesResult = (ErkeztetesIktatasResult)result.Record;
                                }

                                if (result.IsError)
                                {
                                    if (result.ErrorCode.Substring(0, 3) == "PIR") //1000132
                                    {
                                        // BUG_2546
                                        result.ErrorMessage = result.ErrorMessage.Replace("'", "\"");

                                        string javaS = @"alert('" + result.ErrorMessage + "');";
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PirError", javaS, true);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel1.Update();
                                        haserror = true;
                                    }
                                }
                                else
                                {
                                    mellekletekPanel.Clear();
                                    if (!UpdateTargyszavak(NewId))
                                    {
                                        haserror = true;
                                    }
                                }

                                //Kezelési feljegyzés
                                //EREC_HataridosFeladatok erec_HataridosFeladat = FeljegyzesPanel.GetBusinessObject();
                                //if (erec_HataridosFeladat != null)
                                //{
                                //    erec_HataridosFeladat.Obj_Id = NewId;
                                //    erec_HataridosFeladat.Updated.Obj_Id = true;

                                //    erec_HataridosFeladat.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                                //    erec_HataridosFeladat.Updated.Obj_type = true;

                                //    EREC_HataridosFeladatokService svcHataridosFeladat= eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                                //    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());

                                //    Result res = svcHataridosFeladat.Insert(xpm, erec_HataridosFeladat);
                                //}

                                break;
                            }

                        case CommandName.Modify:
                            {
                                if (String.IsNullOrEmpty(ParentId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                                    ErrorUpdatePanel1.Update();
                                    haserror = true;
                                }
                                else
                                {
                                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                    EREC_KuldKuldemenyek erec_KuldKuldemenyek = GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = ParentId;

                                    Result result = null;

                                    if (Startup == Constants.Startup.Szamla)
                                    {
                                        EREC_Szamlak erec_Szamlak = SzamlaAdatokPanel.GetBusinessObjectFromComponents();

                                        SzamlaErkeztetesParameterek szamlaErkeztetesParameterek = GetBusinessObjectFromComponents_Szamla();

                                        // Normal erkeztetes + partner adoszam es cim kotes + metaadatok
                                        result = service.SzamlaUpdate(execParam, erec_KuldKuldemenyek, erec_Szamlak, szamlaErkeztetesParameterek);

                                        SzamlaAdatokPanel.SetSessionData();
                                    }
                                    else
                                    {
                                        result = service.Update(execParam, erec_KuldKuldemenyek);
                                    }

                                    if (result.IsError)
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                        ErrorUpdatePanel1.Update();

                                        //LZS - BUG_2923 - Használatban lévő vonalkód esetén töröljük megadottat, és így aktív marad a beviteli mező.
                                        if (result.ErrorCode == "52482")
                                        {
                                            VonalkodTextBoxVonalkod.Text = "";
                                        }

                                        // CR3359 Számla átadásnál (PIR interface) új csoportosító mezõ (ettõl függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                                        // CR kapcsán hibakezelés módosítása (a számla el van mentve, de az átküldés hibáját kezelni kell
                                        if (result.ErrorCode.Substring(0, 3) != "PIR") //1000133
                                            haserror = true;
                                        return;
                                    }
                                    else
                                    {
                                        if (!UpdateTargyszavak(ParentId))
                                        {
                                            haserror = true;
                                        }
                                    }
                                }

                                break;
                            }
                    }
                }
                catch (FormatException fe)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", fe.Message);
                    ErrorUpdatePanel1.Update();
                    haserror = true;
                }

                // ha volt hiba, akkor nem lehet bezartni a formot!
                if (haserror)
                {
                    if (!VonalkodTextBoxVonalkod.ReadOnly)
                    {
                        VonalkodTextBoxVonalkod.Text = String.Empty;
                    }
                    return;
                }

                if (e.CommandName == CommandName.Save)
                {
                    if (Command == CommandName.New)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, NewId);

                        // iktató formra id visszaírása, ha szükséges
                        JavaScripts.SendBackResultToCallingWindow(Page, NewId, "", true);

                        // save gombok letiltása, míg át nem irányítódik
                        //TabFooter1.DisableSaveButtons();

                        //UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?"
                        //    + QueryStringVars.Command + "=" + CommandName.Modify
                        //    + "&" + QueryStringVars.Id + "=" + NewId);

                        this.SetResultPanel(erkeztetesResult);
                    }
                    if (Command == CommandName.Modify)
                    {
                        //JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);

                        //// TODO: beallitani a tabokat, lathatora, mert innentol mar modositas!
                        //UI.popupRedirect(Page, "KuldKuldemenyekForm.aspx?"
                        //    + QueryStringVars.Command + "=" + CommandName.Modify
                        //    + "&" + QueryStringVars.Id + "=" + ParentId);

                        ////// újratöltjük a módosítás oldalt
                        ////UI.popupRedirect(Page, "IraIratokForm.aspx?"
                        ////       + QueryStringVars.Command + "=" + CommandName.Modify
                        ////       + "&" + QueryStringVars.Id + "=" + ParentId);            

                        Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                        ReLoadTab();
                    }           
                }
                else if (e.CommandName == CommandName.SaveAndClose)
                {
                    if (Command == CommandName.New)
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, NewId);

                    JavaScripts.SendBackResultToCallingWindow(Page, NewId, "", true);

                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                }

                if (e.CommandName == CommandName.SaveAndNew)
                {
                    //if (Command == CommandName.New)
                    //    JavaScripts.RegisterSelectedRecordIdToParent(Page, NewId);

                    //// save gombok letiltása, míg át nem irányítódik
                    //TabFooter1.DisableSaveButtons();

                    //// Átirányítás ugyanoda:
                    //String oldURL = Request.Url.OriginalString;
                    //UI.popupRedirect(Page, oldURL);                                                                                        
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }

    private bool UpdateTargyszavak(string id)
    {
        #region standard objektumfüggõ tárgyszavak mentése

        Result result_ot_standard = Save_StandardObjektumTargyszavai_Kuldemenyek(id);

        if (result_ot_standard.IsError)
        {
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_standard);
            ErrorUpdatePanel1.Update();
            return false;
        }

        #endregion standard objektumfüggõ tárgyszavak mentése

        #region típusos objektumfüggõ tárgyszavak mentése

        Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Kuldemenyek(id);

        if (result_ot_tipusos.IsError)
        {
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_tipusos);
            ErrorUpdatePanel1.Update();
            return false;
        }

        #endregion típusos objektumfüggõ tárgyszavak mentése

        return true;
    }


    private void SetResultPanel(ErkeztetesIktatasResult erkeztetesResult)
    {
        //Panel-ek beállítása
        EFormPanel1.Visible = false;
        //ObjektumTargyszavai_EFormPanel.Visible = false;
        SzamlaAdatok_EFormPanel.Visible = false;
        TipusosTargyszavakPanel_Kuldemenyek.Visible = false;
        StandardTargyszavakPanel_Kuldemenyek.Visible = false;
        ResultPanel.Visible = true;

        //TabFooter beállítása
        TabFooter1.ImageButton_Close.Visible = true;

        //This is needed because the original value which was : "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        //Doesnt provide reload with chrome and firefox
        TabFooter1.ImageButton_Close.OnClientClick = "window.returnValue=true;" + 
            "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
       + "window.close();";

        TabFooter1.Link_SaveAndNew.Visible = true;
        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        //string saveAndNewUrl = Request.Url.OriginalString;
        string saveAndNewUrl = "~/KuldKuldemenyekForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New;

        // ha volt betöltött template:
        if (!String.IsNullOrEmpty(FormHeader.CurrentTemplateId))
        {
            saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader.CurrentTemplateId;
        }

        // számlából indítva csak számla érkeztetés
        if (Startup == Constants.Startup.Szamla)
        {
            saveAndNewUrl += "&" + QueryStringVars.Startup + "=" + Constants.Startup.Szamla;
        }
        TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

        if (erkeztetesResult == null)
        {
            Logger.Error("iktatasResult == null");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }
        if (String.IsNullOrEmpty(erkeztetesResult.KuldemenyId))
        {
            Logger.Error("String.IsNullOrEmpty(erkeztetesResult.KuldemenyId)");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }

        string kuldemenyID = erkeztetesResult.KuldemenyId;

        try
        {
            //Étkeztetõszám beállítása
            //ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
            //EREC_KuldKuldemenyekService srvKuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            //EREC_KuldKuldemenyekSearch searchKuldemenyek = new EREC_KuldKuldemenyekSearch();
            //searchKuldemenyek.Id.Value = kuldemenyID;
            //searchKuldemenyek.Id.Operator = Query.Operators.equals;
            //Result res = srvKuldemenyek.GetAllWithExtension(xcParam, searchKuldemenyek);
            //if (!String.IsNullOrEmpty(res.ErrorCode))
            //{
            //    throw new Contentum.eUtility.ResultException(res);
            //}
            //if (res.Ds.Tables[0].Rows.Count == 0)
            //{
            //    throw new Contentum.eUtility.ResultException(52106);
            //}
            //labelKuldemenyErkSzam.Text = res.Ds.Tables[0].Rows[0]["FullErkeztetoSzam"].ToString();
            labelKuldemenyErkSzam.Text = erkeztetesResult.KuldemenyAzonosito ?? Resources.Error.UIAzonositoUndefined;

            //Módosítás,megtekintés gombok beállítása
            string qsStartup = String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup);

            imgKuldemenyMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + kuldemenyID
                                            + qsStartup, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgKuldemenyModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + kuldemenyID
                                            + qsStartup, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            #region Küldemény módosítható-e
            // CR3359 Számla átadásnál (PIR interface) új csoportosító mezõ (ettõl függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            // CR kapcsán, számla státusz vizsgálat a módosíthatóság megállapításához
            // A Küldemény-vizsgálat nem volt jó, pir-állapotot nem nézett
            Kuldemenyek.Statusz kuldstatusz = Kuldemenyek.GetAllapotById(kuldemenyID, Page, EErrorPanel1);
            // Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            ErrorDetails errorDetail = null;
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            imgKuldemenyModosit.Enabled = Kuldemenyek.Modosithato(kuldstatusz, execParam, out errorDetail);
            #endregion Küldemény módosítható-e

            imgCsatolmanyFeltoltes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + kuldemenyID
                                            + qsStartup
                                            + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            if (FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasNew") || FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolasNew"))
            {
                imgCsatolas.Visible = true;
                imgCsatolas.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("CsatolasForm.aspx", QueryStringVars.Command + "=" + CommandName.New +
                                    "&" + QueryStringVars.Id + "=" + kuldemenyID +
                                    "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromKuldemeny, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
            }

            //HA NEM TÜK-ös
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
            {
                //CR 3058
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                    ImageButtonVonalkodNyomtatas.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kuldemeny&" + QueryStringVars.Command + "=Modify" + "')";
                else
                {
                    ImageButtonVonalkodNyomtatas.Visible = false;
                    ImageButtonVonalkodNyomtatas.Enabled = false;
                }
            }
            else//HA TÜK-ös
            {
                bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                ImageButtonVonalkodNyomtatas.Visible = _visible;
                ImageButtonVonalkodNyomtatas.Enabled = _visible;
            }

            //Funkciógombsor beállítása: átvételi elismervény meg hasonlól
            divFunkcioGombsor.Visible = true;
            if (Startup == Constants.Startup.Szamla)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
                FunkcioGombsor.KiserolapVisible = false;
                FunkcioGombsor.AtveteliElismervenyVisible = true;
                FunkcioGombsor.XMLExportVisible = true;
                FunkcioGombsor.Borito_nyomtatasa_A3Enabled = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Enabled = false;
                FunkcioGombsor.KiserolapEnabled = false;
                FunkcioGombsor.AtveteliElismervenyEnabled = true;
                FunkcioGombsor.XMLExportEnabled = true;

                // blg 1868   FunkcioGombsor.AtveteliElismervenyOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Atveteli");
                FunkcioGombsor.AtveteliElismervenyOnClientClick = "javascript:window.open('KuldKuldemenyekPrintFormSSRS.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Atveteli')";
                // TODO: számlaként exportálni (metaadatok)
                FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + kuldemenyID + "&tipus=Kuldemeny')";
            }
            else
            {
                if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
                {
                    FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                    FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
                    FunkcioGombsor.Borito_nyomtatasa_A3Enabled = false;
                    FunkcioGombsor.Borito_nyomtatasa_A4Enabled = false;
                }
                else
                {
                    FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                    FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
                    FunkcioGombsor.Borito_nyomtatasa_A3Enabled = true;
                    FunkcioGombsor.Borito_nyomtatasa_A4Enabled = true;
                    // FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA3");
                    // FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA4");
                    FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = "javascript:window.open('KuldKuldemenyekSSRSBoritoA3.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA3')";
                    FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = "javascript:window.open('KuldKuldemenyekSSRSBorito.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=BoritoA4')";
                }

                FunkcioGombsor.KiserolapVisible = true;
                FunkcioGombsor.AtveteliElismervenyVisible = true;
                FunkcioGombsor.XMLExportVisible = true;

                FunkcioGombsor.KiserolapEnabled = true;
                FunkcioGombsor.AtveteliElismervenyEnabled = true;
                FunkcioGombsor.XMLExportEnabled = true;

                // blg 1868 FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kisero");
                FunkcioGombsor.KiserolapOnClientClick = "javascript:window.open('KuldKuldemenyekPrintFormSSRSKisero.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kisero')";
                //blg 1868   FunkcioGombsor.AtveteliElismervenyOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Atveteli");
                FunkcioGombsor.AtveteliElismervenyOnClientClick = "javascript:window.open('KuldKuldemenyekPrintFormSSRS.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Atveteli')";
                FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + kuldemenyID + "&tipus=Kuldemeny')";
            }
            #region 3100 Az érkeztetõszám visszajelzõ panelre kerüljön ki az átadás gomb, hogy onnan közvetlenül meg lehessen hívni. Mehet mindenhova nem kell ORG specifikusnak lennie.
            FunkcioGombsor.Atadasra_kijelolesVisible = true;
            FunkcioGombsor.Atadasra_kijelolesEnabled = true;
            // Átadásra kijelölés
            if (FunkcioGombsor.Atadasra_kijelolesEnabled)
            {
                var statusz = Kuldemenyek.GetAllapotById(kuldemenyID, Page, EErrorPanel1);
                Kuldemenyek.SetAtadasraKijeloles(kuldemenyID, statusz, FunkcioGombsor.Atadasra_kijeloles_ImageButton, KuldemenyKuldemenyUpdatePanel);

            }

            #endregion
        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }

        FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);

        if (fm != null)
        {
            fm.ObjektumId = kuldemenyID;
        }
    }

    #endregion

    protected void ErrorUpdatePanel1_Load(object sender, EventArgs e)
    {
    }

    private void LoadEmailBoritekKuldKuldemenyId(String _EmailBoritekokId)
    {
        EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
        execparam.Record_Id = _EmailBoritekokId;
        Result _ret = erec_eMailBoritekokService.Get(execparam);
        EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)_ret.Record;

        if (_ret.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, _ret);
            ErrorUpdatePanel1.Update();
            eMailBoritekok = null;
            return;
        }
        eMailBoritekok = erec_eMailBoritekok;
    }

    protected void cbBontoasNelkul_CheckedChanged(object sender, EventArgs e)
    {
        if (cbBontoasNelkul.Checked)
        {
            trBontas.Visible = false;
            trBontasMegjegyzes.Visible = false;
        }
        else
        {
            trBontas.Visible = true;
            trBontasMegjegyzes.Visible = true;
        }
    }

    // a régi küldemény template-ek miatt elágazunk küldemény és egyéb irányba
    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            _TemplateMode = true;

            if (Startup == Constants.Startup.Szamla)
            {
                LoadComponentsFromTemplate((SzamlaErkeztetesFormTemplateObject)FormHeader.TemplateObject);
            }
            else
            {
                LoadComponentsFromBusinessObject((EREC_KuldKuldemenyek)FormHeader.TemplateObject);
            }

            _TemplateMode = false;
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            _TemplateMode = true;

            if (Startup == Constants.Startup.Szamla)
            {
                FormHeader.NewTemplate(GetTemplateObjectFromComponents());
            }
            else
            {
                FormHeader.NewTemplate(GetBusinessObjectFromComponents());
            }

            _TemplateMode = false;
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            _TemplateMode = true;

            if (Startup == Constants.Startup.Szamla)
            {
                FormHeader.SaveCurrentTemplate(GetTemplateObjectFromComponents());
            }
            else
            {
                FormHeader.SaveCurrentTemplate(GetBusinessObjectFromComponents());
            }

            _TemplateMode = false;
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    private void LoadComponentsFromTemplate(SzamlaErkeztetesFormTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            #region Küldemény komponensek
            LoadComponentsFromBusinessObject(templateObject.KuldemenyComponents);
            #endregion

            if (Startup == Constants.Startup.Szamla)
            {
                #region Számla komponensek
                SzamlaAdatokPanel.LoadComponentsFromBusinessObject(templateObject.SzamlaComponents, EErrorPanel1, ErrorUpdatePanel1);
                #endregion
            }
        }
    }

    private SzamlaErkeztetesFormTemplateObject GetTemplateObjectFromComponents()
    {
        SzamlaErkeztetesFormTemplateObject templateObject = new SzamlaErkeztetesFormTemplateObject();

        #region Küldemény komponensek
        templateObject.KuldemenyComponents = GetBusinessObjectFromComponents();
        #endregion

        if (Startup == Constants.Startup.Szamla)
        {
            #region Számla komponensek
            templateObject.SzamlaComponents = SzamlaAdatokPanel.GetBusinessObjectFromComponents();
            #endregion
        }

        return templateObject;
    }

    #region ISelectableUserComponentContainer Members

    public List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        componentList.Add(EFormPanel1);
        componentList.AddRange(PageView.GetSelectableChildComponents(EFormPanel1.Controls));
        componentList.Add(IktatasAdatokFormPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IktatasAdatokFormPanel.Controls));
        componentList.Add(SzamlaAdatok_EFormPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(SzamlaAdatok_EFormPanel.Controls));
        // BLG_1721
        componentList.Add(KimenoKuldemeny_EFormPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(KimenoKuldemeny_EFormPanel.Controls));
        return componentList;
    }

    #endregion

    #region tárgyszavak

    #region standard

    protected void FillStandardObjektumTargyszavaiPanel_Kuldemenyek(String id)
    {
        StandardTargyszavakPanel_Kuldemenyek.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak_Kuldemenyek.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_KuldKuldemenyek, null, null, false
            , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, true, EErrorPanel1);

        StandardTargyszavakPanel_Kuldemenyek.Visible = (otpStandardTargyszavak_Kuldemenyek.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpStandardTargyszavak_Kuldemenyek.Count > 0)
            {
                otpStandardTargyszavak_Kuldemenyek.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak_Kuldemenyek.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak_Kuldemenyek.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpStandardTargyszavak_Kuldemenyek.ReadOnly = true;
            otpStandardTargyszavak_Kuldemenyek.ViewMode = true;
        }
    }

    Result Save_StandardObjektumTargyszavai_Kuldemenyek(string kuldemenyId)
    {
        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak_Kuldemenyek.GetEREC_ObjektumTargyszavaiList
            (true);
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                        , EREC_ObjektumTargyszavaiList
                        , kuldemenyId
                        , null
                        , Constants.TableNames.EREC_KuldKuldemenyek
                        , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                        , false
                        );
        }

        return result_ot;
    }

    #endregion

    #region típusos
    private void KuldesModja_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(ParentId);
        // BLG_1721
        //SetFutarJegyzekHaFutarSzolgalat();
        SetKezbesites();
        // BUG_10649
        if (Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_ERKEZTETESKOR, false))
        {
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = RagszamRequiredTextBox.CheckRagszamRequired(KuldesModja_DropDownList.SelectedValue);
        }
    }

    protected void FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(String id)
    {
        FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(id, KuldesModja_DropDownList.SelectedValue);
    }
    protected void FillTipusosObjektumTargyszavaiPanel_Kuldemenyek(String id, string kuldesModja)
    {
        TipusosTargyszavakPanel_Kuldemenyek.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_Kuldemenyek.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_KuldKuldemenyek, "KuldesMod", new string[] { kuldesModja }, false
            , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_Kuldemenyek.Visible = (otpTipusosTargyszavak_Kuldemenyek.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_Kuldemenyek.Count > 0)
            {
                otpTipusosTargyszavak_Kuldemenyek.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_Kuldemenyek.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpTipusosTargyszavak_Kuldemenyek.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_Kuldemenyek.ReadOnly = true;
            otpTipusosTargyszavak_Kuldemenyek.ViewMode = true;
        }
    }

    Result Save_TipusosObjektumTargyszavai_Kuldemenyek(string kuldemenyId)
    {
        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpTipusosTargyszavak_Kuldemenyek.GetEREC_ObjektumTargyszavaiList(true);
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                        , EREC_ObjektumTargyszavaiList
                        , kuldemenyId
                        , null
                        , Constants.TableNames.EREC_KuldKuldemenyek
                        , "*"
                        , null
                        , false
                        , null
                        , false
                        , false
                        );
        }

        return result_ot;
    }

    #endregion

    #region kimenő küldemény

    protected void FillTipusosObjektumTargyszavaiPanel_KimenoKuldemenyek(String id)
    {
        TipusosTargyszavakPanel_KimenoKuldemenyek.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_KimenoKuldemenyek.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_KuldKuldemenyek, "PostazasIranya", new string[] { KodTarak.POSTAZAS_IRANYA.Kimeno }, false
            , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_KimenoKuldemenyek.Visible = (otpTipusosTargyszavak_KimenoKuldemenyek.Count > 0);


        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_KimenoKuldemenyek.ReadOnly = true;
            otpTipusosTargyszavak_KimenoKuldemenyek.ViewMode = true;
        }
    }

    #endregion

    #endregion

    #region BLG_452_FUTARJEGYZEK

    /// <summary>
    /// Beallitja a futarjegyzek parametereinek lathatosagat
    /// </summary>
    private void SetKezbesitesVisibility()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        TextBoxFutarjegyzekListaSzama.Visible = isTUK;
        trFutarJegyzekParametersB.Visible = isTUK;
        trTerjedelemMainPanel.Visible = isTUK;
        TerjedelemPanelA.Visible = isTUK;

        SetKezbesites();

        MinositoPartnerControlKuldKuldemenyekFormTab.Validate = false;
        MinositoPartnerControlKuldKuldemenyekFormTab.ValidationGroup = null;
    }
    /// <summary>
    /// ha futárszolgálat a küldés módja, beállítja  akontrolokat hozzá
    /// </summary>
    /// <param name="isTUK"></param>
    private void SetKezbesites()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            MinositoPartnerControlKuldKuldemenyekFormTab.Visible = true;
            CalendarControlMinositesErvenyessegIdeje.Visible = true;

            if (KuldesModja_DropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Futarszolgalat)
            {
                cbRagszamMegorzes.Visible = false;
                labelRagszam.Visible = false;
                RagszamRequiredTextBox.Visible = false;

                LabelFutarjegyzekListaSzama.Visible = true;
                // BLG_1721
                LabelFutarjegyzekListaSzama.Text = "Futárjegyzék lista száma:";

                TextBoxFutarjegyzekListaSzama.Visible = true;
            }
            else
            // BLG_1721
            if (KuldesModja_DropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Szemelyes_kezbesites)
            {
                cbRagszamMegorzes.Visible = false;
                labelRagszam.Visible = false;
                RagszamRequiredTextBox.Visible = false;

                LabelFutarjegyzekListaSzama.Visible = true;
                LabelFutarjegyzekListaSzama.Text = "Külsõ kézbesítõ könyv száma:";
                TextBoxFutarjegyzekListaSzama.Visible = true;
            }
            else
            {
                cbRagszamMegorzes.Visible = true;
                labelRagszam.Visible = true;
                RagszamRequiredTextBox.Visible = true;

                LabelFutarjegyzekListaSzama.Visible = false;
                TextBoxFutarjegyzekListaSzama.Visible = false;
            }
        }
        else
        {
            LabelFutarjegyzekListaSzama.Visible = false;
            TextBoxFutarjegyzekListaSzama.Visible = false;
            MinositoPartnerControlKuldKuldemenyekFormTab.Visible = false;
            CalendarControlMinositesErvenyessegIdeje.Visible = false;

            cbRagszamMegorzes.Visible = true;
            labelRagszam.Visible = true;
            RagszamRequiredTextBox.Visible = true;
        }
    }

    /// <summary>
    /// Beallitja a futarjegyzek parametereinek modosithatosagat
    /// </summary>
    /// <param name="isReadOnly"></param>
    private void SetKezbesitesReadOnly(bool isReadOnly)
    {
        TextBoxFutarjegyzekListaSzama.ReadOnly = isReadOnly;
        CalendarControlMinositesErvenyessegIdeje.ReadOnly = isReadOnly;

        MinositoPartnerControlKuldKuldemenyekFormTab.SetReadOnly(isReadOnly);
        TerjedelemPanelA.SetReadOnly(isReadOnly);
    }

    private void LoadKezbesitesParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (Command == CommandName.View)
        {
            SetKezbesitesReadOnly(true);
            TextBoxFutarjegyzekListaSzama.Text = erec_KuldKuldemenyek.FutarJegyzekListaSzama;
            CalendarControlMinositesErvenyessegIdeje.Text = erec_KuldKuldemenyek.MinositesErvenyessegiIdeje;

            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId = erec_KuldKuldemenyek.Minosito;
            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoSzuloPartnerId = erec_KuldKuldemenyek.MinositoSzervezet;
            // Névfeloldás Id alapján:
            MinositoPartnerControlKuldKuldemenyekFormTab.SetPartnerTextBoxById(EErrorPanel1);

            TerjedelemPanelA.SetTerjedelem(erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMegjegyzes);
        }
        else if (Command == CommandName.Modify)
        {
            TextBoxFutarjegyzekListaSzama.Text = erec_KuldKuldemenyek.FutarJegyzekListaSzama;
            CalendarControlMinositesErvenyessegIdeje.Text = erec_KuldKuldemenyek.MinositesErvenyessegiIdeje;

            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId = erec_KuldKuldemenyek.Minosito;
            MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoSzuloPartnerId = erec_KuldKuldemenyek.MinositoSzervezet;
            // Névfeloldás Id alapján:
            MinositoPartnerControlKuldKuldemenyekFormTab.SetPartnerTextBoxById(EErrorPanel1);

            TerjedelemPanelA.SetTerjedelem(erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMegjegyzes);

            SetKezbesitesReadOnly(false);
        }
        else if (Command == CommandName.New)
        {
            SetKezbesitesReadOnly(false);
        }
    }
    /// <summary>
    /// Fill kuldemeny bo from futarjegyzek params
    /// </summary>
    /// <param name="erec_KuldKuldemenyek"></param>
    private void FillKuldemenyBOFromKezbesitesParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        #region BLG_452
        if (!string.IsNullOrEmpty(TextBoxFutarjegyzekListaSzama.Text))
        {
            erec_KuldKuldemenyek.FutarJegyzekListaSzama = TextBoxFutarjegyzekListaSzama.Text;
            erec_KuldKuldemenyek.Updated.FutarJegyzekListaSzama = true;
        }
        if (!string.IsNullOrEmpty(CalendarControlMinositesErvenyessegIdeje.SelectedDateText))
        {
            erec_KuldKuldemenyek.MinositesErvenyessegiIdeje = CalendarControlMinositesErvenyessegIdeje.SelectedDateText;
            erec_KuldKuldemenyek.Updated.MinositesErvenyessegiIdeje = true;
        }

        if (!string.IsNullOrEmpty(MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoSzuloPartnerId))
        {
            erec_KuldKuldemenyek.MinositoSzervezet = MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoSzuloPartnerId;
            erec_KuldKuldemenyek.Updated.MinositoSzervezet = true;
        }

        if (!string.IsNullOrEmpty(MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId))
        {
            erec_KuldKuldemenyek.Minosito = MinositoPartnerControlKuldKuldemenyekFormTab.HiddenFieldMinositoPartnerId;
            erec_KuldKuldemenyek.Updated.Minosito = true;
        }
        string megj, menny, mennyEgys;
        TerjedelemPanelA.GetTerjedelem(out menny, out mennyEgys, out megj);

        if (!string.IsNullOrEmpty(megj))
        {
            erec_KuldKuldemenyek.TerjedelemMegjegyzes = megj;
            erec_KuldKuldemenyek.Updated.TerjedelemMegjegyzes = true;
        }
        if (!string.IsNullOrEmpty(menny))
        {
            erec_KuldKuldemenyek.TerjedelemMennyiseg = menny;
            erec_KuldKuldemenyek.Updated.TerjedelemMennyiseg = true;
        }
        if (!string.IsNullOrEmpty(mennyEgys))
        {
            erec_KuldKuldemenyek.TerjedelemMennyisegiEgyseg = mennyEgys;
            erec_KuldKuldemenyek.Updated.TerjedelemMennyisegiEgyseg = true;
        }
        #endregion BLG_452
    }

    #endregion BLG_452_FUTARJEGYZEK

    private void ConvertSzamlaAdatokToPartnerTextbox()
    {
        /*1*/
        //PartnerTextBox1.ClonePartnerTextBox = SzamlaAdatokPanel.Szallito_PartnerTextBox;
        var item = (Component_EditablePartnerTextBoxWithTypeFilter)Page.LoadControl("~/Component/EditablePartnerTextBoxWithTypeFilter.ascx");

        item.AjaxEnabled = SzamlaAdatokPanel.Szallito_PartnerTextBox.AjaxEnabled;
        item.CimHiddenField = SzamlaAdatokPanel.Szallito_PartnerTextBox.CimHiddenField;
        item.CimTextBox = SzamlaAdatokPanel.Szallito_PartnerTextBox.CimTextBox;
        item.CloneCimHiddenField = SzamlaAdatokPanel.Szallito_PartnerTextBox.CloneCimHiddenField;
        item.CloneCimTextBox = SzamlaAdatokPanel.Szallito_PartnerTextBox.CloneCimTextBox;
        item.Enabled = SzamlaAdatokPanel.Szallito_PartnerTextBox.Enabled;
        item.EnableViewState = SzamlaAdatokPanel.Szallito_PartnerTextBox.EnableViewState;
        item.Id_HiddenField = SzamlaAdatokPanel.Szallito_PartnerTextBox.Id_HiddenField;
        item.OnClick_Lov = SzamlaAdatokPanel.Szallito_PartnerTextBox.OnClick_Lov;
        item.OnClick_New = SzamlaAdatokPanel.Szallito_PartnerTextBox.OnClick_New;
        item.OnClick_View = SzamlaAdatokPanel.Szallito_PartnerTextBox.OnClick_View;
        item.SearchMode = SzamlaAdatokPanel.Szallito_PartnerTextBox.SearchMode;
        item.Text = SzamlaAdatokPanel.Szallito_PartnerTextBox.Text;
        item.TryFireChangeEvent = SzamlaAdatokPanel.Szallito_PartnerTextBox.TryFireChangeEvent;
        item.ViewButtonEnabled = SzamlaAdatokPanel.Szallito_PartnerTextBox.ViewButtonEnabled;
        item.ViewEnabled = SzamlaAdatokPanel.Szallito_PartnerTextBox.ViewEnabled;
        item.ViewMode = SzamlaAdatokPanel.Szallito_PartnerTextBox.ViewMode;
        item.ViewVisible = SzamlaAdatokPanel.Szallito_PartnerTextBox.ViewVisible;
        item.WithAllCimCheckBoxVisible = SzamlaAdatokPanel.Szallito_PartnerTextBox.WithAllCimCheckBoxVisible;
        item.WithElosztoivek = SzamlaAdatokPanel.Szallito_PartnerTextBox.WithElosztoivek;
        item.WithKuldemeny = SzamlaAdatokPanel.Szallito_PartnerTextBox.WithKuldemeny;
        item.WorldJumpEnabled = SzamlaAdatokPanel.Szallito_PartnerTextBox.WorldJumpEnabled;

        PartnerTextBox1.ClonePartnerTextBox = item;
    }
}