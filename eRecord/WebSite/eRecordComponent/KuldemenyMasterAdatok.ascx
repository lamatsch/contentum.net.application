<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KuldemenyMasterAdatok.ascx.cs"
    Inherits="eRecordComponent_KuldemenyMasterAdatok" %>
<%@ Register src="KuldemenyTextBox.ascx" tagname="KuldemenyTextBox" tagprefix="uc1" %>
<%@ Register src="../Component/KodtarakDropDownList.ascx" tagname="KodtarakDropDownList" tagprefix="uc2" %>
<%@ Register src="../Component/CsoportTextBox.ascx" tagname="CsoportTextBox" tagprefix="uc3" %>
<%@ Register src="../Component/CalendarControl.ascx" tagname="CalendarControl" tagprefix="uc4" %>
<%@ Register src="../Component/PartnerTextBox.ascx" tagname="PartnerTextBox" tagprefix="uc5" %>
<%@ Register src="../Component/CimekTextBox.ascx" tagname="CimekTextBox" tagprefix="uc6" %>
<%@ Register src="../Component/ReadOnlyTextBox.ascx" tagname="ReadOnlyTextBox" tagprefix="uc7" %>
<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <eUI:eFormPanel ID="KuldemenyForm" runat="server">
                    <table cellpadding="0" cellspacing="0" runat="server" id="MainTable" width="100%">
                        <tr class="urlapSor" id="trKuldemeny" runat="server">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label2" runat="server" Text="K�ldem�ny:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KuldemenyTextBox ID="KuldemenyTextBox1" runat="server" ViewMode="true" ReadOnly="true" />                                
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label3" runat="server" Text="�llapot:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true" />                                
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trCimzett" runat="server">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="C�mzett neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CsoportTextBox ID="CsoportId_Cimzett_CsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label4" runat="server" Text="Be�rkez�s id�pontja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:CalendarControl ID="BeerkezesiIdoCalendarControl1" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trKuldo" runat="server">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label5" runat="server" Text="K�ld�/felad� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:PartnerTextBox ID="Kuldo_PartnerTextBox1" runat="server" ViewMode="true" ReadOnly="true" />
                            </td>                        
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label6" runat="server" Text="K�ld�/felad� c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="3">
                                <uc6:CimekTextBox ID="KuldoCime_CimekTextBox1" runat="server" ViewMode="true" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trTargy" runat="server">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label7" runat="server" Text="T�rgy:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="2" >
                            <asp:TextBox ID="Targy_ReadOnlyTextBox1" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Rows="2" TextMode="MultiLine" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
</asp:Panel>
