using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_KuldemenyMasterAdatok : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{

    private const string kcs_KULDEMENY_ALLAPOT = "KULDEMENY_ALLAPOT";
   
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public EREC_KuldKuldemenyek SetKuldemenyMasterAdatok(string kuldemenyId, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (String.IsNullOrEmpty(kuldemenyId))
        {
            return null;
        }

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam_kuldGet = UI.SetExecParamDefault(Page);
        execParam_kuldGet.Record_Id = kuldemenyId;

        Result result = service.Get(execParam_kuldGet);
        if (result.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
            return null;
        }

        EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)result.Record;

        KuldemenyTextBox1.SetKuldemenyTextBoxByBusinessObject(kuldemenyObj, errorPanel, errorUpdatePanel);

        Allapot_KodtarakDropDownList.FillWithOneValue(kcs_KULDEMENY_ALLAPOT, kuldemenyObj.Allapot, errorPanel);

        CsoportId_Cimzett_CsoportTextBox.Id_HiddenField = kuldemenyObj.Csoport_Id_Cimzett;

        BeerkezesiIdoCalendarControl1.Text = kuldemenyObj.BeerkezesIdeje;

        Kuldo_PartnerTextBox1.Id_HiddenField = kuldemenyObj.Partner_Id_Bekuldo;
        Kuldo_PartnerTextBox1.Text = kuldemenyObj.NevSTR_Bekuldo;

        KuldoCime_CimekTextBox1.Id_HiddenField = kuldemenyObj.Cim_Id;
        KuldoCime_CimekTextBox1.Text = kuldemenyObj.CimSTR_Bekuldo;

        Targy_ReadOnlyTextBox1.Text = kuldemenyObj.Targy;


        return kuldemenyObj;
    }


    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(KuldemenyForm);
        componentList.AddRange(Contentum.eUtility.PageView.GetSelectableChildComponents(KuldemenyForm.Controls));
        return componentList;
    }

    #endregion

}
