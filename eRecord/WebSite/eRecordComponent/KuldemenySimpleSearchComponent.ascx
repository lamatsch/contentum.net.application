<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KuldemenySimpleSearchComponent.ascx.cs" Inherits="eRecordComponent_KuldemenySimpleSearchComponent" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<div class="DisableWrap" id="ContainerDiv" runat="server">
	<uc7:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList" Mode="ErkeztetoKonyvek" EvTextBoxId="TextBoxEv"
	    runat="server" ToolTip="Érkeztetőkönyv" />
	-
	<asp:TextBox ID="TextBoxErkeztetoszam" CssClass="mrUrlapInputNumber" runat="server" ToolTip="Érkeztetési azonosító" />
	/
	<asp:TextBox ID="TextBoxEv" CssClass="mrUrlapInputNumber" runat="server" ToolTip="Év" MaxLength="4" />
</div>
<ajaxToolkit:FilteredTextBoxExtender ID="ftbErkeztetoszam" FilterType="Numbers" TargetControlID="TextBoxErkeztetoszam" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="ftbEv" FilterType="Numbers" TargetControlID="TextBoxEv" runat="server" />