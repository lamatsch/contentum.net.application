using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_KuldemenySimpleSearchComponent : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties


    public bool Enabled
    {
        set
        {
            IktatoKonyvekDropDownList.Enabled = value;
            TextBoxEv.Enabled = value;
            TextBoxErkeztetoszam.Enabled = value;
        }
        get
        {
            return IktatoKonyvekDropDownList.Enabled;
        }
    }

    public bool ReadOnly
    {
        set
        {
            IktatoKonyvekDropDownList.ReadOnly = value;
            TextBoxEv.ReadOnly = value;
            TextBoxErkeztetoszam.ReadOnly = value;
        }
        get { return IktatoKonyvekDropDownList.ReadOnly; }
    }

    public string Text
    {
        get
        {
            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue)
                || !String.IsNullOrEmpty(this.Erkeztetoszam)
                || !String.IsNullOrEmpty(this.Ev))
            {
                string[] erkeztetoHelyek =  IktatoKonyvekDropDownList.SelectedValue.Split(new string[] { IktatoKonyvek.valueDelimeter }, StringSplitOptions.None);

                string txtErkeztetohely = "?";
                if(erkeztetoHelyek != null && erkeztetoHelyek.Length > 1)
                {
                    txtErkeztetohely = erkeztetoHelyek[1];
                }
                string txtErkeztetoszam = (String.IsNullOrEmpty(this.Erkeztetoszam) ? "?" : this.Erkeztetoszam);
                string txtEv = (String.IsNullOrEmpty(this.Ev) ? "?" : this.Ev);
                return txtErkeztetohely + " - " + txtErkeztetoszam + " /" + txtEv;
            }
            else
            {
                return "";
            }
        }

    }

    private int _AlapertelmezettEv = 0;
    public int AlapertelmezettEv
    {
        get { return _AlapertelmezettEv; }
        set { _AlapertelmezettEv = value; }
    }

    public string ErkeztetoKonyv
    {
        set { IktatoKonyvekDropDownList.SetSelectedValue(value); }
        get { return IktatoKonyvekDropDownList.SelectedValue; }
    }

    public string Erkeztetoszam
    {
        set { TextBoxErkeztetoszam.Text = value; }
        get { return TextBoxErkeztetoszam.Text; }
    }

    public string Ev
    {
        set { TextBoxEv.Text = value; }
        get { return TextBoxEv.Text; }
    }

    public DropDownList ErkeztetoKonyvDropDownList
    {
        get { return IktatoKonyvekDropDownList.DropDownList; }
    }

    public TextBox ErkeztetoszamTextBox
    {
        get { return TextBoxErkeztetoszam; }
    }

    public TextBox EvTextBox
    {
        get { return TextBoxEv; }
    }

    public HtmlControl Container
    {
        get { return this.ContainerDiv; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //JavaScripts.RegisterPopupWindowClientScript(Page);

        //ASP.NET 2.0 bug work around
        //TextBoxErkeztetoszam.Attributes.Add("readonly", "readonly");
        //TextBoxEv.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    // ha a kit�ltend� mez�knek megfelel� �rt�kek k�z�l mindegyik �res, akkor defaultnak tekintj�k
    protected bool IsDefaultSearchObject(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        bool isDefaultSearchObject = true;
        if (erec_KuldKuldemenyekSearch != null)
        {
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value);
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Erkezteto_Szam.ValueTo);
            if (erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                isDefaultSearchObject &= String.IsNullOrEmpty(IktatoKonyvek.GetIktatokonyvValue(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch));
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value);
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo);
            }
        }
        return isDefaultSearchObject;
    }

    public void FillIktatoKonyvek(string MegkulJelzes, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        IktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                       , this.Ev, this.Ev
                       , true, false, MegkulJelzes, ErrorPanel);
    }

    public void FillSearchObjectFromComponents(ref EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
    {
        if (erec_KuldKuldemenyekSearch != null)
        {
            if (erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }

            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue))
            {
                IktatoKonyvekDropDownList.SetSearchObject(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);
            }

            if (!String.IsNullOrEmpty(this.Erkeztetoszam))
            {
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value = this.Erkeztetoszam;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.ValueTo = this.Erkeztetoszam;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator = Query.Operators.equals;
            }
            if (!String.IsNullOrEmpty(this.Ev))
            {
                erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = this.Ev;
                erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo = this.Ev;
                erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;
            }
        }
    }

    /// <summary>
    /// A keres�si objektum alapj�n kit�lti a beviteli mez�ket.
    /// </summary>
    /// <param name="erec_KuldKuldemenyekSearch">A kit�lt�s alapj�ul szolg�l� keres�si objektum.</param>
    /// <returns>True, ha a keres�si objektum form�tuma egyszer� (azaz intervallumok n�lk�li), false, ha intervallum t�pus� megad�sok is voltak. </returns>
    public bool FillComponentsFromSearchObject(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        bool isSimpleSearch = true;
        this.ClearInputFields();
        bool isDefaultSearchObject = IsDefaultSearchObject(erec_KuldKuldemenyekSearch);

        if (erec_KuldKuldemenyekSearch != null)
        {
            if (erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                this.Ev = erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value;
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek(IktatoKonyvek.GetIktatokonyvValue(erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch), ErrorPanel);

                isSimpleSearch &= (
                    (erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo
                        && erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == Query.Operators.equals)
                    ||
                    (erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == ""
                        && erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo == ""
                        && erec_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == "")
                        );
            }
            else
            {
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek("", ErrorPanel);
            }

            this.Erkeztetoszam = erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value;

            isSimpleSearch &= (
                (erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value == erec_KuldKuldemenyekSearch.Erkezteto_Szam.ValueTo
                    && erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator == Query.Operators.equals)
                ||
                (erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value == ""
                    && erec_KuldKuldemenyekSearch.Erkezteto_Szam.ValueTo == ""
                    && erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator == "")
                    );

        }
        return isSimpleSearch;
    }


    public void ClearInputFields()
    {
        IktatoKonyvekDropDownList.SetSelectedValue("");
        this.Erkeztetoszam = "";
        this.Ev = "";
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IktatoKonyvekDropDownList.DropDownList);
        componentList.Add(TextBoxErkeztetoszam);
        componentList.Add(TextBoxEv);

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
