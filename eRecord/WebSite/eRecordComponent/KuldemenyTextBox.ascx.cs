using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class eRecordComponent_KuldemenyTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, UI.ILovListTextBox
{
    #region public properties


    private string filter;

    public string Filter
    {
        get { return filter; }
        set { filter = value; }
    }

    private string filterByObjektumKapcsolat = String.Empty;

    public string FilterByObjektumKapcsolat
    {
        get { return filterByObjektumKapcsolat; }
        set { filterByObjektumKapcsolat = value; }
    }

    private string objektumId = String.Empty;

    public string ObjektumId
    {
        get { return objektumId; }
        set { objektumId = value; }
    }

    private string postazasIranya = String.Empty;

    /// <summary>
    /// Sz�r�s a post�z�s ir�nya szerint
    /// Lehet t�bb �rt�k is, vessz�vel elv�lasztva, aposztr�fok n�lk�l
    /// </summary>

    public string PostazasIranya
    {
        get { return postazasIranya; }
        set { postazasIranya = value; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            KuldemenyTargy.Enabled = value;
            LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
                //UI.SwapImageToDisabled(NewImageButton);
            }
        }
        get { return KuldemenyTargy.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            KuldemenyTargy.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return KuldemenyTargy.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}

    public string Text
    {
        set { KuldemenyTargy.Text = value; }
        get { return KuldemenyTargy.Text; }
    }

    public TextBox TextBox
    {
        get { return KuldemenyTargy; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                KuldemenyTargy.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                KuldemenyTargy.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetKuldemenyTextBoxById(errorPanel, errorUpdatePanel);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetKuldemenyTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_KuldKuldemenyek erec_Kuldemenyek = (EREC_KuldKuldemenyek)result.Record;
                SetKuldemenyTextBoxByBusinessObject(erec_Kuldemenyek, errorPanel, errorUpdatePanel);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }

    public void SetKuldemenyTextBoxByBusinessObject(EREC_KuldKuldemenyek erec_kuldemenyek, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Id_HiddenField = erec_kuldemenyek.Id;

        String kuldemenyAzonosito = "";
        if (erec_kuldemenyek.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            kuldemenyAzonosito = String.Format(Resources.List.UI_FormatString_Kuldemeny_Cimzett, erec_kuldemenyek.NevSTR_Bekuldo);
        }
        else
        {
            kuldemenyAzonosito = erec_kuldemenyek.Azonosito;
        }

        Text = kuldemenyAzonosito;
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        string filterQuery = String.Empty;
        if (!String.IsNullOrEmpty(filter))
        {
            filterQuery = "&" + QueryStringVars.Filter + "=" + filter;
        }

        if (!String.IsNullOrEmpty(FilterByObjektumKapcsolat) && !String.IsNullOrEmpty(objektumId))
        {
            filterQuery += "&" + QueryStringVars.ObjektumKapcsolatTipus + "=" + filterByObjektumKapcsolat;
            filterQuery += "&" + QueryStringVars.ObjektumId + "=" + ObjektumId;
        }
        
        string qs = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + KuldemenyTargy.ClientID
           + filterQuery;

        if (!String.IsNullOrEmpty(PostazasIranya))
        {
            qs += "&" + QueryStringVars.PostazasIranya + "=" + PostazasIranya;
        }
        OnClick_Lov = JavaScripts.SetOnClientClick("KuldKuldemenyekLovList.aspx", qs
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "KuldKuldemenyekForm.aspx", "", HiddenField1.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
                HiddenField1.ClientID + "').value = '';return false;";

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(KuldemenyTargy);
        componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
