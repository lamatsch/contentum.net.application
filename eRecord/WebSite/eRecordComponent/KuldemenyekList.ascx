<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="KuldemenyekList.ascx.cs" Inherits="eRecordComponent_KuldemenyekList" %>

<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="DosszieTreeView.ascx" TagName="DosszieTreeView" TagPrefix="dtv" %>
<%@ Register Src="~/eRecordComponent/SztornoPopup.ascx" TagName="SztornoPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <%--Hiba megjelenites--%>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />

<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td rowspan="2" valign="top" align="left" id="PanelDossziekTD" runat="server">
            <div id="PanelDossziekDiv" style="position: relative;">
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="PanelDossziekTVPanel" runat="server">
                                <dtv:DosszieTreeView ID="DosszieTreeView" runat="server" />
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="DossziekCE" runat="server" TargetControlID="PanelDossziekTVPanel"
                                CollapsedSize="0" Collapsed="true"
                                AutoCollapse="false" AutoExpand="false"
                                ExpandDirection="Horizontal"
                                CollapsedImage="../images/hu/egyeb/dosszie_panel_ki.gif" ExpandedImage="../images/hu/egyeb/dosszie_panel_be.gif"
                                ImageControlID="DossziekCEButton"
                                ScrollContents="false">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                        <td valign="middle" style="background-image: url(images/hu/egyeb/dosszie_panel_hatter.jpg); background-repeat: repeat-y;" onclick="$find('<%=DossziekCE.ClientID %>').togglePanel();"
                            style="cursor: hand;">
                            <asp:ImageButton runat="server" ID="DossziekCEButton"
                                OnClientClick="return false;" ImageUrl="../images/hu/egyeb/dosszie_panel_ki.gif" />

                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton runat="server" ID="KuldKuldemenyekCPEButton" OnClientClick="return false;" ImageUrl="../images/hu/Grid/minus.gif" />
        </td>
        <td style="text-align: left; vertical-align: top; width: 100%;">
            <asp:UpdatePanel ID="KuldKuldemenyekUpdatePanel" runat="server" OnLoad="KuldKuldemenyekUpdatePanel_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="KuldemenyekCPE" runat="server" TargetControlID="Panel1"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="KuldKuldemenyekCPEButton"
                        CollapseControlID="KuldKuldemenyekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                        AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif" ExpandedImage="../images/hu/Grid/minus.gif"
                        ImageControlID="KuldKuldemenyekCPEButton" ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja"
                        CollapsedText="K�ldem�nyek list�ja">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="Panel1" runat="server">
                        <table style="width: 98%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <uc:SztornoPopup runat="server" ID="SztornoPopup" />
                                    <uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
                                    <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                    <asp:GridView ID="KuldKuldemenyekGridView" runat="server" OnRowCommand="KuldKuldemenyekGridView_RowCommand"
                                        CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                        PagerSettings-Visible="false" AllowSorting="True" OnPreRender="KuldKuldemenyekGridView_PreRender"
                                        AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="KuldKuldemenyekGridView_Sorting"
                                        OnRowDataBound="KuldKuldemenyekGridView_RowDataBound">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <div class="DisableWrap">
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" HeaderStyle-CssClass="GridViewSelectRowImage"
                                                ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                <HeaderStyle Width="25px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Csny.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="K.">
                                                <ItemTemplate>
                                                    <asp:Image ID="CsatoltImage" AlternateText="Kapcsolat" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="../images/hu/ikon/csatolt.gif" Style="cursor: pointer"
                                                        onmouseover="Utility.UI.SetElementOpacity(this,0.7);" onmouseout="Utility.UI.SetElementOpacity(this,1);" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage" HeaderText="F."
                                                HeaderStyle-Width="25px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="FeladatImage" AlternateText="Kezel�si feljegyz�s" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MinositesNev" HeaderText="Min�s�t�s" SortExpression="EREC_KuldKuldemenyek.Minosites" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MegkulJelzes" HeaderText="�rkeztet�k�nyv" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Erkezteto_Szam" HeaderText="�rkeztet� sz�m" SortExpression="EREC_KuldKuldemenyek.Erkezteto_Szam" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Ev" HeaderText="�v" SortExpression="EREC_IraIktatokonyvek.Ev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FullErkeztetoSzam" HeaderText="�rkeztet�si azonos�t�" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_KuldKuldemenyek.Erkezteto_Szam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="40px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d" SortExpression="EREC_KuldKuldemenyek.BarCode">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Hivatkoz�si&nbsp;sz�m" SortExpression="EREC_KuldKuldemenyek.HivatkozasiSzam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NevSTR_Bekuldo" HeaderText="K�ld�/felad� neve" SortExpression="EREC_KuldKuldemenyek.NevSTR_Bekuldo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CimSTR_Bekuldo" HeaderText="K�ld�/felad� c�me" Visible="false" SortExpression="EREC_KuldKuldemenyek.CimSTR_Bekuldo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Csoport_Id_CimzettNev" HeaderText="C�mzett&nbsp;neve" SortExpression="Csoportok_CimzettNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_KuldKuldemenyek.Targy">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="125px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BeerkezesIdeje" HeaderText="Be�rk.id�p." ItemStyle-HorizontalAlign="Center"
                                                SortExpression="BeerkezesIdeje">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ErkeztetoNev" HeaderText="�rkeztet�" SortExpression="Csoportok_ErkeztetoNev.Nev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KuldesModNev" HeaderText="Be�rkez�s m�dja" Visible="false" SortExpression="EREC_KuldKuldemenyek.KuldesMod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tartalom" HeaderText="Tartalom" Visible="false" SortExpression="EREC_KuldKuldemenyek.Tartalom">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--BLG_237--%>
                                            <asp:TemplateField HeaderText="Postai azonos�t�" SortExpression="EREC_KuldKuldemenyek.RagSzam" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <%-- iktatott �llapot eset�n link az iratra --%>
                                                    <asp:Label ID="labelRagszam" runat="server" Text='<%#Eval("Ragszam").ToString().Length>20?String.Concat(Eval("Ragszam").ToString().Substring(0,20),"..."):Eval("Ragszam").ToString() %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField DataField="RagSzam" HeaderText="Postai azonos�t�" Visible="false" SortExpression="EREC_KuldKuldemenyek.RagSzam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="50px" />
                                            </asp:BoundField>--%>

                                            <asp:BoundField DataField="SurgossegNev" HeaderText="K�zbes�t�s priorit�sa" Visible="false" SortExpression="EREC_KuldKuldemenyek.Surgosseg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AdathordozoTipusa_Nev" HeaderText="K�ldem�ny t�pusa" Visible="false" SortExpression="EREC_KuldKuldemenyek.AdathordozoTipusa">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Munkaallomas" HeaderText="Munka�llom�s" Visible="false" SortExpression="EREC_KuldKuldemenyek.Munkaallomas">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <%-- iktatott �llapot eset�n link az iratra --%>
                                                    <asp:Label ID="labelAllapotNev" runat="server" Text='<%#                                        
	string.Concat(                                          
	  Eval("AllapotNev")                                                     
	 , (Eval("Allapot") as string) == "04" ?
		"<a href=\"IraIratokList.aspx?Id=" + this.GetIdFrom_IratIktatoszamEsId(Eval("IratIktatoszamEsId") as string)
		+ "\" style=\"text-decoration:underline\">(" + this.GetIktatoszamFrom_IratIktatoszamEsId(Eval("IratIktatoszamEsId") as string)
		+")<a />"
		: (Eval("Allapot") as string) == "03" ?
			"<br/>(" + (Eval("TovabbitasAlattAllapotNev") as string) +
			    ((Eval("TovabbitasAlattAllapot") as string) == "04" ?
					" <a href=\"IraIratokList.aspx?Id=" + this.GetIdFrom_IratIktatoszamEsId(Eval("IratIktatoszamEsId") as string)
					+ "\" style=\"text-decoration:underline\">(" + this.GetIktatoszamFrom_IratIktatoszamEsId(Eval("IratIktatoszamEsId") as string)
					+")<a />" : ""
				) + ")" : "")  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IktatniKellNev" HeaderText="Iktat�s" SortExpression="IktatasiKotelezettsegKodTarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PIRAllapotNev" HeaderText="PIR" Visible="false" SortExpression="EREC_Szamlak.PIRAllapot">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="10px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KezelesTipusNev" HeaderText="Kezel�si �tas�t�s">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="KulsoAzonosito" HeaderText="<%$Resources:Form,KuldemenyKulsoAzonosito %>" SortExpression="EREC_KuldKuldemenyek.KulsoAzonosito" Visible="true">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>                                            
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
