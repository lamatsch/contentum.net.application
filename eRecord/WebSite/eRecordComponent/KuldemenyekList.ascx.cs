﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eIntegrator.Service;

public partial class eRecordComponent_KuldemenyekList : System.Web.UI.UserControl
{
    private ASP.component_listheader_ascx ListHeader1;
    private String Startup = String.Empty;

    public ASP.component_listheader_ascx ListHeader
    {
        get { return this.ListHeader1; }
        set { this.ListHeader1 = value; }
    }

    private ScriptManager ScriptManager1;

    public ScriptManager ScriptManager
    {
        get { return this.ScriptManager1; }
        set { this.ScriptManager1 = value; }
    }

    public bool DossziePanelVisible
    {
        get { return PanelDossziekTD.Visible; }
        set { PanelDossziekTD.Visible = value; }
    }

    public ASP.erecordcomponent_dosszietreeview_ascx AttachedDosszieTreeView
    {
        get { return this.DosszieTreeView; }
        set { this.DosszieTreeView = value; }
    }

    private string _dosszieId = string.Empty;

    public string DosszieId
    {
        get { return this._dosszieId; }
        set { this._dosszieId = value; }
    }


    private string _dosszieId_HiddenField_ClientId;

    public string DosszieId_HiddenField_ClientId
    {
        get { return this._dosszieId_HiddenField_ClientId; }
        set { this._dosszieId_HiddenField_ClientId = value; }
    }


    public GridView KuldemenyekGridView
    {
        get { return this.KuldKuldemenyekGridView; }
    }

    UI ui = new UI();

    private bool isActive = true;

    public bool IsActive
    {
        get { return isActive; }
        set { isActive = value; }
    }


    #region Base Page

    public void InitPage()
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        if (Startup == Constants.Startup.Szamla || Startup == Constants.Startup.SzamlaSearchForm || Startup == Constants.Startup.SzamlaFelvitel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Szamla" + CommandName.List);
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Kuldemeny" + CommandName.List);
        }

        if (Session["KuldKuldemenyekListStartup"] == null)
        {
            // ha Outlookbol nyitjak meg, akkor a querystringeket atkell tenni sessionbe, majd atiranyitani querystring nelkul!!!
            if (Startup == Constants.Startup.FromEmail && !String.IsNullOrEmpty(Page.Request.QueryString.Get("EmailBoritekokId")))
            {
                Session["KuldKuldemenyekListStartup"] = Startup;
                Session["KuldKuldemenyekListEmailBoritekokId"] = Page.Request.QueryString.Get("EmailBoritekokId");
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
            else if (Startup == "ErkeztetesIktatasNelkul")
            {
                Session["KuldKuldemenyekListStartup"] = Startup;
                //Session["KuldKuldemenyekListEmailBoritekokId"] = Page.Request.QueryString.Get("EmailBoritekokId");
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }

            else if (Startup == Constants.Startup.SearchForm)
            {
                Session["KuldKuldemenyekListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }

            else if (Startup == Constants.Startup.SzamlaSearchForm)
            {
                Session["KuldKuldemenyekListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath + "?" + QueryStringVars.Startup + "=" + Constants.Startup.Szamla);
            }

            else if (Startup == Constants.Startup.SzamlaFelvitel)
            {
                Session["KuldKuldemenyekListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath + "?" + QueryStringVars.Startup + "=" + Constants.Startup.Szamla);
            }
        }



        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        // Jobb oldali dossziélista
        if (FunctionRights.GetFunkcioJog(Page, "MappakList"))
        {
            AttachedDosszieTreeView.FilterList += KuldKuldemenyekGridViewBind;
            JavaScripts.RegisterSetDossziePositionScript(Page, false);
        }
        else
        {
            this.DossziePanelVisible = false;
        }

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, KuldKuldemenyekGridView);

        if (Startup == Constants.Startup.Szamla)
        {
            string[] BoundFieldsToHide = new string[] { "HivatkozasiSzam", "Tartalom", "SurgossegNev", "AdathordozoTipusa_Nev", "IktatniKellNev" };

            bool isPIREnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            string[] BoundFieLdsToShowForPIR = new string[] { "PIRAllapotNev" };

            for (int i = KuldemenyekGridView.Columns.Count - 1; i >= 0; i--)
            {
                if (KuldemenyekGridView.Columns[i] is BoundField)
                {
                    BoundField bf = (BoundField)KuldemenyekGridView.Columns[i];
                    if (Array.IndexOf(BoundFieldsToHide, bf.DataField) > -1)
                    {
                        bf.Visible = false;
                        //KuldemenyekGridView.Columns.RemoveAt(i);
                    }
                    else
                    {
                        if (!IsPostBack && isPIREnabled && Array.IndexOf(BoundFieLdsToShowForPIR, bf.DataField) > -1)
                        {
                            bf.Visible = true;
                        }
                    }

                }
            }
        }

        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            UI.SetGridViewColumnVisiblity(KuldemenyekGridView, "KezelesTipusNev", false);
        }

        ///BLG_453
        SetGridTukColumns();
        //SetGridTukColumnsVisibility();
    }

    public void LoadPage()
    {
        if (Startup == Constants.Startup.Szamla)
        {
            ListHeader1.HeaderLabel = Resources.List.KuldKuldemenyekListHeaderTitle_Szamla;

            //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.SzamlaSearch;



            #region PIR állapot aszinkron update
            ExecParam execParam = UI.SetExecParamDefault(Page);
            bool isPIREnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            if (isPIREnabled)
            {
                // aszinkron update hívás
                System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(PirEdokSzamlak.PIRAllapotUpdate_TomegesForAsync), execParam);
            }
            #endregion PIR állapot aszinkron update
        }
        else
        {
            ListHeader1.HeaderLabel = Resources.List.KuldKuldemenyekLisHeader;
        }
        ListHeader1.SearchObjectType = typeof(EREC_KuldKuldemenyekSearch);


        ListHeader1.SetRightFunctionButtonsVisible(false);

        // Dossziés ikonok láthatóságának állítása
        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            ListHeader1.DossziebaHelyezVisible = false;
            ListHeader1.DossziebolKiveszVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakNew");
        }
        else
        {
            ListHeader1.DossziebaHelyezVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakInvalidate");
            ListHeader1.DossziebolKiveszVisible = false;
        }

        // bal oldali gombsor lathatosaga:
        // nincsen invalidalas, nahem sztorno!!
        ListHeader1.InvalidateVisible = false;

        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager.RegisterPostBackControl(ListHeader1.ExportButton);
        //bernat.laszlo eddig

        // A baloldali, alapból invisible ikonok megjelenítése:
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        // jobb oldali gombsor lathatosaga:
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //ListHeader1.BontasVisible = true;
        ListHeader1.SzignalasVisible = true;
        ListHeader1.AtadasraKijelolVisible = true;
        ListHeader1.AtvetelVisible = true;
        ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;;

        ListHeader1.SztornoVisible = true;

        if (Startup == Constants.Startup.Szamla)
        {
            ListHeader1.BejovoIratIktatasVisible = false;

            ListHeader1.IktatasMegtagadasVisible = false;
            // BUG_7723 
            //ListHeader1.SzlaExportVisible = true;


        }
        else
        {
            ListHeader1.BejovoIratIktatasVisible = true;

            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEJOVO_IRAT_MUNKAIRATBA_IKTATAS))
            {
                ListHeader1.MunkapeldanyIktatasaVisible = true;
            }

            ListHeader1.IktatasMegtagadasVisible = true;
        }
        ListHeader1.LezarasVisible = true;
        ListHeader1.LezarasVisszavonasVisible = true;

        if (Startup == Constants.Startup.Szamla)
        {
            ListHeader1.BoritonyomtatasVisible = false;
        }
        else
        {
            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                ListHeader1.BoritonyomtatasVisible = false;
            }
            else
            {
                ListHeader1.BoritonyomtatasVisible = true;
            }
            //Hianyzik: A3 es A4 nyomtatas + XML export! :-(
        }

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        string qsStartup = String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekSearch.aspx", qsStartup
            , 1024, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + qsStartup
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(KuldKuldemenyekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekListajaPrintForm.aspx");

        #region BLG 1131 visszavétel
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        {
            ListHeader1.VisszavetelVisible = true;
            ListHeader1.VisszavetelEnabled = true;
            ListHeader1.VisszavetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + KuldemenyekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
        #endregion

        //string column_v = "";
        //for (int i = 0; i < KuldemenyekGridView.Columns.Count; i++)
        //{
        //    if (KuldemenyekGridView.Columns[i].Visible)
        //    {
        //        column_v = column_v + "1";
        //    }
        //    else
        //    {
        //        column_v = column_v + "0";
        //    }
        //}

        //if (Startup == Constants.Startup.Szamla)
        //{
        //    ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("SzamlakSSRS.aspx"
        //        , QueryStringVars.Filter + "=" + column_v);
        //}
        //else
        //{
        //    ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("KuldKuldemenyekSSRS.aspx"
        //        , QueryStringVars.Filter + "=" + column_v);
        //}

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(KuldKuldemenyekGridView.ClientID);

        //ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
        //        + KuldKuldemenyekGridView.ClientID + "','check'); " 
        //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + KuldKuldemenyekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //ListHeader1.AtadasraKijelolOnClientClick += JavaScripts.SetOnClientClick("AtadasForm.aspx", 
        //    QueryStringVars.KuldemenyId + "=" + "'+getIdsBySelectedCheckboxes('"+KuldKuldemenyekGridView.ClientID+"','check')+'"
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                        + KuldKuldemenyekGridView.ClientID + "','check'); "
                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LezarasVisszavonasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.CsatolasVisible = true;

        // Zarolas:
        Kuldemenyek.SetZarolas(KuldKuldemenyekGridView.ClientID, ListHeader1.Lock, KuldKuldemenyekUpdatePanel);

        // Zarolas_feloldasa:
        Kuldemenyek.SetZarolas_feloldasa(KuldKuldemenyekGridView.ClientID, ListHeader1.Unlock, KuldKuldemenyekUpdatePanel);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(KuldKuldemenyekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, "", true);

        // jobb oldali gombsor kattintas:
        //ListHeader1.BontasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickSztornoConfirm(KuldKuldemenyekGridView.ClientID);        
        //ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.BoritonyomtatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //Hianyzik: A3 es A4 nyomtatas + XML export! :-(

        ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.DossziebaHelyezOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                + KuldKuldemenyekGridView.ClientID + "','check'); "
                                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                                + " if (!$get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value) {alert('Nincs dosszié kijelölve' + $get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value); return false;}";

        ListHeader1.DossziebolKiveszOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                + KuldKuldemenyekGridView.ClientID + "','check'); "
                                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        Search.SetIdsToSearchObject(Page, "Id", new EREC_KuldKuldemenyekSearch(true));

        // Ha érkeztető képernyőt is kell nyitni, nem töltjük fel a listát:
        if (Session["KuldKuldemenyekListStartup"] != null &&
                (Session["KuldKuldemenyekListStartup"].ToString() == "ErkeztetesIktatasNelkul"
                  || Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.FromEmail
                  || Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SearchForm
                  || Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SzamlaSearchForm
                  || Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SzamlaFelvitel)
            )
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            KuldKuldemenyekGridViewBind();
        }

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = KuldKuldemenyekGridView;

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    public void PreRenderPage()
    {
        string FunkcioBase = null;
        if (Startup == Constants.Startup.Szamla)
        {
            FunkcioBase = "Szamla";
        }
        else
        {
            FunkcioBase = "Kuldemeny";
        }

        // TODO: atirni KuldKuldemenyek
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Modify);
        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Invalidate);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.ViewHistory);
        //bernat.laszlo added
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.ExcelExport);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        //ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Lock);
        //ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Unlock);

        //TODO: nincsen ilyen funkcio, es a sehol nem ellenorizzuk a jogot ra! :-(
        //ListHeader1.SendObjectsEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.SendObjects);

        // jobb oldali gombsor jogosultsag engedelyezes:
        Kuldemenyek.SetIktatas(null, null, ListHeader1.BejovoIratIktatas, null);

        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEJOVO_IRAT_MUNKAIRATBA_IKTATAS))
        {
            Kuldemenyek.SetIktatasMunkapeldany(null, null, ListHeader1.MunkapeldanyIktatasa, null);
        }

        // Email iktatás megtagadás
        Kuldemenyek.SetIktatasMegtagadas(null, null, ListHeader1.IktatasMegtagadas, null);

        // Lezaras:
        Kuldemenyek.SetLezaras(null, null, ListHeader1.Lezaras, null);

        ListHeader1.LezarasVisszavonasEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + "LezarasVisszavonas");

        // Sztornozas:
        Kuldemenyek.SetSztorno(null, null, ListHeader1.Sztorno, null);

        ListHeader1.SzignalasEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + "Szignalas");

        ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + "Atadas");

        ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + "Atvetel");

        ListHeader1.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + "Atvetel"); // egyelőre az átvétel jogához kötjük

        //ListHeader1.BontasEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Bontas);
        //ListHeader1.SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Sztorno);
        //ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, CommandName.BejovoIratIktatas);
        //ListHeader1.LezarasEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.Lezaras);
        ListHeader1.BoritonyomtatasEnabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Boritonyomtatas);
        //Hianyzik: A3 es A4 nyomtatas + XML export! :-(

        ListHeader1.CsatolasEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + "CsatolasNew");

        // BUG_7723
        bool szlaexportvisible = false;
        if (Startup == Constants.Startup.Szamla)
        {
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            string PIR_Rendszer = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.PIR_RENDSZER);

            if (PIR_Rendszer.ToUpper().Equals("PROCUSYS"))
                szlaexportvisible = FunctionRights.GetFunkcioJog(Page, "SzamlaExport");
        }
        ListHeader1.SzlaExportVisible = szlaexportvisible;


        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(KuldKuldemenyekGridView);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        if (Session["KuldKuldemenyekListStartup"] != null)
        {
            // Ha Outlook-bol nyitjak meg, akkor new popup-ot kell csinalni!
            if (Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.FromEmail)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&EmailBoritekokId=" + Session["KuldKuldemenyekListEmailBoritekokId"].ToString()
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "EmailErkeztetes", script, true);
                Session.Remove("KuldKuldemenyekListStartup");
                Session.Remove("KuldKuldemenyekListEmailBoritekokId");
            }
            // Ha menubol nyitottak meg
            else if (Session["KuldKuldemenyekListStartup"].ToString() == "ErkeztetesIktatasNelkul")
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErkeztetesIktatasNelkul", script, true);
                Session.Remove("KuldKuldemenyekListStartup");
            }
            else if (Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekSearch.aspx", String.Empty
                        , 1024, 768, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "KuldemenySearch", script, true);
                Session.Remove("KuldKuldemenyekListStartup");
            }
            else if (Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SzamlaSearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekSearch.aspx", QueryStringVars.Startup + "=" + Constants.Startup.Szamla
                        , 1024, 768, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SzamlaSearch", script, true);
                Session.Remove("KuldKuldemenyekListStartup");
            }
            else if (Session["KuldKuldemenyekListStartup"].ToString() == Constants.Startup.SzamlaFelvitel)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                    JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekForm.aspx",
                    QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Startup + "=" + Constants.Startup.Szamla
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                        , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SzamlaSearch", script, true);
                Session.Remove("KuldKuldemenyekListStartup");
            }
        }
        //// Ha menubol nyitottak meg
        //else if (Startup == Constants.Startup.Szamla)
        //{
        //    string qs = String.Empty;
        //    if (Startup == Constants.Startup.Szamla)
        //    {
        //        qs = String.Format("{0}={1}", QueryStringVars.Startup, Startup);
        //    }

        //    string script = "OpenNewWindow(); function OpenNewWindow() { ";
        //    script +=
        //        JavaScripts.SetOnClientClick_DisplayUpdateProgress("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + qs
        //            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
        //            , CustomUpdateProgress1.UpdateProgress.ClientID);
        //    script += "}";
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Szamla", script, true);
        //}

        ui.SetClientScriptToGridViewSelectDeSelectButton(KuldKuldemenyekGridView);

        string column_v = "";
        for (int i = 0; i < KuldemenyekGridView.Columns.Count; i++)
        {
            DataControlField col = KuldemenyekGridView.Columns[i];
            if (col is BoundField && (col as BoundField).DataField == "MinositesNev")
                continue;

            if (KuldemenyekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        if (Startup == Constants.Startup.Szamla)
        {
            ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("SzamlakSSRS.aspx"
                , QueryStringVars.Filter + "=" + column_v);
        }
        else
        {
            ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("KuldKuldemenyekSSRS.aspx"
                , QueryStringVars.Filter + "=" + column_v);
        }
    }


    #endregion

    #region Master List

    public void KuldKuldemenyekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KuldKuldemenyekGridView", ViewState, "EREC_KuldKuldemenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KuldKuldemenyekGridView", ViewState, SortDirection.Descending);

        KuldKuldemenyekGridViewBind(sortExpression, sortDirection);
    }

    protected void KuldKuldemenyekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (!this.IsActive)
            return;

        DokumentumVizualizerComponent.ClearDokumentElements();

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldKuldemenyekSearch search = null;

        if (!IsPostBack)
        {
            //string Startup = Page.Request.QueryString.Get(QueryStringVars.Startup);
            // CR3439 Constants javítás
            if (Startup == Constants.Startup.FromVezetoiPanel_Kuldemeny) // ha vezetoi panelról indítottuk
            {
                search = new EREC_KuldKuldemenyekSearch(true);
                Kuldemenyek.FilterObject filterObject = new Kuldemenyek.FilterObject();
                filterObject.QsDeserialize(Page.Request.QueryString);
                filterObject.SetSearchObject(ExecParam, ref search);

                Search.SetSearchObject(Page, search);
            }
            else if (Startup == Constants.Startup.FromFeladataim) // ha feladataim panelról indítottuk
            {
                // új search objektumot hozunk létre, és sessionbe tesszük
                search = new EREC_KuldKuldemenyekSearch(true);
                Search.SetSearchObject(Page, search);
            }
        }

        // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
        if (Startup == Constants.Startup.Szamla)
        {
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SzamlaSearch))
            {
                search = (EREC_KuldKuldemenyekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_KuldKuldemenyekSearch), Page);

                // TODO: összehasonlítás  Search-be
                //if ((Search.IsDefaultSearchObject(typeof(EREC_KuldKuldemenyekSearch), search)
                //    && search.fts_tree_objektumtargyszavai == null) == false)
                if (Search.IsDefaultSearchObject(typeof(EREC_KuldKuldemenyekSearch), search) == false)
                {

                    //search.Tipus.Value = KodTarak.KULDEMENY_TIPUS.Szamla;
                    //search.Tipus.Operator = Query.Operators.equals;

                    // sessionbe mentés
                    Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.SzamlaSearch);
                }
            }

            search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_KuldKuldemenyekSearch(true), Constants.CustomSearchObjectSessionNames.SzamlaSearch);
            search.Tipus.Value = KodTarak.KULDEMENY_TIPUS.Szamla;
            search.Tipus.Operator = Query.Operators.equals;

        }
        else
        {
            if (!Search.IsSearchObjectInSession(Page, typeof(EREC_KuldKuldemenyekSearch)))
            {
                search = (EREC_KuldKuldemenyekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_KuldKuldemenyekSearch), Page);

                if (Search.IsDefaultSearchObject(typeof(EREC_KuldKuldemenyekSearch), search) == false)
                {
                    // sessionbe mentés
                    Search.SetSearchObject(Page, search);
                }
            }


            search = (EREC_KuldKuldemenyekSearch)Search.GetSearchObject(Page, new EREC_KuldKuldemenyekSearch(true));
        }

        // Van-e szűrés kérés paraméterben? (Saját küldeményekre)
        // (Csak első kéréskor figyeljük)
        if (!IsPostBack)
        {
            string filter = Request.QueryString.Get(QueryStringVars.Filter);
            if (filter == Constants.Sajat)
            {
                search.Manual_Sajat.Value =
                    Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
                search.Manual_Sajat.Operator = Contentum.eQuery.Query.Operators.equals;

                // Nem kell:
                // (Iktatott, Postázott, Kimenő, összeállítás alatt, Expediált, Sztornózott)
                string NotInAllapotok = "'" + KodTarak.KULDEMENY_ALLAPOT.Iktatva
                    + "','" + KodTarak.KULDEMENY_ALLAPOT.Postazott
                    + "','" + KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt
                    + "','" + KodTarak.KULDEMENY_ALLAPOT.Expedialt
                    + "','" + KodTarak.KULDEMENY_ALLAPOT.Sztorno + "'";

                search.Manual_Sajat_Allapot.Value = NotInAllapotok;
                search.Manual_Sajat_Allapot.Operator = Contentum.eQuery.Query.Operators.notinner;

                search.Manual_Sajat_TovabbitasAlattAllapot.Value = NotInAllapotok;
                search.Manual_Sajat_TovabbitasAlattAllapot.Operator = Contentum.eQuery.Query.Operators.isnullornotinner;

                search.ReadableWhere = "Saját ";

                if (Search.IsSearchObjectInSession(Page, typeof(EREC_KuldKuldemenyekSearch)) == false)
                {
                    Search.SetSearchObject(Page, search);
                }
            }
        }

        /// Alap szűrési feltétel:
        /// (Nem kell hozni azokat, ahol Postazas_Iranya == Kimeno)
        /// 
        Kuldemenyek.SetDefaultFilter(search);

        search.OrderBy = Search.GetOrderBy("KuldKuldemenyekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = new KRT_MappakSearch();

            search.Extended_KRT_MappakSearch.Id.Value = this.AttachedDosszieTreeView.SelectedId;
            search.Extended_KRT_MappakSearch.Id.Operator = Query.Operators.equals;
        }

        // BUG_8847
        Result res;
        if (Startup == Constants.Startup.FromVezetoiPanel_Kuldemeny)
        {
            res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, false);

            // AddMetaColumnsToGridView(res, IraIratokGridView, true);
        } else
           res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = null;
        }

        //Result res = service.GetAllWithExtension(ExecParam, search);
        UI.GridViewFill(KuldKuldemenyekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void KuldKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetVegyesInfo(e);
        GridView_RowDataBound_SetCsatolasInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_KuldKuldemenyek);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        //tárolt eljárás viszgálja
                        //#region BLG_577
                        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        //{   /*NINCS JOGA*/
                        //    CsatolmanyImage.Visible = false;
                        //    return;
                        //}
                        //#endregion

                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                        else
                        {
                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            if (!String.IsNullOrEmpty(id))
                            {
                                string qsStartup = (String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup));

                                // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                                    + qsStartup
                                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolasCount_elozmeny = String.Empty;
            string csatolasCount_kapcsolt = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolasCount_Elozmeny") && drw["CsatolasCount_Elozmeny"] != null)
            {
                csatolasCount_elozmeny = drw["CsatolasCount_Elozmeny"].ToString();
            }

            if (drw.Row.Table.Columns.Contains("CsatolasCount_Kapcsolt") && drw["CsatolasCount_Kapcsolt"] != null)
            {
                csatolasCount_kapcsolt = drw["CsatolasCount_Kapcsolt"].ToString();
            }

            int count_elozmeny = 0;
            if (!String.IsNullOrEmpty(csatolasCount_elozmeny))
            {
                Int32.TryParse(csatolasCount_elozmeny, out count_elozmeny);
            }
            int count_kapcsolt = 0;
            if (!String.IsNullOrEmpty(csatolasCount_kapcsolt))
            {
                Int32.TryParse(csatolasCount_kapcsolt, out count_kapcsolt);
            }

            int count = count_elozmeny + count_kapcsolt;

            Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

            if (CsatoltImage != null)
            {
                if (count > 0)
                {
                    CsatoltImage.Visible = true;
                    CsatoltImage.ToolTip = String.Format("Kapcsolt: {0} db\n(előzmény: {1} db, következmény: {2} db)", count, count_elozmeny, count_kapcsolt);
                    string qsStartup = (String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup));
                    if (drw["Id"] != null)
                    {
                        string kuldemenyId = drw["Id"].ToString();
                        string onclick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + kuldemenyId +
                        "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolasTab + qsStartup
                         , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID);
                        CsatoltImage.Attributes.Add("onclick", onclick);

                    }
                }
                else
                {
                    CsatoltImage.Visible = false;
                }
            }
        }
    }


    protected void KuldKuldemenyekGridView_PreRender(object sender, EventArgs e)
    {

        UI.GridViewSetScrollable(ListHeader1.Scrollable, KuldemenyekCPE);

        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KuldKuldemenyekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, KuldemenyekCPE);
        KuldKuldemenyekGridViewBind();
    }

    protected void KuldKuldemenyekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KuldKuldemenyekGridView, selectedRowNumber, "check");
            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            string qsStartup = String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup);

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   + qsStartup
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID);
            string tableName = "EREC_KuldKuldemenyek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName + qsStartup
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, KuldKuldemenyekUpdatePanel.ClientID);
            ListHeader1.PrintOnClientClick = "javascript:window.open('KuldKuldemenyekPrintForm.aspx?" + QueryStringVars.ErkeztetokonyvId + "=" + id + "')";

            // jobb oldali gombsor kattintas:
            //ListHeader1.BontasOnClientClick = JavaScripts.SetOnClientClick("Bontas.aspx"
            //    , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
            //    , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, KuldKuldemenyekUpdatePanel.ClientID);

            // Allapot szerint beallitja a headerben a gombok viselkedeset:
            Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(id, Page, EErrorPanel1);
            ErrorDetails errorDetail = null;
            ExecParam execParam = UI.SetExecParamDefault(Page);

            // Iktatas:
            Kuldemenyek.SetIktatas(id, statusz, ListHeader1.BejovoIratIktatas, KuldKuldemenyekUpdatePanel);

            // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEJOVO_IRAT_MUNKAIRATBA_IKTATAS))
            {
                Kuldemenyek.SetIktatasMunkapeldany(id, statusz, ListHeader1.MunkapeldanyIktatasa, KuldKuldemenyekUpdatePanel);
            }

            // Email iktatás megtagadás:
            Kuldemenyek.SetIktatasMegtagadas(id, statusz, ListHeader1.IktatasMegtagadas, KuldKuldemenyekUpdatePanel);

            // Lezaras:
            Kuldemenyek.SetLezaras(id, statusz, ListHeader1.Lezaras, KuldKuldemenyekUpdatePanel);

            //// Átadásra kijelölés:
            //Kuldemenyek.SetAtadasraKijeloles(id, statusz, ListHeader1.AtadasraKijelolImageButton, KuldKuldemenyekUpdatePanel);

            //ExecParam execParam = UI.SetExecParamDefault(Page);
            //ErrorDetails errorDetail = null;

            // Szignálás
            if (statusz != null && Kuldemenyek.Szignalhato(statusz, execParam, out errorDetail))
            {
                ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClick("SzignalasForm.aspx",
                    QueryStringVars.KuldemenyId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.SzignalasOnClientClick = "alert('" + Resources.Error.ErrorCode_52582
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }



            // Átvétel:
            if (statusz != null && Kuldemenyek.Atveheto(execParam, statusz, out errorDetail))
            {
                ListHeader1.AtvetelOnClientClick = "if (confirm('"
                    + Resources.Question.UIConfirmHeader_Atvetel
                    + "')) { } else { return false; }";
            }
            else
            {
                // Nem vehető át:
                ListHeader1.AtvetelOnClientClick = "alert('" + Resources.Error.ErrorCode_52391
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // az esetleg felesleges hívások elkerülése miatt nem végezzük el a teljes ellenőrzést
            //if (Kuldemenyek.CheckVisszakuldhetoWithFunctionRight(Page, statusz, out errorDetail))
            if (statusz != null && Kuldemenyek.Visszakuldheto(execParam, statusz, out errorDetail))
            {
                VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
            }
            else
            {
                // Nem küldhető vissza:
                ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // Módosítás
            if (statusz != null && !Kuldemenyek.Modosithato(statusz, execParam, out errorDetail))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_KuldemenyMegtekintes
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" +
                    JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    + qsStartup
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + qsStartup
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }

            // Csatolás:
            if (statusz != null && Kuldemenyek.Csatolhato(statusz, out errorDetail) == true)
            {
                ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx",
                    QueryStringVars.Id + "=" + id + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromKuldemeny
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.CsatolasOnClientClick = "alert('" + Resources.Error.ErrorCode_55010
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            if (statusz != null && Kuldemenyek.LezarasVisszavonhato(execParam, statusz, out errorDetail))
            {
                ListHeader1.LezarasVisszavonasOnClientClick =
                  "if (confirm('" + Resources.Question.UIConfirmHeader_LezarasVisszavonas + " ')) { } else { return false; }";
            }
            else
            {
                // Lezárás nem vonható vissza:
                ListHeader1.LezarasVisszavonasOnClientClick = "alert('"
                    + Resources.Error.ErrorCode_52781
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            // Sztornozas:
            if (statusz != null && Kuldemenyek.SetSztorno(id, statusz, ListHeader1.Sztorno, KuldKuldemenyekUpdatePanel))
            {
                SztornoPopup.SetOnclientClickShowFunction(ListHeader1.SztornoButton);
            }

            //  ListHeader1.BoritonyomtatasOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?"
            //       + QueryStringVars.KuldemenyId + "=" + id + "&tipus=BoritoA4");

            ListHeader1.BoritonyomtatasOnClientClick = "javascript:window.open('KuldKuldemenyekSSRSBoritoA3.aspx?KuldemenyId=" + id + "&tipus=BoritoA3')";

            //Hianyzik: A3 es A4 nyomtatas + XML export! :-(

        }
    }

    protected void KuldKuldemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KuldKuldemenyekGridViewBind();
                    break;
                case EventArgumentConst.refreshKuldemenyekPanel:
                    KuldKuldemenyekGridViewBind();
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedKuldKuldemenyek();
        //    KuldKuldemenyekGridViewBind();
        //}

        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            List<double> columnSizeList = new List<double>() { 5, 4, 4, 20.6, 14.9, 13.57, 25.86, 14.86, 19.7, 22.29, 11, 24.4, 13, 20.7 };
            ex_Export.SaveGridView_ToExcel(exParam, KuldKuldemenyekGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, columnSizeList, browser);
        }
        if (e.CommandName == CommandName.SzlaExport)
        {
            Contentum.eIntegrator.Service.ServiceFactory sf =
                        Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            INT_PIRService svcPIRService = sf.getINT_PIRService();

            if (!svcPIRService.SetPIR_Rendszer("PROCUSYS"))
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "SetPIR_Rendszer hiba!");
                return;
            }

            Result res = svcPIRService.PirSzamlaExportToCsv(ExecParam);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res, "Számlaexport");
            }
            else
            {
                KuldKuldemenyekGridViewBind();
                string js = @"alert('A számlaexport sikeresen megtörtént!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);

            }


        }
        //bernat.laszlo eddig
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //string js = string.Empty;

        String RecordId = UI.GetGridViewSelectedRecordId(KuldKuldemenyekGridView);
        switch (e.CommandName)
        {
            case CommandName.Lock:
                Kuldemenyek.Zarolas(KuldKuldemenyekGridView, Page, EErrorPanel1, ErrorUpdatePanel);
                KuldKuldemenyekGridViewBind();
                break;

            case CommandName.Unlock:
                Kuldemenyek.Zarolas_feloldasa(KuldKuldemenyekGridView, Page, EErrorPanel1, ErrorUpdatePanel);
                KuldKuldemenyekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedKuldKuldemenyek();
                break;
            //case CommandName.Bontas:
            //    BontasKuldemenyRecords();
            //    KuldKuldemenyekGridViewBind();
            //    break;
            case CommandName.Sztorno:
                Kuldemenyek.Sztorno(RecordId, SztornoPopup.SztornoIndoka, Page, EErrorPanel1);
                KuldKuldemenyekGridViewBind();
                break;
            case CommandName.DossziebaHelyez:
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (string s in ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session["SelectedKuldemenyIds"] = sb.ToString();

                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedIratPeldanyIds"] = null;
                    Session["SelectedDosszieIds"] = null;

                    string js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebaHelyez + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyDossziebaMozgatas", js, true);
                }
                break;
            case CommandName.DossziebolKivesz:
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (string s in ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session["SelectedKuldemenyIds"] = sb.ToString();

                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedIratPeldanyIds"] = null;
                    Session["SelectedDosszieIds"] = null;

                    string js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebolKivesz + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyDossziebaMozgatas", js, true);
                }
                break;

            case CommandName.Atvetel:
                {
                    bool mindigTomegesAtvetel = Rendszerparameterek.GetBoolean(Page, "TOMEGES_ATVETEL_ENABLED", false);

                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(KuldemenyekGridView, "check") == 1
                        && !String.IsNullOrEmpty(RecordId)
                        && lstGridViewSelectedRows.Contains(RecordId) && !mindigTomegesAtvetel)
                    {
                        if (lstGridViewSelectedRows.Count > 0)
                        {
                            ErrorDetails errorDetail;
                            bool atveheto = Kuldemenyek.CheckAtvehetoWithFunctionRight(Page, RecordId, out errorDetail);

                            if (!atveheto)
                            {
                                string js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52391, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAt", js, true);
                            }
                            else
                            {
                                Kuldemenyek.Atvetel(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
                                KuldKuldemenyekGridViewBind();
                            }
                        }
                    }
                    else
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session["SelectedKuldemenyIds"] = sb.ToString();

                        Session["SelectedBarcodeIds"] = null;
                        Session["SelectedUgyiratIds"] = null;
                        Session["SelectedIratPeldanyIds"] = null;
                        Session["SelectedDosszieIds"] = null;
                        //ne okozzon gondot, ha korábban hívtuk a tük visszavétel funkciót
                        Session["TUKVisszavetel"] = null;

                        string js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                    }
                }
                break;
            case CommandName.Visszakuldes:
                {
                    ErrorDetails errorDetail;
                    bool visszakuldheto = Kuldemenyek.CheckVisszakuldhetoWithFunctionRight(Page, RecordId, out errorDetail);

                    if (!visszakuldheto)
                    {
                        string js = String.Format("alert('{0}{1}');", Resources.Error.UINemVisszakuldhetoTetel, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemKuldhetoVissza", js, true);
                    }
                    else
                    {
                        Kuldemenyek.Visszakuldes(RecordId, VisszakuldesPopup.VisszakuldesIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                        KuldKuldemenyekGridViewBind();
                    }
                }
                break;
            case CommandName.BejovoIratIktatas:
                KuldKuldemenyekGridViewBind();
                break;
            case CommandName.IktatasMegtagadas:
                KuldKuldemenyekGridViewBind();
                break;
            case CommandName.Lezaras:
                Kuldemenyek.Lezaras(RecordId, Page, EErrorPanel1);
                KuldKuldemenyekGridViewBind();
                break;
            case CommandName.LezarasVisszavonas:
                #region Lezárás visszavonása
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = RecordId;

                Result result = service.LezarasVisszavonasa(execParam);
                if (result.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
                else
                {
                    KuldKuldemenyekGridViewBind();
                }
                #endregion
                break;
            case CommandName.Boritonyomtatas:
                BoritonyomtatasKuldemenyRecords();
                break;
            case CommandName.AtadasraKijeloles:
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (string s in ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel))
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session["SelectedKuldemenyIds"] = sb.ToString();

                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedIratPeldanyIds"] = null;
                    Session["SelectedDosszieIds"] = null;

                    string js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                }
                break;
            case CommandName.Visszavetel:
                {
                    #region BLG1131Visszavétel
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(KuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
                    foreach (string s in lstGridViewSelectedRows)
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session["SelectedKuldemenyIds"] = sb.ToString();

                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedIratPeldanyIds"] = null;
                    Session["SelectedDosszieIds"] = null;
                    Session["TUKVisszavetel"] = "1";

                    string js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);

                    #endregion
                }
                break;

        }
    }

    //private void AtadasraKijeloles()
    //{
    //    List<String> selectedRecordIds = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
    //    String str_Ids = "";
    //    int index = 0;
    //    foreach (String recordId in selectedRecordIds)
    //    {
    //        if (index == 0)
    //        {
    //            str_Ids += recordId;
    //        }
    //        else
    //        {
    //            str_Ids += "," + recordId;
    //        }
    //        index++;
    //    }

    //    string script = "function OpenAtadasraKijelolesWindow() { ";
    //    script +=
    //        JavaScripts.SetOnClientClick("AtadasForm.aspx", QueryStringVars.KuldemenyId  + "=" + str_Ids
    //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    //    script += "} OpenAtadasraKijelolesWindow(); ";

    //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "AtadasraKijelolesPopup", script, true);

    //}

    //private void BontasKuldemenyRecords()
    //{ 

    //}

    //private void BejovoIratIktatasKuldemenyRecords()
    //{

    //}

    private void BoritonyomtatasKuldemenyRecords()
    {

    }

    //private void deleteSelectedKuldKuldemenyek()
    //{        
    //    if (FunctionRights.GetFunkcioJog(Page, "KuldemenyInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel);
    //        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    private void SendMailSelectedKuldKuldemenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_KuldKuldemenyek");
        }
    }

    protected void KuldKuldemenyekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KuldKuldemenyekGridViewBind(e.SortExpression, UI.GetSortToGridView("KuldKuldemenyekGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region segedFv

    // Segedfv.

    private const string IratIktatoszamEsId_Delimitter = "***";

    protected string GetIktatoszamFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return "";
        }
    }

    protected string GetIdFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    #endregion

    #region BLG_453
    /// <summary>
    /// Tük specifikus oszlopok hozzáadása a gridhez.
    /// </summary>
    private void SetGridTukColumns()
    {
        bool isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);
        if (!isTuk)
            return;

        //BoundField bfMinosites = new BoundField();
        //bfMinosites.DataField = "MinositesNev";
        //bfMinosites.HeaderText = "Minősítés";
        //bfMinosites.SortExpression = "EREC_KuldKuldemenyek.MinositesNev";
        //bfMinosites.HeaderStyle.CssClass = "GridViewBorderHeader";
        //bfMinosites.HeaderStyle.Width = new Unit("80");
        //bfMinosites.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        //bfMinosites.Visible = true;

        ////KuldemenyekGridView.Columns.Add(bfMinosites);
        ////LZS - BUG_7245
        //KuldemenyekGridView.Columns.Insert(5, bfMinosites);
        UI.SetGridViewColumnVisiblity(KuldemenyekGridView, "MinositesNev", true);

        BoundField bfMinositoSzervezet = new BoundField();
        bfMinositoSzervezet.DataField = "MinositoSzervezetNev";
        bfMinositoSzervezet.HeaderText = "Minősítő szervezet";
        bfMinositoSzervezet.SortExpression = "EREC_KuldKuldemenyek.MinositoSzervezet";
        bfMinositoSzervezet.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinositoSzervezet.HeaderStyle.Width = new Unit("80");
        bfMinositoSzervezet.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinositoSzervezet.Visible = false;
        KuldemenyekGridView.Columns.Add(bfMinositoSzervezet);

        BoundField bfMinosito = new BoundField();
        bfMinosito.DataField = "MinositoNev";
        bfMinosito.HeaderText = "Minősítő";
        bfMinosito.SortExpression = "EREC_KuldKuldemenyek.MinositoNev";
        bfMinosito.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinosito.HeaderStyle.Width = new Unit("80");
        bfMinosito.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinosito.Visible = false;
        KuldemenyekGridView.Columns.Add(bfMinosito);

        BoundField bfFutjegyzek = new BoundField();
        bfFutjegyzek.DataField = "FutarJegyzekListaSzama";
        bfFutjegyzek.HeaderText = "Futárjegyzék";
        bfFutjegyzek.SortExpression = "EREC_KuldKuldemenyek.FutarJegyzekListaSzama";
        bfFutjegyzek.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfFutjegyzek.HeaderStyle.Width = new Unit("80");
        bfFutjegyzek.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfFutjegyzek.Visible = false;
        KuldemenyekGridView.Columns.Add(bfFutjegyzek);

        BoundField bfMinErvIdo = new BoundField();
        bfMinErvIdo.DataField = "MinositesErvenyessegiIdeje";
        bfMinErvIdo.HeaderText = "Min.érv.idő";
        bfMinErvIdo.SortExpression = "EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje";
        // BUG_1446
        //bfMinErvIdo.DataFormatString = "{0:yyyy.MM.dd hh:mm}";
        bfMinErvIdo.DataFormatString = "{0:yyyy.MM.dd}";
        bfMinErvIdo.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinErvIdo.HeaderStyle.Width = new Unit("80");
        bfMinErvIdo.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinErvIdo.Visible = false;
        KuldemenyekGridView.Columns.Add(bfMinErvIdo);

        BoundField bfTerjedelem = new BoundField();
        bfTerjedelem.DataField = "TerjedelemMennyiseg";
        bfTerjedelem.HeaderText = "Terjedelem";
        bfTerjedelem.SortExpression = "EREC_KuldKuldemenyek.TerjedelemMennyiseg";
        bfTerjedelem.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfTerjedelem.HeaderStyle.Width = new Unit("80");
        bfTerjedelem.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfTerjedelem.Visible = false;
        KuldemenyekGridView.Columns.Add(bfTerjedelem);

        BoundField bfIrattariHely = new BoundField();
        bfIrattariHely.DataField = "IrattariHely";
        bfIrattariHely.HeaderText = "Fizikai hely";
        bfIrattariHely.SortExpression = "EREC_KuldKuldemenyek.IrattariHely";
        bfIrattariHely.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfIrattariHely.HeaderStyle.Width = new Unit("80");
        bfIrattariHely.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfIrattariHely.Visible = true;
        KuldemenyekGridView.Columns.Add(bfIrattariHely);
    }
    #endregion
}
