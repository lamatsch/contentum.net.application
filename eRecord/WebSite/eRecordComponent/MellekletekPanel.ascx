﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MellekletekPanel.ascx.cs" Inherits="eRecordComponent_MellekletekPanel" %>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
TagPrefix="uc" %>    

<asp:UpdatePanel ID="up" runat="server">
    <ContentTemplate>
    <asp:Panel ID="MainPanel" runat="server">
        <ajaxToolkit:CollapsiblePanelExtender ID="cpeMellekletek" runat="server" TargetControlID="panelMellekletek"
            CollapsedSize="0" Collapsed="true" ExpandControlID="panelCpeHeader" CollapseControlID="panelCpeHeader"
            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
            ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="ibtnMellekletCpe"
            ExpandedSize="0" ExpandedText="Mellékletek elrejt" CollapsedText="Mellékletek mutat"
            TextLabelID="labelMellekletCpeText">
        </ajaxToolkit:CollapsiblePanelExtender>
        <asp:Panel runat="server" ID="panelCpeHeaderWrapper" class="tabExtendableListFejlec" style="text-align: left;">
            <asp:Panel ID="panelCpeHeader" runat="server" Style="display: block; padding-left: 5px;
                cursor: pointer; font-weight: bold;">
                <asp:ImageButton runat="server" ID="ibtnMellekletCpe" ImageUrl="../images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
                <asp:Label ID="labelMellekletCpeText" runat="server" Text="Melléklet"></asp:Label>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="panelMellekletek" runat="server">
            <%--Hiba megjelenites--%>
            <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                    </eUI:eErrorPanel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--/Hiba megjelenites--%>
            <asp:GridView ID="GridViewMellekletek" runat="server" AutoGenerateColumns="false"
                ShowFooter="true" OnRowCommand="GridViewMellekletek_RowCommand" OnRowDataBound="GridViewMellekletek_RowDataBound"
                OnRowDeleting="GridViewMellekletek_RowDeleting" OnDataBound="GridViewMellekletek_DataBound"
                BorderWidth="0px" style="text-align:left">
                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                <HeaderStyle CssClass="GridViewHeaderStyle" />
                <Columns>
                    <asp:TemplateField HeaderText="Adathordozó fajtája">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelAdathordozoTipus" runat="server" Text='<%# Eval("AdathordozoTipusNev") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <uc:KodtarakDropDownList ID="ktdlAdathordozoTipus" runat="server" Width="100px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mennyiség">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelMennyiseg" runat="server" Text='<%# Eval("Mennyiseg") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <uc:RequiredNumberBox ID="requiredNumberBoxMennyiseg" runat="server" Width="100px"
                                Text="1" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Egység">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelMennyisegiEgyseg" runat="server" Text='<%# Eval("MennyisegiEgysegNev") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <uc:KodtarakDropDownList ID="ktdlMennyisegiEgyseg" runat="server" Width="100px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Melléklet adathordozó típusa">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelElsodlegesAdathordozo" runat="server" Text='<%# Eval("MellekletAdathordozoNev") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <uc:KodtarakDropDownList ID="MellekletAdathordozoTipus_DropDownList" runat="server" Width="200px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vonalkód">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelBarCode" runat="server" Text='<%# Eval("BarCode") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <uc:VonalKodTextBox ID="vonalKodTextBoxBarCode" runat="server" Width="150px" 
                            Validate="false" ValidationGroup="Mellekletek"/>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Megjegyzés">
                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                        <ItemStyle CssClass="MellekletGridViewBoundFieldItemStyle" />
                        <ItemTemplate>
                            <asp:Label ID="labelMegjegyzes" runat="server" Text='<%# Eval("Megjegyzes") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMegjegyzes" runat="server" Width="120px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle BackColor="Transparent"/>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnDelete" runat="server"
								ImageUrl="~/images/hu/egyeb/reset_icon.png"
								onmouseover="swapByName(this.id,'reset_icon_keret.png')"
								onmouseout="swapByName(this.id,'reset_icon.png')"								
							 	CommandName="Delete" CausesValidation="false"
							 	ToolTip="Melléklet törlése"
							 	style="vertical-align:middle"/>	    
                        </ItemTemplate>
                        <FooterTemplate>
							 	<asp:ImageButton ID="ibtnInsert" runat="server"
								ImageUrl="~/images/hu/lov/hozzaad.gif"
								onmouseover="swapByName(this.id,'hozzaad_keret.gif')"
								onmouseout="swapByName(this.id,'hozzaad.gif')"								
							 	CommandName="Insert" CausesValidation="true" ValidationGroup="Mellekletek"
							 	ToolTip="Melléklet hozzáadása"
							 	style="vertical-align:middle"/> 	
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
     </asp:Panel>    
    </ContentTemplate>
</asp:UpdatePanel>
