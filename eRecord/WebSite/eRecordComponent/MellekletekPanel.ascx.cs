﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class eRecordComponent_MellekletekPanel : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    private const string KodCsoportAdathordozoTipus = "ADATHORDOZO_TIPUSA";
    private const string KodCsoportMennyisegiEgyseg = "MENNYISEGI_EGYSEG";

    //bernat.laszlo added
    private const string KodCsoportElsodlegesAdathordozo = "ELSODLEGES_ADATHORDOZO";
    //bernat.laszlo eddig

    [Serializable]
    private class Melleklet
    {
        private int id = 0;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string adathordozoTipus;

        public string AdathordozoTipus
        {
            get { return adathordozoTipus; }
            set { adathordozoTipus = value; }
        }

        private string adathordozoTipusNev;

        public string AdathordozoTipusNev
        {
            get { return adathordozoTipusNev; }
            set { adathordozoTipusNev = value; }
        }

        private string mennyiseg;

        public string Mennyiseg
        {
            get { return mennyiseg; }
            set { mennyiseg = value; }
        }

        private string mennyisegiEgyseg;

        public string MennyisegiEgyseg
        {
            get { return mennyisegiEgyseg; }
            set { mennyisegiEgyseg = value; }
        }

        private string mennyisegiEgysegNev;

        public string MennyisegiEgysegNev
        {
            get { return mennyisegiEgysegNev; }
            set { mennyisegiEgysegNev = value; }
        }

        private string mellekletAdathordozoTipus;

        public string MellekletAdathordozoTipus
        {
            get { return mellekletAdathordozoTipus; }
            set { mellekletAdathordozoTipus = value; }
        }

        private string mellekletAdathordozoNev;

        public string MellekletAdathordozoNev
        {
            get { return mellekletAdathordozoNev; }
            set { mellekletAdathordozoNev = value; }
        }

        private string barCode;

        public string BarCode
        {
            get { return barCode; }
            set { barCode = value; }
        }

        private string megjegyzes;

        public string Megjegyzes
        {
            get { return megjegyzes; }
            set { megjegyzes = value; }
        }

        public static Melleklet Empty
        {
            get
            {
                Melleklet emptyMelleklet = new Melleklet();
                emptyMelleklet.Id = -1;
                return emptyMelleklet;
            }
        }
    }

    private List<Melleklet> _insertedMellekletek
    {
        get
        {
            object o = ViewState["_insertedMellekletek"];
            if (o == null)
                o = ViewState["_insertedMellekletek"] = new List<Melleklet>();

            return o as List<Melleklet>;
        }
        set
        {
            Session["ViewState"] = value;
        }
    }

    public int MellekletekSzama
    {
        get
        {
            if (_insertedMellekletek == null)
                return 0;

            return Math.Max(_insertedMellekletek.Count - 1, 0);
        }
    }

    public List<EREC_Mellekletek> GetMellekletek()
    {
        if (_insertedMellekletek == null || _insertedMellekletek.Count <2)
            return null;

        List<EREC_Mellekletek> mellekletekList = new List<EREC_Mellekletek>();

        foreach (Melleklet melleklet in _insertedMellekletek)
        {
            if (melleklet.Id > -1)
            {
                EREC_Mellekletek erec_Melleklet = new EREC_Mellekletek();
                erec_Melleklet.AdathordozoTipus = melleklet.AdathordozoTipus;
                erec_Melleklet.Mennyiseg = melleklet.Mennyiseg;
                erec_Melleklet.MennyisegiEgyseg = melleklet.MennyisegiEgyseg;
                erec_Melleklet.BarCode = melleklet.BarCode;
                erec_Melleklet.MellekletAdathordozoTipus = melleklet.MellekletAdathordozoTipus;
                erec_Melleklet.Megjegyzes = melleklet.Megjegyzes;
                mellekletekList.Add(erec_Melleklet);
            }
        }

        return mellekletekList;
    }

    public void Clear()
    {
        _insertedMellekletek = null;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _insertedMellekletek = new List<Melleklet>();
            _insertedMellekletek.Add(Melleklet.Empty);
            DataBindMellekletek();
        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

    }

    private void Page_PreRender(object sender, EventArgs e)
    {
        cpeMellekletek.ExpandedText = String.Format("Mellékletek ({0}) elrejt", MellekletekSzama);
        cpeMellekletek.CollapsedText = String.Format("Mellékletek ({0}) mutat", MellekletekSzama);
    }

    private void DataBindMellekletek()
    {
        GridViewMellekletek.DataSource = _insertedMellekletek;
        GridViewMellekletek.DataBind();
    }

    protected void GridViewMellekletek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Insert")
        {
            if (this.InsertMelleklet())
            {
                DataBindMellekletek();
            }
        }

    }

    private bool InsertMelleklet()
    {
        try
        {
            Component_KodtarakDropDownList ktdlAdathordozoTipus = GridViewMellekletek.FooterRow.FindControl("ktdlAdathordozoTipus") as Component_KodtarakDropDownList;
            Component_RequiredNumberBox requiredNumberBoxMennyiseg = GridViewMellekletek.FooterRow.FindControl("requiredNumberBoxMennyiseg") as Component_RequiredNumberBox;
            Component_KodtarakDropDownList ktdlMennyisegiEgyseg = GridViewMellekletek.FooterRow.FindControl("ktdlMennyisegiEgyseg") as Component_KodtarakDropDownList;
            Component_KodtarakDropDownList MellekletAdathordozoTipus_DropDownList = GridViewMellekletek.FooterRow.FindControl("MellekletAdathordozoTipus_DropDownList") as Component_KodtarakDropDownList;
            eRecordComponent_VonalKodTextBox vonalKodTextBoxBarCode = GridViewMellekletek.FooterRow.FindControl("vonalKodTextBoxBarCode") as eRecordComponent_VonalKodTextBox;
            TextBox txtMegjegyzes = GridViewMellekletek.FooterRow.FindControl("txtMegjegyzes") as TextBox;
            Melleklet newMelleklet = new Melleklet();
            newMelleklet.AdathordozoTipus = ktdlAdathordozoTipus.SelectedValue;
            newMelleklet.AdathordozoTipusNev = ktdlAdathordozoTipus.Text;
            newMelleklet.Mennyiseg = requiredNumberBoxMennyiseg.Text;
            newMelleklet.MennyisegiEgyseg = ktdlMennyisegiEgyseg.SelectedValue;
            newMelleklet.MennyisegiEgysegNev = ktdlMennyisegiEgyseg.Text;
            newMelleklet.BarCode = vonalKodTextBoxBarCode.Text;
            newMelleklet.Megjegyzes = txtMegjegyzes.Text;
            newMelleklet.MellekletAdathordozoTipus = MellekletAdathordozoTipus_DropDownList.SelectedValue;
            newMelleklet.MellekletAdathordozoNev = MellekletAdathordozoTipus_DropDownList.Text;

            if (CheckBarCode(newMelleklet.BarCode))
            {
                _insertedMellekletek.Add(newMelleklet);
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
            return false;
        }

        return true;
    }

    private bool CheckBarCode(string barCode)
    {
        if (!String.IsNullOrEmpty(barCode))
        {
            foreach (Melleklet melleklet in _insertedMellekletek)
            {
                if (!String.IsNullOrEmpty(melleklet.BarCode))
                {
                    if (melleklet.BarCode == barCode)
                    {
                        Result resErrorUsedBarCode = ResultError.CreateNewResultWithErrorCode(52482);
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resErrorUsedBarCode);
                        return false;
                    }
                }
            }
            //Vonalkód ellenőrzése
            KRT_BarkodokService srvBarCode = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam xpmBarCode = UI.SetExecParamDefault(Page);
            Result resBarCode = srvBarCode.CheckBarcode(xpmBarCode, barCode);

            if (resBarCode.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resBarCode);
                return false;
            }
        }

        return true;
    }

    protected void GridViewMellekletek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Melleklet melleklet = e.Row.DataItem as Melleklet;

            if (melleklet.Id == -1)
            {
                e.Row.Visible = false;
            }
        }
    }

    protected void GridViewMellekletek_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (this.DeleteMelleklet(e.RowIndex))
        {
            DataBindMellekletek();
        }
    }

    private bool DeleteMelleklet(int index)
    {
        try
        {
            _insertedMellekletek.RemoveAt(index);
        }
        catch (Exception ex)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
        }
        return true;
    }

    protected void GridViewMellekletek_DataBound(object sender, EventArgs e)
    {
        try
        {
            Component_KodtarakDropDownList ktdlAdathordozoTipus = GridViewMellekletek.FooterRow.FindControl("ktdlAdathordozoTipus") as Component_KodtarakDropDownList;
            ktdlAdathordozoTipus.FillDropDownList(KodCsoportAdathordozoTipus, EErrorPanel1);

            Component_KodtarakDropDownList MellekletAdathordozoTipus_DropDownList = GridViewMellekletek.FooterRow.FindControl("MellekletAdathordozoTipus_DropDownList") as Component_KodtarakDropDownList;
            MellekletAdathordozoTipus_DropDownList.FillDropDownList(KodCsoportElsodlegesAdathordozo, EErrorPanel1);

            List<string> notMellekletTipus = new List<string>();
            notMellekletTipus.Add(KodTarak.AdathordozoTipus.Boritek);
            notMellekletTipus.Add(KodTarak.AdathordozoTipus.PapirAlapu);
            foreach (string adathordozoTipus in notMellekletTipus)
            {
                ListItem item = ktdlAdathordozoTipus.DropDownList.Items.FindByValue(adathordozoTipus);
                if (item != null)
                {
                    ktdlAdathordozoTipus.DropDownList.Items.Remove(item);
                }
            }

            Component_KodtarakDropDownList ktdlMennyisegiEgyseg = GridViewMellekletek.FooterRow.FindControl("ktdlMennyisegiEgyseg") as Component_KodtarakDropDownList;
            ktdlMennyisegiEgyseg.FillDropDownList(KodCsoportMennyisegiEgyseg, EErrorPanel1);
        }
        catch (Exception ex)
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
        }
    }

    #region ISelectableUserComponentContainer Members

    public List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        cpeMellekletek.Enabled = false;
        cpeMellekletek.Collapsed = false;
        componentList.Add(MainPanel);
        return componentList;
    }

    #endregion
}
