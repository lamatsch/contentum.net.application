<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MellekletekTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratMellekletekTab" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc3" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="../eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="MellekletekUpdatePanel" runat="server" OnLoad="MellekletekUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <ajaxToolkit:CollapsiblePanelExtender ID="MellekletekCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" AutoCollapse="false" AutoExpand="false"
                ExpandedSize="200" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <%-- TabHeader --%>
                        <uc1:TabHeader ID="TabHeader1" runat="server" />
                        <%-- /TabHeader --%>
                        <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
                            <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponentMellekletek" />
                            <asp:GridView ID="MellekletekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                                OnRowCommand="MellekletekGridView_RowCommand" OnRowDataBound="MellekletekGridView_RowDataBound"
                                OnSorting="MellekletekGridView_Sorting">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <HeaderTemplate>
                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                CssClass="HideCheckBoxText" />
                                            <%--'<%# Eval("Id") %>'--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="AdathordozoTipusNev" HeaderText="Mell�kletek (adathordoz�) fajt�ja"
                                        SortExpression="AdathordozoTipusNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Mennyiseg" HeaderText="Mennyis�g" SortExpression="Mennyiseg">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MennyisegiEgysegNev" HeaderText="Egys�g" SortExpression="MennyisegiEgysegNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MellekletAdathordozoTipus" HeaderText="Mell�klet adathordoz� t�pusa" SortExpression="MellekletAdathordozoTipus">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyz�s" SortExpression="Megjegyzes">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d" SortExpression="BarCode">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBorder" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <ItemTemplate>
                                            <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="SubListPanel" runat="server">
                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="0">
                                <ajaxToolkit:TabPanel ID="CsatolmanyokTabPanel" runat="server" TabIndex="0" HeaderText="Mell�klet csatolm�nyai">
                                    <ContentTemplate>
                                    <asp:UpdatePanel ID="CsatolmanyokUpdatePanel" runat="server" OnLoad="CsatolmanyokUpdatePanel_Load">
                                                            <ContentTemplate>
                                        <asp:Panel ID="CsatolmanyokPanel" runat="server" Visible="false" Width="100%">
                                            <uc1:SubListHeader ID="CsatolmanyokSubListHeader" runat="server" />
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="CsatolmanyokCPE" runat="server" TargetControlID="Panel2"
                                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                            AutoExpand="false" ExpandedSize="0">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <asp:Panel ID="Panel2" runat="server">
                                                            <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponentCsatolmanyok" />
                                                            <asp:GridView ID="CsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" OnRowCommand="CsatolmanyokGridView_RowCommand"
                                                                OnRowDataBound="CsatolmanyokGridView_RowDataBound"
                                                                OnSorting="CsatolmanyokGridView_Sorting" OnPreRender="CsatolmanyokGridView_PreRender" Width="100%">
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                        <HeaderTemplate>
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;&nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                                                CssClass="HideCheckBoxText" />
                                                                            <%--'<%# Eval("Id") %>'--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                                    </asp:CommandField>
                                                                    <asp:BoundField DataField="FajlNev" HeaderText="�llom�ny" SortExpression="FajlNev">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="" SortExpression="KRT_Dokumentumok.Formatum">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="0px" />
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                                                                ImageUrl="images/hu/fileicons/default.gif" ToolTip="Megnyit�s" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="VerzioJel" HeaderText="Verzi�" SortExpression="VerzioJel">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Formatum" HeaderText="Form�tum" SortExpression="Formatum">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                     <asp:TemplateField>
                                                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="labelExternalLink" runat="server" Text='<%#Eval("External_Link")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                                    
                                                                    
                                                                    <%--  Begin: Megnyithat�, Olvashat�, Elektronikus al��r�s --%>
                                                                    <%--<asp:TemplateField>
						                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
						                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelMegnyithato" Text="Megnyithat�" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:CheckBox ID="cbMegnyithato" runat="server" AutoPostBack="false" Text='<%# Eval("Megnyithato") %>' Enabled="false" CssClass="HideCheckBoxText" />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>
					                                                    <asp:TemplateField>
						                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
						                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelOlvashato" Text="Olvashat�" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:CheckBox ID="cbOlvashato" runat="server" AutoPostBack="false" Text='<%# Eval("Olvashato") %>' Enabled="false" CssClass="HideCheckBoxText" />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>--%>
                                                                    <asp:BoundField DataField="ElektronikusAlairasNev" HeaderText="Elektronikus al��r�s"
                                                                        SortExpression="ElektronikusAlairasNev">
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                        <ItemStyle CssClass="GridViewBorder" />
                                                                    </asp:BoundField>
                                                                    <%--<asp:BoundField DataField="AlairasFelulvizsgalat" HeaderText="Ellen�rizve" SortExpression="AlairasFelulvizsgalat">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                            <ItemStyle CssClass="GridViewBorder" />
                                                                        </asp:BoundField>--%>
                                                                    <%--<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                                        <HeaderTemplate>
                                                                            <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                </Columns>
                                                                <PagerSettings Visible="False" />
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </asp:Panel>
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                            </ajaxToolkit:TabContainer>
                        </asp:Panel>
                        <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;" class="mrUrlapCaption">
                                        <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text="Mell�kletek (adathordoz�) fajt�ja:"></asp:Label>
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ktddl:KodtarakDropDownList ID="AdathordozoTipusKodtarakDropDownList" runat="server" />
                                        <uc3:RequiredNumberBox ID="MennyisegTextBox" runat="server" Text="1" />
                                        <ktddl:KodtarakDropDownList ID="MennyisegiEgysegKodtarakDropDownList" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;" class="mrUrlapCaption">
                                        <asp:Label ID="Label150" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="MellekletTipus_Label" runat="server" Text="Mell�kletek adathordoz� t�pusa:"></asp:Label>
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ktddl:KodtarakDropDownList ID="MellekletAdathordozoTipus_DropdownList" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;" class="mrUrlapCaption">
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label>
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <uc4:VonalKodTextBox ID="VonalkodTextBox1" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td style="text-align: right; vertical-align: middle;" class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" Text="Megjegyz�s:"></asp:Label>
                                    </td>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:TextBox ID="MegjegyzesTextBox" runat="server" Width="595" />
                                    </td>
                                </tr>
                            </table>
                            <%-- TabFooter --%>
                            <uc2:TabFooter ID="TabFooter1" runat="server" />
                            <%-- /TabFooter --%>
                        </eUI:eFormPanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
