using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_UgyiratMellekletekTab : System.Web.UI.UserControl
{
    private const string KodCsoportAdathordozoTipus = "ADATHORDOZO_TIPUSA";
    private const string KodCsoportMennyisegiEgyseg = "MENNYISEGI_EGYSEG";
    private const string Funkcio_CsatolmanyMellekletKapcsolatNew = "CsatolmanyMellekletKapcsolatNew";
    private const string Funkcio_CsatolmanyMellekletKapcsolatInvalidate = "CsatolmanyMellekletKapcsolatInvalidate";

    //bernat.laszlo added
    private const string KodCsoportElsodlegesAdathordozo = "ELSODLEGES_ADATHORDOZO";
    //bernat.laszlo eddig

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }



    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private bool hasRight_Csatolmanyok = true;

    public Label TabHeader = new Label();

    #region public Properties
    
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion

    #region Base Page

    // TODO: Funkci�jogosults�gok

    protected void Page_Init(object sender, EventArgs e)
    {        
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);
        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        CsatolmanyokSubListHeader.AttachedGridView = CsatolmanyokGridView;
        CsatolmanyokSubListHeader.ButtonsClick += new CommandEventHandler(CsatolmanyokSubListHeader_ButtonsClick);
        CsatolmanyokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsatolmanyokSubListHeader_ErvenyessegFilter_Changed);
        CsatolmanyokSubListHeader.RowCount_Changed += new EventHandler(CsatolmanyokSubListHeader_RowCount_Changed);
               
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.New);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.Invalidate);
        //TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.View);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + CommandName.ElosztoLista);

        CsatolmanyokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatNew);
        CsatolmanyokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatInvalidate);

        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            CsatolmanyokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyCsatolmany" + CommandName.View);
        }
        else if (ParentForm == Constants.ParentForms.IraIrat
            || ParentForm == Constants.ParentForms.IratPeldany)
        {
            CsatolmanyokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.View);
        }
        else
        {
            CsatolmanyokSubListHeader.ViewEnabled = false;
        }

        if (IsPostBack)
        {
            string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MellekletekGridView);
            RefreshOnClientClicksByGridViewSelectedRow(MasterListSelectedRowId);
            
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                // subList ki�r�t�se
                ui.GridViewClear(CsatolmanyokGridView);
            }
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(MellekletekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsatolmanyokGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        //TODO:
        // Csak azokat a mezoket fogja ellenorizni a Save gomb, amik ebbe a csoportba tartoznak:        
        TabFooter1.SaveValidationGroup = "Melleklet";
        MennyisegTextBox.ValidationGroup = TabFooter1.SaveValidationGroup;
        MennyisegTextBox.ValidatorCalloutExtender.Enabled = false;        
    }

    protected void MellekletekUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;


        if (Command != CommandName.New)
        {
            //if (ViewState["HasRight_Csatolmanyok"] == null)
            //{
                // jog a csatolm�nyok megtekint�s�re: "bizalmas" iratn�l csak az �rz� �s vezet�je
                switch (_ParentForm)
                {
                    case Constants.ParentForms.Kuldemeny:
                        hasRight_Csatolmanyok = Kuldemenyek.CheckRights_Csatolmanyok(Page, ParentId, false);
                        break;
                    case Constants.ParentForms.IraIrat:
                        hasRight_Csatolmanyok = Iratok.CheckRights_Csatolmanyok(Page, ParentId, false);
                        break;
                    case Constants.ParentForms.IratPeldany:
                        hasRight_Csatolmanyok = IratPeldanyok.CheckRights_Csatolmanyok(Page, ParentId, false);
                        break;
                }

            //    ViewState["HasRight_Csatolmanyok"] = hasRight_Csatolmanyok;
            //}
            //else
            //{
            //    hasRight_Csatolmanyok = (Boolean)ViewState["HasRight_Csatolmanyok"];
            //}
        }

        // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
        TabHeader1.ViewVisible = false;
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;

        TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(MellekletekGridView.ClientID);
        //TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        CsatolmanyokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsatolmanyokSubListHeader.ModifyVisible = false;
        CsatolmanyokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsatolmanyokGridView.ClientID);     
        
        // View m�dban bizonyos gombok elt�ntet�se:

        if (Request.QueryString.Get(QueryStringVars.Command) == CommandName.View)
        {
            CsatolmanyokSubListHeader.NewVisible = false;
            CsatolmanyokSubListHeader.InvalidateVisible = false;
        }

        ClearForm();
        EFormPanel1.Visible = false;
        SubListPanel.Visible = hasRight_Csatolmanyok;

        MellekletekGridViewBind();
        pageView.SetViewOnPage(Command);
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        AdathordozoTipusKodtarakDropDownList.FillDropDownList(KodCsoportAdathordozoTipus, EErrorPanel1);
        MennyisegiEgysegKodtarakDropDownList.FillDropDownList(KodCsoportMennyisegiEgyseg, EErrorPanel1);
        MellekletAdathordozoTipus_DropdownList.FillAndSetEmptyValue(KodCsoportElsodlegesAdathordozo, EErrorPanel1);

        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
            // Als� list�t elt�ntetj�k, mert kev�s a hely
            SubListPanel.Visible = false;

            VonalkodTextBox1.ReadOnly = false;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;
            SubListPanel.Visible = false;

            String RecordId = UI.GetGridViewSelectedRecordId(MellekletekGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;
                Result result = null;

                if (ParentForm == Constants.ParentForms.Kuldemeny || ParentForm == Constants.ParentForms.IraIrat)
                {
                    EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();
                    result = service.Get(execParam);
                }

                if (String.IsNullOrEmpty(result.ErrorCode))                    
                {
                    LoadComponentsFromBusinessObject((EREC_Mellekletek) result.Record);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedMellekletek();
            MellekletekGridViewBind();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, ParentForm + "Melleklet" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        SubListPanel.Visible = hasRight_Csatolmanyok;
                        break;
                    case CommandName.New:
                        {
                            Result result = null;
                            if (ParentForm == Constants.ParentForms.Kuldemeny || ParentForm == Constants.ParentForms.IraIrat)
                            {
                                EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();

                                EREC_Mellekletek erec_Mellekletek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                result = service.Insert(execParam, erec_Mellekletek);
                            }
                            
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ReLoadTab();
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = UI.GetGridViewSelectedRecordId(MellekletekGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                Result result = null;
                                if (ParentForm == Constants.ParentForms.Kuldemeny || ParentForm == Constants.ParentForms.IraIrat)
                                {
                                    EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();
                                    EREC_Mellekletek erec_Mellekletek = (EREC_Mellekletek)GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    result = service.Update(execParam, erec_Mellekletek);
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
            SubListPanel.Visible = hasRight_Csatolmanyok;
        }
    }

    private void ClearForm()
    {
        if (AdathordozoTipusKodtarakDropDownList.DropDownList.Items.Count > 0)
            AdathordozoTipusKodtarakDropDownList.DropDownList.SelectedIndex = 0;
        MennyisegTextBox.Text = "1";
        if (MennyisegiEgysegKodtarakDropDownList.DropDownList.Items.Count > 0)
            MennyisegiEgysegKodtarakDropDownList.DropDownList.SelectedIndex = 0;
        if (MellekletAdathordozoTipus_DropdownList.DropDownList.Items.Count > 0)
            MellekletAdathordozoTipus_DropdownList.DropDownList.SelectedIndex = 0;
        MegjegyzesTextBox.Text = "";
        VonalkodTextBox1.Text = "";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_Mellekletek erec_Mellekletek)
    {
        AdathordozoTipusKodtarakDropDownList.SetSelectedValue(erec_Mellekletek.AdathordozoTipus);
        MennyisegTextBox.Text = erec_Mellekletek.Mennyiseg;
        MennyisegiEgysegKodtarakDropDownList.SetSelectedValue(erec_Mellekletek.MennyisegiEgyseg);
        MellekletAdathordozoTipus_DropdownList.SetSelectedValue(erec_Mellekletek.MellekletAdathordozoTipus);
        MegjegyzesTextBox.Text = erec_Mellekletek.Megjegyzes;
        VonalkodTextBox1.Text = erec_Mellekletek.BarCode;

        Record_Ver_HiddenField.Value = erec_Mellekletek.Base.Ver;

        if (Command == CommandName.View)
        {
            AdathordozoTipusKodtarakDropDownList.Enabled = false;
            MennyisegTextBox.ReadOnly = true;
            MennyisegiEgysegKodtarakDropDownList.Enabled = false;
            MellekletAdathordozoTipus_DropdownList.Enabled = false;
            MegjegyzesTextBox.ReadOnly = true;
            VonalkodTextBox1.ReadOnly = true;
        }
        else
        {
            AdathordozoTipusKodtarakDropDownList.Enabled = true;
            MennyisegTextBox.ReadOnly = false;
            MennyisegiEgysegKodtarakDropDownList.Enabled = true;
            MellekletAdathordozoTipus_DropdownList.Enabled = true;
            MegjegyzesTextBox.ReadOnly = false;
            VonalkodTextBox1.ReadOnly = false;
        }
        
        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
                // vonalk�d m�dos�that�s�g�nak ellen�rz�se
                EREC_KuldKuldemenyekService kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam kuldemenyekExecParam = UI.SetExecParamDefault(Page,new ExecParam());
                kuldemenyekExecParam.Record_Id = ParentId;
                Result kuldemenyekResult = kuldemenyekService.Get(kuldemenyekExecParam);
                if (!string.IsNullOrEmpty(kuldemenyekResult.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kuldemenyekResult);
                    ErrorUpdatePanel1.Update();
                }

                VonalkodTextBox1.ReadOnly = !IsBarcodeModifiable((EREC_KuldKuldemenyek)kuldemenyekResult.Record, erec_Mellekletek);            
        }
        
    }

    // form --> business object
    private EREC_Mellekletek GetBusinessObjectFromComponents()
    {
        EREC_Mellekletek erec_Mellekletek = new EREC_Mellekletek();
        erec_Mellekletek.Updated.SetValueAll(false);
        erec_Mellekletek.Base.Updated.SetValueAll(false);

        if (ParentForm == Constants.ParentForms.Kuldemeny)
        {
            erec_Mellekletek.KuldKuldemeny_Id = ParentId;
            erec_Mellekletek.Updated.KuldKuldemeny_Id = true;
        }
        else if (ParentForm == Constants.ParentForms.IraIrat)
        {
            erec_Mellekletek.IraIrat_Id = ParentId;
            erec_Mellekletek.Updated.IraIrat_Id = true;
        }
        else
        {
            return null;
        }

        erec_Mellekletek.AdathordozoTipus = AdathordozoTipusKodtarakDropDownList.SelectedValue;
        erec_Mellekletek.Updated.AdathordozoTipus = pageView.GetUpdatedByView(AdathordozoTipusKodtarakDropDownList);

        erec_Mellekletek.Mennyiseg = MennyisegTextBox.Text;
        erec_Mellekletek.Updated.Mennyiseg = pageView.GetUpdatedByView(MennyisegTextBox);

        erec_Mellekletek.MennyisegiEgyseg = MennyisegiEgysegKodtarakDropDownList.SelectedValue;
        erec_Mellekletek.Updated.MennyisegiEgyseg = pageView.GetUpdatedByView(MennyisegiEgysegKodtarakDropDownList);

        erec_Mellekletek.MellekletAdathordozoTipus = MellekletAdathordozoTipus_DropdownList.SelectedValue;
        erec_Mellekletek.Updated.MellekletAdathordozoTipus = pageView.GetUpdatedByView(MellekletAdathordozoTipus_DropdownList);

        erec_Mellekletek.Megjegyzes = MegjegyzesTextBox.Text;
        erec_Mellekletek.Updated.Megjegyzes = pageView.GetUpdatedByView(MegjegyzesTextBox);

        erec_Mellekletek.BarCode = VonalkodTextBox1.Text;
        erec_Mellekletek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBox1);

        erec_Mellekletek.Base.Ver = Record_Ver_HiddenField.Value;
        erec_Mellekletek.Base.Updated.Ver = true;

        return erec_Mellekletek;
    }

    private bool IsBarcodeModifiable(EREC_KuldKuldemenyek kuldemeny, EREC_Mellekletek melleklet)
    {
        return (kuldemeny.BarCode != melleklet.BarCode);
    }

    #endregion

    #region List
    // mell�kletek m�dosul�sa (felv�tel, t�rl�s) ut�ni �jrak�t�shez
    private void MellekletekGridViewBind_WithReselectRow()
    {
        #region kiv�laszott sor megjegyz�se
        int selectedIndex = MellekletekGridView.SelectedIndex;
        string dataKey = String.Empty;
        if (selectedIndex > -1 && MellekletekGridView.DataKeys.Count > selectedIndex)
        {
            dataKey = MellekletekGridView.DataKeys[selectedIndex].Value.ToString();
        }
        #endregion kiv�laszott sor megjegyz�se

        MellekletekGridViewBind();

        #region sor �jra kiv�laszt�sa
        if (MellekletekGridView.Rows.Count > selectedIndex)
        {
            if (selectedIndex > -1 && MellekletekGridView.DataKeys.Count > selectedIndex)
            {
                if (dataKey == MellekletekGridView.DataKeys[selectedIndex].Value.ToString())
                {
                    MellekletekGridView.SelectedIndex = selectedIndex;
                    UI.SetGridViewCheckBoxesToSelectedRow(MellekletekGridView, selectedIndex, "check");
                }
            }
        }
        #endregion sor �jra kiv�laszt�sa
    }

    protected void MellekletekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MellekletekGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MellekletekGridView", ViewState);
        MellekletekGridViewBind(sortExpression, sortDirection);
    }

    protected void MellekletekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponentMellekletek.ClearDokumentElements();

        UI.ClearGridViewRowSelection(MellekletekGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result res = null;

        EREC_MellekletekService erec_MellekletekService = eRecordService.ServiceFactory.GetEREC_MellekletekService();
        EREC_MellekletekSearch erec_MellekletekSearch = (EREC_MellekletekSearch)Search.GetSearchObject(Page, new EREC_MellekletekSearch());

        if (ParentForm.Equals(Constants.ParentForms.Kuldemeny))
        {
            erec_MellekletekSearch.KuldKuldemeny_Id.Value = ParentId;
            erec_MellekletekSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        }
        else if (ParentForm.Equals(Constants.ParentForms.IraIrat))
        {
            erec_MellekletekSearch.IraIrat_Id.Value = ParentId;
            erec_MellekletekSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        }
        else
        {
            return;
        }

        erec_MellekletekSearch.OrderBy = Search.GetOrderBy("MellekletekGridView", ViewState, SortExpression, SortDirection);
        res = erec_MellekletekService.GetAllWithExtension(ExecParam, erec_MellekletekSearch);

        ui.GridViewFill(MellekletekGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";

            string queryString = QueryStringVars.MellekletId + "=" + id;
            if (ParentForm == Constants.ParentForms.Kuldemeny)
            {
                queryString += "&" + QueryStringVars.KuldemenyId + "=" + ParentId;
            }
            else if (ParentForm == Constants.ParentForms.IraIrat)
            {
                queryString += "&" + QueryStringVars.IratId + "=" + ParentId;
            }

            CsatolmanyokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("CsatolmanyMellekletKapcsolatForm.aspx"
                , queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            if (CsatolmanyokGridView.SelectedRow != null)
            {
                Label labelExternalLink = (Label)CsatolmanyokGridView.SelectedRow.FindControl("labelExternalLink");
                if (labelExternalLink != null)
                {
                    CsatolmanyokSubListHeader.ViewOnClientClick = "window.open('" + labelExternalLink.Text + "'); return false;";
                }
            }
            else
            {
                CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }
        }
    }

    protected void MellekletekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MellekletekGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            SubListPanel.Visible = hasRight_Csatolmanyok;
            if (hasRight_Csatolmanyok)
            {
                ActiveTabRefresh(TabContainer1, id);
            }
        }
    }

    /// <summary>
    /// T�rli a MellekletekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedMellekletek()
    {
        if (FunctionRights.GetFunkcioJog(Page, ParentForm + "MellekletInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(MellekletekGridView, EErrorPanel1, ErrorUpdatePanel1);
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = null;

            if (ParentForm == Constants.ParentForms.Kuldemeny || ParentForm == Constants.ParentForms.IraIrat)
            {
                EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();
                result = service.MultiInvalidate(execParams);
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void MellekletekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        GridView_RowDataBound_SetCsatolmanyImage(e);
    }

    private void GridView_RowDataBound_SetCsatolmanyImage(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        #region BLG_577
                        if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        {   /*NINCS JOGA*/
                            CsatolmanyImage.Visible = false;
                            return;
                        }
                        #endregion

                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.AlternateText = CsatolmanyImage.ToolTip;

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.Attributes.Add("onclick", String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]));
                            DokumentumVizualizerComponentMellekletek.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }

                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    protected void MellekletekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MellekletekGridViewBind(e.SortExpression, UI.GetSortToGridView("MellekletekGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region Detail Tab



    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string mellekletId)
    {        
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsatolmanyokGridViewBind(mellekletId);
                CsatolmanyokPanel.Visible = true;                
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsatolmanyokSubListHeader.RowCount = RowCount;        
    }


    #endregion


    #region Csatolm�nyok Sublist

    private void CsatolmanyokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IratelemKapcsolatok();
            CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
            // mell�kleteket is friss�teni kell a csatolm�nyokra vonatkoz� inf�k miatt
            MellekletekGridViewBind_WithReselectRow();
        }
    }

    protected void CsatolmanyokGridViewBind(string mellekletId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolmanyokGridView", ViewState, "Letrehozasido");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolmanyokGridView", ViewState, SortDirection.Descending);

        CsatolmanyokGridViewBind(mellekletId, sortExpression, sortDirection);
    }

    protected void CsatolmanyokGridViewBind(string mellekletId, String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponentCsatolmanyok.ClearDokumentElements();

        if (!string.IsNullOrEmpty(mellekletId))
        {
            EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratelemKapcsolatokSearch search = new EREC_IratelemKapcsolatokSearch();

            search.Melleklet_Id.Value = mellekletId;
            search.Melleklet_Id.Operator = Query.Operators.equals;
            
            search.OrderBy = Search.GetOrderBy("CsatolmanyokGridView", ViewState, SortExpression, SortDirection);

            CsatolmanyokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(execParam, search);

            UI.GridViewFill(CsatolmanyokGridView, res, CsatolmanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel1);
            UI.SetTabHeaderRowCountText(res, CsatolmanyokTabPanel);
        }
        else
        {
            ui.GridViewClear(CsatolmanyokGridView);
        }
    }


    void CsatolmanyokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
    }

    protected void CsatolmanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainer1.ActiveTab.Equals(CsatolmanyokTabPanel))
                    {
                        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
                        // mell�kleteket is friss�teni kell a csatolm�nyokra vonatkoz� inf�k miatt
                        MellekletekGridViewBind_WithReselectRow();
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsatolmanyokGridView -ban kijel�lt kapcsolatokat
    /// </summary>
    private void deleteSelected_IratelemKapcsolatok()
    {
        // Jogosults�gkezel�s:
        if (FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatInvalidate))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsatolmanyokGridView, EErrorPanel1, ErrorUpdatePanel1);

            EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
        }
    }

    protected void CsatolmanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedRowNumber, "check");
        }
    }

    private void CsatolmanyokGridView_RefreshOnClientClicks(string mellekletId)
    {
        //string id = mellekletId;
        //if (!String.IsNullOrEmpty(id))
        //{
        //    CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);
        //    CsatolmanyokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        //}
    }

    protected void CsatolmanyokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsatolmanyokGridView.PageIndex;

        CsatolmanyokGridView.PageIndex = CsatolmanyokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != CsatolmanyokGridView.PageIndex)
        {
            ////scroll �llapot�nak t�rl�se
            //JavaScripts.ResetScroll(Page, CsoportokCPE);
            CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
        }
        else
        {
            //UI.GridViewSetScrollable(CsatolmanyokSubListHeader.Scrollable, CsatolmanyokCPE);
        }
        CsatolmanyokSubListHeader.PageCount = CsatolmanyokGridView.PageCount;
        CsatolmanyokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsatolmanyokGridView);
    }

    protected void CsatolmanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");
    }

    /// <summary>
    /// Formatum mez� alapj�n be�ll�tja a f�jlikonokat
    /// </summary>
    public void SetFileIcons_GridViewRowDataBound(GridViewRowEventArgs e, Page page, string imageButtonId)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string formatum = String.Empty;
            string externalLink = String.Empty;

            if (drv != null && drv["Formatum"] != null)
            {
                formatum = drv["Formatum"].ToString();
            }

            if (drv != null && drv["External_Link"] != null)
            {
                //externalLink = drv["External_Link"].ToString();
                externalLink = "GetDocumentContent.aspx?id=" + drv["krtDok_Id"];
            }

            if (!string.IsNullOrEmpty(formatum))
            {
                ImageButton fileIcon = (ImageButton)e.Row.FindControl(imageButtonId);

                if (fileIcon != null)
                {
                    fileIcon.ImageUrl = "~/images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum);

                    if (!String.IsNullOrEmpty(externalLink))
                    {
                        fileIcon.OnClientClick = "window.open('" + externalLink + "'); return false;";
                        fileIcon.Enabled = true;
                        DokumentumVizualizerComponentCsatolmanyok.AddDokumentElement(
                        fileIcon.ClientID, drv["krtDok_Id"].ToString(), drv["FajlNev"].ToString(), drv["VerzioJel"].ToString(), drv["External_Link"].ToString());
                    }
                }
            }
        }
    }

    void CsatolmanyokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsatolmanyokSubListHeader.RowCount);
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView));
    }

    protected void CsatolmanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(MellekletekGridView)
            , e.SortExpression, UI.GetSortToGridView("CsatolmanyokGridView", ViewState, e.SortExpression));
    }

    #endregion

}
