<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MetaAdatokTab.ascx.cs" 
Inherits="eRecordComponent_MetaAdatokTab" %>

<%@ Register Src="../Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList" TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="../Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="UgyMetaAdatokUpdatePanel" runat="server" OnLoad="UgyMetaAdatokUpdatePanel_Load">
    <ContentTemplate>  
    <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <%--<ajaxToolkit:CollapsiblePanelExtender ID="UgyMetaAdatokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>  --%>                                                
  
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
<asp:HiddenField ID="ObjTip_Id_HiddenField" runat="server" />
<asp:HiddenField ID="MaxSorszam_HiddenField" runat="server" />
<%-- a TextBoxErtek l�that�s�g�nak vez�rl�s�hez --%>
<asp:HiddenField ID="TargyszoIds_HiddenField" runat="server" />
<asp:HiddenField ID="TipusFlags_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
       <tr>               
               <td>                       
                <asp:UpdatePanel ID="updatePanelMetaAdatok" runat="server" OnLoad="updatePanelMetaAdatok_Load">
                    <ContentTemplate>
                        <uc1:SubListHeader ID="SubListHeaderUgyMetaAdatok" runat="server"/> 
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeUgyMetaAdatok" runat="server" TargetControlID="panelMetaAdatok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeUgyMetaAdatok" CollapseControlID="btnCpeUgyMetaAdatok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
                            ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="btnCpeUgyMetaAdatok"
                            ExpandedSize="0" ExpandedText="�gyh�z rendelt t�rgyszavak" CollapsedText="�gyh�z rendelt t�rgyszavak">
                        </ajaxToolkit:CollapsiblePanelExtender>                     
                        <asp:Panel ID="panelMetaAdatok" runat="server" Visible="true" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top; width: 0px;">
                                    <asp:ImageButton ImageUrl="../images/hu/Grid/minus.gif" runat="server" ID="btnCpeUgyMetaAdatok" OnClientClick="return false;" />
                                </td>   
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewMetaAdatok" runat="server" GridLines="Both" BorderWidth="1px" 
                                     BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewMetaAdatok_RowCommand" 
                                     OnPreRender="gridViewMetaAdatok_PreRender" AllowSorting="True" PagerSettings-Visible="false" 
                                     CellPadding="0" DataKeyNames="Id" AutoGenerateColumns="False"
                                     OnRowDataBound="gridViewMetaAdatok_RowDataBound" OnSorting="gridViewMetaAdatok_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
						                    <asp:BoundField DataField="Targyszo" HeaderText="Megnevez�s" SortExpression="Targyszo">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField>
						                    <asp:BoundField DataField="Ertek" HeaderText="�rt�k" SortExpression="Ertek">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField>						                    
						                    <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField>
						                    <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="Note">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField>
						                    <asp:BoundField DataField="Sorszam" HeaderText="Sorsz�m" SortExpression="Sorszam">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField>
						                    
						                    <%-- nem l�that�, de kell az �rt�ke a k�telez�s�g vizsg�lat�hoz --%>
						                    <%-- <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus" Visible = "false">
							                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
							                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
						                    </asp:BoundField> --%>
                                            <asp:TemplateField Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader"  />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:Label ID="lbTipusHeader"  runat="server" Text="T�pus" />
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:Label id="Tipus" runat="server" Text='<%# Eval("Tipus") %>' />
                                                 </ItemTemplate>
                                            </asp:TemplateField>
						                    						                    
                                            <asp:TemplateField Visible = "false">
                                                    <HeaderStyle  CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:Label ID="labelRendszer" runat="server" Text="Rendszer" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSystem" runat="server" Enabled="false"/>
                                                    </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--   
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            --%> 						                    
                                        </Columns>
                                     </asp:GridView>
                                   </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>
         </tr>
    </table>
    <tr class = "urlapElvalasztoSor">
        <td>
            <asp:Image ID="SpacerImage" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif" Visible="true" />
        </td>
    </tr>
 <%-- radio --%>
 	<table cellpadding="0" cellspacing="0" width="100%">
         <tr>       
         	<td>       	
			  <eUI:eFormPanel ID="EFormPanel1" runat="server">
				<table cellspacing="0" cellpadding="0" width="100%" align="center">
					<tr class="urlapSor">
						<td class="mrUrlapCaption" align="left">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label ID="Label2" runat="server" Text="Metadat megnevez�se:" /></td>
						<td class="mrUrlapMezo" align="left">
							<asp:TextBox ID="TextBoxEgyedi" runat="server" CssClass="mrUrlapInput">
							</asp:TextBox></td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption" align="left">
                            <asp:Label ID="labelErtekStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label ID="LabelErtek" runat="server" Text="�rt�k" /></td>					
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxErtek" runat="server" CssClass="mrUrlapInput">
							</asp:TextBox></td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption" align="left">
                            <asp:Label ID="labelSorszam" runat="server" Text="Sorsz�m:"></asp:Label></td>					
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxSorszam" runat="server" Text="" Width="35"></asp:TextBox><ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server"
							    Minimum="0" Maximum="100" TargetControlID="TextBoxSorszam" Width="60"/>
						</td>
					</tr>
					
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label4" runat="server" Text="Opcion�lis:"></asp:Label></td>
						<td class="mrUrlapMezo">
                            <asp:CheckBox ID="CheckBox1" runat="server" /></td>
					</tr>												
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label5" runat="server" Text="T�pus:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList1" runat="server" />
                        </td>
                    </tr>
										
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="mrUrlapCaption">
							<asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
						<td class="mrUrlapMezo">
						    <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
					</tr>

                    <tr class = "urlapElvalasztoSor">
                        <td>
                            <asp:Image ID="SpacerImage1" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif" Visible="true" /></td>
                    </tr>
   	                <tr>
   	                    <td>
   	                    </td>
		                <td class="mrUrlapMezo">
		                    <asp:ImageButton ID="btnAddNewKeyword" runat="server" ImageUrl="~/images/hu/ovalgomb/hozzaadas.jpg"
		                    onmouseover="swapByName(this.id,'hozzaadas2.jpg')" onmouseout="swapByName(this.id,'hozzaadas.jpg')"
		                    />
		                </td>
	                </tr>		
				</table>
			</eUI:eFormPanel>         
         	</td>
         </tr>         
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

