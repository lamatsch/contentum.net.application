using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

// TODO: kitakar�tani, tiszt�tani a k�dot
// TODO: funkci�jogok

public partial class eRecordComponent_MetaAdatokTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    private PageView pageView = null;
    UI ui = new UI();

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";


    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);

        if (selectedRow != null)
        {
            CheckBox cbSystem = (CheckBox)selectedRow.FindControl("cbSystem");
            if (cbSystem != null)
            {
                modosithato = !cbSystem.Checked;
            }
        }
        return modosithato;
    }


    /// <summary>
    /// Tipus �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getTipus(string ID, GridView gridView)
    {
        string tipus = "";
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);

        if (selectedRow != null)
        {
            Label lbTipus = (Label)selectedRow.FindControl("Tipus");
            if (lbTipus != null)
            {
                tipus = lbTipus.Text;
            }
        }
        return tipus;
    }

    // mag�t�l nem �ll vissza a st�lus, ha ReadOnly=true-r�l visszav�ltunk false-ra
    protected void SetTextBoxReadOnly(TextBox tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        if (!isReadOnly)
        {
            // vissza�ll�tjuk a st�lust
            tbox.CssClass = normalCssClass;
        }

    }

    /// <summary>
    /// Meghat�rozza az eredm�nyhalmazban szerepl� max. sorsz�mot
    /// -> alap�rtelmezett k�vetkez� sorsz�m meghat�roz�s�hoz
    /// </summary>
    /// <param name="columnName">Oszlop neve, amiben a maximumot keress�k</param> 
    private int GetMaxSorszam(Result result, string columnName)
    {
        int max = 0;
        int currentValue;

        if (result.Ds != null && result.Ds.Tables[0].Rows.Count != 0 && result.Ds.Tables[0].Rows[0][columnName] != null)
        {

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                if (int.TryParse(result.Ds.Tables[0].Rows[i][columnName].ToString(), out currentValue))
                {
                    if (currentValue > max)
                        max = currentValue;
                }
            }
        }

        return max;

    }

    // a numeric updown maximum�t adja vissza a gridview sorsz�mok �rt�keihez vagy a
    // gridviewsorainak sz�m�hoz
    protected int GetNupeMaximum()
    {
        int maxSorszam;
        int maximum;
        if (int.TryParse(MaxSorszam_HiddenField.Value.ToString(), out maxSorszam))
        {
            maximum = maxSorszam + 1;
        }
        else
        {
            maximum = gridViewMetaAdatok.Rows == null ? 1 : gridViewMetaAdatok.Rows.Count + 1;
        }

        return maximum;
    }

    #endregion

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MetaAdatokList");
        pageView = new PageView(Page, ViewState);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);
        SubListHeaderUgyMetaAdatok.RowCount_Changed += new EventHandler(SubListHeaderUgyMetaAdatok_RowCount_Changed);
        SubListHeaderUgyMetaAdatok.ErvenyessegFilter_Changed += new EventHandler(SubListHeaderUgyMetaAdatok_ErvenyessegFilter_Changed);
        SubListHeaderUgyMetaAdatok.ButtonsClick += new CommandEventHandler(SubListHeaderUgyMetaAdatokButtonsClick);
        btnAddNewKeyword.Click += new ImageClickEventHandler(btnAddNewKeyword_Click);
        //ddownSzabvanyos.DropDownList.AutoPostBack = true;
        //ddownSzabvanyos.DropDownList.SelectedIndexChanged += new EventHandler(ddownSzabvanyos_SelectedIndexChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        if (pageView == null)
            pageView = new PageView(Page, ViewState);

        //F� lista gombjainak l�that�s�g�nak be�ll�t�sa
        SubListHeaderUgyMetaAdatok.NewVisible = false;
        SubListHeaderUgyMetaAdatok.ModifyVisible = true;
        SubListHeaderUgyMetaAdatok.ViewVisible = true;
        SubListHeaderUgyMetaAdatok.InvalidateVisible = true;
        SubListHeaderUgyMetaAdatok.ValidFilterVisible = true;

        //Keres�si objektum t�pus�nak megad�sa

        //F� lista megjelen�sek testreszab�sa

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        SubListHeaderUgyMetaAdatok.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewMetaAdatok.ClientID, "check", "cbSystem", false);

        //selectedRecordId kezel�se
        SubListHeaderUgyMetaAdatok.AttachedGridView = gridViewMetaAdatok;


        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewMetaAdatok);

        //F� lista esem�nykezel� f�ggv�nyei

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        // search object
        Search.SetIdsToSearchObject(Page, "Id", new EREC_ObjektumTargyszavaiSearch());

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        // controlok megengedetts�ge
        //ddownSzabvanyos.Enabled = rbSzabvanyos.Checked;
        //SetTextBoxReadOnly(TextBoxEgyedi, !rbEgyedi.Checked, "mrUrlapInput");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //SubListHeaderUgyMetaAdatok.NewEnabled = FunctionRights.GetFunkcioJog(Page, "MetaAdatok" + CommandName.New);
        EFormPanel1.Enabled = FunctionRights.GetFunkcioJog(Page, "MetaAdatok" + CommandName.New);
        SubListHeaderUgyMetaAdatok.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "MetaAdatok" + CommandName.View);
        SubListHeaderUgyMetaAdatok.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "MetaAdatok" + CommandName.Modify);
        SubListHeaderUgyMetaAdatok.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "MetaAdatok" + CommandName.Invalidate);

        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewMetaAdatok);

        if (IsPostBack)
        {
            // biztos�tja, hogy a tabok k�z�tti v�lt�skor is meg�rz�dj�k a kiv�lasztott sor Id-ja
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewMetaAdatok);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        // ha ReloadTab()-ban lenne, egy sor kiv�laszt�sakor megjelenn�nek a letiltott dolgok
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
    }

    protected void UgyMetaAdatokUpdatePanel_Load(object sender, EventArgs e)
    {
        UgyMetaAdatokUpdatePanel.Visible = true;
    }

    public void ReLoadTab()
    {
        // Ugyirat objektumt�pus ObjTip_Id-j�nek meghat�roz�sa
        // TODO: �ltal�nosabban meghat�rozni
        string ObjTip_Kod = "";
        switch (_ParentForm)
        {
            case Constants.ParentForms.Ugyirat:
                ObjTip_Kod = "EREC_UgyUgyiratok";
                break;
            case Constants.ParentForms.IraIrat:
                ObjTip_Kod = "EREC_IraIratok";
                break;
            default:    // hiba
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_ObjTipUnknown);
                ErrorUpdatePanel.Update();
                UgyMetaAdatokUpdatePanel.Visible = false;
                return;
        }

        Contentum.eAdmin.Service.KRT_ObjTipusokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();

        search.Kod.Filter(ObjTip_Kod);
        Result result = service.GetAll(ExecParam, search);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (result.GetCount > 0)
            {
                ObjTip_Id_HiddenField.Value = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {  // TODO: hibakezel�s
                ObjTip_Id_HiddenField.Value = "00000000-0000-0000-0000-000000000000";
            }

        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
            UgyMetaAdatokUpdatePanel.Visible = false;
            return;
            //ObjTip_Id_HiddenField.Value = "00000000-0000-0000-0000-000000000000";
        }

        SubListHeaderUgyMetaAdatok.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SubListHeaderUgyMetaAdatok.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        MetaAdatokGridViewBind();
        // ne csin�ljuk meg feleslegesen
        if (Command != CommandName.View)
        {
            MetaAdatokDropDownBind();
            nupeSorszam.Maximum = GetNupeMaximum();

        }
        ClearForm();

        pageView.SetViewOnPage(Command);
    }

    private void SetViewControls()
    {
        EFormPanel1.Visible = false;

        SubListHeaderUgyMetaAdatok.ModifyVisible = false;
        SubListHeaderUgyMetaAdatok.InvalidateVisible = false;
    }
    #endregion

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void MetaAdatokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewMetaAdatok", ViewState, "Sorszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewMetaAdatok", ViewState);

        MetaAdatokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void MetaAdatokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_ObjektumTargyszavaiService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();//(EREC_ObjektumTargyszavaiSearch)Search.GetSearchObject(Page, new EREC_ObjektumTargyszavaiSearch());

        search.Obj_Id.Value = ParentId;
        search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        search.ObjTip_Id.Value = ObjTip_Id_HiddenField.Value;
        search.ObjTip_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        search.OrderBy = Search.GetOrderBy("gridViewMetaAdatok", ViewState, SortExpression, SortDirection);
        SubListHeaderUgyMetaAdatok.SetErvenyessegFields(search.ErvKezd, search.ErvVege);
        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewMetaAdatok, res, SubListHeaderUgyMetaAdatok, EErrorPanel1, ErrorUpdatePanel);

        // max. sorszam meghatarozasa
        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            MaxSorszam_HiddenField.Value = GetMaxSorszam(res, "Sorszam").ToString();
        }

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewMetaAdatok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            //DataRowView drw = (DataRowView)e.Row.DataItem;

            //BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }

        //Lockol�s jelz�se
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewMetaAdatok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewMetaAdatok.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewMetaAdatok.PageIndex = SubListHeaderUgyMetaAdatok.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewMetaAdatok.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeUgyMetaAdatok);
            MetaAdatokGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(SubListHeaderUgyMetaAdatok.Scrollable, cpeUgyMetaAdatok);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewMetaAdatok);

        //oldalsz�mok be�ll�t�sa
        SubListHeaderUgyMetaAdatok.PageCount = gridViewMetaAdatok.PageCount;
        SubListHeaderUgyMetaAdatok.PagerLabel = UI.GetGridViewPagerLabel(gridViewMetaAdatok);
    }


    void SubListHeaderUgyMetaAdatok_RowCount_Changed(object sender, EventArgs e)
    {
        MetaAdatokGridViewBind();
    }

    void SubListHeaderUgyMetaAdatok_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        MetaAdatokGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewMetaAdatok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewMetaAdatok, selectedRowNumber, "check");

            string id = gridViewMetaAdatok.DataKeys[selectedRowNumber].Value.ToString();

            //gridViewMetaAdatok_SelectRowCommand(id);
            RefreshOnClientClicksByMasterListSelectedRow(id);
        }

    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            SubListHeaderUgyMetaAdatok.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyMetaAdatokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelMetaAdatok.ClientID);

            //m�dos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewMetaAdatok);

            if (modosithato)
            {
                SubListHeaderUgyMetaAdatok.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyMetaAdatokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.MaxSorszam + "=" + MaxSorszam_HiddenField.Value
                + "&" + QueryStringVars.Tipus + "=" + getTipus(id, gridViewMetaAdatok)
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelMetaAdatok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                SubListHeaderUgyMetaAdatok.ModifyOnClientClick = jsNemModosithato;
            }

        }
    }


    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelMetaAdatok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    MetaAdatokGridViewBind();
                    // ne csin�ljuk meg feleslegesen
                    if (Command != CommandName.View)
                    {
                        MetaAdatokDropDownBind();
                    }
                    break;
            }
        }
    }


    //GridView Sorting esm�nykezel�je
    protected void gridViewMetaAdatok_Sorting(object sender, GridViewSortEventArgs e)
    {
        MetaAdatokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewMetaAdatok", ViewState, e.SortExpression));
    }


    protected void MetaAdatokDropDownBind()
    {
        string WhereBy = "";
        // a hierarchi�ban a felhaszn�l� felett �ll� szervezetek meghat�roz�sa
        SzervezetHierarchiaService serviceSZH = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetSzervezetHierarchiaService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = serviceSZH.GetAll(ExecParam);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else if (result.Ds.Tables[0].Rows.Count > 0)
        {
            WhereBy = " and convert(nvarchar(36),Csoport_Id_Tulaj) in ({0})";
            string[] Ids = new string[result.Ds.Tables[0].Rows.Count];
            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                Ids[i] = "'" + result.Ds.Tables[0].Rows[i]["Id"].ToString() + "'";
            }
            WhereBy = String.Format(WhereBy, String.Join(",", Ids));

        //    // a t�rgyszavak kigy�jt�se, a szervezeti hierarchia seg�ts�g�vel sz�rve
        //    EREC_MetaAdatokService service = eRecordService.ServiceFactory.GetEREC_MetaAdatokService();

        //    EREC_MetaAdatokSearch search = (EREC_MetaAdatokSearch)Search.GetSearchObject(Page, new EREC_MetaAdatokSearch());
        //    search.WhereByManual = WhereBy;

        //    result = service.GetAll(ExecParam, search);
        //    ddownSzabvanyos.FillDropDownList(result, "Id", "MetaAdatok", true, EErrorPanel1);
        //    if (String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        GetTipusFlags(result);
        //    }
        }   // szervezeti hierarchia sz�r�s sikeres volt

    }
    #endregion


    #region adatbeviteli mezok

    /// <summary>
    /// �sszef�zi az eredm�nyhalmazb�l a Tipus flageket, egy�tt az id-kel
    /// -> az Ertek mez� megjelen�t�s�nek vizsg�lat�hoz
    /// </summary>
    /// <param name="result">Az (EREC_MetaAdatok) eredm�nyhalmaz, amiben keres�nk</param> 
    private void GetTipusFlags(Result result)
    {

        if (result.Ds == null || result.Ds.Tables[0].Rows.Count == 0)
        {
            TipusFlags_HiddenField.Value = "";
            return;
        }
        string ids = "";
        string flags = "";
        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
        {
            if (result.Ds.Tables[0].Rows[i]["Id"] != null && result.Ds.Tables[0].Rows[i]["Tipus"] != null)
            {
                if (ids != "")
                    ids += "|";
                if (flags != "")
                    flags += "|";
                ids += result.Ds.Tables[0].Rows[i]["Id"];
                flags += result.Ds.Tables[0].Rows[i]["Tipus"];
            }
        }
        TargyszoIds_HiddenField.Value = ids;
        TipusFlags_HiddenField.Value = flags;

    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_ObjektumTargyszavai erec_ObjektumTargyszavai)
    {
        Record_Ver_HiddenField.Value = erec_ObjektumTargyszavai.Base.Ver;
    }

    // form --> business object
    private EREC_ObjektumTargyszavai GetBusinessObjectFromComponents()
    {
        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

        //if (rbSzabvanyos.Checked == true)
        //{
        //    erec_ObjektumTargyszavai.Targyszo_Id = ddownSzabvanyos.DropDownList.SelectedItem.Value;
        //    erec_ObjektumTargyszavai.Updated.Targyszo_Id = pageView.GetUpdatedByView(ddownSzabvanyos);
        //    erec_ObjektumTargyszavai.Targyszo = ddownSzabvanyos.DropDownList.SelectedItem.Text;
        //    erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(ddownSzabvanyos);
        //    erec_ObjektumTargyszavai.Forras = "02"; // Szervezeti
        //    erec_ObjektumTargyszavai.Updated.Forras = true;
        //}
        //else
        //    if (rbEgyedi.Checked == true)
        //    {
        //        erec_ObjektumTargyszavai.Targyszo_Id = "";
        //        erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;
        //        erec_ObjektumTargyszavai.Targyszo = TextBoxEgyedi.Text;
        //        erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(TextBoxEgyedi);
        //        erec_ObjektumTargyszavai.Forras = "03"; // K�zi
        //        erec_ObjektumTargyszavai.Updated.Forras = true;
        //        //new System.Data.SqlTypes.SqlGuid("00000000-0000-0000-0000-000000000000");
        //    }

        erec_ObjektumTargyszavai.Ertek = TextBoxErtek.Text;
        erec_ObjektumTargyszavai.Updated.Ertek = pageView.GetUpdatedByView(TextBoxErtek);

        erec_ObjektumTargyszavai.Sorszam = TextBoxSorszam.Text;
        erec_ObjektumTargyszavai.Updated.Sorszam = pageView.GetUpdatedByView(TextBoxSorszam);

        erec_ObjektumTargyszavai.Obj_Id = ParentId;
        erec_ObjektumTargyszavai.Updated.Obj_Id = true;

        erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id_HiddenField.Value;
        erec_ObjektumTargyszavai.Updated.ObjTip_Id = pageView.GetUpdatedByView(ObjTip_Id_HiddenField);
        erec_ObjektumTargyszavai.Base.Ver = Record_Ver_HiddenField.Value;
        erec_ObjektumTargyszavai.Base.Updated.Ver = pageView.GetUpdatedByView(Record_Ver_HiddenField);

        erec_ObjektumTargyszavai.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_ObjektumTargyszavai.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_ObjektumTargyszavai.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.Base.Note = textNote.Text;
        erec_ObjektumTargyszavai.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        return erec_ObjektumTargyszavai;
    }

    protected void rgbForras_CheckedChanged(object sender, EventArgs e)
    {
        //if (rbSzabvanyos.Checked == true)
        //{
        //    //RequiredFieldValidatorEgyediTargyszo.Enabled = false;
        //    ddownSzabvanyos.Enabled = true;
        //    ddownSzabvanyos.Focus();
        //    SetTextBoxReadOnly(TextBoxEgyedi, true, "mrUrlapInput");
        //    // a TextBoxErtek kezel�se a SelectedIndexChanged esem�nyhez k�t�dik

        //}
        //else if (rbEgyedi.Checked == true)
        //{
        //    //RequiredFieldValidatorEgyediTargyszo.Enabled = true;
        //    ddownSzabvanyos.Enabled = false;

        //    SetTextBoxReadOnly(TextBoxEgyedi, false, "mrUrlapInput");
        //    TextBoxEgyedi.Focus();

        //    SetTextBoxReadOnly(TextBoxErtek, false, "mrUrlapInput");
        //    labelErtekStar.Visible = false;
        //    //�rt�k k�telez�s�g figyelmeztet�s t�rl�se
        //    btnAddNewKeyword.Attributes.Remove("onclick");

        //}
        ClearForm();
    }

    #endregion

    #region sublistheader/btnAddNewKeyword

    private void SubListHeaderUgyMetaAdatokButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();
        Command = e.CommandName;

        if (e.CommandName == CommandName.Modify)
        {

        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedMetaAdatok();
            MetaAdatokGridViewBind();
        }
    }

    private void btnAddNewKeyword_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (String.IsNullOrEmpty(ParentId))
        {
            ResultError.DisplayNoIdParamError(EErrorPanel1);
            ErrorUpdatePanel.Update();
        }
        else
        {
            // ellen�rz�s, ki van-e t�ltve a relev�ns beviteli mez� 
            string emptyField = "";
            //if (rbEgyedi.Checked == true && TextBoxEgyedi.Text == "")
            //{
            //    emptyField = "'" + rbEgyedi.Text + "'";
            //}
            //else if (rbSzabvanyos.Checked && ddownSzabvanyos.SelectedValue == "")
            //{
            //    emptyField = "'" + rbSzabvanyos.Text + "'";
            //}

            if (emptyField != "")
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, String.Format(Resources.Error.ErrorText_InsertRecordFieldEmpty, emptyField));
                ErrorUpdatePanel.Update();
            }
            else
            {
                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = GetBusinessObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                // ellen�rz�s, ne lehessen t�bbsz�r felvenni ua.t�rgysz�/�rt�k kombin�ci�t...
                // szervezeti: Targyszo_Id-re is ellen�rz�nk
                // egyedi: Targyszo-ra ellen�rz�nk, csak pontos egyez�s
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                search.Obj_Id.Value = erec_ObjektumTargyszavai.Obj_Id;
                search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                search.ObjTip_Id.Value = erec_ObjektumTargyszavai.ObjTip_Id;
                search.ObjTip_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                search.Ertek.Value = erec_ObjektumTargyszavai.Ertek;
                search.Ertek.Operator = Contentum.eQuery.Query.Operators.equals;
                //if (rbSzabvanyos.Checked == true)
                //{
                //    search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id.ToString();
                //    search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                //}
                //else if (rbEgyedi.Checked == true)
                //{
                //    search.Targyszo.Value = erec_ObjektumTargyszavai.Targyszo.ToString();
                //    search.Targyszo.Operator = Contentum.eQuery.Query.Operators.equals;
                //}

                Result result = service.GetAll(execParam, search);

                if (String.IsNullOrEmpty(result.ErrorCode)) // GetAll
                {
                    if (result.Ds.Tables[0].Rows.Count == 0)
                    {
                        result = service.Insert(execParam, erec_ObjektumTargyszavai);

                        if (String.IsNullOrEmpty(result.ErrorCode)) // Insert
                        {
                            Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                            ReLoadTab();
                        }
                        else
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                            ErrorUpdatePanel.Update();
                        }
                    }
                    else  // volt m�r ilyen felvitel
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                        ErrorUpdatePanel.Update();
                    }

                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }

            }
        }

    }

    protected void ddownSzabvanyos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetTextBoxReadOnly(TextBoxErtek, false, "mrUrlapInput");
        labelErtekStar.Visible = false;

        string[] Ids = TargyszoIds_HiddenField.Value.ToString().Split('|');
        string[] Flags = TipusFlags_HiddenField.Value.ToString().Split('|');
        int i = 0;
        while (i < Ids.Length && Ids[i] != ((DropDownList)sender).SelectedValue)
        {
            i++;
        }

        if (i < Ids.Length)
        {
            if (Flags[i] == "1")
            {
                labelErtekStar.Visible = true;
                string js = "var text = $get('" + TextBoxErtek.ClientID + @"');
                    if(text && text.value == '') {
                        alert('Ehhez a t�rgysz�hoz k�telez� az �rt�k megad�sa!');
                        return false;
                     }";
                btnAddNewKeyword.Attributes.Add("onclick", js);
            }
            else
            {
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                // t�r�lj�k is az �rt�ket �s elt�ntetj�k a csillagot
                TextBoxErtek.Text = "";

                btnAddNewKeyword.Attributes.Remove("onclick");
            }

        }

    }


    #endregion

    private void ClearForm()
    {
        TextBoxEgyedi.Text = "";
        TextBoxErtek.Text = "";
        textNote.Text = "";
        TextBoxSorszam.Text = GetNupeMaximum().ToString(); // a k�vetkez� sz�ba j�het� �rt�kre �ll�tjuk
        labelErtekStar.Visible = false;

    }

    /// <summary>
    /// T�rli a MetaAdatokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedMetaAdatok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "MetaAdatokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewMetaAdatok, EErrorPanel1, ErrorUpdatePanel);
            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewMetaAdatok);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }

            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

}
