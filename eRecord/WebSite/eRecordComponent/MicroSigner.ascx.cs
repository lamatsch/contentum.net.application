﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Net;

public partial class eRecordComponent_MicroSigner : System.Web.UI.UserControl
{
    #region properties

    private List<string> _iratIds = new List<string>();

    public List<string> iratIds
    {
        get { return _iratIds; }
        set { _iratIds = value; }
    }

    private List<string> _fileNames = new List<string>();

    public List<string> fileNames
    {
        get { return _fileNames; }
        set { _fileNames = value; }
    }

    private List<string> _externalLinks = new List<string>();

    public List<string> externalLinks
    {
        get { return _externalLinks; }
        set { _externalLinks = value; }
    }

    private List<string> _recordIds = new List<string>();

    public List<string> recordIds
    {
        get { return _recordIds; }
        set { _recordIds = value; }
    }

    private List<string> _csatolmanyokIds = new List<string>();

    public List<string> csatolmanyokIds
    {
        get { return _csatolmanyokIds; }
        set { _csatolmanyokIds = value; }
    }

    private List<string> _folyamatTetelekIds = new List<string>();

    public List<string> folyamatTetelekIds
    {
        get { return _folyamatTetelekIds; }
        set { _folyamatTetelekIds = value; }
    }
    #region BLG 2947

    private string _procId = string.Empty;

    public string procId
    {
        get { return _procId; }
        set { _procId = value; }
    }
    #endregion

    private string _megjegyzes = string.Empty;

    public string megjegyzes
    {
        get { return _megjegyzes; }
        set { _megjegyzes = value; }
    }

    #endregion

    public void StartNewSigningSession()
    {
        StartNewSession(runType.SimpleSigningSession);
    }

    //BLG 2947
    public void StartNewVerifySession()
    {
        StartNewSession(runType.VerifySession);
    }

    #region BLG 6473
    private enum runType
    {
        SimpleSigningSession,
        ServerSideSigningSession,
        VerifySession        
    }

    public void StartNewServerSideSession()
    {
        StartNewSession(runType.ServerSideSigningSession);
    }
    #endregion

    private void StartNewSession(runType currentRunType)
    {
        string microsecUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerURL");
        string microsecUserName = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerUserName");
        string microsecPassword = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerPassword");

        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Newtonsoft.Json.Formatting.Indented;
            writer.WriteStartObject();
            writer.WritePropertyName("mode");
            writer.WriteValue("SELECT_CERTIFICATE_AND_SIGN");
            writer.WritePropertyName("SPName");
            writer.WriteValue("MicroSigner");
            writer.WritePropertyName("message");
            writer.WriteValue("Folyamatban...");

            writer.WritePropertyName("filters");
            writer.WriteStartArray();
            writer.WriteStartObject();
            writer.WritePropertyName("keyUsage");
            writer.WriteStartObject();
            writer.WritePropertyName("nonRepudiation");
            writer.WriteValue(true);
            writer.WritePropertyName("keyEncipherment");
            writer.WriteValue(false);
            writer.WritePropertyName("dataEncipherment");
            writer.WriteValue(false);
            writer.WritePropertyName("keyAgreement");
            writer.WriteValue(false);
            writer.WritePropertyName("keyCertSign");
            writer.WriteValue(false);
            writer.WritePropertyName("cRLSign");
            writer.WriteValue(false);
            writer.WritePropertyName("encipherOnly");
            writer.WriteValue(false);
            writer.WritePropertyName("decipherOnly");
            writer.WriteValue(false);
            writer.WriteEndObject();
            writer.WriteEndObject();
            writer.WriteEndArray();

            writer.WritePropertyName("documents");
            writer.WriteStartArray();
            foreach (var item in fileNames)
            {
                writer.WriteStartObject();
                writer.WritePropertyName("name");
                writer.WriteValue(item);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }

        var jsonEsign = sb.ToString();

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(microsecUrl + "newSigningSession");
        request.Credentials = new NetworkCredential(microsecUserName, microsecPassword);
        byte[] bytesEsign = System.Text.Encoding.ASCII.GetBytes(jsonEsign);
        request.ContentType = "application/json";
        request.ContentLength = bytesEsign.Length;
        request.Method = "POST";

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytesEsign, 0, bytesEsign.Length);
        requestStream.Close();

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream responseStream = response.GetResponseStream();
            hfEsignResponse.Value = new StreamReader(responseStream).ReadToEnd();
            hfExternalLinks.Value = "";
            foreach (var item in externalLinks)
            {
                hfExternalLinks.Value += item + "|";
            }
            hfExternalLinks.Value = hfExternalLinks.Value.Substring(0, hfExternalLinks.Value.Length - 1);
            hfRecordIds.Value = "";
            foreach (var item in recordIds)
            {
                hfRecordIds.Value += item + ";";
            }
            hfRecordIds.Value = hfRecordIds.Value.Substring(0, hfRecordIds.Value.Length - 1);
            hfCsatolmanyokIds.Value = "";
            foreach (var item in csatolmanyokIds)
            {
                hfCsatolmanyokIds.Value += item + ";";
            }
            hfCsatolmanyokIds.Value = hfCsatolmanyokIds.Value.Substring(0, hfCsatolmanyokIds.Value.Length - 1);
            hfFolyamatTetelekIds.Value = "";
            foreach (var item in folyamatTetelekIds)
            {
                hfFolyamatTetelekIds.Value += item + ";";
            }
            hfFolyamatTetelekIds.Value = hfFolyamatTetelekIds.Value.Substring(0, hfFolyamatTetelekIds.Value.Length - 1);
            hfIratIds.Value = "";
            foreach (var item in iratIds)
            {
                hfIratIds.Value += item + ";";
            }
            hfProcId.Value = procId;

            hfMegjegyzes.Value = megjegyzes;

            hfIratIds.Value = hfIratIds.Value.Substring(0, hfIratIds.Value.Length - 1);
            String startupScript = String.Empty;
            if(currentRunType == runType.SimpleSigningSession)
            {
                startupScript = "selectCertAndSign();";
            }else if(currentRunType == runType.ServerSideSigningSession)
            {
                startupScript = "serversideSign('" + hfEsignResponse.Value + "','" + hfIratIds.Value + "','" + hfRecordIds.Value + "','" + hfExternalLinks.Value + "','" + hfFolyamatTetelekIds.Value + "','" + procId + "','"+ megjegyzes + "','" + hfCsatolmanyokIds.Value + "');";
            }
            else if(currentRunType == runType.VerifySession)
            {
                startupScript = "verifyPdfs('" + hfEsignResponse.Value + "','" + hfRecordIds.Value + "','" + hfExternalLinks.Value + "','" + hfFolyamatTetelekIds.Value + "','" + procId + "');";
            }
            if (!String.IsNullOrEmpty(startupScript))
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", startupScript, true);
            }
            else
            {
                Logger.Error("MicroSigner.StartNewSession error: eszigno futtatási mód meghatározása sikertelen!");
                throw new Exception("MicroSigner.StartNewSession error: eszigno futtatási mód meghatározása sikertelen!");
            }
        }
        else
        {
            Logger.Error("ESign szerverhez csatlakozás sikertelen. Url: " + microsecUrl);
            throw new Exception("ESign szerverhez csatlakozás sikertelen. Url: " + microsecUrl);
        }
    }
}