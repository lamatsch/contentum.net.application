﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MinositesFelulvizsgalatTab.ascx.cs" Inherits="eRecordComponent_MinositesFelulvizsgalatTab" %>

<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc2" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc4" %>
<%@ Register Src="../Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="UgyiratJellemzokUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                <table cellspacing="0" cellpadding="0">
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="labelEredmeny" runat="server" Text="Felülvizsgálat eredménye:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <asp:DropDownList ID="ddlistMinositesFelulvizsgalatEredmenye" Enabled="false" CausesValidation="false" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label1" runat="server" Text="Módosítás érvényességének kezdete:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc4:CalendarControl ID="CalendarControlModositasErvenyesseg" runat="server" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label2" runat="server" Text="Új minősítési szint:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc2:KodtarakDropDownList ID="Minosites_KodtarakDropDownList" runat="server" ReadOnly="true"/>
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label3" runat="server" Text="Új min. érvényességi idő:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc4:CalendarControl ID="CalendarControlUjMinErvenyesseg" runat="server" TimeVisible="true" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label4" runat="server" Text="Minősítő neve:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc7:PartnerTextBox ID="Minosito_PartnerTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label5" runat="server" Text="Megszüntető határozat, javaslat:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <asp:TextBox ID="tbMegszuntetoHatarozat" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label6" runat="server" Text="Felülvizsgálat dátuma:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc4:CalendarControl ID="CalendarControlFelulvizsgalatIdeje" runat="server" TimeVisible="true" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <%--BUG_8507--%>
                            <asp:Label ID="labelUserNev" runat="server" Text="<%$Forditas:labelUserNev|Felülvizsgáló:%>" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <uc3:FelhasznaloCsoportTextBox ID="Felulvizsgalo_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="label8" runat="server" Text="Bizottsági tagok:" />
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                            <asp:TextBox ID="MinositesFelulvizsgalatBizTag" runat="server" TextMode="MultiLine" Rows="5" CssClass="mrUrlapInput" ReadOnly="true"  />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
            <br>
            <br>
            <br>
            <br></br>
            <div style="text-align:center;">
                <%-- TabFooter --%>
                <uc:TabFooter ID="TabFooter1" runat="server" />
                <%-- /TabFooter --%>
            </div>
            <br>
            <br></br>
            <br></br>
            <br></br>
            </br>
            </br>
            </br>
            </br>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>