﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;

public partial class eRecordComponent_MinositesFelulvizsgalatTab : System.Web.UI.UserControl
{
    public Boolean _Active = false;
    private bool isTuk = false;
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;

    public string Command = "";

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public EREC_IraIratok obj_Irat = null;


    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    //private Boolean _Load = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        isTuk = Rendszerparameterek.IsTUK(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

        ReLoadTab();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        ReLoadTab(true);
    }

    private void ReLoadTab(bool loadRecord)
    {
        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        if (obj_Irat == null)
                        {
                            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = ParentId;

                            Result result = service.Get(execParam);
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                                obj_Irat = erec_IraIratok;

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }

                        if (obj_Irat != null)
                        {
                            LoadComponentsFromBusinessObject(obj_Irat);
                        }
                    }
                }
            }


            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter1.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    private void SetComponentsVisibility(String _command)
    {

        #region TabFooter gombok állítása

        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_SaveAndClose.Visible = false;
        TabFooter1.ImageButton_SaveAndNew.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion

    }

    private void LoadComponentsFromBusinessObject(EREC_IraIratok erec_IraIratok)
    {
        if (erec_IraIratok != null)
        {
            if (isTuk)
            {
                try
                {
                    Felulvizsgalat.FillAndSetSelectedValue(ddlistMinositesFelulvizsgalatEredmenye, "", isTuk, true, true, erec_IraIratok.MinositesFelulvizsgalatEredm);
                    //ezek az adatok csak akkor kellenek, ha volt felülvizsgálat
                    if (!string.IsNullOrEmpty(erec_IraIratok.MinositesFelulvizsgalatEredm))
                    {
                        CalendarControlModositasErvenyesseg.Text = erec_IraIratok.ModositasErvenyessegKezdete;
                        Minosites_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_IraIratok.Minosites, true, null);
                        CalendarControlUjMinErvenyesseg.Text = erec_IraIratok.MinositesErvenyessegiIdeje;
                        if (!string.IsNullOrEmpty(erec_IraIratok.Minosito))
                        {
                            Minosito_PartnerTextBox.Id_HiddenField = erec_IraIratok.Minosito;
                            Minosito_PartnerTextBox.SetPartnerTextBoxById(EErrorPanel1);
                        }
                        tbMegszuntetoHatarozat.Text = erec_IraIratok.MegszuntetoHatarozat;
                        CalendarControlFelulvizsgalatIdeje.Text = erec_IraIratok.FelulvizsgalatDatuma;
                        if (!string.IsNullOrEmpty(erec_IraIratok.Felulvizsgalo_id))
                        {
                            Felulvizsgalo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraIratok.Felulvizsgalo_id;
                            Felulvizsgalo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
                        }
                        MinositesFelulvizsgalatBizTag.Text = erec_IraIratok.MinositesFelulvizsgalatBizTag;
                    }
                }
                catch (Exception ex)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", ex.Message);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
    }
}