﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MunkanapSearchControl.ascx.cs" Inherits="eRecordComponent_MunkanapSearchControl" %>

<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>

<div>
    <asp:CheckBox ID="tizMunkanapCheckBox" style="white-space: nowrap;" Text="" runat="server"/>
    <asp:Label ID="labelMunakanapStart" runat="server" Text="utolsó&nbsp;"></asp:Label>
    <uc:RequiredNumberBox ID="numberTextBoxMunakanap" runat="server" Validate="false" Width="20px"/>
    <asp:Label ID="labelMunakanapEnd" runat="server" Text="&nbsp;munkanapon belül iktatás történt"></asp:Label>
</div>