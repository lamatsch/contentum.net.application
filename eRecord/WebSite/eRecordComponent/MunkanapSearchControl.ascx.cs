﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_MunkanapSearchControl : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    private const string defaultMunkanap = "10";

    public bool Checked
    {
        get
        {
            return tizMunkanapCheckBox.Checked;
        }
        set
        {
            tizMunkanapCheckBox.Checked = value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        numberTextBoxMunakanap.Text = defaultMunkanap;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        tizMunkanapCheckBox.Checked = !String.IsNullOrEmpty(SearchField.Value);

        if (tizMunkanapCheckBox.Checked)
        {
            numberTextBoxMunakanap.Text = SearchField.Value.TrimStart('-');
        }
        else
        {
            numberTextBoxMunakanap.Text = defaultMunkanap;
        }
    }

    public void SetSearchObjectFields(Field SearchField)
    {
        if (tizMunkanapCheckBox.Checked && !String.IsNullOrEmpty(numberTextBoxMunakanap.Text))
        {
           SearchField.Value = "-" + numberTextBoxMunakanap.Text;
           SearchField.Operator = Query.Operators.greaterorequal;
        }
        else
        {
           SearchField.Clear();
        }
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        //Keresési feltételek elmentése kiiratható formában
        if (tizMunkanapCheckBox.Checked && !String.IsNullOrEmpty(numberTextBoxMunakanap.Text))
        {
            return String.Format("{0}{1}{2}", labelMunakanapStart.Text, numberTextBoxMunakanap.Text, labelMunakanapEnd.Text);
        }

        return String.Empty;
    }

    #endregion
}
