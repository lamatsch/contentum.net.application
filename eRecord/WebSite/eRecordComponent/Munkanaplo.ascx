<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc1" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Munkanaplo.ascx.cs" Inherits="eRecordComponent_Munkanaplo" %>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>   
     
         <asp:Panel ID="MainPanel" runat="server" Visible="true" HorizontalAlign="Left">
         <asp:UpdatePanel ID="UpdatePanelMunkanaplo" runat="server" OnLoad="UpdatePanelMunkanaplo_Load">
         <ContentTemplate>
         <asp:HiddenField ID="hfSelectedNode" runat="server" />
         
            <ajaxToolkit:CollapsiblePanelExtender ID="MunkanaploCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" 
                 AutoCollapse="false" AutoExpand="false"    
                ExpandedSize="500" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>   
                 
            <asp:Panel ID="Panel1" runat="server" Visible="true">                                        
                <asp:TreeView ID="treeViewMunkanaplo" runat="server" EnableClientScript="true" 
                 BackColor="White" ShowLines="True" PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip="" SkipLinkText="" OnTreeNodePopulate="treeViewMunkanaplo_TreeNodePopulate" OnSelectedNodeChanged="treeViewMunkanaplo_SelectedNodeChanged">
                    <SelectedNodeStyle Font-Bold="False" CssClass="tvSelectedRowStyle" />
                </asp:TreeView>
            </asp:Panel>
         </ContentTemplate>
        </asp:UpdatePanel>
        </asp:Panel>  
