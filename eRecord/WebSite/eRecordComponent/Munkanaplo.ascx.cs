using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_Munkanaplo : System.Web.UI.UserControl
{
    public ASP.component_listheader_ascx ParentListHeader = null;

    #region Variables
    private const string alternateRowStyle = "tvAlternateRowStyle";
    private const string rowStyle = "TreeViewRowTable";
    private bool alternate = false;

    bool ugyiratModosithato = true;
    bool iratModosithato = true;
    bool iratPeldanyModosithato = true;
    bool kuldemenyModosithato = true;

    private bool loadTreeViewState = false;

    private string imageWidth = "25px";
    private string imageHeight = "20px";
    #endregion

    #region Helper Functions

    private string getRowStyle()
    {
        string style = rowStyle;

        if (alternate)
            style = alternateRowStyle;

        alternate = !alternate;

        return style;
    }

    private string getParentRowStyle(TreeNode node)
    {
        string text = node.Text;

        if (text.Contains(alternateRowStyle))
            return alternateRowStyle;
        else
            return rowStyle;
    }

    //private string getNodeStateInJS()
    //{
    //    string js = "var id = this.parentNode.id; var index = '" + treeViewMunkanaplo.ClientID + "'.length;" +
    //                "var nodeIndex = id.substring(index + 1);var state = " + treeViewMunkanaplo.ClientID + "_Data.expandState.value.charAt(nodeIndex);";
    //    return js;
    //}

    private string saveSelectedNodeInJS(string valuePath)
    {
        string js = "$get('" + hfSelectedNode.ClientID + "').value = '" + valuePath + "'";

        return js;
    }

    private void SetTreeNodeState(TreeNode node, int depth)
    {
        node.SelectAction = TreeNodeSelectAction.Select;
        node.PopulateOnDemand = true;
        string selectedValue = hfSelectedNode.Value.Trim();

        //kezdeti eset
        if (!loadTreeViewState || String.IsNullOrEmpty(selectedValue))
        {
            if (depth < 2)
            {
                node.Expanded = false;
            }
            else
            {
                node.Expanded = true;
            }
        }
        //kiv�lasztott node be�ll�t�sa hidden field-b�l
        else
        {
            string[] selectedNodes = selectedValue.Split('\\');
            int selectedDepth = selectedNodes.Length - 1;

            if (selectedDepth > depth )
            {
                if (node.Value == selectedNodes[depth])
                {
                    node.Expand();
                }
            }

            if (selectedDepth == depth)
            {
                if (node.Value == selectedNodes[depth])
                {
                    string js = "var selectedNode = $get('" + node.Value + "').parentNode;if(selectedNode){" + treeViewMunkanaplo.ClientID + @"_Data.selectedNodeID.value = selectedNode.id;" +
                                "var data = " + treeViewMunkanaplo.ClientID + @"_Data ;node = WebForm_GetParentByTagName(selectedNode, 'TD');WebForm_AppendToClassName(node, data.selectedClass);}";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectNode", js, true);
                }

                if (depth < 2)
                {
                    node.Expanded = false;
                }
                else
                {
                    node.Expanded = true;
                }
            }
        }
    }

    private String SetUgyUgyiratUrl(String szoveg, String Id)
    {
        string valuePath = Id;
        string js = saveSelectedNodeInJS(valuePath);
        return "<span url=\"UgyUgyiratokForm.aspx\" id=\"" + Id + "\" modosithato=\"" + ugyiratModosithato + "\"" +
               "onclick=\"" + js + "\">" + szoveg + "</span>";
    }
    private String SetUgyUgyiratdarabokUrl(String szoveg, String UgyiratId, String Id)
    {
        string valuePath = UgyiratId + "\\\\" + Id;
        string js = saveSelectedNodeInJS(valuePath);
        return "<span url=\"KuldKuldemenyekForm.aspx\" id=\"" + Id + "\"" +
               "onclick=\"" + js + "\">" + szoveg + "</span>";
    }
    private String SetIraIratokUrl(String szoveg, String UgyiratId, String UgyiratDarabId, String Id)
    {
        string valuePath = UgyiratId + "\\\\" + Id;
        string js = saveSelectedNodeInJS(valuePath);
        return "<span url=\"IraIratokForm.aspx\" id=\"" + Id + "\" modosithato=\"" + iratModosithato + "\"" +
               "onclick=\"" + js + "\">" + szoveg + "</span>";
    }
    private String SetPldIratPeldanyokUrl(String szoveg, String UgyiratId, String UgyiratDarabId, String IraIratId, String Id)
    {
        string valuePath = UgyiratId + "\\\\" + IraIratId + "\\\\" + Id;
        string js = saveSelectedNodeInJS(valuePath);
        return "<span url=\"PldIratPeldanyokForm.aspx\" id=\"" + Id + "\" modosithato=\"" + iratPeldanyModosithato + "\"" +
               "onclick=\"" + js + "\">" + szoveg + "</span>";
    }
    private String SetKuldemenyekUrl(String szoveg, String UgyiratId, String UgyiratDarabId, String IraIratId, String Id)
    {
        string valuePath = UgyiratId + "\\\\" + IraIratId + "\\\\" + Id;
        string js = saveSelectedNodeInJS(valuePath);
        return "<span url=\"KuldKuldemenyekForm.aspx\" id=\"" + Id + "\" modosithato=\"" + kuldemenyModosithato + "\"" +
               "onclick=\"" + js + "\">" + szoveg + "</span>";
    }

    private string getStringWithComma(string text)
    {
        if(String.IsNullOrEmpty(text.Trim()))
            return String.Empty;
        else
            return ", " + text;
    }

    private void SetListHeaderButtons()
    {
        string jscheckSelectedNodeBegin = "var selectedNodeID = " + treeViewMunkanaplo.ClientID + "_Data.selectedNodeID.value;" +
                                     "if(selectedNodeID == ''){alert('" + Resources.Error.UINoSelectedItem + "');return false;}" +
                                     "else {var selectedNode = $get(selectedNodeID); if(!selectedNode) {alert('" + Resources.Error.UINoSelectedItem + "');}else {";

        string jscheckSelectedNodeEnd = "}}";

        #region Megtekintes

        string url = "' + selectedNode.firstChild.attributes.getNamedItem('url').value + '";

        string queryView = QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + @"='
                               + selectedNode.firstChild.id + '";

        ParentListHeader.ViewOnClientClick = jscheckSelectedNodeBegin + JavaScripts.SetOnCLientClick_NoPostBack(url
               , queryView, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max) + jscheckSelectedNodeEnd;

        #endregion

        #region Modositas

        string jscheckModifyBegin = "var modosithato = selectedNode.firstChild.attributes.getNamedItem('modosithato').value; " +
                                    "if(!Boolean.parse(modosithato)){if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes + "')) {";
        string jscheckModifyMiddle = "}else {return false;}} else {";

        string jscheckModifyEnd = "}";

        string queryModify = QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + @"='
                               + selectedNode.firstChild.id + '";

        string jsModify = JavaScripts.SetOnClientClick(url, queryModify, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max,
                          UpdatePanelMunkanaplo.ClientID, EventArgumentConst.refreshMasterList);



        ParentListHeader.ModifyOnClientClick = jscheckSelectedNodeBegin + jscheckModifyBegin + jsModify
                                               + jscheckModifyMiddle + jsModify + jscheckModifyEnd + jscheckSelectedNodeEnd;

        #endregion

        ParentListHeader.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UpdatePanelMunkanaplo.ClientID);
    }

    private string getSzereltImage(DataRow ugyiratRow)
    {
        int szereltCount = 0;
        Int32.TryParse(ugyiratRow["SzereltCount"].ToString(), out szereltCount);
        if (szereltCount > 0)
        {
            return "<img src=\"images\\hu\\ikon\\szerelt.gif\" alt=\"szerelt\" height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
        }
        else
        {
            return "&nbsp";
        }
    }

    private string getCsatolasImage(DataRow ugyiratRow)
    {
        int csatolasCount = 0;
        Int32.TryParse(ugyiratRow["CsatolasCount"].ToString(), out csatolasCount);
        if (csatolasCount > 0)
        {
            return "<img src=\"images\\hu\\ikon\\csatolt.gif\" alt=\"csatolt\"  height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
        }
        else
        {
            return "&nbsp";
        }
    }

    private string getCsatolmanyImage(DataRow iratRow)
    {
        int csatolmanyCount = 0;
        Int32.TryParse(iratRow["CsatolmanyCount"].ToString(), out csatolmanyCount);
        if (csatolmanyCount > 0)
        {
            return "<img src=\"images\\hu\\ikon\\csatolmany.gif\" alt=\"csatolm�ny\"  height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
        }
        else
        {
            return "&nbsp";
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        if (!IsPostBack)
        {
            PopulateUgyiratok();
            SetListHeaderButtons();
        }
    }

    protected void UpdatePanelMunkanaplo_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    {
                        loadTreeViewState = true;
                        PopulateUgyiratok();
                        loadTreeViewState = false;
                    }
                    break;
            }
        }
    }

    protected void treeViewMunkanaplo_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        switch (e.Node.Depth)
        {
            case 0:
                PopulateIrat(e.Node);
                break;
            case 1:
                PopulateIratPeldanyOrKuldemeny(e.Node);
                break;
        }
    }

    private void PopulateUgyiratok()
    {
        //TreeView t�rl�se
        treeViewMunkanaplo.Nodes.Clear();

        #region Init
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
        ExecParam erec_UgyUgyiratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_UgyUgyiratokResult = new Result();
        #endregion

        #region Search be�ll�t�s
        erec_UgyUgyiratokSearch.Manual_Sajat.Value =
                    Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        erec_UgyUgyiratokSearch.Manual_Sajat.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_UgyUgyiratokResult = erec_UgyUgyiratokService.GetAllWithExtensionAndJogosultak(erec_UgyUgyiratokExecParam, erec_UgyUgyiratokSearch, true);
        if (!String.IsNullOrEmpty(erec_UgyUgyiratokResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s �s TreeNode-ok hozz�ad�sa
        DataSet erec_UgyUgyiratokDataSet = erec_UgyUgyiratokResult.Ds;

        foreach (DataRow rowUgyirat in erec_UgyUgyiratokDataSet.Tables[0].Rows)
        {
            //Modosithatosag be�ll�t�sa
            //Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotFromDataRow(rowUgyirat);
            //ugyiratModosithato = Ugyiratok.ModosithatoAllapotu(ugyiratStatusz);

            //Id be�ll�t�s
            string UgyUgyiratokId = rowUgyirat["Id"].ToString();

            //Text be�ll�t�s
            String UgyUgyiratokText = "<table border='1' cellspacing='0' cellpadding='0' class='" + this.getRowStyle() + "'><tr><td rowspan='2' class='TreeViewRowTD'><img src='images/hu/egyeb/treeview_ugyirat.gif' alt='�gyirat' />&nbsp;</td>";
            UgyUgyiratokText += "<td style='width: 190px;'>";

            UgyUgyiratokText += "<b>" + rowUgyirat["Foszam_Merge"].ToString() + "</b>";
            UgyUgyiratokText += "</td><td style='width: 180px;'>";
            UgyUgyiratokText += "<b>�llapot: </b>" + rowUgyirat["Allapot_Nev"].ToString() + "&nbsp;&nbsp;";
            UgyUgyiratokText += "</td><td style='width:300px;'>";
            UgyUgyiratokText += "<b> T�rgy: </b>" + rowUgyirat["Targy"].ToString() + "";
            UgyUgyiratokText += "</td><td style='width: 150px;'>";
            UgyUgyiratokText += "<b> �gyint�z�: </b>" + rowUgyirat["Ugyintezo_Nev"].ToString();
            UgyUgyiratokText += "</td><td style=\"width:" + imageWidth + ";\">" + this.getSzereltImage(rowUgyirat)
                              + "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolasImage(rowUgyirat) 
                              + "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(rowUgyirat)
                              + "</td></tr></table>";

            UgyUgyiratokText = SetUgyUgyiratUrl(UgyUgyiratokText, UgyUgyiratokId);

            TreeNode ugyiratTreeNode = new TreeNode(UgyUgyiratokText, UgyUgyiratokId);
            SetTreeNodeState(ugyiratTreeNode, 0);

            treeViewMunkanaplo.Nodes.Add(ugyiratTreeNode);

        }

        #endregion

    }

    private void PopulateIrat(TreeNode UgyUgyiratTreeNode)
    {
        #region Init
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);
        ExecParam erec_IraIratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIratokResult = new Result();
        #endregion

        #region Search be�ll�t�s
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = UgyUgyiratTreeNode.Value;
        erec_IraIratokSearch.OrderBy = "EREC_IraIratok.LetrehozasIdo ASC";
        erec_IraIratokResult = erec_IraIratokService.GetAllWithExtensionAndJogosultak(erec_IraIratokExecParam, erec_IraIratokSearch, true);
        if (!String.IsNullOrEmpty(erec_IraIratokResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_IraIratokDataSet = erec_IraIratokResult.Ds;
        string UgyUgyiratokId = UgyUgyiratTreeNode.Value;

        foreach (DataRow rowIrat in erec_IraIratokDataSet.Tables[0].Rows)
        {
            //Modosithatosag be�ll�t�sa
            //Iratok.Statusz iratStatusz = Iratok.GetAllapotFromDataRow(rowIrat);
            //iratModosithato = Iratok.ModosithatoAllapotu(iratStatusz, erec_IraIratokExecParam);

            string UgyUgyiratdarabokId = rowIrat["UgyUgyIratDarab_Id"].ToString();
            //Id be�ll�t�sa
            string IraIratokId = rowIrat["Id"].ToString();
            //Text be�ll�t�sa
            String IraIratokText = "<table border='1' cellspacing='0' cellpadding='0' class='" + this.getParentRowStyle(UgyUgyiratTreeNode) + "'><tr><td rowspan='2' class='TreeViewRowTD'><img src='images/hu/egyeb/treeview_irat.gif' alt='Irat' />&nbsp;</td>";
            IraIratokText += "<td style='width: 170px;'>";
            IraIratokText += rowIrat["IktatoSzam_Merge"].ToString();
            IraIratokText += "</td><td style='width: 180px;'>";

            string Allapot_nev = rowIrat["Allapot_nev"].ToString();
            string Allapot = rowIrat["Allapot"].ToString();
            string allapotHtml = Allapot_nev;
            if (Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott)
            {
                allapotHtml = "<span style='color: Red;'><b>" + Allapot_nev
                    + "(" + rowIrat["UjIktatoSzam"].ToString().Replace(" ", "&nbsp;") + ")" + "</b></span>";
            }

            IraIratokText += " <b>�llapot: </b>" + allapotHtml;
            IraIratokText += "</td><td style='width:300px;'>";
            IraIratokText += "  <b>T�rgy: </b>" + rowIrat["Targy1"].ToString();
            IraIratokText += "</td><td style='width: 150px;'>";
            IraIratokText += "  <b>�gyint�z�: </b>" + rowIrat["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();
            IraIratokText += "</td><td style='width: 200px;'>";
            IraIratokText += " <b>Iktat�s id�pontja: </b>" + rowIrat["IktatasDatuma"].ToString();
            IraIratokText += "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolasImage(rowIrat);
            IraIratokText += "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(rowIrat)
                             + "</td></tr></table>";

            IraIratokText = SetIraIratokUrl(IraIratokText, UgyUgyiratokId, UgyUgyiratdarabokId, IraIratokId);

            TreeNode iratTreeNode = new TreeNode(IraIratokText, IraIratokId);
            SetTreeNodeState(iratTreeNode, 1);

            UgyUgyiratTreeNode.ChildNodes.Add(iratTreeNode);
        }

        #endregion
    }

    private void PopulateIratPeldanyOrKuldemeny(TreeNode IratTreeNode)
    {
        #region Init
        EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
        ExecParam erec_KuldKuldemenyekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_KuldKuldemenyekResult = new Result();

        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
        ExecParam erec_PldIratPeldanyokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_PldIratPeldanyokResult = new Result();

        string UgyUgyiratokId = IratTreeNode.ValuePath.Split('/')[0];
        string UgyUgyiratdarabokId = IratTreeNode.ValuePath.Split('/')[1];
        string IraIratokId = IratTreeNode.Value;
        #endregion

        #region Irat lek�r�se
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam erec_IraIratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIratokResult = new Result();
        erec_IraIratokExecParam.Record_Id = IratTreeNode.Value;
        erec_IraIratokResult = erec_IraIratokService.Get(erec_IraIratokExecParam);
        if (!String.IsNullOrEmpty(erec_IraIratokResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region Ha bejovo keletkezesu az irat, van kuldemenye!
        if ((erec_IraIratokResult.Record as EREC_IraIratok).PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
        {
            erec_KuldKuldemenyekSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            erec_KuldKuldemenyekSearch.Id.Value = (erec_IraIratokResult.Record as EREC_IraIratok).KuldKuldemenyek_Id;
            erec_KuldKuldemenyekResult = erec_KuldKuldemenyekService.GetAllWithExtensionAndJogosultak(erec_KuldKuldemenyekExecParam, erec_KuldKuldemenyekSearch,true);
            if (!String.IsNullOrEmpty(erec_KuldKuldemenyekResult.ErrorCode))
            {
                return;
            }

            DataSet erec_KuldKuldemenyekDataSet = erec_KuldKuldemenyekResult.Ds;
            // Iratok:
            foreach(DataRow rowKuldemeny in erec_KuldKuldemenyekDataSet.Tables[0].Rows)
            {
                //Modosithatosag be�ll�t�sa
                //Kuldemenyek.Statusz kuldemenyStatusz = Kuldemenyek.GetAllapotFromDataRow(rowKuldemeny, IraIratokId);
                //kuldemenyModosithato = Kuldemenyek.Modosithato(kuldemenyStatusz, erec_KuldKuldemenyekExecParam);

                String KuldKuldemenyekId = rowKuldemeny["Id"].ToString();

                // TODO: kene majd egy custom checkbox is!
                String KuldKuldemenyekText = "<table border='1' cellspacing='0' cellpadding='0' class='" + this.getParentRowStyle(IratTreeNode) +"'><tr><td rowspan='2' class='TreeViewRowTD'><img src='images/hu/egyeb/treeview_kuldemeny.jpg' alt='K�ldem�ny (Bej�v� iratp�ld�ny)' />&nbsp;</td>";
                KuldKuldemenyekText += "<td style='width: 150px;'>";
                KuldKuldemenyekText += "<b>" + rowKuldemeny["FullErkeztetoSzam"].ToString() + "</b>";
                KuldKuldemenyekText += "</td><td style='width: 180px;'>";
                KuldKuldemenyekText += " <b>�llapot: </b>" + rowKuldemeny["AllapotNev"].ToString();
                KuldKuldemenyekText += "</td>";
                KuldKuldemenyekText += "<td style='width:300px;'>";
                KuldKuldemenyekText += " <b>K�ld�/felad�: </b>" + rowKuldemeny["NevSTR_Bekuldo"].ToString() + this.getStringWithComma(rowKuldemeny["CimSTR_Bekuldo"].ToString());
                KuldKuldemenyekText += "</td></tr></table>";

                KuldKuldemenyekText = SetKuldemenyekUrl(KuldKuldemenyekText, UgyUgyiratokId, UgyUgyiratdarabokId, IraIratokId, KuldKuldemenyekId);

                TreeNode tn = new TreeNode(KuldKuldemenyekText, KuldKuldemenyekId);
                SetTreeNodeState(tn, 2);

                IratTreeNode.ChildNodes.Add(tn);
            }
        }

        #endregion

        #region iratp�ld�nya mindig!
            erec_PldIratPeldanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            erec_PldIratPeldanyokSearch.IraIrat_Id.Value = (erec_IraIratokResult.Record as EREC_IraIratok).Id;
            erec_PldIratPeldanyokResult = erec_PldIratPeldanyokService.GetAllWithExtensionAndJogosultak(erec_PldIratPeldanyokExecParam, erec_PldIratPeldanyokSearch, true);
            if (!String.IsNullOrEmpty(erec_PldIratPeldanyokResult.ErrorCode))
            {
                return;
            }

            DataSet erec_PldIratPeldanyokDataSet = erec_PldIratPeldanyokResult.Ds;
            // Iratok:
            foreach (DataRow rowIratPeldany in erec_PldIratPeldanyokDataSet.Tables[0].Rows)
            {
                //Modosithatosag be�ll�t�sa
                //IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotFromDataRow(rowIratPeldany);
                //iratPeldanyModosithato = IratPeldanyok.ModosithatoAllapotu(iratPeldanyStatusz, erec_PldIratPeldanyokExecParam);

                String PldIratPeldanyokId = rowIratPeldany["Id"].ToString();

                String PldIratPeldanyokText = "<table border='1' cellspacing='0' cellpadding='0' class='" + this.getParentRowStyle(IratTreeNode) + "'><tr><td rowspan='2'><img src='images/hu/egyeb/treeview_belsoirat.jpg' alt='Bels� iratp�ld�ny' />&nbsp;</td>";
                PldIratPeldanyokText += "<td style='width: 150px;'>";
                PldIratPeldanyokText += "<b>" + rowIratPeldany["Sorszam"].ToString() + ". p�ld�ny </b>";
                PldIratPeldanyokText += "</td><td style='width: 180px;'>";
                PldIratPeldanyokText += " <b>�llapot: </b>" + rowIratPeldany["Allapot_Nev"].ToString();
                PldIratPeldanyokText += "</td>";
                PldIratPeldanyokText += "<td style='width:300px;'>";
                PldIratPeldanyokText += " <b>C�mzett: </b>" + rowIratPeldany["NevSTR_Cimzett"].ToString() + this.getStringWithComma(rowIratPeldany["CimSTR_Cimzett"].ToString());
                PldIratPeldanyokText += "</td></tr></table>";

                PldIratPeldanyokText = SetPldIratPeldanyokUrl(PldIratPeldanyokText, UgyUgyiratokId, UgyUgyiratdarabokId, IraIratokId, PldIratPeldanyokId);

                TreeNode tn = new TreeNode(PldIratPeldanyokText, PldIratPeldanyokId);
                SetTreeNodeState(tn, 2);

                IratTreeNode.ChildNodes.Add(tn);
            }
        #endregion
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {       
        ParentListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ParentListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Modify);
    }



    protected void treeViewMunkanaplo_SelectedNodeChanged(object sender, EventArgs e)
    {

    }
}
