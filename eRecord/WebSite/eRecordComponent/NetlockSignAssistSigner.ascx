﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetlockSignAssistSigner.ascx.cs" Inherits="eRecordComponent_NetlockSignAssistSigner" %>

 <div id="UpdateProgressPanel" style="position: fixed; 	top: 250px; 	left: 400px; 	width: 250px; 	height: 100px; display:none;">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <table>
            <tr>
            <td>
                <img src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="" />
            </td>
            <td class="updateProgressText">
                <span id="updateProgressText">Feldolgozás folyamatban...</span>
            </td>
            </tr>
        </table>
   </eUI:eFormPanel> 
</div>

        <script type="text/javascript">
            function log_text(message) {
                hideUpdateProgress();
                alert(message);
            }

            function showUpdateProgress (text) {
                if (text) {
                    $('#updateProgressText').html(text);
                }
                $('#UpdateProgressPanel').show();
            }

            function hideUpdateProgress () {
                $('#UpdateProgressPanel').hide();
            }

            function closeWindow() {
                window.returnValue = true;
                if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }
                window.close();
            }

            function NetLockSignAssist_Sign() {
                showUpdateProgress('Feldolgozás folyamatban...');

                var netLockSignAssistForIratok = new NetLockSignAssistForIratok(window.hwcrypto,
                    '<%=this.NetlockSignAssistUrl%>',
                     <%=this.IratokToSignSerialized %>,
                    '<%=this.SignatureLevel%>',
               function (signedIrat) {
                   var params = { signedIrat: signedIrat };
                   $.ajax({
                       type: "POST",
                       url: '<%=this.AsyncPostBackUrl%>',
                       data: JSON.stringify(params),
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       success: function (msg) {
                           if (msg.d)
                               log_text(msg.d);
                           else
                               netLockSignAssistForIratok.SignNextIrat();
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           log_text(xhr.responseText);
                       },
                       timeout: 120000
                   });

                        }, function () {
                            hideUpdateProgress();
                            closeWindow();
               },
                    log_text);

               netLockSignAssistForIratok.Sign();
        }
    </script>
