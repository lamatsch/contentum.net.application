﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_NetlockSignAssistSigner : System.Web.UI.UserControl, IScriptControl
{

    protected string NetlockSignAssistUrl
    {
        get
        {
            return WebConfigurationManager.AppSettings["NetlockSignAssistUrl"]; //https://nltokendemo.hu:8443/api
        }
    }

    protected string SignatureLevel
    {
        get
        {
            return "PAdES_BASELINE_B";
        }
    }

    public string AsyncPostBackUrl
    {
        get;
        set;
    }

    [Serializable]
    public class FileProperties
    {
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public string SignedFileData { get; set; }
        public string IratId { get; set; }
        public string ProcId { get; set; }
        public string DokumentumId { get; set; }
        public string CsatolmanyId { get; set; }
        public string FolyamatTetelId { get; set; }
        public bool IsLastFile { get; set; }
        public string Error { get; set; }
    }

    [Serializable]
    public class IratFiles
    {
        public string IratId { get; set; }

        public List<FileProperties> Files { get; set; }
    }

    private List<IratFiles> _IratokToSign = new List<IratFiles>();

    public List<IratFiles> IratokToSign
    {
        get { return _IratokToSign; }
        set { _IratokToSign = value; }
    }

    public string IratokToSignSerialized
    {
        get
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(this.IratokToSign);
        }
    }

    public string SignJSFunction
    {
        get
        {
            return "NetLockSignAssist_Sign()";
        }
    }

    public void LoadFiles(string procId, string[] csatolmanyokArray)
    {
        EREC_Alairas_Folyamat_TetelekService tetelekService = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
        EREC_Alairas_Folyamat_TetelekSearch tetelekSearch = new EREC_Alairas_Folyamat_TetelekSearch();
        tetelekSearch.AlairasFolyamat_Id.Value = procId;
        tetelekSearch.AlairasFolyamat_Id.Operator = Query.Operators.equals;

        Result tetekResult = tetelekService.GetAll(UI.SetExecParamDefault(Page), tetelekSearch);
        tetekResult.CheckError();

        Dictionary<string, string> csatFolyamat = new Dictionary<string, string>();

        foreach (DataRow row in tetekResult.Ds.Tables[0].Rows)
        {
            string tetelId = row["Id"].ToString();
            string csatolmanyId = row["Csatolmany_Id"].ToString();
            csatFolyamat.Add(csatolmanyId, tetelId);
        }

        EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

        EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();
        search_csatolmanyok.Id.Value = Search.GetSqlInnerString(csatolmanyokArray);
        search_csatolmanyok.Id.Operator = Query.Operators.inner;

        Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);


        List<string> dokumentumIdList = new List<string>();
        Dictionary<string, string> dokIrat = new Dictionary<string, string>();
        Dictionary<string, string> dokCsat = new Dictionary<string, string>();
        foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
        {
            string dokumentumId = row["Dokumentum_Id"].ToString();
            dokumentumIdList.Add(dokumentumId);

            string iraIratId = row["IraIrat_Id"].ToString();
            dokIrat.Add(dokumentumId, iraIratId);
            string csatolmanyId = row["Id"].ToString();
            dokCsat.Add(dokumentumId, csatolmanyId);
        }

        Contentum.eDocument.Service.KRT_DokumentumokService svcDokumentumok = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
        schDokumentumok.Id.Value = Search.GetSqlInnerString(dokumentumIdList.ToArray());
        schDokumentumok.Id.Operator = Query.Operators.inner;

        Result resDokumentumok = svcDokumentumok.GetAll(UI.SetExecParamDefault(Page), schDokumentumok);


        List<FileProperties> filesToSign = new List<FileProperties>();

        foreach (DataRow row in resDokumentumok.Ds.Tables[0].Rows)
        {
            string id = row["Id"].ToString();
            string fileName = row["FajlNev"].ToString();
            string externalLink = row["External_Link"].ToString();
            string formatum = row["Formatum"].ToString();
            string fileUrl = this.ResolveClientUrl(String.Format("~/GetDocumentContent.aspx?id={0}&redirected=true&disableChunking=1", id));

            if (IsPkiFile(formatum))
            {
                FileProperties file = new FileProperties();
                file.FileName = fileName;
                file.FileUrl = HttpUtility.UrlPathEncode(fileUrl);
                file.IratId = dokIrat[id];
                file.ProcId = procId;
                file.DokumentumId = id;
                file.CsatolmanyId = dokCsat[id];
                file.FolyamatTetelId = csatFolyamat.ContainsKey(file.CsatolmanyId) ? csatFolyamat[file.CsatolmanyId] : String.Empty;

                filesToSign.Add(file);
            }
        }

        List<IratFiles> iratokToSign = new List<IratFiles>();

        foreach (string iratId in filesToSign.Select(f => f.IratId).Distinct())
        {
            IratFiles irat = new IratFiles();
            irat.IratId = iratId;
            irat.Files = filesToSign.Where(f => f.IratId == iratId).ToList();
            iratokToSign.Add(irat);
        }

        this.IratokToSign = iratokToSign;
    }

    bool IsPkiFile(string formatum)
    {
        if (!String.IsNullOrEmpty(formatum))
        {
            string pkiFileTypes = Rendszerparameterek.Get(Page, "PKI_FILETYPES");

            if (String.IsNullOrEmpty(pkiFileTypes))
            {
                pkiFileTypes = "pdf";
            }

            if (pkiFileTypes.ToLower().Contains(formatum.ToLower()))
                return true;
        }

        return false;
    }

    public static string IratSigned(IratFiles signedIrat)
    {
        try
        {
            ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
            foreach (FileProperties signedFile in signedIrat.Files)
            {
                UploadSignedFile(execParam, signedFile);
                SetFolyamatTetelStatus(execParam, signedFile.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas, String.Empty);
            }
            IratCsatolmanyAlairas(execParam, signedIrat, String.Empty);
        }
        catch (Exception ex)
        {
            Result error = ResultException.GetResultFromException(ex);
            return ResultError.GetErrorMessageFromResultObject(error);
        }

        return String.Empty;
    }

    static void UploadSignedFile(ExecParam execParam, FileProperties signedFile)
    {
        Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        string filename = signedFile.FileName;

        byte[] documentData = Convert.FromBase64String(signedFile.SignedFileData);
        Result spUploadResult = eDocumentService.UploadSignedFile(execParam.Clone(), signedFile.IratId, signedFile.DokumentumId, String.Empty, String.Empty, String.Empty, filename, documentData);

        if (spUploadResult.IsError)
        {
            Logger.Error(String.Format("UploadSignedFile ({0}) hiba - eDocumentService.UploadSignedFile", signedFile.FileName), execParam, spUploadResult);
            SetFolyamatTetelStatus(execParam, signedFile.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, ResultError.GetErrorMessageFromResultObject(spUploadResult));
            throw new ResultException(spUploadResult);
        }
    }

    static void IratCsatolmanyAlairas(ExecParam execParam, IratFiles signedIrat, string megjegyzes)
    {
        Contentum.eDocument.Service.DokumentumAlairasService service = eDocumentService.ServiceFactory.GetDokumentumAlairasService();
        ExecParam execParamAlairas = execParam.Clone();

        Result resultCsatolmanyokAlairas = service.IratCsatolmanyokAlairas(execParamAlairas, signedIrat.Files.Select(f => f.CsatolmanyId).ToArray(), megjegyzes);

        if (resultCsatolmanyokAlairas.IsError)
        {
            Logger.Error(String.Format("IratCsatolmanyAlairas ({0}) -  DokumentumAlairasService.IratCsatolmanyokAlairas hiba", signedIrat.IratId), execParamAlairas, resultCsatolmanyokAlairas);
            throw new ResultException(resultCsatolmanyokAlairas);
        }
    }

    static void SetFolyamatTetelStatus(ExecParam execParam, string folyamatTetelId, string status, string hiba)
    {
        Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParam, folyamatTetelId, status, hiba);

        if (resTetel.IsError)
        {
            Logger.Error("SetFolyamatTetelStatus hiba -  eDocumentService.SetFolyamatTetelStatus", execParam, resTetel);
        }
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        List<ScriptReference> references = new List<ScriptReference>()
        {
                new ScriptReference("~/netlockSignAssist/jquery-1.9.0.js"),
                new ScriptReference("~/netlockSignAssist/bootstrap.min.js"),
                new ScriptReference("~/netlockSignAssist/knockout-2.2.1.js"),
                new ScriptReference("~/netlockSignAssist/hwcrypto-legacy.js"),
                new ScriptReference("~/netlockSignAssist/hwcrypto.js"),
                new ScriptReference("~/netlockSignAssist/hex2base.js"),
                new ScriptReference("~/netlockSignAssist/jquery_002.js"),
                new ScriptReference("~/netlockSignAssist/NetLockSignAssist.js")

        };

        return references;
    }

    #endregion

}