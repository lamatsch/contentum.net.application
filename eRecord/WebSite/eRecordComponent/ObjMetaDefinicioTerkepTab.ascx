<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjMetaDefinicioTerkepTab.ascx.cs" Inherits="eRecordComponent_ObjMetaDefinicioTerkepTab" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
    
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %> 
<%@ Register Src="../Component/TreeViewHeader.ascx" TagName="TreeViewHeader" TagPrefix="tvh" %> 
   
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <tvh:TreeViewHeader ID="TreeViewHeader1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
<%--    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />--%>
    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always" OnLoad="TreeViewUpdatePanel_Load">
        <ContentTemplate>
        
    <%-- NodeFilter --%>
    <asp:HiddenField ID="IsFilteredWithoutHit_HiddenField" runat="server" />
    <asp:HiddenField ID="ObjMetaDefinicioFilter_HiddenField" runat="server" />
    <asp:HiddenField ID="ObjMetaAdataiFilter_HiddenField" runat="server" />
   <%-- /NodeFilter --%>   
     
     <asp:Panel ID="MainPanel" runat="server" Visible="true">
     
    <ajaxToolkit:CollapsiblePanelExtender ID="MetaAdatokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="400"
        ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>   
         
         <table style="width: 100%;">
             <tr>
                <td style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100%; text-align: left; border: solid 1px Silver; ">
                                <%-- az ujonnan letrehozott node-ok kivalasztasahoz szukseges adatok tarolasara --%>
                                <asp:HiddenField ID="SelectedNodeId_HiddenField" runat="server" />
                                <asp:HiddenField ID="SelectedNodeName_HiddenField" runat="server" />
                                <asp:Panel ID="Panel1" runat="server" Visible="true">
                                    <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="false" 
                                                OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"
                                                OnTreeNodePopulate="TreeView1_TreeNodePopulate" BackColor="White"
                                                ShowLines="True"
                                                PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip=""
                                                SkipLinkText="">
                                               <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                                    </asp:TreeView>
                                     
                                </asp:Panel> 
                            </td>                                   
                        </tr>
                               
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
