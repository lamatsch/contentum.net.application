//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class eRecordComponent_ObjMetaDefinicioTerkepTab : System.Web.UI.UserControl
{
    public enum NodeType { Unknown, ObjMetaDefinicio, ObjMetaAdat };
    private String[] _NodeTypeClass = { "", "", "TreeViewCustomNode" };
    
    //private const string kcs_DEFINICIOTIPUS = "OBJMETADEFINICIO_TIPUS";
    
    private const string strSorszam = "sorsz�m";
    private const string valueOpcionalis0 = "k�telez�";
    private const string valueOpcionalis1 = "opcion�lis";
    private const string valueIsmetlodo0 = "egyszeri";
    private const string valueIsmetlodo1 = "ism�tl�d�";
    private const string strFunkcio = "<b> Funkci�: </b>";
    private const string strFelettes = "<b> K�zvetlen&nbsp;felettes: </b>";
    private const string strSzinkronizalt = "szinkroniz�lt";
    private const string strOrokolt = "eredet";

    private const string strMetaDefinicioNodeNev = "Definicio";
    private const string strMetaDefinicioNodeText = "Metadefin�ci�";

    private const string strIconPathMetaDefinicio = "images/hu/egyeb/treeview_metadefinicio.gif";
    private const string strIconAltMetaDefinicio = "Metadefin�ci�";
    private const string strIconPathMetaAdat = "images/hu/egyeb/treeview_metaadat.gif";
    private const string strIconAltMetaAdat = "Metaadat";

    private const string strMetaAdatNodeNev = "Metaadat";
    private const string strMetaAdatNodeText = "Metaadat";
    //private const string strCustomNodeClass = "TreeViewCustomNode";
    //private const string strCustomNodeClassInherited = "TreeViewCustomNodeInherited";

    private string ItemFormatString = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>{0}&nbsp;</td><td style='width: 500px;'>{1}</td><td style='width: 100%;'>{2}</td></tr></table>";
    private string ClassFormatString = " class='{0}'";

    //private bool IsFilteredWithoutHit;
    //private string ObjMetaDefinicioFilter = "";
    //private string ObjMetaAdataiFilter = "";

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            //MainPanel.Visible = value;
            TreeViewHeader1.Visible = value;
            if (!value)
            {
                ClearTreeView();
            }
        }
    }

    public string Command = "";
    private PageView pageView = null;

    private String ParentId = "";

    private string RegisterClientScriptDeleteConfirm()
    {
        string js = "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n\\n"
        + "Biztosan t�rli a kijel�lt csom�pontot �s lesz�rmazottait?"
        + "')) {  } else return false;";
        return js;
    }

    private string RegisterClientScriptSetElementValue(string componentId, string value)
    {
        return "var element = window.document.getElementById('" + componentId + "'); if (element == null) return; else element.value = '" + value + "';";
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;


    private String GetNodeTypeClass(NodeType nodeType)
    {
        return _NodeTypeClass[(int)nodeType];
    }


    public bool IsFilteredWithoutHit
    {
        get { return (IsFilteredWithoutHit_HiddenField.Value == "1" ? true : false); }
        set { IsFilteredWithoutHit_HiddenField.Value = (value == true ? "1" : "0"); }
    }

    public string ObjMetaDefinicioFilter
    {
        get { return ObjMetaDefinicioFilter_HiddenField.Value; }
        set { ObjMetaDefinicioFilter_HiddenField.Value = value; }
    }

    public string ObjMetaAdataiFilter
    {
        get { return ObjMetaAdataiFilter_HiddenField.Value; }
        set { ObjMetaAdataiFilter_HiddenField.Value = value; }
    }
    
    #region FilterUtils

    private void SetNodeFilter(bool isFilteredWithoutHit, string objMetaDefinicioFilter, string objMetaAdataiFilter)
    {
        IsFilteredWithoutHit = isFilteredWithoutHit;
        ObjMetaDefinicioFilter = objMetaDefinicioFilter;
        ObjMetaAdataiFilter = objMetaAdataiFilter;
    }

    private void ClearNodeFilter()
    {
        IsFilteredWithoutHit = false;
        ObjMetaDefinicioFilter = "";
        ObjMetaAdataiFilter = "";
    }
    
    /// <summary>
    /// Egy (szimpla id�z�jelek k�z�tt l�v�, vagy csak felsorolt) elemekb�l (Id-k) �ll�,
    /// vessz�kkel elv�lasztott list�ban megkeresi az �tadott Id-t, �s visszaadja a sorsz�m�t
    /// </summary>
    /// <param name="strElement">A keresett elem (Id)</param>
    /// <param name="strFilter">Az elemek vessz�vel tagolt sorozata, az egyes Id-k lehetnek szimpla id�z�jelben is.</param>
    /// <returns>-1, ha az elem nem tal�lhat�, egy�bk�nt az els� el�fordul�s indexe</returns>
    public static int findInFilter(string strElement, string strFilter)
    {
        char[] Separators = new char[1];
        Separators[0] = ',';

        int iPosition = -1;

        string[] arrayFilter = strFilter.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
        int i = -1;
        foreach (String item in arrayFilter)
        {
            i++;
            if (item.Replace("'", "") == strElement)
            {
                iPosition = i;
                break;
            }
        }

        return iPosition;

    }

    /// <summary>
    /// Egy (szimpla id�z�jelek k�z�tt l�v�, vagy csak felsorolt) elemekb�l (Id-k) �ll�,
    /// vessz�kkel elv�lasztott list�ban megvizsg�lja, hogy az �tadott Id szerepel-e benne
    /// </summary>
    /// <param name="strElement">A keresett elem (Id)</param>
    /// <param name="strFilter">Az elemek vessz�vel tagolt sorozata, az egyes Id-k lehetnek szimpla id�z�jelben is.</param>
    /// <returns>False, ha az elem nem tal�lhat�, egy�bk�nt True</returns>
    public static bool isInFilter(string strElement, string strFilter)
    {
        return (findInFilter(strElement, strFilter) > -1 ? true : false);
    }

    #endregion FilterUtils

    public char ValuePathSeparator
    {
        get { return TreeView1.PathSeparator; }
        set { TreeView1.PathSeparator = value; }
    }

    #region TreeViewUtils
    public void DeleteChildrenNodes(TreeNode node)
    {
        if (node != null)
        {
            foreach (TreeNode tn in node.ChildNodes)
            {
                // csak a Dictionaryt �r�ti
                RemoveNodesFromDictionaries(tn);
            }
            // node-ok t�nyleges t�rl�se
            node.ChildNodes.Clear();
        }
    }

    public void DeleteNode(TreeNode node)
    {
        if (node != null)
        {
            if (TreeView1.SelectedNode == node)
            {
                DeleteSelectedNode();
            }
            else
            {
                RemoveNodesFromDictionaries(node);
                node.Parent.ChildNodes.Remove(node);
            }
        }
    }

    public void DeleteSelectedNode()
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeNode ParentNode = TreeView1.SelectedNode.Parent;
            if (ParentNode != null)
            {
                RemoveNodesFromDictionaries(TreeView1.SelectedNode);
                ParentNode.ChildNodes.Remove(TreeView1.SelectedNode);
                ParentNode.Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
        }

    }

    // hierarchikusan t�rli a Dictionary-kb�l a node-oknak megfelel� Id-et
    private void RemoveNodesFromDictionaries(TreeNode treeNode)
    {
        if (treeNode == null) return;

        foreach (TreeNode tn in treeNode.ChildNodes)
        {
            RemoveNodesFromDictionaries(tn);
        }

        if (treeNode.ChildNodes.Count == 0)
        {
            string Id = treeNode.Value;
            NodeType nodeType =  GetNodeTypeById(treeNode.Value);

            switch (nodeType)
            {
                case NodeType.ObjMetaDefinicio:
                    if (Obj_MetaDefinicioDictionary.ContainsKey(Id))
                        Obj_MetaDefinicioDictionary.Remove(Id);
                    break;
                case NodeType.ObjMetaAdat:
                    if (Obj_MetaAdataiDictionary.ContainsKey(Id))
                        Obj_MetaAdataiDictionary.Remove(Id);
                    break;
                default:
                    break;
            }
        }
    }


    public void ClearTreeView()
    {
        // Dictionary-k t�rl�se:
        Obj_MetaDefinicioDictionary.Clear();
        Obj_MetaAdataiDictionary.Clear();

        TreeView1.Nodes.Clear();
    }

    // TreeView node selected node gyermek kiv�laszt�sa Id alapj�n
    // felt�telezi az Id-k egyedis�g�t, t�pust�l f�ggetlen�l
    public void SelectChildNode(String Id)
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.Expanded = true;

            if (!String.IsNullOrEmpty(Id))
            {
                foreach (TreeNode tn in TreeView1.SelectedNode.ChildNodes)
                {
                    if (tn.Value == Id)
                    {
                        tn.Selected = true;
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                        return;
                    }
                }
            }
        }

    }

    // TreeView friss�t�se a kijel�lt sor szintj�ig
    public void ExpandSelectedNode()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        selectedTreeNode.Expanded = true;
        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

    }

    #endregion TreeViewUtils


    #region dictionary item oszt�lyok

    [Serializable]
    private class Obj_MetaAdataiItem
    {
        private string _Id;
        private string _Obj_MetaDefinicio_Id;
        private string _Targyszavak_Id;
        private string _TargySzavak;
        private int _Sorszam;
        private string _Funkcio;
        private string _Ismetlodo;
        private string _Opcionalis;
        private string _SPSSzinkronizalt;
        private int _Szint;

        public Obj_MetaAdataiItem()
        {
            _Id = "";
            _Obj_MetaDefinicio_Id = "";
            _Targyszavak_Id = "";
            _TargySzavak = "";
            _Sorszam = 0;
            _Funkcio = "";
            _Ismetlodo = "";
            _Opcionalis = "";
            _SPSSzinkronizalt = "0";
            _Szint = 0;
        }

        public Obj_MetaAdataiItem(string Id, string Obj_MetaDefinicio_Id, string Targyszavak_Id, string TargySzavak,
            int Sorszam, string Funkcio, string Ismetlodo, string Opcionalis, string SPSSzinkronizalt)
        {
            _Id = Id;
            _Obj_MetaDefinicio_Id = Obj_MetaDefinicio_Id;
            _Targyszavak_Id = Targyszavak_Id;
            _TargySzavak = TargySzavak;
            _Sorszam = Sorszam;
            _Funkcio = Funkcio;
            _Ismetlodo = Ismetlodo;
            _Opcionalis = Opcionalis;
            _SPSSzinkronizalt = SPSSzinkronizalt;
        }

        public Obj_MetaAdataiItem(string Id, string Obj_MetaDefinicio_Id, string Targyszavak_Id, string TargySzavak,
            int Sorszam, string Funkcio, string Ismetlodo, string Opcionalis, string SPSSzinkronizalt, int Szint)
        {
            _Id = Id;
            _Obj_MetaDefinicio_Id = Obj_MetaDefinicio_Id;
            _Targyszavak_Id = Targyszavak_Id;
            _TargySzavak = TargySzavak;
            _Sorszam = Sorszam;
            _Funkcio = Funkcio;
            _Ismetlodo = Ismetlodo;
            _Opcionalis = Opcionalis;
            _SPSSzinkronizalt = SPSSzinkronizalt;
            _Szint = Szint;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Obj_MetaDefinicio_Id
        {
            get { return _Obj_MetaDefinicio_Id; }
            set { _Obj_MetaDefinicio_Id = value; }
        }

        public string Targyszavak_Id
        {
            get { return _Targyszavak_Id; }
            set { _Targyszavak_Id = value; }
        }

        public string TargySzavak
        {
            get { return _TargySzavak; }
            set { _TargySzavak = value; }
        }

        public int Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }

        public string Funkcio
        {
            get { return _Funkcio; }
            set { _Funkcio = value; }
        }

        public string Ismetlodo
        {
            get { return _Ismetlodo; }
            set { _Ismetlodo = value; }
        }

        public string Opcionalis
        {
            get { return _Opcionalis; }
            set { _Opcionalis = value; }
        }

        public int Szint
        {
            get { return _Szint; }
            set { _Szint = value; }
        }

        #endregion properties

    }

    [Serializable]
    private class Obj_MetaDefinicioItem
    {
        private string _Id;
        private string _Objtip_Id;
        private string _Objtip_Id_Column;
        private string _ColumnValue; // ColumnValue
        private string _DefinicioTipus; // KodTarak.OBJMETADEFINICIO_TIPUS
        private string _Felettes_Obj_Meta;
        private string _Felettes_Obj_Meta_CTT;
        private string _ContentType;
        private string _SPSSzinkronizalt;
        //private int _Szint;

        public Obj_MetaDefinicioItem()
        {
            _Id = "";
            _DefinicioTipus = "";
            _Objtip_Id = "";
            _Objtip_Id_Column = "";
            _ColumnValue = "";
            _Felettes_Obj_Meta = "";
            _Felettes_Obj_Meta_CTT = "";
            _ContentType = "";
            _SPSSzinkronizalt = "0";
            //_Szint = 0;
        }

        public Obj_MetaDefinicioItem(string Id, string DefinicioTipus, string Objtip_Id, string Objtip_Id_Column,
            string ColumnValue, string Felettes_Obj_Meta, string Felettes_Obj_Meta_CTT, string ContentType, string SPSSzinkronizalt
            //, int Szint
            )
        {
            _Id = Id;
            _DefinicioTipus = DefinicioTipus;
            _Objtip_Id = Objtip_Id;
            _Objtip_Id_Column = Objtip_Id_Column;
            _ColumnValue = ColumnValue;
            _Felettes_Obj_Meta = Felettes_Obj_Meta;
            _Felettes_Obj_Meta_CTT = Felettes_Obj_Meta_CTT;
            _ContentType = ContentType;
            _SPSSzinkronizalt = SPSSzinkronizalt;
            //_Szint = Szint;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string DefinicioTipus
        {
            get { return _DefinicioTipus; }
            set { _DefinicioTipus = value; }
        }
        
        public string Objtip_Id
        {
            get { return _Objtip_Id; }
            set { _Objtip_Id = value; }
        }

        public string Objtip_Id_Column
        {
            get { return _Objtip_Id_Column; }
            set { _Objtip_Id_Column = value; }
        }

        public string ColumnValue
        {
            get { return _ColumnValue; }
            set { _ColumnValue = value; }
        }

        public string Felettes_Obj_Meta
        {
            get { return _Felettes_Obj_Meta; }
            set { _Felettes_Obj_Meta = value; }
        }

        public string Felettes_Obj_Meta_CTT
        {
            get { return _Felettes_Obj_Meta_CTT; }
            set { _Felettes_Obj_Meta_CTT = value; }
        }

        public string ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }

        public string SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }
        
        //public int Szint
        //{
        //    get { return _Szint; }
        //    set { _Szint = value; }
        //}

        #endregion properties

    }

    #endregion dictionary item oszt�lyok


    #region Base Tree Dictionary
    System.Collections.Generic.Dictionary<String, Obj_MetaDefinicioItem> Obj_MetaDefinicioDictionary = new System.Collections.Generic.Dictionary<string, Obj_MetaDefinicioItem>();
    System.Collections.Generic.Dictionary<String, Obj_MetaAdataiItem> Obj_MetaAdataiDictionary = new System.Collections.Generic.Dictionary<string, Obj_MetaAdataiItem>();
    #endregion

    #region public Properties
    
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion


    #region Utils

    private String GetIdListFromTable(DataTable table, string Separator, bool bQuoteId)
    {
        List<String> Ids = new List<string>();
        if (table == null) return "";

        foreach (DataRow row in table.Rows)
        {
            string row_Id = row["Id"].ToString();
            if (bQuoteId)
            {
                row_Id = "'" + row_Id + "'";
            }

            Ids.Add(row_Id);
        }

        return String.Join(Separator, Ids.ToArray());
    }

    /// <summary>
    /// A viewstate-ben elt�rolt dictionary-k alapj�n eld�nti,
    /// hogy az adott id objektum metadefin�ci�, objektum metaadat, stb...
    /// </summary>    
    private NodeType GetNodeTypeById(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            if (Obj_MetaDefinicioDictionary != null && Obj_MetaDefinicioDictionary.ContainsKey(id))
            {
                // �gyirat:
                return NodeType.ObjMetaDefinicio;
            }
            else if (Obj_MetaAdataiDictionary != null && Obj_MetaAdataiDictionary.ContainsKey(id))
            {
                return NodeType.ObjMetaAdat;
            }
            else
            {
                return NodeType.Unknown;
            }
        }
        else
        {
            return NodeType.Unknown;
        }
    }

    //public void FindNodeByValue(TreeNodeCollection n, string val)
    //{
    //    if (intNodeFound) return;

    //    for (int i = 0; i < n.Count; i++)
    //    {
    //        if (n[i].Value == val)
    //        {
    //            n[i].Select();
    //            intNodeFound = true;
    //            return;
    //        }
    //        n[i].Expand();
    //        FindNodeByValue(n[i].ChildNodes, val);
    //        if (intNodeFound) return;
    //        n[i].Collapse();
    //    }
    //}

    public bool FindNodeByValue(TreeNodeCollection treeNodes, string value)
    {
        if (treeNodes == null) return false;
        for (int i = 0; i < treeNodes.Count; i++)
        {
            if (treeNodes[i].Value == value)
            {
                treeNodes[i].Select();
                return true;
            }
            treeNodes[i].Expand();
            if (FindNodeByValue(treeNodes[i].ChildNodes, value)) return true;
            treeNodes[i].Collapse();
        }

        return false;
    }

    #endregion Utils

    
    protected void Page_Init(object sender, EventArgs e)
    {        
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);


        TreeViewHeader1.HeaderLabel = "Objektum metadefin�ci�/metaadat t�rk�p";
        TreeViewHeader1.SearchObjectType = typeof(MetaAdatHierarchiaSearch);
        TreeViewHeader1.SetFilterFunctionButtonsVisible(true);
        TreeViewHeader1.SetNodeFunctionButtonsVisible(true);
        TreeViewHeader1.SetChildFunctionButtonsVisible(true);
        TreeViewHeader1.SetCustomNodeFunctionButtonsVisible(true);

        TreeViewHeader1.HeaderCustomNodeButtonsLabel = strMetaDefinicioNodeText;
        TreeViewHeader1.AttachedTreeView = TreeView1;

        TreeViewHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("MetaAdatokHierarchiaSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TreeViewHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        TreeViewHeader1.NodeFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderNodeFunctionButtonsClick);
        TreeViewHeader1.ChildFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderChildFunctionButtonsClick);

        TreeViewHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TreeViewUpdatePanel.ClientID);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

             //ViewState -bol visszatolti az elmentett statuszokat
            try
            {
                if (ViewState["Obj_MetaAdataiDictionary"] != null)
                {
                    Obj_MetaAdataiDictionary = (System.Collections.Generic.Dictionary<String, Obj_MetaAdataiItem>)ViewState["Obj_MetaAdataiDictionary"];
                }

                if (ViewState["Obj_MetaDefinicioDictionary"] != null)
                {
                    Obj_MetaDefinicioDictionary = (System.Collections.Generic.Dictionary<String, Obj_MetaDefinicioItem>)ViewState["Obj_MetaDefinicioDictionary"];
                }

            }
            catch
            { }
        }
        else
        {
            //ReLoadTab();        
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    private void TreeViewHeaderNodeFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Invalidate:
                InvalidateSelectedNode();
                break;
            case CommandName.ExpandNode:
                TreeView1.SelectedNode.ExpandAll();
                break;
        }

    }

    private void TreeViewHeaderChildFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        //switch (e.CommandName)
        //{
        //}
    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        //if (Command == CommandName.View)
        //{
        //    if (Request.Params["__EVENTARGUMENT"] != null)
        //    {
        //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
        //        switch (eventArgument)
        //        {
        //            case EventArgumentConst.refreshDetailList:
        //                KeresesClick(sender, null);
        //                break;
        //        }
        //    }
        //}
        //else if (Command == CommandName.Modify)
        //{
        //    if (Request.Params["__EVENTARGUMENT"] != null)
        //    {
        //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
        //        switch (eventArgument)
        //        {
        //            case EventArgumentConst.refreshSelectedNode:// m�dos�t�s
        //                RefreshSelectedNode();
        //                break;
        //            case EventArgumentConst.refreshMasterList: // gyerek felv�tele
        //                RefreshSelectedNodeChildren();
        //                ExpandSelectedNode();
        //                // l�trehoz�skor, ig�ny szerint kiv�lasztjuk az �j node-ot
        //                if (cbKivalasztUjElem.Checked == true)
        //                {
        //                    String SelectedId = SelectedNodeId_HiddenField.Value;
        //                    SelectChildNode(SelectedId);
        //                }
        //                //KeresesClick(sender, null);
        //                break;
        //            case EventArgumentConst.refreshDetailList:
        //                KeresesClick(sender, null);
        //                break;
        //        }
        //    }
        //}
        if (Command == CommandName.View)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshSelectedNode:
                        break;
                    case EventArgumentConst.refreshSelectedNodeChildren:
                        break;
                    case EventArgumentConst.refreshMasterList:
                        LoadTreeView();
                        break;
                }
            }
        }
    }

    public void LoadTreeView()
    {

        ViewState["Loaded"] = false;
        LoadTreeView(NodeType.ObjMetaDefinicio, "");
        ViewState["Loaded"] = true;
    }

    public TreeNode LoadTreeView(NodeType nodeType, String nodeId)
    {
        // Dictionary-k t�rl�se:
        Obj_MetaDefinicioDictionary.Clear();
        Obj_MetaAdataiDictionary.Clear();

        TreeView1.Nodes.Clear();

        switch (nodeType)
        {
            //case NodeType.ObjMetaAdat:
            //    { 
            //        EREC_Obj_MetaAdataiService omas = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            //        ExecParam omaEP = new ExecParam();
            //        omaEP.Record_Id = nodeId;
            //        Result res = omas.Get(omaEP);

            //        TreeNode ObjMetaDefinicioNode = LoadTreeView(NodeType.ObjMetaDefinicio, (res.Record as EREC_Obj_MetaAdatai).Obj_MetaDefinicio_Id);
            //        ObjMetaDefinicioNode.Expand();

            //        foreach (TreeNode tn in ObjMetaDefinicioNode.ChildNodes)
            //            if (tn.Value == nodeId)
            //            {
            //                tn.Selected = true;
            //                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

            //            }

            //        return TreeView1.SelectedNode;
            //    }
            //    break;
            case NodeType.ObjMetaDefinicio:
                {
                    TreeNode ObjMetaDefinicioNode = AddObj_MetaDefinicioNode(null);

                    if (ObjMetaDefinicioNode != null)
                    {
                        String Id = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(Id))
                        {
                            ObjMetaDefinicioNode.Selected = true;
                        }
                        else
                        {
                            if (FindNodeByValue(TreeView1.Nodes, Id))
                            {
                                TreeView1.SelectedNode.Expanded = true;
                            }
                        }
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                    }
                    return TreeView1.SelectedNode;
                }
                break;
            default:
                break;
        }

        return null;
    }

    public void ReLoadTab()
    {
        if (ParentForm != "MetaAdatKarbantartas")    // TODO: a szulo form meghatarozasa utan ezt kivenni
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            Active = false;
        }
        else
        {
            if (!Active) return;
            ViewState["Loaded"] = false;
            LoadTreeView();
            ViewState["Loaded"] = true;

            pageView.SetViewOnPage(Command);
        }    
    }

    protected void RefreshSelectedNode()
    {

        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        NodeType selectedTreeNodeType = GetNodeTypeById(selectedTreeNode.Value);

        switch (selectedTreeNodeType)
        {
            case NodeType.ObjMetaDefinicio:
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    Result result;

                    EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                    EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();

                    search.Id.Value = selectedTreeNode.Value;
                    search.Id.Operator = Query.Operators.equals;

                    result = service.GetAllWithExtension(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables[0].Rows.Count == 1)
                    {
                        DataRow dataRow_objMetaDefinicioExtended = result.Ds.Tables[0].Rows[0];
                        UpdateObj_MetaDefinicioNode(dataRow_objMetaDefinicioExtended, selectedTreeNode.Parent);
                    }
                }
                break;
            case NodeType.ObjMetaAdat:
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    Result result;

                    EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                    EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                    search.Id.Value = selectedTreeNode.Value;
                    search.Id.Operator = Query.Operators.equals;

                    result = service.GetAllWithExtension(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables[0].Rows.Count == 1)
                    {
                        DataRow dataRow_objMetaAdataiExtended = result.Ds.Tables[0].Rows[0];
                        UpdateObj_MetaAdataiNode(dataRow_objMetaAdataiExtended, selectedTreeNode.Parent);
                    }

                }
                break;
        }
    }

    protected void RefreshSelectedNodeChildren()
    {
        if (IsFilteredWithoutHit) return;

        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        NodeType selectedTreeNodeType = GetNodeTypeById(selectedTreeNode.Value);

        switch (selectedTreeNodeType)
        {
            case NodeType.ObjMetaDefinicio:
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    Result result;

                    EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                    EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();

                    search.Obj_MetaDefinicio_Id.Value = selectedTreeNode.Value;
                    search.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

                    result = service.GetAllWithExtension(execParam, search);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string Id = row["Id"].ToString();

                            bool canBeAdded = true;

                            if (!String.IsNullOrEmpty(ObjMetaAdataiFilter) && !isInFilter(Id, ObjMetaAdataiFilter))
                            {
                                canBeAdded = false;
                            }

                            if (!Obj_MetaAdataiDictionary.ContainsKey(Id))   // �j node, mindenk�ppen hozz�adjuk
                            {
                                UpdateObj_MetaAdataiNode(row, selectedTreeNode);
                            }
                            else if (!canBeAdded && Obj_MetaAdataiDictionary.ContainsKey(Id))
                            {
                                Obj_MetaAdataiDictionary.Remove(Id);
                                TreeNode node = TreeView1.FindNode(selectedTreeNode.ValuePath + ValuePathSeparator.ToString() + Id);
                                if (node != null)
                                {
                                    DeleteNode(node);
                                }
                            }
                        }
                    }
                }
                break;
            case NodeType.ObjMetaAdat:
                {
                    // ennek nem lehetnek gyermekei
                }
                break;
        }       

        // l�trehoz�skor, ig�ny szerint kiv�lasztjuk az �j node-ot
        if (TreeViewHeader1.SelectNewChild == true)
        {
            String SelectedId = SelectedNodeId_HiddenField.Value;
            SelectChildNode(SelectedId);
        }

    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        NodeType nodeType = GetNodeTypeById(TreeView1.SelectedNode.Value);

        //Torles.OnClientClick = RegisterClientScriptDeleteConfirm();
        TreeViewHeader1.InvalidateOnClientClick = RegisterClientScriptDeleteConfirm();

        switch (nodeType)
        {
            case NodeType.ObjMetaDefinicio:
                SetButtonsByObj_MetaDefinicio();
                TreeViewHeader1.HeaderNodeButtonsLabel = strMetaDefinicioNodeText; //"Objektum metadefin�ci�";
                TreeViewHeader1.HeaderChildButtonsLabel = strMetaAdatNodeText;
                break;
            case NodeType.ObjMetaAdat:
                SetButtonsByObj_MetaAdatai();
                TreeViewHeader1.HeaderNodeButtonsLabel = strMetaAdatNodeText; //"T�rgysz� hozz�rendel�s";
                TreeViewHeader1.HeaderChildButtonsLabel = ""; break;
            case NodeType.Unknown:
                break;
            default:
                break;
        }

    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        TreeNode treeNode = e.Node;
        NodeType nodeType = GetNodeTypeById(treeNode.Value);

        switch (nodeType)
        {
            case NodeType.ObjMetaDefinicio:
                AddObj_MetaAdataiNode(e.Node);
                AddObj_MetaDefinicioNode(e.Node);
                break;
            case NodeType.ObjMetaAdat:
                break;
            default:
                break;
        }
    }


    // Lehet legfels� szint (ObjMetaDefinicioTreeNode == null) vagy al�rendel�s is
    private TreeNode AddObj_MetaDefinicioNode(TreeNode ObjMetaDefinicioTreeNode)
    {
        TreeNode CustomTreeNode = null;
        if (IsFilteredWithoutHit) return null;

        String Felettes_Obj_Meta = "";
        String ParentNodeValuePath = "";

        if (ObjMetaDefinicioTreeNode != null)
        {
            Felettes_Obj_Meta = ObjMetaDefinicioTreeNode.Value;
            // hiba, nem l�tezik a felettes objektum metadefin�ci� a sz�t�rban
            if (GetNodeTypeById(Felettes_Obj_Meta) != NodeType.ObjMetaDefinicio) return null;

            ParentNodeValuePath = ObjMetaDefinicioTreeNode.ValuePath;
        }

        #region Init
        EREC_Obj_MetaDefinicioService erec_Obj_MetaDefinicioService = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
        EREC_Obj_MetaDefinicioSearch erec_Obj_MetaDefinicioSearch = new EREC_Obj_MetaDefinicioSearch();
        ExecParam erec_Obj_MetaDefinicioExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_Obj_MetaDefinicioResult = new Result();
        #endregion

        #region Search be�ll�t�s

        if (!String.IsNullOrEmpty(Felettes_Obj_Meta))
        {
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Value = Felettes_Obj_Meta;
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Operator = Query.Operators.equals;
        }
        else
        {
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Value = "";
            erec_Obj_MetaDefinicioSearch.Felettes_Obj_Meta.Operator = Query.Operators.isnull;
        }
        
        erec_Obj_MetaDefinicioSearch.OrderBy = "DefinicioTipus, ContentType";

        erec_Obj_MetaDefinicioResult = erec_Obj_MetaDefinicioService.GetAllWithExtension(erec_Obj_MetaDefinicioExecParam, erec_Obj_MetaDefinicioSearch);
        if (!String.IsNullOrEmpty(erec_Obj_MetaDefinicioResult.ErrorCode))
        {
            return null;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_Obj_MetaDefinicioDataSet = erec_Obj_MetaDefinicioResult.Ds;

        for (int Obj_MetaDefinicioIndex = 0; Obj_MetaDefinicioIndex < erec_Obj_MetaDefinicioDataSet.Tables[0].Rows.Count; Obj_MetaDefinicioIndex++)
        {
            String Obj_MetaDefinicio_Id = erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Id"].ToString();
            String strContentType = erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["ContentType"].ToString();
            String strDefinicioTipus = erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["DefinicioTipus"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(ObjMetaDefinicioFilter) && !isInFilter(Obj_MetaDefinicio_Id, ObjMetaDefinicioFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                // load to Dictionary 
                // ha m�g nincs a dictionary-ben:
                if (Obj_MetaDefinicioDictionary.ContainsKey(Obj_MetaDefinicio_Id) == false)
                {
                    Obj_MetaDefinicioItem Obj_MetaDefinicioItem = new Obj_MetaDefinicioItem(
                        Obj_MetaDefinicio_Id,
                        strDefinicioTipus,
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Objtip_Id"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Objtip_Id_Column"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["ColumnValue"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Felettes_Obj_Meta"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Felettes_Obj_Meta_CTT"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["ContentType"].ToString(),
                        erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["SPSSzinkronizalt"].ToString()
                        );
                    Obj_MetaDefinicioDictionary.Add(Obj_MetaDefinicio_Id, Obj_MetaDefinicioItem);

                }

                String SPSSzinkronizalt = "";
                if (erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["SPSSzinkronizalt"].ToString() == "1")
                {
                    SPSSzinkronizalt = " (" + strSzinkronizalt + ")";
                }
                String Felettes_CTT = "";
                if (!String.IsNullOrEmpty(erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Felettes_Obj_Meta_CTT"].ToString()))
                {
                    Felettes_CTT = strFelettes + erec_Obj_MetaDefinicioDataSet.Tables[0].Rows[Obj_MetaDefinicioIndex]["Felettes_Obj_Meta_CTT"].ToString();
                }

                String Kod = strMetaDefinicioNodeText;
                String Nev = strContentType + SPSSzinkronizalt + Felettes_CTT;

                String NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaDefinicio, ParentNodeValuePath, strIconPathMetaDefinicio, strIconAltMetaDefinicio, Obj_MetaDefinicio_Id);
                String NodeValue = Obj_MetaDefinicio_Id;

                CustomTreeNode = new TreeNode(NodeText, NodeValue);
                CustomTreeNode.PopulateOnDemand = true;
                CustomTreeNode.Expanded = false;

                if (ObjMetaDefinicioTreeNode != null)
                {
                    ObjMetaDefinicioTreeNode.ChildNodes.Add(CustomTreeNode);
                }
                else
                {
                    TreeView1.Nodes.Add(CustomTreeNode);
                }

            }
            else if (Obj_MetaDefinicioDictionary.ContainsKey(Obj_MetaDefinicio_Id) == true)
            {
                Obj_MetaDefinicioDictionary.Remove(Obj_MetaDefinicio_Id);
            }

        }

        ViewState["Obj_MetaDefinicioDictionary"] = Obj_MetaDefinicioDictionary;

        if (ObjMetaDefinicioTreeNode == null)
        {
            return TreeView1.Nodes[0] ?? new TreeNode();
        }
        else
        {
            return CustomTreeNode;
        }

       #endregion

    }

    // Objektum metadefin�ci�hoz kapcsol�dik
    private void AddObj_MetaAdataiNode(TreeNode ObjMetaDefinicioTreeNode)
    {
        if (IsFilteredWithoutHit) return;
        if (ObjMetaDefinicioTreeNode == null) return;

        #region Init
        EREC_Obj_MetaAdataiService erec_Obj_MetaAdataiService = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
        EREC_Obj_MetaAdataiSearch erec_Obj_MetaAdataiSearch = new EREC_Obj_MetaAdataiSearch();
        ExecParam erec_Obj_MetaAdataiExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_Obj_MetaAdataiResult = new Result();
        #endregion

        string Obj_MetaDefinicio_Id = ObjMetaDefinicioTreeNode.Value;
        if (String.IsNullOrEmpty(Obj_MetaDefinicio_Id)) return;

        #region Search be�ll�t�s
        erec_Obj_MetaAdataiSearch.Obj_MetaDefinicio_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_Obj_MetaAdataiSearch.Obj_MetaDefinicio_Id.Value = Obj_MetaDefinicio_Id;

        erec_Obj_MetaAdataiSearch.OrderBy = "Sorszam";

        erec_Obj_MetaAdataiResult = erec_Obj_MetaAdataiService.GetAllWithExtension(erec_Obj_MetaAdataiExecParam, erec_Obj_MetaAdataiSearch);
        if (!String.IsNullOrEmpty(erec_Obj_MetaAdataiResult.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        DataSet erec_Obj_MetaAdataiDataSet = erec_Obj_MetaAdataiResult.Ds;

        for (int Obj_MetaAdataiIndex = 0; Obj_MetaAdataiIndex < erec_Obj_MetaAdataiDataSet.Tables[0].Rows.Count; Obj_MetaAdataiIndex++)
        {
            String Obj_MetaAdatai_Id = erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Id"].ToString();

            bool canBeAdded = true;

            if (!String.IsNullOrEmpty(ObjMetaAdataiFilter) && !isInFilter(Obj_MetaAdatai_Id, ObjMetaAdataiFilter))
            {
                canBeAdded = false;
            }

            if (canBeAdded)
            {
                string targySzavak = erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["TargySzavak"].ToString();
                string strTargySzavak = "";
                string sorszam = "";
                string opcionalis = "";
                string ismetlodo = "";
                string funkcio = "";
                string SPSSzinkronizalt = "";
                string szint = "";

                if (!String.IsNullOrEmpty(targySzavak))
                {
                    //targySzavak = (erec_TargySzavakResult.Record as EREC_TargySzavak).TargySzavak;
                    strTargySzavak = targySzavak;

                    sorszam = erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Sorszam"].ToString();
                    szint = erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Szint"].ToString();
                    switch (erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Opcionalis"].ToString())
                    {
                        case "0":
                            opcionalis = valueOpcionalis0;
                            break;
                        case "1":
                            opcionalis = valueOpcionalis1;
                            break;
                        default:
                            break;
                    }

                    switch (erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Ismetlodo"].ToString())
                    {
                        case "0":
                            ismetlodo = valueIsmetlodo0;
                            break;
                        case "1":
                            ismetlodo = valueIsmetlodo1;
                            break;
                        default:
                            break;
                    }

                    if (erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["SPSSzinkronizalt"].ToString() == "1")
                    {
                        SPSSzinkronizalt = strSzinkronizalt;
                    }

                    strTargySzavak += " (" + strSorszam + ": " + sorszam;
                    if (!String.IsNullOrEmpty(opcionalis))
                    {
                        strTargySzavak += ", " + opcionalis;
                    }
                    if (!String.IsNullOrEmpty(ismetlodo))
                    {
                        strTargySzavak += ", " + ismetlodo;
                    }

                    if (!String.IsNullOrEmpty(SPSSzinkronizalt))
                    {
                        strTargySzavak += ", " + SPSSzinkronizalt;
                    }

                    strTargySzavak += ")";

                    funkcio = erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Funkcio"].ToString();
                    if (!String.IsNullOrEmpty(funkcio))
                    {
                        strTargySzavak += strFunkcio + funkcio;
                    }
                }

                int nSorszam;
                Int32.TryParse(sorszam, out nSorszam);

                int nSzint;
                Int32.TryParse(szint, out nSzint);
                // load to Dictionary 
                // ha m�g nincs a dictionary-ben:
                if (Obj_MetaAdataiDictionary.ContainsKey(Obj_MetaAdatai_Id) == false)
                {
                    Obj_MetaAdataiItem Obj_MetaAdataiItem = new Obj_MetaAdataiItem(
                        Obj_MetaAdatai_Id,
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Obj_MetaDefinicio_Id"].ToString(),
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Targyszavak_Id"].ToString(),
                        targySzavak,
                        nSorszam,
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Funkcio"].ToString(),
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Ismetlodo"].ToString(),
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["Opcionalis"].ToString(),
                        erec_Obj_MetaAdataiDataSet.Tables[0].Rows[Obj_MetaAdataiIndex]["SPSSzinkronizalt"].ToString(),
                        nSzint
                        );
                    Obj_MetaAdataiDictionary.Add(Obj_MetaAdatai_Id, Obj_MetaAdataiItem);

                }

                String Kod = strMetaAdatNodeNev;
                String Nev = strTargySzavak;

                String NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaAdat, ObjMetaDefinicioTreeNode.ValuePath, strIconPathMetaAdat, strIconAltMetaAdat, Obj_MetaAdatai_Id);
                String NodeValue = Obj_MetaAdatai_Id;

                TreeNode CustomTreeNode = new TreeNode(NodeText, NodeValue);
                CustomTreeNode.PopulateOnDemand = true;
                CustomTreeNode.Expanded = false;

                ObjMetaDefinicioTreeNode.ChildNodes.Add(CustomTreeNode);
                                
            }
            else if (Obj_MetaAdataiDictionary.ContainsKey(Obj_MetaAdatai_Id) == true)
            {
                Obj_MetaAdataiDictionary.Remove(Obj_MetaAdatai_Id);
            }

        }
        ViewState["Obj_MetaAdataiDictionary"] = Obj_MetaAdataiDictionary;


        #endregion
    }

    #region UpdateTreeNodes

    private void UpdateObj_MetaDefinicioNode(DataRow dataRow_objMetaDefinicioExtended, TreeNode ObjMetaDefinicioTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        bool isFelettesChanged = false;

        string ParentTreeNodeValuePath = (ObjMetaDefinicioTreeNode == null ? "" : ObjMetaDefinicioTreeNode.ValuePath);

        String Obj_MetaDefinicio_Id = dataRow_objMetaDefinicioExtended["Id"].ToString();
        String strDefinicioTipus = dataRow_objMetaDefinicioExtended["DefinicioTipus"].ToString();
        String strContentType = dataRow_objMetaDefinicioExtended["ContentType"].ToString();
        String Felettes_Obj_Meta_CTT = dataRow_objMetaDefinicioExtended["Felettes_Obj_Meta_CTT"].ToString();
        String Felettes_Obj_Meta = dataRow_objMetaDefinicioExtended["Felettes_Obj_Meta"].ToString();

        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (Obj_MetaDefinicioDictionary.ContainsKey(Obj_MetaDefinicio_Id) == true)
        {
            Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
            Obj_MetaDefinicioDictionary.TryGetValue(Obj_MetaDefinicio_Id, out obj_MetaDefinicioItem);

            OrderChanged = (obj_MetaDefinicioItem.DefinicioTipus.ToString() == strDefinicioTipus.ToString() 
                            && obj_MetaDefinicioItem.ContentType.ToString() == strContentType ? false : true);
            isFelettesChanged = (obj_MetaDefinicioItem.Felettes_Obj_Meta.ToString() == Felettes_Obj_Meta ? false : true);

            obj_MetaDefinicioItem.Felettes_Obj_Meta = Felettes_Obj_Meta;
            obj_MetaDefinicioItem.Felettes_Obj_Meta_CTT = Felettes_Obj_Meta_CTT;
            obj_MetaDefinicioItem.ContentType = strContentType;
            obj_MetaDefinicioItem.DefinicioTipus = strDefinicioTipus;

            Obj_MetaDefinicioDictionary.Remove(Obj_MetaDefinicio_Id);
            Obj_MetaDefinicioDictionary.Add(Obj_MetaDefinicio_Id, obj_MetaDefinicioItem);
        }
        else
        {
            Obj_MetaDefinicioItem Obj_MetaDefinicioItem = new Obj_MetaDefinicioItem(
                Obj_MetaDefinicio_Id,
                strDefinicioTipus,
                dataRow_objMetaDefinicioExtended["Objtip_Id"].ToString(),
                dataRow_objMetaDefinicioExtended["Objtip_Id_Column"].ToString(),
                dataRow_objMetaDefinicioExtended["ColumnValue"].ToString(),
                Felettes_Obj_Meta,
                Felettes_Obj_Meta_CTT,
                strContentType,
                dataRow_objMetaDefinicioExtended["SPSSzinkronizalt"].ToString()
                );
            Obj_MetaDefinicioDictionary.Add(Obj_MetaDefinicio_Id, Obj_MetaDefinicioItem);
            OrderChanged = true;
            isFelettesChanged = true;
        }

        ViewState["Obj_MetaDefinicioDictionary"] = Obj_MetaDefinicioDictionary;

        String SPSSzinkronizalt = "";
        if (dataRow_objMetaDefinicioExtended["SPSSzinkronizalt"].ToString() == "1")
        {
            SPSSzinkronizalt = " (" + strSzinkronizalt + ")";
        }

        if (!String.IsNullOrEmpty(Felettes_Obj_Meta_CTT))
        {
            Felettes_Obj_Meta_CTT = strFelettes + Felettes_Obj_Meta_CTT;
        }

        String Kod = strMetaDefinicioNodeText;
        String Nev = strContentType + SPSSzinkronizalt + Felettes_Obj_Meta_CTT;
        String NodeText = "";

        String NodeValue = Obj_MetaDefinicio_Id;
        String NodePath = ParentTreeNodeValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaDefinicio, ParentTreeNodeValuePath, strIconPathMetaDefinicio, strIconAltMetaDefinicio, Obj_MetaDefinicio_Id);

            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                ObjMetaDefinicioTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            // megkeress�k a hely�t
            bool bFound = false;
            int i = -1;

            //switch (customNodePosition)
            //{
            //    case IMDterkep.CustomNodePositionType.AfterOthers:
            //// fel�l vannak az irat metadefin�ci� node-ok, alatta a custom node-ok, h�tulr�l keres�nk
            //// vigy�zat: String.Compare rel�ci�s jel �ll�sa!
            //i = ObjMetaDefinicioTreeNode.ChildNodes.Count;
            //while (i > 0 && !bFound
            //    && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
            //{
            //    TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
            //    string tnObj_MetaDefinicio_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

            //    Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
            //    Obj_MetaDefinicioDictionary.TryGetValue(tnObj_MetaDefinicio_Id, out obj_MetaDefinicioItem);
            //    if (obj_MetaDefinicioItem != null && String.Compare(obj_MetaDefinicioItem.ContentType, strContentType, true) <= 0)
            //    {
            //        bFound = true;
            //    }
            //    else
            //    {
            //        i--;
            //    }
            //}
            //break;
            //case IMDterkep.CustomNodePositionType.BeforeOthers:
            // alul vannak az irat metadefin�ci� node-ok, felette a custom node-ok, el�lr�l keres�nk
            // vigy�zat: String.Compare rel�ci�s jel �ll�sa!
            i = 0;
            while (i < ObjMetaDefinicioTreeNode.ChildNodes.Count && !bFound
                && GetNodeTypeById(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == NodeType.ObjMetaAdat)
            {
                TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
                string tnObj_MetaDefinicio_Id = tn.Value;

                Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
                Obj_MetaDefinicioDictionary.TryGetValue(tnObj_MetaDefinicio_Id, out obj_MetaDefinicioItem);
                if (obj_MetaDefinicioItem != null
                    && (
                        String.Compare(obj_MetaDefinicioItem.DefinicioTipus, strDefinicioTipus, true) > 0
                        || (
                            String.Compare(obj_MetaDefinicioItem.DefinicioTipus, strDefinicioTipus, true) == 0
                            && String.Compare(obj_MetaDefinicioItem.ContentType, strContentType, true) > 0
                           )
                       )
                    )
                {
                    bFound = true;
                }
                else
                {
                    i++;
                }
            }
            //        break;
            //}

            NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaDefinicio, ObjMetaDefinicioTreeNode.ValuePath, strIconPathMetaDefinicio, strIconAltMetaDefinicio, Obj_MetaDefinicio_Id);

            TreeNode CustomTreeNode = new TreeNode(NodeText, NodeValue);
            CustomTreeNode.PopulateOnDemand = true;
            CustomTreeNode.Expanded = false;

            ObjMetaDefinicioTreeNode.ChildNodes.AddAt(i, CustomTreeNode);

            if (isSelected)
            {
                ObjMetaDefinicioTreeNode.ChildNodes[i].Select();
            }
        }

        //if (isFelettesChanged)
        //{
        DeleteChildrenNodes(currentNode);
        RefreshSelectedNodeChildren();
        //}
    }


    private void UpdateObj_MetaAdataiNode(DataRow dataRow_objMetaAdataiExtended, TreeNode ObjMetaDefinicioTreeNode)
    {
        bool OrderChanged = false;
        bool isSelected = false;

        String Obj_MetaAdatai_Id = dataRow_objMetaAdataiExtended["Id"].ToString();

        string targySzavak = dataRow_objMetaAdataiExtended["TargySzavak"].ToString();
        string strTargySzavak = "";
        string sorszam = "";
        string opcionalis = "";
        string ismetlodo = "";
        string funkcio = "";
        string SPSSzinkronizalt = "";
        string szint = "";

        if (!String.IsNullOrEmpty(targySzavak))
        {
            strTargySzavak = targySzavak;

            sorszam = dataRow_objMetaAdataiExtended["Sorszam"].ToString();
            szint = dataRow_objMetaAdataiExtended["Szint"].ToString();
            switch (dataRow_objMetaAdataiExtended["Opcionalis"].ToString())
            {
                case "0":
                    opcionalis = valueOpcionalis0;
                    break;
                case "1":
                    opcionalis = valueOpcionalis1;
                    break;
                default:
                    break;
            }

            switch (dataRow_objMetaAdataiExtended["Ismetlodo"].ToString())
            {
                case "0":
                    ismetlodo = valueIsmetlodo0;
                    break;
                case "1":
                    ismetlodo = valueIsmetlodo1;
                    break;
                default:
                    break;
            }
            if (dataRow_objMetaAdataiExtended["SPSSzinkronizalt"].ToString() == "1")
            {
                SPSSzinkronizalt = strSzinkronizalt;
            }

            strTargySzavak += " (" + strSorszam + ": " + sorszam;
            if (!String.IsNullOrEmpty(opcionalis))
            {
                strTargySzavak += ", " + opcionalis;
            }
            if (!String.IsNullOrEmpty(ismetlodo))
            {
                strTargySzavak += ", " + ismetlodo;
            }
            if (!String.IsNullOrEmpty(SPSSzinkronizalt))
            {
                strTargySzavak += ", " + SPSSzinkronizalt;
            }
            strTargySzavak += ")";

            funkcio = dataRow_objMetaAdataiExtended["Funkcio"].ToString();
            if (!String.IsNullOrEmpty(funkcio))
            {
                strTargySzavak += strFunkcio + funkcio;
            }
        }

        int nSorszam;
        Int32.TryParse(sorszam, out nSorszam);

        int nSzint;
        Int32.TryParse(szint, out nSzint);
        // load to Dictionary 
        // ha m�g nincs a dictionary-ben:
        if (Obj_MetaAdataiDictionary.ContainsKey(Obj_MetaAdatai_Id) == true)
        {
            Obj_MetaAdataiItem obj_MetaAdataiItem = null;
            Obj_MetaAdataiDictionary.TryGetValue(Obj_MetaAdatai_Id, out obj_MetaAdataiItem);

            OrderChanged = (obj_MetaAdataiItem.Sorszam.ToString() == sorszam ? false : true);

            obj_MetaAdataiItem.Targyszavak_Id = dataRow_objMetaAdataiExtended["Targyszavak_Id"].ToString();    // elvileg nem v�ltozhat
            obj_MetaAdataiItem.TargySzavak = targySzavak;
            obj_MetaAdataiItem.Sorszam = nSorszam;
            obj_MetaAdataiItem.Funkcio = dataRow_objMetaAdataiExtended["Funkcio"].ToString();
            obj_MetaAdataiItem.Ismetlodo = dataRow_objMetaAdataiExtended["Ismetlodo"].ToString();
            obj_MetaAdataiItem.Opcionalis = dataRow_objMetaAdataiExtended["Opcionalis"].ToString();
            obj_MetaAdataiItem.Szint = nSzint;

            Obj_MetaAdataiDictionary.Remove(Obj_MetaAdatai_Id);
            Obj_MetaAdataiDictionary.Add(Obj_MetaAdatai_Id, obj_MetaAdataiItem);
        }
        else
        {
            Obj_MetaAdataiItem Obj_MetaAdataiItem = new Obj_MetaAdataiItem(
                Obj_MetaAdatai_Id,
                null, //erec_Obj_MetaAdatai.IratMetadefinicio_Id,
                dataRow_objMetaAdataiExtended["Targyszavak_Id"].ToString(),
                targySzavak,
                nSorszam,
                dataRow_objMetaAdataiExtended["Funkcio"].ToString(),
                dataRow_objMetaAdataiExtended["Ismetlodo"].ToString(),
                dataRow_objMetaAdataiExtended["Opcionalis"].ToString(),
                dataRow_objMetaAdataiExtended["SPSSzinkronizalt"].ToString(),
                nSzint
                );
            Obj_MetaAdataiDictionary.Add(Obj_MetaAdatai_Id, Obj_MetaAdataiItem);
            OrderChanged = true;

        }

        ViewState["Obj_MetaAdataiDictionary"] = Obj_MetaAdataiDictionary;

        String orokolt = "";
        if (nSzint > 0)
        {
            orokolt = " <b>(" + strOrokolt + ": " + dataRow_objMetaAdataiExtended["ContentType"].ToString() + ")</b>";
            //// st�lus �t�ll�t�s
            //IratMetaDefinicioTerkep1.SetNodeTypeClass(IMDterkep.NodeType.Custom, strCustomNodeClassInherited);
        }

        String Kod = strMetaAdatNodeNev;
        String Nev = strTargySzavak + orokolt;
        String NodeText = "";

        String NodeValue = Obj_MetaAdatai_Id;
        String NodePath = ObjMetaDefinicioTreeNode.ValuePath + ValuePathSeparator + NodeValue;

        TreeNode currentNode = TreeView1.FindNode(NodePath);

        if (currentNode != null)
        {
            NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaAdat, ObjMetaDefinicioTreeNode.ValuePath, strIconPathMetaAdat, strIconAltMetaAdat, Obj_MetaAdatai_Id);

            currentNode.Text = NodeText;

            isSelected = currentNode.Selected;

            if (OrderChanged)
            {
                // TODO: t�rl�s, besz�r�si index keres�s, besz�r�s
                ObjMetaDefinicioTreeNode.ChildNodes.Remove(currentNode);
                currentNode = null;
            }

        }

        if (currentNode == null)
        {
            // megkeress�k a hely�t
            bool bFound = false;
            int i = -1;

            //switch (customNodePosition)
            //{
            //    case IMDterkep.CustomNodePositionType.AfterOthers:
            //        // fel�l vannak az irat metadefin�ci� node-ok, alatta a custom node-ok, h�tulr�l keres�nk
            //        // vigy�zat: rel�ci�s jel �ll�sa!
            //        i = ObjMetaDefinicioTreeNode.ChildNodes.Count;
            //        while (i > 0 && !bFound
            //            && IratMetaDefinicioTerkep1.ExtractNodeTypeFromNodeTypeName(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == IMDterkep.NodeType.Custom)
            //        {
            //            TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
            //            string tnObj_MetaAdatai_Id = IratMetaDefinicioTerkep1.ExtractNodeIdFromNodeTypeName(tn.Value);

            //            Obj_MetaAdataiItem obj_MetaAdataiItem = null;
            //            Obj_MetaAdataiDictionary.TryGetValue(tnObj_MetaAdatai_Id, out obj_MetaAdataiItem);
            //            if (obj_MetaAdataiItem != null && obj_MetaAdataiItem.Sorszam <= nSorszam)
            //            {
            //                bFound = true;
            //            }
            //            else
            //            {
            //                i--;
            //            }
            //        }
            //        break;
            //    case IMDterkep.CustomNodePositionType.BeforeOthers:
            //        // alul vannak az irat metadefin�ci� node-ok, felette a custom node-ok, el�lr�l keres�nk
            //        // vigy�zat: rel�ci�s jel �ll�sa!
            i = 0;
            while (i < ObjMetaDefinicioTreeNode.ChildNodes.Count && !bFound
                && GetNodeTypeById(ObjMetaDefinicioTreeNode.ChildNodes[i].Value) == NodeType.ObjMetaAdat)
            {
                TreeNode tn = ObjMetaDefinicioTreeNode.ChildNodes[i];
                string tnObj_MetaAdatai_Id = tn.Value;

                Obj_MetaAdataiItem obj_MetaAdataiItem = null;
                Obj_MetaAdataiDictionary.TryGetValue(tnObj_MetaAdatai_Id, out obj_MetaAdataiItem);
                if (obj_MetaAdataiItem != null && obj_MetaAdataiItem.Sorszam > nSorszam)
                {
                    bFound = true;
                }
                else
                {
                    i++;
                }
            }
            //        break;
            //}

            NodeText = GetTreeNodeText(Kod, Nev, NodeType.ObjMetaAdat, ObjMetaDefinicioTreeNode.ValuePath, strIconPathMetaAdat, strIconAltMetaAdat, Obj_MetaAdatai_Id);

            TreeNode CustomTreeNode = new TreeNode(NodeText, NodeValue);
            CustomTreeNode.PopulateOnDemand = true;
            CustomTreeNode.Expanded = false;

            ObjMetaDefinicioTreeNode.ChildNodes.AddAt(i, CustomTreeNode);


            //// st�lus vissza�ll�t�s
            //IratMetaDefinicioTerkep1.SetNodeTypeClass(IMDterkep.NodeType.Custom, strCustomNodeClass);

            if (isSelected)
            {
                ObjMetaDefinicioTreeNode.ChildNodes[i].Select();
            }
        }

    }


    #endregion UpdateTreeNodes


    #region Buttons

    private void SetButtonsByObj_MetaDefinicio()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        Obj_MetaDefinicioItem obj_MetaDefinicioItem = null;
        Obj_MetaDefinicioDictionary.TryGetValue(TreeView1.SelectedNode.Value, out obj_MetaDefinicioItem);
        if (obj_MetaDefinicioItem == null)
            return;


        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioView");

            if (TreeViewHeader1.ViewEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaDefinicio_Id
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + obj_MetaDefinicioItem.Id
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioModify");

            if (TreeViewHeader1.ModifyEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaDefinicio_Id
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + obj_MetaDefinicioItem.Id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }


            TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiNew");

            if (TreeViewHeader1.NewEnabled)
            {
                TreeViewHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + "&" + QueryStringVars.ObjMetaDefinicioId + "=" + TreeView1.SelectedNode.Value
                    + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                    + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
            }

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate");

            if (obj_MetaDefinicioItem.DefinicioTipus == KodTarak.OBJMETADEFINICIO_TIPUS.A0)
            {
                TreeViewHeader1.NewCustomNodeEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioNew");

                if (TreeViewHeader1.NewCustomNodeEnabled)
                {

                    TreeViewHeader1.NewCustomNodeOnClientClick = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.ObjMetaDefinicioId + "=" + obj_MetaDefinicioItem.Id
                        + "&" + QueryStringVars.HiddenFieldId + "=" + SelectedNodeId_HiddenField.ClientID
                        + "&" + QueryStringVars.RefreshCallingWindow + "=1"
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNodeChildren);
                }
            }
            else
            {
                TreeViewHeader1.NewCustomNodeEnabled = false;
            }
        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetButtonsByObj_MetaAdatai()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        Obj_MetaAdataiItem obj_MetaAdataiItem = null;
        Obj_MetaAdataiDictionary.TryGetValue(TreeView1.SelectedNode.Value, out obj_MetaAdataiItem);
        if (obj_MetaAdataiItem == null)
            return;


        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiView");

            if (TreeViewHeader1.ViewEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaAdatai_Id
                TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + obj_MetaAdataiItem.Id
                       + "&" + QueryStringVars.TargyszavakId + "=" + obj_MetaAdataiItem.Targyszavak_Id
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }
        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {
            TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiModify");

            TreeViewHeader1.ModifyEnabled = TreeViewHeader1.ModifyEnabled && (obj_MetaAdataiItem.Szint == 0);

            if (TreeViewHeader1.ModifyEnabled)
            {
                // TODO: QueryStringVars.IratMetadefinicio_Id, QueryStringVars.Obj_MetaAdatai_Id
                TreeViewHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ObjMetaAdataiForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + obj_MetaAdataiItem.Id
                    + "&" + QueryStringVars.TargyszavakId + "=" + obj_MetaAdataiItem.Targyszavak_Id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshSelectedNode);
            }

            TreeViewHeader1.NewEnabled = false;

            TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiInvalidate");
            TreeViewHeader1.InvalidateEnabled = TreeViewHeader1.InvalidateEnabled && (obj_MetaAdataiItem.Szint == 0);

            TreeViewHeader1.NewCustomNodeEnabled = false;

        }

        TreeViewHeader1.SelectNewChildEnabled = TreeViewHeader1.NewEnabled;
        #endregion

    }

    private void SetAllButtonsEnabled(Boolean value)
    {
        TreeViewHeader1.ViewEnabled = value;
        TreeViewHeader1.ModifyEnabled = value;
        TreeViewHeader1.NewEnabled = value;
        TreeViewHeader1.InvalidateEnabled = value;
        TreeViewHeader1.NewCustomNodeEnabled = value;

        TreeViewHeader1.SelectNewChildEnabled = value;
    }

    private void ResetAllButtonsOnClientClick()
    {
        TreeViewHeader1.ViewOnClientClick = "";
        TreeViewHeader1.ModifyOnClientClick = "";
        TreeViewHeader1.NewOnClientClick = "";
        TreeViewHeader1.NewCustomNodeOnClientClick = "";
        //Torles.OnClientClick = "";
    }

    #endregion Buttons

    // ha a keres�s akt�v, true-val t�r vissza, egy�bk�nt false
    private bool SetKeresesFilter()
    {
        bool isSearchActive = false;

        if (Search.IsSearchObjectInSession(Page, typeof(MetaAdatHierarchiaSearch)))
        {
            MetaAdatHierarchiaSearch metaAdatHierarchiaSearch = (MetaAdatHierarchiaSearch)Search.GetSearchObject(Page, new MetaAdatHierarchiaSearch());

            if (metaAdatHierarchiaSearch != null)
            {
                EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.GetAllMetaAdatokHierarchiaFilter(execParam, metaAdatHierarchiaSearch, false);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader.ErrorPanel, result);
                }
                else
                {
                    //String IsFilteredWithNoHit = result.Ds.Tables[0].Rows[0]["IsFilteredWithoutHit"].ToString();
                    bool IsFilteredWithNoHit = (bool)result.Record;
                    String AgazatiJelek_Ids = GetIdListFromTable(result.Ds.Tables[1], ",", true);
                    String IraIrattariTetelek_Ids = GetIdListFromTable(result.Ds.Tables[2], ",", true);
                    //String IratMetaDefinicio_Ids = GetIdListFromTable(result.Ds.Tables[3], ",", true);
                    String Ugytipus_Ids = GetIdListFromTable(result.Ds.Tables[4], ",", true);
                    String EljarasiSzakasz_Ids = GetIdListFromTable(result.Ds.Tables[5], ",", true);
                    String Irattipus_Ids = GetIdListFromTable(result.Ds.Tables[6], ",", true);
                    String Obj_MetaDefinicio_Ids = GetIdListFromTable(result.Ds.Tables[7], ",", true);
                    String Obj_MetaAdatai_Ids = GetIdListFromTable(result.Ds.Tables[8], ",", true);
                    String TargySzavak_Ids = GetIdListFromTable(result.Ds.Tables[9], ",", true);

                    SetNodeFilter(IsFilteredWithNoHit, Obj_MetaDefinicio_Ids, Obj_MetaAdatai_Ids);

                    isSearchActive = true;
                }

            }
            else
            {
                ClearNodeFilter();
            }

        }

        return isSearchActive;
    }

    /// <summary>
    /// T�rli a kijel�lt elemet
    /// </summary>
    private void InvalidateSelectedNode()
    {
        if (TreeView1.SelectedNode == null) return;

        String nodeId = TreeView1.SelectedNode.Value;

        NodeType nodeType = GetNodeTypeById(nodeId);
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = nodeId;
        Result result = null;

        switch (nodeType)
        {
            case NodeType.ObjMetaDefinicio:
                {

                    if (FunctionRights.GetFunkcioJog(Page, "ObjMetaDefinicioInvalidate"))
                    {
                        EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
                        result = service.Invalidate(execParam);
                        // t�rl�s a sz�t�rb�l
                        if (Obj_MetaDefinicioDictionary.ContainsKey(nodeId))
                        {
                            DeleteSelectedNode();
                        }

                    }
                    else
                    {
                        UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                    }
                }
                break;
            case NodeType.ObjMetaAdat:
                {
                    if (FunctionRights.GetFunkcioJog(Page, "ObjMetaAdataiInvalidate"))
                    {
                        EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                        result = service.Invalidate(execParam);
                        // t�rl�s a sz�t�rb�l
                        if (Obj_MetaAdataiDictionary.ContainsKey(nodeId))
                            Obj_MetaAdataiDictionary.Remove(nodeId);
                    }
                    else
                    {
                        UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                    }
                }
                break;
            default:
                break;
        }

        if (result != null)
        {
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
        }
    }


    #region TreeViewRowTexts

    public String GetTreeNodeText(String Kod, String Nev, NodeType nodeType, String ValuePath, String IconPath, String IconAlt, String Id)
    {
        String TreeNodeText = String.Format(ItemFormatString
                                                , SetNodeUrl("<img src='" + IconPath
                                                    + "' alt='" + IconAlt
                                                    + "' />"
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                                , SetNodeUrl(" <b>" + Kod + "&nbsp;</b>"
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                                , SetNodeUrl(Nev
                                                    , ValuePath
                                                    , nodeType
                                                    , Id)
                                            );
        return TreeNodeText;
    }


    #endregion TreeViewRowTexts

    private string SetNodeUrl(String szoveg, String ValuePath, NodeType nodeType, String Id)
    {
        String strClass = GetNodeTypeClass(nodeType);

        return "<a" + (String.IsNullOrEmpty(strClass) ? "" : String.Format(ClassFormatString, strClass))
            + " href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s"
            + (String.IsNullOrEmpty(ValuePath) ? "" : ValuePath.Replace(ValuePathSeparator.ToString(), "\\\\") + "\\\\")
            + Id + "');\">" + szoveg + "</a>";
    }
}
