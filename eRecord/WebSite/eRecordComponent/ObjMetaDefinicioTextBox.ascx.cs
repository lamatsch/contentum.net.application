using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;

public partial class eRecordComponent_ObjMetaDefinicioTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, IScriptControl, Contentum.eUtility.ISearchComponent
{

    #region public properties

    private bool _RefreshCallingWindow = true;

    public bool RefreshCallingWindow
    {
        set { _RefreshCallingWindow = value; }
        get { return _RefreshCallingWindow; }
    }

    // ha felettest akarunk v�lasztani, a k�r�k kisz�r�s�hez meg kell adni az �rintett rekord Id-j�t
    private string _NoAncestorLoopWithId = "";

    public string NoAncestorLoopWithId
    {
        set { _NoAncestorLoopWithId = value; }
        get { return _NoAncestorLoopWithId; }
    }

    // ha al�rendeltet akarunk v�lasztani, a k�r�k kisz�r�s�hez meg kell adni az �rintett rekord Id-j�t
    private string _NoSuccessorLoopWithId = "";

    public string NoSuccessorLoopWithId
    {
        set { _NoSuccessorLoopWithId = value; }
        get { return _NoSuccessorLoopWithId; }
    }

    // ha felettest akarunk v�lasztani, sz�rni kell az A0 defin�ci�t�pusra
    private string _DefinicioTipusFilter = "";

    public string DefinicioTipusFilter
    {
        set { _DefinicioTipusFilter = value; }
        get { return _DefinicioTipusFilter; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            ContentTypeTextBox.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return ContentTypeTextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            ContentTypeTextBox.ReadOnly = value;
            AutoCompleteExtender1.Enabled = !value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return ContentTypeTextBox.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { ContentTypeTextBox.Text = value; }
        get { return ContentTypeTextBox.Text; }
    }

    public TextBox TextBox
    {
        get { return ContentTypeTextBox; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public String DefinicioTipus
    {
        get { return DefinicioTipus_HiddenField.Value; }
    }

    public String SPSSzinkronizalt
    {
        get { return SPSSzinkronizalt_HiddenField.Value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                ContentTypeTextBox.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                ContentTypeTextBox.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            //if (value == true)
            //{
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                //ViewImageButton.Visible = value;
                ViewImageButton.Visible = true;
                ResetImageButton.Visible = !value;
            //}
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_Reset
    {
        get { return ResetImageButton; }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public EREC_Obj_MetaDefinicio SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_Obj_MetaDefinicio = (EREC_Obj_MetaDefinicio)result.Record;
                Text = erec_Obj_MetaDefinicio.ContentType;

                DefinicioTipus_HiddenField.Value = erec_Obj_MetaDefinicio.DefinicioTipus;
                SPSSzinkronizalt_HiddenField.Value = erec_Obj_MetaDefinicio.SPSSzinkronizalt;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
            DefinicioTipus_HiddenField.Value = "";
            SPSSzinkronizalt_HiddenField.Value = "";
        }

        return erec_Obj_MetaDefinicio; // hiba eset�n �rt�ke null
    }

    #endregion

    #region AJAX
    private ScriptManager sm;
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }

    private bool customValueEnabled = true;

    public bool CustomValueEnabled
    {
        get { return customValueEnabled; }
        set { customValueEnabled = value; }
    }


    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {        

        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //String asdf = ContentTypeTextBox.Text;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        //ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.Visible = !ViewMode;

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax_eRecord.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            string contextkeys = felh_Id;

            // ha van megadva DefinicioFilter, �tadjuk
            contextkeys += ";" + DefinicioTipusFilter;

            // ha van megadva No<...>LoopWithId, akkor sz�rni kell, ez�rt ezt is �tadjuk)
            contextkeys += ";" + NoAncestorLoopWithId;
            contextkeys += ";" + NoSuccessorLoopWithId;

            AutoCompleteExtender1.ContextKey = contextkeys;

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        if (ajaxEnabled)
        {
            //Manu�lis be�r�s eset�n az id �s t�pus t�rl�se
            string jsClearHiddenField = "$get('" + HiddenField1.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + DefinicioTipus_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + SPSSzinkronizalt_HiddenField.ClientID + @"').value = '';";

            TextBox.Attributes.Add("onchange", jsClearHiddenField);
        }

    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("ObjMetaDefinicioLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + ContentTypeTextBox.ClientID
            + "&" + QueryStringVars.DefinicioTipusHiddenFieldId + "=" + DefinicioTipus_HiddenField.ClientID
            + "&" + QueryStringVars.SPSSzinkronizaltHiddenFieldId + "=" + SPSSzinkronizalt_HiddenField.ClientID
            + (!String.IsNullOrEmpty(NoAncestorLoopWithId) ? "&" + QueryStringVars.NoAncestorLoopWithId + "=" + NoAncestorLoopWithId : "")
            + (!String.IsNullOrEmpty(NoSuccessorLoopWithId) ? "&" + QueryStringVars.NoSuccessorLoopWithId + "=" + NoSuccessorLoopWithId : "")
            + (!String.IsNullOrEmpty(DefinicioTipusFilter) ? "&" + QueryStringVars.DefinicioTipus + "=" + DefinicioTipusFilter : "")
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0")
            , Defaults.PopupWidth, Defaults.PopupHeight, ContentTypeTextBox.ClientID, "", false);

        OnClick_New = JavaScripts.SetOnClientClick("ObjMetaDefinicioForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + ContentTypeTextBox.ClientID
            + "&" + QueryStringVars.DefinicioTipusHiddenFieldId + "=" + DefinicioTipus_HiddenField.ClientID
            + "&" + QueryStringVars.SPSSzinkronizaltHiddenFieldId + "=" + SPSSzinkronizalt_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0")
            , Defaults.PopupWidth, Defaults.PopupHeight, ContentTypeTextBox.ClientID, "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "ObjMetaDefinicioForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';" +
            "$get('" + DefinicioTipus_HiddenField.ClientID + "').value = '';" +
            "$get('" + SPSSzinkronizalt_HiddenField.ClientID + "').value = '';" +
            "return false;";

        #region Javascript Componens l�trehoz�sa

        if (ajaxEnabled)
        {
            //add script reference
            //ScriptManager.RegisterClientScriptInclude(Page, Page.GetType(), "ObjMetaDefinicio.js", "JavaScripts/ObjMetaDefinicio.js");

            //create javascript component
            //StringBuilder initBuilder = new StringBuilder();
            //initBuilder.AppendLine("Sys.Application.add_init(function() {");
            //initBuilder.Append("    ");

            //string js = "";
            //js = "$create(Utility.ObjMetaDefinicioBehavior,{\"AutoCompleteExtenderId\":\"" + this.AutoCompleteExtender1.ClientID + "\","
            //     + "\"id\":\"" + this.TextBox.ClientID + "_ObjMetaDefinicioBehavior\", \"HiddenFieldId\":\"" + this.HiddenField.ClientID + "\","
            //     + "\"DefinicioTipusHiddenFieldId\":\"" + this.DefinicioTipus_HiddenField.ClientID +"\"},null,null,$get('" + this.TextBox.ClientID + "'));";

            //initBuilder.AppendLine(js);
            //initBuilder.AppendLine("});");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), this.TextBox.ClientID + "_ObjMetaDefinicioBehaviorInit", initBuilder.ToString(), true);
        }

        #endregion

    }

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(ContentTypeTextBox);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion


    #region IScriptControl Members

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.ObjMetaDefinicioBehavior", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldId", this.HiddenField1.ClientID);
        descriptor.AddProperty("DefinicioTipusHiddenFieldId", this.DefinicioTipus_HiddenField.ClientID);
        descriptor.AddProperty("SPSSzinkronizaltHiddenFieldId", this.SPSSzinkronizalt_HiddenField.ClientID);
        descriptor.AddProperty("CustomValueEnabled", this.CustomValueEnabled);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/ObjMetaDefinicio.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
