<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjektumTargyszavaiPanel.ascx.cs"
Inherits="eRecordComponent_ObjektumTargyszavaiPanel" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<hr id="hrTop" runat="server" visible="false" />
<asp:DataList ID="DataListObjektumTargyszavai" runat="server" Width="<%# Width %>" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
    <ItemTemplate>
	        <td class="mrUrlapCaption">
	            <asp:HiddenField ID="TargyszoTipus_HiddenField" runat="server" Value='<%# Eval("Tipus") %>' />
		        <asp:HiddenField ID="ObjektumTargyszavaiId_HiddenField" runat="server" Value='<%# Eval("Id") %>' />
		        <asp:HiddenField ID="ObjMetaAdataiId_HiddenField" runat="server" Value='<%# Eval("Obj_Metaadatai_Id") %>' />
		        <asp:HiddenField ID="Regexp_HiddenField" runat="server" Value='<%# Eval("RegExp") %>' />
		        <asp:HiddenField ID="TargyszoId_HiddenField" runat="server" Value='<%# Eval("Targyszo_Id") %>' />
		        <asp:HiddenField ID="Targyszo_HiddenField" runat="server" Value='<%# Eval("Targyszo") %>' />
		        <asp:HiddenField ID="Targyszo_ControlTypeDataSource" runat="server" Value='<%# Eval("ControlTypeDataSource") %>' />
                <asp:HiddenField ID="Targyszo_ParentTargyszo" runat="server" Value='<%# Eval("ParentTargyszo") %>' />
                <%--BLG_608--%>
<%--		        <asp:Label ID="LabelTargyszoStar" runat="server" Text="*" CssClass="ReqStar"
		            Visible='<%# (SearchMode || (Eval("Opcionalis") as string) == "0") ? false : true %>' />--%>
                <asp:Label ID="LabelTargyszoStar" runat="server" Text="*" CssClass="ReqStar"
		            Visible='<%# (SearchMode || (Eval("Opcionalis") as string) == "1") ? false : true %>' />

		        <asp:Label ID="LabelTargyszo" runat="server" Text='<%# string.Format("{0}:", Eval("Targyszo")) %>' style="white-space:nowrap;" />
	        </td>
	        <td class="mrUrlapMezo">
                 <%--BLG_608--%>
<%--		        <eUI:DynamicValueControl ID="DynamicValueControl_TargyszoErtek" CssClass="mrUrlapInput"
		        DefaultControlTypeSource='<%# Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CustomTextBox %>'
		        ControlTypeSource='<%# SearchMode ? Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.MapForSearchForm(Eval("ControlTypeSource") as string) : Eval("ControlTypeSource") %>'
		        runat="server"
		        SearchMode='<%# SearchMode %>'
		        ViewMode='<%# ViewMode %>'
		        Value='<%# Eval("Ertek") %>'
		        Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>'
                Validate='<%# (SearchMode || (Eval("Opcionalis") as string) == "0") ? false : true %>'
                 />--%>
   		        <eUI:DynamicValueControl ID="DynamicValueControl_TargyszoErtek" CssClass="mrUrlapInput"
		        DefaultControlTypeSource='<%# Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CustomTextBox %>'
		        ControlTypeSource='<%# SearchMode ? Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.MapForSearchForm(Eval("ControlTypeSource") as string) : Eval("ControlTypeSource") %>'
		        runat="server"
		        SearchMode='<%# SearchMode %>'
		        ViewMode='<%# ViewMode %>'
		        Value='<%# Eval("Ertek") %>'
		        Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>'
                Validate='<%# (SearchMode || (Eval("Opcionalis") as string) == "1") ? false : true %>'
                Ismetlodo ='<%# (Eval("Ismetlodo") as string) == "1" ? true : false  %>'
                 />

		        <%-- M�dos�t�s vizsg�lat�hoz --%>
		        <asp:HiddenField ID="Ertek_HiddenField" runat="server" Value='<%# Eval("Ertek") %>' />
		        <asp:HiddenField ID="AlapertelmezettErtek_HiddenField" runat="server" Value='<%# Eval("AlapertelmezettErtek") %>' />
	        </td>
    </ItemTemplate>
</asp:DataList>
<hr id="hrBottom" runat="server" visible="false" />