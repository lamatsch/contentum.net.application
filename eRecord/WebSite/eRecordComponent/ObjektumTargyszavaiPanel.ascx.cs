using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery.FullTextSearch;
using System.ComponentModel;
using Contentum.eUIControls;

public partial class eRecordComponent_ObjektumTargyszavaiPanel : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    #region public properties

    private Char _Separator = '|';

    public Char Separator
    {
        get { return _Separator; }
        set { _Separator = value; }
    }

    public int Count
    {
        get { return DataListObjektumTargyszavai.Items.Count; }
    }

    public bool HrAtTopVisible
    {
        set { hrTop.Visible = value; }
        get { return hrTop.Visible; }
    }

    public bool HrAtBottomVisible
    {
        set { hrBottom.Visible = value; }
        get { return hrBottom.Visible; }
    }

    private bool _Enabled = true;

    public bool Enabled
    {
        set { _Enabled = value; }
        get { return _Enabled; }
    }

    private bool _ReadOnly = false;

    public bool ReadOnly
    {
        set
        {
            _ReadOnly = value;

            //foreach (DataListItem item in DataListObjektumTargyszavai.Items)
            //{
            //    Label lbTargyszo = (Label)item.FindControl("LabelTargyszo");
            //    if (lbTargyszo != null && value)
            //    {
            //        lbTargyszo.CssClass = "mrUrlapInputWaterMarked";
            //    }
            //    else
            //    {
            //        lbTargyszo.CssClass = "";
            //    }

            //    //DynamicValueControl dvc = GetValueControl(item);
            //    //if (dvc != null)
            //    //{
            //    //    dvc.ReadOnly = value;
            //    //}
            //}
        }
        get { return _ReadOnly; }
    }

    private bool _SearchMode = false;

    public bool SearchMode
    {
        set
        {
            _SearchMode = value;
            //foreach (DataListItem item in DataListObjektumTargyszavai.Items)
            //{
            //    DynamicValueControl dvc = GetValueControl(item);
            //    if (dvc != null)
            //    {
            //        dvc.SearchMode = value;
            //    }
            //}
        }
        get { return _SearchMode; }
    }

    private bool _ViewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return _ViewMode;
        }
        set
        {
            if (value == true)
            {
                _ViewMode = value;
                //foreach (DataListItem item in DataListObjektumTargyszavai.Items)
                //{
                //    DynamicValueControl dvc = GetValueControl(item);
                //    if (dvc != null)
                //    {
                //        dvc.ViewMode = value;
                //        dvc.Validate = !value;
                //    }
                //}
            }
        }
    }

    private string _CssClass = "mrUrlapInputKozepes";

    public string CssClass
    {
        set
        {
            _CssClass = value;
            //SetClassForControls(value);
        }
        get { return _CssClass; }
    }

    private string _CssClassSearchMode = "mrUrlapInputKozepesFTS";

    public string CssClassSearchMode
    {
        set
        {
            _CssClassSearchMode = value;
            //SetClassForControls(value);
        }
        get { return _CssClassSearchMode; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                SetClassForControls("ViewReadOnlyWebControl");
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                SetClassForControls("ViewReadOnlyWebControl");
            }
        }
    }

    public int RepeatColumns
    {
        get
        {
            return DataListObjektumTargyszavai.RepeatColumns;
        }
        set
        {
            DataListObjektumTargyszavai.RepeatColumns = value;
        }
    }

    public RepeatDirection RepeatDirection
    {
        get
        {
            return DataListObjektumTargyszavai.RepeatDirection;
        }
        set
        {
            DataListObjektumTargyszavai.RepeatDirection = value;
        }
    }

    public RepeatLayout RepeatLayout
    {
        get
        {
            return DataListObjektumTargyszavai.RepeatLayout;
        }
        set
        {
            DataListObjektumTargyszavai.RepeatLayout = value;
        }
    }

    private Unit _width = Unit.Percentage(50);

    public Unit Width {
        get {
            return _width;
        }
        set {
            _width = value;
        }
    }

    public void SetClassForControls(string className)
    { 
        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            DynamicValueControl dvc = GetValueControl(item);
            if (dvc != null)
            {
                dvc.CssClass = className;
            }
        }
    }

    #endregion

    #region GetStyleForControlType

    private string GetStyleForControlType(string ControlTypeSource)
    {
        if (SearchMode)
        {
            switch (ControlTypeSource)
            {
                case Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CustomTextBox:
                    return "mrUrlapInputFTS";
                case Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.RequiredNumberBox:
                case Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.RequiredDecimalNumberBox:
                    return "mrUrlapInputNumber";
                default:
                    return (String.IsNullOrEmpty(this.CssClass) ? "mrUrlapInput" : this.CssClass);

            }
        }
        else
        {
            switch (ControlTypeSource)
            {
                case Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.RequiredNumberBox:
                case Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.RequiredDecimalNumberBox:
                    return "mrUrlapInputNumber";
                default:
                    return (String.IsNullOrEmpty(this.CssClass) ? "mrUrlapInput" : this.CssClass);

            }
        }
    }

    #endregion GetStyleForControlType

    #region Page

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        DynamicValueControl dvc = GetValueControl(item);
                        if (dvc != null)
                        {
                            dvc.CssClass = GetStyleForControlType(dvc.ControlTypeSource);
                            dvc.Enabled = Enabled;
                            dvc.ReadOnly = ReadOnly;

                            // BUG_8219
                            if (ReadOnly)
                            {
                                dvc.Validate = false;
                            }
                        }
                    }
                    break;
                default: break;
            }
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (RepeatLayout == RepeatLayout.Flow)
        {
            if (RepeatDirection == RepeatDirection.Horizontal)
            {
                for (int i = 0; i < DataListObjektumTargyszavai.Items.Count; i++)
                {
                    DataListItem item = DataListObjektumTargyszavai.Items[i];
                    if (i % RepeatColumns == 0)
                    {
                        writer.Write("<tr>");
                    }
                    item.RenderItem(writer, false, false);
                    if ((i % RepeatColumns == RepeatColumns - 1)
                        || (i == DataListObjektumTargyszavai.Items.Count - 1)
                        )
                    {
                        writer.Write("</tr>");
                    }
                }
            }
            else
            {
                int mod = DataListObjektumTargyszavai.Items.Count % RepeatColumns;
                int cntFullRows = DataListObjektumTargyszavai.Items.Count / RepeatColumns;
                int lineDiff = (RepeatColumns - 1) * cntFullRows + mod;
                int cntRows = ((DataListObjektumTargyszavai.Items.Count - 1) / RepeatColumns) + 1;

                int i = 0;
                for (int s = 0; s < cntRows && i < DataListObjektumTargyszavai.Items.Count; s++)
                {
                    writer.Write("<tr>");
                    for (int o = 0; o < RepeatColumns && (s * RepeatColumns + o < DataListObjektumTargyszavai.Items.Count); o++)
                    {
                        i = s + cntFullRows * o + Math.Min(o, mod);
                        DataListItem item = DataListObjektumTargyszavai.Items[i];
                        item.RenderItem(writer, false, false);
                    }
                    writer.Write("</tr>");
                }
            }
        }
        else
        {
            base.Render(writer);
        }
    }

    #endregion Page

    public FullTextSearchTree BuildFTSTreeFromList(eErrorPanel errorPanel)
    {
        string[] targyszoerteklist = GetTargyszoErtekList();
        FullTextSearchTree FTSTree = null;

        try
        {
            if (targyszoerteklist != null && targyszoerteklist.Length > 0)
            {
                foreach (string item in targyszoerteklist)
                {
                    string targyszo = item.Split(Separator)[0];
                    string ertek = item.Split(Separator)[1];
                    if (!String.IsNullOrEmpty(targyszo) && !String.IsNullOrEmpty(ertek))
                    {
                        // ha nincs benne �rt�kelhet� adat, ne is adjon vissza semmit,
                        // ez�rt csak akkor hozzuk l�tre, ha van mit hozz�adni
                        if (FTSTree == null)
                        {
                            FTSTree = new FullTextSearchTree("EREC_ObjektumTargyszavai");
                            FTSTree.SelectFields = "Obj_Id";
                        }

                        if (FTSTree.LeafCount > 0)
                        {
                            FTSTree.AddOperator(FullTextSearchTree.Operator.Intersect);
                        }

                        FTSTree.AddFieldValuePair("Targyszo", targyszo, true);
                        FTSTree.AddFieldValuePair("Ertek", ertek);
                    }
                }

                if (FTSTree != null)
                {
                    // a FormTemplateLoader csak ez alapj�n tudja vissza�ll�tani a mez�ket
                    FTSTree.SetInorderNodeList();
                }
            }
        }
        catch (FullTextSearchException e)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, e.ErrorCode, e.ErrorMessage);
            }
        }
        return FTSTree;
    }

    public void FillFromFTSTree(FullTextSearchTree FTSTree)
    {
        // vigy�zat, felt�telezz�k, hogy felv�ltva lett hozz�adva t�rgysz� �s �rt�k...!
        if (FTSTree != null)
        {
            List<string> items = FTSTree.GetLeafList();
            if (items != null)
            {
                int i = 0;
                while (i + 1 < items.Count)
                {
                    string items_targyszo = items[i];
                    string items_ertek = items[i + 1];
                    string[] item_elements_targyszo = items_targyszo.Split(FullTextSearchTree.FieldValueSeparator);
                    string[] item_elements_ertek = items_ertek.Split(FullTextSearchTree.FieldValueSeparator);
                    // t�rgysz� �rt�k�nek be�r�sa a mez�be
                    TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

                    i += 2;
                }
            }
        }
        else
        {
            ClearTextBoxValues();
        }

        SetControlsByControlSourceType(null);
    }

    /// <summary>
    /// A field �rt�k alapj�n megpr�b�l azonos�tani egy mez�t (label sz�veg) �s kit�lteni a hozz� tartoz� textboxot
    /// a value �rt�kkel.
    /// </summary>
    /// <param name="field">kit�ltend� mez� c�mk�je</param>
    /// <param name="value">textboxba �rand� �rt�k</param>
    /// <returns>igaz, ha a kit�lt�s siker�lt</returns>
    public bool TryFillFieldWithValue(string field, string value)
    {
        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            HiddenField hf_Targyszo = GetTargyszoHiddenField(item);
            DynamicValueControl dvc = GetValueControl(item);
            if (hf_Targyszo != null && dvc != null && hf_Targyszo.Value == field)
            {
                dvc.Value = value;
                return true;
            }
        }

        return false;
    }

    public void ClearTextBoxValues()
    {
        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            DynamicValueControl dvc = GetValueControl(item);
            if (dvc != null)
            {
                dvc.Value = String.Empty;
            }
        }
    }

    public String GetCheckErtekJavaScript()
    {
        HiddenField hf_RegExp = null;
        HiddenField hf_Tipus = null;

        String js = "";

        String js_Header = @"var errormsg = '';
                    var errorcnt = 0;
                    ";

        String js_Footer = @"if (errorcnt > 0)
                    {
                        alert('" + String.Format(Resources.Form.RegularExpressionValidationMessage, "' + '\\n\\n'") + @" + errormsg);
                        return false;
                    }
                    ";

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            hf_RegExp = (HiddenField)item.FindControl("Regexp_HiddenField");
            hf_Tipus = (HiddenField)item.FindControl("TargyszoTipus_HiddenField");
            HiddenField hf_Targyszo = GetTargyszoHiddenField(item);

            DynamicValueControl dvc = GetValueControl(item);

            if (hf_RegExp != null
                && hf_Tipus != null
                && hf_Targyszo != null
                && dvc != null
                && dvc.Control != null
                && hf_Tipus.Value == "1" && !String.IsNullOrEmpty(hf_RegExp.Value))
            {
                js += @"var text = $get('" + dvc.Control.ClientID + @"');
                    if(text && text.value != '') {
                        var re = new RegExp('" + hf_RegExp.Value + @"');
                        if (!re.test(text.value)) {
                            errormsg += '" + String.Format("{0}: ' + text.value + ' (RegExp: {1})", hf_Targyszo.Value, hf_RegExp.Value) + @"\n'
                            errorcnt++;
                        }
                     }
                    ";
            }

        }
        if (!String.IsNullOrEmpty(js))
        {
            js = js_Header + js + js_Footer;
        }
        return js;
    }

    /// <summary>
    /// A m�g nem mentett, csak defin�ci� szinten l�tez�, 1 t�pus� t�rgyszavak eset�n
    /// az alap�rtelmezett �rt�kkel t�lti ki a textboxot.
    /// </summary>
    /// <returns></returns>
    public void SetDefaultValues()
    {
        HiddenField hf_AlapertelmezettErtek = null;
        HiddenField hf_Tipus = null;
        HiddenField hf_Id = null;

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            hf_AlapertelmezettErtek = (HiddenField)item.FindControl("Alapertelmezettertek_HiddenField");
            hf_Tipus = (HiddenField)item.FindControl("TargyszoTipus_HiddenField");
            hf_Id = (HiddenField)item.FindControl("ObjektumTargyszavaiId_HiddenField");

            DynamicValueControl dvc = GetValueControl(item);
            if (hf_AlapertelmezettErtek != null
                && hf_Tipus != null
                && hf_Id != null
                && dvc != null
                && dvc.Control != null
                && String.IsNullOrEmpty(hf_Id.Value) && hf_Tipus.Value == "1" && !String.IsNullOrEmpty(hf_AlapertelmezettErtek.Value))
            {
                dvc.Value = hf_AlapertelmezettErtek.Value;
                // h�tt�r �s el�t�rsz�n jelzi, hogy az �rt�k m�g nincs mentve, hanem az alap�rtelmezett �rt�k ker�lt kit�lt�sre
                dvc.CssClass = "GridViewTextBoxChanged";
            }
        }
    }


    /// <summary>
    /// Felt�lti a panelt egy adott objektum t�pusnak megfelel�en
    /// a DefinicioTipus oszt�ly� hozz�rendel�sekkel (vagy ha az nem adott, mindegyikkel),
    /// konkr�t vagy �ltal�nos objektumhoz. Ha a hozz�rendel�s (konkr�t objektum eset�n) l�tezik, lek�rdezi az �rt�keket is.
    /// Bemen� param�terek az objektum metadefin�ci� meghat�roz�s�hoz:
    /// Objektum metadefin�ci� Id VAGY
    /// a) t�blan�v (t�bl�hoz k�t�tt objektum metadefin�ci�: B1 t�pus),
    /// b) t�blan�v + oszlopn�v (oszlop�rt�khez k�t�tt objektum metadefin�ci�k B2 vagy C2, mind)
    /// c) t�blan�v + oszlopn�v + oszlop�rt�k (oszlop�rt�khez k�t�tt objektum metadefin�ci�, konkr�t)
    /// d) defin�ci� t�pus (Figyelmen k�v�l hagyva, ha az Objektum metadefin�ci� Id van megadva! A sz�r�s a nem automatikus elemekre nem vonatkozik!)
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlop�rt�keket jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    /// <param name="errorPanel">Hiba ki�r�s�hoz.</param>
    public void FillObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
        , String Obj_Id, String ObjMetaDefinicio_Id
        , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, eErrorPanel errorPanel)
    {

        this.Visible = true;

        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

        if (result.IsError)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }

            this.Visible = false;
        }
        else
        {
            DataListObjektumTargyszavai.DataSource = result.Ds;
            DataListObjektumTargyszavai.DataBind();

            if (this.Count == 0)
            {
                this.Visible = false;
            }
            else
            {
                SetControlsByControlSourceType(errorPanel);
            }
        }
    }

    /// <summary>
    /// Felt�lti a panelt egy adott objektum t�pusnak megfelel�en
    /// a DefinicioTipus oszt�ly� hozz�rendel�sekkel (vagy ha az nem adott, mindegyikkel),
    /// konkr�t vagy �ltal�nos objektumhoz. Ha a hozz�rendel�s (konkr�t objektum eset�n) l�tezik, lek�rdezi az �rt�keket is.
    /// Bemen� param�terek az objektum metadefin�ci� meghat�roz�s�hoz:
    /// Objektum metadefin�ci� Id VAGY
    /// a) t�blan�v (t�bl�hoz k�t�tt objektum metadefin�ci�: B1 t�pus),
    /// b) t�blan�v + oszlopn�v (oszlop�rt�khez k�t�tt objektum metadefin�ci�k B2 vagy C2, mind)
    /// c) t�blan�v + oszlopn�v + oszlop�rt�k (oszlop�rt�khez k�t�tt objektum metadefin�ci�, konkr�t)
    /// d) defin�ci� t�pus (Figyelmen k�v�l hagyva, ha az Objektum metadefin�ci� Id van megadva! A sz�r�s a nem automatikus elemekre nem vonatkozik!)
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlop�rt�keket jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    /// <param name="contentType">ContentType</param>
    /// <param name="errorPanel">Hiba ki�r�s�hoz.</param>
    public void FillObjektumTargyszavaiByContentType(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
        , string Obj_Id, string ObjMetaDefinicio_Id
        , string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude
        , string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus,
        Constants.EnumMetaDefinicioContentType contentType,
        eErrorPanel errorPanel)
    {
        this.Visible = true;

        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

        if (result.IsError)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }

            this.Visible = false;
            return;
        }

        result.Ds.Tables[0].DefaultView.RowFilter = string.Format("ContentType = '{0}'", contentType);
        DataTable dt = (result.Ds.Tables[0].DefaultView).ToTable();

        DataListObjektumTargyszavai.DataSource = dt;
        DataListObjektumTargyszavai.DataBind();

        if (this.Count == 0)
        {
            this.Visible = false;
        }
        else
        {
            SetControlsByControlSourceType(errorPanel);
        }
    }
    /// <summary>
    /// GetAllMetaByObjMetaDefinicio
    /// </summary>
    /// <param name="Obj_Id"></param>
    /// <param name="ObjMetaDefinicio_Id"></param>
    /// <param name="TableName"></param>
    /// <param name="ColumnName"></param>
    /// <param name="ColumnValues"></param>
    /// <param name="bColumnValuesExclude"></param>
    /// <param name="DefinicioTipus"></param>
    /// <param name="bCsakSajatSzint"></param>
    /// <param name="bCsakAutomatikus"></param>
    /// <returns></returns>
    private Result GetAllMetaByObjMetaDefinicio(string Obj_Id, string ObjMetaDefinicio_Id, string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        EREC_ObjektumTargyszavaiService service_ot = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result result = service_ot.GetAllMetaByObjMetaDefinicio(execParam, search, Obj_Id, ObjMetaDefinicio_Id
            , TableName, ColumnName, ColumnValues, bColumnValuesExclude
            , DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);
        return result;
    }


    /// <summary>
    /// Vannak hozz�rendelt targyszavak?
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlop�rt�keket jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    /// <param name="errorPanel">Hiba ki�r�s�hoz.</param>
    public bool HasObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
      , string Obj_Id, string ObjMetaDefinicio_Id
      , string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude
      , string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, eErrorPanel errorPanel)
    {

        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

        if (result.IsError)
            return false;

        if (result.Ds.Tables[0].Rows.Count < 1)
            return false;

        return true;
    }

    /// <summary>
    /// Vannak hozz�rendelt targyszavak?
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlop�rt�keket jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    /// <param name="contentType">contentType</param>
    /// <param name="errorPanel">Hiba ki�r�s�hoz.</param>
    public bool HasObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
      , string Obj_Id, string ObjMetaDefinicio_Id
      , string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude
      , string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus,
       Constants.EnumMetaDefinicioContentType contentType,
        eErrorPanel errorPanel)
    {
        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

        if (result.IsError)
            return false;

        result.Ds.Tables[0].DefaultView.RowFilter = string.Format("ContentType = '{0}'", contentType);
        DataTable dt = (result.Ds.Tables[0].DefaultView).ToTable();

        if (dt.Rows.Count < 1)
            return false;

        return true;
    }
    private void SetControlsByControlSourceType(eErrorPanel errorPanel)
    {
        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            DynamicValueControl dvc = GetValueControl(item);
            if (dvc != null)
            {
                HiddenField hf_ControlTypeDataSource = (HiddenField)item.FindControl("Targyszo_ControlTypeDataSource");
                HiddenField hf_Targyszo_ParentTargyszo = (HiddenField)item.FindControl("Targyszo_ParentTargyszo");
                if (!String.IsNullOrEmpty(dvc.ControlTypeName))
                {
                    // kodtarak dropdown
                    if (dvc.ControlTypeName.Equals("component_kodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (hf_ControlTypeDataSource != null && !String.IsNullOrEmpty(hf_ControlTypeDataSource.Value))
                        {
                            if (String.IsNullOrEmpty(dvc.Value))
                            {
                                dvc.Control.GetType().GetMethod("FillAndSetEmptyValue", new Type[] { typeof(string), typeof(eErrorPanel) }).Invoke(dvc.Control, new object[] { hf_ControlTypeDataSource.Value, errorPanel });
                            }
                            else
                            {
                                dvc.Control.GetType().GetMethod("FillAndSetSelectedValue"
                                    , new Type[] { typeof(String), typeof(String), typeof(bool), typeof(eErrorPanel) }).Invoke(dvc.Control
                                    , new object[] { hf_ControlTypeDataSource.Value, dvc.Value, true, errorPanel });
                            }
                        }
                    }

                    // fuggo kodtarak
                    if (dvc.ControlTypeName.Equals("component_fuggokodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (hf_ControlTypeDataSource != null && !String.IsNullOrEmpty(hf_ControlTypeDataSource.Value))
                        {
                            dvc.Control.GetType().GetProperty("KodcsoportKod").SetValue(dvc.Control, hf_ControlTypeDataSource.Value, null);

                        }

                        if (hf_Targyszo_ParentTargyszo != null && !String.IsNullOrEmpty(hf_Targyszo_ParentTargyszo.Value))
                        {
                            DynamicValueControl parentTargyszo = this.FindControlByTargyszo(hf_Targyszo_ParentTargyszo.Value);
                            if (parentTargyszo != null)
                            {
                                dvc.Control.GetType().GetProperty("ParentControlId").SetValue(dvc.Control, parentTargyszo.Control.UniqueID, null);
                            }
                        }
                    }

                    // kodtarak listbox
                    // BLG_608
                    if (dvc.ControlTypeName.Equals("component_kodtaraklistbox_ascx", StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (hf_ControlTypeDataSource != null && !String.IsNullOrEmpty(hf_ControlTypeDataSource.Value))
                        {
                            dvc.Control.GetType().GetProperty("Ismetlodo").SetValue(dvc.Control, dvc.Ismetlodo, null);
                            if (String.IsNullOrEmpty(dvc.Value))
                            {
                                dvc.Control.GetType().GetMethod("FillListBox", new Type[] { typeof(string), typeof(eErrorPanel) }).Invoke(dvc.Control, new object[] { hf_ControlTypeDataSource.Value, errorPanel });
                            }
                            else
                            {
                                // string[] selectedValues= dvc.Value.Split('|');
                                dvc.Control.GetType().GetMethod("FillAndSetSelectedValues"
                                    , new Type[] { typeof(String), typeof(String), typeof(eErrorPanel) }).Invoke(dvc.Control
                                    , new object[] { hf_ControlTypeDataSource.Value, dvc.Value, errorPanel });
                            }
                        }
                    }

                    // checkbox list
                    if (dvc.ControlTypeName.Equals("component_kodtarakcheckboxlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (hf_ControlTypeDataSource != null && !String.IsNullOrEmpty(hf_ControlTypeDataSource.Value))
                        {
                            if (String.IsNullOrEmpty(dvc.Value))
                            {
                                dvc.Control.GetType().GetMethod("FillListBox", new Type[] { typeof(string), typeof(eErrorPanel) }).Invoke(dvc.Control, new object[] { hf_ControlTypeDataSource.Value, errorPanel });
                            }
                            else
                            {
                                // string[] selectedValues= dvc.Value.Split('|');
                                dvc.Control.GetType().GetMethod("FillAndSetSelectedValues"
                                    , new Type[] { typeof(String), typeof(String), typeof(eErrorPanel) }).Invoke(dvc.Control
                                    , new object[] { hf_ControlTypeDataSource.Value, dvc.Value, errorPanel });
                            }
                        }
                    }
                }

                if (dvc.Control is UI.ILovListTextBox)
                {
                    //dvc.Control.GetType().GetMethod("SetTextBoxById").Invoke(dvc.Control, new object[] { errorPanel, null });
                    dvc.Control.GetType().GetMethod("SetTextBoxById"
                        , new Type[] { typeof(eErrorPanel), typeof(UpdatePanel) }).Invoke(dvc.Control
                            , new object[] { errorPanel, null });
                }
            }
        }
    }
    public DynamicValueControl FindValueControl(string value, string prop)
    {
        HiddenField hf_Targyszo = null;

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            hf_Targyszo = (HiddenField)item.FindControl(prop);
            if (hf_Targyszo != null && hf_Targyszo.Value == value)
            {
                return GetValueControl(item);
            }
        }

        return null;
    }

    public DynamicValueControl FindControlByTargyszo(string targyszo)
    {
        return FindValueControl(targyszo, "Targyszo_HiddenField");
    }

    public DynamicValueControl FindControlByTargyszoId(string targyszoId)
    {
        return FindValueControl(targyszoId, "TargyszoId_HiddenField");
    }

    public DynamicValueControl[] FindAllControlsOfType(Type controltype)
    {
        if (DataListObjektumTargyszavai.Items.Count > 0)
        {
            DynamicValueControl[] dvc_array = new DynamicValueControl[DataListObjektumTargyszavai.Items.Count];

            int index = 0;
            foreach (DataListItem item in DataListObjektumTargyszavai.Items)
            {
                DynamicValueControl dvc = GetValueControl(item);
                if (controltype.IsInstanceOfType(dvc.Control))
                {
                    dvc_array[index] = dvc;
                    index++;
                }
            }

            if (index > 0)
            {
                if (index <= dvc_array.Length)
                {
                    Array.Resize(ref dvc_array, index);
                }
                return dvc_array;
            }
        }
        return null;
    }

    // form --> string[]
    /// <summary>
    /// A t�rgyszavakb�l �s a hozz�rendelend� �rt�keib�l k�pzett, a Separator karakterrel
    /// elv�lasztott p�rokb�l, mint stringekb�l �ll� lista, pl. lek�rdez�sek �ssze�ll�t�s�hoz.
    /// </summary>
    /// <returns></returns>
    public string[] GetTargyszoErtekList()
    {
        List<string> targyszavakList = new List<string>();

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            HiddenField hf_Targyszo = GetTargyszoHiddenField(item);
            DynamicValueControl dvc = GetValueControl(item);
            if (hf_Targyszo != null && dvc != null)
            {
                targyszavakList.Add(hf_Targyszo.Value + Separator + dvc.Value);
            }
        }

        return targyszavakList.ToArray();
    }

    // form --> string[]
    /// <summary>
    /// Az aktualiz�land� �rt�keket tartalmaz� rekordlista - ha a t�pus "1" (azaz az �rt�k k�telez�),
    /// �s az �rt�k nincs kit�ltve, nem j�n l�tre rekord
    /// </summary>
    /// <returns></returns>
    public EREC_ObjektumTargyszavai[] GetEREC_ObjektumTargyszavaiList(bool bCsakModositott)
    {
        return GetEREC_ObjektumTargyszavaiList(bCsakModositott, false);
    }

    // form --> string[]
    /// <summary>
    /// Az aktualiz�land� �rt�keket tartalmaz� rekordlista - ha a t�pus "1" (azaz az �rt�k k�telez�),
    /// �s az �rt�k nincs kit�ltve, nem j�n l�tre rekord
    /// </summary>
    /// <returns></returns>
    public EREC_ObjektumTargyszavai[] GetEREC_ObjektumTargyszavaiList(bool bCsakModositott, bool bWithoutRecordId)
    {
        HiddenField hf_Id = null;
        HiddenField hf_ObjMetaAdataiId = null;
        HiddenField hf_TargyszoId = null;
        HiddenField hf_Tipus = null;
        HiddenField hf_Ertek = null;

        List<EREC_ObjektumTargyszavai> EREC_ObjektumTargyszavaiList = new List<EREC_ObjektumTargyszavai>();

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            hf_Id = (HiddenField)item.FindControl("ObjektumTargyszavaiId_HiddenField");
            hf_ObjMetaAdataiId = (HiddenField)item.FindControl("ObjMetaAdataiId_HiddenField");
            hf_TargyszoId = (HiddenField)item.FindControl("TargyszoId_HiddenField");
            HiddenField hf_Targyszo = GetTargyszoHiddenField(item);
            hf_Tipus = (HiddenField)item.FindControl("TargyszoTipus_HiddenField");
            hf_Ertek = (HiddenField)item.FindControl("Ertek_HiddenField");

            DynamicValueControl dvc = GetValueControl(item);
            if (hf_Id != null && hf_TargyszoId != null && hf_ObjMetaAdataiId != null
                && hf_Targyszo != null && dvc != null && hf_Tipus != null
                && !String.IsNullOrEmpty(hf_ObjMetaAdataiId.Value) && !String.IsNullOrEmpty(hf_TargyszoId.Value))
            {
                //if (hf_Tipus.Value != "1" || !String.IsNullOrEmpty(dvc.Value))
                if (true)
                {
                    if (bCsakModositott == false || (hf_Ertek != null && hf_Ertek.Value != dvc.Value))
                    {
                        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                        if (!String.IsNullOrEmpty(hf_Id.Value) && !bWithoutRecordId)
                        {
                            erec_ObjektumTargyszavai.Id = hf_Id.Value;
                            erec_ObjektumTargyszavai.Updated.Id = true;
                        }

                        erec_ObjektumTargyszavai.Obj_Metaadatai_Id = hf_ObjMetaAdataiId.Value;
                        erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                        erec_ObjektumTargyszavai.Targyszo_Id = hf_TargyszoId.Value;
                        erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                        erec_ObjektumTargyszavai.Targyszo = hf_Targyszo.Value;
                        erec_ObjektumTargyszavai.Updated.Targyszo = true;

                        erec_ObjektumTargyszavai.Ertek = dvc.Value ?? "";
                        erec_ObjektumTargyszavai.Updated.Ertek = true;

                        EREC_ObjektumTargyszavaiList.Add(erec_ObjektumTargyszavai);
                    }
                }
            }
        }

        return EREC_ObjektumTargyszavaiList.ToArray();
    }

    #region Template kezel�s

    public void LoadFromTemplate(TargyszavakTemplate targyszavakTemplate)
    {
        if (targyszavakTemplate == null)
        {
            return;
        }

        var props = targyszavakTemplate.GetProperties();
        foreach (var prop in props)
        {
            var attrs = prop.GetCustomAttributes(typeof(TargyszavakIdAttribute), false);
            if (attrs != null && attrs.Length == 1)
            {
                var id = (attrs[0] as TargyszavakIdAttribute).Id;
                if (!String.IsNullOrEmpty(id))
                {
                    DynamicValueControl dvc = FindControlByTargyszoId(id.ToUpper()) ?? FindControlByTargyszoId(id.ToLower());
                    if (dvc != null)
                    {
                        dvc.Value = prop.GetValue(targyszavakTemplate, null) as string;
                    }
                }
            }
        }
    }

    public void UpdateTemplate(TargyszavakTemplate targyszavakTemplate)
    {
        if (targyszavakTemplate == null)
        {
            return;
        }

        var objektumTargyszavaiList = GetEREC_ObjektumTargyszavaiList(false);
        if (objektumTargyszavaiList != null && objektumTargyszavaiList.Length > 0)
        {
            var props = targyszavakTemplate.GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(typeof(TargyszavakIdAttribute), false);
                if (attrs != null && attrs.Length == 1)
                {
                    var targyszoId = (attrs[0] as TargyszavakIdAttribute).Id;
                    if (!String.IsNullOrEmpty(targyszoId))
                    {
                        foreach (var objTsz in objektumTargyszavaiList)
                        {
                            if (objTsz.Targyszo_Id != null && objTsz.Targyszo_Id.ToLower() == targyszoId.ToLower())
                            {
                                prop.SetValue(targyszavakTemplate, objTsz.Ertek, null);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    public List<WebControl> GetComponentList()
    {
        return new List<WebControl> {
            DataListObjektumTargyszavai
        };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        foreach (DataListItem item in DataListObjektumTargyszavai.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        DynamicValueControl dvc = GetValueControl(item);
                        if (dvc != null)
                        {
                            text += Search.GetReadableWhereOnPanel(item);
                        }
                    }
                    break;
                default: break;
            }
        }

        return text.TrimEnd(Search.whereDelimeter.ToCharArray());
    }

    #endregion

    private DynamicValueControl GetValueControl(DataListItem item)
    {
        return item == null ? null : item.FindControl("DynamicValueControl_TargyszoErtek") as DynamicValueControl;
    }

    private HiddenField GetTargyszoHiddenField(DataListItem item)
    {
        return item == null ? null : item.FindControl("Targyszo_HiddenField") as HiddenField;
    }
}
