<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjektumTargyszavakMultiSearch.ascx.cs"
    Inherits="eRecordComponent_ObjektumTargyszavakMultiSearch" %>
<%@ Register Src="TargySzavakTextBox.ascx" TagName="TargySzavakTextBox" TagPrefix="tsztb" %>
<asp:UpdatePanel ID="ObjektumTargyszavakMultiSearchUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Repeater ID="RepeaterTargyszavak" runat="server"
            OnItemCommand="RepeaterCommand"
            OnItemDataBound="RepeaterItemBound">
            <HeaderTemplate>
                <table cellspacing="0" cellpadding="0" width="95%">
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="mrUrlapSor">
                        <td class="mrUrlapCaption_nowidth">
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:ImageButton ID="ImageButton_Remove" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                AlternateText="<%$Resources:Buttons,Remove%>" CommandName="Delete" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelOperator" runat="server" Text="�S" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelTargyszo" runat="server" Text="T�rgysz�:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tszmsTextBoxTargyszo" CssClass="mrUrlapInput" Text='<%# Eval("Targyszo") %>' runat="server" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelErtek" runat="server" Text="�rt�k:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tszmsTextBoxErtek" CssClass="mrUrlapInputKozepes" Text='<%# Eval("Ertek") %>' runat="server" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tszmsTextBoxNote" CssClass="mrUrlapInputKozepes" Text='<%# Eval("Note") %>' runat="server" />
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    <tr class="mrUrlapSor">
                        <td class="mrUrlapCaption_nowidth">
                            <asp:ImageButton ID="ImageButton_Add" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                AlternateText="<%$Resources:Buttons,Add%>" CommandName="Add" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:ImageButton ID="ImageButton_Cancel" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                AlternateText="<%$Resources:Buttons,Cancel%>" CommandName="Cancel" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelOperator" runat="server" Text="�S" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelTargyszo" runat="server" Text="T�rgysz�:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <%--<asp:TextBox ID="tszmsTextBoxTargyszo"  runat="server" />--%>
                            <tsztb:TargySzavakTextBox ID="Targyszavak_TextBoxTargyszo" runat="server" Validate="false"
                                SearchMode="true" RefreshCallingWindow="false" CssClass="mrUrlapInputFTS" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelErtek" runat="server" Text="�rt�k:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tszmsTextBoxErtek" CssClass="mrUrlapInputKozepesFTS" runat="server" />
                        </td>
                        <td class="mrUrlapCaption_nowidth">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tszmsTextBoxNote" CssClass="mrUrlapInputKozepesFTS" runat="server" />
                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater>        
    </ContentTemplate>
</asp:UpdatePanel>
