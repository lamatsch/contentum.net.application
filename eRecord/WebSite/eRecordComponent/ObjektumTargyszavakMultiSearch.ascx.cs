using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery.FullTextSearch;

public partial class eRecordComponent_ObjektumTargyszavakMultiSearch : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region private
    private enum TextBoxType { Targyszo, Ertek, Note }

    private const string columnnameTargyszo = "Targyszo";
    private const string columnnameErtek = "Ertek";
    private const string columnnameNote = "Note";

    private const string tablenameTargySzavak = "TargyszavakTable";
    
    private DataSet _ds = null;

    #endregion private

    #region public properties

    public int Count
    {
        get { return RepeaterTargyszavak.Items.Count; }
    }

    private bool _Enabled = true;

    public bool Enabled
    {
        set { _Enabled = value; }
        get { return _Enabled;  }
    }

    private bool _ReadOnly = true;

    public bool ReadOnly
    {
        set { _ReadOnly = value; }
        get { return _ReadOnly; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
               //Targyszavak_TextBoxTargyszo.TextBox.CssClass = "ViewReadOnlyWebControl";
               //Targyszavak_TextBoxErtek.CssClass = "ViewReadOnlyWebControl";
               //Targyszavak_TextBoxNote.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                //Targyszavak_TextBoxTargyszo.TextBox.CssClass = "ViewDisabledWebControl";
                //Targyszavak_TextBoxErtek.CssClass = "ViewDisabledWebControl";
                //Targyszavak_TextBoxNote.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region FTSTree handling
    public FullTextSearchTree BuildFTSTree(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FullTextSearchTree FTSTree = null;

        try
        {
            foreach (RepeaterItem item in RepeaterTargyszavak.Items)
            {
                FTSTree = AddItemContentToFTSTree(FTSTree, item);
            }

            // Footer kiolvas�sa:
            if (RepeaterTargyszavak.Controls != null && RepeaterTargyszavak.Controls.Count > 0)
            {
                RepeaterItem item = (RepeaterItem)RepeaterTargyszavak.Controls[RepeaterTargyszavak.Controls.Count - 1];
                FTSTree = AddItemContentToFTSTree(FTSTree, item);
            }

            if (FTSTree != null)
            {
                // a FormTemplateLoader csak ez alapj�n tudja vissza�ll�tani a mez�ket
                FTSTree.SetInorderNodeList();
            }

        }
        catch (FullTextSearchException e)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, e.ErrorCode, e.ErrorMessage);
            }
        }
        catch (Exception ex)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Resources.Error.DefaultErrorHeader, ex.Message);
            }
        }

        return FTSTree;
    }

    private FullTextSearchTree AddItemContentToFTSTree(FullTextSearchTree FTSTree, RepeaterItem item)
    {
        if (item != null)
        {
            TextBox tb_targyszo = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Targyszo);
            TextBox tb_ertek = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Ertek);
            TextBox tb_note = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Note);

            if ((tb_targyszo != null && !String.IsNullOrEmpty(tb_targyszo.Text))
                || (tb_ertek != null && !String.IsNullOrEmpty(tb_ertek.Text))
                || (tb_note != null && !String.IsNullOrEmpty(tb_note.Text))
                )
            {
                // ha nincs benne �rt�kelhet� adat, ne is adjon vissza semmit,
                // ez�rt csak akkor hozzuk l�tre, ha van mit hozz�adni
                if (FTSTree == null)
                {
                    FTSTree = new FullTextSearchTree("EREC_ObjektumTargyszavai");
                    FTSTree.SelectFields = "Obj_Id";
                }

                if (FTSTree.LeafCount > 0)
                {
                    FTSTree.AddOperator(FullTextSearchTree.Operator.Intersect);
                }
                if (tb_targyszo != null && !String.IsNullOrEmpty(tb_targyszo.Text))
                {
                    FTSTree.AddFieldValuePair(columnnameTargyszo, tb_targyszo.Text);
                }
                if (tb_ertek != null && !String.IsNullOrEmpty(tb_ertek.Text))
                {
                    FTSTree.AddFieldValuePair(columnnameErtek, tb_ertek.Text);
                }

                if (tb_note != null && !String.IsNullOrEmpty(tb_note.Text))
                {
                    FTSTree.AddFieldValuePair(columnnameNote, tb_note.Text);
                }
            }
        }

        return FTSTree;
    }

    public void FillFromFTSTree(FullTextSearchTree FTSTree)
    {
        ClearDataTable(false);
        if (FTSTree != null && FTSTree.bIsInorderNodeListSet)
        {
            List<List<string>> blockList = FTSTree.GetLogicallySeparatedLeafList();

            if (blockList != null && blockList.Count > 0)
            {
                // �tvessz�k a szerkezetet
                DataTable table = GetEmptyTable();
                DataRow row = null;

                // vigy�zat, felt�telezz�k, hogy az egyes sorokat halmazoper�tor tagolja
                // �s hogy egy blokkban egy mez�n�v (t�rgysz�, �rt�k vagy megjegyz�s) csak egyszer fordul el�
                // ha m�gis t�bbsz�r szerepelne, a mez�h�z tartoz� utols� �rt�ket �rjuk vissza
                foreach (List<string> items in blockList)
                {
                    row = table.NewRow();
                    foreach (string item in items)
                    {
                        string[] item_elements = item.Split(FullTextSearchTree.FieldValueSeparator);
                        if (item_elements.Length > 1)
                        {
                            string columnName = item_elements[0];
                            string columnValue = item_elements[1];

                            if (table.Columns.Contains(columnName))
                            {
                                row[columnName] = columnValue;
                            }
                        }

                    }

                    table.Rows.Add(row);
                }
                AddDataTable(table, false);
            }

        }
        Bind();
    }
    #endregion FTSTree handling

    #region ViewState handling
    protected void SaveDsToViewState()
    {
        ViewState["RepeaterDs"] = _ds;
    }

    protected DataSet GetDsFromViewstate()
    {
        DataSet ds = null;
        if (ViewState["RepeaterDs"] != null)
        {
            ds = (DataSet)ViewState["RepeaterDs"];
        }
        return ds;
    }
    #endregion ViewState handling

    #region Page
    protected void Page_Load(object sender, EventArgs e)
    {
        _ds = GetDsFromViewstate();

        if (_ds == null)
        {
            InitRepeaterDs();
        }

        // postbackn�l fel�l�rn�nk az eventeket
        if (!IsPostBack)
        {
            Bind();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in RepeaterTargyszavak.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        TextBox tb = (TextBox)item.FindControl("tszmsTextBoxTargyszo");
                        if (tb != null)
                        {
                            tb.ReadOnly = ReadOnly;
                            tb.Enabled = Enabled;
                        }

                        tb = (TextBox)item.FindControl("tszmsTextBoxErtek");
                        if (tb != null)
                        {
                            tb.ReadOnly = ReadOnly;
                            tb.Enabled = Enabled;
                        }

                        tb = (TextBox)item.FindControl("tszmsTextBoxNote");
                        if (tb != null)
                        {
                            tb.ReadOnly = ReadOnly;
                            tb.Enabled = Enabled;
                        }
                    }
                    break;
                default: break;
            }
        }
    }
    #endregion Page

    #region repeater/UI handling
    /// <summary>
    /// A Repeater egyes soraiban l�v� t�rgysz�, �rt�k, note ... textboxok kiolvas�sa a sor t�pus�t�l f�gg�en
    /// </summary>
    /// <param name="item"></param>
    /// <param name="tbType"></param>
    /// <returns></returns>
    private TextBox GetTextBoxFromItemByTextBoxType(RepeaterItem item, TextBoxType tbType)
    {
        TextBox tb = null;

        if (item != null)
        {
            switch (tbType)
            {
                case TextBoxType.Targyszo:
                    switch (item.ItemType)
                    {
                        case ListItemType.Item:
                        case ListItemType.AlternatingItem:
                            tb = (TextBox)item.FindControl("tszmsTextBoxTargyszo");
                            break;
                        case ListItemType.Footer:
                            eRecordComponent_TargySzavakTextBox tsztb = (eRecordComponent_TargySzavakTextBox)item.FindControl("Targyszavak_TextBoxTargyszo");
                            if (tsztb != null)
                            {
                                tb = tsztb.TextBox;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case TextBoxType.Ertek:
                    switch (item.ItemType)
                    {
                        case ListItemType.Item:
                        case ListItemType.AlternatingItem:
                        case ListItemType.Footer:
                            tb = (TextBox)item.FindControl("tszmsTextBoxErtek");
                            break;
                        default:
                            break;
                    }
                    break;
                case TextBoxType.Note:
                    switch (item.ItemType)
                    {
                        case ListItemType.Item:
                        case ListItemType.AlternatingItem:
                        case ListItemType.Footer:
                            tb = (TextBox)item.FindControl("tszmsTextBoxNote");
                            break;
                        default:
                            break;
                    }
                    break;
            }
        }

        return tb;
    }
    
    /// <summary>
    /// Visszat�lti a tartalmat a mentett RepeaterItemb�l a beviteli mez�kbe.
    /// Pl. Sor t�rl�se ut�n h�vhat�, hogy ne vesszen el a felhaszn�l�i input az adat �jrak�t�se sor�n
    /// </summary>
    /// <param name="FTSTree"></param>
    /// <param name="item"></param>
    private void FillRepeaterFooterControlsFromItem(RepeaterItem item)
    {
        if (item != null)
        {
            RepeaterItem footeritem = null;
            if (RepeaterTargyszavak.Controls != null && RepeaterTargyszavak.Controls.Count > 0)
            {
                footeritem = (RepeaterItem)RepeaterTargyszavak.Controls[RepeaterTargyszavak.Controls.Count - 1];
            }

            if (footeritem != null)
            {
                #region get input item controls
                TextBox tb_targyszo = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Targyszo);
                TextBox tb_ertek = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Ertek);
                TextBox tb_note = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Note);
                #endregion get input item controls

                #region get footer item controls
                TextBox tb_targyszo_footer = GetTextBoxFromItemByTextBoxType(footeritem, TextBoxType.Targyszo);
                TextBox tb_ertek_footer = GetTextBoxFromItemByTextBoxType(footeritem, TextBoxType.Ertek);
                TextBox tb_note_footer = GetTextBoxFromItemByTextBoxType(footeritem, TextBoxType.Note);
                #endregion get footer item controls

                #region fill controls
                if (tb_targyszo != null && tb_targyszo_footer != null)
                {
                    tb_targyszo_footer.Text = tb_targyszo.Text;
                }

                if (tb_ertek != null && tb_ertek_footer != null)
                {
                    tb_ertek_footer.Text = tb_ertek.Text;
                }

                if (tb_note != null && tb_note_footer != null)
                {
                    tb_note_footer.Text = tb_note.Text;
                }
                #endregion fill controls
            }
        }

    }

    private void FillRowFromRepeaterItem(RepeaterItem item, DataRow row)
    {
        TextBox tb_targyszo = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Targyszo);
        TextBox tb_ertek = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Ertek);
        TextBox tb_note = GetTextBoxFromItemByTextBoxType(item, TextBoxType.Note);

        if (tb_targyszo != null && row.Table.Columns.Contains(columnnameTargyszo))
        {
            row[columnnameTargyszo] = tb_targyszo.Text;
        }

        if (tb_ertek != null && row.Table.Columns.Contains(columnnameErtek))
        {
            row[columnnameErtek] = tb_ertek.Text;
        }

        if (tb_note != null && row.Table.Columns.Contains(columnnameNote))
        {
            row[columnnameNote] = tb_note.Text;
        }
    }

    #endregion repeater/UI handling

    #region repeater/ds handling
    /// <summary>
    /// A lek�rdez�s forr�s adathalmaz�nak l�trehoz�sa
    /// </summary>
    public void InitRepeaterDs()
    {
        _ds = new DataSet();

        DataTable table = GetEmptyTable();
        _ds.Tables.Add(table);

        SaveDsToViewState();
    }

    /// <summary>
    /// A lek�rdez�s forr�s adathalmaz�nak ki�r�t�se
    /// </summary>
    public void ClearDataTable(bool bBind)
    {
        if (_ds == null)
        {
            _ds = GetDsFromViewstate();
        }

        if (_ds != null && _ds.Tables[tablenameTargySzavak] != null)
        {
            _ds.Tables[tablenameTargySzavak].Rows.Clear();
            SaveDsToViewState();

            if (bBind)
            {
                Bind();
            }
        }
    }

    /// <summary>
    /// �res t�bla l�trehoz�sa a megfelel� szerkezettel
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmptyTable()
    {
        DataTable table = new DataTable(tablenameTargySzavak);
        DataColumn column;

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = columnnameTargyszo;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = columnnameErtek;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = columnnameNote;
        table.Columns.Add(column);

        return table;
    }

    private void Bind()
    {
        RepeaterTargyszavak.DataSource = _ds;
        RepeaterTargyszavak.DataMember = tablenameTargySzavak;
        RepeaterTargyszavak.DataBind();
    }

    /// <summary>
    /// Sor hozz�ad�sa a keres�si felt�telek mez�j�hez
    /// </summary>
    /// <param name="inputDataRow">Adatsor, amely tartalmazza a relev�ns mez�ket.
    /// Pontosan azon mez�k �rt�keit vessz�k �t (stringk�nt), amelyek mind az input adatsorban,
    /// melyek mind keres�si mez� adathalmazban l�v� t�bl�j�nak oszlopai k�z�tt szerepelnek.</param>
    /// <param name="bBind">Ha �rt�ke true, v�grehajtjuk az adatk�t�st (megjelen�t�s), ha false, akkor csak bels�leg t�roljuk az �j sorokat.</param>
    public void AddDataRow(DataRow inputDataRow, bool bBind)
    {
        if (inputDataRow == null) return;
        if (_ds == null)
        {
            InitRepeaterDs();
        }

        #region Add relevant row fields
        DataRow row = _ds.Tables[tablenameTargySzavak].NewRow();

        foreach (DataColumn col in _ds.Tables[tablenameTargySzavak].Columns)
        {
            string columnName = col.ColumnName;
            if (inputDataRow.Table.Columns.Contains(columnName))
            {
                row[columnName] = inputDataRow[columnName].ToString();
            }
        }

        _ds.Tables[tablenameTargySzavak].Rows.Add(row);

        SaveDsToViewState();

        #endregion Add relevant row fields

        if (bBind)
        {
            Bind();
        }
    }

    /// <summary>
    /// Sor hozz�ad�sa a keres�si felt�telek mez�j�hez
    /// </summary>
    /// <param name="inputDataRow">Adatsor, amely tartalmazza a relev�ns mez�ket.
    /// Pontosan azon mez�k �rt�keit vessz�k �t (stringk�nt), amelyek mind az input adatsorban,
    /// melyek mind keres�si mez� adathalmazban l�v� t�bl�j�nak oszlopai k�z�tt szerepelnek. Az �tvett sorokat megjelen�tj�k.</param>
    public void AddDataRow(DataRow inputDataRow)
    {
        AddDataRow(inputDataRow, true);
    }

    /// <summary>
    /// T�bl�zat sorok hozz�ad�sa a keres�si felt�telek mez�j�hez
    /// </summary>
    /// <param name="inputDataRow">datt�bla, amely soraiban tartalmazza a relev�ns mez�ket.
    /// Pontosan azon mez�k �rt�keit vessz�k �t (stringk�nt), amelyek mind az input adatsorban,
    /// melyek mind keres�si mez� adathalmazban l�v� t�bl�j�nak oszlopai k�z�tt szerepelnek.</param>
    public void AddDataTable(DataTable inputDataTable)
    {
        AddDataTable(inputDataTable, true);
    }

    /// <summary>
    /// T�bl�zat sorok hozz�ad�sa a keres�si felt�telek mez�j�hez
    /// </summary>
    /// <param name="inputDataTable">Adatt�bla, amely soraiban tartalmazza a relev�ns mez�ket.
    /// Pontosan azon mez�k �rt�keit vessz�k �t (stringk�nt), amelyek mind az input adatsorban,
    /// melyek mind keres�si mez� adathalmazban l�v� t�bl�j�nak oszlopai k�z�tt szerepelnek.</param>
    /// <param name="bBind">Ha �rt�ke true, v�grehajtjuk az adatk�t�st (megjelen�t�s), ha false, akkor csak bels�leg t�roljuk az �j sorokat.</param>
    public void AddDataTable(DataTable inputDataTable, bool bBind)
    {
        if (inputDataTable == null) return;
        if (inputDataTable.Rows.Count > 0)
        {
            for (int i = 0; i < inputDataTable.Rows.Count - 1; i++)
            {
                DataRow row = inputDataTable.Rows[i];
                AddDataRow(row, false);
            }
            AddDataRow(inputDataTable.Rows[inputDataTable.Rows.Count - 1], bBind);
        }
    }

    #endregion repeater/ds handling

    #region repeater event handling
    protected void RepeaterCommand(object sender, RepeaterCommandEventArgs e)
    {
        Repeater repeater = (Repeater)sender;

        if (repeater != null)
        {
            switch (e.CommandName)
            {
                case "Add":
                    DataRow row = _ds.Tables[tablenameTargySzavak].NewRow();
                    FillRowFromRepeaterItem(e.Item, row);
                    _ds.Tables[tablenameTargySzavak].Rows.Add(row);
                    SaveDsToViewState();
                    Bind();
                    break;
                case "Cancel":
                    Bind();
                    break;
                case "Delete":
                    if (_ds.Tables[tablenameTargySzavak].Rows.Count > 0)
                    {
                        _ds.Tables[tablenameTargySzavak].Rows.RemoveAt(e.Item.ItemIndex);
                    }
                    SaveDsToViewState();
                    // Footer input ment�se
                    RepeaterItem footeritem = null;
                    if (repeater.Controls != null && repeater.Controls.Count > 0)
                    {
                        footeritem = (RepeaterItem)repeater.Controls[repeater.Controls.Count - 1];
                    }
                    Bind();
                    FillRepeaterFooterControlsFromItem(footeritem);
                    break;

            }
        }
    }

    protected void RepeaterItemBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater repeater = (Repeater)sender;

        if (repeater != null)
        {
            Label labelOperator = (Label)e.Item.FindControl("labelOperator");

            if (labelOperator != null)
            {
                labelOperator.Visible = (e.Item.ItemIndex > 0 || (e.Item.ItemType == ListItemType.Footer && repeater.Items.Count > 0));
            }
        }
    }
    #endregion repeater event handling

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        //componentList.Add(...);

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        foreach (RepeaterItem item in RepeaterTargyszavak.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        text += Search.GetReadableWhereOnPanel(item);
                    }
                    break;
                default: break;
            }
        }

           // Footer kiolvas�sa:
        if (RepeaterTargyszavak.Controls != null && RepeaterTargyszavak.Controls.Count > 0)
        {
            RepeaterItem item = (RepeaterItem)RepeaterTargyszavak.Controls[RepeaterTargyszavak.Controls.Count - 1];
            if (item != null)
            {
                text += Search.GetReadableWhereOnPanel(item);
            }
        }


        return text.TrimEnd(Search.whereDelimeter.ToCharArray());
    }

    #endregion
}
