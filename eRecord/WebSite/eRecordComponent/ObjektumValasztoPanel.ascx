﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjektumValasztoPanel.ascx.cs" Inherits="eRecordComponent_ObjektumValasztoPanel" %>

<%@ Register Src="UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc1" %>
<%@ Register Src="IratTextBox.ascx" TagName="IratTextBox" TagPrefix="uc1" %>
<%@ Register Src="PldIratPeldanyokTextBox.ascx" TagName="IratPeldanyTextBox" TagPrefix="uc1" %>
<%@ Register Src="KuldemenyTextBox.ascx" TagName="KuldemenyTextBox" TagPrefix="uc1" %>

<asp:Panel ID="MainPanel" runat="server">
<asp:UpdatePanel runat="server" ID="UpdatePanelMain" UpdateMode="Conditional">
<ContentTemplate>
    <table cellspacing="0" cellpadding="0">
        <tr class="urlapSor">
            <td style="padding-right:5px;">
                <asp:DropDownList ID="ddListObjektumType" runat="server" AutoPostBack="true" CausesValidation="false">
                    <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Küldemény" Value="EREC_KuldKuldemenyek"></asp:ListItem>
                    <asp:ListItem Text="Ügyirat" Value="EREC_UgyUgyiratok"></asp:ListItem>
                    <asp:ListItem Text="Irat" Value="EREC_IraIratok"></asp:ListItem>
                    <asp:ListItem Text="Iratpéldány" Value="EREC_PldIratPeldanyok"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Panel ID="ObjetumTextBoxPanel" runat="server">
                    <asp:TextBox ID="Objetum_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Text=""></asp:TextBox>
                    <uc1:KuldemenyTextBox ID="EREC_KuldKuldemenyek_TextBox" runat="server" Visible="false" Validate="true"/>
                    <uc1:UgyiratTextBox ID="EREC_UgyUgyiratok_TextBox" runat="server" Visible="false" Validate="true"/>
                    <uc1:IratTextBox ID="EREC_IraIratok_TextBox" runat="server" Visible="false" Validate="true"/>
                    <uc1:IratPeldanyTextBox ID="EREC_PldIratPeldanyok_TextBox" runat="server"  Visible="false" Validate="true"/>
                </asp:Panel>
            </td>                            
        </tr>
        <tr class="urlapSor" runat="server" id="trFuggetlen" visible="false">
        <td colspan="2" style="text-align:left;font-weight:bold;position:relative;left:-5px;">
            <asp:CheckBox ID="cbFuggetlen" runat="server" Text="Független feljegyzések" AutoPostBack="true" CausesValidation="false"/>
        </td>    
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>