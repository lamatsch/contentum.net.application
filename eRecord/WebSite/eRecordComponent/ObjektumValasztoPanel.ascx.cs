﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;

public partial class eRecordComponent_ObjektumValasztoPanel : System.Web.UI.UserControl,Contentum.eUtility.ISearchComponent
{
    private bool _VisibleAllObjektumTipus = true;

    public bool VisibleAllObjektumTipus
    {
        get
        {
            object o = ViewState["VisibleAllObjektumTipus"];
            if (o != null)
                return (bool)ViewState["VisibleAllObjektumTipus"];
            return _VisibleAllObjektumTipus;
        }
        set
        {
            ViewState["VisibleAllObjektumTipus"] = value;
        }
    }

    public List<string> VisibleObjektumTipusList
    {
        get
        {
            object o = ViewState["VisibleObjektumTipusList"];
            if (o != null)
                return (List<string>)(ViewState["VisibleObjektumTipusList"]);
            return null;
        }
        set
        {
            ViewState["VisibleObjektumTipusList"] = value;
        }
    }

    public List<string> InVisibleObjektumTipusList
    {
        get
        {
            object o = ViewState["InVisibleObjektumTipusList"];
            if (o != null)
                return (List<string>)(ViewState["InVisibleObjektumTipusList"]);
            return null;
        }
        set
        {
            ViewState["InVisibleObjektumTipusList"] = value;
        }
    }

    private bool _EnableAllObjektumTipus = true;

    public bool EnableAllObjektumTipus
    {
        get
        {
            object o = ViewState["EnableAllObjektumTipus"];
            if (o != null)
                return (bool)ViewState["EnableAllObjektumTipus"];
            return _EnableAllObjektumTipus;
        }
        set
        {
            ViewState["EnableAllObjektumTipus"] = value;
        }
    }

    public List<string> EnabledObjektumTipusList
    {
        get
        {
            object o = ViewState["EnabledObjektumTipusList"];
            if (o != null)
                return (List<string>)(ViewState["EnabledObjektumTipusList"]);
            return null;
        }
        set
        {
            ViewState["EnabledObjektumTipusList"] = value;
        }
    }

    public List<string> DisabledObjektumTipusList
    {
        get
        {
            object o = ViewState["DisabledObjektumTipusList"];
            if (o != null)
                return (List<string>)(ViewState["DisabledObjektumTipusList"]);
            return null;
        }
        set
        {
            ViewState["DesabledObjektumTipusList"] = value;
        }
    }

    private string _Command = CommandName.New;

    public string Command
    {
        get { return _Command; }
        set { _Command = value; }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel = null;

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return errorPanel; }
        set { errorPanel = value; }
    }

    private UpdatePanel errorUpdatePanel = null;

    public UpdatePanel ErrorUpdatePanel
    {
        get { return errorUpdatePanel; }
        set { errorUpdatePanel = value; }
    }

    private bool _Validate = false;

    public bool Validate
    {
        get
        {
            object o = ViewState["Validate"];
            if (o != null)
                return (bool)ViewState["Validate"];
            return _Validate;
        }
        set
        {
            ViewState["Validate"] = value;
        }
    }

    public string ValidationGroup
    {
        set
        {
            foreach (Control c in GetAllObjektumTextBox())
            {
                if (c is UI.ILovListTextBox)
                {
                    UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                    tb.ValidationGroup = value;
                }
            }
        }
    }

    public bool Enabled
    {
        get
        {
            object o = ViewState["Enabled"];
            if (o != null)
                return (bool)o;
            return true;
        }
        set
        {
            ViewState["Enabled"] = value;
            foreach (Control c in GetAllObjektumTextBox())
            {
                if (c is UI.ILovListTextBox)
                {
                    UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                    tb.Enabled = value;
                }
            }
            ddListObjektumType.Enabled = value;
        }
    }

    private int _readonly = -1;
    public bool ReadOnly
    {
        get
        {
            object o = ViewState["ReadOnly"];
            if (o != null)
                return (bool)o;
            return true;
        }
        set
        {
            if (ObjetumTextBoxPanel != null)
            {
                ViewState["ReadOnly"] = value;
                foreach (Control c in GetAllObjektumTextBox())
                {
                    if (c is UI.ILovListTextBox)
                    {
                        UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                        tb.ReadOnly = value;
                    }
                }
                ddListObjektumType.ReadOnly = value;
            }
            else
            {
                _readonly = (value) ? 1 : 0;
            }
        }
    }

    private int _searchmode = -1;
    public bool SearchMode
    {
        get
        {
            object o = ViewState["SearchMode"];
            if (o != null)
                return (bool)o;
            return false;
        }
        set
        {
            if (ObjetumTextBoxPanel != null)
            {
                ViewState["SearchMode"] = value;
                foreach (Control c in GetAllObjektumTextBox())
                {
                    if (c is UI.ILovListTextBox)
                    {
                        UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                        tb.SearchMode = value;
                    }
                }
                trFuggetlen.Visible = value;
            }
            else
            {
                _searchmode = (value) ? 1 : 0;
            }
        }
    }

    public DropDownList ObjektumTipusDropDown
    {
        get
        {
            return ddListObjektumType;
        }
    }

    private event EventHandler _SelectedTipusChanged;

    public event EventHandler SelectedTipusChanged
    {
        add
        {
            _SelectedTipusChanged += value;
        }
        remove
        {
            _SelectedTipusChanged -= value;
        }
    }

    public void RaiseSelectedTipusChanged(object sender, EventArgs e)
    {
        if (_SelectedTipusChanged != null)
        {
            _SelectedTipusChanged(sender, e);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (_readonly > -1)
        {
            ReadOnly = (_readonly == 1) ? true : false;
            _readonly = -1;
        }

        if (_searchmode > -1)
        {
            SearchMode = (_searchmode == 1) ? true : false;
            _searchmode = -1;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ddListObjektumType.SelectedIndexChanged += new EventHandler(ddListObjektumType_SelectedIndexChanged);
        cbFuggetlen.CheckedChanged += new EventHandler(cbFuggetlen_CheckedChanged);
    }

    public bool IsFuggetlen
    {
        get
        {
            return cbFuggetlen.Checked;
        }
        set
        {
            if (value != cbFuggetlen.Checked)
            {
                cbFuggetlen.Checked = value;
                cbFuggetlen_CheckedChanged(cbFuggetlen, EventArgs.Empty);
            }
        }
    }

    void cbFuggetlen_CheckedChanged(object sender, EventArgs e)
    {
        if (IsFuggetlen)
        {
            this.Clear();
        }

        this.Enabled = !IsFuggetlen;

        UpdatePanelMain.Update();
    }

    void ddListObjektumType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetActiveObjectType();
        RaiseSelectedTipusChanged(this,EventArgs.Empty);
    }

    private void SetActiveObjectType()
    {
        SetAllObejktumTextBoxVisiblity(false);

        if (ddListObjektumType.SelectedIndex > -1)
        {
            GetObjektumTextBox(ddListObjektumType.SelectedValue).Visible = true;
        }
        else
        {
            Objetum_TextBox.Visible = true;
        }

        UpdatePanelMain.Update();
    }

    private void SetAllObejktumTextBoxVisiblity(bool value)
    {
        foreach (Control c in GetAllObjektumTextBox())
        {
            c.Visible = value;
        }
    }

    private string GetObjektumTextBoxID(string Obj_Type)
    {
        if (String.IsNullOrEmpty(Obj_Type))
            return String.Empty;

        return Obj_Type + "_TextBox";
    }

    private Control GetObjektumTextBox(string Obj_Type)
    {
        string ObjektumTextBoxID = GetObjektumTextBoxID(Obj_Type);

        if(!String.IsNullOrEmpty(ObjektumTextBoxID))
        {
            Control c = FindControl(ObjektumTextBoxID);
            if (c != null)
                return c;
        }

        return Objetum_TextBox;
    }

    private IEnumerable<Control> GetAllObjektumTextBox()
    {
        foreach (Control c in ObjetumTextBoxPanel.Controls)
        {
            yield return c;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    public void Clear()
    {
        ddListObjektumType.SelectedValue = String.Empty;
        SetActiveObjectType();
        foreach (Control c in GetAllObjektumTextBox())
        {
            if (c is UI.ILovListTextBox)
            {
                UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                tb.Text = tb.Id_HiddenField = String.Empty;
            }
        }
    }

    #region SetObjektum

    private bool IsObjektumSet(string Obj_Id, string Obj_Type)
    {
        if (GetSelected_Obj_Id() == Obj_Id && GetSelected_Obj_Type() == Obj_Type)
            return true;

        return false;
    }

    public void SetObjektum(string Obj_Id, string Obj_Type)
    {
        SetObjektum(Obj_Id, Obj_Type, String.Empty);
    }

    public void SetObjektum(string Obj_Id, string Obj_Type, string Obj_Azonosito)
    {
        if (String.IsNullOrEmpty(Obj_Type))
        {
            Clear();
            return;
        }

        if (IsObjektumSet(Obj_Id, Obj_Type)) return;

        ListItem li = ddListObjektumType.Items.FindByValue(Obj_Type);

        if (li != null)
        {
            ddListObjektumType.SelectedValue = li.Value;
            Control c = GetObjektumTextBox(Obj_Type);
            if (c is UI.ILovListTextBox)
            {
                UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                tb.Id_HiddenField = Obj_Id;
                if (!String.IsNullOrEmpty(Obj_Azonosito))
                {
                    tb.Text = Obj_Azonosito;
                }
                else
                {
                    tb.SetTextBoxById(ErrorPanel, ErrorUpdatePanel);
                }
            }
        }
        else
        {
            ddListObjektumType.SelectedIndex = -1;
        }

        SetActiveObjectType();
    }

    #endregion

    #region GetObjektum

    public Objektum SelectedObjektum
    {
        get
        {
            return GetSelected_Objektum();
        }
    }

    public string GetSelected_Obj_Id()
    {
        return SelectedObjektum.Id;
    }

    public string GetSelected_Obj_Type()
    {
        return SelectedObjektum.Type;
    }

    public string GetSelected_Obj_Type_Text()
    {
        if (ddListObjektumType.SelectedIndex > -1)
        {
            if (!String.IsNullOrEmpty(ddListObjektumType.SelectedValue))
            {
                return ddListObjektumType.SelectedItem.Text;
            }
        }

        return String.Empty;
    }

    public string GetSelected_Obj_Azonosito()
    {
        return SelectedObjektum.Azonosito;
    }

    private Objektum GetSelected_Objektum()
    {
        Objektum obj = new Objektum();
        obj.Azonosito = String.Empty;
        obj.Id = String.Empty;
        obj.Type = String.Empty;

        if (ddListObjektumType.SelectedIndex > -1)
        {
            if (!String.IsNullOrEmpty(ddListObjektumType.SelectedValue))
            {
                Control c = GetObjektumTextBox(ddListObjektumType.SelectedValue);
                if (c is UI.ILovListTextBox)
                {
                    UI.ILovListTextBox tb = c as UI.ILovListTextBox;
                    obj.Id = tb.Id_HiddenField;
                    obj.Azonosito = tb.Text;
                    obj.Type = ddListObjektumType.SelectedValue;
                }
            }
        }

        return obj;
    }
    #endregion

    //#region IScriptControl Members

    //private bool ajaxEnabled = false;
    //public bool AjaxEnabled
    //{
    //    get
    //    {
    //        return ajaxEnabled;
    //    }
    //    set
    //    {
    //        ajaxEnabled = value;
    //    }
    //}

    //private ScriptManager sm;

    //protected override void OnPreRender(EventArgs e)
    //{
    //    if (ajaxEnabled)
    //    {
    //        if (!this.DesignMode)
    //        {
    //            // Test for ScriptManager and register if it exists
    //            sm = ScriptManager.GetCurrent(Page);

    //            if (sm == null)
    //                throw new HttpException("A ScriptManager control must exist on the current page.");


    //            sm.RegisterScriptControl(this);
    //        }
    //    }

    //    base.OnPreRender(e);
    //}

    //protected override void Render(HtmlTextWriter writer)
    //{
    //    if (ajaxEnabled)
    //    {
    //        if (!this.DesignMode)
    //        {
    //            sm.RegisterScriptDescriptors(this);
    //        }
    //    }

    //    base.Render(writer);
    //}

    //public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    //{
    //    ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.ObjektumValasztoPanelBehavior", this.ClientID);
    //    return new ScriptDescriptor[] { descriptor };
    //}

    //public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    //{
    //    ScriptReference reference = new ScriptReference();
    //    reference.Path = "~/JavaScripts/ObjektumValasztoPanel.js";

    //    return new ScriptReference[] { reference };
    //}

    //#endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        if (IsFuggetlen)
        {
            return cbFuggetlen.Text;
        }

        if (!String.IsNullOrEmpty(GetSelected_Obj_Type()))
        {
            if (!String.IsNullOrEmpty(GetSelected_Obj_Azonosito()))
            {
                return GetSelected_Obj_Azonosito() + " (" + ddListObjektumType.SelectedItem.Text + ")";
            }
            else
            {
                return ddListObjektumType.SelectedItem.Text;
            }
        }

        return String.Empty;
    }

    #endregion
}
