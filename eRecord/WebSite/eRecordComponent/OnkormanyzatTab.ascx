<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OnkormanyzatTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratOnkormanyzatTab" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc1" %> 
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc" %>
<style type="text/css">
    .OnkormanyzatPanel .mrUrlapCaption {
        width: 350px;
    }

    .auto-style1 {
        width: 350px;
        height: 38px;
    }

    .auto-style2 {
        height: 38px;
    }
</style>
<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="OnkormanyzatUpdatePanel" runat="server" OnLoad="OnkormanyzatUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false" CssClass="OnkormanyzatPanel">
            <%-- HiddenFields --%>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="UGY_FAJTAJA_HiddenField" runat="server" />
            <%-- /HiddenFields --%>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <%-- 2. sor --%>
                    <tr class="mrUrlapSor" />
                    <td class="mrUrlapCaption"><%if (IsAfter2011)
                              { %>
                        <asp:Label ID="UGY_FAJTAJA_felirat_2012" runat="server" Text="2.) Az �gy fajt�ja:"></asp:Label>
                        <%}
                              else
                              {%>
                        <asp:Label ID="UGY_FAJTAJA_felirat" runat="server" Text="Az �gy fajt�ja:"></asp:Label>
                        <%}%></td>
                    <td class="mrUrlapMezo"><%-- <asp:Label ID="UGY_FAJTAJA_Label" runat="server" Text="" /> --%>
                        <uc1:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" />
                    </td>
                    <td style="text-align: right; padding-right: 10px;">
                        <asp:ImageButton ID="imgAdatlapNyomtatas" runat="server" AlternateText="'A' adatlap nyomtat�sa" ImageUrl="../images/hu/ovalgomb/a_adatlap_nyomtatas.gif" onmouseout="swapByName(this.id,'a_adatlap_nyomtatas.gif')" onmouseover="swapByName(this.id,'a_adatlap_nyomtatas2.gif')" />
                    </td>
                </tbody>
            </table>
            <eUI:eFormPanel ID="OnkormanyzatAdatokForm" runat="server">
                <table id="table_Adatok" cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <%-- 3. sor --%>
                        <tr class="mrUrlapSor">
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="DONTEST_HOZTA_felirat_2012" runat="server" Text="3.) A d�nt�st hozta:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="DONTEST_HOZTA_felirat" runat="server" Text="A d�nt�st hozta:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="DONTEST_HOZTA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 4. sor --%>
                        <tr class="mrUrlapSor">
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="DONTES_FORMAJA_felirat_2012" runat="server" Text="4.) A d�nt�s form�ja:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="DONTES_FORMAJA_felirat" runat="server" Text="A d�nt�s form�ja:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="DONTES_FORMAJA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 5. sor --%>
                        <tr class="mrUrlapSor">
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="UGYINTEZES_IDOTARTAMA_felirat_2012" runat="server" Text="5.) Az �gyint�z�s id�tartama:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="UGYINTEZES_IDOTARTAMA_felirat" runat="server" Text="Az �gyint�z�s id�tartama:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="UGYINTEZES_IDOTARTAMA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%if (IsAfter2011)
                            { %>
                        <%-- 6. sor --%>
                        <tr class="mrUrlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="HATARIDO_TULLEPES_felirat" runat="server" Text="6.) Az �rdemi d�nt�ssel kapcsolatos esetleges hat�rid�-t�ll�p�s id�tartama napokban:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="HATARIDO_TULLEPES_NumberBox" runat="server" MaxLength="3"
                                    Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%} %>
                        <tr>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 7. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="JOGORVOSLATI_ELJARAS_TIPUSA_felirat_2012" runat="server" Text="7.) A jogorvoslati elj�r�s t�pusa:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="JOGORVOSLATI_ELJARAS_TIPUSA_felirat" runat="server" Text="A jogorvoslati elj�r�s t�pusa:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 8. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="JOGORVOSLATI_DONTES_TIPUSA_felirat_2012" runat="server" Text="8.) A jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="JOGORVOSLATI_DONTES_TIPUSA_felirat" runat="server" Text="A jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 9. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="JOGORVOSLATI_DONTEST_HOZTA_felirat_2012" runat="server" Text="9.) A jogorvoslati elj�r�sban sz�letett d�nt�st meghozta:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="JOGORVOSLATI_DONTEST_HOZTA_felirat" runat="server" Text="A jogorvoslati elj�r�sban sz�letett d�nt�st meghozta:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 10. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <%if (IsAfter2011)
                                    { %>
                                <asp:Label ID="JOGORVOSLATI_DONTES_TARTALMA_felirat_2012" runat="server" Text="10.) A jogorvoslati elj�r�sban sz�letett d�nt�st tartalma:"></asp:Label>
                                <%}
                                    else
                                    {%>
                                <asp:Label ID="JOGORVOSLATI_DONTES_TARTALMA_felirat" runat="server" Text="A jogorvoslati elj�r�sban sz�letett d�nt�st tartalma:"></asp:Label>
                                <%}%>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList"
                                    runat="server" ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%if (IsAfter2011)
                            { %>
                        <%-- 11. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="JOGORVOSLATI_DONTES_felirat" runat="server" Text="11.) A d�nt�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="JOGORVOSLATI_DONTES_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <% } %>
                        <%if (IsAfter2014)
                            { %>
                        <%-- 12. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="HatosagiEllenorzes_Felirat" runat="server" Text="12.) Sor ker�lt-e hat�s�gi ellen�rz�s lefolytat�s�ra az adott �gyben:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <%--<asp:CheckBox ID="HatosagiEllenorzes_CheckBox" runat="server" />--%>
                                <asp:RadioButtonList ID="HatosagiEllenorzes_RadioButtonList" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="Igen" />
                                    <asp:ListItem Value="0" Text="Nem" />
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="HatosagiEllenorzes_RadioButtonList"
                                    Display="None" SetFocusOnError="true" Enabled="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender
                                    ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                    <Animations>
                                        <OnShow>
                                        <Sequence>
                                            <HideAction Visible="true" />
                                            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                        </Sequence>    
                                        </OnShow>
                                    </Animations>
                                </ajaxToolkit:ValidatorCalloutExtender>

                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 13. sor --%>
                        <%} %>
                        <tr id="row13" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="MunkaorakSzama_Felirat" runat="server" Text="13.) Az adott d�nt�s meghozatal�ra ford�tott �rdemi munka�r�k sz�ma (f�l�ra pontoss�ggal):"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <uc:RequiredNumberBox ID="MunkaorakSzamaOra_NumberBox" runat="server" MaxLength="3"
                                                Validate="true" Width="50px" RightAlign="True" />
                                        </td>
                                        <td style="vertical-align: bottom;">
                                            <span style="margin-left: 3px; padding-right: 3px; color: Black; font-weight: bold;">,</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="MunkaorakSzamaPerc_DropDownList" runat="server">
                                                <asp:ListItem Text="0" Value="0" Selected="True" />
                                                <asp:ListItem Text="5" Value="5" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%if (IsAfter2014)
                        { %>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 14. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <%--BLG_1026--%>
                                <%--<asp:Label ID="EljarasiKoltseg_Felirat" runat="server" Text="14.) Az adott d�nt�sben meg�llap�tott elj�r�si k�lts�g �sszege (Ft-ban):"></asp:Label>--%>
                                <asp:Label ID="EljarasiKoltseg_Felirat" runat="server" Text="14.) Az adott d�nt�s sor�n felmer�lt elj�r�si k�lts�g (Ft-ban):"></asp:Label>

                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="EljarasiKoltseg_NumberBox" runat="server"
                                    Validate="false" RightAlign="True" Width="100px" MaxLength="7" FormatNumber="True" NumberMaxLength="6"
                                    FilterType="NumbersWithGroupSeparator" />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 15. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="KozigazgatasiBirsagMerteke_Felirat" runat="server" Text="15.) Az adott d�nt�sben kiszabott k�zigazgat�si b�rs�g �sszege (Ft-ban):"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="KozigazgatasiBirsagMerteke_NumberBox" runat="server"
                                    Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                    FilterType="NumbersWithGroupSeparator" />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 16. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="SommasEljDontes_Felirat" runat="server" Text="16.) Somm�s elj�r�sban hozott d�nt�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="SOMMAS_DONTES_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 17. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="NyolcnaponBelulNemSommas_Felirat" runat="server" Text="17.) 8 napon bel�l lez�rt, nem somm�s elj�r�sban hozott d�nt�s:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 18. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="FUGGO_HATALYU_HATAROZAT_Felirat" runat="server" Text="18.) F�gg� hat�ly� hat�rozat:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 19. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="HATAROZAT_HATALYBA_LEPESE_Felirat" runat="server" Text="19.) Hat�rozat hat�lyba l�p�se:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 20. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="FUGGO_HATALYU_VEGZES_Felirat" runat="server" Text="20.) F�gg� hat�ly� v�gz�s:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="FUGGO_HATALYU_VEGZES_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 21. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="VEGZES_HATALYBA_LEPESE_Felirat" runat="server" Text="21.) V�gz�s hat�lyba l�p�se:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="VEGZES_HATALYBA_LEPESE_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 22. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="HatosagAltalVisszafizetettOsszeg_Felirat" runat="server" Text="<%$Forditas:HatosagAltalVisszafizetettOsszeg_Felirat|22.) a Ket 71/A. � (2) a) alapj�n a hat�s�g �ltal visszafizetett �sszeg (Ft-ban):%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="HatosagAltalVisszafizetettOsszeg_NumberBox" runat="server"
                                    Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                    FilterType="NumbersWithGroupSeparator" />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 23. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="HatosagotTerheloEljKtsg_Felirat" runat="server" Text="<%$Forditas:HatosagotTerheloEljKtsg_Felirat|23.) a Ket 71/A. � (2) b) alapj�n a hat�s�got terhel� elj�r�si k�lts�g �sszege (Ft-ban):%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="HatosagotTerheloEljKtsg_NumberBox" runat="server"
                                    Validate="false" RightAlign="True" Width="100px" MaxLength="10" FormatNumber="True" NumberMaxLength="8"
                                    FilterType="NumbersWithGroupSeparator" />
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <%-- 24. sor --%>
                        <tr>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="FELFUGGESZTETT_HATAROZAT" runat="server" Text="24.) Felf�ggesztett hat�rozat:"></asp:Label>
                            </td>
							<td class="mrUrlapMezo">
                                <uc1:KodtarakDropDownList ID="FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList" runat="server"
                                    ReadOnly="false" CssClass="mrUrlapInputComboBox_wide"></uc1:KodtarakDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapElvalasztoSor" />
                        <% } %>
                    </tbody>
                </table>
                <%-- TabFooter --%>
                <uc2:TabFooter ID="TabFooter1" runat="server" />
                <%-- /TabFooter --%>
            </eUI:eFormPanel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
