using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_UgyiratOnkormanyzatTab : System.Web.UI.UserControl
{
    private const string KodtarTextFormat = "#RovidNev) #Nev";

    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
    private const string kcs_DONTEST_HOZTA = "DONTEST_HOZTA";
	private const string kcs_DONTES_FORMAJA_ONK = "DONTES_FORMAJA_ONK";
    private const string kcs_DONTES_FORMAJA_ALLAMIG = "DONTES_FORMAJA_ALLAMIG";
	private const string kcs_UGYINTEZES_IDOTARTAMA = "UGYINTEZES_IDOTARTAMA";
    private const string kcs_JOGORVOSLATI_ELJARAS_TIPUSA = "JOGORVOSLATI_ELJARAS_TIPUSA";
    private const string kcs_JOGORVOSLATI_DONTES_TIPUSA = "JOGORVOSLATI_DONTES_TIPUSA";
    private const string kcs_JOGORVOSLATI_DONTEST_HOZTA = "JOGORVOSLATI_DONTEST_HOZTA";
    private const string kcs_JOGORVOSLATI_DONTES_TARTALMA = "JOGORVOSLATI_DONTES_TARTALMA";
    private const string kcs_JOGORVOSLATI_DONTES = "JOGORVOSLATI_DONTES";
    private const string kcs_SOMMAS_DONTES = "SOMMAS_DONTES";
    private const string kcs_NYOLCNAPON_BELUL_NEM_SOMMAS = "8NAPON_BELUL_NEM_SOMMAS";
    private const string kcs_FUGGO_HATALYU_HATAROZAT = "FUGGO_HATALYU_HATAROZAT";
    private const string kcs_HATAROZAT_HATALYBA_LEPESE = "HATAROZAT_HATALYBA_LEPESE";
    private const string kcs_FUGGO_HATALYU_VEGZES = "FUGGO_HATALYU_VEGZES";
    private const string kcs_VEGZES_HATALYBA_LEPESE = "VEGZES_HATALYBA_LEPESE";
    private const string kcs_FELFUGGESZTETT_HATAROZAT = "FELFUGGESZTETT_HATAROZAT";

    private const string errorText_RequiredValue = "Nincs megadva az �gy fajt�ja (hat�s�gi jellege). A rekord nem menthet�!";
    private const string errorText_ValueConfirm = "Az �gy fajt�j�t (hat�s�gi jelleg�t) k�s�bb m�r nem fogja tudni m�dos�tani.\\n\\nBiztosan menteni akarja a v�ltoz�sokat?";

    private const string strTableNameAdatok = "table_Adatok";

    //private List<System.Web.UI.Control> controls;

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;

    public string Command = "";

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    //private Boolean _Load = false;

    // BLG_354
    // BLG kapcs�n kiemelve az Iratok.cs-be
    //// az �tadott �rt�k alapj�n meghat�rozza, hogy hat�s�gi �gyfajta van-e kiv�lasztva
    //private bool isHatosagiByValue(string Value)
    //{
    //    switch (Value)
    //    {
    //        case "1": 
    //        case "2":
    //            return true;
    //        case "0":
    //            return false;
    //        default:
    //            //return true;
    //            return false;
    //    }
    //}

    protected bool IsAfter2011
    {
        get
        {
            return (HatosagiAdatokLetrehozas == null || HatosagiAdatokLetrehozas.Value.Year > 2011);
        }
    }

    protected bool IsAfter2014
    {
        get
        {
            return (HatosagiAdatokLetrehozas == null || HatosagiAdatokLetrehozas.Value.Year > 2014);
        }
    }

    private DateTime? HatosagiAdatokLetrehozas
    {
        get
        {
            object o = ViewState["HatosagiAdatokLetrehozas"];
            if (o == null)
                return null;

            return (DateTime)o;
        }
        set
        {
            ViewState["HatosagiAdatokLetrehozas"] = value;
            SetA_AdatlapUrl();
        }
    }

    private String RegisterCheckUgyFajtajaJavaScript(DropDownList dropDownList, HiddenField hiddenField)
    {
        string js = "";
        if (dropDownList != null) //if (dropDownList != null && dropDownList.Visible == true)
        {

            js = "var text = $get('" + dropDownList.ClientID + @"');
                    if(text && text.value == '') {
                        alert('" + errorText_RequiredValue + @"');
                        return false;
                     }
                 ";
            if (hiddenField != null)
            {
                js += @"var hidden = $get('" + hiddenField.ClientID + @"');
                        if(text && hidden && hidden.value == '') {
                        if (!confirm('" + errorText_ValueConfirm + @"'))
                        {
                            return false;
                        }
                     }
                  ";
            }

        }

        return js;
    }

    // be�ll�tja a mez�k l�that�s�g�t �s ha nem l�that�k, az controls-ban szerepl� objektumok �rt�k�t ''-re �ll�tja
    private String SetShowFieldsJavaScript(String ObjectName, List<System.Web.UI.Control> controls)
    {
        string js = "";
        if (!String.IsNullOrEmpty(ObjectName))
        {
            js = @"var obj=document.getElementById('" + ObjectName + @"');
                  if(obj) {
                        switch(this.value)
                        {
                            case '1':case '2':
                                obj.style.display = '';
                                break;
                            case '0':
                                obj.style.display = 'none';
                                break;
                            default:
                                obj.style.display = '';
                                break;
                        }
                  }
                 ";
        }

        if (controls != null && controls.Count > 0)
        {
            js += @"if (obj.style.display == 'none')
                  {
                      ";
            foreach (System.Web.UI.Control ctrl in controls)
            {
                js += @"ctrl = $get('" + ctrl.ClientID + @"');
                        if (ctrl) {
                            ctrl.value = '';
                        }
                       ";  
            }

            js += @"}
                   ";  
        }

        js += "return false;";

        return js;
    }

    private String SetShowFieldsJavaScript(String ObjectName)
    {
        string js = "";
        if (!String.IsNullOrEmpty(ObjectName))
        {
            js = @"var obj=document.getElementById('" + ObjectName + @"');
                  if(obj) {
                        switch(this.value)
                        {
                            case '1':case '2':
                                obj.style.display = '';
                                break;
                            case '0':
                                obj.style.display = 'none';
                                break;
                            default:
                                obj.style.display = '';
                                break;
                        }
                  }
                  return false;
                 ";
        }

        return js;
    }

    private void SetViewControls()
    {
        //UGY_FAJTAJA_Label.Enabled = false;
        UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
        DONTEST_HOZTA_KodtarakDropDownList.ReadOnly = true;
        DONTES_FORMAJA_KodtarakDropDownList.ReadOnly = true;
        UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.ReadOnly = true;
        HATARIDO_TULLEPES_NumberBox.ReadOnly = true;
        JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.ReadOnly = true;
        JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.ReadOnly = true;
        JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.ReadOnly = true;
        JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.ReadOnly = true;
        JOGORVOSLATI_DONTES_KodtarakDropDownList.ReadOnly = true;

		SOMMAS_DONTES_KodtarakDropDownList.ReadOnly = true;
		NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.ReadOnly = true;
		FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.ReadOnly = true;
		HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.ReadOnly = true;
		FUGGO_HATALYU_VEGZES_KodtarakDropDownList.ReadOnly = true;
		VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.ReadOnly = true;
		FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.ReadOnly = true;

        HatosagiEllenorzes_RadioButtonList.Enabled = false;
        MunkaorakSzamaOra_NumberBox.ReadOnly = true;
        MunkaorakSzamaPerc_DropDownList.ReadOnly = true;
        EljarasiKoltseg_NumberBox.ReadOnly = true;
        KozigazgatasiBirsagMerteke_NumberBox.ReadOnly = true;
		HatosagAltalVisszafizetettOsszeg_NumberBox.ReadOnly = true;
		HatosagotTerheloEljKtsg_NumberBox.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
        //imgAdatlapNyomtatas.Click += new ImageClickEventHandler(imgAdatlapNyomtatas_Click);
        //imgAdatlapNyomtatas.OnClientClick = "javascript:window.open('A_AdatlapPrintForm.aspx?" + QueryStringVars.Id + "=" + ParentId + "')";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            if (FormHeader != null)
            {
                FormHeader.TemplateObjectType = typeof(EREC_IraOnkormAdatok);
                //FormHeader.FormTemplateLoader1_Visibility = true; // csak akkor lesz l�that�, ha az OnkormanyzatTab akt�v, IraIratokForm �ll�tja
                FormHeader.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);
            }
            //controls = new List<System.Web.UI.Control>();
            //controls.Add(DONTEST_HOZTA_KodtarakDropDownList.DropDownList);
            //controls.Add(DONTES_FORMAJA_KodtarakDropDownList.DropDownList);
            //controls.Add(UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.DropDownList);
            //controls.Add(JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.DropDownList);
            //controls.Add(JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.DropDownList);
            //controls.Add(JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.DropDownList);
            //controls.Add(JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.DropDownList);

        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            // az �gy fajt�j�t meg kell adni
            //TabFooter1.ImageButton_Save.OnClientClick = RegisterCheckUgyFajtajaJavaScript(UGY_FAJTAJA_KodtarakDropDownList.DropDownList, UGY_FAJTAJA_HiddenField);
            // ha m�r volt mentve a rekord, az �gy fajt�ja nem m�dos�that�
            // UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = (String.IsNullOrEmpty(UGY_FAJTAJA_HiddenField.Value) ? false : true);
            UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
            // be�ll�t�st�l f�gg�en elrejti vagy megjelen�ti a beviteli mez�ket
            //UGY_FAJTAJA_KodtarakDropDownList.DropDownList.Attributes.Add("onChange", SetShowFieldsJavaScript(strTableNameAdatok, controls));
            //UGY_FAJTAJA_KodtarakDropDownList.DropDownList.Attributes.Add("onChange", SetShowFieldsJavaScript(strTableNameAdatok));
        }

        SetA_AdatlapUrl();
        row13.Visible = IsAfter2014;
    }

    protected void OnkormanyzatUpdatePanel_Load(object sender, EventArgs e)
    {
        // ha nem ez az akt�v tab, nem fut le
        if (!_Active) return;

        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshUgyiratokPanel:
                        ReLoadTab(true);
                        break;
                }
            }
        }

    }

    //Kiv�lr�l h�vhat�, a tab letilt�s�ra
    public bool IsHatosagiUgy()
    {
        // BLG_354
        // BLG kapcs�n kiemelve Iratok.cs-be
        // BLG_2020 (execcParam)
        return Iratok.IsHatosagiUgy(UI.SetExecParamDefault(Page), ParentId);
        //Result res = this.GetEREC_IraOnkormAdatok();

        //if (!res.IsError)
        //{
        //    EREC_IraOnkormAdatok record = res.Record as EREC_IraOnkormAdatok;
        //    if (record != null)
        //    {
        //        return isHatosagiByValue(record.UgyFajtaja);
        //    }
        //}
        //return true;

    }

    public void ReLoadTab()
    {
        //if (!IsPostBack)
        //{
            ReLoadTab(true);
        //}
        //else
        //{
        //    ReLoadTab(false);
        //}

    }

    private void ReLoadTab(bool loadRecord)
    {
        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord bet�lt�se

                        EREC_IraOnkormAdatokService service = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService();

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = ParentId;

                        // el�bb ellen�rizz�k, l�tezik-e a rekord, ha nem, l�tre kell hozni!
                        EREC_IraOnkormAdatokSearch search = new EREC_IraOnkormAdatokSearch();
                        search.Id.Value = ParentId;
                        search.Id.Operator = Query.Operators.equals;

                        Result result_getall = service.GetAll(execParam, search);
                        if (String.IsNullOrEmpty(result_getall.ErrorCode))
                        {
                            bool voltHiba = false;

                            // elvileg csak r�gi, nem ebben a rendszerben iktatott iratokra fut...
                            #region L�trehoz�s, ha kell
                            if (result_getall.Ds.Tables[0].Rows.Count == 0)
                            {
                                Result result_insert = InsertIraOnkormAdatokRecord(execParam);
                                if (!String.IsNullOrEmpty(result_insert.ErrorCode))
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_insert);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }

                            }
                            #endregion L�trehoz�s, ha kell

                            if (!voltHiba)
                            {
                                Result result = GetEREC_IraOnkormAdatok();
                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    EREC_IraOnkormAdatok erec_IraOnkormAdatok = (EREC_IraOnkormAdatok)result.Record;
                                    if (erec_IraOnkormAdatok == null)
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                            ResultError.CreateNewResultWithErrorCode(50101));
                                        ErrorUpdatePanel1.Update();
                                    }
                                    else
                                    {
                                        LoadComponentsFromBusinessObject(erec_IraOnkormAdatok);
                                        // BLG_354
                                        // BLG_2020 (execParam)
                                        SetVisibility_Hatosagi(Iratok.isHatosagiByValue(UI.SetExecParamDefault(Page), erec_IraOnkormAdatok.UgyFajtaja));
                                    }

                                }
                                else
                                {
                                    // nem l�tezik
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            } // nem volt hiba, ha l�tre is kellett hozni
                        }
                        else
                        {
                            // nem siker�lt lek�rdezni
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_getall);
                            ErrorUpdatePanel1.Update();
                        }

                    }
                    else
                    {
                        // BLG_354
                        // BLG_2020 (execParam)
                        SetVisibility_Hatosagi(Iratok.isHatosagiByValue(UI.SetExecParamDefault(Page), UGY_FAJTAJA_HiddenField.Value));
                    }
                }
            }


            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter1.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // UGY_FAJTAJA: nem m�dos�that� �rt�k, csak az iratt�ri t�telsz�mt�l �s az �gyt�pust�l f�gg

            // Komponensek l�that�s�g�nak be�ll�t�sa:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    private Result GetEREC_IraOnkormAdatok()
    {
        EREC_IraOnkormAdatokService service = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = ParentId;

        Result result = service.Get(execParam);

        return result;
    }

    private Result InsertIraOnkormAdatokRecord(ExecParam execParam)
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = null;
        EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = null;
        EREC_IraIratok erec_IraIratok = null;

        EREC_IraIratokService service_IraIratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        Result result_irathierarchia = service_IraIratok.GetIratHierarchia(execParam);

        if (!String.IsNullOrEmpty(result_irathierarchia.ErrorCode))
        {
            return result_irathierarchia;
        }

        IratHierarchia iratHierarchia = (IratHierarchia)result_irathierarchia.Record;

        if (iratHierarchia != null)
        {

            erec_IraIratok = iratHierarchia.IratObj;
            erec_UgyUgyiratdarabok = iratHierarchia.UgyiratDarabObj;
            erec_UgyUgyiratok = iratHierarchia.UgyiratObj;

            #region EREC_IraOnkormAdatok INSERT
            // mind a rekord Id-je, mind az IraIratok_Id megegyezik az irat azonos�t�j�val
            EREC_IraOnkormAdatokService service_IraOnkormAdatok = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService();
            ExecParam execparam_IraOnkormAdatok = UI.SetExecParamDefault(Page, new ExecParam());

            Logger.Debug("Hat�s�gi adatok INSERT start", execparam_IraOnkormAdatok);

            EREC_IraOnkormAdatok erec_IraOnkormAdatok = new EREC_IraOnkormAdatok();
            erec_IraOnkormAdatok.Id = ParentId;
            erec_IraOnkormAdatok.Updated.Id = true;
            erec_IraOnkormAdatok.IraIratok_Id = ParentId;
            erec_IraOnkormAdatok.Updated.IraIratok_Id = true;

            EREC_IratMetaDefinicioService service_IratMetaDefinicio = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam execparam_IratMetaDefinicio = UI.SetExecParamDefault(Page, new ExecParam());
            Result result_IraMetaDefinicio;

            // UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapj�n
            // (nem hat�s�gi v. �nkorm�nyzati v. �llamigazgat�si)
            // BLG_2020 
            if (Rendszerparameterek.GetBoolean(execparam_IraOnkormAdatok, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IratMetadefinicio_Id))
                {
                    execparam_IratMetaDefinicio.Record_Id = erec_UgyUgyiratok.IratMetadefinicio_Id;
                    result_IraMetaDefinicio = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                    if (!String.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                    {
                        Logger.Debug("Irat metadefin�ci� ill. �gyfajta meghat�roz�sa sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                        // hiba 
                        return result_IraMetaDefinicio;
                    }

                }

                if (!String.IsNullOrEmpty(erec_UgyUgyiratok.UgyTipus))
                {
                    EREC_IratMetaDefinicioSearch search_IratMetaDefinicio = new EREC_IratMetaDefinicioSearch();
                    search_IratMetaDefinicio.Ugytipus.Value = erec_UgyUgyiratok.UgyTipus;
                    search_IratMetaDefinicio.Ugytipus.Operator = Query.Operators.equals;

                    search_IratMetaDefinicio.Ugykor_Id.Value = erec_UgyUgyiratok.IraIrattariTetel_Id;
                    search_IratMetaDefinicio.Ugykor_Id.Operator = Query.Operators.equals;

                    search_IratMetaDefinicio.EljarasiSzakasz.Operator = Query.Operators.isnull;
                    search_IratMetaDefinicio.Irattipus.Operator = Query.Operators.isnull;

                    search_IratMetaDefinicio.TopRow = 1;

                    result_IraMetaDefinicio = service_IratMetaDefinicio.GetAll(execparam_IratMetaDefinicio, search_IratMetaDefinicio);

                    if (!String.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                    {
                        Logger.Debug("Irat metadefin�ci� ill. �gyfajta meghat�roz�sa sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                        // hiba 
                        return result_IraMetaDefinicio;
                    }
                    else
                    {
                        if (result_IraMetaDefinicio.Ds.Tables[0].Rows.Count > 0)
                        {
                            erec_IraOnkormAdatok.UgyFajtaja = result_IraMetaDefinicio.Ds.Tables[0].Rows[0]["UgyFajta"].ToString();
                            erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                        }
                        else
                        {
                            // nincs defin�ci� az UgyFajtaja-ra
                            erec_IraOnkormAdatok.UgyFajtaja = "<null>";
                            erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                        }
                    }
                }
            }
            else
            {
                // Ugyfajta Ugyiratb�l
                erec_IraOnkormAdatok.UgyFajtaja = erec_UgyUgyiratok.Ugy_Fajtaja;
                erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
            }

            Result result_IraOnkormAdatokInsert = service_IraOnkormAdatok.Insert(execparam_IraOnkormAdatok, erec_IraOnkormAdatok);
            if (!String.IsNullOrEmpty(result_IraOnkormAdatokInsert.ErrorCode))
            {
                Logger.Debug("�nkorm�nyzati adatok Insert sikertelen: ", execparam_IraOnkormAdatok, result_IraOnkormAdatokInsert);

                // hiba 
                //return result_IraOnkormAdatokInsert;
            }
            Logger.Debug("�nkorm�nyzati adatok Insert sikeres volt.");
            return result_IraOnkormAdatokInsert;

            #endregion
        }

        // IratHierarchia null
        result_irathierarchia.ErrorCode = "[50101]";
        result_irathierarchia.ErrorMessage = "Sikertelen irathierarchia lek�rdez�s.";
        Logger.Debug("�nkorm�nyzati adatok Insert sikertelen: ", execParam, result_irathierarchia);

        return result_irathierarchia;
    }

    /// <summary>
    /// Komponensek l�that�s�g�nak be�ll�t�sa
    /// </summary>
    private void SetComponentsVisibility(String _command)
    {

        if (_command == CommandName.View)
        {


        }

        if (_command == CommandName.Modify)
        {
        }

        #region TabFooter gombok �ll�t�sa

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion



        #region Form komponensek (TextBoxok,...) �ll�t�sa


        // A gombok l�that�s�ga alapb�l a Modify esetre van be�ll�tva,
        // View-n�l pluszban m�g azokat kell letiltani, amik m�dos�t�sn�l l�tszan�nak
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        #endregion

    }

    /// <summary>
    /// Komponensek l�that�s�g�nak be�ll�t�sa hat�s�gi st�tuszt�l f�gg�en
    /// </summary>
    private void SetVisibility_Hatosagi(bool isHatosagi)
    {
        OnkormanyzatAdatokForm.Visible = isHatosagi;
        imgAdatlapNyomtatas.Visible = isHatosagi;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraOnkormAdatok erec_IraOnkormAdatok)
    {
        if (!erec_IraOnkormAdatok.Base.Typed.LetrehozasIdo.IsNull)
        {
            HatosagiAdatokLetrehozas = erec_IraOnkormAdatok.Base.Typed.LetrehozasIdo.Value;
        }
        else
        {
            HatosagiAdatokLetrehozas = null;
        }

        Record_Ver_HiddenField.Value = erec_IraOnkormAdatok.Base.Ver;

        // ebb�l tudjuk, volt-e elmnetve m�r �gyfajta, vagy most �ll�tottuk be el�sz�r (szerint tiltjuk le a list�t)
        UGY_FAJTAJA_HiddenField.Value = erec_IraOnkormAdatok.UgyFajtaja;

        SetKodtarakErvenyesseg();
        SetKodtarakTextFormat();

         if (String.IsNullOrEmpty(erec_IraOnkormAdatok.UgyFajtaja))
        {
            //UGY_FAJTAJA_KodtarakDropDownList.FillAndSetEmptyValue(kcs_UGY_FAJTAJA, EErrorPanel1);
            UGY_FAJTAJA_KodtarakDropDownList.FillWithOneValue(kcs_UGY_FAJTAJA, "0", EErrorPanel1);
        }
        else
        {
            UGY_FAJTAJA_KodtarakDropDownList.FillWithOneValue(kcs_UGY_FAJTAJA, erec_IraOnkormAdatok.UgyFajtaja, EErrorPanel1);
        }

        if (Command == CommandName.View)
        {
            DONTEST_HOZTA_KodtarakDropDownList.FillWithOneValue(kcs_DONTEST_HOZTA, erec_IraOnkormAdatok.DontestHozta, EErrorPanel1);
            // nekrisz
            //DONTES_FORMAJA_KodtarakDropDownList.FillWithOneValue(kcs_DONTES_FORMAJA, erec_IraOnkormAdatok.DontesFormaja, EErrorPanel1);
            // BLG_2020
            //if (erec_IraOnkormAdatok.UgyFajtaja == "1")
            if ((erec_IraOnkormAdatok.UgyFajtaja == "1") || ((FelhasznaloProfil.OrgIsNMHH(Page) && (erec_IraOnkormAdatok.UgyFajtaja != KodTarak.UGY_FAJTAJA.NemHatosagiUgy))))
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillWithOneValue(kcs_DONTES_FORMAJA_ALLAMIG, erec_IraOnkormAdatok.DontesFormaja, EErrorPanel1);
            } else  // UgyFajtaja = '2'
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillWithOneValue(kcs_DONTES_FORMAJA_ONK, erec_IraOnkormAdatok.DontesFormaja, EErrorPanel1);
            }

            UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.FillWithOneValue(kcs_UGYINTEZES_IDOTARTAMA, erec_IraOnkormAdatok.UgyintezesHataridore,EErrorPanel1);
            JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.FillWithOneValue(kcs_JOGORVOSLATI_ELJARAS_TIPUSA, erec_IraOnkormAdatok.JogorvoslatiEljarasTipusa, EErrorPanel1);
            JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.FillWithOneValue(kcs_JOGORVOSLATI_DONTES_TIPUSA, erec_IraOnkormAdatok.JogorvoslatiDontesTipusa, EErrorPanel1);
            JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.FillWithOneValue(kcs_JOGORVOSLATI_DONTEST_HOZTA, erec_IraOnkormAdatok.JogorvoslatiDontestHozta, EErrorPanel1);
            JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.FillWithOneValue(kcs_JOGORVOSLATI_DONTES_TARTALMA, erec_IraOnkormAdatok.JogorvoslatiDontesTartalma, EErrorPanel1);
            JOGORVOSLATI_DONTES_KodtarakDropDownList.FillWithOneValue(kcs_JOGORVOSLATI_DONTES, erec_IraOnkormAdatok.JogorvoslatiDontes, EErrorPanel1);
  	  		SOMMAS_DONTES_KodtarakDropDownList.FillWithOneValue(kcs_SOMMAS_DONTES, erec_IraOnkormAdatok.SommasEljDontes, EErrorPanel1);
    		NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.FillWithOneValue(kcs_NYOLCNAPON_BELUL_NEM_SOMMAS, erec_IraOnkormAdatok.NyolcNapBelulNemSommas, EErrorPanel1);
            FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.FillWithOneValue(kcs_FUGGO_HATALYU_HATAROZAT, erec_IraOnkormAdatok.FuggoHatalyuHatarozat, EErrorPanel1);
    		HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.FillWithOneValue(kcs_HATAROZAT_HATALYBA_LEPESE, erec_IraOnkormAdatok.FuggoHatHatalybaLepes, EErrorPanel1);
    		FUGGO_HATALYU_VEGZES_KodtarakDropDownList.FillWithOneValue(kcs_FUGGO_HATALYU_VEGZES, erec_IraOnkormAdatok.FuggoHatalyuVegzes, EErrorPanel1);
    		VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.FillWithOneValue(kcs_VEGZES_HATALYBA_LEPESE, erec_IraOnkormAdatok.FuggoVegzesHatalyba, EErrorPanel1);
    		FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.FillWithOneValue(kcs_FELFUGGESZTETT_HATAROZAT, erec_IraOnkormAdatok.FelfuggHatarozat, EErrorPanel1);
			}
        else
        {
            DONTEST_HOZTA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTEST_HOZTA, erec_IraOnkormAdatok.DontestHozta, true, EErrorPanel1);
            // nekrisz
            //DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA, erec_IraOnkormAdatok.DontesFormaja, true, EErrorPanel1);
            // BLG_2020
            //if (erec_IraOnkormAdatok.UgyFajtaja == "1")
            if ((erec_IraOnkormAdatok.UgyFajtaja == "1") || ((FelhasznaloProfil.OrgIsNMHH(Page) && (erec_IraOnkormAdatok.UgyFajtaja != KodTarak.UGY_FAJTAJA.NemHatosagiUgy))))
            {
                    DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ALLAMIG, erec_IraOnkormAdatok.DontesFormaja, true, EErrorPanel1);
            }
            else  // UgyFajtaja = '2'
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ONK, erec_IraOnkormAdatok.DontesFormaja, true, EErrorPanel1);
            }
            UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_IDOTARTAMA, erec_IraOnkormAdatok.UgyintezesHataridore, true, EErrorPanel1);
            JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_ELJARAS_TIPUSA, erec_IraOnkormAdatok.JogorvoslatiEljarasTipusa, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES_TIPUSA, erec_IraOnkormAdatok.JogorvoslatiDontesTipusa, true, EErrorPanel1);
            JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTEST_HOZTA, erec_IraOnkormAdatok.JogorvoslatiDontestHozta, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES_TARTALMA, erec_IraOnkormAdatok.JogorvoslatiDontesTartalma, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES, erec_IraOnkormAdatok.JogorvoslatiDontes, true, EErrorPanel1);
			SOMMAS_DONTES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SOMMAS_DONTES, erec_IraOnkormAdatok.SommasEljDontes, true, EErrorPanel1);
    		NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.FillAndSetSelectedValue(kcs_NYOLCNAPON_BELUL_NEM_SOMMAS, erec_IraOnkormAdatok.NyolcNapBelulNemSommas, true, EErrorPanel1);
    		FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_HATAROZAT, erec_IraOnkormAdatok.FuggoHatalyuHatarozat, true, EErrorPanel1);
    		HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.FillAndSetSelectedValue(kcs_HATAROZAT_HATALYBA_LEPESE, erec_IraOnkormAdatok.FuggoHatHatalybaLepes, true, EErrorPanel1);
    		FUGGO_HATALYU_VEGZES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_VEGZES, erec_IraOnkormAdatok.FuggoHatalyuVegzes, true, EErrorPanel1);
    		VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VEGZES_HATALYBA_LEPESE, erec_IraOnkormAdatok.FuggoVegzesHatalyba, true, EErrorPanel1);
			FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FELFUGGESZTETT_HATAROZAT, erec_IraOnkormAdatok.FelfuggHatarozat, true, EErrorPanel1);
		}

        HATARIDO_TULLEPES_NumberBox.Text = erec_IraOnkormAdatok.HataridoTullepes;
		
        HatosagotTerheloEljKtsg_NumberBox.Text = erec_IraOnkormAdatok.HatTerheloEljKtsg;
        HatosagAltalVisszafizetettOsszeg_NumberBox.Text = erec_IraOnkormAdatok.HatAltalVisszafizOsszeg;

        SetHatosagiEllenorzesValue(erec_IraOnkormAdatok.HatosagiEllenorzes);
        SetMunkaorakSzama(erec_IraOnkormAdatok.MunkaorakSzama);
        EljarasiKoltseg_NumberBox.Text = erec_IraOnkormAdatok.EljarasiKoltseg;
        KozigazgatasiBirsagMerteke_NumberBox.Text = erec_IraOnkormAdatok.KozigazgatasiBirsagMerteke;
    }

    private void SetKodtarakErvenyesseg()
    {
        DateTime? Ervenyesseg = this.HatosagiAdatokLetrehozas;
        UGY_FAJTAJA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        DONTEST_HOZTA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        DONTES_FORMAJA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
        JOGORVOSLATI_DONTES_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
       	//SOMMAS_DONTES_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//FUGGO_HATALYU_VEGZES_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
   		//FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.Ervenyesseg = Ervenyesseg;
}

    private void SetKodtarakTextFormat()
    {
        string format = IsAfter2011 ? KodtarTextFormat : null;
        // BLG_2020
       // UGY_FAJTAJA_KodtarakDropDownList.TextFormat = format;
        DONTEST_HOZTA_KodtarakDropDownList.TextFormat = format;
        DONTES_FORMAJA_KodtarakDropDownList.TextFormat = format;
        UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.TextFormat = format;
        JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.TextFormat = format;
        JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.TextFormat = format;
        JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.TextFormat = format;
        JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.TextFormat = format;
        JOGORVOSLATI_DONTES_KodtarakDropDownList.TextFormat = format;
		SOMMAS_DONTES_KodtarakDropDownList.TextFormat = format;
   		NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.TextFormat = format;
   		FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.TextFormat = format;
   		HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.TextFormat = format;
   		FUGGO_HATALYU_VEGZES_KodtarakDropDownList.TextFormat = format;
   		VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.TextFormat = format;
   		FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.TextFormat = format;
    }

    // form --> business object
    private EREC_IraOnkormAdatok GetBusinessObjectFromComponents()
    {
        EREC_IraOnkormAdatok erec_IraOnkormAdatok = new EREC_IraOnkormAdatok();
        erec_IraOnkormAdatok.Updated.SetValueAll(false);
        erec_IraOnkormAdatok.Base.Updated.SetValueAll(false);

        //erec_IraOnkormAdatok.UgyFajtaja = UGY_FAJTAJA_HiddenField.Value;
        //erec_IraOnkormAdatok.Updated.UgyFajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_HiddenField);

        //erec_IraOnkormAdatok.UgyFajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
        //erec_IraOnkormAdatok.Updated.UgyFajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);

        // BLG_354
        // BLG_2020 (execParam)
        if (Iratok.isHatosagiByValue(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue))
        {
            erec_IraOnkormAdatok.DontestHozta = DONTEST_HOZTA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.DontestHozta = pageView.GetUpdatedByView(DONTEST_HOZTA_KodtarakDropDownList);
            erec_IraOnkormAdatok.DontesFormaja = DONTES_FORMAJA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.DontesFormaja = pageView.GetUpdatedByView(DONTES_FORMAJA_KodtarakDropDownList);
            erec_IraOnkormAdatok.UgyintezesHataridore = UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.UgyintezesHataridore = pageView.GetUpdatedByView(UGYINTEZES_IDOTARTAMA_KodtarakDropDownList);
            erec_IraOnkormAdatok.HataridoTullepes = HATARIDO_TULLEPES_NumberBox.Text;
            erec_IraOnkormAdatok.Updated.HataridoTullepes = pageView.GetUpdatedByView(HATARIDO_TULLEPES_NumberBox);
            erec_IraOnkormAdatok.JogorvoslatiEljarasTipusa = JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.JogorvoslatiEljarasTipusa = pageView.GetUpdatedByView(JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList);
            erec_IraOnkormAdatok.JogorvoslatiDontesTipusa = JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTipusa = pageView.GetUpdatedByView(JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList);
            erec_IraOnkormAdatok.JogorvoslatiDontestHozta = JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.JogorvoslatiDontestHozta = pageView.GetUpdatedByView(JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList);
            erec_IraOnkormAdatok.JogorvoslatiDontesTartalma = JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTartalma = pageView.GetUpdatedByView(JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList);
            erec_IraOnkormAdatok.JogorvoslatiDontes = JOGORVOSLATI_DONTES_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.JogorvoslatiDontes = pageView.GetUpdatedByView(JOGORVOSLATI_DONTES_KodtarakDropDownList);

			erec_IraOnkormAdatok.SommasEljDontes = SOMMAS_DONTES_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.SommasEljDontes = pageView.GetUpdatedByView(SOMMAS_DONTES_KodtarakDropDownList);
			erec_IraOnkormAdatok.NyolcNapBelulNemSommas = NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.NyolcNapBelulNemSommas = pageView.GetUpdatedByView(NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList);
			erec_IraOnkormAdatok.FuggoHatalyuHatarozat = FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.FuggoHatalyuHatarozat = pageView.GetUpdatedByView(FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList);
			erec_IraOnkormAdatok.FuggoHatHatalybaLepes = HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.FuggoHatHatalybaLepes = pageView.GetUpdatedByView(HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList);
			erec_IraOnkormAdatok.FuggoHatalyuVegzes = FUGGO_HATALYU_VEGZES_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.FuggoHatalyuVegzes = pageView.GetUpdatedByView(FUGGO_HATALYU_VEGZES_KodtarakDropDownList);
			erec_IraOnkormAdatok.FuggoVegzesHatalyba = VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.FuggoVegzesHatalyba = pageView.GetUpdatedByView(VEGZES_HATALYBA_LEPESE_KodtarakDropDownList);
			erec_IraOnkormAdatok.FelfuggHatarozat = FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.SelectedValue;
            erec_IraOnkormAdatok.Updated.FelfuggHatarozat = pageView.GetUpdatedByView(FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList);
	
			erec_IraOnkormAdatok.HatosagiEllenorzes = GetHatosagiEllenorzesValue();
            erec_IraOnkormAdatok.Updated.HatosagiEllenorzes = pageView.GetUpdatedByView(HatosagiEllenorzes_RadioButtonList);
            erec_IraOnkormAdatok.MunkaorakSzama = GetMunkaorakSzama();
            erec_IraOnkormAdatok.Updated.MunkaorakSzama = pageView.GetUpdatedByView(MunkaorakSzamaOra_NumberBox) || pageView.GetUpdatedByView(MunkaorakSzamaPerc_DropDownList);
            erec_IraOnkormAdatok.EljarasiKoltseg = GetMaskedNumber(EljarasiKoltseg_NumberBox.Text);
            erec_IraOnkormAdatok.Updated.EljarasiKoltseg = pageView.GetUpdatedByView(EljarasiKoltseg_NumberBox);
            erec_IraOnkormAdatok.KozigazgatasiBirsagMerteke = GetMaskedNumber((KozigazgatasiBirsagMerteke_NumberBox.Text));
            erec_IraOnkormAdatok.Updated.KozigazgatasiBirsagMerteke = pageView.GetUpdatedByView(KozigazgatasiBirsagMerteke_NumberBox);
            erec_IraOnkormAdatok.HatAltalVisszafizOsszeg = GetMaskedNumber((HatosagAltalVisszafizetettOsszeg_NumberBox.Text));
            erec_IraOnkormAdatok.Updated.HatAltalVisszafizOsszeg = pageView.GetUpdatedByView(HatosagAltalVisszafizetettOsszeg_NumberBox);
            erec_IraOnkormAdatok.HatTerheloEljKtsg = GetMaskedNumber((HatosagotTerheloEljKtsg_NumberBox.Text));
            erec_IraOnkormAdatok.Updated.HatTerheloEljKtsg = pageView.GetUpdatedByView(HatosagotTerheloEljKtsg_NumberBox);
        }

        erec_IraOnkormAdatok.IraIratok_Id = ParentId;
        erec_IraOnkormAdatok.Updated.IraIratok_Id = true;

        // verzi� visszaolvas�sa
        erec_IraOnkormAdatok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IraOnkormAdatok.Base.Updated.Ver = true;


        return erec_IraOnkormAdatok;
    }

    private void Load_ComponentSelectModul()
    {
        /*if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            // TODO: felkell sorolni...
            /*compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }*/
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
          e.CommandName == CommandName.SaveAndClose)
        {
            // TODO: funkci�jog
            //if (FunctionRights.GetFunkcioJog(Page, "UgyiratOnkormanyzatiAdat" + Command))
            if (true)
            {
                bool voltHiba = false;

                switch (Command)
                {
                    case CommandName.New:
                        {
                            // New nem lehets�ges
                            return;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_IraOnkormAdatokService service = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                                execparam.Record_Id = ParentId;

                                EREC_IraOnkormAdatok erec_IraOnkormAdatok = GetBusinessObjectFromComponents();

                                Result result = service.Update(execparam, erec_IraOnkormAdatok);
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                                else
                                {
                                    //UGY_FAJTAJA_HiddenField.Value = erec_IraOnkormAdatok.UgyFajtaja;
                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    ReLoadTab(false);
                                    return;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }
                            }


                            break;

                        }
                }   // switch


            }
            else
            {
                // TODO: right check? UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }

    public void imgAdatlapNyomtatas_Click(object sender, ImageClickEventArgs e)
    {
        // 
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (IsPostBack)
        {
            if (e.CommandName == CommandName.LoadTemplate)
            {
                //_Load = true;

                LoadComponentsFromTemplate((EREC_IraOnkormAdatok)FormHeader.TemplateObject);

                //_Load = false;
            }
            else if (e.CommandName == CommandName.NewTemplate)
            {
                FormHeader.NewTemplate(GetTemplateObjectFromComponents());
            }
            else if (e.CommandName == CommandName.SaveTemplate)
            {
                FormHeader.SaveCurrentTemplate(GetTemplateObjectFromComponents());
            }
            else if (e.CommandName == CommandName.InvalidateTemplate)
            {
                // FormTemplateLoader lekezeli           
            }
        }
    }

    private void LoadComponentsFromTemplate(EREC_IraOnkormAdatok templateObject)
    {
        if (templateObject != null)
        {
            DONTEST_HOZTA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTEST_HOZTA, templateObject.DontestHozta, true, EErrorPanel1);
            // nekrisz
            //DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA, templateObject.DontesFormaja, true, EErrorPanel1);
            if (templateObject.UgyFajtaja == "1")
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ALLAMIG, templateObject.DontesFormaja, true, EErrorPanel1);
            }
            else  // UgyFajtaja = '2'
            {
                DONTES_FORMAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DONTES_FORMAJA_ONK, templateObject.DontesFormaja, true, EErrorPanel1);
            }
            UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_IDOTARTAMA, templateObject.UgyintezesHataridore, true, EErrorPanel1);
            HATARIDO_TULLEPES_NumberBox.Text = templateObject.HataridoTullepes;
            JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_ELJARAS_TIPUSA, templateObject.JogorvoslatiEljarasTipusa, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES_TIPUSA, templateObject.JogorvoslatiDontesTipusa, true, EErrorPanel1);
            JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTEST_HOZTA, templateObject.JogorvoslatiDontestHozta, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES_TARTALMA, templateObject.JogorvoslatiDontesTartalma, true, EErrorPanel1);
            JOGORVOSLATI_DONTES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_JOGORVOSLATI_DONTES, templateObject.JogorvoslatiDontes, true, EErrorPanel1);

			SOMMAS_DONTES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SOMMAS_DONTES, templateObject.SommasEljDontes, true, EErrorPanel1);
			NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.FillAndSetSelectedValue(kcs_NYOLCNAPON_BELUL_NEM_SOMMAS, templateObject.NyolcNapBelulNemSommas, true, EErrorPanel1);
			FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_HATAROZAT, templateObject.FuggoHatalyuHatarozat, true, EErrorPanel1);
			HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.FillAndSetSelectedValue(kcs_HATAROZAT_HATALYBA_LEPESE, templateObject.FuggoHatHatalybaLepes, true, EErrorPanel1);
			FUGGO_HATALYU_VEGZES_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FUGGO_HATALYU_VEGZES, templateObject.FuggoHatalyuVegzes, true, EErrorPanel1);
			VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VEGZES_HATALYBA_LEPESE, templateObject.FuggoVegzesHatalyba, true, EErrorPanel1);
			FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.FillAndSetSelectedValue(kcs_FELFUGGESZTETT_HATAROZAT, templateObject.FelfuggHatarozat, true, EErrorPanel1);
            
			SetHatosagiEllenorzesValue(templateObject.HatosagiEllenorzes);
            SetMunkaorakSzama(templateObject.MunkaorakSzama);
            EljarasiKoltseg_NumberBox.Text = templateObject.EljarasiKoltseg;
            KozigazgatasiBirsagMerteke_NumberBox.Text = templateObject.KozigazgatasiBirsagMerteke;
            
			HatosagAltalVisszafizetettOsszeg_NumberBox.Text = templateObject.HatAltalVisszafizOsszeg;
            HatosagotTerheloEljKtsg_NumberBox.Text = templateObject.HatTerheloEljKtsg;
			
            SetHatosagiEllenorzesValue(templateObject.HatosagiEllenorzes);
            SetMunkaorakSzama(templateObject.MunkaorakSzama);
            EljarasiKoltseg_NumberBox.Text = templateObject.EljarasiKoltseg;
            KozigazgatasiBirsagMerteke_NumberBox.Text = templateObject.KozigazgatasiBirsagMerteke;
        }

    }

    private EREC_IraOnkormAdatok GetTemplateObjectFromComponents()
    {
        EREC_IraOnkormAdatok templateObject = new EREC_IraOnkormAdatok();

        templateObject.DontestHozta = DONTEST_HOZTA_KodtarakDropDownList.SelectedValue;
        templateObject.DontesFormaja = DONTES_FORMAJA_KodtarakDropDownList.SelectedValue;
        templateObject.UgyintezesHataridore = UGYINTEZES_IDOTARTAMA_KodtarakDropDownList.SelectedValue;
        templateObject.HataridoTullepes = HATARIDO_TULLEPES_NumberBox.Text;
        templateObject.JogorvoslatiEljarasTipusa = JOGORVOSLATI_ELJARAS_TIPUSA_KodtarakDropDownList.SelectedValue;
        templateObject.JogorvoslatiDontesTipusa = JOGORVOSLATI_DONTES_TIPUSA_KodtarakDropDownList.SelectedValue;
        templateObject.JogorvoslatiDontestHozta = JOGORVOSLATI_DONTEST_HOZTA_KodtarakDropDownList.SelectedValue;
        templateObject.JogorvoslatiDontesTartalma = JOGORVOSLATI_DONTES_TARTALMA_KodtarakDropDownList.SelectedValue;
        templateObject.JogorvoslatiDontes = JOGORVOSLATI_DONTES_KodtarakDropDownList.SelectedValue;

        templateObject.SommasEljDontes = SOMMAS_DONTES_KodtarakDropDownList.SelectedValue;
        templateObject.NyolcNapBelulNemSommas = NYOLCNAPON_BELUL_NEM_SOMMAS_KodtarakDropDownList.SelectedValue;
        templateObject.FuggoHatalyuHatarozat = FUGGO_HATALYU_HATAROZAT_KodtarakDropDownList.SelectedValue;
        templateObject.FuggoHatHatalybaLepes = HATAROZAT_HATALYBA_LEPESE_KodtarakDropDownList.SelectedValue;
        templateObject.FuggoHatalyuVegzes = FUGGO_HATALYU_VEGZES_KodtarakDropDownList.SelectedValue;
		templateObject.FuggoVegzesHatalyba = VEGZES_HATALYBA_LEPESE_KodtarakDropDownList.SelectedValue;
        templateObject.FelfuggHatarozat = FELFUGGESZTETT_HATAROZAT_KodtarakDropDownList.SelectedValue;

        templateObject.HatosagiEllenorzes = GetHatosagiEllenorzesValue();
        templateObject.MunkaorakSzama = GetMunkaorakSzama();
        templateObject.EljarasiKoltseg = GetMaskedNumber(EljarasiKoltseg_NumberBox.Text);
        templateObject.KozigazgatasiBirsagMerteke = GetMaskedNumber(KozigazgatasiBirsagMerteke_NumberBox.Text);
        templateObject.HatAltalVisszafizOsszeg = GetMaskedNumber(HatosagAltalVisszafizetettOsszeg_NumberBox.Text);
        templateObject.HatTerheloEljKtsg = GetMaskedNumber(HatosagotTerheloEljKtsg_NumberBox.Text);

        return templateObject;
    }

    private void SetA_AdatlapUrl()
    {
        string url = "A_AdatlapPrintForm.aspx?" + QueryStringVars.Id + "=" + ParentId;
        if (HatosagiAdatokLetrehozas != null)
        {
            url += "&Ev=" + HatosagiAdatokLetrehozas.Value.Year.ToString();
        }
        imgAdatlapNyomtatas.OnClientClick = "javascript:window.open('" + url  + "')";
    }

    private string GetHatosagiEllenorzesValue()
    {
        if (!String.IsNullOrEmpty(HatosagiEllenorzes_RadioButtonList.SelectedValue))
        {
            if (HatosagiEllenorzes_RadioButtonList.SelectedValue == "1" || HatosagiEllenorzes_RadioButtonList.SelectedValue == "0")
            {
                return HatosagiEllenorzes_RadioButtonList.SelectedValue;
            }
        }

        return String.Empty;
    }

    private void SetHatosagiEllenorzesValue(string value)
    {
        if (!String.IsNullOrEmpty(value))
        {
            if (value == "0" || value == "1")
            {
                HatosagiEllenorzes_RadioButtonList.SelectedValue = value;
            }
        }
    }

    private string GetMunkaorakSzama()
    {
        string text = String.Empty;

        if(!String.IsNullOrEmpty(MunkaorakSzamaOra_NumberBox.Text) || MunkaorakSzamaPerc_DropDownList.SelectedIndex > 0)
        {
            decimal number = 0;

            if(!String.IsNullOrEmpty(MunkaorakSzamaOra_NumberBox.Text))
            {
                number = MunkaorakSzamaOra_NumberBox.Number;
            }

            if(MunkaorakSzamaPerc_DropDownList.SelectedIndex == 1)
            {
                int selectedValue = 5;
                number = number + selectedValue / 10m;
            }

            text = number.ToString();
        }

        return text;
    }

    private void SetMunkaorakSzama(string text)
    {
        string textOra = String.Empty;
        string textPerc = String.Empty;

        if (!String.IsNullOrEmpty(text))
        {
            decimal number;
            if (Decimal.TryParse(text, out number))
            {
                decimal intPart = Math.Truncate(number);
                textOra = intPart.ToString();
                if(intPart != number)
                {
                    textPerc = Math.Truncate((number - intPart) * 10).ToString();
                }
            }
        }

        MunkaorakSzamaOra_NumberBox.Text = textOra;

        if (textPerc == "5")
        {
            MunkaorakSzamaPerc_DropDownList.SelectedIndex = 1;
        }
        else
        {
            MunkaorakSzamaPerc_DropDownList.SelectedIndex = 0;
        }

    }

    private string GetMaskedNumber(string text)
    {
        if (!String.IsNullOrEmpty(text))
        {
            return text.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator, "");
        }

        return String.Empty;
    }

}
