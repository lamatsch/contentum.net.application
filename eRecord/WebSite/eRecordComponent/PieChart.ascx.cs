using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class eRecordComponent_PieChart : System.Web.UI.UserControl
{

    public string ChartTitle = "";
    public int ImageHeight = 450;
    public int ImageWidth = 700;
    public string ImageAlt = "PieChartImage";

    public int PanelPadding = 25;
    public int PanelPaddingTop = 25; // it can differ from padding because of the title
    public int PanelShadowDistance = 3;
    public int PanelShadowAlpha = 50;

    public int ChartPaddingTop = 50;
    public int ChartPaddingBottom = 20;
    public int ChartPaddingLeft = 140;
    public int ChartPaddingRight = 30;
    public int PieHeight = 25;

    public Point LabelPosition = new Point(40, 40);

    public Brush BackgroundBrush = null;
    public Brush TitleBrush = Brushes.White;
    public Brush PanelBackgroundBrush = Brushes.White;

    //-- set display format
    public StringFormat TitleStringFormat = new StringFormat();
    public Font TitleFont = new Font("Verdana", 12, FontStyle.Bold);
    public Font LabelFont = new Font("Verdana", 8, FontStyle.Bold);

    public Color FromPanelBgColor = Color.FromArgb(156, 197, 250);
    public Color ToPanelBgColor = Color.FromArgb(10, 64, 135);
    public float GradientAngle = 0.0f;

    public bool LabelShowPercent = false;    // show percent or value as label
    public int LabelDecimalPlaces = 2;       // how many decimal places should be shown in the labels
    public string LabelExtension = "";       // if value shown, extend value by this (e.g. "kg")

    //-- set directory name for the charts
    public string ChartDirectoryName = "ChartImages";

    public class ChartElement
    {
        public string Name;
        public double Value;
        public Color Color;
    }

    private ArrayList ChartElements;

    public string Src
    {
        get { return PieChartImage.ImageUrl; }
        set
        {
            PieChartImage.ImageUrl = value;
        }
    }

    private double SumValues = 0.0;

    private void Page_Load(object sender, EventArgs e)
    {
        // deleteOldPlotPage moved to the generateChartImage,
        // because of the problems rising when the chart image directory
        // cannot be resolved
    }

    // delete images older than 10 minutes from PieChartImages folder
    private void deleteOldPlotImages()
    {      
        //DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath("/" + ChartDirectoryName));
        DirectoryInfo myDirectoryInfo = new DirectoryInfo(Server.MapPath(".") + "/" + ChartDirectoryName);
        foreach (FileInfo myFileInfo in myDirectoryInfo.GetFiles())
        {
            if (myFileInfo.LastWriteTime.AddMinutes(10) < DateTime.Now)
            {
                myFileInfo.Delete();
            }
        }
    }


    // draw panel
    private void drawPanel(Graphics g)
    {
        // draw background
        Rectangle BackgroundRectangle = new Rectangle(0, 0, ImageWidth, ImageHeight);

        if (BackgroundBrush == null)
        {
            BackgroundBrush = new System.Drawing.Drawing2D.LinearGradientBrush(BackgroundRectangle, FromPanelBgColor, ToPanelBgColor, GradientAngle, false);
        }

        g.FillRectangle(BackgroundBrush, BackgroundRectangle);

        // draw title

        g.DrawString(ChartTitle, TitleFont, TitleBrush, new RectangleF(0, 0, ImageWidth, 20), TitleStringFormat);

        // draw main panel
        Rectangle panelRectangle = new Rectangle(PanelPadding, PanelPaddingTop, ImageWidth - 2 * PanelPadding, ImageHeight - PanelPaddingTop - PanelPadding);
        Rectangle panelShadowRectangle = new Rectangle(PanelPadding + PanelShadowDistance, PanelPaddingTop + PanelShadowDistance, (ImageWidth - 2 * PanelPadding) + PanelShadowDistance, (ImageHeight - PanelPaddingTop - PanelPadding) + PanelShadowDistance);
        SolidBrush transparentSolidBrush = new SolidBrush(Color.FromArgb(PanelShadowAlpha, 0, 0, 0));

        g.FillRectangle(transparentSolidBrush, panelShadowRectangle);
        g.FillRectangle(PanelBackgroundBrush, panelRectangle);
        g.DrawRectangle(Pens.Black, panelRectangle);
    }

    // draw pie slices
    private void drawPie(Graphics g)
    {
        Rectangle ChartRectangle = new Rectangle(PanelPadding + ChartPaddingLeft, PanelPaddingTop + ChartPaddingTop, ImageWidth - 1 - 2 * PanelPadding - ChartPaddingRight - ChartPaddingLeft, ImageHeight - 1 - PanelPaddingTop - PanelPadding - ChartPaddingBottom - ChartPaddingTop);
        for (int hPos = 1; hPos <= PieHeight; ++hPos)
        {
            float startAngle = 180.0f;
            float sweepAngle = 0.0f;

            foreach (ChartElement myChartElement in ChartElements)
            {
                Brush myBrush;

                startAngle += sweepAngle;
                //sweepAngle = (float)(myChartElement.Percent * 3.6);
                if (!SumValues.Equals(0.0)) {
                    sweepAngle = (float)(myChartElement.Value * 360.0 / SumValues);
                } else {
                    sweepAngle = 0.0f;
                }

                if (hPos == PieHeight)
                {
                    // normal color
                    myBrush = new SolidBrush(myChartElement.Color);
                }
                else
                {
                    // little bit darker color
                    int R = (myChartElement.Color.R - 50 > 0) ? myChartElement.Color.R - 30 : 0;
                    int G = (myChartElement.Color.G - 50 > 0) ? myChartElement.Color.G - 30 : 0;
                    int B = (myChartElement.Color.B - 50 > 0) ? myChartElement.Color.B - 30 : 0;
                    Color darkerColor = Color.FromArgb(R, G, B);
                    myBrush = new SolidBrush(darkerColor);
                }
                g.FillPie(myBrush, ChartRectangle.X, ChartRectangle.Y - hPos, ChartRectangle.Width, ChartRectangle.Height, startAngle, sweepAngle);

                // draw border for first and last pie
                if (hPos == PieHeight || hPos == 1)
                {
                    g.DrawPie(Pens.Black, ChartRectangle.X, ChartRectangle.Y - hPos, ChartRectangle.Width, ChartRectangle.Height, startAngle, sweepAngle);
                }
            }
        }
    }

    private void drawLabels(Graphics g)
    {
        float x = (float)LabelPosition.X;
        float y = (float)LabelPosition.Y;

        foreach (ChartElement myChartElement in ChartElements)
        {
            Brush labelBrush = new SolidBrush(myChartElement.Color);
            g.FillRectangle(labelBrush, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
            g.DrawRectangle(Pens.Black, x, (float)(y + LabelFont.Height / 2 - 1), 5.0f, 5.0f);
            string labelText;

            if (LabelShowPercent)
            {
                labelText = myChartElement.Name + " " + (myChartElement.Value / SumValues).ToString("P" + LabelDecimalPlaces.ToString());
            }
            else
            {
                labelText = myChartElement.Name + " " + myChartElement.Value.ToString(String.Format("N{0}",LabelDecimalPlaces.ToString())) + " " + LabelExtension;
            }
            g.DrawString(labelText, LabelFont, Brushes.Black, x + 8, y);
            //y += LabelFont.Height + 5;
            y += 1.3f * LabelFont.Height;
        }
    }

    public void addChartElement(string name, double value, Color color)
    {
        if (this.ChartElements == null) this.ChartElements = new ArrayList();
        ChartElement newChartElement = new ChartElement();
        newChartElement.Name = name;
        newChartElement.Value = value;
        newChartElement.Color = color;
        this.ChartElements.Add(newChartElement);
        if (newChartElement.Value > 0.0)
            SumValues += newChartElement.Value;
    }

    public void addChartElement(ChartElement newChartElement)
    {
        if (this.ChartElements == null) this.ChartElements = new ArrayList();
        this.ChartElements.Add(newChartElement);
        if (newChartElement.Value > 0.0)
            SumValues += newChartElement.Value;
    }

    public void generateChartImage()
    {
        generateChartImage(false);
    }

    public void generateChartImage(bool cleanUpChartDirectory)
    {
        if (cleanUpChartDirectory)
        {
            try
            {
                // possible Exception types
                // System.IO.IOException
                // System.Security.SecurityException
                // System.UnauthorizedAccessException
                deleteOldPlotImages();  // clean up in the directory
            }
            catch (Exception ex)
            {
                //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Delete", ex);
                Exception e = new Exception("Path: " + ChartDirectoryName + "<br />Operation: File Delete", ex);
                throw e;    // throw it with the additional message info
            }
        }

        Bitmap myBitmap = null;
        Graphics myGraphics = null;

        try
        {
            myBitmap = new Bitmap(ImageWidth, ImageHeight, PixelFormat.Format32bppArgb);

            long ticks = (long)(DateTime.Now.Ticks);
            string filename = "/" + ChartDirectoryName + "/" + ID + "_" + ticks.ToString() + ".png";

            PieChartImage.Width = ImageWidth;
            PieChartImage.Height = ImageHeight;
            PieChartImage.ImageUrl = filename;
            PieChartImage.AlternateText = ImageAlt;

            //FileInfo myFileInfo = new FileInfo(Server.MapPath(filename));
            FileInfo myFileInfo = new FileInfo(filename);
            if (myFileInfo.Exists) return;

            myGraphics = Graphics.FromImage(myBitmap);

            drawPanel(myGraphics);
            if (ChartElements != null)
            {
                drawPie(myGraphics);
                drawLabels(myGraphics);
            }

            /*
            MemoryStream memoryStream = new MemoryStream();
            myBitmap.Save(memoryStream, ImageFormat.Png);

            Response.Expires = 0;
            Response.ClearContent();
            Response.ContentType = "image/png";
            Response.BinaryWrite(memoryStream.ToArray());
            Response.End();
            */
            //myBitmap.Save(Server.MapPath(filename), ImageFormat.Png);
            myBitmap.Save(Server.MapPath(".") + filename, ImageFormat.Png);
        }
        catch (Exception ex)
        {
            // error handling
            //Response.Write(e.ToString());
            //Exception e = new Exception("<br />Path: " + Server.MapPath("/" + ChartDirectoryName) + "<br />Operation: File Save", ex);
            Exception e = new Exception("Path: " + Server.MapPath(".") + ChartDirectoryName + "<br />Operation: File Save", ex);
            throw e;    // throw it with the additional message info
        }
        finally
        {
            if (myGraphics != null) myGraphics.Dispose();
            if (myBitmap != null) myBitmap.Dispose();
        }
    }
}
