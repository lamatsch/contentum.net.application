﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="PldIratIktatasPanelUserControl.ascx.cs"
    Inherits="eRecordComponent_PldIratIktatasPanelUserControl" %>

<%@ Register Src="~/Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc2" %>
<%@ Register Src="~/Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="~/eRecordComponent/VonalKodTextBox.ascx" TagPrefix="cc" TagName="VonalKodTextBox" %>
<%@ Register Src="~/eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>
<%@ Register Src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="EPARTTBWF" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>


<eUI:eFormPanel ID="PldIratIktatasPanelUserControlA" runat="server">

    <table style="border-collapse: separate; border-spacing: 1px; width: 100%;">
        <tbody>
            <tr>
                <td>
                    <h3>
                        <asp:Label ID="Label_IratPeldanySorSzam" runat="server" Text="X. Példány"></asp:Label>
                    </h3>
                </td>
                <td></td>
                <td></td>
                <td>
                    <asp:ImageButton runat="server" ID="ImageButtonTobbszorosIratPeldanyBezaras" ImageUrl="~/images/hu/ovalgomb/bezar.jpg" OnClick="ImageButtonTobbszorosIratPeldanyBezaras_Click" CausesValidation="false" />
                </td>
            </tr>
            <tr>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label1" runat="server" Text="Irattári példány:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <asp:CheckBox runat="server" ID="CheckBoxIrattariPeldany" Text="Irattári példány" AutoPostBack="true" OnCheckedChanged="CheckBoxIrattariPeldany_CheckedChanged" />
                </td>
                <td class="mrUrlapCaption_short"></td>
                <td class="mrUrlapMezo"></td>
            </tr>

            <tr class="urlapSor_kicsi" id="IratPeldanyPanel_CimzettTR" runat="server">
                <td class="mrUrlapCaption_short">
                    <div class="DisableWrap">
                        <asp:Label ID="Label53" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="Label17" runat="server" Text="Címzett neve:"></asp:Label>
                    </div>
                </td>
                <td class="mrUrlapMezo">
					<%-- BUG_13212--%>
                    <%--<uc2:PartnerTextBox ID="PldS_PartnerId_Cimzett_PartnerTextBox" runat="server" WithAllCimCheckBoxVisible="false"></uc2:PartnerTextBox>--%>
                     <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="PldS_PartnerId_Cimzett_PartnerTextBox" runat="server" />
               </td>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Pld_KuldesMod_ReqLabel" runat="server" Text="*" CssClass="ReqStar" Visible="true"></asp:Label>
                    <asp:Label ID="Label5" runat="server" Text="Expediálás módja:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <ktddl:KodtarakDropDownList ID="Pld_KuldesMod_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>
                </td>
            </tr>
            <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                <td class="mrUrlapCaption_short"></td>
                <td class="mrUrlapMezo"></td>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="label36" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                    <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                </td>
            </tr>
            <tr class="urlapSor_kicsi" id="IratPeldanyPanel_CimzettCimeTR" runat="server">
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label_Pld_CimzettCime" runat="server" Text="Címzett címe:"></asp:Label>
                    <asp:Label ID="Label_Email_CimzettEmailCime" runat="server" Text="Címzett e-mail címe:" Visible="false"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <uc3:CimekTextBox ID="PldS_CimId_Cimzett_CimekTextBox" runat="server" Validate="false"></uc3:CimekTextBox>
                </td>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label16" runat="server" Text="Elvárás:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <ktddl:KodtarakDropDownList ID="Pld_Visszavarolag_KodtarakDropDownList" runat="server"></ktddl:KodtarakDropDownList>
                </td>
            </tr>
            <tr class="urlapSor_kicsi" id="IratPeldanyPanel_HataridoTR" runat="server">
                <td class="mrUrlapCaption_short">&nbsp; 
                    <asp:Label ID="Label23" runat="server" Text="Elvárt határidő:" />
                </td>
                <td class="mrUrlapMezo">
                    <cc:CalendarControl ID="Pld_VisszaerkezesiHatarido_CalendarControl" runat="server"
                        Validate="false" PopupPosition="TopLeft"></cc:CalendarControl>
                </td>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label10" runat="server" CssClass="ReqStar" Text="*" Style="margin-left: -5px;"></asp:Label>
                    <asp:Label ID="Label_IratPeldanyJellege" runat="server" Text="Iratpéldány jellege:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <ktddl:KodtarakDropDownList ID="Pld_KodtarakDropDownListIratPeldanyJellege" runat="server"></ktddl:KodtarakDropDownList>
                </td>
            </tr>

            <%--  <tr class="urlapSor_kicsi" runat="server" id="trIratPeldanyPanelVonalkod">
                <td class="mrUrlapCaption_short">
                       <asp:UpdatePanel ID="starUpdate" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="Label_Pld_VonalkodElottCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                            <asp:Label ID="Label_Pld_Vonalkod" runat="server" Text="Vonalkód:"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Label ID="Label_Email_CimzettSzervezete" runat="server" Text="Címzett szervezete:"
                        Visible="false"></asp:Label>
                </td>
                <td class="mrUrlapMezo" style="width: 0%;">
                        <cc:VonalKodTextBox runat="server" ID="Pld_BarCode_VonalKodTextBox" Validate="true" />
                    <uc2:PartnerTextBox ID="Email_CimzettSzervezete_PartnerTextBox" runat="server" ReadOnly="true"
                        ViewMode="true" Visible="false"></uc2:PartnerTextBox>
                </td>
                <td></td>
                <td></td>
            </tr>--%>
        </tbody>
    </table>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
</eUI:eFormPanel>      

