﻿using Contentum.eAdmin.BaseUtility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Web.UI;

public partial class eRecordComponent_PldIratIktatasPanelUserControl : System.Web.UI.UserControl
{
    #region KCS
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_IRAT_FAJTA = "IRAT_FAJTA";
    #endregion

    private bool isTUKRendszer = false;

    public delegate void Close(string sorszam, eRecordComponent_PldIratIktatasPanelUserControl self);
    public event Close OnClose;

    #region PROPERTIES
    public enum IrattariPeldanyCime
    {
        [Description("TÜK Irattár")]
        TUK_Irattar,

        [Description("Központi Irattár")]
        Kozponti_irattar,
    }

    private string sorszam;
    [System.ComponentModel.Bindable(true)]
    public string SorSzam
    {
        get { return sorszam; }
        set { sorszam = value; }
    }

    // BUG_11079
    public string Pld_KuldesModDropDown_UniqueID
    {
        get { return Pld_KuldesMod_KodtarakDropDownList.DropDownList.UniqueID; }
    }

    public string Command;
    public string Mode; 
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        SetMode();
        if (!string.IsNullOrEmpty(SorSzam))
            InitSorszam(SorSzam);
    }
    public void InitSorszam(string sorszam)
    {
        if (string.IsNullOrEmpty(sorszam))
            throw new Exception("Sorszám megadása kötelező !");

        SorSzam = sorszam;
        Label_IratPeldanySorSzam.Text = string.Format("{0}. Példány", sorszam);
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        if (!Page.IsPostBack)
            InitControls();

        PldS_PartnerId_Cimzett_PartnerTextBox.CimTextBox = PldS_CimId_Cimzett_CimekTextBox.TextBox;
        PldS_PartnerId_Cimzett_PartnerTextBox.CimHiddenField = PldS_CimId_Cimzett_CimekTextBox.HiddenField;

        PldS_CimId_Cimzett_CimekTextBox.SetCimTipusFilter(Pld_KuldesMod_KodtarakDropDownList.DropDownList, PldS_PartnerId_Cimzett_PartnerTextBox.BehaviorID);

        if (isTUKRendszer)
        {

            tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;            
            IrattariHelyLevelekDropDownTUK.Validate = true;
        }
    }

    private void InitControls()
    {
        Mode = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.Mode);
        Command = Request.QueryString.Get(QueryStringVars.Command);

        PldS_PartnerId_Cimzett_PartnerTextBox.WithElosztoivek = false;
        Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, false, EErrorPanel1);
        Pld_Visszavarolag_KodtarakDropDownList.FillDropDownList(kcs_VISSZAVAROLAG, true, EErrorPanel1);
        Pld_KodtarakDropDownListIratPeldanyJellege.FillDropDownList(kcs_IRAT_FAJTA, false, EErrorPanel1); // kötelező

        if (isTUKRendszer && !IsPostBack)
        {
            ReloadIrattariHelyLevelekDropDownTUK(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null);
        }
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string id, string selectedValue)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                Contentum.eQuery.BusinessDocuments.KRT_CsoportTagokSearch search_csoportok = new Contentum.eQuery.BusinessDocuments.KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Filter(id);

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (result_csoportok.IsError || result_csoportok.GetCount < 1)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }

                Contentum.eQuery.BusinessDocuments.EREC_IrattariHelyekSearch search = new Contentum.eQuery.BusinessDocuments.EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.GetCount > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    if (res.GetCount == 1)
                        IrattariHelyLevelekDropDownTUK.DropDownList.SelectedIndex = 1;
                    else if (!string.IsNullOrEmpty(selectedValue))
                    {
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                    }

                }
            }
        }
    }

    private void SetMode()
    {
        switch (Command)
        {
            case CommandName.New:
            case CommandName.Modify:
                SetReadOnly(false);
                break;
            case CommandName.View:
            case CommandName.DesignView:
                SetReadOnly(true);
                break;
            default:
                break;
        }
    }

    public bool IsValid()
    {
        if (string.IsNullOrEmpty(PldS_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField)
         && string.IsNullOrEmpty(PldS_PartnerId_Cimzett_PartnerTextBox.Text))
            return false;

        if (isTUKRendszer && (String.IsNullOrEmpty(IrattariHelyLevelekDropDownTUK.SelectedValue)))
            return false;

        if (String.IsNullOrEmpty(Pld_KodtarakDropDownListIratPeldanyJellege.SelectedValue))
        {
            return false;
        }

        //if (string.IsNullOrEmpty(PldS_CimId_Cimzett_CimekTextBox.Id_HiddenField)
        // && string.IsNullOrEmpty(PldS_CimId_Cimzett_CimekTextBox.Text))
        //    return false;
        return true;
    }
    public EREC_PldIratPeldanyok GetBO()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        erec_PldIratPeldanyok.Partner_Id_Cimzett = PldS_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
        erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

		// BUG_13213
        erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt = PldS_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue;
        erec_PldIratPeldanyok.Updated.Partner_Id_CimzettKapcsolt = true;

        erec_PldIratPeldanyok.NevSTR_Cimzett = PldS_PartnerId_Cimzett_PartnerTextBox.Text;
        erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;


        erec_PldIratPeldanyok.KuldesMod = Pld_KuldesMod_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.KuldesMod = true;


        erec_PldIratPeldanyok.Cim_id_Cimzett = PldS_CimId_Cimzett_CimekTextBox.Id_HiddenField;
        erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

        erec_PldIratPeldanyok.CimSTR_Cimzett = PldS_CimId_Cimzett_CimekTextBox.Text;
        erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;


        erec_PldIratPeldanyok.Visszavarolag = Pld_Visszavarolag_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.Visszavarolag = true;

        erec_PldIratPeldanyok.VisszaerkezesiHatarido = Pld_VisszaerkezesiHatarido_CalendarControl.Text;
        erec_PldIratPeldanyok.Updated.VisszaerkezesiHatarido = true;

        erec_PldIratPeldanyok.Eredet = Pld_KodtarakDropDownListIratPeldanyJellege.SelectedValue;
        erec_PldIratPeldanyok.Updated.Eredet = true;

        //erec_PldIratPeldanyok.BarCode = Pld_BarCode_VonalKodTextBox.Text;
        //erec_PldIratPeldanyok.Updated.BarCode = true;

        if (isTUKRendszer)
        {
            erec_PldIratPeldanyok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_PldIratPeldanyok.Updated.IrattarId = true;
            erec_PldIratPeldanyok.IrattariHely = IrattariHelyLevelekDropDownTUK.SelectedText;
            erec_PldIratPeldanyok.Updated.IrattariHely = true;
        }

        return erec_PldIratPeldanyok;
    }

    protected void CheckBoxIrattariPeldany_CheckedChanged(object sender, EventArgs e)
    {
        IrattariPeldanyBeallitas();
    }
    private void IrattariPeldanyBeallitas()
    {
        if (CheckBoxIrattariPeldany.Checked)
        {
            // BUG_11907
            string peldanyCimzettje = Rendszerparameterek.Get(Page, Rendszerparameterek.UGYIRATBAN_MARADO_PELDANY_CIMZETTJE);

            bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
            if (isTUK)
            {
                // BUG_11907
                //PldS_PartnerId_Cimzett_PartnerTextBox.Text = GetEnumDescription(IrattariPeldanyCime.TUK_Irattar);
                PldS_PartnerId_Cimzett_PartnerTextBox.Text = peldanyCimzettje;
                // PldS_PartnerId_Cimzett_PartnerTextBox.CimTextBox.Text = GetEnumDescription(IrattariPeldanyCime.TUK_Irattar);

                // BUG_7085
                Pld_KuldesMod_KodtarakDropDownList.SetSelectedValue(KodTarak.KULDEMENY_KULDES_MODJA.TUK_Helyben_marado); // helyben maradó (BUG 7248)
            }
            else
            {
                // BUG_11907
                //PldS_PartnerId_Cimzett_PartnerTextBox.Text = GetEnumDescription(IrattariPeldanyCime.Kozponti_irattar);
                PldS_PartnerId_Cimzett_PartnerTextBox.Text = peldanyCimzettje;

                //PldS_PartnerId_Cimzett_PartnerTextBox.CimTextBox.Text = GetEnumDescription(IrattariPeldanyCime.Kozponti_irattar);
            }
        }
        else
        {
            PldS_PartnerId_Cimzett_PartnerTextBox.Text = string.Empty;
            // PldS_PartnerId_Cimzett_PartnerTextBox.CimTextBox.Text = string.Empty;
        }
    }

    private string GetEnumDescription(Enum FruitType)
    {
        FieldInfo fi = FruitType.GetType().GetField(FruitType.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attributes.Length > 0)
        {
            return attributes[0].Description;
        }
        else
        {
            return FruitType.ToString();
        }
    }

    private void SetReadOnly(bool readOnly)
    {
        if (readOnly)
        {
            PldIratIktatasPanelUserControlA.Enabled = false;

            CheckBoxIrattariPeldany.Enabled = false;
            PldS_PartnerId_Cimzett_PartnerTextBox.Enabled = false;
            Pld_KuldesMod_KodtarakDropDownList.Enabled = false;
            PldS_CimId_Cimzett_CimekTextBox.Enabled = false;
            Pld_Visszavarolag_KodtarakDropDownList.Enabled = false;
            //Pld_BarCode_VonalKodTextBox.Enabled = false;
            //Email_CimzettSzervezete_PartnerTextBox.Enabled = false;
            Pld_VisszaerkezesiHatarido_CalendarControl.Enabled = false;
            Pld_KodtarakDropDownListIratPeldanyJellege.Enabled = false;
            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Enabled = false;
            }
        }
        else
        {
            PldIratIktatasPanelUserControlA.Enabled = true;

            CheckBoxIrattariPeldany.Enabled = true;
            PldS_PartnerId_Cimzett_PartnerTextBox.Enabled = true;
            Pld_KuldesMod_KodtarakDropDownList.Enabled = true;
            PldS_CimId_Cimzett_CimekTextBox.Enabled = true;
            Pld_Visszavarolag_KodtarakDropDownList.Enabled = true;
            //Pld_BarCode_VonalKodTextBox.Enabled = true;
            //Email_CimzettSzervezete_PartnerTextBox.Enabled = true;
            Pld_VisszaerkezesiHatarido_CalendarControl.Enabled = true;
            Pld_KodtarakDropDownListIratPeldanyJellege.Enabled = true;
            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Enabled = true;
            }
        }
    }
    public void Reset()
    {
        CheckBoxIrattariPeldany.Checked = false;
        PldS_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = string.Empty;
        PldS_PartnerId_Cimzett_PartnerTextBox.Text = string.Empty;

        Pld_KuldesMod_KodtarakDropDownList.DropDownList.SelectedValue = null;

        PldS_CimId_Cimzett_CimekTextBox.Id_HiddenField = string.Empty;
        PldS_CimId_Cimzett_CimekTextBox.Text = string.Empty;

        Pld_Visszavarolag_KodtarakDropDownList.SelectedValue = null;

        //Pld_VisszaerkezesiHatarido_CalendarControl.SelectedDate;
        Pld_KodtarakDropDownListIratPeldanyJellege.DropDownList.SelectedIndex = 0;
    }

    protected void ImageButtonTobbszorosIratPeldanyBezaras_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (OnClose != null)
            OnClose(SorSzam, this);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string js = @"(function () {
                try {

                    $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Ugyfelelos_CsoportTextBox_HiddenField1').on('change', function () {

                        loadFizikaihelyek();
                    });

                    $('#" + IrattariHelyLevelekDropDownTUK.DropDownList.ClientID + @"').on('change', function () {
                        var dropdown = $('#" + IrattariHelyLevelekDropDownTUK.DropDownList.ClientID + @"');

                        var dVal = dropdown.val();
                        var dText = dropdown.find('option:selected').text();
                        if (dVal != undefined && dText != undefined) {
                            $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID+ @"').val(dVal);
                            $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID+ @"').val(dText);
                        }
                    });

                    loadFizikaihelyek();

                    $('#" + Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID+ @"').on('change', function () {
                        loadFizikaihelyek();
                    });

                } catch (e) {
                    //console.log(e);
                }

            function loadFizikaihelyek() {
                var inPartnerId = $('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_Ugyfelelos_CsoportTextBox_HiddenField1').val();
                if (inPartnerId === null || inPartnerId === '' || inPartnerId === undefined || inPartnerId === 'undefined') {
                    inPartnerId = '1';
                }
                try {
                    var fel = $('[id$= HiddenField_Svc_FelhasznaloId]').val();
                    var szerv = $('[id$=HiddenField_Svc_FelhasznaloSzervezetId]').val();

                if (fel != undefined || szerv != undefined)
                {
                                $.ajax({
                    url: '" +ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFizikaiHelyek") + @"',
                                    data: JSON.stringify({
                        partnerId: inPartnerId,
                                        contextKey: $('[id$=HiddenField_Svc_FelhasznaloId]').val() + ';' + $('[id$=HiddenField_Svc_FelhasznaloSzervezetId]').val()
                                    }),
                                    dataType: 'json',
                                    type: 'POST',
                                    contentType: 'application/json; charset=utf-8',
                                    success: function(data) {
                            fillFizikaihelyek(data);
                        },
                                    error: function(response) {
                            alert(response.responseText);
                        },
                                    failure: function(response) {
                            alert(response.responseText);
                        }
                    });
                }
            } catch (e) {
                            //console.log(e);
                        }
                        //}
                        //else {
                        //    //console.log('partner id nem stimmel');
                        //}
                    }

                    function fillFizikaihelyek(data)
        {
            var dropdown = $('#" + IrattariHelyLevelekDropDownTUK.DropDownList.ClientID + @"');
            var expDropdownVal = $('#" + Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID+ @"').val(); ;

            if (expDropdownVal != '110')
            {
                if (dropdown != null)
                {
                    dropdown.empty();
                    dropdown.html('<option value=""4f18bb74-aec8-e911-80df-00155d017b07"">' + 'Címzettnél' + '</option>');
                                $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID+ @"').val(dropdown.val());
                                $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID+ @"').val(dropdown.text());
                    return;

                }
            }

            if (dropdown != null)
            {
                var dVal = $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID+ @"').val();
                var dText = $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID+ @"').val();
                dropdown.empty();
                dropdown.html('<option value="""">' + '[Nincs megadva elem]' + '</option>');

                if (data === null || data.d === null)
                {
                    return;
                }
                if (dropdown !== null)
                {
                    var k = JSON.parse(data.d);

                    var s = '';
                    for (var i = 0; i < k.length; i++)
                    {
                        s += '<option value=""' + k[i].Id + '"">' + k[i].Value + '</option>';
                    }
                    dropdown.html(s);

                    if (dVal != undefined && dText != undefined)
                    {
                        dropdown.val(dVal);
                                    $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID+ @"').val(dVal);
                                    $('#" + IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID+ @"').val(dText);
                    }

                }
            }

        }
        }) ();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), js, js, true);
    }
}