<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PldIratPeldanyFormTab.ascx.cs"
    Inherits="eRecordComponent_PldIratPeldanyFormTab" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc13" %>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc9" %>
<%@ Register Src="../Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="../Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList"
    TagPrefix="uc6" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc7" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="FunkcioGombsor.ascx" TagName="FunkcioGombsor" TagPrefix="fgs" %>
<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc10" %>
<%@ Register Src="~/Component/FelelosSzervezetVezetoTextBox.ascx" TagPrefix="fszvez" TagName="FelelosSzervezetVezetoTextBox" %>
<%@ Register Src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="EPARTTBWF" %>
<%@ Register Src="IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="PldIratPeldanyUpdatePanel" runat="server" OnLoad="PldIratPeldanyUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <asp:HiddenField ID="UgyiratHiddenField" runat="server" />
            <asp:HiddenField ID="IratPeldanyHiddenField" runat="server" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <asp:Panel ID="EFormPanel1" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor">
                                    <td colspan="2">

                                        <eUI:eFormPanel ID="IratPanel" runat="server" Visible="true">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor_kicsi" id="trIratPanelTargy" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label41" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="Label50" runat="server" Text="Irat t�rgya:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc8:RequiredTextBox ID="IraIrat_Targy_RequiredTextBox" runat="server" ReadOnly="true"
                                                            Rows="2" TextBoxMode="MultiLine" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short" width="0%">
                                                        <asp:Label ID="Label38" runat="server" Text="Irat kateg�ria:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="IraIrat_Kategoria_KodtarakDropDownList" runat="server"
                                                            ReadOnly="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPanelUgyintezo" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label34" runat="server" Text="�gyirat �gyint�z�:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc13:FelhasznaloCsoportTextBox ID="Ugyirat_FelCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                                            runat="server" ReadOnly="true" ViewMode="true" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <%--BLG_619--%>
                                                        <asp:Label ID="labelFelelosSzervezet" runat="server" Text="<%$Forditas:labelFelelosSzervezet|Felel�s szervezet:%>"></asp:Label>
                                                        <div style="margin-top: 10px;" />
                                                        <asp:Label runat="server" ID="LabelFelelosSzervezeteVezeto" Text="Felel�s szervezet vezet�je" />
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <%--BLG_619--%>
                                                        <uc5:CsoportTextBox ID="Iratfelelos_CsoportTextBox" SzervezetCsoport="true" Validate="false" ReadOnly="true" runat="server" />
                                                        <fszvez:FelelosSzervezetVezetoTextBox runat="server" ID="FelelosSzervezetVezetoTextBoxInstance" />
                                                    </td>
                                                </tr>
                                                <%--BLG#1402 START--%>
                                                <tr id="trTUKMinositesEsKezelesiUtasitas" class="urlapSor" runat="server" visible="false">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelIratMinosites" runat="server" Text="<%$Forditas:labelIratMinosites|Irat min�s�t�se:%>"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <ktddl:KodtarakDropDownList ID="ktDropDownListIratMinosites" runat="server" ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelKezelesiUtasitas" runat="server" Text="Kezel�si utas�t�s:" />
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <asp:TextBox ID="txtIratKezelesiUtasitas" runat="server" ReadOnly="true" CssClass="mrUrlapInput ReadOnlyWebControl" />
                                                    </td>
                                                </tr>
                                                <%--BLG#1402 STOP--%>
                                            </table>
                                        </eUI:eFormPanel>
                                        <eUI:eFormPanel ID="IratPeldanyPanel" runat="server" Visible="true">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelCimzett" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <div class="DisableWrap">
                                                            <asp:Label ID="Label53" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="Label17" runat="server" Text="Iratp�ld�ny c�mzettj�nek neve:"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" />
                                                        <%--<uc2:PartnerTextBox ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" />--%>
                                                        <uc5:CsoportTextBox ID="Pld_Cimzett_CsoportTextBox" runat="server" Visible="false" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short" width="0%">
                                                        <asp:Label ID="Label7" runat="server" Text="V�lasz:" />
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <asp:RadioButton ID="elektronikus" Text="Elektronikus" GroupName="valasz" runat="server"
                                                            AutoPostBack="True" OnCheckedChanged="Valasz_CheckedChanged" />
                                                        <br />
                                                        <asp:RadioButton ID="papir_alapu" Text="Pap�r alap�" GroupName="valasz" runat="server"
                                                            AutoPostBack="True" OnCheckedChanged="Valasz_CheckedChanged" />
                                                        <br />
                                                        <asp:RadioButton ID="egyeb" Text="Egy�b" GroupName="valasz" runat="server" AutoPostBack="True"
                                                            OnCheckedChanged="Valasz_CheckedChanged" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelCimzettCim" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label20" runat="server" Text="Iratp�ld�ny c�mzettj�nek c�me:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc3:CimekTextBox ID="Pld_CimId_Cimzett_CimekTextBox" runat="server" Validate="false" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label11" runat="server" CssClass="ReqStar" Text="*" Style="margin-left: -5px;"></asp:Label>
                                                        <asp:Label ID="Label5" runat="server" Text="Expedi�l�s m�dja:"></asp:Label>&nbsp;
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="Pld_KuldesMod_KodtarakDropDownList" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelAllapot" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label1" runat="server" Text="�llapot:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="Pld_Allapot_KodtarakDropDownList" runat="server"
                                                            ReadOnly="true" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short" width="0%" style="padding-left: 2px">
                                                        <%--<asp:Label ID="Label18" runat="server" Text="Tov�bb�t�:"></asp:Label>--%>
                                                        <asp:Label ID="Label16" runat="server" Text="Elv�r�s:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <%--<ktddl:KodtarakDropDownList ID="Pld_Tovabbito_KodtarakDropDownList" runat="server" />--%>
                                                        <ktddl:KodtarakDropDownList ID="Pld_Visszavarolag_KodtarakDropDownList" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelAdathordozo" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelIratpeldanyTipusa" runat="server" Text="<%$Forditas:labelIratpeldanyTipusa|Iratp�ld�ny t�pusa:%>"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="Pld_UgyintezesModja_KodtarakDropDownList" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short DisableWrap" width="0%">
                                                        <asp:Label ID="Label10" runat="server" CssClass="ReqStar" Text="*" Style="margin-left: -5px;"></asp:Label>
                                                        <asp:Label ID="Label39" runat="server" Text="Iratp�ld�ny jellege:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <ktddl:KodtarakDropDownList ID="Pld_Eredet_KodtarakDropDownList" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelKezelo" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label8" runat="server" Text="P�ld�ny kezel�je:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc5:CsoportTextBox ID="Pld_CsopId_Felelos_CsoportTextBox" runat="server" ReadOnly="true"
                                                            Validate="false" ViewMode="true" />
                                                    </td>                                                                                                 
                                                     <td  runat='server' id="peldanyHelyeLabel" class="mrUrlapCaption_short" width="0%">
                                                        <asp:Label ID="Label9" runat="server" Text="P�ld�ny helye:"></asp:Label>
                                                    </td>                                                  
                                                    <td runat="server" id="peldanyHelyeTextBox"  class="mrUrlapMezo" width="0%">
                                                        <uc13:FelhasznaloCsoportTextBox ID="Pld_FelhCsopId_Orzo_FelhasznaloCsoportTextBox"
                                                            runat="server" ReadOnly="true" ViewMode="true" Validate="false" />
                                                    </td>
                                                </tr>
                                                <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Label12" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                        <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short"></td>
                                                    <td class="mrUrlapMezo"></td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" id="trIratPeldanyPanelVonalkod" runat="server">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:UpdatePanel ID="starUpdate" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="labelVonalkodStar" runat="server" CssClass="ReqStar" Text="*" Style="margin-left: -5px;" />
                                                                <asp:Label ID="labelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">
                                                        <uc10:VonalKodTextBox ID="Barcode_VonalKodTextBox" runat="server" RequiredValidate="false" LabelRequiredIndicatorID="labelVonalkodStar" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short" width="0%">&nbsp;
                                                    </td>
                                                    <td class="mrUrlapMezo" width="0%">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Image ID="SpacerImage1" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif"
                                                            Visible="true" />
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr class="urlapSor_kicsi" id="trIratPeldanyPanelKeltezes" runat="server">
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="Label46" runat="server" Text="Iratp�ld�ny l�trehoz�s�nak d�tuma:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_LetrehozasIdo_CalendarControl" runat="server" ReadOnly="true"
                                                                        Validate="false" TimeVisible="true" />
                                                                </td>
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="Label23" runat="server" Text="Iratp�ld�ny �gyint�z�si hat�rid�:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_VisszaerkezesiHatarido_CalendarControl" runat="server"
                                                                        Validate="false" />
                                                                </td>
                                                                <td class="mrUrlapCaption_short" width="0%">
                                                                    <asp:Label ID="Label3" runat="server" Text="T�ny:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_VisszaerkezesDatuma_CalendarControl" runat="server" Validate="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="mrUrlapCaption_short" width="0%">
                                                                    <asp:Label ID="lblPldIratMegsemmisitve" runat="server" Text="Iratp�ld�ny megsemmis�tve:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <asp:CheckBox ID="cbPldIratMegssemisitve" runat="server" Enabled="false" />
                                                                </td>
                                                                <td class="mrUrlapCaption_short" width="0%">
                                                                    <asp:Label ID="lblPldIratMegsemmisitesDatuma" runat="server" Text="Iratp�ld�ny megsemmis�t�s d�tuma:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="ccPldIratMegsemmisitesDatuma" runat="server" ReadOnly="false" Validate="false" />
                                                                </td>
                                                            </tr>
                                                            <tr class="urlapSor_kicsi" id="trIratPeldanyPanelAtvetel" runat="server">
                                                                <td class="mrUrlapCaption_nowidth">
                                                                    <asp:Label ID="Label2" runat="server" Text="�tv�tel ideje:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_AtvetelDatuma_CalendarControl" runat="server" TimeVisible="true" Validate="false" />
                                                                </td>
                                                                <td class="mrUrlapCaption_short" width="0%">
                                                                    <asp:Label ID="Label4" runat="server" Text="Sztorn�:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_SztornirozasDat_CalendarControl" runat="server" ReadOnly="true"
                                                                        Validate="false" />
                                                                </td>
                                                                <td class="mrUrlapCaption_short" width="0%">
                                                                    <asp:Label ID="Label6" runat="server" Text="Post�ra ad�s d�tuma:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="Pld_PostazasDatuma_CalendarControl" runat="server" ReadOnly="true"
                                                                        Validate="false" TimeVisible="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Image ID="SpacerImage2" runat="server" ImageUrl="~/images/hu/design/spacertrans.gif"
                                                            Visible="true" />
                                                    </td>
                                                </tr>
                                                <tr class="urlapSor_kicsi" runat="server" id="trExpedialas" visible="false">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelExpedialo" runat="server" Text="Expedi�l�:">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc13:FelhasznaloCsoportTextBox ID="fcsExpedialo" runat="server" ReadOnly="true" ViewMode="true" Validate="false" />
                                                    </td>
                                                    <td class="" colspan="2">
                                                        <div style="float: left; width: 130px; font-weight: bold; text-align: right; margin-top: 4px;">
                                                            <asp:Label ID="labeExpedialasDatuma" runat="server" Text="Expedi�l�s id�pontja:">
                                                            </asp:Label>
                                                        </div>
                                                        <div style="margin-left: 135px; text-align: left;">
                                                            <cc:CalendarControl ID="ccExpedialasDatuma" runat="server" ReadOnly="true"
                                                                Validate="false" TimeVisible="true" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%--<table cellspacing="0" cellpadding="0" width="100%">
                                    
                                    <tr class="urlapSor_kicsi">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label6" runat="server" Text="Megkeres�s:" Enabled="False"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%">
                                            <uc1:ReadOnlyTextBox ID="ReadOnlyTextBox1" runat="server" Enabled="false" />
                                        </td>
                                        <td class="mrUrlapCaption_short" width="0%">
                                            <asp:Label ID="Label7" runat="server" Text="V�lasz:" Enabled="False"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" width="0%">
                                            <uc1:ReadOnlyTextBox ID="ReadOnlyTextBox2" runat="server" ReadOnly="false" />
                                        </td>
                                    </tr>
					            </table>--%>
                                        </eUI:eFormPanel>

                                        <eUI:eFormPanel ID="IrattariAdatok_EFormPanel" runat="server" Visible="False">
                                            <table id="Table3" cellspacing="0" cellpadding="0" runat="server">
                                                <tbody>
                                                    <tr>
                                                        <td class="ugyiratugy">
                                                            <table id="Table4" cellspacing="0" cellpadding="0" runat="server">
                                                                <tbody>
                                                                    <tr class="urlapSor_kicsi">
                                                                        <td class="mrUrlapCaption_short">
                                                                            <asp:Label ID="labelLeveltarbaAdas" runat="server" Text="Lev�lt�rba ad�s:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo">
                                                                            <cc:CalendarControl ID="CalendarControlSelejtezes" runat="server" ReadOnly="true"
                                                                                Validate="false"></cc:CalendarControl>
                                                                        </td>
                                                                        <td class="mrUrlapCaption_short">
                                                                            <asp:Label ID="labelLeveltarbaAdo" runat="server" Text="Lev�lt�rba ad�:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo">
                                                                            <uc13:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxSelejtezoAdo" runat="server"
                                                                                ViewMode="true" ReadOnly="true" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor_kicsi">
                                                                        <td class="mrUrlapCaption_short">
                                                                            <asp:Label ID="labelLeveltariAtvevo" runat="server" Text="Lev�lt�ri �tvev�:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo">
                                                                            <asp:TextBox ID="Leveltariatvevo_TextBox" runat="server" ReadOnly="True"
                                                                                CssClass="mrUrlapInput ReadOnlyWebControl"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </eUI:eFormPanel>

                                        <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" CssClass="mrResultPanel">
                                            <div class="mrResultPanelText">
                                                Az iratp�ld�ny l�trej�tt.
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
                                                <tr class="urlapSorBigSize">
                                                    <td class="mrUrlapCaptionBigSize">
                                                        <asp:Label ID="labelIratPeldany" runat="server" Text="Iratp�ld�ny&nbsp;iktat�sz�ma:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoBigSize">
                                                        <asp:Label ID="labelIratPeldanyIktatoszam" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapCaption" style="padding-top: 5px">
                                                        <asp:ImageButton ID="imgPldMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                            onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                    </td>
                                                    <td class="mrUrlapMezo" style="padding-top: 5px">
                                                        <span style="padding-right: 100px">
                                                            <asp:ImageButton ID="imgPldModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                        </span>
                                                    </td>
                                                </tr>

                                            </table>
                                        </eUI:eFormPanel>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption_short" valign="top"></td>
                                    <td class="mrUrlapMezo">
                                        <uc7:TabFooter ID="TabFooter1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td valign="top">
                        <fgs:FunkcioGombsor ID="FunkcioGombsor" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
        <script type="text/javascript">
            function setIFFelelosSzervezetVezeto() {
                try {
                    var id = $('#ctl00_ContentPlaceHolder1_PldIratpeldanyokTabContainer_TabIratpeldanyPanel_IratpeldanyTab1_Iratfelelos_CsoportTextBox_HiddenField1').val();

                    if (id != '') {
                        if (typeof SetFelelosSzervezetVezeto !== 'undefined')
                            SetFelelosSzervezetVezeto(id, null);
                    }

                } catch (e) {
                    console.log(e);
                }
            }
            $(document).ready(function () {
                try {
                    $('#ctl00_ContentPlaceHolder1_PldIratpeldanyokTabContainer_TabIratpeldanyPanel_IratpeldanyTab1_Iratfelelos_CsoportTextBox_HiddenField1').on('change', function () {
                        if (typeof setTimeout !== 'undefined' && typeof setIFFelelosSzervezetVezeto !== 'undefined')
                            setTimeout(setIFFelelosSzervezetVezeto, 1500);
                    });

                    $('#ctl00_ContentPlaceHolder1_PldIratpeldanyokTabContainer_TabIratpeldanyPanel_IratpeldanyTab1_Iratfelelos_CsoportTextBox_HiddenField1').on('change', function () {
                        if (typeof SetFelelosSzervezetVezeto !== 'undefined')
                            SetFelelosSzervezetVezeto($(this).val(), null);
                    });
                } catch (e) {
                    console.log(e);
                }

            });
        </script>
        <asp:HiddenField ID="hfUgyfelelos" runat="server" />
        <%if (isTUKRendszer)
            { %>
        <script type="text/javascript">
            if (!$('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>').is(':disabled')) {
                Sys.Application.add_load(function () {

                    if (true) {
                        try {
                            $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>').on('change', function () {
                                var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');

                                var dVal = dropdown.val();
                                var dText = dropdown.find('option:selected').text();
                                if (dVal != undefined && dText != undefined) {
                                    $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                                    $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                                }
                            });

                            loadFizikaihelyek();

                            <%if (obj_Irat!=null && obj_Irat.PostazasIranya != "1") {  %>
                            $('#<%= Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID%>').on('change', function () {
                                loadFizikaihelyek();
                            });
                             <%} %>

                        } catch (e) {
                            //console.log(e);
                        }
                    }
                });
            }

            function loadFizikaihelyek() {
                var inPartnerId = $('#<%=hfUgyfelelos.ClientID%>').val();
                if (inPartnerId === null || inPartnerId === '' || inPartnerId === undefined || inPartnerId === 'undefined') {
                    inPartnerId = '1';
                }
                try {
                    var fel = $("[id$=HiddenField_Svc_FelhasznaloId]").val();
                    var szerv = $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val();
                    if (fel != undefined || szerv != undefined) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFizikaiHelyek") %>',
                            data: JSON.stringify({
                                partnerId: inPartnerId,
                                contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ';' + $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val()
                            }),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                fillFizikaihelyek(data);
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    }
                } catch (e) {
                    //console.log(e);
                }
            }

            function fillFizikaihelyek(data) {
                //console.log('data');
                var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');
                var expDropdownVal = $('#<%= Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID%>').val();;

                <%if (obj_Irat!=null && obj_Irat.PostazasIranya != "1") {  %>
                if (expDropdownVal != '110') {
                    if (dropdown != null) {
                        dropdown.empty();
                        dropdown.html('<option value="' + '4f18bb74-aec8-e911-80df-00155d017b07' + '">' + 'C�mzettn�l' + '</option>');
                        $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dropdown.val());
                        $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dropdown.text());
                        return;
                        //dropdown.val("C�mzettn�l");

                    }
                }
                 <%} %>

                if (dropdown != null) {
                    var dVal = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val();
                    var dText = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val();
                    dropdown.empty();
                    dropdown.html('<option value="' + '' + '">' + '[Nincs megadva elem]' + '</option>');

                    if (data === null || data.d === null) {
                        return;
                    }
                    if (dropdown !== null) {
                        var k = JSON.parse(data.d);

                        var s = '';
                        for (var i = 0; i < k.length; i++) {
                            s += '<option value="' + k[i].Id + '">' + k[i].Value + '</option>';
                        }
                        dropdown.html(s);

                        if (dVal != undefined && dText != undefined) {
                            dropdown.val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                        }

                    }
                }

            }
        </script>
        <%} %>
    </ContentTemplate>
</asp:UpdatePanel>
