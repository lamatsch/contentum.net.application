﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;

public partial class eRecordComponent_PldIratPeldanyFormTab : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_TOVABBITO_SZERVEZET = "TOVABBITO_SZERVEZET";
    //private const string kcs_IRAT_ALLAPOT = "IRAT_ALLAPOT";
    private const string kcs_IRATPELDANY_ALLAPOT = "IRATPELDANY_ALLAPOT";
    private const string kcs_IRAT_FAJTA = "IRAT_FAJTA";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";

    private const string stringEllenor = "Ellenõr:";
    private const string stringLeveltariAtvevo = "Levéltári átvevõ:";
    private const string stringBizottsagiTagok = "Bizottsági tagok:";

    private const string stringLeveltarbaAdas = "Levéltárba adás dátuma:";
    private const string stringSelejtezes = "Selejtezés idõpontja:";

    private const string stringLeveltarbaAdo = "Levéltárnak átadó:";
    private const string stringSelejtezo = "Selejtezõ:";

    public string Command = "";
    private PageView pageView = null;
    private String ParentId = "";

    public bool isTUKRendszer = false;

    private String _ParentForm = "";
    public bool isKiadmanyozott;

    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A szóban forgó iratpéldány objektum (megtekintés, módosítás esetén használatos)
    public EREC_PldIratPeldanyok obj_IratPeldany = null;
    public EREC_IraIratok obj_Irat = null;
    public EREC_UgyUgyiratdarabok obj_UgyiratDarab = null;
    public EREC_UgyUgyiratok obj_Ugyirat = null;


    // Az objektumban történõ változásokat jelzõ esemény, ami pl. kiválthat egy jogosultságellenõrzést
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }


    #region page

    // funkciójogok ellenõrzése a fõ formon

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Contentum.eUtility.Rendszerparameterek.IsTUK(Page))
        {
            peldanyHelyeTextBox.Visible = false;
            peldanyHelyeLabel.Visible = false;
        }

        pageView = new PageView(Page, ViewState);

        isTUKRendszer = Rendszerparameterek.IsTUK(Page);

        Command = Request.QueryString.Get(CommandName.Command);


        ParentId = Request.QueryString.Get("Id");


        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        //Partner és cím összekapcsolása
        Pld_PartnerId_Cimzett_PartnerTextBox.CimTextBox = Pld_CimId_Cimzett_CimekTextBox.TextBox;
        Pld_PartnerId_Cimzett_PartnerTextBox.CimHiddenField = Pld_CimId_Cimzett_CimekTextBox.HiddenField;

        Pld_CimId_Cimzett_CimekTextBox.SetCimTipusFilter(Pld_KuldesMod_KodtarakDropDownList.DropDownList, Pld_PartnerId_Cimzett_PartnerTextBox.BehaviorID);
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, Pld_KuldesMod_KodtarakDropDownList.DropDownList.UniqueID, Pld_UgyintezesModja_KodtarakDropDownList.DropDownList.ClientID);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!_Active) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        if (!IsPostBack)
        {
            isKiadmanyozott = false;
        }
        string IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);
        bool bFromIratPeldany = !String.IsNullOrEmpty(IratPeldanyId);

        /// Ha iratpéldányból hozunk létre egy új iratpéldányt, a címzett csak saját szervezet alatti belsõ partner lehet
        /// --> lecseréljük a partnerTextBox-ot CsoportTextBox-ra, mert ott ez meg van csinálva,
        /// a partnereknél meg nincs, és nagyon nincs kedvem most ezzel szórakozni...
        if (Command == CommandName.New && bFromIratPeldany)
        {
            /// PartnerTextBox eltüntetése, helyette csoporttextbox, és szûrés a saját szervezetre
            /// (Fontos, a kapott csoportId-ból majd vissza kell nyerni a partnerid-t, mert azt kell majd letárolni)
            Pld_Cimzett_CsoportTextBox.Visible = true;
            Pld_Cimzett_CsoportTextBox.FilterBySzervezetId(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));

            Pld_PartnerId_Cimzett_PartnerTextBox.Visible = false;
        }
        //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        //ExecParam execParam_KRT_Param =  UI.SetExecParamDefault(Page, new ExecParam());
        //execParam_KRT_Param.Record_Id = "F7D555E4-54BC-E611-80BB-00155D020D34"; //VonalkodKezeles, fajtája, hova tegyem a constansokat??
        //Result Result_KRTParam = service_parameterek.Get(execParam_KRT_Param);
        //BLG 1974
        //Amennyiben a VONALKOD_KEZELES = SAVOS és a VONALKOD_KIMENO_GENERALT értéke is 1-es, akkor belsõ keletkezésû/kimenõ irat iktatása esetén nem kötelezõ a vonalkód mezõ
        ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
        string vonalkodKiGeneralt = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KIMENO_GENERALT).Trim();
        //obj_Irat feltöltése a feltétel ellenõrzése elõtt, ha nincs meg
        if (obj_Irat == null)
        {
            string IratId = Request.QueryString.Get(QueryStringVars.IratId);
            if (!String.IsNullOrEmpty(IratId))
            {
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = IratId;

                Result result = service.GetIratHierarchia(execParam);

                if (String.IsNullOrEmpty(result.ErrorCode) && result.Record != null)
                {
                    IratHierarchia iratHier = (IratHierarchia)result.Record;
                    obj_Irat = iratHier.IratObj;
                    // Irat komponensek feltöltése:
                    //LoadIratComponents(obj_Irat);
                }
            }
        }
        //BLG 1974
        if (vonalkodkezeles.ToUpper().Trim().Equals("SAVOS") && vonalkodKiGeneralt.Trim().Equals("1") && obj_Irat != null && (obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno || obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso))
        {
            Barcode_VonalKodTextBox.Validate = false;
            Barcode_VonalKodTextBox.FormatValidate = false;
            labelVonalkodStar.Visible = false;
        }
        else if (Command == CommandName.New && !bFromIratPeldany && !Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).Equals("AZONOSITO"))//!((KRT_Parameterek)Result_KRTParam.Record).Ertek.ToUpper().Trim().Equals("AZONOSITO"))
        {
            //LZS - BUG_5369 vonalkód require validation letiltása
            Barcode_VonalKodTextBox.Validate = false;
            Barcode_VonalKodTextBox.FormatValidate = false;

            //Old version - before 2018.11.30 
            // vonalkód: új példánynál kötelezõ, ha nem elektronikus az ügyintézés (mert akkor generáljuk) és nem iratpéldányból jön létre (belsõ)
            //Barcode_VonalKodTextBox.Validate = (Pld_UgyintezesModja_KodtarakDropDownList.SelectedValue != KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir);
            //Barcode_VonalKodTextBox.FormatValidate = true;
            registerJavascripts();
        }
        else
        {
            Barcode_VonalKodTextBox.Validate = false;
            //BLG 1974 miatt megvan a VONALKOD_KEZELES, nem kell még egyszer lekérdezni
            if ((Command == CommandName.Modify || Command == CommandName.New) && !vonalkodkezeles.Equals("AZONOSITO"))
            {
                Barcode_VonalKodTextBox.FormatValidate = !Barcode_VonalKodTextBox.ReadOnly;
            }
        }

        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).Equals("AZONOSITO"))
        {
            labelVonalkodStar.Visible = false;
            starUpdate.Update();
        }

        if (obj_Irat != null)
        {
            UgyiratHiddenField.Value = obj_Irat.Ugyirat_Id;
        }

        IratPeldanyHiddenField.Value = IratPeldanyId;

        Pld_CimId_Cimzett_CimekTextBox.SetAutomatikusKitoltes(Pld_KuldesMod_KodtarakDropDownList.DropDownList, Pld_UgyintezesModja_KodtarakDropDownList.DropDownList,
            UgyiratHiddenField, IratPeldanyHiddenField);
    }

    protected void PldIratPeldanyUpdatePanel_Load(object sender, EventArgs e)
    {
        // ha nem ez az aktív tab, nem fut le
        if (!_Active) return;

        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshIratPeldanyPanel:
                        RaiseEvent_OnChangedObjectProperties();
                        ReLoadTab(true);
                        break;
                }
            }
        }
    }


    public void ReLoadTab()
    {
        if (!IsPostBack)
        {
            ReLoadTab(true);
        }
        else
        {
            ReLoadTab(false);
        }
    }

    public void ReLoadTab(bool loadRecord)
    {
        //// csak hogy továbbadhassuk a SetComponentsVisibility-nek, hogy a státusz miatt ne kelljen újra lekérni az iratpéldányt
        //IratPeldanyok.Statusz iratPeldanyStatusz = null;

        /*1868 blg   FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("PldIratPeldanyokFormTabPrintForm.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=BoritoA3");
           FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("PldIratPeldanyokFormTabPrintForm.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=BoritoA4");
           FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("PldIratPeldanyokFormTabPrintForm.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=Kisero");
           */
        FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = "javascript:window.open('IratPeldanyokSSRSBoritoA3.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=BoritoA3')";
        FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = "javascript:window.open('IratPeldanyokSSRSBorito.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=Borito')";
        FunkcioGombsor.KiserolapOnClientClick = "javascript:window.open('KuldKuldemenyekPrintFormSSRSIratpeldanyKisero.aspx?" + QueryStringVars.IratPeldanyId + "=" + ParentId + "&tipus=Kisero')";
        FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=IratPeldany')";

        if (ParentForm == Constants.ParentForms.IratPeldany)
        {
            if (!MainPanel.Visible) return;

            FunkcioGombsor.ParentForm = _ParentForm;

            if (Command == CommandName.New)
            {
                #region Command == CommandName.New

                #region BLG_2948
                //LZS - ha új iratpéldányt hozunk létre, akkor nem kell hogy aktív legyen.
                ccPldIratMegsemmisitesDatuma.ReadOnly = true;
                #endregion

                // Csak akkor engedélyezett a New, ha van megadva IratId, vagy Iratpéldány Id
                string IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

                string IratId = Request.QueryString.Get(QueryStringVars.IratId);
                if (String.IsNullOrEmpty(IratId) && String.IsNullOrEmpty(IratPeldanyId))
                {
                    // le kell tiltani mindent a hibapanelen kívül
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader,
                        Resources.Error.UINoIratIdParam);
                    ErrorUpdatePanel1.Update();

                    PldIratPeldanyUpdatePanel.Visible = false;

                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(IratPeldanyId))
                    {
                        #region IratPéldány hierarchia lekérése, komponensek feltöltése

                        EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                        ExecParam execParam_Pld = UI.SetExecParamDefault(Page);
                        execParam_Pld.Record_Id = IratPeldanyId;

                        Result result_PldHier = service_Pld.GetIratPeldanyHierarchia(execParam_Pld);

                        if (result_PldHier.IsError)
                        {
                            // le kell tiltani mindent a hibapanelen kívül
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_PldHier);
                            ErrorUpdatePanel1.Update();
                            PldIratPeldanyUpdatePanel.Visible = false;
                            return;
                        }

                        IratPeldanyHierarchia pldHier = (IratPeldanyHierarchia)result_PldHier.Record;

                        obj_IratPeldany = pldHier.IratPeldanyObj;
                        obj_Irat = pldHier.IratObj;
                        obj_UgyiratDarab = pldHier.UgyiratDarabObj;
                        obj_Ugyirat = pldHier.UgyiratObj;

                        // komponensek feltöltése:
                        LoadComponentsFromBusinessObject(obj_IratPeldany);
                        //if (obj_Irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
                        //{
                        //    LoadIrattariHelyek(obj_Ugyirat.Csoport_Id_Ugyfelelos, obj_IratPeldany.IrattarId);
                        //}
                        //else
                        //{
                        LoadIrattariHelyek(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, obj_IratPeldany.IrattarId);
                        //}
                        //LoadIratComponents(obj_Irat);
                        //LoadUgyiratComponents(obj_Ugyirat);

                        #region Partner_Id_Cimzett alapján Pld_Cimzett_CsoportTextBox kitöltése (partnerId --> csoportId)

                        string partnerId_cimzett = obj_IratPeldany.Partner_Id_Cimzett;
                        if (!String.IsNullOrEmpty(partnerId_cimzett))
                        {
                            KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                            Result result_csoport = service_csoportok.GetByPartnerId(UI.SetExecParamDefault(Page), partnerId_cimzett);
                            if (result_csoport.IsError)
                            {
                                // hiba:
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_csoport, Resources.Error.UICimzettPartnerGetError);
                                ErrorUpdatePanel1.Update();
                                MainPanel.Visible = false;
                                return;
                            }

                            KRT_Csoportok csoport_cimzett = (KRT_Csoportok)result_csoport.Record;
                            if (csoport_cimzett != null)
                            {
                                // Pld_Cimzett_CsoportTextBox beállítása:
                                Pld_Cimzett_CsoportTextBox.Id_HiddenField = csoport_cimzett.Id;
                                Pld_Cimzett_CsoportTextBox.Text = csoport_cimzett.Nev;
                            }
                        }
                        #endregion

                        #endregion
                    }
                    else if (!String.IsNullOrEmpty(IratId))
                    {
                        #region Irathierarchia lekérése, komponensek feltöltése

                        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page);
                        execParam.Record_Id = IratId;

                        Result result = service.GetIratHierarchia(execParam);

                        if (String.IsNullOrEmpty(result.ErrorCode) && result.Record != null)
                        {
                            IratHierarchia iratHier = (IratHierarchia)result.Record;

                            obj_Irat = iratHier.IratObj;
                            obj_UgyiratDarab = iratHier.UgyiratDarabObj;
                            obj_Ugyirat = iratHier.UgyiratObj;

                            // Irat komponensek feltöltése:
                            LoadIratComponents(obj_Irat);
                            LoadUgyiratComponents(obj_Ugyirat);
                            //if (obj_Irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
                            //{
                            //    LoadIrattariHelyek(obj_Ugyirat.Csoport_Id_Ugyfelelos, string.Empty);
                            //}
                            //else
                            //{
                            LoadIrattariHelyek(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, string.Empty);
                            //}
                        }
                        else
                        {
                            // le kell tiltani mindent a hibapanelen kívül
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();

                            PldIratPeldanyUpdatePanel.Visible = false;

                            return;
                        }

                        #endregion
                    }

                    // Komponensek inicializálása:
                    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, false, EErrorPanel1);
                    //Pld_Tovabbito_KodtarakDropDownList.FillDropDownList(kcs_TOVABBITO_SZERVEZET, true, EErrorPanel1);
                    Pld_Visszavarolag_KodtarakDropDownList.FillDropDownList(kcs_VISSZAVAROLAG, true, EErrorPanel1);
                    // eredet kötelezõ
                    Pld_Eredet_KodtarakDropDownList.FillDropDownList(kcs_IRAT_FAJTA, false, EErrorPanel1);

                    // Az iratpéldány fajtájánál a listából le kell venni a 'Vegyes' értéket:
                    List<string> ktUgyintezesAlapjaFilterList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYINTEZES_ALAPJA, this.Page, null)
                                        .Where(kt => kt.Kod != KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                                        .Select(kt => kt.Kod)
                                        .ToList();
                    // BUG_8852
                    //Pld_UgyintezesModja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, ktUgyintezesAlapjaFilterList, false, EErrorPanel1);
                    if (UtilityCsatolmanyok.HasKrCsatolmany(Page, IratId))
                    {
                        Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, false, EErrorPanel1);
                        Pld_UgyintezesModja_KodtarakDropDownList.Enabled = false;
                    }
                    else Pld_UgyintezesModja_KodtarakDropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, ktUgyintezesAlapjaFilterList, false, EErrorPanel1);

                    // Keltezés a mai nap:
                    Pld_LetrehozasIdo_CalendarControl.Text = DateTime.Today.ToString();

                    ClearComponentsForNewPeldany();

                }
                #endregion
            }
            else if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                    MainPanel.Visible = false;
                    return;
                }
                else
                {
                    if (loadRecord)
                    {
                        if (obj_IratPeldany == null)
                        {
                            // rekordok betöltése

                            bool success = IratPeldanyok.Set4ObjectsByIratPeldanyId(ParentId, out obj_IratPeldany, out obj_Irat, out obj_UgyiratDarab, out obj_Ugyirat
                                    , Page, FormHeader.ErrorPanel);

                            if (success == false)
                            {
                                MainPanel.Visible = false;
                                return;
                            }
                        }

                        if (obj_IratPeldany != null)
                        {
                            //iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_IratPeldany);

                            LoadComponentsFromBusinessObject(obj_IratPeldany);
                            //if (obj_Irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
                            //{
                            //    LoadIrattariHelyek(obj_Ugyirat.Csoport_Id_Ugyfelelos, obj_IratPeldany.IrattarId);
                            //}
                            //else
                            //{
                            LoadIrattariHelyek(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, obj_IratPeldany.IrattarId);
                            //}
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                        ResultError.CreateNewResultWithErrorCode(50101));
                            ErrorUpdatePanel1.Update();
                            return;
                        }
                    }
                }
            }

            // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
            TabFooter1.CommandArgument = Command;


            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
    }


    #endregion

    #region private methods

    private void SetComponentsVisibility(String _command)
    {
        #region FunkcióGombsor állítása

        // Az osszes gomb eltuntetese
        FunkcioGombsor.ButtonsVisible(false);

        if (Command == CommandName.View)
        {
            // FunkcioGombSor gombjainak beallitasa
            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
            }
            else
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            }
            FunkcioGombsor.KiserolapVisible = true;
            FunkcioGombsor.XMLExportVisible = true;

        }
        if (Command == CommandName.Modify)
        {
            // FunkcioGombSor gombjainak beallitasa

            FunkcioGombsor.Atadasra_kijelolesVisible = true;
            FunkcioGombsor.IratPeldany_LetrehozasaVisible = true;
            FunkcioGombsor.PostazasVisible = true;
            //FunkcioGombsor.PostazobaVisible = true;
            FunkcioGombsor.ExpedialasVisible = true;
            FunkcioGombsor.SztornozasVisible = true;
            FunkcioGombsor.ValaszVisible = true;

            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
            }
            else
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            }
            FunkcioGombsor.KiserolapVisible = true;
            FunkcioGombsor.XMLExportVisible = true;

            //FunkcioGombsor.Kuldemeny_lezarasaVisible = true;

            // Állapot és funkciójogosultság alapján beállítjuk a funkciógombokat:

            if (obj_IratPeldany == null)
            {
                // rekordok betöltése

                bool success = IratPeldanyok.Set4ObjectsByIratPeldanyId(ParentId, out obj_IratPeldany, out obj_Irat, out obj_UgyiratDarab, out obj_Ugyirat
                        , Page, FormHeader.ErrorPanel);

                if (success == false)
                {
                    MainPanel.Visible = false;
                    return;
                }
            }

            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_IratPeldany);

            // irathoz elsõ iratpéldány õrzõjét csak akkor állítjuk be, ha ez az iratpéldány volt az elsõ
            // TODO: minden esetben be kéne állítani az elsõ iratpéldány õrzõjét
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_UgyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);


            // Átadásra kijelölés gomb
            IratPeldanyok.SetAtadasraKijeloles_Funkciogomb(ParentId, iratPeldanyStatusz, iratStatusz, ugyiratStatusz
                , FunkcioGombsor.Atadasra_kijeloles_ImageButton, Page);

            // BUG_8852
            //IratPeldanyok.SetIratPeldanyLetrehozasa_ImageButton(iratPeldanyStatusz.Id, FunkcioGombsor.IratPeldany_Letrehozasa_ImageButton);
            IratPeldanyok.SetIratPeldanyLetrehozasa_ImageButton(iratStatusz.Id, iratPeldanyStatusz.Id, FunkcioGombsor.IratPeldany_Letrehozasa_ImageButton);

            // TODO:
            FunkcioGombsor.PostazasEnabled = false;
            UI.SetImageButtonStyleToDisabled(FunkcioGombsor.PostazasImageButton);

            //// TODO:
            //FunkcioGombsor.PostazobaEnabled = false;
            //UI.SetImageButtonStyleToDisabled(FunkcioGombsor.PostazobaImageButton);

            bool kiadmanyozando = (obj_Irat.KiadmanyozniKell == "1") ? true : false;

            // Expediálás
            IratPeldanyok.SetExpedialas_Funkciogomb(ParentId, iratPeldanyStatusz, kiadmanyozando, iratStatusz, ugyiratDarabStatusz, ugyiratStatusz
                , FunkcioGombsor.ExpedialasImageButton, Page);

            // TODO:
            FunkcioGombsor.SztornozasEnabled = false;
            UI.SetImageButtonStyleToDisabled(FunkcioGombsor.SztornozasImage);

            // TODO:
            FunkcioGombsor.ValaszEnabled = false;
            UI.SetImageButtonStyleToDisabled(FunkcioGombsor.ValaszImageButton);
        }

        #endregion

        #region TabFooter gombok állítása

        if (_command == CommandName.Modify)
        {
            // Rendben és bezár gomb módosításnál látszódik
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
        }

        // New módban kell a Mégsem gomb, egyébként nem
        if (_command == CommandName.New)
        {
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
            TabFooter1.ImageButton_SaveAndClose.Visible = false;
            TabFooter1.ImageButton_Cancel.Visible = true;
            TabFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue='0'; window.close(); return false;";
        }
        else
        {
            // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
            TabFooter1.ImageButton_Cancel.Visible = false;
        }

        #endregion

        #region Form komponensek (TextBoxok,...) állítása

        /// New és Modify esetben elvileg ugyanolyan a komponensek láthatósága,
        /// erre vannak beállítva alapból

        if (isTUKRendszer)
        {
            tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
            //JavaScripts.SetFocus(IrattariHelyLevelekDropDownTUK);              
        }

        if (Command == CommandName.View)
        {
            Pld_PartnerId_Cimzett_PartnerTextBox.ReadOnly = true;
            Pld_PartnerId_Cimzett_PartnerTextBox.ViewMode = true;

            Pld_KuldesMod_KodtarakDropDownList.ReadOnly = true;

            Pld_CimId_Cimzett_CimekTextBox.ReadOnly = true;
            Pld_CimId_Cimzett_CimekTextBox.ViewMode = true;

            //Pld_Tovabbito_KodtarakDropDownList.ReadOnly = true;
            Pld_Visszavarolag_KodtarakDropDownList.ReadOnly = true;
            //Pld_Ragszam_TextBox.ReadOnly = true;
            Pld_Eredet_KodtarakDropDownList.ReadOnly = true;
            Pld_VisszaerkezesiHatarido_CalendarControl.ReadOnly = true;
            Pld_VisszaerkezesDatuma_CalendarControl.ReadOnly = true;
            Pld_AtvetelDatuma_CalendarControl.ReadOnly = true;
            Pld_PostazasDatuma_CalendarControl.ReadOnly = true;
            elektronikus.Enabled = false;
            papir_alapu.Enabled = false;
            egyeb.Enabled = false;
            Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = true;
            Barcode_VonalKodTextBox.ReadOnly = true;

            //IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotById(id, execParam, FormHeader1.ErrorPanel);
            IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(obj_IratPeldany);

            // irathoz elsõ iratpéldány õrzõjét csak akkor állítjuk be, ha ez az iratpéldány volt az elsõ
            // TODO: minden esetben be kéne állítani az elsõ iratpéldány õrzõjét
            string elsoIratPeldanyOrzo = String.Empty;
            if (iratPeldanyStatusz.Sorszam == "1") { elsoIratPeldanyOrzo = iratPeldanyStatusz.FelhCsopId_Orzo; }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, elsoIratPeldanyOrzo);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_UgyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


            ErrorDetails errorDetail = null;
            bool isModosithato = IratPeldanyok.Modosithato(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            IsOverrideMode = IratPeldanyok.IsModosithatoOveride(iratPeldanyStatusz, execParam);
            if (!isModosithato && IsOverrideMode)
            {
               Pld_Visszavarolag_KodtarakDropDownList.Enabled = true;
                Pld_Visszavarolag_KodtarakDropDownList.ReadOnly = false;
                Pld_Eredet_KodtarakDropDownList.Enabled = true;
                Pld_Eredet_KodtarakDropDownList.ReadOnly = false;
                Pld_UgyintezesModja_KodtarakDropDownList.Enabled = true;
                Pld_UgyintezesModja_KodtarakDropDownList.ReadOnly = false;
                TabFooter1.Command = CommandName.Save;
                TabFooter1.ImageButton_SaveAndClose.Visible = true;              
                TabFooter1.SaveEnabled = true;

            }

            if (isTUKRendszer)
            {
                IrattariHelyLevelekDropDownTUK.Enabled = false;
            }

            // a többi alapból ReadOnly
        }

        if (Command == CommandName.Modify)
        {
            // ha ki van töltve a vonalkód --> readonly
            if (!String.IsNullOrEmpty(Barcode_VonalKodTextBox.Text))
            {
                Barcode_VonalKodTextBox.ReadOnly = true;
            }
        }

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        // Csak TÜK-ös esetben látszódik:
        this.trTUKMinositesEsKezelesiUtasitas.Visible = isTUK;

        #endregion
    }

    private bool IsOverrideMode { get
        {
            object result = Page.Session["IsOverrideMode"];
            if(result == null) { return false; }
            return (bool)result;
        }
        set
        {
            Page.Session["IsOverrideMode"] = value;
        }
    }

    private void ClearComponentsForNewPeldany()
    {
        #region Ha a címzett nem belsõ partner, akkor töröljük a komponenst

        if (!String.IsNullOrEmpty(Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField))
        {
            KRT_PartnerekService service_partnerek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            ExecParam execParam_partnerGet = UI.SetExecParamDefault(Page);
            execParam_partnerGet.Record_Id = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;

            Result result_partnerGet = service_partnerek.Get(execParam_partnerGet);
            if (result_partnerGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partnerGet);
                ErrorUpdatePanel1.Update();

                Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = String.Empty;
                Pld_PartnerId_Cimzett_PartnerTextBox.Text = String.Empty;
                Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;

                Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = String.Empty;
                Pld_CimId_Cimzett_CimekTextBox.Text = String.Empty;
            }
            else
            {
                KRT_Partnerek partner = (KRT_Partnerek)result_partnerGet.Record;

                // ha nem belsõ a partner, töröljük a mezõket
                if (partner.Belso != "1")
                {
                    Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = String.Empty;
                    Pld_PartnerId_Cimzett_PartnerTextBox.Text = String.Empty;
                    Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;

                    Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = String.Empty;
                    Pld_CimId_Cimzett_CimekTextBox.Text = String.Empty;
                }
            }
        }
        else
        {
            // töröljük a címzett szöveget és a cím textboxot is (hátha valamelyik ki volt töltve)
            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = String.Empty;
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = String.Empty;
            Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;

            Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = String.Empty;
            Pld_CimId_Cimzett_CimekTextBox.Text = String.Empty;
        }

        #endregion

        //Pld_Allapot_KodtarakDropDownList.Clear();
        if (isKiadmanyozott)
        {
            Pld_Allapot_KodtarakDropDownList.FillWithOneValue(kcs_IRATPELDANY_ALLAPOT, KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott, EErrorPanel1);
        }
        else
        {
            Pld_Allapot_KodtarakDropDownList.Clear();
        }
        Pld_CsopId_Felelos_CsoportTextBox.Id_HiddenField = String.Empty;
        Pld_CsopId_Felelos_CsoportTextBox.Text = String.Empty;

        Pld_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = String.Empty;
        Pld_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Text = String.Empty;

        Barcode_VonalKodTextBox.Text = String.Empty;

        Pld_SztornirozasDat_CalendarControl.Text = String.Empty;
        Pld_PostazasDatuma_CalendarControl.Text = String.Empty;
    }



    //business object --> form
    private void LoadComponentsFromBusinessObject(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        if (erec_PldIratPeldanyok != null)
        {
            // Iratpéldány rész:

            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = erec_PldIratPeldanyok.Partner_Id_Cimzett;
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
            			
            // BUG_13213
            Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt;

			//if(Command == CommandName.View && String.IsNullOrEmpty(erec_PldIratPeldanyok.Partner_Id_Cimzett))
            //{
            //    Pld_PartnerId_Cimzett_PartnerTextBox.ImageButton_View.Enabled = false;
            //    Pld_PartnerId_Cimzett_PartnerTextBox.ImageButton_View.CssClass = "disabledLovListItem";
            //}

            Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
                erec_PldIratPeldanyok.KuldesMod, false, EErrorPanel1);

            Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = erec_PldIratPeldanyok.Cim_id_Cimzett;
            Pld_CimId_Cimzett_CimekTextBox.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;
            if (Command == CommandName.View && String.IsNullOrEmpty(erec_PldIratPeldanyok.Cim_id_Cimzett))
            {
                Pld_CimId_Cimzett_CimekTextBox.ImageButton_View.Enabled = false;
                Pld_CimId_Cimzett_CimekTextBox.ImageButton_View.CssClass = "disabledLovListItem";
            }

            if (!String.IsNullOrEmpty(erec_PldIratPeldanyok.Cim_id_Cimzett))
            {
                var execParam = UI.SetExecParamDefault(Page, new ExecParam());
                var cim = Contentum.eUtility.CimUtility.GetCimSTRById(execParam, erec_PldIratPeldanyok.Cim_id_Cimzett);
                if (!String.IsNullOrEmpty(cim))
                {
                    Pld_CimId_Cimzett_CimekTextBox.Text = cim;
                }
            }

            //Pld_Tovabbito_KodtarakDropDownList.FillAndSetSelectedValue(kcs_TOVABBITO_SZERVEZET,
            //    erec_PldIratPeldanyok.Tovabbito, true, EErrorPanel1);

            Pld_Allapot_KodtarakDropDownList.FillWithOneValue(kcs_IRATPELDANY_ALLAPOT,
                erec_PldIratPeldanyok.Allapot, EErrorPanel1);

            // eredet kötelezõ
            Pld_Eredet_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_FAJTA,
                erec_PldIratPeldanyok.Eredet, false, EErrorPanel1);

            Pld_Visszavarolag_KodtarakDropDownList.FillAndSetSelectedValue(kcs_VISSZAVAROLAG,
                erec_PldIratPeldanyok.Visszavarolag, true, EErrorPanel1);

            //Pld_Ragszam_TextBox.Text = erec_PldIratPeldanyok.Ragszam;

            Pld_CsopId_Felelos_CsoportTextBox.Id_HiddenField = erec_PldIratPeldanyok.Csoport_Id_Felelos;
            Pld_CsopId_Felelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            Pld_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo;
            Pld_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            // Az iratpéldány fajtájánál a listából le kell venni a 'Vegyes' értéket:
            List<string> ktUgyintezesAlapjaFilterList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYINTEZES_ALAPJA, this.Page, null)
                                .Where(kt => kt.Kod != KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                                .Select(kt => kt.Kod)
                                .ToList();
            // BUG_8852
            //Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_PldIratPeldanyok.UgyintezesModja, ktUgyintezesAlapjaFilterList, true, EErrorPanel1);
            if (UtilityCsatolmanyok.HasKrCsatolmany(Page, erec_PldIratPeldanyok.IraIrat_Id))
            {
                Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir, false, EErrorPanel1);
                Pld_UgyintezesModja_KodtarakDropDownList.Enabled = false;
            }
            else Pld_UgyintezesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA, erec_PldIratPeldanyok.UgyintezesModja, ktUgyintezesAlapjaFilterList, true, EErrorPanel1);



            Barcode_VonalKodTextBox.Text = erec_PldIratPeldanyok.BarCode;

            Pld_LetrehozasIdo_CalendarControl.Text = erec_PldIratPeldanyok.Base.LetrehozasIdo;
            Pld_VisszaerkezesiHatarido_CalendarControl.Text = erec_PldIratPeldanyok.VisszaerkezesiHatarido;
            Pld_VisszaerkezesDatuma_CalendarControl.Text = erec_PldIratPeldanyok.VisszaerkezesDatuma;
            Pld_AtvetelDatuma_CalendarControl.Text = erec_PldIratPeldanyok.AtvetelDatuma;
            Pld_SztornirozasDat_CalendarControl.Text = erec_PldIratPeldanyok.SztornirozasDat;
            Pld_PostazasDatuma_CalendarControl.Text = erec_PldIratPeldanyok.PostazasDatuma;

            #region BLG_2948
            // LZS - Megtekintés képernyőre kitesszük a Megsemmisitve és a Megsemmisítés dátuma mezőket és feliratokat, akkor ha papíralapú iratpéldányt tekintünk meg.
            if (erec_PldIratPeldanyok.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir && !String.IsNullOrEmpty(erec_PldIratPeldanyok.SztornirozasDat))
            {
                cbPldIratMegssemisitve.Checked = erec_PldIratPeldanyok.IratPeldanyMegsemmisitve == "1" ? true : false;
                ccPldIratMegsemmisitesDatuma.ReadOnly = true;
                ccPldIratMegsemmisitesDatuma.Text = erec_PldIratPeldanyok.IratPeldanyMegsemmisitesDatuma;
            }
            else
            {
                lblPldIratMegsemmisitve.Visible =
                lblPldIratMegsemmisitesDatuma.Visible =
                cbPldIratMegssemisitve.Visible =
                ccPldIratMegsemmisitesDatuma.Visible = false;
            }
            #endregion

            switch (erec_PldIratPeldanyok.ValaszElektronikus)
            {
                case "0":
                    elektronikus.Checked = true;
                    break;
                case "1":
                    papir_alapu.Checked = true;
                    break;
                case "2":
                    egyeb.Checked = true;
                    break;
                default:
                    break;
            }

            Record_Ver_HiddenField.Value = erec_PldIratPeldanyok.Base.Ver;

            if (obj_Irat == null)
            {
                #region Irat lekérése a többi komponens feltöltéséhez:

                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam_iratGet = UI.SetExecParamDefault(Page, new ExecParam());
                execParam_iratGet.Record_Id = erec_PldIratPeldanyok.IraIrat_Id;

                Result result_IratGet = erec_IraIratokService.Get(execParam_iratGet);
                if (String.IsNullOrEmpty(result_IratGet.ErrorCode))
                {
                    if (result_IratGet.Record == null)
                    {
                        return;
                    }
                    else
                    {
                        obj_Irat = (EREC_IraIratok)result_IratGet.Record;
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_IratGet);
                    ErrorUpdatePanel1.Update();
                }

                #endregion
            }

            if (obj_Irat != null)
            {
                LoadIratComponents(obj_Irat);
            }
            else { return; }


            if (obj_Ugyirat == null)
            {
                #region Ügyirat lekérése

                EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page);
                execParam_ugyiratGet.Record_Id = obj_Irat.Ugyirat_Id;

                Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyiratGet);
                if (result_ugyiratGet.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGet);
                    return;
                }

                obj_Ugyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                #endregion
            }

            #region Form Címének állítása

            if (FormHeader != null)
            {
                string fullIktatoszam = obj_IratPeldany.Azonosito;
                //string fullIktatoszam = IratPeldanyok.GetFullIktatoszam(obj_UgyiratDarab,
                //        obj_Irat, erec_PldIratPeldanyok, Page, EErrorPanel1, ErrorUpdatePanel1);

                // BLG_619
                if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
                {

                    EREC_IraIratokService tempService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    string iratfelelos_szervezetkod = "";
                    Result tempResult = tempService.GetIratFelelosSzervezetKod(temp_execParam, obj_IratPeldany.IraIrat_Id);
                    if (!tempResult.IsError)
                    {
                        iratfelelos_szervezetkod = tempResult.Record.ToString();
                        if (!String.IsNullOrEmpty(iratfelelos_szervezetkod))
                        {
                            fullIktatoszam = iratfelelos_szervezetkod + " " + obj_IratPeldany.Azonosito;
                        }

                    }

                }

                if (Command == CommandName.View)
                {
                    FormHeader.FullManualHeaderTitle = fullIktatoszam + " " +
                        Resources.Form.Iratpeldanymegtekintese_ManualFormHeaderTitle;
                }
                else if (Command == CommandName.Modify)
                {
                    FormHeader.FullManualHeaderTitle = fullIktatoszam + " " +
                        Resources.Form.Iratpeldanymodositasa_ManualFormHeaderTitle;
                }
            }

            #endregion

            if (obj_Ugyirat != null)
            {
                LoadUgyiratComponents(obj_Ugyirat);
            }

            SetExpedialasiAdatok(erec_PldIratPeldanyok);

            if (erec_PldIratPeldanyok.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Selejtezett
                || erec_PldIratPeldanyok.Allapot == KodTarak.IRATPELDANY_ALLAPOT.LeveltarbaAdott)
            {
                LoadIrattariAdatok(erec_PldIratPeldanyok);
            }
        }
    }

    /// <summary>
    /// Expediálás ideje, Expediáló felhasználó lekérése és megjelenítése
    /// </summary>
    /// <param name="erec_PldIratPeldanyok"></param>
    private void SetExpedialasiAdatok(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        trExpedialas.Visible = false;
        if (erec_PldIratPeldanyok != null)
        {
            if (erec_PldIratPeldanyok.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Iktatott)
            {
                EREC_Kuldemeny_IratPeldanyaiSearch sch = new EREC_Kuldemeny_IratPeldanyaiSearch(true);
                sch.Peldany_Id.Value = erec_PldIratPeldanyok.Id;
                sch.Peldany_Id.Operator = Query.Operators.equals;
                sch.Extended_EREC_KuldKuldemenyekSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Kimeno;
                sch.Extended_EREC_KuldKuldemenyekSearch.PostazasIranya.Operator = Query.Operators.equals;
                sch.Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Value = "getdate()";
                sch.Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
                sch.Extended_EREC_KuldKuldemenyekSearch.ErvVege.Value = "getdate()";
                sch.Extended_EREC_KuldKuldemenyekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
                sch.OrderBy = Constants.BusinessDocument.nullString;
                sch.TopRow = 1;

                ExecParam xpm = UI.SetExecParamDefault(Page);
                EREC_Kuldemeny_IratPeldanyaiService svc = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();

                Result res = svc.GetAllWithExtension(xpm, sch);

                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                    return;
                }

                if (res.Ds != null && res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = res.Ds.Tables[0].Rows[0];
                    string expedialoFelhasznaloId = row["FelhasznaloCsoport_Id_Expedial"].ToString();
                    string expedialasIdeje = row["ExpedialasIdeje"].ToString();

                    if (!String.IsNullOrEmpty(expedialoFelhasznaloId) && !String.IsNullOrEmpty(expedialasIdeje))
                    {
                        trExpedialas.Visible = true;
                        fcsExpedialo.Id_HiddenField = expedialoFelhasznaloId;
                        fcsExpedialo.SetCsoportTextBoxById(EErrorPanel1);
                        ccExpedialasDatuma.Text = expedialasIdeje;
                    }
                }
            }
        }
    }

    private void LoadIratComponents(EREC_IraIratok erec_IraIratok)
    {
        if (erec_IraIratok != null)
        {
            IraIrat_Targy_RequiredTextBox.Text = erec_IraIratok.Targy;
            if (erec_IraIratok.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            {
                isKiadmanyozott = true;
            }
            IraIrat_Kategoria_KodtarakDropDownList.FillWithOneValue(kcs_IRATKATEGORIA,
                erec_IraIratok.Kategoria, EErrorPanel1);
            // BLG_619
            Iratfelelos_CsoportTextBox.Id_HiddenField = erec_IraIratok.Csoport_Id_Ugyfelelos;
            Iratfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            FelelosSzervezetVezetoTextBoxInstance.SetSzervezet(erec_IraIratok.Csoport_Id_Ugyfelelos);

            bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
            // BLG#1402:
            if (isTUK)
            {
                // Irat minõsítés:
                ktDropDownListIratMinosites.FillWithOneValue(kcs_IRATMINOSITES,
                    erec_IraIratok.Minosites, EErrorPanel1);
                // Kezelési utasítás:
                txtIratKezelesiUtasitas.Text = this.GetIratKezelesiUtasitas(erec_IraIratok.Id);
            }
        }
    }

    /// <summary>
    /// A megadott irathoz kapcsolt tárgyszavak közül a "Kezelési utasítás" tárgyszó lekérése
    /// </summary>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private string GetIratKezelesiUtasitas(string iratId)
    {
        string targyszoKod = "Kezelesi_utasitas";

        // Az irathoz rendelt tárgyszavak:
        var resultIratTargyszavak = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService()
                        .GetAllMetaByDefinicioTipus(UI.SetExecParamDefault(Page), new EREC_ObjektumTargyszavaiSearch()
                                , iratId.ToString(), null, "EREC_IraIratok", null, false, false);

        if (resultIratTargyszavak.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultIratTargyszavak);
            return null;
        }

        foreach (DataRow row in resultIratTargyszavak.Ds.Tables[0].Rows)
        {
            if (row["BelsoAzonosito"].ToString() == targyszoKod)
            {
                // Megvan a keresett tárgyszó:
                return row["ErtekByControlSourceType"].ToString();
            }
        }

        return null;
    }

    private void LoadUgyiratComponents(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        if (erec_UgyUgyiratok != null)
        {
            Ugyirat_FelCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField =
                erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            Ugyirat_FelCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
        }
    }

    // form --> business object
    private EREC_PldIratPeldanyok GetBusinessObjectFromComponents()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        // csak azok a mezõk, amelyek módosításnál / felvételnél engedélyezettek:

        erec_PldIratPeldanyok.Partner_Id_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
        erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

		// BUG_13213
        erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt = Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue;
        erec_PldIratPeldanyok.Updated.Partner_Id_CimzettKapcsolt = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

        erec_PldIratPeldanyok.NevSTR_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Text;
        erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

        if (elektronikus.Checked)
        {
            erec_PldIratPeldanyok.ValaszElektronikus = "0";
            erec_PldIratPeldanyok.Updated.ValaszElektronikus = pageView.GetUpdatedByView(elektronikus);
        }
        if (papir_alapu.Checked)
        {
            erec_PldIratPeldanyok.ValaszElektronikus = "1";
            erec_PldIratPeldanyok.Updated.ValaszElektronikus = pageView.GetUpdatedByView(papir_alapu);
        }
        if (egyeb.Checked)
        {
            erec_PldIratPeldanyok.ValaszElektronikus = "2";
            erec_PldIratPeldanyok.Updated.ValaszElektronikus = pageView.GetUpdatedByView(egyeb);
        }

        erec_PldIratPeldanyok.KuldesMod = Pld_KuldesMod_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.KuldesMod = pageView.GetUpdatedByView(Pld_KuldesMod_KodtarakDropDownList);

        erec_PldIratPeldanyok.Cim_id_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField;
        erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);

        erec_PldIratPeldanyok.CimSTR_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Text;
        erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);

        //erec_PldIratPeldanyok.Tovabbito = Pld_Tovabbito_KodtarakDropDownList.SelectedValue;
        //erec_PldIratPeldanyok.Updated.Tovabbito = pageView.GetUpdatedByView(Pld_Tovabbito_KodtarakDropDownList);

        erec_PldIratPeldanyok.Visszavarolag = Pld_Visszavarolag_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.Visszavarolag = pageView.GetUpdatedByView(Pld_Visszavarolag_KodtarakDropDownList);

        //erec_PldIratPeldanyok.Ragszam = Pld_Ragszam_TextBox.Text;
        //erec_PldIratPeldanyok.Updated.Ragszam = pageView.GetUpdatedByView(Pld_Ragszam_TextBox);

        erec_PldIratPeldanyok.Eredet = Pld_Eredet_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.Eredet = pageView.GetUpdatedByView(Pld_Eredet_KodtarakDropDownList);

        erec_PldIratPeldanyok.VisszaerkezesiHatarido = Pld_VisszaerkezesiHatarido_CalendarControl.Text;
        erec_PldIratPeldanyok.Updated.VisszaerkezesiHatarido = pageView.GetUpdatedByView(Pld_VisszaerkezesiHatarido_CalendarControl);

        erec_PldIratPeldanyok.VisszaerkezesDatuma = Pld_VisszaerkezesDatuma_CalendarControl.Text;
        erec_PldIratPeldanyok.Updated.VisszaerkezesDatuma = pageView.GetUpdatedByView(Pld_VisszaerkezesDatuma_CalendarControl);

        erec_PldIratPeldanyok.AtvetelDatuma = Pld_AtvetelDatuma_CalendarControl.Text;
        erec_PldIratPeldanyok.Updated.AtvetelDatuma = pageView.GetUpdatedByView(Pld_AtvetelDatuma_CalendarControl);

        erec_PldIratPeldanyok.UgyintezesModja = Pld_UgyintezesModja_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.UgyintezesModja = pageView.GetUpdatedByView(Pld_UgyintezesModja_KodtarakDropDownList);

        if (!String.IsNullOrEmpty(Barcode_VonalKodTextBox.Text))
        {
            erec_PldIratPeldanyok.BarCode = Barcode_VonalKodTextBox.Text;
            erec_PldIratPeldanyok.Updated.BarCode = pageView.GetUpdatedByView(Barcode_VonalKodTextBox);
        }

        if (isTUKRendszer)
        {
            erec_PldIratPeldanyok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_PldIratPeldanyok.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            erec_PldIratPeldanyok.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
            erec_PldIratPeldanyok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
        }

        erec_PldIratPeldanyok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_PldIratPeldanyok.Base.Updated.Ver = true;


        erec_PldIratPeldanyok.Allapot = Pld_Allapot_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.Allapot = true;

        return erec_PldIratPeldanyok;
    }

    private void Load_ComponentSelectModul()
    {
        /*if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            // TODO: felkell sorolni...
            /*compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }*/
    }



    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
         e.CommandName == CommandName.SaveAndClose || e.CommandName == CommandName.SaveAndNew)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IratPeldany" + Command))
            {
                bool voltHiba = false;
                string formCommandToCheck = Command;
                //Ha override módba nyitjuk meg a formot akkor 3 mező szerkeszthető 
                if(formCommandToCheck == CommandName.View && IsOverrideMode)
                {
                    formCommandToCheck = CommandName.Modify;
                }

                switch (formCommandToCheck)
                {
                    case CommandName.New:
                        {
                            // vagy az iratId, vagy az iratpéldányId van megadva
                            string IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

                            string IratId = Request.QueryString.Get(QueryStringVars.IratId);
                            if (String.IsNullOrEmpty(IratId) && String.IsNullOrEmpty(IratPeldanyId))
                            {
                                // hiba
                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader,
                                   Resources.Error.UINoIratIdParam);
                                ErrorUpdatePanel1.Update();

                                return;
                            }

                            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                            #region Ha az iratpéldány id van megadva
                            if (!String.IsNullOrEmpty(IratPeldanyId))
                            {
                                //ExecParam execParam_pldGet = UI.SetExecParamDefault(Page);
                                //execParam_pldGet.Record_Id = IratPeldanyId;

                                //Result result_pldGet = service.Get(execParam_pldGet);
                                //if (result_pldGet.IsError)
                                //{
                                //    // hiba:
                                //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_pldGet);
                                //    ErrorUpdatePanel1.Update();
                                //    return;
                                //}

                                //EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)result_pldGet.Record;

                                //// irat id beállítása:
                                //IratId = iratPeldany.IraIrat_Id;

                                #region Pld_Cimzett_CsoportTextBox alapján Pld_PartnerId_Cimzett_PartnerTextBox kitöltése (csoportId --> partnerId)

                                KRT_PartnerekService service_partnerek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                                Result result_partner = service_partnerek.GetByCsoportId(UI.SetExecParamDefault(Page), Pld_Cimzett_CsoportTextBox.Id_HiddenField);
                                if (result_partner.IsError)
                                {
                                    // hiba:
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partner, Resources.Error.UICimzettPartnerGetError);
                                    ErrorUpdatePanel1.Update();
                                    return;
                                }

                                KRT_Partnerek partner_cimzett = (KRT_Partnerek)result_partner.Record;
                                if (partner_cimzett == null)
                                {
                                    // hiba:
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, Resources.Error.UICimzettPartnerGetError);
                                    ErrorUpdatePanel1.Update();
                                    return;
                                }

                                // Pld_PartnerId_Cimzett_PartnerTextBox beállítása:
                                Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = partner_cimzett.Id;
                                Pld_PartnerId_Cimzett_PartnerTextBox.Text = partner_cimzett.Nev;

                                #endregion
                            }
                            #endregion

                            ExecParam execparam = UI.SetExecParamDefault(Page);

                            EREC_PldIratPeldanyok erec_PldIratPeldanyok = GetBusinessObjectFromComponents();

                            Result result_insert = null;

                            // Ha az iratpéldány id van megadva, más webservice-t kell hívni:
                            if (!String.IsNullOrEmpty(IratPeldanyId))
                            {
                                result_insert = service.AddNewPldIratPeldanyToIraIrat_ByIratPeldany(execparam
                                    , erec_PldIratPeldanyok, IratPeldanyId);
                            }
                            else
                            {
                                // ITT NEM A SIMA INSERT VAN
                                result_insert = service.AddNewPldIratPeldanyToIraIrat(execparam
                                    , erec_PldIratPeldanyok, IratId);
                            }


                            if (result_insert.IsError)
                            {
                                // hiba
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_insert);
                                ErrorUpdatePanel1.Update();
                                voltHiba = true;
                            }

                            if (!voltHiba)
                            {
                                string newIratPeldany_Id = result_insert.Uid;

                                if (e.CommandName == CommandName.Save)
                                {
                                    //// save gombok letiltása, míg át nem irányítódik
                                    //TabFooter1.DisableSaveButtons();

                                    //// Átirányítás a módosítása oldalra
                                    //UI.popupRedirect(Page, "PldIratPeldanyokForm.aspx?"
                                    //       + QueryStringVars.Command + "=" + CommandName.Modify
                                    //       + "&" + QueryStringVars.Id + "=" + newIratPeldany_Id);

                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, newIratPeldany_Id);

                                    this.SetResultPanel(result_insert);
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, newIratPeldany_Id);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }
                                else if (e.CommandName == CommandName.SaveAndNew)
                                {
                                    //// save gombok letiltása, míg át nem irányítódik
                                    //TabFooter1.DisableSaveButtons();

                                    //// Átirányítás ugyanoda:
                                    //string oldURL = Request.Url.OriginalString;
                                    //UI.popupRedirect(Page, oldURL);
                                }
                            }

                            break;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                                voltHiba = true;
                            }
                            else
                            {
                                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                                execparam.Record_Id = ParentId;

                                EREC_PldIratPeldanyok erec_PldIratPeldanyok = GetBusinessObjectFromComponents();

                                Result result_update = service.Update(execparam, erec_PldIratPeldanyok);
                                if (!String.IsNullOrEmpty(result_update.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_update);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    //// újratöltjük a módosítás oldalt
                                    //UI.popupRedirect(Page, "PldIratPeldanyokForm.aspx?"
                                    //       + QueryStringVars.Command + "=" + CommandName.Modify
                                    //       + "&" + QueryStringVars.Id + "=" + ParentId);

                                    Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    // adatok újratöltése és ellenõrzés:
                                    RaiseEvent_OnChangedObjectProperties();
                                    ReLoadTab();

                                    return;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }

                            }
                            break;
                        }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }

    private void SetResultPanel(Result result_insert)
    {
        // Panelek láthatóságának beállítása:
        IratPanel.Visible = false;
        IratPeldanyPanel.Visible = false;
        ResultPanel.Visible = true;

        //TabFooter beállítása
        TabFooter1.ImageButton_Close.Visible = true;
        TabFooter1.ImageButton_Close.OnClientClick = "window.returnValue='0'; window.close(); return false;";
        TabFooter1.Link_SaveAndNew.Visible = true;
        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        // Rendben és új gomb linkje:
        string saveAndNewUrl = "~/PldIratPeldanyokForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New;

        // ha volt megadva IratId:
        if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratId)))
        {
            saveAndNewUrl += "&" + QueryStringVars.IratId + "=" + Request.QueryString.Get(QueryStringVars.IratId);
        }
        // ha volt megadva IratPeldanyId:
        if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratPeldanyId)))
        {
            saveAndNewUrl += "&" + QueryStringVars.IratPeldanyId + "=" + Request.QueryString.Get(QueryStringVars.IratPeldanyId);
        }

        TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

        string iratPeldanyId = result_insert.Uid;
        EREC_PldIratPeldanyok iratPeldany_Obj = (EREC_PldIratPeldanyok)result_insert.Record;

        // Iktatószám beállítása:
        if (iratPeldany_Obj != null)
        {
            labelIratPeldanyIktatoszam.Text = iratPeldany_Obj.Azonosito;
        }

        //Módosítás,megtekintés gombok beállítása:

        imgPldMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                           "&" + QueryStringVars.Id + "=" + iratPeldanyId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        imgPldModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                        "&" + QueryStringVars.Id + "=" + iratPeldanyId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

    }


    #endregion

    protected void Valasz_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string selectedRadioButton = (sender as RadioButton).ID;
            switch (selectedRadioButton)
            {
                case "elektronikus":
                    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, KodTarak.KULDEMENY_KULDES_MODJA.GetElektronikusFilterList(), false, EErrorPanel1);
                    break;
                case "papir_alapu":
                    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, KodTarak.KULDEMENY_KULDES_MODJA.GetPapirAlapuFilterList(), false, EErrorPanel1);
                    break;
                case "egyeb":
                    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, false, EErrorPanel1);
                    break;
            }
        }
        catch
        {
            // do nothing, not a radio button
        }
    }

    private void registerJavascripts()
    {
        // Validator engedélyezés/letiltás:
        string js_setValidators = @"function setValidatorsEnabledState(validatorIds, bEnabled) {
    if(!validatorIds) return;
    for (var i=0; i<validatorIds.length; i++)
    {
        var val = document.getElementById(validatorIds[i]);
        if (val) { val.enabled = bEnabled; }
    }
}";

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "setValidators", js_setValidators, true);

        //BLG 1974
        //Amennyiben a VONALKOD_KEZELES = SAVOS és a VONALKOD_KIMENO_GENERALT értéke is 1-es, akkor belsõ keletkezésû/kimenõ irat iktatása esetén nem kötelezõ a vonalkód mezõ
        ExecParam execParam_KRT_Param = UI.SetExecParamDefault(Page, new ExecParam());
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
        string vonalkodKiGeneralt = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KIMENO_GENERALT).Trim();

        //BLG 1974
        if (vonalkodkezeles.ToUpper().Trim().Equals("SAVOS") && vonalkodKiGeneralt.Trim().Equals("1") && obj_Irat != null && (obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno || obj_Irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso))
        {
            return;
        }

        // vonalkód: új példánynál kötelezõ, ha nem elektronikus az ügyintézés (mert akkor generáljuk) és nem iratpéldányból jön létre (belsõ)
        if (Barcode_VonalKodTextBox.ValidatorClientIDs != null && Barcode_VonalKodTextBox.ValidatorClientIDs.Length > 0)
        {
            string js_onchange = String.Format(@"var isEnabled = !(this.selectedIndex > -1 && this.options[this.selectedIndex].value == '{0}');
setValidatorsEnabledState(['{1}'], isEnabled);
var lbStar= $get('{2}');
if (lbStar) lbStar.style.visibility=(isEnabled ? 'visible' : 'hidden');"
                , KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
                , Barcode_VonalKodTextBox.ValidatorClientIDs[0] // formátum validálást nem bántjuk
                , labelVonalkodStar.ClientID);
            Pld_UgyintezesModja_KodtarakDropDownList.DropDownList.Attributes["onchange"] = js_onchange;
        }
    }

    //protected void elektronikus_CheckedChanged(object sender, EventArgs e)
    //{
    //    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, true, EErrorPanel1);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(13);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(11);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(10);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(8);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(7);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(6);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(5);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(4);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(3);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(2);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(1);
    //}
    //protected void papir_alapu_CheckedChanged(object sender, EventArgs e)
    //{
    //    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, true, EErrorPanel1);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(9);
    //    Pld_KuldesMod_KodtarakDropDownList.DropDownList.Items.RemoveAt(11);
    //}
    //protected void egyeb_CheckedChanged(object sender, EventArgs e)
    //{
    //    Pld_KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, true, EErrorPanel1);
    //}

    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(IratPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IratPanel.Controls));
        componentList.Add(IratPeldanyPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IratPeldanyPanel.Controls));
        return componentList;
    }

    #endregion

    private void LoadIrattariAdatok(EREC_PldIratPeldanyok erec_Peldanyok)
    {
        IrattariAdatok_EFormPanel.Visible = true;
        CalendarControlSelejtezes.Text = erec_Peldanyok.SelejtezesDat;
        FelhasznaloCsoportTextBoxSelejtezoAdo.Id_HiddenField = erec_Peldanyok.FelhCsoport_Id_Selejtezo;
        FelhasznaloCsoportTextBoxSelejtezoAdo.SetCsoportTextBoxById(EErrorPanel1);
        Leveltariatvevo_TextBox.Text = erec_Peldanyok.LeveltariAtvevoNeve;

        if (erec_Peldanyok.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Selejtezett)
        {
            labelLeveltariAtvevo.Text = Rendszerparameterek.IsTUK(Page) ? stringBizottsagiTagok : stringEllenor;
            labelLeveltarbaAdas.Text = stringSelejtezes;
            labelLeveltarbaAdo.Text = stringSelejtezo;

        }
        else
        {
            labelLeveltariAtvevo.Text = stringLeveltariAtvevo;
            labelLeveltarbaAdas.Text = stringLeveltarbaAdas;
            labelLeveltarbaAdo.Text = stringLeveltarbaAdo;
        }
    }

    EREC_IrattariHelyek GetIrattariHelyById(string irattarId)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        ExecParam.Record_Id = irattarId;
        Result res = service.Get(ExecParam);

        if (!res.IsError && res.Record != null)
        {
            EREC_IrattariHelyek hely = (EREC_IrattariHelyek)res.Record;
            return hely;
        }
        return null;
    }

    void LoadIrattariHelyek(string parnerId, string irattarId)
    {
        if (Command == CommandName.View)
        {
            if (string.IsNullOrEmpty(irattarId))
                return;

            EREC_IrattariHelyek hely = GetIrattariHelyById(irattarId);

            if (hely == null)
                Logger.Debug("Irattári hely nem található. IrattarId: " + hely.Id);

            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Clear();
            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Add(new ListItem(hely.Ertek, irattarId));
            return;
        }

        hfUgyfelelos.Value = parnerId;
        if (isTUKRendszer)
        {
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
            search_csoportok.Csoport_Id_Jogalany.Value = parnerId;
            search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

            Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

            if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
                return;

            List<string> Csoportok = new List<string>();

            foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
            {
                Csoportok.Add(row["Csoport_Id"].ToString());
            }
            Csoportok.Add(parnerId);
            EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
            search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
             "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
            var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
            IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);

            if (obj_Irat != null && obj_Irat.PostazasIranya != "1")
            {
                if (Pld_KuldesMod_KodtarakDropDownList.SelectedValue != "110")
                {
                    IrattariHelyLevelekDropDownTUK.DropDownList.Items.Add(new ListItem("Címzettnél", "4f18bb74-aec8-e911-80df-00155d017b07"));
                }
            }

            if (!String.IsNullOrEmpty(irattarId))
                IrattariHelyLevelekDropDownTUK.SetSelectedValue(irattarId);
        }

    }
}
