using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_PldIratPeldanyokTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, UI.ILovListTextBox
{
    #region public properties

    private string filterByObjektumKapcsolat = String.Empty;

    public string FilterByObjektumKapcsolat
    {
        get { return filterByObjektumKapcsolat; }
        set { filterByObjektumKapcsolat = value; }
    }

    private string objektumId = String.Empty;

    public string ObjektumId
    {
        get { return objektumId; }
        set { objektumId = value; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            Pld_TextBox.Enabled = value;
            LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
                //UI.SwapImageToDisabled(NewImageButton);
            }
        }
        get { return Pld_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            Pld_TextBox.ReadOnly = value;
            LovImageButton.Enabled = !value;
            //NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                //NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return Pld_TextBox.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { Pld_TextBox.Text = value; }
        get { return Pld_TextBox.Text; }
    }

    public TextBox TextBox
    {
        get { return Pld_TextBox; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                Pld_TextBox.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                Pld_TextBox.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                //NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }


    private bool filterForExpedialas = false;
    public bool FilterForExpedialas
    {
        get { return filterForExpedialas; }
        set { filterForExpedialas = value; }
    }

    private bool filterForPostazas = false;
    public bool FilterForPostazas
    {
        get { return filterForPostazas; }
        set { filterForPostazas = value; }
    }
    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }
    
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string filterQuery = String.Empty;
        if (!String.IsNullOrEmpty(FilterByObjektumKapcsolat) && !String.IsNullOrEmpty(objektumId))
        {
            filterQuery += "&" + QueryStringVars.ObjektumKapcsolatTipus + "=" + filterByObjektumKapcsolat;
            filterQuery += "&" + QueryStringVars.ObjektumId + "=" + ObjektumId;
        }

        string parameters = "";
        if (filterForExpedialas == true)
        {   
            string iratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

            parameters = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + Pld_TextBox.ClientID
               + "&" + QueryStringVars.Filter + "=" + Constants.Expedialas
               + "&" + QueryStringVars.IratPeldanyId + "=" + iratPeldanyId;
        } else
        if (filterForPostazas == true)
        {
            string iratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

            parameters = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + Pld_TextBox.ClientID
               + "&" + QueryStringVars.Filter + "=" + Constants.Postazas
               + "&" + QueryStringVars.IratPeldanyId + "=" + iratPeldanyId;
        }
        else
        {
            parameters = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + Pld_TextBox.ClientID;
        }

        parameters += filterQuery;
        

        OnClick_Lov = JavaScripts.SetOnClientClick("PldIratPeldanyokLovList.aspx",parameters           
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        //OnClick_New = JavaScripts.SetOnClientClick("UgyUgyiratForm.aspx"
        //    , CommandName.Command + "=" + CommandName.New
        //    + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + Pld_TextBox.ClientID
        //    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "PldIratPeldanyokForm.aspx", "", HiddenField1.ClientID, "", Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";
    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetIratPeldanyTextBoxById(errorPanel, errorUpdatePanel);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetIratPeldanyTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            search.Id.Value = Id_HiddenField;
            search.Id.Operator = Query.Operators.equals;

            Result result = service.GetAllWithExtension(execParam,search);

            ResultError.CreateNoRowResultError(result);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                DataRow iratPeldanyRow = result.Ds.Tables[0].Rows[0];

                SetIratPeldanyTextBoxByDataRow(iratPeldanyRow, errorPanel, errorUpdatePanel);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        else
        {
            Text = "";
        }
    }

    private void SetIratPeldanyTextBoxByDataRow(DataRow iratPeldanyRow, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Id_HiddenField = iratPeldanyRow["Id"].ToString();

        Text = iratPeldanyRow["IktatoSzam_Merge"].ToString();
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(Pld_TextBox);
        componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion



    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
