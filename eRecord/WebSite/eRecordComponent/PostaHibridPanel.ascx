﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PostaHibridPanel.ascx.cs" Inherits="eRecordComponent_PostaHibridPanel" %>
<%@ Register Src="~/eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>

<style type="text/css">
    .postaHibrid .mrUrlapCaption {
        white-space: normal;
        width: 170px;
        min-width: 100px;
    }
</style>

<eUI:eFormPanel ID="panelPostaHibrid" runat="server" CssClass="postaHibrid">
    <ajaxToolkit:TabContainer ID="PH_TabContainer" runat="server" Width="100%">
        <ajaxToolkit:TabPanel ID="PH_TabBoritek" runat="server">
            <HeaderTemplate>
                Boríték
            </HeaderTemplate>
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0">
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Postakönyv:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:IraIktatoKonyvekDropDownList ID="PH_Postakonyv" runat="server" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Feladó adatai:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label runat="server" ID="PH_FeladoAdatai" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Levél típusa:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_LetType" runat="server" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Boríték típusa:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_EnvType" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Boríték ablakainak száma:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="PH_EnvWindow" runat="server" CssClass="mrUrlapInputComboBox">
                                <asp:ListItem Text="0" Value="0" Selected="True" />
                                <asp:ListItem Text="1" Value="1" />
                                <asp:ListItem Text="2" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Borítékra cím:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_EnvAddressNeeded" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Logó állomány neve:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_EnvLogoFileName" runat="server" CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Címzőlap kell:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="PH_IsAddressPageIncluded" runat="server" Checked="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Hibás címzés esetén a kezelés módja:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_CleanAddress" runat="server" />
                        </td>
                        <td class="mrUrlapCaption"></td>
                        <td class="mrUrlapMezo"></td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="PH_TabTertiveveny" runat="server">
            <HeaderTemplate>
                Tértivevény
            </HeaderTemplate>
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0">
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Dokumentum típus - B/ mező 5. sor:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_DocumentType" runat="server" CssClass="mrUrlapInput" MaxLength="20" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Dokumentum csatolmány1 - B/ mező 6. sor bal oldal:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_Attachment1" runat="server" CssClass="mrUrlapInput" MaxLength="20" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Dokumentum csatolmány2 - B/ mező 6. sor jobb oldal:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_Attachment2" runat="server" CssClass="mrUrlapInput" MaxLength="20" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Tértivevény vonalkód - B/ mezőbe nyomtatandó vonalkód:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_ReturnBarcode" runat="server" CssClass="mrUrlapInput" MaxLength="50" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Átvevő távolléte esetén hagyandó értesítés típusa:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_AwayMarker" runat="server" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Speciális kezelés:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="PH_SK" runat="server" Checked="false" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="PH_Csatolmany" runat="server">
            <HeaderTemplate>
                Csatolmány
            </HeaderTemplate>
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0">
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Nyomtatási felbontás:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:DropDownList ID="PH_DPI" runat="server">
                                <asp:ListItem Text="300" Value="300" />
                                <asp:ListItem Text="600" Value="600" />
                            </asp:DropDownList>
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Kétoldalas nyomtatás:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="PH_IsDuplex" runat="server" Checked="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Papír típus :</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_PaperType" runat="server" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Papír méret:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_PaperSize" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Színes nyomtatás:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="PH_Color" runat="server" Checked="false" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Hiteles másolat készítendő:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="PH_IsAuthenticCopy" runat="server" Checked="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <span>Záradék kiegészítés:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="PH_CopyAuth" runat="server" CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <span>Borítékolási megoldás:</span>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="PH_Finishing" runat="server" checked="true" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</eUI:eFormPanel>
