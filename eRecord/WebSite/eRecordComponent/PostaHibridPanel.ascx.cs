﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eUIControls;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.IO;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using Contentum.eBusinessDocuments.PH;

public partial class eRecordComponent_PostaHibridPanel : System.Web.UI.UserControl
{
    public CssStyleCollection Style
    {
        get
        {
            return panelPostaHibrid.Style;
        }
    }

    bool IsLoaded
    {
        get
        {
            object o = ViewState["IsLoaded"];

            if (o == null)
                return false;

            return (bool)o;
        }
        set
        {
            ViewState["IsLoaded"] = value;
        }
    }

    public eErrorPanel ErrorPanel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        PH_Postakonyv.DropDownList.AutoPostBack = true;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if(Visible)
        {
            LoadComponents();
            LoadFeladoAdatai();
        }
    }

    void LoadComponents()
    {
        if (!IsLoaded)
        {
            PH_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, true, ErrorPanel);
            PH_LetType.FillAndSetSelectedValue("PHSZ_LETTYPE", "E", ErrorPanel);
            PH_EnvType.FillAndSetSelectedValue("PHSZ_ENVTYPE", "C5", ErrorPanel);
            PH_EnvAddressNeeded.FillAndSetSelectedValue("PHSZ_ENVADDR", "A", ErrorPanel);
            PH_CleanAddress.FillAndSetEmptyValue("PHSZ_CLEANADDR", ErrorPanel);
            PH_AwayMarker.FillAndSetEmptyValue("PHSZ_AWAYMARKER", ErrorPanel);
            PH_PaperType.FillAndSetSelectedValue("PHSZ_PAPERTYPE", "StandardPaper", ErrorPanel);
            PH_PaperSize.FillAndSetSelectedValue("PHSZ_PAPERSIZE", "A4", ErrorPanel);
            PH_Finishing.FillAndSetSelectedValue("PHSZ_FINISHING", "Folded", ErrorPanel);
            IsLoaded = true;
        }
    }

    void LoadFeladoAdatai()
    {
        if (!String.IsNullOrEmpty(PH_Postakonyv.SelectedValue))
        {
            try
            {
                ugyfel_adatok ugyfelAdatok = GetUgyfelAdatokFromPostakonyv(PH_Postakonyv.SelectedValue);
                if (ugyfelAdatok != null)
                {
                    PH_FeladoAdatai.Text = String.Format("{0}, {1}, {2}, {3}", ugyfelAdatok.nev, ugyfelAdatok.iranyitoszam, ugyfelAdatok.helyseg, ugyfelAdatok.utca);
                }
                else
                {
                    PH_FeladoAdatai.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Result error = Contentum.eUtility.ResultException.GetResultFromException(ex);
                ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel,error);
            }

        }
        else
        {
            PH_FeladoAdatai.Text = String.Empty;
        }
    }

    ugyfel_adatok GetUgyfelAdatokFromPostakonyv(string postakonyvId)
    {
        EREC_IraIktatoKonyvekService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = postakonyvId;
        Result result_get = service.Get(execParam);

        if (result_get.IsError)
        {
            throw new Contentum.eUtility.ResultException(result_get);
        }

        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_get.Record;
        string ugyfelAdatokXML = erec_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok;

        if (String.IsNullOrEmpty(ugyfelAdatokXML))
            return null;

        ugyfel_adatok ugyfelAdatok = null;
        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(ugyfel_adatok));

        using (TextReader reader = new StringReader(ugyfelAdatokXML))
        {
            ugyfelAdatok = serializer.Deserialize(reader) as ugyfel_adatok;
        }

        return ugyfelAdatok;
    }

    public deliveryInstructions GetKezbesitesiUtasitas(EREC_PldIratPeldanyok peldany, string[] dokumentumId_Array)
    {
        //bemenő paraméterek
        string postakonyvId = PH_Postakonyv.SelectedValue;

        if (String.IsNullOrEmpty(postakonyvId))
        {
            throw new Exception("Nincs megadva postakönyv!");
        }

        ugyfel_adatok ugyfelAdatok = GetUgyfelAdatokFromPostakonyv(postakonyvId);

        if (ugyfelAdatok == null)
        {
            throw new Exception("A postakönyvhöz nincsnek megadva ügyféladatok!");
        }

        deliveryInstructions instructions = new deliveryInstructions();
        instructions.Ver = 1.0m;
        deliveryInstructionsConsignment cons = instructions.consignment = new deliveryInstructionsConsignment();
        //Expediált iratpéldány iktatószáma
        cons.RequestId = "";
        cons.Contract = Rendszerparameterek.GetInt(Page, "PHSZ_ContractNumber", 0);
        //kiválasztott csatolmányok darabszáma
        cons.Annexes = (byte)dokumentumId_Array.Length;
        cons.LetType = PH_LetType.SelectedValue;
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA) == "A")
        {
            cons.RegCode = GetPostaHibridRagszam();
        }
        else
        {
            cons.RegCode = String.Empty;
        }
        cons.EnvType = PH_EnvType.SelectedValue;
        cons.EnvWindow = PH_EnvWindow.SelectedValue;
        cons.EnvAddressNeeded = PH_EnvAddressNeeded.SelectedValue;
        cons.EnvLogoFileName = PH_EnvLogoFileName.Text;
        cons.IsAddressPageIncluded = GetPHBoolean(PH_IsAddressPageIncluded.Checked);

        //sender
        deliveryInstructionsConsignmentSender sender = cons.sender = new deliveryInstructionsConsignmentSender();
        sender.RentPersName = new deliveryInstructionsConsignmentSenderRentPersName { content = ugyfelAdatok.nev };
        sender.RentCity = new deliveryInstructionsConsignmentSenderRentCity { content = ugyfelAdatok.helyseg };
        sender.RentAddress = new deliveryInstructionsConsignmentSenderRentAddress { content = ugyfelAdatok.utca };
        sender.RentZIP = new deliveryInstructionsConsignmentSenderRentZIP { content = ugyfelAdatok.iranyitoszam };
        sender.RentBarcode = new deliveryInstructionsConsignmentSenderRentBarcode();
        //return
        deliveryInstructionsConsignmentReturn ret = cons.@return = new deliveryInstructionsConsignmentReturn();
        ret.RetAddressName1 = new deliveryInstructionsConsignmentReturnRetAddressName1 { content = ugyfelAdatok.nev };
        ret.RetAddressName2 = new deliveryInstructionsConsignmentReturnRetAddressName2();
        ret.RetAddressCity = new deliveryInstructionsConsignmentReturnRetAddressCity { content = ugyfelAdatok.helyseg };
        ret.RetAddressStreet = new deliveryInstructionsConsignmentReturnRetAddressStreet { content = ugyfelAdatok.utca };
        ret.RetBuildingIntAddress = new deliveryInstructionsConsignmentReturnRetBuildingIntAddress();
        ret.RetAddressZIP = new deliveryInstructionsConsignmentReturnRetAddressZIP { content = ugyfelAdatok.iranyitoszam };
        ret.RetAddressCountry = new deliveryInstructionsConsignmentReturnRetAddressCountry();
        ret.RetBarcode = new deliveryInstructionsConsignmentReturnRetBarcode();

        cons.NumberOfRecipients = 1;
        cons.CleanAddress = PH_CleanAddress.SelectedValue;
        //hivatali kapu
        cons.DeliveryChannel = 5;
        cons.DocumentType = PH_DocumentType.Text;
        cons.Attachment1 = PH_Attachment1.Text;
        cons.Attachment2 = PH_Attachment2.Text;
        cons.ReturnBarcode = PH_ReturnBarcode.Text;
        cons.AwayMarker = PH_AwayMarker.SelectedValue;

        //Képezzük minden egyes csatolmány (bele nem értve természetesen a kézbesítési utasítást magát) lenyomatát (SHA256 függvénnyel); A lenyomatokat a kézbesítési utasításban meghatározott sorrendben közvetlenül (minden közbenső karakter nélkül) egymáshoz láncoljuk. Az így készített karakterláncon ismételten alkalmazzuk az SHA256 függvényt és ez kerül ebbe a mezőbe base64 kódolással A kézbesítési utasítás mellett csak egy csatolmányt tartalmazó küldemény esetében megengedett a csatolmány hash-ének (RecordHash) megismétlése.
        cons.ConsignmentHash = "";
        //KuldemenyGUID
        cons.ConsignmentGuid = "";
        cons.EReturnReceipt = "Y";
        cons.SK = PH_SK.Checked ? "CK" : String.Empty;
        cons.ReturnEnvelope = "N";

        deliveryInstructionsConsignmentRecipient recipient = new deliveryInstructionsConsignmentRecipient();
        cons.recipients = new deliveryInstructionsConsignmentRecipient[] { recipient };
        recipient.Name1 = new deliveryInstructionsConsignmentRecipientName1 { content = peldany.NevSTR_Cimzett };
        recipient.Name2 = new deliveryInstructionsConsignmentRecipientName2();

        if (!String.IsNullOrEmpty(peldany.Cim_id_Cimzett))
        {
            KRT_Cimek cim = GetCim(peldany.Cim_id_Cimzett);
            recipient.City = new deliveryInstructionsConsignmentRecipientCity { content = cim.TelepulesNev };
            recipient.Street = new deliveryInstructionsConsignmentRecipientStreet { content = String.Format("{0} {1} {2}", cim.KozteruletNev, cim.KozteruletTipusNev, cim.Hazszam) };
            recipient.BuildingIntAddress = new deliveryInstructionsConsignmentRecipientBuildingIntAddress { content = String.Format("{0} {1} {2}", cim.Lepcsohaz, cim.Szint, cim.AjtoBetujel).Trim() };
            recipient.ZIP = new deliveryInstructionsConsignmentRecipientZIP { content = cim.IRSZ };
            recipient.Country = new deliveryInstructionsConsignmentRecipientCountry();
            recipient.Barcode = new deliveryInstructionsConsignmentRecipientBarcode();
        }
        else
        {
            throw new Exception("A postázási cím nem struktúrált!");
        }

        string PHSZ_ENABLED_FILETYPES = Rendszerparameterek.Get(Page, "PHSZ_ENABLED_FILETYPES");
        List<deliveryInstructionsConsignmentAttachment> attachmentList = new List<deliveryInstructionsConsignmentAttachment>();
        Result dokResult = GetDokumentumok(dokumentumId_Array);

        if (!dokResult.IsError)
        {
            byte sorrend = 1;
            foreach (DataRow row in dokResult.Ds.Tables[0].Rows)
            {
                string elektronikusAlairas = row["ElektronikusAlairas"].ToString();
                string formatum = row["Formatum"].ToString();

                if (!IsEnabledFormatum(PHSZ_ENABLED_FILETYPES, formatum))
                {
                    throw new Exception(String.Format("Csak a következő formátumok küldhetőek ki posta hibrid szolgáltatáson keresztül: {0}!", PHSZ_ENABLED_FILETYPES));
                }

                deliveryInstructionsConsignmentAttachment attachment = new deliveryInstructionsConsignmentAttachment();
                attachment.FileName = row["FajlNev"].ToString();
                attachment.FileType = formatum;
                attachment.DPI = PH_DPI.SelectedValue;
                //A dokumentum SHA2 lenyomata(hash, base64 kódolással)
                attachment.RecordHash = "";
                attachment.RecordAlgorithm = "";
                attachment.PDFSumpage = 99;
                attachment.IsDuplex = GetPHBoolean(PH_IsDuplex.Checked);
                attachment.PaperType = PH_PaperType.SelectedValue;
                attachment.PaperSize = PH_PaperSize.SelectedValue;
                attachment.Color = PH_Color.Checked ? "FC" : "BW";
                attachment.AttachmentGuid = row["Id"].ToString();
                //ha alá van írva a csatolmány, és személyes aláírással írták alá akkor 1, ha szervezetivel, akkor 2, egyébként 0
                if (String.IsNullOrEmpty(elektronikusAlairas) || elektronikusAlairas == "0")
                {
                    attachment.SignatureType = 0;
                }
                else if (elektronikusAlairas == "4")
                {
                    attachment.SignatureType = 1;
                }
                else
                {
                    attachment.SignatureType = 2;
                }
                attachment.IsAuthenticCopy = GetPHBoolean(PH_IsAuthenticCopy.Checked);
                attachment.CopyAuth = PH_CopyAuth.Text;
                //ha az irat kiadmányozandó, akkor 1, egyébként 0
                attachment.IsIssued = "N";
                //az iratot kiadmányozó személy neve
                attachment.SigRentPersName = "";
                //az iratot kiadmányozó felhasználó szervezeti egysége
                attachment.SigRentOrgName = "";
                attachment.Prog = sorrend;
                attachment.Finishing = PH_Finishing.SelectedValue;
                attachmentList.Add(attachment);
                sorrend++;
            }
        }
        else
        {
            throw new Exception("A dokumentumok lekérése sikertelen: " + dokResult.ErrorMessage);
        }

        cons.attachmentList = attachmentList.ToArray();

        return instructions;
    }

    string GetPostaHibridRagszam()
    {
        string postakonyvId = PH_Postakonyv.SelectedValue;

        if (!String.IsNullOrEmpty(postakonyvId))
        {
            string savId = GetRagSzamSavId(postakonyvId);

            if (!String.IsNullOrEmpty(savId))
            {
                KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);

                Result result = service.RagszamSav_Foglalas(
                    execParam,
                    new Guid(savId),
                    new Guid(postakonyvId),
                    1,
                    "H"
                    );

                if (!result.IsError)
                {
                    return FindRagszamById(execParam, result.Uid);
                }
            }
        }

        return String.Empty;
    }

    string GetRagSzamSavId(string postakonyvId)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Postakonyv_Id.Value = postakonyvId;
        search.Postakonyv_Id.Operator = Query.Operators.equals;

        search.SavAllapot.Value = "A";
        search.SavAllapot.Operator = Query.Operators.equals;

        Result res = service.GetAll(execParam, search);

        if (!res.IsError)
        {
            if (res.Ds.Tables[0].Rows.Count > 0)
                return res.Ds.Tables[0].Rows[0]["Id"].ToString();
        }

        return String.Empty;
    }

    private string FindRagszamById(ExecParam execParam, string id)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execPm = execParam.Clone();
        execPm.Record_Id = id;
        Result res = service.Get(execPm);

        if (!res.IsError)
        {
            return (res.Record as KRT_RagSzamok).Kod;
        }

        return String.Empty;
    }

    string GetPHBoolean(bool value)
    {
        return value ? "Y" : "N";
    }

    KRT_Cimek GetCim(string cimId)
    {
        Contentum.eAdmin.Service.KRT_CimekService cimekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();
        ExecParam xpm = UI.SetExecParamDefault(Page);
        xpm.Record_Id = cimId;

        Result res = cimekService.Get(xpm);

        if (!res.IsError)
        {
            return res.Record as KRT_Cimek;
        }

        return null;
    }

    Result GetDokumentumok(string[] dokumentum_Id_Array)
    {
        Contentum.eAdmin.Service.KRT_DokumentumokService dokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_DokumentumokService();
        KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        dokSearch.Id.Value = Search.GetSqlInnerString(dokumentum_Id_Array);
        dokSearch.Id.Operator = Query.Operators.inner;

        Result result = dokService.GetAll(execParam, dokSearch);
        return result;
    }

    bool IsEnabledFormatum(string enabledFileTypes, string formatum)
    {
        if (String.IsNullOrEmpty(enabledFileTypes) || String.IsNullOrEmpty(formatum))
            return true;

        string[] fileTypes = enabledFileTypes.Split(',');

        foreach (string fileType in fileTypes)
        {
            string _fileType = fileType;
            if (fileType.StartsWith("."))
            {
                _fileType = _fileType.Substring(1);
            }

            if (_fileType.Equals(formatum, StringComparison.InvariantCultureIgnoreCase))
                return true;
        }

        return false;
    }
}