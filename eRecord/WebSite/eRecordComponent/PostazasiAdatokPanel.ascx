﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PostazasiAdatokPanel.ascx.cs"
    Inherits="eRecordComponent_PostazasiAdatokPanel" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc1" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc5" %>
<%@ Register Src="RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc6" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc9" %>
<%@ Register Src="RagszamListBox.ascx" TagName="RagszamListBox" TagPrefix="rsztb" %>
<%@ Register Src="~/Component/FullCimField.ascx" TagName="FullCimField" TagPrefix="uc" %>
<%@ Register Src="PldIratPeldanyokTextBox.ascx" TagName="PldIratPeldanyokTextBox"
    TagPrefix="uc10" %>
<%@ Register src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" tagname="EditablePartnerTextBoxWithTypeFilter" tagprefix="EPARTTBWF" %>
<%@ Register Src="~/eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagPrefix="EPARTTBWF" TagName="ObjektumTargyszavaiPanel" %>

<EPARTTBWF:ObjektumTargyszavaiPanel runat="server" ID="ObjektumTargyszavaiPanel" />
<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
    <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    <table cellpadding="0" cellspacing="0" runat="server" id="MainTable" width="100%">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr class="urlapSor">
            <td class="mrUrlapCaption">
                <asp:Label ID="Label21" runat="server" Text="Küldemény száma:"></asp:Label>
            </td>
            <td class="mrUrlapMezo">
                <asp:TextBox ID="KimenoKuldSorszam_TextBox" runat="server" CssClass="mrUrlapInput"
                    ReadOnly="True"></asp:TextBox>
            </td>
        </tr>--%>
                    <tr class="urlapSor" runat="server" id="tr_radiobuttonlist" visible="false">
                        <td class="mrUrlapCaption">&nbsp;
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" CausesValidation="false"
                                RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="0">Ismert címzett</asp:ListItem>
                                <asp:ListItem Value="1">Tömeges kiküldés</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbPostakonyvMegorzes" runat="server" TabIndex="-1" ToolTip="Postakönyv megőrzése"
                                Visible="false" />
                            <asp:Label ID="Label12" runat="server" Text="Postakönyv:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" width="0%" colspan="2">
                            <uc9:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList_Postakonyv" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbBelyegzoDatumaMegorzes" runat="server" TabIndex="-1" ToolTip="Feladás dátuma megőrzése"
                                Visible="false" />
                            <asp:Label ID="Label2" runat="server" Text="Feladás dátuma:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc1:CalendarControl ID="BelyegzoDatuma_CalendarControl" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_IratPldTreeView" runat="server">
                         <td class="mrUrlapCaption"></td>
                        <td class="mrUrlapMezo" colspan="2">
                            <table width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td >
                                            <asp:Panel ID="PanelGombok" runat="server" Visible="true">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg" ValidationGroup="IratPeldanyAdd" /></td>
                                                        <td style="text-align: center; padding-left: 5px;">
                                                            <asp:ImageButton ID="Torles" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg" OnClick="Torles_Click" ValidationGroup="IratPeldanyAdd" /></td>
                                                        <td style="width: 100%"></td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor"  runat="server">
                                        <td >
                                            <eUI:eFormPanel ID="TreeView_EFormPanel" runat="server" >                                              
                                                <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="true"
                                                    BackColor="White" ShowLines="True" PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip="" SkipLinkText="" NodeIndent="40" NodeWrap="True">
                                                    <SelectedNodeStyle Font-Bold="False" CssClass="tvSelectedRowStyle" />
                                                </asp:TreeView>
                                            </eUI:eFormPanel>
                                        </td>
                                        <%--<td style="width: 50%;"></td>--%>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_IratPeldany" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" Text="Iratpéldány kiválasztás:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" width="0%">
                           <uc10:PldIratPeldanyokTextBox ID="PldIratPeldanyokTextBox1" runat="server" Validate="false" FilterForPostazas="true" />
                        </td>
                        <td class="mrUrlapMezo" style="padding-left: 10px; padding-top: 5px;">
                            <asp:ImageButton ID="Hozzaadas" runat="server" ImageUrl="~/images/hu/trapezgomb/hozzaad_trap.jpg" OnClick="Hozzaadas_Click" ValidationGroup="IratPeldanyAdd" />
                        </td>
                    </tr>

                    <tr class="urlapSor" id="tr_Cimzett" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbCimzettMegorzes" runat="server" TabIndex="-1" ToolTip="Címzett és cím megőrzése"
                                Visible="false" />
                            <asp:Label ID="Label_Req_cimzett" runat="server" Text="*" CssClass="ReqStar" Visible="false"></asp:Label>
                            <asp:Label ID="Label22" runat="server" Text="Címzett:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="Cimzett_PartnerTextBox" runat="server" 
                                CreateToolTipOnItems="true" CssClass="mrUrlapInputSzeles" Validate="False"/>
                           <%-- <uc2:PartnerTextBox ID="Cimzett_PartnerTextBox" runat="server" CreateToolTipOnItems="true"
                                CssClass="mrUrlapInputSzeles" Validate="False" />--%>
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_CimzettCime" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label_Req_cimzettCime" runat="server" Text="*" CssClass="ReqStar"
                                Visible="false"></asp:Label>
                            <asp:Label ID="Label23" runat="server" Text="Címzett címe:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor" runat="server" id="tr_FullCimField" visible="false">
                                    <td class="mrUrlapMezo">
                                        <uc:FullCimField ID="CimzettCime_FullCimField" runat="server" Validate="false" IsOrszagVisible="true"
                                            Visible="false" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapMezo">
                                        <uc3:CimekTextBox ID="CimzettCime_CimekTextBox" runat="server" CssClass="mrUrlapInputSzeles"
                                            ModifyVisible="true" Validate="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbKikuldoMegorzes" runat="server" TabIndex="-1" ToolTip="Kiküldő megőrzése"
                                Visible="false" />
                            <asp:Label ID="labelKikuldoStar" runat="server" Text="*" CssClass="ReqStar" Visible="false"></asp:Label>
                            <asp:Label ID="labelKikuldo" runat="server" Text="Kiküldő:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc5:CsoportTextBox ID="Kikuldo_CsoportTextBox" SzervezetCsoport="true" Validate="false"
                                runat="server" LabelRequiredIndicatorID="labelKikuldoStar" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_Vonalkod" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label28" runat="server" Text="Vonalkód:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc5:VonalKodTextBox ID="VonalKodTextBox1" runat="server" RequiredValidate="false" />
                            <asp:CheckBox ID="cbAblakosBoritek" runat="server" Text="Ablakos boríték" Checked="false" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_Ragszam" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelRagszam" runat="server" Text="Postai azonosító:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc5:RagszamTextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" />
                            <%-- validáláshoz szükséges --%>
                            <asp:HiddenField ID="KuldemenyId_HiddenField" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_TertivevenyVonalkod" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelTertivevenyVonalkod" runat="server" Text="Tértivevény vonalkód:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc5:VonalKodTextBox ID="Tertiveveny_VonalkodTextBox" runat="server" Mode="Vonalkod"
                                RequiredValidate="false" CssClass="mrUrlapInput" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="trViszonylatKod">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelViszonylatKod" runat="server" Text="Viszonylat:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <asp:RadioButton runat="server" ID="rbBelfoldi" Text="Belföldi" AutoPostBack="true" GroupName="OrszagViszonylatKod" Checked="true" />
                            <asp:RadioButton runat="server" ID="rbEuropai" Text="Európai" AutoPostBack="true" GroupName="OrszagViszonylatKod" />
                            <asp:RadioButton runat="server" ID="rbEgyebKulfoldi" Text="Egyéb külföldi" AutoPostBack="true" GroupName="OrszagViszonylatKod" />
                             
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_KuldesMod">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelKuldesMod" runat="server" Text="Küldésmód:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" style="padding-right: 15px;">
                            <uc4:KodtarakDropDownList ID="KuldesMod_KodtarakDropDownList" runat="server" ReadOnly="true" />
                        </td>
                        <td class="mrUrlapCaption_short">&nbsp;
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_Jegyzeksz">
                        <td class="mrUrlapCaption">
                            <%--BLG_1721--%>
                            <asp:Label ID="labelJegyzeksz" runat="server" Text="Futárjegyzék lista száma:"></asp:Label>&nbsp;
                        </td>
                        <%--BLG_1721--%>
                         <td class="mrUrlapMezo" style="padding-right: 15px;">
                             <asp:TextBox ID="Jegyzeksz_TextBox" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <%--BLG_1721--%>
                      <tr class="urlapSor" runat="server" id="tr_Atveteldatum">
                        <td class="mrUrlapCaption">        
                            <asp:Label ID="labelAtvetelDatum" runat="server" Text="Átvétel dátuma:"></asp:Label>&nbsp;
                        </td>
                         <td class="mrUrlapMezo" style="padding-right: 15px;">
                             <uc1:CalendarControl ID="Atvetel_CalendarControl" runat="server" />
                        </td>
                    </tr>
                       <tr class="urlapSor" runat="server" id="tr_Atvevo">
                        <td class="mrUrlapCaption">        
                            <asp:Label ID="labelAtvevo" runat="server" Text="Átvevő neve:"></asp:Label>&nbsp;
                        </td>
                         <td class="mrUrlapMezo" style="padding-right: 15px;">
                              <asp:TextBox ID="Atvevo_TextBox" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_kuldemenyfajtaja">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbKuldemenyFajtajaMegorzes" runat="server" TabIndex="-1" ToolTip="Küldemény fajtájának megőrzése"
                                Visible="false" />
                            <asp:Label ID="Label26" runat="server" Text="Küldemény fajtája:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc4:KodtarakDropDownList ID="KimenoKuldFajta_KodtarakDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="KimenoKuldFajta_KodtarakDropDownList_SelectedIndexChanged" />
                            <asp:CheckBox ID="Elsobbsegi_CheckBox" runat="server" Text="Elsőbbségi" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_szolgaltatasok1">
                        <td class="mrUrlapCaption" rowspan="3">
                            <asp:Label ID="labelSzolgaltatasok" runat="server" Text="Szolgáltatások:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <asp:CheckBox ID="Ajanlott_CheckBox" runat="server" Text="Ajánlott" />
                            <asp:CheckBox ID="Tertiveveny_CheckBox" runat="server" Text="Tértivevény" />
                            <asp:CheckBox ID="SajatKezbe_CheckBox" runat="server" Text="Saját kézbe" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_szolgaltatasok2">
                        <td class="mrUrlapMezo" colspan="3">
                            <asp:CheckBox ID="E_Ertesites_CheckBox" runat="server" Text="E-értesítés" />
                            <asp:CheckBox ID="E_Elorejelzes_CheckBox" runat="server" Text="E-előrejelzés" />
                            <asp:CheckBox ID="PostaiLezaroSzolgalat_CheckBox" runat="server" Text="Postai lezáró szolgálat" />
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_erteknyilvanitas">
                        <td class="mrUrlapMezo" valign="top" style="padding-top: 4px">
                            <asp:CheckBox ID="Erteknyilvanitas_CheckBox" runat="server" Text="Értéknyilvánítás" />
                            <asp:Label ID="Erteknyilvanitas_Label" runat="server" Text=" összege:"></asp:Label>
                            <asp:TextBox ID="Erteknyilvanitas_TextBox" runat="server"></asp:TextBox>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_ar">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label27" runat="server" Text="Ár:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <asp:TextBox ID="Ar_TextBox" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor" runat="server" id="tr_mennyiseg" visible="false">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label29" runat="server" Text="Mennyiség:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc6:RequiredNumberBox ID="Peldanyszam_RequiredNumberBox" runat="server" Text="1"
                                Validate="False" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:CheckBox ID="cbMegjegyzesMegorzes" runat="server" TabIndex="-1" ToolTip="Megjegyzés megőrzése"
                                Visible="false" />
                            <asp:Label ID="Label30" runat="server" Text="Megjegyzés:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="95%">
                    <tr id="tr_VonalkodRagszam" runat="server" visible="false" style="padding-top: 10px;">
                        <td>
                            <hr />
                            <rsztb:RagszamListBox ID="RagszamListBox1" runat="server" GridViewPanelHeigth="100px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
