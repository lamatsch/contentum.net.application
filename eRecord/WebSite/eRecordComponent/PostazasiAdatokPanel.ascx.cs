﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;


// nekrisz CR 2960
// Postázáskor Iratpéldányok kiválasztása, hozzárendelése a küldeményekhez 
// Iratpéldányokat Expediálja és Postázza, Küldeményt létrehozza (az expediált iratpéldányokkal) és postázza 
public partial class eRecordComponent_PostazasiAdatokPanel : System.Web.UI.UserControl
{

    private const string kcs_KIMENO_KULDEMENY_FAJTA = "KIMENO_KULDEMENY_FAJTA";
    private const string kcs_KIMENO_KULDEMENY_AR_ELSOBBSEGI = "KIMENO_KULDEMENY_AR_ELSOBBSEGI";
    private const string kcs_KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI = "KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI";
    private const string kcs_KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS = "KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";

    // BLG_1721
    private bool isTUK = false;

    // BLG_591
    private bool isTertivonalkod = false;

    #region Public Properties

    public List<string> HivatalosIratok = new List<string>()
    {
        "08",
        "09",
        "10",
        "11",
        "19",
        "20"
    };

    // BUG_4647
    //Nem kell megjeleníteni ezeket a küldésmódokat
    public List<string> Postai_Spec_Alapjan = new List<string>()
    {
        "40", //Postai - Ajánlott küldemény
        "41", //Postai - Értéknyilvánításos levélküldemény
        "42", //Postai - Hivatalos irat
        "43", //Postai - Értéknyilvánításos hivatalos irat
        "44"  //Postai - „Címzett kezébe” levél
    };

    public enum Mode { Simple, Multiple }

    public enum KuldesModType { Sima, Ajanlott, Tertivevenyes, HivatalosIrat }

    public Mode RogzitesTipus
    {
        get
        {
            if (ViewState["RogzitesTipus"] == null)
            {
                return Mode.Simple;
            }
            else
            {
                return (Mode)ViewState["RogzitesTipus"];
            }
        }

        set { ViewState["RogzitesTipus"] = value; }

    }

    public KuldesModType KuldesMod
    {
        get
        {
            if (ViewState["KuldesMod"] == null)
            {
                return KuldesModType.Sima;
            }
            else
            {
                return (KuldesModType)ViewState["KuldesMod"];
            }
        }

        set { ViewState["KuldesMod"] = value; }

    }

    public string[] VonalkodArray
    {
        get { return RagszamListBox1.VonalkodList.ToArray(); }
    }
    public string[] RagszamArray
    {
        get
        {
            if (this.KuldesMod != KuldesModType.Sima)
            {
                return new List<string>(RagszamListBox1.VonalkodRagszamDictionary.Values).ToArray();
            }
            else
            {
                return null;
            }
        }
    }
    public string[] TertivevenyVonalkodArray
    {
        get
        {
            if (this.KuldesMod == KuldesModType.Tertivevenyes || this.KuldesMod == KuldesModType.HivatalosIrat)
            {
                return new List<string>(RagszamListBox1.VonalkodTertivevenyVonalkodDictionary.Values).ToArray();
            }
            else
            {
                return null;
            }
        }
    }
    public string TertivevenyVonalkod
    {
        get { return Tertiveveny_VonalkodTextBox.Text; }
    }

    public string KuldemenyVonalkod
    {
        get { return VonalKodTextBox1.Text; }
    }

    public DropDownList PostaKonyvekDropDownList
    {
        get { return IraIktatoKonyvekDropDownList_Postakonyv.DropDownList; }
    }

    public string IratPeldanyTextBoxClientID
    {
        get { return PldIratPeldanyokTextBox1.HiddenField.ClientID; }
    }

    //LZS - BUG_7102
    public bool PostazasModTomeges
    {
        get { return Page.AppRelativeVirtualPath.Contains("PostazasTomegesForm.aspx"); }
    }

    public bool IsTomegesKikuldes
    {
        get { return RadioButtonList1.SelectedValue == "1"; }
    }

    #endregion

    #region KuldemenyFajtaja checkboxes
    protected Dictionary<string, CheckBox> GetCheckBoxAndSessionNameDictionary_KuldemenyFajtaja()
    {
        Dictionary<string, CheckBox> CheckBoxAndSessionNameDictionary = new Dictionary<string, CheckBox>();

        CheckBoxAndSessionNameDictionary.Add("Elsobbsegi", Elsobbsegi_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("Ajanlott", Ajanlott_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("Tertiveveny", Tertiveveny_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("SajatKezbe", SajatKezbe_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("E_Ertesites", E_Ertesites_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("E_Elorejelzes", E_Elorejelzes_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("PostaiLezaroSzolgalat", PostaiLezaroSzolgalat_CheckBox);
        CheckBoxAndSessionNameDictionary.Add("Erteknyilvanitas", Erteknyilvanitas_CheckBox);

        return CheckBoxAndSessionNameDictionary;
    }

    protected string GetCheckBoxesSerializedForSession_KuldemenyFajtaja()
    {
        Dictionary<string, CheckBox> CheckBoxAndSessionNamePairs = GetCheckBoxAndSessionNameDictionary_KuldemenyFajtaja();

        string serialized = "";

        if (CheckBoxAndSessionNamePairs.Count > 0)
        {
            List<string> checkboxValueList = new List<string>();
            foreach (string key in CheckBoxAndSessionNamePairs.Keys)
            {
                CheckBox cb = null;
                if (CheckBoxAndSessionNamePairs.TryGetValue(key, out cb))
                {
                    if (cb != null)
                    {
                        checkboxValueList.Add(key + "=" + (cb.Checked ? "1" : "0"));
                    }
                }
            }
            serialized = String.Join("|", checkboxValueList.ToArray());
        }
        return serialized;
    }

    protected void SetCheckBoxesFromSerializedSessionString_KuldemenyFajtaja(string serialized)
    {
        if (String.IsNullOrEmpty(serialized)) return;

        Dictionary<string, CheckBox> CheckBoxAndSessionNamePairs = GetCheckBoxAndSessionNameDictionary_KuldemenyFajtaja();
        if (CheckBoxAndSessionNamePairs.Count == 0) return;

        char[] separatorPairs = new char[] { '|' };
        char[] separatorNameValue = new char[] { '=' };
        string[] nameValueArray = serialized.Split(separatorPairs, StringSplitOptions.RemoveEmptyEntries);

        if (nameValueArray == null || nameValueArray.Length == 0) return;

        foreach (string item in nameValueArray)
        {
            string[] name_value = item.Split(separatorNameValue, StringSplitOptions.RemoveEmptyEntries);
            if (name_value.Length > 1)
            {
                string name = name_value[0];
                bool value = (name_value[1] == "1" ? true : false);
                CheckBox cb = null;
                if (CheckBoxAndSessionNamePairs.TryGetValue(name, out cb))
                {
                    cb.Checked = value;
                }
            }
        }
    }

    #endregion KuldemenyFajtajaCheckboxes



    //CR 3048: needNewPld = ha kell új iratpéldányt létrehozni
    private string JoinTreeNodePldValue(string Pld_Id, string Pld_Vonalkod, string needNewPld)
    {
        return String.Join(";", new string[] { Pld_Id, Pld_Vonalkod, needNewPld });
    }

    private string[] SplitTreeNodePldValue(TreeNode tnPld)
    {
        return tnPld.Value.Split(new char[] { ';' });
    }

    private string GetTreeNodePldId(TreeNode tnPld)
    {
        string[] items = SplitTreeNodePldValue(tnPld);
        return items[0];
    }

    private string GetTreeNodePldVonalkod(TreeNode tnPld)
    {
        string[] items = SplitTreeNodePldValue(tnPld);
        return items[1];
    }

    // CR 3048: needNewPld érték kiszedése a treeviewNode-ból
    private string GetTreeNodePldNeedNewPld(TreeNode tnPld)
    {
        string[] items = SplitTreeNodePldValue(tnPld);
        return items[2];
    }
    //private string IratPeldany_Id = "";
    private string Kuldemeny_Id = "";
    private string Command = "";
    private string EditMode = "";
    private string KuldemenyId = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        // BLG_1721
        isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);

        // Command == Postazas vagy KimenoKuldemenyFelvitel
        Command = Request.QueryString.Get(CommandName.Command);
        KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);

        EditMode = Request.QueryString.Get(QueryStringVars.Mode); // Sima, Ajanlott, Tertivevenyes, HivatalosIrat
        // BLG_591
        isTertivonalkod = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TERTIVONALKOD_KELL, false);

        //Cim összekötése
        CimzettCime_FullCimField.CimTextBox = CimzettCime_CimekTextBox.TextBox;
        CimzettCime_FullCimField.CimHiddenField = CimzettCime_CimekTextBox.HiddenField;

        CimzettCime_CimekTextBox.TryFireChangeEvent = true;
        CimzettCime_CimekTextBox.AutoPostBack = true;

        Cimzett_PartnerTextBox.TryFireChangeEvent = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Címzett partner és cim összekötése
        Cimzett_PartnerTextBox.CimTextBox = CimzettCime_CimekTextBox.TextBox;
        Cimzett_PartnerTextBox.CimHiddenField = CimzettCime_CimekTextBox.HiddenField;


        rbBelfoldi.CheckedChanged += new EventHandler(cbEuropai_CheckedChanged);
        rbEuropai.CheckedChanged += new EventHandler(cbEuropai_CheckedChanged);
        rbEgyebKulfoldi.CheckedChanged += new EventHandler(cbEuropai_CheckedChanged);

        //LZS - BUG_5340 - Tömeges iktatásnál nem kell a change esemény a címzettekhez.
        if (RadioButtonList1.SelectedValue == "0")
        {
            CimzettCime_FullCimField.OrszagTextChanged += new EventHandler(CimzettCime_FullCimField_OrszagTextChanged);
            CimzettCime_CimekTextBox.TextChanged += new EventHandler(CimzettCime_CimekTextBox_TextChanged);
        }
        else
        {
            CimzettCime_FullCimField.OrszagTextChanged -= new EventHandler(CimzettCime_FullCimField_OrszagTextChanged);
            CimzettCime_CimekTextBox.TextChanged -= new EventHandler(CimzettCime_CimekTextBox_TextChanged);
        }

        Ragszam_TextBox.OnServerValidate += new ServerValidateEventHandler(CheckRagszam);

        string js_buttonDisabled = JavaScripts.SetDisableButtonOnClientClick(Page, Hozzaadas);

        Hozzaadas.OnClientClick = "if ($get('" + PldIratPeldanyokTextBox1.HiddenField.ClientID + "').value == '') "
                + " { alert('" + Resources.Error.UINoSelectedItem + "'); return false; } "
                + js_buttonDisabled;

        cbAblakosBoritek.CheckedChanged += new EventHandler(cbAblakosBoritek_CheckedChanged);

        // BLG_1721
        KuldesMod_KodtarakDropDownList.AutoPostBack = true;
        KuldesMod_KodtarakDropDownList.DropDownList.SelectedIndexChanged += KuldesMod_KodtarakDropDownList_SelectedIndexChanged;
        SetKezbesitesVisibility();
    }

    #region BLG_1721

    private void KuldesMod_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetKezbesitesVisibility();
    }

    /// <summary>
	/// Beallitja a futarjegyzek parametereinek lathatosagat
	/// </summary>
    private void SetKezbesitesVisibility()
    {
        //bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            if (KuldesMod_KodtarakDropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Futarszolgalat)
            {
                //cbRagszamMegorzes.Visible = false;
                tr_Ragszam.Visible = false;
                labelRagszam.Visible = false;
                Ragszam_TextBox.Visible = false;

                tr_Jegyzeksz.Visible = true;
                labelJegyzeksz.Visible = true;
                labelJegyzeksz.Text = "Futárjegyzék lista száma:";
                Jegyzeksz_TextBox.Visible = true;

                tr_Atveteldatum.Visible = true;
                labelAtvetelDatum.Visible = true;
                Atvetel_CalendarControl.Visible = true;

                tr_Atvevo.Visible = false;
                labelAtvevo.Visible = false;
                Atvevo_TextBox.Visible = false;
            }
            else
           if (KuldesMod_KodtarakDropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Szemelyes_kezbesites)
            {
                tr_Ragszam.Visible = false;
                //cbRagszamMegorzes.Visible = false;
                labelRagszam.Visible = false;
                Ragszam_TextBox.Visible = false;

                labelJegyzeksz.Visible = true;
                labelJegyzeksz.Text = "Külső kézbesítő könyv száma:";
                Jegyzeksz_TextBox.Visible = true;

                labelAtvetelDatum.Visible = true;
                Atvetel_CalendarControl.Visible = true;

                labelAtvevo.Visible = true;
                Atvevo_TextBox.Visible = true;
            }
            else
            {
                tr_Ragszam.Visible = true;
                //cbRagszamMegorzes.Visible = true;
                labelRagszam.Visible = true;
                Ragszam_TextBox.Visible = true;

                tr_Jegyzeksz.Visible = false;
                labelJegyzeksz.Visible = false;
                Jegyzeksz_TextBox.Visible = false;

                tr_Atveteldatum.Visible = false;
                labelAtvetelDatum.Visible = false;
                Atvetel_CalendarControl.Visible = false;

                tr_Atvevo.Visible = false;
                labelAtvevo.Visible = false;
                Atvevo_TextBox.Visible = false;
            }
        }
        else
        {
            tr_Jegyzeksz.Visible = false;
            tr_Atveteldatum.Visible = false;
            tr_Atvevo.Visible = false;
            //labelJegyzeksz.Visible = false;
            //Jegyzeksz_TextBox.Visible = false;

            tr_Ragszam.Visible = true;
            labelRagszam.Visible = true;
            Ragszam_TextBox.Visible = true;

            //labelAtvetelDatum.Visible = false;
            //Atvetel_CalendarControl.Visible = false;

            //labelAtvevo.Visible = false;
            //Atvevo_TextBox.Visible = false;
        }
    }

    private void LoadKezbesitesParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        //bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            Jegyzeksz_TextBox.Text = erec_KuldKuldemenyek.FutarJegyzekListaSzama;
            Atvetel_CalendarControl.Text = erec_KuldKuldemenyek.KezbesitoAtvetelIdopontja;
            Atvevo_TextBox.Text = erec_KuldKuldemenyek.KezbesitoAtvevoNeve;
        }
    }
    /// <summary>
    /// Fill kuldemeny bo from futarjegyzek params
    /// </summary>
    /// <param name="erec_KuldKuldemenyek"></param>
    private void FillKuldemenyBOFromKezbesitesParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (!string.IsNullOrEmpty(Jegyzeksz_TextBox.Text))
        {
            erec_KuldKuldemenyek.FutarJegyzekListaSzama = Jegyzeksz_TextBox.Text;
            erec_KuldKuldemenyek.Updated.FutarJegyzekListaSzama = true;
        }

        if (!string.IsNullOrEmpty(Jegyzeksz_TextBox.Text))
        {
            erec_KuldKuldemenyek.KezbesitoAtvetelIdopontja = Atvetel_CalendarControl.Text;
            erec_KuldKuldemenyek.Updated.KezbesitoAtvetelIdopontja = true;
        }

        if (!string.IsNullOrEmpty(Atvevo_TextBox.Text))
        {
            erec_KuldKuldemenyek.KezbesitoAtvevoNeve = Atvevo_TextBox.Text;
            erec_KuldKuldemenyek.Updated.KezbesitoAtvevoNeve = true;
        }

    }
    #endregion
    private void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    // TODO: felkell sorolni...
        //    compSelector.Add_ComponentOnClick(Nev);
        //    compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

        //    FormFooter1.SaveEnabled = false;
        //}
    }

    private void InitTreeViewByKuldemeny(string kuldemenyId)
    {
        if (String.IsNullOrEmpty(kuldemenyId)) { return; }

        // küldeményhez rendelt iratpéldányok
        EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.KuldKuldemeny_Id.Filter(kuldemenyId);

        Result result_kuldIratPeldanyaiGetAll = service_kuldIratPeldanyai.GetAllWithExtension(execParam, search);

        if (result_kuldIratPeldanyaiGetAll.GetCount > 0)
        {
            if (result_kuldIratPeldanyaiGetAll.GetCount == 1)
            {
                if (result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows[0]["BarCode"].ToString() == VonalKodTextBox1.Text)
                    cbAblakosBoritek.Checked = true;
            }

            RadioButtonList1.Items[1].Enabled = false;
            foreach (DataRow row in result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows)
            {
                AddIratPeldanyToTreeView(row["Peldany_Id"].ToString(), row["Azonosito"].ToString(), true);
            }
            SetAblakosBoritekComponents();

        }
        else RadioButtonList1.Items[1].Enabled = true;


        //     LoadComponentsFromBusinessObject(obj_iratPeldany);
    }


    private void SetVonalkodComponentsKimenoKuldemeny(string kimenoKuldemenyVonalkod, string iratpldVonalkod)
    {
        if (kimenoKuldemenyVonalkod != null)
        {
            VonalKodTextBox1.Text = kimenoKuldemenyVonalkod;


            if (kimenoKuldemenyVonalkod == iratpldVonalkod)
            {
                cbAblakosBoritek.Checked = true;
            }
        }
    }

    protected void Torles_Click(object sender, ImageClickEventArgs e)
    {
        TreeNode selectedNode = TreeView1.SelectedNode;
        if (selectedNode == null)
        {
            // nincs kijelölve sor:
            ResultError.DisplayWarningOnErrorPanel(ErrorPanel, "", Resources.Error.UINoSelectedRow);
            return;
        }
        else
        {
            if (selectedNode.Depth == 0)
            {
                TreeView1.Nodes.Remove(selectedNode);
            }
            else if (selectedNode.Depth == 1)
            {
                TreeNode parentNode = selectedNode.Parent;
                selectedNode.Parent.ChildNodes.Remove(selectedNode);
                TreeView1.Nodes.Remove(parentNode);
            }

            SetAblakosBoritekComponents();
        }
    }

    // megadott 
    protected void Hozzaadas_Click(object sender, ImageClickEventArgs e)
    {
        ErrorPanel.Visible = false;
        bool isOkIratPldAdd = false;
        if (!String.IsNullOrEmpty(PldIratPeldanyokTextBox1.Id_HiddenField))
        {
            if (TreeView1.FindNode(PldIratPeldanyokTextBox1.Id_HiddenField) == null)
            {
                isOkIratPldAdd = AddIratPeldanyToTreeView(PldIratPeldanyokTextBox1.Id_HiddenField, PldIratPeldanyokTextBox1.Text, false);
                if (isOkIratPldAdd)
                {
                    // példány textbox törlése
                    PldIratPeldanyokTextBox1.Id_HiddenField = "";
                    PldIratPeldanyokTextBox1.Text = "";
                }
            }
            else
            {
                // jelzés hogy már van ilyen
                ResultError.DisplayWarningOnErrorPanel(ErrorPanel, "", Resources.Error.UIAlreadyExistsIratPeldanyInList);
            }
            if (isOkIratPldAdd)
                SetAblakosBoritekComponents();
        }
    }

    void cbAblakosBoritek_CheckedChanged(object sender, EventArgs e)
    {
        SetAblakosBoritekComponents();
    }

    public void SetAblakosBoritekComponents()
    {
        cbAblakosBoritek.Enabled = (TreeView1.Nodes.Count == 1 && TreeView1.Nodes[0].ChildNodes.Count == 1);
        // CR 3048: Ha új iratpéldány jön létre akkor az ablakos boríték checkboxot letiltjuk
        if (cbAblakosBoritek.Enabled)
        {
            if (GetTreeNodePldNeedNewPld(TreeView1.Nodes[0].ChildNodes[0]) == "1")
            //if (TreeView1.Nodes[0].ChildNodes[0].Text.Contains(" *"))
            {
                cbAblakosBoritek.Enabled = false;
            }
        }
        if (!cbAblakosBoritek.Enabled)
        {
            // ha több példány van, nem lehet ablakos a boríték
            cbAblakosBoritek.Checked = false;
        }
        if (cbAblakosBoritek.Checked)
        {
            // ablakos borítékban csak egy példány lehet, ekkor átvesszük a vonalkódját
            VonalKodTextBox1.Text = GetTreeNodePldVonalkod(TreeView1.Nodes[0].ChildNodes[0]);
        }
        else
        {
            string[] pldVonalkodArray = CreateIratPeldanyVonalkodArrayFromTreeView();
            if (pldVonalkodArray != null)
            {
                if (Array.IndexOf(pldVonalkodArray, VonalKodTextBox1.Text) > -1)
                {

                    //if (cbAblakosBoritek.Enabled)
                    //{
                    //   // cbAblakosBoritek.Checked = true;
                    //}
                    //else
                    //{
                    // nem lehet azonos a példány vonalkódjával
                    VonalKodTextBox1.Text = String.Empty;
                    //}
                }
            }

        }

        VonalKodTextBox1.ReadOnly = cbAblakosBoritek.Checked && !String.IsNullOrEmpty(VonalKodTextBox1.Text);
    }

    private bool AddIratPeldanyToTreeView(string ujPld_Id, string ujPld_iktatoszam, bool isView)
    {
        if (String.IsNullOrEmpty(ujPld_Id) || String.IsNullOrEmpty(ujPld_iktatoszam))
        {
            return false;
        }

        // Iratpéldány, irat állapotának ellenőrzése:
        EREC_PldIratPeldanyok obj_iratPeldany = null;
        EREC_IraIratok obj_irat = null;
        EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
        EREC_UgyUgyiratok obj_ugyirat = null;

        bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(ujPld_Id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
            , Page, ErrorPanel);
        if (success_objectsGet == false)
        {
            return false;
        }
        if (!IsPostBack)
        {
            try
            {
                // az irat benne van-e már a treeview-ban?
                if (TreeView1.FindNode(obj_irat.Id) != null)
                {
                    // jelzés hogy már van ilyen
                    ResultError.DisplayWarningOnErrorPanel(errorPanel, "", Resources.Error.UIAlreadyExistsIratInList);
                    return false;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("eRecordComponent_PostazasiAdatokPanel.AddIratPeldanyToTreeView ", exc);
            }
        }

        IratPeldanyok.Statusz statusz_iratpeldany = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);
        Iratok.Statusz statusz_irat = Iratok.GetAllapotByBusinessDocument(obj_irat);
        UgyiratDarabok.Statusz statusz_ugyiratdarab = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
        Ugyiratok.Statusz statusz_ugyirat = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
        bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        ErrorDetails errorDetail = null;
        // CR3229: Az EXPEDIALAS_UJ_IRATPELDANNYAL paraméter értékétől függően új iratpéldány jön létre
        string newPldNeed = "0";
        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.EXPEDIALAS_UJ_IRATPELDANNYAL, false))
        {
            newPldNeed = "1";
        }
        // Iratpéldány Postázható?
        if (IratPeldanyok.Postazhato(statusz_iratpeldany, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
        {
            // Iratpéldány expediálható-e?       
            if (IratPeldanyok.Expedialhato(statusz_iratpeldany, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
            {

                if (errorDetail.Code == 80011)  //ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje)
                {
                    newPldNeed = "1";
                    //ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.DefaultErrorHeader
                    //   , Resources.Error.ErrorCode_80011 + " <br/> " + ResultError.GetErrorDetailInfoMsg(errorDetail, Page));
                }
                else
                {
                    // nem expediálható és nem is postázható
                    if (!isView)
                    //{
                    //    Torles.Visible = false;
                    //    Hozzaadas.Visible = false;
                    //    PldIratPeldanyokTextBox1.ReadOnly = true;
                    //} else
                    {
                        ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.DefaultErrorHeader
                            , Resources.Error.ErrorCode_52501 + " <br/> " + ResultError.GetErrorDetailInfoMsg(errorDetail, Page));
                        //MainPanel.Visible = false;
                        return false;
                    }
                }
            }

        }

        // Irat, iratpéldány Node-ok hozzáadása:


        string irat_iktatoszam = obj_irat.Azonosito;

        string vonalkod_pld = obj_iratPeldany.BarCode;
        TreeNode newIratNode = new TreeNode(irat_iktatoszam, obj_irat.Id);
        TreeNode newPldNode;
        // CR 3048: Postázásnál új iratpéldány létrehozásakor '*'-val jelöljük az eredeti iratpéldányt a kiválasztó treeviewben, 
        //         valamint a treeview valueban is jelöljük, hogy az ablakos boritek checkboxot letilthassuk
        // nekrisz
        string nodetxt = "";
        if (newPldNeed == "1")
        {
            nodetxt = ujPld_iktatoszam + " *";
        }
        else
        {
            nodetxt = ujPld_iktatoszam;
        }

        newPldNode = new TreeNode(nodetxt, JoinTreeNodePldValue(ujPld_Id, vonalkod_pld, newPldNeed));

        TreeView1.Nodes.Add(newIratNode);
        newIratNode.ChildNodes.Add(newPldNode);
        newIratNode.ExpandAll();

        if (!isView)
        {
            // Címadatok beállítása
            Cimzett_PartnerTextBox.Id_HiddenField = obj_iratPeldany.Partner_Id_Cimzett;
            Cimzett_PartnerTextBox.Text = obj_iratPeldany.NevSTR_Cimzett;
            Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;
            CimzettCime_CimekTextBox.Id_HiddenField = obj_iratPeldany.Cim_id_Cimzett;
            CimzettCime_CimekTextBox.Text = obj_iratPeldany.CimSTR_Cimzett;
            UpdateViszonylatkod();

            // Kiküldő
            Kikuldo_CsoportTextBox.Id_HiddenField = obj_iratPeldany.Csoport_Id_Felelos;
            Kikuldo_CsoportTextBox.SetCsoportTextBoxById(errorPanel);
        }
        return true;
    }

    protected void SetComponentsVisibilityByRogzitesTipus(Mode rogzitesTipus)
    {
        switch (rogzitesTipus)
        {
            case Mode.Simple:
                tr_Cimzett.Visible = true;
                tr_CimzettCime.Visible = true;
                tr_Vonalkod.Visible = true;
                tr_Ragszam.Visible = true;
                // BLG_591
                //tr_TertivevenyVonalkod.Visible = true;
                tr_TertivevenyVonalkod.Visible = isTertivonalkod;


                //EFormPanelVonalkodRagszam.Visible = false;
                tr_VonalkodRagszam.Visible = false;
                //  tr_IratPldTreeView_Rows.Visible = true;
                if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.POSTAZAS_IRATPELDANNYAL, false))
                {
                    tr_IratPldTreeView.Visible = true;
                    tr_IratPeldany.Visible = true;
                }
                else
                {
                    tr_IratPldTreeView.Visible = false;
                    tr_IratPeldany.Visible = false;
                }
                //BLG_1721
                SetKezbesitesVisibility();
                break;
            case Mode.Multiple:
                tr_Cimzett.Visible = false;
                tr_CimzettCime.Visible = false;
                tr_Vonalkod.Visible = false;
                tr_Ragszam.Visible = false;
                tr_TertivevenyVonalkod.Visible = false;
                //EFormPanelVonalkodRagszam.Visible = true;
                tr_VonalkodRagszam.Visible = true;
                // tr_IratPldTreeView_Rows.Visible = false;
                tr_IratPldTreeView.Visible = false;
                tr_IratPeldany.Visible = false;
                // BLG_1721
                tr_Jegyzeksz.Visible = false;
                tr_Atveteldatum.Visible = false;
                tr_Atvevo.Visible = false;
                break;
        }
    }

    // komponensek beállítása (Postázáshoz)
    public void SetComponentsForPostazas(string kuldemenyId, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        //LoadComponents(kuldemenyId, true, errorPanel, errorUpdatePanel);

        //// Postakönyvek feltöltése: (Postakönyv jelenleg nem zárható le)
        //IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, false, errorPanel);

        //RegisterArKalkulatorJs();

        //if (String.IsNullOrEmpty(BelyegzoDatuma_CalendarControl.Text))
        //{
        //    BelyegzoDatuma_CalendarControl.Text = DateTime.Today.ToString();
        //}

        ////KimenoKuldSorszam_TextBox.ReadOnly = true;
        //Cimzett_PartnerTextBox.ReadOnly = true;
        //CimzettCime_CimekTextBox.ReadOnly = true;

        //VonalKodTextBox1.ReadOnly = true;
        //// nem jó a .ReadOnly propertyvel, mert akkor nem jön vissza az értéke postbacknél
        //Ar_TextBox.Attributes.Add("readonly", "readonly");
        //Ar_TextBox.CssClass = "ReadOnlyWebControl";

        switch (RogzitesTipus)
        {
            case Mode.Simple:
                LoadComponents(kuldemenyId, true, errorPanel, errorUpdatePanel);

                //KimenoKuldSorszam_TextBox.ReadOnly = true;
                //LZS - BUG_6263
                //Cimzett_PartnerTextBox.ReadOnly = true;
                Cimzett_PartnerTextBox.ReadOnly = false;

                // CR3150 Cím szegmentáltság ellenörzés
                // Ha a cím nem a partnertörzsből jön, akkor a cím módosíthat
                if (String.IsNullOrEmpty(CimzettCime_CimekTextBox.Id_HiddenField))
                {
                    CimzettCime_CimekTextBox.ReadOnly = false;
                }
                else
                {
                    //LZS - BUG_6263
                    //CimzettCime_CimekTextBox.ReadOnly = true;
                    CimzettCime_CimekTextBox.FreeTextEnabled = false;
                }

                //VonalKodTextBox1.ReadOnly = true; // ha írásvédett, és tértis a küldemény, hogyan viszi fel?
                if (!String.IsNullOrEmpty(VonalKodTextBox1.Text))
                {
                    VonalKodTextBox1.ReadOnly = true;
                    cbAblakosBoritek.Enabled = false;
                }


                if (!String.IsNullOrEmpty(Ragszam_TextBox.Text))
                {
                    Ragszam_TextBox.ReadOnly = true;
                }

                if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue))
                {
                    IraIktatoKonyvekDropDownList_Postakonyv.Enabled = false;
                }

                // ragszám ellenőrző összegének és egyediségének vizsgálata
                //Ragszam_TextBox.Validate = true;
                // ragszám textbox IsKulfold
                SetRagszamTextBoxByViszonylat(this.Viszonylatkod);
                Torles.Visible = false;
                Hozzaadas.Visible = false;
                PldIratPeldanyokTextBox1.ReadOnly = true;
                //   SetAblakosBoritekComponents();

                //Kiküldő szervezet automatikus kitöltése
                if (String.IsNullOrEmpty(Kikuldo_CsoportTextBox.Id_HiddenField))
                {
                    string kikuldoCsoport_Id = GetKikuldoCsoportId(kuldemenyId);

                    if (!String.IsNullOrEmpty(kikuldoCsoport_Id))
                    {
                        Kikuldo_CsoportTextBox.Id_HiddenField = kikuldoCsoport_Id;
                        Kikuldo_CsoportTextBox.SetCsoportTextBoxById(errorPanel);
                    }
                }
                break;
            case Mode.Multiple:
                List<string> filterKimenoKuldemenyFajta = new List<string>();
                //LZS - BUG_7102
                //KimenoKuldFajta_KodtarakDropDownList.FillDropDownList(kcs_KIMENO_KULDEMENY_FAJTA, false, errorPanel);
                Fill_KimenoKuldFajta_KodtarakDropDownList(null);

                RagszamListBox1.RagszamVisible = false;
                tr_IratPldTreeView.Visible = false;
                tr_IratPeldany.Visible = false;
                switch (KuldesMod)
                {
                    case KuldesModType.Ajanlott:
                        Ajanlott_CheckBox.Checked = true;
                        Ajanlott_CheckBox.Enabled = false;
                        Tertiveveny_CheckBox.Checked = false;
                        Tertiveveny_CheckBox.Enabled = false;
                        RagszamListBox1.RagszamVisible = true;
                        RagszamListBox1.TertivevenyVonalkodVisible = false;
                        break;
                    case KuldesModType.Tertivevenyes:
                        Ajanlott_CheckBox.Checked = true;
                        Ajanlott_CheckBox.Enabled = false;
                        Tertiveveny_CheckBox.Checked = true;
                        Tertiveveny_CheckBox.Enabled = false;
                        RagszamListBox1.RagszamVisible = true;
                        RagszamListBox1.TertivevenyVonalkodVisible = true;
                        break;
                    case KuldesModType.HivatalosIrat:
                        Ajanlott_CheckBox.Checked = false;
                        Ajanlott_CheckBox.Enabled = false;
                        Tertiveveny_CheckBox.Checked = false;
                        Tertiveveny_CheckBox.Enabled = false;
                        RagszamListBox1.RagszamVisible = true;
                        RagszamListBox1.TertivevenyVonalkodVisible = true;
                        break;
                    case KuldesModType.Sima:
                    default:
                        Ajanlott_CheckBox.Checked = false;
                        Ajanlott_CheckBox.Enabled = false;
                        Tertiveveny_CheckBox.Checked = false;
                        Tertiveveny_CheckBox.Enabled = false;
                        RagszamListBox1.RagszamVisible = false;
                        RagszamListBox1.TertivevenyVonalkodVisible = false;
                        break;
                }

                // hivatalos irat típusok eltávolítása a listából
                if (KuldesMod == KuldesModType.Sima || KuldesMod == KuldesModType.Ajanlott || KuldesMod == KuldesModType.Tertivevenyes)
                {
                    if (!PostazasModTomeges && IsTomegesKikuldes) // BUG_9173
                    {
                        HivatalosIratokEltavolitasa();
                    }
                }
                else if (KuldesMod == KuldesModType.HivatalosIrat)
                {
                    for (int i = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Count - 1; i >= 0; i--)
                    {
                        ListItem li = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items[i];
                        if (!HivatalosIratok.Contains(li.Value))
                        {
                            KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Remove(li);
                        }
                    }
                }


                if (string.IsNullOrEmpty(IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue))
                {
                    // Postakönyvek feltöltése: (Postakönyv jelenleg nem zárható le)
                    IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, false, errorPanel);
                }

                break;

        }

        //BLG_1721
        SetKezbesitesVisibility();

        //RegisterArKalkulatorJs();

        // nem jó a .ReadOnly propertyvel, mert akkor nem jön vissza az értéke postbacknél
        Ar_TextBox.Attributes.Add("readonly", "readonly");
        Ar_TextBox.CssClass = "ReadOnlyWebControl";

        if (String.IsNullOrEmpty(BelyegzoDatuma_CalendarControl.Text))
        {
            BelyegzoDatuma_CalendarControl.Text = DateTime.Today.ToString();
        }

        SetComponentsVisibilityByRogzitesTipus(RogzitesTipus);
        RagszamListBox1.CheckBoxClientIds = new string[] { Ajanlott_CheckBox.ClientID, Tertiveveny_CheckBox.ClientID };


        Kikuldo_CsoportTextBox.Validate = true;
    }

    /// <summary>
    /// Vonalkód beolvasós panel elrejtése
    /// (Pl. PostazasTomegesForm.aspx-en használva nem kell)
    /// </summary>
    public void HideVonalkodRagszamPanel()
    {
        tr_VonalkodRagszam.Visible = false;
    }

    private void HivatalosIratokEltavolitasa()
    {
        foreach (string kod in HivatalosIratok)
        {
            ListItem li = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.FindByValue(kod);
            if (li != null)
            {
                KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Remove(li);
            }
        }
    }

    // komponensek beállítása (KimenoKuldemenyFelvitel)
    public void SetComponentsForKimenoKuldemenyFelvitel(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        //LoadComponents(kuldemenyId, true, errorPanel, errorUpdatePanel);

        // nincs példánytól átvett küldésmód, eltüntetjük
        tr_KuldesMod.Visible = false;

        //BLG_1721
        SetKezbesitesVisibility();

        // Postakönyvek feltöltése: (Postakönyv jelenleg nem zárható le)
        IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv, false, errorPanel);


        Cimzett_PartnerTextBox.Validate = true;
        CimzettCime_CimekTextBox.Validate = true;

        Label_Req_cimzett.Visible = true;
        Label_Req_cimzettCime.Visible = true;


        Kikuldo_CsoportTextBox.Validate = true;

        cbBelyegzoDatumaMegorzes.Visible = true;
        cbCimzettMegorzes.Visible = true;
        cbKuldemenyFajtajaMegorzes.Visible = true;
        cbPostakonyvMegorzes.Visible = true;
        cbKikuldoMegorzes.Visible = true;
        cbMegjegyzesMegorzes.Visible = true;

        Fill_KimenoKuldFajta_KodtarakDropDownList(null);

        //RegisterArKalkulatorJs();

        if (String.IsNullOrEmpty(BelyegzoDatuma_CalendarControl.Text))
        {
            // Ha el volt mentve Session-be, onnan olvassuk ki, egyébként a mai dátum:
            if (Session["KimenoKuld_BelyegzoDatuma"] != null)
            {
                BelyegzoDatuma_CalendarControl.Text = Session["KimenoKuld_BelyegzoDatuma"].ToString();
                cbBelyegzoDatumaMegorzes.Checked = true;
            }
            else
            {
                BelyegzoDatuma_CalendarControl.Text = DateTime.Today.ToString();
                cbBelyegzoDatumaMegorzes.Checked = false;
            }

            if (Session["KimenoKuld_Cimzett_Checked"] != null && Session["KimenoKuld_Cimzett_Checked"].ToString() == "1")
            {
                Cimzett_PartnerTextBox.Id_HiddenField = (string)(Session["KimenoKuld_Cimzett_Id"] ?? String.Empty);
                if (!String.IsNullOrEmpty(Cimzett_PartnerTextBox.Id_HiddenField))
                {
                    Cimzett_PartnerTextBox.SetPartnerTextBoxById(errorPanel);
                }
                else
                {
                    Cimzett_PartnerTextBox.Text = (string)(Session["KimenoKuld_Cimzett_Text"] ?? String.Empty);
                }

                CimzettCime_CimekTextBox.Id_HiddenField = (string)(Session["KimenoKuld_CimzettCim_Id"] ?? String.Empty);
                if (!String.IsNullOrEmpty(CimzettCime_CimekTextBox.Id_HiddenField))
                {
                    CimzettCime_CimekTextBox.SetCimekTextBoxById(errorPanel);
                }
                else
                {
                    CimzettCime_CimekTextBox.Text = (string)(Session["KimenoKuld_CimzettCim_Text"] ?? String.Empty);
                }

                cbCimzettMegorzes.Checked = true;

                UpdateViszonylatkod();
            }

            if (Session["KimenoKuld_KuldemenyFajtaja"] != null)
            {
                Fill_KimenoKuldFajta_KodtarakDropDownList(Session["KimenoKuld_KuldemenyFajtaja"].ToString());

                //if (Session["KimenoKuld_Elsobbsegi"] != null)
                //{
                //    Elsobbsegi_CheckBox.Checked = (Session["KimenoKuld_Elsobbsegi"].ToString() == "1" ? true : false);
                //}

                if (Session["KimenoKuld_KuldemenyFajtaja_CheckBoxes"] != null)
                {
                    SetCheckBoxesFromSerializedSessionString_KuldemenyFajtaja(Session["KimenoKuld_KuldemenyFajtaja_CheckBoxes"].ToString());
                }

                cbKuldemenyFajtajaMegorzes.Checked = true;
            }

            if (Session["KimenoKuld_PostaKonyv"] != null)
            {
                PostaKonyvekDropDownList.SelectedValue = Session["KimenoKuld_PostaKonyv"].ToString();
                cbPostakonyvMegorzes.Checked = true;
            }
            if (Session["KimenoKuld_Kikuldo"] != null)
            {
                Kikuldo_CsoportTextBox.Id_HiddenField = Session["KimenoKuld_Kikuldo"].ToString();
                Kikuldo_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

                cbKikuldoMegorzes.Checked = true;
            }
            if (Session["KimenoKuld_Megjegyzes"] != null)
            {
                Megjegyzes_TextBox.Text = Session["KimenoKuld_Megjegyzes"].ToString();
                cbMegjegyzesMegorzes.Checked = true;
            }
            if (Session["KimenoKuld_ErteknyilvanitasOsszege"] != null)
            {
                Erteknyilvanitas_TextBox.Text = Session["KimenoKuld_ErteknyilvanitasOsszege"].ToString();
                Erteknyilvanitas_CheckBox.Checked = true;
            }
        }

        ////Címzett partner és cim összekötése
        //Cimzett_PartnerTextBox.CimTextBox = CimzettCime_CimekTextBox.TextBox;
        //Cimzett_PartnerTextBox.CimHiddenField = CimzettCime_CimekTextBox.HiddenField;

        //KimenoKuldSorszam_TextBox.ReadOnly = true;
        //Cimzett_PartnerTextBox.ReadOnly = true;
        //CimzettCime_CimekTextBox.ReadOnly = true;
        //VonalKodTextBox1.ReadOnly = true;

        // nem jó a .ReadOnly propertyvel, mert akkor nem jön vissza az értéke postbacknél
        Ar_TextBox.Attributes.Add("readonly", "readonly");
        Ar_TextBox.CssClass = "ReadOnlyWebControl";

        tr_radiobuttonlist.Visible = true;
        RadioButtonList1_SelectedIndexChanged(RadioButtonList1, new EventArgs());

        // ragszám ellenőrző összegének és egyediségének vizsgálata
        //Ragszam_TextBox.Validate = true;
        // ragszám textbox IsKulfold
        SetRagszamTextBoxByViszonylat(this.Viszonylatkod);
        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.POSTAZAS_IRATPELDANNYAL, false))
        {
            tr_IratPldTreeView.Visible = true;
            tr_IratPeldany.Visible = true;
        }
        else
        {
            tr_IratPldTreeView.Visible = false;
            tr_IratPeldany.Visible = false;
        }
    }

    private bool IsKimenoKuldemenyTertivevenyes(EREC_KuldKuldemenyek erec_KuldKuldemenyek, bool onlyUpdated)
    {
        bool isTertivevenyes = false;
        if (erec_KuldKuldemenyek.Tertiveveny == "1")
        {
            if (!onlyUpdated || erec_KuldKuldemenyek.Updated.Tertiveveny)
            {
                isTertivevenyes = true;
            }
        }
        else if (HivatalosIratok.Contains(erec_KuldKuldemenyek.KimenoKuldemenyFajta))
        {
            if (!onlyUpdated || erec_KuldKuldemenyek.Updated.KimenoKuldemenyFajta)
            {
                isTertivevenyes = true;
            }
        }

        return isTertivevenyes;
    }

    // komponensek beállítása (Modify - Kimeno kuldemeny modositas)
    public void SetComponentsForKimenoKuldemenyModositas(string kuldemenyId, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        EREC_KuldKuldemenyek kimenoKuldemeny = LoadComponents(kuldemenyId, true, errorPanel, errorUpdatePanel);

        //if (kimenoKuldemeny.Tertiveveny == "1")
        if (IsKimenoKuldemenyTertivevenyes(kimenoKuldemeny, false))
        {
            LoadComponents_Tertiveveny(kimenoKuldemeny, errorPanel, errorUpdatePanel);
        }

        Tipus tipus = Tipus.KimenoKuldemenyFelvitel_IsmertCimzett;

        if (kimenoKuldemeny != null && !string.IsNullOrEmpty(kimenoKuldemeny.PeldanySzam)
            && kimenoKuldemeny.PeldanySzam != "1")
        {
            // Tömeges felvitel volt:
            tipus = Tipus.KimenoKuldemenyFelvitel_Tomeges;
        }

        //BLG_1721
        SetKezbesitesVisibility();

        Cimzett_PartnerTextBox.Validate = true;
        CimzettCime_CimekTextBox.Validate = true;

        Label_Req_cimzett.Visible = true;
        Label_Req_cimzettCime.Visible = true;


        Kikuldo_CsoportTextBox.Validate = true;

        //KimenoKuldFajta_KodtarakDropDownList.FillDropDownList(kcs_KIMENO_KULDEMENY_FAJTA, false, errorPanel);

        if (tipus == Tipus.KimenoKuldemenyFelvitel_IsmertCimzett)
        {
            RadioButtonList1.SelectedIndex = 0;
        }
        else
        {
            // Tömeges felvitel:
            RadioButtonList1.SelectedIndex = 1;
        }

        RefreshComponents(tipus);
        //RegisterArKalkulatorJs();

        if (String.IsNullOrEmpty(BelyegzoDatuma_CalendarControl.Text))
        {
            BelyegzoDatuma_CalendarControl.Text = DateTime.Today.ToString();
        }

        ////Címzett partner és cim összekötése
        //Cimzett_PartnerTextBox.CimTextBox = CimzettCime_CimekTextBox.TextBox;
        //Cimzett_PartnerTextBox.CimHiddenField = CimzettCime_CimekTextBox.HiddenField;

        //KimenoKuldSorszam_TextBox.ReadOnly = true;
        //Cimzett_PartnerTextBox.ReadOnly = true;
        //CimzettCime_CimekTextBox.ReadOnly = true;
        //VonalKodTextBox1.ReadOnly = true;

        // nem jó a .ReadOnly propertyvel, mert akkor nem jön vissza az értéke postbacknél
        Ar_TextBox.Attributes.Add("readonly", "readonly");
        Ar_TextBox.CssClass = "ReadOnlyWebControl";

        tr_radiobuttonlist.Visible = true;

        // ragszám ellenőrző összegének és egyediségének vizsgálata
        //Ragszam_TextBox.Validate = true;
        // ragszám textbox IsKulfold
        SetRagszamTextBoxByViszonylat(this.Viszonylatkod);
        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.POSTAZAS_IRATPELDANNYAL, false))
        {
            tr_IratPldTreeView.Visible = true;
            tr_IratPeldany.Visible = true;
            Torles.Visible = false;
            Hozzaadas.Visible = false;
            PldIratPeldanyokTextBox1.ReadOnly = true;
        }
        else
        {
            tr_IratPldTreeView.Visible = false;
            tr_IratPeldany.Visible = false;
        }


        SetAblakosBoritekComponents();
        VonalKodTextBox1.ReadOnly = true;
        cbAblakosBoritek.Enabled = false;
    }

    /// <summary>
    /// Feltölti a komponenseket, és beállít mindent ReadOnlyba
    /// </summary>
    public void SetComponents_ViewMode(EREC_KuldKuldemenyek erec_KuldKuldemenyek, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        tr_mennyiseg.Visible = true;

        LoadComponents(erec_KuldKuldemenyek, false, errorPanel, errorUpdatePanel);

        if (IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false))
        {
            LoadComponents_Tertiveveny(erec_KuldKuldemenyek, errorPanel, errorUpdatePanel);
        }

        SetComponentsToReadOnly();
    }

    public void ClearForm()
    {
        IraIktatoKonyvekDropDownList_Postakonyv.DropDownList.Items.Clear();
        BelyegzoDatuma_CalendarControl.Text = String.Empty;
        Cimzett_PartnerTextBox.Id_HiddenField = String.Empty;
        Cimzett_PartnerTextBox.Text = String.Empty;
        Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;
        CimzettCime_CimekTextBox.Id_HiddenField = String.Empty;
        CimzettCime_CimekTextBox.Text = String.Empty;

        Kikuldo_CsoportTextBox.Id_HiddenField = String.Empty;
        Kikuldo_CsoportTextBox.SetCsoportTextBoxById(null);

        VonalKodTextBox1.Text = String.Empty;
        Ragszam_TextBox.Text = String.Empty;
        Tertiveveny_VonalkodTextBox.Text = String.Empty;
        KimenoKuldFajta_KodtarakDropDownList.Clear();
        KuldesMod_KodtarakDropDownList.Clear();
        Elsobbsegi_CheckBox.Checked = false;
        Ajanlott_CheckBox.Checked = false;
        Tertiveveny_CheckBox.Checked = false;
        SajatKezbe_CheckBox.Checked = false;
        E_Ertesites_CheckBox.Checked = false;
        E_Elorejelzes_CheckBox.Checked = false;
        PostaiLezaroSzolgalat_CheckBox.Checked = false;
        Erteknyilvanitas_CheckBox.Checked = false;
        Erteknyilvanitas_TextBox.Text = String.Empty;
        Ar_TextBox.Text = String.Empty;
        Peldanyszam_RequiredNumberBox.Text = "1"; // String.Empty;
        Megjegyzes_TextBox.Text = String.Empty;
        PldIratPeldanyokTextBox1.Text = String.Empty;
        TreeView1.Nodes.Clear();
        // BLG_1721
        Jegyzeksz_TextBox.Text = String.Empty;
        Atvetel_CalendarControl.Text = String.Empty;
        Atvevo_TextBox.Text = String.Empty;
    }


    private EREC_KuldKuldemenyek LoadComponents(string kuldemenyId, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(kuldemenyId))
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = kuldemenyId;

            Result result = service.Get(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
                return null;
            }

            EREC_KuldKuldemenyek kimenoKuldemeny = (EREC_KuldKuldemenyek)result.Record;

            LoadComponents(kimenoKuldemeny, fillDropDownList, errorPanel, errorUpdatePanel);

            return kimenoKuldemeny;
        }
        return null;
    }

    public void LoadComponentsFromTemplate(EREC_KuldKuldemenyek kimenoKuldemeny, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        LoadComponents(kimenoKuldemeny, fillDropDownList, errorPanel, errorUpdatePanel, true);
    }

    private void LoadComponents(EREC_KuldKuldemenyek kimenoKuldemeny, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        LoadComponents(kimenoKuldemeny, fillDropDownList, errorPanel, errorUpdatePanel, false);
    }

    /// <summary>
    /// Panel feltöltése adatokkal
    /// </summary>
    private void LoadComponents(EREC_KuldKuldemenyek kimenoKuldemeny, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel, bool template)
    {
        if (kimenoKuldemeny != null)
        {
            #region Komponensek feltöltése

            // validáláshoz szükséges
            KuldemenyId_HiddenField.Value = kimenoKuldemeny.Id;

            // Postakönyv feltöltése:
            if (string.IsNullOrEmpty(kimenoKuldemeny.IraIktatokonyv_Id))
            {
                // lista feltöltése, üres elemet hozzáadjuk (Postakönyv jelenleg nem zárható le)
                IraIktatoKonyvekDropDownList_Postakonyv.FillDropDownList(false, false, Constants.IktatoErkezteto.Postakonyv
                    , true, errorPanel);
            }
            else
            {
                // Csak azzal az egy elemmel töltjük fel:
                IraIktatoKonyvekDropDownList_Postakonyv.FillWithOneValue(Constants.IktatoErkezteto.Postakonyv
                    , kimenoKuldemeny.IraIktatokonyv_Id, errorPanel);
            }

            if (!template)
            {
                BelyegzoDatuma_CalendarControl.Text = kimenoKuldemeny.BelyegzoDatuma;
            }

            //#region Címzett kinyerése az iratpéldányból

            //EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            //search.Id.Value = kimenoKuldemeny.Id;
            //search.Id.Operator = Query.Operators.equals;

            //Result result = service.KimenoKuldGetAllWithExtension(execParam, search);
            //if (!String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    // hiba:
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            //    errorUpdatePanel.Update();
            //}
            //else
            //{
            //    if (result.Ds.Tables[0].Rows.Count == 1)
            //    {
            //        System.Data.DataRow row = result.Ds.Tables[0].Rows[0];

            //        string cimzett_nev = row["Cimzett_Nev_Partner"].ToString();
            //        string cimzett_id = row["Cimzett_Id_Partner"].ToString();
            //        string cimzett_cim_nev = row["Cimzett_Cim_Partner"].ToString();
            //        string cimzett_cim_id = row["Cimzett_CimId_Partner"].ToString();

            //        Cimzett_PartnerTextBox.Id_HiddenField = cimzett_id;
            //        Cimzett_PartnerTextBox.Text = cimzett_nev;
            //        CimzettCime_CimekTextBox.Id_HiddenField = cimzett_cim_id;
            //        CimzettCime_CimekTextBox.Text = cimzett_cim_nev;
            //    }
            //}

            //#endregion
            // CR3146: Template ne töltse ezeket a fieldeket
            if (!template)
            {
                // Címzett adatai:
                Cimzett_PartnerTextBox.Id_HiddenField = kimenoKuldemeny.Partner_Id_Bekuldo;
                Cimzett_PartnerTextBox.Text = kimenoKuldemeny.NevSTR_Bekuldo;
                Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = kimenoKuldemeny.Partner_Id_BekuldoKapcsolt;
                CimzettCime_CimekTextBox.Id_HiddenField = kimenoKuldemeny.Cim_Id;
                CimzettCime_CimekTextBox.Text = kimenoKuldemeny.CimSTR_Bekuldo;
            }
            UpdateViszonylatkod();

            // Kiküldő
            Kikuldo_CsoportTextBox.Id_HiddenField = kimenoKuldemeny.Csoport_Id_Cimzett;
            Kikuldo_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

            if (!template)
            {
                VonalKodTextBox1.Text = kimenoKuldemeny.BarCode;

                Ragszam_TextBox.Text = kimenoKuldemeny.RagSzam;
            }
            // CR3147 Expediált Postázásakor a Template elrontja a KüldésMód-ot
            if (!template)
            {
                if (!String.IsNullOrEmpty(kimenoKuldemeny.KuldesMod))
                {
                    KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
                        kimenoKuldemeny.KuldesMod, false, errorPanel);
                }
                else
                {
                    if (fillDropDownList)
                    {
                        KuldesMod_KodtarakDropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, false, errorPanel);
                    }
                }
            }

            if (!String.IsNullOrEmpty(kimenoKuldemeny.KimenoKuldemenyFajta))
            {
                Fill_KimenoKuldFajta_KodtarakDropDownList(kimenoKuldemeny.KimenoKuldemenyFajta);
            }
            else
            {
                if (fillDropDownList)
                {
                    Fill_KimenoKuldFajta_KodtarakDropDownList(null);
                }
            }

            // BLG_1721
            if (isTUK)
            {
                LoadKezbesitesParameters(kimenoKuldemeny);
            }
            else
            {
                // a példányból átvett küldésmód alapján való kitöltés
                bool isTertiveveny = false;
                bool isAjanlott = false;
                bool isElsobbsegi = false;
                if (kimenoKuldemeny.Allapot == KodTarak.KULDEMENY_ALLAPOT.Expedialt)
                {
                    switch (kimenoKuldemeny.KuldesMod)
                    {
                        case KodTarak.KULDEMENY_KULDES_MODJA.Postai_tertivevenyes:
                            isTertiveveny = true;
                            goto case KodTarak.KULDEMENY_KULDES_MODJA.Postai_ajanlott;
                        case KodTarak.KULDEMENY_KULDES_MODJA.Postai_ajanlott:
                            isAjanlott = true;
                            break;
                        case KodTarak.KULDEMENY_KULDES_MODJA.Postai_elsobbsegi:
                            isElsobbsegi = true;
                            break;
                    }
                }

                Elsobbsegi_CheckBox.Checked = (kimenoKuldemeny.Elsobbsegi == "1") ? true : isElsobbsegi;

                Ajanlott_CheckBox.Checked = (kimenoKuldemeny.Ajanlott == "1") ? true : isAjanlott;
                Tertiveveny_CheckBox.Checked = (kimenoKuldemeny.Tertiveveny == "1") ? true : isTertiveveny;
                SajatKezbe_CheckBox.Checked = kimenoKuldemeny.SajatKezbe == "1";
                E_Ertesites_CheckBox.Checked = kimenoKuldemeny.E_ertesites == "1";
                E_Elorejelzes_CheckBox.Checked = kimenoKuldemeny.E_elorejelzes == "1";
                PostaiLezaroSzolgalat_CheckBox.Checked = kimenoKuldemeny.PostaiLezaroSzolgalat == "1";

                Erteknyilvanitas_CheckBox.Checked = kimenoKuldemeny.Erteknyilvanitas == "1";
                Erteknyilvanitas_TextBox.Text = kimenoKuldemeny.ErteknyilvanitasOsszege.ToString();

                if (!string.IsNullOrEmpty(kimenoKuldemeny.Ar))
                {
                    Ar_TextBox.Text = kimenoKuldemeny.Ar;
                }
                else
                {
                    RegisterArKalkulatorJs();
                }
            }
            Peldanyszam_RequiredNumberBox.Text = kimenoKuldemeny.PeldanySzam;

            Megjegyzes_TextBox.Text = kimenoKuldemeny.Base.Note;

            Record_Ver_HiddenField.Value = kimenoKuldemeny.Base.Ver;

            #endregion

            // CR3132 Template megjelenítése az iratpéldéldánylista oldalról is
            // Template esetén ne töltse be (újra) az iratpéldányokat
            if (!template)
            {
                InitTreeViewByKuldemeny(kimenoKuldemeny.Id);
            }

            if (Command == CommandName.Modify && (
                kimenoKuldemeny.Allapot == KodTarak.KULDEMENY_ALLAPOT.Postazott ||
                kimenoKuldemeny.Allapot == KodTarak.KULDEMENY_ALLAPOT.Lezarva ||
                kimenoKuldemeny.Allapot == KodTarak.KULDEMENY_ALLAPOT.Sztorno))
            {
                IraIktatoKonyvekDropDownList_Postakonyv.ReadOnly = true;
            }

        }
    }

    public void LoadComponents_Tertiveveny(EREC_KuldKuldemenyek kimenoKuldemeny, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        EREC_KuldTertivevenyek erec_KuldTertivevenyek = new EREC_KuldTertivevenyek();
        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        if (!String.IsNullOrEmpty(kimenoKuldemeny.Id))
        {
            search.Kuldemeny_Id.Filter(kimenoKuldemeny.Id);
        }
        else
        {

            // hiba:
            ResultError.DisplayNoIdParamError(errorPanel);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
            return;
        }

        Result result = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
            return;
        }

        if (result.GetCount == 1)
        {
            Tertiveveny_VonalkodTextBox.Text = result.Ds.Tables[0].Rows[0]["BarCode"].ToString();
        }
    }


    public EREC_KuldKuldemenyek GetBusinessObjectFromComponents()
    {
        return GetBusinessObjectFromComponents(false);
    }

    public EREC_KuldKuldemenyek GetBusinessObjectFromComponents(bool kimenoKuldemenyFelvitel)
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        erec_KuldKuldemenyek.BelyegzoDatuma = BelyegzoDatuma_CalendarControl.Text;
        erec_KuldKuldemenyek.Updated.BelyegzoDatuma = true;

        // Kiküldő:
        erec_KuldKuldemenyek.Csoport_Id_Cimzett = Kikuldo_CsoportTextBox.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = true;

        if (kimenoKuldemenyFelvitel)
        {
            // Kimenő küldeménynél a Címzett partner az a Küldemények tábla PartnerIdBekuldo mezőjébe kerül !! (A cím ugyanígy)

            erec_KuldKuldemenyek.NevSTR_Bekuldo = Cimzett_PartnerTextBox.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = true;

            erec_KuldKuldemenyek.Partner_Id_Bekuldo = Cimzett_PartnerTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = true;

            #region BUG_5197
            erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt = Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue;
            erec_KuldKuldemenyek.Updated.Partner_Id_BekuldoKapcsolt = true;

            Cimzett_PartnerTextBox.CheckPartnerCimek(Cimzett_PartnerTextBox.Id_HiddenField, CimzettCime_CimekTextBox.Id_HiddenField, Record_Ver_HiddenField.Value, ErrorPanel);
            #endregion

            erec_KuldKuldemenyek.CimSTR_Bekuldo = CimzettCime_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = true;

            erec_KuldKuldemenyek.Cim_Id = CimzettCime_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = true;

            //// Vonalkód:
            //erec_KuldKuldemenyek.BarCode = VonalKodTextBox1.Text;
            //erec_KuldKuldemenyek.Updated.BarCode = true;

            #region Példányszám:
            erec_KuldKuldemenyek.PeldanySzam = "1";
            erec_KuldKuldemenyek.Updated.PeldanySzam = true;

            // tömeges kiküldés:
            if (RadioButtonList1.SelectedValue == "1" && !String.IsNullOrEmpty(Peldanyszam_RequiredNumberBox.Text))
            {
                erec_KuldKuldemenyek.PeldanySzam = Peldanyszam_RequiredNumberBox.Text;
            }
            #endregion
        }
        else
        {
            // CR3150 Cím szegmentáltság ellenörzés
            // A cím módosulhat
            erec_KuldKuldemenyek.CimSTR_Bekuldo = CimzettCime_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = true;

            erec_KuldKuldemenyek.Cim_Id = CimzettCime_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = true;

        }

        erec_KuldKuldemenyek.IraIktatokonyv_Id = IraIktatoKonyvekDropDownList_Postakonyv.SelectedValue;
        erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = true;

        if (RogzitesTipus == Mode.Simple)
        {
            // Vonalkód
            erec_KuldKuldemenyek.BarCode = VonalKodTextBox1.Text;
            erec_KuldKuldemenyek.Updated.BarCode = true;

            erec_KuldKuldemenyek.RagSzam = Ragszam_TextBox.Text;
            erec_KuldKuldemenyek.Updated.RagSzam = true;
        }
        // BLG_1721
        erec_KuldKuldemenyek.KuldesMod = KuldesMod_KodtarakDropDownList.SelectedValue;
        erec_KuldKuldemenyek.Updated.KuldesMod = true;

        // BLG_1721 
        if (isTUK)
        {
            FillKuldemenyBOFromKezbesitesParameters(erec_KuldKuldemenyek);
        }
        else
        {
            erec_KuldKuldemenyek.KimenoKuldemenyFajta = KimenoKuldFajta_KodtarakDropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KimenoKuldemenyFajta = true;

            erec_KuldKuldemenyek.Elsobbsegi = (Elsobbsegi_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.Elsobbsegi = true;

            erec_KuldKuldemenyek.Ajanlott = (Ajanlott_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.Ajanlott = true;

            erec_KuldKuldemenyek.Tertiveveny = (Tertiveveny_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.Tertiveveny = true;

            erec_KuldKuldemenyek.SajatKezbe = (SajatKezbe_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.SajatKezbe = true;

            erec_KuldKuldemenyek.E_ertesites = (E_Ertesites_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.E_ertesites = true;

            erec_KuldKuldemenyek.E_elorejelzes = (E_Elorejelzes_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.E_elorejelzes = true;

            erec_KuldKuldemenyek.PostaiLezaroSzolgalat = (PostaiLezaroSzolgalat_CheckBox.Checked) ? "1" : "0";
            erec_KuldKuldemenyek.Updated.PostaiLezaroSzolgalat = true;

            erec_KuldKuldemenyek.Erteknyilvanitas = Erteknyilvanitas_CheckBox.Checked ? "1" : "0";
            erec_KuldKuldemenyek.Updated.Erteknyilvanitas = true;

            erec_KuldKuldemenyek.ErteknyilvanitasOsszege = Erteknyilvanitas_TextBox.Text;
            erec_KuldKuldemenyek.Updated.ErteknyilvanitasOsszege = true;

            erec_KuldKuldemenyek.Ar = Ar_TextBox.Text;
            erec_KuldKuldemenyek.Updated.Ar = true;
        }
        erec_KuldKuldemenyek.Base.Note = Megjegyzes_TextBox.Text;
        erec_KuldKuldemenyek.Base.Updated.Note = true;


        // a módosítás miatt szükséges
        erec_KuldKuldemenyek.Base.Ver = Record_Ver_HiddenField.Value;
        erec_KuldKuldemenyek.Base.Updated.Ver = true;

        return erec_KuldKuldemenyek;

    }

    // küldeményhez rendelt iratpéldányok
    public bool GetIratPeldanyok(out String[] pld_Array)
    {

        pld_Array = null;
        // CR3108 : Postázott iratpéldányok Ügyiratba helyezése 
        // CR kapcsán előjött hiba
        // a CheckVonalkod által visszaadott hibát elnyelte, ezért false, akkor ha hiba van és a hívó helyen ellenőrizni kell hogy van-e eleme a tömbnek
        //if (TreeView1.Nodes.Count == 0 )
        //{
        ////    ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52510);
        //    return false;
        //}
        if (TreeView1.Nodes.Count > 0)
        {

            //if (!CheckParameters())
            //{
            //    return false;
            //}

            pld_Array = CreateIratPeldanyArrayFromTreeView();

            string errormsg;
            if (!CheckVonalkod(pld_Array, out errormsg))
            {
                // hiba
                ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.ErrorLabel, errormsg);
                return false;
            }
        }
        return true;
    }


    protected bool CheckVonalkod(string[] pld_Array, out string errormsg)
    {
        bool isValid = true;
        errormsg = null;
        String vonalkod = VonalKodTextBox1.Text;    // kimenő küldemény vonalkódja


        if (cbAblakosBoritek.Checked)
        {
            // ablakos borítéknál kötelező a vonalkód
            //(megegyezik a példány vonalkódjával, automatikus kitöltés, csak hiba esetén maradhat üres)
            if (pld_Array.Length == 1)
            {
                // átvesszük a példány vonalkódját:
                vonalkod = pld_Array[0];

                if (String.IsNullOrEmpty(vonalkod))
                {
                    // nincs megadva vonalkód:
                    //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoBarCode);
                    errormsg = Resources.Error.UINoBarCode;
                    isValid = false;
                }
            }
            else
            {
                // több példány esetén nem lehet ablakos a boríték (pontosabban mindeképp külön vonalkód kell neki)
                //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Több példány esetén az ablakos boríték opció nem használható! (önálló vonalkódot kell adni a küldeménynek)");
                //"Több példány esetén az ablakos boríték opció nem használható! (önálló vonalkódot kell adni a küldeménynek)";
                errormsg = Resources.Error.ErrorCode_52515;
                isValid = false;
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(vonalkod))
            {
                string[] pldVonalkodArray = CreateIratPeldanyVonalkodArrayFromTreeView();
                if (pldVonalkodArray != null)
                {

                    if (Array.IndexOf(pldVonalkodArray, vonalkod) > -1)
                    {
                        // nem ablakos boríték esetén külön vonalkód kell a küldeménynek, nem egyezhet meg a példányéval
                        //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "A vonalkód megegyezik valamely példány vonalkódjával! (nem ablakos boríték esetén önálló vonalkódot kell adni a küldeménynek)");
                        //"A vonalkód megegyezik valamely példány vonalkódjával! (nem ablakos boríték esetén önálló vonalkódot kell adni a küldeménynek)";
                        errormsg = Resources.Error.ErrorCode_52516;
                        isValid = false;
                    }
                }
            }
        }

        return isValid;
    }

    // iratpéldány tömb összeállítása a treeviewból
    private String[] CreateIratPeldanyArrayFromTreeView()
    {
        String[] pldArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            pldArray[i] = GetTreeNodePldId(TreeView1.Nodes[i].ChildNodes[0]);
        }

        return pldArray;
    }

    // iratpéldány vonalkód tömb összeállítása a treeviewból - ellenőrzéshez
    private String[] CreateIratPeldanyVonalkodArrayFromTreeView()
    {
        String[] pldVonalkodArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            pldVonalkodArray[i] = GetTreeNodePldVonalkod(TreeView1.Nodes[i].ChildNodes[0]);
        }

        return pldVonalkodArray;
    }

    /// <summary>
    /// A megőrzős checkboxok alapján adat Session-be mentése vagy kidobása onnan
    /// </summary>
    public void SetSessionData()
    {
        if (cbBelyegzoDatumaMegorzes.Checked)
        {
            Session["KimenoKuld_BelyegzoDatuma"] = BelyegzoDatuma_CalendarControl.Text;
        }
        else
        {
            Session.Remove("KimenoKuld_BelyegzoDatuma");
        }

        if (cbCimzettMegorzes.Checked)
        {
            Session["KimenoKuld_Cimzett_Checked"] = "1";
            Session["KimenoKuld_Cimzett_Id"] = Cimzett_PartnerTextBox.Id_HiddenField;
            Session["KimenoKuld_Cimzett_Text"] = Cimzett_PartnerTextBox.Text;
            Session["KimenoKuld_CimzettCim_Id"] = CimzettCime_CimekTextBox.Id_HiddenField;
            Session["KimenoKuld_CimzettCim_Text"] = CimzettCime_CimekTextBox.Text;
        }
        else
        {
            Session.Remove("KimenoKuld_Cimzett_Checked");
            Session.Remove("KimenoKuld_Cimzett_Id");
            Session.Remove("KimenoKuld_Cimzett_Text");
            Session.Remove("KimenoKuld_CimzettCim_Id");
            Session.Remove("KimenoKuld_CimzettCim_Text");
        }

        if (cbKuldemenyFajtajaMegorzes.Checked)
        {
            Session["KimenoKuld_KuldemenyFajtaja"] = KimenoKuldFajta_KodtarakDropDownList.SelectedValue;
            //Session["KimenoKuld_Elsobbsegi"] = (Elsobbsegi_CheckBox.Checked ? "1" : "0");

            Session["KimenoKuld_KuldemenyFajtaja_CheckBoxes"] = GetCheckBoxesSerializedForSession_KuldemenyFajtaja();
        }
        else
        {
            Session.Remove("KimenoKuld_KuldemenyFajtaja");
            Session.Remove("KimenoKuld_Elsobbsegi");
        }

        if (cbPostakonyvMegorzes.Checked)
        {
            Session["KimenoKuld_PostaKonyv"] = PostaKonyvekDropDownList.SelectedValue;
        }
        else
        {
            Session.Remove("KimenoKuld_PostaKonyv");
        }

        if (cbKikuldoMegorzes.Checked)
        {
            Session["KimenoKuld_Kikuldo"] = Kikuldo_CsoportTextBox.Id_HiddenField;
        }
        else
        {
            Session.Remove("KimenoKuld_Kikuldo");
        }

        if (cbMegjegyzesMegorzes.Checked)
        {
            Session["KimenoKuld_Megjegyzes"] = Megjegyzes_TextBox.Text;
        }
        else
        {
            Session.Remove("KimenoKuld_Megjegyzes");
        }

        if (Erteknyilvanitas_CheckBox.Checked)
        {
            Session["KimenoKuld_ErteknyilvanitasOsszege"] = Erteknyilvanitas_TextBox.Text;
        }
        else
        {
            Session.Remove("KimenoKuld_ErteknyilvanitasOsszege");
        }
    }

    private void SetComponentsToReadOnly()
    {
        //KimenoKuldSorszam_TextBox.ReadOnly = true;
        IraIktatoKonyvekDropDownList_Postakonyv.ReadOnly = true;
        BelyegzoDatuma_CalendarControl.ReadOnly = true;
        cbBelyegzoDatumaMegorzes.Enabled = false;
        cbCimzettMegorzes.Enabled = false;
        cbKuldemenyFajtajaMegorzes.Enabled = false;
        cbKikuldoMegorzes.Enabled = false;
        cbPostakonyvMegorzes.Enabled = false;
        cbMegjegyzesMegorzes.Enabled = false;
        Cimzett_PartnerTextBox.ReadOnly = true;
        CimzettCime_CimekTextBox.ReadOnly = true;
        Kikuldo_CsoportTextBox.ReadOnly = true;
        VonalKodTextBox1.ReadOnly = true;
        Ragszam_TextBox.ReadOnly = true;
        Tertiveveny_VonalkodTextBox.ReadOnly = true;
        KimenoKuldFajta_KodtarakDropDownList.ReadOnly = true;
        Elsobbsegi_CheckBox.Enabled = false;
        Ajanlott_CheckBox.Enabled = false;
        Tertiveveny_CheckBox.Enabled = false;
        SajatKezbe_CheckBox.Enabled = false;
        E_Ertesites_CheckBox.Enabled = false;
        E_Elorejelzes_CheckBox.Enabled = false;
        PostaiLezaroSzolgalat_CheckBox.Enabled = false;
        Erteknyilvanitas_CheckBox.Enabled = false;
        Erteknyilvanitas_TextBox.Enabled = false;
        Ar_TextBox.ReadOnly = true;
        Peldanyszam_RequiredNumberBox.ReadOnly = true;
        Megjegyzes_TextBox.ReadOnly = true;
        rbBelfoldi.Enabled = rbEuropai.Enabled = rbEgyebKulfoldi.Enabled = false;
        cbAblakosBoritek.Enabled = false;

        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.POSTAZAS_IRATPELDANNYAL, false))
        {
            tr_IratPldTreeView.Visible = true;
            tr_IratPeldany.Visible = true;
            Torles.Visible = false;
            Hozzaadas.Visible = false;
            PldIratPeldanyokTextBox1.ReadOnly = true;
        }
        else
        {
            tr_IratPldTreeView.Visible = false;
            tr_IratPeldany.Visible = false;
            //PldIratPeldanyokTextBox1.ReadOnly = true;
            //Hozzaadas.Visible = false;
            //Torles.Visible = false;
            //TreeView_EFormPanel.Enabled = false;
        }

        // BLG_1721
        Jegyzeksz_TextBox.ReadOnly = true;
        Atvetel_CalendarControl.ReadOnly = true;
        Atvevo_TextBox.ReadOnly = true;

        // BLG_591
        tr_TertivevenyVonalkod.Visible = isTertivonalkod;
    }

    public void MarkFailedRecords(List<string> vonalkodok)
    {
        RagszamListBox1.MarkFailedRecords(vonalkodok);
    }

    public void RegisterArKalkulatorJs()
    {
        // BLG_1721
        //bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK) return;

        string isHivatalosIrat = String.Empty;

        foreach (string kod in HivatalosIratok)
        {
            if (!String.IsNullOrEmpty(isHivatalosIrat)) isHivatalosIrat += " || ";
            isHivatalosIrat += "kimenoKuldFajta_value == '" + kod + "'";
        }
        Dictionary<string, string> elsobbsegiArak =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KIMENO_KULDEMENY_AR_ELSOBBSEGI, Page);
        Dictionary<string, string> nemElsobbsegiArak =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI, Page);
        Dictionary<string, string> tobbletSzolgaltatasArak =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS, Page);

        if (elsobbsegiArak == null || nemElsobbsegiArak == null || tobbletSzolgaltatasArak == null)
        {
            return;
        }

        StringBuilder js = new StringBuilder();
        js.Append(" var elsobbsegiArak = new Object(); ");

        foreach (KeyValuePair<string, string> kvp in elsobbsegiArak)
        {
            js.Append(" elsobbsegiArak.k_" + kvp.Key + "=" + kvp.Value + "; ");
        }

        js.Append(" nemElsobbsegiArak = new Object(); ");

        foreach (KeyValuePair<string, string> kvp in nemElsobbsegiArak)
        {
            js.Append(" nemElsobbsegiArak.k_" + kvp.Key + "=" + kvp.Value + "; ");
        }

        js.Append(" tobbletSzolgaltatasArak = new Object(); ");

        foreach (KeyValuePair<string, string> kvp in tobbletSzolgaltatasArak)
        {
            js.Append(" tobbletSzolgaltatasArak.k_" + kvp.Key + "=" + kvp.Value + "; ");
        }

        js.Append(@"
function getPrice(cb, prices, value, elsePrices)
{
    var k = 'k_' + value;
    if (cb && cb.checked == true && prices[k]) {
        return prices[k];
    }
    else if (elsePrices && elsePrices[k]) {
        return elsePrices[k];
    }
    return 0;
}

var tobbletSzolgaltatasElotag = '';
    
function getSvcPrice(cb, value, elsePrices)
{
    return getPrice(cb, tobbletSzolgaltatasArak, tobbletSzolgaltatasElotag + value, elsePrices);
}

function CalculatePrice() 
{
    var price = 0;
    var kimenoKuldFajta = $get('" + KimenoKuldFajta_KodtarakDropDownList.DropDownList.ClientID + @"');
              
    var kimenoKuldFajtaValue = '';
	if (kimenoKuldFajta != null && kimenoKuldFajta.selectedIndex != null && kimenoKuldFajta.selectedIndex != -1) {
	    kimenoKuldFajtaValue = kimenoKuldFajta.options[kimenoKuldFajta.selectedIndex].value;
    }
                
    var cb_elsobbsegi = $get('" + Elsobbsegi_CheckBox.ClientID + @"');
    price += getPrice(cb_elsobbsegi, elsobbsegiArak, kimenoKuldFajtaValue, nemElsobbsegiArak);

    var isKulfoldiOrszagViszonylatKod = kimenoKuldFajtaValue.startsWith('" + Constants.OrszagViszonylatKod.Europai + @"')
        || kimenoKuldFajtaValue.startsWith('" + Constants.OrszagViszonylatKod.EgyebKulfoldi + @"');
                
    if(isKulfoldiOrszagViszonylatKod)
        tobbletSzolgaltatasElotag = 'K_';

	var cb_ajanlott = $get('" + Ajanlott_CheckBox.ClientID + @"');
	var cb_tertiveveny = $get('" + Tertiveveny_CheckBox.ClientID + @"');
	var cb_sajatKezbe = $get('" + SajatKezbe_CheckBox.ClientID + @"');
	var cb_e_ertesites = $get('" + E_Ertesites_CheckBox.ClientID + @"');
	var cb_e_elorejelzes = $get('" + E_Elorejelzes_CheckBox.ClientID + @"');
	var cb_postaiLezaroSzolgalat = $get('" + PostaiLezaroSzolgalat_CheckBox.ClientID + @"');
    var cb_erteknyilvanitas = $get('" + Erteknyilvanitas_CheckBox.ClientID + @"');
    var tb_erteknyilvanitas = $get('" + Erteknyilvanitas_TextBox.ClientID + @"');

    var tobbletAr = 0;
	tobbletAr += getSvcPrice(cb_ajanlott, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.Ajanlott + @"');
	tobbletAr += getSvcPrice(cb_tertiveveny, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.Tertiveveny + @"');
	tobbletAr += getSvcPrice(cb_sajatKezbe, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.Sajat_kezbe + @"');
	tobbletAr += getSvcPrice(cb_e_ertesites, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.E_ertesites + @"');
	tobbletAr += getSvcPrice(cb_e_elorejelzes, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.E_elorejelzes + @"');
	tobbletAr += getSvcPrice(cb_postaiLezaroSzolgalat, '" + KodTarak.KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS.Postai_lezaro_szolgalat + @"');

    if (cb_erteknyilvanitas && cb_erteknyilvanitas.checked == true) {
        var enyOsszege = tb_erteknyilvanitas && tb_erteknyilvanitas.value ? parseInt(tb_erteknyilvanitas.value) : 0;
        if (enyOsszege < 0) { enyOsszege = 0; }
        var eny = '';
        if (enyOsszege > 150000) { eny = '150001'; }
        else if (enyOsszege > 100000) { eny = '150000'; }
        else if (enyOsszege > 75000) { eny = '100000'; }
        else if (enyOsszege > 50000) { eny = '75000'; }
        else if (enyOsszege > 40000) { eny = '50000'; }
        else if (enyOsszege > 30000) { eny = '40000'; }
        else if (enyOsszege > 20000) { eny = '30000'; }
        else if (enyOsszege > 10000) { eny = '20000'; }
        else if (enyOsszege > 0) { eny = '10000'; }

        if (eny) {
            var enySvcPrice = getSvcPrice(cb_erteknyilvanitas, 'K_ENY_' + eny);
            //console.debug('enyOsszege: ' + enyOsszege + ' eny: ' + eny + ' enySvcPrice:' + enySvcPrice);
            tobbletAr += enySvcPrice;
        }
    }

    if (tobbletAr > 0) {
        price += tobbletAr;
        //console.debug('tobbletAr=' + tobbletAr + ' price=' + price);
    }

    //alert('" + Peldanyszam_RequiredNumberBox.TextBox.ClientID + @"');
    var mennyisegTextBox = $get('" + Peldanyszam_RequiredNumberBox.TextBox.ClientID + @"');
    //alert(mennyisegTextBox);
    if (mennyisegTextBox && mennyisegTextBox.value != '') price *= mennyisegTextBox.value;                

    var arTextBox = $get('" + Ar_TextBox.ClientID + @"');
    // BLG_1721
    if (arTextBox) 
        arTextBox.value = price;
}

function KimenoKuldFajtaDropDown_OnChanged()
{
    var kimenoKuldFajta = $get('" + KimenoKuldFajta_KodtarakDropDownList.DropDownList.ClientID + @"');
    if (kimenoKuldFajta != null && kimenoKuldFajta.selectedIndex != null && kimenoKuldFajta.options[kimenoKuldFajta.selectedIndex] != null)
    {
            var kimenoKuldFajta_value = kimenoKuldFajta.options[kimenoKuldFajta.selectedIndex].value;

        var isKulfoldiOrszagViszonylatKod = false;
        if(kimenoKuldFajta_value.startsWith('" + Constants.OrszagViszonylatKod.Europai + @"')
            || kimenoKuldFajta_value.startsWith('" + Constants.OrszagViszonylatKod.EgyebKulfoldi + @"'))
        {
            isKulfoldiOrszagViszonylatKod = true;
        }

		    if (" + isHivatalosIrat + @")
            {
                SetCheckboxesToEnabledOrDisabled(true, true, isKulfoldiOrszagViszonylatKod);
            }
            else
            {
                SetCheckboxesToEnabledOrDisabled(false, false, isKulfoldiOrszagViszonylatKod);
            }
    }

    displayErteknyilvanitas();
    CalculatePrice();
}

function SetCheckboxesToEnabledOrDisabled(setToDisabled, clearCheckboxes, isKulfoldiOrszagViszonylatKod)
{
    var cb_elsobbsegi = $get('" + Elsobbsegi_CheckBox.ClientID + @"');
    var cb_ajanlott = $get('" + Ajanlott_CheckBox.ClientID + @"');
    var cb_tertiveveny = $get('" + Tertiveveny_CheckBox.ClientID + @"');
    var cb_sajatKezbe = $get('" + SajatKezbe_CheckBox.ClientID + @"');
    var cb_e_ertesites = $get('" + E_Ertesites_CheckBox.ClientID + @"');
    var cb_e_elorejelzes = $get('" + E_Elorejelzes_CheckBox.ClientID + @"');
    var cb_postaiLezaroSzolgalat = $get('" + PostaiLezaroSzolgalat_CheckBox.ClientID + @"');
    var cb_erteknyilvanitas = $get('" + Erteknyilvanitas_CheckBox.ClientID + @"');
    var tb_erteknyilvanitas = $get('" + Erteknyilvanitas_TextBox.ClientID + @"');

    if (cb_elsobbsegi) { cb_elsobbsegi.disabled = setToDisabled; }
    if (cb_ajanlott) { cb_ajanlott.disabled = setToDisabled; }
    if (cb_tertiveveny) { cb_tertiveveny.disabled = setToDisabled; }
    if (cb_sajatKezbe) { cb_sajatKezbe.disabled = setToDisabled; }
    if (cb_e_ertesites) { cb_e_ertesites.disabled = (setToDisabled || isKulfoldiOrszagViszonylatKod); }
    if (cb_e_elorejelzes) { cb_e_elorejelzes.disabled = (setToDisabled || isKulfoldiOrszagViszonylatKod); }
    if (cb_postaiLezaroSzolgalat) { cb_postaiLezaroSzolgalat.disabled = setToDisabled; }
    if (cb_erteknyilvanitas) { cb_erteknyilvanitas.disabled = setToDisabled; }
    if (tb_erteknyilvanitas) { tb_erteknyilvanitas.disabled = setToDisabled; }

    if (clearCheckboxes)
    {
        if (cb_elsobbsegi) { cb_elsobbsegi.checked = false; }
        if (cb_ajanlott) { cb_ajanlott.checked = false; }
        if (cb_tertiveveny) { cb_tertiveveny.checked = false; }
        if (cb_sajatKezbe) { cb_sajatKezbe.checked = false; }
        if (cb_postaiLezaroSzolgalat) { cb_postaiLezaroSzolgalat.checked = false; }
        if (cb_erteknyilvanitas) { cb_erteknyilvanitas.checked = false; }
        if (tb_erteknyilvanitas) { tb_erteknyilvanitas.value = ''; }
    }

    if(clearCheckboxes || isKulfoldiOrszagViszonylatKod)
    {
        if (cb_e_ertesites) { cb_e_ertesites.checked = false; }
        if (cb_e_elorejelzes) { cb_e_elorejelzes.checked = false; }
    }
}

function checkBoundCheckBoxes(cb)
{
	var cb_ajanlott = $get('" + Ajanlott_CheckBox.ClientID + @"');
	var cb_tertiveveny = $get('" + Tertiveveny_CheckBox.ClientID + @"');
 	if (cb_ajanlott && cb_tertiveveny)
	{
        if (cb == cb_ajanlott && cb_ajanlott.checked==false)
        {
            cb_tertiveveny.checked = false;
            CalculatePrice();
        }
        else if (cb == cb_tertiveveny && cb_tertiveveny.checked==true)
        {
            cb_ajanlott.checked = true;
            CalculatePrice();
        }
        else if (cb_tertiveveny.checked==true)
        {
            cb_ajanlott.checked = true;
            CalculatePrice();
        }
	}
}

function displayErteknyilvanitas() {
    var cb_erteknyilvanitas = $get('" + Erteknyilvanitas_CheckBox.ClientID + @"');
    var visible = cb_erteknyilvanitas && cb_erteknyilvanitas.checked;
    " + JavaScripts.VisibleIf("visible", Erteknyilvanitas_Label)
    + JavaScripts.VisibleIf("visible", Erteknyilvanitas_TextBox) +
@"}
");

        js.Append(@" function pageLoad(){
    checkBoundCheckBoxes(null);
	KimenoKuldFajtaDropDown_OnChanged();
                CalculatePrice();
               } ");

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "js_calculatePrice", js.ToString(), true);

        KimenoKuldFajta_KodtarakDropDownList.DropDownList.Attributes["onchange"] = "KimenoKuldFajtaDropDown_OnChanged();";

        var cp = "CalculatePrice();";
        Elsobbsegi_CheckBox.Attributes["onclick"] = cp;
        Ajanlott_CheckBox.Attributes["onclick"] = "checkBoundCheckBoxes(this);" + cp;
        Tertiveveny_CheckBox.Attributes["onclick"] = "checkBoundCheckBoxes(this);" + cp;
        SajatKezbe_CheckBox.Attributes["onclick"] = cp;
        E_Ertesites_CheckBox.Attributes["onclick"] = cp;
        E_Elorejelzes_CheckBox.Attributes["onclick"] = cp;
        PostaiLezaroSzolgalat_CheckBox.Attributes["onclick"] = cp;
        Erteknyilvanitas_CheckBox.InputAttributes.Add("onchange", "displayErteknyilvanitas();" + cp);
        Erteknyilvanitas_TextBox.Attributes["onkeyup"] = cp;
        Peldanyszam_RequiredNumberBox.TextBox.Attributes["onchange"] = cp;
    }

    // rádiógomb eseménykezelője:
    // tömeges kiküldésnél fel kell tenni a Darabszám TextBoxot
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue == "0")
        {
            // Ismert a címzett:
            RefreshComponents(Tipus.KimenoKuldemenyFelvitel_IsmertCimzett);
        }
        else if (RadioButtonList1.SelectedValue == "1")
        {
            // Tömeges kiküldés:
            RefreshComponents(Tipus.KimenoKuldemenyFelvitel_Tomeges);

            //LZS - BLG_5340 - Tömeges kiküldésnél a hivatalos iratok eltávolítása a listából.
            HivatalosIratokEltavolitasa();
        }
    }

    private enum Tipus
    {
        Postazas,
        KimenoKuldemenyFelvitel_IsmertCimzett,
        KimenoKuldemenyFelvitel_Tomeges
    }

    private void RefreshComponents(Tipus tipus)
    {
        string kimenoKuldemenyFajtaSelectedValue = KimenoKuldFajta_KodtarakDropDownList.SelectedValue;
        if (tipus == Tipus.KimenoKuldemenyFelvitel_IsmertCimzett)
        {
            tr_mennyiseg.Visible = false;
            //RegisterArKalkulatorJs();

            Cimzett_PartnerTextBox.Validate = true;
            CimzettCime_CimekTextBox.Validate = true;
            Label_Req_cimzett.Visible = true;
            Label_Req_cimzettCime.Visible = true;
            VonalKodTextBox1.ReadOnly = false;
            Ragszam_TextBox.ReadOnly = false;
            Tertiveveny_VonalkodTextBox.ReadOnly = false;
            VonalKodTextBox1.CssClass = "mrUrlapInput";
            Ragszam_TextBox.CssClass = "mrUrlapInput";
            Tertiveveny_VonalkodTextBox.CssClass = "mrUrlapInput";

            tr_Vonalkod.Visible = true;
            tr_Ragszam.Visible = true;
            // BLG_591
            //tr_TertivevenyVonalkod.Visible = true;
            tr_TertivevenyVonalkod.Visible = isTertivonalkod;


            tr_szolgaltatasok1.Visible = true;
            tr_szolgaltatasok2.Visible = true;

            // BUG_1336
            //tr_IratPldTreeView.Visible = true;
            //tr_IratPeldany.Visible = true;
            if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.POSTAZAS_IRATPELDANNYAL, false))
            {
                tr_IratPldTreeView.Visible = true;
                tr_IratPeldany.Visible = true;
            }
            else
            {
                tr_IratPldTreeView.Visible = false;
                tr_IratPeldany.Visible = false;
            }

            Fill_KimenoKuldFajta_KodtarakDropDownList(kimenoKuldemenyFajtaSelectedValue);


            //LZS - BLG_5946 - Postazasi panel fejlesztési problémák.
            Cimzett_PartnerTextBox.Enabled = true;
            CimzettCime_CimekTextBox.Enabled = true;
            eRecordComponent_ObjektumTargyszavaiPanel MetaAdatokPanel = (eRecordComponent_ObjektumTargyszavaiPanel)Parent.FindControl("otpTipusosTargyszavak_KimenoKuldemenyek");
            MetaAdatokPanel.ReadOnly = false;
            MetaAdatokPanel.ViewMode = true;

            // CR3284 : Tömeges postázásnál nem kell szegmentáltság-ellenőrzés
            RogzitesTipus = Mode.Simple;
        }
        else if (tipus == Tipus.KimenoKuldemenyFelvitel_Tomeges)
        {
            tr_mennyiseg.Visible = true;
            //RegisterArKalkulatorJs();

            Cimzett_PartnerTextBox.Validate = false;
            CimzettCime_CimekTextBox.Validate = false;

            Label_Req_cimzett.Visible = false;
            Label_Req_cimzettCime.Visible = false;

            VonalKodTextBox1.ReadOnly = true;
            Ragszam_TextBox.ReadOnly = true;
            Tertiveveny_VonalkodTextBox.ReadOnly = true;

            tr_Vonalkod.Visible = false;
            tr_Ragszam.Visible = false;
            tr_TertivevenyVonalkod.Visible = false;

            Ajanlott_CheckBox.Checked = false;
            Tertiveveny_CheckBox.Checked = false;
            SajatKezbe_CheckBox.Checked = false;
            E_Ertesites_CheckBox.Checked = false;
            E_Elorejelzes_CheckBox.Checked = false;
            PostaiLezaroSzolgalat_CheckBox.Checked = false;

            tr_szolgaltatasok1.Visible = false;
            tr_szolgaltatasok2.Visible = false;

            tr_IratPldTreeView.Visible = false;
            tr_IratPeldany.Visible = false;

            // tömeges kiküldésnél nem lehet hivatalos irat, mert az egyedileg nyilvántartott
            //KimenoKuldFajta_KodtarakDropDownList.FillDropDownList(kcs_KIMENO_KULDEMENY_FAJTA, false, null);
            foreach (string kod in HivatalosIratok)
            {
                ListItem li = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.FindByValue(kod);
                if (li != null) KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Remove(li);
            }


            // CR3284 : Tömeges postázásnál nem kell szegmentáltság-ellenőrzés
            RogzitesTipus = Mode.Multiple;

            //LZS - BUG_5946
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            eRecordComponent_ObjektumTargyszavaiPanel MetaAdatokPanel = (eRecordComponent_ObjektumTargyszavaiPanel)Parent.FindControl("otpTipusosTargyszavak_KimenoKuldemenyek");

            if (Rendszerparameterek.GetInt(execParam, Rendszerparameterek.POSTAZAS_TOMEGES_MEZO_LETILT, 1) == 1)
            {
                Cimzett_PartnerTextBox.Enabled = false;
                CimzettCime_CimekTextBox.Enabled = false;
                MetaAdatokPanel.ReadOnly = true;
                MetaAdatokPanel.ViewMode = true;

                //Törlés a felületről, ha esetleg van beleírva valami az ismert címzett fülről
                Cimzett_PartnerTextBox.Text = "";
                CimzettCime_CimekTextBox.Text = "";
                MetaAdatokPanel.ClearTextBoxValues();
            }
        }
    }

    protected void CheckRagszam(object source, ServerValidateEventArgs args)
    {
        string errormsg = null;

        bool isValid = args.IsValid;
        if (isValid)
        {
            bool isUnique = true;
            if (!String.IsNullOrEmpty(Ragszam_TextBox.Text))
            {
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();

                search.RagSzam.Filter(Ragszam_TextBox.Text);

                //if (!String.IsNullOrEmpty(KuldemenyId_HiddenField.Value))
                //{
                //    search.Id.NotEquals(KuldemenyId_HiddenField.Value);
                //}

                search.TopRow = 1;

                Result result = service.GetAll(execParam, search);
                if (!result.IsError)
                {
                    if (result.GetCount > 0)
                    {
                        if (result.Ds.Tables[0].AsEnumerable().Any(x => x["Id"].ToString().ToLower().Trim() != KuldemenyId_HiddenField.Value))
                            isUnique = false;
                    }
                }
            }

            if (!isUnique)
            {
                errormsg = "A megadott ragszám már hozzá van rendelve egy küldeményhez.";
                if (source is BaseValidator)
                {
                    ((BaseValidator)source).ErrorMessage = errormsg;
                }
            }

            isValid = isUnique;
        }
        args.IsValid = isValid;
    }

    public bool CheckVonalkod(string vonalkod, out string errormsg)
    {
        bool isValid = true;
        errormsg = null;
        if (!String.IsNullOrEmpty(vonalkod))
        {
            // TODO:
        }

        return isValid;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // ha van kiválasztva valami a treeView-ban, a megtekintés gombot be kell rá állítani
        if (TreeView1.SelectedNode != null)
        {
            int depth = TreeView1.SelectedNode.Depth;
            //string id = TreeView1.SelectedNode.Value;

            if (depth == 0)
            {
                string id = TreeView1.SelectedNode.Value;

                // irat
                Megtekintes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            }
            else if (depth == 1)
            {
                // iratPéldány:
                string id = GetTreeNodePldId(TreeView1.SelectedNode);

                Megtekintes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            }
            else
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }
        }
        else
        {
            Megtekintes.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }

        //LZS - BUG_10667
        if (Command != CommandName.View)
            RegisterArKalkulatorJs();


    }

    void cbEuropai_CheckedChanged(object sender, EventArgs e)
    {
        // BUG_8613
        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.CIM_SZEGMENTALTSAG_ELLENORZES, false))
        {
            UpdateViszonylatkod();
        }
        else
        {
            Fill_KimenoKuldFajta_KodtarakDropDownList(null);
            SetRagszamTextBoxByViszonylat(this.Viszonylatkod);
        }


    }

    void CimzettCime_FullCimField_OrszagTextChanged(object sender, EventArgs e)
    {

        //this.Viszonylatkod = CimzettCime_FullCimField.Orszag.Viszonylatkod;
        //Fill_KimenoKuldFajta_KodtarakDropDownList(null);
        // BUG_8613
        UpdateViszonylatkod();

    }

    void CimzettCime_CimekTextBox_TextChanged(object sender, EventArgs e)
    {
        UpdateViszonylatkod();
    }

    private void UpdateViszonylatkod()
    {
        // BUG_8613
        // Csak akkor van viszonylatellenörzés, ha a CIM_SZEGMENTALTSAG_ELLENORZES be van kapcslva
        if (Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.CIM_SZEGMENTALTSAG_ELLENORZES, false))
        {
            bool isValid = false;
            if (IsBelfoldi && (String.IsNullOrEmpty(CimzettCime_CimekTextBox.Orszag.Viszonylatkod)))
                isValid = true;
            if (IsEuropai && (CimzettCime_CimekTextBox.Orszag.Viszonylatkod == Constants.OrszagViszonylatKod.Europai))
                isValid = true;
            if (IsEgyebKulfoldi && (CimzettCime_CimekTextBox.Orszag.Viszonylatkod == Constants.OrszagViszonylatKod.EgyebKulfoldi))
                isValid = true;

            if (!isValid)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "viszonylatAlert", "alert('A viszonylat beállítás nem egyezik meg a címben beállított ország viszonylatával! A viszonylat a cím alapján kerül beállításra.');", true);
            }
        }
        this.Viszonylatkod = CimzettCime_CimekTextBox.Orszag.Viszonylatkod;

        Fill_KimenoKuldFajta_KodtarakDropDownList(null);
        // BUG_8613
        SetRagszamTextBoxByViszonylat(this.Viszonylatkod);


    }

    private bool IsBelfoldi
    {
        get
        {
            return rbBelfoldi.Checked;
        }
        set
        {
            rbBelfoldi.Checked = value;
        }
    }

    private bool IsEuropai
    {
        get
        {
            return rbEuropai.Checked;
        }
        set
        {
            rbEuropai.Checked = value;
        }
    }

    private bool IsEgyebKulfoldi
    {
        get
        {
            return rbEgyebKulfoldi.Checked;
        }
        set
        {
            rbEgyebKulfoldi.Checked = value;
        }
    }

    private string Viszonylatkod
    {
        get
        {
            if (IsEuropai)
                return Constants.OrszagViszonylatKod.Europai;

            if (IsEgyebKulfoldi)
                return Constants.OrszagViszonylatKod.EgyebKulfoldi;

            return String.Empty;
        }
        set
        {
            switch (value)
            {
                case Constants.OrszagViszonylatKod.Europai:
                    IsBelfoldi = false;
                    IsEuropai = true;
                    IsEgyebKulfoldi = false;
                    break;
                case Constants.OrszagViszonylatKod.EgyebKulfoldi:
                    IsBelfoldi = false;
                    IsEuropai = false;
                    IsEgyebKulfoldi = true;
                    break;
                default:
                    IsBelfoldi = true;
                    IsEuropai = false;
                    IsEgyebKulfoldi = false;
                    break;
            }
        }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel = null;
    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get
        {
            return errorPanel;
        }
        set
        {
            errorPanel = value;
        }
    }

    private void SetRagszamTextBoxByViszonylat(string viszonylatkod)
    {
        var isKulfold = viszonylatkod == Constants.OrszagViszonylatKod.Europai || viszonylatkod == Constants.OrszagViszonylatKod.EgyebKulfoldi;
        switch (RogzitesTipus)
        {
            case Mode.Simple:
                Ragszam_TextBox.IsKulfold = isKulfold;
                break;

            case Mode.Multiple:
                RagszamListBox1.IsKulfold = isKulfold;
                break;
        }
    }

    private void Fill_KimenoKuldFajta_KodtarakDropDownList(string SelectedValue)
    {
        string prevSelectedText = String.Empty;
        if (!String.IsNullOrEmpty(SelectedValue))
        {
            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(SelectedValue))
                this.Viszonylatkod = Constants.OrszagViszonylatKod.Europai;

            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(SelectedValue))
                this.Viszonylatkod = Constants.OrszagViszonylatKod.EgyebKulfoldi;

            if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(SelectedValue))
                this.Viszonylatkod = String.Empty;
        }
        else
        {
            if (KimenoKuldFajta_KodtarakDropDownList.DropDownList.SelectedItem != null)
            {
                prevSelectedText = KimenoKuldFajta_KodtarakDropDownList.DropDownList.SelectedItem.Text;
            }
        }

        List<string> filterList = GetKimenoKuldemenyFajta(this.Viszonylatkod);

        if (!String.IsNullOrEmpty(SelectedValue))
        {
            KimenoKuldFajta_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KIMENO_KULDEMENY_FAJTA,
                    SelectedValue, filterList, false, errorPanel);
        }
        else
        {
            KimenoKuldFajta_KodtarakDropDownList.FillDropDownList(kcs_KIMENO_KULDEMENY_FAJTA, filterList, false, this.errorPanel);
        }

        if (!String.IsNullOrEmpty(prevSelectedText))
        {
            ListItem prevSelectedItem = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.FindByText(prevSelectedText);
            if (prevSelectedItem != null)
            {
                prevSelectedItem.Selected = true;
            }
        }

        //LZS - BUG_7102 - Tömegesnél hivatalos irat eltávolítás
        if (!PostazasModTomeges && IsTomegesKikuldes) // BUG_9173: PostazasTomegesForm-nál nem, tömeges kiküldésnél hivatalos iratok eltávolítása
        {
            // hivatalos irat típusok eltávolítása a listából
            if (KuldesMod == KuldesModType.Sima || KuldesMod == KuldesModType.Ajanlott || KuldesMod == KuldesModType.Tertivevenyes)
            {
                HivatalosIratokEltavolitasa();
            }
        }

        FilterKuldemenyFajtaByKuldesMod();
    }

    private List<string> GetKimenoKuldemenyFajta(string Viszonylatkod)
    {
        List<string> filterList = new List<string>();

        Dictionary<String, String> kodtarak_Dictionary =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KIMENO_KULDEMENY_FAJTA, Page);

        foreach (KeyValuePair<String, String> kvp in kodtarak_Dictionary)
        {
            string kodtarKod = kvp.Key; // Key a kódtár kód
            string kodtarNev = kvp.Value; // Value a kódtárérték neve
            // BUG_4647
            if (!Postai_Spec_Alapjan.Contains(kodtarKod))
            {

                switch (Viszonylatkod)
                {
                    case Constants.OrszagViszonylatKod.Europai:
                        if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEuropai(kodtarKod))
                        {
                            filterList.Add(kodtarKod);
                        }
                        break;
                    case Constants.OrszagViszonylatKod.EgyebKulfoldi:
                        if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsEgyebKulfoldi(kodtarKod))
                        {
                            filterList.Add(kodtarKod);
                        }
                        break;
                    default:
                        if (KodTarak.KIMENO_KULDEMENY_FAJTA.IsBelfoldi(kodtarKod))
                        {
                            filterList.Add(kodtarKod);
                        }
                        break;
                }
            }
        }

        return filterList;
    }
    /// <summary>
    /// Pre fill ragszam parameters
    /// </summary>
    /// <param name="postakonyvId"></param>
    /// <param name="ragszam"></param>
    /// <param name="tertiBarcode"></param>
    /// <param name="kuldFajta"></param>
    public void FillFixRagSzamParameters(string postakonyvId, string ragszam, string tertiBarcode, string kuldFajta)
    {
        PostaKonyvekDropDownList.SelectedValue = postakonyvId;
        PostaKonyvekDropDownList.Enabled = false;

        Tertiveveny_VonalkodTextBox.Text = tertiBarcode;
        Tertiveveny_VonalkodTextBox.Enabled = false;
        Tertiveveny_VonalkodTextBox.ReadOnly = true;

        KimenoKuldFajta_KodtarakDropDownList.SelectedValue = kuldFajta;
        KimenoKuldFajta_KodtarakDropDownList.Enabled = false;

        Ragszam_TextBox.Text = ragszam;
        SetRagszamReadOnly();
    }

    public string KuldesModja
    {
        get
        {
            return KuldesMod_KodtarakDropDownList.SelectedValue;
        }
    }

    public void FillAndSetKuldesModja(string kuldesMod)
    {
        if (!String.IsNullOrEmpty(kuldesMod))
        {
            KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA, kuldesMod, false, errorPanel);
        }
    }

    /// <summary>v
    /// SetRagszamReadOnly
    /// </summary>s
    public void SetRagszamReadOnly()
    {
        Ragszam_TextBox.Enabled = false;
        Ragszam_TextBox.ReadOnly = true;
    }



    protected void KimenoKuldFajta_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public string KimenoKuldemenyFajta
    {
        get { return KimenoKuldFajta_KodtarakDropDownList.SelectedValue; }
    }

    string GetKikuldoCsoportId(string kuldemenyId)
    {
        EREC_IraIratokService iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
        iratokSearch.Id.Value = String.Format("select p.IraIrat_Id from EREC_PldIratPeldanyok p join EREC_Kuldemeny_IratPeldanyai kp on p.Id = kp.Peldany_Id where getdate() between kp.ErvKezd and kp.ErvVege and kp.KuldKuldemeny_Id='{0}'", kuldemenyId);
        iratokSearch.Id.Operator = Query.Operators.inner;

        Result result = iratokService.GetAll(execParam, iratokSearch);

        if (!result.IsError)
        {
            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                DataRow row = result.Ds.Tables[0].Rows[0];
                return GetKikuldoCsoportId(row["Csoport_Id_Ugyfelelos"].ToString(), row["FelhasznaloCsoport_Id_Ugyintez"].ToString());
            }
        }

        return String.Empty;
    }

    string GetKikuldoCsoportId(string csoport_Id_Ugyfelelos, string felhasznaloCsoport_Id_Ugyintez)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);

        //Felelős szervezet
        if (!String.IsNullOrEmpty(csoport_Id_Ugyfelelos))
        {
            return csoport_Id_Ugyfelelos;
        }

        //Intézkedő ügyintéző
        if (!String.IsNullOrEmpty(felhasznaloCsoport_Id_Ugyintez))
        {
            Contentum.eAdmin.Service.KRT_CsoportTagokService cstService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch cstSearch = new KRT_CsoportTagokSearch();
            cstSearch.Csoport_Id_Jogalany.Value = felhasznaloCsoport_Id_Ugyintez;
            cstSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result cstRes = cstService.GetAll(execParam, cstSearch);

            if (!cstRes.IsError && cstRes.Ds.Tables[0].Rows.Count > 0)
            {
                string csoport_id = cstRes.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                return csoport_id;
            }
        }

        return String.Empty;
    }

    void FilterKuldemenyFajtaByKuldesMod()
    {
        if (Rendszerparameterek.GetBoolean(Page, "POSTAZAS_KULDEMENY_FAJTA_ENABLED", false))
        {
            string vezerloKodcsoportKod = "KULDEMENY_KULDES_MODJA";
            string fuggoKodcsoportKod = "KIMENO_KULDEMENY_FAJTA";
            string kuldesModKod = KuldesMod_KodtarakDropDownList.SelectedValue;

            if (!String.IsNullOrEmpty(kuldesModKod))
            {
                bool hasNincsItem;
                List<string> engedeleyzettKuldemenyFajtak = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(UI.SetExecParamDefault(Page), vezerloKodcsoportKod, fuggoKodcsoportKod, kuldesModKod, out hasNincsItem);

                if (engedeleyzettKuldemenyFajtak != null && engedeleyzettKuldemenyFajtak.Count > 0)
                {
                    for (int i = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Count - 1; i >= 0; i--)
                    {
                        ListItem item = KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items[i];

                        if (!engedeleyzettKuldemenyFajtak.Contains(item.Value, StringComparer.CurrentCultureIgnoreCase))
                        {
                            KimenoKuldFajta_KodtarakDropDownList.DropDownList.Items.Remove(item);
                        }
                    }
                }
            }
        }
    }
}
