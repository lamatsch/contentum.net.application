﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RagszamListBox.ascx.cs"
    Inherits="eRecordComponent_RagszamListBox" %>
<%@ Register Src="VonalkodTextBox.ascx" TagName="VonalkodTextBox" TagPrefix="uc1" %>
<%@ Register Src="RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc1" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<table cellspacing="0" cellpadding="0" width="100%" align="center">
    <tr class="urlapSor">
        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelVonalkod" runat="server" Text="Vonalkód:"></asp:Label>
        </td>
        <td class="mrUrlapMezo">
            <uc1:VonalkodTextBox ID="VonalkodTextBoxVonalkod" runat="server" CssClass="mrUrlapInputKozepes"
                RequiredValidate="false" />
        </td>
        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelRagszam" runat="server" Text="Postai azonosító:" />
        </td>
        <td class="mrUrlapMezo">
            <uc1:RagszamTextBox ID="Ragszam_TextBox" runat="server" CssClass="mrUrlapInputKozepes"
                RequiredValidate="false" />
        </td>
        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelTertivevenyVonalkod" runat="server" Text="Tértivevény vonalkód:" />
        </td>
        <td class="mrUrlapMezo">
            <uc1:VonalkodTextBox ID="VonalkodTextBoxTertiveveny" runat="server" CssClass="mrUrlapInputKozepes"
                RequiredValidate="false" />
        </td>
        <td class="mrUrlapMezo">
            <asp:Image ID="ImageAdd" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                AlternateText="Hozzáadás a listához" ToolTip="Hozzáadás a listához" />
            <asp:CheckBox ID="cbSoundEffect" runat="server" ToolTip="Sikeres beolvasás hangjelzés" />
            <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender_SoundEffect" runat="server"
                TargetControlID="cbSoundEffect" UncheckedImageUrl="../images/hu/egyeb/sound_off.jpg"
                UncheckedImageAlternateText="Sikeres beolvasás hangjelzés bekapcsolása" CheckedImageUrl="../images/hu/egyeb/sound_on.jpg"
                CheckedImageAlternateText="Sikeres beolvasás hangjelzés kikapcsolása" ImageWidth="13"
                ImageHeight="13">
            </ajaxToolkit:ToggleButtonExtender>
        </td>
    </tr>
    <tr valign="top">
<%--        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelRagszamList" runat="server" Text="Beolvasott kódok:"></asp:Label>
        </td>--%>
        <td class="mrUrlapMezo" class="GridViewHeaderBackGroundStyle" colspan="6">
            <%--            <eUI:MultiRowSelectGridView ID="RagszamokGridView" runat="server" CellPadding="0"
                CellSpacing="0" BorderWidth="1" GridLines="Vertical" AllowPaging="true" PagerSettings-Visible="true"
                AllowSorting="false" AutoGenerateColumns="false" DataKeyNames="Id" Width="100%"
                PageSize="10">--%>
            <asp:Panel ID="GridViewPanel" runat="server" ScrollBars="Auto" Height="200px">
                <eUI:MultiRowSelectGridView ID="RagszamokGridView" runat="server" CellPadding="0"
                    CellSpacing="0" BorderWidth="1" GridLines="Vertical" AllowPaging="true" PagerSettings-Visible="false"
                    AllowSorting="false" AutoGenerateColumns="false" DataKeyNames="Id" Width="100%" PageSize="1000">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <asp:BoundField DataField="BarCode" HeaderText="Vonalkód" SortExpression="BarCode">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Ragszam" HeaderText="Postai azonosító" SortExpression="Ragszam">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TertivevenyVonalkod" HeaderText="Tértivevény vonalkód"
                            SortExpression="TertivevenyVonalkod">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                    </Columns>
                </eUI:MultiRowSelectGridView>
            </asp:Panel>
            <asp:HiddenField ID="hfRagszamClientState" runat="server" />
        </td>
        <td class="mrUrlapMezo">
            <asp:Image ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                AlternateText="Tétel kivétele a listából" ToolTip="Tétel kivétele a listából" />
            <br />
        </td>
    </tr>
</table>
