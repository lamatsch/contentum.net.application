﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_RagszamListBox : System.Web.UI.UserControl, IScriptControl//, Contentum.eUtility.ISearchComponent
{
    //private const int barcodeLength = 13; // a vonalkódok hossza

    private int maxElementsNumber = 500;

    public int MaxElementsNumber
    {
        get { return maxElementsNumber; }
        set { maxElementsNumber = value; }
    }

    public GridView GridView
    {
        get
        {
            return this.RagszamokGridView;
        }
    }

    public Unit GridViewPanelHeigth
    {
        get { return this.GridViewPanel.Height; }
        set { this.GridViewPanel.Height = value; }
    }

    public TextBox VonalkodTextBox
    {
        get
        {
            return this.VonalkodTextBoxVonalkod.TextBox;
        }
    }

    public TextBox RagszamTextBox
    {
        get { return this.Ragszam_TextBox.TextBox; }
    }

    public TextBox TertivevenyVonalkodTextBox
    {
        get
        {
            return this.VonalkodTextBoxTertiveveny.TextBox;
        }
    }

    public bool IsKulfold
    {
        get { return Ragszam_TextBox.IsKulfold; }
        set { Ragszam_TextBox.IsKulfold = value; }
    }

    public bool DetectKulfoldAutomatically
    {
        get { return Ragszam_TextBox.DetectKulfoldAutomatically; }
        set { Ragszam_TextBox.DetectKulfoldAutomatically = value; }
    }

    public bool RagszamVisible
    {
        get { return this.Ragszam_TextBox.Visible; }
        set
        {
            this.Ragszam_TextBox.Visible = value;
            this.labelRagszam.Visible = value;
            this.RagszamokGridView.Columns[2].Visible = value;
        }
    }

    public bool TertivevenyVonalkodVisible
    {
        get { return this.VonalkodTextBoxTertiveveny.TextBox.Visible; }
        set
        {
            this.VonalkodTextBoxTertiveveny.TextBox.Visible = value;
            this.labelTertivevenyVonalkod.Visible = value;
            this.RagszamokGridView.Columns[3].Visible = value;
        }
    }

    //public int Rows
    //{
    //    get
    //    {
    //        return RagszamokListBox.Rows;
    //    }
    //    set
    //    {
    //        RagszamokListBox.Rows = value;
    //    }
    //}

    public string RagszamClientList
    {
        get
        {
            return hfRagszamClientState.Value.Trim();
        }
        set
        {
            hfRagszamClientState.Value = value;
        }
    }

    public string[] CheckBoxClientIds
    {
        get
        {
            if (ViewState["CheckBoxClientIds"] != null)
                return (string[])ViewState["CheckBoxClientIds"];
            return null;
        }

        set { ViewState["CheckBoxClientIds"] = value; }
    }

    public List<string> VonalkodList
    {
        get {
            List<string> lst = new List<string>();
            if (!String.IsNullOrEmpty(this.RagszamClientList))
            {
                foreach (string element in this.RagszamClientList.Split(new char[] { ';' }))
                {
                    if (!String.IsNullOrEmpty(element))
                    {
                        string[] items = element.Split(new char[] { ',' });
                        lst.Add(items[0]);
                    }
                }
            }

            return lst;
        }
    }

    public Dictionary<string, string> VonalkodRagszamDictionary
    {
        get
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(this.RagszamClientList))
            {
                foreach (string element in this.RagszamClientList.Split(new char[] { ';' }))
                {
                    if (!String.IsNullOrEmpty(element))
                    {
                        string[] items = element.Split(new char[] { ',' });
                        dict.Add(items[0], items.Length > 1 ? items[1] : String.Empty);
                    }
                }
            }

            return dict;
        }
    }

    public Dictionary<string, string> VonalkodTertivevenyVonalkodDictionary
    {
        get
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(this.RagszamClientList))
            {
                foreach (string element in this.RagszamClientList.Split(new char[] { ';' }))
                {
                    if (!String.IsNullOrEmpty(element))
                    {
                        string[] items = element.Split(new char[] { ',' });
                        dict.Add(items[0], items.Length > 2 ? items[2] : String.Empty);
                    }
                }
            }

            return dict;
        }
    }

    //private int maxSearchTextElementsCount = 3;
    ///// <summary>
    ///// Legfeljebb ennyi vonalkód elem kerül a GetSearchText által generált kimenetbe.
    ///// Ha ennél több vonalkód is meg van adva, azt "..." jelzi a szöveg végén
    ///// </summary>
    //public int MaxSearchTextElementsCount
    //{
    //    get { return maxSearchTextElementsCount; }
    //    set
    //    {
    //        if (value < 0) value = 0;
    //        maxSearchTextElementsCount = value;
    //    }
    //}

    public void Reset()
    {
        //RagszamokListBox.Items.Clear();
        RagszamokGridView.DataSource = null;
        RagszamokGridView.DataBind();
        hfRagszamClientState.Value = String.Empty;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
    }

    private void BindData()
    {
        RagszamokGridView.DataSource = GetDataTable();
        RagszamokGridView.DataBind();
        if (((DataTable)RagszamokGridView.DataSource).Rows[0]["Id"].ToString() == "x")
        {
            RagszamokGridView.Rows[0].Style.Add("display", "none");
        }
    }

    private DataTable GetDataTable()
    {
        string[] barcodeArray = hfRagszamClientState.Value.Split(new char[] { ';' });

        string[] columns = new string[] {"Id", "BarCode", "Ragszam", "TertivevenyVonalkod"};

        DataTable tb = new DataTable();
        foreach (string colName in columns)
        {
            tb.Columns.Add(colName);
        }
        tb.PrimaryKey = new DataColumn[] { tb.Columns["Id"] };

        // mintasor a javascripttel való másoláshoz (formázás stb.), el lesz rejve
        DataRow dummyRow = tb.NewRow();
        dummyRow["Id"] = "x";
        tb.Rows.Add(dummyRow);

        foreach (string barcode in barcodeArray)
        {
            if (!String.IsNullOrEmpty(barcode))
            {
                string[] items = barcode.Split(new char[] { ',' });
                DataRow row = tb.NewRow();
                row["Id"] = items[0];
                row["BarCode"] = items[0];
                for (int i = 1; i < items.Length; i++)
                {
                    row[i + 1] = items[i];
                }
                if (items.Length > 1)
                {
                    row["Ragszam"] = items[1];

                    if (items.Length > 2)
                    {
                        row["TertivevenyVonalkod"] = items[2];
                    }
                }
                tb.Rows.Add(row);
            }
        }
        return tb;
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.BarcodeRagszamListaBehavior", this.RagszamokGridView.ClientID);
        descriptor.AddProperty("BarCodeBehaviorId", VonalkodTextBoxVonalkod.BehaviorID);
        if (TertivevenyVonalkodVisible)
        {
            descriptor.AddProperty("TertivevenyVonalkodBehaviorId", VonalkodTextBoxTertiveveny.BehaviorID);
        }
        if (RagszamVisible)
        {
            //descriptor.AddProperty("RagszamTextBoxClientId", Ragszam_TextBox.ClientID);
            descriptor.AddProperty("RagszamBehaviorId", Ragszam_TextBox.BehaviorID);
        }
        descriptor.AddProperty("Tipus", TertivevenyVonalkodVisible ? "3" : (RagszamVisible ? "2" : "1"));
        descriptor.AddProperty("CheckBoxClientIds", this.CheckBoxClientIds);
        descriptor.AddProperty("ImageAddId", ImageAdd.ClientID);
        descriptor.AddProperty("ImageDeleteId", DeSelectingRowsImageButton.ClientID);
        descriptor.AddProperty("HiddenFieldClientStateId", hfRagszamClientState.ClientID);
        descriptor.AddProperty("MaxElementsNumber", MaxElementsNumber);
        descriptor.AddProperty("ListIsFullMessage", Resources.Form.UI_ListIsFull);
        descriptor.AddProperty("CbSoundEffectId", cbSoundEffect.ClientID);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/BarcodeRagszamLista.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    public void MarkFailedRecords(List<string> vonalkodok)
    {
        if (vonalkodok != null && vonalkodok.Count > 0)
        {
            foreach (GridViewRow grv in RagszamokGridView.Rows)
            {
                if ((grv.RowType == DataControlRowType.DataRow) && (vonalkodok.Contains(RagszamokGridView.DataKeys[grv.RowIndex].Value.ToString())))
                {
                    grv.BackColor = System.Drawing.Color.Red;
                }
            }
        }
    }

    //#region ISearchComponent Members

    //public string GetSearchText()
    //{
    //    string ragszamok = RagszamClientList;
    //    int length = maxSearchTextElementsCount * barcodeLength;
    //    if (maxSearchTextElementsCount > 0)
    //    {
    //        // vesszők
    //        length += maxSearchTextElementsCount;   // belevesszük az utolsó vesszőt is, ezért nem maxSearchTextElementsCount-1
    //    }

    //    if (ragszamok.Length > length)
    //    {
    //        ragszamok = ragszamok.Substring(0, length) + "...";
    //    }

    //    return ragszamok;
    //}

    //#endregion
}
