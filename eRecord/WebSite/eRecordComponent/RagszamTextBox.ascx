<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RagszamTextBox.ascx.cs"
    Inherits="eRecordComponent_RagszamTextBox" %>
<asp:HiddenField ID="hfIsKulfold" runat="server" Value="false" />
<asp:HiddenField ID="hfDetectKulfoldAutomatically" runat="server" Value="false" />
<asp:TextBox ID="ragszamTextBox" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="ragszamTextBox"
    SetFocusOnError="true" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<asp:CustomValidator ID="CustomValidatorBarcodeCheckSum" runat="server" ControlToValidate="ragszamTextBox"
    Display="None" SetFocusOnError="true" OnServerValidate="CustomValidator_CheckSum_ServerValidate" />
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderBarcodeCheckSum"
    runat="server" TargetControlID="CustomValidatorBarcodeCheckSum">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
