using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using System.Collections.Generic;
using System.Linq;

public partial class eRecordComponent_RagszamTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    private const string bgSoundHtmlId = "SoundContainer";

    #region public events
    public event ServerValidateEventHandler OnServerValidate;
    #endregion public events

    #region public properties

    public bool IsKulfold
    {
        get { return GetBooleanValueFromHiddenField(hfIsKulfold, false); }
        set { SetBooleanValueToHiddenField(hfIsKulfold, value); }
    }

    public bool DetectKulfoldAutomatically
    {
        get { return GetBooleanValueFromHiddenField(hfDetectKulfoldAutomatically, false); }
        set { SetBooleanValueToHiddenField(hfDetectKulfoldAutomatically, value); }
    }

    private bool GetBooleanValueFromHiddenField(HiddenField hf, bool defaultValue)
    {
        bool boolValue = false;
        if (Boolean.TryParse(hf.Value, out boolValue))
        {
            return boolValue;
        }
        return defaultValue;
    }

    private void SetBooleanValueToHiddenField(HiddenField hf, bool value)
    {
        hf.Value = value ? "true" : "false";
    }

    private bool validate = true;

    public bool Validate
    {
        set 
        { 
            Validator1.Enabled = value;
            CustomValidatorBarcodeCheckSum.Enabled = value;
            validate = value;
        }
        get { return validate; }
    }

    public bool RequiredValidate
    {
        get { return Validator1.Enabled; }
        set { Validator1.Enabled = value; }
    }

    public bool FormatValidate
    {
        get { return CustomValidatorBarcodeCheckSum.Enabled; }
        set { CustomValidatorBarcodeCheckSum.Enabled = value; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    // ClientID t�mb pl. javascriptb�l val� letilt�shoz
    public string[] ValidatorClientIDs
    {
        get {
            string[] result = new string[2];
            result[0] = Validator1.ClientID;
            result[1] = CustomValidatorBarcodeCheckSum.ClientID;
            return result;
        }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox.Enabled = value;

        }
        get { return TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            TextBox.ReadOnly = value;
            if (value == true)
            {
                Validate = false;
            }
        }
        get { return TextBox.ReadOnly; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Text
    {
        set { TextBox.Text = value; }
        get { return TextBox.Text; }
    }

    public TextBox TextBox
    {
        get { return ragszamTextBox; }
    }


    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
        }
    }


    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
            }
        }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }
    
    private string postKeyUpEventJs = "";
    public string PostKeyUpEventJs
    {
        get { return postKeyUpEventJs; }
        set { postKeyUpEventJs = value; }
    }

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }
    
    // JavaScript sound effects
    private bool _playSoundOnError = false;
    public bool PlaySoundOnError 
    {
        get { return _playSoundOnError;}
        set { _playSoundOnError = value;}
    }

    private bool _playSoundOnAccept = false;
    public bool PlaySoundOnAccept 
    {
        get { return _playSoundOnAccept;}
        set { _playSoundOnAccept = value;}
    }

    private string _soundOnError = "sounds/barcodeOnError.wav";
    public string SoundOnError
    {
        get { return _soundOnError; }
        set { _soundOnError = value; }
    }

    private string _soundOnAccept = "sounds/barcodeOnAccept.wav";
    public string SoundOnAccept
    {
        get { return _soundOnAccept; }
        set { _soundOnAccept = value; }
    }

     #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlGenericControl bgsound = (HtmlGenericControl)Page.Header.FindControl(bgSoundHtmlId);
        if (bgsound == null)
        {
            bgsound = new HtmlGenericControl("bgsound");
            bgsound.Attributes.Add("id", bgSoundHtmlId);
            bgsound.Attributes.Add("src", "");
            bgsound.Attributes.Add("loop", "1");
            bgsound.ID = bgSoundHtmlId;
            Page.Header.Controls.Add(bgsound);
        }
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        string js = String.Empty;
        if (postKeyUpEventJs.Length > 0)
        {
            js += postKeyUpEventJs;
        }
        TextBox.Attributes.Add("onkeyup", js);
    }


    protected void CustomValidator_CheckSum_ServerValidate(object source, ServerValidateEventArgs arguments)
    {
        string errormsg = null;
        string ragszam = arguments.Value;
        arguments.IsValid = true;

        if (String.IsNullOrEmpty(ragszam))
        {
            return;
        }

        // K�lf�ld
        if (IsKulfold)
            {
                #region Specification
                // K�lf�ldi azonos�t�
                // Szerkezet: aj�nlott, �rt�klev�l
                // 1-2.	    K�ldem�nyfajta azonos�t� (RR)	    Karakteres (2)
                // 3-10.	Egyedi sorsz�m	                    Sz�m (8)
                // 11.	    CDV ellen�rz�sz�m	                Sz�m (1)
                // 12-13.	Sz�rmaz�si orsz�g ISO-2 k�dja  (HU)	Karakteres (2)

                // Az egyedi sorsz�m (3-10.) CDV ellen�rz�sz�m k�pz�s�nek algoritmusa (8 karak-terre):
                // -	az ellen�rizend� sz�mot rendre meg kell szorozni a 8, 6, 4, 2, 3, 5, 9, 7 s�lyt�-nyez�kkel,
                // -	a szorzatokat �ssze kell adni,
                // -	az eredm�nnyel el kell v�gezni a MOD 11-es m�veletet,
                // -	a marad�kot ki kell vonni 11-b�l, az �gy kapott sz�m lesz az ellen�rz�sz�m, ki-v�ve
                // -	ha a marad�k = 0, akkor a CDV = 5
                // -	ha a marad�k = 1, akkor a CDV = 0

                // K�lf�ldi aj�nlott lev�l: RR
                // �rt�klev�l: VV
                #endregion Specification

                int ragszamSpecifiedLength = 13;

                if (ragszam.Length != ragszamSpecifiedLength)
                {
                    arguments.IsValid = false;
                    //"A nemzetk�zi k�ldem�nyazonos�t� (ragsz�m) hossza nem megfelel�! V�rt hossz: {0} - T�nyleges hossz: {1}"
                    errormsg = String.Format(Resources.Error.ErrorCode_52523, ragszamSpecifiedLength, ragszam.Length);
                    ((BaseValidator)source).ErrorMessage = errormsg;
                    return;
                }

                string pattern = @"^((RR|VV)[0-9]{9}HU)$";
                System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(pattern);
                if (!re.IsMatch(ragszam))
                {
                    arguments.IsValid = false;
                    //"A nemzetk�zi k�ldem�nyazonos�t� form�tuma hib�s!"
                    errormsg = Resources.Error.ErrorCode_52524;
                    ((BaseValidator)source).ErrorMessage = errormsg;
                    return;
                }
                else
                {
                    int check = Int32.Parse(ragszam.Substring(10, 1));
                    int[] vector = new int[] { 8, 6, 4, 2, 3, 5, 9, 7 };
                    int sum = 0;
                    for (int i = 2; i < 10; i++)
                    {
                        sum += Int32.Parse(ragszam.Substring(i, 1)) * vector[i - 2];
                    }

                    int modresult = sum % 11;
                    int calculatedcheck = -1;
                    switch (modresult)
                    {
                        case 0:
                            calculatedcheck = 5;
                            break;
                        case 1:
                            calculatedcheck = 0;
                            break;
                        default:
                            calculatedcheck = 11 - modresult;
                            break;
                    }

                    if (calculatedcheck != check)
                    {
                        arguments.IsValid = false;
                        //"A nemzetk�zi k�ldem�nyazonos�t� ellen�rz� �sszege hib�s! Ellen�rz� jegy: {0} - Sz�m�tott: {1}"
                        errormsg = String.Format(Resources.Error.ErrorCode_52525, check, calculatedcheck);
                        ((BaseValidator)source).ErrorMessage = errormsg;
                        return;
                    }
                }
            }
            // Belf�ld
            else
            {
                #region Specification
                // Belf�ldi azonos�t�
                //Szerkezet:
                //1-2.	K�ldem�nyfajta azonos�t�	Karakter (2)
                //3-6.	Felvev� posta ir�ny�t�sz�ma	Sz�m (4)
                //7-15.	Egyedi sorsz�m	            Sz�m (9)
                //16.	CDV ellen�rz�sz�m	        Sz�m (1)

                // A CDV ellen�rz�sz�mot kiz�r�lag az azonos�t� �egyedi sorsz�m� r�sz�nek (7-15. poz�ci�) 9 numerikus karakter�re kell k�pezni az al�bbi algoritmus szerint:
                // -	a p�ratlan helyi�rt�k� sz�mokat balr�l jobbra haladva 3-mal kell megszorozni (1. l�p�s),
                // -	a p�ros helyi�rt�k� sz�mokat balr�l jobbra haladva 1-gyel kell megszorozni (1. l�p�s), 
                // -	a szorzatokat �ssze kell adni (2. l�p�s),
                // -	az eredm�nnyel el kell v�gezni a MOD10-es m�veletet (3. l�p�s).

                //Aj�nlott: RL
                //�rt�knyilv�n�tott k�ldem�ny: EL
                //Aj�nlott v�laszk�ldem�ny: KR
                //�rt�knyilv�n�tott v�laszk�ldem�ny: RB

                // Alapszolg�ltat�s:
                // V�laszlev�l	A_161_LEV
                // V�lasz levelez�lap	A_161_LLP
                // K�l�nszolg�ltat�s
                // Aj�nlott	K_AJN
                // �rt�knyilv�n�t�s 	K_ENY
                // �rt�knyilv�n�tott v�laszk�ldem�ny K_BAJ
                #endregion Specification

                int ragszamSpecifiedLength = 16;

                if (ragszam.Length != ragszamSpecifiedLength)
                {
                    arguments.IsValid = false;
                    //"A belf�ldi k�ldem�nyazonos�t� (ragsz�m) hossza nem megfelel�! V�rt hossz: {0} - T�nyleges hossz: {1}"
                    errormsg = String.Format(Resources.Error.ErrorCode_52526, ragszamSpecifiedLength, ragszam.Length);
                    ((BaseValidator)source).ErrorMessage = errormsg;
                    return;
                }

                string pattern = @"^((RL|EL|KR|RB)[0-9]{14})$";

                System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(pattern);
                if (!re.IsMatch(ragszam))
                {
                    arguments.IsValid = false;
                    //"A belf�ldi k�ldem�nyazonos�t� form�tuma hib�s!"
                    errormsg = Resources.Error.ErrorCode_52527;
                    ((BaseValidator)source).ErrorMessage = errormsg;
                    return;
                }
                else
                {
                    int check = Int32.Parse(ragszam.Substring(15, 1));
                    int sum = 0;
                    for (int i = 6; i < 15; i++)
                    {
                        sum += Int32.Parse(ragszam.Substring(i, 1)) * (i % 2 == 0 ? 3 : 1);
                    }

                    int calculatedcheck = sum % 10;
                    if (calculatedcheck != check)
                    {
                        arguments.IsValid = false;
                        //"A belf�ldi k�ldem�nyazonos�t� ellen�rz� �sszege hib�s! Ellen�rz� jegy: {0} - Sz�m�tott: {1}"
                        errormsg = String.Format(Resources.Error.ErrorCode_52528, check, calculatedcheck);
                        ((BaseValidator)source).ErrorMessage = errormsg;
                        return;
                    }
                }
            }

        if (OnServerValidate != null)
        {
                this.OnServerValidate(source, arguments);
        }
        return;
    }

    // BUG_10649
    public bool CheckRagszamRequired(string KuldesModSelectedValue)
    {
        Boolean required = false;
        string kuldemenyKuldesModjaGuid = "";
        string PostaiAzonositoRequiredGuid = "";
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        kuldemenyKuldesModjaGuid = Contentum.eRecord.Utility.KodTarak.GetKodTarId(execParam, KuldesModSelectedValue, " and KRT_KodCsoportok.Kod='KULDEMENY_KULDES_MODJA' ");
        PostaiAzonositoRequiredGuid = Contentum.eRecord.Utility.KodTarak.GetKodTarId(execParam, "IGEN", " and KRT_KodCsoportok.Kod='POSTAI_AZONOSITO_KOTELEZO' ");

        Contentum.eAdmin.Service.KRT_KodtarFuggosegService kodservice = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        Contentum.eQuery.BusinessDocuments.KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "KULDEMENY_KULDES_MODJA");
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "POSTAI_AZONOSITO_KOTELEZO");
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

        Result res = kodservice.GetAll(execParam, search);

        if (!res.IsError)
        {
            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                List<string> fuggoKodtarak = new List<string>();


                var requireditem = fuggosegek.Items.FirstOrDefault(x => (x.VezerloKodTarId == kuldemenyKuldesModjaGuid && (x.FuggoKodtarId == PostaiAzonositoRequiredGuid)));

                required = (requireditem != null);

            }
        }
        return required;

    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(ragszamTextBox);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);

            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    c.Visible = Validate;
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.RagszamBehavior", this.TextBox.ClientID);
        descriptor.AddProperty("IsKulfold", IsKulfold);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("AutomaticFormatDetection", DetectKulfoldAutomatically);
        descriptor.AddProperty("ChecksumValidatorId", CustomValidatorBarcodeCheckSum.ClientID);
        descriptor.AddProperty("ChecksumValidatorCalloutId", ValidatorCalloutExtenderBarcodeCheckSum.ClientID);

        HtmlGenericControl bgsound = (HtmlGenericControl)Page.Header.FindControl(bgSoundHtmlId);
        if (bgsound != null)
        {
            descriptor.AddProperty("BgSoundId", bgsound.ClientID);
            //descriptor.AddProperty("PlaySoundOnError", false);
            //descriptor.AddProperty("PlaySoundOnAccept", false);
            descriptor.AddProperty("SoundOnError", SoundOnError);
            descriptor.AddProperty("SoundOnAccept", SoundOnAccept);
        }

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/RagszamBehavior.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
