﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SablonDokumentumokList.ascx.cs" Inherits="eRecordComponent_DokumentumokList" %>

<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register src="DokumentumHierarchia.ascx" tagname="DokumentumHierarchia" tagprefix="uc2" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="MessageHiddenField" runat="server" />

<table width="100%" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    <tr>
        <td style="width:10px"></td>
        <td>
            <eUI:eFormPanel ID="TipusPanel" runat="server" Visible="false">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="labelTipus" runat="server" Text="Típus:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_Tipus" runat="server" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
        </td>
    </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DokumentumokCPEButton" ImageUrl="../images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="DokumentumokUpdatePanel" runat="server" OnLoad="DokumentumokUpdatePanel_Load">
<%--                                <Triggers>
                <asp:PostBackTrigger ControlID="KodtarakDropDownList_Tipus" />
                </Triggers>--%>
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="DokumentumokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="DokumentumokCPEButton"
                            CollapseControlID="DokumentumokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif" ExpandedImage="../images/hu/Grid/minus.gif"
                            ImageControlID="DokumentumokCPEButton" ExpandedSize="0" ExpandedText="Dokumentumok listája"
                            CollapsedText="Dokumentumok listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server" style="padding-bottom:10px">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:MultiView ID="multiViewGridViews" runat="server" ActiveViewIndex="0">
                                            <asp:View ID="viewDokumentumok" runat="server">
                                                <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                                <asp:GridView ID="DokumentumokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                    OnRowCommand="DokumentumokGridView_RowCommand" OnPreRender="DokumentumokGridView_PreRender"
                                                    OnSorting="DokumentumokGridView_Sorting" OnRowDataBound="DokumentumokGridView_RowDataBound"
                                                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <div class="DisableWrap">
                                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                </div>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                            SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:TemplateField HeaderText="Állomány" SortExpression="KRT_Dokumentumok.FajlNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelFileName" runat="server" Text='<%# Eval("FajlNev") %>'>                                                         
                                                                </asp:Label>
                                                                <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                                                    ImageUrl="../images/hu/fileicons/default.gif" ToolTip="Megnyitás" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Verzió" SortExpression="KRT_Dokumentumok.VerzioJel">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                                            <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="linkVerzio" runat="server" NavigateUrl='<%# Contentum.eRecord.Utility.Dokumentumok.GetVerziokLink(Eval("Id"),Eval("VerzioJel")) %>'
                                                                    Text='<%# Eval("VerzioJel") %>' ToolTip="Korábbi verziók megtekintése" Visible='<%# Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                                                <asp:Label ID="labelVerzio" runat="server" Text='<%# Eval("VerzioJel") %>' Visible='<%# !Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Formatum" HeaderText="Formátum" SortExpression="KRT_Dokumentumok.Formatum">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Tipus" HeaderText="Típus" SortExpression="KRT_Dokumentumok.Tipus>
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Meret" HeaderText="Méret" SortExpression="KRT_Dokumentumok.Meret">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LetrehozasIdo" HeaderText="Feltöltve" SortExpression="KRT_Dokumentumok.LetrehozasIdo">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                       
                                                        <asp:TemplateField HeaderText="Érvényesség" SortExpression="KRT_Dokumentumok.ErvVege" Visible="false">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelErvenyesseg" runat="server" Text='<%# GetErvenyesseg(Eval("ErvKezd") as DateTime?, Eval("ErvVege") as DateTime?) %>'  />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
<%--                                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                            <HeaderTemplate>
                                                                <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </asp:View>
                                            <asp:View ID="viewDokumentumokVersions" runat="server">
                                                <div>
                                                    <asp:DropDownList ID="ddListVerzioTol" runat="server" DataTextField="version" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    -
                                                    <asp:DropDownList ID="ddListVerzioIg" runat="server" DataTextField="version" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:GridView ID="DokumentumokGridViewVersions" runat="server" CellPadding="0" CellSpacing="0"
                                                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                    OnRowCommand="DokumentumokGridView_RowCommand" OnPreRender="DokumentumokGridView_PreRender"
                                                    OnSorting="DokumentumokGridView_Sorting" OnRowDataBound="DokumentumokGridView_RowDataBound"
                                                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Állomány">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelFileName" runat="server" Text='<%# Eval("name") %>'>                                                         
                                                                </asp:Label>
                                                                <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                                                    ImageUrl="../images/hu/fileicons/default.gif" ToolTip="Megnyitás" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="version" HeaderText="Verzió" SortExpression="version">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="size" DataFormatString="{0}&nbsp;Kb" HeaderText="Méret" SortExpression="size">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="created" HeaderText="Létrehozva" SortExpression="created">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="comments" HeaderText="Megjegyzések" SortExpression="comments">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr id="trSublists" runat="server">
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="../images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="../images/hu/Grid/plus.gif"
                                ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
                                    <ajaxToolkit:TabPanel ID="CsatolmanyokTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelIdeIktatok" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Header" runat="server" Text="Kapcsolódó elemek"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="CsatolmanyokUpdatePanel" runat="server" OnLoad="CsatolmanyokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="CsatolmanyokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:sublistheader id="CsatolmanyokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CsatolmanyokCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <asp:GridView ID="CsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="CsatolmanyokGridView_Sorting"
                                                                            OnPreRender="CsatolmanyokGridView_PreRender" OnRowCommand="CsatolmanyokGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <%--<asp:BoundField DataField="Azonosito" HeaderText="Azonosító" SortExpression="Azonosito">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField> --%>
                                                                                <asp:TemplateField HeaderText="Azonosító" SortExpression="Azonosito">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Label23" runat="server" Text='<%#                                        
                                                                                            string.Concat(                                                                                                        
                                                                                              (Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_IraIratok ? "<a href=\"IraIratokList.aspx?Id="+ Eval("IraIrat_Id") as string + "\" target=\"_self\" style=\"text-decoration:underline\">" : ""                                                                                             
                                                                                             ,(Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek ? "<a href=\"KuldKuldemenyekList.aspx?Id="+ Eval("KuldKuldemeny_Id") as string + "\" target=\"_self\" style=\"text-decoration:underline\">" : ""
                                                                                             , Eval("Azonosito")
                                                                                             , ((Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_IraIratok
                                                                                               || (Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek) ? "<a />" : "") 
                                                                                             %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Típus" SortExpression="Azonosito_Tipus">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Label24" runat="server" Text='<%#                                        
                                                                                            string.Concat(                                                                                                        
                                                                                              (Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_IraIratok ? "Irat" : ""                                                                                             
                                                                                             ,(Eval("Azonosito_Tipus") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek ? "Küldemény" : "") 
                                                                                             %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Leiras" HeaderText="Megjegyzés" SortExpression="Leiras">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Label_Azonosito_Tipus" runat="server" Text='<%# Eval("Azonosito_Tipus") %>' /></ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Label_IraIrat_Id" runat="server" Text='<%# Eval("IraIrat_Id") %>' /></ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="Label_KuldKuldemeny_Id" runat="server" Text='<%# Eval("KuldKuldemeny_Id") %>' /></ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="OsszetettDokuKapcsolatokTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="UpdatePanel_Label" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text="Összetett dokumentum kapcsolatok"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>                                            
                                            <uc2:DokumentumHierarchia ID="DokumentumHierarchia1" runat="server" />
                                        </ContentTemplate>
                                   </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>    