﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_DokumentumokList : System.Web.UI.UserControl
{
    private ASP.component_listheader_ascx ListHeader1;

    public ASP.component_listheader_ascx ListHeader
    {
        get { return this.ListHeader1; }
        set { this.ListHeader1 = value; }
    }

    private ASP.component_sublistheader_ascx SubListHeader1;

    public ASP.component_sublistheader_ascx SubListHeader
    {
        get { return this.SubListHeader1; }
        set { this.SubListHeader1 = value; }
    }

    private ScriptManager ScriptManager1;

    public ScriptManager ScriptManager
    {
        get { return this.ScriptManager1; }
        set { this.ScriptManager1 = value; }
    }

    private string Startup = String.Empty;  //üres (csatolmányok), Standalone (különálló dokumentumok)
    private string DokumentumTipus = String.Empty;  //Szakági nyilvántartások szűréséhez
    private string DokumentumTipusNev = String.Empty; // fejlécekbe való kiíráshoz

    private const string kcs_DOKUMENTUMTIPUS = "DOKUMENTUMTIPUS";

    UI ui = new UI();

    private const string funkcio_DokumentumokModify = "DokumentumokModify";
    private const string funkcio_DokumentumokView = "DokumentumokView";
    private const string funkcio_DokumentumokList = "DokumentumokList";
    private const string funkcio_DokumentumokLock = "DokumentumokLock";
    private const string funkcio_DokumentumokForceLock = "DokumentumokForceLock";
    private const string funkcio_DokumentumokViewHistory = "DokumentumokViewHistory";

    private const string funkcio_StandaloneDokumentumokNew = "StandaloneDokumentumokNew";
    private const string funkcio_StandaloneDokumentumokModify = "StandaloneDokumentumokModify";
    private const string funkcio_StandaloneDokumentumokView = "StandaloneDokumentumokView";
    private const string funkcio_StandaloneDokumentumokInvalidate = "StandaloneDokumentumokInvalidate";
    private const string funkcio_StandaloneDokumentumokList = "StandaloneDokumentumokList";
    private const string funkcio_StandaloneDokumentumokViewHistory = "StandaloneDokumentumokViewHistory";
    private const string funkcio_StandaloneDokumentumokLock = "StandaloneDokumentumokDokumentumokLock";
    private const string funkcio_StandaloneDokumentumokForceLock = "StandaloneDokumentumokDokumentumokForceLock";

    private const string funkcio_SzakagiDokumentumokList = "SzakagiDokumentumokList";

    private const string lockColumnName = "LockField";
    private const string lockedImageName = "LockedImage";

    private string dokumentumId = String.Empty;

    public string DokumentumId
    {
        get { return dokumentumId; }
        set { dokumentumId = value; }
    }

    private bool isVersionView = false;

    public bool IsVersionView
    {
        get { return isVersionView; }
        set { isVersionView = value; }
    }

    public bool TipusPanelVisible
    {
        get { return TipusPanel.Visible; }
        set { TipusPanel.Visible = value; }
    }

    private bool isPartnerMode = false;

    public bool IsPartnerMode
    {
        get { return isPartnerMode; }
        set { isPartnerMode = value; }
    }

    private string partnerId = "";

    public string PartnerId
    {
        get { return partnerId; }
        set { partnerId = value; }
    }

    public GridView GridView
    {
        get { return DokumentumokGridView; }
    }
    protected string[] EnabledValues_DokumentumTipus
    {
        get
        {
            // ha még nem volt (sikeres) lekérés
            if (ViewState["EnabledValues_DokumentumTipus"] == null && ViewState["ForbiddenValues_DokumentumTipus"] == null)
            {
                FillViewStateEnabledAndForbiddenValues_Tipus("EnabledValues_DokumentumTipus", "ForbiddenValues_DokumentumTipus");
            }

            return ViewState["EnabledValues_DokumentumTipus"] as string[];
        }
    }

    protected string[] ForbiddenValues_DokumentumTipus
    {
        get
        {
            // ha még nem volt (sikeres) lekérés
            if (ViewState["EnabledValues_DokumentumTipus"] == null && ViewState["ForbiddenValues_DokumentumTipus"] == null)
            {
                FillViewStateEnabledAndForbiddenValues_Tipus("EnabledValues_DokumentumTipus", "ForbiddenValues_DokumentumTipus");
            }

            return ViewState["ForbiddenValues_DokumentumTipus"] as string[];
        }
    }

    public int LockColumnIndex
    {
        get
        {
            if (ViewState["LockColumnIndex"] != null)
            {
                return (int)ViewState["LockColumnIndex"];
            }
            return -1;
        }

        set { ViewState["LockColumnIndex"] = value; }
    }

    #region Dinamikus TemplateFieldek mentése

    private Dictionary<string, string> TemplateFieldSources
    {
        get
        {
            if (ViewState["TemplateFieldSources"] != null)
            {
                return (Dictionary<string, string>)ViewState["TemplateFieldSources"];
            }
            else
            {
                ViewState["TemplateFieldSources"] = new Dictionary<string, string>();
            }
            return (Dictionary<string, string>)ViewState["TemplateFieldSources"];
        }

        set
        {
            ViewState["TemplateFieldSources"] = value;
        }
    }

    private void SaveTemplateFieldSourceToViewState(string key, string controlTypeSource)
    {
        if (!TemplateFieldSources.ContainsKey(key))
        {
            TemplateFieldSources.Add(key, controlTypeSource);
        }
        else
        {
            TemplateFieldSources[key] = controlTypeSource;
        }
    }

    private string GetTemplateFieldSourceFromViewState(string key)
    {
        if (!TemplateFieldSources.ContainsKey(key))
        {
            return null;
        }
        else
        {
            return TemplateFieldSources[key];
        }
    }
    #endregion Dinamikus TemplateFieldek mentése

    #region Tárgyszó/Meta oszlopok hozzáadása
    private void AddMetaColumnsToGridView(Result result, GridView gv)
    {
        if (!result.IsError)
        {
            if (result.Ds.Tables.Contains(Constants.TableNames.EREC_ObjektumTargyszavai))
            {
                DataTable table = result.Ds.Tables[Constants.TableNames.EREC_ObjektumTargyszavai];

                
                foreach (DataRow row in table.Rows)
                {
                    ArrayList lstGridViewColumns = new ArrayList(gv.Columns);
                    DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));
                    string Targyszo = row["Targyszo"].ToString();
                    if (!String.IsNullOrEmpty(Targyszo) && result.Ds.Tables[0].Columns.Contains(String.Format("{0}", Targyszo)))
                    {
                        if (!Array.Exists<DataControlField>(arrayGridViewColumns,
                                delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == Targyszo; }))
                        {
                            string ControlTypeSource = row[Constants.ColumnNames.EREC_ObjektumTargyszavai.WithExtension.ControlTypeSource].ToString();

                            //int positionBeforeLastColumn = gv.Columns.Count - 1;
                            //if (positionBeforeLastColumn < 0)
                            //{
                            //    positionBeforeLastColumn = 0;
                            //}

                            string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                            if (ControlTypeSourceForGrid == KodTarak.CONTROLTYPE_SOURCE.BoundField)
                            {
                                BoundField boundField = new BoundField();
                                boundField.HeaderText = Targyszo;
                                boundField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                boundField.HeaderStyle.CssClass = "GridViewBorderHeader";
                                boundField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                boundField.DataField = String.Format("{0}", Targyszo);
                                boundField.AccessibleHeaderText = Targyszo;

                                //gv.Columns.Insert(positionBeforeLastColumn, boundField);
                                gv.Columns.Add(boundField);
                            }
                            else if (ControlTypeSourceForGrid == KodTarak.CONTROLTYPE_SOURCE.CheckBoxField)
                            {
                                // ha van hibás formátumú érték, BoudFieldet csinálunk CheckBoxField helyett
                                if (result.Ds.Tables[0].Select(String.Format("IsNull([{0}],{1}) NOT IN ({1}, {2})", Targyszo, Boolean.FalseString, Boolean.TrueString)).Length == 0)
                                {
                                    CheckBoxField checkboxField = new CheckBoxField();
                                    checkboxField.HeaderText = Targyszo;
                                    checkboxField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                    checkboxField.HeaderStyle.CssClass = "GridViewCheckBoxTemplateHeaderStyle";
                                    checkboxField.ItemStyle.CssClass = "GridViewCheckBoxTemplateItemStyle";
                                    checkboxField.DataField = String.Format("{0}", Targyszo);
                                    checkboxField.AccessibleHeaderText = Targyszo;

                                    //gv.Columns.Insert(positionBeforeLastColumn, checkboxField);
                                    gv.Columns.Add(checkboxField);
                                }
                                else
                                {
                                    BoundField boundField = new BoundField();
                                    boundField.HeaderText = Targyszo;
                                    boundField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                    boundField.HeaderStyle.CssClass = "GridViewBorderHeader";
                                    boundField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                    boundField.DataField = String.Format("{0}", Targyszo);
                                    boundField.AccessibleHeaderText = Targyszo;

                                    //gv.Columns.Insert(positionBeforeLastColumn, boundField);
                                    gv.Columns.Add(boundField);
                                }
                            }
                            else
                            {
                                TemplateField templateField = new TemplateField();
                                templateField.HeaderText = Targyszo;
                                templateField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                templateField.HeaderStyle.CssClass = "GridViewBorderHeader";
                                templateField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                templateField.ItemTemplate = new DynamicValueControlGridViewTemplate(Targyszo
                                    , DataControlRowType.DataRow
                                    , ControlTypeSourceForGrid);
                                templateField.AccessibleHeaderText = Targyszo;

                                //gv.Columns.Insert(positionBeforeLastColumn, templateField);
                                gv.Columns.Add(templateField);
                            }

                            // mentés a ViewState-be
                            SaveTemplateFieldSourceToViewState(Targyszo, ControlTypeSource);
                        }
                        else if (IsPostBack)
                        {
                            #region Template újragenerálás
                            // .NET framework hiba: template field esetén postback után elveszik a template
                            // workaround: újra generáljuk a template-et
                            DataControlField currentColumn = Array.Find<DataControlField>(arrayGridViewColumns,
                                delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == Targyszo; });
                            if (currentColumn != null)
                            {
                                string ControlTypeSource = row[Constants.ColumnNames.EREC_ObjektumTargyszavai.WithExtension.ControlTypeSource].ToString();
                                string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                                if (currentColumn is TemplateField)
                                {
                                    ((TemplateField)currentColumn).ItemTemplate = new DynamicValueControlGridViewTemplate(Targyszo
                                        , DataControlRowType.DataRow
                                        , ControlTypeSourceForGrid);
                                }
                            }
                            #endregion Template újragenerálás
                        }
                    }
                }
            }
        }
    }
    #endregion Tárgyszó/Meta oszlopok hozzáadása


    #region Enabled és Forbidden tipusok ViewState-be mentése
    
    private void FillViewStateEnabledAndForbiddenValues_Tipus(string keyEnabled, string keyForbidden)
    {
        Dictionary<String, String> kodtarak_Dictionary =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_DOKUMENTUMTIPUS, Page);

        if (kodtarak_Dictionary != null)
        {
            KRT_FunkciokSearch search = new KRT_FunkciokSearch();
            search.Kod.Value = String.Format("KT_{0}_*_*abled", kcs_DOKUMENTUMTIPUS);  // "KT_<kódcsoport kód>_<kódtár kód>_Enabled", "KT_<kódcsoport kód>_<kódtár kód>_Disabled";
            search.Kod.Operator = Query.Operators.like;
            List<string> lstFunkcioKodok = FunctionRights.GetAllFunkcioKod(Page, search);

            List<string> lstEnabledTipusok = new List<string>();
            List<string> lstForbiddenTipusok = new List<string>();

            foreach (string kod in kodtarak_Dictionary.Keys)
            {
                string enabledFunkcio = String.Format("KT_{0}_{1}_Enabled", kcs_DOKUMENTUMTIPUS, kod);
                string disabledFunkcio = String.Format("KT_{0}_{1}_Disabled", kcs_DOKUMENTUMTIPUS, kod);

                if ((lstFunkcioKodok.Contains(enabledFunkcio) && !FunctionRights.GetFunkcioJog(Page, enabledFunkcio))
                    || (lstFunkcioKodok.Contains(disabledFunkcio) && FunctionRights.GetFunkcioJog(Page, disabledFunkcio)))
                {
                    lstForbiddenTipusok.Add(kod);
                }
                else
                {
                    lstEnabledTipusok.Add(kod);
                }
            }

            ViewState[keyEnabled] = lstEnabledTipusok.ToArray();
            ViewState[keyForbidden] = lstForbiddenTipusok.ToArray();
        }

    }
    #endregion Enabled és Forbidden tipusok ViewState-be mentése

    #region Base Page

    public void InitPage()
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        if (Session["DokumentumokListStartup"] == null)
        {
            if (Startup == Constants.Startup.SearchForm)
            {
                Session["DokumentumokListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
        }

        DokumentumTipus = Request.QueryString.Get(QueryStringVars.Tipus);

        if (!String.IsNullOrEmpty(DokumentumTipus))
        {
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_DOKUMENTUMTIPUS, Page).TryGetValue(DokumentumTipus, out DokumentumTipusNev);
        }

        if (Startup == Constants.Startup.Standalone)
        {
            // funkciójogok
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_StandaloneDokumentumokList);

            if (Rendszerparameterek.IsTUK(Page))
            {
                UI.SetGridViewColumnVisiblity(DokumentumokGridView, "PartnerNev", true);
                SetGridViewColumnVisiblityBySortExpression(DokumentumokGridView, "KRT_Dokumentumok.ErvVege", true);
            }
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            // funkciójogok
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_SzakagiDokumentumokList);
        }
        else
        {
            // funkciójogok
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_DokumentumokList);
        }

        if (!IsVersionView)
        {
            ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
            ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

            CsatolmanyokSubListHeader.RowCount_Changed += new EventHandler(CsatolmanyokSubListHeader_RowCount_Changed);
            CsatolmanyokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsatolmanyokSubListHeader_ErvenyessegFilter_Changed);

            DokumentumHierarchia1.InitComponent(EErrorPanel1, ErrorUpdatePanel, true, funkcio_DokumentumokList);
            DokumentumHierarchia1.Startup = this.Startup;
        }

        multiViewGridViews.ActiveViewIndex = (IsVersionView) ? 1 : 0;
        trSublists.Visible = !IsVersionView;
        ListHeader1.DafaultPageLinkVisible = !IsVersionView;
        ListHeader1.PagerVisible = !IsVersionView;
        ListHeader1.SearchVisible = !IsVersionView;
        ListHeader1.RefreshVisible = !IsVersionView;
        ListHeader1.DefaultFilterVisible = !IsVersionView;
        ListHeader1.NewVisible = !IsVersionView;
        ListHeader1.InvalidateVisible = !IsVersionView;
        ListHeader1.ViewVisible = !IsVersionView;
        ListHeader1.ModifyVisible = !IsVersionView;
        ListHeader1.HistoryVisible = !IsVersionView;
        ListHeader1.SendObjectsVisible = !IsVersionView;
        ListHeader1.LockVisible = !IsVersionView;
        ListHeader1.UnlockVisible = !IsVersionView;
    }

    public void LoadPage()
    {
        ListHeader1.SetRightFunctionButtonsVisible(false);

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        if (!IsVersionView)
        {

            if (Startup == Constants.Startup.Standalone)
            {
                ListHeader1.HeaderLabel = Resources.List.StandaloneDokumentumokListHeaderTitle;

                ListHeader1.NewVisible = !FelhasznaloProfil.OrgIsNMHH(this.Page);
                ListHeader1.InvalidateVisible = false;
                //ListHeader1.FTSearchVisible = true;
            }//LZS - BUG_10927
            else if (Startup == Constants.Startup.OktatasiAnyagok)
            {
                ListHeader1.HeaderLabel = Resources.List.OktatasiAnyagokList;

                ListHeader1.NewVisible = !FelhasznaloProfil.OrgIsNMHH(this.Page);
                ListHeader1.InvalidateVisible = false;
            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                ListHeader1.HeaderLabel = Resources.List.SzakagiDokumentumokListHeaderTitle;

                if (!String.IsNullOrEmpty(DokumentumTipus))
                {
                    ListHeader1.HeaderLabel += String.Format(" ({0})", DokumentumTipusNev);
                }

                ListHeader1.NewVisible = true;
                ListHeader1.InvalidateVisible = false;
                //ListHeader1.FTSearchVisible = true;

                if (!IsPostBack)
                {
                    TipusPanelVisible = true;
                    KodtarakDropDownList_TipusFill();
                }

                KodtarakDropDownList_Tipus.SelectedIndexChanged += new EventHandler(KodtarakDropDownList_SelectedIndexChanged);
                KodtarakDropDownList_Tipus.AutoPostBack = true;
                //if (KodtarakDropDownList_Tipus.DropDownList.Attributes["onchange"] != null && KodtarakDropDownList_Tipus.DropDownList.Attributes["onchange"].EndsWith("return false;"))
                //{
                //    KodtarakDropDownList_Tipus.DropDownList.Attributes["onchange"].TrimEnd("return false;".ToCharArray());
                //}
                //KodtarakDropDownList_Tipus.DropDownList.Attributes["onchange"] += Page.ClientScript.GetPostBackEventReference(DokumentumokUpdatePanel, EventArgumentConst.refreshMasterList) + ";";
            }
            else
            {
                if (isPartnerMode)
                {
                    KodtarakDropDownList_Tipus.SelectedIndexChanged += new EventHandler(KodtarakDropDownListPartner_SelectedIndexChanged);
                    KodtarakDropDownList_Tipus.AutoPostBack = true;
                }
                else
                {
                    ListHeader1.HeaderLabel = Resources.List.DokumentumokListHeaderTitle;

                    ListHeader1.NewVisible = false;
                    ListHeader1.InvalidateVisible = false;
                    //ListHeader1.FTSearchVisible = true;
                }

            }

            //// A baloldali, alapból invisible ikonok megjelenítése:
            //ListHeader1.PrintVisible = true;

            ListHeader1.LockVisible = true;
            ListHeader1.UnlockVisible = true;

            if (Startup == Constants.Startup.Standalone)
            {
                   //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
                    ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch;
                    ListHeader1.SearchObjectType = typeof(KRT_DokumentumokSearch);
            }
            if (Startup == Constants.Startup.Szakagi)
            {
                //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch;
                ListHeader1.SearchObjectType = typeof(KRT_DokumentumokSearch);
            }//LZS - BUG_10927
            else if(Startup == Constants.Startup.OktatasiAnyagok)
            {
                ListHeader1.SearchObjectType = typeof(KRT_DokumentumokSearch);
                KRT_DokumentumokSearch search = new KRT_DokumentumokSearch();
                search.Tipus.Value = KodTarak.DOKUMENTUMTIPUS.OktatasiAnyag;
                search.Tipus.Operator = Query.Operators.equals;

                Search.SetSearchObject(Page, search);
            }
            else
            {
                ListHeader1.SearchObjectType = typeof(KRT_DokumentumokSearch);
            }

            JavaScripts.RegisterPopupWindowClientScript(Page);
            JavaScripts.registerOuterScriptFile(Page, "CheckBoxes", "Javascripts/CheckBoxes.js");
            JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
            ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

            ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
            ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

            if (Startup == Constants.Startup.Standalone)
            {
                string queryString = String.Format("{0}={1}", QueryStringVars.Startup, Startup);

                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("DokumentumokSearch.aspx", queryString
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New 
                    + (String.IsNullOrEmpty(Startup) ? "" : "&" + QueryStringVars.Startup + "=" + Startup)
                    , Defaults.PopupWidth, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                if (!String.IsNullOrEmpty(DokumentumTipus))
                {
                    string queryString = String.Format("{0}={1}&{2}={3}", QueryStringVars.Startup, Startup, QueryStringVars.Tipus, HttpUtility.UrlEncode(DokumentumTipus));

                    ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("DokumentumokSearch.aspx", queryString
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    string queryString = String.Format("{0}={1}&{2}={3}", QueryStringVars.Startup, Startup, QueryStringVars.Tipus, HttpUtility.UrlEncode(DokumentumTipus));

                    ListHeader1.SearchOnClientClick = "alert('Kérem, előbb válasszon típust!'); return false;";

                }
            }
            else
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("DokumentumokSearch.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(DokumentumokGridView.ClientID);

            //ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratokListajaPrintForm.aspx");

            ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(DokumentumokGridView.ClientID);

            ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(DokumentumokGridView.ClientID);
            ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(DokumentumokGridView.ClientID);

            ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(DokumentumokGridView.ClientID);

            ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, "", true);

            //selectedRecordId kezelése
            ListHeader1.AttachedGridView = DokumentumokGridView;

            if (Startup == Constants.Startup.Standalone)
            {
                // csatolmány tab eltüntetése
                TabContainer1.Tabs[0].Enabled = false;
                TabContainer1.ActiveTabIndex = 1;
                
            }
            else
            {

                CsatolmanyokSubListHeader.AttachedGridView = CsatolmanyokGridView;

                CsatolmanyokSubListHeader.NewEnabled = false;
                //CsatolmanyokSubListHeader.ViewEnabled = false;
                CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                CsatolmanyokSubListHeader.ModifyEnabled = false;
                CsatolmanyokSubListHeader.InvalidateEnabled = false;
                //CsatolmanyokSubListHeader.ButtonsClick += new CommandEventHandler(CsatolmanyokSubListHeader_ButtonsClick);
            }

            Search.SetIdsToSearchObject(Page, "Id", new KRT_DokumentumokSearch());

            //scroll állapotának mentése
            JavaScripts.RegisterScrollManagerScript(Page);

        }
        else
        {
            ListHeader1.HeaderLabel = Resources.List.DokumentumokListHeaderTitle;
            ListHeader1.CustomSearchObjectSessionName = "-----";
            ddListVerzioIg.SelectedIndexChanged += new EventHandler(ddListVerzioFilter_SelectedIndexChanged);
            ddListVerzioTol.SelectedIndexChanged += new EventHandler(ddListVerzioFilter_SelectedIndexChanged);
        }

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["DokumentumokListStartup"] != null)
        {
            if (Session["DokumentumokListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";

                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("DokumentumokSearch.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "DokumentumokSearch", script, true);
                Session.Remove("DokumentumokListStartup");

                popupNyitas = true;

            }
        }

        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            DokumentumokGridViewBind();
        }
    }

    void ddListVerzioFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        DokumentumokGridViewBind();
    }

    void KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DokumentumokGridViewBind();
        string qs = Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] {QueryStringVars.Tipus});
        qs += String.Format("&{0}={1}", QueryStringVars.Tipus, KodtarakDropDownList_Tipus.SelectedValue);//"&" + QueryStringVars.Tipus + "=" + KodtarakDropDownList_Tipus.SelectedValue;

        Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
    }

    void KodtarakDropDownListPartner_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DokumentumokGridViewBind();
        string qs = Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Tipus });
        qs += String.Format("&{0}={1}", QueryStringVars.Tipus, KodtarakDropDownList_Tipus.SelectedValue);//"&" + QueryStringVars.Tipus + "=" + KodtarakDropDownList_Tipus.SelectedValue;

        Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
    }

    public void PreRenderPage()
    {
        if (!IsVersionView)
        {
            if (Startup == Constants.Startup.Standalone)
            {
                ListHeader1.NewEnabled = FelhasznaloProfil.OrgIsNMHH(this.Page) ? false : FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokNew); //true;
                ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokView); // false;
                ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokModify); // false;
                ListHeader1.InvalidateEnabled = false;
                ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokViewHistory); // false;

                ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokLock);
                ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_StandaloneDokumentumokLock);

                //// nincsenek csatolmányok
                //CsatolmanyokSubListHeader.NewEnabled = false;
                //CsatolmanyokSubListHeader.ViewEnabled = false;
                //CsatolmanyokSubListHeader.ModifyEnabled = false;
                //CsatolmanyokSubListHeader.InvalidateEnabled = false;
            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                ListHeader1.NewEnabled = false;
                ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokView); // false;
                ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokModify); // false;
                ListHeader1.InvalidateEnabled = false;
                ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokViewHistory); // false;

                ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokLock);
                ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokLock);

                CsatolmanyokSubListHeader.NewEnabled = false;
                // KuldemenyView vagy IraIratView jog kell:
                CsatolmanyokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratView")
                                                        || FunctionRights.GetFunkcioJog(Page, "KuldemenyView");
                CsatolmanyokSubListHeader.ModifyEnabled = false;
                CsatolmanyokSubListHeader.InvalidateEnabled = false;
            }
            else
            {
                ListHeader1.NewEnabled = false;
                ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Dokumentumok" + CommandName.View);
                ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Dokumentumok" + CommandName.Modify);
                ListHeader1.InvalidateEnabled = false;
                ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Dokumentumok" + CommandName.ViewHistory);

                ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokLock);
                ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, funkcio_DokumentumokLock);

                CsatolmanyokSubListHeader.NewEnabled = false;
                // KuldemenyView vagy IraIratView jog kell:
                CsatolmanyokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratView")
                                                        || FunctionRights.GetFunkcioJog(Page, "KuldemenyView");
                CsatolmanyokSubListHeader.ModifyEnabled = false;
                CsatolmanyokSubListHeader.InvalidateEnabled = false;
            }

            //if (IsPostBack)
            {
                String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(DokumentumokGridView);

                RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
                if (String.IsNullOrEmpty(MasterListSelectedRowId))
                {
                    ActiveTabClear();
                }
                //ActiveTabRefreshOnClientClicks();
                ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
            }

            ui.SetClientScriptToGridViewSelectDeSelectButton(DokumentumokGridView);
        }

        if (IsPartnerMode)
        {
            trSublists.Visible = false;
        }
    }

    public void KodtarakDropDownList_TipusFill()
    {
        if (!String.IsNullOrEmpty(DokumentumTipus))
        {
            KodtarakDropDownList_Tipus.FillAndSetSelectedValue(kcs_DOKUMENTUMTIPUS, DokumentumTipus, true, EErrorPanel1, true);
        }
        else
        {
            KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, EErrorPanel1, true);
        }
    }

    #endregion Base Page


    #region Master List

    private string SortingViewStatename
    {
        get
        {
            if (IsVersionView)
            {
                return "DokumentumokGridViewVersion";
            }

            return "DokumentumokGridView";
        }
    }

    private void FillVerzioFilterDropDownLists(Result res)
    {
        if (!IsPostBack)
        {
            if (!res.IsError)
            {
                DataTable table = res.Ds.Tables[0];
                DataView view = new DataView(table);
                view.Sort = "version asc";
                ddListVerzioTol.DataSource = view;
                ddListVerzioTol.DataBind();
                ddListVerzioTol.SelectedIndex = 0;
                ddListVerzioIg.DataSource = view;
                ddListVerzioIg.DataBind();
                ddListVerzioIg.SelectedIndex = view.Count - 1;
            }
        }
    }

    private string GetVersionFilter()
    {
        string filter = String.Empty;
        string verzoTol = String.Empty;
        string verzioIg = String.Empty;

        if (ddListVerzioTol.SelectedIndex > -1 && !String.IsNullOrEmpty(ddListVerzioTol.SelectedValue))
        {
            verzoTol = ddListVerzioTol.Text;
        }

        if (ddListVerzioIg.SelectedIndex > -1 && !String.IsNullOrEmpty(ddListVerzioIg.SelectedValue))
        {
            verzioIg = ddListVerzioIg.Text;
        }

        if (!String.IsNullOrEmpty(verzoTol) && !String.IsNullOrEmpty(verzioIg))
        {
            filter = String.Format("version >= '{0}' and version <= '{1}'", verzoTol, verzioIg);
        }
        else
        {
            if (!String.IsNullOrEmpty(verzoTol))
            {
                filter = String.Format("version >= '{0}'", verzoTol);
            }
            if (!String.IsNullOrEmpty(verzioIg))
            {
                filter = String.Format("version <= '{1}'", verzioIg);
            }
        }

        return filter;
    }

    protected void DokumentumokGridViewBind()
    {
        string sortExpression = Search.GetSortExpressionFromViewState(SortingViewStatename, ViewState, IsVersionView ? "version" : "KRT_Dokumentumok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState(SortingViewStatename, ViewState, SortDirection.Descending);

        DokumentumokGridViewBind(sortExpression, sortDirection);
    }

    protected void DokumentumokGridViewBind(string SortExpression, SortDirection SortDirection)
    {
        if (isPartnerMode)
        {
            return;
        }
        // BUG_2677
        //else
        //{
        //    DokumentumokGridView.Columns.Remove(getGridViewDataColumn("Típus"));
        //}

        DokumentumVizualizerComponent.ClearDokumentElements();

        Contentum.eDocument.Service.KRT_DokumentumokService service = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

        ExecParam execParam = UI.SetExecParamDefault(Page);

        if (IsVersionView)
        {
            execParam.Record_Id = DokumentumId;
            DocumentService docService = eDocumentService.ServiceFactory.GetDocumentService();
            Result resdocVersions = docService.GetDocumentVersionsFromMOSS(execParam);
            if (!resdocVersions.IsError)
            {
                FillVerzioFilterDropDownLists(resdocVersions);
                DataTable table = resdocVersions.Ds.Tables[0];
                DataView view = new DataView(table);
                view.Sort = Search.GetOrderBy(SortingViewStatename, ViewState, SortExpression, SortDirection);
                view.RowFilter = GetVersionFilter();
                resdocVersions.Ds.Tables.RemoveAt(0);
                resdocVersions.Ds.Tables.Add(view.ToTable());
                if(table.Rows.Count > 0)
                {
                    ListHeader1.HeaderLabel = String.Format(Resources.List.DokumentumVerziokListHeaderTitle, table.Rows[0]["name"]);
                }
            }
            UI.GridViewFill(DokumentumokGridViewVersions, resdocVersions, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {

            KRT_DokumentumokSearch search = null;

            if (!IsPostBack)
            {
                Startup = Request.QueryString.Get(QueryStringVars.Startup);
                string Obj_Id = Request.QueryString.Get(QueryStringVars.ObjektumId);
                string Azonosito = Request.QueryString.Get(QueryStringVars.Azonosito);

                if (Startup == Constants.Startup.FromUgyirat)
                {
                    // új kereső objektumot hozunk létre és betesszük a sessionbe
                    search = new KRT_DokumentumokSearch();
                    if (!String.IsNullOrEmpty(Obj_Id))
                    {
                        search.Manual_Ugyirat_Id.Value = Obj_Id;
                        search.Manual_Ugyirat_Id.Operator = Query.Operators.equals;

                        if (!String.IsNullOrEmpty(Azonosito))
                        {
                            Azonosito = System.Web.HttpUtility.UrlDecode(Azonosito);
                        }

                        search.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_Ugyirat;

                        if (!String.IsNullOrEmpty(Azonosito))
                        {
                            search.ReadableWhere += System.Web.HttpUtility.UrlDecode(Azonosito);
                        }
                    }
                    Search.SetSearchObject(Page, search);
                }

            }
            
            if (Startup == Constants.Startup.Standalone)
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch);
            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch);
            }
            else
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject(Page, new KRT_DokumentumokSearch());
            }

            search.OrderBy = Search.GetOrderBy(SortingViewStatename, ViewState, SortExpression, SortDirection);
            search.TopRow = UI.GetTopRow(Page);

            // Lapozás beállítása:
            UI.SetPaging(execParam, ListHeader1);

            Result res = null;

            if (Startup == Constants.Startup.Standalone)
            {
                if (ForbiddenValues_DokumentumTipus != null && ForbiddenValues_DokumentumTipus.Length > 0)
                {
                    search.Tipus.Value = Search.GetSqlInnerString(ForbiddenValues_DokumentumTipus);
                    search.Tipus.Operator = Query.Operators.notinner;
                }

                res = service.GetAllWithExtensionAndMetaAndJogosultak(execParam, search
                    , null, null
                    , Constants.ColumnNames.KRT_Dokumentumok.Tipus, ForbiddenValues_DokumentumTipus, true
                    , null, false, false, true // bStandaloneMode = true
                    , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));

                AddMetaColumnsToGridView(res, DokumentumokGridView);

            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                if (!String.IsNullOrEmpty(KodtarakDropDownList_Tipus.SelectedValue)
                    && !Array.Exists<string>(ForbiddenValues_DokumentumTipus
                                , delegate(string item){return item == KodtarakDropDownList_Tipus.SelectedValue; }))
                {
                    search.Tipus.Value = KodtarakDropDownList_Tipus.SelectedValue;
                    search.Tipus.Operator = Query.Operators.equals;
                }
                else
                {
                    ui.GridViewClear(DokumentumokGridView);
                    return;
                }

                res = service.GetAllWithExtensionAndMetaAndJogosultak(execParam, search
                    , null, null
                    , Constants.ColumnNames.KRT_Dokumentumok.Tipus, new string[] {KodtarakDropDownList_Tipus.SelectedValue}, false
                    , null, false, false, false // bStandaloneMode = false
                    , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));

                AddMetaColumnsToGridView(res, DokumentumokGridView);
            }
            else
            {
                res = service.GetAllWithExtensionAndJogosultak(execParam, search
                    , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));
            }

            // zárolás oszlop hozzáadása a grid végére (image field: a template field template-jeitől eltérően
            // nem vész el a postback során, kivéve a generált Image kódból definiált ID-ját)
            if (!IsPostBack)
            {
                ImageField imageField = new ImageField();
                imageField.HeaderStyle.CssClass = "GridViewLockedImage";
                imageField.ItemStyle.CssClass = "GridViewLockedImage";
                imageField.HeaderImageUrl = "../images/hu/egyeb/locked.gif";
                imageField.HeaderText = Resources.Buttons.Zarolas; // "Z&aacute;rol&aacute;s"

                imageField.DataImageUrlField = "Zarolo_id";
                imageField.DataImageUrlFormatString = "../images/hu/egyeb/locked.gif";
                imageField.AccessibleHeaderText = lockColumnName;

                DokumentumokGridView.Columns.Add(imageField);

                LockColumnIndex = DokumentumokGridView.Columns.Count - 1;
            }

            UI.GridViewFill(DokumentumokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    protected void DokumentumokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        GridView_RowDataBound_SetLockingInfoForImageField(e, Page, this.LockColumnIndex);

        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");

        GridView_RowDataBound_SetCsatolmanyImage(e, "CsatolmanyImage");
    }

    /// <summary>
    /// Formatum mező alapján beállítja a fájlikonokat
    /// </summary>
    public void SetFileIcons_GridViewRowDataBound(GridViewRowEventArgs e, Page page, string imageButtonId)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string formatum = String.Empty;
            string externalLink = String.Empty;
            string dokumentumId = String.Empty;

            if (drv != null && drv["Formatum"] != null)
            {
                formatum = drv["Formatum"].ToString();
            }

            if (drv != null && drv["External_Link"] != null)
            {
                externalLink = drv["External_Link"].ToString();
                dokumentumId = drv["Id"].ToString();
                externalLink = "GetDocumentContent.aspx?id=" + dokumentumId;
            }

            if (!string.IsNullOrEmpty(formatum))
            {
                ImageButton fileIcon = (ImageButton)e.Row.FindControl(imageButtonId);

                if (fileIcon != null)
                {
                    fileIcon.ImageUrl = "../images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum);

                    if (!String.IsNullOrEmpty(externalLink))
                    {
                        fileIcon.OnClientClick = "window.open('" + externalLink + "'); return false;";
                        fileIcon.Enabled = true;

                        DokumentumVizualizerComponent.AddDokumentElement(
                        fileIcon.ClientID, dokumentumId, drv["FajlNev"].ToString(), drv["VerzioJel"].ToString(), drv["External_Link"].ToString());
                    }
                }
            }
        }
    }


    private void GridView_RowDataBound_SetCsatolmanyImage(GridViewRowEventArgs e, string imageId)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl(imageId);
                if (CsatolmanyImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        //#region BLG_577
                        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        //{   /*NINCS JOGA*/
                        //    CsatolmanyImage.Visible = false;
                        //    return;
                        //}
                        //#endregion

                        CsatolmanyImage.AlternateText = Resources.Form.UI_CsatolmanyImage_ToolTip;
                        CsatolmanyImage.ToolTip = Resources.Form.UI_CsatolmanyImage_ToolTip;
                        CsatolmanyImage.Visible = true;
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatolmanyImage = (Image)e.Row.FindControl(imageId);
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    protected void DokumentumokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, DokumentumokCPE);

        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        DokumentumokGridViewBind();
        ActiveTabClear();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, DokumentumokCPE);
        DokumentumokGridViewBind();
    }

    protected void DokumentumokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(DokumentumokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            if (!isPartnerMode)
            {
                ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                        + (String.IsNullOrEmpty(Startup) ? "" : "&" + QueryStringVars.Startup + "=" + Startup)
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, DokumentumokUpdatePanel.ClientID);
                string tableName = "KRT_Dokumentumok";
                ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                    , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                    , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, DokumentumokUpdatePanel.ClientID);
                ListHeader1.PrintOnClientClick = "javascript:window.open('DokumentumokPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + id + "')";

                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                //execParam.Record_Id = id;
                //Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(id, execParam, EErrorPanel1);
                //ErrorDetails errorDetail;


                // Módosítás
                //if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
                //{
                //ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes
                //   + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" + JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                //, QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                //, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                //+ "} else return false;";
                //}
                //else
                //{
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + (String.IsNullOrEmpty(Startup) ? "" : "&" + QueryStringVars.Startup + "=" + Startup)
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                //}
            }
            else
            {
                SubListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                   , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.PartnerId + "=" + PartnerId
                   + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromPartnerekList
                   , Defaults.PopupWidth, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID);
                SubListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.PartnerId + "=" + PartnerId
                    + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromPartnerekList
                    , Defaults.PopupWidth, Defaults.PopupHeight, DokumentumokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
        }
    }

    protected void DokumentumokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    DokumentumokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(DokumentumokGridView));
                    break;
                default:
                    #region Template újragenerálás
                    // .NET framework hiba: template field esetén postback után elveszik a template
                    // workaround: újra generáljuk a template-et
                    foreach (DataControlField currentColumn in DokumentumokGridView.Columns)
                    {
                        if (currentColumn is TemplateField && !String.IsNullOrEmpty(currentColumn.AccessibleHeaderText))
                        {
                            if (TemplateFieldSources.ContainsKey(currentColumn.AccessibleHeaderText))
                            {
                                int colindex = DokumentumokGridView.Columns.IndexOf(currentColumn);

                                string ControlTypeSource = GetTemplateFieldSourceFromViewState(currentColumn.AccessibleHeaderText);
                                string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                                if (((TemplateField)currentColumn).ItemTemplate == null)
                                {
                                    ((TemplateField)currentColumn).ItemTemplate = new DynamicValueControlGridViewTemplate(currentColumn.AccessibleHeaderText
                                        , DataControlRowType.DataRow
                                        , ControlTypeSourceForGrid);


                                    foreach (GridViewRow row in DokumentumokGridView.Rows)
                                    {
                                        ((TemplateField)currentColumn).ItemTemplate.InstantiateIn(row.Cells[colindex]);
                                    }
                                }
                            }
                        }
                    }
                    #endregion Template újragenerálás
                    //if (Startup == Constants.Startup.Standalone)
                    //{
                    //    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(DokumentumokGridView));
                    //}
                    break;
            }

            //ArrayList lstGridViewColumns = new ArrayList(DokumentumokGridView.Columns);
            //DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));

            //int lockColumnIndex = Array.FindIndex<DataControlField>(arrayGridViewColumns,
            //    delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == lockColumnName; });

            int lockColumnIndex = this.LockColumnIndex;

            if (lockColumnIndex >= 0)
            {
                foreach (GridViewRow row in DokumentumokGridView.Rows)
                {
                    if (row.Cells[lockColumnIndex].Controls.Count > 0)
                    {
                        if (row.Cells[lockColumnIndex].Controls[0] is Image)
                        {
                            //Image lockImage = (Image)row.Cells[lockColumnIndex].Controls[0];
                            //lockImage.ID = lockedImageName;
                            row.Cells[lockColumnIndex].Controls[0].ID = lockedImageName;
                        }
                    }
                }
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedDokumentumok();
        //    DokumentumokGridViewBind();
        //}
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedRecords_Dokumentumok();
                DokumentumokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedRecords_Dokumentumok();
                DokumentumokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedDokumentumok();
                break;
        }
    }

    private void LockSelectedRecords_Dokumentumok()
    {
        if (Startup == Constants.Startup.Standalone)
        {
            LockManager.LockSelectedGridViewRecords(DokumentumokGridView, "KRT_Dokumentumok"
                , funkcio_StandaloneDokumentumokLock, funkcio_StandaloneDokumentumokForceLock
                 , Page, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {
            LockManager.LockSelectedGridViewRecords(DokumentumokGridView, "KRT_Dokumentumok"
                , funkcio_DokumentumokLock, funkcio_StandaloneDokumentumokForceLock
                 , Page, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    private void UnlockSelectedRecords_Dokumentumok()
    {
        if (Startup == Constants.Startup.Standalone)
        {
            LockManager.LockSelectedGridViewRecords(DokumentumokGridView, "KRT_Dokumentumok"
                , funkcio_StandaloneDokumentumokLock, funkcio_StandaloneDokumentumokForceLock
                 , Page, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {
            LockManager.UnlockSelectedGridViewRecords(DokumentumokGridView, "KRT_Dokumentumok"
                , funkcio_DokumentumokLock, funkcio_DokumentumokForceLock
                , Page, EErrorPanel1, ErrorUpdatePanel);
        }
    }


    /// <summary>
    /// Elkuldi emailben a DokumentumokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedDokumentumok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(DokumentumokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Dokumentumok");
        }
    }

    protected void DokumentumokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DokumentumokGridViewBind(e.SortExpression, UI.GetSortToGridView(SortingViewStatename, ViewState, e.SortExpression));
        ActiveTabClear();
    }

    protected string GetErvenyesseg(DateTime? ervKezd, DateTime? ervVege)
    {
        if (ervVege != null)
        {
            if (ervVege.Value.Year > 4000)
            {
                ervVege = null;
            }
        }
        return String.Format("{0:d} - {1:d}", ervKezd, ervVege);
    }

    #endregion



    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(DokumentumokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (DokumentumokGridView.SelectedIndex == -1)
        {
            return;
        }
        string DokumentumId = UI.GetGridViewSelectedRecordId(DokumentumokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, DokumentumId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string DokumentumId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsatolmanyokGridViewBind(DokumentumId);
                CsatolmanyokPanel.Visible = true;
                break;
            case 1:
                // BUG_10252
                char standalone = (this.Startup == Constants.Startup.Standalone) ? '1' : '0';
                DokumentumHierarchia1.LoadComponent(DokumentumId, standalone);
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(CsatolmanyokGridView);
                CsatolmanyokGridView.SelectedIndex = -1;
                break;
            case 1:
                DokumentumHierarchia1.Clear();
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsatolmanyokSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(CsatolmanyokTabPanel))
        {
            CsatolmanyokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsatolmanyokGridView));
        }
    }


    #endregion


    #region Csatolmanyok Detail

    //private void CsatolmanyokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    //{
    //    if (e.CommandName == CommandName.Invalidate)
    //    {
    //        deleteSelected_IdeErkeztetok();
    //        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView));
    //    }
    //}

    protected void CsatolmanyokGridViewBind(string DokumentumId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolmanyokGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolmanyokGridView", ViewState);

        CsatolmanyokGridViewBind(DokumentumId, sortExpression, sortDirection);
    }

    protected void CsatolmanyokGridViewBind(string DokumentumId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(DokumentumId))
        {
            Contentum.eRecord.Service.EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page);


            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
            search.OrderBy = Search.GetOrderBy("CsatolmanyokGridView", ViewState, SortExpression, SortDirection);

            CsatolmanyokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByDokumentum(execParam, DokumentumId, search);

            UI.GridViewFill(CsatolmanyokGridView, res, CsatolmanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

        }
        else
        {
            ui.GridViewClear(CsatolmanyokGridView);
            UI.ClearTabHeaderRowCountText(Header);
        }
    }


    void CsatolmanyokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView));
    }

    protected void CsatolmanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(CsatolmanyokTabPanel))
                    //{
                    //    CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView));
                    //}
                    ActiveTabRefreshDetailList(CsatolmanyokTabPanel);
                    break;
            }
        }
    }


    protected void CsatolmanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedRowNumber, "check");
        }
    }

    private void CsatolmanyokGridView_RefreshOnClientClicks(string csatolmanyId)
    {

        string azonosito_tipus = UI.GetGridViewColumnValueOfSelectedRow(CsatolmanyokGridView, "Label_Azonosito_Tipus");
        if (azonosito_tipus == Constants.TableNames.EREC_IraIratok)
        {
            string iratId = UI.GetGridViewColumnValueOfSelectedRow(CsatolmanyokGridView, "Label_IraIrat_Id");
            CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratId
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, String.Empty, String.Empty);
        }
        else if (azonosito_tipus == Constants.TableNames.EREC_KuldKuldemenyek)
        {
            string kuldemenyId = UI.GetGridViewColumnValueOfSelectedRow(CsatolmanyokGridView, "Label_KuldKuldemeny_Id");
            CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + kuldemenyId
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, String.Empty, String.Empty);
        }

        //string id = csatolmanyId;
        //if (!String.IsNullOrEmpty(id))
        //{
        //    CsatolmanyokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID);
        //    CsatolmanyokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, CsatolmanyokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        //}
    }

    protected void CsatolmanyokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsatolmanyokGridView.PageIndex;

        CsatolmanyokGridView.PageIndex = CsatolmanyokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != CsatolmanyokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, CsatolmanyokCPE);
            CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(CsatolmanyokSubListHeader.Scrollable, CsatolmanyokCPE);
        }
        CsatolmanyokSubListHeader.PageCount = CsatolmanyokGridView.PageCount;
        CsatolmanyokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsatolmanyokGridView);
    }


    void CsatolmanyokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsatolmanyokSubListHeader.RowCount);
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView));
    }

    protected void CsatolmanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(DokumentumokGridView)
            , e.SortExpression, UI.GetSortToGridView("CsatolmanyokGridView", ViewState, e.SortExpression));
    }

    #endregion


    #region Dynamic LockField
    // ha dinamikusan adunk hozzá oszlopokat a GridViewhoz, a postback során elveszik a templatefield
    // sajnos a workaround sem igazán működik...
    // ImageField esetén csak az Image control kódból hozzáadott ID-ja ill. CLientID-ja vész el

    // lokálisan megkeressük a zárolás oszlopot, és utána hívjuk meg az UI.SetLockingInfo metódust
    // az oszlop indexét bekérjük, hogy ne kelljen minden sorra elvégezni a cast + keresés folyamatokat
    public static void GridView_RowDataBound_SetLockingInfoForImageField(GridViewRowEventArgs e, Page page, int lockColumnIndex)
    {
        if (e == null || page == null) return;

        if (lockColumnIndex < 0) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            if (drv != null)
            {
                DataRow row = drv.Row;

                // A LockField oszlop megkeresése
                GridView gv = (GridView)e.Row.NamingContainer;
                if (gv != null)
                {
                    //ArrayList lstGridViewColumns = new ArrayList(gv.Columns);
                    //DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));

                    //int lockColumnIndex = Array.FindIndex<DataControlField>(arrayGridViewColumns,
                    //    delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == lockColumnName; });

                    if (gv.Columns.Count < lockColumnIndex + 1) return;

                    if (e.Row.Cells[lockColumnIndex].Controls.Count > 0)
                    {
                        if (e.Row.Cells[lockColumnIndex].Controls[0] is Image)
                        {
                            Image lockImage = (Image)e.Row.Cells[lockColumnIndex].Controls[0];
                            lockImage.ID = lockedImageName;
                            UI.SetLockingInfo(row, page, lockImage);
                        }
                    }
                }
            }

        }
    }

    #endregion Dynamic LockField
    // BUG_2677
    //private DataControlField getGridViewDataColumn(string headerText)
    //{
    //    foreach (DataControlField dcf in DokumentumokGridView.Columns)
    //    {
    //        if (dcf is BoundField)
    //        {
    //            if (dcf.HeaderText == headerText)
    //            {
    //                return dcf;
    //            }
    //        }
    //    }
    //    return null;
    //}

    public static void SetGridViewColumnVisiblityBySortExpression(GridView gridView, string sortExpression, bool visible)
    {
        for (int i = 0; i < gridView.Columns.Count; i++)
        {
            DataControlField field = gridView.Columns[i];

            if (field.SortExpression == sortExpression)
            {
                field.Visible = visible;
                break;
            }
        }
    }

    public void ClearDokumentElements()
    {
        DokumentumVizualizerComponent.ClearDokumentElements();
    }
}
