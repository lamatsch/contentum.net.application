<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StandardObjektumTargyszavak.ascx.cs"
Inherits="eRecordComponent_StandardObjektumTargyszavak" %>
<asp:Repeater ID="RepeaterStandardTargyszavak" runat="server">
    <HeaderTemplate>
	    <table cellspacing="0" cellpadding="0" width="100%">
    </HeaderTemplate>
    <ItemTemplate>
	    <tr>
	        <td class="mrUrlapCaption">
	            <asp:HiddenField ID="StandardTargyszoTipus_HiddenField" runat="server" Value='<%# Eval("Tipus") %>' />
		        <asp:HiddenField ID="StandardTargyszoId_HiddenField" runat="server" Value='<%# Eval("Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoObjMetaAdataiId_HiddenField" runat="server" Value='<%# Eval("Obj_Metaadatai_Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoRegexp_HiddenField" runat="server" Value='<%# Eval("RegExp") %>' />
		        <asp:HiddenField ID="StandardTargyszoTargyszoId_HiddenField" runat="server" Value='<%# Eval("Targyszo_Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoTargyszo_HiddenField" runat="server" Value='<%# Eval("Targyszo") %>' />
		        <asp:Label ID="LabelStandardTargyszo" runat="server" Text='<%# string.Format("{0}:", Eval("Targyszo")) %>' />
	        </td>
	        <td class="mrUrlapMezo">
		        <asp:TextBox ID="TextBoxStandardTargyszoErtek" runat="server" Text='<%# Eval("Ertek") %>'
		        Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>' />
		        <%-- M�dos�t�s vizsg�lat�hoz --%>
		        <asp:HiddenField ID="Ertek_HiddenField" runat="server" Value='<%# Eval("Ertek") %>' />
		        <asp:HiddenField ID="AlapertelmezettErtek_HiddenField" runat="server" Value='<%# Eval("AlapertelmezettErtek") %>' />
	        </td>
    </ItemTemplate>
    <AlternatingItemTemplate>
	        <td class="mrUrlapCaption">
	            <asp:HiddenField ID="StandardTargyszoTipus_HiddenField" runat="server" Value='<%# Eval("Tipus") %>' />
		        <asp:HiddenField ID="StandardTargyszoId_HiddenField" runat="server" Value='<%# Eval("Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoObjMetaAdataiId_HiddenField" runat="server" Value='<%# Eval("Obj_Metaadatai_Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoRegexp_HiddenField" runat="server" Value='<%# Eval("RegExp") %>' />
		        <asp:HiddenField ID="StandardTargyszoTargyszoId_HiddenField" runat="server" Value='<%# Eval("Targyszo_Id") %>' />
		        <asp:HiddenField ID="StandardTargyszoTargyszo_HiddenField" runat="server" Value='<%# Eval("Targyszo") %>' />
		        <asp:Label ID="LabelStandardTargyszo" runat="server" Text='<%# string.Format("{0}:", Eval("Targyszo")) %>' />
	        </td>
	        <td class="mrUrlapMezo">                                                
		        <asp:TextBox ID="TextBoxStandardTargyszoErtek" runat="server" Text='<%# Eval("Ertek") %>'
		        Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>' />
		        <%-- M�dos�t�s vizsg�lat�hoz --%>
		        <asp:HiddenField ID="Ertek_HiddenField" runat="server" Value='<%# Eval("Ertek") %>' />
		        <asp:HiddenField ID="AlapertelmezettErtek_HiddenField" runat="server" Value='<%# Eval("AlapertelmezettErtek") %>' />
	        </td>
	    </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
	    </table>
    </FooterTemplate>
</asp:Repeater>