using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery.FullTextSearch;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class eRecordComponent_StandardObjektumTargyszavak : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    private Char _Separator = '|';

    public Char Separator
    {
        get { return _Separator; }
        set { _Separator = value; }
    }

    public int Count
    {
        get { return RepeaterStandardTargyszavak.Items.Count; }
    }

    private bool _Enabled = true;

    public bool Enabled
    {
        set { _Enabled = value; }
        get { return _Enabled;  }
    }

    //private bool _ReadOnly = false;

    public bool ReadOnly
    {
        set { ViewState["ReadOnly"] = value; }
        get
        {
            if (ViewState["ReadOnly"] != null)
            {
                return (bool)ViewState["ReadOnly"];
            }
            return false;
        }
    }

    private bool _SearchMode = false;

    public bool SearchMode
    {
        set { _SearchMode = value; }
        get { return _SearchMode; }
    }

    private string _CssClass = "mrUrlapInputKozepes";

    public string CssClass
    {
        set { _CssClass = value; }
        get { return _CssClass; }
    }

    private string _CssClassSearchMode = "mrUrlapInputKozepesFTS";

    public string CssClassSearchMode
    {
        set { _CssClassSearchMode = value; }
        get { return _CssClassSearchMode; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox tb = (TextBox)RepeaterStandardTargyszavak.FindControl("TextBoxStandardTargyszoErtek");
                if (tb != null)
                {
                    tb.CssClass = "ViewReadOnlyWebControl";
                }
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox tb = (TextBox)RepeaterStandardTargyszavak.FindControl("TextBoxStandardTargyszoErtek");
                if (tb != null)
                {
                    tb.CssClass = "ViewDisabledWebControl";
                }
            }
        }
    }


    #endregion

    public FullTextSearchTree BuildFTSTreeFromList(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        string[] targyszoerteklist = GetTargyszoErtekList();
        FullTextSearchTree FTSTree = null;

        try
        {
            if (targyszoerteklist != null && targyszoerteklist.Length > 0)
            {
                foreach (string item in targyszoerteklist)
                {
                    string targyszo = item.Split(Separator)[0];
                    string ertek = item.Split(Separator)[1];
                    if (!String.IsNullOrEmpty(targyszo) && !String.IsNullOrEmpty(ertek))
                    {
                        // ha nincs benne �rt�kelhet� adat, ne is adjon vissza semmit,
                        // ez�rt csak akkor hozzuk l�tre, ha van mit hozz�adni
                        if (FTSTree == null)
                        {
                            FTSTree = new FullTextSearchTree("EREC_ObjektumTargyszavai");
                            FTSTree.SelectFields = "Obj_Id";
                        }

                        if (FTSTree.LeafCount > 0)
                        {
                            FTSTree.AddOperator(FullTextSearchTree.Operator.Intersect);
                        }

                        FTSTree.AddFieldValuePair("Targyszo", targyszo, true);
                        FTSTree.AddFieldValuePair("Ertek", ertek);
                    }
                }

                if (FTSTree != null)
                {
                    // a FormTemplateLoader csak ez alapj�n tudja vissza�ll�tani a mez�ket
                    FTSTree.SetInorderNodeList();
                }
            }
        }
        catch (FullTextSearchException e)
        {
            if (errorPanel != null)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, e.ErrorCode, e.ErrorMessage);
            }
        }
        return FTSTree;
    }

    public void FillFromFTSTree(FullTextSearchTree FTSTree)
    {
        // vigy�zat, felt�telezz�k, hogy felv�ltva lett hozz�adva t�rgysz� �s �rt�k...!
        if (FTSTree != null)
        {
            List<string> items = FTSTree.GetLeafList();
            if (items != null)
            {
                int i = 0;
                while (i + 1 < items.Count)
                {
                    string items_targyszo = items[i];
                    string items_ertek = items[i + 1];
                    string[] item_elements_targyszo = items_targyszo.Split(FullTextSearchTree.FieldValueSeparator);
                    string[] item_elements_ertek = items_ertek.Split(FullTextSearchTree.FieldValueSeparator);
                    // t�rgysz� �rt�k�nek be�r�sa a mez�be
                    TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

                    i += 2;
                }
            }
        }
    }

    /// <summary>
    /// A field �rt�k alapj�n megpr�b�l azonos�tani egy mez�t (label sz�veg) �s kit�lteni a hozz� tartoz� textboxot
    /// a value �rt�kkel.
    /// </summary>
    /// <param name="field">kit�ltend� mez� c�mk�je</param>
    /// <param name="value">textboxba �rand� �rt�k</param>
    /// <returns>igaz, ha a kit�lt�s siker�lt</returns>
    public bool TryFillFieldWithValue(string field, string value)
    {
        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var hf_Targyszo = (HiddenField)item.FindControl("StandardTargyszoTargyszo_HiddenField");
            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
            if (hf_Targyszo != null && tb != null && hf_Targyszo.Value == field)
            {
                tb.Text = value;
                return true;
            }
        }

        return false;
    }

    public void ClearTextBoxValues()
    {
        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
            if (tb != null)
            {
                tb.Text = String.Empty;
            }
        }
    }

    public String GetCheckErtekJavaScript()
    {
        String js = "";

        String js_Header = @"var errormsg = '';
                    var errorcnt = 0;
                    ";

        String js_Footer = @"if (errorcnt > 0)
                    {
                        alert('" + String.Format(Resources.Form.RegularExpressionValidationMessage, "' + '\\n\\n'") + @" + errormsg);
                        return false;
                    }
                    ";

        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var hf_RegExp = (HiddenField)item.FindControl("StandardTargyszoRegexp_HiddenField");
            var hf_Tipus = (HiddenField)item.FindControl("StandardTargyszoTipus_HiddenField");
            var hf_Targyszo = (HiddenField)item.FindControl("StandardTargyszoTargyszo_HiddenField");
            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");

            if (hf_RegExp != null
                && hf_Tipus != null
                && hf_Targyszo != null
                && tb != null
                && hf_Tipus.Value == "1" && !String.IsNullOrEmpty(hf_RegExp.Value))
            {
                js += @"var text = $get('" + tb.ClientID + @"');
                    if(text && text.value != '') {
                        var re = new RegExp('" + hf_RegExp.Value + @"');
                        if (!re.test(text.value)) {
                            errormsg += '" + String.Format("{0}: ' + text.value + ' (RegExp: {1})", hf_Targyszo.Value, hf_RegExp.Value)+ @"\n'
                            errorcnt++;
                        }
                     }
                    ";
            }

        }
        if (!String.IsNullOrEmpty(js))
        {
            js = js_Header + js + js_Footer;
        }
        return js;
    }

    /// <summary>
    /// A m�g nem mentett, csak defin�ci� szinten l�tez�, 1 t�pus� t�rgyszavak eset�n
    /// az alap�rtelmezett �rt�kkel t�lti ki a textboxot.
    /// </summary>
    /// <returns></returns>
    public void SetDefaultValues()
    {
        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var hf_AlapertelmezettErtek = (HiddenField)item.FindControl("Alapertelmezettertek_HiddenField");
            var hf_Tipus = (HiddenField)item.FindControl("StandardTargyszoTipus_HiddenField");
            var hf_Id = (HiddenField)item.FindControl("StandardTargyszoId_HiddenField");

            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
            if (hf_AlapertelmezettErtek != null
                && hf_Tipus != null
                && hf_Id != null
                && tb != null
                && String.IsNullOrEmpty(hf_Id.Value) && hf_Tipus.Value == "1" && !String.IsNullOrEmpty(hf_AlapertelmezettErtek.Value))
            {
                tb.Text = hf_AlapertelmezettErtek.Value;
                // h�tt�r �s el�t�rsz�n jelzi, hogy az �rt�k m�g nincs mentve, hanem az alap�rtelmezett �rt�k ker�lt kit�lt�sre
                tb.CssClass = "GridViewTextBoxChanged";
            }
        }
    }


    /// <summary>
    /// Felt�lti a panelt egy adott objektum t�pusnak megfelel�en
    /// a B1 oszt�ly� hozz�rendel�sekkel,
    /// konkr�t vagy �ltal�nos objektumhoz. Ha a hozz�rendel�s (konkr�t objektum eset�n) l�tezik, lek�rdezi az �rt�keket is.
    /// </summary>
    /// <param name="Obj_Id">Ha meg van adva, azt az objektumot azonos�tja, amelyhez a metaadatokat rendelj�k.</param>
    /// <param name="ObjTip_Id_Tabla">Ha meg van adva, az objektum t�pus�t azonos�tja. Ha nincs megadva, az ObjTip_Kod_Tabla megad�sa k�telez�!</param>
    /// <param name="ObjTip_Kod_Tabla">Ha meg van adva, az objektum t�pus�t azonos�tja. Ha nincs megadva, az ObjTip_Id_Tabla megad�sa k�telez�!</param>
    /// <param name="errorPanel">Hiba ki�r�s�hoz.</param>
    public void FillStandardTargyszavak(String Obj_Id, String ObjTip_Id_Tabla, String ObjTip_Kod_Tabla, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_ObjektumTargyszavaiService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        var result = service.GetAllMetaByDefinicioTipus(ExecParam
            , search
            , Obj_Id
            , ObjTip_Id_Tabla
            , ObjTip_Kod_Tabla
            , KodTarak.OBJMETADEFINICIO_TIPUS.B1
            , true
            , true);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            RepeaterStandardTargyszavak.DataSource = result.Ds;
            RepeaterStandardTargyszavak.DataBind();
        }
        else
        {
            if (errorPanel != null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
    }

    // form --> string[]
    /// <summary>
    /// A t�rgyszavakb�l �s a hozz�rendelend� �rt�keib�l k�pzett, a Separator karakterrel
    /// elv�lasztott p�rokb�l, mint stringekb�l �ll� lista, pl. lek�rdez�sek �ssze�ll�t�s�hoz.
    /// </summary>
    /// <returns></returns>
    public string[] GetTargyszoErtekList()
    {
        List<string> targyszavakList = new List<string>();

        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var hf_Targyszo = (HiddenField)item.FindControl("StandardTargyszoTargyszo_HiddenField");
            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
            if (hf_Targyszo != null && tb != null)
            {
                var targyszo = hf_Targyszo.Value;
                var ertek = tb.Text;
                targyszavakList.Add(targyszo + Separator + ertek);
            }
        }

        return targyszavakList.ToArray();
    }

    // form --> string[]
    /// <summary>
    /// Az aktualiz�land� �rt�keket tartalmaz� rekordlista - ha a t�pus "1" (azaz az �rt�k k�telez�),
    /// �s az �rt�k nincs kit�ltve, nem j�n l�tre rekord
    /// </summary>
    /// <returns></returns>
    public EREC_ObjektumTargyszavai[] GetEREC_ObjektumTargyszavaiList(bool bCsakModositott)
    {
        return GetEREC_ObjektumTargyszavaiList(bCsakModositott, false);
    }

    // form --> string[]
    /// <summary>
    /// Az aktualiz�land� �rt�keket tartalmaz� rekordlista - ha a t�pus "1" (azaz az �rt�k k�telez�),
    /// �s az �rt�k nincs kit�ltve, nem j�n l�tre rekord
    /// </summary>
    /// <returns></returns>
    public EREC_ObjektumTargyszavai[] GetEREC_ObjektumTargyszavaiList(bool bCsakModositott, bool bWithoutRecordId)
    {
        var EREC_ObjektumTargyszavaiList = new List<EREC_ObjektumTargyszavai>();

        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            var hf_Id = (HiddenField)item.FindControl("StandardTargyszoId_HiddenField");
            var hf_ObjMetaAdataiId = (HiddenField)item.FindControl("StandardTargyszoObjMetaAdataiId_HiddenField");
            var hf_TargyszoId = (HiddenField)item.FindControl("StandardTargyszoTargyszoId_HiddenField");
            var hf_Targyszo = (HiddenField)item.FindControl("StandardTargyszoTargyszo_HiddenField");
            var hf_Tipus = (HiddenField)item.FindControl("StandardTargyszoTipus_HiddenField");
            var hf_Ertek = (HiddenField)item.FindControl("Ertek_HiddenField");

            var tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
            if (hf_Id != null && hf_TargyszoId != null && hf_ObjMetaAdataiId != null
                && hf_Targyszo != null && tb != null && hf_Tipus != null
                && !String.IsNullOrEmpty(hf_ObjMetaAdataiId.Value) && !String.IsNullOrEmpty(hf_TargyszoId.Value))
            {
                if (hf_Tipus.Value != "1" || !String.IsNullOrEmpty(tb.Text))
                {
                    if (bCsakModositott == false || (hf_Ertek != null && hf_Ertek.Value != tb.Text))
                    {
                        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                        if (!String.IsNullOrEmpty(hf_Id.Value) && !bWithoutRecordId)
                        {
                            erec_ObjektumTargyszavai.Id = hf_Id.Value;
                            erec_ObjektumTargyszavai.Updated.Id = true;
                        }

                        erec_ObjektumTargyszavai.Obj_Metaadatai_Id = hf_ObjMetaAdataiId.Value;
                        erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                        erec_ObjektumTargyszavai.Targyszo_Id = hf_TargyszoId.Value;
                        erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                        erec_ObjektumTargyszavai.Targyszo = hf_Targyszo.Value;
                        erec_ObjektumTargyszavai.Updated.Targyszo = true;

                        erec_ObjektumTargyszavai.Ertek = tb.Text;
                        erec_ObjektumTargyszavai.Updated.Ertek = true;

                        EREC_ObjektumTargyszavaiList.Add(erec_ObjektumTargyszavai);
                    }
                }
            }
        }

        return EREC_ObjektumTargyszavaiList.ToArray();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //ASP.NET 2.0 bug work around
        //TextBox.Attributes.Add("readonly", "readonly");
        //RepeaterStandardTargyszavak.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
        //RepeaterStandardTargyszavak.ItemCreated += new RepeaterItemEventHandler(Repeater_ItemCreated);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        TextBox tb = (TextBox)item.FindControl("TextBoxStandardTargyszoErtek");
                        if (tb != null)
                        {
                            if (SearchMode)
                            {
                                tb.CssClass = CssClassSearchMode;
                            }
                            else
                            {
                                tb.CssClass = CssClass;
                            }

                            tb.ReadOnly = ReadOnly;
                            tb.Enabled = Enabled;
                        }
                    }
                    break;
                default: break;
            }
        }
    }


    //protected void Repeater_ItemCreated(object sender, RepeaterItemEventArgs e)
    //{
    //    switch (e.Item.ItemType)
    //    {
    //        case ListItemType.Item:
    //        case ListItemType.AlternatingItem:
    //            {
    //                TextBox tb = (TextBox)e.Item.FindControl("TextBoxStandardTargyszoErtek");
    //                if (tb != null)
    //                {
    //                    tb.Enabled = Enabled;
    //                    tb.ReadOnly = ReadOnly;
    //                    //ASP.NET 2.0 bug work around
    //                    tb.Attributes.Add("readonly", "readonly");
    //                }
    //            }
    //            break;
    //    }
    //}

    //protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    TextBox tb = (TextBox)e.Item.FindControl("TextBoxStandardTargyszoErtek");
    //    if (tb != null)
    //    {
    //        tb.Enabled = Enabled;
    //        tb.ReadOnly = ReadOnly;
    //        //ASP.NET 2.0 bug work around
    //        tb.Attributes.Add("readonly", "readonly");
    //    }

    //}


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        //componentList.Add(...);

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        foreach (RepeaterItem item in RepeaterStandardTargyszavak.Items)
        {
            switch (item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    {
                        text += Search.GetReadableWhereOnPanel(item);
                    }
                    break;
                default: break;
            }
        }

        return text.TrimEnd(Search.whereDelimeter.ToCharArray());
    }

    #endregion
}
