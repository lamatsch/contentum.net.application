﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SzamlaAdatokPanel.ascx.cs"
    Inherits="eRecordComponent_SzamlaAdatokPanel" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="../Component/AdoszamTextBox.ascx" TagName="AdoszamTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/BankszamlaszamTextBox.ascx" TagName="BankszamlaszamTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc6" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="../Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="cc" %>
<%@ Register Src="../Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/FullCimField.ascx" TagName="FullCimField" TagPrefix="uc11" %>

<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
    <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    <asp:HiddenField ID="SzamlaId_HiddenField" runat="server" />
    <asp:HiddenField ID="ObjId_HiddenField" runat="server" />
    <hr id="hrTop" runat="server" visible="false" />
    <table cellpadding="0" cellspacing="0" runat="server" id="MainTable" width="100%">
        <tr class="urlapSor" runat="server" id="tr_Szallito">
            <td class="mrUrlapCaption">
                <asp:CheckBox ID="cbSzallitoMegorzes" runat="server" TabIndex="-1" ToolTip="Szállító megőrzése"
                    Visible="false" />
                <asp:Label ID="labelSzallitoStar" runat="server" Text="*" CssClass="ReqStar" Visible="false" />
                <asp:Label ID="labelSzallito" runat="server" Text="Szállító megnevezése:" />
            </td>
            <td class="mrUrlapMezo" colspan="3">
                <uc2:PartnerTextBox ID="PartnerTextBox_Szallito" runat="server" TryFireChangeEvent="true"
                LabelRequiredIndicatorID="labelSzallitoStar" WithAllCimCheckBoxVisible="false"/>
            </td>
              
        </tr>
      
        <tr class="urlapSor" runat="server" id="tr_SzallitoMegorzes">
            <td class="mrUrlapCaption">
                <asp:CheckBox ID="cbSzallitoCimeMegorzes" runat="server" TabIndex="-1" ToolTip="Szállító címének megőrzése"
                    Visible="false" />
                <asp:Label ID="labelSzallitoCimeStar" runat="server" Text="*" CssClass="ReqStar"
                    Visible="false" />
                <asp:Label ID="labelSzallitoCime" runat="server" Text="Szállító címe:" />
            </td>
            <td class="mrUrlapMezo" colspan="3">
                  <table cellspacing="0" cellpadding="0" width="100%">
                    <tr class="urlapSor" runat="server" id="tr_FullCimField">
                        <td class="mrUrlapMezo">
                            <uc11:FullCimField ID="FullCimField1" runat="server" Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td>
                          <uc3:CimekTextBox ID="CimekTextBox_Szallito" runat="server" Validate="false" LabelRequiredIndicatorID="labelSzallitoCimeStar" />
                        </td>
                    </tr>
               </table>
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_Adoszam_PIRAllapot">
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelAdoszamStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelAdoszam" runat="server" Text="Szállító adószáma:" />
            </td>
            <td class="mrUrlapMezo">
                <uc1:AdoszamTextBox ID="AdoszamTextBox1" runat="server" CssClass="mrUrlapInputKozepes"
                    LabelRequiredIndicatorID="labelAdoszamStar" Validate="false" ValidateFormat="true"
                    KulfoldiAdoszamCheckBoxVisible="true" KulfoldiAdoszamCheckBoxTextVisible="true" />
            </td>
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelPIRAllapot" runat="server" Text="PIR állapot:" Visible="false" />
            </td>
            <td class="mrUrlapMezo">
                <uc4:KodtarakDropDownList ID="PIRAllapot_KodTarakDropDownList" runat="server" CssClass="mrUrlapComboBoxKozepes" Visible="false" />
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_Bankszla">
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelBankszamlaszamStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelBankszamlaszam" runat="server" Text="Bankszámlaszám:" />
            </td>
            <td class="mrUrlapMezo">
                <uc1:BankszamlaszamTextBox ID="BankszamlaszamTextBox1" runat="server" Mode="Select"
                    LabelRequiredIndicatorID="labelBankszamlaszamStar" Validate="false"
                    ValidateFormat="true" />
            </td>
           <%-- // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)--%>
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelVevoBesorolasStar" runat="server" Text="*" CssClass="ReqStar" />
                 <asp:Label ID="labelVevoBesorolas" runat="server" Text="Vevő besorolása:" />   
            </td>
            <td class="mrUrlapMezo">
                <uc4:KodtarakDropDownList ID="VevoBesorolas_KodTarakDropDownList" Width="210px"
                    runat="server" LabelRequiredIndicatorID="labelVevoBesorolasStar" />
            </td>
        </tr>
<%--        <tr class="urlapSor">
            <td class="mrUrlapCaption">
                <asp:CheckBox ID="cbFizetesModjaMegorzes" runat="server" TabIndex="-1" ToolTip="Fizetés módjának megőrzése"
                    Visible="false" />
                <asp:Label ID="labelFizetesModjaStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelFizetesModja" runat="server" Text="Fizetés módja:" />
            </td>
            <td class="mrUrlapMezo">
                <uc4:KodtarakDropDownList ID="FizetesModja_KodTarakDropDownList" CssClass="mrUrlapInput"
                    runat="server" LabelRequiredIndicatorID="labelFizetesModjaStar" />
            </td>
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelPIRAllapot" runat="server" Text="PIR állapot:" Visible="false" />
            </td>
            <td class="mrUrlapMezo">
                <uc4:KodtarakDropDownList ID="PIRAllapot_KodTarakDropDownList" CssClass="mrUrlapInputRovid"
                    runat="server" ReadOnly="true" Visible="false" />
            </td>
        </tr>--%>
        <tr class="urlapSor" runat="server" id="tr_SzlaSzamlaSorszam">
            <td class="mrUrlapCaption">
                <asp:Label ID="labelSzamlaSorszamStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelSzamlaSorszam" runat="server" Text="Számlán a szállító által feltüntetett sorszám:" />
            </td>
            <td class="mrUrlapMezo">
                <uc5:RequiredTextBox ID="SzamlaSorszam_TextBox" runat="server" CssClass="mrUrlapInputKozepes"
                    MaxLength="50" LabelRequiredIndicatorID="labelSzamlaSorszamStar" />
            </td>
            <td class="mrUrlapCaption_nowidth" runat="server" id="FizMod">
                <asp:CheckBox ID="cbFizetesModjaMegorzes" runat="server" TabIndex="-1" ToolTip="Fizetés módjának megőrzése"
                    Visible="false" />
                <asp:Label ID="labelFizetesModjaStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelFizetesModja" runat="server" Text="Fizetés módja:" />
            </td>
            <td class="mrUrlapMezo" runat ="server" id="FizMod2">
                <uc4:KodtarakDropDownList ID="FizetesModja_KodTarakDropDownList" Width="210px"
                    runat="server" LabelRequiredIndicatorID="labelFizetesModjaStar" />
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_SzlaBruttoVegosszeg">
            <td class="mrUrlapCaption">
                <asp:Label ID="labelSzamlaBruttoVegosszegeStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelSzamlaBruttoVegosszege" runat="server" Text="Számla bruttó végösszege:" />
            </td>
            <td class="mrUrlapMezo">
                <uc6:RequiredNumberBox ID="SzamlaBruttoVegosszege_NumberTextBox" runat="server" LabelRequiredIndicatorID="labelSzamlaBruttoVegosszegeStar"
                     FilterType="NumbersWithDecimalSeparator" EnableNegativeNumbers="true" />
                <uc1:SzamIntervallum_SearchFormControl ID="SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl"
                    runat="server" FilterType="NumbersWithDecimalSeparator" EnableNegativeNumbers="true" Visible="false" />
            </td>
            <%--        </tr>
        <tr class="urlapSor">--%>
            <td class="mrUrlapCaption_nowidth">
                <asp:CheckBox ID="cbDevizaKodMegorzes" runat="server" TabIndex="-1" ToolTip="Devizakód megőrzése"
                    Visible="false" />
                <asp:Label ID="labelDevizaKod" runat="server" Text="Devizakód:" />
            </td>
            <td class="mrUrlapMezo">
                <uc4:KodtarakDropDownList ID="Devizakod_KodtarakDropDownList" CssClass="mrUrlapInputRovid"
                    runat="server" />
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_SzlaFizetesiHatarideje">
            <td class="mrUrlapCaption">
                <asp:Label ID="labelSzamlaFizetesiHataridejeStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelSzamlaFizetesiHatarideje" runat="server" Text="Számla fizetési határideje:" />
            </td>
            <td class="mrUrlapMezo">
                <cc:CalendarControl ID="SzamlaFizetesiHatarideje_CalendarControl" runat="server"
                    LabelRequiredIndicatorID="labelSzamlaFizetesiHataridejeStar"
                    bOpenDirectionTop="true" />
                <cc:DatumIntervallum_SearchCalendarControl ID="SzamlaFizetesiHatarideje_SearchCalendarControl"
                    runat="server" Validate="false" Visible="false" CalendarPosition="TopRight" />
            </td>
            <%--        </tr>
        <tr class="urlapSor">--%>
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelSzamlaKelteStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelSzamlaKelte" runat="server" Text="Számla kelte:" />
            </td>
            <td class="mrUrlapMezo">
                <cc:CalendarControl ID="SzamlaKelte_CalendarControl" runat="server" Validate="false"
                    LabelRequiredIndicatorID="labelSzamlaKelteStar" bOpenDirectionTop="true" />
                <cc:DatumIntervallum_SearchCalendarControl ID="SzamlaKelte_SearchCalendarControl"
                    runat="server" Validate="false" Visible="false" CalendarPosition="TopRight" />
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_SzlaTeljesitesIdopontja">
            <td class="mrUrlapCaption">
                <asp:Label ID="labelSzamlaTeljesitesIdopontjaStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelSzamlaTeljesitesIdopontja" runat="server" Text="Számlán feltüntetett teljesítés időpontja:" />
            </td>
            <td class="mrUrlapMezo">
                <cc:CalendarControl ID="SzamlaTeljesitesIdopontja_CalendarControl" runat="server"
                    Validate="false" LabelRequiredIndicatorID="labelSzamlaTeljesitesIdopontjaStar"
                    bOpenDirectionTop="true" />
                <cc:DatumIntervallum_SearchCalendarControl ID="SzamlaTeljesitesIdopontja_SearchCalendarControl"
                    runat="server" Validate="false" Visible="false" CalendarPosition="TopRight" />
            </td>
            <%--        </tr>
        <tr class="urlapSor">--%>
            <td class="mrUrlapCaption_nowidth">
                <asp:Label ID="labelVisszakuldesDatumaStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelVisszakuldesDatuma" runat="server" Text="Visszaküldés dátuma:" />
            </td>
            <td class="mrUrlapMezo">
                <cc:CalendarControl ID="VisszakuldesDatuma_CalendarControl" runat="server" Validate="false"
                    LabelRequiredIndicatorID="labelVisszakuldesDatumaStar" bOpenDirectionTop="true" />
                <cc:DatumIntervallum_SearchCalendarControl ID="VisszakuldesDatuma_SearchCalendarControl"
                    runat="server" Validate="false" Visible="false" CalendarPosition="TopRight" />
            </td>
        </tr>
        <tr class="urlapSor" runat="server" id="tr_SzlaMegjegyzes">
            <td class="mrUrlapCaption">
                <asp:Label ID="labelMegjegyzesStar" runat="server" Text="*" CssClass="ReqStar" />
                <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyzés:" />
            </td>
            <td class="mrUrlapMezo">
                <uc5:RequiredTextBox ID="Megjegyzes_TextBox" runat="server" MaxLength="40" CssClass="mrUrlapInput"
                    Validate="false" LabelRequiredIndicatorID="labelMegjegyzesStar" />
            </td>
            <td class="mrUrlapCaption">
            </td>
            <td class="mrUrlapMezo" colspan="3">
            </td>
        </tr>
    </table>
    <hr id="hrBottom" runat="server" visible="false" />
</asp:Panel>
