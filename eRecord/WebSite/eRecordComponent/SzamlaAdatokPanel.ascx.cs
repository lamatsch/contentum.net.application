﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using System.Text;
using System.Data;

public partial class eRecordComponent_SzamlaAdatokPanel : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent 
{

    private const string kcs_FIZETESI_MOD = "FIZETESI_MOD";
    private const string kcs_PIR_ALLAPOT = "PIR_ALLAPOT";
    private const string kcs_DEVIZAKOD = "DEVIZAKOD";

    private const string Devizakod_Default = "HUF";
    private const string Fizetesi_Mod_Default = "FIZU"; // Banki átutalás

    // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
    private const string kcs_SZAMLA_VEVOBESOROLAS = "SZAMLA_VEVOBESOROLAS";
    private const string VevoBesorolas_Default = "POLGHIV";


    private bool isPirEnabled = false;

    #region Public Properties

    public enum SzuloType
    {
        None,
        Kuldemeny,
        Dokumentum
    }

    private string _Mode = String.Empty;
    private string Mode
    {
        get { return this._Mode; }
        set { this._Mode = value; }
    }

    // alapértelmezés: küldemény
    private SzuloType _SzuloTipus = SzuloType.None;
    public SzuloType SzuloTipus
    {
        get { return this._SzuloTipus; }
        set { this._SzuloTipus = value; }
    }

    public bool HrAtTopVisible
    {
        set { hrTop.Visible = value; }
        get { return hrTop.Visible; }
    }

    public bool HrAtBottomVisible
    {
        set { hrBottom.Visible = value; }
        get { return hrBottom.Visible; }
    }

    public bool Visible
    {
        set { MainPanel.Visible = value; }
        get { return MainPanel.Visible; }
    }

    public bool ReadOnly
    {
        set
        {
            PartnerTextBox_Szallito.ReadOnly = value;
            CimekTextBox_Szallito.ReadOnly = value;
            AdoszamTextBox1.ReadOnly = value;
            BankszamlaszamTextBox1.ReadOnly = value;
            FizetesModja_KodTarakDropDownList.ReadOnly = value;
            SzamlaBruttoVegosszege_NumberTextBox.ReadOnly = value;
            Devizakod_KodtarakDropDownList.ReadOnly = value;
            SzamlaFizetesiHatarideje_CalendarControl.ReadOnly = value;
            SzamlaKelte_CalendarControl.ReadOnly = value;
            SzamlaSorszam_TextBox.ReadOnly = value;
            SzamlaTeljesitesIdopontja_CalendarControl.ReadOnly = value;
            VisszakuldesDatuma_CalendarControl.ReadOnly = value;
            Megjegyzes_TextBox.ReadOnly = value;

            cbSzallitoMegorzes.Enabled = !value;
            cbSzallitoCimeMegorzes.Enabled = !value;
            cbFizetesModjaMegorzes.Enabled = !value;
            cbDevizaKodMegorzes.Enabled = !value;

            PIRAllapot_KodTarakDropDownList.ReadOnly = true;
            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            VevoBesorolas_KodTarakDropDownList.ReadOnly = value;

            // BUG_4731
            if (isBekuldoPartnerCimNemTorzsbolDisabled)
            {
                tr_FullCimField.Visible = false;
            }
            else
               tr_FullCimField.Visible = !value;
            
            SetComponentsValidateStatus(!value);
        }

        get { return PartnerTextBox_Szallito.ReadOnly; }
    }

    public bool SearchMode
    {
        set
        {
            PartnerTextBox_Szallito.SearchMode = value;
            CimekTextBox_Szallito.SearchMode = value;
            AdoszamTextBox1.SearchMode = value;
            BankszamlaszamTextBox1.SearchMode = value;
            //FizetesModja_KodTarakDropDownList.SearchMode = value;
            //SzamlaBruttoVegosszege_NumberTextBox.SearchMode = value;
            //Devizakod_KodtarakDropDownList.SearchMode = value;
            //SzamlaFizetesiHatarideje_CalendarControl.SearchMode = value;
            //SzamlaKelte_CalendarControl.SearchMode = value;
            //SzamlaSorszam_TextBox.SearchMode = value;
            //SzamlaTeljesitesIdopontja_CalendarControl.SearchMode = value;
            //VisszakuldesDatuma_CalendarControl.SearchMode = value;
            //PIRAllapot_KodTarakDropDownList.SearchMode = false;
            //Megjegyzes_TextBox.SearchMode = value;

            cbSzallitoMegorzes.Enabled = !value;
            cbSzallitoCimeMegorzes.Enabled = !value;
            cbFizetesModjaMegorzes.Enabled = !value;
            cbDevizaKodMegorzes.Enabled = !value;

            SetComponentsValidateStatus(!value);

            if (value)
            {

                Mode = CommandName.Search;

                // ha a kontrol nem látható, nem fut le a PreRender, ezért explicit el kell tüntetni
                labelAdoszamStar.Visible = false;
                labelSzallitoStar.Visible = false;
                labelSzallitoCimeStar.Visible = false;
                labelFizetesModjaStar.Visible = false;
                labelSzamlaBruttoVegosszegeStar.Visible = false;
                //labelDevizaKodStar.Visible = false;
                labelSzamlaFizetesiHataridejeStar.Visible = false;
                labelSzamlaKelteStar.Visible = false;
                labelSzamlaSorszamStar.Visible = false;
                labelSzamlaTeljesitesIdopontjaStar.Visible = false;
                labelVisszakuldesDatumaStar.Visible = false;
                labelMegjegyzesStar.Visible = false;

                // keresésnél speciálisan select üzemmód
                // (nem search, mert itt most csak Id szerint tudunk keresni, szövegre nem!)
                BankszamlaszamTextBox1.Mode = Component_BankszamlaszamTextBox.ModeType.Select;
            }

            SzamlaBruttoVegosszege_NumberTextBox.Visible = !value;
            SzamlaFizetesiHatarideje_CalendarControl.Visible = !value;
            SzamlaKelte_CalendarControl.Visible = !value;
            SzamlaTeljesitesIdopontja_CalendarControl.Visible = !value;
            VisszakuldesDatuma_CalendarControl.Visible = !value;

            SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl.Visible = value;
            SzamlaFizetesiHatarideje_SearchCalendarControl.Visible = value;
            SzamlaKelte_SearchCalendarControl.Visible = value;
            SzamlaTeljesitesIdopontja_SearchCalendarControl.Visible = value;
            VisszakuldesDatuma_SearchCalendarControl.Visible = value;
        }
        get { return PartnerTextBox_Szallito.SearchMode; }
    }

    public string Partner_Id_Szallito
    {
        get { return PartnerTextBox_Szallito.Id_HiddenField; }
    }

    public Component_EditablePartnerTextBox Szallito_PartnerTextBox
    {
        get { return this.PartnerTextBox_Szallito; }
    }

    public string Cim_Id_Szallito
    {
        get { return CimekTextBox_Szallito.Id_HiddenField; }
    }

    public Component_EditableCimekTextBox Szallito_CimekTextBox
    {
        get { return this.CimekTextBox_Szallito; }
    }

    public string Adoszam
    {
        get { return AdoszamTextBox1.Text; }
    }

    public bool IsAdoszamVisible
    {
        get
        {
            return AdoszamTextBox1.Visible;
        }
    }

    public bool IsKulfoldiAdoszam
    {
        get { return AdoszamTextBox1.IsKulfoldiAdoszam; }
    }

    public bool IsAdoszamChanged
    {
        get { return AdoszamTextBox1.IsAdoszamChanged; }
    }

    public string Bankszamlaszam_Id
    {
        get { return BankszamlaszamTextBox1.Id_HiddenField; }
    }

    public string Bankszamlaszam
    {
        get { return BankszamlaszamTextBox1.Text; }
    }

    // szabályozza, hogy felvihetõ-e nem törzsbõl vett partner és cím a beküldõhöz
    bool isBekuldoPartnerCimNemTorzsbolDisabled;

    // szabályozza, hogy bevihetõ-e szabad szöveg a konkatenált címmezõbe
    bool isCimSzabadSzovegEnabled;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        // BUG_4731
        isBekuldoPartnerCimNemTorzsbolDisabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED) == false;
        isCimSzabadSzovegEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.CIM_SZABAD_SZOVEG_ENABLED);

        // adószám textbox partnerhez kötése
        AdoszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
        // bankszámlaszám textbox partnerhez kötése
        BankszamlaszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;

        // BUG_4731
        //PartnerTextBox_Szallito.WithKuldemeny = false;

        //Partner és cim összekötése
        PartnerTextBox_Szallito.CimTextBox = CimekTextBox_Szallito.TextBox;
        PartnerTextBox_Szallito.CimHiddenField = CimekTextBox_Szallito.HiddenField;


        if (isBekuldoPartnerCimNemTorzsbolDisabled)
        {
            CimekTextBox_Szallito.FreeTextEnabled = false;
            PartnerTextBox_Szallito.FreeTextEnabled = false;
            // strukturált cím (csak törzsbõl lehet)
            tr_FullCimField.Visible = false;
            PartnerTextBox_Szallito.WithKuldemeny = false;
        } else
        {
            CimekTextBox_Szallito.FreeTextEnabled = isCimSzabadSzovegEnabled;   // rendszerparaméter szabályozza
            PartnerTextBox_Szallito.FreeTextEnabled = true;
            PartnerTextBox_Szallito.WithKuldemeny = false;
            //tr_FullCimField.Visible = isCimSzabadSzovegEnabled;
            //Cim összekötése
            FullCimField1.CimTextBox = CimekTextBox_Szallito.TextBox;
            FullCimField1.CimHiddenField = CimekTextBox_Szallito.HiddenField;
        }

         PartnerTextBox_Szallito.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(86);
        CimekTextBox_Szallito.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(86);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        isPirEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PIR_INTERFACE_ENABLED);
        // trükk: a rendszerparaméter csak ezen a ponton kerül beállításra,
        // ezért a láthatóságokat itt újra beállítjuk a hMode aktuális értékének megfelelően
        SetComponentsVisibilityByPIR(Mode, isPirEnabled);
    }

    private void SetComponentsVisibilityByPIR(string Mode, bool isPirEnabled)
    {
        switch (Mode)
        {
            case CommandName.New:
                tr_Adoszam_PIRAllapot.Visible = true;
                labelPIRAllapot.Visible = false;
                PIRAllapot_KodTarakDropDownList.Visible = false;
                break;
            case CommandName.View:
            case CommandName.Modify:
                tr_Adoszam_PIRAllapot.Visible = true;
                labelPIRAllapot.Visible = isPirEnabled;
                PIRAllapot_KodTarakDropDownList.Visible = isPirEnabled;
                break;
            case CommandName.Search:
                tr_Adoszam_PIRAllapot.Visible = isPirEnabled;
                labelPIRAllapot.Visible = isPirEnabled;
                PIRAllapot_KodTarakDropDownList.Visible = isPirEnabled;
                // adószámra nem lehet keresni
                labelAdoszamStar.Visible = false;
                labelAdoszam.Visible = false;
                AdoszamTextBox1.Visible = false;
                break;
        }
    }

    // komponensek beállítása (SzamlaFejFelvitel)
    public void SetComponentsForFelvitel(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Mode = CommandName.New;

        cbSzallitoMegorzes.Visible = true;
        cbSzallitoCimeMegorzes.Visible = true;
        cbFizetesModjaMegorzes.Visible = true;
        cbDevizaKodMegorzes.Visible = true;

        labelPIRAllapot.Visible = false;
        PIRAllapot_KodTarakDropDownList.Visible = false;

        SetComponentsValidateStatus(true);

        FizetesModja_KodTarakDropDownList.FillAndSetSelectedValue(kcs_FIZETESI_MOD, Fizetesi_Mod_Default, false, errorPanel);

        AdoszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
        BankszamlaszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
        BankszamlaszamTextBox1.Mode = Component_BankszamlaszamTextBox.ModeType.Select;
        BankszamlaszamTextBox1.ValidateFormat = false;


        Devizakod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DEVIZAKOD, Devizakod_Default, true, errorPanel);

        if (String.IsNullOrEmpty(PartnerTextBox_Szallito.Id_HiddenField))
        {
            // Ha el volt mentve Session-be, onnan olvassuk ki
            if (Session["Szamla_Szallito"] != null)
            {
                PartnerTextBox_Szallito.Id_HiddenField = Session["Szamla_Szallito"].ToString();
                PartnerTextBox_Szallito.SetPartnerTextBoxById(errorPanel);

                AdoszamTextBox1.SetTextBoxByPartnerId(errorPanel);

                cbSzallitoMegorzes.Checked = true;
            }
            else
            {
                //PartnerTextBox_Szallito.Id_HiddenField = String.Empty;
                //PartnerTextBox_Szallito.SetPartnerTextBoxById(errorPanel);
                cbSzallitoMegorzes.Checked = false;
            }

            // BUG_4731
            //if (Session["Szamla_SzallitoCime"] != null)
            //{
            //    CimekTextBox_Szallito.Id_HiddenField = Session["Szamla_SzallitoCime"].ToString();
            //    CimekTextBox_Szallito.SetCimekTextBoxById(errorPanel);

            //    cbSzallitoCimeMegorzes.Checked = true;
            //}
            //else
            //{
            //    //CimekTextBox_Szallito.Id_HiddenField = String.Empty;
            //    //CimekTextBox_Szallito.SetCimekTextBoxById(errorPanel);
            //    cbSzallitoCimeMegorzes.Checked = false;
            //}

            if (UI.GetSession(Page, "CimChecked") == "true")
            {
                CimekTextBox_Szallito.Id_HiddenField = UI.GetSession(Page, "CimId");
                CimekTextBox_Szallito.Text = UI.GetSession(Page, "CimText");
                cbSzallitoCimeMegorzes.Checked = true;
            }
            else 
            //if (!_TemplateMode)
            {
                CimekTextBox_Szallito.Id_HiddenField = "";
                CimekTextBox_Szallito.Text = "";
            }
 
            if (Session["Szamla_FizetesModja"] != null)
            {
                FizetesModja_KodTarakDropDownList.SetSelectedValue(Session["Szamla_FizetesModja"].ToString());
                cbFizetesModjaMegorzes.Checked = true;
            }
            else
            {
                //FizetesModja_KodTarakDropDownList.SetSelectedValue(String.Empty);
                cbFizetesModjaMegorzes.Checked = false;
            }

            if (Session["Szamla_DevizaKod"] != null)
            {
                Devizakod_KodtarakDropDownList.SetSelectedValue(Session["Szamla_DevizaKod"].ToString());
                cbDevizaKodMegorzes.Checked = true;
            }
            else
            {
                cbDevizaKodMegorzes.Checked = false;
            }

            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            VevoBesorolas_KodTarakDropDownList.FillAndSetSelectedValue(kcs_SZAMLA_VEVOBESOROLAS,VevoBesorolas_Default, false, errorPanel);
        }
    }

    // komponensek beállítása (SzamlaFejModositas)
    public void SetComponentsForModositas(string Parent_Obj_Id, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Mode = CommandName.Modify;

        labelPIRAllapot.Visible = isPirEnabled;
        PIRAllapot_KodTarakDropDownList.Visible = isPirEnabled;

        BankszamlaszamTextBox1.Mode = Component_BankszamlaszamTextBox.ModeType.Select;
        BankszamlaszamTextBox1.ValidateFormat = false;

        DataRow szamlaRow = LoadComponents(Parent_Obj_Id, true, errorPanel, errorUpdatePanel);

        SetComponentsValidateStatus(true);

        //FizetesModja_KodTarakDropDownList.FillDropDownList(kcs_FIZETESI_MOD, false, errorPanel);
    }

    //// komponensek beállítása (Search)
    //public void SetComponentsForSearch(EREC_SzamlakSearch erec_SzamlakSearch, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    //{
    //    LoadComponentsFromSearchObject(erec_SzamlakSearch, errorPanel, errorUpdatePanel);

    //    this.SearchMode = true;
    //}

    /// <summary>
    /// Feltölti a komponenseket, és beállít mindent ReadOnlyba
    /// </summary>
    public void SetComponents_ViewMode(DataRow szamlaRow, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Mode = CommandName.View;

        labelPIRAllapot.Visible = isPirEnabled;
        PIRAllapot_KodTarakDropDownList.Visible = isPirEnabled;

        BankszamlaszamTextBox1.Mode = Component_BankszamlaszamTextBox.ModeType.Search;

        LoadComponents(szamlaRow, false, errorPanel, errorUpdatePanel);

        ReadOnly = true;
        
    }

    public void ClearForm()
    {
        PartnerTextBox_Szallito.Id_HiddenField = String.Empty;
        PartnerTextBox_Szallito.SetPartnerTextBoxById(null);
        AdoszamTextBox1.Text = String.Empty;
        BankszamlaszamTextBox1.Id_HiddenField = String.Empty;
        BankszamlaszamTextBox1.SetTextBoxById(null);
        FizetesModja_KodTarakDropDownList.Clear();
        SzamlaBruttoVegosszege_NumberTextBox.Text = String.Empty;
        Devizakod_KodtarakDropDownList.Clear();
        SzamlaFizetesiHatarideje_CalendarControl.Text = String.Empty;
        SzamlaKelte_CalendarControl.Text = String.Empty;
        SzamlaSorszam_TextBox.Text = String.Empty;
        SzamlaTeljesitesIdopontja_CalendarControl.Text = String.Empty;
        VisszakuldesDatuma_CalendarControl.Text = String.Empty;
        PIRAllapot_KodTarakDropDownList.Clear();
        Megjegyzes_TextBox.Text = String.Empty;

        SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl.SzamTol = "";
        SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl.SzamIg = "";
        SzamlaFizetesiHatarideje_SearchCalendarControl.Clear();
        SzamlaKelte_SearchCalendarControl.Clear();
        SzamlaTeljesitesIdopontja_SearchCalendarControl.Clear();
        VisszakuldesDatuma_SearchCalendarControl.Clear();

        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        VevoBesorolas_KodTarakDropDownList.Clear();
    }


    private DataRow LoadComponents(string Parent_Obj_Id, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Parent_Obj_Id))
        {

            EREC_SzamlakSearch search = new EREC_SzamlakSearch();
            if (SzuloTipus == SzuloType.Kuldemeny)
            {
                search.KuldKuldemeny_Id.Value = Parent_Obj_Id;
                search.KuldKuldemeny_Id.Operator = Query.Operators.equals;
            }
            else if (SzuloTipus == SzuloType.Dokumentum)
            {
                search.Dokumentum_Id.Value = Parent_Obj_Id;
                search.Dokumentum_Id.Operator = Query.Operators.equals;
            }
            else
            {
                // TODO: hiba
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Resources.Error.ErrorLabel,
                    String.Format("Szülőtípus nincs implementálva: {0}", Enum.GetName(typeof(SzuloType), SzuloTipus)));
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
                return null;
            }

            EREC_SzamlakService service = eRecordService.ServiceFactory.GetEREC_SzamlakService();
            ExecParam execParam = UI.SetExecParamDefault(Page);
            Result result = service.GetAll(execParam, search);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
                return null;
            }

            if (result.Ds.Tables[0].Rows.Count != 1)
            {
                // TODO: új hibakód (A rekord nem található! helyett)
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_50101);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
                return null;
            }
            else
            {
                DataRow szamlaRow = result.Ds.Tables[0].Rows[0];
                LoadComponents(szamlaRow, fillDropDownList, errorPanel, errorUpdatePanel);
                return szamlaRow;
            }
        }

        // TODO: új hibakód (A rekord nem található! helyett)
        ResultError.DisplayNoIdParamError(errorPanel);
        if (errorUpdatePanel != null)
        {
            errorUpdatePanel.Update();
        }
        return null;
    }

    /// <summary>
    /// Panel feltöltése adatokkal
    /// </summary>
    private void LoadComponents(DataRow szamlaRow, bool fillDropDownList, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (szamlaRow != null)
        {
            #region Komponensek feltöltése

            SzamlaId_HiddenField.Value = szamlaRow["Id"].ToString();

            if (SzuloTipus == SzuloType.Kuldemeny)
            {
                ObjId_HiddenField.Value = szamlaRow["KuldKuldemeny_Id"].ToString();
            }
            else if (SzuloTipus == SzuloType.Dokumentum)
            {
                ObjId_HiddenField.Value = szamlaRow["Dokumentum_Id"].ToString();
            }

            PartnerTextBox_Szallito.Id_HiddenField = szamlaRow["Partner_Id_Szallito"].ToString();
            // BUG_4731
            //PartnerTextBox_Szallito.SetPartnerTextBoxById(errorPanel);
            PartnerTextBox_Szallito.Text = szamlaRow["NevSTR_Szallito"].ToString();

            CimekTextBox_Szallito.Id_HiddenField = szamlaRow["Cim_Id_Szallito"].ToString();
            // BUG_4731
            //CimekTextBox_Szallito.SetCimekTextBoxById(errorPanel);
            CimekTextBox_Szallito.Text = szamlaRow["CimSTR_Szallito"].ToString();

            //AdoszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
            AdoszamTextBox1.SetTextBoxByPartnerId(errorPanel);

            BankszamlaszamTextBox1.Id_HiddenField = szamlaRow["Bankszamlaszam_Id"].ToString();
            BankszamlaszamTextBox1.SetTextBoxById(errorPanel);

            FizetesModja_KodTarakDropDownList.SetSelectedValue("");

            if (!String.IsNullOrEmpty(szamlaRow["FizetesiMod"].ToString()))
            {
                FizetesModja_KodTarakDropDownList.FillAndSetSelectedValue(kcs_FIZETESI_MOD,
                    szamlaRow["FizetesiMod"].ToString(), false, errorPanel);
            }
            else
            {
                if (fillDropDownList)
                {
                    FizetesModja_KodTarakDropDownList.FillDropDownList(kcs_FIZETESI_MOD, false, errorPanel);
                }
            }

            PIRAllapot_KodTarakDropDownList.SetSelectedValue("");

            if (!String.IsNullOrEmpty(szamlaRow["PIRAllapot"].ToString()))
            {
                PIRAllapot_KodTarakDropDownList.FillAndSetSelectedValue(kcs_PIR_ALLAPOT,
                    szamlaRow["PIRAllapot"].ToString(), true, errorPanel);
            }
            else
            {
                if (fillDropDownList)
                {
                    PIRAllapot_KodTarakDropDownList.FillDropDownList(kcs_PIR_ALLAPOT, true, errorPanel);
                }
            }

            SzamlaBruttoVegosszege_NumberTextBox.Text = szamlaRow["EllenorzoOsszeg"].ToString();

            Devizakod_KodtarakDropDownList.SetSelectedValue("");

            if (!String.IsNullOrEmpty(szamlaRow["Devizakod"].ToString()))
            {
                Devizakod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DEVIZAKOD,
                    szamlaRow["Devizakod"].ToString(), true, errorPanel);
            }
            else
            {
                if (fillDropDownList)
                {
                    Devizakod_KodtarakDropDownList.FillDropDownList(kcs_DEVIZAKOD, true, errorPanel);
                }
            }

            SzamlaFizetesiHatarideje_CalendarControl.Text = szamlaRow["FizetesiHatarido"].ToString();
            SzamlaKelte_CalendarControl.Text = szamlaRow["BizonylatDatuma"].ToString();
            SzamlaSorszam_TextBox.Text = szamlaRow["SzamlaSorszam"].ToString();
            SzamlaTeljesitesIdopontja_CalendarControl.Text = szamlaRow["TeljesitesDatuma"].ToString();
            VisszakuldesDatuma_CalendarControl.Text = szamlaRow["VisszakuldesDatuma"].ToString();
            Megjegyzes_TextBox.Text = szamlaRow["Note"].ToString();

            Record_Ver_HiddenField.Value = szamlaRow["Ver"].ToString();

            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            VevoBesorolas_KodTarakDropDownList.SetSelectedValue("");
            if (!String.IsNullOrEmpty(szamlaRow["VevoBesorolas"].ToString()))  
            {
                VevoBesorolas_KodTarakDropDownList.FillAndSetSelectedValue(kcs_SZAMLA_VEVOBESOROLAS,
                    szamlaRow["VevoBesorolas"].ToString(), false, errorPanel);
            }
            else
            {
                if (fillDropDownList)
                {
                    VevoBesorolas_KodTarakDropDownList.FillDropDownList(kcs_SZAMLA_VEVOBESOROLAS, false, errorPanel);
                }
            }

            #endregion

        }
    }

    /// <summary>
    /// Üzleti objektum --> Form
    /// </summary>
    public void LoadComponentsFromBusinessObject(EREC_Szamlak erec_Szamlak, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (erec_Szamlak != null)
        {
            #region Komponensek feltöltése

            //if (SzuloTipus == SzuloType.Kuldemeny)
            //{
            //    ObjId_HiddenField.Value = erec_Szamlak.KuldKuldemeny_Id;
            //}
            //else if (SzuloTipus == SzuloType.Dokumentum)
            //{
            //    ObjId_HiddenField.Value = erec_Szamlak.Dokumentum_Id;
            //}

            PartnerTextBox_Szallito.Id_HiddenField = erec_Szamlak.Partner_Id_Szallito;
            // BUG_4731
            //PartnerTextBox_Szallito.SetPartnerTextBoxById(errorPanel);
            PartnerTextBox_Szallito.Text = erec_Szamlak.NevSTR_Szallito;

            CimekTextBox_Szallito.Id_HiddenField = erec_Szamlak.Cim_Id_Szallito;
            // BUG_4731
            //CimekTextBox_Szallito.SetCimekTextBoxById(errorPanel);
            CimekTextBox_Szallito.Text = erec_Szamlak.CimSTR_Szallito;

            // Adószám
            AdoszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
            AdoszamTextBox1.SetTextBoxByPartnerId(errorPanel);

            // Bankszámlaszám
            //BankszamlaszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
            BankszamlaszamTextBox1.Id_HiddenField = erec_Szamlak.Bankszamlaszam_Id;
            BankszamlaszamTextBox1.SetTextBoxById(errorPanel);

            FizetesModja_KodTarakDropDownList.FillAndSetSelectedValue(kcs_FIZETESI_MOD,
                    erec_Szamlak.FizetesiMod, false, errorPanel);

            PIRAllapot_KodTarakDropDownList.FillAndSetSelectedValue(kcs_PIR_ALLAPOT,
                    erec_Szamlak.FizetesiMod, true, errorPanel);

            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            VevoBesorolas_KodTarakDropDownList.FillAndSetSelectedValue(kcs_SZAMLA_VEVOBESOROLAS,
                    erec_Szamlak.VevoBesorolas, false, errorPanel);       

            SzamlaBruttoVegosszege_NumberTextBox.Text = erec_Szamlak.EllenorzoOsszeg;

            Devizakod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DEVIZAKOD,
                    erec_Szamlak.DevizaKod, true, errorPanel);

            SzamlaFizetesiHatarideje_CalendarControl.Text = erec_Szamlak.FizetesiHatarido;
            SzamlaKelte_CalendarControl.Text = erec_Szamlak.BizonylatDatuma;
            SzamlaSorszam_TextBox.Text = erec_Szamlak.SzamlaSorszam;
            SzamlaTeljesitesIdopontja_CalendarControl.Text = erec_Szamlak.TeljesitesDatuma;
            VisszakuldesDatuma_CalendarControl.Text = erec_Szamlak.VisszakuldesDatuma;
            Megjegyzes_TextBox.Text = erec_Szamlak.Base.Note;
            #endregion

        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    public void LoadComponentsFromSearchObject(EREC_SzamlakSearch erec_SzamlakSearch, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (erec_SzamlakSearch != null)
        {
            #region Komponensek feltöltése

            if (SzuloTipus == SzuloType.Kuldemeny)
            {
                ObjId_HiddenField.Value = erec_SzamlakSearch.KuldKuldemeny_Id.Value;
            }
            else if (SzuloTipus == SzuloType.Dokumentum)
            {
                ObjId_HiddenField.Value = erec_SzamlakSearch.Dokumentum_Id.Value;
            }

            PartnerTextBox_Szallito.Id_HiddenField = erec_SzamlakSearch.Partner_Id_Szallito.Value;
            PartnerTextBox_Szallito.SetPartnerTextBoxById(errorPanel);

            CimekTextBox_Szallito.Id_HiddenField = erec_SzamlakSearch.Cim_Id_Szallito.Value;
            CimekTextBox_Szallito.SetCimekTextBoxById(errorPanel);

            // Adószámra nem lehet keresni
            ////AdoszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;
            //AdoszamTextBox1.SetTextBoxByPartnerId(errorPanel);

            BankszamlaszamTextBox1.PartnerTextBox = PartnerTextBox_Szallito;

            BankszamlaszamTextBox1.Id_HiddenField = erec_SzamlakSearch.Bankszamlaszam_Id.Value;
            BankszamlaszamTextBox1.SetTextBoxById(errorPanel);

            FizetesModja_KodTarakDropDownList.FillAndSetSelectedValue(kcs_FIZETESI_MOD,
                    erec_SzamlakSearch.FizetesiMod.Value, true, errorPanel);

            PIRAllapot_KodTarakDropDownList.FillAndSetSelectedValue(kcs_PIR_ALLAPOT,
                    erec_SzamlakSearch.PIRAllapot.Value, true, errorPanel);

            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            VevoBesorolas_KodTarakDropDownList.FillAndSetSelectedValue(kcs_SZAMLA_VEVOBESOROLAS,
                   erec_SzamlakSearch.VevoBesorolas.Value, false, errorPanel);      

            SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_SzamlakSearch.EllenorzoOsszeg);

            Devizakod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_DEVIZAKOD,
                    erec_SzamlakSearch.DevizaKod.Value, true, errorPanel);

            SzamlaFizetesiHatarideje_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_SzamlakSearch.FizetesiHatarido);
            SzamlaKelte_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_SzamlakSearch.BizonylatDatuma);
            SzamlaSorszam_TextBox.Text = erec_SzamlakSearch.SzamlaSorszam.Value;
            SzamlaTeljesitesIdopontja_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_SzamlakSearch.TeljesitesDatuma);
            VisszakuldesDatuma_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_SzamlakSearch.VisszakuldesDatuma);
            Megjegyzes_TextBox.Text = erec_SzamlakSearch.Manual_Note.Value;
            #endregion

        }
    }

    public EREC_Szamlak GetBusinessObjectFromComponents()
    {
        EREC_Szamlak erec_Szamlak = new EREC_Szamlak();
        erec_Szamlak.Updated.SetValueAll(false);
        erec_Szamlak.Base.Updated.SetValueAll(false);

        erec_Szamlak.Id = SzamlaId_HiddenField.Value;

        if (SzuloTipus == SzuloType.Kuldemeny)
        {
            erec_Szamlak.KuldKuldemeny_Id = ObjId_HiddenField.Value;
            erec_Szamlak.Updated.KuldKuldemeny_Id = true;
        }
        else if (SzuloTipus == SzuloType.Dokumentum)
        {
            erec_Szamlak.Dokumentum_Id = ObjId_HiddenField.Value;
            erec_Szamlak.Updated.Dokumentum_Id = true;
        }

        erec_Szamlak.Partner_Id_Szallito = PartnerTextBox_Szallito.Id_HiddenField;
        erec_Szamlak.Updated.Partner_Id_Szallito = true;

        erec_Szamlak.Cim_Id_Szallito = CimekTextBox_Szallito.Id_HiddenField;
        erec_Szamlak.Updated.Cim_Id_Szallito = true;

        // BUG_4731
        erec_Szamlak.NevSTR_Szallito = PartnerTextBox_Szallito.Text;
        erec_Szamlak.Updated.NevSTR_Szallito = true;

        erec_Szamlak.CimSTR_Szallito = CimekTextBox_Szallito.Text;
        erec_Szamlak.Updated.CimSTR_Szallito = true;

        erec_Szamlak.Bankszamlaszam_Id = BankszamlaszamTextBox1.Id_HiddenField;
        erec_Szamlak.Updated.Bankszamlaszam_Id = true;

        erec_Szamlak.FizetesiMod = FizetesModja_KodTarakDropDownList.SelectedValue;
        erec_Szamlak.Updated.FizetesiMod = true;

        // ReadOnly!
        //erec_Szamlak.PIRAllapot = PIRAllapot_KodTarakDropDownList.SelectedValue;
        //erec_Szamlak.Updated.PIRAllapot = true;

        erec_Szamlak.EllenorzoOsszeg = (string.IsNullOrEmpty(SzamlaBruttoVegosszege_NumberTextBox.Text)) ? 0.ToString() : SzamlaBruttoVegosszege_NumberTextBox.Text;                 
        erec_Szamlak.Updated.EllenorzoOsszeg = true;

        erec_Szamlak.DevizaKod = Devizakod_KodtarakDropDownList.SelectedValue;
        erec_Szamlak.Updated.DevizaKod = true;

        erec_Szamlak.FizetesiHatarido = SzamlaFizetesiHatarideje_CalendarControl.Text;
        erec_Szamlak.Updated.FizetesiHatarido = true;

        erec_Szamlak.BizonylatDatuma = SzamlaKelte_CalendarControl.Text;
        erec_Szamlak.Updated.BizonylatDatuma = true;

        erec_Szamlak.SzamlaSorszam = SzamlaSorszam_TextBox.Text;
        erec_Szamlak.Updated.SzamlaSorszam = true;

        erec_Szamlak.TeljesitesDatuma = SzamlaTeljesitesIdopontja_CalendarControl.Text;
        erec_Szamlak.Updated.TeljesitesDatuma = true;

        erec_Szamlak.VisszakuldesDatuma = VisszakuldesDatuma_CalendarControl.Text;
        erec_Szamlak.Updated.VisszakuldesDatuma = true;

        erec_Szamlak.Base.Note = Megjegyzes_TextBox.Text;
        erec_Szamlak.Base.Updated.Note = true;

        // a módosítás miatt szükséges
        erec_Szamlak.Base.Ver = Record_Ver_HiddenField.Value;
        erec_Szamlak.Base.Updated.Ver = true;

        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        erec_Szamlak.VevoBesorolas = VevoBesorolas_KodTarakDropDownList.SelectedValue;
        erec_Szamlak.Updated.VevoBesorolas = true;

        return erec_Szamlak;

    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    public EREC_SzamlakSearch SetSearchObjectFromComponents(EREC_SzamlakSearch erec_SzamlakSearch)
    {
        if (erec_SzamlakSearch == null)
        {
            erec_SzamlakSearch = new EREC_SzamlakSearch();
        }

        if (!String.IsNullOrEmpty(ObjId_HiddenField.Value))
        {
            if (SzuloTipus == SzuloType.Kuldemeny)
            {
                erec_SzamlakSearch.KuldKuldemeny_Id.Value = ObjId_HiddenField.Value;
                erec_SzamlakSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;
            }
            else if (SzuloTipus == SzuloType.Dokumentum)
            {
                erec_SzamlakSearch.Dokumentum_Id.Value = ObjId_HiddenField.Value;
                erec_SzamlakSearch.Dokumentum_Id.Operator = Query.Operators.equals;
            }
        }

        if (!String.IsNullOrEmpty(PartnerTextBox_Szallito.Id_HiddenField))
        {
            erec_SzamlakSearch.Partner_Id_Szallito.Value = PartnerTextBox_Szallito.Id_HiddenField;
            erec_SzamlakSearch.Partner_Id_Szallito.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(CimekTextBox_Szallito.Id_HiddenField))
        {
            erec_SzamlakSearch.Cim_Id_Szallito.Value = CimekTextBox_Szallito.Id_HiddenField;
            erec_SzamlakSearch.Cim_Id_Szallito.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(BankszamlaszamTextBox1.Id_HiddenField))
        {
            erec_SzamlakSearch.Bankszamlaszam_Id.Value = BankszamlaszamTextBox1.Id_HiddenField;
            erec_SzamlakSearch.Bankszamlaszam_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(FizetesModja_KodTarakDropDownList.SelectedValue))
        {
            erec_SzamlakSearch.FizetesiMod.Value = FizetesModja_KodTarakDropDownList.SelectedValue;
            erec_SzamlakSearch.FizetesiMod.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(PIRAllapot_KodTarakDropDownList.SelectedValue))
        {
            erec_SzamlakSearch.PIRAllapot.Value = PIRAllapot_KodTarakDropDownList.SelectedValue;
            erec_SzamlakSearch.PIRAllapot.Operator = Query.Operators.equals;
        }

        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        if (!String.IsNullOrEmpty(VevoBesorolas_KodTarakDropDownList.SelectedValue))
        {
            erec_SzamlakSearch.VevoBesorolas.Value = VevoBesorolas_KodTarakDropDownList.SelectedValue;
            erec_SzamlakSearch.VevoBesorolas.Operator = Query.Operators.equals;
        }

        SzamlaBruttoVegosszege_SzamIntervallum_SearchFormControl.SetSearchObjectFields(erec_SzamlakSearch.EllenorzoOsszeg);

        if (!String.IsNullOrEmpty(Devizakod_KodtarakDropDownList.SelectedValue))
        {
            erec_SzamlakSearch.DevizaKod.Value = Devizakod_KodtarakDropDownList.SelectedValue;
            erec_SzamlakSearch.DevizaKod.Operator = Query.Operators.equals;
        }

        SzamlaFizetesiHatarideje_SearchCalendarControl.SetSearchObjectFields(erec_SzamlakSearch.FizetesiHatarido);
 
        SzamlaKelte_SearchCalendarControl.SetSearchObjectFields(erec_SzamlakSearch.BizonylatDatuma);

        if (!String.IsNullOrEmpty(SzamlaSorszam_TextBox.Text))
        {
            erec_SzamlakSearch.SzamlaSorszam.Value = SzamlaSorszam_TextBox.Text;
            erec_SzamlakSearch.SzamlaSorszam.Operator = Search.GetOperatorByLikeCharater(SzamlaSorszam_TextBox.Text);
        }

        SzamlaTeljesitesIdopontja_SearchCalendarControl.SetSearchObjectFields(erec_SzamlakSearch.TeljesitesDatuma);

        VisszakuldesDatuma_SearchCalendarControl.SetSearchObjectFields(erec_SzamlakSearch.VisszakuldesDatuma);

        if (!String.IsNullOrEmpty(Megjegyzes_TextBox.Text))
        {
            erec_SzamlakSearch.Manual_Note.Value = Megjegyzes_TextBox.Text;
            erec_SzamlakSearch.Manual_Note.Operator = Search.GetOperatorByLikeCharater(Megjegyzes_TextBox.Text);
        }

        return erec_SzamlakSearch;
    }

    /// <summary>
    /// A megőrzős checkboxok alapján adat Session-be mentése vagy kidobása onnan
    /// </summary>
    public void SetSessionData()
    {
        if (cbSzallitoMegorzes.Checked)
        {
            Session["Szamla_Szallito"] = PartnerTextBox_Szallito.Id_HiddenField;
        }
        else
        {
            Session.Remove("Szamla_Szallito");
        }

        if (cbSzallitoCimeMegorzes.Checked)
        {
            // BUG_4731
            //Session["Szamla_SzallitoCime"] = CimekTextBox_Szallito.Id_HiddenField;
            Session["Szamla_SzallitoCimId"] = CimekTextBox_Szallito.Id_HiddenField;
            Session["Szamla_SzallitoCimText"] = CimekTextBox_Szallito.Text;
            Session["Szamla_SzallitoCimChecked"] = "true";
        }
        else
        {
            // BUG_4731
            //Session.Remove("Szamla_SzallitoCime");
            Session.Remove("Szamla_SzallitoCimId");
            Session.Remove("Szamla_SzallitoCimText");
            Session.Remove("Szamla_SzallitoCimChecked");
        }

        if (cbFizetesModjaMegorzes.Checked)
        {
            Session["Szamla_FizetesModja"] = FizetesModja_KodTarakDropDownList.SelectedValue;
        }
        else
        {
            Session.Remove("Szamla_FizetesModja");
        }

        if (cbDevizaKodMegorzes.Checked)
        {
            Session["Szamla_DevizaKod"] = Devizakod_KodtarakDropDownList.SelectedValue;
        }
        else
        {
            Session.Remove("Szamla_DevizaKod");
        }
    }

    // ha a value értéke true, a kötelező kontrolok
    // Validate property-jét true-ra állítja, de csak azokat
    protected void SetComponentsValidateStatus(bool value)
    {
        PartnerTextBox_Szallito.Validate = value;
        CimekTextBox_Szallito.Validate = value;
        AdoszamTextBox1.Validate = value;
        BankszamlaszamTextBox1.Validate = false;
        FizetesModja_KodTarakDropDownList.Validate = value;
        SzamlaBruttoVegosszege_NumberTextBox.Validate = value;
        SzamlaBruttoVegosszege_NumberTextBox.ValidateFormat = value;
        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
		// CR kapcsán dátum-mezők kötelező kitöltésének beállítása
        SzamlaFizetesiHatarideje_CalendarControl.Validate = value;
        SzamlaKelte_CalendarControl.Validate = value;
        SzamlaSorszam_TextBox.Validate = value;
        SzamlaTeljesitesIdopontja_CalendarControl.Validate = value;
        VisszakuldesDatuma_CalendarControl.Validate = false;
        Megjegyzes_TextBox.Validate = false;

        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        VevoBesorolas_KodTarakDropDownList.Validate = value;
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return Search.GetReadableWhereOnPanel(MainPanel);
    }
      
    #endregion

}
