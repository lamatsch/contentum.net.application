﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Szemely4TAdatok.ascx.cs" Inherits="eRecordComponent_Szemely4TAdatok" %>

<%@ Register Src="~/Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc" %>

<table cellspacing="0" cellpadding="0">
    <tr class="urlapSor">
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnyNev" runat="server" Text="Viselt név:" />
        </td>
        <td class="mrUrlapMezo">
            <asp:TextBox ID="rnyNev" runat="server" CssClass="mrUrlapInputSearch" />
        </td>
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnySzuletesiNev" runat="server" Text="Születési név:" />
        </td>
        <td class="mrUrlapMezo">
            <asp:TextBox ID="rnySzuletesiNev" runat="server" CssClass="mrUrlapInputSearch" />
        </td>
    </tr>
    <tr class="urlapSor">
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnyAnyjaNeveVezNev" runat="server" Text="Anyja születési vezeték neve:" />
        </td>
        <td class="mrUrlapMezo">
            <asp:TextBox ID="rnyAnjaNeveVezNev" runat="server" CssClass="mrUrlapInputSearch" Text="" />
        </td>
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnyAnyjaNeveUtoNev" runat="server" Text="Anyja születési utó neve:" />
        </td>
        <td class="mrUrlapMezo">
            <asp:TextBox ID="rnyAnjaNeveUtoNev" runat="server" CssClass="mrUrlapInputSearch" Text="" />
        </td>
    </tr>
    <tr class="urlapSor">
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnySzuletesiHely" runat="server" Text="Születési hely:" />
        </td>
        <td class="mrUrlapMezo">
            <uc:telepulestextbox id="rnySzuletesiHely" runat="server" cssclass="mrUrlapInputSearch" validate="false" />
        </td>
        <td class="mrUrlapCaption">
            <asp:Label ID="labelRnySzuletesDatuma" runat="server" Text="Születési idő:" />
        </td>
        <td class="mrUrlapMezo">
            <uc:calendarcontrol id="rnySzuletesDatuma" runat="server" cssclass="mrUrlapInput" validate="false" />
        </td>
    </tr>
</table>
