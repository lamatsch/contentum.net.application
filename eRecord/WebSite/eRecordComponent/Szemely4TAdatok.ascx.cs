﻿using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_Szemely4TAdatok : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public Szemely4TAdatok Get4TAdatok()
    {
        Szemely4TAdatok adatok = new Szemely4TAdatok();
        adatok.AnyjaNeve.VezNev = rnyAnjaNeveVezNev.Text;
        adatok.AnyjaNeve.UtoNev = rnyAnjaNeveUtoNev.Text;
        adatok.Nev = rnyNev.Text;
        adatok.SzuletesiHely = rnySzuletesiHely.Text;
        adatok.SzuletesiIdo = rnySzuletesDatuma.SelectedDate;
        adatok.SzuletesiNev = rnySzuletesiNev.Text;
        return adatok;
    }
}