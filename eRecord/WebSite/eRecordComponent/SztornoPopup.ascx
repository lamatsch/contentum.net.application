﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SztornoPopup.ascx.cs" Inherits="eRecordComponent_SztornoPopup" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagPrefix="uc" TagName="CalendarControl" %>

<asp:UpdatePanel ID="update" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlSztorno" runat="server" CssClass="emp_Panel" Style="display: none">
            <script>  
                function ShowMegsemmisitesDatuma() {
                    if (document.getElementById('<%= papirAlapuRow2.ClientID %>').style.visibility == "hidden") {
                        document.getElementById('<%= papirAlapuRow2.ClientID %>').style.visibility = "visible";
                    }
                    else {
                        document.getElementById('<%= papirAlapuRow2.ClientID %>').style.visibility = "hidden";
                    }

                };
            </script>
            <div style="padding-bottom: 8px">
                <h2 runat="server" id="header" class="szmp_HeaderWrapper">
                    <asp:Label ID="labelHeader" runat="server" Text="Sztornózás" CssClass="emp_Header"></asp:Label>
                </h2>
                <div id="errorText" class="szmp_Body">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; padding-left: 5px;">
                                <asp:Label ID="LabelReq" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelSztornoOka" runat="server" Text="Sztornózás oka:"></asp:Label>
                            </td>
                            <td style="text-align: left; padding-left: 5px;">
                                <uc:RequiredTextBox ID="txtSztornoOka" LabelRequiredIndicatorID="LabelReq" runat="server" Width="200" Rows="2" TextBoxMode="MultiLine" />
                            </td>
                        </tr>
                        <tr id="papirAlapuRow1" runat="server" visible="false">
                            <td style="text-align: left; padding-left: 5px;">
                                <asp:Label ID="labelMegsemmisitve1" runat="server" Text="Iratpéldány"></asp:Label>
                                <br />
                                <asp:Label ID="labelMegsemmisitve2" runat="server" Text="megsemmisítve"></asp:Label>
                            </td>
                            <td style="text-align: left; padding-left: 5px; vertical-align: bottom;">
                                <asp:CheckBox ID="cbMegsemmisitve" runat="server" onchange="ShowMegsemmisitesDatuma();" />
                            </td>
                        </tr>
                        <tr id="papirAlapuRow2" runat="server" style="visibility: hidden;">
                            <td style="text-align: left; padding-left: 5px;">
                                <br />
                                <br />
                                <asp:Label ID="labelMegsemmisitesDatuma1" runat="server" Visible="true" Text="Megsemmisítés"></asp:Label>
                                <br />
                                <asp:Label ID="labelMegsemmisitesDatuma2" runat="server" Visible="true" Text="dátuma"></asp:Label>
                            </td>
                            <td style="text-align: left; padding-left: 5px; vertical-align: central;">
                                <uc:CalendarControl runat="server" ID="ccMegsemmisitesDatuma" Visible="true" TimeVisible="true" Validate="false" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 50%; text-align: center;">
                            <asp:ImageButton ID="btnOk" runat="server"
                                ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" />
                        </td>
                        <td style="width: 50%; text-align: center;">
                            <asp:ImageButton ID="btnCancel" runat="server"
                                ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>

        <asp:Button ID="targetControl" runat="server" Text="Button" Style="display: none;" />
        <ajaxToolkit:ModalPopupExtender ID="mdeSztorno" runat="server" TargetControlID="targetControl"
            PopupControlID="pnlSztorno" OkControlID="targetControl" BackgroundCssClass="emp_modalBackground"
            CancelControlID="btnCancel" DropShadow="true" PopupDragHandleControlID="header" />
    </ContentTemplate>

</asp:UpdatePanel>
