﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_SztornoPopup : System.Web.UI.UserControl
{
    public string SztornoIndoka
    {
        get
        {
            return txtSztornoOka.Text;
        }
    }

    //BLG_2948 - LZS - SztornoMegsemmisitesDatuma property felvétele, amely a felületen kiválasztott dátumot adja vissza, vagy üres stringet.
    public string SztornoMegsemmisitesDatuma
    {
        get
        {
            return (ccMegsemmisitesDatuma.Visible && cbMegsemmisitve.Checked) ? ccMegsemmisitesDatuma.Text : String.Empty;
        }
    }

    //BLG_2948 - LZS - SztornoMegsemmisitve property felvétele, amely a felületen bejelelölt értéket adja vissza, vagy null-t.
    public bool? SztornoMegsemmisitve
    {
        get
        {
            return cbMegsemmisitve.Visible ? cbMegsemmisitve.Checked : new bool?();
        }
    }

    public void SetOnclientClickShowFunction(ImageButton target, bool isPapiralapu)
    {
        //target elmentése, postback miatt
        this.Target = target;
        target.OnClientClick = GetOnClientClickShow();

        //BLG_2948 - LZS - A papíralapú iratpéldányokra vonatkozó sor megjelenítése az "isPapiralapu" paraméternek megfelelően történik.
        papirAlapuRow1.Visible = isPapiralapu;

        //BLG_2948 - LZS - Ha nem papíralapú, akkor le is tiltjuk a calendar controlt, hogy ne validáljon.
        ccMegsemmisitesDatuma.Enabled = isPapiralapu;
    }

    public void SetOnclientClickShowFunction(ImageButton target)
    {
        //target elmentése, postback miatt
        this.Target = target;
        target.OnClientClick = GetOnClientClickShow();
    }

    private string GetOnClientClickShow()
    {
        string js = "$find('" + mdeSztorno.ClientID + @"').show();
                     $get('" + txtSztornoOka.TextBox.ClientID + "').focus();return false;";
        return js;
    }

    private ImageButton target;

    private ImageButton Target
    {
        get { return target; }
        set { target = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //BLG_2948 - LZS - Validation group hozzáadása
        ccMegsemmisitesDatuma.ValidationGroup = cbMegsemmisitve.ValidationGroup =
        btnOk.ValidationGroup = txtSztornoOka.ValidationGroup = this.ClientID;
        RegisterJavaScripts();
        txtSztornoOka.Text = String.Empty;

        //BLG_2948 - LZS - alapértelmezésben nincs kijelölve a 'cbMegsemmisitve' checkbox.
        cbMegsemmisitve.Checked = false;

        //BLG_2948 - LZS - alapértelmezésben a pillanatnyi dátumot és időt mutatja.
        ccMegsemmisitesDatuma.Text = DateTime.Now.ToString();
    }

    private void RegisterJavaScripts()
    {
        if (Target != null)
        {
            btnOk.OnClientClick = "if (typeof(Page_ClientValidate) == 'function' && !Page_ClientValidate('" + btnOk.ValidationGroup + "')) {return false;}"
                                  + "else {$find('" + mdeSztorno.ClientID + "').hide();};"
                                  + Page.ClientScript.GetPostBackEventReference(
                                  new PostBackOptions(Target, "", "", true, false, false, true, true, btnOk.ValidationGroup))
                                  + ";return false;";
        }
        mdeSztorno.OnCancelScript = "$get('" + txtSztornoOka.TextBox.ClientID + "').value = '';";
    }

}
