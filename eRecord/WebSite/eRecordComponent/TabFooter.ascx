﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabFooter.ascx.cs" Inherits="eRecordComponent_TabFooter" %>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <asp:ImageButton ID="ImageSave" TabIndex="1" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                OnClick="ImageButton_Click" CommandName="Save" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:ImageButton ID="ImageClose" TabIndex="2"  runat="server" Visible="false" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                OnClientClick="window.close(); return true;" OnClick="ImageButton_Click" CommandName="Close" />
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndClose" TabIndex="3"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_bezar.png"
                onmouseover="swapByName(this.id,'rendben_es_bezar2.png')" onmouseout="swapByName(this.id,'rendben_es_bezar.png')"
                CommandName="SaveAndClose" OnClick="ImageButton_Click" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndNew" TabIndex="4"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                CommandName="SaveAndNew" HAlign="right" OnClick="ImageButton_Click" Visible="False"
                CausesValidation="true" />
        </td>
        <td>
            <asp:HyperLink ID="linkSaveAndNew" TabIndex="5"  runat="server" Visible = "false">
                <asp:Image ID="imgSaveAndNew" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                    onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                    />
            </asp:HyperLink>    
        </td>
        <td>
            <asp:ImageButton ID="ImageCsakErkeztet" TabIndex="6"  runat="server" ImageUrl="~/images/hu/ovalgomb/csak_erkeztet.gif"
                onmouseover="swapByName(this.id,'csak_erkeztet.gif')" onmouseout="swapByName(this.id,'csak_erkeztet.gif')"
                CommandName="CsakErkeztet" OnClick="ImageButton_Click" Visible="False"
                CausesValidation="true" />
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndAlszam" TabIndex="7"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam2.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam2.gif')"
                CommandName="SaveAndAlszam" OnClick="ImageButton_Click" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:HyperLink ID="linkSaveAndAlszam" TabIndex="8"  runat="server" Visible = "false">
                <asp:Image ID="imgSaveAndAlszam" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam2.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam2.gif')" />
            </asp:HyperLink>    
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndAlszamBejovo" TabIndex="9"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam_bejovo.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam_bejovo2.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam_bejovo.gif')"
                CommandName="SaveAndAlszamBejovo" OnClick="ImageButton_Click" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:HyperLink ID="linkSaveAndAlszamBejovo" TabIndex="10"  runat="server" Visible = "false">
                <asp:Image ID="imgSaveAndAlszamBejovo" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam_bejovo.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam_bejovo2.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam_bejovo.gif')" />
            </asp:HyperLink>    
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndAlszamBelso" TabIndex="11"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam_belso.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam_belso2.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam_belso.gif')"
                CommandName="SaveAndAlszamBelso" OnClick="ImageButton_Click" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:HyperLink ID="linkSaveAndAlszamBelso" TabIndex="12"  runat="server" Visible = "false">
                <asp:Image ID="imgSaveAndAlszamBelso" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_alszam_belso.gif"
                onmouseover="swapByName(this.id,'rendben_es_alszam_belso2.gif')" onmouseout="swapByName(this.id,'rendben_es_alszam_belso.gif')" />
            </asp:HyperLink>    
        </td>                
        <td>
            <asp:ImageButton ID="ImageCancel" TabIndex="13"  runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                CommandName="Cancel" OnClick="ImageButton_Click"
                Visible="False" CausesValidation="false"/></td>
    </tr>
</table>
<%--A végére beillesztve egy hidden input mező, amely majd tárolni fogja, hogy a confirmation ablakban milyen választ adtunk a kérdésre. Alap esetben „default” a value értéke, ha a confirmation ablakban OK-t nyomunk, akkor ennek az értéke „1” lesz.--%>
<input type="hidden" id="confirmResult" value="default" runat="server">

     

