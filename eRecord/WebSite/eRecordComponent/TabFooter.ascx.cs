using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System.Collections.Generic;

public partial class eRecordComponent_TabFooter : System.Web.UI.UserControl
{
    private string _Command = "";

    public bool SaveEnabled
    {
        get
        {
            return ImageSave.Enabled;
        }
        set
        {
            ImageSave.Enabled = value;
            ImageSaveAndNew.Enabled = value;
            ImageSaveAndClose.Enabled = value;
        }
    }

    public HtmlInputHidden ConfirmationResult
    {
        get { return confirmResult; }
    }

    public ImageButton ImageButton_Save
    {
        get { return ImageSave; }
    }
    public ImageButton ImageButton_SaveAndNew
    {
        get { return ImageSaveAndNew; }
    }
    public ImageButton ImageButton_SaveAndClose
    {
        get { return ImageSaveAndClose; }
    }
    public ImageButton ImageButton_Cancel
    {
        get { return ImageCancel; }
    }

    public ImageButton ImageButton_SaveAndAlszam
    {
        get { return ImageSaveAndAlszam; }
    }

    public ImageButton ImageButton_SaveAndAlszamBejovo
    {
        get { return ImageSaveAndAlszamBejovo; }
    }
    public ImageButton ImageButton_SaveAndAlszamBelso
    {
        get { return ImageSaveAndAlszamBelso; }
    }

    public ImageButton ImageButton_Close
    {
        get { return ImageClose; }
    }

    public HyperLink Link_SaveAndNew
    {
        get { return linkSaveAndNew; }
    }

    public HyperLink Link_SaveAndAlszam
    {
        get { return linkSaveAndAlszam; }
    }

    public ImageButton ImageButton_CsakErkeztet
    {
        get { return ImageCsakErkeztet; }
    }

    public HyperLink Link_SaveAndAlszamBejovo
    {
        get { return linkSaveAndAlszamBejovo; }
    }

    public HyperLink Link_SaveAndAlszamBelso
    {
        get { return linkSaveAndAlszamBelso; }
    }

    public string Command
    {
        get { return _Command; }
        set { _Command = value; }
    }

    public string CommandArgument
    {
        get
        {
            return ImageButton_Save.CommandArgument;
        }
        set
        {
            ImageButton_Save.CommandArgument = value;
            if (Command == "")
            {
                Command = CommandArgument;
            }

            setButtonsVisibleMode(Command);

            UI.SwapImageToDisabled(ImageSave);

        }
    }

    public String SaveValidationGroup
    {
        get
        {
            return ImageSave.ValidationGroup;
        }
        set { ImageSave.ValidationGroup = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Ne lehessen t�bbsz�r kattintani szkript
        List<ImageButton> bl = new List<ImageButton>(3);
        bl.Add(ImageButton_Save);
        bl.Add(ImageButton_SaveAndClose);
        bl.Add(ImageButton_SaveAndNew);
        bl.Add(ImageSaveAndAlszam);
        bl.Add(ImageSaveAndAlszamBejovo);
        bl.Add(ImageSaveAndAlszamBelso);
        bl.Add(ImageCsakErkeztet);
        ImageButton_Save.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_Save, bl);
        ImageButton_SaveAndClose.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_SaveAndClose, bl);
        ImageButton_SaveAndNew.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_SaveAndNew, bl);
        ImageCsakErkeztet.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_CsakErkeztet, bl);
    }

    public event CommandEventHandler ButtonsClick;
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, (sender as ImageButton).CommandArgument);
            ButtonsClick(this, args);
        }
    }

    public void DisableSaveButtons()
    {
        ImageButton_Save.Enabled = false;
        ImageButton_SaveAndClose.Enabled = false;
        ImageButton_SaveAndNew.Enabled = false;
        ImageButton_SaveAndAlszam.Enabled = false;
        ImageButton_SaveAndAlszamBejovo.Enabled = false;
        ImageButton_SaveAndAlszamBelso.Enabled = false;
        ImageButton_CsakErkeztet.Enabled = false;

        UI.SetImageButtonStyleToDisabled(ImageButton_Save);
        UI.SetImageButtonStyleToDisabled(ImageButton_SaveAndClose);
        UI.SetImageButtonStyleToDisabled(ImageButton_SaveAndNew);
        UI.SetImageButtonStyleToDisabled(ImageButton_SaveAndAlszam);
        UI.SetImageButtonStyleToDisabled(ImageButton_SaveAndAlszamBejovo);
        UI.SetImageButtonStyleToDisabled(ImageButton_SaveAndAlszamBelso);
        UI.SetImageButtonStyleToDisabled(ImageButton_CsakErkeztet);
    }

    private void setButtonsVisibleMode(string mode)
    {
        switch (mode)
        {
            case CommandName.New:
                ImageSave.Visible = true;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            case CommandName.View:
                ImageSave.Visible = false;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            case CommandName.Modify:
                ImageSave.Visible = true;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            case CommandName.AlairasEllenorzes:
                ImageSave.Visible = true;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            case CommandName.ElektronikusAlairas:
                ImageSave.Visible = true;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            case CommandName.VisszaUtasitas:
                ImageSave.Visible = true;
                ImageSaveAndNew.Visible = false;
                ImageSaveAndClose.Visible = false;
                ImageCancel.Visible = true;
                break;
            default:
                HideButton();
                break;
        }

    }

    public void HideButton()
    {
        ImageSave.Visible = false;
        ImageSaveAndNew.Visible = false;
        ImageSaveAndClose.Visible = false;
        ImageCancel.Visible = false;
        ImageButton_SaveAndAlszam.Visible = false;
        ImageButton_SaveAndAlszamBejovo.Visible = false;
        ImageButton_SaveAndAlszamBelso.Visible = false;
        ImageButton_CsakErkeztet.Visible = false;
    }


}
