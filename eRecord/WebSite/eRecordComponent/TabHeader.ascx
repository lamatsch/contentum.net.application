<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabHeader.ascx.cs" Inherits="eRecordComponent_TabHeader" %>
<table cellspacing="0" cellpadding="0" width="100%">
    <tr class="urlapSor">
        <td style="text-align: left; vertical-align: top;" colspan="2">

            <asp:ImageButton ID="ImageButtonNew" runat="server"
                ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                onmouseover="swapByName(this.id,'felvitel_trap2.jpg')"
                onmouseout="swapByName(this.id,'felvitel_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="New"
                Visible="True" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonView" runat="server"
                ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')"
                onmouseout="swapByName(this.id,'megtekintes_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="View"
                Visible="True" CausesValidation="false" />

            <a id="aLinkView" runat="server" target="_blank" visible="false">
                <%--<a id="aLinkView" runat="server" target="_self" visible="false" onclick="DispDocItem(this,'SharePoint.OpenDocuments'); return false;">							--%>
                <asp:Image ID="ViewImage" runat="server"
                    ImageUrl="~/images/hu/trapezgomb/megnyitas_trap.jpg"
                    onmouseover="swapByName(this.id,'megnyitas_trap2.jpg')"
                    onmouseout="swapByName(this.id,'megnyitas_trap.jpg')"
                    Visible="false" />
            </a>

            <asp:ImageButton ID="ImageButtonModify" runat="server"
                ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                onmouseover="swapByName(this.id,'modositas_trap2.jpg')"
                onmouseout="swapByName(this.id,'modositas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="Modify"
                Visible="True" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonInvalidate" runat="server"
                ImageUrl="~/images/hu/trapezgomb/torles.gif"
                onmouseover="swapByName(this.id,'torles.gif')"
                onmouseout="swapByName(this.id,'torles.gif')"
                OnClick="Buttons_OnClick" CommandName="Invalidate"
                Visible="True" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonVersion" runat="server"
                ImageUrl="~/images/hu/trapezgomb/verziok.jpg"
                onmouseover="swapByName(this.id,'verziok2.jpg')"
                onmouseout="swapByName(this.id,'verziok.jpg')"
                OnClick="Buttons_OnClick" CommandName="Version"
                Visible="True" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonElosztoLista" runat="server"
                ImageUrl="~/images/hu/trapezgomb/ElosztoLista.jpg"
                onmouseover="swapByName(this.id,'ElosztoLista2.jpg')"
                onmouseout="swapByName(this.id,'ElosztoLista.jpg')"
                OnClick="Buttons_OnClick" CommandName="ElosztoLista"
                Visible="True" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonElektronikusAlairas" runat="server"
                ImageUrl="~/images/hu/trapezgomb/alairas_trap.jpg"
                onmouseover="swapByName(this.id,'alairas_trap2.jpg')"
                onmouseout="swapByName(this.id,'alairas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="ElektronikusAlairas"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonElektronikusAlairasVisszavonas" runat="server"
                ImageUrl="~/images/hu/trapezgomb/alairas_visszavonas.png"
                onmouseover="swapByName(this.id,'alairas_visszavonas2.png')"
                onmouseout="swapByName(this.id,'alairas_visszavonas.png')"
                OnClick="Buttons_OnClick" CommandName="ElektronikusAlairasVisszavonas"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonElektronikusAlairasReset" runat="server"
                ImageUrl="~/images/hu/trapezgomb/alairas_alapallapot.png"
                onmouseover="swapByName(this.id,'alairas_alapallapot2.png')"
                onmouseout="swapByName(this.id,'alairas_alapallapot.png')"
                OnClick="Buttons_OnClick" CommandName="ElektronikusAlairasReset"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonElektronikusAlairasEllenorzes" runat="server"
                ImageUrl="~/images/hu/trapezgomb/alairas_ellenorzes_trap.jpg"
                onmouseover="swapByName(this.id,'alairas_ellenorzes_trap2.jpg')"
                onmouseout="swapByName(this.id,'alairas_ellenorzes_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="AlairasEllenorzes"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonAlairasAdatok" runat="server"
                ImageUrl="~/images/hu/trapezgomb/alairas_adatok_trap.jpg"
                onmouseover="swapByName(this.id,'alairas_adatok_trap2.jpg')"
                onmouseout="swapByName(this.id,'alairas_adatok_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="AlairasAdatok"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonLock" runat="server"
                ImageUrl="~/images/hu/trapezgomb/zarolas_trap.jpg"
                onmouseover="swapByName(this.id,'zarolas_trap2.jpg')"
                onmouseout="swapByName(this.id,'zarolas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="Lock"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonUnLock" runat="server"
                ImageUrl="~/images/hu/trapezgomb/zarolas_feloldas_trap.jpg"
                onmouseover="swapByName(this.id,'zarolas_feloldas_trap2.jpg')"
                onmouseout="swapByName(this.id,'zarolas_feloldas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="Unlock"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonHitelessegiZaradek" runat="server"
                ImageUrl="~/images/hu/trapezgomb/hitelessegi_zaradek_trap.jpg"
                onmouseover="swapByName(this.id,'hitelessegi_zaradek_trap2.jpg')"
                onmouseout="swapByName(this.id,'hitelessegi_zaradek_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="HitelessegiZaradek"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonVisszaUtasitas" runat="server"
                ImageUrl="~/images/hu/trapezgomb/visszautasitas_trap.jpg"
                onmouseover="swapByName(this.id,'visszautasitas_trap2.jpg')"
                onmouseout="swapByName(this.id,'visszautasitas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="VisszaUtasitas"
                Visible="false" CausesValidation="false" />

            <asp:ImageButton ID="ImageButtonOCR" runat="server"
                ImageUrl="~/images/hu/trapezgomb/OCR_trap.jpg"
                onmouseover="swapByName(this.id,'OCR_trap2.jpg')"
                onmouseout="swapByName(this.id,'OCR_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="OCR"
                Visible="false" CausesValidation="false" />

             <asp:ImageButton ID="ImageButtonDecrypt" runat="server"
                ImageUrl="~/images/hu/trapezgomb/kititkositas_trap.jpg"
                onmouseover="swapByName(this.id,'kititkositas_trap2.jpg')"
                onmouseout="swapByName(this.id,'kititkositas_trap.jpg')"
                OnClick="Buttons_OnClick" CommandName="Kititkosit"
                Visible="false" CausesValidation="false" />

        </td>
    </tr>
</table>
