using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eUtility;

using Contentum.eUIControls;

public partial class eRecordComponent_TabHeader : System.Web.UI.UserControl
{
    #region Public Properies

    private string _Command = "";

    public string Command
    {
        get { return _Command; }
        set
        {
            if (_Command != value)
            {
                _Command = value;
            }
        }
    }
    

    private bool _EnabledButton = true;
    public bool EnabledButton
    {
        get 
        {
            return _EnabledButton; 
        }
        set 
        {
            _EnabledButton = value;
            ImageButtonNew.Enabled = _EnabledButton;
            ImageButtonModify.Enabled = _EnabledButton;
            ImageButtonView.Enabled = _EnabledButton;
            ImageButtonInvalidate.Enabled = _EnabledButton;
            ImageButtonVersion.Enabled = _EnabledButton;
            ImageButtonElosztoLista.Enabled = _EnabledButton;
            ImageButtonElektronikusAlairas.Enabled = _EnabledButton;
            ImageButtonElektronikusAlairasReset.Enabled= _EnabledButton;
            ImageButtonElektronikusAlairasVisszavonas.Enabled = _EnabledButton;
            ImageButtonAlairasAdatok.Enabled = _EnabledButton;
            ImageButtonLock.Enabled = _EnabledButton;
            ImageButtonUnLock.Enabled = _EnabledButton;
            ImageButtonHitelessegiZaradek.Enabled = _EnabledButton;
            ImageButtonVisszaUtasitas.Enabled = _EnabledButton;
            ImageButtonElektronikusAlairasEllenorzes.Enabled = _EnabledButton;
        }
    }

    private bool _VisibleButton = true;
    public bool VisibleButton
    {
        get
        {
            return _VisibleButton;
        }
        set
        {
            _VisibleButton = value;
            ImageButtonNew.Visible = _VisibleButton;
            ImageButtonModify.Visible = _VisibleButton;
            ImageButtonView.Visible = _VisibleButton;
            ImageButtonInvalidate.Visible = _VisibleButton;
            ImageButtonVersion.Visible = _VisibleButton;
            ImageButtonElosztoLista.Visible = _VisibleButton;
            ImageButtonElektronikusAlairas.Visible = _VisibleButton;
            ImageButtonElektronikusAlairasVisszavonas.Visible = _VisibleButton;
            ImageButtonElektronikusAlairasReset.Visible = _VisibleButton;
            ImageButtonAlairasAdatok.Visible = _VisibleButton;
            ImageButtonLock.Visible = _VisibleButton;
            ImageButtonUnLock.Visible = _VisibleButton;
            ImageButtonHitelessegiZaradek.Visible = _VisibleButton;
            ImageButtonVisszaUtasitas.Visible = _VisibleButton;
            ImageButtonElektronikusAlairasEllenorzes.Visible = _VisibleButton;
        }
    }

    #region BLG 2947
    private bool _checkVisible = false;
    public bool CheckVisible
    {
        get
        {
            return _checkVisible;
        }
        set
        {
            _checkVisible = value;
            ImageButtonElektronikusAlairasEllenorzes.Visible = _checkVisible;
        }
    }
    #endregion


    public string LinkViewHRef
    { 
        get
        {
            return aLinkView.HRef;
        }
        set
        {
            aLinkView.HRef = value;
        }
    }

    public string LinkViewOnClientClick
    {
        get
        {
            return Attributes["onclick"].ToString();
        }
        set
        {
            Attributes["onclick"] = value;
        }
    }

    public bool LinkViewEnabled
    {
        get
        {            
            return ViewImage.Enabled;
        }
        set
        {
            aLinkView.Disabled = !value;
            aLinkView.Visible = !value;
            ViewImage.Enabled = value;
        }
    }

    public bool LinkViewVisible
    {
        get
        {
            return ViewImage.Visible;
        }
        set
        {
            aLinkView.Visible = value;
            ViewImage.Visible = value;
        }
    }

    //public Image LinkView
    public Image LinkView
    {
        get
        {
            return ViewImage;
        }
        set
        {
            ViewImage = value;
        }
    }


    public CustomFunctionImageButton ImageNew
    {
        get
        {
            return ImageButtonNew;
        }
        set
        {
            ImageButtonNew = value;
        }
    }
    public CustomFunctionImageButton ImageModify
    {
        get
        {
            return ImageButtonModify;
        }
        set
        {
            ImageButtonModify = value;
        }
    }
    public CustomFunctionImageButton ImageView
    {
        get
        {
            return ImageButtonView;
        }
        set
        {
            ImageButtonView = value;
        }
    }
    public CustomFunctionImageButton ImageInvalidate
    {
        get
        {
            return ImageButtonInvalidate;
        }
    }
    public CustomFunctionImageButton ImageElektronikusAlairas
    {
        get
        {
            return ImageButtonElektronikusAlairas;
        }
        set
        {
            ImageButtonElektronikusAlairas = value;
        }
    }

    public CustomFunctionImageButton ImageElektronikusAlairasReset
    {
        get
        {
            return ImageButtonElektronikusAlairasReset;
        }
        set
        {
            ImageButtonElektronikusAlairasReset = value;
        }
    }

    public CustomFunctionImageButton ImageElektronikusAlairasVisszavonas
    {
        get
        {
            return ImageElektronikusAlairasVisszavonas;
        }
        set
        {
            ImageElektronikusAlairasVisszavonas = value;
        }
    }



    public CustomFunctionImageButton ImageElektronikusAlairasEllenorzes
    {
        get 
        {
            return ImageButtonElektronikusAlairasEllenorzes;
        }
        set
        {
            ImageButtonElektronikusAlairasEllenorzes = value;
        }
    }

    public CustomFunctionImageButton ImageAlairasAdatok
    {
        get
        {
            return ImageButtonAlairasAdatok;
        }
        set
        {
            ImageButtonAlairasAdatok = value;
        }
    }
    public CustomFunctionImageButton ImageLock
    {
        get
        {
            return ImageButtonLock;
        }
        set
        {
            ImageButtonLock = value;
        }
    }
    public CustomFunctionImageButton ImageUnLock
    {
        get
        {
            return ImageButtonUnLock;
        }
        set
        {
            ImageButtonUnLock = value;
        }
    }

    public CustomFunctionImageButton ImageHitelessegiZaradek
    {
        get
        {
            return ImageButtonHitelessegiZaradek;
        }
        set
        {
            ImageButtonHitelessegiZaradek = value;
        }
    }

    public CustomFunctionImageButton ImageOCR
    {
        get
        {
            return ImageButtonOCR;
        }
    }

    public CustomFunctionImageButton ImageDecrypt
    {
        get
        {
            return ImageButtonDecrypt;
        }
        set
        {
            ImageButtonDecrypt = value;
        }
    }

    public Boolean NewVisible
    {
        get
        {
            return ImageButtonNew.Visible;
        }
        set
        {
            ImageButtonNew.Visible = value;
        }
    }
    public Boolean ViewVisible
    {
        get
        {
            return ImageButtonView.Visible;
        }
        set
        {
            ImageButtonView.Visible = value;
        }
    }
    public Boolean ModifyVisible
    {
        get
        {
            return ImageButtonModify.Visible;
        }
        set
        {
            ImageButtonModify.Visible = value;
        }
    }
    public Boolean InvalidateVisible
    {
        get
        {
            return ImageButtonInvalidate.Visible;
        }
        set
        {
            ImageButtonInvalidate.Visible = value;
        }
    }
    public Boolean VersionVisible
    {
        get
        {
            return ImageButtonVersion.Visible;
        }
        set
        {
            ImageButtonVersion.Visible = value;
        }
    }
    public Boolean ElosztoListaVisible
    {
        get
        {
            return ImageButtonElosztoLista.Visible;
        }
        set
        {
            ImageButtonElosztoLista.Visible = value;
        }
    }
    public Boolean ElektronikusAlairasVisible
    {
        get
        {
            return ImageButtonElektronikusAlairas.Visible;
        }
        set
        {
            ImageButtonElektronikusAlairas.Visible = value;
        }
    }
    public Boolean ElektronikusAlairasEllenorzesVisible
    {
        get
        {
            return ImageButtonElektronikusAlairasEllenorzes.Visible;
        }
        set
        {
            ImageButtonElektronikusAlairasEllenorzes.Visible = value;
        }
    }
    public Boolean ElektronikusAlairasVisszavonasVisible
    {
        get
        {
            return ImageButtonElektronikusAlairasVisszavonas.Visible;
        }
        set
        {
            ImageButtonElektronikusAlairasVisszavonas.Visible = value;
        }
    }
    public Boolean ElektronikusAlairasResetVisible
    {
        get
        {
            return ImageButtonElektronikusAlairasReset.Visible;
        }
        set
        {
            ImageButtonElektronikusAlairasReset.Visible = value;
        }
    }
    public bool AlairasAdatokVisible
    {
        get
        {
            return ImageButtonAlairasAdatok.Visible;
        }
        set
        {
            ImageButtonAlairasAdatok.Visible = value;
        }
    }
    public bool VisszaUtasitasVisible
    {
        get { return ImageButtonVisszaUtasitas.Visible; }
        set { ImageButtonVisszaUtasitas.Visible = value; }
    }
    public bool LockVisible
    {
        get { return ImageButtonLock.Visible; }
        set { ImageButtonLock.Visible = value; }
    }
    public bool UnLockVisible
    {
        get { return ImageButtonUnLock.Visible; }
        set { ImageButtonUnLock.Visible = value; }
    }

    public bool HitelessegiZaradekVisible
    {
        get { return ImageButtonHitelessegiZaradek.Visible; }
        set { ImageButtonHitelessegiZaradek.Visible = value; }
    }
    public Boolean OCRVisible
    {
        get
        {
            return ImageButtonOCR.Visible;
        }
        set
        {
            ImageButtonOCR.Visible = value;
        }
    }
    public Boolean NewEnabled
    {
        get
        {
            return ImageButtonNew.Enabled;
        }
        set
        {
            ImageButtonNew.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonNew.CssClass = "disableditem";
            //}
        }
    }

    public Boolean DecryptEnabled
    {
        get
        {
            return ImageButtonDecrypt.Enabled;
        }
        set
        {
            ImageButtonDecrypt.Enabled = value;            
        }
    }

    public Boolean DecryptVisible
    {
        get
        {
            return ImageButtonDecrypt.Visible;
        }
        set
        {
            ImageButtonDecrypt.Visible = value;
        }
    }

    public Boolean ViewEnabled
    {
        get
        {
            return ImageButtonView.Enabled;
        }
        set
        {
            ImageButtonView.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonView.CssClass = "disableditem";
            //}
        }
    }
    public Boolean ModifyEnabled
    {
        get
        {
            return ImageButtonModify.Enabled;
        }
        set
        {
            ImageButtonModify.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonModify.CssClass = "disableditem";
            //}
        }
    }
    public Boolean InvalidateEnabled
    {
        get
        {
            return ImageButtonInvalidate.Enabled;
        }
        set
        {
            ImageButtonInvalidate.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonInvalidate.CssClass = "disableditem";
            //}
        }
    }
    public Boolean VersionEnabled
    {
        get
        {
            return ImageButtonVersion.Enabled;
        }
        set
        {
            ImageButtonVersion.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonVersion.CssClass = "disableditem";
            //}
        }
    }
    public Boolean ElosztoListaEnabled
    {
        get
        {
            return ImageButtonElosztoLista.Enabled;
        }
        set
        {
            ImageButtonElosztoLista.Enabled = value;
            //if (value == false)
            //{
            //    ImageButtonElosztoLista.CssClass = "disableditem";
            //}
        }
    }
    public Boolean ElektronikusAlairasEnabled
    {
        get
        {
            return ImageButtonElektronikusAlairas.Enabled;
        }
        set
        {
            ImageButtonElektronikusAlairas.Enabled = value;
            //ImageButtonElektronikusAlairas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public Boolean ElektronikusAlairasEllenorzesEnabled
    {
        get
        {
            return ImageButtonElektronikusAlairasEllenorzes.Enabled;
        }
        set
        {
            ImageButtonElektronikusAlairasEllenorzes.Enabled = value;
        }
    }
    public Boolean ElektronikusAlairasVisszavonasEnabled
    {
        get
        {
            return ImageButtonElektronikusAlairasVisszavonas.Enabled;
        }
        set
        {
            ImageButtonElektronikusAlairasVisszavonas.Enabled = value;
        }
    }
    public Boolean ElektronikusAlairasResetEnabled
    {
        get
        {
            return ImageButtonElektronikusAlairasReset.Enabled;
        }
        set
        {
            ImageButtonElektronikusAlairasReset.Enabled = value;
        }
    }
    public bool AlairasAdatokEnabled
    {
        get
        {
            return ImageButtonAlairasAdatok.Enabled;
        }
        set
        {
            ImageButtonAlairasAdatok.Enabled = value;
            //ImageButtonAlairasAdatok.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool VisszaUtasitasEnabled
    {
        get { return ImageButtonVisszaUtasitas.Enabled; }
        set
        {
            ImageButtonVisszaUtasitas.Enabled = value;
            //ImageButtonVisszaUtasitas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool LockEnabled
    {
        get { return ImageButtonLock.Enabled; }
        set 
        { 
            ImageButtonLock.Enabled = value;
            //ImageButtonLock.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool UnLockEnabled
    {
        get { return ImageButtonUnLock.Enabled; }
        set 
        { 
            ImageButtonUnLock.Enabled = value;
            //ImageButtonUnLock.CssClass = (value ? "highlightit" : "disableditem");
        }
    }

    public bool HitelessegiZaradekEnabled
    {
        get { return ImageButtonHitelessegiZaradek.Enabled; }
        set { ImageButtonHitelessegiZaradek.Enabled = value; }
    }
    public Boolean OCREnabled
    {
        get
        {
            return ImageButtonOCR.Enabled;
        }
        set
        {
            ImageButtonOCR.Enabled = value;
        }
    }
    public String NewOnClientClick
    {
        get
        {
            return ImageButtonNew.OnClientClick;
        }
        set
        {
            ImageButtonNew.OnClientClick = value;
        }
    }
    public String ViewOnClientClick
    {
        get
        {
            return ImageButtonView.OnClientClick;
        }
        set
        {
            ImageButtonView.OnClientClick = value;
        }
    }
    public String ModifyOnClientClick
    {
        get
        {
            return ImageButtonModify.OnClientClick;
        }
        set
        {
            ImageButtonModify.OnClientClick = value;
        }
    }
    public String InvalidateOnClientClick
    {
        get
        {
            return ImageButtonInvalidate.OnClientClick;
        }
        set
        {
            ImageButtonInvalidate.OnClientClick = value;
        }
    }
    public String VersionOnClientClick
    {
        get
        {
            return ImageButtonVersion.OnClientClick;
        }
        set
        {
            ImageButtonVersion.OnClientClick = value;
        }
    }
    public String ElosztoListaOnClientClick
    {
        get
        {
            return ImageButtonElosztoLista.OnClientClick;
        }
        set
        {
            ImageButtonElosztoLista.OnClientClick = value;
        }
    }
    public String ElektronikusAlairasOnClientClick
    {
        get
        {
            return ImageButtonElektronikusAlairas.OnClientClick;
        }
        set
        {
            ImageButtonElektronikusAlairas.OnClientClick = value;
        }
    }
    public String ElektronikusAlairasVisszavonasOnClientClick
    {
        get
        {
            return ImageButtonElektronikusAlairasVisszavonas.OnClientClick;
        }
        set
        {
            ImageButtonElektronikusAlairasVisszavonas.OnClientClick = value;
        }
    }
    public String ElektronikusAlairasResetOnClientClick
    {
        get
        {
            return ImageButtonElektronikusAlairasReset.OnClientClick;
        }
        set
        {
            ImageButtonElektronikusAlairasReset.OnClientClick = value;
        }
    }
    public String ElektronikusAlairasEllenorzesOnClientClick
    {
        get
        {
            return ImageButtonElektronikusAlairasEllenorzes.OnClientClick;
        }
        set
        {
            ImageButtonElektronikusAlairasEllenorzes.OnClientClick = value;
        }
    }
    public String AlairasAdatokOnClientClick
    {
        get
        {
            return ImageButtonAlairasAdatok.OnClientClick;
        }
        set
        {
            ImageButtonAlairasAdatok.OnClientClick = value;
        }
    }
    public string VisszaUtasitasOnClientClick
    {
        get { return ImageButtonVisszaUtasitas.OnClientClick; }
        set { ImageButtonVisszaUtasitas.OnClientClick = value; }
    }
    public string LockOnClientClick
    {
        get { return ImageButtonLock.OnClientClick; }
        set { ImageButtonLock.OnClientClick = value; }
    }
    public string UnLockOnClientClick
    {
        get { return ImageButtonUnLock.OnClientClick; }
        set { ImageButtonUnLock.OnClientClick = value; }
    }

    public string HitelessegiZaradekOnClientClick
    {
        get { return ImageButtonHitelessegiZaradek.OnClientClick; }
        set { ImageButtonHitelessegiZaradek.OnClientClick = value; }
    }
    public string OCROnClientClick
    {
        get
        {
            return ImageButtonOCR.OnClientClick;
        }
        set
        {
            ImageButtonOCR.OnClientClick = value;
        }
    }

    public string DecryptOnClientClick
    {
        get
        {
            return ImageButtonDecrypt.OnClientClick;
        }
        set
        {
            ImageButtonDecrypt.OnClientClick = value;
        }
    }

    #endregion

    #region IsDisabledButtonToSetHidden by RPM
    private bool IsDisabledButtonToSetHidden()
    {
        return Contentum.eUtility.Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS);
    }
    #endregion IsDisabledButtonToSetHidden by RPM

    protected void Page_Init(object sender, EventArgs e)
    {
        _Command = Request.QueryString.Get(CommandName.Command);
        setButtonsVisibleMode(_Command);

        bool isDisabledIconToSetHidden = IsDisabledButtonToSetHidden();
        ImageButtonNew.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonModify.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonView.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonInvalidate.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonVersion.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonElosztoLista.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonElektronikusAlairas.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonElektronikusAlairasVisszavonas.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonAlairasAdatok.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonHitelessegiZaradek.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonVisszaUtasitas.HideIfDisabled = isDisabledIconToSetHidden;
        ImageButtonOCR.HideIfDisabled = isDisabledIconToSetHidden;
    }

    protected void Page_PreLoad(object sender, EventArgs e)
    {
    }

    public event CommandEventHandler ButtonsClick;

    public virtual void Buttons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    public void setButtonsVisibleMode(string mode)
    {
        switch (mode)
        {
            case CommandName.New:
                ImageButtonNew.Visible = true;
                ImageButtonModify.Visible = true;
                ImageButtonView.Visible = true;
                ImageButtonInvalidate.Visible = true;
                ImageButtonVersion.Visible = true;
                ImageButtonElosztoLista.Visible = true;
                ImageButtonElektronikusAlairas.Visible = false;
                ImageButtonAlairasAdatok.Visible = false;
                ImageButtonHitelessegiZaradek.Visible = false;
                ImageButtonVisszaUtasitas.Visible = false;
                ImageButtonElektronikusAlairasEllenorzes.Visible = false;
                ImageButtonElektronikusAlairasVisszavonas.Visible = false;
                ImageButtonElektronikusAlairasReset.Visible = false;
                ImageButtonOCR.Visible = false;
                ImageButtonDecrypt.Visible = false;
                break;
            case CommandName.View:
                ImageButtonNew.Visible = false;
                ImageButtonModify.Visible = false;
                ImageButtonView.Visible = true;
                ImageButtonInvalidate.Visible = false;
                ImageButtonVersion.Visible = true;
                ImageButtonElosztoLista.Visible = true;
                ImageButtonElektronikusAlairas.Visible = false;
                ImageButtonAlairasAdatok.Visible = false;
                ImageButtonHitelessegiZaradek.Visible = false;
                ImageButtonVisszaUtasitas.Visible = false;
                ImageButtonElektronikusAlairasEllenorzes.Visible = false;
                ImageButtonElektronikusAlairasVisszavonas.Visible = false;
                ImageButtonElektronikusAlairasReset.Visible = false;
                ImageButtonOCR.Visible = false;
                ImageButtonDecrypt.Visible = false;
                break;
            case CommandName.Modify:
                ImageButtonNew.Visible = true;
                ImageButtonModify.Visible = true;
                ImageButtonView.Visible = true;
                ImageButtonInvalidate.Visible = true;
                ImageButtonVersion.Visible = true;
                ImageButtonElosztoLista.Visible = true;
                ImageButtonElektronikusAlairas.Visible = false;
                ImageButtonAlairasAdatok.Visible = false;
                ImageButtonHitelessegiZaradek.Visible = false;
                ImageButtonVisszaUtasitas.Visible = false;
                ImageButtonElektronikusAlairasEllenorzes.Visible = false;
                ImageButtonElektronikusAlairasVisszavonas.Visible = false;
                ImageButtonElektronikusAlairasReset.Visible = false;
                ImageButtonOCR.Visible = false;
                ImageButtonDecrypt.Visible = false;
                break;
            default:
                HideButton();
                break;
        }

    }

    private void HideButton()
    {
        ImageButtonNew.Visible = false;
        ImageButtonModify.Visible = false;
        ImageButtonView.Visible = true;
        ImageButtonInvalidate.Visible = false;
        ImageButtonVersion.Visible = true;
        ImageButtonElosztoLista.Visible = true;
        ImageButtonElektronikusAlairas.Visible = false;
        ImageButtonElektronikusAlairasVisszavonas.Visible = false;
        ImageButtonElektronikusAlairasReset.Visible = false;
        ImageButtonAlairasAdatok.Visible = false;
        ImageButtonLock.Visible = false;
        ImageButtonUnLock.Visible = false;
        ImageButtonVisszaUtasitas.Visible = false;
        ImageButtonElektronikusAlairasEllenorzes.Visible = false;
        ImageButtonOCR.Visible = false;
    }

}
