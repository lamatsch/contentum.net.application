<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TargySzavakTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratTargySzavakTab" %>

<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>

<%@ Register Src="../eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox" TagPrefix="tsztb" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="../Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="../Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<style type="text/css">
    select.GridViewTextBox, select.GridViewTextBoxChanged, select.GridViewTextBoxChangedIllegalValue {
        max-width: 320px !important;
    }
</style>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="TargyszavakUpdatePanel" runat="server" OnLoad="TargyszavakUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <ajaxToolkit:CollapsiblePanelExtender ID="TargyszavakCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="300" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>                                                  
    
    <ajaxToolkit:CollapsiblePanelExtender ID="OrokoltTargyszavakCPE" runat="server" TargetControlID="Panel2"
        CollapsedSize="0" Collapsed="true" 
        AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="300" ScrollContents="true"
        ExpandedText="�r�k�lt t�rgyszavakat elrejt" CollapsedText="�r�k�lt t�rgyszavakat mutat"
        ExpandControlID="OrokoltTargyszavakCPEButton" CollapseControlID = "OrokoltTargyszavakCPEButton"
        ExpandedImage="~/images/hu/Grid/minus.gif" CollapsedImage="~/images/hu/Grid/plus.gif"
        ImageControlID="OrokoltTargyszavakCPEButton"
        >
    </ajaxToolkit:CollapsiblePanelExtender>
                    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
<asp:HiddenField ID="ObjTip_Id_HiddenField" runat="server" />
<asp:HiddenField ID="MaxSorszam_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		    <td>
                <asp:Panel ID="OrokoltTargyszavakPanel" runat="server">
   
                    <table cellpadding="0" cellspacing="0" width="100%">
	                    <tr class="tabExtendableListFejlec">
		                    <td style="text-align: left; vertical-align: middle; width: 0px;">
                                <asp:ImageButton runat="server" ID="OrokoltTargyszavakCPEButton" ImageUrl="~/images/hu/Grid/plus.gif" OnClientClick="return false;" />
                            </td>
                            <td style="text-align: center; vertical-align: middle; width: 100%; height: 20px;">
                                <asp:Label ID="labelHeaderOrokoltTargyszavak" Text="�r�k�lt t�rgyszavak (�gyirat, �gyiratdarab)" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%-- Orokolt targyszavak --%>
                                <asp:Panel ID="Panel2" runat="server" BackColor="whitesmoke">
                                    
				                        <asp:GridView ID="OrokoltTargyszavakGridView"
				                                      runat="server"
                                                      CellPadding="0"
				                                      CellSpacing="0"
				                                      BorderWidth="1"
				                                      GridLines="Both"
				                                      AllowPaging="True"
				                                      PagerSettings-Visible="false"
				                                      AllowSorting="True"
				                                      DataKeyNames="Id"
				                                      AutoGenerateColumns="False" Width="98%"
				                                      OnRowCommand="TargyszavakGridView_RowCommand" OnRowDataBound="TargyszavakGridView_RowDataBound" OnSorting="TargyszavakGridView_Sorting"				                      
				                                      >
						                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						                    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						                    <Columns>
							                    <asp:TemplateField>
								                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								                    <HeaderTemplate />
								                    <ItemTemplate>
									                    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
								                    </ItemTemplate>
							                    </asp:TemplateField>			    
							                    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							                    </asp:CommandField>
			                                    <asp:BoundField DataField="Targyszo" HeaderText="T�rgysz�" SortExpression="Targyszo">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>
			                                    <asp:BoundField DataField="Funkcio" HeaderText="Funkci�" SortExpression="Funkcio">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>
			                                    <asp:BoundField DataField="ErtekByControlSourceType" HeaderText="�rt�k" SortExpression="ErtekByControlSourceType">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>
			                                    <asp:BoundField DataField="Sorszam" HeaderText="Sorsz�m" SortExpression="Sorszam">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>			                                                  
			                                    <asp:BoundField DataField="ForrasNev" HeaderText="Forr�s" SortExpression="ForrasNev">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>                                
			                                    <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="Note">
				                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                                    </asp:BoundField>                                                                 						                    				                                                
				                               </Columns>				                      
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>	        				        
                                    </asp:Panel>
                                </td>
                           </tr>
                       </table>
               </asp:Panel>
            </td>
        </tr>
	    <tr>
		    <td>
                <%-- TabHeader --%>
                <uc1:TabHeader ID="TabHeader1" runat="server" />
			    <%-- /TabHeader --%>

                <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
                    
				        <asp:GridView ID="TargyszavakGridView"
				                      runat="server"
                                      CellPadding="0"
				                      CellSpacing="0"
				                      BorderWidth="1"
				                      GridLines="Both"
				                      AllowPaging="True"
				                      PagerSettings-Visible="false"
				                      AllowSorting="True"
				                      DataKeyNames="Id"
				                      AutoGenerateColumns="False" Width="98%"
				                      OnRowCommand="TargyszavakGridView_RowCommand" OnRowDataBound="TargyszavakGridView_RowDataBound" OnSorting="TargyszavakGridView_Sorting"				                      
				                      >
						    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						    <Columns>
							    <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
									    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
									    &nbsp;&nbsp;
									    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
								    </HeaderTemplate>
								    <ItemTemplate>
									    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" /> <%--'<%# Eval("Id") %>'--%>
									    <%--
									    <asp:Label ID="labelGridDefinicio" runat="server" Text="(defin�ci�)" Visible="false" />
									    --%>
									    <asp:Image ID="imageMetaHiany" runat="server" Visible="false"
                                                ImageUrl="~/images/hu/egyeb/felkialtojel.gif"
                                                ToolTip="K�telez�" />
									    <asp:Image ID="imageOpcionalisMetaHiany" runat="server" Visible="false"
                                                ImageUrl="~/images/hu/egyeb/felkialtojel_zold.gif"
                                                ToolTip="Opcion�lis" />
                                                
								    </ItemTemplate>
							    </asp:TemplateField>
                                							    
							    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							    </asp:CommandField>
							    
			                    <asp:BoundField DataField="Targyszo" HeaderText="T�rgysz�" SortExpression="Targyszo">
				                    <HeaderStyle CssClass="GridViewBorderHeader" />
				                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                    </asp:BoundField>
			                    <asp:BoundField DataField="Funkcio" HeaderText="Funkci�" SortExpression="Funkcio">
				                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                    </asp:BoundField>
			                    <%--				                    
                                <asp:TemplateField>
                                    <HeaderStyle  CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="headerGridMeta" runat="server" CommandName="Sort" CommandArgument="Tipus">Meta</asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <asp:Image ID="imageMeta" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/pipa.gif" />
                                       <asp:Image ID="imageMetaHiany" runat="server" Visible="false"
                                                ImageUrl="~/images/hu/egyeb/felkialtojel.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                --%>  
                                <asp:TemplateField Visible="true">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:LinkButton ID="headerGridErtek" runat="server" CommandName="Sort" CommandArgument="Ertek">�rt�k</asp:LinkButton>
                                     </HeaderTemplate>
                                     <ItemTemplate>
<%--                                         <asp:TextBox id="TextBoxGridErtek" runat="server" Text='<%# Eval("Ertek") %>' 
                                            CssClass="GridViewTextBox" Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                                            ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>'
                                         />
                                         <br />--%>
                                         <%--BLG_608--%>
                                        <%-- <eUI:DynamicValueControl ID="DynamicValueControl_TargyszoErtek" CssClass="GridViewTextBox"
		                                    DefaultControlTypeSource='<%# DefaultControlTypeSource_TextBoxErtek %>'
		                                    ControlTypeSource='<%# Convert.IsDBNull(Eval("ControlTypeSource")) ? DefaultControlTypeSource_TextBoxErtek : Eval("ControlTypeSource") %>'
		                                    runat="server"
		                                    Value='<%# Eval("Ertek") %>'
		                                    Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                                            ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>'
                                            Validate="false"
                                             />--%>
                                          <eUI:DynamicValueControl ID="DynamicValueControl_TargyszoErtek" CssClass="GridViewTextBox" 
		                                    DefaultControlTypeSource='<%# DefaultControlTypeSource_TextBoxErtek %>'
		                                    ControlTypeSource='<%# Convert.IsDBNull(Eval("ControlTypeSource")) ? DefaultControlTypeSource_TextBoxErtek : Eval("ControlTypeSource") %>'
		                                    runat="server"
		                                    Value='<%# Eval("Ertek") %>'
		                                    Visible='<%# (Eval("Tipus") as string) == "1" ? true : false %>'
                                            ToolTip='<%# (Eval("ToolTip") as string ?? "").Length > 0 ? Eval("ToolTip") + ((Eval("RegExp") as string ?? "").Length > 0 ? "\nRegExp: " + Eval("RegExp") : "") : (Eval("RegExp") as string ?? "").Length > 0 ? "RegExp: " + Eval("RegExp") : "" %>'
                                            Validate="false"
                                            Ismetlodo ='<%# (Eval("Ismetlodo") as string) == "1" ? true : false %>'  
                                             />
                                            <asp:Label ID="labelGridControlTypeDataSource" runat="server" Text='<%# Eval("ControlTypeDataSource") %>' Visible="false" />
                                         <%-- OnTextChanged = "GridElement_TextChanged" AutoPostBack ="true" --%>
                                         
                                     </ItemTemplate>
                                </asp:TemplateField>						                    						                    
                                <asp:TemplateField Visible="true">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:LinkButton ID="headerGridSorszam" runat="server" CommandName="Sort" CommandArgument="Sorszam">Sorsz�m</asp:LinkButton>
                                     </HeaderTemplate>
                                     <ItemTemplate>
                                         <asp:TextBox id="TextBoxGridSorszam" CssClass="GridViewNumberTextBoxShort" runat="server"
                                            Text='<%# Eval("Sorszam") %>'/>
                                            <%--  OnTextChanged = "GridElement_TextChanged" AutoPostBack ="true"--%>
                                         
                                     </ItemTemplate>
                                </asp:TemplateField>
                             
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:LinkButton ID="headerGridForras" runat="server" CommandName="Sort" CommandArgument="Forras">Forr�s</asp:LinkButton>
                                     </HeaderTemplate>
                                     <ItemTemplate>
                                         <asp:Label id="labelGridForras" runat="server" Text='<%# Eval("Forras") %>'/>
                                     </ItemTemplate>
                                </asp:TemplateField>                                
                                
			                    <asp:BoundField DataField="ForrasNev" HeaderText="Forr�s" SortExpression="ForrasNev">
				                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                    </asp:BoundField>                                
							    
							    <%-- Ment�s blokk eleje
							    Vigy�zat, ebben a blokkban l�v� elemeket elt�ntetj�k View m�dban, ez�rt mozgat�sukkor m�dos�tani
							    kell az oszlopsz�mot a k�dban
							    --%>
							    <%-- checkboxban jegyezz�k meg, hogy v�ltozott a sor, a t�meges ment�shez --%>
                                <asp:TemplateField Visible="true">
								    <HeaderStyle CssClass="GridViewBorderHeader"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
								        <asp:Label ID="labelGridRowChanged" runat="server" Text="Mentend�" />
								    </HeaderTemplate>
								    <ItemTemplate>
								        <%-- checkboxban jegyezz�k meg, hogy v�ltozott a sor, a t�meges ment�shez --%>
                                        <asp:CheckBox ID="cbRowChanged" Checked="false" runat="server" Visible="true"
									        AutoPostBack="false" OnCheckedChanged = "cbRowChanged_CheckChanged"
									        CssClass="HideCheckBoxText" />								    
								    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- Soronk�nti ment�s lehet�s�ge --%>
                                <asp:TemplateField Visible="true">
								    <HeaderStyle CssClass="GridViewBorderHeader"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
								        <asp:Label ID="labelGridSaveRow" runat="server" Text="Sort ment" />
								    </HeaderTemplate>
								    <ItemTemplate>							    
									    <asp:ImageButton ID="ImageButton_SaveRow"  runat="server" CausesValidation="false"
                                            ImageUrl="~/images/hu/Grid/saverow.gif" CssClass="highlightit"
                                            CommandName="SaveRow" CommandArgument='<%# Eval("Id") %>' />
								    </ItemTemplate>
                                </asp:TemplateField>                                
                                <%-- Ment�s blokk v�ge --%>                                                                                
			                                                    
                                <%-- �rt�kvizsg�lat --%>                                                                              
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:LinkButton ID="headerGridRegExp" runat="server" CommandName="Sort" CommandArgument="RegExp">RegExp</asp:LinkButton>
                                     </HeaderTemplate>
                                     <ItemTemplate>
                                         <asp:Label id="labelGridRegExp" runat="server" Text='<%# Eval("RegExp") %>'/>
                                     </ItemTemplate>
                                </asp:TemplateField>
			                     <%-- �rt�kvizsg�lat v�ge --%>

                               <asp:TemplateField Visible="true">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:LinkButton ID="headerGridNote" runat="server" CommandName="Sort" CommandArgument="Note">Megjegyz�s</asp:LinkButton>
                                     </HeaderTemplate>
                                     <ItemTemplate>
                                         <asp:TextBox id="TextBoxGridNote" CssClass="GridViewTextBox" runat="server"
                                            Text='<%# Eval("Note") %>' />
                                          <%--  OnTextChanged = "GridElement_TextChanged" AutoPostBack ="false" --%>                                          
                                     </ItemTemplate>
                                </asp:TemplateField>                                                                						                    				                    
			                    
			                    <%--  
			                    <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="Note">
				                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                    </asp:BoundField>
			                    --%>
			                    
			                    
			                    <%-- nem l�that�, de kell az �rt�ke a k�telez�s�g vizsg�lat�hoz --%>
			                    <%-- <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus" Visible = "false">
				                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
				                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
			                    </asp:BoundField> --%>
                                <asp:TemplateField Visible="false">
                                    <HeaderStyle CssClass="GridViewBorderHeader"  />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <HeaderTemplate>                                
                                        <asp:Label ID="headerGridTipus"  runat="server" Text="T�pus" />
                                     </HeaderTemplate>
                                     <ItemTemplate>
                                         <asp:Label id="labelGridTipus" runat="server" Text='<%# Eval("Tipus") %>' />
                                     </ItemTemplate>
                                </asp:TemplateField>
                                
                                <%-- seg�dmez�k az Id �s egy�b adatok t�rol�s�hoz --%>
                                <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"  />
								    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
								    <HeaderTemplate>
								    </HeaderTemplate>
								    <ItemTemplate>
								        <asp:Label ID="labelGridRecordId" runat="server" Text='<%# Eval("Id") %>' />
									    <asp:Label ID="labelGridTargyszo_Id" runat="server" Text='<%# Eval("Targyszo_Id") %>' />
									    <asp:Label ID="labelGridObj_Metaadatai_Id" runat="server" Text='<%# Eval("Obj_Metaadatai_Id") %>' />
									    <asp:Label ID="labelGridTargyszo" runat="server" Text='<%# Eval("Targyszo") %>' />
									    <asp:Label ID="labelGridErvVege" runat="server" Text='<%# Eval("ErvVege") %>' />
									    <asp:Label ID="labelGridAlapertelmezettErtek" runat="server" Text='<%# Eval("AlapertelmezettErtek") %>' />
									    
									    <asp:Label ID="labelGridToolTip" runat="server" Text='<%# Eval("ToolTip") %>' />
									    
									     <%-- m�dos�tott �rt�k helyre�ll�t�s�hoz, rejtett --%>
                                         <asp:Label ID="labelGridErtek" runat="server" Text='<%# Eval("Ertek") %>' />
                                         <asp:Label ID="labelGridErtekText" runat="server" Text='<%# Eval("Ertek") %>' />
                                         <asp:Label ID="labelGridSorszam" runat="server" Text='<%# Eval("Sorszam") %>' />
                                         <asp:Label ID="labelGridNote" runat="server" Text='<%# Eval("Note") %>' />
                                         
                                         <asp:Label ID="labelGridOpcionalis" runat="server" Text='<%# Eval("Opcionalis") %>' />
								    </ItemTemplate>
							    </asp:TemplateField>
                                                            
						    </Columns>				                      
                            <PagerSettings Visible="False" />
                        </asp:GridView>	        				        
                    </asp:Panel>
                    <eUI:eFormPanel ID="EFormPanelSaveGrid" runat="server">
                        <asp:ImageButton id="ImageButton_GridShowChanges" runat="server"
                             ImageUrl="~/images/hu/ovalgomb/valtozasok_mutatasa.gif" CssClass="highlightit"
                             OnClick = "GridShowChanges_Click" CausesValidation="false"
                              />
                        <asp:ImageButton id="ImageButton_Restore" runat="server"
                             ImageUrl="~/images/hu/ovalgomb/alapallapot.jpg" CssClass="highlightit"
                             OnClick = "GridRestoreChanges_Click" CausesValidation="false"
                              />                                                  
                        <asp:ImageButton id="ImageButton_GridSave" runat="server"
                             ImageUrl="~/images/hu/ovalgomb/sorok_mentese.gif" CssClass="highlightit"
                             OnClick = "GridSave_Click" CausesValidation="false"
                              />
                    </eUI:eFormPanel>                                               
        
			        <eUI:eFormPanel ID="EFormPanel1" runat="server">
				<table cellspacing="0" cellpadding="0" width="100%" align="center">
													
					<tr class="urlapSor">
					    <td class="mrUrlapCaption">
					        <asp:Label ID="labelForrasCaption" runat="server" Text="Forr�s:"></asp:Label>
					     </td>
						<td class="mrUrlapMezo">
							<asp:RadioButton ID="rbSzabvanyos" runat="server" GroupName="rbgForras"
							    Checked="true" Text="Szabv�nyos t�rgysz�" Enabled="false">
						    </asp:RadioButton>
						    
							<asp:RadioButton ID="rbEgyedi" runat="server" GroupName="rbgForras"
							    Checked="false" Text="Egyedi t�rgysz�" Enabled="false">
							</asp:RadioButton>
							
							<asp:Label ID="labelForras" runat="server" Text=""></asp:Label>
							
							<asp:HiddenField ID="Forras_HiddenField" runat="server" Value="" />
							<asp:HiddenField ID="ObjMetaAdataiId_HiddenField" runat="server" Value="" />	
						</td>
					</tr>
					<tr class="urlapSor">					    
						<td class="mrUrlapCaption">
							<asp:Label ID="labelRequiredTextBoxStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelTargySzavak" runat="server" Text="T�rgyszavak:"></asp:Label>						    
						</td>
						<td class="mrUrlapMezo" align="left" visible="false">
							<tsztb:TargySzavakTextBox ID="TargySzavakTextBoxSzabvanyos" runat="server" Validate="false" />
							
							<uc5:RequiredTextBox ID="requiredTextBoxTargyszo" runat="server" Validate="false" />
						</td>											
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
						    <asp:Label ID="labelErtekStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErtek" runat="server" Text="�rt�k:" />
						</td>					
						<td class="mrUrlapMezo">
							<%-- <uc5:RequiredTextBox ID="requiredTextBoxErtek" runat="server" Validate="false" /> --%>
							<eUI:DynamicValueControl ID="TextBoxErtek" CssClass="mrUrlapInput" runat="server"
							DefaultControlTypeSource='<%# DefaultControlTypeSource_TextBoxErtek %>'
							 />
							<asp:HiddenField ID="Regex_HiddenField" runat="server" />
							<asp:Label ID="labelRegex" runat="server" Visible="false" />
							<%-- valami�rt nem �ll�totta meg a ment�st hib�n�l sem, helyette javascript ellen�riz --%>
							<%--  <asp:RegularExpressionValidator id="RegExValidator" runat="server"
							    ControlToValidate="TextBoxErtek"
                                ValidationExpression=""
                                ErrorMessage="<%$Resources:Form,RegularExpressionValidationMessage%>"
                                Display="None"
                                SetFocusOnError="true"
                                Enabled="false">
                            </asp:RegularExpressionValidator>	
                            <ajaxToolkit:ValidatorCalloutExtender
                                ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegExValidator">
                            <Animations>
                                    <OnShow>
                                    <Sequence>
                                        <HideAction Visible="true" />
                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                    </Sequence>    
                                    </OnShow>
                             </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender> --%>                         					
						</td>
					</tr>
					
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="labelSorszam" runat="server" Text="Sorsz�m:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:TextBox ID="TextBoxSorszam" runat="server" Text=""  CssClass="NumberTextBoxShort"></asp:TextBox>
							<ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server"
							    Minimum="0" Maximum="100" TargetControlID="TextBoxSorszam" Width="60"/>
						</td>
					</tr>												
		
										
					<tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
						<td class="mrUrlapMezo">
							<uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="mrUrlapCaption">
							<asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label></td>
						<td class="mrUrlapMezo">
						    <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
						</td>
					</tr>

					    </table>

                        <%-- TabFooter --%>
    
                        <uc2:TabFooter ID="TabFooter1" runat="server" />
						
					    <%-- /TabFooter --%>
			    </eUI:eFormPanel>    		    
			    			
		    </td>
	    </tr>
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
