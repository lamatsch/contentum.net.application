using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.Text.RegularExpressions;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

using Contentum.eDocument.Service;


public partial class eRecordComponent_UgyiratTargySzavakTab : System.Web.UI.UserControl
{
    private const bool bShowInheritedMetaData = true;  // ezzel szab�lyozzuk, hogy az iratn�l az �gyirat metaadatai/t�gyszavai is megjelenjenek-e
    private const string errorText_RequiredValue = "Ehhez a t�rgysz�hoz k�telez� az �rt�k megad�sa!";
    private const string errorText_RequiredTargyszo = "A t�rgysz� megad�sa k�telez�!";
    private const string errorText_IllegalValueConfirm = "A m�dos�tott �rt�kek k�z�l n�h�ny nem felel meg a formai k�vetelm�nyeknek\\n(k�telez� �rt�k hi�nyzik vagy a form�tum nem megfelel�)!\\n\\nHa folytatja, hib�s sorok nem lesznek mentve.\\nBiztosan menteni akarja a v�ltoz�sokat?";
    private const string errorText_IllegalValue = "A m�dos�tott �rt�k nem felel meg a formai k�vetelm�nyeknek\\n(k�telez� �rt�k hi�nyzik vagy a form�tum nem megfelel�)!A sor nem menthet�.";
    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    protected const string DefaultControlTypeSource_TextBoxErtek = KodTarak.CONTROLTYPE_SOURCE.CustomTextBox;

    public Boolean _Active = false;
    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Utils

    protected TextBox GetTextBoxFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        TextBox tb = null;
        if (dvc != null)
        {
            if (dvc.Control != null && dvc.Control is TextBox)
            {
                tb = (TextBox)dvc.Control;
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megn�zz�k, hogy van-e textbox a vez�rl�k k�z�tt, az els�t kivessz�k
                        tb = (TextBox)c;
                        break;
                    }
                }
            }
        }

        return tb;
    }

    protected WebControl GetWebControlFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        WebControl wc = null;
        if (dvc != null)
        {
            if (dvc.Control != null && dvc.Control is TextBox)
            {
                wc = (TextBox)dvc.Control;
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megn�zz�k, hogy van-e textbox a vez�rl�k k�z�tt, az els�t kivessz�k
                        wc = (TextBox)c;
                        break;
                    }
                    else if (c is DropDownList)
                    {
                        // megn�zz�k, hogy van-e dropdownlist a vez�rl�k k�z�tt, az els�t kivessz�k
                        wc = (DropDownList)c;
                        break;
                    }
                    // BLG_608
                    else if (c is ListBox)
                    {
                        // megn�zz�k, hogy van-e listbox a vez�rl�k k�z�tt, az els�t kivessz�k
                        wc = (ListBox)c;
                        break;
                    }
                }
            }
        }

        return wc;
    }

    protected Control GetValueControlFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        Control tb = null;
        if (dvc != null && dvc.Control != null)
        {
            if (dvc.Control is TextBox)
            {
                tb = (TextBox)dvc.Control;
            }
            else if (dvc.Control is UI.ILovListTextBox)
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is HiddenField)
                    {
                        // megn�zz�k, hogy van-e hiddenfield a vez�rl�k k�z�tt, az els�t kivessz�k
                        tb = (HiddenField)c;
                        break;
                    }
                }
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megn�zz�k, hogy van-e textbox a vez�rl�k k�z�tt, az els�t kivessz�k
                        tb = (TextBox)c;
                        break;
                    }
                    else if (c is DropDownList)
                    {
                        // megn�zz�k, hogy van-e dropdownlist a vez�rl�k k�z�tt, az els�t kivessz�k
                        tb = (DropDownList)c;
                        break;
                    }
                    // BLG_608
                    else if (c is ListBox)
                    {
                        // megn�zz�k, hogy van-e listbox a vez�rl�k k�z�tt, az els�t kivessz�k
                        tb = (ListBox)c;
                        break;
                    }
                }
            }
        }

        return tb;
    }

    private void SetControlByControlSourceType(Contentum.eUIControls.DynamicValueControl dvc, string ControlTypeDataSource, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (dvc != null)
        {
            if (!String.IsNullOrEmpty(dvc.ControlTypeName))
            {
                if (dvc.ControlTypeName.Equals("component_kodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!String.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            dvc.Control.GetType().GetMethod("FillAndSetEmptyValue"
                                , new Type[] { typeof(string), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, errorPanel });
                        }
                        else
                        {
                            dvc.Control.GetType().GetMethod("FillAndSetSelectedValue"
                                , new Type[] { typeof(String), typeof(String), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, dvc.Value, errorPanel });
                        }
                    }
                }

                if (dvc.ControlTypeName.Equals("component_fuggokodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!String.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        dvc.Control.GetType().GetProperty("KodcsoportKod").SetValue(dvc.Control, ControlTypeDataSource, null);

                    }

                    //if (hf_Targyszo_ParentTargyszo != null && !String.IsNullOrEmpty(hf_Targyszo_ParentTargyszo.Value))
                    //{
                    //    DynamicValueControl parentTargyszo = this.FindControlByTargyszo(hf_Targyszo_ParentTargyszo.Value);
                    //    if (parentTargyszo != null)
                    //    {
                    //        dvc.Control.GetType().GetProperty("ParentControlId").SetValue(dvc.Control, parentTargyszo.Control.UniqueID, null);
                    //    }
                    //}
                }
                // BLG_608
                if (dvc.ControlTypeName.Equals("component_kodtaraklistbox_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!String.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        dvc.Control.GetType().GetProperty("Ismetlodo").SetValue(dvc.Control, dvc.Ismetlodo, null);
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            dvc.Control.GetType().GetMethod("FillListBox", new Type[] { typeof(string), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control, new object[] { ControlTypeDataSource, errorPanel });
                        }
                        else
                        {
                            // string[] selectedValues= dvc.Value.Split('|');
                            dvc.Control.GetType().GetMethod("FillAndSetSelectedValues"
                                , new Type[] { typeof(String), typeof(String), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, dvc.Value, errorPanel });
                        }
                    }
                }

                // checkbox list
                if (dvc.ControlTypeName.Equals("component_kodtarakcheckboxlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!String.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            dvc.Control.GetType().GetMethod("FillListBox", new Type[] { typeof(string), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control, new object[] { ControlTypeDataSource, errorPanel });
                        }
                        else
                        {
                            // string[] selectedValues= dvc.Value.Split('|');
                            dvc.Control.GetType().GetMethod("FillAndSetSelectedValues"
                                , new Type[] { typeof(String), typeof(String), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, dvc.Value, errorPanel });
                        }
                    }
                }
            }

            if (dvc.Control is UI.ILovListTextBox)
            {
                dvc.Control.GetType().GetMethod("SetTextBoxById"
                    , new Type[] { typeof(Contentum.eUIControls.eErrorPanel), typeof(UpdatePanel) }).Invoke(dvc.Control
                        , new object[] { errorPanel, null });
            }
        }
    }

    private String RegisterIllegalValuesJavaScript()
    {
        String js = "if (!confirm('" + errorText_IllegalValueConfirm + "')) return false;";
        return js;
    }

    //private String RegisterCheckErtekJavaScript(TextBox ertekTextBox, string RegExp)
    private String RegisterCheckErtekJavaScript(Contentum.eUIControls.DynamicValueControl dvc, string RegExp)
    {
        string js = "";

        //TextBox ertekTextBox = null; 
        Control valueControlErtek = null;
        WebControl tbErtek = null;
        if (dvc != null && dvc.Visible == true)
        {
            //ertekTextBox = GetTextBoxFromDVC(dvc);
            valueControlErtek = GetValueControlFromDVC(dvc);
            tbErtek = GetWebControlFromDVC(dvc);
        }

        if (valueControlErtek != null && tbErtek != null && tbErtek.Visible == true)
        {

            js = @"var text = $get('" + tbErtek.ClientID + @"');
var valueControl = $get('" + valueControlErtek.ClientID + @"');
";
            if (valueControlErtek is DropDownList) // select
            {
                js += @"
var vcErtek = (valueControl && valueControl.selectedIndex > -1 ) ? valueControl.options[valueControl.selectedIndex].value : '';
";
            }
            else // textbox
            {
                js += @"
var vcErtek = (valueControl) ? valueControl.value : '';
";
            }
            js += @"
                    if(valueControl && vcErtek == '') {
                        alert('" + errorText_RequiredValue + @"');
                        return false;
                     }";


            if (!String.IsNullOrEmpty(RegExp))
            {
                js += @"
                    else if(valueControl) {
                        var re= new RegExp('" + RegExp + @"');
                        if (!re.test(vcErtek)) {
                            alert('" + String.Format(Resources.Form.RegularExpressionValidationMessage, "(RegExp: " + RegExp + ")") + @"');
                            return false;
                        }
                    }";
            }

        }

        return js;
    }

    private String RegisterCheckErtekJavaScriptForGridView(GridView gridView)
    {
        String js = "";

        if (gridView != null)
        {
            String js_Header = @"var errormsg_regexp = '';
var errorcnt_regexp = 0;
var errormsg_missing = '';
var errorcnt_missing = 0;
var errormsg = '';
";

            String js_Footer = @"var errorcnt = errorcnt_regexp + errorcnt_missing;
if (errorcnt_regexp > 0)
{
    errormsg += '" + String.Format(Resources.Form.RegularExpressionValidationMessage, "\\n") + @"' + errormsg_regexp;
}

if (errorcnt_missing > 0)
{
    errormsg += '\n" + errorText_RequiredValue + @"\n' + errormsg_missing;
}

if (errorcnt > 0)
{
    if (!confirm(errormsg + '\n" + errorText_IllegalValueConfirm + @"')) return false;
}
";
            foreach (GridViewRow row in gridView.Rows)
            {
                //Control ertek = row.FindControl("TextBoxGridErtek");
                Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)row.FindControl("DynamicValueControl_TargyszoErtek");
                //TextBox ertek = GetTextBoxFromDVC(dvc);
                WebControl ertek = GetWebControlFromDVC(dvc);
                Control valueControlErtek = GetValueControlFromDVC(dvc);
                Label regexp = (Label)row.FindControl("labelGridRegExp");
                Control check = row.FindControl("cbRowChanged");
                Label tipus = (Label)row.FindControl("labelGridTipus");
                Label targyszo = (Label)row.FindControl("labelGridTargyszo");

                if (ertek != null && valueControlErtek != null && regexp != null && check != null && tipus != null && tipus.Text == "1")
                {

                    js += @"var text = $get('" + ertek.ClientID + @"');
var valueControl = $get('" + valueControlErtek.ClientID + @"');
var check = $get('" + check.ClientID + @"');
";
                    if (valueControlErtek is DropDownList) // select
                    {
                        js += @"
var vcErtek = (valueControl && valueControl.selectedIndex > -1 ) ? valueControl.options[valueControl.selectedIndex].value : '';
";
                    }
                    else // textbox
                    {
                        js += @"
var vcErtek = (valueControl) ? valueControl.value : '';
";
                    }

                    js += @"
if (check && check.checked == true && text && valueControl)
{
    if (vcErtek == '')
    {
        errormsg_missing += '" + targyszo.Text + @"\n';
        text.className = 'GridViewTextBoxChangedIllegalValue';
        errorcnt_missing++;
    }
";

                    if (!String.IsNullOrEmpty(regexp.Text))
                    {
                        js += @"    else {
        var re = new RegExp('" + regexp.Text + @"');
        if (!re.test(vcErtek)) {
            errormsg_regexp += '" + String.Format("{0}: ' + vcErtek + ' (RegExp: {1})", targyszo.Text, regexp.Text) + @"\n';
            text.className = 'GridViewTextBoxChangedIllegalValue';
            errorcnt_regexp++;
        }
    }
";
                    }
                    js += @"}
";
                }
            }

            if (!String.IsNullOrEmpty(js))
            {
                js = js_Header + js + js_Footer;
            }
        }

        return js;
    }

    // ellen�rzi a textbox sz�veg�nek megv�ltoz�s�t, �s ha van, sz�nezi a textboxot 
    private void SetTextBoxChangedJavaScriptForGridViewRow(GridViewRow gridViewRow)
    {

        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        WebControl tbErtek = GetWebControlFromDVC(dvc);
        Control valueControlErtek = GetValueControlFromDVC(dvc);
        TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
        TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
        Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
        Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
        Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

        Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
        Label lbRegExp = (Label)gridViewRow.FindControl("labelGridRegExp");

        if (lbTipus != null && lbTipus.Text == "1" && tbErtek != null && valueControlErtek != null && lbErtek != null)
        {
            string js = @"var lbErtek = $get('" + lbErtek.ClientID + @"');
var ertek = '';
if (lbErtek && lbErtek.firstChild)
{
    ertek = lbErtek.firstChild.nodeValue;
}
if (this.value != ertek)
{
   this.className = 'GridViewTextBoxChanged';
}
else
{
   this.className = 'GridViewTextBox';
}

if (this.value == '')
{
    this.className = 'GridViewTextBoxChangedIllegalValue';
}";
            if (!String.IsNullOrEmpty(lbRegExp.Text))
            {
                // regexp ne a textbox sz�vegre, hanem az �rt�kre fusson

                js += @"    else
    {
        var valueControl = $get('" + valueControlErtek + @"');
";
            if (valueControlErtek is DropDownList) // select
            {
                js += @"
var vcErtek = (valueControl && valueControl.selectedIndex > -1 ) ? valueControl.options[valueControl.selectedIndex].value : '';
";
            }
            else // textbox
            {
                js += @"
var vcErtek = (valueControl) ? valueControl.value : '';
";
            }
                js += @"
        var re = new RegExp('" + lbRegExp.Text + @"');
        if (valueControl && !re.test(valueControl.value)) {
            this.className = 'GridViewTextBoxChangedIllegalValue';
        }
    }
";

            }

            tbErtek.Attributes.Add("onchange", js);
        }

        if (tbNote != null && lbNote != null)
        {
            string js = @"var lbNote = $get('" + lbNote.ClientID + @"');
var note = '';
if (lbNote && lbNote.firstChild)
{
    note = lbNote.firstChild.nodeValue;
}
if (this.value != note)
{
   this.className = 'GridViewTextBoxChanged';
}
else
{
   this.className = 'GridViewTextBox';
}
";
            tbNote.Attributes.Add("onchange", js);
        }

        if (tbSorszam != null && lbSorszam != null)
        {
            string js = @"
var lbSorszam = $get('" + lbSorszam.ClientID + @"');
var sorszam = '';
if (lbSorszam && lbSorszam.firstChild)
{
    sorszam = lbSorszam.firstChild.nodeValue;
}
if (this.value != sorszam)
{
   this.className = 'GridViewNumberTextBoxShortChanged';
}
else
{
   this.className = 'GridViewNumberTextBoxShort';
}
";
            tbSorszam.Attributes.Add("onchange", js);
        }

    }

    private String RegisterShowChangesJavaScriptForGridViewRow(GridViewRow gridViewRow)
    {
        String js = "";

        if (gridViewRow != null && gridViewRow.RowType == DataControlRowType.DataRow)
        {
            CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");

            if (cb != null)
            {
                //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
                Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
                WebControl tbErtek = GetWebControlFromDVC(dvc);
                Control valueControlErtek = GetValueControlFromDVC(dvc);
                TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
                TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
                Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
                Label lbErtekText = (Label)gridViewRow.FindControl("labelGridErtekText");
                Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
                Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

                Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
                Label lbRegExp = (Label)gridViewRow.FindControl("labelGridRegExp");

                Label lbDefault = (Label)gridViewRow.FindControl("labelGridAlapertelmezettErtek");
                Label lbRecordId = (Label)gridViewRow.FindControl("labelGridRecordId");

                //if (tbErtek != null && tbSorszam != null && tbNote != null
                //    && lbErtek != null && lbSorszam != null && lbNote != null
                //    && lbTipus != null && lbRegExp != null && lbTipus.Text == "1")
                if (tbErtek != null && valueControlErtek != null && tbSorszam != null && tbNote != null
                    && lbErtek != null && lbSorszam != null && lbNote != null
                    && lbTipus != null && lbRegExp != null && lbTipus.Text == "1")
                {

                    js = @"var cb = $get('" + cb.ClientID + @"');
var tbErtek = $get('" + tbErtek.ClientID + @"');
var valueControl = $get('" + valueControlErtek.ClientID + @"');
var tbSorszam = $get('" + tbSorszam.ClientID + @"');
var tbNote = $get('" + tbNote.ClientID + @"');

var lbErtek = $get('" + lbErtek.ClientID + @"');
var lbErtekText = $get('" + lbErtekText.ClientID + @"');
var lbSorszam = $get('" + lbSorszam.ClientID + @"');
var lbNote = $get('" + lbNote.ClientID + @"');

var ertek = '';
var ertekText = '';
var sorszam = '';
var note = '';

if (lbErtek && lbErtek.firstChild)
{
    ertek = lbErtek.firstChild.nodeValue;
}

if (lbErtekText && lbErtekText.firstChild)
{
    ertekText = lbErtekText.firstChild.nodeValue;
}

if (lbSorszam && lbSorszam.firstChild)
{
    sorszam = lbSorszam.firstChild.nodeValue;
}

if (lbNote && lbNote.firstChild)
{
    note = lbNote.firstChild.nodeValue;
}

if (cb && cb.checked == true)
{
    if (tbSorszam && tbSorszam.value != sorszam)
    {
        tbSorszam.className = 'GridViewNumberTextBoxShortChanged';
    }
    else if (tbSorszam)
    {
        tbSorszam.className = 'GridViewNumberTextBoxShort';
    }

    if (tbNote && tbNote.value != note)
    {
        tbNote.className = 'GridViewTextBoxChanged';
    }
    else if(tbNote)
    {
        tbNote.className = 'GridViewTextBox';
    }
";
                    if (lbTipus.Text == "1")
                    {
                        if (tbErtek is DropDownList) // select: valueControl.options[valueControl.selectedIndex].value
                        {
                            js += @"
    var vcErtek = (valueControl && valueControl.selectedIndex > -1 ) ? valueControl.options[valueControl.selectedIndex].value : '';
";
                        }
                        else // textbox
                        {
                            js += @"
    var vcErtek = (valueControl) ? valueControl.value : '';
";
                        }

                            js += @"
    if (tbErtek && valueControl && vcErtek != ertek)
    {
        tbErtek.className = 'GridViewTextBoxChanged';
    }

    if (tbErtek && valueControl && vcErtek == '')
    {
        tbErtek.className = 'GridViewTextBoxChangedIllegalValue';
    }
";

                        if (!String.IsNullOrEmpty(lbRegExp.Text))
                        {
                            js += @"    else if (tbErtek && valueControl)
    {
        var re = new RegExp('" + lbRegExp.Text + @"');
        if (!re.test(vcValue)) {
            tbErtek.className = 'GridViewTextBoxChangedIllegalValue';
        }
    }
";
                        }
                    }


                    js += @"
}
else
{
    if (tbSorszam)
    {
        tbSorszam.className = 'GridViewNumberTextBoxShort';
        tbSorszam.value = sorszam;
    }

    if (tbNote)
    {
        tbNote.className = 'GridViewTextBox';
        tbNote.value = note;
    }
";
                    if (lbTipus.Text == "1")
                    {

                        js += @"
    if (tbErtek && valueControl)
    {
        var recordId = $get('" + lbRecordId.ClientID + @"');
        var lbDefault = $get('" + lbDefault.ClientID + @"');

        var alapertelmezett = '';
        if (lbDefault && lbDefault.firstChild)
        {
            alapertelmezett = lbDefault.firstChild.nodeValue;
        }
";

                        if (tbErtek is DropDownList) // select: valueControl.options[valueControl.selectedIndex].value
                        {
                            js += @"
        if (recordId && (!recordId.firstChild || recordId.firstChild.nodeValue == ''))
        {
            for (var i = 0; i<valueControl.options.length; i++)
            {
                if (valueControl.options[i].value==alapertelmezett) 
                {
                    valueControl.selectedIndex=i;
                    break;
                }
            }
        }
        else
        {
            for (var i = 0; i<valueControl.options.length; i++)
            {
                if (valueControl.options[i].value==ertek) 
                {
                    valueControl.selectedIndex=i;
                    break;
                }
            }
        }

        var vcErtek = (valueControl && valueControl.selectedIndex > -1 ) ? valueControl.options[valueControl.selectedIndex].value : '';
        if (valueControl && vcErtek != ertek)
        {
            tbErtek.className = 'GridViewTextBoxChanged';
        }
        else
        {
            tbErtek.className = 'GridViewTextBox';
        }
";
                        
                        }
                        else // textbox
                        {
                            js += @"
        if (recordId && (!recordId.firstChild || recordId.firstChild.nodeValue == ''))
        {
            valueControl.value = alapertelmezett;
            tbErtek.value = ertekText == null ? alapertelmezett : ertekText; // TODO;
        }
        else
        {
            valueControl.value = ertek;
            tbErtek.value = ertekText;
        }

        if (valueControl.value != ertek)
        {
            tbErtek.className = 'GridViewTextBoxChanged';
        }
        else
        {
            tbErtek.className = 'GridViewTextBox';
        }
";
                        }
                        js += @"
    }
";
                    }


                    js += @"
}
";

                }
            }
        }

        return js;
    }


    //private void SetMetaSigns(GridViewRow gridViewRow)
    //{
    //    Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
    //    TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridertek");
    //    if (lbTipus != null && tbErtek != null)
    //    {

    //        Image imgMetaHiany = (Image)gridViewRow.FindControl("imageMetaHiany");
    //        Image imgMeta = (Image)gridViewRow.FindControl("imageMeta");
    //        if (lbTipus.Text == "1" && tbErtek.Text == "")
    //        {
    //            if (imgMetaHiany != null) imgMetaHiany.Visible = true;
    //            if (imgMeta != null) imgMeta.Visible = false;
    //        }
    //        else if (lbTipus.Text == "1" && tbErtek.Text != "" && imgMeta != null)
    //        {
    //            if (imgMetaHiany != null) imgMetaHiany.Visible = false;
    //            if (imgMeta != null) imgMeta.Visible = true;
    //        }
    //        else
    //        {
    //            if (imgMetaHiany != null) imgMetaHiany.Visible = false;
    //            if (imgMeta != null) imgMeta.Visible = false;
    //        }
    //    }

    //}

    private void SetMetaSigns(GridViewRow gridViewRow)
    {
        Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl tbErtek = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        Label lbForras = (Label)gridViewRow.FindControl("labelGridForras");
        Label lbOpcionalis = (Label)gridViewRow.FindControl("labelGridOpcionalis");

        Image imgMetaHiany = (Image)gridViewRow.FindControl("imageMetaHiany");
        Image imgOpcionalisMetaHiany = (Image)gridViewRow.FindControl("imageOpcionalisMetaHiany");

        if (imgMetaHiany != null && lbTipus != null && tbErtek != null && lbForras != null && lbOpcionalis != null)
        {
            bool isAuto = (lbForras.Text == KodTarak.TARGYSZO_FORRAS.Automatikus ? true : false);
            bool isOnlyDefinition = String.IsNullOrEmpty(getRecordIdLabel(gridViewRow));

            if (isOnlyDefinition)
            {
                if (lbOpcionalis.Text == "0")
                {
                    if (imgMetaHiany != null) imgMetaHiany.Visible = true;
                    if (imgOpcionalisMetaHiany != null) imgOpcionalisMetaHiany.Visible = false;
                }
                else
                {
                    if (imgMetaHiany != null) imgMetaHiany.Visible = false;
                    if (imgOpcionalisMetaHiany != null) imgOpcionalisMetaHiany.Visible = true;
                }

            }
            else if (lbTipus.Text == "1" && tbErtek.Text != "")
            {
                if (imgMetaHiany != null) imgMetaHiany.Visible = false;
                if (imgOpcionalisMetaHiany != null) imgOpcionalisMetaHiany.Visible = false;
            }
            else
            {
                if (imgMetaHiany != null) imgMetaHiany.Visible = false;
                if (imgOpcionalisMetaHiany != null) imgOpcionalisMetaHiany.Visible = false;
            }
        }

    }

    private void SetDefaultValue(GridViewRow gridViewRow)
    {
        bool isOnlyDefinition = String.IsNullOrEmpty(getRecordIdLabel(gridViewRow));

        if (isOnlyDefinition)
        {
            Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
            //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
            Contentum.eUIControls.DynamicValueControl tbErtek = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
            Label lbDefault = (Label)gridViewRow.FindControl("labelGridAlapertelmezettErtek");

            Label lbControlTypeDataSource = (Label)gridViewRow.FindControl("labelGridControlTypeDataSource");

            if (lbTipus != null && tbErtek != null && lbDefault != null)
            {
                if (lbTipus.Text == "1" && !String.IsNullOrEmpty(lbDefault.Text))
                {
                    //tbErtek.Text = lbDefault.Text;
                    tbErtek.Value = lbDefault.Text;

                    string controlTypeDataSource = (lbControlTypeDataSource == null ? null : lbControlTypeDataSource.Text);
                    SetControlByControlSourceType(tbErtek, controlTypeDataSource, EErrorPanel1);
                }
            }
        }

    }

    //// l�that�v� teszi a "(defin�ci�)" labelt, ha nincs val�di k�t�s (hi�nyzik a rekord Id)
    //private void SetDefinicioLabelByGridViewRow(GridViewRow gridViewRow)
    //{
    //    Label lbDefinicio = (Label)gridViewRow.FindControl("labelGridDefinicio");
    //    if (lbDefinicio != null)
    //    {
    //        if (String.IsNullOrEmpty(getRecordIdLabel(gridViewRow)))
    //        {
    //            lbDefinicio.Visible = true;
    //        }
    //        else
    //        {
    //            lbDefinicio.Visible = false;
    //        }
    //    }
    //}

    private void SetErtekValidatorScriptIfRequired(GridViewRow gridViewRow)
    {
        ImageButton imgSaveRow = (ImageButton)gridViewRow.FindControl("ImageButton_SaveRow");
        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
        Label lbRegExp = (Label)gridViewRow.FindControl("labelGridRegExp");
        //if (imgSaveRow != null && tbErtek != null && lbTipus != null && lbTipus.Text == "1" && lbRegExp != null)
        if (imgSaveRow != null && dvc != null && lbTipus != null && lbTipus.Text == "1" && lbRegExp != null)
        {
            imgSaveRow.OnClientClick = RegisterCheckErtekJavaScript(dvc, lbRegExp.Text);
        }
    }

    // A GridView soraiban l�v� TextBox elemek Enabled propertyj�nek �ll�t�sa
    protected void SetGridTextBoxesEnabledState(List<string> TextBoxNames, GridView gridView, bool isEnabled)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                SetGridRowTextBoxesEnabledState(TextBoxNames, row, isEnabled);
            }
        }
    }

    // A GridView soraiban l�v� cbRowChanged �s ImageButton_SaveRow elemek Visible propertyj�nek �ll�t�sa 
    protected void SetGridSaveControlsVisibleState(GridView gridView, bool isVisible)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                SetGridRowSaveControlsVisibleState(row, isVisible);
            }
        }

    }

    // Egy GridView sorban l�v� TextBox elemek Enabled propertyj�nek �ll�t�sa
    protected void SetGridRowTextBoxesEnabledState(List<string> TextBoxNames, GridViewRow gridViewRow, bool isEnabled)
    {
        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            foreach (string element in TextBoxNames)
            {
                Control ctrl = gridViewRow.FindControl(element);
                if (ctrl != null)
                {
                    List<WebControl> webcontrols = new List<WebControl>();
                    // k�l�n kezelj�k, mert nem �rv�nyes�ti a gyerek vez�rl�k�n a WebControl �sszes jellemz�j�t...
                    // fontos, hogy megel�zze a WebControl vizsg�latot
                    if (ctrl is Contentum.eUIControls.DynamicValueControl)
                    {
                        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl(element);
                        dvc.ViewMode = !isEnabled;
                        dvc.ReadOnly = !isEnabled;
                        //if (dvc != null && dvc.Control != null)
                        //{
                        //    if (dvc.Control is WebControl)
                        //    {
                        //        webcontrols.Add((WebControl)dvc.Control);
                        //    }
                        //    else
                        //    {
                        //        if (dvc.Control.HasControls())
                        //        {
                        //            foreach (Control c in dvc.Control.Controls)
                        //            {
                        //                if (c is WebControl)
                        //                {
                        //                    webcontrols.Add((WebControl)c);
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    else if (ctrl is WebControl)
                    {
                        webcontrols.Add((WebControl)ctrl);
                    }

                    foreach (WebControl wc in webcontrols)
                    {
                        wc.Enabled = isEnabled;
                        if (isEnabled == false)
                        {
                            wc.BorderStyle = BorderStyle.None;
                            wc.BackColor = System.Drawing.Color.Transparent;
                        }
                    }
                }
            }
        }
    }

    // Egy GridView sorban l�v� CheckBox elemek Enabled propertyj�nek �ll�t�sa
    protected void SetGridRowCheckBoxesEnabledState(List<string> CheckBoxNames, GridViewRow gridViewRow, bool isEnabled)
    {
        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            foreach (string element in CheckBoxNames)
            {
                CheckBox cb = (CheckBox)gridViewRow.FindControl(element);
                if (cb != null)
                {
                    cb.Enabled = isEnabled;
                    if (isEnabled == false)
                    {
                        cb.Checked = false;
                    }
                }
            }
        }
    }

    // Egy GridView sorban l�v� CheckBox elemek Visible propertyj�nek �ll�t�sa
    protected void SetGridRowCheckBoxesVisibleState(List<string> CheckBoxNames, GridViewRow gridViewRow, bool isVisible)
    {
        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            foreach (string element in CheckBoxNames)
            {
                CheckBox cb = (CheckBox)gridViewRow.FindControl(element);
                if (cb != null)
                {
                    cb.Visible = isVisible;
                    if (isVisible == false)
                    {
                        cb.Checked = false;
                    }
                }
            }
        }
    }

    // Egy GridView sorban l�v� l�v� cbRowChanged �s ImageButton_SaveRow elemek Visible propertyj�nek �ll�t�sa 
    protected void SetGridRowSaveControlsVisibleState(GridViewRow gridViewRow, bool isVisible)
    {
        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
            if (cb != null)
            {
                cb.Visible = isVisible;
            }

            ImageButton ib = (ImageButton)gridViewRow.FindControl("ImageButton_SaveRow");
            if (ib != null)
            {
                ib.Visible = isVisible;
            }
        }
    }

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// A GridView sor�nak kijel�l�se recordID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected void selectRowByID(string ID, GridView gridView, string checkBoxId)
    {
        gridView.SelectedIndex = -1;
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                gridView.SelectedIndex = row.RowIndex;
                UI.SetGridViewCheckBoxesToSelectedRow(gridView, row.RowIndex, checkBoxId);
                return;
            }
        }
    }


    /// <summary>
    /// Tipus �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getTipus(GridView gridView)
    {
        string tipus = "";
        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];

            if (selectedRow != null)
            {
                Label lbTipus = (Label)selectedRow.FindControl("labelGridTipus");
                if (lbTipus != null)
                {
                    tipus = lbTipus.Text;
                }
            }
        }
        return tipus;
    }

    private string getForrasKod(GridView gridView)
    {
        string forras = "";

        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];


            if (selectedRow != null)
            {
                Label lbForras = (Label)selectedRow.FindControl("labelGridForras");
                if (lbForras != null)
                {
                    forras = lbForras.Text;
                }
            }
        }
        return forras;
    }

    /// <summary>
    /// RegExp �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getRegExp(GridView gridView)
    {
        string regexp = "";

        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];


            if (selectedRow != null)
            {
                Label lbRegExp = (Label)selectedRow.FindControl("labelGridRegExp");
                if (lbRegExp != null)
                {
                    regexp = lbRegExp.Text;
                }
            }
        }
        return regexp;
    }

    /// <summary>
    /// DynamicValueControl ControlSourceType �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getControlSourceType(GridView gridView)
    {
        string controlTypeSource = DefaultControlTypeSource_TextBoxErtek;
        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];

            if (selectedRow != null)
            {
                Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)selectedRow.FindControl("DynamicValueControl_TargyszoErtek");
                if (dvc != null)
                {
                    controlTypeSource = dvc.ControlTypeSource;
                }
            }
        }
        return controlTypeSource;
    }

    /// <summary>
    /// DynamicValueControl ControlTypeDataSource �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getControlTypeDataSource(GridView gridView)
    {
        string controlTypeDataSource = "";
        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];

            controlTypeDataSource = getControlTypeDataSource(selectedRow);
        }
        return controlTypeDataSource;
    }

    /// <summary>
    /// DynamicValueControl ControlTypeDataSource �rt�ke a GridView kiv�lasztott sor�b�l
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getControlTypeDataSource(GridViewRow gridViewRow)
    {
        string controlTypeDataSource = "";
        if (gridViewRow != null)
        {
            Label lb = (Label)gridViewRow.FindControl("labelGridControlTypeDataSource");
            if (lb != null)
            {
                controlTypeDataSource = lb.Text;
            }
        }
        return controlTypeDataSource;
    }

    /// <summary>
    /// RecordId �rt�ke a GridView kiv�lasztott sor�b�l rejtett labelr�l
    /// (mivel �j rekord ment�sn�l, ha nem h�vunk GridVewBind-ot,
    /// nem tudjuk �ll�tani k�zvetlen�l az Id �rt�k�t, ez�rt a label �rt�k�t �ll�tjuk, �s itt azt olvassuk vissza)
    /// </summary>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected string getSelectedRecordIdLabel(GridView gridView)
    {
        string recordId = "";

        if (gridView.SelectedIndex >= 0 && gridView.Rows[gridView.SelectedIndex].RowType == DataControlRowType.DataRow)
        {
            GridViewRow selectedRow = gridView.Rows[gridView.SelectedIndex];

            if (selectedRow != null)
            {
                Label lbRecordId = (Label)selectedRow.FindControl("labelGridRecordId");
                if (lbRecordId != null)
                {
                    recordId = lbRecordId.Text;
                }
            }
        }
        return recordId;
    }

    /// <summary>
    /// RecordId �rt�ke a GridView egy sor�b�l, rejtett labelr�l
    /// (mivel �j rekord ment�sn�l, ha nem h�vunk GridVewBind-ot,
    /// nem tudjuk �ll�tani k�zvetlen�l az Id �rt�k�t, ez�rt a label �rt�k�t �ll�tjuk)
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="recordId">Az �j rekord Id-je, amit a Labelre �runk</param>
    /// <returns></returns>
    protected String getRecordIdLabel(GridViewRow gridViewRow)
    {
        string recordId = "";

        if (gridViewRow != null)
        {

            if (gridViewRow.RowType == DataControlRowType.DataRow)
            {
                Label lbRecordId = (Label)gridViewRow.FindControl("labelGridRecordId");
                if (lbRecordId != null)
                {
                    recordId = lbRecordId.Text;
                }
            }
        }
        return recordId;
    }

    /// <summary>
    /// RecordId �rt�k�nek be�ll�t�sa a GridView egy sor�ban rejtett labelen
    /// (mivel �j rekord ment�sn�l, ha nem h�vunk GridVewBind-ot,
    /// nem tudjuk �ll�tani k�zvetlen�l az Id �rt�k�t, ez�rt a label �rt�k�t �ll�tjuk)
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="recordId">Az �j rekord Id-je, amit a Labelre �runk</param>
    /// <returns></returns>
    protected void setRecordIdLabel(GridViewRow gridViewRow, String recordId)
    {
        if (gridViewRow == null || recordId == null) return;

        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            Label lbRecordId = (Label)gridViewRow.FindControl("labelGridRecordId");
            if (lbRecordId != null)
            {
                lbRecordId.Text = recordId;
            }
        }
    }

    // mag�t�l nem �ll vissza a st�lus, ha ReadOnly=true-r�l visszav�ltunk false-ra
    protected void SetTextBoxReadOnly(TextBox tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        //tbox.Enabled = !isReadOnly;
        if (!isReadOnly)
        {
            // vissza�ll�tjuk a st�lust
            tbox.CssClass = normalCssClass;
        }

    }
    protected void SetTextBoxReadOnly(Contentum.eUIControls.DynamicValueControl tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        //tbox.Enabled = !isReadOnly;
        if (!isReadOnly)
        {
            // vissza�ll�tjuk a st�lust
            tbox.CssClass = normalCssClass;

            // bals� textbox st�lus�ll�t�s
            TextBox tb = GetTextBoxFromDVC(tbox);
            if (tb != null)
            {
                SetTextBoxReadOnly(tb, isReadOnly, normalCssClass);
            }
        }

    }

    /// <summary>
    /// Meghat�rozza az eredm�nyhalmazban szerepl� max. sorsz�mot
    /// -> alap�rtelmezett k�vetkez� sorsz�m meghat�roz�s�hoz
    /// </summary>
    /// <param name="columnName">Oszlop neve, amiben a maximumot keress�k</param> 
    /// <param name="tableindex">T�bla sorsz�ma a result.Ds-ben, mely alapj�n a sorsz�mot meghat�rozzuk</param>
    private int GetMaxSorszam(Result result, string columnName, int tableindex)
    {
        int max = 0;
        int currentValue;

        if (result.Ds != null && result.Ds.Tables.Count > tableindex && result.Ds.Tables[tableindex].Rows.Count != 0 && result.Ds.Tables[tableindex].Rows[0][columnName] != null)
        {

            for (int i = 0; i < result.Ds.Tables[tableindex].Rows.Count; i++)
            {
                if (int.TryParse(result.Ds.Tables[tableindex].Rows[i][columnName].ToString(), out currentValue))
                {
                    if (currentValue > max)
                        max = currentValue;
                }
            }
        }

        return max;

    }

    // a numeric updown maximum�t adja vissza a gridview sorsz�mok �rt�keihez vagy a
    // gridviewsorainak sz�m�hoz
    protected int GetNupeMaximum()
    {
        int maxSorszam;
        int maximum;
        if (int.TryParse(MaxSorszam_HiddenField.Value.ToString(), out maxSorszam))
        {
            maximum = maxSorszam + 1;
        }
        else
        {
            maximum = TargyszavakGridView.Rows == null ? 1 : TargyszavakGridView.Rows.Count + 1;
        }

        return maximum;
    }

    // az �tadott �rt�kek alapj�n �ssze�ll�t egy ToolTipet az �rt�k mez�re
    protected String GetErtekToolTip(string ToolTip, string RegExp)
    {
        String strToolTip = null;
        if (!String.IsNullOrEmpty(ToolTip))
        {
            strToolTip = ToolTip;
            if (!String.IsNullOrEmpty(RegExp))
            {
                strToolTip += "\nRegExp: " + RegExp;
            }
        }
        else if (!String.IsNullOrEmpty(RegExp))
        {
            strToolTip += "RegExp: " + RegExp;
        }
        return strToolTip;

    }

    #endregion Utils


    #region Base Page


    // TODO: funkci�jogosults�gok!!!
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TargyszavakList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //        if (!MainPanel.Visible) return;

        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);
        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        //TargySzavakTextBoxSzabvanyos.ValueChanged += new EventHandler(TargySzavakTextBox_ValueChanged);
        TargySzavakTextBoxSzabvanyos.RefreshCallingWindow = true; // a Tipus �rt�k v�ltoz�s�nak �rz�kel�s�hez sz�ks�ges
        TargySzavakTextBoxSzabvanyos.TextBox.AutoPostBack = true;
        TargySzavakTextBoxSzabvanyos.TextBox.TextChanged += new EventHandler(TargySzavakTextBox_TextChanged);
        requiredTextBoxTargyszo.TextBox.AutoPostBack = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        //// valid�tor
        //JavaScripts.RegisterOnValidatorOverClientScript(Page);

        //Command = Request.QueryString.Get(QueryStringVars.Command);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.New);
        TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.View);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Targyszavak" + CommandName.Invalidate);
        TabHeader1.VersionEnabled = false;
        TabHeader1.ElosztoListaEnabled = false;
        TabHeader1.LinkViewVisible = false;
        TabHeader1.ViewVisible = true;

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(getSelectedRecordIdLabel(TargyszavakGridView));
            ShowChangesInGridView(false);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(TargyszavakGridView);

        //selectedRecordId kezel�se
        //ListHeader1.AttachedGridView = TargyszavakGridView;


    }

    protected void TargyszavakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;
    }


    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        if (_ParentForm == Constants.ParentForms.IraIrat
            || _ParentForm == Constants.ParentForms.Ugyirat)
        {
            Command = Request.QueryString.Get(QueryStringVars.Command);

            #region Object_Id
            // Ugyirat objektumt�pus ObjTip_Id-j�nek meghat�roz�sa
            // TODO: �ltal�nosabban meghat�rozni
            string ObjTip_Kod = "";
            switch (_ParentForm)
            {
                case Constants.ParentForms.Ugyirat:
                    ObjTip_Kod = "EREC_UgyUgyiratok";
                    break;
                case Constants.ParentForms.IraIrat:
                    ObjTip_Kod = "EREC_IraIratok";
                    break;
                default:    // hiba
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_ObjTipUnknown);
                    ErrorUpdatePanel.Update();
                    TargyszavakUpdatePanel.Visible = false;
                    return;
                //ObjTip_Kod = "";
            }

            Contentum.eAdmin.Service.KRT_ObjTipusokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();

            search.Kod.Value = ObjTip_Kod;
            search.Kod.Operator = Contentum.eQuery.Query.Operators.equals;
            Result result = service.GetAll(ExecParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    ObjTip_Id_HiddenField.Value = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
                else
                {  // TODO: hibakezel�s
                    ObjTip_Id_HiddenField.Value = "00000000-0000-0000-0000-000000000000";
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                TargyszavakUpdatePanel.Visible = false;
                return;
                //ObjTip_Id_HiddenField.Value = "00000000-0000-0000-0000-000000000000";
            }

            #endregion Object_Id

            // itt kell a tiltast megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
            TabHeader1.VersionVisible = false;
            TabHeader1.ElosztoListaVisible = false;

            TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(TargyszavakGridView.ClientID, "", "check");
            //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabFooter1.ImageButton_Save.OnClientClick = "";
            TabFooter1.ImageButton_Save.CausesValidation = false;

            // ne csin�ljuk meg feleslegesen
            if (Command != CommandName.View)
            {
                //TargySzavakDropDownBind();
                nupeSorszam.Maximum = GetNupeMaximum();

            }

            ClearForm();
            EFormPanel1.Visible = false;
            EFormPanelSaveGrid.Visible = (Command == CommandName.View ? false : true);
            TargyszavakGridViewBind();

            if (_ParentForm == Constants.ParentForms.IraIrat)
            {
                OrokoltTargyszavakGridViewBind();
            }
            else
            {
                OrokoltTargyszavakPanel.Visible = false;
            }

            pageView.SetViewOnPage(Command);

            if (Command == CommandName.View)
            {
                //EFormPanelSaveGrid.Visible = false;
                TargyszavakGridView.Columns[8].Visible = false;
                TargyszavakGridView.Columns[9].Visible = false;
            }

        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel.Update();
            MainPanel.Visible = false;
        }
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();

        String SubCommand = e.CommandName;

        TabFooter1.CommandArgument = SubCommand;

        if (e.CommandName == CommandName.New)
        {
            SetNewControls();
            // t�r�lj�k a kiv�laszt�st a gridviewban, �s vissza�ll�tjuk az ellen�rz�
            // JavaScripteket
            ui.GridViewDeSelectAllCheckBox(TargyszavakGridView);
            UI.ClearGridViewRowSelection(TargyszavakGridView);
            TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            EFormPanel1.Visible = true;
            EFormPanelSaveGrid.Visible = false;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;

            //String RecordId = UI.GetGridViewSelectedRecordId(TargyszavakGridView);
            String RecordId = getSelectedRecordIdLabel(TargyszavakGridView);

            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = null;

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs azonos�t�, �s nem automatikus elvi hozz�rendel�s -> hiba
                if (getForrasKod(TargyszavakGridView) != KodTarak.TARGYSZO_FORRAS.Automatikus)
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel.Update();
                }
                else
                {
                    // nincs azonos�t�, de automatikus elvi hozz�rendel�s
                    erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                    FillBusinessObjectFromGridViewRow(TargyszavakGridView.SelectedRow, true, ref erec_ObjektumTargyszavai);

                    // itt k�zzel kell megadni az adatokat
                    erec_ObjektumTargyszavai.Obj_Id = ParentId;
                    erec_ObjektumTargyszavai.Updated.Obj_Id = true;

                    erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id_HiddenField.Value;
                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                    erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
                    erec_ObjektumTargyszavai.Updated.Forras = true;
                }

            }
            else
            {
                // van azonos�t�, lek�rj�k a rekordot
                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                execParam.Record_Id = RecordId;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    erec_ObjektumTargyszavai = (EREC_ObjektumTargyszavai)result.Record;
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }

            if (erec_ObjektumTargyszavai != null)
            {
                TextBoxErtek.DefaultControlTypeSource = DefaultControlTypeSource_TextBoxErtek;
                TextBoxErtek.ControlTypeSource = getControlSourceType(TargyszavakGridView);
                LoadComponentsFromBusinessObject(erec_ObjektumTargyszavai);

                if (e.CommandName == CommandName.Modify)
                {
                    SetModifyControls(erec_ObjektumTargyszavai.Forras);

                    // kezel�s t�pus szerint
                    switch (getTipus(TargyszavakGridView))
                    {
                        case "1":
                            SetTextBoxReadOnly(TextBoxErtek, false, "mrUrlapInput");
                            labelErtekStar.Visible = true;
                            TextBoxErtek.ToolTip = GetErtekToolTip(TargySzavakTextBoxSzabvanyos.ToolTip, TargySzavakTextBoxSzabvanyos.RegExp);
                            break;
                        case "0":
                            SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                            labelErtekStar.Visible = false;
                            TextBoxErtek.ToolTip = "";
                            break;
                        default:
                            SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                            labelErtekStar.Visible = false;
                            TextBoxErtek.ToolTip = "";
                            break;
                    }

                    //TabFooter1.ImageButton_Save.OnClientClick = RegisterCheckErtekJavaScript(requiredTextBoxErtek.TextBox.ClientID);
                    if (TargySzavakTextBoxSzabvanyos.Tipus == "1")
                    {
                        TabFooter1.ImageButton_Save.OnClientClick = RegisterCheckErtekJavaScript(TextBoxErtek, TargySzavakTextBoxSzabvanyos.RegExp);
                    }
                    else
                    {
                        TabFooter1.ImageButton_Save.OnClientClick = "";
                    }
                }
                else if (e.CommandName == CommandName.View)
                {
                    SetViewControls(erec_ObjektumTargyszavai.Forras);

                    // kezel�s t�pus szerint
                    switch (getTipus(TargyszavakGridView))
                    {
                        case "1":
                            labelErtekStar.Visible = true;
                            break;
                        case "0":
                            labelErtekStar.Visible = false;
                            break;
                        default:
                            labelErtekStar.Visible = false;
                            break;
                    }
                }

                EFormPanelSaveGrid.Visible = false;

            }

        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTargyszavak();
            ReLoadTab();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
    }


    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {

            // TODO: Altalanos hiba kezeles mindket modulhoz:

            Result result = new Result();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            if (FunctionRights.GetFunkcioJog(Page, "Targyszavak" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        EFormPanelSaveGrid.Visible = (Request.QueryString.Get(CommandName.Command) == CommandName.View ? false : true);
                        break;
                    case CommandName.New:
                        {
                            //// ellen�rz�s, ki van-e t�ltve a relev�ns beviteli mez� 
                            if (rbSzabvanyos.Checked && TargySzavakTextBoxSzabvanyos.Text == ""
                                    || rbEgyedi.Checked && TargySzavakTextBoxSzabvanyos.Text == "")
                            {
                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorText_RequiredTargyszo);
                                ErrorUpdatePanel.Update();
                            }
                            else if (rbSzabvanyos.Checked
                                    && TargySzavakTextBoxSzabvanyos.Tipus == "1"
                                //&& requiredTextBoxErtek.Text == ""
                                    && TextBoxErtek.Text == ""
                                )
                            {
                                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorText_RequiredValue);
                                ErrorUpdatePanel.Update();
                            }
                            else
                            {
                                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = GetBusinessObjectFromComponents(true);

                                // ellen�rz�s, ne lehessen t�bbsz�r felvenni ua.t�rgysz�/�rt�k kombin�ci�t...
                                // szervezeti: Targyszo_Id-re is ellen�rz�nk
                                // egyedi: Targyszo-ra ellen�rz�nk, csak pontos egyez�s
                                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                                search.Obj_Id.Value = erec_ObjektumTargyszavai.Obj_Id;
                                search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                search.ObjTip_Id.Value = erec_ObjektumTargyszavai.ObjTip_Id;
                                search.ObjTip_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                if (String.IsNullOrEmpty(erec_ObjektumTargyszavai.Ertek))
                                {
                                    search.Ertek.Operator = Contentum.eQuery.Query.Operators.isnull;
                                }
                                else
                                {
                                    search.Ertek.Value = erec_ObjektumTargyszavai.Ertek;
                                    search.Ertek.Operator = Contentum.eQuery.Query.Operators.equals;
                                }
                                if (rbSzabvanyos.Checked == true)
                                {
                                    search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id;
                                    search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                }
                                else if (rbEgyedi.Checked == true)
                                {
                                    search.Targyszo.Value = erec_ObjektumTargyszavai.Targyszo;
                                    search.Targyszo.Operator = Contentum.eQuery.Query.Operators.equals;
                                }

                                result = service.GetAll(execParam, search);

                                if (String.IsNullOrEmpty(result.ErrorCode)) // GetAll
                                {
                                    if (result.Ds.Tables[0].Rows.Count == 0)
                                    {
                                        result = service.Insert(execParam, erec_ObjektumTargyszavai);

                                        if (String.IsNullOrEmpty(result.ErrorCode)) // Insert
                                        {
                                            //Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                            ReLoadTab();
                                            selectRowByID(result.Uid, TargyszavakGridView, "check");
                                        }
                                        else
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                            ErrorUpdatePanel.Update();
                                        }
                                    }
                                    else  // volt m�r ilyen felvitel
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                                        ErrorUpdatePanel.Update();
                                    }

                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel.Update();
                                }
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            //String recordId = UI.GetGridViewSelectedRecordId(TargyszavakGridView);
                            String recordId = getSelectedRecordIdLabel(TargyszavakGridView);

                            bool isNewRecord = String.IsNullOrEmpty(recordId);    // ha automatikus elvi hozz�rendel�s �s nincs recordId, akkor �j rekord

                            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = null;

                            if (String.IsNullOrEmpty(recordId)
                                && getForrasKod(TargyszavakGridView) != KodTarak.TARGYSZO_FORRAS.Automatikus)
                            {
                                // nincs Id megadva �s nem automatikus elvi hozz�rendel�s:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel.Update();
                            }
                            else
                            {
                                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                erec_ObjektumTargyszavai = GetBusinessObjectFromComponents(false);

                                //// ellen�rz�s, ki van-e t�ltve a relev�ns beviteli mez� 
                                if (TargySzavakTextBoxSzabvanyos.Visible == true && TargySzavakTextBoxSzabvanyos.Text == ""
                                        || requiredTextBoxTargyszo.Visible == true && requiredTextBoxTargyszo.Text == "")
                                {
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorText_RequiredTargyszo);
                                    ErrorUpdatePanel.Update();
                                }
                                else if ((getTipus(TargyszavakGridView) == "1"
                                            || TargySzavakTextBoxSzabvanyos.Visible == true && TargySzavakTextBoxSzabvanyos.Tipus == "1"
                                            )
                                    ////&& requiredTextBoxErtek.Text == "")
                                    //        && TextBoxErtek.Text == ""
                                        && String.IsNullOrEmpty(TextBoxErtek.Value)
                                    )
                                {
                                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorText_RequiredValue);
                                    ErrorUpdatePanel.Update();
                                }
                                else
                                {
                                    // ellen�rz�s, ne lehessen t�bbsz�r felvenni ua.t�rgysz�/�rt�k kombin�ci�t...
                                    // szervezeti: Targyszo_Id-re is ellen�rz�nk
                                    // egyedi: Targyszo-ra ellen�rz�nk, csak pontos egyez�s
                                    EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                                    search.Obj_Id.Value = erec_ObjektumTargyszavai.Obj_Id;
                                    search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                    search.ObjTip_Id.Value = erec_ObjektumTargyszavai.ObjTip_Id;
                                    search.ObjTip_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                    if (String.IsNullOrEmpty(erec_ObjektumTargyszavai.Ertek))
                                    {
                                        search.Ertek.Operator = Contentum.eQuery.Query.Operators.isnull;
                                    }
                                    else
                                    {
                                        search.Ertek.Value = erec_ObjektumTargyszavai.Ertek;
                                        search.Ertek.Operator = Contentum.eQuery.Query.Operators.equals;
                                    }
                                    if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Listas)     // szervezeti
                                    {
                                        search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id.ToString();
                                        search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                    }
                                    else if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Automatikus)     // auto
                                    {
                                        search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id.ToString();
                                        search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                                        search.Obj_Metaadatai_Id.Value = erec_ObjektumTargyszavai.Obj_Metaadatai_Id;
                                        search.Obj_Metaadatai_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                    }
                                    else // k�zi
                                    {
                                        search.Targyszo.Value = erec_ObjektumTargyszavai.Targyszo.ToString();
                                        search.Targyszo.Operator = Contentum.eQuery.Query.Operators.equals;
                                    }

                                    if (isNewRecord == false)
                                    {
                                        search.Id.Value = recordId;
                                        search.Id.Operator = Contentum.eQuery.Query.Operators.notequals;
                                    }

                                    result = service.GetAll(execParam, search);

                                    if (String.IsNullOrEmpty(result.ErrorCode)) // GetAll
                                    {
                                        if (result.Ds.Tables[0].Rows.Count == 0)
                                        {
                                            if (isNewRecord == true)
                                            {
                                                result = service.Insert(execParam, erec_ObjektumTargyszavai);
                                            }
                                            else
                                            {
                                                execParam.Record_Id = recordId;
                                                result = service.Update(execParam, erec_ObjektumTargyszavai);
                                            }
                                            if (String.IsNullOrEmpty(result.ErrorCode)) //Update vagy Insert
                                            {
                                                if (isNewRecord == true)
                                                {
                                                    recordId = result.Uid;
                                                }
                                                //Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                                ReLoadTab();
                                                selectRowByID(recordId, TargyszavakGridView, "check");
                                            }
                                            else
                                            {
                                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                                ErrorUpdatePanel.Update();
                                            }
                                        }
                                        else  // volt m�r ilyen felvitel
                                        {
                                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                                            ErrorUpdatePanel.Update();
                                        }

                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            }

        }
        else if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
            EFormPanelSaveGrid.Visible = (Request.QueryString.Get(CommandName.Command) == CommandName.View ? false : true);
        }

    }

    private void ClearForm()
    {
        labelErtekStar.Visible = false;
        requiredTextBoxTargyszo.Text = "";
        //TextBoxErtek.Text = "";
        TextBoxErtek.Value = "";
        SetControlByControlSourceType(TextBoxErtek, null, EErrorPanel1);
        textNote.Text = "";
        nupeSorszam.Maximum = GetNupeMaximum();
        TextBoxSorszam.Text = ((int)nupeSorszam.Maximum).ToString(); // a k�vetkez� sz�ba j�het� �rt�kre �ll�tjuk
        TargySzavakTextBoxSzabvanyos.Id_HiddenField = "";
        TargySzavakTextBoxSzabvanyos.SetTargySzavakTextBoxById(EErrorPanel1);
        labelForras.Text = "";

    }


    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_ObjektumTargyszavai erec_ObjektumTargyszavai)
    {

        if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Listas) // szervezeti v. k�zponti
        {
            //TargySzavakTextBoxSzabvanyos.Visible = true;
            //SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, false, "mrUrlapInput");
            //TargySzavakTextBoxSzabvanyos.Validate = true;
            //requiredTextBoxTargyszo.Visible = false;
            //requiredTextBoxTargyszo.Validate = false;
            rbEgyedi.Checked = false;
            rbSzabvanyos.Checked = true;

            TargySzavakTextBoxSzabvanyos.Id_HiddenField = erec_ObjektumTargyszavai.Targyszo_Id;
            TargySzavakTextBoxSzabvanyos.SetTargySzavakTextBoxById(EErrorPanel1);

        }
        else if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Automatikus) // auto
        {
            //TargySzavakTextBoxSzabvanyos.Visible = true;
            //SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
            //TargySzavakTextBoxSzabvanyos.Validate = false;
            //requiredTextBoxTargyszo.Visible = false;
            //requiredTextBoxTargyszo.Validate = false;
            rbEgyedi.Checked = false;
            rbSzabvanyos.Checked = true;

            TargySzavakTextBoxSzabvanyos.Id_HiddenField = erec_ObjektumTargyszavai.Targyszo_Id;
            TargySzavakTextBoxSzabvanyos.SetTargySzavakTextBoxById(EErrorPanel1);
        }
        else // k�zi
        {
            //SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, false, "mrUrlapInput");
            //TargySzavakTextBoxSzabvanyos.Visible = false;
            //SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, false, "mrUrlapInput");
            //TargySzavakTextBoxSzabvanyos.Validate = false;
            //requiredTextBoxTargyszo.Visible = true;
            //requiredTextBoxTargyszo.Validate = true;
            rbEgyedi.Checked = true;
            rbSzabvanyos.Checked = false;

            TargySzavakTextBoxSzabvanyos.Id_HiddenField = String.Empty;
            TargySzavakTextBoxSzabvanyos.Text = erec_ObjektumTargyszavai.Targyszo;
            //requiredTextBoxTargyszo.Text = erec_ObjektumTargyszavai.Targyszo;
        }

        Forras_HiddenField.Value = erec_ObjektumTargyszavai.Forras;
        ObjMetaAdataiId_HiddenField.Value = erec_ObjektumTargyszavai.Obj_Metaadatai_Id;

        // Forras n�v kiv�tele adatb�zisb�l
        string forrasNev;
        if ((Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport("TARGYSZO_FORRAS", Page)).TryGetValue(erec_ObjektumTargyszavai.Forras, out forrasNev))
        {
            labelForras.Text = forrasNev;
        }

        if (getTipus(TargyszavakGridView) == "1")
        {
            //TextBoxErtek.Text = erec_ObjektumTargyszavai.Ertek;
            TextBoxErtek.Value = erec_ObjektumTargyszavai.Ertek;

            string controlTypeDataSource = getControlTypeDataSource(TargyszavakGridView);
            SetControlByControlSourceType(TextBoxErtek, controlTypeDataSource, EErrorPanel1);
        }
        TextBoxSorszam.Text = erec_ObjektumTargyszavai.Sorszam;
        ErvenyessegCalendarControl1.ErvKezd = erec_ObjektumTargyszavai.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_ObjektumTargyszavai.ErvVege;
        textNote.Text = erec_ObjektumTargyszavai.Base.Note;

        Record_Ver_HiddenField.Value = erec_ObjektumTargyszavai.Base.Ver;
    }

    // form --> business object
    private EREC_ObjektumTargyszavai GetBusinessObjectFromComponents(bool isNew)
    {
        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

        if (isNew)
        {
            if (rbSzabvanyos.Checked == true)
            {
                erec_ObjektumTargyszavai.Targyszo_Id = TargySzavakTextBoxSzabvanyos.Id_HiddenField;
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = pageView.GetUpdatedByView(TargySzavakTextBoxSzabvanyos);

                erec_ObjektumTargyszavai.Targyszo = TargySzavakTextBoxSzabvanyos.Text;
                erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(TargySzavakTextBoxSzabvanyos);

                erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Listas; // Szervezeti
                erec_ObjektumTargyszavai.Updated.Forras = true;
            }
            else
                if (rbEgyedi.Checked == true)
                {
                    erec_ObjektumTargyszavai.Targyszo_Id = "";
                    erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;
                    erec_ObjektumTargyszavai.Targyszo = TargySzavakTextBoxSzabvanyos.Text;
                    erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(TargySzavakTextBoxSzabvanyos);
                    erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Kezi; // K�zi
                    erec_ObjektumTargyszavai.Updated.Forras = true;
                    //new System.Data.SqlTypes.SqlGuid("00000000-0000-0000-0000-000000000000");
                }
        }
        else
        {
            if (rbSzabvanyos.Checked == true)
            {
                erec_ObjektumTargyszavai.Targyszo_Id = TargySzavakTextBoxSzabvanyos.Id_HiddenField;
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = pageView.GetUpdatedByView(TargySzavakTextBoxSzabvanyos);

                erec_ObjektumTargyszavai.Targyszo = TargySzavakTextBoxSzabvanyos.Text;
                erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(TargySzavakTextBoxSzabvanyos);
            }
            else
                if (rbEgyedi.Checked == true)
                {
                    erec_ObjektumTargyszavai.Targyszo_Id = "";
                    erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                    erec_ObjektumTargyszavai.Targyszo = requiredTextBoxTargyszo.Text;
                    erec_ObjektumTargyszavai.Updated.Targyszo = pageView.GetUpdatedByView(requiredTextBoxTargyszo);
                }

            if (!String.IsNullOrEmpty(Forras_HiddenField.Value))
            {
                erec_ObjektumTargyszavai.Forras = Forras_HiddenField.Value;
                erec_ObjektumTargyszavai.Updated.Forras = true;
            }

            if (!String.IsNullOrEmpty(ObjMetaAdataiId_HiddenField.Value))
            {
                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = ObjMetaAdataiId_HiddenField.Value;
                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;
            }

        }

        erec_ObjektumTargyszavai.Ertek = TextBoxErtek.Value; //TextBoxErtek.Text;
        erec_ObjektumTargyszavai.Updated.Ertek = pageView.GetUpdatedByView(TextBoxErtek);

        erec_ObjektumTargyszavai.Sorszam = TextBoxSorszam.Text;
        erec_ObjektumTargyszavai.Updated.Sorszam = pageView.GetUpdatedByView(TextBoxSorszam);

        erec_ObjektumTargyszavai.Obj_Id = ParentId;
        erec_ObjektumTargyszavai.Updated.Obj_Id = true;

        erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id_HiddenField.Value;
        erec_ObjektumTargyszavai.Updated.ObjTip_Id = pageView.GetUpdatedByView(ObjTip_Id_HiddenField);
        erec_ObjektumTargyszavai.Base.Ver = Record_Ver_HiddenField.Value;
        erec_ObjektumTargyszavai.Base.Updated.Ver = pageView.GetUpdatedByView(Record_Ver_HiddenField);

        erec_ObjektumTargyszavai.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_ObjektumTargyszavai.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_ObjektumTargyszavai.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_ObjektumTargyszavai.Base.Note = textNote.Text;
        erec_ObjektumTargyszavai.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        return erec_ObjektumTargyszavai;
    }

    // form --> business object
    /// <summary>
    /// Business Object elemeinek felt�lt�se Gridrow sor�b�l
    /// </summary>
    /// <param name="gridViewRow"></param>
    /// <param name="isNewRecord">Ha igaz, a Targyszo, Targyszo_Id, Obj_MetaAdatai_Id, ErvVege mez�k is kit�lt�sre ker�lnek,
    /// ha hamis, akkor csak az Ertek, Sorszam, Note.</param>
    /// <param name="erec_ObjektumTargyszavai"></param>
    /// <returns>True, ha a Sorsz�m v�ltozott, false hib�n�l �s ha a sorsz�m nem v�ltozott</returns>
    private bool FillBusinessObjectFromGridViewRow(GridViewRow gridViewRow, bool isNewRecord, ref EREC_ObjektumTargyszavai erec_ObjektumTargyszavai)
    {
        if (gridViewRow == null) return false;
        if (erec_ObjektumTargyszavai == null) erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();

        bool sorszamChanged = false;

        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl tbErtek = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
        TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
        CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
        Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
        Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
        Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

        Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");

        if (tbErtek != null)
        {
            //erec_ObjektumTargyszavai.Ertek = tbErtek.Text;
            erec_ObjektumTargyszavai.Ertek = tbErtek.Value;
            erec_ObjektumTargyszavai.Updated.Ertek = pageView.GetUpdatedByView(tbErtek);
            // TODO: ellen�rz�s, hogy ne legyen azonos t�rgysz�-�rt�kp�r
        }

        if (tbSorszam != null)
        {
            erec_ObjektumTargyszavai.Sorszam = tbSorszam.Text;
            erec_ObjektumTargyszavai.Updated.Sorszam = pageView.GetUpdatedByView(tbSorszam);

            // sorsz�m v�ltozott
            if (lbSorszam != null && lbSorszam.Text != tbSorszam.Text)
            {
                sorszamChanged = true;
            }
        }

        if (tbNote != null)
        {
            erec_ObjektumTargyszavai.Base.Note = tbNote.Text;
            erec_ObjektumTargyszavai.Base.Updated.Note = pageView.GetUpdatedByView(tbNote);
        }

        if (isNewRecord == true)
        {
            Label lbTargyszo = (Label)gridViewRow.FindControl("labelGridTargyszo");
            Label lbTargyszo_Id = (Label)gridViewRow.FindControl("labelGridTargyszo_Id");
            Label lbObj_Metaadatai_Id = (Label)gridViewRow.FindControl("labelGridObj_Metaadatai_Id");
            Label lbErvVege = (Label)gridViewRow.FindControl("labelGridErvVege");
            //Label lbForras = (Label)gridViewRow.FindControl("labelGridForras");   // �j rekordn�l csak Automatikus lehet

            if (lbTargyszo != null)
            {
                erec_ObjektumTargyszavai.Targyszo = lbTargyszo.Text;
                erec_ObjektumTargyszavai.Updated.Targyszo = true;
            }

            if (lbTargyszo_Id != null)
            {
                erec_ObjektumTargyszavai.Targyszo_Id = lbTargyszo_Id.Text;
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;
            }

            if (lbObj_Metaadatai_Id != null)
            {
                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = lbObj_Metaadatai_Id.Text;
                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;
            }

            if (lbErvVege != null)
            {
                erec_ObjektumTargyszavai.ErvVege = lbErvVege.Text;
                erec_ObjektumTargyszavai.Updated.ErvVege = true;
            }

            erec_ObjektumTargyszavai.ErvKezd = System.DateTime.Now.ToString();
            erec_ObjektumTargyszavai.Updated.ErvKezd = true;

            //if (lbForras != null)
            //{
            //    erec_ObjektumTargyszavai.Forras = lbForras.Text;
            //    erec_ObjektumTargyszavai.Updated.Forras = true;
            //}
        }

        return sorszamChanged;

    }

    private bool CheckGridViewRowValuesForValidity(GridViewRow gridViewRow)
    {
        bool isValid = true;

        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        //TextBox tbErtek = GetTextBoxFromDVC(dvc);
        Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
        Label lbRegExp = (Label)gridViewRow.FindControl("labelGridRegExp");
        // BLG_608
        Label lbOpcionalis = (Label)gridViewRow.FindControl("labelGridOpcionalis");
        // ha az �rt�k k�telez� �s a regexp nem stimmel, vagy nincs �rt�k, jelezz�k a hib�s �rt�ket
        if (dvc != null && lbTipus != null && lbTipus.Text == "1")
        {
            //if (tbErtek.Text == "")
            if (String.IsNullOrEmpty(dvc.Value) && (lbOpcionalis.Text=="0"))
            {
                isValid = false;
            }
            else if (lbRegExp != null && !String.IsNullOrEmpty(lbRegExp.Text))
            {
                Regex re = new Regex(lbRegExp.Text);
                //if (!re.IsMatch(tbErtek.Text))
                if (!re.IsMatch(dvc.Value))
                {
                    isValid = false;
                }
            }
        }

        return isValid;

    }

    private bool CheckGridViewChangedValuesForValidity()
    {
        foreach (GridViewRow gridViewRow in TargyszavakGridView.Rows)
        {
            if (gridViewRow.RowType == DataControlRowType.DataRow)
            {
                //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
                Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
                CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
                Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");

                if ((cb != null && cb.Checked == true)
                    || (dvc != null && lbErtek != null && dvc.Value != lbErtek.Text)
                    )
                {
                    // ha az �rt�k k�telez� �s a regexp nem stimmel, vagy nincs �rt�k, jelezz�k a hib�s �rt�ket
                    if (!CheckGridViewRowValuesForValidity(gridViewRow)) return false;
                }
            }
        }

        return true;
    }

    private void ShowChangesInGridView(bool bSetSaveRowCheckBox)
    {
        foreach (GridViewRow gridViewRow in TargyszavakGridView.Rows)
        {
            if (gridViewRow.RowType == DataControlRowType.DataRow)
            {
                CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
                //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
                Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
                //TextBox tbErtek = GetTextBoxFromDVC(dvc);
                WebControl tbErtek = GetWebControlFromDVC(dvc);
                TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
                TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
                Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
                Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
                Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

                //Label lbTipus = (Label)gridViewRow.FindControl("labelGridTipus");
                //Label lbRegExp = (Label)gridViewRow.FindControl("labelGridRegExp");

                bool isThereAChange = false;

                if (tbErtek != null)
                {
                    if (lbErtek != null)
                    {
                        //if (tbErtek.Text != lbErtek.Text)
                        if (dvc.Value != lbErtek.Text)
                        {
                            isThereAChange = true;
                        }

                        if (cb != null && cb.Checked && !CheckGridViewRowValuesForValidity(gridViewRow))
                        {
                            // ha az �rt�k k�telez� �s a regexp nem stimmel, vagy nincs �rt�k, jelezz�k a hib�s �rt�ket
                            tbErtek.CssClass = "GridViewTextBoxChangedIllegalValue";
                        }
                        //else if (tbErtek.Text != lbErtek.Text)
                        else if (dvc.Value != lbErtek.Text)
                        {
                            tbErtek.CssClass = "GridViewTextBoxChanged";
                        }
                        else
                        {

                            tbErtek.CssClass = "GridViewTextBox";
                        }
                    }

                }

                if (tbSorszam != null)
                {
                    if (lbSorszam != null)
                    {
                        if (tbSorszam.Text != lbSorszam.Text)
                        {
                            isThereAChange = true;
                            tbSorszam.CssClass = "GridViewNumberTextBoxShortChanged";
                        }
                        else
                        {
                            tbSorszam.CssClass = "GridViewNumberTextBoxShort";
                        }
                    }

                }

                if (tbNote != null)
                {
                    if (lbNote != null)
                    {
                        if (tbNote.Text != lbNote.Text)
                        {
                            isThereAChange = true;
                            tbNote.CssClass = "GridViewTextBoxChanged";
                        }
                        else
                        {
                            tbNote.CssClass = "GridViewTextBox";
                        }
                    }
                }

                if (bSetSaveRowCheckBox == true)
                {
                    if (cb != null)
                    {
                        cb.Checked = isThereAChange;
                    }
                }
            }
        }

    }


    /// <summary>
    /// Gridrow sor�ban a rejtett �rt�kek aktualiz�l�sa a l�that� �rt�kekb�l �s st�lus
    /// </summary>
    /// <param name="gridViewRow"></param>
    /// <returns></returns>
    private void UpdateGridViewRowHiddenValues(GridViewRow gridViewRow, String recordId)
    {
        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl tbErtek = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
        TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
        CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
        Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
        Label lbErtekText = (Label)gridViewRow.FindControl("labelGridErtekText");
        Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
        Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

        // st�lus helyre�ll�t�s
        if (cb != null)
        {
            cb.Checked = false;
        }

        if (tbErtek != null)
        {
            if (lbErtek != null)
            {
                //lbErtek.Text = tbErtek.Text;
                lbErtek.Text = tbErtek.Value;
            }
            if (lbErtekText != null)
            {
                lbErtekText.Text = tbErtek.Text;
            }
        }

        if (tbSorszam != null)
        {
            if (lbSorszam != null)
            {
                lbSorszam.Text = tbSorszam.Text;
            }
        }

        if (tbNote != null)
        {
            if (lbNote != null)
            {
                lbNote.Text = tbNote.Text;
            }
        }

        // st�lus vissza�ll�t�s
        RestoreGridViewRowStyle(gridViewRow);

        // rekord Id kit�lt�se, ha eddig �res volt
        Label lbRecordId = (Label)gridViewRow.FindControl("labelGridRecordId");
        if (lbRecordId != null && String.IsNullOrEmpty(lbRecordId.Text) && !String.IsNullOrEmpty(recordId))
        {
            lbRecordId.Text = recordId;
        }
    }

    /// <summary>
    /// Gridrow sor�nak vissza�ll�t�sa a rejtett �rt�kek alapj�n, �rt�k �s st�lus
    /// </summary>
    /// <param name="gridViewRow"></param>
    /// <returns></returns>
    private void RestoreGridViewRow(GridViewRow gridViewRow)
    {
        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        TextBox tbErtek = GetTextBoxFromDVC(dvc);
        TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
        TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
        CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");
        Label lbErtek = (Label)gridViewRow.FindControl("labelGridErtek");
        Label lbErtekText = (Label)gridViewRow.FindControl("labelGridErtekText");
        Label lbSorszam = (Label)gridViewRow.FindControl("labelGridSorszam");
        Label lbNote = (Label)gridViewRow.FindControl("labelGridNote");

        // st�lus helyre�ll�t�s
        if (cb != null)
        {
            cb.Checked = false;
        }

        //if (tbErtek != null)
        if (dvc != null)
        {
            if (lbErtek != null)
            {
                //tbErtek.Text = lbErtek.Text;
                dvc.Value = lbErtek.Text;
                string controlTypeDataSource = getControlTypeDataSource(gridViewRow);
                SetControlByControlSourceType(dvc, controlTypeDataSource, EErrorPanel1);
            }
        }

        if (tbSorszam != null)
        {
            if (lbSorszam != null)
            {
                tbSorszam.Text = lbSorszam.Text;
            }
        }

        if (tbNote != null)
        {
            if (lbNote != null)
            {
                tbNote.Text = lbNote.Text;
            }
        }

        // st�lus vissza�ll�t�s
        RestoreGridViewRowStyle(gridViewRow);

        SetDefaultValue(gridViewRow);
    }

    /// <summary>
    /// Gridrow sor st�lus�nak vissza�ll�t�sa 
    /// </summary>
    /// <param name="gridViewRow"></param>
    /// <returns></returns>
    private void RestoreGridViewRowStyle(GridViewRow gridViewRow)
    {
        //TextBox tbErtek = (TextBox)gridViewRow.FindControl("TextBoxGridErtek");
        Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)gridViewRow.FindControl("DynamicValueControl_TargyszoErtek");
        //TextBox tbErtek = GetTextBoxFromDVC(dvc);
        WebControl tbErtek = GetWebControlFromDVC(dvc);
        TextBox tbSorszam = (TextBox)gridViewRow.FindControl("TextBoxGridSorszam");
        TextBox tbNote = (TextBox)gridViewRow.FindControl("TextBoxGridNote");
        CheckBox cb = (CheckBox)gridViewRow.FindControl("cbRowChanged");


        // st�lus helyre�ll�t�s
        if (cb != null)
        {
            cb.Checked = false;
        }

        if (tbErtek != null)
        {
            tbErtek.CssClass = "GridViewTextBox";
        }

        if (tbSorszam != null)
        {
            tbSorszam.CssClass = "GridViewNumberTextBoxShort";
        }

        if (tbNote != null)
        {
            tbNote.CssClass = "GridViewTextBox";
        }
    }
    #endregion

    #region List
    protected void TargyszavakGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("TargyszavakGridView", ViewState, "Sorszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("TargyszavakGridView", ViewState);
        TargyszavakGridViewBind(sortExpression, sortDirection);
    }

    protected void TargyszavakGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(TargyszavakGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        EREC_ObjektumTargyszavaiService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();//(EREC_ObjektumTargyszavaiSearch)Search.GetSearchObject(Page, new EREC_ObjektumTargyszavaiSearch());
        search.WhereByManual = String.Format("(isnull(DefinicioTipus,'x') not in ('{0}','{1}'))", KodTarak.OBJMETADEFINICIO_TIPUS.B3, KodTarak.OBJMETADEFINICIO_TIPUS.B4);
        search.OrderBy = Search.GetOrderBy("TargyszavakGridView", ViewState, SortExpression, SortDirection);

        // TODO: k�v�ns�gra sz�r�s a t�nyleges hozz�rendel�sekre
        //search.WhereByManual = " Id is not null";

        result = service.GetAllMetaByDefinicioTipus(ExecParam, search, ParentId, ObjTip_Id_HiddenField.Value, null, null, false, false);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            //a 0. t�bla a saj�t adatokat tartalmazza, itt nincsenek �r�k�lt adatok

            // max. sorszam meghatarozasa
            MaxSorszam_HiddenField.Value = GetMaxSorszam(result, "Sorszam", 0).ToString();
        }

        ui.GridViewFill(TargyszavakGridView, result, EErrorPanel1, ErrorUpdatePanel);
        UI.SetTabHeaderRowCountText(result, TabHeader);

        if (Command == CommandName.View)
        {
            List<string> TextBoxNames = new List<string>();
            //TextBoxNames.Add("TextBoxGridErtek");
            TextBoxNames.Add("DynamicValueControl_TargyszoErtek");
            TextBoxNames.Add("TextBoxGridSorszam");
            TextBoxNames.Add("TextBoxGridNote");

            SetGridTextBoxesEnabledState(TextBoxNames, TargyszavakGridView, false);
            SetGridSaveControlsVisibleState(TargyszavakGridView, false);
        }


        ImageButton_GridSave.OnClientClick = RegisterCheckErtekJavaScriptForGridView(TargyszavakGridView);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id) || getForrasKod(TargyszavakGridView) == KodTarak.TARGYSZO_FORRAS.Automatikus)
        {
            TabHeader1.ViewOnClientClick = "";

            string forras = getForrasKod(TargyszavakGridView);

            //if (ParentForm == Constants.ParentForms.IraIrat && orokolt == true) // �r�k�lt
            //{
            //    TabHeader1.ModifyOnClientClick = jsNemModosithato;
            //}
            //else
            //{
            TabHeader1.ModifyOnClientClick = "";
            //}
            //TabHeader1.VersionOnClientClick = "";
            //TabHeader1.ElosztoListaOnClientClick = "";


        }

    }


    // visszat�r�si �rt�ke azt mutatja, v�ltozott-e a sorsz�m �s kell-e rendezni
    // sikertelen ment�sn�l false
    private bool SaveGridViewRow(GridViewRow currentRow, ref String recordId)
    {
        bool sorszamChanged = false;

        bool isNewRecord = String.IsNullOrEmpty(recordId);

        if (currentRow.RowType == DataControlRowType.DataRow)
        {

            if (CheckGridViewRowValuesForValidity(currentRow) == false)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorText_IllegalValue);
                ErrorUpdatePanel.Update();
                return false;    // figyelmeztet�s, elvileg sosem jelenik meg, mert m�r kliens oldalon elkapjuk....
            }

            //string recordId = TargyszavakGridView.DataKeys[currentRow.RowIndex].Value.ToString();
            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = null;
            Result result = new Result();

            if (!isNewRecord)
            {
                execParam.Record_Id = recordId;
                result = service.Get(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return false;
                }
                else
                {
                    erec_ObjektumTargyszavai = (EREC_ObjektumTargyszavai)result.Record;

                }
            }
            else
            {
                erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                // itt k�zzel kell megadni az adatokat
                erec_ObjektumTargyszavai.Obj_Id = ParentId;
                erec_ObjektumTargyszavai.Updated.Obj_Id = true;

                erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id_HiddenField.Value;
                erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
                erec_ObjektumTargyszavai.Updated.Forras = true;
            }
            if (erec_ObjektumTargyszavai != null)
            {
                sorszamChanged = FillBusinessObjectFromGridViewRow(currentRow, isNewRecord, ref erec_ObjektumTargyszavai);

                // ellen�rz�s, ne lehessen t�bbsz�r felvenni ua.t�rgysz�/�rt�k kombin�ci�t...
                // szervezeti: Targyszo_Id-re is ellen�rz�nk
                // egyedi: Targyszo-ra ellen�rz�nk, csak pontos egyez�s
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                search.Obj_Id.Value = erec_ObjektumTargyszavai.Obj_Id;
                search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                search.ObjTip_Id.Value = erec_ObjektumTargyszavai.ObjTip_Id;
                search.ObjTip_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                if (String.IsNullOrEmpty(erec_ObjektumTargyszavai.Ertek))
                {
                    search.Ertek.Operator = Contentum.eQuery.Query.Operators.isnull;
                }
                else
                {
                    search.Ertek.Value = erec_ObjektumTargyszavai.Ertek;
                    search.Ertek.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Listas)     // szervezeti
                {
                    search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id;
                    search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                else if (erec_ObjektumTargyszavai.Forras == KodTarak.TARGYSZO_FORRAS.Automatikus)     // auto
                {
                    search.Targyszo_Id.Value = erec_ObjektumTargyszavai.Targyszo_Id;
                    search.Targyszo_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    search.Obj_Metaadatai_Id.Value = erec_ObjektumTargyszavai.Obj_Metaadatai_Id;
                    search.Obj_Metaadatai_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                else // k�zi
                {
                    search.Targyszo.Value = erec_ObjektumTargyszavai.Targyszo.ToString();
                    search.Targyszo.Operator = Contentum.eQuery.Query.Operators.equals;
                }

                if (!isNewRecord)
                {
                    search.Id.Value = recordId;
                    search.Id.Operator = Contentum.eQuery.Query.Operators.notequals;
                }

                result = service.GetAll(execParam, search);

                if (String.IsNullOrEmpty(result.ErrorCode)) // GetAll
                {
                    // vizsg�lat, hogy ne lehessen azonos t�rgysz�-�rt�k p�r
                    if (result.Ds.Tables[0].Rows.Count == 0)
                    {
                        if (isNewRecord)
                        {
                            result = service.Insert(execParam, erec_ObjektumTargyszavai);
                        }
                        else
                        {
                            result = service.Update(execParam, erec_ObjektumTargyszavai);
                        }

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel.Update();
                            return false;
                        }
                        else
                        {
                            if (isNewRecord)
                            {
                                recordId = result.Uid;
                                setRecordIdLabel(currentRow, recordId);

                                List<string> CheckBoxNames = new List<string>();
                                CheckBoxNames.Add("check");
                                SetGridRowCheckBoxesVisibleState(CheckBoxNames, currentRow, true);

                                //SetDefinicioLabelByGridViewRow(currentRow);
                            }
                            UpdateGridViewRowHiddenValues(currentRow, recordId);
                            SetMetaSigns(currentRow);
                        }
                    }
                    else  // volt m�r ilyen felvitel
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_RecordNotUnique);
                        ErrorUpdatePanel.Update();
                        return false;
                    }

                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                    return false;
                }
            }

        }
        return sorszamChanged;
    }

    protected void TargyszavakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(TargyszavakGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

            // ha nyitva volt, a m�dos�t�/megtekint� panel elt�ntet�se
            EFormPanel1.Visible = false;
            // ha nem View-m�d, a grid ment�s gombok megjelen�t�se
            Command = Page.Request.QueryString.Get(CommandName.Command);
            if (Command == CommandName.New || Command == CommandName.Modify)
            {
                EFormPanelSaveGrid.Visible = true;
            }

            RefreshOnClientClicksByGridViewSelectedRow(getSelectedRecordIdLabel(TargyszavakGridView));
        }
        #region egy sor ment�se
        else if (e.CommandName == "SaveRow")
        {
            //string recordId = e.CommandArgument.ToString();

            //GridViewRow currentRow = getSelectedRowByID(recordId, TargyszavakGridView); //(GridViewRow)((ImageButton)sender).NamingContainer;
            GridViewRow currentRow = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;

            if (currentRow == null) return;

            if (currentRow.RowType == DataControlRowType.DataRow)
            {
                string recordId = getRecordIdLabel(currentRow);
                bool sorszamChanged = false;

                sorszamChanged = SaveGridViewRow(currentRow, ref recordId);

                // sorsz�m v�ltozott, kijel�lj�k a sort az �j hely�n
                if (sorszamChanged == true)
                {
                    TargyszavakGridViewBind();
                    selectRowByID(recordId, TargyszavakGridView, "check");
                }

                // ha nyitva volt, a m�dos�t�/megtekint� panel elt�ntet�se
                EFormPanel1.Visible = false;
                // ha nem View-m�d, a grid ment�s gombok megjelen�t�se
                Command = Page.Request.QueryString.Get(CommandName.Command);
                if (Command == CommandName.New || Command == CommandName.Modify)
                {
                    EFormPanelSaveGrid.Visible = true;
                }

            }
        }
        #endregion egy sor ment�se
    }

    // t�rlend� rekordok azonos�t�inek �sszegy�jt�se, nem a DatKEy haszn�lat�val, hanem labelr�l
    // mivel sorment�s ut�n nincs felt�tlen�l GridViewBind, az Id-k nem ker�lnek be a DataKeybe
    private List<string> GetGridViewSelectedRowsByLabel(GridView gridView)
    {
        if (gridView == null)
        {
            return null;
        }
        // t�r�lhet� elemek list�ja
        List<string> deletableItemsList = new List<string>();

        foreach (GridViewRow row in gridView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cb = (CheckBox)row.FindControl("check");
                if (cb != null && cb.Visible == true && cb.Checked == true)
                {
                    string recordId = getRecordIdLabel(row);
                    if (!String.IsNullOrEmpty(recordId))
                    {
                        // felvessz�k a list�ba az id-t:
                        deletableItemsList.Add(recordId);
                    }
                }
            }
        }
        return deletableItemsList;
    }

    /// <summary>
    /// T�rli a TargyszavakGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedTargyszavak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "TargyszavakInvalidate"))
        {
            //List<string> deletableItemsList = ui.GetGridViewSelectedRows(TargyszavakGridView, EErrorPanel1, ErrorUpdatePanel);
            List<string> deletableItemsList = GetGridViewSelectedRowsByLabel(TargyszavakGridView);

            if (deletableItemsList != null)
            {

                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

                List<ExecParam> execParams = new List<ExecParam>();
                for (int i = 0; i < deletableItemsList.Count; i++)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
                if (execParams.Count > 0)
                {
                    Result result = service.MultiInvalidate(execParams.ToArray());
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel.Update();
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            }
        }

    }

    protected void TargyszavakGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {

            if (bShowInheritedMetaData == true)
            {
                // az �r�k�lt metaadatok �r�sv�dettek
                CheckBox cbOrokolt = (CheckBox)e.Row.FindControl("cbGridOrokolt");
                if (cbOrokolt != null)
                {
                    if (cbOrokolt.Checked == true) // �r�k�lt
                    {
                        List<string> TextBoxNames = new List<string>();
                        //TextBoxNames.Add("TextBoxGridErtek");
                        TextBoxNames.Add("DynamicValueControl_TargyszoErtek");
                        TextBoxNames.Add("TextBoxGridSorszam");
                        TextBoxNames.Add("TextBoxGridNote");

                        SetGridRowTextBoxesEnabledState(TextBoxNames, e.Row, false);
                        SetGridRowSaveControlsVisibleState(e.Row, false);

                        List<string> CheckBoxNames = new List<string>();
                        CheckBoxNames.Add("check");
                        SetGridRowCheckBoxesVisibleState(CheckBoxNames, e.Row, false);
                    }
                }
            }
        }

        SetMetaSigns(e.Row);
        //SetDefinicioLabelByGridViewRow(e.Row);  // ha nincs rekord Id, l�that� a "(defin�ci�)" label, egy�bk�nt nem 

        CheckBox cb = (CheckBox)e.Row.FindControl("cbRowChanged");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", RegisterShowChangesJavaScriptForGridViewRow(e.Row));
        }

        // textboxokb�l kil�pve a m�dos�t�sok �s hib�k megjelen�t�se
        SetTextBoxChangedJavaScriptForGridViewRow(e.Row);

        // a sor ment�sre kattintva ellen�rz�s, ahol kell
        SetErtekValidatorScriptIfRequired(e.Row);

        if (String.IsNullOrEmpty(getRecordIdLabel(e.Row)))
        {
            List<string> CheckBoxNames = new List<string>();
            CheckBoxNames.Add("check");
            SetGridRowCheckBoxesVisibleState(CheckBoxNames, e.Row, false);
            if (Command == CommandName.Modify)
            {
                SetDefaultValue(e.Row);
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)e.Row.FindControl("DynamicValueControl_TargyszoErtek");
            string controlTypeDataSource = getControlTypeDataSource(e.Row);
            SetControlByControlSourceType(dvc, controlTypeDataSource, EErrorPanel1);

            Label lbErtekText = (Label)e.Row.FindControl("labelGridErtekText");
            if (lbErtekText != null && dvc != null)
            {
                lbErtekText.Text = dvc.Text;
            }
        }
    }

    protected void TargyszavakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TargyszavakGridViewBind(e.SortExpression, UI.GetSortToGridView("TargyszavakGridView", ViewState, e.SortExpression));
    }

    protected void cbRowChanged_CheckChanged(object sender, EventArgs e)
    {
        CheckBox cb = (CheckBox)sender;

        if (cb != null)
        {
            GridViewRow currentRow = (GridViewRow)cb.NamingContainer;

            if (cb.Checked == false)
            {
                RestoreGridViewRow(currentRow);
            }
        }


    }

    protected void GridElement_TextChanged(object sender, EventArgs e)
    {
        TextBox tb = (TextBox)sender;

        if (tb.ID == "TextBoxGridSorszam")
        {
            tb.CssClass = "GridViewNumberTextBoxShortChanged";
        }
        else
        {
            tb.CssClass = "GridViewTextBoxChanged";
        }

        GridViewRow currentRow = (GridViewRow)tb.NamingContainer;

        CheckBox cb = (CheckBox)currentRow.FindControl("cbRowChanged");
        if (cb != null)
        {
            cb.Checked = true;
        }

    }

    protected void GridShowChanges_Click(object sender, ImageClickEventArgs e)
    {
        //if (CheckGridViewChangedValuesForValidity() == false)
        //{
        //    ImageButton_GridSave.OnClientClick += RegisterIllegalValuesJavaScript();
        //}
        //else
        //{
        //    ImageButton_GridSave.OnClientClick = "";
        //}

        ShowChangesInGridView(true);
    }

    protected void GridRestoreChanges_Click(object sender, ImageClickEventArgs e)
    {
        //ImageButton_GridSave.OnClientClick = "";
        foreach (GridViewRow gridViewRow in TargyszavakGridView.Rows)
        {
            if (gridViewRow.RowType == DataControlRowType.DataRow)
            {
                RestoreGridViewRow(gridViewRow);
            }
        }
    }

    protected void GridSave_Click(object sender, ImageClickEventArgs e)
    {
        bool bSorszamChanged = false;

        string selectedRecordId = getSelectedRecordIdLabel(TargyszavakGridView);

        foreach (GridViewRow currentRow in TargyszavakGridView.Rows)
        {
            if (currentRow.RowType == DataControlRowType.DataRow)
            {
                if (CheckGridViewRowValuesForValidity(currentRow) == true)
                {
                    CheckBox cb = (CheckBox)currentRow.FindControl("cbRowChanged");
                    if (cb != null && cb.Checked == true)
                    {
                        string recordId = getRecordIdLabel(currentRow);
                        bSorszamChanged = (bSorszamChanged || SaveGridViewRow(currentRow, ref recordId));
                    }
                }

            }
        }

        if (bSorszamChanged == true)
        {
            TargyszavakGridViewBind();
            if (!String.IsNullOrEmpty(selectedRecordId))
            {
                selectRowByID(selectedRecordId, TargyszavakGridView, "check");
            }
        }

    }

    #endregion List

    #region OrokoltList
    protected void OrokoltTargyszavakGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("OrokoltTargyszavakGridView", ViewState, "Sorszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("OrokoltTargyszavakGridView", ViewState);
        OrokoltTargyszavakGridViewBind(sortExpression, sortDirection);
    }

    protected void OrokoltTargyszavakGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (bShowInheritedMetaData == true)
        {
            UI.ClearGridViewRowSelection(OrokoltTargyszavakGridView);
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = new Result();

            if (ParentForm == Constants.ParentForms.IraIrat)
            {
                EREC_ObjektumTargyszavaiService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();//(EREC_ObjektumTargyszavaiSearch)Search.GetSearchObject(Page, new EREC_ObjektumTargyszavaiSearch());

                string Irat_Id = ParentId;

                search.OrderBy = Search.GetOrderBy("OrokoltTargyszavakGridView", ViewState, SortExpression, SortDirection);

                // TODO: k�v�ns�gra sz�r�s a t�nyleges hozz�rendel�sekre
                search.WhereByManual = " Id is not null";

                result = service.GetAllWithExtensionIratEsUgyirat(ExecParam, Irat_Id, search);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    int cntTables = result.Ds.Tables.Count;
                    DataTable table = new DataTable();
                    //a 0. t�bla a saj�t adatokat tartalmazza, a t�bbi �r�k�lt
                    for (int i = 1; i < cntTables; i++)
                    {
                        table.Merge(result.Ds.Tables[i].Copy());
                    }

                    result.Ds.Tables.Clear();
                    result.Ds.Tables.Add(table.Copy());
                }
            }

            ui.GridViewFill(OrokoltTargyszavakGridView, result, EErrorPanel1, ErrorUpdatePanel);
        }

    }

    protected void OrokoltTargyszavakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(OrokoltTargyszavakGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

            // ha nyitva volt, a m�dos�t�/megtekint� panel elt�ntet�se
            EFormPanel1.Visible = false;
            // ha nem View-m�d, a grid ment�s gombok megjelen�t�se
            Command = Page.Request.QueryString.Get(CommandName.Command);
            if (Command == CommandName.New || Command == CommandName.Modify)
            {
                EFormPanelSaveGrid.Visible = true;
            }

            RefreshOnClientClicksByGridViewSelectedRow(getSelectedRecordIdLabel(OrokoltTargyszavakGridView));
        }
    }

    protected void OrokoltTargyszavakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrokoltTargyszavakGridViewBind(e.SortExpression, UI.GetSortToGridView("OrokoltTargyszavakGridView", ViewState, e.SortExpression));
    }

    #endregion OrokoltList

    #region Panelfunkci�k

    protected void TargySzavakTextBox_TextChanged(object sender, EventArgs e)
    {
        if (TargySzavakTextBoxSzabvanyos.Visible)
        {
            if (String.IsNullOrEmpty(TargySzavakTextBoxSzabvanyos.Id_HiddenField))
            {
                rbEgyedi.Checked = true;
                rbSzabvanyos.Checked = false;
                labelErtekStar.Visible = false;
                //TextBoxErtek.Text = String.Empty;
                TextBoxErtek.ControlTypeSource = DefaultControlTypeSource_TextBoxErtek;
                TextBoxErtek.Value = String.Empty;
                SetControlByControlSourceType(TextBoxErtek, null, EErrorPanel1);
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");

                //RegExValidator.Enabled = false;
                labelRegex.Text = "";
                TextBoxErtek.ToolTip = "";
            }
            else
            {
                rbEgyedi.Checked = false;
                rbSzabvanyos.Checked = true;
                SetTextBoxReadOnly(TextBoxErtek, (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? true : false), "mrUrlapInput");
                labelRegex.Text = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? "" : TargySzavakTextBoxSzabvanyos.RegExp);
                //TextBoxErtek.Text = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? "" : TargySzavakTextBoxSzabvanyos.AlapertelmezettErtek);
                TextBoxErtek.Value = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? "" : TargySzavakTextBoxSzabvanyos.AlapertelmezettErtek);
                TextBoxErtek.ControlTypeSource = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? DefaultControlTypeSource_TextBoxErtek : TargySzavakTextBoxSzabvanyos.ControlTypeSource);
                SetControlByControlSourceType(TextBoxErtek, TargySzavakTextBoxSzabvanyos.ControlTypeDataSource, EErrorPanel1);
                labelErtekStar.Visible = (TargySzavakTextBoxSzabvanyos.Tipus == "1" ? true : false);
                if (TargySzavakTextBoxSzabvanyos.Tipus == "0")
                {
                    TextBoxErtek.ToolTip = "";
                }
                else
                {
                    TextBoxErtek.ToolTip = GetErtekToolTip(TargySzavakTextBoxSzabvanyos.ToolTip, TargySzavakTextBoxSzabvanyos.RegExp);
                }

                //string RegExp = TargySzavakTextBoxSzabvanyos.RegExp;
                //if (!String.IsNullOrEmpty(RegExp))
                //{
                //    RegExValidator.ValidationExpression = RegExp;
                //    RegExValidator.ErrorMessage = String.Format(Resources.Form.RegularExpressionValidationMessage, "(RegExp: " + RegExp + ")");
                //    RegExValidator.Enabled = true;
                //}
                //else
                //{
                //    RegExValidator.Enabled = false;
                //}
            }

            if (TargySzavakTextBoxSzabvanyos.Tipus == "1")
            {
                TabFooter1.ImageButton_Save.OnClientClick = RegisterCheckErtekJavaScript(TextBoxErtek, TargySzavakTextBoxSzabvanyos.RegExp);
            }
            else
            {
                TabFooter1.ImageButton_Save.OnClientClick = "";
            }

        }

    }

    protected void rgbForras_CheckedChanged(object sender, EventArgs e)
    {
        if (rbSzabvanyos.Checked == true)
        {
            TargySzavakTextBoxSzabvanyos.Visible = true;
            //TargySzavakTextBoxSzabvanyos.Validate = true;
            SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, false, "mrUrlapInput");

            requiredTextBoxTargyszo.Visible = false;
            requiredTextBoxTargyszo.Validate = false;
            // a TextBoxErtek kezel�se a TextChanged esem�nyhez k�t�dik

        }
        else if (rbEgyedi.Checked == true)
        {
            TargySzavakTextBoxSzabvanyos.Visible = false;
            TargySzavakTextBoxSzabvanyos.Validate = false;

            requiredTextBoxTargyszo.Visible = true;

            SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
            SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, false, "mrUrlapInput");
            //requiredTextBoxTargyszo.Validate = true;

            labelErtekStar.Visible = false;
            //�rt�k k�telez�s�g figyelmeztet�s t�rl�se
            TabFooter1.ImageButton_Save.OnClientClick = "";

        }
        ClearForm();
    }


    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls(string targyszoForras)
    {

        // show "view/modify" controls
        switch (targyszoForras)
        {
            case KodTarak.TARGYSZO_FORRAS.Kezi:
                requiredTextBoxTargyszo.Visible = true;
                requiredTextBoxTargyszo.Validate = false;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, true, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.Visible = false;
                TargySzavakTextBoxSzabvanyos.Validate = false;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = true;
                //TargySzavakTextBoxSzabvanyos.Enabled = false;
                break;
            case KodTarak.TARGYSZO_FORRAS.Listas:
                requiredTextBoxTargyszo.Visible = false;
                requiredTextBoxTargyszo.Validate = false;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, true, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.Visible = true;
                TargySzavakTextBoxSzabvanyos.Validate = false;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = true;
                //TargySzavakTextBoxSzabvanyos.Enabled = false;
                break;
            case KodTarak.TARGYSZO_FORRAS.Automatikus:
                requiredTextBoxTargyszo.Visible = false;
                requiredTextBoxTargyszo.Validate = false;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, true, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.Visible = true;
                TargySzavakTextBoxSzabvanyos.Validate = false;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = true;
                //TargySzavakTextBoxSzabvanyos.Enabled = false;
                break;
        }

        // show "view/modify" controls
        labelRequiredTextBoxStar.Visible = true;
        labelForras.Visible = true;
        labelForrasCaption.Visible = true;
        labelTargySzavak.Visible = true;

        // hide "new" controls
        rbEgyedi.Visible = false;
        rbSzabvanyos.Visible = false;

        labelErtekStar.Visible = false;

        SetTextBoxReadOnly(TextBoxSorszam, true, "NumberTextBoxShort");
        nupeSorszam.Enabled = false;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        SetTextBoxReadOnly(textNote, true, "mrUrlapInput");

        labelTargySzavak.CssClass = "mrUrlapInputWaterMarked";
        labelErtek.CssClass = "mrUrlapInputWaterMarked";
        labelSorszam.CssClass = "mrUrlapInputWaterMarked";
        labelForrasCaption.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelForras.CssClass = "mrUrlapInputWaterMarked";

        EFormPanelSaveGrid.Visible = false;
    }


    /// <summary>
    /// Modify m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>

    private void SetModifyControls(string targyszoForras)
    {
        // show "view/modify" controls
        switch (targyszoForras)
        {
            case KodTarak.TARGYSZO_FORRAS.Kezi:
                requiredTextBoxTargyszo.Visible = true;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, false, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput"); // k�zi t�rgysz�hoz nem vihet� fel �rt�k
                //requiredTextBoxErtek.Validate = false;
                TargySzavakTextBoxSzabvanyos.Visible = false;
                TargySzavakTextBoxSzabvanyos.Validate = false;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, false, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = false;
                break;
            case KodTarak.TARGYSZO_FORRAS.Listas:
                requiredTextBoxTargyszo.Visible = false;
                requiredTextBoxTargyszo.Validate = false;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, true, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, false, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.Visible = true;
                //TargySzavakTextBoxSzabvanyos.Validate = true;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = true;
                break;
            case KodTarak.TARGYSZO_FORRAS.Automatikus:
                requiredTextBoxTargyszo.Visible = false;
                requiredTextBoxTargyszo.Validate = false;
                SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, true, "mrUrlapInput");
                TextBoxErtek.Visible = true;
                SetTextBoxReadOnly(TextBoxErtek, false, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.Visible = true;
                //TargySzavakTextBoxSzabvanyos.Validate = true;
                SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, true, "mrUrlapInput");
                TargySzavakTextBoxSzabvanyos.ViewMode = true;
                break;
        }

        labelRequiredTextBoxStar.Visible = true;
        labelForras.Visible = true;
        labelForrasCaption.Visible = true;
        labelTargySzavak.Visible = true;

        // hide "new" controls
        rbEgyedi.Visible = false;
        rbSzabvanyos.Visible = false;

        labelErtekStar.Visible = false;

        SetTextBoxReadOnly(TextBoxSorszam, false, "NumberTextBoxShort");
        nupeSorszam.Enabled = true;
        ErvenyessegCalendarControl1.Enable_ErvKezd = true;
        ErvenyessegCalendarControl1.Enable_ErvVege = true;
        SetTextBoxReadOnly(textNote, false, "mrUrlapInput");

        labelTargySzavak.CssClass = "";
        labelErtek.CssClass = "";
        labelSorszam.CssClass = "";
        labelForrasCaption.CssClass = "";
        labelErvenyesseg.CssClass = "";
        labelNote.CssClass = "";
        labelForras.CssClass = "";

        nupeSorszam.Maximum = GetNupeMaximum();

        EFormPanelSaveGrid.Visible = true;
    }

    /// <summary>
    /// New m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetNewControls()
    {
        // hide "view/modify" controls        
        labelForras.Visible = false;
        labelForrasCaption.Visible = false;
        labelTargySzavak.Visible = true;

        // show "new" controls
        labelRequiredTextBoxStar.Visible = true;
        requiredTextBoxTargyszo.Visible = false;
        SetTextBoxReadOnly(requiredTextBoxTargyszo.TextBox, false, "mrUrlapInput");
        TargySzavakTextBoxSzabvanyos.Visible = true;
        SetTextBoxReadOnly(TargySzavakTextBoxSzabvanyos.TextBox, false, "mrUrlapInput");
        TargySzavakTextBoxSzabvanyos.ViewMode = false;
        rbEgyedi.Visible = true;
        rbSzabvanyos.Visible = true;

        //TargySzavakTextBoxSzabvanyos.Validate = rbSzabvanyos.Checked;
        //requiredTextBoxTargyszo.Validate = rbEgyedi.Checked;

        //SetTextBoxReadOnly(requiredTextBoxErtek.TextBox, true, "mrUrlapInput");
        SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");

        SetTextBoxReadOnly(TextBoxSorszam, false, "mrUrlapInput");
        nupeSorszam.Enabled = true;
        ErvenyessegCalendarControl1.Enable_ErvKezd = true;
        ErvenyessegCalendarControl1.Enable_ErvVege = true;
        SetTextBoxReadOnly(textNote, false, "mrUrlapInput");

        labelTargySzavak.CssClass = "mrUrlapInput";
        labelErtek.CssClass = "mrUrlapInput";
        labelSorszam.CssClass = "mrUrlapInput";
        labelErvenyesseg.CssClass = "mrUrlapInput";
        labelNote.CssClass = "mrUrlapInput";

        nupeSorszam.Maximum = GetNupeMaximum();

        EFormPanelSaveGrid.Visible = true;
    }
    #endregion Panelfunkci�k
}
