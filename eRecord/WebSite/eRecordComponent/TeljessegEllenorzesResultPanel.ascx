﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TeljessegEllenorzesResultPanel.ascx.cs" Inherits="eRecordComponent_TeljessegEllenorzesResultPanel" %>

<%@ Register Src="UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc1" %>

<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Panel ID="MainWrapper" runat="server" Style="display: none;background-color:White;border: solid 2px gray;" Width="800">
            <asp:Panel ID="MainPanel" runat="server" Visible="false">
                <asp:Button ID="HiddenButton" runat="server" Style="display: none" />
                <h3 class="ErrorText" id="HeaderRow" style="margin-top:10px; text-align:center; cursor:move;background-color:#E1E1E1">A teljességellenőrzés sikertelen!</h3>
                <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                        </eUI:eErrorPanel>
                    </ContentTemplate>
                 </asp:UpdatePanel>
                <asp:Panel ID="ResultPanel" runat="server">
                    <table cellpadding="0" cellspacing="0" width="97%">
                    <tr style="padding-bottom:5px;">
                        <td>
                            <uc1:UgyiratMasterAdatok ID="UgyiratMasterAdatok1" runat="server"></uc1:UgyiratMasterAdatok>
                        </td>
                    </tr>
                    </table>
                    <asp:Panel ID="ListPanel" runat="server" ScrollBars="Auto" Height="500px">
                    <table cellpadding="0" cellspacing="0" width="97%">
                        <tr>
                            <td>
                            
                                <asp:Panel ID="EFormPanelUgyiratok" runat="server" Visible="false" style="padding-bottom:5px;">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <asp:GridView ID="GridViewUgyiratok" runat="server" CssClass="GridViewLovListStyle"
                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                    PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                    DataKeyNames="Id" OnRowDataBound="GridViewUgyiratok_RowDataBound">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        </asp:BoundField>
                                                        <asp:HyperLinkField DataTextField="Foszam_Merge" HeaderText="Iktatószám"
                                                        DataNavigateUrlFields="Id" DataNavigateUrlFormatString="~/UgyUgyiratokList.aspx?Id={0}">
                                                            <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="Targy" HeaderText="Tárgy">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <%--<asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>--%>
                                                        <asp:TemplateField HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <ItemTemplate>
                                                                <%-- skontróban esetén (07) skontró vége megjelenítése --%>
                                                                <asp:Label ID="labelAllapotNev" runat="server" Text='<%#                                        
                                                            string.Concat(                                          
                                                              Eval("Allapot_Nev")                                     
                                                             , (Eval("Foszam_Merge_Szulo") as string) != null ? 
                                                                    (((Eval("Allapot") as string) != "60" && (Eval("TovabbitasAlattAllapot") as string) != "60") 
                                                                    ? "<img src=\"images/hu/ikon/szerelt_piros.gif\" height=\"20px\" width=\"25px\" alt=\"Előkészített szerelés\" />&nbsp;" : "")
                                                                    + "<a href=\"UgyUgyiratokList.aspx?Id="+ Eval("UgyUgyirat_Id_Szulo") as string + "\" style=\"text-decoration:underline\"> (" 
                                                                    + Eval("Foszam_Merge_Szulo") as string +")<a />" : "") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Ügyirat helye">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Hiba oka">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle ErrorText" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelErrorCode" runat="server" Text='<%#Eval("ErrorCode")%>'>></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            
                            
                                <asp:Panel ID="EFormPanelIratok" runat="server" Visible="false" style="padding-bottom:5px;">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <asp:GridView ID="GridViewIratok" runat="server" CssClass="GridViewLovListStyle"
                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                    PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                    DataKeyNames="Id" OnRowDataBound="GridViewIratok_RowDataBound">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        </asp:BoundField>
                                                        <asp:HyperLinkField DataTextField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="IktatoSzam_Merge"
                                                        DataNavigateUrlFields="Id" DataNavigateUrlFormatString="~/IraIratokList.aspx?Id={0}">
                                                            <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="Targy1" HeaderText="Tárgy">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FelhasznaloCsoport_Id_Iktato_Nev" HeaderText="Iktató">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Hiba oka">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle ErrorText" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelErrorCode" runat="server" Text='<%#Eval("ErrorCode")%>'>></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="EFormPanelPeldanyok" runat="server" Visible="false">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <asp:GridView ID="GridViewPeldanyok" runat="server" CssClass="GridViewLovListStyle"
                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                    PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                    DataKeyNames="Id" OnRowDataBound="GridViewPeldanyok_RowDataBound">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                        </asp:BoundField>
                                                        <asp:HyperLinkField DataTextField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="IktatoSzam_Merge"
                                                        DataNavigateUrlFields="Id" DataNavigateUrlFormatString="~/PldIratPeldanyokList.aspx?Id={0}">
                                                            <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok_Targy">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="NevSTR_Cimzett">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Példány helye"
                                                            SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Hiba oka">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle ErrorText" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="labelErrorCode" runat="server" Text='<%#Eval("ErrorCode")%>'>></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </asp:Panel>
                <div style="text-align:center;padding-top:5px;">
                <asp:ImageButton TabIndex="4" ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mdeMainPanel" runat="server" TargetControlID="HiddenButton" RepositionMode="RepositionOnWindowResize"
                PopupControlID="MainWrapper" BackgroundCssClass="emp_modalBackground" CancelControlID="ImageClose" PopupDragHandleControlID="HeaderRow"/>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

