﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eUIControls;

public partial class eRecordComponent_TeljessegEllenorzesResultPanel : System.Web.UI.UserControl
{
    public static bool DisplayWarningMessage(Page page,EREC_UgyUgyiratok erec_ugyugyirat, eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam xpm = UI.SetExecParamDefault(page, new ExecParam());
        Result res = svc.TeljessegEllenorzes(xpm, null, erec_ugyugyirat, false);
        if (res.IsError)
        {
            if (res.ErrorCode == "58000")
            {
                res.ErrorType = Constants.ErrorType.Warning;
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, res, "Figyelmeztetés!");
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, res, "Hiba a teljesség ellenőrzés során!");
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }

            return false;
        }
        else if (res.ErrorDetail != null && !String.IsNullOrEmpty(res.ErrorDetail.Message))
        {
            // BLG#2366: A sikeresség ellenére lehet hogy van egyéb megjegyzés is, amit meg kell jeleníteni, ezeket a webservice az ErrorDetails-be teszi:
            res.ErrorType = Constants.ErrorType.Warning;
            ResultError.DisplayWarningOnErrorPanel(errorPanel, "Figyelmeztetés!", res.ErrorDetail.Message);
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }

            // A teljességellenőrzés sikeres volt ebben az esetben, csak figyelmeztetést adunk ki:
            return true;
        }

        return true;
    }

    public bool Ellenorzes(string ugyiratId)
    {
        return Ellenorzes(ugyiratId, true);
    }

    public bool Ellenorzes(string ugyiratId, bool modalPopup)
    {
        string warningMsg;
        return Ellenorzes(ugyiratId, modalPopup, false, out warningMsg);
    }
    
    /// <summary>
    /// Teljesség-ellenőrzés hívása
    /// </summary>
    /// <param name="ugyiratId"></param>
    /// <param name="modalPopup"></param>
    /// <param name="showWarningMsg"></param>
    /// <returns></returns>
    public bool Ellenorzes(string ugyiratId, bool modalPopup, bool showWarningMsg, out string warningMsg)
    {
        EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        warningMsg = String.Empty;
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = svc.TeljessegEllenorzes(xpm, ugyiratId, null, true);
        if (res.IsError)
        {
            ShowErrorPanel(res, modalPopup);
            return false;
        }
        else
        {
            // BLG#2366: A sikeresség ellenére lehet hogy van egyéb megjegyzés is, amit meg kell jeleníteni, ezeket a webservice az ErrorDetails-be teszi:            
            if (res.ErrorDetail != null && showWarningMsg)
            {
                warningMsg = res.ErrorDetail.Message;
            }

            if (modalPopup 
                || (showWarningMsg && !String.IsNullOrEmpty(warningMsg)))
            {                
                ShowSuccessMessage(warningMsg);
            }

            return true;
        }
    }

    private void ShowSuccessMessage(string detailMessage)
    {
        string js;

        // BLG#2366: A sikeresség ellenére lehet hogy van egyéb megjegyzés is, amit meg kell jeleníteni, ezeket a webservice az ErrorDetails-be teszi:
        if (!String.IsNullOrEmpty(detailMessage))
        {
            js = "alert('" + Resources.Form.UI_UgyiratTeljes + "\\n\\nMegjegyzés:\\n" + detailMessage + "');";
        }
        else
        {
            js = "alert('" + Resources.Form.UI_UgyiratTeljes + "');";
        }
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TeljessegResultAlert", js, true);
    }

    private bool showErrorPanel = false;

    private void ShowErrorPanel(Result res)
    {
        ShowErrorPanel(res, true);
    }

    private void ShowErrorPanel(Result res, bool modalPopup)
    {
        SetDefaultState();

        if (res.ErrorCode == "58000")
        {
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;
            UgyiratMasterAdatok1.SetUgyiratMasterAdatokByBusinessObject(ugyirat, EErrorPanel1,ErrorUpdatePanel);

            if(res.Ds.Tables.Contains(Constants.TableNames.EREC_PldIratPeldanyok))
            {
                DataTable tablePeldanyok = res.Ds.Tables[Constants.TableNames.EREC_PldIratPeldanyok];
                DataBindPeldanyok(tablePeldanyok);
            }

            if (res.Ds.Tables.Contains(Constants.TableNames.EREC_IraIratok))
            {
                DataTable tableIratok= res.Ds.Tables[Constants.TableNames.EREC_IraIratok];
                DataBindIratok(tableIratok);
            }

            if (res.Ds.Tables.Contains(Constants.TableNames.EREC_UgyUgyiratok))
            {
                DataTable tableUgyirat = res.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratok];
                DataBindUgyiratok(tableUgyirat);
            }
        }
        else
        {
            ResultPanel.Visible = false;
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            ErrorUpdatePanel.Update();
            showErrorPanel = true;
        }

        MainPanel.Visible = true;
        if (modalPopup)
        {
            mdeMainPanel.Show();
        }
        else
        {
            // Nem modalpopupos megjelenítés:
            mdeMainPanel.Enabled = false;
            MainWrapper.Style[HtmlTextWriterStyle.Display] = "block";
            ImageClose.OnClientClick = "window.close(); return false;";
        }
        upMain.Update();

        SetupUserInterface();
    }

    private void DataBindUgyiratok(DataTable tableUgyirat)
    {
        EFormPanelUgyiratok.Visible = true;
        GridViewUgyiratok.DataSource = tableUgyirat;
        GridViewUgyiratok.DataBind();
    }

    private void DataBindIratok(DataTable tableIratok)
    {
        EFormPanelIratok.Visible = true;
        GridViewIratok.DataSource = tableIratok;
        GridViewIratok.DataBind();
    }

    protected void GridViewIratok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetErrorMessageFromErrorCode(e.Row);
    }

    private void DataBindPeldanyok(DataTable tablePeldanyok)
    {
        EFormPanelPeldanyok.Visible = true;
        GridViewPeldanyok.DataSource = tablePeldanyok;
        GridViewPeldanyok.DataBind();
    }

    protected void GridViewPeldanyok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetErrorMessageFromErrorCode(e.Row);
    }

    protected void GridViewUgyiratok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetErrorMessageFromErrorCode(e.Row);
    }    


    protected void SetErrorMessageFromErrorCode(GridViewRow gridViewRow)
    {
        if (gridViewRow.RowType == DataControlRowType.DataRow)
        {
            Label labelErrorCode = (Label)gridViewRow.FindControl("labelErrorCode");
            if (labelErrorCode != null)
            {
                int errorCode;
                if (Int32.TryParse(labelErrorCode.Text, out errorCode))
                {
                    string errorMessage = ResultError.GetErrorMessageByErrorCode(errorCode);
                    labelErrorCode.Text = errorMessage;
                }
            }
        }
    }

    private void SetDefaultState()
    {
        MainPanel.Visible = false;
        EFormPanelPeldanyok.Visible = false;
        EFormPanelIratok.Visible = false;
        ResultPanel.Visible = true;
    }

    private void SetupUserInterface()
    {
        int RowNumber = 0;

        if (EFormPanelIratok.Visible)
        {
            RowNumber += GridViewIratok.Rows.Count;
        }

        if (EFormPanelPeldanyok.Visible)
        {
            RowNumber += GridViewPeldanyok.Rows.Count;
        }

        if (RowNumber > 5)
        {
            ListPanel.Height = Unit.Pixel(300);
        }
        else
        {
            ListPanel.Height = Unit.Empty;
        }

        if (EErrorPanel1.Visible && !showErrorPanel)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
