﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TerjedelemPanel.ascx.cs" Inherits="eRecordComponent_TerjedelemPanel" %>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc" %>

<asp:UpdatePanel ID="up" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanelTerjedelem" runat="server">
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeTerjedelem" runat="server" TargetControlID="panelTerjedelem"
                CollapsedSize="0" Collapsed="true" ExpandControlID="panelCpeHeader" CollapseControlID="panelCpeHeader"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
                ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="ibtnTerjedelemCpe"
                ExpandedSize="0" ExpandedText="Terjedelem elrejt" CollapsedText="Terjedelem mutat"
                TextLabelID="labelTerjedelemCpeText">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel runat="server" ID="panelCpeHeaderWrapper" class="tabExtendableListFejlec" Style="text-align: left;">
                <asp:Panel ID="panelCpeHeader" runat="server" Style="display: block; padding-left: 5px; cursor: pointer; font-weight: bold;">
                    <asp:ImageButton runat="server" ID="ibtnTerjedelemCpe" ImageUrl="../images/hu/Grid/minus.gif"
                        OnClientClick="return false;" />
                    <asp:Label ID="labelTerjedelemCpeText" runat="server" Text="Terjedelem"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="panelTerjedelem" runat="server">
                <%--Hiba megjelenites--%>
                <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                        </eUI:eErrorPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%--/Hiba megjelenites--%>
                <table>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Mennyiség" Width="200px" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Egység" Width="200px" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Megjegyzés" Width="300px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:RequiredNumberBox ID="requiredNumberBoxMennyiseg" runat="server" Width="200px" Text="1" AutoPostBack="true"/>
                        </td>
                        <td>
                            <uc:KodtarakDropDownList ID="ktdlMennyisegiEgyseg" runat="server" Width="200px" AutoPostBack="true"/>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMegjegyzes" runat="server" Width="300px" AutoPostBack="true"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
