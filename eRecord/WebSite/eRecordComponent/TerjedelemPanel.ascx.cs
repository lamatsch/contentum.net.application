﻿using System;

public partial class eRecordComponent_TerjedelemPanel : System.Web.UI.UserControl
{
    private const string KodCsoportAdathordozoTipus = "ADATHORDOZO_TIPUSA";
    private const string KodCsoportMennyisegiEgyseg = "MENNYISEGI_EGYSEG";

    [Serializable]
    public class Terjedelem
    {
        private string mennyiseg;

        public string Mennyiseg
        {
            get { return mennyiseg; }
            set { mennyiseg = value; }
        }

        private string mennyisegiEgyseg;

        public string MennyisegiEgyseg
        {
            get { return mennyisegiEgyseg; }
            set { mennyisegiEgyseg = value; }
        }

        private string mennyisegiEgysegNev;

        public string MennyisegiEgysegNev
        {
            get { return mennyisegiEgysegNev; }
            set { mennyisegiEgysegNev = value; }
        }
        private string megjegyzes;

        public string Megjegyzes
        {
            get { return megjegyzes; }
            set { megjegyzes = value; }
        }

        public static Terjedelem Empty
        {
            get
            {
                Terjedelem emptyTerjedelem = new Terjedelem();
                return emptyTerjedelem;
            }
        }
    }

    internal Terjedelem TerjedelemProperty { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (TerjedelemProperty != null)
                DataBindTerjedelem(TerjedelemProperty);

            ktdlMennyisegiEgyseg.FillDropDownList(KodCsoportMennyisegiEgyseg, EErrorPanel1);
        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }
    public void DataBindTerjedelem(Terjedelem item)
    {
        TerjedelemProperty = item;

        requiredNumberBoxMennyiseg.Text = TerjedelemProperty.Mennyiseg;
        ktdlMennyisegiEgyseg.SelectedValue = TerjedelemProperty.MennyisegiEgyseg;
        txtMegjegyzes.Text = TerjedelemProperty.Megjegyzes;
    }
    public void SetTerjedelem(string mennyiseg, string mennyisegiEgyseg, string megjegyzes)
    {
        TerjedelemProperty = new Terjedelem();
        TerjedelemProperty.Mennyiseg = mennyiseg;
        TerjedelemProperty.MennyisegiEgyseg = mennyisegiEgyseg;
        TerjedelemProperty.Megjegyzes = megjegyzes;

        requiredNumberBoxMennyiseg.Text = TerjedelemProperty.Mennyiseg;
        ktdlMennyisegiEgyseg.SelectedValue = TerjedelemProperty.MennyisegiEgyseg;
        txtMegjegyzes.Text = TerjedelemProperty.Megjegyzes;
    }
    public void GetTerjedelem(out string mennyiseg, out string mennyisegiEgyseg, out string megjegyzes)
    {
        mennyiseg = requiredNumberBoxMennyiseg.Text;
        mennyisegiEgyseg = ktdlMennyisegiEgyseg.SelectedValue;
        megjegyzes = txtMegjegyzes.Text;
    }
    public void SetReadOnly(bool isReadOnly)
    {
        requiredNumberBoxMennyiseg.ReadOnly = isReadOnly;
        ktdlMennyisegiEgyseg.ReadOnly = isReadOnly;
        txtMegjegyzes.ReadOnly = isReadOnly;
    }
}
