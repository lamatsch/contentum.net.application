<%@ Control Language="C#"  AutoEventWireup="true" CodeFile="TomegesIktatasFileInput.ascx.cs" Inherits="Component_TomegesIktatasFileInput" %>

<%@ Register Src="../Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc8" %>
<%@ Register Src="../Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc7" %>
<%@ Register Src="../eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="../eRecordComponent/TomegesIktatasForrasRendszerDropdown.ascx" TagName="TomegesIktatasForrasRendszerDropdownList" TagPrefix="uc5" %>

    <uc8:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:Form,TomegesIkatatasTitle %>" />
    &nbsp;<br />
   <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <%--Hiba megjelenites--%>
                <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <eUI:eErrorPanel ID="ErrorPanel1" runat="server" Visible="false">
                        </eUI:eErrorPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%--/Hiba megjelenites--%>

                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <ContentTemplate>
							<table cellspacing="0" cellpadding="0" width="600px">
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo">
                                        <asp:FileUpload id="FileUpload1" runat="server"  accept=".xml,.dbf"></asp:FileUpload>
                                        <asp:RequiredFieldValidator id="Validator1" runat="server" ErrorMessage="<%$Resources:LovList,UploadLovHasNoFile%>" Display="None" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
							            <ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1"></ajaxToolkit:ValidatorCalloutExtender>    
                                    </td>
                                </tr>
               <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="label2" runat="server" Text="Forr�s rendszer:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">

                                        <uc5:TomegesIktatasForrasRendszerDropdownList id="TomegesIktatasForrasRendszerDropdownlist" runat="server"
                                          />
                                    </td>
                                    
                                </tr> 
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label55" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelIktatokonyv" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">

                                        <uc4:iraiktatokonyvekdropdownlist id="IraIktatoKonyvek_DropDownList" runat="server"
                                             Mode="Iktatokonyvek"
                                            Filter_IdeIktathat="False" IsMultiSearchMode="False"/>

                                    </td>
                                 </tr>
                                 <!-- CR3172- el�zm�nyez�s kell-e paratm�ter -->
                                 <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption">
                                        <asp:CheckBox runat="server" Text="El�zm�nyez�s kell?" Checked="true" ID="ElozmenyKellCheckBox" />
                                    </td>
                                </tr>
							</table>

						</ContentTemplate>
                    &nbsp;
                    &nbsp;
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <eUI:eFormPanel ID="ResultPanel_TomegesIktatas" runat="server" Visible="false"
        CssClass="mrResultPanel">
        <table cellspacing="0" cellpadding="0" width="600px">
            <tr class="urlapSor_kicsi">
                <td class="mrUrlapCaption">
                    <asp:Label ID="label3" runat="server" Text="A folyamat elindult az eredm�nyr�l, e-mail-t k�ld�nk"></asp:Label>
                </td>
            </tr>
            <tr class="urlapSor_kicsi">
                <td class="mrUrlapCaption">
                    <asp:ImageButton TabIndex = "1" ID="ImageButtonBack" runat="server" 
                   ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png" 
                   onmouseover="swapByName(this.id,'rendben_es_uj2.png')" 
                   onmouseout="swapByName(this.id,'rendben_es_uj.png')" 
                    OnClick="BackBtn_Click" CommandName="Ok" Visible="False" />
                </td>
            </tr>
        </table>
    </eUI:eFormPanel>	
    <table style="width: 70%;">
        <tr>
            <td style="text-align: center;">
                <asp:ImageButton TabIndex = "1" ID="ImageOk" runat="server" 
                   ImageUrl="~/images/hu/ovalgomb/kivalaszt.jpg" 
                   onmouseover="swapByName(this.id,'kivalaszt2.jpg')" 
                   onmouseout="swapByName(this.id,'kivalaszt.jpg')" 
                    OnClick="UploadBtn_Click" CommandName="Ok" Visible="True" />         
            </td>  
        </tr>
</table>
