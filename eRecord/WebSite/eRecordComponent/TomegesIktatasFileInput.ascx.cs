using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml.Linq;
using System.IO;

public partial class Component_TomegesIktatasFileInput : System.Web.UI.UserControl
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    #endregion

    private Type _type = typeof(EREC_UgyUgyiratokSearch);
    private XmlDocument xmlDoc = null;
    private bool isValid = true;
    private string errMsg = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            if (ErrorPanel1.Visible)
            {
                ErrorPanel1.Visible = false;
            }
            if (ImageButtonBack.Visible)
            {
                ImageButtonBack.Visible = false;
            }
            if (ResultPanel_TomegesIktatas.Visible)
            {
                ResultPanel_TomegesIktatas.Visible = true;
            }
        }
        LoadComponentsFromSearchObject(/*searchObject*/);
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!FileUpload1.HasFile)
        {
            Validator1.Enabled = true;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    #region iktat�k�nyvek dropdown

    private void LoadComponentsFromSearchObject()
    {        
        #region CR3141
        if (!IsPostBack)
        {
            GetSystemsParams();
        }
        
        
        using (Contentum.eRecord.Service.EREC_TomegesIktatasService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
        { 
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            ExecParam.Record_Id= TomegesIktatasForrasRendszerDropdownlist.SelectedValue;
            var res = service.Get(ExecParam);
            // CR3172 modified by nekrisz
            if (res.Record != null)
            {
                IraIktatoKonyvek_DropDownList.FillDropDownListByIrattariTetel(true, true, Constants.IktatoErkezteto.Iktato
                    , ((EREC_TomegesIktatas)res.Record).IraIrattariTetelek_Id, ErrorPanel1);
            }
        }
        
        #endregion
        
    }

    //private BaseSearchObject GetDefaultSearchObject()
    //{
    //    BaseSearchObject searchObject;
    //    searchObject = (BaseSearchObject)Search.GetDefaultSearchObject(_type);

    //    return searchObject;
    //}
    #endregion

    #region T�meges iktat�s
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            if (FileUpload1.PostedFile.ContentType != "text/xml" && FileUpload1.PostedFile.ContentType != "application/octet-stream")
            {
                ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                    "Csak xml vagy dbf file t�lthet� fel.");
                return;
            }
             
        }
        if (string.IsNullOrEmpty(IraIktatoKonyvek_DropDownList.SelectedValue))
        {
            ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                Resources.Form.IktatoKonyvNotSelected);
            return;
        }
        else if (string.IsNullOrEmpty(TomegesIktatasForrasRendszerDropdownlist.SelectedValue))
        {
            ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                Resources.Form.ForrasRendszerNotSelected);
            return;
        }
        else
        {
            ErrorPanel1.Visible = false;
        }
        //Todo megkaptam a file ebb�l xml-t kell gy�rtani �s valid�lni, hogy minden adatom meg van-e az iktat�shoz az �sszes xml elemre 
        // Ha ez megvan, akkor asyncron megh�vni az �sszes elemre az iktat�st, ha siker�ltek az iktat�sok akkor �ssze�ll�tani az �j xml file az iktat�sz�mokkal kieg�sz�tve �s lementeni a Share-re, ha ez is megvan akkor mail k�ld�s a felhaszn�l�nak
        

        if (FileUpload1.PostedFile.ContentType == "text/xml")
        {
            xmlDoc = new XmlDocument();
            if (!ValidateXMLFile()) return;
            try
            {
                string dbfFilePath = CreateXMLTempFile();
                var service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService();
                //CR3172 - El�zm�nyez�s kell-e param�ter
                service.BeginTomegesIktatas(UI.SetExecParamDefault(Page, new ExecParam()), xmlDoc, IraIktatoKonyvek_DropDownList.SelectedValue, TomegesIktatasForrasRendszerDropdownlist.SelectedValue, dbfFilePath, ElozmenyKellCheckBox.Checked,null, null);

            }
            catch (Exception ex)
            {
                ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                    ex.Message);
                return ;
            }
        }
        else if (FileUpload1.PostedFile.ContentType == "application/octet-stream")
        {
            try
            {
                string dbfFilePath = CreateDBFTempFile();
                var service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService();
                //CR3172 - El�zm�nyez�s kell-e param�ter
                service.BeginTomegesIktatas(UI.SetExecParamDefault(Page, new ExecParam()), xmlDoc, IraIktatoKonyvek_DropDownList.SelectedValue, TomegesIktatasForrasRendszerDropdownlist.SelectedValue, dbfFilePath, ElozmenyKellCheckBox.Checked, null, null);

            }
            catch (Exception ex)
            {
                ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                    ex.Message);
                return;
            }
        }
        ResultPanel_TomegesIktatas.Visible = true;
        ImageButtonBack.Visible = true;
        EFormPanel1.Visible = false;
        ImageOk.Visible = false;
        //ImageClose.Visible = true;
    }
    protected void BackBtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("TomegesIktatas.aspx");
    }
    private string CreateDBFTempFile()
    {
        string tempDirectory = string.Empty;
        string tempFileName = "t_iktatas" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".dbf";
        Contentum.eRecord.Utility.FileFunctions.Upload.SaveFileToTempDirectory(FileUpload1.FileBytes,
            tempFileName, out tempDirectory);
        string dirPath = Path.Combine(FileFunctions.Upload.tempDirectory, tempDirectory);
        string filePath = Path.Combine(dirPath, tempFileName);


        return filePath;
    }
    
    private string CreateXMLTempFile()
    {
        string tempDirectory = string.Empty;
        string tempFileName = "t_iktatas" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";


        Contentum.eRecord.Utility.FileFunctions.Upload.SaveFileToTempDirectory(System.Text.Encoding.Convert(GetEncoding(FileUpload1.FileBytes), System.Text.Encoding.Default, FileUpload1.FileBytes),
            tempFileName, out tempDirectory);
        string dirPath = Path.Combine(FileFunctions.Upload.tempDirectory, tempDirectory);
        string filePath = Path.Combine(dirPath, tempFileName);

        return filePath;
    }
    public System.Text.Encoding GetEncoding(byte[] bom)
    {
        // Analyze the BOM
        if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return System.Text.Encoding.UTF7;
        if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return System.Text.Encoding.UTF8;
        if (bom[0] == 0xff && bom[1] == 0xfe) return System.Text.Encoding.Unicode; //UTF-16LE
        if (bom[0] == 0xfe && bom[1] == 0xff) return System.Text.Encoding.BigEndianUnicode; //UTF-16BE
        if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return System.Text.Encoding.UTF32;
        return System.Text.Encoding.ASCII;
    }
    private bool ValidateXMLFile()
    {
        Result result = new Result();

        try
        {
            XmlSchema xmlSchema = Contentum.eUtility.XmlHelper.GetXsdSchemaFromFile(PathToTomegesIktatasXsd);
            ValidationEventHandler eventHandler = new ValidationEventHandler(SchemaValidationEventHandler);
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationEventHandler += eventHandler;
            settings.ValidationType = ValidationType.Schema;            
            XmlSchemaSet schemas = new XmlSchemaSet();            
            schemas.Add(xmlSchema);
            settings.Schemas.Add(schemas);
            settings.ValidationFlags =
                XmlSchemaValidationFlags.ReportValidationWarnings |
                XmlSchemaValidationFlags.ProcessIdentityConstraints |
                XmlSchemaValidationFlags.ProcessInlineSchema |
                XmlSchemaValidationFlags.ProcessSchemaLocation;

            XmlReader reader = XmlReader.Create(FileUpload1.FileContent, settings);
           
            xmlDoc.Load(reader);


            // the following call to Validate succeeds.
            //document.Validate(schemas,eventHandler);
            if (!isValid)
            {
                ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader, errMsg);
                return false; 
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayWarningOnErrorPanel(ErrorPanel1, Resources.Error.DefaultErrorHeader,
                ex.Message);
            return false;
        }
       
        return isValid;
    }

    private void SchemaValidationEventHandler(object sender, ValidationEventArgs e)
    {
        isValid = false;
        errMsg += "XML "+e.Severity.ToString()+": "+e.Message.ToString()+" (Line "+e.Exception.LineNumber.ToString()+")<br/>";
    }
    #endregion

    #region eUtility
    public const string DirectoryPathToXsdTomegesIktatas = "App_Data";
    public const string FileNameXsdTomegesIktatas = "OnkAdo.xsd";
   
    public static string PathToTomegesIktatasXsd
    {
        get
        {
            return System.IO.Path.Combine(
                     System.IO.Path.Combine(
                         System.Web.HttpContext.Current.Server.MapPath(".")
                         , DirectoryPathToXsdTomegesIktatas
                     )
                     , FileNameXsdTomegesIktatas
                  );
        }
    }
    #endregion

    #region CR3141
    private void GetSystemsParams()
    {//ha t�bb van kiv�laszt�s
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eRecord.Service.EREC_TomegesIktatasService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TomegesIktatasService();
        EREC_TomegesIktatasSearch search = (EREC_TomegesIktatasSearch)Search.GetSearchObject(Page,new EREC_TomegesIktatasSearch());
        var res = service.GetAll(ExecParam, search);
        TomegesIktatasForrasRendszerDropdownlist.FillDropDownList(res, "Id", "ForrasTipusNev", ErrorPanel1);
        if (res.Ds.Tables[0].Rows.Count < 2)
            TomegesIktatasForrasRendszerDropdownlist.Enabled = false;
        
        return ;
    }
    #endregion
}