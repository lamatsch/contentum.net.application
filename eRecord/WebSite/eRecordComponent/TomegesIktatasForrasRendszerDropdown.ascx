<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TomegesIktatasForrasRendszerDropdown.ascx.cs" Inherits="eRecordComponent_TomegesIktatasForrasRendszerDropdown" %>

<asp:DropDownList ID="ForrasRendszerDropdownList" EnableViewState="true" runat="server" AutoPostBack="true"  CssClass="mrUrlapInputComboBox" >
</asp:DropDownList>
 <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"  InitialValue="" ErrorMessage="<%$Resources:Form,IktatoKonyvNotSelected%>" Display="None" ControlToValidate="ForrasRendszerDropdownList" Enabled="true"></asp:RequiredFieldValidator>
 <ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator1"></ajaxToolkit:ValidatorCalloutExtender>