using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUIControls;

public partial class eRecordComponent_TomegesIktatasForrasRendszerDropdown : System.Web.UI.UserControl
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");

    protected void Page_Load(object sender, EventArgs e)
    {
        ForrasRendszerDropdownList.SelectedIndexChanged += new EventHandler(dropDownList1_SelectedIndexChanged);
    }
    public eDropDownList DropDownList
    {
        get { return ForrasRendszerDropdownList; }
    }
    /// <summary>
    /// Be�ll�tja a m�r felt�lt�tt lista �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ForrasRendszerDropdownList.SelectedValue = selectedValue;
    }
    public String SelectedValue
    {
        get { return ForrasRendszerDropdownList.SelectedValue; }
        set { SetSelectedValue(value); }
    }

    public bool Enabled
    {
        get { return ForrasRendszerDropdownList.Enabled; }
        set { ForrasRendszerDropdownList.Enabled = value; }
    }

    public Unit Width
    {
        get { return ForrasRendszerDropdownList.Width; }
        set { ForrasRendszerDropdownList.Width = value; }
    }

    public string CssClass
    {
        get { return ForrasRendszerDropdownList.CssClass; }
        set { ForrasRendszerDropdownList.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            ForrasRendszerDropdownList.ValidationGroup = value;
        }
        get { return ForrasRendszerDropdownList.ValidationGroup; }
    }

    /// <summary>
    /// Method executed with an item from the dropdownlist is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectedValue = ForrasRendszerDropdownList.SelectedItem.Value;
    }

    /// <summary>
    /// Felt�lti a DropDownList-et
    /// </summary>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        ForrasRendszerDropdownList.Items.Clear();

        if (Res.Ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < Res.Ds.Tables[0].Rows.Count; i++)
            {
                ForrasRendszerDropdownList.Items.Add(new ListItem(Res.Ds.Tables[0].Rows[i][DisplayColumnName].ToString(), Res.Ds.Tables[0].Rows[i][IdColumnName].ToString()));
            }

            if (addEmptyItem)
            {
                ForrasRendszerDropdownList.Items.Insert(0, emptyListItem);
            }
        }
    }

    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, false, errorPanel);
    }

}