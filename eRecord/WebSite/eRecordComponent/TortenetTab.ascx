<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TortenetTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratTortenetTab" %>

<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register src="../Component/PrintHtml.ascx" tagname="PrintHtml" tagprefix="print" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="TortenetUpdatePanel" runat="server" OnLoad="TortenetUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <%--<ajaxToolkit:CollapsiblePanelExtender ID="TortenetCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>  --%>                                                
  
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="../images/hu/Grid/minus.gif" runat="server" ID="btnCpeEsemenyek" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelEsemenyek" runat="server" OnLoad="updatePanelEsemenyek_Load">
                    <ContentTemplate>
                        <uc1:SubListHeader ID="ListHeader1" runat="server" />
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeEsemenyek" runat="server" TargetControlID="panelEsemenyek"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeEsemenyek" CollapseControlID="btnCpeEsemenyek"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif"
                            ExpandedImage="../images/hu/Grid/minus.gif" ImageControlID="btnCpeEsemenyek"
                            ExpandedSize="0" ExpandedText="Esem�nyek list�ja" CollapsedText="Esem�nyek list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelEsemenyek" runat="server" Visible="true" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewEsemenyek" runat="server" GridLines="Both" BorderWidth="1px" 
                                     BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewEsemenyek_RowCommand" 
                                     OnPreRender="gridViewEsemenyek_PreRender" AllowSorting="True" PagerSettings-Visible="false" 
                                     CellPadding="0" DataKeyNames="Id" AutoGenerateColumns="False"
                                     OnRowDataBound="gridViewEsemenyek_RowDataBound" OnSorting="gridViewEsemenyek_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Funkcio_Id") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
                                            <asp:BoundField DataField="Funkciok_Nev" HeaderText="Esem�ny" SortExpression="KRT_Funkciok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Azonositoja" HeaderText="Azonos�t�" SortExpression="KRT_Esemenyek.Azonositoja">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObjTip_Id_Nev" HeaderText="Obj. t�pusa" SortExpression="KRT_ObjTipusok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Felhasznalo_Id_User_Nev" HeaderText="Kinek a nev�ben" SortExpression="KRT_Felhasznalok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Felhasznalo_Id_Login_Nev" HeaderText="V�grehajt�" SortExpression="KRT_Felhasznalok1.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>                                          
                                            <asp:BoundField DataField="Csoport_Id_FelelosUserSzerveze_Nev" HeaderText="Vh. szervezete" SortExpression="KRT_Csoportok.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="HelyettesitesMod_Nev" HeaderText="Vh. jogalap" SortExpression="KRT_Helyettesitesek.HelyettesitesMod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="D�tum" SortExpression="KRT_Esemenyek.LetrehozasIdo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Note" HeaderText="Megjegyz�s" SortExpression="KRT_Esemenyek.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <%-- bernat.laszlo added: Munka�llom�s �s M�velet kimenetele--%>
                                            <asp:BoundField DataField="Munkaallomas" HeaderText="Munka�llomas" SortExpression="KRT_Esemenyek.Munkaallomas">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MuveletKimenete" HeaderText="M�velet kimenetele" SortExpression="KRT_Esemenyek.MuveletKimenete">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <%-- bernat.laszlo eddig--%>
                                        </Columns>
                                     </asp:GridView>
                                   </td>
                                 </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbEnableItems_View" runat="server" Text="Megtekint�sek is" Checked="false" AutoPostBack="false" />
                                        &nbsp
                                        <asp:CheckBox ID="cbEnableAllItems_View" runat="server" Text="�sszes kapcsol�d� esem�ny" Checked="false" AutoPostBack="false" />
                                     </td>
                                </tr>
                             </table>
                             <div style="text-align: right;">
                                <asp:ImageButton ID="ImageButton_Print" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_trap.jpg" 
                                                            onmouseover="swapByName(this.id,'nyomtatas_trap2.jpg')" onmouseout="swapByName(this.id,'nyomtatas_trap.jpg')"/>
                             </div>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>
         </tr>
    </table>

</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>


