﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratTortenetTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get
        {
            //return _Active;
            return MainPanel.Visible;
        }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    private String FunkcioIdsAll_View
    {
        get
        {
            if (ViewState["FunkcioIdsAll_View"] == null)
            {
                KRT_FunkciokService service_funkciok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();
                ExecParam ExecParam_funkciok = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_FunkciokSearch search_funkciok = new KRT_FunkciokSearch();

                search_funkciok.Kod.Value = "'IraIratView','UgyiratView','IratPeldanyView'"
                + ",'IraIratViewHistory','UgyiratViewHistory','IratPeldanyViewHistory'"
                + ",'KuldemenyView','KuldemenyViewHistory'"
                + ",'IraIratCsatolmanyView','VezetoiPanelView'";
                search_funkciok.Kod.Operator = Contentum.eQuery.Query.Operators.inner;


                Result result_funkciok = service_funkciok.GetAll(ExecParam_funkciok, search_funkciok);
                string ViewIds = "";
                if (String.IsNullOrEmpty(result_funkciok.ErrorCode))
                {

                    List<string> ViewIdsList = new List<string>();
                    foreach (DataRow row in result_funkciok.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(id))
                        {
                            ViewIdsList.Add("'" + id + "'");
                        }
                    }

                    if (ViewIdsList.Count > 0)
                    {
                        ViewIds = String.Join(",", ViewIdsList.ToArray());
                    }

                }

                ViewState["FunkcioIdsAll_View"] = ViewIds;
            }
            return ViewState["FunkcioIdsAll_View"].ToString();
        }

    }

    //LZS - BLG_12369
    private String FunkcioIds_View
    {
        get
        {
            if (ViewState["FunkcioIds_View"] == null)
            {
                KRT_FunkciokService service_funkciok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();
                ExecParam ExecParam_funkciok = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_FunkciokSearch search_funkciok = new KRT_FunkciokSearch();

                string Parent_View = string.Format("'{0}View'", ParentForm);
                string Parent_ViewHistory = string.Format("'{0}ViewHistory'", ParentForm);
                string Get = "'IraIratView','UgyiratView','IratPeldanyView'"
                + ",'IraIratViewHistory','UgyiratViewHistory','IratPeldanyViewHistory'"
                + ",'KuldemenyView','KuldemenyViewHistory'"
                + ",'IraIratCsatolmanyView','VezetoiPanelView'";

                //LZS - BLG_12369 - A ParentForm view funkciókódokat kivesszük a teljes listából. Ezzel elérve azt, hogy minden
                //mást kiszűrünk majd, kivéve ezeket.
                search_funkciok.Kod.Value = Get.Replace(Parent_View, "").Replace(Parent_ViewHistory, "").Replace(",,",",").TrimStart(','); 
                search_funkciok.Kod.Operator = Contentum.eQuery.Query.Operators.inner;


                Result result_funkciok = service_funkciok.GetAll(ExecParam_funkciok, search_funkciok);
                string ViewIds = "";
                if (String.IsNullOrEmpty(result_funkciok.ErrorCode))
                {

                    List<string> ViewIdsList = new List<string>();
                    foreach (DataRow row in result_funkciok.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(id))
                        {
                            ViewIdsList.Add("'" + id + "'");
                        }
                    }

                    if (ViewIdsList.Count > 0)
                    {
                        ViewIds = String.Join(",", ViewIdsList.ToArray());
                    }

                }

                ViewState["FunkcioIds_View"] = ViewIds;
            }
            return ViewState["FunkcioIds_View"].ToString();
        }

    }


    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sorának lekérdezése ID alapján
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiválasztott sorának módosíthatósága
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "EsemenyekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Fõ lista gombjainak láthatóságának beállítása
        ListHeader1.NewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.ValidFilterVisible = false;

        //Keresési objektum típusának megadása

        //Fõ lista megjelenések testreszabása

        //Fõ lista gombokhoz kliens oldali szkriptek regisztrálása

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = gridViewEsemenyek;


        //Fõ lista összes kiválasztása és az összes kiválasztás megszüntetése checkbox-aihoz szkriptek beállítása
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEsemenyek);

        //Fõ lista eseménykezelõ függvényei

        //szkriptek regisztrálása
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hibaüzenet panel elüntetése
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire jó */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_EsemenyekSearch());

        //A fõ lista adatkötés, ha nem postback a kérés
        //if (!IsPostBack)
        //    EsemenyekGridViewBind();

        //scroll állapotának mentése
        //JavaScripts.RegisterScrollManagerScript(Page);

        string js = "__doPostBack('" + updatePanelEsemenyek.ClientID + "','" + EventArgumentConst.refresh + "');";
        if (cbEnableItems_View.Attributes["onclick"] == null)
        {
            cbEnableItems_View.Attributes.Add("onclick", js);
        }

        if (cbEnableAllItems_View.Attributes["onclick"] == null)
        {
            cbEnableAllItems_View.Attributes.Add("onclick", js);
        }

        string TableName = "";

        if (ParentForm == Constants.ParentForms.Ugyirat)
        {
            TableName = Constants.TableNames.EREC_UgyUgyiratok;
        }
        else if (ParentForm == Constants.ParentForms.IraIrat)
        {
            TableName = Constants.TableNames.EREC_IraIratok;
        }
        else if (ParentForm == Constants.ParentForms.IratPeldany)
        {
            TableName = Constants.TableNames.EREC_PldIratPeldanyok;
        }

        ImageButton_Print.OnClientClick = "javascript:window.open('TortenetTabPrintForm.aspx?" + QueryStringVars.Id + "=" + Request.QueryString.Get("Id")
            + "&" + QueryStringVars.Filter + "=" + (cbEnableItems_View.Checked == true ? "0" : "1")
            + "&" + QueryStringVars.TableName + "=" + TableName
            + "')";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;
        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEsemenyek);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }

    protected void TortenetUpdatePanel_Load(object sender, EventArgs e)
    {

    }

    public void ReLoadTab()
    {
        if (!Active) return;

        EsemenyekGridViewBind();
    }

    #endregion

    #region Master List

    //adatkötés meghívása default értékekkel
    protected void EsemenyekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEsemenyek", ViewState, "KRT_Esemenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEsemenyek", ViewState);

        EsemenyekGridViewBind(sortExpression, sortDirection);
    }

    //adatkötés webszolgáltatástól kapott adatokkal
    protected void EsemenyekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_EsemenyekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_EsemenyekSearch search = new KRT_EsemenyekSearch();//(KRT_EsemenyekSearch)Search.GetSearchObject(Page, new KRT_EsemenyekSearch());

        //search.Obj_Id.Value = Request.QueryString.Get("Id");
        //search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        //LZS - BLG_12369
        ViewState["FunkcioIds_View"] = 
        ViewState["FunkcioIdsAll_View"] = null;
        //Kivesszük az összes megtekintést:
        if (!cbEnableItems_View.Checked && !cbEnableAllItems_View.Checked)
        {
            string FilterViewIds = FunkcioIdsAll_View;
            if (!String.IsNullOrEmpty(FilterViewIds))
            {
                search.Funkcio_Id.Value = FilterViewIds;
                search.Funkcio_Id.Operator = Contentum.eQuery.Query.Operators.notinner;
            }
        }

        //LZS - BLG_12369
        //Kivesszük a nem a parentForm-hoz tartozó megtekintéseket:
        if (cbEnableItems_View.Checked && !cbEnableAllItems_View.Checked)
        {
            string FilterViewIds = FunkcioIds_View;
            if (!String.IsNullOrEmpty(FilterViewIds))
            {
                search.Funkcio_Id.Value = FilterViewIds;
                search.Funkcio_Id.Operator = Contentum.eQuery.Query.Operators.notinner;
            }
        }

        search.OrderBy = Search.GetOrderBy("gridViewEsemenyek", ViewState, SortExpression, SortDirection);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        //Result res = service.GetAllWithExtension(ExecParam, search);
        string Obj_Id = Request.QueryString.Get("Id");
        Result res = service.GetAllWithExtensionForTortenet(ExecParam, Obj_Id, search);

        UI.GridViewFill(gridViewEsemenyek, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        UI.SetTabHeaderRowCountText(res, TabHeader);

    }

    //A GridView RowDataBound esemény kezelõje, ami egy sor adatkötése után hívódik
    protected void gridViewEsemenyek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok beállítása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            //DataRowView drw = (DataRowView)e.Row.DataItem;

            //BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }

        //Lockolás jelzése
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender eseménykezelõje
    protected void gridViewEsemenyek_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEsemenyek);
        ListHeader1.RefreshPagerLabel();

        /*int prev_PageIndex = gridViewEsemenyek.PageIndex;

        //oldalszámok beállítása
        gridViewEsemenyek.PageIndex = ListHeader1.PageIndex;

        //lapozás esetén adatok frissítése
        if (prev_PageIndex != gridViewEsemenyek.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, cpeEsemenyek);
            EsemenyekGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEsemenyek);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEsemenyek);

        //oldalszámok beállítása
        ListHeader1.PageCount = gridViewEsemenyek.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewEsemenyek);
         * */
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEsemenyek);
        EsemenyekGridViewBind();
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EsemenyekGridViewBind();
    }


    //GridView RowCommand eseménykezelõje, amit valamelyik sorban fellépett parancs vált ki
    protected void gridViewEsemenyek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiválasztása (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEsemenyek, selectedRowNumber, "check");

            string id = gridViewEsemenyek.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewEsemenyek_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modosíthatóság ellenõrzése
            bool modosithato = getModosithatosag(id, gridViewEsemenyek);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            //Fõ lista kiválasztott elem esetén érvényes funkciók regisztrálása
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID);
        }
    }


    //UpdatePanel Load eseménykezelõje
    protected void updatePanelEsemenyek_Load(object sender, EventArgs e)
    {
        ////Fõ lista frissítése
        //if (Request.Params["__EVENTARGUMENT"] != null)
        //{
        //    string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
        //    switch (eventArgument)
        //    {
        //        case EventArgumentConst.refreshMasterList:
        //            EsemenyekGridViewBind();
        //            //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewEsemenyek));
        //            ////gridViewEsemenyek_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewEsemenyek));
        //            break;
        //    }
        //}

        if (!MainPanel.Visible) return;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refresh:
                    EsemenyekGridViewBind();
                    break;
            }
        }
    }

    //Fõ lista bal oldali funkciógombjainak eseménykezelõje
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //törlés parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            EsemenyekGridViewBind();
        }
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEsemenyek -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEsemenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEsemenyek, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Esemenyek");
        }
    }

    //GridView Sorting esménykezelõje
    protected void gridViewEsemenyek_Sorting(object sender, GridViewSortEventArgs e)
    {
        EsemenyekGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewEsemenyek", ViewState, e.SortExpression));
    }

    #endregion

}
