<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TovabbiBekuldokTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratTovabbiBekuldokTab" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc2" %>
<%@ Register Src="TabHeader.ascx" TagName="TabHeader" TagPrefix="uc1" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc3" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
															                
<asp:UpdatePanel ID="TovabbiBekuldokUpdatePanel" runat="server" OnLoad="TovabbiBekuldokUpdatePanel_Load">
    <ContentTemplate>  

 <asp:Panel ID="MainPanel" runat="server" Visible="false">

    <ajaxToolkit:CollapsiblePanelExtender ID="TovabbiBekuldokCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="200" ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>                                                  
    
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
    
    <table cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		    <td>
				<%-- TabHeader --%>
                
                <uc1:TabHeader ID="TabHeader1" runat="server" />
							    
			    <%-- /TabHeader --%>
                        
                <asp:Panel ID="Panel1" runat="server" BackColor="whitesmoke">
				        <asp:GridView ID="TovabbiBekuldokGridView"
				                      runat="server"
                                      CellPadding="0"
				                      CellSpacing="0"
				                      BorderWidth="1"
				                      GridLines="Both"
				                      AllowPaging="True"
				                      PagerSettings-Visible="false"
				                      AllowSorting="True"
				                      DataKeyNames="Id"
				                      AutoGenerateColumns="False" Width="98%"
				                      OnRowCommand="TovabbiBekuldokGridView_RowCommand" OnRowDataBound="TovabbiBekuldokGridView_RowDataBound" OnSorting="TovabbiBekuldokGridView_Sorting"
				                      >
						    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
						    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
						    <HeaderStyle CssClass="GridViewHeaderStyle"  />
						    <Columns>
							    <asp:TemplateField>
								    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
								    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
								    <HeaderTemplate>
									    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
									    &nbsp;&nbsp;
									    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
								    </HeaderTemplate>
								    <ItemTemplate>
									    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" /> <%--'<%# Eval("Id") %>'--%>
								    </ItemTemplate>
							    </asp:TemplateField>
							    <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
								    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
								    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
							    </asp:CommandField>
									<asp:BoundField DataField="NevSTR" 
									                HeaderText="PartnerN�v" 
									                HeaderStyle-Wrap="false"
									                SortExpression="NevSTR">
										<HeaderStyle CssClass="GridViewBorderHeader" Width="45%"/>
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
									<asp:BoundField DataField="CimSTR" 
									                HeaderText="C�m" 
									                HeaderStyle-Wrap="false"
									                SortExpression="CimSTR">
										<HeaderStyle CssClass="GridViewBorderHeader" Width="40%" />
										<ItemStyle CssClass="GridViewBorder" />
									</asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
						    </Columns>
				                      
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>                            
        
			        <eUI:eFormPanel ID="EFormPanel1" runat="server">
    					<table cellspacing="0" cellpadding="0" width="60%">
					        <tr class="urlapSor">
						        <td class="mrUrlapCaptionBal">
                                    <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>&nbsp;
							        <asp:Label ID="Label1" runat="server" Text="N�v:"></asp:Label>
						        </td>
						        <td style="text-align: left; vertical-align: top;">
                                    <uc3:PartnerTextBox ID="Nev" runat="server"/>
						        </td>
						        <td style="width: 20%"></td>
				            </tr>
					        <tr class="urlapSor">
						        <td class="mrUrlapCaptionBal">
							        <asp:Label ID="Label2" runat="server" Text="C�m:"></asp:Label>
						        </td>
						        <td style="text-align: left; vertical-align: top;">
							        <uc3:CimekTextBox ID="Cim" runat="server" Validate="false"/>
						        </td>
						        <td style="width: 20%"></td>
					        </tr>
					    </table>
					    <%-- TabFooter --%>
    
                        <uc2:TabFooter ID="TabFooter1" runat="server" />
						
					    <%-- /TabFooter --%>
			    </eUI:eFormPanel>    			
		    </td>
	    </tr>
    </table>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>


