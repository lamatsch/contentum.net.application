using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratTovabbiBekuldokTab : System.Web.UI.UserControl
{

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    public Label TabHeader = new Label();

    #region public Properties

    #endregion

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

//        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
        TabHeader1.ButtonsClick += new CommandEventHandler(TabHeaderButtonsClick);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        TabHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.New);
        TabHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.View);
        TabHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.Modify);
        TabHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.Invalidate);
        //TabHeader1.VersionEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.Version);
        //TabHeader1.ElosztoListaEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + CommandName.ElosztoLista);

        if (IsPostBack)
        {
            RefreshOnClientClicksByGridViewSelectedRow(UI.GetGridViewSelectedRecordId(TovabbiBekuldokGridView));
        }
        ui.SetClientScriptToGridViewSelectDeSelectButton(TovabbiBekuldokGridView);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Nev.ValidatorCalloutExtender.Enabled = true;
        Cim.ValidatorCalloutExtender.Enabled = true;

        //Partner �s cim �sszek�t�se
        Nev.CimTextBox = Cim.TextBox;
        Nev.CimHiddenField = Cim.HiddenField;


        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    protected void TovabbiBekuldokUpdatePanel_Load(object sender, EventArgs e)
    {
    }

    public void ReLoadTab()
    {
        if (!MainPanel.Visible) return;

        // itt kell a tiltas megadni, hogy mely gombok NEM latszodhatnak ebben a funkcioban:        
        TabHeader1.VersionVisible = false;
        TabHeader1.ElosztoListaVisible = false;

        TabHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TabHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(TovabbiBekuldokGridView.ClientID);
        //TabHeader1.VersionOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //TabHeader1.ElosztoListaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ClearForm();
        EFormPanel1.Visible = false;
        TovabbiBekuldokGridViewBind();

        pageView.SetViewOnPage(Command);
    }

    private void TabHeaderButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ClearForm();

        Command = e.CommandName;

        TabFooter1.CommandArgument = Command;

        if (e.CommandName == CommandName.New)
        {
            EFormPanel1.Visible = true;
        }
        if (e.CommandName == CommandName.View || e.CommandName == CommandName.Modify)
        {
            EFormPanel1.Visible = true;

            String RecordId = UI.GetGridViewSelectedRecordId(TovabbiBekuldokGridView);

            if (String.IsNullOrEmpty(RecordId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                EREC_KuldBekuldokService service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = RecordId;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_KuldBekuldok erec_KuldBekuldok = (EREC_KuldBekuldok)result.Record;
                    LoadComponentsFromBusinessObject(erec_KuldBekuldok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }
            }
        }
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTovabbiBekuldok();
            TovabbiBekuldokGridViewBind();
        }
        if (e.CommandName == CommandName.Version)
        {

        }
        if (e.CommandName == CommandName.ElosztoLista)
        {

        }
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String SubCommand = (String)e.CommandArgument;
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldo" + SubCommand))
            {
                switch (SubCommand)
                {
                    case CommandName.Cancel:
                        EFormPanel1.Visible = false;
                        break;
                    case CommandName.New:
                        {
                            EREC_KuldBekuldokService service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
                            EREC_KuldBekuldok erec_KuldBekuldok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            
                            Result result = service.Insert(execParam, erec_KuldBekuldok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ReLoadTab();
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = UI.GetGridViewSelectedRecordId(TovabbiBekuldokGridView);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_KuldBekuldokService service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
                                EREC_KuldBekuldok erec_KuldBekuldok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_KuldBekuldok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ReLoadTab();
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            EFormPanel1.Visible = false;
        }
    }

    private void ClearForm()
    {
        Nev.Text = "";
        Nev.Id_HiddenField = "";
        Cim.Text = "";
        Cim.Id_HiddenField = "";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_KuldBekuldok erec_KuldBekuldok)
    {
        Nev.Id_HiddenField = erec_KuldBekuldok.Partner_Id_Bekuldo;
        Nev.Text = erec_KuldBekuldok.NevSTR;
        //Nev.SetPartnerTextBoxById(EErrorPanel1);

        Cim.Id_HiddenField = erec_KuldBekuldok.PartnerCim_Id_Bekuldo;
        Cim.Text = erec_KuldBekuldok.CimSTR;
        //Cim.SetCimekTextBoxById(EErrorPanel1);

        Record_Ver_HiddenField.Value = erec_KuldBekuldok.Base.Ver;

        if (Command == CommandName.View)
        {
            Nev.ReadOnly = true;
            Cim.ReadOnly = true;
        }
        else
        {
            Nev.ReadOnly = false;
            Cim.ReadOnly = false;
        }

        //FormPart_CreatedModified1.SetComponentValues(erec_KuldBekuldok.Base);
    }

    // form --> business object
    private EREC_KuldBekuldok GetBusinessObjectFromComponents()
    {
        EREC_KuldBekuldok erec_KuldBekuldok = new EREC_KuldBekuldok();
        erec_KuldBekuldok.Updated.SetValueAll(false);
        erec_KuldBekuldok.Base.Updated.SetValueAll(false);

        erec_KuldBekuldok.KuldKuldemeny_Id = ParentId;
        erec_KuldBekuldok.Updated.KuldKuldemeny_Id = true;

        erec_KuldBekuldok.Partner_Id_Bekuldo = Nev.Id_HiddenField;
        erec_KuldBekuldok.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(Nev);

        erec_KuldBekuldok.NevSTR = Nev.Text;
        erec_KuldBekuldok.Updated.NevSTR = pageView.GetUpdatedByView(Nev);

        erec_KuldBekuldok.PartnerCim_Id_Bekuldo = Cim.Id_HiddenField;
        erec_KuldBekuldok.Updated.PartnerCim_Id_Bekuldo = pageView.GetUpdatedByView(Cim);

        erec_KuldBekuldok.CimSTR = Cim.Text;
        erec_KuldBekuldok.Updated.CimSTR = pageView.GetUpdatedByView(Cim);

        erec_KuldBekuldok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_KuldBekuldok.Base.Updated.Ver = true;

        return erec_KuldBekuldok;
    }


    #endregion

    #region List
    protected void TovabbiBekuldokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("TovabbiBekuldokGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("TovabbiBekuldokGridView", ViewState);
        TovabbiBekuldokGridViewBind(sortExpression, sortDirection);
    }

    protected void TovabbiBekuldokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(TovabbiBekuldokGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_KuldBekuldokService service = null;
        EREC_KuldBekuldokSearch search = null;

        if (Page.ToString().Equals("ASP.kuldkuldemenyekform_aspx"))
        {
            service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
            search = (EREC_KuldBekuldokSearch)Search.GetSearchObject(Page, new EREC_KuldBekuldokSearch());
            if (!String.IsNullOrEmpty(ParentId))
            {
                search.KuldKuldemeny_Id.Value = ParentId;
                search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
        }
        search.OrderBy = Search.GetOrderBy("TovabbiBekuldokGridView", ViewState, SortExpression, SortDirection);
        Result res = service.GetAllWithExtension(ExecParam, search);

        ui.GridViewFill(TovabbiBekuldokGridView, res, EErrorPanel1, ErrorUpdatePanel1);
        UI.SetTabHeaderRowCountText(res, TabHeader);

        ui.SetClientScriptToGridViewSelectDeSelectButton(TovabbiBekuldokGridView);
    }

    private void RefreshOnClientClicksByGridViewSelectedRow(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            TabHeader1.ViewOnClientClick = "";
            TabHeader1.ModifyOnClientClick = "";
            TabHeader1.VersionOnClientClick = "";
            TabHeader1.ElosztoListaOnClientClick = "";
        }
    }

    protected void TovabbiBekuldokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(TovabbiBekuldokGridView, selectedRowNumber, "check");
            EFormPanel1.Visible = false;

        }
    }

    /// <summary>
    /// T�rli a TovabbiBekuldokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedTovabbiBekuldok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "KuldemenyTovabbiBekuldoInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(TovabbiBekuldokGridView, EErrorPanel1, ErrorUpdatePanel1);
            EREC_KuldBekuldokService service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    #endregion
    protected void TovabbiBekuldokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }
    protected void TovabbiBekuldokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TovabbiBekuldokGridViewBind(e.SortExpression, UI.GetSortToGridView("TovabbiBekuldokGridView", ViewState, e.SortExpression));
    }
}
