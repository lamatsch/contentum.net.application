﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyUgyiratFormTab.ascx.cs"
    Inherits="eRecordComponent_UgyiratUgyTab" %>
<%@ Register Src="../Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="../Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc10" %>
<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc9" %>
<%@ Register Src="../Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc8" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc5" %>
<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc4" %>
<%@ Register Src="IraIrattariTetelTextBoxWithExtensions.ascx" TagName="IraIrattariTetelTextBoxWithExtensions"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc2" %>
<%@ Register Src="UgyiratTrapezGombSor.ascx" TagName="UgyiratTrapezGombSor" TagPrefix="uc1" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="FunkcioGombsor.ascx" TagName="FunkcioGombsor"
    TagPrefix="fgs" %>
<%@ Register Src="ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagPrefix="uc13" TagName="FuggoKodtarakDropDownList" %>
<%--  CR3206 - Irattári struktúra --%>
<%--CR3246 Irattári Helyek kezelésének módosítása --%>
<%@ Register Src="IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>
<%--  CR3206 - Irattári struktúra --%>
<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="~/Component/AlkalmazasTextBox.ascx" TagName="AlkalmazasTextBox" TagPrefix="uc" %>
<%--BLG_1348--%>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="UgyUgyiratUpdatePanel" runat="server" OnLoad="UgyUgyiratUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="IktatokonyvId_HiddenField" runat="server" />

            <%--LZS - BUG_7099--%>
            <%--A korábban az EREC_UgyUgyiratok.Note mezőben lévő szöveghez
            fűzzük hozzá a felületen megadott megjegyzés szövegét. Ehhez kiszedjük a már benne lévő szöveget, és ezt eltároljuk a 
            hiddenUgyiratNote.Value változóba.--%>
            <%--BUG_10591--%>
           <%-- <asp:HiddenField ID="hiddenUgyiratNote" runat="server" />--%>
             <asp:HiddenField ID="hiddenUgyiratNote" runat="server" Visible="False" />
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr>
                        <td valign="top">
                            <eUI:eFormPanel ID="UgyForm" runat="server">
                                <table cellspacing="0" cellpadding="0" runat="server">
                                    <tbody>
                                        <tr>
                                            <td class="ugyiratugy">
                                                <table id="Table5" cellspacing="0" cellpadding="0" runat="server">
                                                    <tbody>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormIktatoszam">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label8" runat="server" Text="Iktatószám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:Label ID="SzervezetiKodLabel" runat="server" CssClass="foszamText" BorderStyle="Ridge"
                                                                    Style="vertical-align: middle; text-align: center;" BorderColor="LightGray" BorderWidth="1px"
                                                                    Height="18px" Width="30px"></asp:Label>
                                                                <asp:Label ID="FoszamLabel" runat="server" CssClass="foszamText" BorderStyle="Ridge"
                                                                    Style="padding-left: 6px; vertical-align: middle;" BorderColor="LightGray" BorderWidth="1px"
                                                                    Height="18px"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label6" runat="server" Text="Vonalkód:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc9:VonalKodTextBox ID="BarCode_VonalKodTextBox" runat="server" ReadOnly="true"
                                                                    ViewMode="true"></uc9:VonalKodTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormIktatasDatuma">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label15" runat="server" Text="Iktatás idõpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="IktatasDatuma_CalendarControl" runat="server" Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Allapot_felirat" runat="server" Text="Ügyirat állapota:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true"></uc2:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormTargy">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Ugyirat_targya_felirat" runat="server" Text="Ügyirat tárgya:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <asp:TextBox ID="Targy_TextBox" runat="server" TextMode="MultiLine" Rows="2" Width="95%"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trMegjegyzes">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="lblUgyiratMegjegyzes" runat="server" Text="<%$Forditas:lblUgyiratMegjegyzes|Megjegyzés:%>"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <asp:TextBox ID="txtUgyiratMegjegyzes" runat="server" TextMode="MultiLine" Rows="2" Width="95%"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormUgyfel">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label9" runat="server" Text="Ügyfél, ügyindító:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <uc11:EditablePartnerTextBox ID="Ugy_PartnerId_Ugyindito_PartnerTextBox" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormugyindito">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelUgyinditoCime" runat="server" Text="Ügyindító címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <uc3:CimekTextBox ID="CimekTextBoxUgyindito" runat="server" Validate="false"></uc3:CimekTextBox>
                                                            </td>
                                                        </tr>
                                                        <%--BLG_1348--%>
                                                        <tr class="urlapSor_kicsi" runat="server">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label runat="server" Text="Ügyirat ügyintézési ideje:" ID="trUgyiratIntezesiIdoLabel" Visible="false" />
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <input id="inputUgyintezesiNapok" runat="server" visible="false"
                                                                    list='datalistUgyintezesiNapok'
                                                                    name="inputNameUgyintezesiNapok"
                                                                    min="0" max="10000000"
                                                                    style="width: 150px;" autopostback="true" />
                                                                <datalist id="datalistUgyintezesiNapok" runat="server">
                                                                </datalist>
                                                                <ktddl:KodtarakDropDownList ID="IntezesiIdoegyseg_KodtarakDropDownList" runat="server" CssClass="" Visible="false" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label_UgyintezesKezdete" runat="server" Text="Ügyirat ügyintézési kezdete:" />
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="CalendarControl_UgyintezesKezdete" runat="server" Validate="false" TimeVisible="true" Enabled="false" ReadOnly="true" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormSurgosseg">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Surgosseg_felirat" runat="server" Text="Sürgõsség:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:KodtarakDropDownList ID="Surgosseg_KodtarakDropDownList" runat="server"></uc2:KodtarakDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Ugyirat_hatarido_felirat" runat="server" Text="Ügyirat ügyintézési határideje:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="Hatarido_CalendarControl" runat="server" Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                                <%-- módosításnál a határidõ módosulásának figyeléséhez --%>
                                                                <asp:HiddenField ID="UgyiratHataridoKitolas_Ugyirat_HiddenField" runat="server" />
                                                                <%--// BLG_1348--%>
                                                                <asp:HiddenField ID="Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormUgyiratFajtaja">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label10" runat="server" Text="Ügyirat típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:KodtarakDropDownList ID="UgyiratFajtaja_KodtarakDropDownList" runat="server"></uc2:KodtarakDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short" rowspan="2">
                                                                <asp:Label ID="Label14" runat="server" Text="Elintézés módja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" rowspan="2">
                                                                <uc2:KodtarakDropDownList ID="Elintezes_KodtarakDropDownList" runat="server" ReadOnly="true"></uc2:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormElsodlegesAdathordozoTipusa">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label13" runat="server" Text="Elsõdleges adathordozó típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:KodtarakDropDownList ID="KezelesModja_KodtarakDropDownList" runat="server"></uc2:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormFelelos">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="LabelSzervezetiEgyseg" runat="server" Text="<%$Forditas:LabelSzervezetiEgyseg|Szervezeti egység:%>"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc7:CsoportTextBox ID="CsopId_UgyFelelos_CsoportTextBox" runat="server" ReadOnly="true"
                                                                    ViewMode="true"></uc7:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <%--BLG_1014--%>
                                                                <%--<asp:Label ID="Ugyintezo_felirat" runat="server" Text="Ügyintézõ:"></asp:Label>--%>
                                                                <asp:Label ID="Ugyintezo_felirat" runat="server" Text="<%$Forditas:Ugyintezo_felirat|Ügyintézõ:%>"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc6:FelhasznaloCsoportTextBox ID="FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                                                    runat="server" Validate="false"></uc6:FelhasznaloCsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trUgyFormKezelo">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Felelos_felirat" runat="server" Text="Kezelõ:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc7:CsoportTextBox ID="CsopId_Felelos_CsoportTextBox" runat="server" ReadOnly="true"
                                                                    ViewMode="true"></uc7:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Orzo_felirat" runat="server" Text="Irat helye:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc6:FelhasznaloCsoportTextBox ID="FelhCsopId_Orzo_FelhasznaloCsoportTextBox" runat="server"
                                                                    ReadOnly="true" ViewMode="true"></uc6:FelhasznaloCsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short"></td>
                                                            <td class="mrUrlapMezo"></td>
                                                            <td class="mrUrlapCaption_short">                                                                
                                                                <asp:Label ID="Label41" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_jovahagyo" runat="server" visible="false" class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label_Jovahagyo" runat="server" Text="Jóváhagyó:" Visible="false"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc7:CsoportTextBox ID="Jovahagyo_CsoportTextBox" runat="server" ReadOnly="true"
                                                                    ViewMode="true" Visible="false"></uc7:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short"></td>
                                                            <td class="mrUrlapMezo"></td>
                                                        </tr>
                                                        <tr id="trUgyFormUgyazonosito" runat="server" class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelUgyazonosito" runat="server" Text="Ügyazonosító:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="Ugyazonosito_TextBox" runat="server" CssClass="mrUrlapInput" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelUgykezeloRendszer" runat="server" Text="Ügykezelõ rendszer azonosítója:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc:AlkalmazasTextBox ID="UgykezeloRendszer_AlkalmazasTextBox" runat="server" ReadOnly="true" />
                                                            </td>
                                                        </tr>
                                                        <%-- bernat.laszlo added: Elõirat, utóirat iktatószámok --%>
                                                        <tr id="trEloUtoIktatoszamok" runat="server" class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label150" runat="server" Text="Elõirat iktatószáma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eloirat_IktatoSzamTextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label151" runat="server" Text="Utóirat iktatószáma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="utoirat_IktatoSzamTextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                            </td>
                                                        </tr>
                                                        <%-- bernat.laszlo eddig --%>
                                                    </tbody>
                                                </table>
                                                <%--<asp:Image ID="SpacerImage1" runat="server" Visible="true" ImageUrl="~/images/hu/design/spacertrans.gif">
                                                </asp:Image>--%>
                                                <table cellspacing="0" cellpadding="0" runat="server" id="tableSkontro">
                                                    <tbody>
                                                        <tr id="tr_skontroOka" class="urlapSor_kicsi" runat="server">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label2" runat="server" Text="Skontró oka:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="5">
                                                                <asp:TextBox ID="SkontroOka_TextBox" runat="server" ReadOnly="True" Width="80%" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trSkontroKezdete">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Skontro_kezdete_felirat" runat="server" Text="Határidõbe tétel idõpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="SkontrobaDat_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Skontro_hatarido_felirat" runat="server" Text="Határidõbe tétel lejárata:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="SkontroVege_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label1" runat="server" Text="Határidõzés idõtartama:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc8:RequiredNumberBox ID="SkontrobanOsszesen_RequiredNumberBox" runat="server" ReadOnly="true"
                                                                    Validate="false"></uc8:RequiredNumberBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trSkontroElintezesDatuma">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Elintezes_felirat" runat="server" Text="Ügyirat elintézési idõpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="ElintezesDat_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Lezar_felirat" runat="server" Text="Ügyirat lezárási idõpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="LezarasDat_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Sztorno_felirat" runat="server" Text="Sztornó dátuma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="SztornirozasDat_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false"></cc:CalendarControl>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" id="trLezarasOka" runat="server">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label11" runat="server" Text="Lezárás oka:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="5">
                                                                <uc2:KodtarakDropDownList ID="LezarasOka_KodtarakDropDownList" runat="server" ReadOnly="true" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" id="trIrattarHelyfoglalas" runat="server">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label12" runat="server" Text="Irattári helyfoglalás:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="5">
                                                                <asp:TextBox ID="TextBoxIrattarHelyfoglalas" runat="server" />
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ErrorMessage="Hib�s sz�mform�tum." Operator="DataTypeCheck" Type="Double" ControlToValidate="TextBoxIrattarHelyfoglalas"></asp:CompareValidator>
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                                                                    TargetControlID="CompareValidator1">
                                                                    <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                                                    </Animations>
                                                                </ajaxToolkit:ValidatorCalloutExtender>

                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" id="trFelfuggesztesOka" runat="server">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="LabelFelfuggesztesOka" runat="server" Text="Felfüggesztés oka:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="5">
                                                                <asp:TextBox ID="TextBoxFelfuggesztesOka" runat="server" ReadOnly="True" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </eUI:eFormPanel>
                            <eUI:eFormPanel ID="JegyzekEgyebSzervezetnekAtadas_EFormPanel" runat="server" Visible="false">
                                <table id="tableJegyzekEgyebSzervezetnekAtadas" cellspacing="0" cellpadding="0" runat="server">
                                    <tbody>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_nowidth">
                                                <asp:Label ID="labelVegrehajtasDatuma" runat="server" Text="Egyéb szervezetnek átadva:" />
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <cc:CalendarControl ID="CalendarControl_VegrehajtasDatuma" runat="server" ReadOnly="true"
                                                    Validate="false" />
                                            </td>
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelAtvevoIktatoSzama" runat="server" Text="Átvevõ iktatószáma:" />
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="txtNote" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Width="150px" />
                                            </td>
                                            <%--BUG_9374--%>
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="Label17" runat="server" Text="Jegyzék azonosító:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="JegyzekAzonEgyebSzerv_TextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_nowidth">
                                                <asp:Label ID="labelVegrehajto" runat="server" Text="Átadó:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <uc6:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxVegrehajto" runat="server"
                                                    ViewMode="true" ReadOnly="true" />
                                            </td>
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyzés:" />
                                            </td>
                                            <td class="mrUrlapMezo" rowspan="2" colspan="3">
                                                <asp:TextBox ID="txtMegjegyzes" runat="server" CssClass="mrUrlapInput" ReadOnly="true"
                                                    TextMode="MultiLine" Rows="3" />
                                            </td>
                                        </tr>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_nowidth">
                                                <asp:Label ID="labelAtvevo" runat="server" Text="Átvevõ:" />
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="txtAtvevo" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </eUI:eFormPanel>

                            <eUI:eFormPanel ID="IrattariTetel_EFormPanel" runat="server">

                                <table id="Table1" cellspacing="0" cellpadding="0" runat="server">
                                    <tbody>
                                        <tr runat="server" id="trIrattariTetelTextBox">
                                            <td class="ugyiratugy">

                                                <uc3:IraIrattariTetelTextBoxWithExtensions ID="IraIrattariTetelId_IraIrattariTetelTextBox"
                                                    runat="server" Validate="false"></uc3:IraIrattariTetelTextBoxWithExtensions>

                                            </td>
                                        </tr>
                                        <tr class="urlapSor_kicsi" runat="server" id="trIrattariTetelUgytipus">
                                            <td style="text-align: left;">
                                                <div class="DisableWrap">
                                                    <table id="Table2" runat="server" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr class="urlapSor_kicsi">
                                                                <td class="mrUrlapCaptionBal_nowidth" style="font-weight: bold; padding-left: 6px; width: 94px;">
                                                                    <asp:Label ID="Ugytipus_felirat" runat="server" Text="Ügytípus:"></asp:Label>
                                                                    <%--BLG_439--%>
                                                                    <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:"></asp:Label>

                                                                </td>
                                                                <td class="mrUrlapMezo">
                                                                    <ajaxToolkit:CascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="Ugytipus_DropDownList"
                                                                        UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetUgytipusokByUgykorForModify"
                                                                        Category="Ugytipus" Enabled="false" EmptyText="---" EmptyValue="" SelectedValue=""
                                                                        LoadingText="[Feltöltés folyamatban...]" PromptText="<Ügytípus kiválasztása>"
                                                                        PromptValue="">
                                                                    </ajaxToolkit:CascadingDropDown>
                                                                    <asp:DropDownList ID="Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox_veryWide">
                                                                    </asp:DropDownList>
                                                                    <asp:DropDownList ID="Ugytipus_DropDownList_View" runat="server" CssClass="mrUrlapInputComboBox_veryWide"
                                                                        Visible="false">
                                                                    </asp:DropDownList>
                                                                    <%--BLG_439--%>
                                                                    <uc2:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" CssClass="" Style="width: auto" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Ugytipus_DropDownList"
                                                                        Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                                                        TargetControlID="RequiredFieldValidator1">
                                                                        <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                                                        </Animations>
                                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                                </td>
                                                                <%--                                        <td class="mrUrlapCaption_short">
                                        </td>
                                        <td class="mrUrlapMezo">
                                        </td>--%>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </eUI:eFormPanel>

                            <eUI:eFormPanel ID="IrattariAdatok_EFormPanel" runat="server" Visible="False">

                                <table id="Table3" cellspacing="0" cellpadding="0" runat="server">
                                    <tbody>
                                        <tr>
                                            <td class="ugyiratugy">
                                                <table id="Table4" cellspacing="0" cellpadding="0" runat="server">
                                                    <tbody>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label3" runat="server" Text="Irattárba helyezés dátuma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="CalendarControlIrattarbaHelyezes" runat="server" ReadOnly="true"
                                                                    Validate="false"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label5" runat="server" Text="Irattári átvevõ:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc6:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxIrattarbaVevo" runat="server"
                                                                    ViewMode="true" ReadOnly="true"></uc6:FelhasznaloCsoportTextBox>
                                                            </td>
                                                            <%--BUG_9374--%>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label16" runat="server" Text="Jegyzék azonosító:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="JegyzekAzon_TextBox" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label4" runat="server" Text="Irattárba vétel:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="CalendarControlIrattarbaVetel" runat="server" ReadOnly="true"
                                                                    Validate="false"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label7" runat="server" Text="Irattári hely:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <%--  CR3206 - Irattári struktúra --%>
                                                                <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                                                                <%--Irattári hely Ügyiratformon nem módosítható--%>
                                                                <%--<uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDown" runat="server" />--%>
                                                                <asp:TextBox ID="IrattariHely_TextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                <%--<asp:TextBox ID="IrattariHely_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="regexpValidatorIrattariHely" runat="server" Display="None"
                                                                    SetFocusOnError="true" ErrorMessage="" ControlToValidate="IrattariHely_TextBox"
                                                                    ValidationExpression="" Enabled="false" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderRegexp" runat="server"
                                                                    TargetControlID="regexpValidatorIrattariHely">
                                                                    <Animations>
                                                                            <OnShow>
                                                                            <Sequence>
                                                                                <HideAction Visible="true" />
                                                                                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                            </Sequence>    
                                                                            </OnShow>
                                                                    </Animations>
                                                                </ajaxToolkit:ValidatorCalloutExtender>--%>
                                                                <%--  CR3206 - Irattári struktúra --%>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelMegorzesiIdoVege" runat="server" Text="Megõrzési idõ vége:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="MegorzesiIdoVege_CalendarControl" runat="server" ReadOnly="true"
                                                                    Validate="false"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelUjOrzesiIdo" runat="server" Text="Új õrzési idõ:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <asp:TextBox ID="UjOrzesiIdo_TextBox" runat="server" ReadOnly="true" Width="100px"></asp:TextBox>
                                                                <%--BLG_2156--%>
                                                                <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" KodcsoportKod="IDOEGYSEG_FELHASZNALAS" CssClass="hiddenitem" />
                                                                <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_Idoegyseg" KodcsoportKod="IDOEGYSEG" ParentControlId="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" CssClass="mrUrlapInputComboBoxKozepes" ReadOnly="True" />

                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelLeveltarbaAdas" runat="server" Text="Levéltárba adás:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cc:CalendarControl ID="CalendarControlSelejtezes" runat="server" ReadOnly="true"
                                                                    Validate="false"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelLeveltarbaAdo" runat="server" Text="Levéltárba adó:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <uc6:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxSelejtezoAdo" runat="server"
                                                                    ViewMode="true" ReadOnly="true"></uc6:FelhasznaloCsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelLeveltariAtvevo" runat="server" Text="Levéltári átvevõ:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="5">
                                                                <asp:TextBox ID="Leveltariatvevo_TextBox" Width="487" runat="server" ReadOnly="True"
                                                                    CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </eUI:eFormPanel>
                            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                                <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak" RepeatColumns="2" RepeatDirection="Horizontal"
                                    runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                            </eUI:eFormPanel>
                            <uc5:TabFooter ID="TabFooter1" runat="server"></uc5:TabFooter>
                        </td>
                        <td style="width: 100px" valign="top">
                            <fgs:FunkcioGombsor ID="FunkcioGombsor" runat="server"></fgs:FunkcioGombsor>
                        </td>
                        <td>&nbsp</td>
                    </tr>
                    <%--
<tr>
  <td valign=top width="100%">
    <eUI:eFormPanel ID="UgyFa" runat="server">
       <table cellpadding="0" cellspacing="0">
       <tr>
         <td ID="FaControl" valign=top style="width: 646px">
         <asp:TextBox ID="TextBox1" runat="server" Height="134px" Width="699px"></asp:TextBox>
         </td>
       </tr>
       <tr>       
         <td ID="Trapezgombsor" valign=top style="width: 646px">
             <uc1:UgyiratTrapezGombSor ID="UgyiratTrapezGombSor1" runat="server" />
         </td>
       </tr>
       </table>
    </eUI:eFormPanel>
  </td>
</tr>--%>
                </tbody>
            </table>
        </asp:Panel>
        <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                try {

                    //BUG_4366
                    $(window).scrollTop(0);

                } catch (e) {
                    //console.log(e);
                }

            });
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
