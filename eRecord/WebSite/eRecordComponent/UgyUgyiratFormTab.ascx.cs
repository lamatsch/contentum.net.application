﻿using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;
using Newtonsoft.Json;
using Contentum.eAdmin.Service;

public partial class eRecordComponent_UgyiratUgyTab : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{

    bool UgyfelelosModifyEnabled = false; // annak szabályozására, hogy Modify módban átállítható-e az ügyfelelős (szervezethierarchia váltás miatt ideiglenesen engedélyezve)

    private const char separator = '|';
    private const string stringEllenor = "Ellenőr:";
    private const string stringLeveltariAtvevo = "Levéltári átvevő:";
    private const string stringBizottsagiTagok = "Bizottsági tagok:";

    private const string stringLeveltarbaAdas = "Levéltárba adás dátuma:";
    private const string stringSelejtezes = "Selejtezés időpontja:";

    private const string stringLeveltarbaAdo = "Levéltárnak átadó:";
    private const string stringSelejtezo = "Selejtező:";

    private const string stringLeveltarbaAdasiIdo = "Levéltárba adhatóság dátuma";
    private const string stringSelejtezesiIdo = "Selejtezhetőség dátuma";


    private const string kcs_SURGOSSEG = "SURGOSSEG";
    //private const string kcs_UGYTIPUS = "UGYTIPUS";
    private const string kcs_UGYIRAT_ALLAPOT = "UGYIRAT_ALLAPOT";
    private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";

    private const string kcs_UGYIRAT_JELLEG = "UGYIRAT_JELLEG";
    private const string kcs_ELSODLEGES_ADATHORDOZO = "ELSODLEGES_ADATHORDOZO";


    private const string kcs_ELINTEZESMOD = "ELINTEZESMOD";
    private const string kcs_IRATTARI_JEL = "IRATTARI_JEL";

    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";

    private const string kcs_LEZARAS_OKA = "LEZARAS_OKA";

    // BLG_439
    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";

    // BLG_1348
    private const string kcs_UGYIRAT_INTEZESI_IDO = "UGYIRAT_INTEZESI_IDO";

    bool LezarasOkaVisible { get { return true; } }

    public string Command = "";
    public bool IrattarbolModositas = false;

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }

    // A Parent formról kell beállítani, hogy innen tudjuk állítani a form címet
    public Component_FormHeader FormHeader = null;


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // BLG_1348
    private bool Uj_Ugyirat_Hatarido_Kezeles
    {
        get
        {
            return Ugyiratok.Uj_Ugyirat_Hatarido_Kezeles(Page);
        }
    }

    private bool trUgyiratIntezesiIdoVisible
    {
        get
        {
            return trUgyiratIntezesiIdoLabel.Visible;
        }
        set
        {
            trUgyiratIntezesiIdoLabel.Visible = value;
            inputUgyintezesiNapok.Visible = value;
            IntezesiIdoegyseg_KodtarakDropDownList.Visible = value;
        }
    }

    // A szóban forgó ügyirat objektum (megtekintés, módosítás esetén használatos)
    public EREC_UgyUgyiratok obj_ugyirat = null;


    // Az objektumban történő változásokat jelző esemény, ami pl. kiválthat egy jogosultságellenőrzést
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    //be van-e töltve az ügyirat
    public bool IsUgyiratLoaded
    {
        get
        {
            if (ViewState["IsUgyiratLoaded"] != null)
            {
                bool loaded;
                if (Boolean.TryParse(ViewState["IsUgyiratLoaded"].ToString(), out loaded))
                {
                    return loaded;
                }
            }

            return false;
        }
        set
        {
            ViewState["IsUgyiratLoaded"] = value;
        }
    }

    private string GetJovahagyoTextByAllapot(string Allapot)
    {
        // label szöveg visszadása állapottól függően:
        switch (Allapot)
        {
            case KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa:
                return String.Concat(Resources.Form.UI_Jovahagyo_SkontrobaHelyezesre, ":");
            case KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa:
                return String.Concat(Resources.Form.UI_Jovahagyo_Elintezesre, ":");
            case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                return String.Concat(Resources.Form.UI_Jovahagyo_Kolcsonzesre, ":");
            case KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa:
                return String.Concat(Resources.Form.UI_Jovahagyo_Irattarozasra, ":");
            default:
                return String.Empty;
        }
    }

    private void SetViewControls()
    {
        Targy_TextBox.ReadOnly = true;
        //LZS - BUG_7099
        //View módban ReadOnly-ra állítjul az ügyirat megjegyzés textboxot. ("txtUgyiratMegjegyzes")
        txtUgyiratMegjegyzes.ReadOnly = true;


        RequiredFieldValidator1.Enabled = false;
        Hatarido_CalendarControl.ReadOnly = true;
        Hatarido_CalendarControl.Enabled = false;
        Surgosseg_KodtarakDropDownList.ReadOnly = true;

        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.ReadOnly = true;
        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.ViewMode = true;

        Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.ViewMode = true;

        CimekTextBoxUgyindito.ReadOnly = true;
        CimekTextBoxUgyindito.ViewMode = true;

        UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
        //      UGY_FAJTAJA_KodtarakDropDownList.ViewEnabled = false;

        // BLG_439
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {

            if (!IrattarbolModositas)
            {
                IraIrattariTetelId_IraIrattariTetelTextBox.ReadOnly = true;
                IraIrattariTetelId_IraIrattariTetelTextBox.ViewMode = true;

                // CR#2202: WorkAround: a View módhoz másik dropdownt kötünk, másképp a kikapcsolt(!)
                // Cascading Dropdown kitörli a dropdownlist tartalmát a postback során
                Ugytipus_DropDownList.Visible = false;
                Ugytipus_DropDownList_View.Visible = true;
                Ugytipus_DropDownList_View.ReadOnly = true;
            }
            Ugytipus_felirat.Visible = true;
            labelUgyFajtaja.Visible = false;
            //Ugytipus_DropDownList.Visible = true;
            // Ugytipus_DropDownList_View.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = false;
        }
        else
        {
            Ugytipus_felirat.Visible = false;
            labelUgyFajtaja.Visible = true;
            //Ugytipus_DropDownList.Visible = true;
            // Ugytipus_DropDownList_View.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = true;

            if (!IrattarbolModositas)
            {
                IraIrattariTetelId_IraIrattariTetelTextBox.ReadOnly = true;
                IraIrattariTetelId_IraIrattariTetelTextBox.ViewMode = true;

                // CR#2202: WorkAround: a View módhoz másik dropdownt kötünk, másképp a kikapcsolt(!)
                // Cascading Dropdown kitörli a dropdownlist tartalmát a postback során
                Ugytipus_DropDownList.Visible = false;
                // Ugytipus_DropDownList_View.Visible = true;
                // Ugytipus_DropDownList_View.ReadOnly = true;
                //UGY_FAJTAJA_KodtarakDropDownList.Visible = true;
                // UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;
            }

        }
        SkontroOka_TextBox.ReadOnly = true;
        Ugyazonosito_TextBox.ReadOnly = true;
        TextBoxIrattarHelyfoglalas.ReadOnly = true;

        // BLG_1348
        ////IntezesiIdo_KodtarakDropDownList.ReadOnly = true;
        IntezesiIdoegyseg_KodtarakDropDownList.ReadOnly = true;
        inputUgyintezesiNapok.Attributes.Add("readonly", "readonly");

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Enabled = false;            
        }
        // BUG_10524 kapcsán javítva
        inputUgyintezesiNapok.Disabled = true;


    }

    private void SetIrattariAdatok(Ugyiratok.Statusz ugyiratStatusz)
    {
        string startup = Request.QueryString.Get(Constants.Startup.StartupName);

        if (startup == Constants.Startup.FromKozpontiIrattar || startup == Constants.Startup.FromAtmenetiIrattar
            || startup == Constants.Startup.FromJegyzekTetel || ShowIrattariAdatok(ugyiratStatusz))
            IrattariAdatok_EFormPanel.Visible = true;
        #region CR3206 - Irattári struktúra
        if (Command == CommandName.Modify || IrattarbolModositas)
        {
            // CR3246 Irattári Helyek kezelésének módosítása
            // Irattári hely nem módosítható
            //IrattariHelyLevelekDropDown.DropDownList.ReadOnly = false;
            //JavaScripts.SetFocus(IrattariHelyLevelekDropDown);

            // reguláris kifejezés ellenőrzése
            //string RegExp = Rendszerparameterek.Get(Page, Rendszerparameterek.REGEXP_UGYIRAT_IRATTARIHELY);
            //if (!String.IsNullOrEmpty(RegExp))
            //{
            //    regexpValidatorIrattariHely.ErrorMessage = String.Format(Resources.Form.RegularExpressionValidationMessage, "(RegExp: " + RegExp + ")");
            //    regexpValidatorIrattariHely.ValidationExpression = RegExp;
            //    regexpValidatorIrattariHely.Enabled = true;
            //}
        }
        else
        {
            // CR3246 Irattári Helyek kezelésének módosítása
            // Irattári hely nem módosítható
            // IrattariHelyLevelekDropDown.DropDownList.ReadOnly = true;
            //regexpValidatorIrattariHely.Enabled = false;
        }

        if (isTUKRendszer)
        {
            tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
            //JavaScripts.SetFocus(IrattariHelyLevelekDropDownTUK);  
            IrattariHely_TextBox.Visible = false;
        }

        #endregion
    }

    private bool ShowIrattariAdatok(Ugyiratok.Statusz ugyiratStatusz)
    {
        bool ret = false;
        string ugyiratAllapot = ugyiratStatusz.Allapot;

        if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt
            || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
        {
            ugyiratAllapot = ugyiratStatusz.TovabbitasAlattiAllapot;
        }

        switch (ugyiratAllapot)
        {
            case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
            case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
            case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
            case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
            case KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott:
            case KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott:
            case KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett:
            case KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo:
            case KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott:
            case KodTarak.UGYIRAT_ALLAPOT.Selejtezett:
                ret = true;
                break;
        }

        return ret;

    }

    private void SetJegyzekEgyebSzervezetnekAtadasAdatai(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott
            || (erec_UgyUgyiratok.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott
                && erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt)
            )
        {
            JegyzekEgyebSzervezetnekAtadas_EFormPanel.Visible = true;

            if (!String.IsNullOrEmpty(erec_UgyUgyiratok.Id))
            {
                EREC_IraJegyzekTetelekService service_jegyzektetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                EREC_IraJegyzekTetelekSearch search_jegyzektetelek = new EREC_IraJegyzekTetelekSearch();
                search_jegyzektetelek.Obj_Id.Value = erec_UgyUgyiratok.Id;
                search_jegyzektetelek.Obj_Id.Operator = Query.Operators.equals;

                search_jegyzektetelek.TopRow = 1; // elvileg csak egy jegyzéken lehet...

                ExecParam execParam = UI.SetExecParamDefault(Page);

                Result result_jegyzektetelekGetAll = service_jegyzektetelek.GetAll(execParam, search_jegyzektetelek);

                if (result_jegyzektetelekGetAll.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_jegyzektetelekGetAll);
                    ErrorUpdatePanel1.Update();
                    return;
                }

                if (result_jegyzektetelekGetAll.Ds.Tables[0].Rows.Count > 0)
                {
                    txtNote.Text = result_jegyzektetelekGetAll.Ds.Tables[0].Rows[0]["Note"].ToString(); // Átvevő iktatószáma
                    txtMegjegyzes.Text = result_jegyzektetelekGetAll.Ds.Tables[0].Rows[0]["Megjegyzes"].ToString();

                    string jegyzek_Id = result_jegyzektetelekGetAll.Ds.Tables[0].Rows[0]["Jegyzek_Id"].ToString();
                    if (!String.IsNullOrEmpty(jegyzek_Id))
                    {
                        EREC_IraJegyzekekService service_jegyzekek = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                        execParam.Record_Id = jegyzek_Id;

                        Result result_jegyzekekGet = service_jegyzekek.Get(execParam);

                        if (result_jegyzekekGet.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_jegyzekekGet);
                            ErrorUpdatePanel1.Update();
                            return;
                        }

                        EREC_IraJegyzekek erec_IraJegyzekek = (EREC_IraJegyzekek)result_jegyzekekGet.Record;

                        CalendarControl_VegrehajtasDatuma.Text = erec_IraJegyzekek.VegrehajtasDatuma;
                        FelhasznaloCsoportTextBoxVegrehajto.Id_HiddenField = erec_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj;
                        FelhasznaloCsoportTextBoxVegrehajto.SetCsoportTextBoxById(FormHeader.ErrorPanel);
                        txtAtvevo.Text = erec_IraJegyzekek.Base.Note;

                    }
                }
            }
        }
        else
        {
            JegyzekEgyebSzervezetnekAtadas_EFormPanel.Visible = false;
        }
    }

    #region JavaScripts

    // ügykör, ügytípus változás figyelése módosításkor, határidő automatikus kitöltése
    private void RegisterJavascriptsForWSCall()
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null)
        {
            throw new Exception("ScriptManager not found.");
        }

        sm.Services.Add(new ServiceReference("~/WrappedWebService/Ajax_eRecord.asmx"));

        ScriptReference sr = new ScriptReference();
        sr.Path = "~/JavaScripts/WsCalls.js?t=20190918";
        sm.Scripts.Add(sr);

        string js_variables = @"
                    var userId = '" + FelhasznaloProfil.FelhasznaloId(Page) + @"';
                    var UgykorHiddenField_Id = '" + IraIrattariTetelId_IraIrattariTetelTextBox.HiddenField.ClientID + @"'; 
                    var UgytipusDropDown_Id = '" + Ugytipus_DropDownList.ClientID + @"'                    
                    var UgyUgyiratHatarido_TextBox_Id = '" + Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_HourTextBox_Id = '" + Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_MinuteTextBox_Id = '" + Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_CalendarImage_Id = '" + Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var UgyiratHataridoKitolas_Ugyirat_HiddenField_Id = '" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"'
                    var IktatasDatumUgyirat_HiddenFieldId = '" + IktatasDatuma_CalendarControl.TextBox.ClientID + @"'
                    var UgyintezesKezdete_TextBox_Id = '" + CalendarControl_UgyintezesKezdete.TextBox.ClientID + @"';
                    var UgyintezesKezdete_HourTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetHourTextBox.ClientID + @"';
                    var UgyintezesKezdete_MinuteTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetMinuteTextBox.ClientID + @"';

                    var Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.ClientID + @"';
                    var IntezesiIdo_DropDownList_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var inputUgyintezesiNapok_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var IntezesiIdoegyseg_DropDownList_Id = '" + IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"'; 
                    var IraIratHatarido_TextBox_Id = '" + Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var IraIratHatarido_HourTextBox_Id = '" + Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var IraIratHatarido_MinuteTextBox_Id = '" + Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"'; 
                    var IraIratHatarido_CalendarImage_Id = '" + Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IraIratHatarido_ArrowUpImage_Id = '" + Hatarido_CalendarControl.ArrowUpImage.ClientID + @"';
                    var IraIratHatarido_ArrowDownImage_Id = '" + Hatarido_CalendarControl.ArrowDownImage.ClientID + @"';
                    var IktatasDatumIrat_HiddenFieldId = '" + IktatasDatuma_CalendarControl.TextBox.ClientID + @"';
";

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "UgyiratHtaridoVariables", js_variables, true);

        string js_AddHandler = @"
        function UgyTipusChangeHandler()
            {
                SetTextBoxesByIratMetaDefinicioForUgyiratModify();
            }    

        function UgykorChangeHandler()
            {
                //FillUgytipusDropDown();
                var cdde = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
                var hfUgykorId = $get('" + IraIrattariTetelId_IraIrattariTetelTextBox.HiddenField.ClientID + @"');
                
                var contextKey = '';

                if (cdde && hfUgykorId)
                {
                    // csak az ügykör Id-t cseréljük ki és az ügytípust töröljük
                    var items = cdde.get_contextKey().split('|');
                    if (items && items.length > 0)
                    {
                        items[0] = hfUgykorId.value;
                        if (items.length > 1)
                        {
                            items[1] = '';
                        }
                        contextKey = items.join('|');
                    }

                    cdde.set_contextKey(contextKey);
                    cdde._onParentChange(null, true);
                }
                SetTextBoxesByIratMetaDefinicioForUgyiratModify();
            }

        function callCreateItemToolTips(sender, args)
            {
                var dropdown = sender.get_element();
                if (dropdown)
                {
                    createItemToolTips(dropdown);
                }
            }        

        function pageLoad(){//be called automatically.

            var cascadingDropDown_ugytipus = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
            if (cascadingDropDown_ugytipus)
            {
                cascadingDropDown_ugytipus.add_selectionChanged(UgyTipusChangeHandler);
                cascadingDropDown_ugytipus.add_populated(callCreateItemToolTips);
            }
         }
        ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dropDown_handler", js_AddHandler, true);

        IraIrattariTetelId_IraIrattariTetelTextBox.TextBox.Attributes.Add("onchange", "UgykorChangeHandler()");
        //Ugytipus_DropDownList.Attributes.Add("onchange", "UgyTipusChangeHandler()");
        Ugytipus_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");

        // BLG_1348
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            inputUgyintezesiNapok.Attributes.Add("onchange", "SetUgyIntezesiIdoManualv2()");
            /////IntezesiIdo_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIntezesiIdoManual()");
            IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetUgyIntezesiIdoManualv2()");
        }
    }
    #endregion JavaScripts

    private IratForm _iratForm;
    private bool isTUKRendszer = false;

    // funkciójogok ellenőrzése a fő formon

    protected void Page_Init(object sender, EventArgs e)
    {
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        pageView = new PageView(Page, ViewState);
        _iratForm = new IratForm(this, pageView)
        {
            CalendarControl_UgyintezesKezdete = CalendarControl_UgyintezesKezdete,
            Label_CalendarControl_UgyintezesKezdete = Label_UgyintezesKezdete
        };

        Command = Request.QueryString.Get(QueryStringVars.Command);

        string SubCommand = Request.QueryString.Get("SubCommand");
        {
            if (!String.IsNullOrEmpty(SubCommand) && SubCommand == "IrattarbolModositas")
            {
                IrattarbolModositas = true;
            }
        }

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        Ugy_PartnerId_Ugyindito_PartnerTextBox.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(80);
        CimekTextBoxUgyindito.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(80);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.IRATTARIHELY_KAPACITAS).ToUpper().Trim().Equals("1");
        TextBoxIrattarHelyfoglalas.Visible = _visible;

        //if (Command == CommandName.Modify)
        //{
        //    Ugytipus_DropDownList.AutoPostBack = true;
        //    Ugytipus_DropDownList.SelectedIndexChanged += new EventHandler(UgytipusDropDownList_SelectedIndexChanged);
        //}

        IraIrattariTetelId_IraIrattariTetelTextBox.TryFireChangeEvent = true;

        RegisterJavascriptsForWSCall();
        // BLG_2156
        FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Selejtezes;

        InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

        Ugy_PartnerId_Ugyindito_PartnerTextBox.Validate = false;
        CimekTextBoxUgyindito.Validate = false;

        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimTextBox = CimekTextBoxUgyindito.TextBox;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimHiddenField = CimekTextBoxUgyindito.HiddenField;

        UgyiratFajtaja_KodtarakDropDownList.ReadOnly = true;
        KezelesModja_KodtarakDropDownList.ReadOnly = true;

        IraIrattariTetelId_IraIrattariTetelTextBox.OnClick_Lov =
            JavaScripts.SetOnClientClick("IraIrattariTetelekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + IraIrattariTetelId_IraIrattariTetelTextBox.HiddenField.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + IraIrattariTetelId_IraIrattariTetelTextBox.TextBox.ClientID
           + "&" + QueryStringVars.RefreshCallingWindow + "=1"
           , Defaults.PopupWidth, Defaults.PopupHeight, IraIrattariTetelId_IraIrattariTetelTextBox.ImageButton_Lov.ClientID
           , EventArgumentConst.refreshIrattariTetelPanel, true);
        #region CR3206 - Irattári struktúra
        // CR3246 Irattári Helyek kezelésének módosítása
        // Irattári hely nem módosítható
        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Validate = true;
        }
        #endregion
        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertől függ)
        if (!Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
        {
            RequiredFieldValidator1.Enabled = false;
        }
        InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
        // BUG_10355
        if (Command == CommandName.Modify)
        {
            if ((UI.SetExecParamDefault(Page, new ExecParam()).FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIktato(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id))
            {
                UgyfelelosModifyEnabled = true;
            }
        }
    }


    //private void UgytipusDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    //    Result result = service.GetIratMetaDefinicioByUgykorUgytipus(execParam
    //        , IraIrattariTetelId_IraIrattariTetelTextBox.Id_HiddenField
    //        , Ugytipus_DropDownList.SelectedValue);

    //    EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
    //    IratMetaDefinicio.SetHataridoToolTip(Page, erec_IratMetaDefinicio, Hatarido_CalendarControl.TextBox);
    //}

    protected void FillObjektumTargyszavaiPanel(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak.FillObjektumTargyszavai(search, id, null
        , Constants.TableNames.EREC_UgyUgyiratok, null, null, false
        , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, true, EErrorPanel1);

        //otpStandardTargyszavak.SetDefaultValues();
        //TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
    }

    protected void UgyUgyiratUpdatePanel_Load(object sender, EventArgs e)
    {
        // ha nem ez az aktív tab, nem fut le
        if (!_Active) return;

        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshIrattariTetelPanel:
                        LoadIrattariTetelComponents();
                        //UgytipusDropDownList_SelectedIndexChanged(null, null);
                        break;
                    case EventArgumentConst.refreshUgyiratokPanel:
                        ReLoadTab(true);
                        RaiseEvent_OnChangedObjectProperties();
                        break;
                }
            }
        }
    }

    public void ReLoadTab()
    {
        if (!IsPostBack || !IsUgyiratLoaded)
        {
            ReLoadTab(true);
        }
        else
        {
            ReLoadTab(false);
        }
    }

    private void ReLoadTab(bool loadRecord)
    {

        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        // csak hogy továbbadhassuk a SetComponentsVisibility-nek, hogy a státusz miatt ne kelljen újra lekérni az ügyiratot
        Ugyiratok.Statusz ugyiratStatusz = null;

        //   FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyUgyiratokFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=BoritoA3");
        //   FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyUgyiratokFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=BoritoA4");

        FunkcioGombsor.Borito_nyomtatasa_A3OnClientClick = JavaScripts.SetSSRSPrintOnClientClick("UgyUgyiratokPrintFormSSRSBoritoA3.aspx", QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=BoritoA3");
        FunkcioGombsor.Borito_nyomtatasa_A4OnClientClick = JavaScripts.SetSSRSPrintOnClientClick("UgyUgyiratokPrintFormSSRSBorito.aspx", QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=BoritoA4");



        // blg 1868 FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyUgyiratokFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Kisero");
        FunkcioGombsor.KiserolapOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("UgyUgyiratokPrintFormSSRSUgyiratKisero.aspx", QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Kisero");
        FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Ugyirat')";
        //CR 3058
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            FunkcioGombsor.VonalkodNyomtatasa_OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ParentId + "&tipus=Ugyirat&" + QueryStringVars.Command + "=Modify" + "')";
        //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny");
        #region CR3083 iktatáskor van egy olyan mező, hogy "ügyfél, ügyindító", innen kerüljön le az ügyindító, csak "ügyfél" maradjon
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            Label9.Text = "Ügyfél:";
        }
        #endregion

        if (_ParentForm == Constants.ParentForms.Ugyirat)
        {
            if (!MainPanel.Visible) return;

            #region standard objektumfüggő tárgyszavak ügyirathoz

            FillObjektumTargyszavaiPanel(ParentId);

            if (otpStandardTargyszavak.Count > 0)
            {
                StandardTargyszavakPanel.Visible = true;

                if (Command == CommandName.Modify)
                {
                    otpStandardTargyszavak.SetDefaultValues();
                    TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                    TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
                }
            }
            else
            {
                StandardTargyszavakPanel.Visible = false;
            }
            #endregion standard objektumfüggő tárgyszavak ügyirathoz

            FunkcioGombsor.ParentForm = _ParentForm;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord betöltése

                        if (obj_ugyirat == null)
                        {
                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = ParentId;

                            Result result = service.Get(execParam);
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                obj_ugyirat = (EREC_UgyUgyiratok)result.Record;

                                if (obj_ugyirat == null)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                        ResultError.CreateNewResultWithErrorCode(50101));
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }

                        if (obj_ugyirat != null)
                        {
                            ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);

                            //bernat.laszlo added
                            string eloirat_iktatoszam = "";
                            string utoirat_iktatoszam = "";

                            // BLG_1348
                            if (Uj_Ugyirat_Hatarido_Kezeles)
                            {
                                trUgyiratIntezesiIdoVisible = true;
                                InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
                                //IntezesiIdo_KodtarakDropDownList.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, EErrorPanel1);

                                List<string> filterList = new List<string>();
                                filterList.Add(KodTarak.IDOEGYSEG.Nap);
                                filterList.Add(KodTarak.IDOEGYSEG.Munkanap);
                                IntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);
                            }

                            // BLG_619
                            string ugyfelelos_szervezetkod = "";
                            EREC_UgyUgyiratokService tempService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
                            ExecParam temp_execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            sch.Id.Value = obj_ugyirat.Id;
                            sch.Id.Operator = "=";
                            temp_execParam.Record_Id = obj_ugyirat.Id;

                            Result tempResult = tempService.GetAllWithExtension(temp_execParam, sch);

                            if (string.IsNullOrEmpty(tempResult.ErrorCode))
                            {
                                if (tempResult.Ds.Tables.Count != 0)
                                {
                                    if (tempResult.Ds.Tables[0].Rows.Count != 0)
                                    {
                                        utoirat_iktatoszam = tempResult.Ds.Tables[0].Rows[0]["Foszam_Merge_Szulo"].ToString();
                                        eloirat_iktatoszam = string.IsNullOrEmpty(tempResult.Ds.Tables[0].Rows[0]["Leszarmazott_Iktatoszam"].ToString()) ? tempResult.Ds.Tables[0].Rows[0]["Regirendszeriktatoszam"].ToString() : tempResult.Ds.Tables[0].Rows[0]["Leszarmazott_Iktatoszam"].ToString();

                                        // BLG_619
                                        ugyfelelos_szervezetkod = tempResult.Ds.Tables[0].Rows[0]["UgyFelelos_SzervezetKod"].ToString();

                                        // BUG_9374
                                        if (tempResult.Ds.Tables[0].Rows[0]["JegyzekAllapot"].ToString() == KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott)
                                        {
                                            JegyzekAzon_TextBox.Text = tempResult.Ds.Tables[0].Rows[0]["JegyzekAzon"].ToString();
                                            JegyzekAzonEgyebSzerv_TextBox.Text = tempResult.Ds.Tables[0].Rows[0]["JegyzekAzon"].ToString();
                                        }
                                    }
                                }
                            }
                            //bernat.laszlo eddig

                            // BLG_619
                            //LoadComponentsFromBusinessObject(obj_ugyirat, eloirat_iktatoszam, utoirat_iktatoszam);
                            LoadComponentsFromBusinessObject(obj_ugyirat, eloirat_iktatoszam, utoirat_iktatoszam, ugyfelelos_szervezetkod);
                            SetJegyzekEgyebSzervezetnekAtadasAdatai(obj_ugyirat);

                            EREC_IratMetaDefinicio erec_IratMetaDefinicio = IratMetaDefinicio.GetBusinessObjectById(Page, obj_ugyirat.IratMetadefinicio_Id);

                            #region Intézési idő
                            // Intézési idő kiolvasása az EREC_IratMetaDefinicio táblából
                            IratMetaDefinicio.SetHataridoToolTipByIratMetaDefinicio(Page, erec_IratMetaDefinicio, Hatarido_CalendarControl.TextBox);
                            #endregion Intézési idő

                            if (erec_IratMetaDefinicio != null)
                            {
                                if (erec_IratMetaDefinicio.UgyiratHataridoKitolas == "0")
                                {
                                    Hatarido_CalendarControl.ReadOnly = true;
                                    Hatarido_CalendarControl.Enabled = false;
                                }
                                else if (Command == CommandName.Modify)
                                {
                                    Hatarido_CalendarControl.ReadOnly = false;
                                    Hatarido_CalendarControl.Enabled = true;
                                    if (Uj_Ugyirat_Hatarido_Kezeles)
                                    {
                                        trUgyiratIntezesiIdoVisible = true;
                                    }
                                }

                            }
                            else
                            {
                                UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = "1";
                                if (Command == CommandName.Modify)
                                {
                                    SetHataridoControlEnabled(false);
                                    if (Uj_Ugyirat_Hatarido_Kezeles)
                                    {
                                        trUgyiratIntezesiIdoVisible = true;
                                    }
                                }

                            }
                        }
                    }
                }
            }

            // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
            TabFooter1.CommandArgument = Command;

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command, ugyiratStatusz);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }




    #region private methods

    /// <summary>
    /// Komponensek láthatóságának beállítása
    /// </summary>
    /// <param name="_ugyiratStatusz">lehet null is, csak olyankor újra le kell kérni</param>
    private void SetComponentsVisibility(String _command, Ugyiratok.Statusz _ugyiratStatusz)
    {
        if (_command == CommandName.DesignView)
            return;

        #region FunkcióGombsor állítása

        // Az osszes gomb eltuntetese
        FunkcioGombsor.ButtonsVisible(false);

        if (_command == CommandName.View)
        {
            // FunkcioGombSor gombjainak beallitasa

            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
            }
            else
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            }
            FunkcioGombsor.KiserolapVisible = true;
            FunkcioGombsor.XMLExportVisible = true;
            //CR 3058
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
            else
                FunkcioGombsor.VonalkodNyomtatasa_Visible = false;

            inputUgyintezesiNapok.Attributes.Add("readonly", "readonly");
            // BUG_10524 kapcsán javítva
            inputUgyintezesiNapok.Disabled = true;
        }

        if (_command == CommandName.Modify)
        {
            // FunkcioGombSor gombjainak beallitasa

            FunkcioGombsor.SzignalasVisible = true;
            FunkcioGombsor.AtvetelUgyintezesreVisible = true;
            FunkcioGombsor.Atadasra_kijelolesVisible = true;
            FunkcioGombsor.Szkontro_inditasVisible = true;
            FunkcioGombsor.Elintezette_nyilvanitasVisible = true;
            FunkcioGombsor.Ugyirat_lezarasaVisible = true;
            FunkcioGombsor.Ugyirat_irattarozasra_kijelolesVisible = true;
            FunkcioGombsor.Ugyirat_AlszamraIktatasKimenoBelsoVisible = true;
            FunkcioGombsor.Ugyirat_AlszamraIktatasraElokeszitVisible = true;
            FunkcioGombsor.Ugyirat_AlszamraIktatasBejovoVisible = true;
            FunkcioGombsor.CsatolasVisible = true;
            FunkcioGombsor.SzerelesVisible = true;

            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = false;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = false;
            }
            else
            {
                FunkcioGombsor.Borito_nyomtatasa_A3Visible = true;
                FunkcioGombsor.Borito_nyomtatasa_A4Visible = true;
            }

            FunkcioGombsor.KiserolapVisible = true;
            FunkcioGombsor.XMLExportVisible = true;
            //CR 3058
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                FunkcioGombsor.VonalkodNyomtatasa_Visible = true;
            else
                FunkcioGombsor.VonalkodNyomtatasa_Visible = false;
            FunkcioGombsor.Ugyirat_AlszamraIktatasraElokeszitVisible = true;
            FunkcioGombsor.Ugyirat_AlszamraIktatasraElokeszitEnabled = true;

            // Állapot és funkciójogosultság alapján beállítjuk a funkciógombokat:

            if (_ugyiratStatusz == null)
            {
                _ugyiratStatusz = Ugyiratok.GetAllapotById(ParentId, UI.SetExecParamDefault(Page, new ExecParam()), EErrorPanel1);
            }

            // Szignálás gomb
            Ugyiratok.SetSzignalasFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.SzignalasImageButton, Page);

            // Átvétel ügyintézésre gomb
            Ugyiratok.SetAtvetelUgyintezesreFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.AtvetelUgyintezesreImageButton, Page);

            // Skontró indítás gomb
            Ugyiratok.SetSkontrobaHelyezesFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Szkontro_inditasImageButton, Page);

            //Ugyirat_AlszamraIktatasraElokeszit Alszámra iktatás előkészítés
            Ugyiratok.SetAlszamraIktatas_Elokeszit_Funkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Ugyirat_AlszamraIktatasraElokeszit_ImageButton, Page);

            // Alszámra iktatás belső gomb
            Ugyiratok.SetAlszamraIktatas_Belso_Funkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Ugyirat_AlszamraIktatasKimenoBelso_ImageButton, Page);

            // Alszámra iktatás - bejövő - gomb
            Ugyiratok.SetAlszamraIktatas_Bejovo_Funkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Ugyirat_AlszamraIktatasBejovo_ImageButton, Page);

            // Átadásra kijelölés gomb
            Ugyiratok.SetAtadasraKijeloles_Funkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Atadasra_kijeloles_ImageButton, Page);

            // Elintézetté nyilvánítás gomb
            Ugyiratok.SetElintezetteNyilvanitas_Funkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Elintezette_nyilvanitas_ImageButton, Page);

            // Lezárás gomb
            Ugyiratok.SetLezarasFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Ugyirat_lezarasa_ImageButton, Page);


            // TODO:
            FunkcioGombsor.Ugyirat_irattarozasra_kijelolesEnabled = false;
            UI.SetImageButtonStyleToDisabled(FunkcioGombsor.Ugyirat_irattarozasra_kijeloles_ImageButton);


            // Csatolás gomb:
            Ugyiratok.SetCsatolasFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Csatolas_ImageButton, Page);

            // Szerelés gomb
            Ugyiratok.SetSzerelesFunkciogomb(ParentId, _ugyiratStatusz
                , FunkcioGombsor.Szereles_ImageButton, Page);

        }
        #endregion

        #region TabFooter gombok állítása

        if (_command == CommandName.Modify || IrattarbolModositas)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion



        #region Form komponensek (TextBoxok,...) állítása


        if (_ugyiratStatusz == null)
        {
            _ugyiratStatusz = Ugyiratok.GetAllapotById(ParentId, UI.SetExecParamDefault(Page, new ExecParam()), EErrorPanel1);
        }

        if (_ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
        {
            tr_skontroOka.Visible = true;
        }
        else
        {
            tr_skontroOka.Visible = false;
        }

        // A gombok láthatósága alapból a Modify esetre van beállítva,
        // View-nál pluszban még azokat kell letiltani, amik módosításnál látszanának
        IraIrattariTetelId_IraIrattariTetelTextBox.AutoComplete = true; // BUG_7088
        IktatasDatuma_CalendarControl.ReadOnly = true;
        Leveltariatvevo_TextBox.ReadOnly = true;
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        this.SetIrattariAdatok(_ugyiratStatusz);

        if (_command == CommandName.Modify)
        {

            if ((UI.SetExecParamDefault(Page, new ExecParam()).FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIktato(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id))
            {
                UgyfelelosModifyEnabled = true;
            }

            CsopId_UgyFelelos_CsoportTextBox.ReadOnly = !UgyfelelosModifyEnabled;
            CsopId_UgyFelelos_CsoportTextBox.ViewMode = !UgyfelelosModifyEnabled;

            // BLG_439
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                //if (!IrattarbolModositas)
                //{
                //    IraIrattariTetelId_IraIrattariTetelTextBox.ReadOnly = true;
                //    IraIrattariTetelId_IraIrattariTetelTextBox.ViewMode = true;

                //    // CR#2202: WorkAround: a View módhoz másik dropdownt kötünk, másképp a kikapcsolt(!)
                //    // Cascading Dropdown kitörli a dropdownlist tartalmát a postback során
                //    Ugytipus_DropDownList.Visible = false;
                //    Ugytipus_DropDownList_View.Visible = true;
                //    Ugytipus_DropDownList_View.ReadOnly = true;
                //}

                Ugytipus_felirat.Visible = true;
                labelUgyFajtaja.Visible = false;
                //Ugytipus_DropDownList.Visible = true;
                // Ugytipus_DropDownList_View.Visible = true;
                UGY_FAJTAJA_KodtarakDropDownList.Visible = false;
            }
            else
            {
                Ugytipus_felirat.Visible = false;
                labelUgyFajtaja.Visible = true;
                Ugytipus_DropDownList.Visible = false;
                // Ugytipus_DropDownList_View.Visible = true;
                UGY_FAJTAJA_KodtarakDropDownList.Visible = true;
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;

                //if (!IrattarbolModositas)
                //{
                //    IraIrattariTetelId_IraIrattariTetelTextBox.ReadOnly = true;
                //    IraIrattariTetelId_IraIrattariTetelTextBox.ViewMode = true;

                //    // CR#2202: WorkAround: a View módhoz másik dropdownt kötünk, másképp a kikapcsolt(!)
                //    // Cascading Dropdown kitörli a dropdownlist tartalmát a postback során
                //    Ugytipus_DropDownList.Visible = false;
                //    // Ugytipus_DropDownList_View.Visible = true;
                //    // Ugytipus_DropDownList_View.ReadOnly = true;
                //    //UGY_FAJTAJA_KodtarakDropDownList.Visible = true;
                //    UGY_FAJTAJA_KodtarakDropDownList.Enabled = false;
                //}

            }

            // BLG_8826
            _iratForm.AllowChangeUgyintezesKezdete();
        }
        else
        {

            CsopId_UgyFelelos_CsoportTextBox.ReadOnly = true;
            CsopId_UgyFelelos_CsoportTextBox.ViewMode = true;
        }

        #endregion

        #region StandardTargyszavakPanel állítása
        if (_command == CommandName.View)
        {
            otpStandardTargyszavak.ReadOnly = true;
        }
        else
        {
            otpStandardTargyszavak.ReadOnly = false;
        }
        #endregion StandardTargyszavakPanel állítása
    }


    //business object --> form
    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok, string eloirat, string utoirat, string ugyfelelos_szervezetkod)
    {
        if (erec_UgyUgyiratok != null)
        {
            Targy_TextBox.Text = erec_UgyUgyiratok.Targy;



            #region LZS - BUG_7099 - obsolate
            ////LZS - BUG_7099 működés: - obsolate
            ////Az ügyirat megjegyzés (EREC_UgyUgyiratok.Note) mező kezelése. A korábban az EREC_UgyUgyiratok.Note mezőben lévő szöveghez
            ////fűzzük hozzá a felületen megadott megjegyzés szövegét. Ehhez kiszedjük a már benne lévő szöveget, és ezt eltároljuk a 
            ////hiddenUgyiratNote.Value változóba. A mentéskor ezt vissza kell tennünk, és ehhez hozzáfűzni a felületen megadott megjegyzés szövegét.
            ////Ha volt már hozzáfűzve sortöréssel szöveg, akkor a sortörés utáni részt megjelenítjük.

            ////Felületen megadott ügyirat megjegyzés szövegét tároló változó
            //string megjegyzesFromNoteField = "";

            ////Megvizsgáljuk, van-e már sortörés a mezőben, ha igen, akkor annál vágjuk ketté a szöveget, és az első része megy 
            ////a hiddenUgyiratNote.Value-be, a második része pedig megjelenítésre kerül a felületen.
            //if (erec_UgyUgyiratok.Base.Note.Contains("\n"))
            //{
            //    //Sortörés előtti rész (nem jelenik meg a felületen)
            //    hiddenUgyiratNote.Value = erec_UgyUgyiratok.Base.Note.Split('\n')[0].ToString().Trim();
            //    //Sortörés utáni rész (megjelenik a felületen)
            //    megjegyzesFromNoteField = erec_UgyUgyiratok.Base.Note.Split('\n')[1].ToString().Trim();
            //}
            //else//Ha nem volt még sortörés a mezőben, akkor a benne lévő szöveg kerül a hiddenUgyiratNote.Value-ba.
            //{
            //    //Nem jelenik meg a felületen. Mert ott csak a sortörés utáni szövegek kell, hogy látszódjanak.
            //    hiddenUgyiratNote.Value = erec_UgyUgyiratok.Base.Note;
            //}

            ////A felületre kitesszük a megjegyzés szövegét.
            //txtUgyiratMegjegyzes.Text = megjegyzesFromNoteField;
            #endregion

            #region LZS - BUG_11081
            //LZS - BUG_11081 működés:
            //Az ügyirat megjegyzés (EREC_UgyUgyiratok.Note) mező kezelése. Az EREC_UgyUgyiratok.Note mezőben serializált JSON object-ként
            //tároljuk az irattárazáskor megadható megjegyzést/feljegyzést és a munkairat iktatáskor megadható javasolt előzmény mező értékét.
            //Itt a betöltéskor kiszedjük a JSON objectet és deserializáljuk, azért, hogy betölthessük a felületre a megfelelő mezők értékét.
            //Serializált formában is eltároljuk a hiddenUgyiratNote.Value változóba. A mentéskor ezt töltjük vissza, majd átírjuk a változott megjegyzés mezőt.
            //Ezt követően serializálva visszatöltjük az EREC_UgyUgyiratok.Note mezőbe.

            //EREC_UgyUgyiratok.Note mező serializált stringként.
            string noteJSON = erec_UgyUgyiratok.Base.Note;

            if (!string.IsNullOrEmpty(noteJSON))
            {
                //EREC_UgyUgyiratok.Note mező deserializált objektumként.
                Note_JSON Note;

                try
                {
                    Note = JsonConvert.DeserializeObject<Note_JSON>(noteJSON);
                }
                catch (Exception)
                {
                    Note = null;
                }

                if (Note != null)
                {
                    //A felületre kitesszük a megjegyzés/feljegyzés szövegét.
                    txtUgyiratMegjegyzes.Text = Note.Megjegyzes;

                    //A hiddenUgyiratNote-be eltároljuk az eredeti erec_UgyUgyiratok.Base.Note mezőt serializálva.
                    hiddenUgyiratNote.Value = erec_UgyUgyiratok.Base.Note;
                }
                else
                {
                    //A felületre kitesszük a megjegyzés/feljegyzés szövegét.
                    txtUgyiratMegjegyzes.Text = "";

                    //A hiddenUgyiratNote-be eltároljuk az eredeti erec_UgyUgyiratok.Base.Note mezőt serializálva.
                    hiddenUgyiratNote.Value = "";
                }
            }
            #endregion

            // Iktatókönyv hiddenFieldbe (az Ajaxos ügyintéző választásos iktatókönyvre szűkítéses dologhoz kell)
            IktatokonyvId_HiddenField.Value = erec_UgyUgyiratok.IraIktatokonyv_Id;

            //UgyTipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYTIPUS,
            //    erec_UgyUgyiratok.UgyTipus, true, EErrorPanel1);

            // Ügytípus: ha nincs megadva és View mód van, fölösleges feltölteni a listát:
            //if (!string.IsNullOrEmpty(erec_UgyUgyiratok.UgyTipus) || Command != CommandName.View)            
            //{
            //    //Ugyiratok.FillUgytipusDropDownByIrattariTetel(Page, Ugytipus_DropDownList, erec_UgyUgyiratok.IraIrattariTetel_Id
            //    //        , erec_UgyUgyiratok.UgyTipus, EErrorPanel1);
            //    CascadingDropDown_Ugytipus.SelectedValue = erec_UgyUgyiratok.UgyTipus;
            //}
            if (Command == CommandName.Modify || (Command == CommandName.View && IrattarbolModositas))
            {
                // az iktatás dátumához igazítjuk az irattári terv elemeinek kiválasztását
                IraIrattariTetelId_IraIrattariTetelTextBox.ErvenyessegDatum = erec_UgyUgyiratok.Base.LetrehozasIdo;

                string strCheckErvenyesseg = "1";
                CascadingDropDown_Ugytipus.ContextKey = String.Join("|", new string[] { erec_UgyUgyiratok.IraIrattariTetel_Id, erec_UgyUgyiratok.UgyTipus
                    ,strCheckErvenyesseg , erec_UgyUgyiratok.Base.LetrehozasIdo });
                CascadingDropDown_Ugytipus.Enabled = true;
                CascadingDropDown_Ugytipus.SelectedValue = erec_UgyUgyiratok.UgyTipus;

                // az iktatókönyv alapján megszűrjük az irattári tételeket a LovListen
                IraIrattariTetelId_IraIrattariTetelTextBox.IraIktatokonyv_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;
            }
            else if (!string.IsNullOrEmpty(erec_UgyUgyiratok.UgyTipus) && Command == CommandName.View)
            {
                //Ugyiratok.FillUgytipusDropDownByIrattariTetel(Page, Ugytipus_DropDownList, erec_UgyUgyiratok.IraIrattariTetel_Id
                //        , erec_UgyUgyiratok.UgyTipus, EErrorPanel1);

                // CR#2202: WorkAround: a View módhoz másik dropdownt kötünk, másképp a kikapcsolt(!)
                // Cascading Dropdown kitörli a dropdownlist tartalmát a postback során
                Ugyiratok.FillUgytipusDropDownByIrattariTetel(Page, Ugytipus_DropDownList_View, erec_UgyUgyiratok.IraIrattariTetel_Id
                        , erec_UgyUgyiratok.UgyTipus, EErrorPanel1);
            }

            UgyiratFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYIRAT_JELLEG,
                erec_UgyUgyiratok.Jelleg, true, EErrorPanel1);

            KezelesModja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO,
                erec_UgyUgyiratok.UgyintezesModja, true, EErrorPanel1);

            if (Command == CommandName.Modify && erec_UgyUgyiratok.Jelleg == KodTarak.UGYIRAT_JELLEG.Vegyes)
            {
                KezelesModja_KodtarakDropDownList.ReadOnly = false;
            }

            Hatarido_CalendarControl.Text = erec_UgyUgyiratok.Hatarido;
            // ha ki van töltve, nem engedjük kitörölni
            if (!String.IsNullOrEmpty(erec_UgyUgyiratok.Hatarido))
            {
                Hatarido_CalendarControl.Validate = true;
            }

            // BLG_1348
            Ugyirat_Ugyintezes_Kezdete_Datum_Hiddenfield.Value = erec_UgyUgyiratok.UgyintezesKezdete;
            CalendarControl_UgyintezesKezdete.Text = erec_UgyUgyiratok.UgyintezesKezdete;
            ////IntezesiIdo_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdo;
            LoadUgyintezesiNapokFromBo(erec_UgyUgyiratok);
            InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
            IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdoegyseg;

            IktatasDatuma_CalendarControl.Text = erec_UgyUgyiratok.Base.LetrehozasIdo;

            //bernat.laszlo added: Előirat, utóirat iktatószámok
            eloirat_IktatoSzamTextBox.Text = eloirat;
            utoirat_IktatoSzamTextBox.Text = utoirat;
            //bernat.laszlo eddig

            //IktatoszamKieg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATOSZAM_KIEG,
            //    erec_UgyUgyiratok.IktatoszamKieg, true, EErrorPanel1);

            Surgosseg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SURGOSSEG,
                erec_UgyUgyiratok.Surgosseg, true, EErrorPanel1);

            Allapot_KodtarakDropDownList.FillWithOneValue(kcs_UGYIRAT_ALLAPOT, erec_UgyUgyiratok.Allapot, EErrorPanel1);

            BarCode_VonalKodTextBox.Text = erec_UgyUgyiratok.BARCODE;

            Elintezes_KodtarakDropDownList.FillWithOneValue(kcs_ELINTEZESMOD, erec_UgyUgyiratok.ElintezesMod, EErrorPanel1);

            Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
            CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);

            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            // ha ki van töltve, ne lehessen kitörölni, csak mást megadni helyette
            if (!String.IsNullOrEmpty(FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
            {
                FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Validate = true;
            }

            CsopId_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Felelos;
            CsopId_Felelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;
            FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

            CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Ugyfelelos;
            CsopId_UgyFelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
            #region CR3206 - Irattári struktúra
            // CR3246 Irattári Helyek kezelésének módosítása
            // Irattári hely nem módosítható
            if (isTUKRendszer)
            {
                ReloadIrattariHelyLevelekDropDownTUK(erec_UgyUgyiratok.IrattarId);
            }
            else
            {
                IrattariHely_TextBox.Text = erec_UgyUgyiratok.IrattariHely;
            }
            IrattariHely_TextBox.Text = erec_UgyUgyiratok.IrattariHely;

            #endregion
            SkontrobaDat_CalendarControl.Text = erec_UgyUgyiratok.SkontrobaDat;
            SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
            CalendarControlSelejtezes.Text = erec_UgyUgyiratok.SelejtezesDat;
            ElintezesDat_CalendarControl.Text = erec_UgyUgyiratok.ElintezesDat;
            LezarasDat_CalendarControl.Text = erec_UgyUgyiratok.LezarasDat;
            SztornirozasDat_CalendarControl.Text = erec_UgyUgyiratok.SztornirozasDat;
            // BLG_2156
            UjOrzesiIdo_TextBox.Text = erec_UgyUgyiratok.UjOrzesiIdo;
            FuggoKodtarakDropDownList_Idoegyseg.SelectedValue = erec_UgyUgyiratok.UjOrzesiIdoIdoegyseg;

            // BLG_439
            UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_UgyUgyiratok.Ugy_Fajtaja, true, EErrorPanel1);

            SkontroOka_TextBox.Text = erec_UgyUgyiratok.SkontroOka;

            try
            {
                int skontrobanEddig = Int32.Parse(erec_UgyUgyiratok.SkontrobanOsszesen);
                SkontrobanOsszesen_RequiredNumberBox.Text = erec_UgyUgyiratok.SkontrobanOsszesen;

                if (!String.IsNullOrEmpty(erec_UgyUgyiratok.SkontrobaDat))
                {
                    DateTime skontroKezd = DateTime.Parse(erec_UgyUgyiratok.SkontrobaDat);

                    TimeSpan kulonbseg_TimeSpan = DateTime.Now.Subtract(skontroKezd);
                    int kulonbseg = kulonbseg_TimeSpan.Days;

                    if (kulonbseg > 0)
                    {
                        int skontrobanOsszesen = skontrobanEddig + kulonbseg;
                        SkontrobanOsszesen_RequiredNumberBox.Text = skontrobanOsszesen.ToString();
                    }
                }
            }
            catch
            {
                SkontrobanOsszesen_RequiredNumberBox.Text = erec_UgyUgyiratok.SkontrobanOsszesen;
            }

            IraIrattariTetelId_IraIrattariTetelTextBox.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;

            /// Ha irattározásra jóváhagyásra vár az ügyirat, megjelenítjük a jóváhagyót
            if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa
                || erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa
                || erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa
                || erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert && !String.IsNullOrEmpty(erec_UgyUgyiratok.Kovetkezo_Felelos_Id))
            {
                Jovahagyo_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Kovetkezo_Felelos_Id;
                Jovahagyo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                // Állapottól függő szöveg megjelenítés):
                Label_Jovahagyo.Text = GetJovahagyoTextByAllapot(erec_UgyUgyiratok.Allapot);

                // megjelenítjük:
                tr_jovahagyo.Visible = true;
                Label_Jovahagyo.Visible = true;
                Jovahagyo_CsoportTextBox.Visible = true;
            }

            LoadIrattariTetelComponents();

            LoadIrattariAdatok(erec_UgyUgyiratok);

            // Form Címének állítása
            if (FormHeader != null)
            {
                String fullFoszam = erec_UgyUgyiratok.Azonosito;

                if (Command == CommandName.View)
                {
                    FormHeader.FullManualHeaderTitle = fullFoszam + " " +
                        Resources.Form.Ugyiratmegtekintese_ManualFormHeaderTitle;
                }
                else if (Command == CommandName.Modify)
                {
                    FormHeader.FullManualHeaderTitle = fullFoszam + " " +
                        Resources.Form.Ugyiratmodositasa_ManualFormHeaderTitle;
                }

                FoszamLabel.Text = fullFoszam;

                // BLG_619
                FoszamLabel.Text = fullFoszam;
                if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
                {
                    ExecParam _ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    bool isTUK = Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.TUK, false);

                    #region LZS - BUG_11081
                    //LZS - BUG_11081
                    //Az SZK mező felületi megjelenítésénél TÜK rendszer esetén(TUK rendszer paraméter = 1), 
                    //és a FELELOS_SZERV_KOD_MEGJELENITES rsz.paraméter = 1, ha az EREC_UgyUgyiratok.Note mezőjében van SZK element, 
                    //akkor annak az értékét kell bele írni az SZK mezőbe.
                    //Ha nincs ilyen element, vagy üres az értéke, akkor a jelenlegi módon 
                    //(az adatbázisból lekérdezett érték alapján) kell megjeleníteni az értékét.
                    if (isTUK)
                    {
                        Note_JSON Note;

                        try
                        {
                            Note = JsonConvert.DeserializeObject<Note_JSON>(erec_UgyUgyiratok.Base.Note);
                        }
                        catch (Exception)
                        {
                            Note = null;
                        }

                        if (Note != null)
                        {
                            if (!string.IsNullOrEmpty(Note.SZK))
                            {
                                SzervezetiKodLabel.Text = Note.SZK;
                                SzervezetiKodLabel.Visible = true;

                                FormHeader.FullManualHeaderTitle = Note.SZK + " " + FormHeader.FullManualHeaderTitle;

                            }//Ha van a note mezőben bejegyzés, de abban nincs SZK érték:
                            else if (string.IsNullOrEmpty(Note.SZK))
                            {
                                FillSZK(ugyfelelos_szervezetkod, ParentId);

                                SzervezetiKodLabel.Text = ugyfelelos_szervezetkod;
                                SzervezetiKodLabel.Visible = true;
                                if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
                                    FormHeader.FullManualHeaderTitle = ugyfelelos_szervezetkod + " " + FormHeader.FullManualHeaderTitle;
                            }
                            else
                            {
                                SzervezetiKodLabel.Text = ugyfelelos_szervezetkod;
                                SzervezetiKodLabel.Visible = true;
                                if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
                                    FormHeader.FullManualHeaderTitle = ugyfelelos_szervezetkod + " " + FormHeader.FullManualHeaderTitle;
                            }
                        }
                        else
                        {
                            FillSZK(ugyfelelos_szervezetkod, ParentId);
                            SzervezetiKodLabel.Text = ugyfelelos_szervezetkod;
                            SzervezetiKodLabel.Visible = true;
                            if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
                                FormHeader.FullManualHeaderTitle = ugyfelelos_szervezetkod + " " + FormHeader.FullManualHeaderTitle;
                        }
                    }
                    else
                    {

                        SzervezetiKodLabel.Text = ugyfelelos_szervezetkod;
                        SzervezetiKodLabel.Visible = true;
                        if (!String.IsNullOrEmpty(ugyfelelos_szervezetkod))
                            FormHeader.FullManualHeaderTitle = ugyfelelos_szervezetkod + " " + FormHeader.FullManualHeaderTitle;
                    }
                    #endregion
                }
                else SzervezetiKodLabel.Visible = false;
                // FoszamLabel.Text = ugyfelelos_szervezetkod +" "+ fullFoszam;

            }

            Ugyazonosito_TextBox.Text = erec_UgyUgyiratok.Ugyazonosito;
            TextBoxIrattarHelyfoglalas.Text = erec_UgyUgyiratok.IrattarHelyfoglalas;
            UgykezeloRendszer_AlkalmazasTextBox.Id_HiddenField = erec_UgyUgyiratok.Alkalmazas_Id;
            UgykezeloRendszer_AlkalmazasTextBox.SetTextBoxById(EErrorPanel1);

            LezarasOka_KodtarakDropDownList.FillWithOneValue(kcs_LEZARAS_OKA, erec_UgyUgyiratok.LezarasOka, EErrorPanel1);

            /// 2018.08.31; KA
            /// Egy eltűnt kódrészlet visszahelyezve (a 2018.04.11. 15:12:20 -as commitban tűnt el valamiért (sokadmagával együtt...))
            /// 
            if (erec_UgyUgyiratok.Allapot != KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett)
            {
                trFelfuggesztesOka.Visible = false;
            }
            else
            {
                trFelfuggesztesOka.Visible = true;
                TextBoxFelfuggesztesOka.Text = erec_UgyUgyiratok.FelfuggesztesOka;
                TextBoxFelfuggesztesOka.ReadOnly = true;
            }

            // verzió elmentése
            Record_Ver_HiddenField.Value = erec_UgyUgyiratok.Base.Ver;
            //Betöltött állapot elmentése kliens oldalra
            IsUgyiratLoaded = true;

            SetHataridoControlEnabled(false);
        }
    }

    private void FillSZK(string db_UgyFelelos_SzervezetKod, string UgyiratId)
    {
        if (!String.IsNullOrEmpty(UgyiratId))
        {
            Note_JSON note = new Note_JSON();
            note.SZK = db_UgyFelelos_SzervezetKod;

            EREC_UgyUgyiratokService serviceUgyUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgyUgyiratok = UI.SetExecParamDefault(Page, new ExecParam());
            execParamUgyUgyiratok.Record_Id = UgyiratId;

            Result resGet = serviceUgyUgyiratok.Get(execParamUgyUgyiratok);

            if (resGet.IsError)
                return;

            EREC_UgyUgyiratok eREC_UgyUgyirat = resGet.Record as EREC_UgyUgyiratok;
            eREC_UgyUgyirat.Updated.SetValueAll(false);
            eREC_UgyUgyirat.Base.Updated.SetValueAll(false);
            eREC_UgyUgyirat.Base.Updated.Ver = true;

            eREC_UgyUgyirat.Base.Note = JsonConvert.SerializeObject(note);
            eREC_UgyUgyirat.Base.Updated.Note = true;

            Result resUpdate = serviceUgyUgyiratok.Update(execParamUgyUgyiratok, eREC_UgyUgyirat);

            //Hibakezelés, megjelenítés
            if (resUpdate.IsError)
            {
                return;
            }
        }
    }

    EREC_IrattariHelyek GetIrattariHelyById(string irattarId)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        ExecParam.Record_Id = irattarId;
        Result res = service.Get(ExecParam);

        if (!res.IsError && res.Record != null)
        {
            EREC_IrattariHelyek hely = (EREC_IrattariHelyek)res.Record;
            return hely;
        }
        return null;
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string value)
    {
        if (Command == CommandName.View)
        {
            if (string.IsNullOrEmpty(value))
                return;

            EREC_IrattariHelyek hely = GetIrattariHelyById(value);

            if (hely == null)
                Logger.Debug("Irattári hely nem található. IrattarId: " + hely.Id);

            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Clear();
            IrattariHelyLevelekDropDownTUK.DropDownList.Items.Add(new ListItem(hely.Ertek, value));
            return;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = ExecParam.Felhasznalo_Id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }
                Csoportok.Add(ExecParam.Felhasznalo_Id);
                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    if (!string.IsNullOrEmpty(value))
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(value);

                }
            }
        }
    }
    private void LoadIrattariAdatok(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        CalendarControlIrattarbaHelyezes.Text = erec_UgyUgyiratok.IrattarbaKuldDatuma;
        CalendarControlIrattarbaVetel.Text = erec_UgyUgyiratok.IrattarbaVetelDat;
        FelhasznaloCsoportTextBoxIrattarbaVevo.Id_HiddenField = erec_UgyUgyiratok.FelhCsoport_Id_IrattariAtvevo;
        FelhasznaloCsoportTextBoxIrattarbaVevo.SetCsoportTextBoxById(EErrorPanel1);
        #region CR3206 - Irattári struktúra
        // CR3246 Irattári Helyek kezelésének módosítása
        // Irattári hely nem módosítható
        if (isTUKRendszer)
        {
            if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IrattarId))
                IrattariHelyLevelekDropDownTUK.SetSelectedValue(erec_UgyUgyiratok.IrattarId);
        }
        else
        {
            IrattariHely_TextBox.Text = erec_UgyUgyiratok.IrattariHely;
        }
        #endregion
        CalendarControlSelejtezes.Text = erec_UgyUgyiratok.SelejtezesDat;
        FelhasznaloCsoportTextBoxSelejtezoAdo.Id_HiddenField = erec_UgyUgyiratok.FelhCsoport_Id_Selejtezo;
        FelhasznaloCsoportTextBoxSelejtezoAdo.SetCsoportTextBoxById(EErrorPanel1);
        Leveltariatvevo_TextBox.Text = erec_UgyUgyiratok.LeveltariAtvevoNeve;
        MegorzesiIdoVege_CalendarControl.Text = erec_UgyUgyiratok.MegorzesiIdoVege;

    }

    private void LoadIrattariTetelComponents()
    {
        // BLG_439
        //  if (!String.IsNullOrEmpty(IraIrattariTetelId_IraIrattariTetelTextBox.Id_HiddenField))
        //  {
        DataRow row_irattariTetel = IraIrattariTetelId_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxWithExtensionsById(EErrorPanel1);

        if (row_irattariTetel != null)
        {
            //IraIttl_IrattariJel_KodtarakDropDownList.FillWithOneValue(kcs_IRATTARI_JEL,
            //    row_irattariTetel["IrattariJel"].ToString(), EErrorPanel1);

            //IraIttl_Nev_TextBox.Text = row_irattariTetel["Nev"].ToString();
        }

        //EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //execParam.Record_Id = IraIrattariTetelId_IraIrattariTetelTextBox.Id_HiddenField;

        //Result result = service.Get(execParam);
        //if (String.IsNullOrEmpty(result.ErrorCode))
        //{
        //    if (result.Record != null)
        //    {
        //        EREC_IraIrattariTetelek erec_IraIrattariTetel = (EREC_IraIrattariTetelek)result.Record;

        //        IraIrattariTetelId_IraIrattariTetelTextBox.Text = erec_IraIrattariTetel.IrattariTetelszam;

        //        IraIttl_IrattariJel_KodtarakDropDownList.FillWithOneValue(kcs_IRATTARI_JEL,
        //            erec_IraIrattariTetel.IrattariJel, EErrorPanel1);

        //        IraIttl_Nev_TextBox.Text = erec_IraIrattariTetel.Nev;
        //        IraIttl_MegorzesiIdo_RequiredNumberBox.Text = erec_IraIrattariTetel.MegorzesiIdo;
        //    }
        //}
        //else
        //{
        //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //    ErrorUpdatePanel1.Update();
        //}
        // }
    }

    // form --> business object
    private EREC_UgyUgyiratok GetBusinessObjectFromComponents()
    {
        // TODO: 
        // Csak azokat a mezőket állítjuk be, amik módosíthatók

        EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
        // összes mező update-elhetőségét kezdetben letiltani:
        erec_UgyUgyiratok.Updated.SetValueAll(false);
        erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

        erec_UgyUgyiratok.Targy = Environment.NewLine + Targy_TextBox.Text;
        erec_UgyUgyiratok.Updated.Targy = pageView.GetUpdatedByView(Targy_TextBox);

        #region BUG_7099 - obsolate
        //LZS - BUG_7099 - obsolate
        //Megjegyzés mező összerakása a mentéshez. Letárolva a EREC_UgyUgyiratok.Note mezőbe lesz. A korábbi (sortörés előtti) szöveget a "hiddenUgyiratNote.Value"
        //tartalmazza, ehhez fűzzük hozzá az ügyirat megjegyzés "txtUgyiratMegjegyzes.Text" szövegét egy sortörést kövezően.
        //erec_UgyUgyiratok.Base.Note = hiddenUgyiratNote.Value + Environment.NewLine + txtUgyiratMegjegyzes.Text;
        //erec_UgyUgyiratok.Base.Updated.Note = pageView.GetUpdatedByView(txtUgyiratMegjegyzes);
        #endregion

        #region BUG_11081
        //LZS - BUG_11081
        //Megjegyzés mező összerakása a mentéshez. Letárolva a EREC_UgyUgyiratok.Note mezőbe lesz. 
        //Az eredeti értéket a "hiddenUgyiratNote.Value" tárolja.
        //Ezt deserializálva megkapjuk objektumként:
        Note_JSON Note;
        try
        {
            Note = JsonConvert.DeserializeObject<Note_JSON>(hiddenUgyiratNote.Value);
        }
        catch (Exception)
        {
            Note = null;
        }

        if (Note == null)
            Note = new Note_JSON();

        Note.Megjegyzes = txtUgyiratMegjegyzes.Text;
        //Visszamentjük UPDATE-el.
        erec_UgyUgyiratok.Base.Note = JsonConvert.SerializeObject(Note);
        erec_UgyUgyiratok.Base.Updated.Note = pageView.GetUpdatedByView(txtUgyiratMegjegyzes);
        #endregion

        erec_UgyUgyiratok.UgyTipus = Ugytipus_DropDownList.SelectedValue;
        erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(Ugytipus_DropDownList);

        erec_UgyUgyiratok.UgyintezesModja = KezelesModja_KodtarakDropDownList.SelectedValue;
        erec_UgyUgyiratok.Updated.UgyintezesModja = pageView.GetUpdatedByView(KezelesModja_KodtarakDropDownList);

        erec_UgyUgyiratok.Jelleg = UgyiratFajtaja_KodtarakDropDownList.SelectedValue;
        erec_UgyUgyiratok.Updated.Jelleg = pageView.GetUpdatedByView(UgyiratFajtaja_KodtarakDropDownList);

        erec_UgyUgyiratok.Hatarido = Hatarido_CalendarControl.Text;
        erec_UgyUgyiratok.Updated.Hatarido = pageView.GetUpdatedByView(Hatarido_CalendarControl);

        //erec_UgyUgyiratok.IktatoszamKieg = IktatoszamKieg_KodtarakDropDownList.SelectedValue;
        //erec_UgyUgyiratok.Updated.IktatoszamKieg = pageView.GetUpdatedByView(IktatoszamKieg_KodtarakDropDownList);

        erec_UgyUgyiratok.Surgosseg = Surgosseg_KodtarakDropDownList.SelectedValue;
        erec_UgyUgyiratok.Updated.Surgosseg = pageView.GetUpdatedByView(Surgosseg_KodtarakDropDownList);

        erec_UgyUgyiratok.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
        erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
        erec_UgyUgyiratok.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
        erec_UgyUgyiratok.Updated.NevSTR_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);

        erec_UgyUgyiratok.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
        erec_UgyUgyiratok.Updated.Cim_Id_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);
        erec_UgyUgyiratok.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;
        erec_UgyUgyiratok.Updated.CimSTR_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);

        erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox);

        erec_UgyUgyiratok.IraIrattariTetel_Id = IraIrattariTetelId_IraIrattariTetelTextBox.Id_HiddenField;
        erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(IraIrattariTetelId_IraIrattariTetelTextBox);

        #region CR3206 - Irattári struktúra
        //Irattári Adatok
        // CR3246 Irattári Helyek kezelésének módosítása
        // Irattári hely nem módosítható
        if (isTUKRendszer)
        {
            erec_UgyUgyiratok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_UgyUgyiratok.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            erec_UgyUgyiratok.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
            erec_UgyUgyiratok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
        }

        //erec_UgyUgyiratok.IrattariHely = IrattariHely_TextBox.Text;
        //erec_UgyUgyiratok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHely_TextBox);
        #endregion

        erec_UgyUgyiratok.LeveltariAtvevoNeve = Leveltariatvevo_TextBox.Text;
        erec_UgyUgyiratok.Updated.LeveltariAtvevoNeve = pageView.GetUpdatedByView(Leveltariatvevo_TextBox);

        // Ügyfelelős
        if (UgyfelelosModifyEnabled)
        {
            erec_UgyUgyiratok.Csoport_Id_Ugyfelelos = CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = pageView.GetUpdatedByView(CsopId_UgyFelelos_CsoportTextBox);

            #region LZS - BUG_11081
            //LZS
            if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
            {
                Note_JSON note;

                try
                {
                    note = JsonConvert.DeserializeObject<Note_JSON>(erec_UgyUgyiratok.Base.Note);
                }
                catch (Exception)
                {
                    note = null;
                }

                bool SZK_Update = note == null;

                if (note != null)
                {
                    SZK_Update = string.IsNullOrEmpty(note.SZK);
                }

                //LZS - BUG_11081
                //Amennyiben a FELELOS_SZERV_KOD_MEGJELENITES = 1, akkor vizsgálni kell, hogy a Note mezőben már van-e SZK element (és ki van töltve az értéke).
                //Amennyiben nincs SZK element, vagy nincs érték megadva hozzá(NULL vagy empty string), 
                if (SZK_Update)
                {
                    if (!String.IsNullOrEmpty(CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField))
                    {
                        //szervezet_Id alapján lekérjük a csoportot, majd használjuk a kódját:
                        KRT_CsoportokService krtKodCsoportService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                        ExecParam execParamKRT = UI.SetExecParamDefault(Page, new ExecParam());
                        execParamKRT.Record_Id = CsopId_UgyFelelos_CsoportTextBox.Id_HiddenField;

                        KRT_Csoportok krt_csoport = (KRT_Csoportok)krtKodCsoportService.Get(execParamKRT).Record;

                        //akkor a Note mezőbe is fel kell venni a CsoportId_Ugyfelelos mezőbe megadott csoport kódját(KRT_Csoportok.Kod)
                        string SZK = krt_csoport.Kod;

                        if (note == null)
                            note = new Note_JSON();

                        note.SZK = SZK;

                        erec_UgyUgyiratok.Base.Note = JsonConvert.SerializeObject(note);
                        erec_UgyUgyiratok.Base.Updated.Note = true;
                    }
                }
                #endregion

            }
        }

        erec_UgyUgyiratok.Ugyazonosito = Ugyazonosito_TextBox.Text;
        erec_UgyUgyiratok.Updated.Ugyazonosito = pageView.GetUpdatedByView(Ugyazonosito_TextBox);

        erec_UgyUgyiratok.IrattarHelyfoglalas = TextBoxIrattarHelyfoglalas.Text;
        erec_UgyUgyiratok.Updated.IrattarHelyfoglalas = pageView.GetUpdatedByView(TextBoxIrattarHelyfoglalas);

        // BLG_439
        erec_UgyUgyiratok.Ugy_Fajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
        erec_UgyUgyiratok.Updated.Ugy_Fajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);


        // verzió visszaolvasása
        erec_UgyUgyiratok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_UgyUgyiratok.Base.Updated.Ver = true;

        // BLG_1348
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            LoadUgyintezesiNapokBoFromComponents(ref erec_UgyUgyiratok);
            //erec_UgyUgyiratok.IntezesiIdo = IntezesiIdo_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.IntezesiIdo = pageView.GetUpdatedByView(IntezesiIdo_KodtarakDropDownList);
            erec_UgyUgyiratok.IntezesiIdoegyseg = IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = pageView.GetUpdatedByView(IntezesiIdoegyseg_KodtarakDropDownList);
        }

        // BLG_8826
        _iratForm.UpdateUgyiratUgyintezesKezdete(erec_UgyUgyiratok);

        if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page)))
        {
            Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(UI.SetExecParamDefault(Page), ref erec_UgyUgyiratok);
        }

        return erec_UgyUgyiratok;

    }


    private void Load_ComponentSelectModul()
    {
        /*if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            // TODO: felkell sorolni...
            /*compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }*/
    }






    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
          e.CommandName == CommandName.SaveAndClose || e.CommandName == CommandName.SaveAndNew)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Ugyirat" + Command)
                || FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + Command)
                || FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + Command))
            {
                bool voltHiba = false;

                switch (Command)
                {
                    case CommandName.New:
                        {
                            // New nem lehetséges
                            return;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                                voltHiba = true;
                            }
                            else
                            {
                                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                                execparam.Record_Id = ParentId;


                                EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)service.Get(execparam).Record;
                                //LZS - BUG_11672
                                Record_Ver_HiddenField.Value = erec_UgyUgyiratok.Base.Ver;
                                erec_UgyUgyiratok = GetBusinessObjectFromComponents();



                                Result result_update = service.Update(execparam, erec_UgyUgyiratok);
                                if (!String.IsNullOrEmpty(result_update.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_update);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                                else
                                {
                                    #region EREC_ObjektumTargyszavai UPDATE

                                    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak.GetEREC_ObjektumTargyszavaiList(true);

                                    if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                                    {
                                        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

                                        Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                            , EREC_ObjektumTargyszavaiList
                                            , ParentId
                                            , null
                                            , Constants.TableNames.EREC_UgyUgyiratok
                                            , "B1"
                                            , false
                                            );

                                        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                        {
                                            // hiba
                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                            ErrorUpdatePanel1.Update();
                                            //voltHiba = true; // TODO: ??? true or false ???
                                        }

                                    }

                                    #endregion EREC_ObjektumTargyszavai UPDATE

                                    #region EREC_IraOnkormAdatok UPDATE

                                    //// hatósági adatok módosítása, ha változott az
                                    //// irattári tételszám /ügytípus
                                    // -> átkerült az EREC_UgyUgyiratokService Update metódusba

                                    #endregion

                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    //// újratöltjük a módosítás oldalt
                                    //UI.popupRedirect(Page, "UgyUgyiratokForm.aspx?"
                                    //       + QueryStringVars.Command + "=" + CommandName.Modify
                                    //       + "&" + QueryStringVars.Id + "=" + ParentId);

                                    //Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    ReLoadTab(true);
                                    RaiseEvent_OnChangedObjectProperties();
                                    return;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }

                            }

                            break;

                        }

                    case CommandName.View:
                        {
                            if (IrattarbolModositas)
                            {
                                goto case "Modify";
                            }
                        }
                        break;

                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }

    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IrattarbolModositas)
        {
            FormHeader.HeaderTitle = "Irattári adatok módosítása";
        }

        if (Command == CommandName.Modify)
        {
            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterByIktatokonyv(IktatokonyvId_HiddenField);
            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterBySzervezet(CsopId_UgyFelelos_CsoportTextBox);

            if (UgyiratFajtaja_KodtarakDropDownList.SelectedValue == KodTarak.UGYIRAT_JELLEG.Vegyes)
            {
                KezelesModja_KodtarakDropDownList.ReadOnly = false;
            }
        }

        if (IrattariAdatok_EFormPanel.Visible)
        {
            if (IraIrattariTetelId_IraIrattariTetelTextBox.IrattariJel == UI.JegyzekTipus.SelejtezesiJegyzek.Value)
            {
                labelLeveltariAtvevo.Text = Rendszerparameterek.IsTUK(Page) ? stringBizottsagiTagok : stringEllenor;
                labelLeveltarbaAdas.Text = stringSelejtezes;
                labelLeveltarbaAdo.Text = stringSelejtezo;
                labelMegorzesiIdoVege.Text = stringSelejtezesiIdo;

            }
            else
            {
                labelLeveltariAtvevo.Text = stringLeveltariAtvevo;
                labelLeveltarbaAdas.Text = stringLeveltarbaAdas;
                labelLeveltarbaAdo.Text = stringLeveltarbaAdo;
                labelMegorzesiIdoVege.Text = stringLeveltarbaAdasiIdo;
            }
        }

        trLezarasOka.Visible = LezarasOkaVisible;
    }

    #endregion

    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(UgyForm);
        componentList.AddRange(PageView.GetSelectableChildComponents(UgyForm.Controls));
        componentList.Add(IrattariTetel_EFormPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IrattariTetel_EFormPanel.Controls));
        componentList.Add(IrattariAdatok_EFormPanel);
        componentList.AddRange(PageView.GetSelectableChildComponents(IrattariAdatok_EFormPanel.Controls));
        return componentList;
    }

    #endregion

    #region HA HATOSAGI UGY
    /// <summary>
    /// SetUgyControlsDisableIfHatosagi
    /// </summary>
    /// <param name="ugy"></param>
    private void SetUgyControlsDisableIfHatosagi(EREC_UgyUgyiratok ugy)
    {
        if (ugy == null || string.IsNullOrEmpty(ugy.Ugy_Fajtaja))
            return;
        ExecParam xpm = UI.SetExecParamDefault(Page);

        if (Contentum.eUtility.Sakkora.IsUgyFajtaHatosagi(xpm, ugy.Ugy_Fajtaja))
        {
            Hatarido_CalendarControl.Enabled = false;
            Hatarido_CalendarControl.ReadOnly = true;
        }
    }
    #endregion

    #region IDO
    private void LoadUgyintezesiNapokFromBo(EREC_UgyUgyiratok ugy)
    {
        inputUgyintezesiNapok.Value = ugy.IntezesiIdo;
    }
    private void LoadUgyintezesiNapokBoFromComponents(ref EREC_UgyUgyiratok ugy)
    {
        CheckUgyintezesiNapok();
        ugy.IntezesiIdo = inputUgyintezesiNapok.Value;
        ugy.Updated.IntezesiIdo = true;
    }
    private bool CheckUgyintezesiNapok()
    {
        if (string.IsNullOrEmpty(inputUgyintezesiNapok.Value))
            return true;

        int value;
        if (int.TryParse(inputUgyintezesiNapok.Value, out value))
        {
            if (value < 0)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageUgyintzesiIdoNegativ);
                ErrorUpdatePanel1.Update();
                throw new Exception(Resources.Error.MessageUgyintzesiIdoNegativ);
            }
        }
        return true;
    }
    /// <summary>
    /// InitUgyintezesiNapok
    /// </summary>
    /// <param name="execParam"></param>
    public void InitUgyintezesiNapok(ExecParam execParam)
    {
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
          Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYIRAT_INTEZESI_IDO, Page, null);

        var builder = new System.Text.StringBuilder();
        string kodtarKod;
        string kodtarNev;
        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
        {
            kodtarKod = kte.Kod; // Key a kódtár kód
            kodtarNev = kte.Nev; // Value a kódtárérték neve
            builder.Append(String.Format("<option value='{0}'>", kodtarKod));
        }
        datalistUgyintezesiNapok.InnerHtml = builder.ToString();
        inputUgyintezesiNapok.Attributes["list"] = datalistUgyintezesiNapok.ClientID;
    }
    #endregion
    /// <summary>
    /// SetHataridoControlEnabled
    /// </summary>
    /// <param name="enabled"></param>
    private void SetHataridoControlEnabled(bool enabled)
    {
        if (enabled)
        {
            Hatarido_CalendarControl.Enabled = true;
            //Hatarido_CalendarControl.ReadOnly = false;
        }
        else
        {
            Hatarido_CalendarControl.Enabled = false;
            //Hatarido_CalendarControl.ReadOnly = true;
        }
    }
}
