<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratDarabMasterAdatok.ascx.cs" Inherits="eRecordComponent_UgyiratDarabMasterAdatok" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc8" %>
<%@ Register Src="UgyiratDarabTextBox.ascx" TagName="UgyiratDarabTextBox" TagPrefix="uc7" %>


<%@ Register Src="UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc6" %>

<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc4" %>
<%@ Register Src="IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc2" %>
<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <eUI:eFormPanel ID="UgyiratDarabForm" runat="server">
                    <table id="MainTable" cellspacing="0" cellpadding="0" width="100%" runat="server">
                        <tbody>
                            <tr class="urlapSor_kicsi" id="trUgyiratdarab" runat="server">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label2" runat="server" Text="�gyiratdarab:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc7:UgyiratDarabTextBox ID="UgyiratDarabTextBox1" runat="server" ReadOnly="true"
                                        ViewMode="true"></uc7:UgyiratDarabTextBox>
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label_Leiras" runat="server" Text="Le�r�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Leiras_TextBox" runat="server" ReadOnly="True" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor_kicsi" id="trEljarasiSzakasz" runat="server">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label_eljarasiszakasz" runat="server" Text="Elj�r�si szakasz:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc2:KodtarakDropDownList ID="EljarasiSzakasz_KodtarakDropDownList" runat="server" ReadOnly="true"></uc2:KodtarakDropDownList>
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Allapot_felirat" runat="server" Text="�llapot:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true"></uc2:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" id="trKezelo" runat="server">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Felelos_felirat" runat="server" Text="Kezel�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc6:CsoportTextBox ID="CsoportId_Felelos_CsoportTextBox" runat="server" ReadOnly="true"
                                        ViewMode="true"></uc6:CsoportTextBox>
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Ugyintezo_felirat" runat="server" Text="�gyint�z�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc5:FelhasznaloCsoportTextBox ID="FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                        runat="server" ReadOnly="true" ViewMode="true"></uc5:FelhasznaloCsoportTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" id="trHatarido" runat="server">
                                <td class="mrUrlapCaption_short">
                                    &nbsp;<asp:Label ID="Label1" runat="server" Text="Hat�rid�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc8:CalendarControl ID="Hatarido_CalendarControl" runat="server" ReadOnly="true" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                </td>
                                <td class="mrUrlapMezo">
                                    &nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
</asp:Panel>
