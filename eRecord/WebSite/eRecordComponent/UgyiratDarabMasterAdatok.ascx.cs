using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class eRecordComponent_UgyiratDarabMasterAdatok : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    #region public properties

    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_UGYIRATDARAB_ALLAPOT = "UGYIRATDARAB_ALLAPOT";


    public String UgyiratDarabId
    {
        get { return UgyiratDarabTextBox1.Id_HiddenField; }
        set { UgyiratDarabTextBox1.Id_HiddenField = value; }
    }



    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetComponentsVisibility();
    }

    private void SetComponentsVisibility()
    {
        EljarasiSzakasz_KodtarakDropDownList.ReadOnly = true;
        Allapot_KodtarakDropDownList.ReadOnly = true;

        Leiras_TextBox.ReadOnly = true;
    }

    public EREC_UgyUgyiratdarabok SetUgyiratDarabMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        return SetUgyiratDarabMasterAdatokById(errorPanel, null);
    }

    public EREC_UgyUgyiratdarabok SetUgyiratDarabMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(UgyiratDarabId))
        {
            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = UgyiratDarabId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result.Record;
                    
                    UgyiratDarabTextBox1.Id_HiddenField = erec_UgyUgyiratdarabok.Id;
                    UgyiratDarabTextBox1.SetUgyiratDarabTextBoxById(errorPanel);

                    Leiras_TextBox.Text = erec_UgyUgyiratdarabok.Leiras;

                    EljarasiSzakasz_KodtarakDropDownList.FillWithOneValue(kcs_ELJARASI_SZAKASZ
                        , erec_UgyUgyiratdarabok.EljarasiSzakasz, errorPanel);

                    Allapot_KodtarakDropDownList.FillWithOneValue(kcs_UGYIRATDARAB_ALLAPOT,
                        erec_UgyUgyiratdarabok.Allapot, errorPanel);
                    
                    FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez;
                    FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);

                    CsoportId_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratdarabok.Csoport_Id_Felelos;
                    CsoportId_Felelos_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

                    Hatarido_CalendarControl.Text = erec_UgyUgyiratdarabok.Hatarido;

                    return erec_UgyUgyiratdarabok;
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        return null;
    }

    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        System.Collections.Generic.List<Control> componentList = new System.Collections.Generic.List<Control>();
        componentList.Add(UgyiratDarabForm);
        componentList.AddRange(Contentum.eUtility.PageView.GetSelectableChildComponents(UgyiratDarabForm.Controls));
        return componentList;
    }

    #endregion
}