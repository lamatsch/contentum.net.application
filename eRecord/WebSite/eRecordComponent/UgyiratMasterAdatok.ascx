<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratMasterAdatok.ascx.cs" 
Inherits="eRecordComponent_UgyiratMasterAdatok" %>
<%@ Register Src="UgyiratTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc1" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="../Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc6" %>

<%@ Register Src="../Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc4" %>
<%@ Register Src="IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc3" %>
<%@ Register Src="../Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc2" %>


<asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>   
   <eUI:eFormPanel ID="UgyForm" runat="server">
       <table cellpadding="0" cellspacing="0" runat="server" id="MainTable" width="100%">
           <tr class="urlapSor" id="trUgyirat" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label2" runat="server" Text="�gyirat:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc1:UgyiratTextBox ID="UgyiratTextBox1" runat="server" ViewMode="true" ReadOnly="true" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Ugyirat_targya_felirat" runat="server" Text="�gyirat t�rgya:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <asp:TextBox ID="Targy_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="True"></asp:TextBox>
               </td>
           </tr>
           <tr class="urlapSor" id="trFelelos" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Label1" runat="server" Text="Felel�s:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc6:CsoportTextBox ID="Ugyfelelos_CsoportTextBox" runat="server" ViewMode="true"
                       ReadOnly="true" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Ugyintezo_felirat" runat="server" Text="�gyint�z�:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc5:FelhasznaloCsoportTextBox ID="FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                       runat="server" ViewMode="true" ReadOnly="true" />
               </td>
           </tr>
           <tr class="urlapSor" id="trKezelo" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Felelos_felirat" runat="server" Text="Kezel�:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc6:CsoportTextBox ID="CsoportId_Felelos_CsoportTextBox" runat="server" ViewMode="true"
                       ReadOnly="true" />
               </td>
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Orzo_felirat" runat="server" Text="�gyirat helye:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc5:FelhasznaloCsoportTextBox ID="FelhCsopId_Orzo_FelhasznaloCsoportTextBox" runat="server"
                       ViewMode="true" ReadOnly="true" />
               </td>
           </tr>
           <tr class="urlapSor" id="trAllapot" runat="server">
               <td class="mrUrlapCaption_short">
                   <asp:Label ID="Allapot_felirat" runat="server" Text="�llapot:"></asp:Label>
               </td>
               <td class="mrUrlapMezo">
                   <uc2:KodtarakDropDownList ID="Allapot_KodtarakDropDownList" runat="server" ReadOnly="true"/>
               </td>
               <td class="mrUrlapCaption_short">
               </td>
               <td class="mrUrlapMezo">
               </td>
           </tr>
       </table>
   </eUI:eFormPanel>
  </td>    
</tr>
</table>
</asp:Panel>