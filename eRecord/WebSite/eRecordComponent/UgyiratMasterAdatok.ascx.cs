using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;


public partial class eRecordComponent_UgyiratMasterAdatok : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    #region public properties

    private const string kcs_UGYTIPUS = "UGYTIPUS";
    private const string kcs_UGYIRAT_ALLAPOT = "UGYIRAT_ALLAPOT";
    private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";

    public String UgyiratId
    {
        get { return UgyiratTextBox1.Id_HiddenField; }
        set { UgyiratTextBox1.Id_HiddenField = value; }
    }

    public string UgyiratFoszam
    {
        get
        {
            return UgyiratTextBox1.Text;
        }
    }
	


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        SetComponentsVisibility();
    }

    private void SetComponentsVisibility()
    {
    }

    public EREC_UgyUgyiratok SetUgyiratMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        return SetUgyiratMasterAdatokById(errorPanel, null);
    }

    public EREC_UgyUgyiratok SetUgyiratMasterAdatokById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(UgyiratId))
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = UgyiratId;

            Result result = service.GetWithRightCheck(execParam, '0');
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record == null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                        ResultError.CreateNewResultWithErrorCode(50101));
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
                else
                {
                    EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;

                    this.SetUgyiratMasterAdatokByBusinessObject(erec_UgyUgyiratok, errorPanel, errorUpdatePanel);

                    return erec_UgyUgyiratok;
                }
                
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }            
        }
        return null;
    }

    public void SetUgyiratMasterAdatokByBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (erec_UgyUgyiratok != null)
        {

            UgyiratTextBox1.Id_HiddenField = erec_UgyUgyiratok.Id;
            UgyiratTextBox1.Text = erec_UgyUgyiratok.Azonosito;
            //UgyiratTextBox1.SetUgyiratTextBoxById(errorPanel, errorUpdatePanel);

            Targy_TextBox.Text = erec_UgyUgyiratok.Targy;

            //IktatoSzamKieg_KodtarakDropDownList.FillWithOneValue(kcs_IKTATOSZAM_KIEG,
            //    erec_UgyUgyiratok.IktatoszamKieg, errorPanel);

            //Ugytipus_KodtarakDropDownList.FillWithOneValue(kcs_UGYTIPUS,
            //    erec_UgyUgyiratok.UgyTipus, errorPanel);

            Ugyfelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Ugyfelelos;
            Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

            Allapot_KodtarakDropDownList.FillWithOneValue(kcs_UGYIRAT_ALLAPOT,
                erec_UgyUgyiratok.Allapot, errorPanel);

            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);

            CsoportId_Felelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Felelos;
            CsoportId_Felelos_CsoportTextBox.SetCsoportTextBoxById(errorPanel);

            FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;
            FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(errorPanel);

        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(errorPanel, "Hiba!","�gyirat �zleti objektum �res!");
            if (errorUpdatePanel != null)
            {
                errorUpdatePanel.Update();
            }
        }
    }


    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        componentList.Add(UgyForm);
        componentList.AddRange(Contentum.eUtility.PageView.GetSelectableChildComponents(UgyForm.Controls));
        return componentList;
    }

    #endregion
}
