<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratSimpleSearchComponent.ascx.cs" Inherits="eRecordComponent_UgyiratSimpleSearchComponent" %>
<%@ Register Src="IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<div class="DisableWrap" id="ContainerDiv" runat="server">
	<uc7:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList" Mode="Iktatokonyvek" EvTextBoxId="TextBoxEv"
	    runat="server" ToolTip="Iktat�k�nyv" />
	/
	<asp:TextBox ID="TextBoxFoszam" CssClass="mrUrlapInputNumber" runat="server" ToolTip="F�sz�m" />
	/
	<asp:TextBox ID="TextBoxEv" CssClass="mrUrlapInputNumber" runat="server" ToolTip="�v" MaxLength="4" />
</div>
<ajaxToolkit:FilteredTextBoxExtender ID="ftbFoszam" FilterType="Numbers" TargetControlID="TextBoxFoszam" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="ftbEv" FilterType="Numbers" TargetControlID="TextBoxEv" runat="server" />