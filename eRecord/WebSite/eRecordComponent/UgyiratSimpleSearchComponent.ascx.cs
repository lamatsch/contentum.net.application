using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class eRecordComponent_UgyiratSimpleSearchComponent : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties


    public bool Enabled
    {
        set
        {
            IktatoKonyvekDropDownList.Enabled = value;
            TextBoxEv.Enabled = value;
            TextBoxFoszam.Enabled = value;
        }
        get
        {
            return IktatoKonyvekDropDownList.Enabled;
        }
    }

    public bool ReadOnly
    {
        set
        {
            IktatoKonyvekDropDownList.ReadOnly = value;
            TextBoxEv.ReadOnly = value;
            TextBoxFoszam.ReadOnly = value;
        }
        get { return IktatoKonyvekDropDownList.ReadOnly; }
    }

    public string Text
    {
        get
        {
            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue)
                || !String.IsNullOrEmpty(this.Foszam)
                || !String.IsNullOrEmpty(this.Ev))
            {
                string[] IktatokonyvJelzes = IktatoKonyvekDropDownList.SelectedValue.Split(new string[] { IktatoKonyvek.valueDelimeter }, StringSplitOptions.None);
                string txtIktatohely = (IktatokonyvJelzes.Length > 0) ? IktatokonyvJelzes[0] : String.Empty;
                if (String.IsNullOrEmpty(txtIktatohely))
                {
                    txtIktatohely = "?";
                }
                string txtAzonosito = (IktatokonyvJelzes.Length > 1) ? IktatokonyvJelzes[1] : String.Empty;
                string txtFoszam = (String.IsNullOrEmpty(this.Foszam) ? "?" : this.Foszam);
                string txtEv = (String.IsNullOrEmpty(this.Ev) ? "?" : this.Ev);
                return txtIktatohely + " /" + txtFoszam + " /" + txtEv + (!String.IsNullOrEmpty(txtAzonosito) ? " /" + txtAzonosito : "");
            }
            else
            {
                return "";
            }
        }

    }

    private int _AlapertelmezettEv = 0;
    public int AlapertelmezettEv
    {
        get { return _AlapertelmezettEv; }
        set { _AlapertelmezettEv = value; }
    }

    public string IktatoKonyv
    {
        set { IktatoKonyvekDropDownList.SetSelectedValue(value); }
        get { return IktatoKonyvekDropDownList.SelectedValue; }
    }

    public string Foszam
    {
        set { TextBoxFoszam.Text = value; }
        get { return TextBoxFoszam.Text; }
    }

    public string Ev
    {
        set { TextBoxEv.Text = value; }
        get { return TextBoxEv.Text; }
    }

    public DropDownList IktatoKonyvDropDownList
    {
        get { return IktatoKonyvekDropDownList.DropDownList; }
    }

    public TextBox FoszamTextBox
    {
        get { return TextBoxFoszam; }
    }

    public TextBox EvTextBox
    {
        get { return TextBoxEv; }
    }

    public HtmlControl Container
    {
        get { return this.ContainerDiv; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                IktatoKonyvekDropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //JavaScripts.RegisterPopupWindowClientScript(Page);

        //ASP.NET 2.0 bug work around
        //TextBoxFoszam.Attributes.Add("readonly", "readonly");
        //TextBoxEv.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    // ha a kit�ltend� mez�knek megfelel� �rt�kek k�z�l mindegyik �res, akkor defaultnak tekintj�k
    protected bool IsDefaultSearchObject(EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch)
    {
        bool isDefaultSearchObject = true;
        if (erec_UgyUgyiratokSearch != null)
        {
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Foszam.Value);
            isDefaultSearchObject &= String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Foszam.ValueTo);
            if (erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                isDefaultSearchObject &= String.IsNullOrEmpty(IktatoKonyvek.GetIktatokonyvValue(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch));
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value);
                isDefaultSearchObject &= String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo);

            }
        }
        return isDefaultSearchObject;
    }

    public void FillIktatoKonyvek(string MegkulJelzes, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        IktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                       , this.Ev, this.Ev
                       , true, false, MegkulJelzes, ErrorPanel);
    }

    public void FillSearchObjectFromComponents(ref EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch)
    {
        if (erec_UgyUgyiratokSearch != null)
        {
            if (erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }

            if (!String.IsNullOrEmpty(IktatoKonyvekDropDownList.SelectedValue))
            {
                IktatoKonyvekDropDownList.SetSearchObject(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
            }

            if (!String.IsNullOrEmpty(this.Foszam))
            {
                erec_UgyUgyiratokSearch.Foszam.Value = this.Foszam;
                erec_UgyUgyiratokSearch.Foszam.ValueTo = this.Foszam;
                erec_UgyUgyiratokSearch.Foszam.Operator = Query.Operators.equals;
            }
            if (!String.IsNullOrEmpty(this.Ev))
            {
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = this.Ev;
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo = this.Ev;
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;
            }
        }
    }

    /// <summary>
    /// A keres�si objektum alapj�n kit�lti a beviteli mez�ket.
    /// </summary>
    /// <param name="erec_UgyUgyiratokSearch">A kit�lt�s alapj�ul szolg�l� keres�si objektum.</param>
    /// <returns>True, ha a keres�si objektum form�tuma egyszer� (azaz intervallumok n�lk�li), false, ha intervallum t�pus� megad�sok is voltak. </returns>
    public bool FillComponentsFromSearchObject(EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch, Contentum.eUIControls.eErrorPanel ErrorPanel)
    {
        bool isSimpleSearch = true;
        this.ClearInputFields();
        bool isDefaultSearchObject = IsDefaultSearchObject(erec_UgyUgyiratokSearch);

        if (erec_UgyUgyiratokSearch != null)
        {
            if (erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                this.Ev = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value;
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek(IktatoKonyvek.GetIktatokonyvValue(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch), ErrorPanel);

                isSimpleSearch &= (
                    (erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo
                        && erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == Query.Operators.equals)
                    ||
                    (erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value == ""
                        && erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.ValueTo == ""
                        && erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator == "")
                        );
            }
            else
            {
                if (isDefaultSearchObject)
                {
                    if (String.IsNullOrEmpty(this.Ev) && this.AlapertelmezettEv > 0)
                    {
                        this.Ev = this.AlapertelmezettEv.ToString();
                    }
                }
                this.FillIktatoKonyvek("", ErrorPanel);
            }

            this.Foszam = erec_UgyUgyiratokSearch.Foszam.Value;

            isSimpleSearch &= (
                (erec_UgyUgyiratokSearch.Foszam.Value == erec_UgyUgyiratokSearch.Foszam.ValueTo
                    && erec_UgyUgyiratokSearch.Foszam.Operator == Query.Operators.equals)
                ||
                (erec_UgyUgyiratokSearch.Foszam.Value == ""
                    && erec_UgyUgyiratokSearch.Foszam.ValueTo == ""
                    && erec_UgyUgyiratokSearch.Foszam.Operator == "")
                    );

        }
        return isSimpleSearch;
    }


    public void ClearInputFields()
    {
        IktatoKonyvekDropDownList.SetSelectedValue("");
        this.Foszam = "";
        this.Ev = "";
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(IktatoKonyvekDropDownList.DropDownList);
        componentList.Add(TextBoxFoszam);
        componentList.Add(TextBoxEv);

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
