﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratSzerelesiLista.ascx.cs" Inherits="eRecordComponent_UgyiratSzerelesiLista" %>
<asp:UpdatePanel ID="updatePanelSzerelesLista" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <eUI:eFormPanel ID="SzerelesListaPanel" runat="server" Visible="false">
            <asp:Label ID="labelHeader" runat="server" Text='Figyelem! A választott ügyirat szerelt, ezért a szülő ügyiratra előzményezünk: ' style="font-size:12px; font-weight:bold"/>
            <asp:Repeater ID="RepeaterRegiAdatSzerelesLista" runat="server" 
                onitemdatabound="RepeaterRegiAdatSzerelesLista_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                <a href="javascript:void(0);" class="linkStyle">
                    <asp:Label ID="labelUgyiratAzonosito" runat="server" Text='<%# string.Format("{0}", Eval("Foszam_Merge")) %>'
                        style="font-size:12px;" CssClass="linkStyle" ToolTip='<%# string.Format("{0} megtekintése", Eval("Foszam_Merge")) %>'/>
                </a>
                </ItemTemplate>
                <SeparatorTemplate>
                    <asp:Label ID="labelDelimeter" runat="server" Text='->' />
                </SeparatorTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Label ID="labelDelimeter" runat="server" Text='->' Visible = "false"/>
            <asp:Repeater ID="RepeaterSzerelesLista" runat="server" 
                onitemdatabound="RepeaterSzerelesLista_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                <a href="javascript:void(0);" class="linkStyle">
                    <asp:Label ID="labelUgyiratAzonosito" runat="server" Text='<%# string.Format("{0}", Eval("Foszam_Merge")) %>'
                        style="font-size:12px;" CssClass="linkStyle" ToolTip='<%# string.Format("{0} megtekintése", Eval("Foszam_Merge")) %>'/>
                </a>
                </ItemTemplate>
                <SeparatorTemplate>
                    <asp:Label ID="labelDelimeter" runat="server" Text='->' />
                </SeparatorTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </eUI:eFormPanel>
    </ContentTemplate>
</asp:UpdatePanel>
