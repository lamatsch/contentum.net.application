﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_UgyiratSzerelesiLista : System.Web.UI.UserControl
{
    public object DataSourceRegiAdat
    {
        get
        {
            return RepeaterRegiAdatSzerelesLista.DataSource;
        }
        set
        {
            RepeaterRegiAdatSzerelesLista.DataSource = value;
        }
    }

    public void DataBindRegiAdat()
    {
        RepeaterRegiAdatSzerelesLista.DataBind();
        SzerelesListaPanel.Visible = true;

        if (String.IsNullOrEmpty(SelectedId))
            labelDelimeter.Visible = true;
        else
            labelDelimeter.Visible = false;

        RepeaterRegiAdatSzerelesLista.Visible = true;
        updatePanelSzerelesLista.Update();
    }

    public object DataSource
    {
        get 
        {
            return RepeaterSzerelesLista.DataSource;
        }
        set
        {
            RepeaterSzerelesLista.DataSource = value;
        }
    }

    public void DataBind()
    {
        RepeaterSzerelesLista.DataBind();
        SzerelesListaPanel.Visible = true;
        RepeaterSzerelesLista.Visible = true;
        updatePanelSzerelesLista.Update();
    }

    private string selectedId = String.Empty;

    public string SelectedId
    {
        get { return selectedId; }
        set { selectedId = value; }
    }

    public void Reset()
    {
        SzerelesListaPanel.Visible = false;
        updatePanelSzerelesLista.Update();
        RepeaterSzerelesLista.Visible = false;
        RepeaterRegiAdatSzerelesLista.Visible = false;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void RepeaterSzerelesLista_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label labelUgyiratAzonosito = (Label)e.Item.FindControl("labelUgyiratAzonosito");
            if (labelUgyiratAzonosito != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv != null)
                {
                    string ugyiratId = drv["Id"].ToString();
                    string onclick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + ugyiratId
                                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                    labelUgyiratAzonosito.Attributes.Add("onclick",onclick);

                    if (!String.IsNullOrEmpty(SelectedId) && SelectedId == ugyiratId)
                    {
                        labelUgyiratAzonosito.Font.Bold = true;
                    }
                }
            }

        }
    }

    protected void RepeaterRegiAdatSzerelesLista_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label labelUgyiratAzonosito = (Label)e.Item.FindControl("labelUgyiratAzonosito");
            if (labelUgyiratAzonosito != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv != null)
                {
                    string ugyiratId = drv["Id"].ToString();
                    string ugyiratAzonosito = labelUgyiratAzonosito.Text;
                    string onclick = JavaScripts.SetOnCLientClick_NoPostBack(eMigration.migralasFormUrl,
                          QueryStringVars.Command + "=" + CommandName.View + "&" +
                          QueryStringVars.RegiAdatAzonosito + "=" + Server.UrlEncode(ugyiratAzonosito),
                          Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                    labelUgyiratAzonosito.Attributes.Add("onclick", onclick);

                    if (!String.IsNullOrEmpty(SelectedId) && SelectedId == ugyiratId)
                    {
                        labelUgyiratAzonosito.Font.Bold = true;
                    }
                }
            }

        }
    }
}
