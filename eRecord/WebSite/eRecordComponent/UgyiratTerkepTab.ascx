<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratTerkepTab.ascx.cs" Inherits="eRecordComponent_UgyiratTerkepTab" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>

<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm" %>
<%@ Register Src="~/eRecordComponent/SztornoPopup.ascx" TagName="SztornoPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />--%>


<asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always" OnLoad="TreeViewUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <uc:SztornoPopup runat="server" ID="SztornoPopup" />
            <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
            <ajaxToolkit:CollapsiblePanelExtender ID="CsatolmanyokCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" AutoCollapse="false" AutoExpand="false"
                ExpandedSize="450" ScrollContents="true">
            </ajaxToolkit:CollapsiblePanelExtender>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 100%">
                        <asp:HiddenField ID="FoUgyiratFoszam_HiddenField" runat="server" />
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 100%; text-align: left; border: solid 1px Silver;">
                                    <table cellpadding="0" cellspacing="0" class="UgyiratTerkepHeader">
                                        <tr class="GridViewHeaderStyle">
                                            <td class="GridViewBorderHeader" colspan="5" style="width: 200px; text-align: left;">Azonos�t�
                                            </td>
                                            <td class="GridViewBorderHeader" style="width: 95px; text-align: left;">�llapot
                                            </td>
                                            <td class="GridViewBorderHeader" style="width: 145px; text-align: left;">T�rgy
                                            </td>
                                            <td class="GridViewBorderHeader" style="width: 780px; text-align: left;">&nbsp</td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="Panel1" runat="server" Visible="true">
                                        <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="false" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"
                                            OnTreeNodePopulate="TreeView1_TreeNodePopulate" BackColor="White" ShowLines="True"
                                            PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip=""
                                            SkipLinkText="" CssClass="UgyiratTerkep" OnTreeNodeCollapsed="TreeView1_TreeNodeCollapsed"
                                            NodeWrap="True">
                                            <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                                        </asp:TreeView>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; height: 50px;">
                                    <asp:Panel ID="PanelGombok" runat="server" Visible="false">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%; text-align: center;">
                                                    <asp:ImageButton ID="Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                        onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')" />
                                                    <asp:ImageButton ID="Modositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')" />
                                                    <asp:ImageButton ID="Letrehozas" runat="server" ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                                                        onmouseover="swapByName(this.id,'felvitel_trap2.jpg')" onmouseout="swapByName(this.id,'felvitel_trap.jpg')" />
                                                    <asp:ImageButton ID="Sztorno" runat="server" ImageUrl="~/images/hu/trapezgomb/sztorno_trap.jpg"
                                                        onmouseover="swapByName(this.id,'sztorno_trap2.jpg')" onmouseout="swapByName(this.id,'sztorno_trap.jpg')"
                                                        CommandName="Sztorno" OnClick="FunctionButtons_Click" />
                                                    <asp:ImageButton ID="Valasz" runat="server" ImageUrl="~/images/hu/trapezgomb/valasz_trap.jpg"
                                                        onmouseover="swapByName(this.id,'valasz_trap2.jpg')" onmouseout="swapByName(this.id,'valasz_trap.jpg')" />
                                                    <asp:ImageButton ID="Atiktatas" runat="server" ImageUrl="~/images/hu/trapezgomb/felszabaditas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'felszabaditas_trap2.jpg')" onmouseout="swapByName(this.id,'felszabaditas_trap.jpg')" />
                                                    <asp:ImageButton ID="SzerelesVisszavonasa" runat="server" ImageUrl="~/images/hu/trapezgomb/szereles_visszavonas_trap.gif"
                                                        onmouseover="swapByName(this.id,'szereles_visszavonas_trap2.gif')" onmouseout="swapByName(this.id,'szereles_visszavonas_trap.gif')"
                                                        OnClick="SzerelesVisszavonasa_Click" />
                                                    <asp:ImageButton ID="EMailNyomtatas" runat="server" ImageUrl="~/images/hu/trapezgomb/email_nyomtatas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'email_nyomtatas_trap2.jpg')" onmouseout="swapByName(this.id,'email_nyomtatas_trap.jpg')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%; text-align: center;">
                                                    <asp:ImageButton ID="Kuldemeny_osszesites" runat="server" ImageUrl="~/images/hu/trapezgomb/kuldemeny_osszesit_trap1.jpg"
                                                        onmouseover="swapByName(this.id,'kuldemeny_osszesit_trap2.jpg')" onmouseout="swapByName(this.id,'kuldemeny_osszesit_trap1.jpg')" />
                                                    <asp:ImageButton ID="Expedialas" runat="server" ImageUrl="~/images/hu/trapezgomb/expedialas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'expedialas_trap2.jpg')" onmouseout="swapByName(this.id,'expedialas_trap.jpg')" />
                                                    <asp:ImageButton ID="Postazas" runat="server" ImageUrl="~/images/hu/trapezgomb/postazas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'postazas_trap2.jpg')" onmouseout="swapByName(this.id,'postazas_trap.jpg')"
                                                        CommandName="Postazas" OnClick="FunctionButtons_Click" />
                                                    <asp:ImageButton ID="BoritoA4" runat="server" ImageUrl="~/images/hu/trapezgomb/borito_trap.jpg"
                                                        onmouseover="swapByName(this.id,'borito_trap2.jpg')" onmouseout="swapByName(this.id,'borito_trap.jpg')" />
                                                    <asp:ImageButton ID="Lezaras" runat="server" ImageUrl="~/images/hu/trapezgomb/lezaras_trap.jpg"
                                                        onmouseover="swapByName(this.id,'lezaras_trap2.jpg')" onmouseout="swapByName(this.id,'lezaras_trap.jpg')" />
                                                    <asp:ImageButton ID="Felszabaditas" runat="server" ImageUrl="~/images/hu/trapezgomb/felszabaditas_trap.png"
                                                        onmouseover="swapByName(this.id,'felszabaditas_trap2.png')" onmouseout="swapByName(this.id,'felszabaditas_trap.png')"
                                                        CommandName="Felszabaditas" OnClick="FunctionButtons_Click" />
                                                    <asp:ImageButton ID="ValaszIrat" runat="server" ImageUrl="~/images/hu/trapezgomb/valaszirat_trap.jpg"
                                                        onmouseover="swapByName(this.id,'valaszirat_trap2.jpg')" onmouseout="swapByName(this.id,'valaszirat_trap.jpg')" />
                                                    <asp:ImageButton ID="MunkaanyagBeiktatas" runat="server" ImageUrl="~/images/hu/trapezgomb/iratta_minosites_trap.gif"
                                                        Visible="false" onmouseover="swapByName(this.id,'iratta_minosites_trap2.gif')"
                                                        onmouseout="swapByName(this.id,'iratta_minosites_trap.gif')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%; text-align: center;">
                                                    <asp:ImageButton ID="ImageExpandNode" runat="server" ImageUrl="~/images/hu/trapezgomb/csomopont_kibontasa_trap.jpg"
                                                        onmouseover="swapByName(this.id,'csomopont_kibontasa_trap2.jpg')" onmouseout="swapByName(this.id,'csomopont_kibontasa_trap.jpg')"
                                                        AlternateText="<%$Resources:Buttons,ExpandNode %>" ToolTip="<%$Resources:Buttons,ExpandNode %>" />
                                                    <asp:ImageButton ID="ImageCollapseNode" runat="server" ImageUrl="~/images/hu/trapezgomb/csomopont_bezarasa_trap.jpg"
                                                        onmouseover="swapByName(this.id,'csomopont_bezarasa_trap2.jpg')" onmouseout="swapByName(this.id,'csomopont_bezarasa_trap.jpg')"
                                                        AlternateText="<%$Resources:Buttons,CollapseNode %>" ToolTip="<%$Resources:Buttons,CollapseNode %>" />
                                                    <asp:ImageButton ID="ImageButton_TreeViewPrint" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'nyomtatas_trap2.jpg')" onmouseout="swapByName(this.id,'nyomtatas_trap.jpg')" />
                                                    <asp:ImageButton ID="ImageButton_AtadasraKijeloles" runat="server" ImageUrl="~/images/hu/trapezgomb/atadasra_kijelol_trap.jpg"
                                                        onmouseover="swapByName(this.id,'atadasra_kijelol_trap2.jpg')" onmouseout="swapByName(this.id,'atadasra_kijelol_trap.jpg')"
                                                        CommandName="AtadasraKijeloles" OnClick="FunctionButtons_Click" />
                                                    <%--BLG_336--%>
                                                    <asp:ImageButton ID="ImageButton_Atvetel" runat="server" ImageUrl="~/images/hu/trapezgomb/atvetel_trap.jpg"
                                                        onmouseover="swapByName(this.id,'atvetel_trap2.jpg')" onmouseout="swapByName(this.id,'atvetel_trap.jpg')"
                                                        CommandName="Atvetel" OnClick="FunctionButtons_Click" />
                                                    <asp:ImageButton ID="ImageButton_AtvetelUgyintezesre" runat="server" ImageUrl="~/images/hu/trapezgomb/atvetel_ugyintezesre_trap.jpg"
                                                        onmouseover="swapByName(this.id,'atvetel_ugyintezesre_trap2.jpg')" onmouseout="swapByName(this.id,'atvetel_ugyintezesre_trap.jpg')"
                                                        CommandName="AtvetelUgyintezesre" OnClick="FunctionButtons_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: top">
                        <table cellpadding="0" cellspacing="0" style="padding-top: 5px;">
                            <tr>
                                <td>
                                    <eUI:eFormPanel ID="EFormPanel_IratMozgatas" runat="server" Style="margin-left: 20px;">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaptionCenter_nowidth">
                                                    <asp:Label ID="Label1" runat="server" Text="Iratok&nbsp;mozgat�sa"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <hr style="width: 90%;" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Label ID="Label_Mozgatas" runat="server" Text="Mozgat�s ide:"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:DropDownList ID="UgyiratDarabok_DropDownList" runat="server" CssClass="mrUrlapInputKozepes">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Button ID="Button_Mozgat" runat="server" Text="Mehet" OnClick="Button_Mozgat_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <hr style="width: 90%;" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Label ID="Label_KivetelUgyiratdarabbol" runat="server" Text="�gyiratdarabb�l&nbsp;kiv�tel:"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Button ID="Button_KivetelUgyiratDarabbol" runat="server" Text="Mehet" OnClick="Button_KivetelUgyiratDarabbol_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </eUI:eFormPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <eUI:eFormPanel ID="EFormPanel_UgyiratDarabKezeles" runat="server" Style="margin-left: 20px;">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaptionCenter_nowidth">
                                                    <asp:Label ID="Label2" runat="server" Text="�gyiratdarabok&nbsp;kezel�se"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <hr style="width: 90%;" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Label ID="Label_UjUgyiratDarab" runat="server" Text="�j �gyiratdarab:"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:TextBox ID="UjUgyiratDarabNeve_TextBox" runat="server" CssClass="mrUrlapInputKozepes"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:ImageButton ID="ImageButton_UjUgyiratDarab" runat="server" ImageUrl="~/images/hu/trapezgomb/uj_trap.jpg"
                                                        OnClick="ImageButton_UjUgyiratDarab_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <hr style="width: 90%;" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:Label ID="Label_DeleteUgyiratDarab" runat="server" Text="�gyiratdarab t�rl�se:"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td>
                                                    <asp:ImageButton ID="ImageButton_DeleteUgyiratDarab" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg"
                                                        OnClick="ImageButton_DeleteUgyiratDarab_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </eUI:eFormPanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
