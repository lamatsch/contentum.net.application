﻿//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using AjaxControlToolkit;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_UgyiratTerkepTab : System.Web.UI.UserControl
{
    // Listára ugrás szabályozása (más, mint a popup, mert listáról ki lehet navigálni teljesen új ablakba:
    // - ha true, akkor az ügyirat csatolmány ikonról el lehet navigálni a dokumentumok listájára,
    // - ha false, akkor az ikon nem mûködik linkként
    private const bool bEnableUgyiratCsatolmanyImageLink = true;

    // jogellenõrzés objektum szinten:
    // ha igaz, már a térkép elemének kiválasztásakor lekérjük az objektumot és ellenõrizzük,
    // van-e joga megtekinteni az iratkezelési elemet, ennek alapján engedélyezzük a gombokat (megtekintés, módosítás)
    // ha hamis, csak a funkciójogot ellenõrizzük, és csak akkor nézzük meg az objektumhoz való jogokat, ha megpróbálja megnyitni
    private const bool bEnableObjectRightCheck = true;

    // kapcsolt küldemények elhelyezése a többi gyerek csomóponthoz képest
    // true értéknél a többi gyerek elé, false esetén a gyerek csomópontok mögé
    //private const bool bUgyiratKapcsoltKuldemenyBeforeOtherChildren = true;
    //private const bool bIratKapcsoltKuldemenyBeforeOtherChildren = true;

    //private const string CssClass_disableditem = "disableditem";

    private String _SelectedId = "";

    public String SelectedId
    {
        get { return _SelectedId; }
        set { _SelectedId = value; }
    }
    private String _SelectedNode = "";

    public String SelectedNode
    {
        get { return _SelectedNode; }
        set { _SelectedNode = value; }
    }

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            //MainPanel.Visible = value;
            PanelGombok.Visible = value;
            if (!value)
            {
                TreeView1.Nodes.Clear();
            }
        }
    }

    public string Command = "";
    UI ui = new UI();
    private PageView pageView = null;

    private String ParentId = "";

    private string FoUgyirat_Id = "";

    // Az objektumban történõ változásokat jelzõ esemény, ami pl. kiválthat egy jogosultságellenõrzést
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    #region SetImageButtonEnabledProperty
    private bool IsDisabledButtonToSetHidden()
    {
        return Contentum.eUtility.Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS);
    }
    #endregion SetImageButtonEnabledProperty


    #region Base Tree Dictionary

    System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Ugyiratok.Statusz> UgyiratDictionary = new System.Collections.Generic.Dictionary<string, Contentum.eRecord.Utility.Ugyiratok.Statusz>();
    System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.UgyiratDarabok.Statusz> UgyiratDarabokDictionary = new System.Collections.Generic.Dictionary<string, Contentum.eRecord.Utility.UgyiratDarabok.Statusz>();
    System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Iratok.Statusz> IratokDictionary = new System.Collections.Generic.Dictionary<string, Contentum.eRecord.Utility.Iratok.Statusz>();
    System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.IratPeldanyok.Statusz> IratPeldanyokDictionary = new System.Collections.Generic.Dictionary<string, Contentum.eRecord.Utility.IratPeldanyok.Statusz>();
    System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Kuldemenyek.Statusz> KuldemenyekDictionary = new System.Collections.Generic.Dictionary<string, Contentum.eRecord.Utility.Kuldemenyek.Statusz>();

    #endregion

    #region public Properties

    private TabContainer _ParentTabContainer;

    public TabContainer ParentTabContainer
    {
        get
        {
            return _ParentTabContainer;
        }

        set
        {
            _ParentTabContainer = value;
        }
    }

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion

    #region ButtonsProperty
    Boolean MegtekintesEnabled
    {
        get { return Megtekintes.Enabled; }
        set { Megtekintes.Enabled = value; }
    }

    Boolean ModositasEnabled
    {
        get { return Modositas.Enabled; }
        set { Modositas.Enabled = value; }
    }

    Boolean LetrehozasEnabled
    {
        get { return Letrehozas.Enabled; }
        set { Letrehozas.Enabled = value; }
    }

    Boolean SztornoEnabled
    {
        get { return Sztorno.Enabled; }
        set { Sztorno.Enabled = value; }
    }

    Boolean ValaszEnabled
    {
        get { return Valasz.Enabled; }
        set { Valasz.Enabled = value; }
    }

    Boolean AtiktatasEnabled
    {
        get { return Atiktatas.Enabled; }
        set { Atiktatas.Enabled = value; }
    }

    Boolean EMailNyomtatasEnabled
    {
        get { return EMailNyomtatas.Enabled; }
        set { EMailNyomtatas.Enabled = value; }
    }

    bool MunkaanyagBeiktatasEnabled
    {
        get { return MunkaanyagBeiktatas.Enabled; }
        set { MunkaanyagBeiktatas.Enabled = value; }
    }

    Boolean SzerelesVisszavonasaEnabled
    {
        get { return SzerelesVisszavonasa.Enabled; }
        set { SzerelesVisszavonasa.Enabled = value; }
    }

    Boolean PostazasEnabled
    {
        get { return Postazas.Enabled; }
        set { Postazas.Enabled = value; }
    }

    Boolean ExpedialasEnabled
    {
        get { return Expedialas.Enabled; }
        set { Expedialas.Enabled = value; }
    }

    Boolean Kuldemeny_osszesitesEnabled
    {
        get { return Kuldemeny_osszesites.Enabled; }
        set { Kuldemeny_osszesites.Enabled = value; }
    }

    Boolean BoritoA4Enabled
    {
        get { return BoritoA4.Enabled; }
        set { BoritoA4.Enabled = value; }
    }

    Boolean LezarasEnabled
    {
        get { return Lezaras.Enabled; }
        set { Lezaras.Enabled = value; }
    }

    bool FelszabaditasEnabled
    {
        get { return Felszabaditas.Enabled; }
        set { Felszabaditas.Enabled = value; }
    }

    Boolean ValaszIratEnabled
    {
        get { return ValaszIrat.Enabled; }
        set { ValaszIrat.Enabled = value; }
    }

    Boolean AtadasraKijelolesEnabled
    {
        get { return ImageButton_AtadasraKijeloles.Enabled; }
        set { ImageButton_AtadasraKijeloles.Enabled = value; }
    }

    // BLG_336
    Boolean AtvetelEnabled
    {
        get { return ImageButton_Atvetel.Enabled; }
        set { ImageButton_Atvetel.Enabled = value; }
    }
    Boolean AtvetelUgyintezesreEnabled
    {
        get { return ImageButton_AtvetelUgyintezesre.Enabled; }
        set { ImageButton_AtvetelUgyintezesre.Enabled = value; }
    }

    #endregion

    // jogszint: O vagy I (olvasás v. írás)
    private bool CheckRight(string id, int nodeType, char jogszint)
    {
        bool hasRight = false;

        #region paraméter ellenõrzés
        if (String.IsNullOrEmpty(id)) return false;
        if (jogszint != 'O' && jogszint != 'I') return false;
        #endregion paraméter ellenõrzés

        switch (nodeType)
        {
            case Constants.UgyiratTerkepNodeTypes.Ugyirat:
                {
                    Contentum.eRecord.Service.EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;
                    Result result = service.GetWithRightCheckWithoutEventLogging(execParam, jogszint);
                    if (!result.IsError && (EREC_UgyUgyiratok)result.Record != null)
                    {
                        hasRight = true;
                    }
                }
                break;
            case Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                {
                    Contentum.eRecord.Service.EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;
                    Result result = service.GetWithRightCheck(execParam, jogszint);
                    if (!result.IsError && (EREC_UgyUgyiratdarabok)result.Record != null)
                    {
                        hasRight = true;
                    }
                }
                break;
            case Constants.UgyiratTerkepNodeTypes.Irat:
                {
                    Contentum.eRecord.Service.EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;
                    Result result = service.GetWithRightCheckWithoutEventLogging(execParam, jogszint);
                    if (!result.IsError && (EREC_IraIratok)result.Record != null)
                    {
                        hasRight = true;
                    }
                }
                break;
            case Constants.UgyiratTerkepNodeTypes.IratPeldany:
                {
                    Contentum.eRecord.Service.EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;
                    Result result = service.GetWithRightCheckWithoutEventLogging(execParam, jogszint);
                    if (!result.IsError && (EREC_PldIratPeldanyok)result.Record != null)
                    {
                        hasRight = true;
                    }
                }
                break;
            case Constants.UgyiratTerkepNodeTypes.Kuldemeny:
                {
                    Contentum.eRecord.Service.EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;
                    Result result = service.GetWithRightCheckWithoutEventLogging(execParam, jogszint);
                    if (!result.IsError && (EREC_KuldKuldemenyek)result.Record != null)
                    {
                        hasRight = true;
                    }
                }
                break;
            default:
                break;
        }

        return hasRight;
    }

    private string imageWidth = "25px";
    private string imageHeight = "20px";

    private string getSzereltImage(DataRow ugyiratRow)
    {
        int elokeszitettSzerelesCount = 0;
        Int32.TryParse(ugyiratRow["ElokeszitettSzerelesCount"].ToString(), out elokeszitettSzerelesCount);

        /// ha van szerelésre elõkészített ügyirat (UgyUgyiratId_Szulo ki van töltve erre, de nem szerelt még az állapota),
        /// akkor a piros szerelt ikont rakjuk ki, amúgy a feketét

        if (elokeszitettSzerelesCount > 0)
        {
            return "<img src=\"images/hu/ikon/szerelt_piros.gif\" alt=\"Elõkészített szerelés\" height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
        }
        else
        {
            int szereltCount = 0;
            Int32.TryParse(ugyiratRow["SzereltCount"].ToString(), out szereltCount);
            if (szereltCount > 0)
            {
                return "<img src=\"images/hu/ikon/szerelt.gif\" alt=\"Szerelés\" height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
            }
            else
            {
                return "&nbsp";
            }
        }
    }

    private string getCsatolasImage(DataRow ugyiratRow)
    {
        int csatolasCount = 0;
        Int32.TryParse(ugyiratRow["CsatolasCount"].ToString(), out csatolasCount);
        if (csatolasCount > 0)
        {
            string tooltipText = String.Format("Csatolás: {0} darab", csatolasCount);
            return "<img src=\"images/hu/ikon/csatolt.gif\" alt=\"" + tooltipText + "\" title=\"" + tooltipText + "\"  height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" />";
        }
        else
        {
            return "&nbsp";
        }
    }

    private string getCsatolmanyImage(DataRow iratRow, int nodeType, TreeNode parentTreeNode)
    {
        int csatolmanyCount = 0;
        Int32.TryParse(iratRow["CsatolmanyCount"].ToString(), out csatolmanyCount);
        string onclick = "";
        string imageId = String.Format("csatolmanyImage_{0}", iratRow["Id"].ToString());
        DokumentumVizualizerComponent.RemoveDokumentumElement(imageId);

        if (csatolmanyCount == 0)
        {
            return "&nbsp";
        }

        string iratHelye = iratRow["FelhasznaloCsoport_Id_Orzo"] == null ? null : iratRow["FelhasznaloCsoport_Id_Orzo"].ToString();
        if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, iratHelye))
        {           
            string result = "<img disabled=\"disabled\"  class=\"disableditem\" id=\"" + imageId 
                + "\" src=\"images/hu/ikon/csatolmany.gif\" alt=\"" + Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight + "\""
                + "title=\"" + Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight + "\""
                + "height=\"" + imageHeight 
                + "\" width=\"" + imageWidth 
                + "\" onclick=\"" +  ""  + "\" +  />";
            return result;            
        }

        string tooltipText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

        #region BLG_577
        if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, iratRow["Dokumentum_Id"].ToString()))
        {
            onclick = "alert('" + Resources.Error.ErrorCode_52794 + "');";
            tooltipText += System.Environment.NewLine + Resources.Error.ErrorCode_52794; // Önnek nincs joga, hogy megtekintse a dokumentum tartalmát!
        }
        #endregion
        else if (iratRow.Table.Columns.Contains("CsatolmanyJogosult") && (bool)iratRow["CsatolmanyJogosult"] == false)
        {
            onclick = "alert('" + Resources.Error.ErrorCode_52794 + "');";
            tooltipText += System.Environment.NewLine + Resources.Error.ErrorCode_52794; // Önnek nincs joga, hogy megtekintse a dokumentum tartalmát!
        }
        else if (csatolmanyCount == 1 && iratRow.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(iratRow["Dokumentum_Id"].ToString()))
        {
            onclick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", iratRow["Dokumentum_Id"]);
            DokumentumVizualizerComponent.AddDokumentElement(imageId, iratRow["Dokumentum_Id"].ToString());
        }
        else
        {

            string id = iratRow["Id"].ToString();
            if (!String.IsNullOrEmpty(id))
            {
                string popupForm = "";
                string qs_popup = QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                        + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab;

                // csak ügyiratnál használjuk
                string Azonosito = ""; // iktatószám, a ReadableWhere fogja használni a dokumentumok listáján
                if (iratRow.Table.Columns.Contains("Foszam_Merge"))
                {
                    Azonosito = iratRow["Foszam_Merge"].ToString();
                }
                string qs_list = QueryStringVars.Startup + "=" + Constants.Startup.FromUgyirat
                    + "&" + QueryStringVars.ObjektumId + "=" + id
                    + "&" + QueryStringVars.Azonosito + "=" + System.Web.HttpUtility.UrlEncode(Azonosito);


                bool bIsNormalForm = true;

                if (nodeType == Constants.UgyiratTerkepNodeTypes.IratPeldany)
                {
                    popupForm = "PldIratPeldanyokForm.aspx";
                }
                else if (nodeType == Constants.UgyiratTerkepNodeTypes.Irat)
                {
                    popupForm = "IraIratokForm.aspx";
                }
                else if (nodeType == Constants.UgyiratTerkepNodeTypes.Kuldemeny)
                {
                    popupForm = "KuldKuldemenyekForm.aspx";
                }
                else if (nodeType == Constants.UgyiratTerkepNodeTypes.Ugyirat)
                {
                    popupForm = "UgyUgyiratokForm.aspx";
                }

                if (!String.IsNullOrEmpty(popupForm))
                {
                    if (bIsNormalForm)
                    {
                        string valuePath = (parentTreeNode == null ? "" : parentTreeNode.ValuePath + "/") + id;
                        // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                        onclick = JavaScripts.SetOnClientClick(popupForm, qs_popup
                            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh + "|" + valuePath);

                    }
                    else
                    {
                        //// lista hívás
                        if (bEnableUgyiratCsatolmanyImageLink)
                        {
                            string valuePath = (parentTreeNode == null ? "" : parentTreeNode.ValuePath + "/") + id;
                            onclick = JavaScripts.SetOnClientClick(popupForm, qs_list
                                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh + "|" + valuePath);
                        }

                    }
                    }
                }
            }     

        string str = "<img id=\"" + imageId + "\" src=\"images/hu/ikon/csatolmany.gif\" alt=\"" + tooltipText + " \"  height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" onclick=\"" + (String.IsNullOrEmpty(onclick) ? "" : onclick) + "\" +  />";
        return str;
    }

    private string getTertivevenyImage(DataRow kuld_pldRow, TreeNode parentTreeNode)
    {
        string str = "&nbsp";
        string onclick = "";

        if (kuld_pldRow.Table.Columns.Contains("Tertiveveny_Dokumentum_Id"))
        {
            string tertivevenyDokumentumId = kuld_pldRow["Tertiveveny_Dokumentum_Id"].ToString();
            //string onclick = "";
            string imageId = String.Format("tertivevenyImage_{0}", kuld_pldRow["Id"].ToString());
            DokumentumVizualizerComponent.RemoveDokumentumElement(imageId);

            if (!String.IsNullOrEmpty(tertivevenyDokumentumId))
            {
                string tooltipText = "Tértivevény (csatolmány)";

                onclick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", tertivevenyDokumentumId);
                DokumentumVizualizerComponent.AddDokumentElement(imageId, tertivevenyDokumentumId);

                str = String.Format("<img id=\"{0}\" src=\"images/hu/ikon/tertiveveny.gif\" alt=\"{1}\" height=\"{2}\" width=\"{3}\" onclick=\"{4}\" />"
                    , imageId
                    , tooltipText
                    , imageHeight
                    , imageWidth
                    , onclick ?? "");
            }

        }

        if (kuld_pldRow.Table.Columns.Contains("Tertiveveny_Id"))
        {
            string tertivevenyId = kuld_pldRow["Tertiveveny_Id"].ToString();
            onclick = "";
            str = "&nbsp";
            string imageId = String.Format("tertivevenyImage_{0}", kuld_pldRow["Id"].ToString());
            DokumentumVizualizerComponent.RemoveDokumentumElement(imageId);

            if (!String.IsNullOrEmpty(tertivevenyId))
            {
                string tooltipText = "Tértivevény";
                //popup('KuldTertivevenyekForm.aspx?Command=View&amp;Id=9a367d30-1118-e611-80ba-00155d020b4b', 800, 650)

                string popupForm = "KuldTertivevenyekForm.aspx";
                string qs_popup = QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + tertivevenyId;

                string valuePath = (parentTreeNode == null ? "" : parentTreeNode.ValuePath + "/") + kuld_pldRow["Id"].ToString();
                // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                onclick = JavaScripts.SetOnClientClick(popupForm, qs_popup
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh + "|" + valuePath);

                //   onclick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", tertivevenyDokumentumId);
                DokumentumVizualizerComponent.AddDokumentElement(imageId, tertivevenyId);

                str = String.Format("<img id=\"{0}\" src=\"images/hu/ikon/tertiveveny_link.gif\" alt=\"{1}\" height=\"{2}\" width=\"{3}\" onclick=\"{4}\" />"
                    , imageId
                    , tooltipText
                    , imageHeight
                    , imageWidth
                    , onclick ?? "");
            }

        }
        return str;
    }

    private string getFeladatImage(DataRow row, string ObjektumTipus)
    {
        if (!HataridosFeladatok.IsFeladatJelzoIkonEnabledOnTerkep(Page))
            return "&nbsp;";

        ImageButton FeladatImage = new ImageButton();
        UI.SetFeladatInfo(row, FeladatImage, ObjektumTipus);
        string imageString = UI.RenderControl(FeladatImage);
        if (!String.IsNullOrEmpty(imageString))
        {
            return imageString;
        }
        else
        {
            return "&nbsp;";
        }
    }

    private string GetUgyFajtajaLink(DataRow iratRow, TreeNode parentTreeNode)
    {
        string existsHatosagiAdat = iratRow["ExistsHatosagiAdat"].ToString();

        if (existsHatosagiAdat == "1")
        {
            string iratId = iratRow["Id"].ToString();
            // BUG_11670
            //string ugyFajtaja = iratRow["UgyFajtaja"].ToString();
            string ugyFajtaja = iratRow["Ugy_Fajtaja"].ToString();
            HyperLink link = new HyperLink();
            link.Text = UI.GetUgyFajtajaLabel(Page, ugyFajtaja);
            link.ToolTip = UI.GetUgyFajtajaToolTip(Page, ugyFajtaja);
            link.NavigateUrl = UI.GetHatosagiAdatokUrl(iratId, false);
            link.Target = "_blank";
            string valuePath = (parentTreeNode == null ? "" : parentTreeNode.ValuePath + "/") + iratId;
            link.Attributes.Add("onclick", UI.OpenHatosagiAdatok(iratId, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh + "|" + valuePath));
            link.CssClass = "linkStyle";
            return UI.RenderControl(link);
        }

        return String.Empty;
    }

    /// <summary>
    /// A viewstate-ben eltárolt dictionary-k alapján eldönti, hogy az adott id az ügyirat, irat, stb...
    /// (Constants.UgyiratTerkepNodeTypes értékekkel térhet vissza)
    /// </summary>    
    private int getNodeTypeById(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            if (UgyiratDictionary != null && UgyiratDictionary.ContainsKey(id))
            {
                // ügyirat:
                return Constants.UgyiratTerkepNodeTypes.Ugyirat;
            }
            else if (UgyiratDarabokDictionary != null && UgyiratDarabokDictionary.ContainsKey(id))
            {
                return Constants.UgyiratTerkepNodeTypes.UgyiratDarab;
            }
            else if (IratokDictionary != null && IratokDictionary.ContainsKey(id))
            {
                return Constants.UgyiratTerkepNodeTypes.Irat;
            }
            else if (IratPeldanyokDictionary != null && IratPeldanyokDictionary.ContainsKey(id))
            {
                return Constants.UgyiratTerkepNodeTypes.IratPeldany;
            }
            else if (KuldemenyekDictionary != null && KuldemenyekDictionary.ContainsKey(id))
            {
                return Constants.UgyiratTerkepNodeTypes.Kuldemeny;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    #region register JavaScripts
    //

    private void RegisterPrintControlJavaScript(string cssPath)
    {
        if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "PrintTreeViewScript"))
        {
            string js = "";
            js = @"function PrintContent(ctrl_Id, title, footer)
{
    var DocumentContainer = $get(ctrl_Id);
    if (DocumentContainer)
    {
        var WindowObject = window.open('', 'Nyomtatás','width=800,height=600,top=100,left=150,location=no,menubar=no,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<html><head>');";
            if (!String.IsNullOrEmpty(cssPath))
            {
                js += @"
        WindowObject.document.writeln('<link href=""" + cssPath + @""" type=""text/css"" rel=""stylesheet"" />');";
            }
            js += @"
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln('<h1 class=""PrintHeader"">' + title + '</h1>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('<p class=""PrintFooter"">' + footer + '</p>');
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
}
";

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "PrintTreeViewScript", js, true);
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);


        string cssPath = Request.ApplicationPath + "/App_Themes/" + Page.Theme + "/StyleSheet.css";
        RegisterPrintControlJavaScript(cssPath);

        // TODO: testbol!
        //MainPanel.Visible = true;

        bool isDisabledIconToSetHidden = IsDisabledButtonToSetHidden();
        Megtekintes.HideIfDisabled = isDisabledIconToSetHidden;
        Modositas.HideIfDisabled = isDisabledIconToSetHidden;
        Letrehozas.HideIfDisabled = isDisabledIconToSetHidden;
        Sztorno.HideIfDisabled = isDisabledIconToSetHidden;
        Valasz.HideIfDisabled = isDisabledIconToSetHidden;
        Atiktatas.HideIfDisabled = isDisabledIconToSetHidden;
        EMailNyomtatas.HideIfDisabled = isDisabledIconToSetHidden;
        MunkaanyagBeiktatas.HideIfDisabled = isDisabledIconToSetHidden;
        SzerelesVisszavonasa.HideIfDisabled = isDisabledIconToSetHidden;
        Postazas.HideIfDisabled = isDisabledIconToSetHidden;
        Expedialas.HideIfDisabled = isDisabledIconToSetHidden;
        Kuldemeny_osszesites.HideIfDisabled = isDisabledIconToSetHidden;
        BoritoA4.HideIfDisabled = isDisabledIconToSetHidden;
        Lezaras.HideIfDisabled = isDisabledIconToSetHidden;
        Felszabaditas.HideIfDisabled = isDisabledIconToSetHidden;
        ValaszIrat.HideIfDisabled = isDisabledIconToSetHidden;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        string js_buttonDisabled = JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_UjUgyiratDarab);

        ImageButton_UjUgyiratDarab.OnClientClick = "if ($get('" + UjUgyiratDarabNeve_TextBox.ClientID + "').value == '') "
                + " { alert('" + Resources.Error.UINoUgyiratDarabNev + "'); return false; } "
                + js_buttonDisabled;

        ImageExpandNode.Click += new ImageClickEventHandler(ImageExpandNode_Click);
        ImageCollapseNode.Click += new ImageClickEventHandler(ImageCollapseNode_Click);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

            //ViewState -bol visszatolti az elmentett statuszokat
            try
            {
                if (ViewState["UgyiratDictionary"] != null)
                {
                    UgyiratDictionary = (System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Ugyiratok.Statusz>)ViewState["UgyiratDictionary"];
                }
                if (ViewState["UgyiratDarabokDictionary"] != null)
                {
                    UgyiratDarabokDictionary = (System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.UgyiratDarabok.Statusz>)ViewState["UgyiratDarabokDictionary"];
                }
                if (ViewState["IratokDictionary"] != null)
                {
                    IratokDictionary = (System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Iratok.Statusz>)ViewState["IratokDictionary"];
                }
                if (ViewState["IratPeldanyokDictionary"] != null)
                {
                    IratPeldanyokDictionary = (System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.IratPeldanyok.Statusz>)ViewState["IratPeldanyokDictionary"];
                }
                if (ViewState["KuldemenyekDictionary"] != null)
                {
                    KuldemenyekDictionary = (System.Collections.Generic.Dictionary<String, Contentum.eRecord.Utility.Kuldemenyek.Statusz>)ViewState["KuldemenyekDictionary"];
                }
            }
            catch
            { }
        }
        else
        {
            //ReLoadTab();        
        }

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    private string GetObjektumType(TreeNode node)
    {
        string objektumType = String.Empty;
        if (node != null)
        {
            string selectedId = node.Value;
            int nodeType = getNodeTypeById(selectedId);
            switch (nodeType)
            {
                case Constants.UgyiratTerkepNodeTypes.Ugyirat:
                    objektumType = Constants.TableNames.EREC_UgyUgyiratok;
                    break;
                case Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                    objektumType = Constants.TableNames.EREC_UgyUgyiratdarabok;
                    break;
                case Constants.UgyiratTerkepNodeTypes.Irat:
                    objektumType = Constants.TableNames.EREC_IraIratok;
                    break;
                case Constants.UgyiratTerkepNodeTypes.IratPeldany:
                    objektumType = Constants.TableNames.EREC_PldIratPeldanyok;
                    break;
                case Constants.UgyiratTerkepNodeTypes.Kuldemeny:
                    objektumType = Constants.TableNames.EREC_KuldKuldemenyek;
                    break;
            }
        }

        return objektumType;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        #region Iratmozgatás, ügyiratdarab-kezelõ panelok engedélyezése:

        bool IratMozgatasPanelokVisible = false;

        string FoUgyirat_Id = "";
        if (TreeView1.Nodes.Count > 0)
        {
            FoUgyirat_Id = TreeView1.Nodes[0].Value;
            string title = String.Format(Resources.Form.Ugyiratterkep_PrintHeader, FoUgyiratFoszam_HiddenField.Value);
            string felhasznaloNev = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            string footer = String.Format(Resources.Form.Ugyiratterkep_PrintFooter, felhasznaloNev, System.DateTime.Now.ToShortDateString());
            ImageButton_TreeViewPrint.OnClientClick = "PrintContent('" + TreeView1.ClientID + @"','" + title + @"','" + footer + @"');return false;";
        }

        if (Command == CommandName.Modify && UgyiratDictionary != null && !String.IsNullOrEmpty(FoUgyirat_Id)
            && UgyiratDictionary.ContainsKey(FoUgyirat_Id) == true)
        {
            // Ügyintézés alattinak kell lennie az ügyiratnak, és módosíthatónak
            Ugyiratok.Statusz ugyiratStatusz = UgyiratDictionary[FoUgyirat_Id];
            ErrorDetails errorDetail;
            if (ugyiratStatusz != null && ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                && Ugyiratok.Modosithato(ugyiratStatusz, UI.SetExecParamDefault(Page, new ExecParam()), out errorDetail) == true)
            {
                if (FunctionRights.GetFunkcioJog(Page, "IratMozgatasPanelEngedelyezes"))
                {
                    IratMozgatasPanelokVisible = true;
                }
            }
        }

        if (IratMozgatasPanelokVisible == true)
        {
            EFormPanel_IratMozgatas.Visible = true;
            EFormPanel_UgyiratDarabKezeles.Visible = true;

            #region Irat mozgatása panelen gombok engedélyezése/tiltása:

            TreeNode selectedNode = TreeView1.SelectedNode;
            TreeNode parentUgyiratNode = null;
            if (TreeView1.Nodes.Count > 0)
            {
                parentUgyiratNode = TreeView1.Nodes[0];
            }
            string selectedNodeId = selectedNode == null ? "" : selectedNode.Value;
            int selectedNodeType = getNodeTypeById(selectedNodeId);

            bool isInParentUgyirat = false;
            if (selectedNode != null
                &&
                ((selectedNodeType == Constants.UgyiratTerkepNodeTypes.Ugyirat && selectedNode == parentUgyiratNode)
                    || (selectedNodeType == Constants.UgyiratTerkepNodeTypes.UgyiratDarab && selectedNode.Parent == parentUgyiratNode)
                    || (selectedNodeType == Constants.UgyiratTerkepNodeTypes.Irat && selectedNode.Parent.Parent == parentUgyiratNode)
                    || (selectedNodeType == Constants.UgyiratTerkepNodeTypes.IratPeldany && selectedNode.Parent.Parent.Parent == parentUgyiratNode)
                    || (selectedNodeType == Constants.UgyiratTerkepNodeTypes.Kuldemeny && selectedNode.Parent.Parent.Parent == parentUgyiratNode)
                ))
            {
                isInParentUgyirat = true;
            }

            // Irat mozgatás engedélyezése:
            if (selectedNode != null && selectedNodeType == Constants.UgyiratTerkepNodeTypes.Irat && isInParentUgyirat)
            {
                Label_Mozgatas.Enabled = true;
                UgyiratDarabok_DropDownList.Enabled = true;
                Button_Mozgat.Enabled = true;
            }
            else
            {
                Label_Mozgatas.Enabled = false;
                UgyiratDarabok_DropDownList.Enabled = false;
                Button_Mozgat.Enabled = false;
            }

            // Ügyiratdarabból kivétel:
            if (selectedNode != null && selectedNodeType == Constants.UgyiratTerkepNodeTypes.Irat && selectedNode.Parent.Checked == false && isInParentUgyirat)
            {
                Label_KivetelUgyiratdarabbol.Enabled = true;
                Button_KivetelUgyiratDarabbol.Enabled = true;
            }
            else
            {
                Label_KivetelUgyiratdarabbol.Enabled = false;
                Button_KivetelUgyiratDarabbol.Enabled = false;
            }

            // Új ügyiratdarab: 
            // ha ki van nyitva a gyökér node:
            if (parentUgyiratNode != null && parentUgyiratNode.Expanded == true && isInParentUgyirat)
            {
                Label_UjUgyiratDarab.Enabled = true;
                UjUgyiratDarabNeve_TextBox.Enabled = true;
                ImageButton_UjUgyiratDarab.Enabled = true;
                ImageButton_UjUgyiratDarab.CssClass = "";
            }
            else
            {
                Label_UjUgyiratDarab.Enabled = false;
                UjUgyiratDarabNeve_TextBox.Enabled = false;
                ImageButton_UjUgyiratDarab.Enabled = false;
                ImageButton_UjUgyiratDarab.CssClass = "disableditem";
            }

            // ügyiratdarab törlése:
            if (selectedNode != null && selectedNodeType == Constants.UgyiratTerkepNodeTypes.UgyiratDarab && selectedNode.Checked == false && isInParentUgyirat)
            {
                Label_DeleteUgyiratDarab.Enabled = true;
                ImageButton_DeleteUgyiratDarab.Enabled = true;
                ImageButton_DeleteUgyiratDarab.CssClass = "";
            }
            else
            {
                Label_DeleteUgyiratDarab.Enabled = false;
                ImageButton_DeleteUgyiratDarab.Enabled = false;
                ImageButton_DeleteUgyiratDarab.CssClass = "disableditem";
            }

            #endregion

        }
        else
        {
            EFormPanel_IratMozgatas.Visible = false;
            EFormPanel_UgyiratDarabKezeles.Visible = false;
        }

        #endregion

        if (TreeView1.Nodes.Count > 0)
        {
            TreeNode selectedNode = TreeView1.SelectedNode;
            FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
            if (fm != null && selectedNode != null)
            {
                string objekumId = selectedNode.Value;
                string objektumType = GetObjektumType(selectedNode);
                fm.ObjektumId = objekumId;
                fm.ObjektumType = objektumType;
            }
        }

    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        //if (Command == CommandName.Modify)
        //{
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshUgyiratTerkep:
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                    break;
                default:
                    string[] items = eventArgument.Split("|".ToCharArray());
                    if (items.Length > 1 && items[0] == EventArgumentConst.refresh)
                    {
                        TreeNode treeNode = TreeView1.FindNode(items[1]);
                        if (treeNode != null)
                        {
                            treeNode.Selected = true;
                            RefreshSelectedNode();
                            RaiseEvent_OnChangedObjectProperties();
                        }
                    }
                    break;
            }
        }
        //}
    }

    public void ReLoadTab()
    {
        if (ParentForm != Constants.ParentForms.IraIrat
            && ParentForm != Constants.ParentForms.IratPeldany
            && ParentForm != Constants.ParentForms.Ugyirat
            && ParentForm != Constants.ParentForms.UgyiratDarab)
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            Active = false;
        }
        else
        {
            if (!Active) return;

            //switch (ParentForm)
            //{ 
            //    case Constants.ParentForms.Ugyirat:
            //        SetButtonsByUgyUgyiratok();
            //        break;
            //    case Constants.ParentForms.UgyiratDarab:
            //        SetButtonsByUgyUgyiratDarabok();
            //        break;
            //    case Constants.ParentForms.IraIrat:
            //        SetButtonsByIraIratok();
            //        break;
            //    case Constants.ParentForms.IratPeldany:
            //        SetButtonsByPldIratPeldanyok();
            //        break;
            //}

            ViewState["Loaded"] = false;
            //if (ViewState["UgyiratDictionary"] == null) LoadTreeView(ParentForm, ParentId);
            LoadTreeView(ParentForm, ParentId);
            ViewState["Loaded"] = true;
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Visszadja az ügyirattérkép fájából azon node-okat, melyek kibonthatóak (nem migrált, nem iratpéldány és nem küldemény)
    /// </summary>
    /// <param name="root">A figyelembe vett részfa gyökér node-ja.</param>
    /// <returns>Dictionary, melynek kucsai az objektum típusát jelzõ táblanevek, értékei pedig TreeNode típusú elemlisták
    /// (késõbb ezekhez a node-okhoz adjuk hozzá a lekért új elemeket)</returns>
    private Dictionary<string, List<TreeNode>> GetAllCompletedEventArgsLeaves(TreeNode root)
    {
        Stack<TreeNode> VisitStack = new Stack<TreeNode>();

        Dictionary<TreeNode, int> ParentChildrenCnt = new Dictionary<TreeNode, int>();
        Dictionary<TreeNode, TreeNode> ChildAndParent = new Dictionary<TreeNode, TreeNode>();

        List<TreeNode> Ugyiratok = new List<TreeNode>();
        List<TreeNode> UgyiratDarabok = new List<TreeNode>();
        List<TreeNode> Iratok = new List<TreeNode>();

        Dictionary<string, List<TreeNode>> dictionaryResult = new Dictionary<string, List<TreeNode>>();
        if (root != null)
        {
            VisitStack.Push(root);

            while (VisitStack.Count > 0)
            {
                TreeNode tn = VisitStack.Pop();
                ParentChildrenCnt.Add(tn, 0);

                string objektumType = GetObjektumType(tn);
                string value = tn.Value;

                if (tn.ChildNodes.Count > 0)
                {
                    foreach (TreeNode child in tn.ChildNodes)
                    {
                        VisitStack.Push(child);

                        ChildAndParent.Add(child, tn);
                        ParentChildrenCnt[tn]++;
                    }
                }
                else
                {
                    switch (objektumType)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            if (UgyiratDictionary.ContainsKey(value))
                            {
                                Ugyiratok.Statusz statusz = UgyiratDictionary[value];
                                if (statusz.Allapot == UgyiratTipus.EloIratMigralt.ToString())
                                {
                                    // leaf type: remove parents from dictionary
                                    TreeNode Child = tn;
                                    TreeNode Parent = ChildAndParent[Child];
                                    ChildAndParent.Remove(Child);
                                    if (Ugyiratok.Contains(Child))
                                    {
                                        Ugyiratok.Remove(Child);
                                    }
                                    ParentChildrenCnt[Parent]--;

                                    while (ParentChildrenCnt[Parent] <= 0 && ChildAndParent.ContainsKey(Parent))
                                    {
                                        Child = Parent;
                                        Parent = ChildAndParent[Child];
                                        ChildAndParent.Remove(Child);
                                        if (Ugyiratok.Contains(Child))
                                        {
                                            Ugyiratok.Remove(Child);
                                        }
                                        ParentChildrenCnt[Parent]--;
                                    }
                                }
                                else
                                {
                                    Ugyiratok.Add(tn);
                                }
                            }
                            else
                            {
                                Ugyiratok.Add(tn);
                            }
                            break;
                        case Constants.TableNames.EREC_UgyUgyiratdarabok:
                            UgyiratDarabok.Add(tn);
                            break;
                        case Constants.TableNames.EREC_IraIratok:
                            Iratok.Add(tn);
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            {
                                // leaf type: remove parents from dictionary
                                TreeNode Child = tn;
                                TreeNode Parent = ChildAndParent[Child];
                                ChildAndParent.Remove(Child);
                                if (Iratok.Contains(Child))
                                {
                                    Iratok.Remove(Child);
                                }
                                ParentChildrenCnt[Parent]--;

                                while (ParentChildrenCnt[Parent] <= 0 && ChildAndParent.ContainsKey(Parent))
                                {
                                    Child = Parent;
                                    Parent = ChildAndParent[Child];
                                    ChildAndParent.Remove(Child);
                                    if (UgyiratDarabok.Contains(Child))
                                    {
                                        UgyiratDarabok.Remove(Child);
                                    }
                                    else if (Ugyiratok.Contains(Child))
                                    {
                                        Ugyiratok.Remove(Child);
                                    }
                                    ParentChildrenCnt[Parent]--;
                                }
                            }
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            {
                                // leaf type: remove parents from dictionary
                                TreeNode Child = tn;
                                TreeNode Parent = ChildAndParent[Child];
                                ChildAndParent.Remove(Child);
                                if (Iratok.Contains(Child))
                                {
                                    Iratok.Remove(Child);
                                }
                                else if (Ugyiratok.Contains(Child))
                                {
                                    Ugyiratok.Remove(Child);
                                }
                                ParentChildrenCnt[Parent]--;

                                while (ParentChildrenCnt[Parent] <= 0 && ChildAndParent.ContainsKey(Parent))
                                {
                                    Child = Parent;
                                    Parent = ChildAndParent[Child];
                                    ChildAndParent.Remove(Child);
                                    if (UgyiratDarabok.Contains(Child))
                                    {
                                        UgyiratDarabok.Remove(Child);
                                    }
                                    else if (Ugyiratok.Contains(Child))
                                    {
                                        Ugyiratok.Remove(Child);
                                    }
                                    ParentChildrenCnt[Parent]--;
                                }
                            }
                            break;
                    }
                }
            }

            dictionaryResult.Add(Constants.TableNames.EREC_UgyUgyiratok, Ugyiratok);
            dictionaryResult.Add(Constants.TableNames.EREC_UgyUgyiratdarabok, UgyiratDarabok);
            dictionaryResult.Add(Constants.TableNames.EREC_IraIratok, Iratok);
        }

        return dictionaryResult;
    }

    protected void ImageExpandNode_Click(object sender, ImageClickEventArgs e)
    {
        if (TreeView1.SelectedNode != null)
        {
            bool bCanBeExpanded = false;
            string objektumType = GetObjektumType(TreeView1.SelectedNode);
            string value = TreeView1.SelectedNode.Value;
            switch (objektumType)
            {
                case Constants.TableNames.EREC_UgyUgyiratok:
                    if (!UgyiratDictionary.ContainsKey(value)
                        || UgyiratDictionary[value].Allapot != UgyiratTipus.EloIratMigralt.ToString())
                    {
                        bCanBeExpanded = true;
                    }
                    break;
                case Constants.TableNames.EREC_UgyUgyiratdarabok:
                case Constants.TableNames.EREC_IraIratok:
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    bCanBeExpanded = true;
                    break;
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    // leaf type: do nothing
                    break;
            }


            if (bCanBeExpanded == true)
            {
                Dictionary<string, List<TreeNode>> dictionaryResult = GetAllCompletedEventArgsLeaves(TreeView1.SelectedNode);
                List<string> lstUgyiratIds = new List<string>();
                List<string> lstUgyiratDarabIds = new List<string>();
                List<string> lstIratIds = new List<string>();
                List<string> lstIratPldIds = new List<string>();

                // a dictionary elemei idõközben bõvülhetnek, ezért biztonság kedvéért létrehozzuk a kulcsait üres listával,
                // ha véletlenül nem létezne (bár elvileg ilyen nem lehet)
                if (dictionaryResult.ContainsKey(Constants.TableNames.EREC_UgyUgyiratok))
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_UgyUgyiratok])
                    {
                        lstUgyiratIds.Add(tn.Value);
                    }
                }
                else
                {
                    dictionaryResult.Add(Constants.TableNames.EREC_UgyUgyiratok, new List<TreeNode>());
                }

                if (dictionaryResult.ContainsKey(Constants.TableNames.EREC_UgyUgyiratdarabok))
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_UgyUgyiratdarabok])
                    {
                        lstUgyiratDarabIds.Add(tn.Value);
                    }
                }
                else
                {
                    dictionaryResult.Add(Constants.TableNames.EREC_UgyUgyiratdarabok, new List<TreeNode>());
                }

                if (dictionaryResult.ContainsKey(Constants.TableNames.EREC_IraIratok))
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_IraIratok])
                    {
                        lstIratIds.Add(tn.Value);
                    }
                }
                else
                {
                    dictionaryResult.Add(Constants.TableNames.EREC_IraIratok, new List<TreeNode>());
                }
                if (dictionaryResult.ContainsKey(Constants.TableNames.EREC_PldIratPeldanyok))
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_PldIratPeldanyok])
                    {
                        lstIratPldIds.Add(tn.Value);
                    }
                }
                else
                {
                    dictionaryResult.Add(Constants.TableNames.EREC_PldIratPeldanyok, new List<TreeNode>());
                }

                ExecParam execParam_ugyirat = UI.SetExecParamDefault(Page);

                EREC_UgyUgyiratokService service_ugyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                Result result_datasets = service_ugyirat.GetAllUgyiratTerkepDataSetsForExpandAll(execParam_ugyirat, lstUgyiratIds.ToArray(), lstUgyiratDarabIds.ToArray(), lstIratIds.ToArray());

                if (result_datasets.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_datasets);
                    ErrorUpdatePanel1.Update();
                    MainPanel.Visible = false;
                    return;
                }

                DataTable table_ugyirat = null;
                DataTable table_regi = null;
                DataTable table_ugyiratdarabok = null;
                DataTable table_iratok = null;
                DataTable table_pld = null;
                DataTable table_kuld = null;

                DataTable table_kuld_kapcsolt = null;
                DataTable table_kuld_ugyiratkapcsolt = null;

                DataTable table_kuld_IratPld = null; //nekrisz CR 2961

                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_UgyUgyiratok))
                {
                    table_ugyirat = result_datasets.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratok];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.MIG_Foszam))
                {
                    table_regi = result_datasets.Ds.Tables[Constants.TableNames.MIG_Foszam];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_UgyUgyiratdarabok))
                {
                    table_ugyiratdarabok = result_datasets.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratdarabok];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_IraIratok))
                {
                    table_iratok = result_datasets.Ds.Tables[Constants.TableNames.EREC_IraIratok];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_PldIratPeldanyok))
                {
                    table_pld = result_datasets.Ds.Tables[Constants.TableNames.EREC_PldIratPeldanyok];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_KuldKuldemenyek))
                {
                    table_kuld = result_datasets.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_KuldKuldemenyek + "Kapcsolt"))
                {
                    table_kuld_kapcsolt = result_datasets.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek + "Kapcsolt"];
                }
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_KuldKuldemenyek + "UgyiratKapcsolt"))
                {
                    table_kuld_ugyiratkapcsolt = result_datasets.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek + "UgyiratKapcsolt"];
                }
                // nekrisz CR2961
                if (result_datasets.Ds.Tables.Contains(Constants.TableNames.EREC_Kuldemeny_IratPeldanyai))
                {
                    table_kuld_IratPld = result_datasets.Ds.Tables[Constants.TableNames.EREC_Kuldemeny_IratPeldanyai];
                }

                if (table_ugyirat != null)
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_UgyUgyiratok])
                    {
                        AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratdarabok, tn.Value, true, tn);

                        AddUgyiratDarabNodes(tn, table_ugyiratdarabok, false);

                        // Ügyirathoz kapcsolt küldemények
                        AddKapcsoltKuldemenyNodes(tn, table_kuld_ugyiratkapcsolt, false);

                        // ügyiratdarab node-ok bõvítése az újonnan a fához adott ügyiratok gyerekeivel
                        foreach (TreeNode child in tn.ChildNodes)
                        {
                            if (!dictionaryResult[Constants.TableNames.EREC_UgyUgyiratdarabok].Contains(child))
                            {
                                if (GetObjektumType(child) == Constants.TableNames.EREC_UgyUgyiratdarabok)
                                {
                                    dictionaryResult[Constants.TableNames.EREC_UgyUgyiratdarabok].Add(child);
                                }
                            }
                        }
                    }
                }

                if (table_iratok != null)
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_UgyUgyiratdarabok])
                    {
                        string id = tn.Value;

                        AddIratNode(tn, table_iratok);

                        // irat node-ok bõvítése az újonnan a fához adott ügyiratdarabok gyerekeivel
                        foreach (TreeNode child in tn.ChildNodes)
                        {
                            if (!dictionaryResult[Constants.TableNames.EREC_IraIratok].Contains(child))
                            {
                                if (GetObjektumType(child) == Constants.TableNames.EREC_IraIratok)
                                {
                                    dictionaryResult[Constants.TableNames.EREC_IraIratok].Add(child);
                                }
                            }
                        }
                    }
                }

                if (table_pld != null)
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_IraIratok])
                    {
                        string id = tn.Value;

                        DataRow[] iratRows = table_iratok.Select("Id='" + id + "'");

                        if (iratRows.Length > 0)
                        {
                            DataRow iratRow = iratRows[0];

                            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
                            erec_IraIratok.Id = id;
                            erec_IraIratok.PostazasIranya = iratRow["PostazasIranya"].ToString();
                            erec_IraIratok.KuldKuldemenyek_Id = iratRow["KuldKuldemenyek_Id"].ToString();
                            //erec_IraIratok.Ugyirat_Id = iratRow["Ugyirat_Id"].ToString();
                            //erec_IraIratok.UgyUgyIratDarab_Id = iratRow["UgyUgyIratDarab_Id"].ToString();

                            //  AddKuldemenyOrIratPeldanyNode(tn, erec_IraIratok, table_pld, table_kuld, table_kuld_IratPld);
                            AddKuldemenyForIratNode(tn, erec_IraIratok, table_kuld);
                            AddIratPeldanyNodes(tn, table_pld, false);
                        }
                    }
                }

                if (table_kuld_kapcsolt != null)
                {
                    foreach (TreeNode tn in dictionaryResult[Constants.TableNames.EREC_IraIratok])
                    {
                        string id = tn.Value;

                        DataRow[] iratRows = table_iratok.Select("Id='" + id + "'");

                        if (iratRows.Length > 0)
                        {
                            DataRow iratRow = iratRows[0];

                            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
                            erec_IraIratok.Id = id;
                            erec_IraIratok.PostazasIranya = iratRow["PostazasIranya"].ToString();
                            //erec_IraIratok.KuldKuldemenyek_Id = iratRow["KuldKuldemenyek_Id"].ToString();
                            //erec_IraIratok.Ugyirat_Id = iratRow["Ugyirat_Id"].ToString();
                            //erec_IraIratok.UgyUgyIratDarab_Id = iratRow["UgyUgyIratDarab_Id"].ToString();

                            AddKapcsoltKuldemenyNodes(tn, erec_IraIratok, table_kuld_kapcsolt);

                        }
                    }
                }

                TreeView1.SelectedNode.ExpandAll();
            }
        }
    }

    protected void ImageCollapseNode_Click(object sender, ImageClickEventArgs e)
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.CollapseAll();
        }
    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        TreeView tv = (TreeView)sender;

        SelectedId = tv.SelectedValue;

        if (String.IsNullOrEmpty(SelectedId)) return;
        if (tv.SelectedNode == null) return;

        int SelectedNodeType = getNodeTypeById(SelectedId);

        switch (SelectedNodeType)
        {
            case Constants.UgyiratTerkepNodeTypes.Ugyirat:
                SelectedNode = Constants.ParentForms.Ugyirat;
                SetButtonsByUgyUgyiratok();
                break;
            case Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                SelectedNode = Constants.ParentForms.UgyiratDarab;
                SetButtonsByUgyUgyiratDarabok();
                break;
            case Constants.UgyiratTerkepNodeTypes.Irat:
                SelectedNode = Constants.ParentForms.IraIrat;
                SetButtonsByIraIratok();
                break;
            case Constants.UgyiratTerkepNodeTypes.IratPeldany:
            case Constants.UgyiratTerkepNodeTypes.Kuldemeny:
                if (tv.SelectedNode.Checked)
                {
                    SelectedNode = Constants.ParentForms.Kuldemeny;
                    SetButtonsByKuldemenyek();
                }
                else
                {
                    SelectedNode = Constants.ParentForms.IratPeldany;
                    SetButtonsByPldIratPeldanyok();
                }
                break;
        }

    }

    #region Buttons

    private void SetButtonsByUgyUgyiratok()
    {
        // -------------- Engedelyek Beallitasa -------------------
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        //// ha a belepesi pont ugyanaz, mint a kijelolt node, akkor minden gomb letiltva!
        //if (ParentForm == SelectedNode && ParentId.ToUpper() == SelectedId.ToUpper())
        //{
        //    return;
        //}


        Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
        UgyiratDictionary.TryGetValue(SelectedId, out UgyiratokStatusz);
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (UgyiratokStatusz == null)
            return;


        #region Modify ÉS View módban is engedélyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MegtekintesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratView");

            if (UgyiratokStatusz.Allapot == UgyiratTipus.EloIratMigralt.ToString())
            {
                ModositasEnabled = false;

                if (MegtekintesEnabled)
                {
                    Megtekintes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack(eMigration.migralasFormUrl,
                      QueryStringVars.Command + "=" + CommandName.View + "&" +
                      QueryStringVars.RegiAdatAzonosito + "=" + Server.UrlEncode(SelectedId),
                      Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                }

                if (Command == CommandName.Modify)
                {
                    SzerelesVisszavonasaEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSzerelesVisszavonasa");

                    if (SzerelesVisszavonasaEnabled)
                    {
                        SzerelesVisszavonasa.OnClientClick = "if (confirm('"
                            + Resources.Question.UIConfirmHeader_SzerlesVisszavonasa
                            + "')) {return true; } else { return false; }";
                    }
                }

                return;
            }

            ErrorDetails errorDetail_modositas;
            ModositasEnabled = MegtekintesEnabled && FunctionRights.GetFunkcioJog(Page, "UgyiratModify")
               && Contentum.eRecord.Utility.Ugyiratok.Modosithato(UgyiratokStatusz, execParam, out errorDetail_modositas);

            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                BoritoA4Enabled = false;
            }
            else
            {
                BoritoA4Enabled = true;
            }

            if (bEnableObjectRightCheck)
            {
                if (MegtekintesEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Ugyirat, 'O'))
                    {
                        MegtekintesEnabled = false;
                        ModositasEnabled = false;
                    }
                }

                if (ModositasEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Ugyirat, 'I'))
                    {
                        ModositasEnabled = false;
                    }
                }
            }

            if (MegtekintesEnabled)
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                           , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                           , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (ModositasEnabled)
            {
                Modositas.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            if (BoritoA4Enabled)
            {
                //LZS - BUG_4776 - A a selectedId alapján hívjuk a riport page-t.
                BoritoA4.OnClientClick = "javascript:window.open('UgyUgyiratokPrintFormSSRSBorito.aspx?" + QueryStringVars.UgyiratId + "=" + SelectedId + "&tipus=BoritoA4')";

            }
        }

        #endregion

        #region Csak Command==Modify esetén engedélyezett gombok:

        if (Command == CommandName.Modify)
        {
            ErrorDetails errorDetail = null;
            SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSztorno")
                && Ugyiratok.Sztornozhato(UgyiratokStatusz, execParam, out errorDetail);

            LezarasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratLezaras")
                && Ugyiratok.Lezarhato(UgyiratokStatusz, execParam, out errorDetail);

            SzerelesVisszavonasaEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSzerelesVisszavonasa")
                && Ugyiratok.SzereltVisszavonhato(UgyiratokStatusz, execParam, out errorDetail);


            if (SzerelesVisszavonasaEnabled)
            {
                SzerelesVisszavonasa.OnClientClick = "if (confirm('"
                    + Resources.Question.UIConfirmHeader_SzerlesVisszavonasa
                    + "')) {return true; } else { return false; }";
            }

            if (SztornoEnabled)
            {
                SztornoPopup.SetOnclientClickShowFunction(Sztorno);
            }

            if (LezarasEnabled)
            {
                Lezaras.OnClientClick = JavaScripts.SetOnClientClick("LezarasForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + SelectedId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }
        }

        #endregion

        AtadasraKijelolesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtadas");
        // BLG_336
        ErrorDetails errorDetail_atvetel;
        if (Ugyiratok.AtvehetoAllapotu(UgyiratokStatusz, out errorDetail_atvetel))
        {
            AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetel");
            AtvetelUgyintezesreEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetelUgyintezesre");
        }
        else
        {
            AtvetelEnabled = false;
            AtvetelUgyintezesreEnabled = false;
        }

        Session["SelectedUgyiratIds"] = SelectedId.ToString();
        Session["SelectedKuldemenyIds"] = null;
        Session["SelectedBarcodeIds"] = null;
        Session["SelectedIratPeldanyIds"] = null;
        Session["SelectedDosszieIds"] = null;

        if (AtadasraKijelolesEnabled)
        {
            ImageButton_AtadasraKijeloles.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
        }
        if (AtvetelEnabled)
        {
            ImageButton_Atvetel.OnClientClick = JavaScripts.SetOnClientClick("AtvetelForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
        }
        if (AtvetelUgyintezesreEnabled)
            ImageButton_AtvetelUgyintezesre.OnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreTomegesForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);

    }

    private void SetButtonsByUgyUgyiratDarabok()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        //// ha a belepesi pont ugyanaz, mint a kijelolt node, akkor minden gomb letiltva!
        //if (ParentForm == SelectedNode && ParentId == SelectedId)
        //{
        //    return;
        //}


        Contentum.eRecord.Utility.UgyiratDarabok.Statusz UgyiratDarabokStatusz = null;
        UgyiratDarabokDictionary.TryGetValue(SelectedId, out UgyiratDarabokStatusz);
        if (UgyiratDarabokStatusz == null)
            return;

        Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
        UgyiratDictionary.TryGetValue(UgyiratDarabokStatusz.Ugyirat_Id, out UgyiratokStatusz);
        if (UgyiratokStatusz == null)
            return;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


        #region Modify ÉS View módban is engedélyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MegtekintesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratdarabView");

            ErrorDetails errorDetail_modositas;
            ModositasEnabled = MegtekintesEnabled && FunctionRights.GetFunkcioJog(Page, "UgyiratdarabModify")
               && Contentum.eRecord.Utility.UgyiratDarabok.Modosithato(UgyiratDarabokStatusz, UgyiratokStatusz, out errorDetail_modositas);

            if (bEnableObjectRightCheck)
            {
                if (MegtekintesEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.UgyiratDarab, 'O'))
                    {
                        MegtekintesEnabled = false;
                        ModositasEnabled = false;
                    }
                }

                if (ModositasEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.UgyiratDarab, 'I'))
                    {
                        ModositasEnabled = false;
                    }
                }
            }


            if (MegtekintesEnabled)
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID);
            }

            if (ModositasEnabled)
            {
                Modositas.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratdarabokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }
        }

        #endregion

        #region Csak Command==Modify esetén engedélyezett gombok:

        if (Command == CommandName.Modify)
        {



        }

        #endregion


    }

    private void SetButtonsByIraIratok()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        //bool sajatNode = false;
        //// ha a belepesi pont ugyanaz, mint a kijelolt node, akkor a megtekintés/módosítás gombok letiltva!
        //if (ParentForm == SelectedNode && ParentId == SelectedId)
        //{
        //    sajatNode = true;
        //}


        Contentum.eRecord.Utility.Iratok.Statusz IratokStatusz = null;
        IratokDictionary.TryGetValue(SelectedId, out IratokStatusz);
        if (IratokStatusz == null)
            return;

        Contentum.eRecord.Utility.UgyiratDarabok.Statusz UgyiratDarabokStatusz = null;
        UgyiratDarabokDictionary.TryGetValue(IratokStatusz.UgyiratDarab_Id, out UgyiratDarabokStatusz);
        if (UgyiratDarabokStatusz == null)
            return;

        Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
        UgyiratDictionary.TryGetValue(UgyiratDarabokStatusz.Ugyirat_Id, out UgyiratokStatusz);
        if (UgyiratokStatusz == null)
            return;

        ExecParam execParam = UI.SetExecParamDefault(Page);
        ErrorDetails errorDetail = null;


        #region Modify ÉS View módban is engedélyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MegtekintesEnabled = FunctionRights.GetFunkcioJog(Page, "IraIratView");

            ErrorDetails errorDetail_Modositas;
            ModositasEnabled = MegtekintesEnabled && FunctionRights.GetFunkcioJog(Page, "IraIratModify")
               && Contentum.eRecord.Utility.Iratok.Modosithato(IratokStatusz, UgyiratokStatusz, execParam, out errorDetail_Modositas);

            if (bEnableObjectRightCheck)
            {
                if (MegtekintesEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Irat, 'O'))
                    {
                        MegtekintesEnabled = false;
                        ModositasEnabled = false;
                    }
                }

                if (ModositasEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Irat, 'I'))
                    {
                        ModositasEnabled = false;
                    }
                }
            }


            if (MegtekintesEnabled)
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (ModositasEnabled)
            {
                Modositas.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            AtiktatasEnabled = FunctionRights.GetFunkcioJog(Page, "AtIktatas")
                && Iratok.AtIktathato(IratokStatusz, UgyiratokStatusz, execParam, out errorDetail);

            if (AtiktatasEnabled)
            {
                Atiktatas.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IratId + "=" + SelectedId
                  + "&" + QueryStringVars.Mode + "=" + CommandName.AtIktatas
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            FelszabaditasEnabled = FunctionRights.GetFunkcioJog(Page, "IratFelszabaditas")
                && Iratok.Felszabadithato(IratokStatusz, UgyiratokStatusz, execParam, out errorDetail);

            if (FelszabaditasEnabled)
            {
                Felszabaditas.OnClientClick = JavaScripts.SetOnClientClick("FelszabaditasForm.aspx"
                , QueryStringVars.IratId + "=" + SelectedId
                , Defaults.PopupWidth_Max, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam irat_execParam = UI.SetExecParamDefault(Page, new ExecParam());
            irat_execParam.Record_Id = SelectedId;

            Result res = service.Get(irat_execParam);

            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)res.Record;

                if (erec_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
                {
                    // ellenõrizendõ, hogy bizalmas-e
                    if (Iratok.CheckRights_Csatolmanyok(Page, erec_IraIratok, false))
                    {
                        EMailNyomtatasEnabled = true;
                        EMailNyomtatas.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("eMail_PrintForm.aspx?" + QueryStringVars.IratId + "=" + SelectedId);
                    }
                    else
                    {
                        EMailNyomtatasEnabled = false;
                        EMailNyomtatas.OnClientClick = "";
                    }
                }
            }

            IratokStatuszExtended extendedIratokStatusz = IratokStatusz as IratokStatuszExtended;
            if (extendedIratokStatusz.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                ValaszIratEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

                if (ValaszIratEnabled)
                {
                    ValaszIrat.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.New
                     + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                     + "&" + QueryStringVars.UgyiratId + "=" + IratokStatusz.UgyiratId
                     + "&" + QueryStringVars.IratId + "=" + SelectedId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
                }
            }

        }

        #endregion

        #region Csak Command==Modify esetén engedélyezett gombok:

        if (Command == CommandName.Modify)
        {
            MunkaanyagBeiktatasEnabled = (FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Bejovo")
                || FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Belso"))
                && Iratok.BeiktathatoAMunkairat(IratokStatusz, out errorDetail);
            if (MunkaanyagBeiktatasEnabled)
            {
                MunkaanyagBeiktatas.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas
                          + "&" + QueryStringVars.Id + "=" + SelectedId
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
                MunkaanyagBeiktatas.Visible = true;
            }
            else
            {
                // ne is látszódjon:
                MunkaanyagBeiktatas.Visible = false;
            }
        }
        #endregion

    }

    private void SetButtonsByPldIratPeldanyok()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        //// ha a belepesi pont ugyanaz, mint a kijelolt node, akkor minden gomb letiltva!
        //if (ParentForm == SelectedNode && ParentId == SelectedId)
        //{
        //    return;
        //}

        Contentum.eRecord.Utility.IratPeldanyok.Statusz IratPeldanyokStatusz = null;
        IratPeldanyokDictionary.TryGetValue(SelectedId, out IratPeldanyokStatusz);
        if (IratPeldanyokStatusz == null)
            return;

        Contentum.eRecord.Utility.Iratok.Statusz IratokStatusz = null;
        IratokDictionary.TryGetValue(IratPeldanyokStatusz.Irat_Id, out IratokStatusz);
        if (IratokStatusz == null)
            return;

        Contentum.eRecord.Utility.UgyiratDarabok.Statusz UgyiratDarabokStatusz = null;
        UgyiratDarabokDictionary.TryGetValue(IratokStatusz.UgyiratDarab_Id, out UgyiratDarabokStatusz);
        if (UgyiratDarabokStatusz == null)
            return;

        Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
        UgyiratDictionary.TryGetValue(UgyiratDarabokStatusz.Ugyirat_Id, out UgyiratokStatusz);
        if (UgyiratokStatusz == null)
            return;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


        #region Modify ÉS View módban is engedélyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MegtekintesEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyView");

            ErrorDetails errorDetail_Modositas;
            bool hasFunctionRight = FunctionRights.GetFunkcioJog(Page, "IratPeldanyModify");
            bool isIratPeldanyModosithato = Contentum.eRecord.Utility.IratPeldanyok.Modosithato(IratPeldanyokStatusz, IratokStatusz, UgyiratokStatusz
                , execParam, out errorDetail_Modositas);
            bool isModosithatoOverride = Contentum.eRecord.Utility.IratPeldanyok.IsModosithatoOveride(IratPeldanyokStatusz, execParam); // BOPMH nál ha az IRATPELDANY_MODOSITHATO_MINDIG rendszer paraméter igaz és nem sztornozott akkor lehet módosítani 3 mezőt mindig
            ModositasEnabled = MegtekintesEnabled && hasFunctionRight && (isIratPeldanyModosithato || isModosithatoOverride);

            BoritoA4Enabled = true;

            if (bEnableObjectRightCheck)
            {
                if (MegtekintesEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.IratPeldany, 'O'))
                    {
                        MegtekintesEnabled = false;
                        ModositasEnabled = false;
                    }
                }

                if (ModositasEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.IratPeldany, 'I'))
                    {
                        ModositasEnabled = false;
                    }
                }
            }

            if (MegtekintesEnabled)
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (ModositasEnabled)
            {
                Modositas.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            if (BoritoA4Enabled)
            {
                //LZS - BUG_4776 - A a selectedId alapján hívjuk a riport page-t.
                BoritoA4.OnClientClick = BoritoA4.OnClientClick = "javascript:window.open('IratPeldanyokSSRSBorito.aspx?" + QueryStringVars.IratPeldanyId + "=" + SelectedId + "&tipus=Borito')";
            }


        }

        #endregion

        #region Csak Command==Modify esetén engedélyezett gombok:

        if (Command == CommandName.Modify)
        {
            ErrorDetails errorDetail = null;

            SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanySztorno")
                && IratPeldanyok.Sztornozhato(IratPeldanyokStatusz, UgyiratokStatusz, execParam, out errorDetail);

            ExpedialasEnabled = FunctionRights.GetFunkcioJog(Page, "Expedialas")
                && IratPeldanyok.Expedialhato(IratPeldanyokStatusz, false, IratokStatusz, UgyiratokStatusz, execParam, out errorDetail);

            PostazasEnabled = FunctionRights.GetFunkcioJog(Page, "Postazas")
                && IratPeldanyok.Postazhato(IratPeldanyokStatusz, false, IratokStatusz, UgyiratokStatusz, execParam, out errorDetail);


            if (SztornoEnabled)
            {
                SztornoPopup.SetOnclientClickShowFunction(Sztorno);
            }

            if (ExpedialasEnabled)
            {
                Expedialas.OnClientClick = JavaScripts.SetOnClientClick("ExpedialasForm.aspx"
                    , QueryStringVars.IratPeldanyId + "=" + SelectedId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
            }

            if (PostazasEnabled)
            {
                Postazas.OnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx",
                   QueryStringVars.IratPeldanyId + "=" + SelectedId
                   , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);

                //Postazas.OnClientClick = "if (confirm('"
                //        + Resources.Question.UIConfirmHeader_IratPeldanyPostazas
                //        + "')) { } else { return false; }";
            }
        }

        #endregion


        if (Contentum.eRecord.Utility.IratPeldanyok.Iktatott(IratPeldanyokStatusz) ||
            Contentum.eRecord.Utility.IratPeldanyok.Jovahagyas_alatt(IratPeldanyokStatusz) ||
            Contentum.eRecord.Utility.IratPeldanyok.Kiadmanyozott(IratPeldanyokStatusz) ||
            Contentum.eRecord.Utility.IratPeldanyok.Ujrakuldendo(IratPeldanyokStatusz)
            )
        {
            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
            {
                BoritoA4Enabled = false;
            }
            else
            {
                BoritoA4Enabled = true;
            }
        }
        else
        {
            BoritoA4Enabled = false;
        }

        // BLG_336
        ErrorDetails errorDetail_atvetel;
        if (IratPeldanyok.Atveheto(IratPeldanyokStatusz, UgyiratokStatusz, execParam, out errorDetail_atvetel))
        {
            AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtvetel");
        }
        else
        {
            AtvetelEnabled = false;
        }

        AtadasraKijelolesEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtadas");
        AtvetelUgyintezesreEnabled = false;

        Session["SelectedIratPeldanyIds"] = SelectedId.ToString();
        Session["SelectedKuldemenyIds"] = null;
        Session["SelectedBarcodeIds"] = null;
        Session["SelectedUgyiratIds"] = null;
        Session["SelectedDosszieIds"] = null;

        if (AtadasraKijelolesEnabled)
        {
            ImageButton_AtadasraKijeloles.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
        }
        if (AtvetelEnabled)
        {
            ImageButton_Atvetel.OnClientClick = JavaScripts.SetOnClientClick("AtvetelForm.aspx"
                  , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);
        }
        if (AtvetelUgyintezesreEnabled)
            ImageButton_AtvetelUgyintezesre.OnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreTomegesForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Atvetel + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);


    }

    private void SetButtonsByKuldemenyek()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        //// ha a belepesi pont ugyanaz, mint a kijelolt node, akkor minden gomb letiltva!
        //if (ParentForm == SelectedNode && ParentId == SelectedId)
        //{
        //    return;
        //}

        Contentum.eRecord.Utility.Kuldemenyek.Statusz KuldemenyekStatusz = null;
        string objektumTypeParent = null;
        if (TreeView1.SelectedNode != null && TreeView1.SelectedNode.Parent != null)
        {
            objektumTypeParent = GetObjektumType(TreeView1.SelectedNode.Parent);
        }

        bool isKapcsolt = (objektumTypeParent == Constants.TableNames.EREC_UgyUgyiratok
            || objektumTypeParent == Constants.TableNames.EREC_IraIratok || objektumTypeParent == Constants.TableNames.EREC_PldIratPeldanyok);

        if (isKapcsolt)
        {
            KuldemenyekDictionary.TryGetValue(SelectedId, out KuldemenyekStatusz);
            if (KuldemenyekStatusz == null)
                return;
        }

        //Contentum.eRecord.Utility.Iratok.Statusz IratokStatusz = null;
        //IratokDictionary.TryGetValue(KuldemenyekStatusz.Irat_Id, out IratokStatusz);
        //if (IratokStatusz == null)
        //    return;

        //Contentum.eRecord.Utility.UgyiratDarabok.Statusz UgyiratDarabokStatusz = null;
        //UgyiratDarabokDictionary.TryGetValue(IratokStatusz.UgyiratDarab_Id, out UgyiratDarabokStatusz);
        //if (UgyiratDarabokStatusz == null)
        //    return;

        //Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
        //UgyiratDictionary.TryGetValue(UgyiratDarabokStatusz.Ugyirat_Id, out UgyiratokStatusz);
        //if (UgyiratokStatusz == null)
        //    return;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


        #region Modify ÉS View módban is engedélyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            MegtekintesEnabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyView");

            if (isKapcsolt)
            {
                // kapcsolt küldemények lehetnek módosíthatók
                ErrorDetails errorDetail_Modositas;
                ModositasEnabled = MegtekintesEnabled && FunctionRights.GetFunkcioJog(Page, "KuldemenyModify")
                   && Contentum.eRecord.Utility.Kuldemenyek.Modosithato(KuldemenyekStatusz, execParam, out errorDetail_Modositas);

                if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.BORITO_PRINT) == 0)
                {
                    BoritoA4Enabled = false;
                }
                else
                {
                    BoritoA4Enabled = true;
                }
            }
            else
            {
                ModositasEnabled = false;
                BoritoA4Enabled = false;
            }


            //BoritoA4Enabled = true;

            if (bEnableObjectRightCheck)
            {
                if (MegtekintesEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Kuldemeny, 'O'))
                    {
                        MegtekintesEnabled = false;
                        ModositasEnabled = false;
                    }
                }

                if (ModositasEnabled)
                {
                    if (!CheckRight(SelectedId, Constants.UgyiratTerkepNodeTypes.Kuldemeny, 'I'))
                    {
                        ModositasEnabled = false;
                    }
                }
            }

            if (MegtekintesEnabled)
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (ModositasEnabled)
            {
                Modositas.OnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (BoritoA4Enabled)
            {
                BoritoA4.OnClientClick =
                    JavaScripts.SetOnClientClickIFramePrintForm("KuldKuldemenyekFormTabPrintForm.aspx?"
                    + QueryStringVars.KuldemenyId + "=" + SelectedId + "&tipus=BoritoA4");
            }
            //ErrorDetails errorDetail_Postazas;
            //PostazasEnabled = MegtekintesEnabled && FunctionRights.GetFunkcioJog(Page, "Postazas")
            //  && Kuldemenyek.Postazhato(KuldemenyekStatusz,execParam, out errorDetail_Postazas);

            //if (PostazasEnabled)
            //{
            //    Postazas.OnClientClick = JavaScripts.SetOnClientClick("PostazasForm.aspx",
            //       QueryStringVars.KuldemenyId + "=" + SelectedId
            //       , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshUgyiratTerkep);

            //    //Postazas.OnClientClick = "if (confirm('"
            //    //        + Resources.Question.UIConfirmHeader_IratPeldanyPostazas
            //    //        + "')) { } else { return false; }";
            //}
        }

        #endregion



    }

    private void SetAllButtonsEnabled(Boolean value)
    {
        MegtekintesEnabled = value;
        ModositasEnabled = value;
        LetrehozasEnabled = value;
        SztornoEnabled = value;
        ValaszEnabled = value;
        AtiktatasEnabled = value;
        EMailNyomtatasEnabled = value;
        SzerelesVisszavonasaEnabled = value;
        PostazasEnabled = value;
        Kuldemeny_osszesitesEnabled = value;
        ExpedialasEnabled = value;
        BoritoA4Enabled = value;
        LezarasEnabled = value;
        FelszabaditasEnabled = value;
        ValaszIratEnabled = value;
        MunkaanyagBeiktatasEnabled = value;
        // BLG_336
        AtadasraKijelolesEnabled = value;
        AtvetelEnabled = value;
        AtvetelUgyintezesreEnabled = value;
    }

    private void ResetAllButtonsOnClientClick()
    {
        Megtekintes.OnClientClick = "";
        Modositas.OnClientClick = "";
        Letrehozas.OnClientClick = "";
        Sztorno.OnClientClick = "";
        Valasz.OnClientClick = "";
        Atiktatas.OnClientClick = "";
        EMailNyomtatas.OnClientClick = "";
        SzerelesVisszavonasa.OnClientClick = "";
        Postazas.OnClientClick = "";
        Kuldemeny_osszesites.OnClientClick = "";
        Expedialas.OnClientClick = "";
        BoritoA4.OnClientClick = "";
        Lezaras.OnClientClick = "";
        Felszabaditas.OnClientClick = "";
        ValaszIrat.OnClientClick = "";
        MunkaanyagBeiktatas.OnClientClick = "";
    }

    #endregion Buttons

    private void LoadTreeView(String NodeName, String NodeId)
    {
        // státusz Dictionary-k törlése:
        UgyiratDictionary.Clear();
        UgyiratDarabokDictionary.Clear();
        IratokDictionary.Clear();
        IratPeldanyokDictionary.Clear();
        KuldemenyekDictionary.Clear();

        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        int ugyiratTerkepNodeType = 0;

        switch (NodeName)
        {
            case Constants.ParentForms.IratPeldany:
                ugyiratTerkepNodeType = Constants.UgyiratTerkepNodeTypes.IratPeldany;
                break;
            case Constants.ParentForms.IraIrat:
                ugyiratTerkepNodeType = Constants.UgyiratTerkepNodeTypes.Irat;
                break;
            case Constants.ParentForms.UgyiratDarab:
                ugyiratTerkepNodeType = Constants.UgyiratTerkepNodeTypes.UgyiratDarab;
                break;
            case Constants.ParentForms.Ugyirat:
                ugyiratTerkepNodeType = Constants.UgyiratTerkepNodeTypes.Ugyirat;
                break;
            case Constants.ParentForms.Kuldemeny:
                // TODO: megvalósítani a lekérést
                // Jelenleg ez az ág nincs kezelve a webszervízben és hibát okozna.
                // Jelenleg csak megtekintést engedünk
                //return; 
                ugyiratTerkepNodeType = Constants.UgyiratTerkepNodeTypes.Kuldemeny;
                break;
        }

        Result result = service_ugyiratok.GetAllUgyiratTerkepDataSets(execParam, ugyiratTerkepNodeType, NodeId, true);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
            return;
        }

        DataTable table_ugyirat = result.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratok];
        DataTable table_ugyiratDarabok = result.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratdarabok];
        DataTable table_iratok = result.Ds.Tables[Constants.TableNames.EREC_IraIratok];
        DataTable table_pld = result.Ds.Tables[Constants.TableNames.EREC_PldIratPeldanyok];
        DataTable table_kuld = result.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek];
        DataTable table_regi = result.Ds.Tables[Constants.TableNames.MIG_Foszam];

        DataTable table_kuld_kapcsolt = result.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek + "Kapcsolt"];
        DataTable table_kuld_ugyiratkapcsolt = result.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek + "UgyiratKapcsolt"];
        DataTable table_kuld_IratPld = result.Ds.Tables[Constants.TableNames.EREC_Kuldemeny_IratPeldanyai];     // nekrisz CR 2961
        switch (NodeName)
        {
            case Constants.ParentForms.IratPeldany:
                {
                    IratPeldanyHierarchia iratPldHier = (IratPeldanyHierarchia)result.Record;

                    string ugyiratId = iratPldHier.UgyiratObj.Id;
                    string ugyiratDarabId = iratPldHier.UgyiratDarabObj.Id;
                    string iratId = iratPldHier.IratObj.Id;

                    // Ügyirat szint feltöltése:
                    TreeNode ugyiratNode = AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratDarabok, ugyiratId, false, null);

                    // Ügyiratdarabok:
                    AddUgyiratDarabNodes(ugyiratNode, table_ugyiratDarabok, false);

                    // Ügyirathoz kapcsolt küldemények
                    AddKapcsoltKuldemenyNodes(ugyiratNode, table_kuld_ugyiratkapcsolt, true);

                    ugyiratNode.Expanded = true;

                    TreeNode ugyiratDarabNode = TreeView1.FindNode(ugyiratNode.ValuePath + "/" + ugyiratDarabId);
                    if (ugyiratDarabNode != null)
                    {
                        // Iratok:
                        AddIratNode(ugyiratNode, table_iratok);
                        ugyiratDarabNode.Expanded = true;

                        #region Default ÜgyiratDarab(ok) kibontása
                        // Azokat kell kibontani, ahol Checked == true ÉS ChildNodes.Count > 0
                        foreach (TreeNode uiDarabNode in ugyiratNode.ChildNodes)
                        {
                            if (uiDarabNode.Checked && uiDarabNode.ChildNodes.Count > 0)
                            {
                                uiDarabNode.Expanded = true;
                            }
                        }
                        #endregion

                        TreeNode iratNode = TreeView1.FindNode(ugyiratDarabNode.ValuePath + "/" + iratId);
                        if (iratNode != null)
                        {
                            // Iratpéldányok/küldemények:
                            //AddKuldemenyOrIratPeldanyNode(iratNode, ugyiratId, ugyiratDarabId, iratId, iratPldHier.IratObj, table_pld, table_kuld);
                            // AddKuldemenyOrIratPeldanyNode(iratNode, iratPldHier.IratObj, table_pld, table_kuld);
                            AddKuldemenyForIratNode(iratNode, iratPldHier.IratObj, table_kuld);
                            AddIratPeldanyNodes(iratNode, table_pld, false);
                            AddKapcsoltKuldemenyNodes(iratNode, iratPldHier.IratObj, table_kuld_kapcsolt);
                            iratNode.Expanded = true;

                            TreeNode iratPeldanyTreeNode = TreeView1.FindNode(iratNode.ValuePath + "/" + iratPldHier.IratPeldanyObj.Id);
                            if (iratPeldanyTreeNode != null)
                            {
                                iratPeldanyTreeNode.Selected = true;
                                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                            }
                        }
                    }
                }
                break;
            case Constants.ParentForms.IraIrat:
                {
                    IratHierarchia iratHier = (IratHierarchia)result.Record;

                    string ugyiratId = iratHier.UgyiratObj.Id;
                    string ugyiratDarabId = iratHier.UgyiratDarabObj.Id;
                    string iratId = iratHier.IratObj.Id;

                    // Ügyirat szint feltöltése:
                    TreeNode ugyiratNode = AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratDarabok, ugyiratId, false, null);

                    // Ügyiratdarabok:
                    AddUgyiratDarabNodes(ugyiratNode, table_ugyiratDarabok, false);

                    // Ügyirathoz kapcsolt küldemények
                    AddKapcsoltKuldemenyNodes(ugyiratNode, table_kuld_ugyiratkapcsolt, true);

                    //BUG_9377
                    TreeNode ugyiratDarabNode = null;

                    if (ugyiratNode != null)
                    {
                        ugyiratNode.Expanded = true;
                        ugyiratDarabNode = TreeView1.FindNode(ugyiratNode.ValuePath + "/" + ugyiratDarabId);
                    }


                    if (ugyiratDarabNode != null)
                    {
                        // Iratok:
                        AddIratNode(ugyiratNode, table_iratok);
                        ugyiratDarabNode.Expanded = true;

                        #region Default ÜgyiratDarab(ok) kibontása
                        // Azokat kell kibontani, ahol Checked == true ÉS ChildNodes.Count > 0
                        foreach (TreeNode uiDarabNode in ugyiratNode.ChildNodes)
                        {
                            if (uiDarabNode.Checked && uiDarabNode.ChildNodes.Count > 0)
                            {
                                uiDarabNode.Expanded = true;
                            }
                        }
                        #endregion

                        // Irat Select:
                        TreeNode iratNode = TreeView1.FindNode(ugyiratDarabNode.ValuePath + "/" + iratId);
                        if (iratNode != null)
                        {
                            iratNode.Selected = true;
                            TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                        }
                    }
                }
                break;
            case Constants.ParentForms.UgyiratDarab:
                {
                    EREC_UgyUgyiratdarabok ugyiratDarab = (EREC_UgyUgyiratdarabok)result.Record;

                    // Ügyirat szint feltöltése:
                    TreeNode ugyiratNode = AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratDarabok, ugyiratDarab.UgyUgyirat_Id, false, null);

                    // Ügyiratdarabok:
                    AddUgyiratDarabNodes(ugyiratNode, table_ugyiratDarabok, false);

                    // Ügyirathoz kapcsolt küldemények
                    AddKapcsoltKuldemenyNodes(ugyiratNode, table_kuld_ugyiratkapcsolt, true);

                    ugyiratNode.Expanded = true;

                    AddIratNode(ugyiratNode, table_iratok);

                    #region Default ÜgyiratDarab(ok) kibontása
                    // Azokat kell kibontani, ahol Checked == true ÉS ChildNodes.Count > 0
                    foreach (TreeNode ugyiratDarabNode in ugyiratNode.ChildNodes)
                    {
                        if (ugyiratDarabNode.Checked && ugyiratDarabNode.ChildNodes.Count > 0)
                        {
                            ugyiratDarabNode.Expanded = true;
                        }
                    }
                    #endregion

                    // ÜgyiratDarab Node Select
                    TreeNode selectedUgyiratDarabNode = TreeView1.FindNode(ugyiratNode.ValuePath + "/" + ugyiratDarab.Id);
                    if (selectedUgyiratDarabNode != null)
                    {
                        selectedUgyiratDarabNode.Selected = true;
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                    }
                }
                break;
            case Constants.ParentForms.Ugyirat:
                {
                    string ugyiratId = NodeId;

                    // Ügyirat szint feltöltése:
                    TreeNode ugyiratNode = AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratDarabok, ugyiratId, false, null);

                    // Ügyiratdarabok:
                    AddUgyiratDarabNodes(ugyiratNode, table_ugyiratDarabok, false);

                    // Ügyirathoz kapcsolt küldemények
                    AddKapcsoltKuldemenyNodes(ugyiratNode, table_kuld_ugyiratkapcsolt, true);

                    ugyiratNode.Expanded = true;

                    AddIratNode(ugyiratNode, table_iratok);

                    #region Default ÜgyiratDarab(ok) kibontása
                    // Azokat kell kibontani, ahol Checked == true ÉS ChildNodes.Count > 0
                    foreach (TreeNode ugyiratDarabNode in ugyiratNode.ChildNodes)
                    {
                        if (ugyiratDarabNode.Checked && ugyiratDarabNode.ChildNodes.Count > 0)
                        {
                            ugyiratDarabNode.Expanded = true;
                        }
                    }
                    #endregion

                    // Ügyirat Node Select
                    ugyiratNode.Selected = true;
                    TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                }
                break;
        }

    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        TreeNode treeNode = e.Node;
        int nodeType = getNodeTypeById(treeNode.Value);

        switch (nodeType)
        {
            case Constants.UgyiratTerkepNodeTypes.Ugyirat:
                AddUgyiratDarabEsEloiratNodes(treeNode);
                break;
            case Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                string[] idsFromValuePath = treeNode.ValuePath.Split('/');
                int ugyiratSzint = idsFromValuePath.Length - 2;
                string ugyiratId = idsFromValuePath[ugyiratSzint >= 0 ? ugyiratSzint : 0];
                AddIratNode(ugyiratId, e.Node);
                break;
            case Constants.UgyiratTerkepNodeTypes.Irat:
                AddKuldemenyNodes(treeNode);
                AddIratPeldanyNodes(treeNode);

                //AddKuldemenyOrIratPeldanyNode(treeNode);
                break;
            case Constants.UgyiratTerkepNodeTypes.IratPeldany:
                AddKuldemenyForIratPeldanyNodes(treeNode);
                break;
            default:
                break;
        }
    }

    enum UgyiratTipus
    {
        Normal,
        EloIrat,
        UtoIrat,
        EloIratMigralt
    }

    // Ügyirat szint feltöltése
    private TreeNode AddUgyiratNode(DataTable dataTable_ugyiratok, DataTable dataTable_regi, DataTable dataTable_ugyiratDarabok, string UgyUgyiratokId, bool csakEloiratok, TreeNode UgyUgyiratTreeNode)
    {
        DataRow ugyiratRow = null;
        List<DataRow> SzuloUgyiratRowList = new List<DataRow>();
        List<DataRow> EloIratokList = new List<DataRow>();

        foreach (DataRow row in dataTable_ugyiratok.Rows)
        {
            string row_ugyiratId = row["Id"].ToString();
            string row_szuloUgyiratId = row["UgyUgyirat_Id_Szulo"].ToString();

            if (row_ugyiratId == UgyUgyiratokId)
            {
                // ez a szóbanforgó ügyirat rekord:
                if (ugyiratRow != null)
                {
                    // hiba, már volt ez a sor!
                    return null;
                }
                else
                {
                    ugyiratRow = row;
                }
            }
            else if (row_szuloUgyiratId == UgyUgyiratokId)
            {
                EloIratokList.Add(row);
            }
            else
            {
                // elvileg ez szülõ ügyirat:
                SzuloUgyiratRowList.Add(row);
            }
        }

        if (ugyiratRow == null)
        {
            // hiba:
            return null;
        }


        if (!csakEloiratok)
        {
            // Ügyiratfa létrehozása:

            UgyiratTree ugyiratTree = new UgyiratTree(ugyiratRow["Id"].ToString(), ugyiratRow, ugyiratRow["UgyUgyirat_Id_Szulo"].ToString(), false);

            ugyiratTree = BuildUgyiratTree(ugyiratTree, SzuloUgyiratRowList, dataTable_regi);

            UgyUgyiratTreeNode = CreateUgyiratTreeNodes(ugyiratTree, UgyUgyiratokId, null, dataTable_ugyiratDarabok);
        }
        else
        {
            // UgyUgyiratTreeNode -ot paraméterben meg kellett adni
        }

        // elõiratok hozzáadása a fõ ügyirathoz:
        foreach (DataRow row in EloIratokList)
        {
            // Elõiratoknál az UgyUgyiratTreeNode lesz a parent node
            AddUgyiratNode(row, UgyiratTipus.EloIrat, UgyUgyiratTreeNode);
        }

        // migrált elõiratok hozzáadása a fõ ügyirathoz
        if (dataTable_regi != null)
        {
            foreach (DataRow row in dataTable_regi.Rows)
            {
                string row_regiAdat_SzuloUgyiratId = row["SzuloUgyiratId"].ToString();

                if (row_regiAdat_SzuloUgyiratId == UgyUgyiratokId)
                {
                    // Elõiratoknál az UgyUgyiratTreeNode lesz a parent node
                    AddUgyiratNode(row, UgyiratTipus.EloIratMigralt, UgyUgyiratTreeNode);
                }
            }
        }

        return UgyUgyiratTreeNode;
    }

    #region UgyiratTree struktúra

    private class UgyiratTree
    {
        public string ugyiratId;
        public DataRow ugyiratDataRow = null;
        public bool migraltRegiAdat = false;

        public string utoIratId = null;
        public List<UgyiratTree> eloIratList = new List<UgyiratTree>();

        public UgyiratTree(string ugyiratId, DataRow ugyiratDataRow, string utoIratId, bool migraltRegiAdat)
        {
            this.ugyiratId = ugyiratId;
            this.ugyiratDataRow = ugyiratDataRow;
            this.utoIratId = utoIratId;
            this.migraltRegiAdat = migraltRegiAdat;
        }
    }

    private UgyiratTree BuildUgyiratTree(UgyiratTree ugyiratTree, List<DataRow> ugyiratRowList, DataTable dataTable_regi)
    {
        if (ugyiratTree != null && !string.IsNullOrEmpty(ugyiratTree.ugyiratId))
        {
            // ha nincs utóiratId, vége a láncnak, ez a legfelsõbb szint:
            if (String.IsNullOrEmpty(ugyiratTree.utoIratId))
            {
                return ugyiratTree;
            }
            else
            {
                // utóirat megkeresése a listában:
                foreach (DataRow row in ugyiratRowList)
                {
                    string row_Id = row["Id"].ToString();
                    if (row_Id == ugyiratTree.utoIratId)
                    {
                        string row_UgyUgyirat_Id_Szulo = row["UgyUgyirat_Id_Szulo"].ToString();

                        UgyiratTree ugyiratTree_UtoIrat = new UgyiratTree(row_Id, row, row_UgyUgyirat_Id_Szulo, false);
                        //// DataRow kiszedése a listából:
                        //ugyiratRowList.Remove(row);

                        // ugyiratTree felfûzése az elõiratlistához:
                        ugyiratTree_UtoIrat.eloIratList.Add(ugyiratTree);

                        // További elõiratok kiszedése a listából:
                        // (ahol UgyUgyirat_Id_Szulo == ugyiratTree_UtoIrat.ugyiratId
                        foreach (DataRow eloIratRow in ugyiratRowList)
                        {
                            string eloiratRow_UgyUgyirat_Id_Szulo = eloIratRow["UgyUgyirat_Id_Szulo"].ToString();
                            string eloiratRow_Id = eloIratRow["Id"].ToString();

                            if (eloiratRow_UgyUgyirat_Id_Szulo == ugyiratTree_UtoIrat.ugyiratId
                                && eloiratRow_Id != ugyiratTree.ugyiratId)
                            {
                                // felvétel az elõiratlistába:
                                UgyiratTree ugyiratTree_eloirat = new UgyiratTree(eloiratRow_Id, eloIratRow, eloiratRow_UgyUgyirat_Id_Szulo, false);
                                ugyiratTree_UtoIrat.eloIratList.Add(ugyiratTree_eloirat);

                                //// DataRow kiszedése a listából:
                                //ugyiratRowList.Remove(eloIratRow);
                            }
                        }

                        // migrált elõiratok kiszedése (régi adatok)
                        foreach (DataRow row_regiAdat in dataTable_regi.Rows)
                        {
                            string row_regiAdat_SzuloUgyiratId = row_regiAdat["SzuloUgyiratId"].ToString();
                            string row_regiAdat_Id = row_regiAdat["RegiRendszerIktatoszamId"].ToString();

                            if (row_regiAdat_SzuloUgyiratId == ugyiratTree_UtoIrat.ugyiratId
                                && row_regiAdat_Id != ugyiratTree.ugyiratId)
                            {
                                // felvétel az elõiratlistába:
                                UgyiratTree ugyiratTree_migraltEloirat = new UgyiratTree(row_regiAdat_Id, row_regiAdat, row_regiAdat_SzuloUgyiratId, true);
                                ugyiratTree_UtoIrat.eloIratList.Add(ugyiratTree_migraltEloirat);
                            }
                        }

                        // ha már nincs utóirat, vége a láncnak, egyébként rekurzív hívás:
                        if (String.IsNullOrEmpty(ugyiratTree_UtoIrat.utoIratId))
                        {
                            return ugyiratTree_UtoIrat;
                        }
                        else
                        {
                            // rekurzív hívás:
                            return BuildUgyiratTree(ugyiratTree_UtoIrat, ugyiratRowList, dataTable_regi);
                        }
                    }
                }
            }
        }

        return ugyiratTree;
    }

    /// <summary>
    /// Létrehozza az ügyiratfa TreeNode-jait
    /// </summary>
    /// <param name="ugyiratTree"></param>
    /// <param name="foUgyiratId"></param>
    /// <param name="parentTreeNode"></param>
    /// <returns>A fõügyirat TreeNode-jával térünk vissza</returns>
    private TreeNode CreateUgyiratTreeNodes(UgyiratTree ugyiratTree, string foUgyiratId, TreeNode parentTreeNode, DataTable dataTable_ugyiratDarabok)
    {
        if (ugyiratTree == null)
        {
            return null;
        }

        UgyiratTipus ugyiratTipus = UgyiratTipus.Normal;
        if (ugyiratTree.migraltRegiAdat)
        {
            ugyiratTipus = UgyiratTipus.EloIratMigralt;
        }
        else if (ugyiratTree.ugyiratId == foUgyiratId)
        {
            ugyiratTipus = UgyiratTipus.Normal;
        }
        else if (ugyiratTree.eloIratList.Count > 0)
        {
            ugyiratTipus = UgyiratTipus.UtoIrat;
        }
        else
        {
            ugyiratTipus = UgyiratTipus.EloIrat;
        }

        TreeNode fougyiratTreeNode = null;

        TreeNode UgyUgyiratTreeNode = AddUgyiratNode(ugyiratTree.ugyiratDataRow, ugyiratTipus, parentTreeNode);

        if (UgyUgyiratTreeNode != null && UgyUgyiratTreeNode.Value == foUgyiratId)
        {
            fougyiratTreeNode = UgyUgyiratTreeNode;
        }

        // Létrehozzuk a gyerekeit:
        foreach (UgyiratTree childUgyiratTree in ugyiratTree.eloIratList)
        {
            TreeNode foUgyiratTreeNodeFromChild = CreateUgyiratTreeNodes(childUgyiratTree, foUgyiratId, UgyUgyiratTreeNode, dataTable_ugyiratDarabok);
            if (foUgyiratTreeNodeFromChild != null && foUgyiratTreeNodeFromChild.Value == foUgyiratId)
            {
                fougyiratTreeNode = foUgyiratTreeNodeFromChild;
            }
        }

        // Hozzáadjuk a hozzá tartozó ügyiratdarabokat (A fõügyirathoz nem itt)      
        if (ugyiratTipus != UgyiratTipus.Normal)
        {
            AddUgyiratDarabNodes(UgyUgyiratTreeNode, dataTable_ugyiratDarabok, false);
        }

        // ha volt gyereke, kinyitjuk:
        if (UgyUgyiratTreeNode.ChildNodes.Count > 0)
        {
            UgyUgyiratTreeNode.Expand();
        }

        // A fõ ügyirat TreeNode-dal térünk vissza, ha megvan
        return fougyiratTreeNode;
    }

    #endregion

    private const int maxPadding = 5 * 20;
    private const int minWidth = 150;

    private int GetPadding(TreeNode parentTreeNode)
    {
        int depth = parentTreeNode == null ? -1 : parentTreeNode.Depth;
        int padding = Math.Max(0, maxPadding - (depth + 1) * 20);
        return padding;
    }

    private int GetWidth(TreeNode parentTreeNode)
    {
        int width = minWidth + GetPadding(parentTreeNode);
        return width;
    }

    private TreeNode AddUgyiratNode(DataRow dataRow_ugyirat, UgyiratTipus ugyiratTipus, TreeNode parentTreeNode)
    {
        string UgyUgyiratokId = "";

        //if (ugyiratTipus == UgyiratTipus.UtoIrat)
        //{
        //    UgyUgyiratokId = dataRow_ugyirat["UgyUgyirat_Id_Szulo"].ToString();
        //}
        //else
        //{

        //}
        if (ugyiratTipus != UgyiratTipus.EloIratMigralt)
        {
            UgyUgyiratokId = dataRow_ugyirat["Id"].ToString();
        }

        if (ugyiratTipus == UgyiratTipus.Normal)
        {
            // Page_PreRender -ben kell
            FoUgyirat_Id = UgyUgyiratokId;
            FoUgyiratFoszam_HiddenField.Value = dataRow_ugyirat["Foszam_Merge"].ToString();
        }

        string elotag = "";
        // Már nem kell az elõtag, mert már hierarchiába vannak rendezve, onnan meg egyértelmû
        //if (ugyiratTipus == UgyiratTipus.EloIrat)
        //{
        //    elotag = "[Elõirat]&nbsp;";
        //}
        //else if (ugyiratTipus == UgyiratTipus.UtoIrat)
        //{
        //    elotag = "[Utóirat]&nbsp;";
        //}

        String UgyUgyiratokText = String.Empty;
        if (ugyiratTipus == UgyiratTipus.EloIratMigralt)
        {
            //int aktualisEv = System.DateTime.Now.Year;
            //elotag = String.Format("[Elõirat, {0} elõtti]&nbsp;", aktualisEv);
            string migralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE).ToString();
            if (migralasEve == "0") migralasEve = "migrálás";
            elotag = String.Format("[Elõirat, {0} elõtti]&nbsp;", migralasEve);

            UgyUgyiratokId = dataRow_ugyirat["RegirendszerIktatoszam"].ToString();
            string azonosito = dataRow_ugyirat["RegirendszerIktatoszam"].ToString();

            UgyUgyiratokText = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>"
            + SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_ugyirat.gif' alt='Ügyirat' />", UgyUgyiratokId, parentTreeNode) + " &nbsp;</td>";
            UgyUgyiratokText += "<td style='width: 300px;'>";
            UgyUgyiratokText += SetTreeNodeUrl(elotag + "<b>" + azonosito + "</b>", UgyUgyiratokId, parentTreeNode);
            UgyUgyiratokText += "</td>";
            int padding = GetPadding(parentTreeNode);
            UgyUgyiratokText += String.Format("<td style='width: {0}px;'>", 525 + padding);
            UgyUgyiratokText += "</td></tr></table>";
        }
        else
        {

            UgyUgyiratokText = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>"
                + SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_ugyirat.gif' alt='Ügyirat' />", UgyUgyiratokId, parentTreeNode) + " &nbsp;</td>";

            //LZS - BUG_8793
            int fixWidth = SetFixedColumnWidthForUgyiratRow(dataRow_ugyirat, parentTreeNode);

            UgyUgyiratokText += String.Format("<td style='width: {0}px;'>", fixWidth);

            UgyUgyiratokText += SetTreeNodeUrl(elotag + "<b>" + dataRow_ugyirat["Foszam_Merge"].ToString() + "</b>", UgyUgyiratokId, parentTreeNode);
            UgyUgyiratokText += "</td><td style='width: 100px;'>";

            // elõkészített szerelés ikon, ha kell:
            string elokeszitettSzerelesHtml = String.Empty;
            string row_UgyUgyirat_Id_Szulo = dataRow_ugyirat["UgyUgyirat_Id_Szulo"].ToString();
            string row_Allapot = dataRow_ugyirat["Allapot"].ToString();
            string row_TovabbitasAlattAllapot = dataRow_ugyirat["TovabbitasAlattAllapot"].ToString();
            if (!String.IsNullOrEmpty(row_UgyUgyirat_Id_Szulo)
                && row_Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                && row_TovabbitasAlattAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
            {
                elokeszitettSzerelesHtml =
                    "<img src=\"images/hu/ikon/szerelt_piros.gif\" alt=\"Elõkészített szerelés\" height=\"" + imageHeight + "\" width=\"" + imageWidth + "\" style=\"vertical-align: middle;\" />";
            }

            UgyUgyiratokText += SetTreeNodeUrl(elokeszitettSzerelesHtml + dataRow_ugyirat["Allapot_Nev"].ToString() + "&nbsp;&nbsp;", UgyUgyiratokId, parentTreeNode);
            //LZS - BUG_7692
            UgyUgyiratokText += String.Format("</td><td title=\"{0}\" style='word-wrap:break-word; width: 274px; padding-right: 5px;'>", dataRow_ugyirat["Targy"].ToString());
            //UgyUgyiratokText += SetTreeNodeUrl(SpliceText(dataRow_ugyirat["Targy"].ToString(), 50), UgyUgyiratokId, parentTreeNode);
            //BUG_9377
            string targyHtml = dataRow_ugyirat["Targy"].ToString();
            // BUG_12491
            //if (targyHtml.Length >= 40)
            //{
            //    targyHtml = targyHtml.Substring(0, 40) + "...";
            //}

            UgyUgyiratokText += SetTreeNodeUrl(targyHtml, UgyUgyiratokId, parentTreeNode);
            UgyUgyiratokText += "</td><td style='width: 150px;'>";
            // BLG_1014
            //UgyUgyiratokText += SetTreeNodeUrl("<b> Ügyintézõ: </b>" + dataRow_ugyirat["Ugyintezo_Nev"].ToString(), UgyUgyiratokId, parentTreeNode);
            // BUG_12491
            UgyUgyiratokText += SetTreeNodeUrl("<b> " + Contentum.eUtility.ForditasExpressionBuilder.GetForditas("StrUgyiratUgyintezo", "Ügyintézõ:", this.AppRelativeVirtualPath) + " <br /></b>" + dataRow_ugyirat["Ugyintezo_Nev"].ToString(), UgyUgyiratokId, parentTreeNode);
            UgyUgyiratokText += "</td><td style='width: 140px;padding-left:10px;'>";
            UgyUgyiratokText += SetTreeNodeUrl("<b> Ügyfél: </b>" + dataRow_ugyirat["NevSTR_Ugyindito"].ToString(), UgyUgyiratokId, parentTreeNode);
            UgyUgyiratokText += "</td><td style='width: 25px;'>";
            UgyUgyiratokText += "</td><td style=\"width:" + imageWidth + ";\">" + this.getSzereltImage(dataRow_ugyirat)
                                  + "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolasImage(dataRow_ugyirat)
                                  + "</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(dataRow_ugyirat, Constants.UgyiratTerkepNodeTypes.Ugyirat, parentTreeNode)
                                  + "</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(dataRow_ugyirat, Constants.TableNames.EREC_UgyUgyiratok);
            UgyUgyiratokText += "</td></tr></table>";
        }

        #region TreeNode hozzáadása
        // load to Dictionary 
        if (UgyiratDictionary.ContainsKey(UgyUgyiratokId) == false)
        {
            if (ugyiratTipus == UgyiratTipus.EloIratMigralt)
            {
                UgyiratDictionary.Add(UgyUgyiratokId,
                   new Contentum.eRecord.BaseUtility.Ugyiratok.Statusz(UgyUgyiratokId, UgyiratTipus.EloIratMigralt.ToString()));
            }
            else
            {
                UgyiratDictionary.Add(UgyUgyiratokId, Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataRow(dataRow_ugyirat));
            }
        }

        TreeNode UgyUgyiratTreeNode = new TreeNode(UgyUgyiratokText, UgyUgyiratokId);
        if (ugyiratTipus == UgyiratTipus.EloIratMigralt)
        {
            UgyUgyiratTreeNode.PopulateOnDemand = false;
            UgyUgyiratTreeNode.Expanded = true;
        }
        else
        {
            UgyUgyiratTreeNode.PopulateOnDemand = true;
            UgyUgyiratTreeNode.Expanded = false;
        }

        // Az elõiratot a fõ ügyirat alá tesszük az ügyiratdarabok szintjére, egyébként a legfelsõ szintre
        if (parentTreeNode != null)
        {
            parentTreeNode.ChildNodes.Add(UgyUgyiratTreeNode);
        }
        else
        {
            TreeView1.Nodes.Add(UgyUgyiratTreeNode);
        }

        ViewState["UgyiratDictionary"] = UgyiratDictionary;

        return UgyUgyiratTreeNode;

        #endregion


    }


    private void AddUgyiratDarabEsEloiratNodes(TreeNode UgyUgyiratTreeNode)
    {
        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        // itt nem kell lehozni az utóirathierarchiát
        Result result = service_ugyiratok.GetAllUgyiratTerkepDataSets(execParam, Constants.UgyiratTerkepNodeTypes.Ugyirat, UgyUgyiratTreeNode.Value, false);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            return;
        }

        DataTable table_ugyirat = result.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratok];
        DataTable table_ugyiratDarabok = result.Ds.Tables[Constants.TableNames.EREC_UgyUgyiratdarabok];
        DataTable table_iratok = result.Ds.Tables[Constants.TableNames.EREC_IraIratok];
        DataTable table_pld = result.Ds.Tables[Constants.TableNames.EREC_PldIratPeldanyok];
        DataTable table_kuld = result.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek];
        DataTable table_regi = result.Ds.Tables[Constants.TableNames.MIG_Foszam];

        DataTable table_kuld_ugyiratkapcsolt = result.Ds.Tables[Constants.TableNames.EREC_KuldKuldemenyek + "UgyiratKapcsolt"];

        //EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
        //EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
        //ExecParam erec_UgyUgyiratdarabokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //Result erec_UgyUgyiratdarabokResult = new Result();

        //erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        //erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = UgyUgyiratTreeNode.Value;
        //erec_UgyUgyiratdarabokResult = erec_UgyUgyiratdarabokService.GetAllWithExtension(erec_UgyUgyiratdarabokExecParam, erec_UgyUgyiratdarabokSearch);
        //if (!String.IsNullOrEmpty(erec_UgyUgyiratdarabokResult.ErrorCode))
        //{
        //    return;
        //}

        // esetleges elõiratok hozzáadása:
        AddUgyiratNode(table_ugyirat, table_regi, table_ugyiratDarabok, UgyUgyiratTreeNode.Value, true, UgyUgyiratTreeNode);

        AddUgyiratDarabNodes(UgyUgyiratTreeNode, table_ugyiratDarabok, true);

        // Ügyirathoz kapcsolt küldemények
        AddKapcsoltKuldemenyNodes(UgyUgyiratTreeNode, table_kuld_ugyiratkapcsolt, true);
    }

    private void AddUgyiratDarabNodes(TreeNode UgyUgyiratTreeNode, DataTable dataTable_UgyiratDarabok, bool autoPopulateDefaults)
    {
        if (UgyUgyiratTreeNode == null) return;

        // leszûrjük, hogy csak az ügyirathoz tartozó ügyiratdarabokat nézzük át
        DataView dv_ugyiratdarabok = new DataView(dataTable_UgyiratDarabok);
        dv_ugyiratdarabok.RowFilter = "UgyUgyirat_Id='" + UgyUgyiratTreeNode.Value + "'";

        // Elõször a default ügyiratdarabo(ka)t adjuk hozzá:
        // ezeket kigyûjtjük:

        List<DataRow> defaultUgyiratDarabList = new List<DataRow>();
        List<DataRow> restUgyiratDarabList = new List<DataRow>();

        //foreach (DataRow row in dataTable_UgyiratDarabok.Rows)
        foreach (DataRowView rowview in dv_ugyiratdarabok)
        {
            DataRow row = rowview.Row;
            string EljarasiSzakasz = row["EljarasiSzakasz"].ToString();
            //string row_UgyUgyirat_Id = row["UgyUgyirat_Id"].ToString();

            // a megadott ügyirathoz tartozik-e?
            //if (UgyUgyiratTreeNode != null && UgyUgyiratTreeNode.Value == row_UgyUgyirat_Id)
            //{
            bool defaultUgyiratDarab = (string.IsNullOrEmpty(EljarasiSzakasz) || EljarasiSzakasz == KodTarak.ELJARASI_SZAKASZ.Osztatlan) ? true : false;

            if (defaultUgyiratDarab == true)
            {
                defaultUgyiratDarabList.Add(row);
            }
            else
            {
                restUgyiratDarabList.Add(row);
            }
            //}
        }

        // csak hogy az elõiratok elé tudjuk beszúrni az ügyiratdarabokat:
        int eloiratCount = UgyUgyiratTreeNode.ChildNodes.Count;

        // elõször a defaultot vesszük fel:
        foreach (DataRow row in defaultUgyiratDarabList)
        {
            AddUgyiratDarabNode(row, UgyUgyiratTreeNode, true, autoPopulateDefaults, eloiratCount);
        }

        // maradék:
        foreach (DataRow row in restUgyiratDarabList)
        {
            AddUgyiratDarabNode(row, UgyUgyiratTreeNode, false, autoPopulateDefaults, eloiratCount);
        }

    }

    private void AddUgyiratDarabNode(DataRow dataRow_ugyiratDarab, TreeNode UgyUgyiratTreeNode, bool defaultUgyiratDarab, bool autoPopulateDefaults, int eloiratCount)
    {
        string UgyUgyiratokId = UgyUgyiratTreeNode.Value;

        String UgyUgyiratdarabokId = dataRow_ugyiratDarab["Id"].ToString();
        string Azonosito = dataRow_ugyiratDarab["Azonosito"].ToString();

        String UgyUgyiratdarabokText = GetTreeNodeText_UgyiratDarab(Azonosito, UgyUgyiratTreeNode, UgyUgyiratdarabokId, defaultUgyiratDarab);


        TreeNode ugyiratDarabokTreeNode = new TreeNode(UgyUgyiratdarabokText, UgyUgyiratdarabokId);
        ugyiratDarabokTreeNode.PopulateOnDemand = true;
        ugyiratDarabokTreeNode.Expanded = false;

        // az ügyiratdarabot az elõiratok elé, de a többi ügyiratdarab mögé szúrjuk be:
        int ugyiratdarabHelye = UgyUgyiratTreeNode.ChildNodes.Count - eloiratCount;
        if (ugyiratdarabHelye < 0) { ugyiratdarabHelye = 0; }
        UgyUgyiratTreeNode.ChildNodes.AddAt(ugyiratdarabHelye, ugyiratDarabokTreeNode);

        if (defaultUgyiratDarab == true)
        {
            // beállítjuk Checked-re (ezzel jelezzük, hogy õ default node)
            ugyiratDarabokTreeNode.Checked = true;

            if (autoPopulateDefaults)
            {
                // ügyiratdarab node-ot rögtön feltöltjük az alatta lévõ iratokkal:
                AddIratNode(UgyUgyiratokId, ugyiratDarabokTreeNode);
                ugyiratDarabokTreeNode.Expand();
            }
        }


        // DropDown listához hozzáadjuk, ha még nincs (CSAK A NEM DEFAULTOKAT, amik a legfelsõ ügyirathoz tartoznak)
        if (defaultUgyiratDarab == false && UgyUgyiratTreeNode.Depth == 0)
        {
            ListItem item = UgyiratDarabok_DropDownList.Items.FindByValue(UgyUgyiratdarabokId);
            if (item == null)
            {
                UgyiratDarabok_DropDownList.Items.Add(new ListItem(Azonosito, UgyUgyiratdarabokId));
            }
        }


        // load to Dictionary 
        if (UgyiratDarabokDictionary.ContainsKey(UgyUgyiratdarabokId) == false)
        {
            UgyiratDarabokDictionary.Add(UgyUgyiratdarabokId,
                new Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz(
                    UgyUgyiratdarabokId, dataRow_ugyiratDarab["Allapot"].ToString(), UgyUgyiratokId));
        }

        ViewState["UgyiratDarabokDictionary"] = UgyiratDarabokDictionary;
    }

    private string GetTreeNodeText_UgyiratDarab(string Azonosito, TreeNode parentNode, string UgyUgyiratdarabokId, bool defaultUgyiratDarab)
    {
        String UgyUgyiratdarabokText = "";

        if (defaultUgyiratDarab)
        {
            UgyUgyiratdarabokText += "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTableDefaultUgyiratDarab'><tr><td rowspan='2' class='TreeViewRowDefaultUgyiratDarab'>";
            // + "<div style='width: 30px; height: 10px; background-color: White; color: White; position: relative; left: -30px; top: 8px;' onclick='return false;'></div>";


            //UgyUgyiratdarabokText += SetUgyUgyiratdarabokUrl(""
            //, UgyUgyiratokId, UgyUgyiratdarabokId);
        }
        else
        {
            UgyUgyiratdarabokText += "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>"
                + SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_ugyiratdarab.gif' alt='Ügyiratdarab' />", UgyUgyiratdarabokId, parentNode);
        }

        UgyUgyiratdarabokText += "&nbsp;</td>";
        UgyUgyiratdarabokText += String.Format("<td style='width: {0}px;'>", 200/*GetWidth(parentNode)*/) + SetTreeNodeUrl("<b>"
            + Azonosito + "</b>", UgyUgyiratdarabokId, parentNode)
            + "<td style='width: 100px;'></td><td style='width: 150px;'></td><td style='width: 425px;'></td></tr></table>";

        return UgyUgyiratdarabokText;
    }

    private void AddIratNode(string ugyiratId, TreeNode ugyiratDarabTreeNode)
    {
        #region Init
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();
        ExecParam erec_IraIratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIratokResult = new Result();
        #endregion

        #region Search beállítás
        erec_IraIratokSearch.UgyUgyIratDarab_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_IraIratokSearch.UgyUgyIratDarab_Id.Value = ugyiratDarabTreeNode.Value;
        erec_IraIratokSearch.OrderBy = "Alszam DESC";
        erec_IraIratokSearch.IsUgyiratTerkepQuery = true;
        erec_IraIratokResult = erec_IraIratokService.GetAllWithExtension(erec_IraIratokExecParam, erec_IraIratokSearch);
        if (!String.IsNullOrEmpty(erec_IraIratokResult.ErrorCode))
        {
            return;
        }
        #endregion

        AddIratNode(ugyiratDarabTreeNode.Parent, erec_IraIratokResult.Ds.Tables[0]);
    }

    /// <summary>
    /// Irat szint feltöltése
    /// (több ügyiratdarabhoz tartozó iratok is lehetnek a DataTable-ben)
    /// vagy 
    /// </summary>
    private void AddIratNode(TreeNode ugyiratTreeNode, DataTable table_iratok)
    {
        //string UgyUgyiratokId = UgyUgyiratDarabTreeNode.ValuePath.Split('/')[0];
        //string UgyUgyiratdarabokId = UgyUgyiratDarabTreeNode.Value;

        if (table_iratok == null)
        {
            Logger.Error("Ügyirattérkép: üres az Iratok ResultSet");
            return;
        }

        if (ugyiratTreeNode == null) return;

        // leszûrjük, hogy csak az ügyirathoz tartozó iratokat nézzük át
        DataView dv_iratok = new DataView(table_iratok);
        dv_iratok.RowFilter = "Ugyirat_Id='" + ugyiratTreeNode.Value + "'";

        //for (int IraIratokIndex = 0; IraIratokIndex < table_iratok.Rows.Count; IraIratokIndex++)
        for (int IraIratokIndex = 0; IraIratokIndex < dv_iratok.Count; IraIratokIndex++)
        {
            //DataRow row = table_iratok.Rows[IraIratokIndex];
            DataRow row = dv_iratok[IraIratokIndex].Row;
            string IraIratokId = row["Id"].ToString();
            string UgyUgyIratDarab_Id = row["UgyUgyIratDarab_Id"].ToString();
            string Ugyirat_Id = row["Ugyirat_Id"].ToString();

            // Ügyiratdarab node megkeresése:
            TreeNode ugyiratDarabTreeNode = TreeView1.FindNode(ugyiratTreeNode.ValuePath + "/" + UgyUgyIratDarab_Id);

            string IraIratokText = GetTreeNodeText_Irat(row, ugyiratDarabTreeNode, IraIratokId);

            // load to Dictionary 
            // ha még nincs a dictionary-ben:
            if (IratokDictionary.ContainsKey(IraIratokId) == false)
            {
                IratokDictionary.Add(IraIratokId, new IratokStatuszExtended(
                    IraIratokId, row["Allapot"].ToString(), UgyUgyIratDarab_Id, Ugyirat_Id
                    , row["FelhasznaloCsoport_Id_Iktato"].ToString()
                    , row["ElsoIratPeldanyOrzo_Id"].ToString()
                    , row["ElsoIratPeldanyAllapot"].ToString()
                    , row["Alszam"].ToString()
                    , row["PostazasIranya"].ToString()));
            }

            TreeNode IratokTreeNode = new TreeNode(IraIratokText, IraIratokId);
            IratokTreeNode.PopulateOnDemand = true;
            IratokTreeNode.Expanded = false;
            //IratokTreeNode.ShowCheckBox = true;


            if (ugyiratDarabTreeNode != null)
            {
                ugyiratDarabTreeNode.ChildNodes.Add(IratokTreeNode);
            }
        }
        ViewState["IratokDictionary"] = IratokDictionary;
    }

    private string GetTreeNodeText_Irat(DataRow row, TreeNode parentNode, string IraIratokId)
    {
        string IktatoSzam_Merge = row["IktatoSzam_Merge"].ToString();
        string Allapot_nev = row["Allapot_nev"].ToString();
        string Targy1 = row["Targy1"].ToString();
        string Ugyintezo = row["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();
        string Allapot = row["Allapot"].ToString();
        string IktatasDatuma = row["IktatasDatuma"].ToString();
        //string UgyintezesAlapja = row["UgyintezesAlapja"].ToString();
        // ha átiktatással keletkezett:
        string RegiIktatoSzam = row["RegiIktatoSzam"].ToString();
        // ha átiktatott:
        string UjIktatoSzam = row["UjIktatoSzam"].ToString();
        string Jelleg = row["Jelleg"].ToString();
        string PostazasIranya = row["PostazasIranya"].ToString();
        string PostazasIranyaNev = row["PostazasIranyaNev"].ToString();

        //LZS - BUG_9377
        string BeerkezesIdeje = row["BeerkezesIdeje"].ToString();
        string PostazasDatuma = row["PostazasDatuma"].ToString();

        int postazasIkonWidth = 16;

        StringBuilder iratokText_sb = new StringBuilder();

        iratokText_sb.Append(String.Format("<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr class='UgyiratTerkepTreeViewRow'><td rowspan='2' class='TreeViewRowTD' style='width:{0}px;'>", 22 + postazasIkonWidth));


        //postázás iránya
        bool bejovo = (PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo);
        string iratIranyaHTML = String.Format("<img src='images/hu/egyeb/{0}' alt='{1}' style=\"padding: 2px;\"/>", bejovo ? "treeview_bejovo_irat.gif" : "treeview_kimeno_irat.gif", PostazasIranyaNev);
        iratokText_sb.Append(SetTreeNodeUrl(iratIranyaHTML, IraIratokId, parentNode));

        // Más ikon kell papír alapú és elektronikus iratnál:
        string pic = "";
        string title = "";

        bool hkp = row["KuldesMod"] != null && row["KuldesMod"].ToString().ToLower().Contains("hivatali kapu"); // KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu
        if (hkp)
        {
            pic = "treeview_irat_hkp.gif";
            title = "Irat (elektronikus, HKP)";
        }
        else
        {
            if (Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
            {
                pic = "treeview_irat_elektronikus.gif";
                title = "Irat (elektronikus, e-mail)";
            }
            else if (Jelleg == KodTarak.IRAT_JELLEG.Word_dokumentum)
            {
                pic = "treeview_irat_word.gif";
                title = "Irat (elektronikus, word)";
            }
            else
            {
                pic = "treeview_irat.gif";
                title = "Irat (papír alapú)";
            }
        }
        iratokText_sb.Append(SetTreeNodeUrl(String.Format("<img src='images/hu/egyeb/{0}' alt='{1}' />", pic, title), IraIratokId, parentNode));

        //LZS - BUG_8793
        int fixWidth = 130;

        iratokText_sb.AppendFormat("&nbsp;</td><td style='width: {0}px;'>", fixWidth/*Math.Max(minWidth, GetWidth(parentNode) - postazasIkonWidth)*/);
        iratokText_sb.Append(SetTreeNodeUrl(IktatoSzam_Merge, IraIratokId, parentNode));
        iratokText_sb.Append("</td><td style='width: 100px;'>");
        // Átiktatott irat állapotát kiemeljük:
        string allapotHtml = Allapot_nev;
        if (Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott)
        {
            allapotHtml = "<span style='color: Red;'><b>" + Allapot_nev
               + "(" + UjIktatoSzam.Replace(" ", "&nbsp;") + ")" + "</b></span>";
        }
        if (Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott)
        {
            allapotHtml = "<span style='color: Red;font-weight:bold;'>" + Allapot_nev + "</span>";
        }
        iratokText_sb.Append(SetTreeNodeUrl(allapotHtml, IraIratokId, parentNode));

        //iratokText_sb.Append("</td><td style='word-wrap:break-word; width: 274px; padding-right: 5px;'>");
        // BUG_12491
        //iratokText_sb.Append(String.Format("</td><td title=\"{0}\" style='word-wrap:break-word; width: 105px; padding-right: 5px;'>", Targy1));
        iratokText_sb.Append(String.Format("</td><td title=\"{0}\" style='word-wrap:break-word; width: 274px; padding-right: 5px;'>", Targy1));

        //LZS - BUG_6840
        //iratokText_sb.Append(SetTreeNodeUrl(SpliceText(Targy1, 50), IraIratokId, parentNode));
        //BUG_9377
        string targy1Html = Targy1;
        //LZS - BUG_9377
        bool TertiIsVisible = TertivevenyekIsVisible(IraIratokId);

        #region TertiIsVisible condition
        //if (TertiIsVisible)
        //{
        //    int length = 10;

        //    if (targy1Html.Length >= length)
        //    {
        //        targy1Html = targy1Html.Substring(0, length - 3) + "...";
        //    }
        //    else
        //    {
        //        targy1Html = targy1Html.PadRight(length);
        //    }
        //}
        //else
        //{
        #endregion

        // BUG_12491
        //int length = 15;
        //int length = 40;
        //if (targy1Html.Length >= length)
        //{
        //    targy1Html = targy1Html.Substring(0, length - 3) + "...";
        //}
        //else
        //{
        //    targy1Html = targy1Html.PadRight(length);
        //}
        //TertiIsVisible condition }

        iratokText_sb.Append(SetTreeNodeUrl(targy1Html, IraIratokId, parentNode));
        // BUG_12491
        //iratokText_sb.Append(String.Format("</td><td title=\"{0}\" style='min-width: 200px; max-width: 200px'>", Ugyintezo));
        iratokText_sb.Append(String.Format("</td><td title=\"{0}\" style='word-wrap:break-word; width: 200px; padding-right: 2px;'>", Ugyintezo));

        // BLG_1014
        //iratokText_sb.Append(SetTreeNodeUrl("  <b>Ügyintézõ: </b>" + Ugyintezo, IraIratokId, parentNode));
        string ugyintezoHtml = Ugyintezo;

        #region TertiIsVisible condition
        //if (TertiIsVisible)
        //{
        //    int length = 10;

        //    if (ugyintezoHtml.Length >= length)
        //    {
        //        ugyintezoHtml = ugyintezoHtml.Substring(0, length - 3) + "...";
        //    }
        //    else
        //    {
        //        ugyintezoHtml = ugyintezoHtml.PadRight(length);
        //    }
        //}
        //else
        //{
        #endregion

        // BUG_12491
        //length = 13;

        //if (ugyintezoHtml.Length >= length)
        //{
        //    ugyintezoHtml = ugyintezoHtml.Substring(0, length - 3) + "...";
        //}
        //TertiIsVisible condition }

        iratokText_sb.Append(SetTreeNodeUrl("  <b>" + Contentum.eUtility.ForditasExpressionBuilder.GetForditas("StrIratUgyintezo", "Ügyintézõ:", this.AppRelativeVirtualPath) + "<br /></b>" + ugyintezoHtml, IraIratokId, parentNode)); //  "/eRecord/eRecordComponent/UgyiratTerkepTab.ascx"
        iratokText_sb.Append("</td><td style='width: 140px;padding-left:10px'>");
        // ha átiktatással keletkezett:
        string iktatasDatumaHtml = IktatasDatuma;
        if (!string.IsNullOrEmpty(RegiIktatoSzam))
        {
            iktatasDatumaHtml += "(" + RegiIktatoSzam + " átiktatva)";
        }

        iratokText_sb.Append(SetTreeNodeUrl(" <b>Iktatás időpontja: </b>" + iktatasDatumaHtml, IraIratokId, parentNode));
        iratokText_sb.Append("</td><td style=\"width:25px;padding-left:10px\">");

        //LZS - BUG_9377
        if (!string.IsNullOrEmpty(PostazasDatuma))
        {
            #region TertiIsVisible condition
            //if (TertiIsVisible)
            //{
            //    string iktatas_PostazasHtml = SpliceText(" <b>Iktatás időpontja: </b>" + iktatasDatumaHtml + " <b>Postázás időpontja: </b>" + PostazasDatuma, 47);
            //    iratokText_sb.Append(SetTreeNodeUrl(iktatas_PostazasHtml, IraIratokId, parentNode));

            //}
            //else
            //{
            #endregion

            iratokText_sb.Append(SetTreeNodeUrl("<b>Postázás időpontja: </b>" + PostazasDatuma + "<br />" + TertivevenyekInfo(IraIratokId), IraIratokId, parentNode));
            //TertiIsVisible condition }
        }
        else if (!string.IsNullOrEmpty(BeerkezesIdeje))
        {
            iratokText_sb.Append(SetTreeNodeUrl("  <b>Beérkezés időpontja: </b>" + BeerkezesIdeje, IraIratokId, parentNode));
        }

        iratokText_sb.Append("</td><td style=\"width:25px;\">");
        if (!string.IsNullOrEmpty(RegiIktatoSzam) || !string.IsNullOrEmpty(UjIktatoSzam))
        {
            // Iktatási lánc ikon megjelenítése:      
            string atiktatasPopupClientClick = JavaScripts.SetOnCLientClick_NoPostBack("AtiktatasiLanc.aspx", QueryStringVars.IratId + "=" + IraIratokId
                , Defaults.PopupWidth, Defaults.PopupHeight);
            iratokText_sb.Append("<img src=\"images/hu/egyeb/info_icon.png\" title=\"Átiktatási lánc\" onclick=\"" + atiktatasPopupClientClick + "\" />");
        }

        //BUG_9377
        string ugyfajtaja = this.GetUgyFajtajaLink(row, parentNode);
        string csatolasImg = this.getCsatolasImage(row);
        string csatolmanyImg = this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.Irat, parentNode);
        string feladatImg = this.getFeladatImage(row, Constants.TableNames.EREC_IraIratok);

        if (!string.IsNullOrEmpty(ugyfajtaja) && ugyfajtaja != "&nbsp;")
            iratokText_sb.Append("</td><td style=\"width:25px;font-weight:bold;text-align:center;\">" + this.GetUgyFajtajaLink(row, parentNode));

        if (!string.IsNullOrEmpty(csatolasImg) && csatolasImg != "&nbsp")
            iratokText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolasImage(row));

        if (!string.IsNullOrEmpty(csatolmanyImg) && csatolmanyImg != "&nbsp")
            iratokText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.Irat, parentNode));

        if (!string.IsNullOrEmpty(feladatImg) && csatolmanyImg != "&nbsp")
            iratokText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(row, Constants.TableNames.EREC_IraIratok));

        iratokText_sb.Append("</td>");

        //BUG_9377 - Átkerült a Postázás dátuma alá
        //iratokText_sb.Append(TertivevenyekInfo(IraIratokId));
        iratokText_sb.Append("</tr></table>");

        return iratokText_sb.ToString();
    }



    #region BUG_4783
    private string[] GetPostazottKuldemenyek(string iratId)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_Kuldemeny_IratPeldanyaiService service = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch(true);

        search.Extended_EREC_PldIratPeldanyokSearch.IraIrat_Id.Value = iratId;
        search.Extended_EREC_PldIratPeldanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;
        search.Extended_EREC_PldIratPeldanyokSearch.IraIrat_Id.GroupOperator = Query.Operators.and;

        search.Extended_EREC_KuldKuldemenyekSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Kimeno;

        string[] allapotok = new string[] { KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette, KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at, KodTarak.IRATPELDANY_ALLAPOT.Postazott };
        search.Extended_EREC_PldIratPeldanyokSearch.Allapot.Value = Search.GetSqlInnerString(allapotok);
        search.Extended_EREC_PldIratPeldanyokSearch.Allapot.Operator = Query.Operators.inner;

        Result result = service.GetAllWithExtension(execParam, search);
        if (result.IsError)
        {
            //ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel1, result);
            //ErrorUpdatePanel.Update();
            return null;
        }

        List<string> res = new List<string>();
        if (result.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
            {
                res.Add(row["KuldKuldemeny_Id"].ToString());
            }
        }

        return res.ToArray();
    }

    //LZS - BUG_9377
    private bool TertivevenyekIsVisible(string iratId)
    {
        try
        {
            string[] kuldemenyek = GetPostazottKuldemenyek(iratId);

            if (kuldemenyek != null && kuldemenyek.Length > 0)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                search.Kuldemeny_Id.Value = Search.GetSqlInnerString(kuldemenyek);
                search.Kuldemeny_Id.Operator = Query.Operators.inner;

                Result result = service.GetAllWithExtension(execParam, search);
                if (result.IsError)
                {
                    //ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel1, result);
                    //ErrorUpdatePanel.Update();
                    return false;
                }

                int kikuldott = result.Ds.Tables[0].Rows.Count;
                if (kikuldott > 0)
                {
                    return true;
                }


            }
        }
        catch (Exception x)
        {
            Logger.Error("TertivevenyekInfo", x);
            return false;
        }

        return false;
    }

    private string TertivevenyekInfo(string iratId)
    {
        try
        {
            string[] kuldemenyek = GetPostazottKuldemenyek(iratId);

            if (kuldemenyek != null && kuldemenyek.Length > 0)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                search.Kuldemeny_Id.Value = Search.GetSqlInnerString(kuldemenyek);
                search.Kuldemeny_Id.Operator = Query.Operators.inner;

                Result result = service.GetAllWithExtension(execParam, search);
                if (result.IsError)
                {
                    //ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel1, result);
                    //ErrorUpdatePanel.Update();
                    return "";
                }

                int kikuldott = result.Ds.Tables[0].Rows.Count;
                if (kikuldott > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    int visszaerkezett = 0;
                    int atnemvett = 0;
                    DateTime? utoljaraAtvett = null;

                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string tertivisszaKod = row["TertivisszaKod"] as string;
                        if (!String.IsNullOrEmpty(tertivisszaKod))
                        {
                            visszaerkezett++;
                            if (tertivisszaKod != KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek
                                && tertivisszaKod != KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek
                                // Bug_5218 miatt:
                                && tertivisszaKod != KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek
                                && tertivisszaKod != KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_meghatalmazottnak
                                && tertivisszaKod != KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_helyettes_atvevonek)
                            {
                                atnemvett++;
                            }
                            else
                            {
                                DateTime? atvetelDat = row["AtvetelDat"] as DateTime?;
                                if (atvetelDat != null)
                                {
                                    if (utoljaraAtvett == null || atvetelDat > utoljaraAtvett)
                                    {
                                        utoljaraAtvett = atvetelDat;
                                    }
                                }
                            }
                        }
                    }

                    //const string td = "<td style='padding-left:10px'>";
                    //sb.AppendLine(String.Format(/*td +*/ "<b>Kiküldött tv.:</b> {0}</td>", kikuldott));
                    sb.AppendLine(String.Format(/*td +*/ "<b>Kiküldött tv.:</b> {0}", kikuldott));
                    if (atnemvett > 0)
                    {
                        //sb.AppendLine(String.Format(td + "<b>Át nem vett:</b> {0}</td>", atnemvett));
                        sb.AppendLine(String.Format(/*td +*/ "<b>Át nem vett:</b> {0}", atnemvett));
                    }
                    //sb.AppendLine(String.Format(td + "<b>Visszaérk.:</b> {0}</td>", visszaerkezett));
                    sb.AppendLine(String.Format(/*td +*/ "<b>Visszaérk.:</b> {0}", visszaerkezett));
                    if (utoljaraAtvett.HasValue)
                    {
                        //sb.AppendLine(String.Format(td + "<b>Utoljára átvett:</b> {0}</td>", utoljaraAtvett.Value.ToString("yyyy.MM.dd.")));
                        sb.AppendLine(String.Format(/*td + */"<b>Utoljára átvett:</b> {0}", utoljaraAtvett.Value.ToString("yyyy.MM.dd.")));
                    }

                    return sb.ToString();
                }
            }
        }
        catch (Exception x)
        {
            Logger.Error("TertivevenyekInfo", x);
        }
        return "";
    }
    #endregion

    //LZS - BUG_6840
    public static string SpliceText(string text, int lineLength)
    {
        return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + @"<br \>");
    }

    private void AddIratPeldanyNodes(TreeNode IratTreeNode)
    {
        #region Init
        EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
        ExecParam erec_KuldKuldemenyekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_KuldKuldemenyekResult = new Result();

        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
        ExecParam erec_PldIratPeldanyokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_PldIratPeldanyokResult = new Result();




        //string UgyUgyiratokId = IratTreeNode.ValuePath.Split('/')[0];
        //string UgyUgyiratdarabokId = IratTreeNode.ValuePath.Split('/')[1];
        //string IraIratokId = IratTreeNode.Value;
        #endregion

        #region Irat lekérése
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam erec_IraIratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIratokResult = new Result();
        erec_IraIratokExecParam.Record_Id = IratTreeNode.Value;
        erec_IraIratokResult = erec_IraIratokService.Get(erec_IraIratokExecParam);
        if (erec_IraIratokResult.IsError)
        {
            return;
        }
        #endregion



        // Iratpéldányok lekérése
        erec_PldIratPeldanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        erec_PldIratPeldanyokSearch.IraIrat_Id.Value = (erec_IraIratokResult.Record as EREC_IraIratok).Id;
        erec_PldIratPeldanyokSearch.OrderBy = "Sorszam";
        erec_PldIratPeldanyokResult = erec_PldIratPeldanyokService.GetAllWithExtension(erec_PldIratPeldanyokExecParam, erec_PldIratPeldanyokSearch);
        if (erec_PldIratPeldanyokResult.IsError)
        {
            return;
        }


        AddIratPeldanyNodes(IratTreeNode, erec_PldIratPeldanyokResult.Ds.Tables[0], true);

        //#region IratKuldemeny kapcsolaton keresztüli küldemények lekérése

        //EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        //Result result_kuld_kapcsolt = service_ugyiratok.GetIratKapcsoltKuldemenyei(UI.SetExecParamDefault(Page), IratTreeNode.Value, null);

        //if (result_kuld_kapcsolt.IsError)
        //{
        //    return;
        //}

        //DataTable table_kuld_kapcsolt = result_kuld_kapcsolt.Ds.Tables[0];

        //AddKapcsoltKuldemenyNodes(IratTreeNode, erec_IraIrat, table_kuld_kapcsolt);

        //#endregion IratKuldemeny kapcsolaton keresztüli küldemények lekérése
    }

    private void AddIratPeldanyNodes(TreeNode IratTreeNode, DataTable dataTable_IratPeldanyok, bool autoPopulateDefaults)
    {
        if (IratTreeNode == null) return;

        DataView dv_iratPld = new DataView(dataTable_IratPeldanyok);
        dv_iratPld.RowFilter = "IraIrat_Id='" + IratTreeNode.Value + "'";

        for (int PldIratPeldanyIndex = 0; PldIratPeldanyIndex < dv_iratPld.Count; PldIratPeldanyIndex++)
        {
            #region Iratpéldányhoz tartozó küldemények lekérése
            // nekrisz CR 2961
            EREC_Kuldemeny_IratPeldanyaiService erec_Kuldemeny_IratPeldanyaiService = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
            EREC_Kuldemeny_IratPeldanyaiSearch erec_Kuldemeny_IratPeldanyaiSearch = new EREC_Kuldemeny_IratPeldanyaiSearch();
            ExecParam erec_Kuldemeny_IratPeldanyaiExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result erec_Kuldemeny_IratPeldanyaiResult = new Result();

            erec_Kuldemeny_IratPeldanyaiSearch.Peldany_Id.Value = dv_iratPld[PldIratPeldanyIndex].Row["Id"].ToString();
            //(erec_PldIratPeldanyokResult.Record as EREC_PldIratPeldanyok).Id; ;
            erec_Kuldemeny_IratPeldanyaiSearch.Peldany_Id.Operator = Query.Operators.equals;
            //ExecParam execParam_kuldGetAll = execParam.Clone();
            erec_Kuldemeny_IratPeldanyaiResult = erec_Kuldemeny_IratPeldanyaiService.GetAllWithExtension(erec_Kuldemeny_IratPeldanyaiExecParam, erec_Kuldemeny_IratPeldanyaiSearch);
            if (!String.IsNullOrEmpty(erec_Kuldemeny_IratPeldanyaiResult.ErrorCode))
            {
                // hiba:
                return;
            }
            DataTable table_kuldIratPld = erec_Kuldemeny_IratPeldanyaiResult.Ds.Tables[0].Copy();
            table_kuldIratPld.TableName = Contentum.eUtility.Constants.TableNames.EREC_Kuldemeny_IratPeldanyai;

            #endregion
            AddIratPeldanyNode(dv_iratPld[PldIratPeldanyIndex].Row, IratTreeNode, table_kuldIratPld, autoPopulateDefaults);

        }

    }

    private void AddIratPeldanyNode(DataRow dataRow_IratPeldany, TreeNode IratTreeNode, DataTable datatable_kuld_IratPld, bool autoPopulateDefaults)
    {
        string IraIratId = IratTreeNode.Value;
        String PldIratPeldanyId = dataRow_IratPeldany["Id"].ToString();

        string iratPeldanyText = GetTreeNodeText_IratPeldany(dataRow_IratPeldany, IratTreeNode, PldIratPeldanyId);


        TreeNode iratPldTreeNode = new TreeNode(iratPeldanyText, PldIratPeldanyId);
        iratPldTreeNode.PopulateOnDemand = true;
        iratPldTreeNode.Expanded = false;
        iratPldTreeNode.Checked = false;

        IratTreeNode.ChildNodes.Add(iratPldTreeNode);

        //if (autoPopulateDefaults)
        //{
        // iratpéldany alatti küldemények feltöltése:

        //AddKapcsoltKuldemenyNodes(iratPldTreeNode, datatable_kuld_IratPld, false);
        AddKuldemenyForIratPeldanyNodes(iratPldTreeNode, datatable_kuld_IratPld);
        iratPldTreeNode.Expand();
        //}

        // load to Dictionary 
        if (IratPeldanyokDictionary.ContainsKey(PldIratPeldanyId) == false)
        {
            IratPeldanyokDictionary.Add(PldIratPeldanyId, new Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz(
                              PldIratPeldanyId
                            , dataRow_IratPeldany["Allapot"].ToString()
                            , IraIratId
                            , dataRow_IratPeldany["FelhasznaloCsoport_Id_Orzo"].ToString()
                            , dataRow_IratPeldany["Csoport_Id_Felelos"].ToString()
                            , dataRow_IratPeldany["Sorszam"].ToString()
                            , dataRow_IratPeldany["KuldesMod"].ToString())
                        );
        }

        ViewState["IratPeldanyokDictionary"] = IratPeldanyokDictionary;
    }

    private void AddKuldemenyForIratNode(TreeNode IratTreeNode, EREC_IraIratok erec_IraIrat, DataTable table_kuldemeny)
    {
        if (erec_IraIrat == null)
        {
            return;
        }

        string IraIratokId = IratTreeNode.Value;

        #region Ha bejovo keletkezesu az irat, van kuldemenye és iratpéldánya
        if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo && table_kuldemeny != null)
        {
            // leszûrjük, hogy csak az irathoz tartozó küldemény(eke)t nézzük át
            DataView dv_kuld = new DataView(table_kuldemeny);
            dv_kuld.RowFilter = "Id='" + erec_IraIrat.KuldKuldemenyek_Id + "'";

            //for (int KuldKuldemenyekIndex = 0; KuldKuldemenyekIndex < table_kuldemeny.Rows.Count; KuldKuldemenyekIndex++)
            for (int KuldKuldemenyekIndex = 0; KuldKuldemenyekIndex < dv_kuld.Count; KuldKuldemenyekIndex++)
            {
                DataRow row = dv_kuld[KuldKuldemenyekIndex].Row;
                string KuldKuldemenyekId = row["Id"].ToString();

                string KuldKuldemenyekText = GetTreeNodeText_Kuldemeny(row, IratTreeNode, KuldKuldemenyekId);

                // load to Dictionary                 
                //if (KuldemenyekDictionary.Count==0) 
                // ha még nincs a dictionary-ben:
                if (KuldemenyekDictionary.ContainsKey(KuldKuldemenyekId) == false)
                {
                    KuldemenyekDictionary.Add(KuldKuldemenyekId, new Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz(
                        KuldKuldemenyekId
                        , row["Allapot"].ToString(), "", IraIratokId, ""
                        , row["FelhasznaloCsoport_Id_Orzo"].ToString()
                        , row["Zarolo_id"].ToString()
                        , row["KuldesMod"].ToString()
                        , row["Csoport_Id_Felelos"].ToString()
                        ));
                }

                // Küldemény treenode-nál: Checked=true (CheckBox nem látható)
                TreeNode tr = new TreeNode(KuldKuldemenyekText, KuldKuldemenyekId);
                tr.PopulateOnDemand = true;
                tr.Checked = true;
                //tr.Expanded = false;
                tr.Expanded = true;

                IratTreeNode.ChildNodes.Add(tr);
            }
            ViewState["KuldemenyekDictionary"] = KuldemenyekDictionary;
        }

        #endregion

    }



    private void AddKuldemenyNodes(TreeNode IratTreeNode)
    {
        #region Init
        EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
        ExecParam erec_KuldKuldemenyekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_KuldKuldemenyekResult = new Result();

        //string UgyUgyiratokId = IratTreeNode.ValuePath.Split('/')[0];
        //string UgyUgyiratdarabokId = IratTreeNode.ValuePath.Split('/')[1];
        //string IraIratokId = IratTreeNode.Value;
        #endregion

        #region Irat lekérése
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam erec_IraIratokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_IraIratokResult = new Result();
        erec_IraIratokExecParam.Record_Id = IratTreeNode.Value;
        erec_IraIratokResult = erec_IraIratokService.Get(erec_IraIratokExecParam);
        if (erec_IraIratokResult.IsError)
        {
            return;
        }
        #endregion

        DataTable table_kuldemeny = null;
        EREC_IraIratok erec_IraIrat = erec_IraIratokResult.Record as EREC_IraIratok;

        // Ha bejovo keletkezesu az irat, van kuldemenye és iratpéldánya
        if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
        {
            erec_KuldKuldemenyekSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            erec_KuldKuldemenyekSearch.Id.Value = (erec_IraIratokResult.Record as EREC_IraIratok).KuldKuldemenyek_Id;
            erec_KuldKuldemenyekResult = erec_KuldKuldemenyekService.GetAllWithExtension(erec_KuldKuldemenyekExecParam, erec_KuldKuldemenyekSearch);
            if (erec_KuldKuldemenyekResult.IsError)
            {
                return;
            }

            table_kuldemeny = erec_KuldKuldemenyekResult.Ds.Tables[0];

            // csatolmány jogosultság vizsgáalt
            #region Küldemények csatolmányának jogosultságvizsgálata
            Result erec_IraIratok_CheckRightResult = new Result();
            //LZS - BLG_12369
            erec_IraIratok_CheckRightResult = erec_IraIratokService.GetWithRightCheckWithoutEventLogging(erec_IraIratokExecParam, 'O');
            if (erec_IraIratok_CheckRightResult.IsError)
            {
                table_kuldemeny.Columns.Add("CsatolmanyJogosult", typeof(bool));

                foreach (DataRow row in table_kuldemeny.Rows)
                {
                    row["CsatolmanyJogosult"] = false;
                }
            }
            #endregion Küldemények csatolmányának jogosultságvizsgálata

            AddKuldemenyForIratNode(IratTreeNode, erec_IraIrat, table_kuldemeny);
        }


        //// Iratpéldányok lekérése
        //erec_PldIratPeldanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        //erec_PldIratPeldanyokSearch.IraIrat_Id.Value = (erec_IraIratokResult.Record as EREC_IraIratok).Id;
        //erec_PldIratPeldanyokResult = erec_PldIratPeldanyokService.GetAllWithExtension(erec_PldIratPeldanyokExecParam, erec_PldIratPeldanyokSearch);
        //if (erec_PldIratPeldanyokResult.IsError)
        //{
        //    return;
        //}

        ////AddKuldemenyOrIratPeldanyNode(IratTreeNode, UgyUgyiratokId, UgyUgyiratdarabokId, IraIratokId, erec_IraIrat, erec_PldIratPeldanyokResult.Ds.Tables[0], table_kuldemeny);
        //AddKuldemenyOrIratPeldanyNode(IratTreeNode, erec_IraIrat, erec_PldIratPeldanyokResult.Ds.Tables[0], table_kuldemeny);

        #region IratKuldemeny kapcsolaton keresztüli küldemények lekérése

        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        Result result_kuld_kapcsolt = service_ugyiratok.GetIratKapcsoltKuldemenyei(UI.SetExecParamDefault(Page), IratTreeNode.Value, null);

        if (result_kuld_kapcsolt.IsError)
        {
            return;
        }

        DataTable table_kuld_kapcsolt = result_kuld_kapcsolt.Ds.Tables[0];

        AddKapcsoltKuldemenyNodes(IratTreeNode, erec_IraIrat, table_kuld_kapcsolt);

        #endregion IratKuldemeny kapcsolaton keresztüli küldemények lekérése
    }


    private void AddKapcsoltKuldemenyNodes(TreeNode IratTreeNode, EREC_IraIratok erec_IraIrat, DataTable table_kuldemeny)
    {
        if (erec_IraIrat == null)
        {
            return;
        }

        string IraIratokId = IratTreeNode.Value;

        if (table_kuldemeny != null && table_kuldemeny.Rows.Count > 0)
        {
            // leszûrjük, hogy csak az irathoz tartozó küldemény(eke)t nézzük át - IraIrat_Id mesterségesen tartalmazza az elõzményt!
            DataView dv_kuld = new DataView(table_kuldemeny);
            dv_kuld.RowFilter = "IraIratok_Id='" + erec_IraIrat.Id + "'";

            // a flag határozza meg, hogy csökkenõ vagy bnövekvõ sorrendben megyünk (és elõre vagy a végére szúrunk be)
            //if (bIratKapcsoltKuldemenyBeforeOtherChildren)
            //{
            for (int KuldKuldemenyekIndex = dv_kuld.Count - 1; KuldKuldemenyekIndex >= 0; KuldKuldemenyekIndex--)
            {
                DataRow row = dv_kuld[KuldKuldemenyekIndex].Row;
                TreeNode tr = GetKapcsoltKuldemenyNode(row, IratTreeNode);
                IratTreeNode.ChildNodes.AddAt(0, tr);
            }
            //}
            //else
            //{
            //    for (int KuldKuldemenyekIndex = 0; KuldKuldemenyekIndex < dv_kuld.Count; KuldKuldemenyekIndex++)
            //    {
            //        DataRow row = dv_kuld[KuldKuldemenyekIndex].Row;
            //        TreeNode tr = GetKapcsoltKuldemenyNode(row, IratTreeNode);
            //        IratTreeNode.ChildNodes.Add(tr);
            //    }
            //}

            ViewState["KuldemenyekDictionary"] = KuldemenyekDictionary;
        }
    }

    private void AddKapcsoltKuldemenyNodes(TreeNode UgyiratTreeNode, DataTable table_kuldemeny, bool processParents)
    {
        if (table_kuldemeny != null && table_kuldemeny.Rows.Count > 0)
        {
            TreeNode tn = UgyiratTreeNode;

            while (tn != null)
            {
                if (GetObjektumType(tn) == Constants.TableNames.EREC_UgyUgyiratok)
                {
                    // leszûrjük, hogy csak az ügyirathoz tartozó küldemény(eke)t nézzük át - UgyUgyiratok_Id mesterségesen tartalmazza az elõzményt!
                    DataView dv_kuld = new DataView(table_kuldemeny);
                    dv_kuld.RowFilter = String.Format("UgyUgyiratok_Id='{0}'", tn.Value);

                    // a flag határozza meg, hogy csökkenõ vagy bnövekvõ sorrendben megyünk (és elõre vagy a végére szúrunk be)
                    //if (bUgyiratKapcsoltKuldemenyBeforeOtherChildren)
                    //{
                    for (int KuldKuldemenyekIndex = dv_kuld.Count - 1; KuldKuldemenyekIndex >= 0; KuldKuldemenyekIndex--)
                    {
                        DataRow row = dv_kuld[KuldKuldemenyekIndex].Row;
                        TreeNode tr = GetKapcsoltKuldemenyNode(row, tn);
                        tn.ChildNodes.AddAt(0, tr);
                    }
                    //}
                    //else
                    //{
                    //    for (int KuldKuldemenyekIndex = 0; KuldKuldemenyekIndex < dv_kuld.Count; KuldKuldemenyekIndex++)
                    //    {
                    //        DataRow row = dv_kuld[KuldKuldemenyekIndex].Row;
                    //        TreeNode tr = GetKapcsoltKuldemenyNode(row, tn);
                    //        tn.ChildNodes.Add(tr);
                    //    }
                    //}
                }
                tn = processParents ? tn.Parent : null;
            }
            ViewState["KuldemenyekDictionary"] = KuldemenyekDictionary;
        }
    }

    private void AddKuldemenyForIratPeldanyNodes(TreeNode IratPldTreeNode)
    {
        #region Init
        //EREC_Kuldemeny_IratPeldanyaiService service_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        EREC_Kuldemeny_IratPeldanyaiService erec_Kuldemeny_IratPeldanyaiService = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
        EREC_Kuldemeny_IratPeldanyaiSearch erec_Kuldemeny_IratPeldanyaiSearch = new EREC_Kuldemeny_IratPeldanyaiSearch();
        ExecParam erec_Kuldemeny_IratPeldanyaiExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result erec_Kuldemeny_IratPeldanyaiResult = new Result();

        //EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        //EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
        //ExecParam erec_KuldKuldemenyekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //Result erec_KuldKuldemenyekResult = new Result();

        //string UgyUgyiratokId = IratTreeNode.ValuePath.Split('/')[0];
        //string UgyUgyiratdarabokId = IratTreeNode.ValuePath.Split('/')[1];
        //string IraIratokId = IratTreeNode.Value;
        #endregion

        #region Iratpéldányhoz tartozó küldemények lekérése
        // nekrisz CR 2961
        // EREC_Kuldemeny_IratPeldanyaiSearch search_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiSearch();
        //////átgondolni, nekrisz
        // erec_Kuldemeny_IratPeldanyaiSearch.Peldany_Id.Value = (erec_PldIratPeldanyokResult.Record as EREC_PldIratPeldanyok).Id;
        erec_Kuldemeny_IratPeldanyaiSearch.Peldany_Id.Value = IratPldTreeNode.Value;
        erec_Kuldemeny_IratPeldanyaiSearch.Peldany_Id.Operator = Query.Operators.equals;
        //ExecParam execParam_kuldGetAll = execParam.Clone();
        erec_Kuldemeny_IratPeldanyaiResult = erec_Kuldemeny_IratPeldanyaiService.GetAllWithExtension(erec_Kuldemeny_IratPeldanyaiExecParam, erec_Kuldemeny_IratPeldanyaiSearch);
        if (!String.IsNullOrEmpty(erec_Kuldemeny_IratPeldanyaiResult.ErrorCode))
        {
            // hiba:
            return;
        }
        DataTable table_kuldIratPld = erec_Kuldemeny_IratPeldanyaiResult.Ds.Tables[0].Copy();
        table_kuldIratPld.TableName = Contentum.eUtility.Constants.TableNames.EREC_Kuldemeny_IratPeldanyai;

        AddKuldemenyForIratPeldanyNodes(IratPldTreeNode, table_kuldIratPld);
        #endregion

    }




    private void AddKuldemenyForIratPeldanyNodes(TreeNode IratPldTreeNode, DataTable table_kuldemeny)
    {
        if (table_kuldemeny != null && table_kuldemeny.Rows.Count > 0)
        {
            TreeNode tn = IratPldTreeNode;

            //leszûrjük, hogy csak az iratpéldányhoz tartozó küldemény(eke)t nézzük át

            DataView dv_kuld_pld = new DataView(table_kuldemeny);
            dv_kuld_pld.RowFilter = String.Format("Peldany_Id='{0}'", tn.Value);

            for (int KuldKuldemenyekIndex = 0; KuldKuldemenyekIndex < dv_kuld_pld.Count; KuldKuldemenyekIndex++)
            {
                DataRow row = dv_kuld_pld[KuldKuldemenyekIndex].Row;
                TreeNode tr = GetKuldemenyForIratPeldanyNode(row, tn);
                tn.ChildNodes.Add(tr);
            }

            ViewState["KuldemenyekDictionary"] = KuldemenyekDictionary;
        }
    }
    //private TreeNode GetKapcsoltKuldemenyNode(DataRow kuldemenyRow, TreeNode parentTreeNode)
    //{
    //    string KuldKuldemenyekId = kuldemenyRow["Id"].ToString();

    //    string KuldKuldemenyekText = GetTreeNodeText_KapcsoltKuldemeny(kuldemenyRow, parentTreeNode, KuldKuldemenyekId);

    //    // load to Dictionary                 
    //    //if (KuldemenyekDictionary.Count==0) 
    //    // ha még nincs a dictionary-ben:
    //    if (KuldemenyekDictionary.ContainsKey(KuldKuldemenyekId) == false)
    //    {
    //        KuldemenyekDictionary.Add(KuldKuldemenyekId, new Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz(
    //            KuldKuldemenyekId
    //            , kuldemenyRow["Allapot"].ToString(), "", parentTreeNode.Value, ""
    //            , kuldemenyRow["FelhasznaloCsoport_Id_Orzo"].ToString()
    //            , kuldemenyRow["Zarolo_id"].ToString()
    //            , kuldemenyRow["KuldesMod"].ToString()
    //            , kuldemenyRow["Csoport_Id_Felelos"].ToString()
    //            ));
    //    }

    //    // Küldemény treenode-nál: Checked=true (CheckBox nem látható)
    //    TreeNode tr = new TreeNode(KuldKuldemenyekText, KuldKuldemenyekId);
    //    tr.PopulateOnDemand = true;
    //    tr.Checked = true;
    //    //tr.Expanded = false;
    //    tr.Expanded = true;

    //    return tr;
    //}

    private TreeNode GetKapcsoltKuldemenyNode(DataRow kuldemenyRow, TreeNode parentTreeNode)
    {
        string KuldKuldemenyekId = kuldemenyRow["Id"].ToString();

        string KuldKuldemenyekText = GetTreeNodeText_KapcsoltKuldemeny(kuldemenyRow, parentTreeNode, KuldKuldemenyekId);

        // load to Dictionary                 
        //if (KuldemenyekDictionary.Count==0) 
        // ha még nincs a dictionary-ben:
        if (KuldemenyekDictionary.ContainsKey(KuldKuldemenyekId) == false)
        {
            KuldemenyekDictionary.Add(KuldKuldemenyekId, new Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz(
                KuldKuldemenyekId
                , kuldemenyRow["Allapot"].ToString(), "", parentTreeNode.Value, ""
                , kuldemenyRow["FelhasznaloCsoport_Id_Orzo"].ToString()
                , kuldemenyRow["Zarolo_id"].ToString()
                , kuldemenyRow["KuldesMod"].ToString()
                , kuldemenyRow["Csoport_Id_Felelos"].ToString()
                ));
        }

        // Küldemény treenode-nál: Checked=true (CheckBox nem látható)
        TreeNode tr = new TreeNode(KuldKuldemenyekText, KuldKuldemenyekId);
        tr.PopulateOnDemand = true;
        tr.Checked = true;
        //tr.Expanded = false;
        tr.Expanded = true;

        return tr;
    }

    private TreeNode GetKuldemenyForIratPeldanyNode(DataRow kuldemenyRow, TreeNode parentTreeNode)
    {
        string KuldKuldemenyekId = kuldemenyRow["KuldKuldemeny_Id"].ToString();

        string KuldKuldemenyekText = GetTreeNodeText_Kuldemeny_IratPld(kuldemenyRow, parentTreeNode, KuldKuldemenyekId);

        // load to Dictionary                 
        //if (KuldemenyekDictionary.Count==0) 
        // ha még nincs a dictionary-ben:
        if (KuldemenyekDictionary.ContainsKey(KuldKuldemenyekId) == false)
        {
            KuldemenyekDictionary.Add(KuldKuldemenyekId, new Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz(
                           KuldKuldemenyekId
                           , kuldemenyRow["Kuldemeny_Allapot"].ToString()
                           , kuldemenyRow["IktatniKell"].ToString()
                           , kuldemenyRow["IraIrat_Id"].ToString()
                           , kuldemenyRow["Peldany_Id"].ToString()
                           , kuldemenyRow["FelhasznaloCsoport_Id_Orzo"].ToString()
                           , kuldemenyRow["Zarolo_Id"].ToString()
                           , kuldemenyRow["KuldesMod"].ToString()
                           , kuldemenyRow["Csoport_Id_Felelos"].ToString()
                           ));
        }

        // Küldemény treenode-nál: Checked=true (CheckBox nem látható)
        TreeNode tr = new TreeNode(KuldKuldemenyekText, KuldKuldemenyekId);
        tr.PopulateOnDemand = true;
        tr.Checked = true;
        //tr.Expanded = false;
        tr.Expanded = true;

        return tr;
    }


    private string GetTreeNodeText_Kuldemeny(DataRow row, TreeNode parentTreeNode, string KuldKuldemenyekId)
    {
        string FullErkeztetoSzam = row["FullErkeztetoSzam"].ToString();
        string AllapotNev = row["AllapotNev"].ToString();
        string NevSTR_Bekuldo = row["NevSTR_Bekuldo"].ToString();
        string CimSTR_Bekuldo = row["CimSTR_Bekuldo"].ToString();

        StringBuilder kuldemenyText_sb = new StringBuilder();

        kuldemenyText_sb.Append("<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_kuldemeny.jpg' alt='Küldemény (Bejövõ iratpéldány)' />", KuldKuldemenyekId, parentTreeNode));

        //LZS - BUG_8793
        int tmpWidth = SetFixedColumnWidthForKuldemeny(parentTreeNode);

        kuldemenyText_sb.AppendFormat("&nbsp;</td> <td style='width: {0}px;'>", tmpWidth/*GetWidth(parentTreeNode)*/);
        kuldemenyText_sb.Append(SetTreeNodeUrl("<b>" + FullErkeztetoSzam + "</b>", KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td><td style='width: 100px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(AllapotNev, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 105px;'>");
        kuldemenyText_sb.Append("</td> <td style='width: 300px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(" <b>Küldõ/feladó: </b>" + NevSTR_Bekuldo + ", " + CimSTR_Bekuldo, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 75px;'>");
        kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.Kuldemeny, parentTreeNode));
        kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(row, Constants.TableNames.EREC_KuldKuldemenyek));
        kuldemenyText_sb.Append("</td></tr></table>");

        return kuldemenyText_sb.ToString();
    }



    private string GetTreeNodeText_KapcsoltKuldemeny(DataRow row, TreeNode parentTreeNode, string KuldKuldemenyekId)
    {
        string FullErkeztetoSzam = row["FullErkeztetoSzam"].ToString();
        string AllapotNev = row["AllapotNev"].ToString();
        string NevSTR_Bekuldo = row["NevSTR_Bekuldo"].ToString();
        string CimSTR_Bekuldo = row["CimSTR_Bekuldo"].ToString();

        StringBuilder kuldemenyText_sb = new StringBuilder();

        kuldemenyText_sb.Append("<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/csatolt_kuldemeny.gif' alt='Kapcsolt küldemény' height='" + imageHeight + "' />", KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.AppendFormat("&nbsp;</td> <td style='width: {0}px;'>", GetWidth(parentTreeNode));
        kuldemenyText_sb.Append(SetTreeNodeUrl("<b>" + FullErkeztetoSzam + "</b>", KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td><td style='width: 100px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(AllapotNev, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 105px;'>");
        kuldemenyText_sb.Append("</td> <td style='width: 300px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(" <b>Küldõ/feladó: </b>" + NevSTR_Bekuldo + ", " + CimSTR_Bekuldo, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 75px;'>");
        kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.Kuldemeny, parentTreeNode));
        kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(row, Constants.TableNames.EREC_KuldKuldemenyek));
        kuldemenyText_sb.Append("</td></tr></table>");

        return kuldemenyText_sb.ToString();
    }

    private string GetTreeNodeText_IratPeldany(DataRow row, TreeNode parentTreeNode, string PldIratPeldanyokId)
    {
        string Sorszam = row["Sorszam"].ToString();
        string Allapot_Nev = row["Allapot_Nev"].ToString();
        string allapot = row["Allapot"].ToString();
        string NevSTR_Cimzett = row["NevSTR_Cimzett"].ToString();
        string CimSTR_Cimzett = row["CimSTR_Cimzett"].ToString();

        //LZS - BUG_8793
        int fixWidth = 130;

        StringBuilder pldText_sb = new StringBuilder();
        pldText_sb.Append("<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>");
        pldText_sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_belsoirat.jpg' alt='Belsõ iratpéldány' />", PldIratPeldanyokId, parentTreeNode));
        pldText_sb.AppendFormat("&nbsp;</td> <td style='width: {0}px;'> <b>", fixWidth);//GetWidth(parentTreeNode));
        pldText_sb.Append(SetTreeNodeUrl(Sorszam + ". példány </b>", PldIratPeldanyokId, parentTreeNode));
        pldText_sb.Append("</td><td style='width: 100px;'>");
        string allapotHTML = Allapot_Nev;
        if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
        {
            allapotHTML = "<span style=\"color:Red;font-weight:bold;\">" + Allapot_Nev + "</span>";
        }
        pldText_sb.Append(SetTreeNodeUrl(allapotHTML, PldIratPeldanyokId, parentTreeNode));
        pldText_sb.Append("</td> <td style='width: 105px;'>");
        pldText_sb.Append("</td> <td style='width: 300px;'>");
        pldText_sb.Append(SetTreeNodeUrl(" <b>Címzett: </b>" + NevSTR_Cimzett + ", " + CimSTR_Cimzett, PldIratPeldanyokId, parentTreeNode));
        //pldText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.IratPeldany, parentTreeNode)));
        pldText_sb.Append("</td> <td>");
        //pldText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getTertivevenyImage(row, parentTreeNode));
        pldText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(row, Constants.TableNames.EREC_PldIratPeldanyok));
        pldText_sb.Append("</td></tr></table>");

        return pldText_sb.ToString();
    }

    private string GetTreeNodeText_Kuldemeny_IratPld(DataRow row, TreeNode parentTreeNode, string KuldKuldemenyekId)
    {
        // string FullErkeztetoSzam = row["FullErkeztetoSzam"].ToString();
        string AllapotNev = row["Kuldemeny_Allapot_Nev"].ToString();
        string allapot = row["Kuldemeny_Allapot"].ToString();
        string NevSTR_Kikuldo = row["Csoport_Id_CimzettNev"].ToString();
        string NevSTR_Cimzett = row["NevSTR_Bekuldo"].ToString();
        string CimSTR_Cimzett = row["CimSTR_Bekuldo"].ToString();
        string Post_datum = row["BelyegzoDatuma"].ToString();

        StringBuilder kuldemenyText_sb = new StringBuilder();

        kuldemenyText_sb.Append("<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl("<img src='images/hu/egyeb/treeview_kuldemeny.jpg' alt='Küldemény (Iratpéldányhoz rendelt)' />", KuldKuldemenyekId, parentTreeNode));

        //kuldemenyText_sb.AppendFormat("&nbsp;</td> <td style='width: {0}px;'>", GetWidth(parentTreeNode));
        //kuldemenyText_sb.Append("</td><td style='width: 100px;'>");
        kuldemenyText_sb.Append("&nbsp;</td> <td style='width: 170px;'>");
        //   kuldemenyText_sb.Append(SetTreeNodeUrl(AllapotNev, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append(SetTreeNodeUrl(" <b>Címzett: </b>" + NevSTR_Cimzett + ", <br/>" + CimSTR_Cimzett, KuldKuldemenyekId, parentTreeNode));

        kuldemenyText_sb.Append("</td> <td style='width: 98px;'>");
        string allapotHTML = "";
        if (allapot == KodTarak.KULDEMENY_ALLAPOT.Sztorno)
        {
            allapotHTML = "<span style=\"color:Red;font-weight:bold;\">" + AllapotNev + "</span>";
        }
        kuldemenyText_sb.Append(SetTreeNodeUrl(allapotHTML, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 105px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(" <b>Küldõ/feladó: </b><br/>" + NevSTR_Kikuldo, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td style='width: 75px;'>");
        kuldemenyText_sb.Append(SetTreeNodeUrl(" <b>Postázás: </b>" + Post_datum, KuldKuldemenyekId, parentTreeNode));
        kuldemenyText_sb.Append("</td> <td >");
        kuldemenyText_sb.Append("</td><td >" + this.getTertivevenyImage(row, parentTreeNode));
        //kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getCsatolmanyImage(row, Constants.UgyiratTerkepNodeTypes.Kuldemeny, parentTreeNode));
        //kuldemenyText_sb.Append("</td><td style=\"width:" + imageWidth + ";\">" + this.getFeladatImage(row, Constants.TableNames.EREC_KuldKuldemenyek));
        kuldemenyText_sb.Append("</td></tr></table>");

        return kuldemenyText_sb.ToString();
    }
    // TreeView frissítése a kijelölt sor szintjéig
    private void RefreshSelectedNode()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }

        int selectedTreeNodeType = getNodeTypeById(selectedTreeNode.Value);

        switch (selectedTreeNodeType)
        {
            case Constants.UgyiratTerkepNodeTypes.Ugyirat:
                #region Ügyiratok:

                //string ugyiratId = selectedTreeNode.Value;

                // A fõ ügyiratra frissítünk mindig, nem az elõ- vagy utóiratra
                string ugyiratId = TreeView1.Nodes[0].Value;

                TreeView1.Nodes.Clear();

                LoadTreeView(Constants.ParentForms.Ugyirat, ugyiratId);

                //// Megjegyezzük az ügyirat node gyerekeit, és újratöltés után visszakapcsoljuk rá:
                //TreeNodeCollection ugyiratChildNodes = selectedTreeNode.ChildNodes;

                //// TreeView nodejainak törlése, újratöltése
                //TreeView1.Nodes.Clear();

                //selectedTreeNode = AddUgyiratNode(ugyiratId);

                //// Az elmentett gyerekeinek hozzákapcsolása az új ügyiratnode-hoz
                //if (selectedTreeNode.ChildNodes.Count == 0)
                //{
                //    foreach (TreeNode childNode in ugyiratChildNodes)
                //    {
                //        selectedTreeNode.ChildNodes.Add(childNode);
                //    }
                //}
                //// node kinyitása, ha van alatta más node
                //if (selectedTreeNode.ChildNodes.Count > 0)
                //{
                //    selectedTreeNode.Expanded = true;
                //}

                //// ügyirat node újbóli kiválasztása --> funkciógombok beállítása
                //selectedTreeNode.Select();
                #endregion
                break;
            case Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                #region ÜgyiratDarabok:

                string ugyiratDarabId = selectedTreeNode.Value;
                TreeView1.Nodes.Clear();
                LoadTreeView(Constants.ParentForms.UgyiratDarab, ugyiratDarabId);

                #endregion
                break;
            case Constants.UgyiratTerkepNodeTypes.Irat:
                #region Iratok:

                string iratId = selectedTreeNode.Value;
                TreeView1.Nodes.Clear();
                LoadTreeView(Constants.ParentForms.IraIrat, iratId);

                #endregion
                break;
            case Constants.UgyiratTerkepNodeTypes.IratPeldany:
            case Constants.UgyiratTerkepNodeTypes.Kuldemeny:
                // TreeNode.Checked érték dönti el, hogy küldemény, vagy iratpéldány
                if (selectedTreeNodeType == Constants.UgyiratTerkepNodeTypes.Kuldemeny)
                {
                    // TODO: megvalósítani a lekérést (LoadTreeView -> GetAllUgyiratTerkepDataSets)
                    // Jelenleg ez az ág nincs kezelve a webszervízben és hibát okozna.
                    // Mivel úgyis csak megtekintésre nyitható, nem csinálunk semmit
                    return;

                    //#region Küldemény:

                    //string kuldemenyId = selectedTreeNode.Value;
                    //TreeView1.Nodes.Clear();
                    //LoadTreeView(Constants.ParentForms.Kuldemeny, kuldemenyId);

                    //#endregion
                }
                else
                {
                    #region Iratpéldányok:

                    string iratPeldanyId = selectedTreeNode.Value;
                    TreeView1.Nodes.Clear();
                    LoadTreeView(Constants.ParentForms.IratPeldany, iratPeldanyId);

                    #endregion
                }
                break;
        }
    }

    private string SetTreeNodeUrl(string szoveg, string Id, TreeNode parentNode)
    {
        string parentNodeValuePath = "";
        if (parentNode != null)
        {
            parentNodeValuePath = parentNode.ValuePath.Replace("/", "\\\\") + "\\\\";
        }
        return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + parentNodeValuePath + Id + "');\">" + szoveg + "</a>";
    }

    //private String SetUgyUgyiratUrl(String szoveg, String Id)
    //{
    //    return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + Id + "');\">" + szoveg + "</a>";
    //}
    //private String SetUgyUgyiratdarabokUrl(String szoveg, String UgyiratId, String Id)
    //{
    //    return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + UgyiratId +  "\\\\" + Id + "');\">" + szoveg + "</a>";
    //}
    //private String SetIraIratokUrl(String szoveg, String UgyiratId, String UgyiratDarabId, String Id)
    //{
    //    return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + UgyiratId + "\\\\" + UgyiratDarabId + "\\\\" + Id + "');\">" + szoveg + "</a>";
    //}
    //private String SetPldIratPeldanyokUrl(String szoveg, String UgyiratId, String UgyiratDarabId, String IraIratId, String Id)
    //{
    //    return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + UgyiratId + "\\\\" + UgyiratDarabId + "\\\\" + IraIratId + "\\\\" + Id + "');\">" + szoveg + "</a>";
    //}

    /// <summary>
    /// Kijelölt ügyiratdarab törlése
    /// </summary>    
    protected void ImageButton_DeleteUgyiratDarab_Click(object sender, ImageClickEventArgs e)
    {
        if (TreeView1.Nodes.Count == 0)
        {
            return;
        }

        TreeNode ugyiratNode = TreeView1.Nodes[0];

        TreeNode selectedNode = TreeView1.SelectedNode;

        if (selectedNode == null || getNodeTypeById(selectedNode.Value) != Constants.UgyiratTerkepNodeTypes.UgyiratDarab)
        {
            // Nincs kijelölve ügyiratdarab
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.UINincsKivalasztvaUgyiratDarab);
            ErrorUpdatePanel1.Update();
            return;
        }
        else
        {
            if (selectedNode.Parent != ugyiratNode || String.IsNullOrEmpty(selectedNode.Value))
            {
                return;
            }

            string ugyiratDarab_Id = selectedNode.Value;

            // Invalidate:
            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = ugyiratDarab_Id;

            Result result = service.Invalidate(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
                return;
            }
            else
            {

                // TreeNode eltávolítása:
                ugyiratNode.ChildNodes.Remove(selectedNode);

                ugyiratNode.Select();

                // DropDown listából kivesszük:
                ListItem item = UgyiratDarabok_DropDownList.Items.FindByValue(ugyiratDarab_Id);
                if (item != null)
                {
                    UgyiratDarabok_DropDownList.Items.Remove(item);
                }

            }

        }
    }

    /// <summary>
    /// Új ügyiratdarab hozzáadása
    /// </summary>    
    protected void ImageButton_UjUgyiratDarab_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(UjUgyiratDarabNeve_TextBox.Text))
        {
            if (TreeView1.Nodes.Count == 0)
            {
                return;
            }

            TreeNode ugyiratNode = TreeView1.Nodes[0]; // Figyelni kell, hogy mindig az elsõ node legyen az eredeti ügyirat!!
            string ugyiratId = ugyiratNode.Value;

            // Db-be:
            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.CreateNewUgyiratDarab(execParam, ugyiratId, UjUgyiratDarabNeve_TextBox.Text);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
                return;
            }


            string ujUgyiratDarabId = result.Uid;


            string ugyiratDarabText = GetTreeNodeText_UgyiratDarab(UjUgyiratDarabNeve_TextBox.Text, ugyiratNode, ujUgyiratDarabId, false);

            TreeNode newUgyiratDarab_TreeNode = new TreeNode(ugyiratDarabText, ujUgyiratDarabId);

            ugyiratNode.ChildNodes.Add(newUgyiratDarab_TreeNode);

            newUgyiratDarab_TreeNode.Expand();
            newUgyiratDarab_TreeNode.Select();

            // DropDown Listába felvétel:
            UgyiratDarabok_DropDownList.Items.Add(new ListItem(UjUgyiratDarabNeve_TextBox.Text, ujUgyiratDarabId));


            // load to Dictionary 
            if (UgyiratDarabokDictionary.ContainsKey(ujUgyiratDarabId) == false)
            {
                UgyiratDarabokDictionary.Add(ujUgyiratDarabId,
                    new Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz(ujUgyiratDarabId, KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott, ugyiratId));
            }

            ViewState["UgyiratDarabokDictionary"] = UgyiratDarabokDictionary;


            UjUgyiratDarabNeve_TextBox.Text = "";
        }
        else
        {
            // nincs megadva az ügyiratdarab neve
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, "", Resources.Error.UINoUgyiratDarabNev);
            ErrorUpdatePanel1.Update();
        }

    }

    protected void Button_KivetelUgyiratDarabbol_Click(object sender, EventArgs e)
    {
        if (TreeView1.Nodes.Count == 0)
        {
            return;
        }

        TreeNode ugyiratNode = TreeView1.Nodes[0];
        TreeNode selectedNode = TreeView1.SelectedNode;



        if (selectedNode == null || getNodeTypeById(selectedNode.Value) != Constants.UgyiratTerkepNodeTypes.Irat)
        {
            // Nincs kijelölve irat
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.UINincsKivalasztvaIrat);
            ErrorUpdatePanel1.Update();
            return;
        }
        else
        {
            if (selectedNode.Parent.Parent != ugyiratNode || String.IsNullOrEmpty(selectedNode.Value))
            {
                return;
            }

            TreeNode defaultUgyiratDarabNode = ugyiratNode.ChildNodes[0];
            // a defaultnak Checked-ben kell lennie!!
            if (defaultUgyiratDarabNode.Checked == false)
            {
                return;
            }


            string src_Irat_Id = selectedNode.Value;
            string ugyiratDarab_Id_Cel = defaultUgyiratDarabNode.Value;

            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.IratAthelyezeseUgyiratDarabba(execParam, src_Irat_Id, ugyiratDarab_Id_Cel);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
                return;
            }

            // TreeView-ban is áthelyezzük:
            TreeNode celUgyiratDarabTreeNode = TreeView1.FindNode(ugyiratNode.ValuePath + "/" + ugyiratDarab_Id_Cel);
            if (celUgyiratDarabTreeNode != null)
            {
                selectedNode.Parent.ChildNodes.Remove(selectedNode);

                if (celUgyiratDarabTreeNode.Expanded == true)
                {
                    celUgyiratDarabTreeNode.ChildNodes.Add(selectedNode);
                    selectedNode.Select();
                }
            }


        }

    }

    /// <summary>
    /// Irat átmozgatása másik ügyiratdarabba
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button_Mozgat_Click(object sender, EventArgs e)
    {
        if (TreeView1.Nodes.Count == 0)
        {
            return;
        }

        TreeNode ugyiratNode = TreeView1.Nodes[0];

        TreeNode selectedNode = TreeView1.SelectedNode;

        if (selectedNode == null || getNodeTypeById(selectedNode.Value) != Constants.UgyiratTerkepNodeTypes.Irat)
        {
            // Nincs kijelölve irat
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.UINincsKivalasztvaIrat);
            ErrorUpdatePanel1.Update();
            return;
        }
        else
        {
            if (selectedNode.Parent.Parent != ugyiratNode || String.IsNullOrEmpty(selectedNode.Value)
                || UgyiratDarabok_DropDownList == null || String.IsNullOrEmpty(UgyiratDarabok_DropDownList.SelectedValue))
            {
                return;
            }

            string src_Irat_Id = selectedNode.Value;
            string ugyiratDarab_Id_Cel = UgyiratDarabok_DropDownList.SelectedValue;

            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.IratAthelyezeseUgyiratDarabba(execParam, src_Irat_Id, ugyiratDarab_Id_Cel);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
                return;
            }

            // TreeView-ban is áthelyezzük:
            TreeNode celUgyiratDarabTreeNode = TreeView1.FindNode(ugyiratNode.ValuePath + "/" + ugyiratDarab_Id_Cel);
            if (celUgyiratDarabTreeNode != null)
            {
                selectedNode.Parent.ChildNodes.Remove(selectedNode);

                if (celUgyiratDarabTreeNode.Expanded == true)
                {
                    celUgyiratDarabTreeNode.ChildNodes.Add(selectedNode);
                    selectedNode.Select();
                }
            }
        }
    }

    // Funkciógombok megnyomásának lekezelése
    protected void FunctionButtons_Click(object sender, ImageClickEventArgs e)
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;
        if (selectedTreeNode == null)
        {
            return;
        }

        int selectedTreeNodeType = getNodeTypeById(selectedTreeNode.Value);
        StringBuilder selectedId = new StringBuilder();
        selectedId.Append(selectedTreeNode.Value);
        string js = string.Empty;

        switch ((sender as ImageButton).CommandName)
        {
            case CommandName.Sztorno:
                if (selectedTreeNodeType == Constants.UgyiratTerkepNodeTypes.Ugyirat)
                {
                    // Ügyirat
                    Ugyiratok.Sztornozas(selectedTreeNode.Value, SztornoPopup.SztornoIndoka, Page, EErrorPanel1, ErrorUpdatePanel1);
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                }
                else if (selectedTreeNodeType == Constants.UgyiratTerkepNodeTypes.IratPeldany)
                {
                    // Iratpéldány
                    IratPeldanyok.Sztornozas(selectedTreeNode.Value, SztornoPopup.SztornoIndoka, Page, EErrorPanel1, ErrorUpdatePanel1);
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                }
                break;
                //case CommandName.Felszabaditas:
                //    if (selectedTreeNodeType == Constants.UgyiratTerkepNodeTypes.Irat)
                //    {
                //        // Irat
                //        Iratok.Felszabaditas(selectedTreeNode.Value, Page, EErrorPanel1, ErrorUpdatePanel1);
                //        RefreshSelectedNode();
                //        RaiseEvent_OnChangedObjectProperties();
                //    }
                //    break;
                //case CommandName.Postazas:
                //    if (selectedTreeNodeType == Constants.UgyiratTerkepNodeTypes.IratPeldany)
                //    {
                //        // Iratpéldány
                //        IratPeldanyok.Postazas(selectedTreeNode.Value, Page, EErrorPanel1, ErrorUpdatePanel1);
                //        RefreshSelectedNode();
                //    }
                //    break;
        }
    }

    /// <summary>
    /// Szerles Viszzavonása
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SzerelesVisszavonasa_Click(object sender, EventArgs e)
    {
        Logger.Debug("SzerelesVisszavonasa_Click start");
        if (TreeView1.Nodes.Count == 0)
        {
            Logger.Debug("TreeView1.Nodes.Count == 0");
            return;
        }

        TreeNode selectedNode = TreeView1.SelectedNode;

        if (selectedNode == null || getNodeTypeById(selectedNode.Value) != Constants.UgyiratTerkepNodeTypes.Ugyirat)
        {
            // Nincs kijelölve ügyirat
            if (selectedNode == null) Logger.Debug("selectedNode == null");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "", Resources.Error.UINincsKivalasztvaUgyirat);
            ErrorUpdatePanel1.Update();
            return;
        }
        else
        {
            SelectedId = selectedNode.Value;
            Logger.Debug("SelectedId: " + SelectedId);

            Contentum.eRecord.Utility.Ugyiratok.Statusz UgyiratokStatusz = null;
            UgyiratDictionary.TryGetValue(SelectedId, out UgyiratokStatusz);
            if (UgyiratokStatusz == null)
            {
                Logger.Debug("UgyiratokStatusz == null");
                return;
            }

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            Result res = new Result();

            if (UgyiratokStatusz.Allapot == UgyiratTipus.EloIratMigralt.ToString())
            {
                Logger.Debug("Regi adat szerelesenek visszavonasa");
                string szulo_uygirat_Id = TreeView1.Nodes[0].Value;
                Logger.Debug("Szulo_Ugyirat_Id: " + szulo_uygirat_Id);
                string visszavonando_ugyirat_azonosito = SelectedId;
                Logger.Debug("Visszavonando_Ugyirat_Azonosito: " + visszavonando_ugyirat_azonosito);
                res = service.RegiAdatSzerelesVisszavonasa(xpm, szulo_uygirat_Id, visszavonando_ugyirat_azonosito);
            }
            else
            {
                Logger.Debug("Idei adat szerelesenek visszavonasa");
                res = service.SzerelesVisszavonasa(xpm, SelectedId);
            }

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel1.Update();
                return;
            }

            //TreeView.ban is törlés

            TreeNode parentNode = selectedNode.Parent;
            if (parentNode != null)
            {
                Logger.Debug("parentNode.ChildNodes.Remove(selectedNode);");
                parentNode.ChildNodes.Remove(selectedNode);
                Logger.Debug("parentNode.Select();");
                parentNode.Select();
                Logger.Debug("TreeView1_SelectedNodeChanged(TreeView1, EventArgs.Empty);");
                TreeView1_SelectedNodeChanged(TreeView1, EventArgs.Empty);
            }
        }

        Logger.Debug("SzerelesVisszavonasa_Click end");
    }

    private int SetFixedColumnWidthForKuldemeny(TreeNode parentTreeNode)
    {
        //LZS - BUG_8793
        int fixedWidth = 0;
        int fixWide = 130;
        int width = 170;

        if (GetWidth(parentTreeNode) >= width)
            fixedWidth = fixWide;

        return fixedWidth;
    }

    private int SetFixedColumnWidthForUgyiratRow(DataRow dataRow_ugyirat, TreeNode parentTreeNode)
    {
        //LZS - BUG_8793
        int fixWidth = 0;
        int fixWide = 135;
        int fixWider = 165;
        int fixWidest = 180;
        int ColumnWidth1 = 250;
        int ColumnWidth2 = 230;

        if (GetWidth(parentTreeNode) >= ColumnWidth1)
            fixWidth = fixWidest;
        else if (GetWidth(parentTreeNode) <= ColumnWidth2)
        {
            if (parentTreeNode != null)
            {
                if (!String.IsNullOrEmpty(dataRow_ugyirat["UgyUgyirat_Id_Szulo"].ToString())
            && dataRow_ugyirat["Allapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.Szerelt
            && dataRow_ugyirat["TovabbitasAlattAllapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                {
                    fixWidth = fixWide;
                }
                else
                {
                    fixWidth = fixWider;
                }
            }
            else
            {
                fixWidth = fixWide;
            }
        }
        else
            fixWidth = fixWide;

        return fixWidth;
    }


    protected void TreeView1_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
        // nekrisz CR 3023 : Ha - jellel csukták be akkor nem ez lenne a Selected, ami a nyomógombos kinyításkor gondot jelent
        e.Node.Selected = true;

    }

    [Serializable]
    class IratokStatuszExtended : Contentum.eRecord.BaseUtility.Iratok.Statusz
    {
        public string PostazasIranya { get; set; }
        public IratokStatuszExtended(string _Id, string _Allapot, string _UgyiratDarab_Id, string UgyiratId
                , string _FelhCsopId_Iktato, string _ElsoIratPeldanyOrzo_Id, string _ElsoIratPeldanyAllapot, string _alszam, string postazasIranya)
            : base(_Id, _Allapot, _UgyiratDarab_Id, UgyiratId, _FelhCsopId_Iktato, _ElsoIratPeldanyOrzo_Id, _ElsoIratPeldanyAllapot, _alszam)
        {
            this.PostazasIranya = postazasIranya;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

    }
}
