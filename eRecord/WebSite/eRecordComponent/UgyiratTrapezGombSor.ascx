<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratTrapezGombSor.ascx.cs" 
Inherits="eRecordComponent_UgyiratTrapezGombSor" %>

<asp:UpdatePanel ID="_FunkcioGombsorUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="_FunkcioGombsorPanel" runat="server">
                <asp:ImageButton TabIndex = "1" ID="Megtekintes" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/megtekintes_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Megtekintes" AlternateText="<%$Resources:Buttons,Megtekintes %>" />        
                <asp:ImageButton TabIndex = "2" ID="Modositas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/modositas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Modositas" AlternateText="<%$Resources:Buttons,Modositas %>" />        
                <asp:ImageButton TabIndex = "3" ID="Letrehozas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/letrehozas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Letrehozas" AlternateText="<%$Resources:Buttons,Letrehozas %>" />
                <asp:ImageButton TabIndex = "4" ID="Sztornozas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/sztornozas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Sztornozas" AlternateText="<%$Resources:Buttons,Sztornozas %>" />                    
                <asp:ImageButton TabIndex = "5" ID="Valasz" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/szignalas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Valasz" AlternateText="<%$Resources:Buttons,Valasz %>" />
                <asp:ImageButton TabIndex = "6" ID="Felszabaditas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/felszabaditas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Felszabaditas" AlternateText="<%$Resources:Buttons,Felszabaditas %>" />
                <asp:ImageButton TabIndex = "7" ID="Postazas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/postazas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Postazas" AlternateText="<%$Resources:Buttons,Postazas %>" />    
                <asp:ImageButton TabIndex = "8" ID="Expedialas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/expedialas_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Expedialas" AlternateText="<%$Resources:Buttons,Expedialas %>" />
                <asp:ImageButton TabIndex = "9" ID="Borito_a4" CssClass="highlightit" runat="server" ImageUrl="../images/hu/trapezgomb/borito_a4_trap.jpg"
                    OnClick="ImageButton_Click" CommandName="Borito_a4" AlternateText="<%$Resources:Buttons,Borito_a4 %>" />
                <asp:ImageButton TabIndex = "10" ID="Iratfordulat" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/trapezgomb/iratfordulat_trap.jpg" OnClick="ImageButton_Click"
                    CommandName="Iratfordulat" AlternateText="<%$Resources:Buttons,Iratfordulat %>" />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
