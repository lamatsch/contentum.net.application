using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;

public partial class eRecordComponent_UgyiratTrapezGombSor : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Gomb megnyomásakor funkció kezelése.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case "Megtekintes":
                break;
            case "Modositas":
                break;
            case "Letrehozas":
                break;
            case "Sztornozas":
                break;
            case "Valasz":
                break;
            case "Felszabaditas":
                break;
            case "Postazas":
                break;
            case "Expedialas":
                break;
            case "Borito_a4":
                break;
            case "Iratfordulat":
                break;

        }
    }
    /* UgyiratFunkcioGombsor
        Megtekintes Modositas Letrehozas Sztornozas Valasz Felszabaditas
        Postazas Expedialas Borito_a4 Iratfordulat
    
     * "Megtekintes", "Modositas", "Letrehozas", "Sztornozas", "Valasz", "Felszabaditas",
        "Postazas", "Expedialas", "Borito_a4", "Iratfordulat";
    */

    /// <summary>
    /// Buttonses the visible.
    /// </summary>
    /// <param name="value"><c>true</c> látszanak a gombok.</param>
    private void ButtonsVisible(Boolean value)
    {
        Megtekintes.Visible = value;
        Modositas.Visible = value;
        Letrehozas.Visible = value;
        Sztornozas.Visible = value;
        Valasz.Visible = value;
        Felszabaditas.Visible = value;
        Postazas.Visible = value;
        Expedialas.Visible = value;
        Borito_a4.Visible = value;
        Iratfordulat.Visible = value;
    }

    /// <summary>
    /// Gombok engedélyezése
    /// A gombok látszanak de "halványak"
    /// </summary>
    /// <param name="value"><c>true</c> engedélyezettek a gombok</param>
    private void ButtonsEnable(Boolean value)
    {
        Megtekintes.Enabled = value;
        Megtekintes.CssClass = (value ? "highlightit" : "disableditem");
        Modositas.Enabled = value;
        Modositas.CssClass = (value ? "highlightit" : "disableditem");
        Letrehozas.Enabled = value;
        Letrehozas.CssClass = (value ? "highlightit" : "disableditem");
        Sztornozas.Enabled = value;
        Sztornozas.CssClass = (value ? "highlightit" : "disableditem");
        Valasz.Enabled = value;
        Valasz.CssClass = (value ? "highlightit" : "disableditem");
        Felszabaditas.Enabled = value;
        Felszabaditas.CssClass = (value ? "highlightit" : "disableditem");
        Postazas.Enabled = value;
        Postazas.CssClass = (value ? "highlightit" : "disableditem");
        Expedialas.Enabled = value;
        Expedialas.CssClass = (value ? "highlightit" : "disableditem");
        Borito_a4.Enabled = value;
        Borito_a4.CssClass = (value ? "highlightit" : "disableditem");
        Iratfordulat.Enabled = value;
        Iratfordulat.CssClass = (value ? "highlightit" : "disableditem");

    }

    public void HideButtons()
    {
        this.ButtonsVisible(false);
    }

    public void ShowButtons()
    {
        this.ButtonsVisible(true);
    }

    public void DisableButtons()
    {
        this.ButtonsEnable(false);
    }

    public void EnableButtons()
    {
        this.ButtonsEnable(true);
    }

    #region Megtekintes
    public bool MegtekintesEnabled
    {
        get { return Megtekintes.Enabled; }
        set
        {
            Megtekintes.Enabled = value;
            Megtekintes.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool MegtekintesVisible
    {
        get { return Megtekintes.Visible; }
        set
        {
            Megtekintes.Visible = value;
        }
    }
    public String MegtekintesOnClientClick
    {
        get { return Megtekintes.OnClientClick; }
        set { MegtekintesOnClientClick = value; }
    }
    #endregion

    #region Modositas
    public bool ModositasEnabled
    {
        get { return Modositas.Enabled; }
        set
        {
            Modositas.Enabled = value;
            Modositas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool ModositasVisible
    {
        get { return Modositas.Visible; }
        set
        {
            Modositas.Visible = value;
        }
    }
    public String ModositasOnClientClick
    {
        get { return Modositas.OnClientClick; }
        set { Modositas.OnClientClick = value; }
    }
    #endregion

    #region Letrehozas
    public bool LetrehozasEnabled
    {
        get { return Letrehozas.Enabled; }
        set
        {
            Letrehozas.Enabled = value;
            Letrehozas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool LetrehozasVisible
    {
        get { return Letrehozas.Visible; }
        set
        {
            Letrehozas.Visible = value;
        }
    }
    public String LetrehozasOnClientClick
    {
        get { return Letrehozas.OnClientClick; }
        set { Letrehozas.OnClientClick = value; }
    }
    #endregion

    #region Sztornozas
    public bool SztornozasEnabled
    {
        get { return Sztornozas.Enabled; }
        set
        {
            Sztornozas.Enabled = value;
            Sztornozas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool SztornozasVisible
    {
        get { return Sztornozas.Visible; }
        set
        {
            Sztornozas.Visible = value;
        }
    }
    public String SztornozasOnClientClick
    {
        get { return Sztornozas.OnClientClick; }
        set { SztornozasOnClientClick = value; }
    }
    #endregion

    #region Valasz
    public bool ValaszEnabled
    {
        get { return Valasz.Enabled; }
        set
        {
            Valasz.Enabled = value;
            Valasz.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool ValaszVisible
    {
        get { return Valasz.Visible; }
        set
        {
            Valasz.Visible = value;
        }
    }
    public String ValaszOnClientClick
    {
        get { return Valasz.OnClientClick; }
        set { ValaszOnClientClick = value; }
    }
    #endregion

    #region Felszabaditas
    public bool FelszabaditasEnabled
    {
        get { return Felszabaditas.Enabled; }
        set
        {
            Felszabaditas.Enabled = value;
            Felszabaditas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool FelszabaditasVisible
    {
        get { return Felszabaditas.Visible; }
        set
        {
            Felszabaditas.Visible = value;
        }
    }
    public String FelszabaditasOnClientClick
    {
        get { return Felszabaditas.OnClientClick; }
        set { FelszabaditasOnClientClick = value; }
    }
    #endregion

    #region Postazas
    public bool PostazasEnabled
    {
        get { return Postazas.Enabled; }
        set
        {
            Postazas.Enabled = value;
            Postazas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool PostazasVisible
    {
        get { return Postazas.Visible; }
        set
        {
            Postazas.Visible = value;
        }
    }
    public String PostazasOnClientClick
    {
        get { return Postazas.OnClientClick; }
        set { PostazasOnClientClick = value; }
    }
    #endregion

    #region Expedialas
    public bool ExpedialasEnabled
    {
        get { return Expedialas.Enabled; }
        set
        {
            Expedialas.Enabled = value;
            Expedialas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool ExpedialasVisible
    {
        get { return Expedialas.Visible; }
        set
        {
            Expedialas.Visible = value;
        }
    }
    public String ExpedialasOnClientClick
    {
        get { return Expedialas.OnClientClick; }
        set { ExpedialasOnClientClick = value; }
    }
    #endregion

    #region Borito_a4
    public bool Borito_a4Enabled
    {
        get { return Borito_a4.Enabled; }
        set
        {
            Borito_a4.Enabled = value;
            Borito_a4.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Borito_a4Visible
    {
        get { return Borito_a4.Visible; }
        set
        {
            Borito_a4.Visible = value;
        }
    }
    public String Borito_a4OnClientClick
    {
        get { return Borito_a4.OnClientClick; }
        set { Borito_a4OnClientClick = value; }
    }
    #endregion

    #region Iratfordulat
    public bool IratfordulatEnabled
    {
        get { return Iratfordulat.Enabled; }
        set
        {
            Iratfordulat.Enabled = value;
            Iratfordulat.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool IratfordulatVisible
    {
        get { return Iratfordulat.Visible; }
        set
        {
            Iratfordulat.Visible = value;
        }
    }
    public String IratfordulatOnClientClick
    {
        get { return Iratfordulat.OnClientClick; }
        set { IratfordulatOnClientClick = value; }
    }
    #endregion


}
