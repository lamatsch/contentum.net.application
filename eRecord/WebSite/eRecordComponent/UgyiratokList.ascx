﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratokList.ascx.cs" Inherits="eRecordComponent_UgyiratokList" %>

<%@ Register Src="../Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="TeljessegEllenorzesResultPanel.ascx" TagName="TeljessegEllenorzesReultPanel" TagPrefix="uc2" %>
<%@ Register Src="DosszieTreeView.ascx" TagName="DosszieTreeView" TagPrefix="dtv" %>
<%@ Register Src="~/eRecordComponent/SztornoPopup.ascx" TagName="SztornoPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%@ Import Namespace="Contentum.eRecord.Utility" %>

<style type="text/css">
    .preformatted {
        white-space: pre-line;
    }
</style>

<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--HiddenFields--%>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="MessageHiddenField" runat="server" />
<asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
<%--/HiddenFields--%>
<uc2:TeljessegEllenorzesReultPanel runat="server" ID="TeljessegEllenorzesResultPanel1" />
<%--Tablazat / Grid--%>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td rowspan="2" valign="top" align="left" id="PanelDossziekTD" runat="server">
            <div id="PanelDossziekDiv" style="position: relative;">
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="PanelDossziekTVPanel" runat="server">
                                <dtv:DosszieTreeView ID="DosszieTreeView" runat="server" />
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="DossziekCE" runat="server" TargetControlID="PanelDossziekTVPanel"
                                CollapsedSize="0" Collapsed="true"
                                AutoCollapse="false" AutoExpand="false"
                                ExpandDirection="Horizontal"
                                CollapsedImage="../images/hu/egyeb/dosszie_panel_ki.gif" ExpandedImage="../images/hu/egyeb/dosszie_panel_be.gif"
                                ImageControlID="DossziekCEButton"
                                ScrollContents="false">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                        <td valign="middle" style="background-image: url(images/hu/egyeb/dosszie_panel_hatter.jpg); background-repeat: repeat-y;" onclick="$find('<%=DossziekCE.ClientID %>').togglePanel();"
                            style="cursor: hand;">
                            <asp:ImageButton runat="server" ID="DossziekCEButton"
                                OnClientClick="return false;" ImageUrl="../images/hu/egyeb/dosszie_panel_ki.gif" />

                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton runat="server" ID="UgyUgyiratokCPEButton" ImageUrl="../images/hu/Grid/minus.gif"
                OnClientClick="return false;" />
        </td>
        <td style="text-align: left; vertical-align: top; width: 100%;">
            <asp:UpdatePanel ID="UgyUgyiratokUpdatePanel" runat="server" OnLoad="UgyUgyiratokUpdatePanel_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="UgyUgyiratokCPE" runat="server" TargetControlID="Panel1"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="UgyUgyiratokCPEButton"
                        CollapseControlID="UgyUgyiratokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                        AutoExpand="false" CollapsedImage="../images/hu/Grid/plus.gif" ExpandedImage="../images/hu/Grid/minus.gif"
                        ImageControlID="UgyUgyiratokCPEButton" ExpandedSize="0" ExpandedText="Ügyiratok listája"
                        CollapsedText="Ügyiratok listája">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="Panel1" runat="server">
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <uc:SztornoPopup runat="server" ID="SztornoPopup" />
                                    <uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
                                    <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                    <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                        OnRowCommand="UgyUgyiratokGridView_RowCommand" OnPreRender="UgyUgyiratokGridView_PreRender"
                                        OnSorting="UgyUgyiratokGridView_Sorting" OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                                        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <div class="DisableWrap">
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;
                                                       
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                SelectImageUrl="../images/hu/Grid/3Drafts.gif">
                                                <HeaderStyle Width="25px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <asp:TemplateField AccessibleHeaderText="Sz" ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Sz">
                                                <ItemTemplate>
                                                    <label id="szColumn"></label>
                                                    <asp:Image ID="SzereltImage" AlternateText="Szerelés" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="../images/hu/ikon/szerelt.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="Csatolmany" ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Csat.">
                                                <ItemTemplate>
                                                    <asp:Image ID="CsatoltImage" AlternateText="Csatolás" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="../images/hu/ikon/csatolt.gif" Style="cursor: pointer"
                                                        onmouseover="Utility.UI.SetElementOpacity(this,0.7);" onmouseout="Utility.UI.SetElementOpacity(this,1);" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="Csny" ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="Csny.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                        runat="server" ImageUrl="../images/hu/ikon/csatolmany.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField  AccessibleHeaderText="F" ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                HeaderStyle-Width="25px" HeaderText="F.">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="FeladatImage" AlternateText="Kezelési feljegyzés" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField AccessibleHeaderText="Iktatohely" DataField="IktatoHely" HeaderText="Iktatókönyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Foszam" DataField="Foszam" HeaderText="Fõszám" SortExpression="EREC_UgyUgyiratok.Foszam" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Ev" DataField="Ev" HeaderText="Év" SortExpression="EREC_IraIktatokonyvek.Ev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--BLG_619--%>
                                            <%--LZS - obsolate BUG_11081--%>
                                            <%--<asp:BoundField DataField="UgyFelelos_SzervezetKod" HeaderText="<%$Forditas:BoundFieldUgyFelelos_SzervezetKod|Szk%>" SortExpression="Csoportok_UgyFelelosNev.Kod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="20px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>

                                            <%----LZS - BUG_11081--%>
                                            <%-- A Note mezőből érkező JSON string feldolgozása a GETSZK() függvénnyel, ami a deserializálást végzi. --%>
                                            <asp:TemplateField AccessibleHeaderText="UgyfeleloSzervezetKod" HeaderText="<%$Forditas:BoundFieldUgyFelelos_SzervezetKod|Szk%>" SortExpression="">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="20px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelSZK" runat="server"
                                                        Text='<%# GetSZK( Eval("Note") as string, Eval("UgyFelelos_SzervezetKod") as string, Eval("Id").ToString() ) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField AccessibleHeaderText="Foszam_Merge" DataField="Foszam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Targy" DataField="Targy" HeaderText="Tárgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="UgyTipus_Nev" DataField="UgyTipus_Nev" HeaderText="Ügytípus" SortExpression="EREC_IratMetaDefinicio.UgytipusNev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="NevSTR_Ugyindito" DataField="NevSTR_Ugyindito" HeaderText="Ügyfél, ügyindító" Visible="false" SortExpression="EREC_UgyUgyiratok.NevSTR_Ugyindito">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Ugyindito_Cim" DataField="Ugyindito_Cim" HeaderText="Ügyindító címe" Visible="false" SortExpression="EREC_UgyUgyiratok.CimStr_Ugyindito">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Felelos_Nev" DataField="Felelos_Nev" HeaderText="Kezelõ" SortExpression="Csoportok_FelelosNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="BARCODE" DataField="BARCODE" HeaderText="Vonalkód" SortExpression="EREC_UgyUgyiratok.BARCODE">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%--LZS - BUG_7099 - obsolate
                                            Ügyiratok megjegyzés (EREC_UgyUgyiratok.Note) mező gridre helyezése--%>
                                            <%-- <asp:BoundField DataField="Note" HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="EREC_UgyUgyiratok.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <%----LZS - BUG_11081--%>
                                            <asp:TemplateField AccessibleHeaderText="Megjegyzes"  HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelNote" runat="server"
                                                        Text='<%# NoteJSONParser( Eval("Note") as string ) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="Merge_IrattariTetelszam"  AccessibleHeaderText="ITSZ" HeaderText="ITSZ" SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                            </asp:BoundField>
                                            <%--BLG_1014--%>
                                            <%-- <asp:BoundField DataField="Ugyintezo_Nev" HeaderText="Ügyintézõ" SortExpression="Csoportok_UgyintezoNev.Nev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <asp:BoundField AccessibleHeaderText="Ugyintezo_Nev" DataField="Ugyintezo_Nev" HeaderText="<%$Forditas:BoundField_UgyiratUgyintezo|Ügyintézõ%>" SortExpression="Csoportok_UgyintezoNev.Nev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Iktato" DataField="Iktato" HeaderText="Iktató" SortExpression="Csoportok_Iktato.Nev" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField AccessibleHeaderText="Allapot" HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <%-- skontróban esetén (07) skontró vége megjelenítése --%>
                                                    <asp:Label ID="labelAllapotNev" runat="server"
                                                        Text='<%#                                        
                                    string.Concat(                                          
                                      Eval("Allapot_Nev")
                                     , ((Eval("Allapot") as string) == "07" || (Eval("Allapot") as string) == "57") ? "<br />(" + Eval("SkontroVege_Rovid") + ")" : ""
                                     , (Eval("Foszam_Merge_Szulo") as string) != null ? 
                                            (((Eval("Allapot") as string) != "60" && (Eval("TovabbitasAlattAllapot") as string) != "60") 
                                            ? "<img src=\"images/hu/ikon/szerelt_piros.gif\" height=\"20px\" width=\"25px\" alt=\"Elõkészített szerelés\" />&nbsp;" : "")
                                            + "<a href=\"UgyUgyiratokList.aspx?Id="+ Eval("UgyUgyirat_Id_Szulo") as string + "\" style=\"text-decoration:underline\"> ("
                                            + Eval("Foszam_Merge_Szulo") as string +")<a />" : "",
                                            ((Eval("Allapot") as string) == "13" ? ("<br/>(" + (Eval("KolcsonzesDatuma") as string) + " - " + (Eval("KolcsonzesiHatarido") as string) + ")"): "")) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField AccessibleHeaderText="UgyintezesModja_Nev" DataField="UgyintezesModja_Nev" HeaderText="Ügyirat típusa" Visible="false" SortExpression="EREC_UgyUgyiratok.UgyintezesModja">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>

                                            <asp:BoundField AccessibleHeaderText="LetrehozasIdo_Rovid" DataField="LetrehozasIdo_Rovid" HeaderText="Ikt. idõpontja" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>

                                            <asp:TemplateField AccessibleHeaderText="Hatarido" ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" HeaderText="Ügyint. határidõ" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#ReformatDate(Eval("Hatarido", "{0}"), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd") %>'
                                                        Visible='<%# UgyintezesiHataridoLathatosag(Container.DataItem) %>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="ElteltUgyintezesiIdo" ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" HeaderText="Eltelt ügyint. idõ" SortExpression="EREC_UgyUgyiratok.ElteltIdo">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("ElteltUgyintezesiIdo") %>'
                                                        Visible='<%# SakkoraMezokLathatosaga(Container.DataItem) %>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--  <asp:BoundField DataField="UgyiratElintezesiHatarido" HeaderText="Elintézési határidõ">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <asp:BoundField AccessibleHeaderText="KRT_Csoportok_Orzo_Nev" DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>

                                            <asp:BoundField AccessibleHeaderText="LezarasDat" DataField="LezarasDat" HeaderText="Lezárás dátum" Visible="false" SortExpression="EREC_UgyUgyiratok.LezarasDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="LezarasOka_Nev" DataField="LezarasOka_Nev" HeaderText="Lezárás oka" Visible="false" SortExpression="EREC_UgyUgyiratok.LezarasDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%-- CR#2197: választható, alapból rejtett dátummezõk START --%>
                                            <%-- jelenleg tárolt eljárásban formázva a dátum adatok, egyébként pl. 
                                                DataFormatString="{0:yyyy.MM.dd}" HtmlEncodeFormatString="true" kellene --%>
                                            <asp:BoundField AccessibleHeaderText="SztornirozasDat" DataField="SztornirozasDat" HeaderText="Sztornó dátum" Visible="false" SortExpression="EREC_UgyUgyiratok.SztornirozasDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="SkontrobaDat" DataField="SkontrobaDat" HeaderText="Határidõbe t. idõpontja" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontrobaDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="SkontroVege_Rovid" DataField="SkontroVege_Rovid" HeaderText="Határidõbe t. lejárata" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontroVege">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="SkontrobanOsszesen" DataField="SkontrobanOsszesen" HeaderText="Határidõzés idõtartama" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontrobanOsszesen">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="IrattarbaKuldDatuma" DataField="IrattarbaKuldDatuma" HeaderText="Irattárba küldés" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaKuldDatuma">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="IrattarbaVetelDat" DataField="IrattarbaVetelDat" HeaderText="Irattárba vétel" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaVetelDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>

                                            <asp:BoundField AccessibleHeaderText="KolcsonzesDatuma" DataField="KolcsonzesDatuma" HeaderText="Kölcsönzés kezdete" Visible="false" SortExpression="EREC_IrattariKikero.KikerKezd">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="KolcsonzesiHatarido" DataField="KolcsonzesiHatarido" HeaderText="Kölcsönzés határideje" Visible="false" SortExpression="EREC_IrattariKikero.KikerVege">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="MegorzesiIdoVege" DataField="MegorzesiIdoVege" HeaderText="Megõrzési idõ vége" Visible="false" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="FelulvizsgalatDat" DataField="FelulvizsgalatDat" HeaderText="Felülvizsgálat" Visible="false" SortExpression="EREC_UgyUgyiratok.FelulvizsgalatDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="SelejtezesDat" DataField="SelejtezesDat" HeaderText="Selejtezve/ Levéltárba" Visible="false" SortExpression="EREC_UgyUgyiratok.SelejtezesDat">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <%-- CR#2197: választható, alapból rejtett dátummezõk END --%>
                                            <asp:TemplateField AccessibleHeaderText="Jelleg" HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="labelJelleg" runat="server"
                                                        Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                        ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="AlszamDb" HeaderText="Alszám/Db" SortExpression="EREC_UgyUgyiratok.UtolsoAlszam">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelUtolsoAlszam" runat="server"
                                                        Text='<%#String.Format("{0} / {1}",Eval("UtolsoAlszam"),Eval("IratSzam"))%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="UgyFajtaja" HeaderText="Ügy fajtája" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="labelUgyFajtajaCaption" runat="server" Text="Ü." ToolTip="Ügy fajtája" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="labelUgyFajtaja" runat="server"
                                                        Text='<%# UI.GetUgyFajtajaLabel(Page, Eval("Ugy_Fajtaja").ToString())%>'
                                                        ToolTip='<%# UI.GetUgyFajtajaToolTip(Page, Eval("Ugy_Fajtaja").ToString())%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField AccessibleHeaderText="SakkoraAllapotNev" DataField="SakkoraAllapotNev" HeaderText="Sakkóra" Visible="true" SortExpression="EREC_UgyUgyiratok.SakkoraAllapot">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField AccessibleHeaderText="IH" ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" HeaderText="IH">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# GetIratHatasaTipusIkon(Eval("SakkoraAllapotTipus")) %>' Font-Size="Large"
                                                        ToolTip='<%# Eval("SakkoraAllapotTipus") %>'
                                                        ForeColor='<%# GetIratHatasaTipusColor(Eval("SakkoraAllapotTipus")) %>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField AccessibleHeaderText="EljarasiSzakaszFokNev" DataField="EljarasiSzakaszFokNev" HeaderText="Eljárási fok" SortExpression="EljarasiSzakaszFokKodTarak.Nev">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Font-Size="X-Small" />
                                                <HeaderStyle CssClass="GridViewBorderHeader" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:TemplateField AccessibleHeaderText="HatralevoNapok" ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" HeaderText="Hátralévõ napok">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# HatralevoNapokSzovegesen(Eval("HatralevoNapok"),Eval("SakkoraAllapotTipus")) %>' Font-Size="X-Small"
                                                        ForeColor='<%# HatralevoNapokSzinesen(Eval("HatralevoNapok")) %>'
                                                        Visible='<%# SakkoraMezokLathatosaga(Container.DataItem) %>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField AccessibleHeaderText="HatralevoMunkanapok" ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" HeaderText="Hátralévõ munkanapok">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# HatralevoMunkaNapokSzovegesen(Eval("HatralevoMunkaNapok"),Eval("SakkoraAllapotTipus")) %>' Font-Size="X-Small"
                                                        ForeColor='<%# HatralevoNapokSzinesen(Eval("HatralevoMunkaNapok")) %>'
                                                        Visible='<%# SakkoraMezokLathatosaga(Container.DataItem) %>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField AccessibleHeaderText="UgyintezesIdeje_Nev" DataField="UgyintezesIdeje_Nev" HeaderText="<%$Resources:List,UgyintezesIdeje_Nev%>" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="KezelesTipusNev" DataField="KezelesTipusNev" HeaderText="Kezelési útasítás">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="FT1" DataField="FT1" HeaderText="FT1" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="FT2" DataField="FT2" HeaderText="FT2" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="FT3" DataField="FT3" HeaderText="FT3" Visible="false">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <ItemTemplate>
                                                    <input runat="server" type="button" id="ButtonLotusNotesPostBack" value="&#9852;" title="&#9852;"
                                                        visible='<%# VisiblePostBackCommandForLotusNotes() %>'
                                                        onclick='<%# GetPostBackCommandForLotusNotes(Container.DataItem) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="../images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="../images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle">
                                                <ItemTemplate>
                                                    <asp:Label ID="labelLetrehozasiIdo" runat="server" Text='<%#Eval("LetrehozasIdo") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
